﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI
{
    [TestFixture]
    public class LocationSpaceMaintenanceViewModelTests : TestBase<LocationSpaceMaintenanceViewModel>
    {
        #region Test Fixture Helpers

        private Entity _entity;

        public override void  Setup()
        {
            base.Setup();


            //insert test data
            _entity = Entity.FetchById(this.EntityId);

            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(base.DalFactory, 10, _entity.Id);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            productHierarchy = TestDataHelper.PopulateProductHierarchy(productHierarchy.Id);
            TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 5);

            //create some initial location space's to use
            LocationSpace locationSpace1 = LocationSpace.NewLocationSpace(this.EntityId);
            locationSpace1.LocationId = locationDtos[0].Id;

            //Create new product group record
            LocationSpaceProductGroup newProductGroup = LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            newProductGroup.ProductGroupId = productHierarchy.EnumerateAllGroups().First(p => p.IsRoot == false).Id;
            locationSpace1.LocationSpaceProductGroups.Add(newProductGroup);
            //Save version 1
            locationSpace1 = locationSpace1.Save();

            //create some initial location space's to use
            LocationSpace locationSpace2 = LocationSpace.NewLocationSpace(this.EntityId);
            locationSpace2.LocationId = locationDtos[1].Id;

            //Create new product group record
            LocationSpaceProductGroup newProductGroup2 = LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            newProductGroup2.ProductGroupId = productHierarchy.EnumerateAllGroups().Last(p => p.IsRoot == false).Id;
            locationSpace2.LocationSpaceProductGroups.Add(newProductGroup2);

            //save version 2
            locationSpace2 = locationSpace2.Save();

            this.TestModel = new LocationSpaceMaintenanceViewModel();
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //Ensure location info list has been populated
            Assert.IsNotNull(this.TestModel.MasterLocationInfoList);
            Assert.IsNotEmpty(this.TestModel.MasterLocationInfoList);

            //Ensure product hierarchy has been populated
            Assert.IsNotNull(this.TestModel.MasterProductHierarchyView);
            Assert.IsNotNull(this.TestModel.AvailableProductGroups);
            Assert.IsNotEmpty(this.TestModel.AvailableProductGroups);

            //Ensure other collections/objects are not null
            Assert.IsNotNull(this.TestModel.AvailableLocationSpaceInfos);

            //Ensure a new location space record has been created
            Assert.IsNotNull(this.TestModel.SelectedLocationSpace);
            Assert.IsTrue(this.TestModel.SelectedLocationSpace.IsNew);
        }

        #endregion

        #region Properties

        [Test]
        public void Property_MasterProductHierarchyView()
        {
            String propertyName = "MasterProductHierarchyView";

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.MasterProductHierarchyView);
        }

        [Test]
        public void Property_MasterLocationInfoList()
        {
            String propertyName = "MasterLocationInfoList";

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.MasterLocationInfoList);
        }

        [Test]
        public void Property_AvailableLocationSpaceInfos()
        {
            String propertyName = "AvailableLocationSpaceInfos";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.AvailableLocationSpaceInfosProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.AvailableLocationSpaceInfos);
        }

        [Test]
        public void Property_AvailableProductGroups()
        {
            String propertyName = "AvailableProductGroups";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.AvailableProductGroupsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.AvailableProductGroups);
        }

        [Test]
        public void Property_SelectedLocationSpace()
        {
            String propertyName = "SelectedLocationSpace";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProperty.Path);

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.SelectedLocationSpace);

            //Test setter
            Ccm.Model.LocationSpace newLocationSpace = Ccm.Model.LocationSpace.NewLocationSpace(this.EntityId);
            //Set indentifiable property
            newLocationSpace.LocationId = 56;
            this.TestModel.SelectedLocationSpace = newLocationSpace;
            //Check set has worked
            Assert.AreEqual(56, this.TestModel.SelectedLocationSpace.LocationId);
        }

        [Test]
        public void Property_AvailableLocationSpaceRows()
        {
            String propertyName = "AvailableLocationSpaceRows";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.AvailableLocationSpaceRowsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.AvailableLocationSpaceRows);
        }

        [Test]
        public void Property_SelectedLocationSpaceProductGroupRow()
        {
            String propertyName = "SelectedLocationSpaceProductGroupRow";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProductGroupRowProperty.Path);

            //Check initial value is null
            Assert.IsNull(this.TestModel.SelectedLocationSpaceProductGroupRow);

            Ccm.Model.LocationSpaceProductGroup productGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();

            //Test setter
            LocationSpaceProductGroupRowViewModel newRow = new LocationSpaceProductGroupRowViewModel(productGroup, new Galleria.Framework.Collections.BulkObservableCollection<String>());
            this.TestModel.SelectedLocationSpaceProductGroupRow = newRow;
            //Check set has worked
            Assert.AreEqual(newRow, this.TestModel.SelectedLocationSpaceProductGroupRow);
        }

        [Test]
        public void Property_CurrentLocationSpaceProductGroup()
        {
            String propertyName = "CurrentLocationSpaceProductGroup";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.CurrentLocationSpaceProductGroupProperty.Path);

            //Check initial value is null
            Assert.IsNull(this.TestModel.CurrentLocationSpaceProductGroup);

            //Test setter
            Ccm.Model.LocationSpaceProductGroup productGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();

            //Set indentifiable property
            productGroup.ProductGroupId = 14;
            this.TestModel.CurrentLocationSpaceProductGroup = productGroup;
            //Check set has worked
            Assert.AreEqual(14, this.TestModel.CurrentLocationSpaceProductGroup.ProductGroupId);
        }

        [Test]
        public void Property_SelectedLocationSpaceBay()
        {
            Random random = new Random();

            String propertyName = "SelectedLocationSpaceBay";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.SelectedLocationSpaceBayProperty.Path);

            //Check initial value is null
            Assert.IsNull(this.TestModel.SelectedLocationSpaceBay);

            //Test setter
            Ccm.Model.LocationSpaceBay newBay = Ccm.Model.LocationSpaceBay.NewLocationSpaceBay();

            //Set indentifiable property
            Byte randomOrder = Convert.ToByte(random.Next(1, 255));
            newBay.Order = randomOrder;
            this.TestModel.SelectedLocationSpaceBay = newBay;
            //Check set has worked
            Assert.AreEqual(randomOrder, this.TestModel.SelectedLocationSpaceBay.Order);
        }

        [Test]
        public void Property_SelectedLocationSpaceElement()
        {
            Random random = new Random();

            String propertyName = "SelectedLocationSpaceElement";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.SelectedLocationSpaceElementProperty.Path);

            //Check initial value is null
            Assert.IsNull(this.TestModel.SelectedLocationSpaceElement);

            //Test setter
            Ccm.Model.LocationSpaceElement newElement = Ccm.Model.LocationSpaceElement.NewLocationSpaceElement();

            //Set indentifiable property
            Byte randomOrder = Convert.ToByte(random.Next(1, 255));
            newElement.Order = randomOrder;
            this.TestModel.SelectedLocationSpaceElement = newElement;
            //Check set has worked
            Assert.AreEqual(randomOrder, this.TestModel.SelectedLocationSpaceElement.Order);
        }

        [Test]
        public void Property_ViewType()
        {
            String propertyName = "ViewType";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.ViewTypeProperty.Path);

            //Check initial value is null
            Assert.IsNotNull(this.TestModel.ViewType);
            Assert.AreEqual(LocationSpaceViewType.Grid, this.TestModel.ViewType, "Grid should be initially selected");

            //Test setter
            this.TestModel.ViewType = LocationSpaceViewType.Grid;

            //Check set has worked
            Assert.AreEqual(LocationSpaceViewType.Grid, this.TestModel.ViewType);
        }


        [Test]
        public void Property_SelectedLocationSpaceTitle()
        {
            String propertyName = "SelectedLocationSpaceTitle";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.SelectedLocationSpaceTitleProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Select location
            LocationInfo locInfo = this.TestModel.MasterLocationInfoList.First();
            this.TestModel.SelectedLocationSpace.LocationId = locInfo.Id;

            this.TestModel.OnPropertyChanged(LocationSpaceMaintenanceViewModel.SelectedLocationSpaceTitleProperty);

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.LocationSpacePropertiesTitle);
            //Check it contains correct details
            Assert.IsTrue(this.TestModel.LocationSpacePropertiesTitle.Contains(locInfo.Name));
            Assert.IsTrue(this.TestModel.LocationSpacePropertiesTitle.Contains(locInfo.Code));
        }


        [Test]
        public void Property_LocationSpacePropertiesTitle()
        {
            String propertyName = "LocationSpacePropertiesTitle";

            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.LocationSpacePropertiesTitleProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.First().Id;

            //Check initial value is not null
            Assert.IsNotNull(this.TestModel.LocationSpacePropertiesTitle);
        }

        [Test]
        public void Property_SelectedLocationInfo()
        {
            String propertyName = "SelectedLocationInfo";
            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.SelectedLocationInfoProperty.Path);

            LocationInfo info = this.TestModel.MasterLocationInfoList[0];
            this.TestModel.SelectedLocationInfo = info;

            Assert.AreEqual(info, this.TestModel.SelectedLocationInfo);
            Assert.AreEqual(info.Id, this.TestModel.SelectedLocationSpace.LocationId);
        }

        [Test]
        public void Property_SelectedProductGroup()
        {
            String propertyName = "SelectedProductGroup";
            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.SelectedProductGroupProperty.Path);

            ProductGroup productGroup = ProductGroup.NewProductGroup(0);
            this.TestModel.SelectedProductGroup = productGroup;

            Assert.AreEqual(productGroup, this.TestModel.SelectedProductGroup);
            Assert.AreEqual(productGroup.Id, this.TestModel.SelectedProductGroup.Id);
        }

        [Test]
        public void Property_IsDuplicateProductGroup()
        {
            String propertyName = "IsDuplicateProductGroup";
            Assert.AreEqual(propertyName, LocationSpaceMaintenanceViewModel.IsDuplicateProductGroupProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");

            //check execute
            base.PropertyChangedNotifications.Clear();
            LocationSpace preSelectedItem = this.TestModel.SelectedLocationSpace;
            cmd.Execute();

            LocationSpace postSelectedItem = this.TestModel.SelectedLocationSpace;
            Assert.IsTrue(postSelectedItem.IsNew);
            Assert.AreNotEqual(preSelectedItem, postSelectedItem);
            Assert.Contains(LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProperty.Path, base.PropertyChangedNotifications);
        }

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execut
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            base.PropertyChangedNotifications.Clear();
            cmd.Execute(locSpaceId);

            Assert.AreEqual(locSpaceId, this.TestModel.SelectedLocationSpace.Id, "The item should be the same");
            Assert.Contains(LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProperty.Path, base.PropertyChangedNotifications);

            //check related properties have updated
            Assert.AreEqual(locSpaceId, this.TestModel.SelectedLocationSpace.Id, "The location space should have updated");
            Assert.Contains(LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProperty.Path, base.PropertyChangedNotifications);
        }

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.FirstOrDefault().Id;

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that has already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.AvailableLocationSpaceInfos.First().LocationId;
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that hasn't already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.Last().Id;
            Assert.IsTrue(cmd.CanExecute(), "Selected location space should be valid");

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //needs to be dirty
            Int16 locationId = this.TestModel.SelectedLocationSpace.LocationId;
            this.TestModel.SelectedLocationSpace.LocationId = 0;
            this.TestModel.SelectedLocationSpace.LocationId = locationId;

            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected item has been updated
            Assert.IsFalse(this.TestModel.SelectedLocationSpace.IsNew, "The item should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedLocationSpace.IsDirty, "The item should have been saved");
            Assert.AreEqual(locSpaceId, this.TestModel.SelectedLocationSpace.Id, "The item should be the same");
            Assert.Contains(LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProperty.Path, base.PropertyChangedNotifications);
        }

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.FirstOrDefault().Id;

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that has already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.AvailableLocationSpaceInfos.First().LocationId;
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that hasn't already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.Last().Id;
            Assert.IsTrue(cmd.CanExecute(), "Selected location space should be valid");

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //needs to be dirty
            Int16 locationId = this.TestModel.SelectedLocationSpace.LocationId;
            this.TestModel.SelectedLocationSpace.LocationId = 0;
            this.TestModel.SelectedLocationSpace.LocationId = locationId;

            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected item is new
            Assert.IsTrue(this.TestModel.SelectedLocationSpace.IsNew, "The item should no longer be new");
        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.FirstOrDefault().Id;

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that has already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.AvailableLocationSpaceInfos.First().LocationId;
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that hasn't already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.Last().Id;
            Assert.IsTrue(cmd.CanExecute(), "Selected location space should be valid");

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //needs to be dirty
            Int16 locationId = this.TestModel.SelectedLocationSpace.LocationId;
            this.TestModel.SelectedLocationSpace.LocationId = 0;
            this.TestModel.SelectedLocationSpace.LocationId = locationId;

            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //Cant check window has closed
        }

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that has already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.AvailableLocationSpaceInfos.First().LocationId;
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid");

            //Assign to location id that has already been assigned
            this.TestModel.SelectedLocationSpace.LocationId = this.TestModel.MasterLocationInfoList.Last().Id;
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be valid as its marked as new");

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //Cant test can execute as it opens a window for input
        }

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Selected location space should be invalid as its marked as new");

            //Take orgiainl location space count
            Int32 originalLocationSpaceCount = this.TestModel.AvailableLocationSpaceInfos.Count();

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected item has been updated
            Assert.IsTrue(this.TestModel.SelectedLocationSpace.IsNew, "A new location space should have been created");
            Assert.AreNotEqual(locSpaceId, this.TestModel.SelectedLocationSpace.Id, "The items should be different");
            Assert.Contains(LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProperty.Path, base.PropertyChangedNotifications);

            //Check no version's exist for the original location space for open screen
            Assert.AreEqual(originalLocationSpaceCount - 1, this.TestModel.AvailableLocationSpaceInfos.Count(), "Location space should have been deleted");
        }

        [Test]
        public void Command_Edit()
        {
            RelayCommand cmd = this.TestModel.EditCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as a location space is selected");

            //set selected location space to null
            this.TestModel.SelectedLocationSpace = null;

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as a location space is not selected");

            //Can't test the window that loads
        }

        [Test]
        public void Command_Ok()
        {
            RelayCommand cmd = this.TestModel.OkCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as current location space product group is valid");

            this.TestModel.SelectedLocationSpaceProductGroupRow.LocationSpaceProductGroup.ProductGroupId = 0;
            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as current location space product group is invalid");

            //set selected location space to null
            this.TestModel.CurrentLocationSpaceProductGroup = null;
            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as current location space product group is null");

            //Can't test the effects, UI handles this
        }

        [Test]
        public void Command_Add()
        {
            RelayCommand cmd = this.TestModel.AddCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as current location space is not null");

            this.TestModel.SelectedLocationSpace = null;
            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as current location space is null");

            //Can't test the effects, UI window required
        }

        [Test]
        public void Command_Remove()
        {
            RelayCommand cmd = this.TestModel.RemoveCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no product group row is selected");

            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as a row has been selected");

            //Take copy of selected row
            LocationSpaceProductGroupRowViewModel selectedRow = this.TestModel.SelectedLocationSpaceProductGroupRow;

            //Execute cmd
            cmd.Execute();

            //Ensure it no longer exists
            Assert.IsFalse(this.TestModel.AvailableLocationSpaceRows.Contains(selectedRow), "Shouldn't exist anymore");

            //Ensure it no longer exists on the location space object too
            Assert.IsFalse(this.TestModel.SelectedLocationSpace.LocationSpaceProductGroups.Contains(selectedRow.LocationSpaceProductGroup), "Shouldn't exist anymore");
        }

        [Test]
        public void Command_GroupBy()
        {
            RelayCommand<String> cmd = this.TestModel.GroupByCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //Can't test the effects, UI handles this
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //Can't test the effects, UI handles this
        }

        [Test]
        public void Command_AddElement()
        {
            RelayCommand cmd = this.TestModel.AddElementCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no space in the bay for elements");

            this.TestModel.SelectedLocationSpaceBay.Height = 150;
            this.TestModel.SelectedLocationSpaceBay.Width = 100;

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled");

            Assert.IsTrue(this.TestModel.SelectedLocationSpaceBay.Elements.Count == 1);

            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedLocationSpaceBay.Elements.Count == 2, "Element should have been added");
            Assert.AreEqual(this.TestModel.SelectedLocationSpaceBay.Width, this.TestModel.SelectedLocationSpaceElement.Width, "Width should have been inherited down");
            Assert.AreEqual(2, this.TestModel.SelectedLocationSpaceElement.Order, "Should have next order number in sequence");
        }

        [Test]
        public void Command_CopyElement()
        {
            Random random = new Random();

            RelayCommand cmd = this.TestModel.CopyElementCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no space in the bay for elements");

            this.TestModel.SelectedLocationSpaceBay.Height = 150;
            this.TestModel.SelectedLocationSpaceBay.Width = 100;

            //check can execute
            this.TestModel.SelectedLocationSpaceElement = null;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no element is selected");

            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.First();
            //Set unique random value
            Single randomValue = Convert.ToSingle(random.Next(1, 750));
            this.TestModel.SelectedLocationSpaceElement.Depth = randomValue;
            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled");

            Assert.IsTrue(this.TestModel.SelectedLocationSpaceBay.Elements.Count == 1);

            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedLocationSpaceBay.Elements.Count == 2, "Element should have been added");
            Assert.AreEqual(2, this.TestModel.SelectedLocationSpaceElement.Order, "Should have next order number in sequence");
            Assert.AreEqual(randomValue, this.TestModel.SelectedLocationSpaceElement.Depth, "Should be a copy");
        }

        [Test]
        public void Command_RemoveElement()
        {
            Random random = new Random();

            RelayCommand cmd = this.TestModel.RemoveElementCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            this.TestModel.SelectedLocationSpaceBay.Height = 150;
            this.TestModel.SelectedLocationSpaceBay.Width = 100;

            //check can execute
            this.TestModel.SelectedLocationSpaceElement = null;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no element is selected");

            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.First();

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as 1 element must exist");

            //Add new element
            this.TestModel.AddElementCommand.Execute();

            //Check new element exist
            Assert.AreEqual(2, this.TestModel.SelectedLocationSpaceBay.Elements.Count, "2 elements should exist");

            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.First();

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled");

            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedLocationSpaceBay.Elements.Count == 1, "Element should have been removed");
            Assert.AreEqual(1, this.TestModel.SelectedLocationSpaceBay.Elements.First().Order, "The only element left should have been reordered to 1");
        }

        [Test]
        public void Command_AddBay()
        {
            RelayCommand cmd = this.TestModel.AddBayCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled");

            Assert.IsTrue(this.TestModel.CurrentLocationSpaceProductGroup.Bays.Count == 1);

            cmd.Execute();

            Assert.IsTrue(this.TestModel.CurrentLocationSpaceProductGroup.Bays.Count == 2, "Bay should have been added");
            Assert.AreEqual(2, this.TestModel.SelectedLocationSpaceBay.Order, "Should have next order number in sequence");
        }

        [Test]
        public void Command_CopyBay()
        {
            Random random = new Random();

            RelayCommand cmd = this.TestModel.CopyBayCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //Set unique random value
            Single randomValue = Convert.ToSingle(random.Next(1, 750));
            this.TestModel.SelectedLocationSpaceBay.Depth = randomValue;
            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled");

            Assert.IsTrue(this.TestModel.CurrentLocationSpaceProductGroup.Bays.Count == 1);

            cmd.Execute();

            Assert.IsTrue(this.TestModel.CurrentLocationSpaceProductGroup.Bays.Count == 2, "Bay should have been added");
            Assert.AreEqual(2, this.TestModel.SelectedLocationSpaceBay.Order, "Should have next order number in sequence");
            Assert.AreEqual(randomValue, this.TestModel.SelectedLocationSpaceBay.Depth, "Should be a copy");
        }

        [Test]
        public void Command_RemoveBay()
        {
            Random random = new Random();

            RelayCommand cmd = this.TestModel.RemoveBayCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as 1 bay must exist");

            //Add new bay
            this.TestModel.AddBayCommand.Execute();

            //Check new bay exist
            Assert.AreEqual(2, this.TestModel.CurrentLocationSpaceProductGroup.Bays.Count, "2 bays should exist");

            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled");

            cmd.Execute();

            Assert.IsTrue(this.TestModel.CurrentLocationSpaceProductGroup.Bays.Count == 1, "Bay should have been removed");
            Assert.AreEqual(1, this.TestModel.CurrentLocationSpaceProductGroup.Bays.First().Order, "The only bay left should have been reordered to 1");
        }

        [Test]
        public void Command_IncreaseBaysSelectedElement()
        {
            RelayCommand cmd = this.TestModel.IncreaseBaysSelectedElement;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no space in the bay for elements");

            this.TestModel.SelectedLocationSpaceBay.Height = 150;
            this.TestModel.SelectedLocationSpaceBay.Width = 100;

            //Select only element
            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.First();
            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as only 1 element exists");

            //Add new element
            this.TestModel.AddElementCommand.Execute();

            //Reselect the first element
            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.First();

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled");

            //Execute
            cmd.Execute();

            //Check selected element is order number 2
            Assert.AreEqual(2, this.TestModel.SelectedLocationSpaceElement.Order, "Should be element number 2");

            //Command should now be disabled again
            Assert.IsFalse(cmd.CanExecute());
        }

        [Test]
        public void Command_DecreaseBaysSelectedElement()
        {
            RelayCommand cmd = this.TestModel.DecreaseBaysSelectedElement;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //open an item
            Int32 locSpaceId = this.TestModel.AvailableLocationSpaceInfos.First().Id;
            this.TestModel.OpenCommand.Execute(locSpaceId);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as selected bay  is null");

            //Select a product group & bay
            this.TestModel.SelectedLocationSpaceProductGroupRow = this.TestModel.AvailableLocationSpaceRows.First();
            this.TestModel.SelectedLocationSpaceBay = this.TestModel.CurrentLocationSpaceProductGroup.Bays.First();

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no space in the bay for elements");

            this.TestModel.SelectedLocationSpaceBay.Height = 150;
            this.TestModel.SelectedLocationSpaceBay.Width = 100;

            //Select only element
            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.First();
            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as only 1 element exists");

            //Add new element
            this.TestModel.AddElementCommand.Execute();

            //Reselect the first element
            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.First();

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as its the first element");

            //Select 2nd element
            this.TestModel.SelectedLocationSpaceElement = this.TestModel.SelectedLocationSpaceBay.Elements.Last();

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as its the last element");

            //Execute
            cmd.Execute();

            //Check selected element is order number 1
            Assert.AreEqual(1, this.TestModel.SelectedLocationSpaceElement.Order, "Should be element number 1");

            //Command should now be disabled again
            Assert.IsFalse(cmd.CanExecute());
        }

        [Test]
        public void Command_SelectProductGroup()
        {
            RelayCommand cmd = this.TestModel.SelectProductGroupCommand;
            Assert.AreEqual("SelectProductGroupCommand", LocationSpaceMaintenanceViewModel.SelectProductGroupCommandProperty.Path);

            //Check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            ProductGroup previousProductGroup = this.TestModel.SelectedProductGroup;
            cmd.Execute();

            Assert.AreNotEqual(previousProductGroup, this.TestModel.SelectedProductGroup);
        }

        #endregion

        #region Methods

        //Can test this as it requires app resources to be loaded.
        //[Test]
        //public void Method_GenerateColumnSet()
        //{
        //    //Pull all of the group levels that aren't root level
        //    List<ProductLevel> levels = this.TestModel.MasterProductHierarchyView.Model.FetchAllLevels().ToList();

        //    //Execute method
        //    DataGridColumnCollection columns = this.TestModel.GenerateColumnSet();

        //    //Check number of columns matches the number of non root levels
        //    Assert.AreEqual((levels.Count(p => p.IsRoot == false) + 3), columns.Count, "Columns are missing");
        //}

        ///// <summary>
        ///// Override for property path check
        ///// </summary>
        //[Test]
        //public override void Test_PropertyPathCheck()
        //{
        //    Type testModelType = typeof(LocationSpaceMaintenanceViewModel);

        //    IEnumerable<PropertyInfo> propertyInfos = testModelType.GetProperties()
        //         .Where(p => p.DeclaringType == testModelType);
        //    //.Where(p => p.PropertyType.GetInterface("IRelayCommand") == null);
        //    foreach (PropertyInfo p in propertyInfos)
        //    {
        //        if (p.Name != "MasterProductHierarchyView")
        //        {
        //            //get the static property path
        //            FieldInfo propertyPathInfo = testModelType.GetField(p.Name + "Property");
        //            Assert.IsNotNull(propertyPathInfo, "PropertyPath missing for " + p.Name);

        //            Assert.AreEqual(p.Name, ((PropertyPath)propertyPathInfo.GetValue(this.TestModel)).Path, p.Name + "property path is incorrect");
        //        }
        //    }
        //}

        #endregion
    }
}
