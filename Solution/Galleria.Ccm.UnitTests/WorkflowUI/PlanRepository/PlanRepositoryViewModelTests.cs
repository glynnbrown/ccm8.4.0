﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664 : A.Kuszyk
// Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanRepository
{
    [TestFixture]
    public class PlanRepositoryViewModelTests
    {
        // AddPlanogramGroupToFavourites and RemovePlanogramGroupFromFavourites commands
        // not tested, because they rely on an AttachedControl.
    }
}
