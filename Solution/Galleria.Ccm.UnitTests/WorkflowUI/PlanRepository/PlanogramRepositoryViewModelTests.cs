﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-28535 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 801)
// V8-28701 : J.Pickup
//  Added Unlock planogram command 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PlanRepository;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanRepository
{
    [TestFixture]
    public sealed class PlanogramRepositoryViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private PlanogramHierarchy PopulateHierarchy()
        {
            return TestDataHelper.PopulatePlanogramHierarchy(this.EntityId);
        }

        private List<Int32> InsertTestPlans(PlanogramGroup planGroup, Int32 planCount)
        {
            List<Int32> packageIds = TestDataHelper.InsertPlanograms(planCount);
            PlanogramGroup.AssociatePlanograms(planGroup.Id, packageIds.Select(p=> p));
            return packageIds;
        }

        #endregion

        #region Property tests

        #region PlanogramHierarchyView

        [Test]
        public void PlanogramHierarchyView_Initialized()
        {
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.PlanogramHierarchyView, "Should not be null");
            Assert.IsNotNull(testModel.PlanogramHierarchyView.Model, "Model Should not be null");
        }

        #endregion

        #region SelectedPlanogramGroup

        [Test]
        public void SelectedPlanogramGroup_Intialized()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNotNull(testModel.SelectedPlanogramGroup, "Should not start null");
            Assert.IsTrue(testModel.PlanogramHierarchyView.Model.EnumerateAllGroups().Contains(testModel.SelectedPlanogramGroup));
        }

        #endregion

        #region UserFavouriteGroupsView

        [Test]
        public void UserFavouriteGroupsView_Initialized()
        {
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.UserFavouriteGroupsView, "Should not be null");
            Assert.IsNotNull(testModel.UserFavouriteGroupsView.Model, "Model Should not be null");
        }

        #endregion

        #region PlanogramSearchCriteria

        [Test]
        public void PlanogramSearchCriteria_Initialized()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNullOrEmpty(testModel.PlanogramSearchCriteria, "Should start empty");
        }

        [Test]
        public void PlanogramSearchCriteria_Setter()
        {
            var testModel = new PlanRepositoryViewModel();

            testModel.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            testModel.PlanogramSearchCriteria = "TEST";
            Assert.AreEqual("TEST", testModel.PlanogramSearchCriteria);

            Assert.Contains("PlanogramSearchCriteria", base.PropertyChangedNotifications);
        }

        #endregion

        #region PlanogramRows

        [Test]
        public void PlanogramRows_Initialized()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.PlanogramRows, "Should not be null");
            Assert.AreNotEqual(0, testModel.PlanogramRows.Count);
        }

        //[Test]
        //public void PlanogramRows_UpdatesOnSelectedPlanogramGroupChanged()
        //{
        //    Assert.Ignore("TODO");
        //}

        //[Test]
        //public void PlanogramRows_UpdatesOnPlanogramSearchCriteriaChanged()
        //{
        //    Assert.Ignore("TODO");
        //}

        #endregion

        #region SelectedPlanogramRows Property Tests

        [Test]
        public void SelectedPlanogramRows_Initialized()
        {
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.SelectedPlanogramRows, "Should not be null");
            //Assert.AreEqual(0, testModel.SelectedPlanogramRows.Count, "Should start with 1 plan selected");
        }

        #endregion

        #region ActivePlanogramInfo

        [Test]
        public void ActivePlanogramInfo_Initialized()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            Assert.IsNull(testModel.ActivePlanogramInfo, "Should be null as we should not start with a selection.");
        }

        //[Test]
        //public void ActivePlanogramInfo_UpdatesOnSelectedRowsChanged()
        //{
        //    Assert.Ignore("TODO");
        //}

        #endregion

        #endregion

        #region Command tests

        #region OpenPlanogram

        [Test]
        public void OpenPlanogramCommand_CanExecute_DisabledWhenNoSelectedRows()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows.First());

            Assert.AreNotEqual(0, testModel.SelectedPlanogramRows.Count);
            Assert.IsTrue(testModel.OpenPlanogramCommand.CanExecute());

            testModel.SelectedPlanogramRows.Clear();
            Assert.AreEqual(0, testModel.SelectedPlanogramRows.Count);
            Assert.IsFalse(testModel.OpenPlanogramCommand.CanExecute());

            testModel.Dispose();
        }

        #endregion

        #region CutPlanogram

        [Test]
        public void CutPlanogramCommand_DisabledWhenNoSelectedRows()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            Assert.IsFalse(testModel.CutPlanogramCommand.CanExecute());

            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows.First());
            Assert.IsTrue(testModel.CutPlanogramCommand.CanExecute());
        }

        [Test]
        public void CutPlanogramCommand_DisabledWhenNoDeletePermission()
        {
            //V8-32525 - Checks that the user cannot move planograms when the delete permission is 
            // off.

            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows.First());

            Assert.IsTrue(testModel.CutPlanogramCommand.CanExecute());

            testModel.Dispose();


            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramDelete.ToString());

            //check
            testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows.First());
            Assert.IsFalse(testModel.CutPlanogramCommand.CanExecute());
            testModel.Dispose();
        }

        #endregion



        #region DeletePlanogram

        [Test]
        public void DeletePlanogramCommand_DisabledWhenNoDeletePerm()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramDelete.ToString());

            var testModel = new PlanRepositoryViewModel();

            testModel.SelectedPlanogramRows.Clear();
            var rowToDelete = testModel.PlanogramRows.First();
            testModel.SelectedPlanogramRows.Add(rowToDelete);

            Assert.IsFalse(testModel.DeletePlanogramCommand.CanExecute());
        }

        [Test]
        public void DeletePlanogramCommand_DisabledWhenNoSelectedRows()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();

            testModel.SelectedPlanogramRows.Clear();
            var rowToDelete = testModel.PlanogramRows.First();
            testModel.SelectedPlanogramRows.Add(rowToDelete);

            Assert.IsTrue(testModel.DeletePlanogramCommand.CanExecute());

            testModel.SelectedPlanogramRows.Clear();

            Assert.IsFalse(testModel.DeletePlanogramCommand.CanExecute(), "disabled when no row to delete");
            Assert.IsNotNull(testModel.DeletePlanogramCommand.DisabledReason, "Disabled reason should be populated");


            testModel.Dispose();
        }

        [Test]
        public void DeletePlanogramCommand_IfNoToConfirmNoDelete()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[0]);
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[1]);

            List<PlanogramInfo> plansToDelete = testModel.SelectedPlanogramRows.Select(p => p.Info).ToList();
            Int32 startRowCount = testModel.PlanogramRows.Count;

            //make sure the busy window is shown and that update progress is called twice - once per plan.
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button2); //confirm delete with user.

            testModel.DeletePlanogramCommand.Execute();

            //Check that neither plan is deleted
            PlanogramInfoList planInfos = PlanogramInfoList.FetchByIds(new List<Int32> { plansToDelete[0].Id, plansToDelete[1].Id });
            Assert.AreEqual(2, planInfos.Count);
            Assert.IsFalse(planInfos.Any(p => p.DateDeleted.HasValue));
        }

        [Test]
        public void DeletePlanogramCommand_Executed()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[0]);
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[1]);

            List<PlanogramInfo> plansToDelete = testModel.SelectedPlanogramRows.Select(p => p.Info).ToList();
            Int32 startRowCount = testModel.PlanogramRows.Count;

            //make sure the busy window is shown and that update progress is called twice - once per plan.
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1); //confirm delete with user.
            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
            WindowService.AddNullResponse(NUnitWindowService.UpdateBusyMethod); //update busy after plan 1 delete
            WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,//update busy after plan 2 delete
                (p) =>
                {
                    //make sure the viewmodel has already removed the prev row
                    Assert.AreEqual(startRowCount - 1, testModel.PlanogramRows.Count, "First deleted plan row should have already been removed");
                });
            WindowService.AddResponse(NUnitWindowService.CloseCurrentBusyMethod, //close busy
                (p) =>
                {
                    //make sure the viewmodel has now removed the second plan row
                    Assert.AreEqual(startRowCount - 2, testModel.PlanogramRows.Count, "Second deleted plan row should have already been removed");
                });


            testModel.DeletePlanogramCommand.Execute();

            WindowService.AssertResponsesUsed();


            //Chekc both plans are deleted.
            PlanogramInfoList planInfos = PlanogramInfoList.FetchByIds(new List<Int32> { plansToDelete[0].Id, plansToDelete[1].Id });
            Assert.AreEqual(0, planInfos.Count(p => p.DateDeleted == null));
        }

        [Test]
        public void DeletePlanogramCommand_CancelDelete()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[0]);
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[1]);

            List<PlanogramInfo> plansToDelete = testModel.SelectedPlanogramRows.Select(p => p.Info).ToList();
            Int32 startRowCount = testModel.PlanogramRows.Count;

            EventHandler cancelHandler = null;

            //confirm delete with user.
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);

            //get the cancel handler to call 
            WindowService.AddResponse(NUnitWindowService.ShowDeterminateBusyMethod,
                (p) =>
                {
                    cancelHandler = (EventHandler)p.WindowParameters[2];
                });

            //call cancel after the first plan is deleted.
            WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
                (p) =>
                {
                    cancelHandler.Invoke(null, EventArgs.Empty);
                });

            //window should close
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);


            testModel.DeletePlanogramCommand.Execute();

            WindowService.AssertResponsesUsed();

            //check that only one plan was deleted.
            PlanogramInfoList planInfos = PlanogramInfoList.FetchByIds(new List<Int32> { plansToDelete[0].Id, plansToDelete[1].Id });
            Assert.AreEqual(1, planInfos.Count(p=> p.DateDeleted == null));
        }

        #endregion



        #region DeleteGroup

        [Test]
        public void DeleteGroupCommand_DisabledWhemNoDeletePerm()
        {
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramHierarchyEdit.ToString());

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

            Assert.IsFalse(testModel.DeleteGroupCommand.CanExecute());
        }

        [Test]
        public void DeleteGroupCommand_GroupsWithPlans()
        {
            var testModel = new PlanRepositoryViewModel();

            List<Int32> plansToDelete = new List<Int32>();

            //add group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup;
            WindowService.PromptUserForValueSetReponse(true, "Group A");
            testModel.NewGroupCommand.Execute();

            //add some plans to group a
            plansToDelete.AddRange(InsertTestPlans(testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0], 3));

            //add group b as a child of a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            WindowService.PromptUserForValueSetReponse(true, "Group B");
            testModel.NewGroupCommand.Execute();

            //add some plans to group b
            plansToDelete.AddRange(InsertTestPlans(testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0].ChildList[0], 5));

            //select group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

            //delete
            WindowService.AddNullResponse(NUnitWindowService.ShowIndeterminateBusyMethod); //busy while building list.
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1); //confirm delete

            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //busy while deleting

            Int32 curProgress = 0;
            for (Int32 i = 0; i < (plansToDelete.Count + 2); i++)
            {
                WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
                    (p) =>
                    {
                        //check progress has increased.
                        Int32 newProgress = (p.WindowParameters.Count() == 1) ? (Int32)p.WindowParameters[0] : (Int32)p.WindowParameters[1];
                        Assert.Greater(newProgress, curProgress);
                        Assert.LessOrEqual(newProgress, 100);

                        curProgress = newProgress;

                    }); //one update per item deleted.
            }

            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed

            testModel.DeleteGroupCommand.Execute();

            WindowService.AssertResponsesUsed();

            //check all plans and groups were deleted
            PlanogramInfoList deletedPlans = PlanogramInfoList.FetchByIds(plansToDelete);
            if (deletedPlans.Count != 0)
            {
                Assert.IsTrue(deletedPlans.All(p => p.DateDeleted.HasValue), "all plans should be deleted");
            }

  
            //check groups were deleted
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count, "Groups should have gone.");
        }

        [Test]
        public void DeleteGroupCommand_GroupWithoutPlans()
        {
            var testModel = new PlanRepositoryViewModel();

            //add group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup;
            WindowService.PromptUserForValueSetReponse(true, "Group A");
            testModel.NewGroupCommand.Execute();

            //delete it
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

            WindowService.AddNullResponse(NUnitWindowService.ShowIndeterminateBusyMethod); //busy while building list.
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1); //confirm delete

            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //busy while deleting

            Int32 curProgress = 0;
            WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
                (p) =>
                {
                    //check progress has increased.
                    Int32 newProgress = (p.WindowParameters.Count() == 1) ? (Int32)p.WindowParameters[0] : (Int32)p.WindowParameters[1];
                    Assert.Greater(newProgress, curProgress);
                    Assert.LessOrEqual(newProgress, 100);

                    curProgress = newProgress;

                }); //one update per item deleted.


            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed

            testModel.DeleteGroupCommand.Execute();

            WindowService.AssertResponsesUsed();

            //check groups were deleted
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count, "Groups should have gone.");
        }

        [Test]
        public void DeleteGroupCommand_GroupWithLockedPlans()
        {

            var testModel = new PlanRepositoryViewModel();

            List<Int32> plansToDelete = new List<Int32>();

            //add group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup;
            WindowService.PromptUserForValueSetReponse(true, "Group A");
            testModel.NewGroupCommand.Execute();

            //add group b as a child of a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            WindowService.PromptUserForValueSetReponse(true, "Group B");
            testModel.NewGroupCommand.Execute();

            //add some plans to group b
            plansToDelete.AddRange(InsertTestPlans(testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0].ChildList[0], 2));

            //lock plan 1
            User newUser = User.NewUser();
            newUser.UserName = "USR1";
            newUser.FirstName = "u1";
            newUser.LastName = "u1";
            newUser = newUser.Save();
            Package lockedPlan = Package.FetchById(plansToDelete.First(), newUser.Id, PackageLockType.User);

            //select group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

            //delete
            WindowService.AddNullResponse(NUnitWindowService.ShowIndeterminateBusyMethod); //busy while building list.
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1); //confirm delete

            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //busy while deleting

            Int32 curProgress = 0;


            for (Int32 i = 0; i < (plansToDelete.Count + 2); i++)
            {
                WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
                    (p) =>
                    {
                        //check progress has increased.
                        Int32 newProgress = (p.WindowParameters.Count() == 1) ? (Int32)p.WindowParameters[0] : (Int32)p.WindowParameters[1];
                        Assert.Greater(newProgress, curProgress);
                        Assert.LessOrEqual(newProgress, 100);

                        curProgress = newProgress;

                    }); //one update per item deleted.
            }

            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed

            testModel.DeleteGroupCommand.Execute();

            WindowService.AssertResponsesUsed();

            //check plan 2 was deleted but plan 1 was not.
            PlanogramInfoList deletedPlans = PlanogramInfoList.FetchByIds(plansToDelete.Select(p=>Convert.ToInt32(p)));

            PlanogramInfo p1 = deletedPlans.FirstOrDefault(p=> plansToDelete[0] == p.Id);
            Assert.IsNotNull(p1);
            Assert.IsFalse(p1.DateDeleted.HasValue);

            PlanogramInfo p2 = deletedPlans.FirstOrDefault(p=> plansToDelete[1] == p.Id);
            if (p2 != null) Assert.IsTrue(p2.DateDeleted.HasValue);


            //check the groups were not deleted
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
            Assert.AreEqual(1, hierarchy.RootGroup.ChildList.Count);
            Assert.AreEqual(1, hierarchy.RootGroup.ChildList[0].ChildList.Count);
        }

        #endregion

        #region UnlockSelectedPlanogramsPlanogram

        [Test]
        public void UnlockSelectedPlanogramsCommand_DisabledWhenNoSelectedRows()
        {
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.SelectedPlanogramRows);

            //Ensure none are locked and make sure cannot execute
            Assert.IsNull(testModel.SelectedPlanogramRows.FirstOrDefault(c => c.Info.IsLocked));
            Assert.IsFalse((testModel.UnlockSelectedPlanogramCommand.CanExecute()));

            //Cannot execute as none selected
            testModel.SelectedPlanogramRows.Clear();
            Assert.AreEqual(0, testModel.SelectedPlanogramRows.Count);
            Assert.IsFalse(testModel.UnlockSelectedPlanogramCommand.CanExecute());

            testModel.Dispose();
        }

        #endregion

        #endregion
    }
}
