﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LocationHierarchyMaintenance
{
    [TestFixture]
    public class LocationHierarchyUnitEditViewModelTests : TestBase<LocationHierarchyUnitEditViewModel>
    {
        #region Test Fixture Helpers

        /// <summary>
        /// Creates a fully populated location hierarchy with levels,
        /// groups and locations
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static LocationHierarchy PopulateLocationHierarchy(IDalFactory dalFactory, Int32 entityId)
        {
            LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

            //add 2 levels
            LocationLevel level1 = hierarchy.RootLevel.ChildLevel;
            //LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            //level1.Name = "level1";
            LocationLevel level2 = level1.ChildLevel;
            //LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            //level2.Name = "level2";

            //add 3 level1 groups
            LocationGroup level1Group1 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group1.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            level1Group1.Name = "level1Group1";
            LocationGroup level1Group2 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group2.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            level1Group2.Name = "level1Group2";
            LocationGroup level1Group3 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group3.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            level1Group3.Name = "level1Group3";
            hierarchy.RootGroup.ChildList.Add(level1Group1);
            hierarchy.RootGroup.ChildList.Add(level1Group2);
            hierarchy.RootGroup.ChildList.Add(level1Group3);

            //add 3 level2 groups per level1 group
            foreach (LocationGroup level1Group in hierarchy.RootGroup.ChildList)
            {
                for (int i = 1; i <= 3; i++)
                {
                    LocationGroup level2Group = LocationGroup.NewLocationGroup(level2.Id);
                    level2Group.Code = hierarchy.GetNextAvailableDefaultGroupCode();
                    level2Group.Name = "level2group" + i.ToString();
                    level1Group.ChildList.Add(level2Group);
                }
            }

            hierarchy = hierarchy.Save();

            //insert locations against the hierarchy
            IEnumerable<LocationGroup> leafGroups = hierarchy.EnumerateAllGroups().Where(g => g.ChildList.Count == 0);

            //TestDataHelper.InsertLocationDtos(dalFactory, entityId,leafGroups.Select(g => g.Id), 20);


            return hierarchy;
        }

        private LocationHierarchyViewModel _locHierarchyView;

        public override void  Setup()
        {
            //setup the base
            base.Setup();


            _locHierarchyView = new LocationHierarchyViewModel();
            _locHierarchyView.FetchForCurrentEntity();

            this.TestModel = new LocationHierarchyUnitEditViewModel(_locHierarchyView.Model, null);
        }


        #endregion

        #region Properties

        [Test]
        public void Property_SelectedGroup()
        {
            String propertyName = LocationHierarchyUnitEditViewModel.SelectedGroupProperty.Path;

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsNotNull(this.TestModel.SelectedGroup);
        }

        [Test]
        public void Property_AvailableLevels()
        {
            String propertyName = LocationHierarchyUnitEditViewModel.AvailableLevelsProperty.Path;

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableLevels);

            IEnumerable<LocationGroup> groups = _locHierarchyView.Model.EnumerateAllGroups();

            //should contain all levels, except for the root and any that do not have a group
            foreach (LocationLevel level in _locHierarchyView.Model.EnumerateAllLevels())
            {
                Boolean isValidLevel = (!level.IsRoot);
                if (isValidLevel)
                {
                    isValidLevel = (groups.Where(g => Object.Equals(g.LocationLevelId, level.ParentLevel.Id)).Count() > 0);
                }

                if (isValidLevel)
                {
                    Assert.Contains(level, this.TestModel.AvailableLevels);
                }
                else
                {
                    Assert.IsFalse(this.TestModel.AvailableLevels.Contains(level),
                        "Should not contain the root or any levels without available parent groups");
                }
            }
        }

        [Test]
        public void Property_SelectedLevel()
        {
            String propertyName = LocationHierarchyUnitEditViewModel.SelectedLevelProperty.Path;

            Assert.IsNotNull(this.TestModel.SelectedLevel, "Should start with a level selected");

            IEnumerable<LocationGroup> allGroups = _locHierarchyView.Model.EnumerateAllGroups();
            foreach (LocationLevel level in this.TestModel.AvailableLevels)
            {
                this.TestModel.SelectedLevel = level;
                Assert.AreEqual(this.TestModel.SelectedLevel, level);

                IEnumerable<LocationGroup> expectedAvailableParents = allGroups.Where(g => Object.Equals(g.LocationLevelId, level.ParentLevel.Id));
                Assert.AreEqual(expectedAvailableParents.Count(), this.TestModel.AvailableParents.Count);
                foreach (LocationGroup parentGroup in expectedAvailableParents)
                {
                    Assert.Contains(parentGroup, this.TestModel.AvailableParents);
                }
            }
        }

        [Test]
        public void Property_AvailableParents()
        {
            String propertyName = LocationHierarchyUnitEditViewModel.AvailableParentsProperty.Path;

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableParents);
            Assert.IsNotNull(this.TestModel.AvailableParents);

            IEnumerable<LocationGroup> expectedAvailableParents =
                _locHierarchyView.Model.EnumerateAllGroups().Where(g => Object.Equals(g.LocationLevelId, this.TestModel.SelectedLevel.ParentLevel.Id));
            Assert.AreEqual(expectedAvailableParents.Count(), this.TestModel.AvailableParents.Count);
            foreach (LocationGroup parentGroup in expectedAvailableParents)
            {
                Assert.Contains(parentGroup, this.TestModel.AvailableParents);
            }
        }

        [Test]
        public void Property_SelectedParent()
        {
            String propertyName = LocationHierarchyUnitEditViewModel.SelectedParentProperty.Path;

            Assert.IsNotNull(this.TestModel.SelectedParent);

            foreach (LocationGroup group in _locHierarchyView.Model.EnumerateAllGroups())
            {
                //if not the root
                if (group.ParentGroup != null)
                {
                    LocationLevel parentLevel = _locHierarchyView.Model.EnumerateAllLevels().FirstOrDefault(l => Object.Equals(l.Id, group.LocationLevelId));
                    if (parentLevel.ChildLevel != null)
                    {

                        this.TestModel.SelectedParent = group;
                        Assert.AreEqual(this.TestModel.SelectedParent, group);

                        Assert.AreEqual(this.TestModel.SelectedLevel.ParentLevel.Id, group.LocationLevelId);
                    }
                }
            }

        }


        [Test]
        public void Property_GroupCode()
        {
            String propertyName = "GroupCode";
            Assert.AreEqual(propertyName, LocationHierarchyUnitEditViewModel.GroupCodeProperty.Path);

            //getter
            Assert.AreEqual(this.TestModel.SelectedGroup.Code, this.TestModel.GroupCode);

            //setter
            String newValue = "xxxNewCode";
            base.PropertyChangedNotifications.Clear();
            this.TestModel.GroupCode = newValue;
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(newValue, this.TestModel.GroupCode);
            Assert.AreEqual(newValue, this.TestModel.SelectedGroup.Code);

        }

        [Test]
        public void Property_GroupName()
        {
            String propertyName = "GroupName";
            Assert.AreEqual(propertyName, LocationHierarchyUnitEditViewModel.GroupNameProperty.Path);

            //getter
            Assert.AreEqual(this.TestModel.SelectedGroup.Name, this.TestModel.GroupName);

            //setter
            String newValue = "xxxNewCode";
            base.PropertyChangedNotifications.Clear();
            this.TestModel.GroupName = newValue;
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(newValue, this.TestModel.GroupName);
            Assert.AreEqual(newValue, this.TestModel.SelectedGroup.Name);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //no group provided
            this.TestModel = new LocationHierarchyUnitEditViewModel(_locHierarchyView.Model, null);
            Assert.IsNotNull(this.TestModel.SelectedGroup);
            Assert.IsTrue(this.TestModel.SelectedGroup.IsNew);

            //group provided
            foreach (LocationGroup group in _locHierarchyView.Model.EnumerateAllGroups())
            {
                if (group.ParentGroup != null)
                {
                    this.TestModel = new LocationHierarchyUnitEditViewModel(_locHierarchyView.Model, group);
                    Assert.AreEqual(group.ParentGroup, this.TestModel.SelectedParent);
                    Assert.AreEqual(group.LocationLevelId, this.TestModel.SelectedLevel.Id);
                }
            }
        }


        #endregion

        #region Commands

        [Test]
        public void Command_ApplyAndClose()
        {
            RelayCommand cmd = this.TestModel.ApplyAndCloseCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);

            //check canexecute
            this.TestModel.SelectedGroup.Name = "unit";

            this.TestModel.SelectedGroup.Code = String.Empty;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as is invalid");
            this.TestModel.SelectedGroup.Code = Guid.NewGuid().ToString().Substring(0, 10);
            Assert.IsTrue(cmd.CanExecute());

            //execute
            LocationGroup group = this.TestModel.SelectedGroup;
            LocationGroup selectedParent = this.TestModel.SelectedParent;
            LocationLevel selectedLevel = this.TestModel.SelectedLevel;

            cmd.Execute();

            Assert.Contains(group, selectedParent.ChildList);
            Assert.AreEqual(selectedLevel.Id, group.LocationLevelId);

            //nb cant test close
        }

        [Test]
        public void Command_ApplyAndNew()
        {
            RelayCommand cmd = this.TestModel.ApplyAndNewCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);
            this.TestModel.SelectedGroup.Name = "unit";

            //check canexecute
            this.TestModel.SelectedGroup.Code = String.Empty;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as is invalid");
            this.TestModel.SelectedGroup.Code = Guid.NewGuid().ToString().Substring(0, 10);
            Assert.IsTrue(cmd.CanExecute());

            //execute
            LocationGroup group = this.TestModel.SelectedGroup;
            LocationGroup selectedParent = this.TestModel.SelectedParent;
            LocationLevel selectedLevel = this.TestModel.SelectedLevel;

            cmd.Execute();

            Assert.Contains(group, selectedParent.ChildList);
            Assert.AreEqual(selectedLevel.Id, group.LocationLevelId);

            //check a new group has been loaded
            Assert.AreNotEqual(this.TestModel.SelectedGroup, group);
            Assert.AreEqual(selectedParent, this.TestModel.SelectedParent);
            Assert.AreEqual(selectedLevel, this.TestModel.SelectedLevel);
        }

        #endregion

        #region Other

        [Test]
        public void NewGroupsOnInvalidLevelsBecomeAvailable()
        {
            throw new InconclusiveException("Not sure if this is still relevant");

            //add 2 new levels to the hierarchy
            LocationLevel level1 = _locHierarchyView.Model.RootLevel.ChildLevel;
            if (level1 == null)
            {
                level1 = LocationLevel.NewLocationLevel();
                level1.Name = _locHierarchyView.Model.GetNextAvailableLevelName(_locHierarchyView.Model.RootLevel);
                _locHierarchyView.Model.RootLevel.ChildLevel = level1;
            }

            LocationLevel level2 = level1.ChildLevel;
            if (level2 == null)
            {
                level2 = LocationLevel.NewLocationLevel();
                level2.Name = _locHierarchyView.Model.GetNextAvailableLevelName(level1);
                level1.ChildLevel = level1;
            }

            //start with a new item
            this.TestModel = new LocationHierarchyUnitEditViewModel(_locHierarchyView.Model, null);


            //get an invalid level
            IEnumerable<LocationLevel> invalidLevels =
                _locHierarchyView.Model.EnumerateAllLevels().Where(l => !l.IsRoot && !this.TestModel.AvailableLevels.Contains(l));

            Assert.AreNotEqual(0, invalidLevels.Count(), "need an invalid level for this test");
            LocationLevel invalidLevel = invalidLevels.First();

            //select the lowest available level
            this.TestModel.SelectedLevel = this.TestModel.AvailableLevels.Last();
            this.TestModel.SelectedGroup.Name = "unit";

            //add
            LocationGroup newGroup = this.TestModel.SelectedGroup;
            this.TestModel.ApplyAndNewCommand.Execute();

            //check the invalid level is now available with the previously added node as the only available parent
            Assert.Contains(invalidLevel, this.TestModel.AvailableLevels);
            this.TestModel.SelectedLevel = invalidLevel;
            Assert.AreEqual(1, this.TestModel.AvailableParents.Count);
            Assert.AreEqual(newGroup, this.TestModel.AvailableParents[0]);
        }

        [Test]
        public void GEM21862_CodeUniqueCheckIsCaseInsensitive()
        {
            //add 2 new levels to the hierarchy
            LocationLevel level1 = _locHierarchyView.Model.RootLevel.ChildLevel;
            if (level1 == null)
            {
                level1 = LocationLevel.NewLocationLevel();
                level1.Name = _locHierarchyView.Model.GetNextAvailableLevelName(_locHierarchyView.Model.RootLevel);
                _locHierarchyView.Model.RootLevel.ChildLevel = level1;
            }

            LocationLevel level2 = level1.ChildLevel;
            if (level2 == null)
            {
                level2 = LocationLevel.NewLocationLevel();
                level2.Name = _locHierarchyView.Model.GetNextAvailableLevelName(level1);
                level1.ChildLevel = level1;
            }

            String codeLower = "md5";
            String codeUpper = "MD5";

            //create new item with code lower
            this.TestModel = new LocationHierarchyUnitEditViewModel(_locHierarchyView.Model, null);
            this.TestModel.GroupName = "CaseTest1";
            this.TestModel.GroupCode = codeLower;

            //execute apply and new
            Assert.IsTrue(this.TestModel.ApplyAndNewCommand.CanExecute());
            base.PropertyChangedNotifications.Clear();
            this.TestModel.ApplyAndNewCommand.Execute();
            Assert.AreNotEqual(this.TestModel.GroupCode, codeLower, "Group should have changed");
            Assert.Contains(LocationHierarchyUnitEditViewModel.GroupCodeProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(LocationHierarchyUnitEditViewModel.GroupNameProperty.Path, base.PropertyChangedNotifications);

            //set the 2nd group to same code
            this.TestModel.GroupName = "CaseTest1";
            this.TestModel.GroupCode = codeLower;
            Assert.IsFalse(this.TestModel.ApplyAndNewCommand.CanExecute());
            Assert.IsNotNullOrEmpty(((IDataErrorInfo)this.TestModel)[LocationHierarchyUnitEditViewModel.GroupCodeProperty.Path],
                "Should show error for group code property.");

            //test upper case version of same code.
            this.TestModel.GroupCode = codeUpper;
            Assert.AreNotEqual(this.TestModel.GroupCode, codeLower);
            Assert.IsFalse(this.TestModel.ApplyAndNewCommand.CanExecute());
            Assert.IsNotNullOrEmpty(((IDataErrorInfo)this.TestModel)[LocationHierarchyUnitEditViewModel.GroupCodeProperty.Path],
                "Should show error for group code property.");

            //set to unique code
            this.TestModel.GroupCode = "md6";
            Assert.IsTrue(this.TestModel.ApplyAndNewCommand.CanExecute());
            Assert.IsNullOrEmpty(((IDataErrorInfo)this.TestModel)[LocationHierarchyUnitEditViewModel.GroupCodeProperty.Path]);


        }

        #endregion
    }
}
