﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-24779 : D.Pleasance ~ Created.
#endregion

#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramImportTemplateMaintenance;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanogramImportTemplateMaintenance
{
    [TestFixture]
    public class PlanogramImportTemplateMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private void InsertTemplates()
        {
            var flow1 = Galleria.Ccm.Model.PlanogramImportTemplate.NewPlanogramImportTemplate(this.EntityId, PlanogramImportFileType.SpacemanV9, "1");
            flow1.Name = "Item 1";
            flow1 = flow1.Save();

            var flow2 = Galleria.Ccm.Model.PlanogramImportTemplate.NewPlanogramImportTemplate(this.EntityId, PlanogramImportFileType.SpacemanV9, "1");
            flow2.Name = "Item 2";
            flow2 = flow2.Save();
        }

        #endregion


        #region Properties

        [Test]
        public void AvailablePlanogramImportTemplates_FetchedOnStartup()
        {
            //insert some workflows.
            InsertTemplates();

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            Assert.AreNotEqual(0, viewModel.AvailablePlanogramImportTemplates.Count);
        }

        [Test]
        public void AvailablePlanogramImportTemplates_UpdatedOnItemInsert()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            Int32 count = viewModel.AvailablePlanogramImportTemplates.Count;

            //save the new workflow
            viewModel.SelectedItem.Name = "w1";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailablePlanogramImportTemplates.Count);
        }

        [Test]
        public void AvailablePlanogramImportTemplates_UpdatedOnItemDelete()
        {
            InsertTemplates();

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            Int32 count = viewModel.AvailablePlanogramImportTemplates.Count;

            //open and delete a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailablePlanogramImportTemplates.Count);
        }


        [Test]
        public void SelectedItem_InitializedAsNew()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            Assert.IsTrue(viewModel.SelectedItem.IsNew);
            Assert.IsNullOrEmpty(viewModel.SelectedItem.Name);
        }


        [Test]
        public void SelectedItem()
        {
            const string expected = "SelectedItem";

            var result = PlanogramImportTemplateMaintenanceViewModel.SelectedItemProperty.Path;
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void MappingTemplateName()
        {
            const string expected = "MappingTemplateName";

            var result = PlanogramImportTemplateMaintenanceViewModel.MappingTemplateNameProperty.Path;
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void SelectedFileType()
        {
            const string expected = "SelectedFileType";
            Assert.AreEqual(expected, PlanogramImportTemplateMaintenanceViewModel.SelectedFileTypeProperty.Path);
        }

        [Test]
        public void AvailableVersions()
        {
            const string expected = "AvailableVersions";
            Assert.AreEqual(expected, PlanogramImportTemplateMaintenanceViewModel.AvailableVersionsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(typeof(PlanogramImportTemplateMaintenanceViewModel), expected);
        }

        [Test]
        public void SelectedVersion()
        {
            const string expected = "SelectedVersion";
            Assert.AreEqual(expected, PlanogramImportTemplateMaintenanceViewModel.SelectedVersionProperty.Path);
        }

        [Test]
        public void ProductMappings()
        {
            const string expected = "ProductMappings";
            Assert.AreEqual(expected, PlanogramImportTemplateMaintenanceViewModel.ProductMappingsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(typeof(PlanogramImportTemplateMaintenanceViewModel), expected);
        }

        [Test]
        public void ComponentMappings()
        {
            const string expected = "ComponentMappings";
            Assert.AreEqual(expected, PlanogramImportTemplateMaintenanceViewModel.ComponentMappingsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(typeof(PlanogramImportTemplateMaintenanceViewModel), expected);
        }

        [Test]
        public void BayMappings()
        {
            const string expected = "BayMappings";
            Assert.AreEqual(expected, PlanogramImportTemplateMaintenanceViewModel.BayMappingsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(typeof(PlanogramImportTemplateMaintenanceViewModel), expected);
        }

        [Test]
        public void PlanogramMappings()
        {
            const string expected = "PlanogramMappings";
            Assert.AreEqual(expected, PlanogramImportTemplateMaintenanceViewModel.PlanogramMappingsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(typeof(PlanogramImportTemplateMaintenanceViewModel), expected);
        }

        #endregion

        #region Commands

        #region New

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            var oldModel = viewModel.SelectedItem;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.SelectedItem);
            Assert.IsTrue(viewModel.SelectedItem.IsNew);
        }

        [Test]
        public void NewCommand_DisabledWhenNoCreatePerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramImportTemplateCreate.ToString());

            //check
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            Assert.IsFalse(viewModel.NewCommand.CanExecute());
        }

        [Test]
        public void NewCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = viewModel.NewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.N, cmd.InputGestureKey);
        }

        #endregion

        #region Open

        [Test]
        public void OpenCommand_Execution()
        {
            InsertTemplates();
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            foreach (var info in viewModel.AvailablePlanogramImportTemplates)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.SelectedItem.Id);
                
                Assert.IsTrue(!viewModel.SelectedItem.IsDirty || viewModel.SelectedItem.IsInitialized);
            }
        }

        #endregion

        #region Save

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            Assert.IsFalse(viewModel.SelectedItem.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.SelectedItem.Name = "Test1";

            Assert.IsTrue(viewModel.SelectedItem.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenNewItemNoCreatePerm()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.SelectedItem.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramImportTemplateCreate.ToString());

            viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenOldItemNoCreatePerm()
        {
            InsertTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramImportTemplateCreate.ToString());

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);
            viewModel.SelectedItem.Name = "NewName";

            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenNewItemNoEditPerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramImportTemplateEdit.ToString());

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.SelectedItem.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenOldItemNoEditPerm()
        {
            InsertTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramImportTemplateEdit.ToString());

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);
            viewModel.SelectedItem.Name = "NewName";

            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = viewModel.SaveCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.S, cmd.InputGestureKey);
        }

        [Test]
        public void SaveCommand_Execution()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            viewModel.SelectedItem.Name = "Test1";

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.SelectedItem.Name);
            Assert.IsFalse(viewModel.SelectedItem.IsDirty);
        }

        #endregion

        #region SaveAndNew

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            viewModel.SelectedItem.Name = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.SelectedItem.Name);
            Assert.IsTrue(viewModel.SelectedItem.IsNew);
        }

        [Test]
        public void SaveAndNewCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = viewModel.SaveAndNewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndNew_16, cmd.SmallIcon);
        }

        #endregion

        #region Save And Close

        [Test]
        public void SaveAndCloseCommand_Execution()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            viewModel.SelectedItem.Name = "SaveAndCloseTest";

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.SaveAndCloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");

            //check that the item was saved
            Assert.AreEqual("SaveAndCloseTest", PlanogramImportTemplateInfoList.FetchByEntityId(this.EntityId).Last().Name);
        }

        [Test]
        public void SaveAndCloseCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = viewModel.SaveAndCloseCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndClose_16, cmd.SmallIcon);
        }

        #endregion

        #region Save As To Repository

        [Test]
        public void SaveAsToRepositoryCommand_DisabledWhenNoCreatePerm()
        {
            InsertTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramImportTemplateCreate.ToString());

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);
            viewModel.SelectedItem.Name = "NewName";

            Assert.IsFalse(viewModel.SaveAsToRepositoryCommand.CanExecute());
        }

        [Test]
        public void SaveAsToRepositoryCommand_Execution()
        {
            InsertTemplates();
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            Int32 preCount = PlanogramImportTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);

            String newName = "SaveAsName";

            WindowService.PromptForSaveAsNameSetReponse(true, newName);
            viewModel.SaveAsToRepositoryCommand.Execute();

            Assert.AreEqual(newName, viewModel.SelectedItem.Name);
            Assert.IsFalse(viewModel.SelectedItem.IsDirty);

            Assert.AreEqual(preCount + 1, PlanogramImportTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void SaveAsToRepositoryCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = viewModel.SaveAsToRepositoryCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAs_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.F12, cmd.InputGestureKey);
        }

        #endregion

        #region Delete


        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            InsertTemplates();
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            Assert.IsTrue(viewModel.SelectedItem.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);
            Assert.IsFalse(viewModel.SelectedItem.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_DisabledWhenNoDeletePerm()
        {
            InsertTemplates();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramImportTemplateDelete.ToString());

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);

            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            InsertTemplates();
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            Int32 preCount = PlanogramImportTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(true);
            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.SelectedItem.IsNew);
            Assert.AreEqual(preCount - 1, PlanogramImportTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_CancelUserConfirm()
        {
            InsertTemplates();
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            Int32 preCount = PlanogramImportTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailablePlanogramImportTemplates.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(false);
            viewModel.DeleteCommand.Execute();

            Assert.IsFalse(viewModel.SelectedItem.IsNew);
            Assert.AreEqual(preCount, PlanogramImportTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_HasExpectedProperties()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = viewModel.DeleteCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Delete, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Delete_16, cmd.SmallIcon);
        }

        #endregion

        #region Close

        [Test]
        public void CloseCommand_Execution()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.CloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");
        }

        #endregion

        [Test]
        public void Command_ClearAllMappings()
        {
            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            var cmd = viewModel.ClearAllMappingsCommand;


            PlanogramImportTemplateFieldInfoList externalFieldList =
                PlanogramImportTemplateFieldInfoList.NewPlanogramImportTemplateFieldInfoList(viewModel.SelectedFileType, viewModel.SelectedVersion);

            if (externalFieldList.Count > 0)
            {
                foreach (MappingRow row in viewModel.PlanogramMappings)
                {
                    row.ExternalField = externalFieldList[0];
                }

                cmd.Execute();

                foreach (MappingRow row in viewModel.PlanogramMappings)
                {
                    Assert.IsNull(row.ExternalField, "Mapping still present.");
                }
            }
        }
        

        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            InsertTemplates();

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            InsertTemplates();

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.OpenCommand.Execute(1);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            InsertTemplates();

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.SelectedItem.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsTrue(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            InsertTemplates();

            var viewModel = new PlanogramImportTemplateMaintenanceViewModel(PlanogramImportFileType.SpacemanV9);
            viewModel.OpenCommand.Execute(1);
            viewModel.SelectedItem.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);
            Assert.IsTrue(continueWithItemChangeFired);
        }


        #endregion


    }
}
