﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// CCM-25534 : I.George
//	Initial version
//  V8-27964 : A.Silva
//      Added ProductLevel properties tests.

#endregion

#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Updated with new default data updates on entities
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EntityMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Security;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.SysAdmin.EntityMaintenance
{
    [TestFixture]
    class EntityMaintenanceViewModelTest : TestBase
    {
        #region Test Fixture Helpers

        private const Int32 NumentitiesInserted = 2;

        private EntityInfo _testEntityInfo;

        private void InsertTestData()
        {
            for (Int32 i = 0; i < 3; i++)
            {
                Entity e1 = Entity.NewEntity();
                e1.Name = "e"+i;
                e1 = e1.Save();
            }

            _testEntityInfo = EntityInfoList.FetchAllEntityInfos().First();
            TestDataHelper.PopulatePlanogramHierarchy(_testEntityInfo.Id);
        }

        #endregion

        #region Properties

        [Test]
        public void AvailableEntities_FetchedOnStartup()
        {
            //insert some items.
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableEntities.Count);

            String propertyName = "AvailableEntities";
            Assert.AreEqual(propertyName, EntityMaintenanceViewModel.AvailableEntitiesProperty.Path);
        }

        [Test]
        public void AvailableEntities_UpdatedOnEntityInsert()
        {
            var viewModel = new EntityMaintenanceViewModel();
            Int32 count = viewModel.AvailableEntities.Count;

            //save the new item
            viewModel.SelectedEntity.Name = "w1";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailableEntities.Count);
        }

        [Test]
        public void AvailableEntities_UpdatedOnEntityDelete()
        {
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            Int32 count = viewModel.AvailableEntities.Count;

            //open and delete a item
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.Last().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailableEntities.Count);
        }

        [Test]
        public void SelectedEntity_InitializedAsNew()
        {
            var viewModel = new EntityMaintenanceViewModel();
            Assert.IsTrue(viewModel.SelectedEntity.IsNew);
            Assert.IsNullOrEmpty(viewModel.SelectedEntity.Name);

            String propertyName = "SelectedEntity";
            Assert.AreEqual(propertyName, EntityMaintenanceViewModel.SelectedEntityProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(viewModel.GetType(), propertyName);

        }

        [Test]
        public void HasProductLevels_ShouldHavePropertyBinding()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            const String expectation = "Binding Property Path should be set correctly.";

            Assert.AreEqual("HasProductLevels", EntityMaintenanceViewModel.HasProductLevelsProperty.Path, expectation);
        }

        [Test]
        public void HasProductLevels_ShouldBeReadOnly()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            AssertHelper.AssertPropertyIsReadOnly(viewModel.GetType(), "HasProductLevels");
        }

        [Test]
        public void HasProductLevels_WhenEntityNew_ShouldReturnFalse()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            viewModel.NewCommand.Execute();

            Assert.IsFalse(viewModel.HasProductLevels, 
                "A new entity should not have product levels.");
        }

        [Test]
        public void HasProductLevels_WhenEntityLoaded_ShouldReturnTrue()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            viewModel.OpenCommand.Execute(_testEntityInfo.Id);

            Assert.IsTrue(viewModel.HasProductLevels, "An entity should have product levels.");
        }

        [Test]
        public void AvailableProductLevels_ShouldHavePropertyBinding()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            Assert.AreEqual("AvailableProductLevels", EntityMaintenanceViewModel.AvailableProductLevelsProperty.Path, 
                "Binding Property Path should be set correctly.");
        }

        [Test]
        public void AvailableProductLevels_ShouldBeReadOnly()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();


            AssertHelper.AssertPropertyIsReadOnly(viewModel.GetType(), "AvailableProductLevels");
        }

        [Test]
        public void AvailableProductLevels_CollectionShouldBeReadOnlyObservable()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();


            AssertHelper.AssertIsReadOnlyObservable(viewModel.AvailableProductLevels);
        }

        [Test]
        public void AvailableProductLevels_WhenEntityNew_ShouldBeEmpty()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();


            CollectionAssert.IsEmpty(viewModel.AvailableProductLevels, 
                "The collection of available product levels for a new entity should be empty.");
        }

        [Test]
        public void AvailableProductLevels_FetchedOnStartup()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            viewModel.OpenCommand.Execute(_testEntityInfo.Id);

            CollectionAssert.IsNotEmpty(viewModel.AvailableProductLevels, 
                "The collection of available product levels for a recently loaded entity should contain items.");
        }

        [Test]
        public void AvailableProductLevels_WhenEntityLoaded_ShouldHaveNoneItem()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            viewModel.OpenCommand.Execute(_testEntityInfo.Id);
            
            Assert.IsTrue(String.IsNullOrEmpty(viewModel.AvailableProductLevels.First().Value as String),
                "The collection of available product levels for a recently loaded entity should contain the None item.");
        }

        [Test]
        public void SelectedProductLevel_ShouldHavePropertyBinding()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            Assert.AreEqual("SelectedProductLevel", EntityMaintenanceViewModel.SelectedProductLevelProperty.Path, 
                "Binding Property Path should be set correctly.");
        }

        [Test]
        public void AvailableProductLabels_UpdatedOnEntityChange()
        {
            InsertTestData();
            Int32 entityId =_testEntityInfo.Id;

            //Add a label to the test entity
            Label lab1 = Label.NewLabel(entityId, LabelType.Product);
            lab1.Name = "label1";
            lab1.Save();

            var viewModel = new EntityMaintenanceViewModel();

            Assert.AreEqual(1, viewModel.AvailableProductLabels.Count, "Should contain only the none value.");

            //set the selected entity
            viewModel.OpenCommand.Execute(entityId);
            Assert.AreEqual(7, viewModel.AvailableProductLabels.Count, "Should now contain 7 values"); //5 default, 1 new, 1 none


            // call new
            viewModel.NewCommand.Execute();
            Assert.AreEqual(1, viewModel.AvailableProductLabels.Count, "Should contain only the none value.");

        }

        [Test]
        public void AvailableProductLabels_OnlyShowsProductLabels()
        {
            InsertTestData();
            Int32 entityId = _testEntityInfo.Id;

            //Add a label to the test entity
            Label lab1 = Label.NewLabel(entityId, LabelType.Product);
            lab1.Name = "label1";
            lab1.Save();

            //Add a label to the test entity
            Label lab2 = Label.NewLabel(entityId, LabelType.Fixture);
            lab2.Name = "label2";
            lab2.Save();

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(entityId);
            Assert.AreEqual(7, viewModel.AvailableProductLabels.Count, "Should now contain 7 values not 1"); //5 default, 1 new, 1 none
        }

        [Test]
        public void AvailableFixtureLabels_UpdatedOnEntityChange()
        {
            InsertTestData();
            Int32 entityId = _testEntityInfo.Id;

            //Add a label to the test entity
            Label lab1 = Label.NewLabel(entityId, LabelType.Fixture);
            lab1.Name = "label1";
            lab1.Save();

            var viewModel = new EntityMaintenanceViewModel();

            Assert.AreEqual(1, viewModel.AvailableFixtureLabels.Count, "Should contain only the none value.");

            //set the selected entity
            viewModel.OpenCommand.Execute(entityId);
            Assert.AreEqual(3, viewModel.AvailableFixtureLabels.Count, "Should now contain 3 values"); //2 default, 1 new
            
            // call new
            viewModel.NewCommand.Execute();
            Assert.AreEqual(1, viewModel.AvailableFixtureLabels.Count, "Should contain only the none value.");
        }

        [Test]
        public void AvailableFixtureLabels_OnlyShowsFixtureLabels()
        {
            InsertTestData();
            Int32 entityId = _testEntityInfo.Id;

            //Add a label to the test entity
            Label lab1 = Label.NewLabel(entityId, LabelType.Product);
            lab1.Name = "label1";
            lab1.Save();

            //Add a label to the test entity
            Label lab2 = Label.NewLabel(entityId, LabelType.Fixture);
            lab2.Name = "label2";
            lab2.Save();

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(entityId);
            Assert.AreEqual(3, viewModel.AvailableFixtureLabels.Count, "Should now contain 3 values, not 1");
        }

        [Test]
        public void AvailableHighlights_UpdatedOnEntityChange()
        {
            InsertTestData();
            Int32 entityId = _testEntityInfo.Id;

            //add a highlihgt
            Highlight h1 = Highlight.NewHighlight(entityId);
            h1.Name = "h1";
            h1.Save();

            var viewModel = new EntityMaintenanceViewModel();

            Assert.AreEqual(1, viewModel.AvailableHighlights.Count, "Should contain only the none value.");

            //set the selected entity
            viewModel.OpenCommand.Execute(entityId);
            Assert.AreEqual(41, viewModel.AvailableHighlights.Count, "Should now contain 2 values"); //39 default, 1 none, 1 new


            // call new
            viewModel.NewCommand.Execute();
            Assert.AreEqual(1, viewModel.AvailableHighlights.Count, "Should contain only the none value.");
        }

        #endregion

        #region Commands

        #region New

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new EntityMaintenanceViewModel();
            RelayCommand cmd = viewModel.NewCommand;

            //check and execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            viewModel.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();
            Entity preExecuteItem = viewModel.SelectedEntity;

            cmd.Execute();
            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;

            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, viewModel.SelectedEntity, "this item should have changed.");
            Assert.IsTrue(viewModel.SelectedEntity.IsNew, "this item should be new");
            Assert.IsTrue(viewModel.SelectedEntity.IsInitialized, " this item should be marked initialized");
            Assert.AreNotEqual(viewModel.SelectedEntity, preExecuteItem);
        }

        [Test]
        public void NewCommand_DisabledWhenNoCreatePerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.EntityCreate.ToString());

            //check
            var viewModel = new EntityMaintenanceViewModel();
            Assert.IsFalse(viewModel.NewCommand.CanExecute());
        }

        [Test]
        public void NewCommand_HasExpectedProperties()
        {
            var viewModel = new EntityMaintenanceViewModel();

            RelayCommand cmd = viewModel.NewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_New_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.New_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.N, cmd.InputGestureKey);
        }

        #endregion

        #region Open

        [Test]
        public void OpenCommand_Execution()
        {
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            RelayCommand<Int32?> cmd = viewModel.OpenCommand;

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();
            viewModel.PropertyChanged += base.TestModel_PropertyChanged;
            Entity preExecuteIem = viewModel.SelectedEntity;

            EntityInfo loadInfo = viewModel.AvailableEntities.Last();
            cmd.Execute(loadInfo.Id);
            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;

            Assert.AreNotEqual(preExecuteIem, viewModel.SelectedEntity, "item should not be changed");
            Assert.AreEqual(loadInfo.Id, viewModel.SelectedEntity.Id, "the entity id should have been loaded");
            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications, "selectedstore should have change notification");
        }

        #endregion

        #region Save

        [Test]
        public void SaveCommand_Execution()
        {
            var viewModel = new EntityMaintenanceViewModel();
            RelayCommand cmd = viewModel.SaveCommand;

            //check that command can execute
            Assert.IsFalse(cmd.CanExecute(), "save shoud be disabled as invalid object");
            viewModel.SelectedEntity.Name = "st1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check that the command execute
            base.PropertyChangedNotifications.Clear();

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;
            cmd.Execute();
            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;

            //check that the selected store has been updated
            Assert.IsFalse(viewModel.SelectedEntity.IsNew, "the store should no longer be new");
            Assert.IsFalse(viewModel.SelectedEntity.IsDirty, "the store should have been saved");
            Assert.AreEqual("st1", viewModel.SelectedEntity.Name, "the store should be thesame");
            Assert.AreNotEqual(0, viewModel.SelectedEntity.Id, "the store should have been given a valid id");
            Assert.Contains(EntityMaintenanceViewModel.SelectedEntityProperty.Path, base.PropertyChangedNotifications);

            //check an info has been added for the new store
            EntityInfo addedInfo = viewModel.AvailableEntities.FirstOrDefault(l => l.Id == viewModel.SelectedEntity.Id);
            Assert.IsNotNull(addedInfo, "an info for new store should have been added");

            //check that another store with the same location cannot be saved
            viewModel.NewCommand.Execute();
            viewModel.SelectedEntity.Name = "st2";
            cmd.Execute();
        }

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new EntityMaintenanceViewModel();

            Assert.IsFalse(viewModel.SelectedEntity.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.SelectedEntity.Name = "Test1";

            Assert.IsTrue(viewModel.SelectedEntity.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenNewItemNoCreatePerm()
        {
            var viewModel = new EntityMaintenanceViewModel();
            viewModel.SelectedEntity.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.EntityCreate.ToString());

            viewModel = new EntityMaintenanceViewModel();
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenOldItemNoCreatePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.EntityCreate.ToString());

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.First().Id);
            viewModel.SelectedEntity.Name = "NewName";

            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenNewItemNoEditPerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.EntityEdit.ToString());

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.SelectedEntity.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenOldItemNoEditPerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.EntityEdit.ToString());

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.First().Id);
            viewModel.SelectedEntity.Name = "NewName";

            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_HasExpectedProperties()
        {
            var viewModel = new EntityMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Save_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_32, cmd.Icon);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Save_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.S, cmd.InputGestureKey);
        }

        #endregion

        #region SaveAs

        [Test]
        public void SaveAsCommand_DisabledWhenNoCreatePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.EntityCreate.ToString());

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.First().Id);
            viewModel.SelectedEntity.Name = "NewName";

            Assert.IsFalse(viewModel.SaveAsCommand.CanExecute());
        }

        [Test]
        public void SaveAsCommand_Execution()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            Int32 preCount = EntityInfoList.FetchAllEntityInfos().Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.Last().Id);

            String newName = "SaveAsName";

            WindowService.PromptForSaveAsNameSetReponse(true, newName);
            viewModel.SaveAsCommand.Execute();

            Assert.AreEqual(newName, viewModel.SelectedEntity.Name);
            Assert.IsFalse(viewModel.SelectedEntity.IsDirty);

            Assert.AreEqual(preCount + 1, EntityInfoList.FetchAllEntityInfos().Count);
        }

        [Test]
        public void SaveAsCommand_HasExpectedProperties()
        {
            var viewModel = new EntityMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAsCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAs_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAs_16, cmd.SmallIcon);
            Assert.AreEqual(System.Windows.Input.ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.F12, cmd.InputGestureKey);
        }

        #endregion

        #region SaveAndNew

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new EntityMaintenanceViewModel();

            viewModel.SelectedEntity.Name = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.SelectedEntity.Name);
            Assert.IsTrue(viewModel.SelectedEntity.IsNew);
        }

        [Test]
        public void SaveAndNewCommand_HasExpectedProperties()
        {
            var viewModel = new EntityMaintenanceViewModel();

            RelayCommand cmd = viewModel.SaveAndNewCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndNew_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.SaveAndNew_16, cmd.SmallIcon);
        }

        #endregion

        #region SaveAndClose

        [Test]
        public void SaveAndCloseCommand_Execution()
        {
            var viewModel = new EntityMaintenanceViewModel();

            viewModel.SelectedEntity.Name = "Test1";

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.SaveAndCloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");

            //check that the item was saved
            Assert.AreEqual("Test1", EntityInfoList.FetchAllEntityInfos().Last().Name);
        }

        #endregion

        #region Delete

        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            Assert.IsTrue(viewModel.SelectedEntity.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.Last().Id);
            Assert.IsFalse(viewModel.SelectedEntity.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_DisabledWhenNoDeletePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.EntityDelete.ToString());

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.Last().Id);

            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            Int32 preCount = EntityInfoList.FetchAllEntityInfos().Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.Last().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(true);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute(), "could not execute");
            viewModel.DeleteCommand.Execute();
            WindowService.AssertResponsesUsed();

            Assert.IsTrue(viewModel.SelectedEntity.IsNew);
            Assert.AreEqual(preCount - 1, EntityInfoList.FetchAllEntityInfos().Count);
        }

        [Test]
        public void DeleteCommand_CancelUserConfirm()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();

            Int32 preCount = EntityInfoList.FetchAllEntityInfos().Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(false);
            viewModel.DeleteCommand.Execute();

            Assert.IsFalse(viewModel.SelectedEntity.IsNew);
            Assert.AreEqual(preCount, EntityInfoList.FetchAllEntityInfos().Count);
        }

        [Test]
        public void DeleteCommand_HasExpectedProperties()
        {
            var viewModel = new EntityMaintenanceViewModel();

            RelayCommand cmd = viewModel.DeleteCommand;
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Delete, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Workflow.Client.Wpf.ImageResources.Delete_16, cmd.SmallIcon);
        }

        [Test]
        public void DeleteCommand_DisabledForCurrentEntity()
        {
            //Gem26314 - checks that the Entity opened cannot be deleted
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            RelayCommand cmd = viewModel.DeleteCommand;
            viewModel.OpenCommand.Execute(this.EntityId);
            Assert.IsFalse(cmd.CanExecute());

            //add and delete an entity
            EntityInfo entity = viewModel.AvailableEntities.Last();
            viewModel.OpenCommand.Execute(entity.Id);
            Assert.IsTrue(cmd.CanExecute(), "delete an entity");
        }

        #endregion

        #region Close

        [Test]
        public void CloseCommand_Execution()
        {
            var viewModel = new EntityMaintenanceViewModel();

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.CloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");
        }

        #endregion

        #region SyncMerchandisingHierarchy

        [Test]
        public void SyncMerchandisingHierarchyCommand_Execution()
        {
            InsertTestData();
            var viewModel = new EntityMaintenanceViewModel();
            RelayCommand cmd = viewModel.SyncMerchandisingHierarchyCommand;
            Assert.AreEqual("SyncMerchandisingHierarchyCommand", EntityMaintenanceViewModel.SyncMerchandisingHierarchyCommandProperty.Path);

            // open entity
            viewModel.OpenCommand.Execute(_testEntityInfo.Id);
            viewModel.SelectedProductLevel = (Int32)viewModel.AvailableProductLevels.FirstOrDefault(p => p.Value != null).Value;

            // check can execute
            Assert.IsTrue(cmd.CanExecute());

            // last merchandising hierarchy synced date should be null as it hasn't been synced before
            Assert.IsNull(viewModel.SelectedEntity.DateLastMerchSynced);

            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);
            WindowService.AddNullResponse(NUnitWindowService.ShowIndeterminateBusyMethod);
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);
            cmd.Execute();

            // should now equal the datetime of now
            Assert.AreEqual(viewModel.SelectedEntity.DateLastMerchSynced.Value.Date, DateTime.UtcNow.Date);

        }

        [Test]
        public void SyncMerchandisingHierarchyCommand_DisabledWhenNoSelectedLevel()
        {
            //v8-28928 should be disabled if no level has been selected.
            var viewModel = new EntityMaintenanceViewModel();
            viewModel.SelectedEntity.Name = "test1";
            viewModel.SaveCommand.Execute();
            viewModel.SelectedProductLevel = (Int32)viewModel.AvailableProductLevels.FirstOrDefault(p => p.Value != null).Value;

            Assert.IsTrue(viewModel.SyncMerchandisingHierarchyCommand.CanExecute(), "Have level should be enabled");

            //set selection to no level
            viewModel.SelectedProductLevel = null;

            Assert.IsFalse(viewModel.SyncMerchandisingHierarchyCommand.CanExecute(), "No level should be disabled");
        }

        [Test]
        public void SyncMerchandisingHierarchyCommand_DisabledIfNoPerm()
        {
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.First().Id);
            viewModel.SelectedProductLevel = (Int32)viewModel.AvailableProductLevels.FirstOrDefault(p => p.Value != null).Value;

            Assert.IsTrue(viewModel.SyncMerchandisingHierarchyCommand.CanExecute(),
                "have perm, should be enabled.");

            viewModel.Dispose();
            viewModel = null;


            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.SyncWithMerchandisingHierarchy.ToString());


            viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableEntities.First().Id);
            viewModel.SelectedProductLevel = (Int32)viewModel.AvailableProductLevels.FirstOrDefault(p => p.Value != null).Value;

            Assert.IsFalse(viewModel.SyncMerchandisingHierarchyCommand.CanExecute(),
                "no perm, should be disabled");
        }

        [Test]
        public void SyncMerchandisingHierarchyCommand_ConfirmsWithUser()
        {
            //v8-28928 should confirm if entity is dirty and if it is not.
            var viewModel = new EntityMaintenanceViewModel();
            viewModel.SelectedEntity.Name = "test1";
            viewModel.SaveCommand.Execute();
            viewModel.SelectedProductLevel = (Int32)viewModel.AvailableProductLevels.FirstOrDefault(p => p.Value != null).Value;

            //execute the sync but cancel the confirm
            Assert.IsTrue(viewModel.SyncMerchandisingHierarchyCommand.CanExecute(), "cannot execute");
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button2);
            viewModel.SyncMerchandisingHierarchyCommand.Execute();
            WindowService.AssertResponsesUsed();

            //save the entity
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
            viewModel.SaveCommand.Execute();

            //execute the sync again - confirm should still show even through entity is saved.
            Assert.IsTrue(viewModel.SyncMerchandisingHierarchyCommand.CanExecute(), "cannot execute");
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button2);
            viewModel.SyncMerchandisingHierarchyCommand.Execute();
            WindowService.AssertResponsesUsed();

        }

        #endregion

        #region SetImageProductAttributeFilterCommand

        [Test]
        public void SetImageProductAttributeFilterCommand_Execution()
        {
            var viewmodel = new EntityMaintenanceViewModel();
            viewmodel.SelectedEntity.Name = "e1";

            viewmodel.SelectedEntity.SystemSettings.ProductImageSource = Framework.Planograms.Helpers.RealImageProviderType.Folder;

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (a) =>
                {
                    FieldSelectorViewModel winView = (FieldSelectorViewModel)a.WindowParameters[0];
                    winView.SelectedField = ObjectFieldInfo.NewObjectFieldInfo(typeof(Product), Product.FriendlyName, Product.FrontOverhangProperty);
                    winView.OKCommand.Execute();
                });
            viewmodel.SetImageProductAttributeFilterCommand.Execute();

            WindowService.AssertResponsesUsed();

            Assert.AreEqual(ObjectFieldInfo.NewObjectFieldInfo(typeof(Product), Product.FriendlyName, Product.FrontOverhangProperty).FieldPlaceholder,
                viewmodel.SelectedEntity.SystemSettings.ProductImageAttribute, "Selected field not set");

        }
        #endregion

        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.SelectedEntity.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsTrue(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            InsertTestData();

            var viewModel = new EntityMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.SelectedEntity.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);
            Assert.IsTrue(continueWithItemChangeFired);
        }


        #endregion
    }
}