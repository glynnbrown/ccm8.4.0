﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26124 : L.Ineson
//	Created

#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.UserMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.RolePermissions;
using System.Collections.Generic;
using Galleria.Ccm.Security;
using System.Text;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.SysAdmin.RolePermissions
{
    [TestFixture]
    public class RolePermissionsViewModelTests : TestBase<RolePermissionsViewModel>
    {
        #region Test Fixture Helpers

        private Int32 _entityCount;
        private UserInfoListViewModel _userInfoListView;
        private RoleInfoListViewModel _roleInfoListView;

        public override void  Setup()
        {
            base.Setup();

            Entity currentEntity = Entity.FetchById(this.EntityId);
            _entityCount = EntityInfoList.FetchAllEntityInfos().Count;

            TestDataHelper.InsertUserDtos(base.DalFactory, 10);
            TestDataHelper.InsertRoleDtos(base.DalFactory, 10, currentEntity.Id);

            _userInfoListView = new UserInfoListViewModel();
            _userInfoListView.FetchAll();
            _roleInfoListView = new RoleInfoListViewModel();
            _roleInfoListView.FetchAll();

            this.TestModel = new RolePermissionsViewModel();
        }


        #endregion

        #region Properties

        [Test]
        public void Property_CurrentTab()
        {
            String propertyName = "CurrentTab";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.CurrentTabProperty.Path);

            Assert.AreEqual(RoleTab.General, this.TestModel.CurrentTab);
        }

        [Test]
        public void Property_IsNotAdmin()
        {
            String propertyName = "IsNotAdmin";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.IsNotAdminProperty.Path);

            Assert.IsTrue(this.TestModel.IsNotAdmin);
        }

        [Test]
        public void Property_AvailableEntities()
        {
            String propertyName = "AvailableEntities";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.AvailableEntitiesProperty.Path);

            Assert.AreEqual(_entityCount, this.TestModel.AvailableEntities.Count());
        }

        [Test]
        public void Property_AvailableRoles()
        {
            String propertyName = "AvailableRoles";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.AvailableRolesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableRoles);

            Assert.AreEqual(_roleInfoListView.Model.Count, this.TestModel.AvailableRoles.Count);
            foreach (RoleInfo roleInfo in _roleInfoListView.BindableCollection)
            {
                RoleInfo foundRole = this.TestModel.AvailableRoles.First(r => r.Id == roleInfo.Id);
                Assert.AreEqual(roleInfo.Description, foundRole.Description);
                Assert.AreEqual(roleInfo.IsAdministratorRole, foundRole.IsAdministratorRole);
                Assert.AreEqual(roleInfo.Name, foundRole.Name);
            }
        }

        [Test]
        public void Property_SelectedRole()
        {
            String propertyName = "SelectedRole";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.SelectedRoleProperty.Path);

            Assert.IsTrue(this.TestModel.SelectedRole.IsNew);
        }

        [Test]
        public void Property_AvailablePermissions()
        {
            String propertyName = "AvailablePermissions";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.AvailablePermissionsProperty.Path);
        }

        [Test]
        public void Property_AvailableUsers()
        {
            String propertyName = "AvailableUsers";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.AvailableUsersProperty.Path);

            AssertHelper.AssertIsEditableObservable(this.TestModel.AvailableUsers);

            Assert.AreEqual(_userInfoListView.Model.Count, this.TestModel.AvailableUsers.Count);
            foreach (UserInfo userInfo in _userInfoListView.BindableCollection)
            {
                UserInfo foundUser = this.TestModel.AvailableUsers.First(u => u.Id == userInfo.Id);
                Assert.AreEqual(userInfo.FirstName, foundUser.FirstName);
                Assert.AreEqual(userInfo.LastName, foundUser.LastName);
                Assert.AreEqual(userInfo.UserName, foundUser.UserName);
            }
        }

        [Test]
        public void Property_SelectedAvailableUsers()
        {
            String propertyName = "SelectedAvailableUsers";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.SelectedAvailableUsersProperty.Path);

            AssertHelper.AssertIsEditableObservable(this.TestModel.SelectedAvailableUsers);
        }

        [Test]
        public void Property_TakenUsers()
        {
            String propertyName = "TakenUsers";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.TakenUsersProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.TakenUsers);
        }

        [Test]
        public void Property_SelectedTakenUsers()
        {
            String propertyName = "SelectedTakenUsers";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.SelectedTakenUsersProperty.Path);

            AssertHelper.AssertIsEditableObservable(this.TestModel.SelectedTakenUsers);
        }

        [Test]
        public void Property_AvailableEntites()
        {
            String propertyName = "AvailableEntities";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.AvailableEntitiesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableEntities);
        }

        [Test]
        public void Property_SelectedEntities()
        {
            String propertyName = "SelectedEntities";
            Assert.AreEqual(propertyName, RolePermissionsViewModel.SelectedEntitiesProperty.Path);

            AssertHelper.AssertIsEditableObservable(this.TestModel.SelectedEntities);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            this.TestModel = new RolePermissionsViewModel();

            Assert.IsTrue(this.TestModel.SelectedRole.IsNew);
            Assert.AreEqual(11, this.TestModel.AvailableRoles.Count());

            //Ensure all of the permission header groups have been created
            Assert.AreEqual(1, this.TestModel.AvailablePermissions.Count);
            //Get all lowest level permissions
            IEnumerable<RolePermissionGroupObject> permissionGroups = this.TestModel.AvailablePermissions.First().GetAllChildGroups().Where(p => p.SourcePermission != null);
            Assert.AreNotEqual(0, permissionGroups.Count());
        }

        #endregion

        #region Commands

        #region Command_New

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.NewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            Role preExecuteRole = this.TestModel.SelectedRole;
            cmd.Execute();
            Assert.AreNotEqual(preExecuteRole, this.TestModel.SelectedRole);
            Assert.IsTrue(this.TestModel.SelectedRole.IsNew);
        }

        #endregion

        #region Command_Open

        [Test]
        public void Command_Open()
        {
            RelayCommand cmd = this.TestModel.OpenCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.OpenCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();

            RoleInfo loadInfo = this.TestModel.AvailableRoles.Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreEqual(loadInfo.Id, this.TestModel.SelectedRole.Id);
            Assert.Contains(RolePermissionsViewModel.SelectedRoleProperty.Path, base.PropertyChangedNotifications);
        }

        #endregion

        #region Command_Save

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.SaveCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as role is invalid and new");
            this.TestModel.SelectedRole.Name = "name1";
            this.TestModel.SelectedRole.Description = "description1";

            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected role has been updated
            Assert.IsFalse(this.TestModel.SelectedRole.IsNew, "The role should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedRole.IsNew, "The role should no longer be dirty");
            Assert.AreEqual("name1", this.TestModel.SelectedRole.Name);
            Assert.AreNotEqual(0, this.TestModel.SelectedRole.Id);
            Assert.Contains(RolePermissionsViewModel.SelectedRoleProperty.Path, base.PropertyChangedNotifications);

            //check and info has been added for the new role
            RoleInfo addedInfo = this.TestModel.AvailableRoles.FirstOrDefault(r => r.Id == this.TestModel.SelectedRole.Id);
            Assert.IsNotNull(addedInfo, "An info for the new role should have been added");

            //check another role with the same name cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.SelectedRole.Name = addedInfo.Name;
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to save while the name is the same as an existing role");
        }

        #endregion

        #region Command_SaveAs

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.SaveAsCommandCheckExpectedValues(cmd);

            //save a role to saveAs
            this.TestModel.SelectedRole.Name = "name1";
            this.TestModel.SelectedRole.Description = "description1";
            this.TestModel.SaveCommand.Execute();

            //make valid
            this.TestModel.SelectedRole.Name = "name2";
            Assert.IsTrue(cmd.CanExecute(), "Should be possible to execute now the name has been changed");

            //check execute
            Assert.AreEqual(12, this.TestModel.AvailableRoles.Count());
            cmd.Execute();
            Assert.AreEqual(13, this.TestModel.AvailableRoles.Count());
        }

        #endregion

        #region Command_SaveAndClose

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.SaveAndCloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to save and close");

            //make valid
            this.TestModel.SelectedRole.Name = "name1";
            this.TestModel.SelectedRole.Description = "description1";

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be able to save and close");

            //Execute
            Assert.AreEqual(11, this.TestModel.AvailableRoles.Count());
            this.TestModel.SaveAndCloseCommand.Execute();
            Assert.AreEqual(12, this.TestModel.AvailableRoles.Count());
        }

        #endregion

        #region Command_SaveAndNew

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.SaveAndNewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to save and new");

            //make valid
            this.TestModel.SelectedRole.Name = "name1";
            this.TestModel.SelectedRole.Description = "description1";

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be able to save and new");

            //Execute
            Assert.AreEqual(11, this.TestModel.AvailableRoles.Count());
            this.TestModel.SaveAndNewCommand.Execute();
            Assert.AreEqual(12, this.TestModel.AvailableRoles.Count());

            //Check new role was loaded
            Assert.IsTrue(this.TestModel.SelectedRole.IsNew);
        }

        #endregion

        #region Command_Delete

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.DeleteCommandCheckExpectedValues(cmd);

            //check CanExecute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute on a new role");

            //open a role
            RoleInfo loadInfo = this.TestModel.AvailableRoles.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);

            Assert.IsTrue(cmd.CanExecute(), "It should be possible to execute now a saved role has been opened");

            //Cannot check execute due to the delete confirmation message box
        }

        #endregion

        #region Command_AddUser

        [Test]
        public void Command_AddUser()
        {
            RelayCommand cmd = this.TestModel.AddUserCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check CanExecute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute without a user selected");

            //select a user
            this.TestModel.SelectedAvailableUsers.Add(this.TestModel.AvailableUsers.Last());
            Assert.IsTrue(cmd.CanExecute(), "Should be possible to execute now a user is selected");

            //check execute
            cmd.Execute();
            Assert.AreEqual(1, this.TestModel.TakenUsers.Count());
            Assert.AreEqual(this.TestModel.SelectedAvailableUsers.First(), this.TestModel.TakenUsers.First());

            //Check a roleMember was created and added to the role
            RoleMember addedMember = this.TestModel.SelectedRole.Members.FirstOrDefault(p => p.UserId ==
                this.TestModel.TakenUsers.First().Id);
            Assert.IsNotNull(addedMember);
        }

        #endregion

        #region Command_RemoveUser

        [Test]
        public void Command_RemoveUser()
        {
            RelayCommand<UserInfo> cmd = this.TestModel.RemoveUserCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check CanExecute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute without a user selected");

            //select a user
            this.TestModel.SelectedTakenUsers.Add(this.TestModel.AvailableUsers.Last());
            Assert.IsTrue(cmd.CanExecute(), "Should be possible to execute now a user is selected");

            //check execute
            cmd.Execute();

            //check taken users no longer contains any records
            Assert.AreEqual(0, this.TestModel.TakenUsers.Count());

            //Check a role member was removed from the role
            RoleMember removedMember = this.TestModel.SelectedRole.Members.FirstOrDefault(p => p.UserId ==
                this.TestModel.TakenUsers.First().Id);
            Assert.IsNull(removedMember);
        }

        #endregion

        #region Command_Close

        [Test]
        public void Command_CloseCommand()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", RolePermissionsViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Checks that all required permissions are available
        /// so that we notice if we have forgotten to add any!
        /// </summary>
        [Test]
        public void Other_ConstructPermissions()
        {
            #region Exclusion List
            //get a list of perms that are not required on the screen.

            List<DomainPermission> exclusionList = new List<DomainPermission>()
            {
                //special
                DomainPermission.Authenticated,
                DomainPermission.Restricted,
                DomainPermission.Administrator
            };
            #endregion

            //get the list of expected perms
            List<DomainPermission> expectedPermissions = new List<DomainPermission>();
            foreach (DomainPermission permission in Enum.GetValues(typeof(DomainPermission)))
            {
                if (!exclusionList.Contains(permission))
                {
                    expectedPermissions.Add(permission);
                }
            }

            //remove perms that are covered by the screen
            List<DomainPermission> screenPerms = new List<DomainPermission>();
            foreach (RolePermissionGroupObject permObject in this.TestModel.AvailablePermissions)
            {
                foreach (RolePermissionGroupObject childPermObject in permObject.GetAllChildGroups())
                {
                    if (childPermObject.SourcePermission.HasValue)
                    {
                        expectedPermissions.Remove(childPermObject.SourcePermission.Value);
                    }
                }
            }


            if (expectedPermissions.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("The following perms are missing:");
                foreach (DomainPermission permission in expectedPermissions)
                {
                    sb.AppendLine(permission.ToString());
                }
                Assert.AreEqual(0, expectedPermissions.Count, sb.ToString());
            }

        }

        [Test]
        public void Other_CreateAndSaveRole()
        {
            this.TestModel.SelectedRole.Name = "R1";
            this.TestModel.SelectedRole.Description = "R1";

            this.TestModel.AvailablePermissions[0].ChildObjects[0].IsChecked = true;

            this.TestModel.SaveCommand.Execute();

            Role role = Role.FetchById(this.TestModel.SelectedRole.Id);
            Assert.AreNotEqual(0, role.Permissions.Count);
        }


        #endregion
    }
}
