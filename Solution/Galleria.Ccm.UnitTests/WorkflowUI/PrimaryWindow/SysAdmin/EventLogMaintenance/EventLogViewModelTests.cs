﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EventLogMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.SysAdmin.EventLogMaintenance
{
    [TestFixture]
    public class EventLogViewModelTests : TestBase<EventLogViewModel>
    {
        #region Test Fixture Helpers

        private EventLogViewModel _eventLogView;

        [SetUp]
        public override void Setup()
        {
            //setup the base
            base.Setup();
            _eventLogView = new EventLogViewModel();
            this.TestModel = _eventLogView;
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailableEntityInfo()
        {
            String propertyName = "AvailableEntityInfo";
            Assert.AreEqual(propertyName, EventLogViewModel.AvailableEntityInfoProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableEntityInfo);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //check this contains all available audit log names
            EntityInfoList entityInfoList = EntityInfoList.FetchAllEntityInfos();
            foreach (EntityInfo entityInfo in entityInfoList)
            {
                EntityInfo existingEntityInfo = this.TestModel.AvailableEntityInfo.FirstOrDefault(a => a.Id == entityInfo.Id);
                Assert.IsNotNull(existingEntityInfo, "The existing entity info should be available in the testmodel");
            }
        }

        [Test]
        public void Property_SelectedEntityInfo()
        {
            String propertyName = "SelectedEntityInfo";
            Assert.AreEqual(propertyName, EventLogViewModel.SelectedEntityInfoProperty.Path);
        }

        [Test]
        public void Property_SelectedRecordTopCount()
        {
            String propertyName = "SelectedRecordTopCount";
            Assert.AreEqual(propertyName, EventLogViewModel.SelectedRecordTopCountProperty.Path);
        }

        [Test]
        public void Property_SelectedEventLogWrap()
        {
            String propertyName = "SelectedEventLogWrap";
            Assert.AreEqual(propertyName, EventLogViewModel.SelectedEventLogWrapProperty.Path);
        }

        [Test]
        public void Property_SelectedEventLogsWrap()
        {
            String propertyName = "SelectedEventLogsWrap";
            Assert.AreEqual(propertyName, EventLogViewModel.SelectedEventLogsWrapProperty.Path);
        }

        [Test]
        public void Property_AvailableEventLogTimeGroupings()
        {
            String propertyName = "AvailableEventLogsWrap";
            Assert.AreEqual(propertyName, EventLogViewModel.AvailableEventLogsWrapProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableEventLogsWrap);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_AvailableEventLogNames()
        {
            String propertyName = "AvailableEventLogNames";
            Assert.AreEqual(propertyName, EventLogViewModel.AvailableEventLogNamesProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableEventLogNames);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //check this contains all available audit log names
            EventLogNameList logNameList = EventLogNameList.FetchAll();
            foreach (EventLogName logName in logNameList)
            {
                EventLogName eventLogName = this.TestModel.AvailableEventLogNames.FirstOrDefault(a => a.Id == logName.Id);
                Assert.IsNotNull(eventLogName, "The existing name should be available in the testmodel");
            }
        }

        [Test]
        public void Property_SelectedEventLogName()
        {
            String propertyName = "SelectedEventLogName";
            Assert.AreEqual(propertyName, EventLogViewModel.SelectedEventLogNameProperty.Path);
        }

        [Test]
        public void Property_CurrentRecordTopCount()
        {
            String propertyName = "CurrentRecordTopCount";
            Assert.AreEqual(propertyName, EventLogViewModel.CurrentRecordTopCountProperty.Path);
        }

        [Test]
        public void Property_CurrentDataTimeGroup()
        {
            String propertyName = "CurrentDataTimeGroup";
            Assert.AreEqual(propertyName, EventLogViewModel.CurrentDataTimeGroupProperty.Path);
        }

        [Test]
        public void Property_CurrentCopyToClipboard()
        {
            String propertyName = "CurrentCopyToClipboard";
            Assert.AreEqual(propertyName, EventLogViewModel.CurrentCopyToClipboardProperty.Path);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_ExportToExcel()
        {
            //Menu button
            RelayCommand cmd = this.TestModel.ExportToExcelCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
        }

        [Test]
        public void Command_ExportToExcelAll()
        {
            RelayCommand cmd = this.TestModel.ExportToExcelAllCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }
        }

        [Test]
        public void Command_ExportToExcelSelection()
        {
            RelayCommand cmd = this.TestModel.ExportToExcelSelectionCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }
        }

        [Test]
        public void Command_CopyToClipboard()
        {
            //Menu button
            RelayCommand cmd = this.TestModel.CopyToClipboardCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
        }

        [Test]
        public void Command_CopyToClipboardAll()
        {
            RelayCommand cmd = this.TestModel.CopyToClipboardAllCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_CopyToClipboardSelection()
        {
            RelayCommand cmd = this.TestModel.CopyToClipboardSelectionCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_Refresh()
        {
            RelayCommand cmd = this.TestModel.RefreshCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_Summarise()
        {
            // Menu button
            RelayCommand cmd = this.TestModel.SummariseCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
        }

        [Test]
        public void Command_Summarise00Sec()
        {
            RelayCommand cmd = this.TestModel.Summarise00SecCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_Summarise03Sec()
        {
            RelayCommand cmd = this.TestModel.Summarise03SecCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_Summarise05Sec()
        {
            RelayCommand cmd = this.TestModel.Summarise05SecCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_Summarise10Sec()
        {
            RelayCommand cmd = this.TestModel.Summarise10SecCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_Summarise60Sec()
        {
            RelayCommand cmd = this.TestModel.Summarise60SecCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_SelectEntityInfo()
        {
            RelayCommand cmd = this.TestModel.SelectEntityInfoCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            if (this.TestModel.AvailableEventLogsWrap.Count() > 0)
            {
                Assert.IsTrue(cmd.CanExecute(), "command should always be enabled");
            }
            else
            {
                Assert.IsFalse(cmd.CanExecute(), "command should disabled, as no entries");
            }

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_FilterErrors()
        {
            RelayCommand cmd = this.TestModel.FilterErrorsCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should be enabled");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_ClearAllLogs()
        {
            RelayCommand cmd = this.TestModel.ClearAllLogsCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should be enabled");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            base.CloseCommandCheckExpectedValues(cmd);
        }

        #endregion
    }
}
