﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.UserMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.SysAdmin.UserMaintenance
{
    [TestFixture]
    public class UserMaintenanceViewModelTest : TestBase<UserMaintenanceViewModel>
    {
        #region Test Fixture Helpers

        private const Int32 _numUsersInserted = 5;

        public override void Setup()
        {
            base.Setup();

            Helpers.TestDataHelper.InsertUserDtos(this.DalFactory, _numUsersInserted);
            TestDataHelper.InsertRoleDtos(base.DalFactory, 10, this.EntityId);

            this.TestModel = new UserMaintenanceViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailableUsers()
        {
            String propertyName = "AvailableUsers";
            Assert.AreEqual(propertyName, UserMaintenanceViewModel.AvailableUsersProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableUsers);

            UserInfoList locInfos = UserInfoList.FetchAll();
            Assert.AreEqual(locInfos.Count, this.TestModel.AvailableUsers.Count);
        }

        [Test]
        public void Property_SelectedUser()
        {
            String propertyName = "SelectedUser";
            Assert.AreEqual(propertyName, UserMaintenanceViewModel.SelectedUserProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_EmailAddress()
        {
            String propertyName = "EmailAddress";
            Assert.AreEqual(propertyName, UserMaintenanceViewModel.EmailAddressProperty.Path);
        }

        [Test]
        public void Property_AvailableRoles()
        {
            String propertyName = "AvailableRoles";
            Assert.AreEqual(propertyName, UserMaintenanceViewModel.AvailableRolesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableRoles);

            RoleInfoList roles = RoleInfoList.FetchAll();

            Assert.AreEqual(roles.Count, this.TestModel.AvailableRoles.Count);
            foreach (RoleInfo roleInfo in roles)
            {
                RoleInfo foundRole = this.TestModel.AvailableRoles.First(r => r.Id == roleInfo.Id);
                Assert.AreEqual(roleInfo.Description, foundRole.Description);
                Assert.AreEqual(roleInfo.IsAdministratorRole, foundRole.IsAdministratorRole);
                Assert.AreEqual(roleInfo.Name, foundRole.Name);
            }
        }

        [Test]
        public void Property_SelectedRole()
        {
            String propertyName = "SelectedRole";
            Assert.AreEqual(propertyName, UserMaintenanceViewModel.SelectedRoleProperty.Path);

            //test setter
            base.PropertyChangedNotifications.Clear();

            RoleInfo testValue = this.TestModel.AvailableRoles.First();
            this.TestModel.SelectedRole = testValue;
            Assert.AreEqual(testValue, this.TestModel.SelectedRole);

            Assert.Contains(propertyName, base.PropertyChangedNotifications);
        }

        [Test]
        public void Property_CurrentMemberRoles()
        {
            String propertyName = "CurrentMemberRoles";
            Assert.AreEqual(propertyName, UserMaintenanceViewModel.CurrentMemberRolesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.CurrentMemberRoles);

            Assert.AreEqual(this.TestModel.SelectedUser.RoleMembers, this.TestModel.CurrentMemberRoles);
        }

        [Test]
        public void Property_SelectedCurrentMemberRole()
        {
            String propertyName = "SelectedCurrentMemberRole";
            Assert.AreEqual(propertyName, UserMaintenanceViewModel.SelectedCurrentMemberRoleProperty.Path);

            //check initial value
            Assert.IsNull(this.TestModel.SelectedCurrentMemberRole);

            //check set
            RoleInfoList testList = RoleInfoList.FetchAll();
            RoleInfo test = testList.First();
            this.TestModel.SelectedCurrentMemberRole = test;
            Assert.AreEqual(test, this.TestModel.SelectedCurrentMemberRole);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {

            this.TestModel = new UserMaintenanceViewModel();

            //we should start with a new store
            Assert.IsTrue(this.TestModel.SelectedUser.IsNew);

            //the info list model should also contain existing stores
            UserInfoList list = UserInfoList.FetchAll();
            Assert.AreEqual(list.Count, this.TestModel.AvailableUsers.Count());
        }

        #endregion

        #region Commands

        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;

            //check command is registered and value are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            User preExecuteItem = this.TestModel.SelectedUser;

            cmd.Execute();

            Assert.Contains(UserMaintenanceViewModel.SelectedUserProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedUser, "The user should have changed.");
            Assert.IsTrue(this.TestModel.SelectedUser.IsNew, "The user should be new");
            Assert.IsTrue(this.TestModel.SelectedUser.IsInitialized, "The user should be marked initialized");
        }

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedUser.UserName = "UserNameA";
            this.TestModel.SelectedUser.FirstName = "FirstNameA";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected store has been updated
            Assert.IsFalse(this.TestModel.SelectedUser.IsNew, "The store should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedUser.IsDirty, "The store should have been saved");
            Assert.AreEqual("UserNameA", this.TestModel.SelectedUser.UserName, "The store should be the same");
            Assert.AreNotEqual(0, this.TestModel.SelectedUser.Id, "The store should have been given a valid id");
            Assert.Contains(UserMaintenanceViewModel.SelectedUserProperty.Path, base.PropertyChangedNotifications);

            //check an info has been added for the new store
            UserInfo addedInfo = this.TestModel.AvailableUsers.FirstOrDefault(l => l.Id == this.TestModel.SelectedUser.Id);
            Assert.IsNotNull(addedInfo, "An info for the new store should have been added");

            //check another store with the same username cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.SelectedUser.UserName = "UserNameA";
            this.TestModel.SelectedUser.FirstName = "FirstNameB";
            cmd.Execute();
            Assert.AreNotEqual("UserNameA", this.TestModel.SelectedUser.UserName);
        }

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAsCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedUser.UserName = "UserNameA";
            this.TestModel.SelectedUser.FirstName = "FirstNameA";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();

            //load an existing item
            UserInfo loadInfo = this.TestModel.AvailableUsers.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            User preExecuteItem = this.TestModel.SelectedUser;

            cmd.Execute();

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedUser, "Item should have changed");
            Assert.AreNotEqual(preExecuteItem.Id, this.TestModel.SelectedUser.Id, "Id should be different");
            Assert.AreNotEqual(preExecuteItem.UserName, this.TestModel.SelectedUser.UserName, "Username should be different");
            Assert.Contains(UserMaintenanceViewModel.SelectedUserProperty.Path, base.PropertyChangedNotifications, "should have change notification");

            //check infos are correct
            UserInfo oldItemInfo = this.TestModel.AvailableUsers.FirstOrDefault(l => l.Id == preExecuteItem.Id);
            Assert.IsNotNull(oldItemInfo, "An info for the old item should still exist");

            UserInfo newItemInfo = this.TestModel.AvailableUsers.FirstOrDefault(l => l.Id == this.TestModel.SelectedUser.Id);
            Assert.IsNotNull(newItemInfo, "An info for the new item should have been added");
        }

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new user");

            //add a store and open
            UserInfo loadInfo = this.TestModel.AvailableUsers.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing user");

            //check execute
            Int32 oldUserId = this.TestModel.SelectedUser.Id;
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedUser.IsNew, "A new user should have been loaded");

            UserInfo oldInfo = this.TestModel.AvailableUsers.FirstOrDefault(l => l.Id == oldUserId);
            Assert.IsNull(oldInfo, "The deleted user should have been removed from the available list");


            User oldUser = User.FetchById(oldUserId);
            Assert.IsNotNull(oldUser.DateDeleted, "Should be marked deleted");
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", UserMaintenanceViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            //check execution
            base.PropertyChangedNotifications.Clear();
            String userName = "UserNameA";

            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedUser.UserName = userName;
            this.TestModel.SelectedUser.FirstName = "FirstNameA";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execution
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedUser.IsNew, "A new user should have been loaded");
            Assert.AreEqual(userName, this.TestModel.AvailableUsers.Last().UserName, "The saved User should be in the available list");
            Assert.Contains(UserMaintenanceViewModel.SelectedUserProperty.Path, base.PropertyChangedNotifications, "SelectedStore should have change notification");
        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedUser.UserName = "UserNameA";
            this.TestModel.SelectedUser.FirstName = "FirstNameA";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execution
            //nb cannot test the the window closed.
            Int32 preLocationCount = this.TestModel.AvailableUsers.Count;

            cmd.Execute();

            Assert.AreEqual(preLocationCount + 1, this.TestModel.AvailableUsers.Count);
        }

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            OpenCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();
            User preExecuteItem = this.TestModel.SelectedUser;

            UserInfo loadInfo = this.TestModel.AvailableUsers.Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedUser, "User should have changed");
            Assert.AreEqual(loadInfo.Id, this.TestModel.SelectedUser.Id, "The User with the info id should ahve been loaded");
            Assert.Contains(UserMaintenanceViewModel.SelectedUserProperty.Path, base.PropertyChangedNotifications, "Selected user should have change notification");
        }

        [Test]
        public void Command_AddUserToRole()
        {
            RelayCommand cmd = this.TestModel.AddUserToRoleCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to add a role without one selected");

            //check current member roles is empty
            Assert.AreEqual(0, this.TestModel.CurrentMemberRoles.Count());

            //select a role
            this.TestModel.SelectedRole = this.TestModel.AvailableRoles.Last();
            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            cmd.Execute();
            Assert.AreEqual(1, this.TestModel.CurrentMemberRoles.Count());
            Assert.AreEqual(this.TestModel.SelectedRole, this.TestModel.CurrentMemberRoles.First());
        }

        [Test]
        public void Command_RemoveUserFromRole()
        {
            RelayCommand cmd = this.TestModel.RemoveUserFromRoleCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to remove a role without one selected");

            //add a role to the user first
            this.TestModel.SelectedRole = this.TestModel.AvailableRoles.Last();
            this.TestModel.AddUserToRoleCommand.Execute();
            //Check added
            Assert.AreEqual(1, this.TestModel.CurrentMemberRoles.Count());

            //select added role
            this.TestModel.SelectedCurrentMemberRole = this.TestModel.CurrentMemberRoles.Last();
            Assert.IsTrue(cmd.CanExecute());

            //execute
            cmd.Execute();
            Assert.AreEqual(0, this.TestModel.CurrentMemberRoles.Count());
        }

        #endregion

        #region Others

        [Test]
        public void TestIsEmailValid()
        {
            String validEmail = "valid@email.org";
            String inValidEmail1 = "invalid.email.com";
            String inValidEmail2 = "invalid.@email.com";
            String inValidEmail3 = "this isnot\avalid@email.com";
            String inValidEmail4 = "ab(c)d,e:f;g<h>i[jk]l@email.com";
            String inValidEmail5 = "Abc..123@email.com";

            Assert.IsTrue(this.TestModel.IsEmailValid(validEmail));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail1));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail2));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail3));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail4));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail5));
        }

        #endregion
    }

}
