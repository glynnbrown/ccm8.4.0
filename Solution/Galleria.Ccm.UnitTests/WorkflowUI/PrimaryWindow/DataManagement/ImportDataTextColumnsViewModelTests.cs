﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Hodson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Framework.Imports;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.DataManagement
{
    [TestFixture]
    public class ImportDataTextColumnsViewModelTests : TestBase<ImportDataTextColumnsViewModel>
    {
        #region Test Fixture Helpers

        private ImportFileData _fileData;

       
        public override void Setup()
        {
            //setup the base
            base.Setup();

            _fileData = ImportFileData.NewImportFileData();

            this.TestModel = new ImportDataTextColumnsViewModel(_fileData);
        }

        #endregion

        #region Properties

        #region Property_IsFixedWidth

        [Test]
        public void Property_IsFixedWidth()
        {
            String propertyName = "IsFixedWidth";
            Assert.AreEqual(propertyName, ImportDataTextColumnsViewModel.IsFixedWidthProperty.Path);

            Boolean startValue = this.TestModel.IsFixedWidth;
            this.TestModel.IsFixedWidth = !startValue;

            Assert.AreNotEqual(startValue, this.TestModel.IsFixedWidth);
        }

        #endregion

        #region Property_LoadFilePath

        [Test]
        public void Property_LoadFilePath()
        {
            String propertyName = "LoadFilePath";
            Assert.AreEqual(propertyName, ImportDataTextColumnsViewModel.LoadFilePathProperty.Path);

            String startValue = this.TestModel.LoadFilePath;

            Assert.Throws(typeof(System.UnauthorizedAccessException),
                () =>
                {
                    this.TestModel.LoadFilePath = @"C:\\";
                });

            Assert.AreNotEqual(startValue, this.TestModel.LoadFilePath);
        }

        #endregion

        #region Property_RawPreviewData

        [Test]
        public void Property_RawPreviewData()
        {
            String propertyName = "RawPreviewData";
            Assert.AreEqual(propertyName, ImportDataTextColumnsViewModel.RawPreviewDataProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_SelectedDelimiter

        [Test]
        public void Property_SelectedDelimiter()
        {
            String propertyName = "SelectedDelimiter";
            Assert.AreEqual(propertyName, ImportDataTextColumnsViewModel.SelectedDelimiterProperty.Path);

            ImportDefinitionTextDelimiterType startValue = this.TestModel.SelectedDelimiter;
            this.TestModel.SelectedDelimiter = ImportDefinitionTextDelimiterType.Semicolon;
            Assert.AreNotEqual(startValue, this.TestModel.SelectedDelimiter);
        }

        #endregion

        #region Property_FileData

        [Test]
        public void Property_FileData()
        {
            String propertyName = "FileData";
            Assert.AreEqual(propertyName, ImportDataTextColumnsViewModel.FileDataProperty.Path);

            ImportFileData startValue = this.TestModel.FileData;
            this.TestModel.FileData = ImportFileData.NewImportFileData();
            Assert.AreNotEqual(startValue, this.TestModel.FileData);
        }

        #endregion

        #region Property_PreviewDataColumnSet

        [Test]
        public void Property_PreviewDataColumnSet()
        {
            String propertyName = "PreviewDataColumnSet";
            Assert.AreEqual(propertyName, ImportDataTextColumnsViewModel.PreviewDataColumnSetProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_FixedWidthLines

        [Test]
        public void Property_FixedWidthLines()
        {
            String propertyName = "FixedWidthLines";
            Assert.AreEqual(propertyName, ImportDataTextColumnsViewModel.FixedWidthLinesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #endregion
    }
}
