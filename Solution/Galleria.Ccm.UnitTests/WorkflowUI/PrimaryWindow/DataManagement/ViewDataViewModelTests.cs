﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Hodson
//  Copied from GFS
#endregion
#endregion

using System;
using Galleria.Ccm.Imports;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.DataManagement
{
    [TestFixture]
    public class ViewDataViewModelTests : TestBase<ViewDataViewModel>
    {
        #region Test Fixture Helpers

        public override void Setup()
        {
            //setup the base
            base.Setup();

            this.TestModel = new ViewDataViewModel(CCMDataItemType.LocationHierarchy);
        }


        #endregion

        #region Properties

        #region Property_DataSource

        [Test]
        public void Property_DataSource()
        {
            String propertyName = "DataSource";
            Assert.AreEqual(propertyName, ViewDataViewModel.DataSourceProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_ColumnSet

        [Test]
        public void Property_ColumnSet()
        {
            String propertyName = "ColumnSet";
            Assert.AreEqual(propertyName, ViewDataViewModel.ColumnSetProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion


        #region Property_SelectedItem

        [Test]
        public void Property_SelectedItem()
        {
            String propertyName = "SelectedItem";
            Assert.AreEqual(propertyName, ViewDataViewModel.SelectedItemProperty.Path);

            //Check the set
            Object startValue = this.TestModel.SelectedItem;
            this.TestModel.SelectedItem = new Object();
            Assert.AreNotEqual(startValue, this.TestModel.SelectedItem);
        }

        #endregion

        #region Property_IsProductData

        [Test]
        public void Property_IsProductData()
        {
            String propertyName = "IsProductData";
            Assert.AreEqual(propertyName, ViewDataViewModel.IsProductDataProperty.Path);

            //check is readonly
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_ShowProductButtons

        [Test]
        public void Property_ShowProductButtons()
        {
            String propertyName = "ShowProductButtons";
            Assert.AreEqual(propertyName, ViewDataViewModel.ShowProductButtonsProperty.Path);

            //check is readonly
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_ProductSetPosition

        public void Property_ProductSetPosition()
        {
            String propertyName = "ProductSetPosition";
            Assert.AreEqual(propertyName, ViewDataViewModel.ProductSetPositionProperty.Path);

            //check is readonly
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion


        [Test]
        public void Property_IsLoadingData()
        {
            String propertyName = "IsLoadingData";
            AssertHelper.AssertPropertyIsReadOnly(typeof(ViewDataViewModel), propertyName);

        }

        #endregion

        #region Commands

        #region Command_Close

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
        }

        #endregion


        #region Command_NextProductSet

        [Test]
        public void Command_NextProductSet()
        {
            RelayCommand cmd = this.TestModel.NextProductSetCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            //will be false
            Assert.IsFalse(cmd.CanExecute());

            //can't really test past this point as it depends on the ViewDataProductProvider which cannot be influenced here
        }

        #endregion

        #region Command_PreviousProductSet

        [Test]
        public void Command_PreviousProductSet()
        {
            RelayCommand cmd = this.TestModel.PreviousProductSetCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            //will be false
            Assert.IsFalse(cmd.CanExecute());

            //can't really test past this point as it depends on the ViewDataProductProvider which cannot be influenced here
        }

        #endregion



        #region Command_Edit

        [Test]
        public void Command_Edit()
        {
            RelayCommand cmd = this.TestModel.EditCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region Command_Delete

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
        }

        #endregion

        #endregion
    }
}
