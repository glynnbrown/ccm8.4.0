﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Hodson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Imports;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.DataManagement
{
    [TestFixture]
    public class ImportDataViewModelTests : TestBase<ImportDataViewModel>
    {
        #region Test Fixture Helpers

        private CCMDataItemType _selectedDataType;

        public override void Setup()
        {
            //setup the base
            base.Setup();

            _selectedDataType = CCMDataItemType.LocationHierarchy;

            this.TestModel = new ImportDataViewModel();
            this.TestModel.SelectDataTypeViewModel.SelectedDataType = _selectedDataType;
        }

        #endregion

        #region Properties

        #region Property_CurrentStep

        [Test]
        public void Property_CurrentStep()
        {
            String propertyName = "CurrentStep";
            Assert.AreEqual(propertyName, ImportDataViewModel.CurrentStepProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Should start on SelectDataType
            Assert.AreEqual(MDMImportDataStep.SelectDataType, this.TestModel.CurrentStep);
        }

        #endregion

        #region Property_SelectDataTypeViewModel

        [Test]
        public void Property_SelectDataTypeViewModel()
        {
            String propertyName = "SelectDataTypeViewModel";
            Assert.AreEqual(propertyName, ImportDataViewModel.SelectDataTypeViewModelProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_TextColumnsViewModel

        [Test]
        public void Property_TextColumnsViewModel()
        {
            String propertyName = "TextColumnsViewModel";
            Assert.AreEqual(propertyName, ImportDataViewModel.TextColumnsViewModelProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_ColumnDefinitionViewModel

        [Test]
        public void Property_ColumnDefinitionViewModel()
        {
            String propertyName = "ColumnDefinitionViewModel";
            Assert.AreEqual(propertyName, ImportDataViewModel.ColumnDefinitionViewModelProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_ValidationViewModel

        [Test]
        public void Property_ValidationViewModel()
        {
            String propertyName = "ValidationViewModel";
            Assert.AreEqual(propertyName, ImportDataViewModel.ValidationViewModelProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Property_IsPreviousCommandVisible

        [Test]
        public void Property_IsPreviousCommandVisible()
        {
            String propertyName = "IsPreviousCommandVisible";
            Assert.AreEqual(propertyName, ImportDataViewModel.IsPreviousCommandVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Should start as false
            Assert.IsFalse(this.TestModel.IsPreviousCommandVisible, "IsPreviousCommandVisible should start as false");
        }

        #endregion

        #region Property_IsNextCommandVisible

        [Test]
        public void Property_IsNextCommandVisible()
        {
            String propertyName = "IsNextCommandVisible";
            Assert.AreEqual(propertyName, ImportDataViewModel.IsNextCommandVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Should start as true
            Assert.IsTrue(this.TestModel.IsNextCommandVisible, "IsNextCommandVisible should start as true");
        }

        #endregion

        #region Property_IsNextCommandVisible

        [Test]
        public void Property_IsExportErrorsCommandVisible()
        {
            String propertyName = "IsExportErrorsCommandVisible";
            Assert.AreEqual(propertyName, ImportDataViewModel.IsExportErrorsCommandVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Should start as false
            Assert.IsFalse(this.TestModel.IsExportErrorsCommandVisible, "IsExportErrorsCommandVisible should start as false");
        }

        #endregion

        //#region Property_ImportDefinitionName

        //[Test]
        //public void Property_ImportDefinitionName()
        //{
        //    String propertyName = "ImportDefinitionName";
        //    Assert.AreEqual(propertyName, ImportDataViewModel.ImportDefinitionNameProperty.Path);

        //    String startValue = this.TestModel.ImportDefinitionName;
        //    this.TestModel.ImportDefinitionName = "Different String";
        //    Assert.AreNotEqual(startValue, this.TestModel.ImportDefinitionName);
        //}

        //#endregion

        //#region Property_ImportDefinition

        //[Test]
        //public void Property_ImportDefinition()
        //{
        //    String propertyName = "ImportDefinition";
        //    Assert.AreEqual(propertyName, ImportDataViewModel.ImportDefinitionProperty.Path);

        //    Foundation.Model.ImportDefinition startValue = this.TestModel.ImportDefinition;
        //    this.TestModel.ImportDefinition = Foundation.Model.ImportDefinition.NewImportDefinition();
        //    Assert.AreNotEqual(startValue, this.TestModel.ImportDefinition);
        //}

        //#endregion

        //#region Property_HeaderParentId

        //[Test]
        //public void Property_HeaderParentId()
        //{
        //    String propertyName = "HeaderParentId";
        //    Assert.AreEqual(propertyName, ImportDataViewModel.HeaderParentIdProperty.Path);

        //    Int32? startValue = this.TestModel.HeaderParentId;
        //    if (startValue.HasValue)
        //    {
        //        this.TestModel.HeaderParentId = null;
        //    }
        //    else
        //    {
        //        this.TestModel.HeaderParentId = 1;
        //    }

        //    Assert.AreNotEqual(startValue, this.TestModel.HeaderParentId);
        //}

        //#endregion

        #endregion

        #region Commands

        #region Command_Cancel

        [Test]
        public void Command_Cancel()
        {
            RelayCommand cmd = this.TestModel.CancelCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //cannot check execute
        }

        #endregion

        #region Command_Next

        [Test]
        public void Command_Next()
        {
            RelayCommand cmd = this.TestModel.NextCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(this.TestModel.CurrentStep == MDMImportDataStep.SelectDataType);
            Assert.IsFalse(this.TestModel.SelectDataTypeViewModel.IsFilePathValid);
            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #region Command_Previous

        [Test]
        public void Command_Previous()
        {
            RelayCommand cmd = this.TestModel.PreviousCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(this.TestModel.CurrentStep == MDMImportDataStep.SelectDataType);
            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        //#region Command_Save

        //[Test]
        //public void Command_Save()
        //{
        //    RelayCommand cmd = this.TestModel.SaveCommand;
        //    Assert.Contains(cmd, this.TestModel.ViewModelCommands);

        //    Assert.IsFalse(cmd.CanExecute());
        //}

        //#endregion

        #region Command_Import

        [Test]
        public void Command_Import()
        {
            RelayCommand cmd = this.TestModel.ImportCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
        }

        #endregion

        #region Command_ExportErrors

        [Test]
        public void Command_ExportErrors()
        {
            RelayCommand cmd = this.TestModel.ExportErrorsCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #endregion
    }
}
