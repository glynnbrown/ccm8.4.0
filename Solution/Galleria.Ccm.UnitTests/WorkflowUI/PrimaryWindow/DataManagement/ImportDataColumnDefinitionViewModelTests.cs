﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-25554 : L.Hodson
////  Copied from GFS
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Text;
//using System.Windows.Input;
//using Csla;
//using Galleria.Ccm.Workflow.Client.Wpf;
//using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;
//using Galleria.Ccm.Security;
//using Galleria.Ccm.UnitTests.Helpers;
//using Galleria.Framework.ViewModel;
//using NUnit.Framework;
//using Galleria.Framework.Imports;

//namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.DataManagement
//{
//    [TestFixture]
//    public class ImportDataColumnDefinitionViewModelTests : TestBase<ImportDataColumnDefinitionViewModel>
//    {
//        #region Test Fixture Helpers

//        private ImportFileData _fileData;

//        public override void  Setup()
//        {
//            //setup the base
//            base.Setup();

//            _fileData = ImportFileData.NewImportFileData();
//            ProductAttributeList list =
//                            ProductAttributeList.FetchByEntityId(App.ViewState.EntityId);

//            _fileData.MappingList = ProductImportMappingList.NewProductImportMappingList(list);
//            _fileData.MappingList.First().ColumnReference = "Ref";

//            this.TestModel = new ImportDataColumnDefinitionViewModel(_fileData);
//        }

//        #endregion

//        #region Properties

//        #region Property_ImportStartRowNumber

//        [Test]
//        public void Property_ImportStartRowNumber()
//        {
//            String propertyName = "ImportStartRowNumber";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.ImportStartRowNumberProperty.Path);

//            //Check set
//            Int32 startValue = this.TestModel.ImportStartRowNumber;
//            this.TestModel.ImportStartRowNumber += 1;
//            Assert.AreNotEqual(startValue, this.TestModel.ImportStartRowNumber);
//        }

//        #endregion

//        #region Property_IsFirstRowColumnHeaders

//        [Test]
//        public void Property_IsFirstRowColumnHeaders()
//        {
//            String propertyName = "IsFirstRowColumnHeaders";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.IsFirstRowColumnHeadersProperty.Path);

//            //Check set
//            Boolean startValue = this.TestModel.IsFirstRowColumnHeaders;
//            this.TestModel.IsFirstRowColumnHeaders = !startValue;
//            Assert.AreNotEqual(startValue, this.TestModel.IsFirstRowColumnHeaders);
//        }

//        #endregion

//        #region Property_AvailableWorksheets

//        [Test]
//        public void Property_AvailableWorksheets()
//        {
//            String propertyName = "AvailableWorksheets";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.AvailableWorksheetsProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_SelectedWorksheet

//        [Test]
//        public void Property_SelectedWorksheet()
//        {
//            String propertyName = "SelectedWorksheet";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.SelectedWorksheetProperty.Path);

//            String startValue = this.TestModel.SelectedWorksheet;
//            this.TestModel.SelectedWorksheet = "New SelectedWorksheet";
//            Assert.AreNotEqual(startValue, this.TestModel.SelectedWorksheet);
//        }

//        #endregion

//        #region Property_FileData

//        [Test]
//        public void Property_FileData()
//        {
//            String propertyName = "FileData";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.FileDataProperty.Path);
//        }

//        #endregion

//        #region Property_AreMappingsAllValid

//        [Test]
//        public void Property_AreMappingsAllValid()
//        {
//            String propertyName = "AreMappingsAllValid";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.AreMappingsAllValidProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_AreMappingsAllUnique

//        [Test]
//        public void Property_AreMappingsAllUnique()
//        {
//            String propertyName = "AreMappingsAllUnique";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.AreMappingsAllUniqueProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_AreDefaultValuesAllValid

//        [Test]
//        public void Property_AreDefaultValuesAllValid()
//        {
//            String propertyName = "AreDefaultValuesAllValid";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.AreDefaultValuesAllValidProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

//            Assert.IsTrue(this.TestModel.AreDefaultValuesAllValid, "All default values should be valid at the start");
//        }

//        #endregion

//        #region Property_InvalidDefaults

//        [Test]
//        public void Property_InvalidDefaults()
//        {
//            String propertyName = "InvalidDefaults";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.InvalidDefaultsProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

//            foreach (Boolean value in this.TestModel.InvalidDefaults.Values)
//            {
//                Assert.IsFalse(value, "All values in the InvalidDefaults dictionary should start as false");
//            }
//        }

//        #endregion

//        #region Property_PreviewDataColumnSet

//        [Test]
//        public void Property_PreviewDataColumnSet()
//        {
//            String propertyName = "PreviewDataColumnSet";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.PreviewDataColumnSetProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_AvailableColumns

//        [Test]
//        public void Property_AvailableColumns()
//        {
//            String propertyName = "AvailableColumns";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.AvailableColumnsProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_PreviewData

//        [Test]
//        public void Property_PreviewData()
//        {
//            String propertyName = "PreviewData";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.PreviewDataProperty.Path);
//        }

//        #endregion

//        #region Property_SelectedMappingFromGrid
//        [Test]
//        public void Property_SelectedMappingFromGrid()
//        {
//            String propertyName = "SelectedMappingFromGrid";
//            Assert.AreEqual(propertyName, ImportDataColumnDefinitionViewModel.SelectedMappingFromGridProperty.Path);
//        }

//        #endregion

//        #endregion

//        #region Commands

//        #region Command_ClearAllMappings

//        [Test]
//        public void Command_ClearAllMappings()
//        {
//            RelayCommand cmd = this.TestModel.ClearAllMappingsCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsFalse(cmd.CanExecute());
//        }

//        #endregion

//        #region Command_AutoMap

//        [Test]
//        public void Command_AutoMap()
//        {
//            RelayCommand cmd = this.TestModel.AutoMapCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsEmpty(_fileData.Columns);
//            Assert.IsFalse(cmd.CanExecute());

//            _fileData.Columns.Add(ImportFileDataColumn.NewImportFileDataColumn(1));

//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #region ClearSelectedMappingsCommand

//        [Test]
//        public void Command_ClearSelectedMappings()
//        {
//            RelayCommand<ImportMapping> cmd = this.TestModel.ClearSelectedMappingsCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
//        }

//        #endregion

//        #region ClearSelectedMappingCommand

//        [Test]
//        public void Command_ClearSelectedMapping()
//        {
//            RelayCommand<ImportMapping> cmd = this.TestModel.ClearSelectedMappingCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
//        }

//        #endregion

//        #endregion
//    }
//}
