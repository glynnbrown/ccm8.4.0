﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-25554 : L.Hodson
////  Copied from GFS
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Text;
//using System.Windows.Input;
//using Csla;
//using Galleria.Ccm.Workflow.Client.Wpf;
//using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;
//using Galleria.Ccm.Security;
//using Galleria.Ccm.UnitTests.Helpers;
//using Galleria.Framework.ViewModel;
//using NUnit.Framework;
//using Galleria.Framework.Imports;

//namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.DataManagement
//{
//    [TestFixture]
//    public class ImportDataValidationViewModelTests : TestBase<ImportDataValidationViewModel>
//    {
//        #region Test Fixture Helpers

//        [SetUp]
//        public void Setup()
//        {
//            //setup the base
//            this.TestBaseSetup();
//            ImportFileData importFileData = ImportFileData.NewImportFileData();
//            this.TestModel = new ImportDataValidationViewModel(importFileData);
//        }

//        [TearDown]
//        public void TearDown()
//        {
//            //tear down the base
//            this.TestBaseTeardown();
//        }

//        #endregion

//        #region Properties

//        #region Property_ValidatedFileData

//        [Test]
//        public void Property_ValidatedFileData()
//        {
//            String propertyName = "ValidatedFileData";
//            Assert.AreEqual(propertyName, ImportDataValidationViewModel.ValidatedFileDataProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_ValidatedFileDataColumnSet

//        [Test]
//        public void Property_ValidatedFileDataColumnSet()
//        {
//            String propertyName = "ValidatedFileDataColumnSet";
//            Assert.AreEqual(propertyName, ImportDataValidationViewModel.ValidatedFileDataColumnSetProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_WorksheetErrors

//        [Test]
//        public void Property_WorksheetErrors()
//        {
//            String propertyName = "WorksheetErrors";
//            Assert.AreEqual(propertyName, ImportDataValidationViewModel.WorksheetErrorsProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_SelectedErrorGroup

//        [Test]
//        public void Property_SelectedErrorGroup()
//        {
//            String propertyName = "SelectedErrorGroup";
//            Assert.AreEqual(propertyName, ImportDataValidationViewModel.SelectedErrorGroupProperty.Path);

//            ValidationErrorGroup startValue = this.TestModel.SelectedErrorGroup;
//            this.TestModel.SelectedErrorGroup = new ValidationErrorGroup();
//            Assert.AreNotEqual(startValue, this.TestModel.SelectedErrorGroup);
//        }

//        #endregion

//        #region Property_SelectedErrorItem

//        [Test]
//        public void Property_SelectedErrorItem()
//        {
//            String propertyName = "SelectedErrorItem";
//            Assert.AreEqual(propertyName, ImportDataValidationViewModel.SelectedErrorItemProperty.Path);

//            ValidationErrorItem startValue = this.TestModel.SelectedErrorItem;
//            this.TestModel.SelectedErrorItem = new ValidationErrorItem();
//            Assert.AreNotEqual(startValue, this.TestModel.SelectedErrorItem);
//        }

//        #endregion

//        #region Property_ErrorPercentageText

//        [Test]
//        public void Property_ErrorPercentageText()
//        {
//            String propertyName = "ErrorPercentageText";
//            Assert.AreEqual(propertyName, ImportDataValidationViewModel.ErrorPercentageTextProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #region Property_ImportDataAvailable

//        [Test]
//        public void Property_ImportDataAvailable()
//        {
//            String propertyName = "ImportDataAvailable";
//            Assert.AreEqual(propertyName, ImportDataValidationViewModel.ImportDataAvailableProperty.Path);

//            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
//        }

//        #endregion

//        #endregion

//        #region Commands

//        #region Command_IgnoreRowsGroup

//        [Test]
//        public void Command_IgnoreRowsGroup()
//        {
//            RelayCommand cmd = this.TestModel.IgnoreRowsGroupCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorGroup == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorGroup");

//            this.TestModel.SelectedErrorGroup = new ValidationErrorGroup();

//            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled with a SelectedErrorGroup");
//        }
//        #endregion

//        #region Command_IgnoreRowCommand

//        [Test]
//        public void Command_IgnoreRowCommand()
//        {
//            RelayCommand cmd = this.TestModel.IgnoreRowCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorItem == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorItem");

//            this.TestModel.SelectedErrorItem = new ValidationErrorItem();
//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #region Command_ApplyDefaultDataGroupCommand

//        [Test]
//        public void Command_ApplyDefaultDataGroupCommand()
//        {
//            RelayCommand cmd = this.TestModel.ApplyDefaultDataGroupCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorGroup == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorGroup");

//            this.TestModel.SelectedErrorGroup = new ValidationErrorGroup();
//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #region Command_ApplyDefaultDataCommand

//        [Test]
//        public void Command_ApplyDefaultDataCommand()
//        {
//            RelayCommand cmd = this.TestModel.ApplyDefaultDataCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorItem == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorItem");

//            this.TestModel.SelectedErrorItem = new ValidationErrorItem();
//            this.TestModel.SelectedErrorItem.IsApplyDefaultDataAvailable = true;
//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #region Command_ResetToBoundGroupCommand

//        [Test]
//        public void Command_ResetToBoundGroupCommand()
//        {
//            RelayCommand cmd = this.TestModel.ResetToBoundGroupCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorGroup == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorGroup");

//            this.TestModel.SelectedErrorGroup = new ValidationErrorGroup();
//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #region Command_ResetToBoundCommand

//        [Test]
//        public void Command_ResetToBoundCommand()
//        {
//            RelayCommand cmd = this.TestModel.ResetToBoundCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorItem == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorItem");

//            this.TestModel.SelectedErrorItem = new ValidationErrorItem();
//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #region Command_TruncateDataGroupCommand

//        [Test]
//        public void Command_TruncateDataGroupCommand()
//        {
//            RelayCommand cmd = this.TestModel.TruncateDataGroupCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorGroup == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorGroup");

//            this.TestModel.SelectedErrorGroup = new ValidationErrorGroup();
//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #region Command_TruncateDataCommand

//        [Test]
//        public void Command_TruncateDataCommand()
//        {
//            RelayCommand cmd = this.TestModel.TruncateDataCommand;
//            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

//            Assert.IsTrue(this.TestModel.SelectedErrorItem == null);
//            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without a SelectedErrorItem");

//            this.TestModel.SelectedErrorItem = new ValidationErrorItem();
//            Assert.IsTrue(cmd.CanExecute());
//        }

//        #endregion

//        #endregion
//    }
//}
