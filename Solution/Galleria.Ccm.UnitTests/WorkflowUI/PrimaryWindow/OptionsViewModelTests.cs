﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27709 : L.Luong
//  Created

#endregion

#endregion

using System;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow
{
    [TestFixture]
    public class OptionsViewModelTests : TestBase<OptionsViewModel>
    {
        #region Test Helpers

        public override void Setup()
        {
            base.Setup();
            this.TestModel = new OptionsViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_IsDatabaseSelectionRemembered()
        {
            String propertyName = "IsDatabaseSelectionRemembered";
            Assert.AreEqual(propertyName, OptionsViewModel.IsDatabaseSelectionRememberedProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.IsDatabaseSelectionRemembered);
        }

        [Test]
        public void Property_IsEntitySelectionRemembered()
        {
            String propertyName = "IsEntitySelectionRemembered";
            Assert.AreEqual(propertyName, OptionsViewModel.IsEntitySelectionRememberedProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.IsEntitySelectionRemembered);
        }

        [Test]
        public void Property_DisplayLanguage()
        {
            String propertyName = "DisplayLanguage";
            Assert.AreEqual(propertyName, OptionsViewModel.DisplayLanguageProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.DisplayLanguage);
        }

        [Test]
        public void Property_RequireRestart()
        {
            String propertyName = "RequiresRestart";
            Assert.AreEqual(propertyName, OptionsViewModel.RequiresRestartProperty.Path);

            //Check initial value 
            Assert.IsNotNull(this.TestModel.RequiresRestart);
            //Check readonly
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CancelCommand;
            Assert.AreEqual("CancelCommand", OptionsViewModel.CancelCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_Apply()
        {
            RelayCommand cmd = this.TestModel.ApplyCommand;
            Assert.AreEqual("ApplyCommand", OptionsViewModel.ApplyCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute is false as nothing has changed
            Assert.IsFalse(cmd.CanExecute());

            this.TestModel.Settings.IsDatabaseSelectionRemembered = true;

            //check can execute is true now settings has changed
            Assert.IsTrue(cmd.CanExecute());

            //can't check if the window has closed
        }

        #endregion
    }
}
