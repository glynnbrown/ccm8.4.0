﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26157 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.GFSSetup;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PrimaryWindow.GFSSetup
{
    [TestFixture]
    public class GFSSetupViewModelTests : TestBase<GFSSetupViewModel>
    {
        #region Test Fixture Helpers

        private EntityViewModel _entityView;

        public override void Setup()
        {
            base.Setup();
            _entityView = new EntityViewModel();
            _entityView.FetchCurrent();

            this.TestModel = new GFSSetupViewModel(_entityView);

        }

        #endregion

        #region Properties

        [Test]
        public void Property_SelectedCompression()
        {
            String propertyName = "SelectedCompression";
            Assert.AreEqual(propertyName, GFSSetupViewModel.SelectedCompressionProperty.Path);

            //can't test as relies on a webservice call to populate the compressions
            throw new InconclusiveException("Can't really test this");
        }

        [Test]
        public void Property_AvailableCompressions()
        {
            String propertyName = "AvailableCompressions";
            Assert.AreEqual(propertyName, GFSSetupViewModel.AvailableCompressionsProperty.Path);

            //can't test as relies on a webservice call to populate the compressions
            throw new InconclusiveException("Can't really test this");
        }

        [Test]
        public void Property_HostName()
        {
            String propertyName = "HostName";
            Assert.AreEqual(propertyName, GFSSetupViewModel.HostNameProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "Test1";
            this.TestModel.HostName = testValue;
            Assert.AreEqual(testValue, this.TestModel.HostName);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_PortNumber()
        {
            String propertyName = "PortNumber";
            Assert.AreEqual(propertyName, GFSSetupViewModel.PortNumberProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "1";
            this.TestModel.PortNumber = testValue;
            Assert.AreEqual(testValue, this.TestModel.PortNumber);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_DirectoryName()
        {
            String propertyName = "DirectoryName";
            Assert.AreEqual(propertyName, GFSSetupViewModel.DirectoryNameProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "Test1";
            this.TestModel.DirectoryName = testValue;
            Assert.AreEqual(testValue, this.TestModel.DirectoryName);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_FullConnectionPath()
        {
            String propertyName = "FullConnectionPath";
            Assert.AreEqual(propertyName, GFSSetupViewModel.FullConnectionPathProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            String hostName = "TestHost";
            String directoryName = "TestDirName";
            String portNumber = "TestPortNo";
            this.TestModel.HostName = hostName;
            this.TestModel.DirectoryName = directoryName;
            this.TestModel.PortNumber = portNumber;
            Assert.AreEqual("http://" + hostName + ":" + portNumber + "/" + directoryName, this.TestModel.FullConnectionPath);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_ConnectionTestResult()
        {
            String propertyName = "ConnectionTestResult";
            Assert.AreEqual(propertyName, GFSSetupViewModel.ConnectionTestResultProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            String hostName = "TestHost";
            String directoryName = "TestDirName";
            String portNumber = "TestPortNo";
            this.TestModel.HostName = hostName;
            this.TestModel.DirectoryName = directoryName;
            this.TestModel.PortNumber = portNumber;
            this.TestModel.TestConnectionCommand.Execute();
            //this.TestModel.ConnectionTestResult = testValue;
            Assert.AreEqual(Message.GFSConnectionSetup_TestResult_InProgress, this.TestModel.ConnectionTestResult);


            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_IsBusyProgress()
        {
            String propertyName = "IsBusyProgress";
            Assert.AreEqual(propertyName, GFSSetupViewModel.IsBusyProgressProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            Boolean testValue = true;
            this.TestModel.IsBusyProgress = testValue;
            Assert.AreEqual(testValue, this.TestModel.IsBusyProgress);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_IsGFSConnected()
        {
            String propertyName = "IsGFSConnected";
            Assert.AreEqual(propertyName, GFSSetupViewModel.IsGFSConnectedProperty.Path);

            //check setter
            this.PropertyChangedNotifications.Clear();
            Boolean testValue = true;
            this.TestModel.IsGFSConnected = testValue;
            Assert.AreEqual(testValue, this.TestModel.IsGFSConnected);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_IsConnectionStringSetup()
        {
            String propertyName = "IsConnectionStringSetup";
            Assert.AreEqual(propertyName, GFSSetupViewModel.IsConnectionStringSetupProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as connection details are empty");

            //can't save without a valid connection

            //can't test as relies on SelectedCompression property which needs a
            //webservice call to populate the compressions
            throw new InconclusiveException("Can't really test this");
        }

        [Test]
        public void Command_TestConnection()
        {
            RelayCommand cmd = this.TestModel.TestConnectionCommand;

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as there is no connection path");

            String hostName = "TestHost";
            String directoryName = "TestDirName";
            String portNumber = "TestPortNo";
            this.TestModel.HostName = hostName;
            this.TestModel.DirectoryName = directoryName;
            this.TestModel.PortNumber = portNumber;

            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as there is a connection path");

            //check execute
            cmd.Execute();

            //can't test as relies on a webservice call to populate the compressions
            throw new InconclusiveException("Can't really test this");
        }

        [Test]
        public void Command_ResetConnection()
        {
            RelayCommand cmd = this.TestModel.ResetConnectionCommand;

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "Should always be enabled");

            String hostName = "TestHost";
            String directoryName = "TestDirName";
            String portNumber = "TestPortNo";
            this.TestModel.HostName = hostName;
            this.TestModel.DirectoryName = directoryName;
            this.TestModel.PortNumber = portNumber;

            Assert.IsTrue(cmd.CanExecute(), "Should always be enabled");

            //check execute
            cmd.Execute();

            //Check defaults have been applied
            Assert.AreEqual(String.Empty, this.TestModel.HostName);
            Assert.AreEqual("80", this.TestModel.PortNumber);
            Assert.AreEqual(String.Empty, this.TestModel.DirectoryName);
            //Check other flags have been rest
            Assert.AreEqual(String.Empty, this.TestModel.ConnectionTestResult);
            Assert.AreEqual(String.Empty, this.TestModel.FullConnectionPath);

        }

        #endregion

    }
}
