﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (SA 1.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.Common.ModelObjectViewModels
{
    [TestFixture]
    public class ConsumerDecisionTreeInfoListViewModelTests : TestBase<ConsumerDecisionTreeInfoListViewModel>
    {
        #region Test Fixture Helpers

        [SetUp]
        public new void Setup()
        {
            //setup the base
            base.Setup();
            this.TestModel = new ConsumerDecisionTreeInfoListViewModel();
        }

        [TearDown]
        public new void TearDown()
        {
            //tear down the base
            base.TearDown();
        }

        #endregion

        #region Methods

        [Test]
        public void FetchForCurrentEntity()
        {
            for (Int32 i = 0; i < 5; i++)
            {
                Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
                List<ConsumerDecisionTreeDto> cdtDtoList = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1);

                App.ViewState.EntityId = entityId;

                this.TestModel.FetchForCurrentEntity();

                foreach (ConsumerDecisionTreeDto dto in cdtDtoList)
                {
                    ConsumerDecisionTreeInfo info = this.TestModel.Model.FirstOrDefault(p => p.Id == dto.Id);
                    Galleria.Ccm.UnitTests.Model.ConsumerDecisionTreeInfoTests.AssertDtoAndModelAreEqual(dto, info);
                }
            }

        }

        #endregion
    }
}
