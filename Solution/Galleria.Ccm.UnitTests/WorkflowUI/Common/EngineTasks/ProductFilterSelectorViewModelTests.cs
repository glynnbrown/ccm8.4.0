﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31559 : A.Kuszyk.
//	Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.WorkflowUI.Common.EngineTasks
{
    [TestFixture]
    public class ProductFilterSelectorViewModelTests : TestBase
    {
        private IList<ObjectFieldInfo> _availableFields = 
            PlanogramProduct.EnumerateDisplayableFieldInfos(includeMetadata: true, includeCustom: true, includePerformanceData: true).ToList();

        [Test]
        public void Constructor_AddsFilter([Values(true,false)] Boolean allowMultiple)
        {
            base.WindowService.ShowDialogSetReponse(AddFieldResponse);

            var viewModel = new ProductFilterSelectorViewModel(_availableFields,allowMultiple);

            Assert.That(viewModel.Filters.Count, Is.EqualTo(1)); 
        }

        [Test]
        public void AddFilter_Once_AddsOneFilter([Values(true,false)] Boolean allowMultiple)
        {
            base.WindowService.ShowDialogSetReponse(AddFieldResponse);
            base.WindowService.ShowDialogSetReponse(AddFieldResponse);
            var viewModel = new ProductFilterSelectorViewModel(_availableFields, allowMultiple);

            viewModel.AddFilterCommand.Execute();

            Assert.That(viewModel.Filters.Count, Is.EqualTo(2)); // A filter is added on construction.
        }

        [Test]
        public void AddFilter_Twice_AddsTwoFilters([Values(true,false)] Boolean allowMultiple)
        {
            base.WindowService.ShowDialogSetReponse(AddFieldResponse);
            base.WindowService.ShowDialogSetReponse(AddFieldResponse);
            base.WindowService.ShowDialogSetReponse(AddFieldResponse);
            var viewModel = new ProductFilterSelectorViewModel(_availableFields, allowMultiple);

            viewModel.AddFilterCommand.Execute();
            viewModel.AddFilterCommand.Execute();

            Assert.That(viewModel.Filters.Count, Is.EqualTo(3)); // A filter is added on construction.
        }

        [Test]
        public void SelectField_ChangesFieldForGivenFilter(
            [Values(true, false)] Boolean allowMultiple,
            [Values(0,1,2)] Int32 indexToChange)
        {
            base.WindowService.ShowDialogSetReponse((NUnitWindowService.NUnitWindowServiceArgs e) => AddFieldResponse(e, 0));
            base.WindowService.ShowDialogSetReponse((NUnitWindowService.NUnitWindowServiceArgs e) => AddFieldResponse(e, 1));
            base.WindowService.ShowDialogSetReponse((NUnitWindowService.NUnitWindowServiceArgs e) => AddFieldResponse(e, 2));
            var viewModel = new ProductFilterSelectorViewModel(_availableFields, allowMultiple);
            viewModel.AddFilterCommand.Execute();
            viewModel.AddFilterCommand.Execute();

            base.WindowService.ShowDialogSetReponse((NUnitWindowService.NUnitWindowServiceArgs e) => AddFieldResponse(e, 3));
            viewModel.SelectFieldCommand.Execute(viewModel.Filters[indexToChange]);

            Assert.That(viewModel.Filters[indexToChange].Field, Is.EqualTo(_availableFields.ElementAt(3)));
        }

        public void  AddFieldResponse(NUnitWindowService.NUnitWindowServiceArgs e)
        {
            AddFieldResponse(e, 0);
        }

        private void AddFieldResponse(NUnitWindowService.NUnitWindowServiceArgs e, Int32 indexOfField)
        {
            var selectorViewModel = e.WindowParameters.First() as GenericSingleItemSelectorViewModel;
            selectorViewModel.SelectedItems = new List<ObjectFieldInfo>() { _availableFields.ElementAt(indexOfField) };
            selectorViewModel.DialogResult = true;
        }

    }
}
