﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion

#region Version History: (CCM 8.0.0)
// V8-28444 : M.Shelley
//  Modified the SaveCommand tests as ProductGroupId is now a required field in the
//  ConsumerDecisionTree model object
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf;
using System.Windows.Input;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mock.Implementation;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.UnitTests.WorkflowUI.CdtMaintenance
{
    [TestFixture]
    public class CdtMaintenanceViewModelTests : TestBase<CdtMaintenanceViewModel>
    {

        #region Test Fixture Helpers

        private List<ProductDto> _productDtoList;

        [SetUp]
        public new void Setup()
        {
            //setup the base
            base.Setup();

            _productDtoList = Helpers.TestDataHelper.InsertProductDtos(this.DalFactory, 25, this.EntityId);

            ConsumerDecisionTreeDto consumerDecisionTreeDto = TestDataHelper.InsertConsumerDecisionTreeDtos(this.DalFactory, this.EntityId, 1).First();
            //Add Levels
            List<ConsumerDecisionTreeLevelDto> cdtLevelDtoList =
                TestDataHelper.InsertConsumerDecisionTreeLevelDtos(this.DalFactory, consumerDecisionTreeDto, 3, true);
            //Add Nodes
            List<ConsumerDecisionTreeNodeDto> cdtNodeDtoList =
                TestDataHelper.InsertConsumerDecisionTreeNodeDtos(this.DalFactory, consumerDecisionTreeDto, cdtLevelDtoList, 5, true);
            //Add products
            TestDataHelper.InsertConsumerDecisionTreeNodeProductDtos(this.DalFactory, cdtNodeDtoList, _productDtoList, 5);

            ProductHierarchyDto productHierarchyDto;
            using (var dalContext = base.DalFactory.CreateContext())
            using(var dal = dalContext.GetDal<IProductHierarchyDal>())
            {
                productHierarchyDto = dal.FetchByEntityId(base.EntityId);
            }
            List<ProductLevelDto> productLevelDtos = TestDataHelper.InsertProductLevelDtos(this.DalFactory, productHierarchyDto.Id, 2);
            List<ProductGroupDto> productGroupDtos = TestDataHelper.InsertProductGroupDtos(this.DalFactory, productLevelDtos, 3);
            EntityDto entityDto =
                new EntityDto()
                {
                    Id = 1,
                    Name = "Default"
                };            
            TestDataHelper.InsertProductUniverseDtos(this.DalFactory, new List<EntityDto> { entityDto }, productGroupDtos);

            this.TestModel = new CdtMaintenanceViewModel();
        }


        private Boolean CheckCollectionContainsProductInfo(ProductInfo expected, IEnumerable<ProductInfo> collection)
        {
            foreach (ProductInfo productInfo in collection)
            {
                if (productInfo.Id != expected.Id) { continue; }
                //if (productInfo.RowVersion != expected.RowVersion) { continue; }
                if (productInfo.EntityId != expected.EntityId) { continue; }
                if (productInfo.Gtin != expected.Gtin) { continue; }
                if (productInfo.Name != expected.Name) { continue; }

                return true;
            }
            return false;
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailableCdts()
        {
            String propertyName = "AvailableCdts";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.AvailableCdtsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableCdts);

            Assert.AreEqual(ConsumerDecisionTreeInfoList.FetchByEntityId(this.EntityId).Count, this.TestModel.AvailableCdts.Count);
        }

        [Test]
        public void Property_CurrentCdt()
        {
            String propertyName = "CurrentCdt";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.CurrentCdtProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsTrue(this.TestModel.CurrentCdt.IsNew, "Should be a new CDT loaded");
        }

        [Test]
        public void Property_RootNode()
        {
            String propertyName = "RootNode";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.RootNodeProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            Assert.IsNotNull(this.TestModel.RootNode, "A new root node view should be set when the page loads");
            Assert.IsTrue(this.TestModel.RootNode.IsNew, "A new root node view should be set when the page loads");
        }

        [Test]
        public void Property_SelectedProductGroup()
        {
            String propertyName = "SelectedProductGroup";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedProductGroupProperty.Path);

            ProductHierarchy merchHierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);

            //check setter
            base.PropertyChangedNotifications.Clear();
            this.TestModel.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(merchHierarchy.RootGroup);
            Assert.AreEqual(merchHierarchy.RootGroup.Id, TestModel.SelectedProductGroup.Id);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
        }

        [Test]
        public void Property_SelectedLevelId()
        {
            String propertyName = "SelectedLevelId";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedLevelIdProperty.Path);

            Assert.IsNotNull(this.TestModel.SelectedLevelId, "Selected level id should be set the root level's id");
        }

        [Test]
        public void Property_SelectedNodes()
        {
            String propertyName = "SelectedNodes";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedNodesProperty.Path);

            Assert.AreEqual(1, this.TestModel.SelectedNodes.Count, "Should start with CDT's root node selected");
        }

        [Test]
        public void Property_SelectedUnassignedProducts()
        {
            String propertyName = "SelectedUnassignedProducts";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedUnassignedProductsProperty.Path);

            AssertHelper.AssertIsEditableObservable(this.TestModel.SelectedUnassignedProducts);
            Assert.IsEmpty(this.TestModel.SelectedUnassignedProducts);
        }

        [Test]
        public void Property_UnassignedProducts()
        {
            String propertyName = "UnassignedProducts";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.UnassignedProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyCollection(this.TestModel.UnassignedProducts);

            Assert.IsEmpty(this.TestModel.UnassignedProducts);
        }

        [Test]
        public void Property_SelectedAssignedProducts()
        {
            String propertyName = "SelectedAssignedProducts";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedAssignedProductsProperty.Path);

            AssertHelper.AssertIsEditableObservable(this.TestModel.SelectedAssignedProducts);
            Assert.IsEmpty(this.TestModel.SelectedAssignedProducts);
        }

        [Test]
        public void Property_SelectedNodeProducts()
        {
            String propertyName = "SelectedNodeProducts";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedNodeProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyCollection(this.TestModel.SelectedNodeProducts);

            Assert.IsEmpty(this.TestModel.SelectedNodeProducts);
        }

        [Test]
        public void Property_SearchType()
        {
            String propertyName = "SearchType";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SearchTypeProperty.Path);

            Assert.AreEqual(ProductSearchType.Criteria, this.TestModel.SearchType);
        }

        [Test]
        public void Property_SelectedFinancialGroup()
        {
            String propertyName = "SelectedFinancialGroup";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedFinancialGroupProperty.Path);

            //starts off as null
            Assert.IsNull(this.TestModel.SelectedFinancialGroup);
        }

        [Test]
        public void Property_SelectedProductUniverse()
        {
            String propertyName = "SelectedProductUniverse";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.SelectedProductUniverseProperty.Path);

            Assert.IsNull(this.TestModel.SelectedProductUniverse);
        }

        [Test]
        public void Property_ProductSearchCriteria()
        {
            String propertyName = "ProductSearchCriteria";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.ProductSearchCriteriaProperty.Path);

            Assert.IsNull(this.TestModel.ProductSearchCriteria);
        }

        [Test]
        public void Property_IsProcessingProductSearch()
        {
            String propertyName = "IsProcessingProductSearch";
            Assert.AreEqual(propertyName, CdtMaintenanceViewModel.IsProcessingProductSearchProperty.Path);

            Assert.AreEqual(false, this.TestModel.IsProcessingProductSearch);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //should start with a new item.
            Assert.IsTrue(this.TestModel.CurrentCdt.IsNew);
        }

        #endregion

        #region Commands

        #region NewCommand

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;
            Assert.AreEqual("NewCommand", CdtMaintenanceViewModel.NewCommandProperty.Path);

            //check command is registered and value are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            ConsumerDecisionTree preExecuteItem = this.TestModel.CurrentCdt;

            cmd.Execute();

            Assert.Contains(CdtMaintenanceViewModel.CurrentCdtProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, this.TestModel.CurrentCdt, "The item should have changed.");
            Assert.IsTrue(this.TestModel.CurrentCdt.IsNew, "The item should be new");
            Assert.IsTrue(this.TestModel.CurrentCdt.IsInitialized, "The item should be marked initialized");
        }

        #endregion

        #region OpenCommand

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;
            Assert.AreEqual("OpenCommand", CdtMaintenanceViewModel.OpenCommandProperty.Path);

            //check command is registered and value are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            OpenCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();
            ConsumerDecisionTree preExecuteItem = this.TestModel.CurrentCdt;

            ConsumerDecisionTreeInfo loadInfo = ConsumerDecisionTreeInfoList.FetchByEntityId(1).Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreNotEqual(preExecuteItem, this.TestModel.CurrentCdt, "Item should have changed");
            Assert.AreEqual(loadInfo.Id, this.TestModel.CurrentCdt.Id, "The location with the info id should ahve been loaded");
            Assert.Contains(CdtMaintenanceViewModel.CurrentCdtProperty.Path, base.PropertyChangedNotifications, "Selected item should have change notification");
        }

        #endregion

        #region SaveCommand

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;
            Assert.AreEqual("SaveCommand", CdtMaintenanceViewModel.SaveCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.CurrentCdt.Name = "CDT1";
            this.TestModel.CurrentCdt.ProductGroupId = 1;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected store has been updated
            Assert.IsFalse(this.TestModel.CurrentCdt.IsNew, "The CDT should no longer be new");
            Assert.IsFalse(this.TestModel.CurrentCdt.IsDirty, "The CDT should have been saved");
            Assert.AreEqual("CDT1", this.TestModel.CurrentCdt.Name, "The CDT should be the same");
            Assert.AreNotEqual(0, this.TestModel.CurrentCdt.Id, "The CDT should have been given a valid id");
            Assert.Contains(CdtMaintenanceViewModel.CurrentCdtProperty.Path, base.PropertyChangedNotifications);

            //check another cdt with the same name cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.CurrentCdt.Name = "CDT1";
            this.TestModel.CurrentCdt.ProductGroupId = 1;
            cmd.Execute();
            Assert.AreNotEqual("CDT1", this.TestModel.CurrentCdt.Name);
        }

        #endregion

        #region SaveAndNewCommand

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;
            Assert.AreEqual("SaveAndNewCommand", CdtMaintenanceViewModel.SaveAndNewCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.CurrentCdt.Name = "CDT1";
            this.TestModel.CurrentCdt.ProductGroupId = 1;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as CDT is valid");

            //check execution
            //nb cannot test the window closed.
            IEnumerable<ConsumerDecisionTreeInfo> availableInfoList = ConsumerDecisionTreeInfoList.FetchByEntityId(this.EntityId);
            Int32 preItemCount = availableInfoList.Count();

            cmd.Execute();

            availableInfoList = ConsumerDecisionTreeInfoList.FetchByEntityId(this.EntityId);
            Assert.AreEqual(preItemCount + 1, availableInfoList.Count());

            Assert.IsTrue(TestModel.CurrentCdt.IsNew);
        }

        #endregion

        #region SaveAndCloseCommand

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;
            Assert.AreEqual("SaveAndCloseCommand", CdtMaintenanceViewModel.SaveAndCloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.CurrentCdt.Name = "CDT1";
            this.TestModel.CurrentCdt.ProductGroupId = 1;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as CDT is valid");

            //check execution
            //nb cannot test the window closed.
            IEnumerable<ConsumerDecisionTreeInfo> availableInfoList = ConsumerDecisionTreeInfoList.FetchByEntityId(this.EntityId);
            Int32 preItemCount = availableInfoList.Count();

            cmd.Execute();

            availableInfoList = ConsumerDecisionTreeInfoList.FetchByEntityId(this.EntityId);
            Assert.AreEqual(preItemCount + 1, availableInfoList.Count());
        }

        #endregion

        #region SaveAsCommand

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;
            Assert.AreEqual("SaveAsCommand", CdtMaintenanceViewModel.SaveAsCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAsCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.CurrentCdt.Name = "CDT1";
            this.TestModel.CurrentCdt.ProductGroupId = 1;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as CDT is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();

            //load an existing item
            ConsumerDecisionTreeInfoList availableInfoList = ConsumerDecisionTreeInfoList.FetchByEntityId(1);

            ConsumerDecisionTreeInfo loadInfo = availableInfoList.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            ConsumerDecisionTree preExecuteItem = this.TestModel.CurrentCdt;

            cmd.Execute();

            Assert.AreNotEqual(preExecuteItem, this.TestModel.CurrentCdt, "Item should have changed");
            Assert.AreNotEqual(preExecuteItem.Id, this.TestModel.CurrentCdt.Id, "Id should be different");
            Assert.AreNotEqual(preExecuteItem.Name, this.TestModel.CurrentCdt.Name, "Code should be different");
            Assert.Contains(CdtMaintenanceViewModel.CurrentCdtProperty.Path, base.PropertyChangedNotifications, "should have change notification");

            //check infos are correct
            availableInfoList = ConsumerDecisionTreeInfoList.FetchByEntityId(1);

            ConsumerDecisionTreeInfo oldItemInfo = availableInfoList.FirstOrDefault(l => l.Id == preExecuteItem.Id);
            Assert.IsNotNull(oldItemInfo, "An info for the old item should still exist");

            ConsumerDecisionTreeInfo newItemInfo = availableInfoList.FirstOrDefault(l => l.Id == this.TestModel.CurrentCdt.Id);
            Assert.IsNotNull(newItemInfo, "An info for the new item should have been added");
        }

        #endregion

        #region DeleteCommand

        [Test]
        public void Command_Delete()
        {
            App.ViewState.EntityId = 1;
            RelayCommand cmd = this.TestModel.DeleteCommand;
            Assert.AreEqual("DeleteCommand", CdtMaintenanceViewModel.DeleteCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new item");

            //add a cdt and open
            ConsumerDecisionTreeInfo loadInfo = this.TestModel.AvailableCdts.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing item");

            //check execute
            Int32 oldItemId = this.TestModel.CurrentCdt.Id;
            cmd.Execute();

            Assert.IsTrue(this.TestModel.CurrentCdt.IsNew, "A new item should have been loaded");

            ConsumerDecisionTreeInfo oldInfo = this.TestModel.AvailableCdts.FirstOrDefault(l => l.Id == oldItemId);
            Assert.IsNull(oldInfo, "The deleted item should have been removed from the available list");

            Assert.Throws(typeof(DataPortalException),
                () =>
                {
                    ConsumerDecisionTree oldCdt = ConsumerDecisionTree.FetchById(oldItemId);

                }, "It should not be possible to fetch the old item");
        }

        #endregion

        #region AddNodeCommand

        [Test]
        public void Command_AddNode()
        {
            RelayCommand cmd = this.TestModel.AddNodeCommand;
            Assert.AreEqual("AddNodeCommand", CdtMaintenanceViewModel.AddNodeCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be able to execute with as new CDT selects root node");
            this.TestModel.SelectedNodes.Clear();
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected nodes");
            this.TestModel.SelectedNodes.Add(this.TestModel.RootNode);
            Assert.IsTrue(cmd.CanExecute());

            //execute
            ConsumerDecisionTreeNode previousNode = this.TestModel.SelectedNodes.First();//this.TestModel.SelectedNode;
            cmd.Execute();

            Assert.IsNotNull(previousNode.ChildList.FirstOrDefault());
            Assert.IsTrue(previousNode.ChildList.First().IsNew);
        }

        #endregion

        #region RemoveNodeCommand

        [Test]
        public void Command_RemoveNode()
        {
            RelayCommand cmd = this.TestModel.RemoveNodeCommand;
            Assert.AreEqual("RemoveNodeCommand", CdtMaintenanceViewModel.RemoveNodeCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute as new CDT selects root node");
            this.TestModel.SelectedNodes.Clear();
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected nodes");

            //Add a new node to the cdt to be removed
            this.TestModel.SelectedNodes.Add(this.TestModel.RootNode);
            this.TestModel.AddNodeCommand.Execute();

            this.TestModel.SelectedNodes.Clear();
            this.TestModel.SelectedNodes.AddRange(this.TestModel.RootNode.ChildList);

            Assert.IsTrue(cmd.CanExecute(), "Should now be able to execute the command with the new node selected");

            //execute
            ConsumerDecisionTreeNode previousNode = this.TestModel.RootNode.ChildList.FirstOrDefault();
            cmd.Execute();

            Assert.IsTrue(previousNode.IsDeleted, "The node should have been removed");
            Assert.AreEqual(0, this.TestModel.RootNode.ChildList.Count);
        }

        #endregion

        #region EditNodeCommand

        [Test]
        public void Command_EditNode()
        {
            RelayCommand cmd = this.TestModel.EditNodeCommand;
            Assert.AreEqual("EditNodeCommand", CdtMaintenanceViewModel.EditNodeCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            //Execute opens new window so cannot be tested
        }

        #endregion

        #region SplitNodeCommand

        [Test]
        public void Command_SplitNode()
        {
            RelayCommand<ObjectFieldInfo> cmd = this.TestModel.SplitNodeCommand;
            Assert.AreEqual("SplitNodeCommand", CdtMaintenanceViewModel.SplitNodeCommandProperty.Path);

            //Check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(null));

            //Create a property description to test
            ObjectFieldInfo testProperty = ObjectFieldInfo.NewObjectFieldInfo(typeof(Product), Product.FriendlyName, Product.BrandProperty);
            Assert.IsFalse(cmd.CanExecute(testProperty), "Requires a selected node with assigned products");

            this.TestModel.OpenCommand.Execute(this.TestModel.AvailableCdts.First().Id);
            this.TestModel.SelectedNodes.Clear();

            //Get the first available leaf node to select
            ConsumerDecisionTreeNode leafNode =
                this.TestModel.RootNode.FetchAllChildNodes().First(p => p.ChildList.Count == 0 && p.Products.Count > 0);
            this.TestModel.SelectedNodes.Add(leafNode);

            Assert.IsTrue(cmd.CanExecute(testProperty), "Requires a selected node with assigned products");
            IEnumerable<Product> products = ProductList.FetchByProductIds(this.TestModel.SelectedNodeProducts.Select(p => p.Id));
            IEnumerable<IGrouping<String, Product>> groupedProducts = products.GroupBy(p => p.Size);

            //Check the execute
            cmd.Execute(testProperty);

            //Created nodes should now be selected
            Assert.AreEqual(groupedProducts.Count(), this.TestModel.SelectedNodes.Count, "Node should have been split by size and have the same number as the groups");
            foreach (IGrouping<String, Product> expectedGrouping in groupedProducts)
            {
                ConsumerDecisionTreeNode foundNode = this.TestModel.SelectedNodes.FirstOrDefault(p => p.Name.Equals(expectedGrouping.Key));
                Assert.IsNotNull(foundNode, "A node should have been created for this key");

                Assert.AreEqual(expectedGrouping.Count(), foundNode.Products.Count);

                List<Int32> nodeProductIds = foundNode.Products.Select(p => p.ProductId).ToList();
                foreach (Product expectedProduct in expectedGrouping)
                {
                    Assert.Contains(expectedProduct.Id, nodeProductIds);
                }
            }
        }

        #endregion

        #region SelectProductGroupCommand

        [Test]
        public void Command_SelectProductGroup()
        {
            RelayCommand cmd = this.TestModel.SelectProductGroupCommand;
            Assert.AreEqual("SelectProductGroupCommand", CdtMaintenanceViewModel.SelectProductGroupCommandProperty.Path);

            //Check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            throw new InconclusiveException("Execute launches window");
        }

        #endregion

        [Test]
        public void Command_SelectFinancialGroup()
        {
            RelayCommand cmd = this.TestModel.SelectFinancialGroupCommand;
            Assert.AreEqual("SelectFinancialGroupCommand", CdtMaintenanceViewModel.SelectFinancialGroupCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //cannot check execute as a new window is opened
        }

        [Test]
        public void Command_NewFromProductUniverse()
        {
            RelayCommand cmd = this.TestModel.NewFromProductUniverseCommand;
            Assert.AreEqual("NewFromProductUniverseCommand", CdtMaintenanceViewModel.NewFromProductUniverseCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //Always enabled


            Assert.IsTrue(cmd.CanExecute());

            cmd.Execute();
        }

        [Test]
        public void Command_NewFromConsumerDecisionTree()
        {
            RelayCommand cmd = this.TestModel.NewFromConsumerDecisionTreeCommand;
            Assert.AreEqual("NewFromConsumerDecisionTreeCommand", CdtMaintenanceViewModel.NewFromConsumerDecisionTreeCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //Always enabled

            Assert.IsTrue(cmd.CanExecute());

            cmd.Execute();
        }

        #region AddLevelCommand

        [Test]
        public void Command_AddLevel()
        {
            RelayCommand cmd = this.TestModel.AddLevelCommand;
            Assert.AreEqual("AddLevelCommand", CdtMaintenanceViewModel.AddLevelCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            ConsumerDecisionTreeLevel previousLevel = this.TestModel.CurrentCdt.RootLevel.ChildLevel;
            cmd.Execute();

            ConsumerDecisionTreeLevel newLevel = this.TestModel.CurrentCdt.RootLevel.ChildLevel;
            Assert.AreNotEqual(previousLevel, newLevel);

            Assert.IsTrue(newLevel.IsNew, "Should be a new level");
            Assert.IsTrue(newLevel.IsInitialized, "Should be initialised");
        }

        #endregion

        #region RemoveLevelCommand

        [Test]
        public void Command_RemoveLevel()
        {
            RelayCommand cmd = this.TestModel.RemoveLevelCommand;
            Assert.AreEqual("RemoveLevelCommand", CdtMaintenanceViewModel.RemoveLevelCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //Check can execute
            Assert.IsFalse(cmd.CanExecute(), "Selected level will be the root level whick cannot be removed");

            //Add a level to remove
            this.TestModel.AddLevelCommand.Execute();
            this.TestModel.SelectedLevelId = this.TestModel.CurrentCdt.RootLevel.ChildLevel.Id;

            Assert.IsTrue(cmd.CanExecute(), "The new level should be selected");

            //Check execute
            ConsumerDecisionTreeLevel previousLevel = this.TestModel.CurrentCdt.RootLevel.ChildLevel;
            cmd.Execute();

            ConsumerDecisionTreeLevel newLevel = this.TestModel.CurrentCdt.RootLevel.ChildLevel;
            Assert.AreNotEqual(previousLevel, newLevel);
            Assert.IsNull(newLevel);
        }

        #endregion

        #region AddSelectedProductsCommand

        [Test]
        public void Command_AddSelectedProducts()
        {
            RelayCommand cmd = this.TestModel.AddSelectedProductsCommand;
            Assert.AreEqual("AddSelectedProductsCommand", CdtMaintenanceViewModel.AddSelectedProductsCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Cannot execute without selected unassigned products");

            //Select products
            this.TestModel.SearchType = ProductSearchType.Criteria;
            this.TestModel.ProductSearchCriteria = _productDtoList[0].Name;
            ProductInfo testProduct = this.TestModel.UnassignedProducts.First();

            this.TestModel.SelectedUnassignedProducts.Add(testProduct);

            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            cmd.Execute();

            //Check unassigned products
            Assert.IsEmpty(this.TestModel.SelectedUnassignedProducts, "Selected unassigned products should have been cleared");
            Assert.IsFalse(this.TestModel.UnassignedProducts.Contains(testProduct), "The product should have been removed from the unassigned collection");

            //Check the assigned products
            Assert.AreEqual(1, this.TestModel.SelectedNodeProducts.Count);
            Assert.AreEqual(testProduct.Id, this.TestModel.SelectedNodeProducts.First().Id, "The test product should have been moved into the assigned collection");

            Assert.AreEqual(testProduct.Id, this.TestModel.SelectedNodes.First().Products.First().ProductId);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        [Test]
        public void Command_RemoveSelectedProducts()
        {
            RelayCommand cmd = this.TestModel.RemoveSelectedProductsCommand;
            Assert.AreEqual("RemoveSelectedProductsCommand", CdtMaintenanceViewModel.RemoveSelectedProductsCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected assigned products");

            //Add products
            this.TestModel.SearchType = ProductSearchType.Criteria;
            this.TestModel.ProductSearchCriteria = _productDtoList[0].Name;
            ProductInfo testProduct = this.TestModel.UnassignedProducts.First();
            this.TestModel.SelectedUnassignedProducts.Add(testProduct);
            this.TestModel.AddSelectedProductsCommand.Execute();

            this.TestModel.SelectedAssignedProducts.Add(testProduct);
            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            cmd.Execute();
            Assert.IsEmpty(this.TestModel.SelectedAssignedProducts);
            Assert.IsEmpty(this.TestModel.SelectedNodeProducts);
            Assert.IsTrue(CheckCollectionContainsProductInfo(testProduct, this.TestModel.UnassignedProducts));
        }

        #endregion

        #region AddAllProductsCommand

        [Test]
        public void Command_AddAllProducts()
        {
            RelayCommand cmd = this.TestModel.AddAllProductsCommand;
            Assert.AreEqual("AddAllProductsCommand", CdtMaintenanceViewModel.AddAllProductsCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "There are no unassigned products to add");
            this.TestModel.SearchType = ProductSearchType.Criteria;
            this.TestModel.ProductSearchCriteria = _productDtoList[0].Name;
            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            Int32 expectedCount = this.TestModel.UnassignedProducts.Count;

            cmd.Execute();
            Assert.IsEmpty(this.TestModel.UnassignedProducts, "Unassigned products should have been cleared");
            Assert.AreEqual(expectedCount, this.TestModel.SelectedNodeProducts.Count);
        }

        #endregion

        #region RemoveAllProductsCommand

        [Test]
        public void Command_RemoveAllProducts()
        {
            RelayCommand cmd = this.TestModel.RemoveAllProductsCommand;
            Assert.AreEqual("RemoveAllProductsCommand", CdtMaintenanceViewModel.RemoveAllProductsCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "There are no assigned products to remove");

            this.TestModel.SearchType = ProductSearchType.Criteria;
            this.TestModel.ProductSearchCriteria = _productDtoList[0].Name;
            this.TestModel.AddAllProductsCommand.Execute();

            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            Int32 expectedCount = this.TestModel.SelectedNodeProducts.Count;

            cmd.Execute();
            Assert.IsEmpty(this.TestModel.SelectedNodeProducts, "Unassigned products should have been cleared");
            Assert.AreEqual(expectedCount, this.TestModel.UnassignedProducts.Count);
        }

        #endregion

        #region MoreSplitPropertiesCommand

        [Test]
        public void Command_MoreSplitProperties()
        {
            RelayCommand cmd = this.TestModel.MoreSplitPropertiesCommand;
            Assert.AreEqual("MoreSplitPropertiesCommand", CdtMaintenanceViewModel.MoreSplitPropertiesCommandProperty.Path);

            //Check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute as the selected node has no products");

            //Add products
            foreach (ProductDto product in _productDtoList)
            {
                this.TestModel.CurrentCdt.RootNode.Products.Add(ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(product.Id));
            }
            Assert.IsTrue(cmd.CanExecute());

            this.TestModel.SelectedNodes.Clear();
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected nodes");

            //Add a new node to the cdt
            this.TestModel.SelectedNodes.Add(this.TestModel.RootNode);
            this.TestModel.AddNodeCommand.Execute();

            this.TestModel.SelectedNodes.Clear();
            this.TestModel.SelectedNodes.Add(this.TestModel.RootNode);
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute as the selected node is no longer a leaf");

            this.TestModel.SelectedNodes.Clear();
            this.TestModel.SelectedNodes.AddRange(this.TestModel.RootNode.ChildList);
            Assert.IsTrue(cmd.CanExecute(), "Should now be able to execute the command with the new node selected");

            //Execute opens new window
        }

        #endregion

        [Test]
        public void Command_SelectProductUniverse()
        {
            RelayCommand cmd = this.TestModel.SelectProductUniverseCommand;
            Assert.AreEqual("SelectProductUniverseCommand", CdtMaintenanceViewModel.SelectProductUniverseCommandProperty.Path);
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //New window is opened
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", CdtMaintenanceViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            throw new InconclusiveException("Cannot test window close");
        }

        protected void CloseCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Backstage_Close, cmd.FriendlyName);
            Assert.AreEqual(ImageResources.Backstage_Close, cmd.SmallIcon);
            Assert.AreEqual(ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(Key.F4, cmd.InputGestureKey);
        }

        #endregion
    }
}
