﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453: A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.CdtMaintenance
{
    [TestFixture]
    public class CdtMaintenanceCreateViewModelTests : TestBase<CdtMaintenanceCreateViewModel>
    {
        #region Test Fixture Helpers

        private ConsumerDecisionTreeInfoListViewModel _masterConsumerDecisionTreeInfoListView;
        private List<ProductDto> _productDtoList;
        private ConsumerDecisionTree _masterCDT;
        private List<ProductUniverseDto> _masterProductUniverseDtos;

        [SetUp]
        public new void Setup()
        {
            //setup the base
            base.Setup();

            _productDtoList = Helpers.TestDataHelper.InsertProductDtos(this.DalFactory, 25, this.EntityId);

            ConsumerDecisionTreeDto consumerDecisionTreeDto = TestDataHelper.InsertConsumerDecisionTreeDtos(this.DalFactory, this.EntityId, 3).First();
            //Add Levels
            List<ConsumerDecisionTreeLevelDto> cdtLevelDtoList =
                TestDataHelper.InsertConsumerDecisionTreeLevelDtos(this.DalFactory, consumerDecisionTreeDto, 3, true);
            //Add Nodes
            List<ConsumerDecisionTreeNodeDto> cdtNodeDtoList =
                TestDataHelper.InsertConsumerDecisionTreeNodeDtos(this.DalFactory, consumerDecisionTreeDto, cdtLevelDtoList, 5, true);
            //Add products
            TestDataHelper.InsertConsumerDecisionTreeNodeProductDtos(this.DalFactory, cdtNodeDtoList, _productDtoList, 5);

            _masterCDT = ConsumerDecisionTree.FetchById(consumerDecisionTreeDto.Id);

            EntityDto entityDto = new EntityDto()
            {
                Name = "Default",
                Id = 1
            };
            //ProductHierarchyDto hierarchyDto = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDto.Id)[0];
            ProductHierarchyDto productHierarchyDto;
            using (var dalContext = base.DalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IProductHierarchyDal>())
            {
                productHierarchyDto = dal.FetchByEntityId(base.EntityId);
            }
            List<ProductLevelDto> productLevelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDto.Id, 3);
            List<ProductGroupDto> groupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelList, 2);
            _masterProductUniverseDtos = TestDataHelper.InsertProductUniverseDtos(this.DalFactory, new List<EntityDto>() { entityDto }, groupDtoList);

            _masterConsumerDecisionTreeInfoListView = new ConsumerDecisionTreeInfoListViewModel();
            _masterConsumerDecisionTreeInfoListView.FetchForCurrentEntity();

            _masterCDT.ProductGroupId = _masterProductUniverseDtos.First().ProductGroupId;

            this.TestModel = new CdtMaintenanceCreateViewModel(_masterCDT, _masterProductUniverseDtos.First().Id);
        }

        [TearDown]
        public new void TearDown()
        {
            //tear down the base
            base.TearDown();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_CurrentTree()
        {
            String propertyName = "CurrentTree";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.CurrentTreeProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.AreEqual(_masterCDT.Id, this.TestModel.CurrentTree.Id);
        }

        [Test]
        public void Property_WizardType()
        {
            String propertyName = "WizardType";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.WizardTypeProperty.Path);

            Assert.AreEqual(CdtWizardType.Create, this.TestModel.WizardType);

            this.PropertyChangedNotifications.Clear();
            this.TestModel.WizardType = CdtWizardType.Merge;

            Assert.Contains(CdtMaintenanceCreateViewModel.WizardTypeProperty.Path, this.PropertyChangedNotifications);
            Assert.AreEqual(CdtWizardType.Merge, this.TestModel.WizardType);
        }


        [Test]
        public void Property_WizardStep()
        {
            String propertyName = "WizardStep";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.WizardStepProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.AreEqual(CdtWizardStep.Selection, this.TestModel.WizardStep);
        }

        [Test]
        public void Property_IsPreviousVisible()
        {
            String propertyName = "IsPreviousVisible";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.IsPreviousVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsFalse(this.TestModel.IsPreviousVisible, "IsPreviousVisible should start false");
        }

        [Test]
        public void Property_IsNextVisible()
        {
            String propertyName = "IsNextVisible";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.IsNextVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsTrue(this.TestModel.IsNextVisible, "IsNextVisible should start true");
        }

        [Test]
        public void Property_IsFinishVisible()
        {
            String propertyName = "IsFinishVisible";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.IsFinishVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsFalse(this.TestModel.IsFinishVisible, "IsFinishVisible should start false");
        }

        [Test]
        public void Property_IsYesVisible()
        {
            String propertyName = "IsYesVisible";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.IsYesVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsFalse(this.TestModel.IsYesVisible, "IsYesVisible should start false");
        }

        [Test]
        public void Property_IsNoVisible()
        {
            String propertyName = "IsNoVisible";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.IsNoVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsFalse(this.TestModel.IsNoVisible, "IsNoVisible should start false");
        }

        [Test]
        public void Property_IsCancelVisible()
        {
            String propertyName = "IsCancelVisible";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.IsCancelVisibleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsTrue(this.TestModel.IsCancelVisible, "IsCancelVisible should start true");
        }

        [Test]
        public void Property_IsComplete()
        {
            String propertyName = "IsComplete";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.IsCompleteProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            Assert.IsFalse(this.TestModel.IsComplete, "IsComplete should start false");
        }

        [Test]
        public void Property_ProgressText()
        {
            String propertyName = "ProgressText";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.ProgressTextProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_SelectedProductUniverse()
        {
            String propertyName = "SelectedProductUniverse";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.SelectedProductUniverseProperty.Path);

            Assert.AreEqual(_masterProductUniverseDtos.First().Id, this.TestModel.SelectedProductUniverse.Id);
        }

        [Test]
        public void Property_MasterProductUniverseInfoList()
        {
            String propertyName = "MasterProductUniverseInfoList";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.MasterProductUniverseInfoListProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.MasterProductUniverseInfoList);

            Assert.AreEqual(_masterProductUniverseDtos.Count, this.TestModel.MasterProductUniverseInfoList.Count);
        }

        [Test]
        public void Property_MissingProducts()
        {
            String propertyName = "MissingProducts";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.MissingProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.MissingProducts);
        }

        [Test]
        public void Property_AvailableAttributes()
        {
            String propertyName = "AvailableAttributes";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.AvailableAttributesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableAttributes);
        }

        [Test]
        public void Property_TakenAttributes()
        {
            String propertyName = "TakenAttributes";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.TakenAttributesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.TakenAttributes);

            Assert.IsEmpty(this.TestModel.TakenAttributes);
        }

        [Test]
        public void Property_SelectedAttribute()
        {
            String propertyName = "SelectedAttribute";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.SelectedAttributeProperty.Path);

            Assert.AreEqual(this.TestModel.AvailableAttributes.First(), this.TestModel.SelectedAttribute);
        }

        [Test]
        public void Property_SelectedTakenAttribute()
        {
            String propertyName = "SelectedTakenAttribute";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.SelectedTakenAttributeProperty.Path);

            Assert.IsNull(this.TestModel.SelectedTakenAttribute);
        }

        [Test]
        public void Property_TakenConsumerDecisionTrees()
        {
            String propertyName = "TakenConsumerDecisionTrees";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.TakenConsumerDecisionTreesProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.TakenConsumerDecisionTrees);
        }

        [Test]
        public void Property_SelectedTakenConsumerDecisionTree()
        {
            String propertyName = "SelectedTakenConsumerDecisionTree";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.SelectedTakenConsumerDecisionTreeProperty.Path);
        }

        [Test]
        public void Property_DuplicateProductGridItems()
        {
            String propertyName = "DuplicateProductGridItems";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.DuplicateProductGridItemsProperty.Path);

            AssertHelper.AssertIsEditableObservable(this.TestModel.DuplicateProductGridItems);
        }

        [Test]
        public void Property_HasMergeProblem()
        {
            String propertyName = "HasMergeProblem";
            Assert.AreEqual(propertyName, CdtMaintenanceCreateViewModel.HasMergeProblemProperty.Path);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //should start with no taken attributes
            Assert.IsEmpty(this.TestModel.TakenAttributes);

            //should start with available attributes
            Assert.AreNotEqual(0, this.TestModel.AvailableAttributes.Count);
        }

        #endregion

        #region Commands

        #region AddAttributeCommand

        [Test]
        public void Command_AddAttribute()
        {
            RelayCommand cmd = this.TestModel.AddAttributeCommand;
            Assert.AreEqual("AddAttributeCommand", CdtMaintenanceCreateViewModel.AddAttributeCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            Assert.Contains(CdtMaintenanceCreateViewModel.SelectedTakenAttributeProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(CdtMaintenanceCreateViewModel.SelectedAttributeProperty.Path, base.PropertyChangedNotifications);
            Assert.AreEqual(1, this.TestModel.TakenAttributes.Count);
        }

        #endregion

        #region RemoveAttributeCommand

        [Test]
        public void Command_RemoveAttribute()
        {
            RelayCommand cmd = this.TestModel.RemoveAttributeCommand;
            Assert.AreEqual("RemoveAttributeCommand", CdtMaintenanceCreateViewModel.RemoveAttributeCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Should not start with a selected taken attribute");
            this.TestModel.AddAttributeCommand.Execute();
            ConsumerDecisionTreePropertyComboBoxItem testProperty = this.TestModel.TakenAttributes.First();
            this.TestModel.SelectedTakenAttribute = testProperty;
            Assert.IsTrue(cmd.CanExecute());

            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            Assert.Contains(CdtMaintenanceCreateViewModel.SelectedTakenAttributeProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(CdtMaintenanceCreateViewModel.SelectedAttributeProperty.Path, base.PropertyChangedNotifications);
            Assert.IsEmpty(this.TestModel.TakenAttributes);
            Assert.IsNull(this.TestModel.SelectedTakenAttribute);
            Assert.Contains(testProperty, this.TestModel.AvailableAttributes);
        }

        #endregion

        #region AddConsumerDecisionTreeCommand

        [Test]
        public void Command_AddConsumerDecisionTree()
        {
            RelayCommand cmd = this.TestModel.AddConsumerDecisionTreeCommand;
            Assert.AreEqual("AddConsumerDecisionTreeCommand", CdtMaintenanceCreateViewModel.AddConsumerDecisionTreeCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            //Execute opens new window
        }

        #endregion

        #region RemoveConsumerDecisionTreeCommand

        [Test]
        public void Command_RemoveConsumerDecisionTree()
        {
            RelayCommand<ConsumerDecisionTreeInfo> cmd = this.TestModel.RemoveConsumerDecisionTreeCommand;
            Assert.AreEqual("RemoveConsumerDecisionTreeCommand", CdtMaintenanceCreateViewModel.RemoveConsumerDecisionTreeCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(null), "Should not be able to execute without a given consumer decision tree");

            Assert.IsTrue(cmd.CanExecute(_masterConsumerDecisionTreeInfoListView.Model[1]));

            //Execute will error as the cdt is not contained in the taken collection
        }

        #endregion

        #region PreviousCommand

        [Test]
        public void Command_Previous()
        {
            RelayCommand cmd = this.TestModel.PreviousCommand;
            Assert.AreEqual("PreviousCommand", CdtMaintenanceCreateViewModel.PreviousCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute on the first step");

            //Execute next to enable the previous
            this.TestModel.AddAttributeCommand.Execute();
            this.TestModel.NextCommand.Execute();

            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //The wizard step should have changed
            Assert.Contains(CdtMaintenanceCreateViewModel.WizardStepProperty.Path, base.PropertyChangedNotifications);
            Assert.AreEqual(CdtWizardStep.Selection, this.TestModel.WizardStep, "WizardStep should have changed to Type");
        }

        #endregion

        #region NextCommand

        [Test]
        public void Command_Next()
        {
            RelayCommand cmd = this.TestModel.NextCommand;
            Assert.AreEqual("NextCommand", CdtMaintenanceCreateViewModel.NextCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Should require an added property to continue");

            this.TestModel.AddAttributeCommand.Execute();
            Assert.IsTrue(cmd.CanExecute());

            //Moving to the CdtCreation step will create the CDT so loop until it is completed
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //The wizard step should have changed
            Assert.Contains(CdtMaintenanceCreateViewModel.WizardStepProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(CdtMaintenanceCreateViewModel.IsFinishVisibleProperty.Path, base.PropertyChangedNotifications);

            Assert.AreEqual(CdtWizardStep.CdtCreation, this.TestModel.WizardStep, "WizardStep should have progressed to CdtCreation");
            Assert.IsTrue(this.TestModel.IsFinishVisible, "The finish command should be visible at the end of the cdt creation from attributes");

            //Go back to the selection step and change the WizardType to merge
            this.TestModel.PreviousCommand.Execute();

            this.TestModel.WizardType = CdtWizardType.Merge;

            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute without any TakenConsumerDecisionTrees");

            //Cannot add consumer decision trees as a new window is opened
        }

        #endregion

        #region FinishCommand

        [Test]
        public void Command_Finish()
        {
            RelayCommand cmd = this.TestModel.FinishCommand;
            Assert.AreEqual("FinishCommand", CdtMaintenanceCreateViewModel.FinishCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute when not on the CdtCreation step");

            this.TestModel.NextCommand.Execute();
            this.TestModel.AddAttributeCommand.Execute();
            this.TestModel.NextCommand.Execute();

            Assert.IsTrue(cmd.CanExecute(), "Should be able to execute after the CdtCreation is complete");

            //Execute will close window
        }

        #endregion

        #region YesCommand

        [Test]
        public void Command_Yes()
        {
            RelayCommand cmd = this.TestModel.YesCommand;
            Assert.AreEqual("YesCommand", CdtMaintenanceCreateViewModel.YesCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region NoCommand

        [Test]
        public void Command_No()
        {
            RelayCommand cmd = this.TestModel.NoCommand;
            Assert.AreEqual("NoCommand", CdtMaintenanceCreateViewModel.NoCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            //Cannot check execute as this just closes the window
        }

        #endregion

        #region SelectProductUniverseCommand

        [Test]
        public void Command_ProductUniverseSelect()
        {
            RelayCommand cmd = this.TestModel.SelectProductUniverseCommand;
            Assert.AreEqual("SelectProductUniverseCommand", CdtMaintenanceCreateViewModel.SelectProductUniverseCommandProperty.Path);

            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //Can execute is always true
            //Execute opens new window
        }

        #endregion

        #endregion
    }
}
