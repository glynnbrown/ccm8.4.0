﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

//  V8-26634 : A.Silva ~ Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ValidationTemplateMaintenance
{
    [TestFixture]
    public class PlanItemFieldSelectorViewModelTests : TestBase<PlanItemFieldSelectorViewModel>
    {
        public override void Setup()
        {
            base.Setup();
            TestModel = new PlanItemFieldSelectorViewModel(String.Empty);
        }

        #region Properties

        [Test]
        public void Property_FieldText()
        {
            const string expected = "FieldText";

            var result = PlanItemFieldSelectorViewModel.FieldTextProperty.Path;

            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
        }

        [Test]
        public void Property_PlanItemFields()
        {
            const String expected = "PlanItemFields";

            var result = PlanItemFieldSelectorViewModel.PlanItemFieldsProperty.Path;

            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
            AssertHelper.AssertPropertyIsReadOnly(typeof(PlanItemFieldSelectorViewModel), expected);
        }

        [Test]
        public void Property_PlanItemTypeViews()
        {
            const String expected = "PlanItemTypeViews";

            var result = PlanItemFieldSelectorViewModel.PlanItemTypeViewsProperty.Path;

            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
            AssertHelper.AssertPropertyIsReadOnly(typeof(PlanItemFieldSelectorViewModel), expected);
        }

        [Test]
        public void Property_SelectedPlanItemField()
        {
            const String expected = "SelectedPlanItemField";

            var result = PlanItemFieldSelectorViewModel.SelectedPlanItemFieldProperty.Path;

            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
        }

        [Test]
        public void Property_SelectedPlanItemTypeView()
        {
            const String expected = "SelectedPlanItemTypeView";

            var result = PlanItemFieldSelectorViewModel.SelectedPlanItemTypeViewProperty.Path;

            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
        }

        [Test]
        public void Property_Error()
        {
            //TODO
        }

        [Test]
        public void Property_Item()
        {
            //TODO
        }

        #endregion
    }
}
