﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Added tests for backstage commands and properties.
// V8-26634 : A.Silva ~ Update save and save and new tests to account for changes in how a template is determined to be valid.
// V8-26817 : A.Silva ~ Refactored usage of IValidationTemplateEditorViewModel.

#endregion

#region Version History: (CCM v8.01)
// V8-28023 : L.Ineson
//  Added more tests.
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance;
using NUnit.Framework;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ValidationTemplateMaintenance
{
    [TestFixture]
    public class ValidationTemplateMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private void InsertTestData()
        {
            ValidationTemplate t1 = ValidationTemplate.NewValidationTemplate(this.EntityId);
            t1.Name = "Test1";
            t1.Save();

            ValidationTemplate t2 = ValidationTemplate.NewValidationTemplate(this.EntityId);
            t2.Name = "Test2";
            t2.Save();
        }

        #endregion

        #region Properties

        [Test]
        public void AvailableValidationTemplates_FetchedOnStartup()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableValidationTemplates.Count);
        }

        [Test]
        public void AvailableValidationTemplates_UpdatedOnItemInsert()
        {
            var viewModel = new ValidationTemplateMaintenanceViewModel();
            Int32 count = viewModel.AvailableValidationTemplates.Count;

            //save the new workflow
            viewModel.ValidationTemplateEdit.Name = "w1";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailableValidationTemplates.Count);
        }


        [Test]
        public void AvailableValidationTemplates_UpdatedOnItemDelete()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            Int32 count = viewModel.AvailableValidationTemplates.Count;

            //open and delete a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailableValidationTemplates.Count);
        }

        [Test]
        public void ValidationTemplateEdit_InitializedAsNew()
        {
            var viewModel = new ValidationTemplateMaintenanceViewModel();
            Assert.IsTrue(viewModel.ValidationTemplateEdit.IsNew);
            Assert.IsNullOrEmpty(viewModel.ValidationTemplateEdit.Name);
        }


        #endregion

        #region Commands

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new ValidationTemplateMaintenanceViewModel();
            var oldModel = viewModel.ValidationTemplateEdit;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.ValidationTemplateEdit);
            Assert.IsTrue(viewModel.ValidationTemplateEdit.IsNew);
        }

        [Test]
        public void NewCommand_NotDisabledWhenNoCreatePerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateCreate.ToString());

            //check
            var viewModel = new ValidationTemplateMaintenanceViewModel();
            Assert.IsTrue(viewModel.NewCommand.CanExecute());
        }

        [Test]
        public void OpenCommand_Execution()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();

            foreach (var info in viewModel.AvailableValidationTemplates)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.ValidationTemplateEdit.Id);
                Assert.IsFalse(viewModel.ValidationTemplateEdit.IsDirty);
            }
        }

        [Test]
        public void OpenCommand_DisabledWhenNoFetchPerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateGet.ToString());

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            Assert.IsFalse(viewModel.OpenCommand.CanExecute(viewModel.AvailableValidationTemplates.First().Id));
        }

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new ValidationTemplateMaintenanceViewModel();

            Assert.IsFalse(viewModel.ValidationTemplateEdit.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.ValidationTemplateEdit.Name = "Test1";

            Assert.IsTrue(viewModel.ValidationTemplateEdit.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenNewItemNoCreatePerm()
        {
            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.ValidationTemplateEdit.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateCreate.ToString());

            viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.ValidationTemplateEdit.Name = "Test1";
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenOldItemNoCreatePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateCreate.ToString());

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);
            viewModel.ValidationTemplateEdit.Name = "NewName";

            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenNewItemNoEditPerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateEdit.ToString());

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.ValidationTemplateEdit.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenOldItemNoEditPerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateEdit.ToString());

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);
            viewModel.ValidationTemplateEdit.Name = "NewName";

            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_Execution()
        {
            var viewModel = new ValidationTemplateMaintenanceViewModel();

            viewModel.ValidationTemplateEdit.Name = "Test1";

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.ValidationTemplateEdit.Name);
            Assert.IsFalse(viewModel.ValidationTemplateEdit.IsDirty);
        }

        [Test]
        public void SaveAsCommand_DisabledWhenNoCreatePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateCreate.ToString());

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);
            viewModel.ValidationTemplateEdit.Name = "NewName";

            Assert.IsFalse(viewModel.SaveAsCommand.CanExecute());
        }

        [Test]
        public void SaveAsCommand_Execution()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();

            Int32 preCount = ValidationTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);

            String newName = "SaveAsName";

            WindowService.PromptForSaveAsNameSetReponse(true, newName);
            viewModel.SaveAsCommand.Execute(newName);

            Assert.AreEqual(newName, viewModel.ValidationTemplateEdit.Name);
            Assert.IsFalse(viewModel.ValidationTemplateEdit.IsDirty);

            Assert.AreEqual(preCount + 1, ValidationTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new ValidationTemplateMaintenanceViewModel();

            viewModel.ValidationTemplateEdit.Name = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.ValidationTemplateEdit.Name);
            Assert.IsTrue(viewModel.ValidationTemplateEdit.IsNew);
        }

        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();

            Assert.IsTrue(viewModel.ValidationTemplateEdit.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);
            Assert.IsFalse(viewModel.ValidationTemplateEdit.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_DisabledWhenNoDeletePerm()
        {
            InsertTestData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateDelete.ToString());

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);

            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();

            Int32 preCount = ValidationTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableValidationTemplates.First().Id);

            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.ValidationTemplateEdit.IsNew);
            Assert.AreEqual(preCount - 1, ValidationTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.ValidationTemplateEdit.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(1);

            Assert.IsTrue(continueWithItemChangeFired);
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            InsertTestData();

            var viewModel = new ValidationTemplateMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.ValidationTemplateEdit.Name = "Name";

            Boolean continueWithItemChangeFired = false;

            WindowService.AddResponse(NUnitWindowService.ContinueWithItemChangeMethod,
                (p) => { continueWithItemChangeFired = true; p.Result = false; });

            viewModel.OpenCommand.Execute(2);
            Assert.IsTrue(continueWithItemChangeFired);
        }


        #endregion

    }
}
