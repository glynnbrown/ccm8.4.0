﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26099 : I.George
//	Copied from GFS

#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LocationProductIllegalMaintenance;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LocationProductIllegal
{
    [TestFixture]
    class LocationToProductIllegalViewModelTests : TestBase<LocationToProductIllegalViewModel>
    {
        #region Test Fixture Helpers

        [SetUp]
        public new void Setup()
        {
            this.TestModel = new LocationToProductIllegalViewModel();
        }
      #endregion

        #region Properties

        [Test]
        public void Property_ColumnSet()
        {
            String propertyName = "ColumnSet";
            Assert.AreEqual(propertyName, LocationToProductIllegalViewModel.ColumnSetProperty.Path, "Property names should match");
        }

        [Test]
        public void Property_FilterIllegalLinks()
        {
            String propertyName = "FilterIllegalLinks";
            Assert.AreEqual(propertyName, LocationToProductIllegalViewModel.FilterIllegalLinksProperty.Path, "property names should match");

            //Check property set
            this.PropertyChangedNotifications.Clear();
            this.TestModel.FilterIllegalLinks = false;
            Assert.AreEqual(false, this.TestModel.FilterIllegalLinks);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }


        [Test]
        public void Property_SelectedLocations()
        {
            String propertyName = "SelectedLocations";
            Assert.AreEqual(propertyName, LocationToProductIllegalViewModel.SelectedLocationsProperty.Path, "property names should match");

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_AvailableProductInfos()
        {
            String propertyName = "AvailableProductInfos";
            Assert.AreEqual(propertyName, LocationToProductIllegalViewModel.AvailableProductInfosProperty.Path, "property names should match");

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableProductInfos);
        }

        [Test]
        public void Property_AvailableLocations()
        {
            String propertyName = "AvailableLocations";
            Assert.AreEqual(propertyName, LocationToProductIllegalViewModel.AvailableLocationInfosProperty.Path, "property names should match");

            AssertHelper.AssertIsReadOnlyCollection(this.TestModel.AvailableLocations);
        }

        [Test]
        public void Property_AvailableIllegalLinkRows()
        {
            String propertyName = "AvailableIllegalLinkRows";
            Assert.AreEqual(propertyName, LocationToProductIllegalViewModel.AvailableIllegalLinkRowsProperty.Path, "property names should match");

            AssertHelper.AssertIsEditableObservable(this.TestModel.AvailableIllegalLinkRows);
        }

        [Test]
        public void Property_AvailableProductGroups()
        {
            String propertyName = "AvailableProductGroups";
            Assert.AreEqual(propertyName, LocationToProductIllegalViewModel.AvailableProductGroupsProperty.Path, "Property names should match");

            AssertHelper.AssertIsReadOnlyCollection(this.TestModel.AvailableProductGroups);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {

        }

        #endregion

        #region Commands

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;
            //Check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            SaveCommandCheckExpectedValues(cmd);

            //Command will be disabled on startup as no rows have been fetched or edited
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");
           
         //   Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");
            //Should still be disabled as no edit has been made
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");

             //Command should be enabled after editing
            //Assert.IsTrue(cmd.CanExecute(), "Command should be enabled");

            cmd.Execute();
            //Cannot really test whether the execution was successful because of the way it is handled
        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;
            //Check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            SaveAndCloseCommandCheckExpectedValues(cmd);

            //Command will be disabled on startup as no rows have been fetched or edited
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");

            //Should still be disabled as no edit has been made
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled");

            cmd.Execute();
            //Cannot really test whether the execution was successful because of the way it is handled
        }

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;
            //Check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel");

            //check the command properties
            Assert.IsNotNullOrEmpty(cmd.FriendlyName, "The friendly name should be populated");
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyDescription), "FriendlyDescription should be populated");

            //Command should be disabled when passed a null id
            Assert.IsFalse(cmd.CanExecute(null));

            //Check correct illegal link rows were loaded
            List<ProductInfo> illegalLinkInfos = this.TestModel.AvailableIllegalLinkRows.Select(p => p.ProductInfo).ToList();
        }

        [Test]
        public void Command_CloseCommand()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", LocationToProductIllegalViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

    }
}
