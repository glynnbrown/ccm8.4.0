﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27720 : I.George
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using System.Collections.ObjectModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.UnitTests.WorkflowUI.AssortmentMinorRevisionMaintenance
{
    [TestFixture]
    public class AssortmentMinorRevisionMaintenanceViewModelTests : TestBase<AssortmentMinorRevisionSetupViewModel>
    {
        #region Helper Methods  
        private List<ConsumerDecisionTreeNodeDto> _cdtNodes;
        private AssortmentMinorRevisionInfoListViewModel _assortmentMinorRevisionInfoView;
        private ConsumerDecisionTreeLevel _level;
        private IAssortmentMinorRevisionAction _assormentAction;
        private ConsumerDecisionTreeNode _cdtNode; 
        private String _gtin;
        private String _name;
        private AssortmentMinorRevision assortmentMinor;
        private AssortmentMinorRevisionListAction newAction;
    
        
        // insert test data            
        private List<AssortmentMinorRevision>InsertTestData()
         {
             List<AssortmentMinorRevision> AssortmentList = new List<AssortmentMinorRevision>();
             List<AssortmentMinorRevisionDto> assortments = TestDataHelper.InsertAssortmentMinorRevisionDtos(base.DalFactory, 5, this.EntityId);
             ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
             Int32 groupId = hierarchy.FetchAllGroups().Last().Id;

             AssortmentMinorRevision assortmentMinorRevision = AssortmentMinorRevision.NewAssortmentMinorRevision(this.EntityId);
             assortmentMinorRevision.Name = "MinorRevision1";
             assortmentMinorRevision.ProductGroupId = groupId;
             AssortmentList.Add(assortmentMinorRevision.Save());

              assortmentMinorRevision = AssortmentMinorRevision.NewAssortmentMinorRevision(this.EntityId);
             assortmentMinorRevision.Name = "MinorRevision2";
             assortmentMinorRevision.ProductGroupId = groupId;
             AssortmentList.Add(assortmentMinorRevision.Save());

             _assortmentMinorRevisionInfoView = new AssortmentMinorRevisionInfoListViewModel();
             _assortmentMinorRevisionInfoView.FetchForEntity(this.EntityId);

             return AssortmentList;
        }

        /// <summary>
        /// Method to set the Action, ActionLocation 
        /// </summary>
        /// <param name="assort"></param>
        /// <param name="action"></param>
        public void ArrangeActionLocationTest()
        {
            assortmentMinor = AssortmentMinorRevision.NewAssortmentMinorRevision(this.EntityId);
            base.PropertyChangedNotifications.Clear();
            this.TestModel.PropertyChanged += base.TestModel_PropertyChanged;

            newAction = AssortmentMinorRevisionListAction.NewAssortmentMinorRevisionListAction();
            assortmentMinor.ListActions.Add(newAction);
            AssortmentMinorRevisionListActionLocation item = AssortmentMinorRevisionListActionLocation.NewAssortmentMinorRevisionListActionLocation();
            AssortmentMinorRevisionListActionLocation item2 = AssortmentMinorRevisionListActionLocation.NewAssortmentMinorRevisionListActionLocation();
            AssortmentMinorRevisionListActionLocation item3 = AssortmentMinorRevisionListActionLocation.NewAssortmentMinorRevisionListActionLocation();
             //Add a few locationscodes to see if it gets the distinct location code
            item.LocationCode = "location1";
            item2.LocationCode = "location2";
            item3.LocationCode = "location2";
            Assert.IsNotNull(newAction.Locations);
            newAction.Locations.Add(item);
            newAction.Locations.Add(item2);
            newAction.Locations.Add(item3);
            CollectionAssert.IsNotEmpty(assortmentMinor.ListActions);
        }

        public override void Setup()
        {
            //set up test
            base.Setup();
            InsertTestData();
            this.TestModel = new AssortmentMinorRevisionSetupViewModel();
            //Create hierarchy levels
            List<ConsumerDecisionTreeDto> cdts = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, this.EntityId, 2);
            List<ConsumerDecisionTreeLevelDto> cdtLevels = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(base.DalFactory, cdts[0], 1, true);
            _cdtNodes = TestDataHelper.InsertConsumerDecisionTreeNodeDtos(base.DalFactory, cdts[0], cdtLevels, 2, true);

            _assormentAction = AssortmentMinorRevisionListAction.NewAssortmentMinorRevisionListAction();
            _level = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel();
             _cdtNode = ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(_level.Id);
             _gtin = String.Empty;
            _name = String.Empty;
        }

        #endregion

        #region Properties

        [Test]
        public void Property_SelectedAssortmentMinorRevision_Initialized()
        {
            //check that the property name matches
            String propertyName = "SelectedAssortmentMinorRevision";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.SelectedAssortmentMinorRevisionProperty.Path);

            //check that the SelectedAssortmentMinorRevision gets loaded
            Assert.IsTrue(this.TestModel.SelectedAssortmentMinorRevision.IsNew, "Should be a new AssortmentMinorRevision loaded");
          
            // check that it is not null on start up
            Assert.IsNotNull(this.TestModel.SelectedAssortmentMinorRevision);
        }

        [Test]
        public void Property_SelectedAssortmentMinorRevision_Setter()
        {
            AssortmentMinorRevision assortmentMinor = AssortmentMinorRevision.NewAssortmentMinorRevision(this.EntityId);
            
            base.PropertyChangedNotifications.Clear();
            this.TestModel.PropertyChanged += base.TestModel_PropertyChanged;

            //set an identifiable property
            assortmentMinor.Name = "assortment";
            this.TestModel.SelectedAssortmentMinorRevision = assortmentMinor;

            Assert.AreEqual("assortment", this.TestModel.SelectedAssortmentMinorRevision.Name);
            
            //check that onproperty changed was fired
            Assert.Contains("SelectedAssortmentMinorRevision", base.PropertyChangedNotifications);
            Assert.Contains("IsReadOnly", base.PropertyChangedNotifications);
            
            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SelectedConsumerDecisonTree_UpdatedOnSelectedAssortmentChange()
        {
            this.TestModel.NewCommand.Execute();

            //set the mandatory properties inc selected merch group
            this.TestModel.SelectedAssortmentMinorRevision.Name = "SelectedCdtTest";

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());

            ConsumerDecisionTreeInfoList info = ConsumerDecisionTreeInfoList.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedConsumerDecisonTree = info.Last();
            Int32 cdtId = this.TestModel.SelectedConsumerDecisonTree.Id;

            //save
            this.TestModel.SaveCommand.Execute();
            Int32 saveId = this.TestModel.SelectedAssortmentMinorRevision.Id;

            base.PropertyChangedNotifications.Clear();
            this.TestModel.PropertyChanged += base.TestModel_PropertyChanged;

            this.TestModel.NewCommand.Execute();

            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedConsumerDecisonTreeProperty.Path,
              base.PropertyChangedNotifications, "Property changed was not fired");
            Assert.IsNull(this.TestModel.SelectedConsumerDecisonTree, "null for new item");

            base.PropertyChangedNotifications.Clear();
            this.TestModel.OpenCommand.Execute(saveId);

            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedConsumerDecisonTreeProperty.Path,
               base.PropertyChangedNotifications, "Property changed was not fired");


            Assert.AreEqual(cdtId, this.TestModel.SelectedMerchandisingGroup.Id,
                "Should match the id that was set against the assortment");
            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
       }

        [Test]
        public void Property_SelectedConsumerDecisonTree_Setter()
        {
            this.TestModel.NewCommand.Execute();
            Assert.IsNull(this.TestModel.SelectedConsumerDecisonTree);
            //setter
            //load an item
            ConsumerDecisionTreeInfoList info = ConsumerDecisionTreeInfoList.FetchByEntityId(this.EntityId);
            ConsumerDecisionTreeInfo testCdt = info.Last();

            base.PropertyChangedNotifications.Clear();
            this.TestModel.PropertyChanged += base.TestModel_PropertyChanged;
           
            this.TestModel.SelectedConsumerDecisonTree = testCdt;

            Assert.AreEqual(testCdt.Id, this.TestModel.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId);
            Assert.Contains("SelectedConsumerDecisonTree", base.PropertyChangedNotifications);

            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SelectedMerchandisingGroup_UpdatedOnSelectedAssortmentChange()
        {
            //start with a new item
            this.TestModel.NewCommand.Execute();

            //set the mandatory properties including selected merch group
            this.TestModel.SelectedAssortmentMinorRevision.Name = "SelectedMerchGroupTest";
            
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());
            Int32 groupId = this.TestModel.SelectedMerchandisingGroup.Id;

            //save
            this.TestModel.SaveCommand.Execute();
            Int32 savedAssortId = this.TestModel.SelectedAssortmentMinorRevision.Id;

            //check for selected merch group change on new item
            base.PropertyChangedNotifications.Clear();
            this.TestModel.PropertyChanged += base.TestModel_PropertyChanged;

            this.TestModel.NewCommand.Execute();

            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedMerchandisingGroupProperty.Path,
                base.PropertyChangedNotifications, "Property changed was not fired");

            Assert.IsNull(this.TestModel.SelectedMerchandisingGroup, "New item was loaded merch group should be null");

            //reload saved assort
            base.PropertyChangedNotifications.Clear();
            this.TestModel.OpenCommand.Execute(savedAssortId);


            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedMerchandisingGroupProperty.Path,
               base.PropertyChangedNotifications, "Property changed was not fired");


            Assert.AreEqual(groupId, this.TestModel.SelectedMerchandisingGroup.Id,
                "Should match the id that was set against the assortment");

              //check that the check that SelectedMerchandisingGroup.Id == the assortment minor product group id
            Assert.AreEqual(groupId, this.TestModel.SelectedAssortmentMinorRevision.ProductGroupId,
                "Should match the id that was set against the assortment");


            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SelectedMerchandisingGroup_Setter()
        {
            this.TestModel.NewCommand.Execute();
            Assert.IsNull(this.TestModel.SelectedMerchandisingGroup);

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            base.PropertyChangedNotifications.Clear();
            this.TestModel.PropertyChanged += base.TestModel_PropertyChanged;
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());
            
            Int32 selectedMerchGroup = this.TestModel.SelectedMerchandisingGroup.Id;
            Assert.AreEqual(selectedMerchGroup, this.TestModel.SelectedAssortmentMinorRevision.ProductGroupId);
            Assert.Contains("SelectedMerchandisingGroup", base.PropertyChangedNotifications);

            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_FilterItemsKey_Setter()
        {
            //check that the property name matches
            String propertyName = "FilterItemsKey";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.FilterItemsKeyProperty.Path);

            //check that its empty on startup
            Assert.IsNullOrEmpty(this.TestModel.FilterItemsKey, "originally FilterItemsKey should be empty");

            base.PropertyChangedNotifications.Clear();

            this.TestModel.PropertyChanged += base.TestModel_PropertyChanged;

            // check that it filters items 
            this.TestModel.FilterItemsKey = "v1";
            Assert.IsNotNullOrEmpty(this.TestModel.FilterItemsKey, "FilterItemsKey should no longer be empty");
            Assert.AreEqual("v1", this.TestModel.FilterItemsKey);
            Assert.Contains("FilterItemsKey", base.PropertyChangedNotifications);
            Assert.Contains("FilterItemsCountMessage", base.PropertyChangedNotifications);
           
            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_AvailableAssortmentMinorRevisionInfos_Initialised()
        {

            //check that the property name matches
            String propertyName = "AvailableAssortmentMinorRevisionInfos";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.AvailableAssortmentsProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableAssortmentMinorRevisionInfos);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //check the correct number of actual infos is available
            Int32 expectedInfoCount = _assortmentMinorRevisionInfoView.Model.Count();
            Int32 actualInfoCount = this.TestModel.AvailableAssortmentMinorRevisionInfos.Count();
            Assert.AreEqual(expectedInfoCount, actualInfoCount, "All infos should be available");
        }

        [Test]
        public void Property_AvailableAssortmentMinorRevisionInfos_UpdatedOnItemDeleted()
        {
           
            Int32 count = this.TestModel.AvailableAssortmentMinorRevisionInfos.Count();
            int id = this.TestModel.AvailableAssortmentMinorRevisionInfos.Last().Id;

            // open an already existing info
            this.TestModel.OpenCommand.Execute(id);
            //delete an item 
            this.TestModel.DeleteCommand.Execute();

            //check that the infos are updated
            Assert.AreEqual(count - 1, this.TestModel.AvailableAssortmentMinorRevisionInfos.Count);
            Assert.AreNotEqual(id, this.TestModel.AvailableAssortmentMinorRevisionInfos.Last().Id);

        }

        [Test]
        public void Property_AvailableAssortmentMinorRevisionInfos_UpdatedOnItemAdded()
        {
            Int32 count = this.TestModel.AvailableAssortmentMinorRevisionInfos.Count();
             Int32 id = this.TestModel.AvailableAssortmentMinorRevisionInfos.Last().Id;

            //save an info 
            this.TestModel.SelectedAssortmentMinorRevision.Name = "assortmentInfos";
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());

           
            this.TestModel.SaveCommand.Execute();

            //check that the infos are updated
            Assert.AreEqual(count + 1, this.TestModel.AvailableAssortmentMinorRevisionInfos.Count);
            Assert.AreNotEqual(id, this.TestModel.AvailableAssortmentMinorRevisionInfos.Last().Id);
        }

        [Test]
        public void Property_Actions()
        {
            //check that the property name matches
            String propertyName = "Actions";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.ActionsProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.Actions);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_Actions_UpdatedOnSelectedAssortmentChange()
        {
            // check that the collection is empty
            CollectionAssert.IsEmpty(this.TestModel.Actions);

            //set a list action
            AssortmentMinorRevision testAssortmentMinorRevision = AssortmentMinorRevision.NewAssortmentMinorRevision(this.EntityId);
            base.PropertyChangedNotifications.Clear();
            AssortmentMinorRevisionListAction newAction = AssortmentMinorRevisionListAction.NewAssortmentMinorRevisionListAction();
            testAssortmentMinorRevision.ListActions.Add(newAction);
            //check that it's got an action
            CollectionAssert.IsNotEmpty(testAssortmentMinorRevision.ListActions);

            // on property change
            this.TestModel.SelectedAssortmentMinorRevision = testAssortmentMinorRevision;

           // check that the action added
            CollectionAssert.IsNotEmpty(this.TestModel.Actions, "There should be actions after selecting an AssortmentMinorRevision.");
            // check that action matches
            Assert.AreEqual(this.TestModel.Actions.Select(o => o.Action), testAssortmentMinorRevision.ListActions);
        }

        [Test]
        public void Property_ActionLocations_UpdatedOnSelectedAssortmentChange()
        {
            //start with a new item
            this.TestModel.NewCommand.Execute();
            // check that the collection is empty
            CollectionAssert.IsEmpty(this.TestModel.ActionLocations);

            // calls the method that sets the action location
            ArrangeActionLocationTest();
            MinorRevisionActionWrapperViewModel action = new MinorRevisionActionWrapperViewModel(newAction, _cdtNode, _gtin, _name);
            this.TestModel.SelectedAction = action;

            CollectionAssert.AreEqual(this.TestModel.ActionLocations.Select(o => o.ActionLocation), assortmentMinor.ListActions.SelectMany(o => o.Locations.ToList<AssortmentMinorRevisionListActionLocation>()));
        }

         [Test]
        public void Property_SelectedAction_Setter()
        {
            //check that the property name matches
            String propertyName = "SelectedAction";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.SelectedActionProperty.Path);
            
             base.PropertyChangedNotifications.Clear();

             MinorRevisionActionWrapperViewModel action = new MinorRevisionActionWrapperViewModel(_assormentAction, _cdtNode, _gtin, _name);

            this.TestModel.SelectedAction = action;

            Assert.AreEqual(action, this.TestModel.SelectedAction);
            Assert.Contains("SelectedAction", base.PropertyChangedNotifications);

            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
            
        }

        [Test]
        public void Property_NumberOfLocationsAffected_UpdatedOnSelectedAssortmentChange()
        {
            this.TestModel.NewCommand.Execute();

            // calls the method that sets the action location
            ArrangeActionLocationTest();

            this.TestModel.SelectedAssortmentMinorRevision = assortmentMinor;

            Assert.AreEqual(assortmentMinor.ListActions.SelectMany(p => p.Locations.ToList<AssortmentMinorRevisionListActionLocation>().Select(o => o.LocationCode).Distinct()).Count(), this.TestModel.NumberOfLocationsAffected);

            //check that the property changes
            Assert.Contains("NumberOfLocationsAffected", base.PropertyChangedNotifications);
            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_AvailableCDTNodes()
        {
            //check that the property name matches
            String propertyName = "AvailableCDTNodes";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.AvailableCDTNodesProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableCDTNodes);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_AvailableCDTNodes_UpdatedOnSelectedAssortmentChange()
        {
            this.TestModel.NewCommand.Execute();

            AssortmentMinorRevision testAssortmentMinorRevision = AssortmentMinorRevision.NewAssortmentMinorRevision(this.EntityId);
            
            this.TestModel.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId = _cdtNodes[0].Id;
            testAssortmentMinorRevision.ConsumerDecisionTreeId = this.TestModel.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId;

            this.TestModel.SelectedAssortmentMinorRevision = testAssortmentMinorRevision;

            Assert.AreEqual(this.TestModel.AvailableCDTNodes.Count, _cdtNodes.Count);
        }

        [Test]
        public void Property_ActionLocation()
        {
            //check that the property name matches
            String propertyName = "ActionLocations";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.ActionLocationsProperty.Path);

            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.ActionLocations);
            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }


        [Test]
        public void Property_NumberOfLocationsAffected()
        {
            //check that the property name matches

            String propertyName = "NumberOfLocationsAffected";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.NumberOfLocationsAffectedProperty.Path);
            base.PropertyChangedNotifications.Clear();

            Int32 number = 2;
            this.TestModel.NumberOfLocationsAffected = number;
            Assert.AreEqual(number, this.TestModel.NumberOfLocationsAffected);
            Assert.Contains("NumberOfLocationsAffected", base.PropertyChangedNotifications);
            this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SelectedActionType()
        {
            //check that the property name matches
            String propertyName = "SelectedActionType";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.SelectedActionTypeProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
       }

          [Test]
          public void Property_FilterItemsCountMessage()
        {
            //check that the property name matches
            String propertyName = "FilterItemsCountMessage";
            Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.FilterItemsCountMessageProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }
         
          [Test]
          public void Property_IsReadonly()
          {
              //check that the property name matches
              String propertyName = "IsReadOnly";
              Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.IsReadOnlyProperty.Path);

              AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
          }

          [Test]
          public void Property_ShowLocations()
          {
              //check that the property name matches
              String propertyName = "ShowLocations";
              Assert.AreEqual(propertyName, AssortmentMinorRevisionSetupViewModel.ShowLocationsProperty.Path);

              base.PropertyChangedNotifications.Clear();

              this.TestModel.ShowLocations = false;
              Assert.IsFalse(this.TestModel.ShowLocations);
              Assert.Contains("ShowLocations", base.PropertyChangedNotifications);

              this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
          }

          [Test]
          public void Property_SelectedActionType_OnSelectedActionChange()
          {
              //start with a new item
              this.TestModel.NewCommand.Execute();
              // check that the collection is empty
              Assert.IsNull(this.TestModel.SelectedActionType);

              // calls the method that sets the action location
              ArrangeActionLocationTest();
              MinorRevisionActionWrapperViewModel action = new MinorRevisionActionWrapperViewModel(newAction, _cdtNode, _gtin, _name);
              this.TestModel.SelectedAction = action;

              Assert.AreEqual(this.TestModel.SelectedActionType, action.Action.Type);
              Assert.Contains("SelectedActionType", base.PropertyChangedNotifications);
              this.TestModel.PropertyChanged -= base.TestModel_PropertyChanged;
           }

        #endregion

        #region Constructor
        [Test]
        public void Constructor()
        {
            // checks these properties are not null on start up
            Assert.IsNotNull(this.TestModel.SelectedAssortmentMinorRevision);
            Assert.IsTrue(this.TestModel.SelectedAssortmentMinorRevision.IsNew);
            Assert.IsNotNull(this.TestModel.AvailableAssortmentMinorRevisionInfos);
        }
           
        #endregion

        #region Command

        #region NewCommand

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;
            Assert.AreEqual("NewCommand", AssortmentMinorRevisionSetupViewModel.NewCommandProperty.Path);

            //check command is registered and values are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            AssortmentMinorRevision preExecuteItem = this.TestModel.SelectedAssortmentMinorRevision;

            cmd.Execute();

            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedAssortmentMinorRevisionProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedAssortmentMinorRevision, "The item should have changed.");
            Assert.IsTrue(this.TestModel.SelectedAssortmentMinorRevision.IsNew, "The item should be new");
            Assert.IsTrue(this.TestModel.SelectedAssortmentMinorRevision.IsInitialized, "The item should be marked initialized");
        }
        #endregion

        #region OpenCommand

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;
            Assert.AreEqual("OpenCommand", AssortmentMinorRevisionSetupViewModel.OpenCommandProperty.Path);

            //check command is registered and value are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            OpenCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();
            AssortmentMinorRevision preExecuteItem = this.TestModel.SelectedAssortmentMinorRevision;

            AssortmentMinorRevisionInfo loadInfo = AssortmentMinorRevisionInfoList.FetchByEntityId(1).Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedAssortmentMinorRevision, "Item should have changed");
            Assert.AreEqual(loadInfo.Id, this.TestModel.SelectedAssortmentMinorRevision.Id, "The assortment minor revision with the info id should have been loaded");
            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedAssortmentMinorRevisionProperty.Path, base.PropertyChangedNotifications, "Selected item should have change notification");
        }
        #endregion

        #region Save Command
        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;
            SaveCommandCheckExpectedValues(cmd);
            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //when no item is dirty
            Assert.IsFalse(cmd.CanExecute());
            //Add a name
            this.TestModel.SelectedAssortmentMinorRevision.Name = "AMR";

            Assert.IsFalse(this.TestModel.SelectedAssortmentMinorRevision.IsValid, "no merch group set, not valid");
            Assert.IsFalse(cmd.CanExecute(), "save disabled as item is invalid");

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());

            Assert.IsTrue(cmd.CanExecute(), "save enabled as assortment minor revision is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected store has been updated
            Assert.IsFalse(this.TestModel.SelectedAssortmentMinorRevision.IsNew, "The AMR should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedAssortmentMinorRevision.IsDirty, "The AMR should have been saved");
            Assert.AreEqual("AMR", this.TestModel.SelectedAssortmentMinorRevision.Name, "The AMR should be the same");
            Assert.AreNotEqual(0, this.TestModel.SelectedAssortmentMinorRevision.Id, "The AMR should have been given a valid id");
            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedAssortmentMinorRevisionProperty.Path, base.PropertyChangedNotifications);

            Assert.IsTrue(cmd.CanExecute(), "Command should still be enabled");

            //check that another Assortment Minor Revision with the same name cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.SelectedAssortmentMinorRevision.Name = "AMR";
            Assert.IsFalse(cmd.CanExecute());
        }

        #endregion

        #region SaveAsCommand
        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");
            Assert.NotNull(cmd.SmallIcon, "Icon should be populated");

            //check that it cannot execute since no item is valid
            Assert.IsFalse(cmd.CanExecute());

            // check that it cannot execute since the it has no merch group
            this.TestModel.SelectedAssortmentMinorRevision.Name = "Test";
            Assert.IsFalse(this.TestModel.SelectedAssortmentMinorRevision.IsValid, "no merch group set, not valid");
            Assert.IsFalse(cmd.CanExecute(), "save disabled as item is invalid");

            //set the merch group
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());
            Assert.IsTrue(cmd.CanExecute(), "Command should be enabled when selected item is populated & valid");

            //check execute
            base.PropertyChangedNotifications.Clear();

            //load an existing item
            AssortmentMinorRevisionInfoList availableInfoList = AssortmentMinorRevisionInfoList.FetchByEntityId(1);

            AssortmentMinorRevisionInfo loadInfo = availableInfoList.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            AssortmentMinorRevision beforeItem = this.TestModel.SelectedAssortmentMinorRevision;
            cmd.Execute();


            //Get the current category and alter it
            Assert.AreNotEqual(beforeItem, this.TestModel.SelectedAssortmentMinorRevision, "Item should have changed");
            Assert.AreNotEqual(beforeItem.Id, this.TestModel.SelectedAssortmentMinorRevision.Id, "Id should be different");
            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedAssortmentMinorRevisionProperty.Path, base.PropertyChangedNotifications, "should have change notification");

            //execute
            cmd.Execute();

            //check infos are correct
            availableInfoList = AssortmentMinorRevisionInfoList.FetchByEntityId(1);

            AssortmentMinorRevisionInfo oldItemInfo = availableInfoList.FirstOrDefault(l => l.Id == beforeItem.Id);
            Assert.IsNotNull(oldItemInfo, "An info for the old item should still exist");

            AssortmentMinorRevisionInfo newItemInfo = availableInfoList.FirstOrDefault(l => l.Id == this.TestModel.SelectedAssortmentMinorRevision.Id);
            Assert.IsNotNull(newItemInfo, "An info for the new item should have been added");
 
        }
        #endregion

        #region DeleteCommand
        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new item");

          
            //load an existing item
            AssortmentMinorRevisionInfoList availableInfoList = AssortmentMinorRevisionInfoList.FetchByEntityId(1);
            AssortmentMinorRevisionInfo loadInfo = availableInfoList.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing item");

            //check execute
            Int32 oldItemId = this.TestModel.SelectedAssortmentMinorRevision.Id;
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedAssortmentMinorRevision.IsNew, "A new item should have been loaded");

            availableInfoList = AssortmentMinorRevisionInfoList.FetchByEntityId(1);
            AssortmentMinorRevisionInfo oldInfo = availableInfoList.FirstOrDefault(l => l.Id == oldItemId);
            Assert.IsNull(oldInfo, "The deleted item should have been removed from the available list");
        }
        #endregion

        #region SaveAndCloseCommand
        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;
            Assert.AreEqual("SaveAndCloseCommand", AssortmentMinorRevisionSetupViewModel.SaveAndCloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            this.TestModel.SelectedAssortmentMinorRevision.Name = "st1";

            Assert.IsFalse(this.TestModel.SelectedAssortmentMinorRevision.IsValid, "no merch group set, not valid");
            Assert.IsFalse(cmd.CanExecute(), "save disabled as item is invalid");

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execution
            //nb cannot test the the window closed.
            //load an existing item
            AssortmentMinorRevisionInfoList availableInfoList = AssortmentMinorRevisionInfoList.FetchByEntityId(1);
            Int32 preItemCount = availableInfoList.Count();

            cmd.Execute();

            availableInfoList = AssortmentMinorRevisionInfoList.FetchByEntityId(1);
            Assert.AreEqual(preItemCount + 1, availableInfoList.Count());
        }
        #endregion

        #region SaveandNew Command

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;

            //check that the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            //check cannot execute
            Assert.IsFalse(cmd.CanExecute(), "Item is not dirty");
            String assortmentName = "AMR";

            //check cannot execute without a merch group
            this.TestModel.SelectedAssortmentMinorRevision.Name = assortmentName;
            Assert.IsFalse(this.TestModel.SelectedAssortmentMinorRevision.IsValid, "no merch group set, not valid");
            Assert.IsFalse(cmd.CanExecute(), "save disabled as item is invalid");

            //set a merch group
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            this.TestModel.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(hierarchy.FetchAllGroups().Last());

            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check excution
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedAssortmentMinorRevision.IsNew, "A new store should have been loaded");
            Assert.AreEqual(assortmentName, this.TestModel.AvailableAssortmentMinorRevisionInfos.Last().Name, "the saved store should be in the available list");
            Assert.Contains(AssortmentMinorRevisionSetupViewModel.SelectedAssortmentMinorRevisionProperty.Path, base.PropertyChangedNotifications, "selected store should have changed notifications");
        }
        #endregion

        #endregion
    }
}
