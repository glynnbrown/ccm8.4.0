﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-27630 : I.George
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LocationProductAttributemaintenance
{
    [TestFixture]
    public class LocationProductAttributeViewModelTests : TestBase<LocationProductAttributeMaintenanceViewModel>
    {
        #region Test Fixture Helpers
        private LocationInfoListViewModel _locationInfoListView;
        private ProductInfoListViewModel _productInfoListView;
        private Entity _entity;
        List<Product> _productList;
        List<LocationDto> _locationDtoList;
        private String _searchCriteria = String.Empty;
        public override void Setup()
        {
            base.Setup();

            //insert test data
            _entity = Entity.FetchById(this.EntityId);

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(_entity.Id);
            _productList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
             _locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, _entity.Id);

             _productInfoListView = new ProductInfoListViewModel();
            _locationInfoListView = new LocationInfoListViewModel();
            _locationInfoListView.FetchAllForEntity();
            _productInfoListView.BeginFetchForCurrentEntityBySearchCriteria(_searchCriteria);
           
            this.TestModel = new LocationProductAttributeMaintenanceViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailableLocations()
        {
            String propertyName = "AvailableLocations";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.AvailableLocationsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableLocations);

            foreach (LocationInfo info in _locationInfoListView.Model)
            {
                LocationInfo availableInfo = this.TestModel.AvailableLocations.FirstOrDefault(l => l.Id == info.Id);
                Assert.IsNotNull(availableInfo, "All locations should be available");
            }
        }

        [Test]
        public void Property_AvailableProducts()
        {
            String propertyName = "ProductSearchResults";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.AvailableProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.ProductSearchResults);
            foreach (ProductInfo info in _productInfoListView.Model)
            {
                ProductInfo availableInfo = this.TestModel.ProductSearchResults.FirstOrDefault(l => l.Id == info.Id);
                Assert.IsNotNull(availableInfo, "All products should be available");
            }
       }

        

        [Test]
        public void Property_SelectedLinkedProduct_UpdatesOnSelectedItemChange()
        {
           var testmodel = new LocationProductAttributeMaintenanceViewModel();

            base.PropertyChangedNotifications.Clear();
            testmodel.PropertyChanged += base.TestModel_PropertyChanged;

            //open a location product attribute to set selected item
            Int16 locId = _locationInfoListView.Model.First().Id;
            Int32 prodId = _productList[1].Id;
            testmodel.OpenCommand.Execute(new Tuple<Int16?, Int32?>(locId, prodId));

            Assert.Contains("SelectedItem", base.PropertyChangedNotifications);

            //check that selected linked product was updated
            Assert.Contains("SelectedLinkedProduct", base.PropertyChangedNotifications);
            Assert.AreEqual(prodId, testmodel.SelectedLinkedProduct.Id, "Product was not loaded");
            testmodel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SelectedItemTitle_UpdatesOnSelectedItemChange()
        {
            var testmodel = new LocationProductAttributeMaintenanceViewModel();

            base.PropertyChangedNotifications.Clear();
            testmodel.PropertyChanged += base.TestModel_PropertyChanged;

            //open a location product attribute to set selected item
            Int16 locId = _locationInfoListView.Model.First().Id;
            Int32 prodId = _productList[1].Id;
            testmodel.OpenCommand.Execute(new Tuple<Int16?, Int32?>(locId, prodId));

            // check that an item is selected
            Assert.Contains("SelectedItem", base.PropertyChangedNotifications);

            // check that the selected item title changed
            Assert.Contains("SelectedItemTitle", base.PropertyChangedNotifications);
            Assert.IsNotNull(testmodel.SelectedItemTitle,"selected item title did not change");

            testmodel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SelectedItemProduct_UpdatesOnSelectedItemChange()
        {
            var testmodel = new LocationProductAttributeMaintenanceViewModel();

            base.PropertyChangedNotifications.Clear();
            testmodel.PropertyChanged += base.TestModel_PropertyChanged;

            //open a location product attribute to set selected item
            Int16 locId = _locationInfoListView.Model.First().Id;
            Int32 prodId = _productList[1].Id;
            testmodel.OpenCommand.Execute(new Tuple<Int16?, Int32?>(locId, prodId));

            // check that an item is selected
            Assert.Contains("SelectedItem", base.PropertyChangedNotifications);

            // check that the selected item product has changed
            Assert.Contains("SelectedItemProduct", base.PropertyChangedNotifications);
            Assert.AreEqual(prodId, testmodel.SelectedItemProduct.Id, "product was not selected");

            testmodel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SelectedItemLocation_UpdatesOnSelectedItemChange()
        {
            var testmodel = new LocationProductAttributeMaintenanceViewModel();

            base.PropertyChangedNotifications.Clear();
            testmodel.PropertyChanged += base.TestModel_PropertyChanged;

            //open a location product attribute to set selected item
            Int16 locId = _locationInfoListView.Model.First().Id;
            Int32 prodId = _productList[1].Id;
            testmodel.OpenCommand.Execute(new Tuple<Int16?, Int32?>(locId, prodId));
            // check that an item is selected
            Assert.Contains("SelectedItem", base.PropertyChangedNotifications);

            // check that the selected item location has changed
            Assert.Contains("SelectedItemLocation", base.PropertyChangedNotifications);
            Assert.AreEqual(locId, testmodel.SelectedItemLocation.Id, "Location was not selected");

            testmodel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_ProductSearchCriteria()
        {
            String propertyName = "ProductSearchCriteria";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.ProductSearchCriteriaProperty.Path);

            //Test setter
            String setString = "something to search for";
            this.TestModel.ProductSearchCriteria = setString;
            Assert.AreEqual(setString, this.TestModel.ProductSearchCriteria);

            this.TestModel.ProductSearchCriteria = null;
            Assert.AreNotEqual(setString, this.TestModel.ProductSearchCriteria);
        }

        [Test]
        public void Property_SelectedItemTitle()
        {
            String propertyName = "SelectedItemTitle";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.SelectedItemTitleProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //Ensure its not null on startup
            Assert.IsNotNull(this.TestModel.SelectedItemTitle);
        }

        [Test]
        public void Property_SelectedItem()
        {
            String propertyName = "SelectedItem";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.SelectedItemProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_SelectedInfoItem()
        {
            String propertyName = "SelectedInfoItem";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.SelectedInfoItemProperty.Path);

            //Check initial value
            Assert.AreEqual(null, this.TestModel.SelectedInfoItem);
        }

        [Test]
        public void Property_SelectedLinkedProduct()
        {
            String propertyName = "SelectedLinkedProduct";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.SelectedLinkedProductProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_SelectedItemLocation()
        {
            String propertyName = "SelectedItemLocation";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.SelectedItemLocationProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_SelectedItemProduct()
        {
            String propertyName = "SelectedItemProduct";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.SelectedItemProductProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }
        [Test]
        public void Property_IsProcessingProductSearch()
        {
            String propertyName = "IsProcessingProductSearch";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.IsProcessingProductSearchProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
           
            //Check initial value
            Assert.IsFalse(this.TestModel.IsProcessingProductSearch);
        }

        [Test]
        public void Property_IsProcessingLocationProductAttributeMaintenanceSearch()
        {
            String propertyName = "IsProcessingLocationProductAttributeMaintenanceSearch";
            Assert.AreEqual(propertyName, LocationProductAttributeMaintenanceViewModel.IsProcessingLocationProductAttributeMaintenanceSearchProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            //Check initial value
            Assert.IsFalse(this.TestModel.IsProcessingLocationProductAttributeMaintenanceSearch);
        }
        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            // selected item should be null
            Assert.IsNull(this.TestModel.SelectedItem);
            Assert.IsNull(this.TestModel.SelectedItemLocation);
            Assert.IsNull(this.TestModel.SelectedItemProduct);
        }

        #endregion

        #region Commands
        [Test]
        public void Command_Open()
        {
            RelayCommand<Tuple<Int16?, Int32?>> cmd = this.TestModel.OpenCommand;

            //check values
            OpenCommandCheckExpectedValues(cmd);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
           
            //check execut
            Int32 prodId = _productList.First().Id;
            Int16 locId = this.TestModel.AvailableLocations.First().Id;
            
            base.PropertyChangedNotifications.Clear();
            cmd.Execute(new Tuple<Int16?, Int32?>(locId, prodId));

            Assert.AreEqual(locId, this.TestModel.SelectedItem.LocationId, "The item should be the same");
            Assert.AreEqual(prodId, this.TestModel.SelectedItem.ProductId, "The item should be the same");
            Assert.Contains(LocationProductAttributeMaintenanceViewModel.SelectedItemProperty.Path, base.PropertyChangedNotifications);

            //check related properties have updated
            Assert.AreEqual(prodId, this.TestModel.SelectedItemProduct.Id, "The priduct should have updated");
            Assert.Contains(LocationProductAttributeMaintenanceViewModel.SelectedItemProductProperty.Path, base.PropertyChangedNotifications);
           
            Assert.AreEqual(locId, this.TestModel.SelectedItemLocation.Id, "The location should have updated");
            Assert.Contains(LocationProductAttributeMaintenanceViewModel.SelectedItemLocationProperty.Path, base.PropertyChangedNotifications);
            
        }

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check values
            SaveCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as no object");

            //open an item
            Int16 locId = this.TestModel.AvailableLocations.First().Id;
            Int32 prodId = _productList.First().Id;
            this.TestModel.OpenCommand.Execute(new Tuple<Int16?, Int32?>(locId, prodId));

            this.TestModel.SelectedItem.TrayDeep = 1;
            this.TestModel.SelectedItem.TrayHigh = 1;
            this.TestModel.SelectedItem.TrayWide = 1;

            Assert.IsTrue(cmd.CanExecute(), "save enabled as item is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected item has been updated
            Assert.IsFalse(this.TestModel.SelectedItem.IsNew, "The item should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedItem.IsDirty, "The item should have been saved");
            Assert.AreEqual(locId, this.TestModel.SelectedItem.LocationId, "The item should be the same");
            Assert.AreEqual(prodId, this.TestModel.SelectedItem.ProductId, "The item should be the same");
            Assert.Contains(LocationProductAttributeMaintenanceViewModel.SelectedItemProperty.Path, base.PropertyChangedNotifications);
         }

        [Test]
        public void Command_CloseCommand()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", LocationProductAttributeMaintenanceViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check values
            DeleteCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a null item");

            //open an item
            Int16 locId = this.TestModel.AvailableLocations.First().Id;
            Int32 prodId = _productList.First().Id;
            this.TestModel.OpenCommand.Execute(new Tuple<Int16?, Int32?>(locId, prodId));

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new item");

            //this item will be new so save it
            this.TestModel.SaveCommand.Execute();

            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing item");

            //check execute
            cmd.Execute();

            Assert.IsNull(this.TestModel.SelectedItem, "The selected item should now be null");
        }

        #endregion
    }
}
