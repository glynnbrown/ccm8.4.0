﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25451 : N.Haywood
//  Created
// V8-26217 : A.Kuszyk
//  Added tests for SetProductImage and ClearProductImage.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.WorkflowUI.ProductMaintenance
{

    [TestFixture]
    public class ProductMaintenanceViewModelTests : TestBase<ProductMaintenanceViewModel>
    {
        #region Fields
        private ProductDto _productDto;
        private String _imagePath = System.IO.Path.Combine("Environment", "TestImage.png"); 
        #endregion

        #region Test Fixture Helpers

        [SetUp]
        public new void Setup()
        {
            _productDto = Helpers.TestDataHelper.InsertProductDtos(this.DalFactory, 25, 1).First(); ;

            this.TestModel = new ProductMaintenanceViewModel();
            
        }

        #endregion

        #region Properties

        #region Search Properties

        [Test]
        public void Property_ProductSearchCriteria()
        {
            String propertyName = "ProductSearchCriteria";
            Assert.AreEqual(propertyName, ProductMaintenanceViewModel.ProductSearchCriteriaProperty.Path);

            //Test setter
            String setString = Guid.NewGuid().ToString();
            this.TestModel.ProductSearchCriteria = setString;
            Assert.AreEqual(setString, this.TestModel.ProductSearchCriteria);
        }

        [Test]
        public void Property_IsProcessingProductSearch()
        {
            String propertyName = "IsProcessingProductSearch";
            Assert.AreEqual(propertyName, ProductMaintenanceViewModel.IsProcessingProductSearchProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_ProductSearchResults()
        {
            String propertyName = "ProductSearchResults";
            Assert.AreEqual(propertyName, ProductMaintenanceViewModel.ProductSearchResultsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.ProductSearchResults);
        }

        #endregion

        [Test]
        public void Property_SelectedProduct()
        {
            String propertyName = "SelectedProduct";
            Assert.AreEqual(propertyName, ProductMaintenanceViewModel.SelectedProductProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        //[Test]
        //public void Property_AvailableAttributes()
        //{
        //    String propertyName = "AvailableAttributes";
        //    Assert.AreEqual(propertyName, ProductMaintenanceViewModel.AvailableAttributesProperty.Path);

        //    AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        //    AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableAttributes);

        //    Assert.AreEqual(ProductAttributeList.FetchByEntityId(this.EntityId).Count, this.TestModel.AvailableAttributes.Count);
        //}

        //[Test]
        //public void Property_CurrentEditingAttribute()
        //{
        //    String propertyName = "CurrentEditingAttribute";
        //    Assert.AreEqual(propertyName, ProductMaintenanceViewModel.CurrentEditingAttributeProperty.Path);

        //    AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

        //    Assert.IsTrue(this.TestModel.CurrentEditingAttribute.IsNew, "A new item should be loaded");
        //}

        //[Test]
        //public void Property_AttributeValues()
        //{
        //    String propertyName = "AttributeValues";
        //    Assert.AreEqual(propertyName, ProductMaintenanceViewModel.AttributeValuesProperty.Path);

        //    AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AttributeValues);
        //}

        [Test]
        public void Property_MinDeep()
        {
            String propertyName = "MinDeep";
            Assert.AreEqual(propertyName, ProductMaintenanceViewModel.MinDeepProperty.Path);
        }

        [Test]
        public void Property_MaxDeep()
        {
            String propertyName = "MaxDeep";
            Assert.AreEqual(propertyName, ProductMaintenanceViewModel.MaxDeepProperty.Path);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //should start with a new item.
            Assert.IsTrue(this.TestModel.SelectedProduct.IsNew);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;
            Assert.AreEqual("NewCommand", ProductMaintenanceViewModel.NewCommandProperty.Path);

            //check command is registered and value are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            Product preExecuteItem = this.TestModel.SelectedProduct;

            cmd.Execute();

            Assert.Contains(ProductMaintenanceViewModel.SelectedProductProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedProduct, "The item should have changed.");
            Assert.IsTrue(this.TestModel.SelectedProduct.IsNew, "The item should be new");
            Assert.IsTrue(this.TestModel.SelectedProduct.IsInitialized, "The item should be marked initialized");
        }

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;
            Assert.AreEqual("OpenCommand", ProductMaintenanceViewModel.OpenCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            OpenCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();
            Product preExecuteItem = this.TestModel.SelectedProduct;

            ProductInfo loadInfo = ProductInfoList.FetchByEntityId(1).Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedProduct, "Item should have changed");
            Assert.AreEqual(loadInfo.Id, this.TestModel.SelectedProduct.Id, "The location with the info id should ahve been loaded");
            Assert.Contains(ProductMaintenanceViewModel.SelectedProductProperty.Path, base.PropertyChangedNotifications, "Selected item should have change notification");
        }

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;
            Assert.AreEqual("SaveCommand", ProductMaintenanceViewModel.SaveCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedProduct.Name = "st1";
            this.TestModel.SelectedProduct.Gtin = "code1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected store has been updated
            Assert.IsFalse(this.TestModel.SelectedProduct.IsNew, "The store should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedProduct.IsDirty, "The store should have been saved");
            Assert.AreEqual("st1", this.TestModel.SelectedProduct.Name, "The store should be the same");
            Assert.AreNotEqual(0, this.TestModel.SelectedProduct.Id, "The store should have been given a valid id");
            Assert.Contains(ProductMaintenanceViewModel.SelectedProductProperty.Path, base.PropertyChangedNotifications);

            ////check an info has been added for the new item
            //nb cant do this as product search prevents.
            //ProductInfo addedInfo = this.TestModel.AvailableProducts.FirstOrDefault(l => l.Id == this.TestModel.SelectedProduct.Id);
            //Assert.IsNotNull(addedInfo, "An info for the new item should have been added");

            //check another product with the same code cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.SelectedProduct.Name = "st2";
            this.TestModel.SelectedProduct.Gtin = "code1";
            cmd.Execute();
            Assert.AreNotEqual("code1", this.TestModel.SelectedProduct.Gtin);
        }

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;
            Assert.AreEqual("SaveAsCommand", ProductMaintenanceViewModel.SaveAsCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAsCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedProduct.Name = "st1";
            this.TestModel.SelectedProduct.Gtin = "code1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();

            //load an existing item
            ProductInfoList availableInfoList = ProductInfoList.FetchByEntityId(1);

            ProductInfo loadInfo = availableInfoList.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Product preExecuteItem = this.TestModel.SelectedProduct;

            cmd.Execute();

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedProduct, "Item should have changed");
            Assert.AreNotEqual(preExecuteItem.Id, this.TestModel.SelectedProduct.Id, "Id should be different");
            Assert.AreNotEqual(preExecuteItem.Gtin, this.TestModel.SelectedProduct.Gtin, "Code should be different");
            Assert.Contains(ProductMaintenanceViewModel.SelectedProductProperty.Path, base.PropertyChangedNotifications, "should have change notification");

            //check infos are correct
            availableInfoList = ProductInfoList.FetchByEntityId(1);

            ProductInfo oldItemInfo = availableInfoList.FirstOrDefault(l => l.Id == preExecuteItem.Id);
            Assert.IsNotNull(oldItemInfo, "An info for the old item should still exist");

            ProductInfo newItemInfo = availableInfoList.FirstOrDefault(l => l.Id == this.TestModel.SelectedProduct.Id);
            Assert.IsNotNull(newItemInfo, "An info for the new item should have been added");
        }

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;
            Assert.AreEqual("SaveAndNewCommand", ProductMaintenanceViewModel.SaveAndNewCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            //check execution
            base.PropertyChangedNotifications.Clear();
            String locationName = "st1";

            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedProduct.Name = locationName;
            this.TestModel.SelectedProduct.Gtin = "code1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execution
            cmd.Execute();

            IEnumerable<ProductInfo> availableInfoList = ProductInfoList.FetchByEntityId(1);

            Assert.IsTrue(this.TestModel.SelectedProduct.IsNew, "A new item should have been loaded");
            Assert.AreEqual(locationName, availableInfoList.Last().Name, "The saved item should be in the available list");
            Assert.Contains(ProductMaintenanceViewModel.SelectedProductProperty.Path, base.PropertyChangedNotifications, "Selected item should have change notification");
        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;
            Assert.AreEqual("SaveAndCloseCommand", ProductMaintenanceViewModel.SaveAndCloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedProduct.Name = "st1";
            this.TestModel.SelectedProduct.Gtin = "code1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execution
            //nb cannot test the the window closed.
            IEnumerable<ProductInfo> availableInfoList = ProductInfoList.FetchByEntityId(1);
            Int32 preItemCount = availableInfoList.Count();

            cmd.Execute();

            availableInfoList = ProductInfoList.FetchByEntityId(1);
            Assert.AreEqual(preItemCount + 1, availableInfoList.Count());
        }

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new item");

            //add a store and open
            IEnumerable<ProductInfo> availableInfoList = ProductInfoList.FetchByEntityId(1);
            ProductInfo loadInfo = availableInfoList.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing item");

            //check execute
            Int32 oldItemId = this.TestModel.SelectedProduct.Id;
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedProduct.IsNew, "A new item should have been loaded");

            availableInfoList = ProductInfoList.FetchByEntityId(1);
            ProductInfo oldInfo = availableInfoList.FirstOrDefault(l => l.Id == oldItemId);
            Assert.IsNull(oldInfo, "The deleted item should have been removed from the available list");
        }

        //[Test]
        //public void Command_AddProductAttribute()
        //{
        //    RelayCommand cmd = this.TestModel.AddProductAttributeCommand;
        //    Assert.AreEqual("AddProductAttributeCommand", ProductMaintenanceViewModel.AddProductAttributeCommandProperty.Path);
        //    Assert.Contains(cmd, this.TestModel.ViewModelCommands);

        //    Assert.IsNotNull(cmd.FriendlyDescription);
        //    Assert.IsNotNull(cmd.SmallIcon);

        //    //add a test one to start
        //    this.TestModel.CurrentEditingAttribute.Name = "test";
        //    this.TestModel.AddProductAttributeCommand.Execute();

        //    //check can execute
        //    Assert.IsFalse(cmd.CanExecute(), "Should be disabled as attribute has no name");
        //    this.TestModel.CurrentEditingAttribute.Name = this.TestModel.AvailableAttributes.First().Name;
        //    Assert.IsFalse(cmd.CanExecute(), "Should be disabled as attribute name is not unique");
        //    String attributeName = "UniqueName";
        //    this.TestModel.CurrentEditingAttribute.Name = attributeName;
        //    Assert.IsTrue(cmd.CanExecute(), "Should be enabled as name is unique");

        //    //check execution
        //    Byte expectedColNumber = this.TestModel.AvailableAttributes.First().ColumnNumber;
        //    expectedColNumber++;
        //    cmd.Execute();

        //    ProductAttribute availableType = this.TestModel.AvailableAttributes.FirstOrDefault(a => a.Name == attributeName);
        //    Assert.IsNotNull(availableType, "The attribute should have been added to the available list");
        //    Assert.AreEqual(expectedColNumber, availableType.ColumnNumber, "The correct col number should have been allocated");

        //    //check there is a corresponding attribute value
        //    ProductMaintenanceCustomAttribute customAttribute = this.TestModel.AttributeValues.FirstOrDefault(a => a.AttributeName == attributeName);
        //    Assert.IsNotNull(customAttribute, "A corresponding custom attribute should have been added");
        //}

        //[Test]
        //public void Command_RemoveProductAttribute()
        //{
        //    RelayCommand<ProductAttribute> cmd = this.TestModel.RemoveProductAttributeCommand;
        //    Assert.AreEqual("RemoveProductAttributeCommand", ProductMaintenanceViewModel.RemoveProductAttributeCommandProperty.Path);
        //    Assert.Contains(cmd, this.TestModel.ViewModelCommands);

        //    Assert.IsNotNull(cmd.FriendlyDescription);
        //    Assert.IsNotNull(cmd.SmallIcon);

        //    //add an attribute to remove
        //    this.TestModel.CurrentEditingAttribute.Name = "test";
        //    this.TestModel.AddProductAttributeCommand.Execute();


        //    ProductAttribute availableAttribute = this.TestModel.AvailableAttributes.First();

        //    //check can execute
        //    Assert.IsTrue(cmd.CanExecute(availableAttribute));

        //    //check execution
        //    String attributeName = availableAttribute.Name;
        //    cmd.Execute(availableAttribute);

        //    ProductAttribute foundAttribute = this.TestModel.AvailableAttributes.FirstOrDefault(a => a.Name == attributeName);
        //    Assert.IsNull(foundAttribute, "The attribute should have been removed from the available list");

        //    ProductMaintenanceCustomAttribute customAttribute = this.TestModel.AttributeValues.FirstOrDefault(a => a.AttributeName == attributeName);
        //    Assert.IsNull(customAttribute, "The corresponding custom attribute should have been removed");
        //}

        [Test]
        public void Command_CloseCommand()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #region Others

        [Test]
        public void SetProductImage_SetsImageDataWhenSaved()
        {
            TestModel.OpenCommand.Execute(_productDto.Id);
            TestModel.SelectedProduct.Name = "Name";
            TestModel.SelectedProduct.Gtin = "Gtin";


            var productModel = TestModel.SelectedProduct;
            Byte[] originalData;
            using (var fs = new System.IO.FileStream(_imagePath, System.IO.FileMode.Open))
            {
                originalData = new Byte[fs.Length];
                fs.Read(originalData, 0, (Int32)fs.Length);
            }

            TestModel.SetProductImage(
                _imagePath,
                ProductImageType.None,
                ProductImageFacing.Front);
            productModel.Save();
            
            var product = Product.FetchById(productModel.Id);
            Assert.That(product.ImageList.Count > 0);
            var savedData = product.ImageList.First().ProductImageData.OriginalFile.FileBlob.Data;
            CollectionAssert.AreEqual(originalData, savedData);
        }

        [Test]
        public void SetProductImage_ThrowsWhenFileNameNull()
        {
            TestDelegate test = () => TestModel.SetProductImage(null, ProductImageType.Display, ProductImageFacing.Front);
            Assert.Throws<ArgumentOutOfRangeException>(test);
        }

        [Test]
        public void SetProductImage_ThrowsWhenFileNameEmpty()
        {
            TestDelegate test = () => TestModel.SetProductImage(String.Empty, ProductImageType.Display, ProductImageFacing.Front);
            Assert.Throws<ArgumentOutOfRangeException>(test);
        }

        [Test]
        public void ClearProductImage_RemovesImageFromList()
        {
            TestModel.OpenCommand.Execute(_productDto.Id);
            TestModel.SelectedProduct.Name = "Name";
            TestModel.SelectedProduct.Gtin = "Gtin";

            TestModel.SetProductImage(
                _imagePath,
                ProductImageType.None,
                ProductImageFacing.Front);
            TestModel.SelectedProduct.Save();

            TestModel.ClearProductImage(ProductImageType.None, ProductImageFacing.Front);

            var product = Product.FetchById(TestModel.SelectedProduct.Id);
            CollectionAssert.IsEmpty(TestModel.SelectedProduct.ImageList);
        }

        //[Test]
        //public void CheckProductAttributesAreSavedOnExit()
        //{
        //    //add an attribute to edit
        //    this.TestModel.CurrentEditingAttribute.Name = "test";
        //    this.TestModel.AddProductAttributeCommand.Execute();

        //    //edit an existing location type
        //    ProductAttribute item = this.TestModel.AvailableAttributes.FirstOrDefault();

        //    String newName = "EDITED";
        //    item.Name = newName;
        //    Int32 id = item.Id;

        //    //dispose of the viewmodel as if the window has closed.
        //    this.TestModel.Dispose();

        //    //pull back the location type
        //    ProductAttributeList attributeList = ProductAttributeList.FetchByEntityId(this.EntityId);

        //    item = attributeList.FirstOrDefault(t => t.Id == id);
        //    Assert.AreEqual(item.Name, newName);
        //}

        #endregion
    }
}
