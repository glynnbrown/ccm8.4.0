﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using Csla.Core;
using FluentAssertions;
using Galleria.Framework.Collections;
using Galleria.Ccm.Workflow.Client.Wpf;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LocationSpaceMaintenance
{
    [TestFixture]
    public class LocationSpaceAddLocationsViewModelTests : TestBase<LocationSpaceAddLocationsViewModel>
    {
        #region Test Fixture Helpers

        private Entity _entity;

        public override void Setup()
        {
            base.Setup();


            //insert test data
            _entity = Entity.FetchById(this.EntityId);

            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(base.DalFactory, 10, _entity.Id);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            productHierarchy = TestDataHelper.PopulateProductHierarchy(productHierarchy.Id);
            TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 5);

            //create some initial location space's to use
            LocationSpace locationSpace1 = LocationSpace.NewLocationSpace(this.EntityId);
            locationSpace1.LocationId = locationDtos[0].Id;

            //Create new product group record
            LocationSpaceProductGroup newProductGroup = LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            newProductGroup.ProductGroupId = productHierarchy.EnumerateAllGroups().First(p => p.IsRoot == false).Id;
            newProductGroup.BayCount = 15;
            locationSpace1.LocationSpaceProductGroups.Add(newProductGroup);


            //Find group names
            BulkObservableCollection<String> groupNames = new BulkObservableCollection<String>();

            GetGroupNames(productHierarchy, newProductGroup, groupNames);


            //Create new locationSpaceProductGroupRow
            LocationSpaceProductGroupRowViewModel Category1 = new LocationSpaceProductGroupRowViewModel(newProductGroup, groupNames);

            //Save version 1
            locationSpace1 = locationSpace1.Save();

            //create some initial location space's to use
            LocationSpace locationSpace2 = LocationSpace.NewLocationSpace(this.EntityId);
            locationSpace2.LocationId = locationDtos[1].Id;

            //Create new product group record
            LocationSpaceProductGroup newProductGroup2 = LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            newProductGroup2.ProductGroupId = productHierarchy.EnumerateAllGroups().Last(p => p.IsRoot == false).Id;
            newProductGroup2.BayCount = 10;
            locationSpace2.LocationSpaceProductGroups.Add(newProductGroup2);

            //save version 2
            locationSpace2 = locationSpace2.Save();

            BulkObservableCollection<String> groupNames2 = new BulkObservableCollection<String>();

            GetGroupNames(productHierarchy, newProductGroup2, groupNames2);

            //Create new locationSpaceProductGroupRow
            LocationSpaceProductGroupRowViewModel Category2 = new LocationSpaceProductGroupRowViewModel(newProductGroup2, groupNames2);

            ObservableCollection<LocationSpaceProductGroupRowViewModel> locationSpaceCategory = new ObservableBindingList<LocationSpaceProductGroupRowViewModel>();

            locationSpaceCategory.Add(Category1);
            locationSpaceCategory.Add(Category2);


            //Get Location list
            LocationList masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);

            ObservableCollection<Location> selectedLocations = masterLocationList.Take(2).ToObservableCollection();

            this.TestModel = new LocationSpaceAddLocationsViewModel(locationSpaceCategory, masterLocationList, selectedLocations);

        }

        private void GetGroupNames(ProductHierarchy productHierarchy, LocationSpaceProductGroup newProductGroup, BulkObservableCollection<String> groupNames)
        {
            IEnumerable<ProductGroup> groups = productHierarchy.FetchAllGroups();

            //Find assigned group
            ProductGroup group = groups.FirstOrDefault(p => p.Id == newProductGroup.ProductGroupId);

            //If group can be located
            if (@group != null)
            {
                if (!@group.IsRoot)
                {
                    //Add group code/names
                    foreach (ProductGroup parentGroup in @group.GetParentPath().Where(p => p.IsRoot != true))
                    {
                        groupNames.Add(String.Format("{0} : {1}", parentGroup.Code, parentGroup.Name));
                    }
                    //If assigned group is not root as these details too
                    if (!@group.IsRoot)
                    {
                        groupNames.Add(String.Format("{0} : {1}", @group.Code, @group.Name));
                    }

                }
            }
        }


        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {

            //Ensure location list has been populated
            Assert.IsNotNull(this.TestModel.SelectedLocations);
            Assert.IsNotEmpty(this.TestModel.SelectedLocations);

            //Ensure selected location space category has been populated
            Assert.IsNotNull(this.TestModel.SelectedLocationSpaceCategory);

            //Ensure selected location has been populated
            Assert.IsNotNull(this.TestModel.MasterLocationList);
            Assert.IsNotEmpty(this.TestModel.MasterLocationList);

            //Ensure other collections/objects are not null
            Assert.IsNotNull(this.TestModel.AvailableLocations);
        }

        #endregion

        #region AddLocationCommands

        [Test]
        public void Command_AddLocations()
        {
            // Arrange
            IRelayCommand command = TestModel.AddLocationsCommand;
            ObservableCollection<IRelayCommand> modelCommands = TestModel.ViewModelCommands;

            // Act

            // Assert
            modelCommands.Should()
                .Contain(command,
                    because:
                        $"the command {nameof(TestModel.AddLocationsCommand)} should have been registered with the view model's commands");
        }

        [Test]
        public void AddLocationsCommandIsEnableGivenLocationsWhereSelected()
        {
            //Arrange
            TestModel.SelectedAvailableLocations = TestModel.MasterLocationList.Take(3).ToObservableCollection();

            //Act
            TestModel.AddLocationsCommand.CanExecute();

            //Assert
            Boolean canExecute = TestModel.AddLocationsCommand.CanExecute();
            canExecute.Should().BeTrue(because: "the command should be enabled when a location is selected");
        }


        [Test]
        public void AddLocationsCommandIsDisableGivenNoLocationWasSelected()
        {
            //Arrange
            TestModel.SelectedLocations.Clear();

            //Act
            TestModel.AddLocationsCommand.CanExecute();

            //Assert
            Boolean canExecute = TestModel.AddLocationsCommand.CanExecute();
            canExecute.Should().BeFalse(because: "the command should be disable when no location has been selected");
        }

        [Test]
        public void AddLocationsCommandAddsLocationsWhenExecuted()
        {
            //Arrange
            TestModel.SelectedLocations.Clear();
            TestModel.SelectedAvailableLocations = TestModel.MasterLocationList.Take(3).ToObservableCollection();

            //Act
            TestModel.AddLocationsCommand.Execute();

            //Assert
            TestModel.SelectedLocations.Count()
                .Should()
                .Be(3, because: "Added Locations should be 3 after 3 locations has been added");
        }

        #endregion

        #region RemoveLocationCommands

        [Test]
        public void Command_RemoveLocations()
        {
            // Arrange
            IRelayCommand command = TestModel.RemoveLocationCommand;
            ObservableCollection<IRelayCommand> modelCommands = TestModel.ViewModelCommands;

            // Act

            // Assert
            modelCommands.Should()
                .Contain(command,
                    because:
                        $"the command {nameof(TestModel.RemoveLocationCommand)} should have been registered with the view model's commands");
        }

        [Test]
        public void RemoveLocationsCommandIsEnableGivenLocationsWhereSelected()
        {
            //Arrange
            TestModel.SelectedLocationsToRemove = TestModel.SelectedLocations.Take(1).ToObservableCollection();

            //Act
            TestModel.RemoveLocationCommand.CanExecute();

            //Assert
            Boolean canExecute = TestModel.RemoveLocationCommand.CanExecute();
            canExecute.Should().BeTrue(because: "the command should be enabled when a location is selected");
        }


        [Test]
        public void RemoveLocationsCommandIsDisableGivenNoLocationWasSelected()
        {
            //Arrange
            TestModel.SelectedLocationsToRemove.Clear();

            //Act
            TestModel.RemoveLocationCommand.CanExecute();

            //Assert
            Boolean canExecute = TestModel.RemoveLocationCommand.CanExecute();
            canExecute.Should().BeFalse(because: "the command should be disable when no location has been selected");
        }

        [Test]
        public void RemoveLocationsCommandRemovesLocationsWhenExecuted()
        {
            //Arrange
            TestModel.SelectedLocations.Clear();
            TestModel.SelectedAvailableLocations = TestModel.MasterLocationList.Take(3).ToObservableCollection();
            TestModel.AddLocationsCommand.Execute();

            TestModel.SelectedLocationsToRemove = TestModel.SelectedLocations.Take(1).ToObservableCollection();

            //Act
            TestModel.RemoveLocationCommand.Execute();

            //Assert
            TestModel.SelectedLocations.Count()
                .Should()
                .Be(2, because: "Remaining Locations should be 2 as one was removed");
        }

        #endregion
    }
}
