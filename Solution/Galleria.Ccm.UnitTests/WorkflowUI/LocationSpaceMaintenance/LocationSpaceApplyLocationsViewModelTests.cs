﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using Csla.Core;
using FluentAssertions;
using Galleria.Framework.Collections;
using Galleria.Ccm.Workflow.Client.Wpf;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LocationSpaceMaintenance
{
    [TestFixture]
    public class LocationSpaceApplyLocationsViewModelTests : TestBase<LocationSpaceApplyLocationsViewModel>
    {
        #region Test Fixture Helpers

        private Entity _entity;

        public override void Setup()
        {
            base.Setup();


            //insert test data
            _entity = Entity.FetchById(this.EntityId);

            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(base.DalFactory, 10, _entity.Id);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(this.EntityId);
            productHierarchy = TestDataHelper.PopulateProductHierarchy(productHierarchy.Id);
            TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 5);

            //create some initial location space's to use
            LocationSpace locationSpace1 = LocationSpace.NewLocationSpace(this.EntityId);
            locationSpace1.LocationId = locationDtos[0].Id;

            //Create new product group record
            LocationSpaceProductGroup newProductGroup = LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            newProductGroup.ProductGroupId = productHierarchy.EnumerateAllGroups().First(p => p.IsRoot == false).Id;
            newProductGroup.BayCount = 15;
            locationSpace1.LocationSpaceProductGroups.Add(newProductGroup);


            //Find group names
            BulkObservableCollection<String> groupNames = new BulkObservableCollection<String>();

            GetGroupNames(productHierarchy, newProductGroup, groupNames);


            //Create new locationSpaceProductGroupRow
            LocationSpaceProductGroupRowViewModel Category1 = new LocationSpaceProductGroupRowViewModel(newProductGroup, groupNames);

            //Save version 1
            locationSpace1 = locationSpace1.Save();

            //create some initial location space's to use
            LocationSpace locationSpace2 = LocationSpace.NewLocationSpace(this.EntityId);
            locationSpace2.LocationId = locationDtos[1].Id;

            //Create new product group record
            LocationSpaceProductGroup newProductGroup2 = LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            newProductGroup2.ProductGroupId = productHierarchy.EnumerateAllGroups().Last(p => p.IsRoot == false).Id;
            newProductGroup2.BayCount = 10;
            locationSpace2.LocationSpaceProductGroups.Add(newProductGroup2);

            //save version 2
            locationSpace2 = locationSpace2.Save();

            BulkObservableCollection<String> groupNames2 = new BulkObservableCollection<String>();

            GetGroupNames(productHierarchy, newProductGroup2, groupNames2);

            //Create new locationSpaceProductGroupRow
            LocationSpaceProductGroupRowViewModel Category2 = new LocationSpaceProductGroupRowViewModel(newProductGroup2, groupNames2);

            ObservableCollection<LocationSpaceProductGroupRowViewModel> locationSpaceCategory = new ObservableBindingList<LocationSpaceProductGroupRowViewModel>();

            locationSpaceCategory.Add(Category1);
            locationSpaceCategory.Add(Category2);


            //Get Location list
            LocationList masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);

            ObservableCollection<Location> selectedLocations = masterLocationList.Take(2).ToObservableCollection();

            this.TestModel = new LocationSpaceApplyLocationsViewModel(locationSpaceCategory, masterLocationList, selectedLocations);

        }

        private void GetGroupNames(ProductHierarchy productHierarchy, LocationSpaceProductGroup newProductGroup, BulkObservableCollection<String> groupNames)
        {
            IEnumerable<ProductGroup> groups = productHierarchy.FetchAllGroups();

            //Find assigned group
            ProductGroup group = groups.FirstOrDefault(p => p.Id == newProductGroup.ProductGroupId);

            //If group can be located
            if (@group != null)
            {
                if (!@group.IsRoot)
                {
                    //Add group code/names
                    foreach (ProductGroup parentGroup in @group.GetParentPath().Where(p => p.IsRoot != true))
                    {
                        groupNames.Add(String.Format("{0} : {1}", parentGroup.Code, parentGroup.Name));
                    }
                    //If assigned group is not root as these details too
                    if (!@group.IsRoot)
                    {
                        groupNames.Add(String.Format("{0} : {1}", @group.Code, @group.Name));
                    }

                }
            }
        }


        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {

            //Ensure location list has been populated
            Assert.IsNotNull(this.TestModel.AddedLocations);
            Assert.IsNotEmpty(this.TestModel.AddedLocations);

            //Ensure selected location space category has been populated
            Assert.IsNotNull(this.TestModel.SelectedLocationSpaceCategory);

            //Ensure selected location has been populated
            Assert.IsNotNull(this.TestModel.MasterLocationList);
            Assert.IsNotEmpty(this.TestModel.MasterLocationList);


            //Ensure other collections/objects are not null
            Assert.IsNotNull(this.TestModel.AllLocationSpaceInfo);

        }

        #endregion

        #region Methods

        [Test]
        public void StoreFixtureCreateAsNewShouldBePopulatedGivenSelectedCategoriesAreNotPresentInStore()
        {
            //Arrange
            TestModel.AddedLocations.Clear();
            TestModel.AddedLocations = TestModel.MasterLocationList.Take(2).ToObservableCollection();
            TestModel.SelectedLocationSpaceCategory[0].LocationSpaceProductGroup.ProductGroupId = 100;
            TestModel.SelectedLocationSpaceCategory[1].LocationSpaceProductGroup.ProductGroupId = 100;

            //Act
            TestModel.CreateStoreFixtureRelationships();

            //Assert
            TestModel.StoreFixtureCreateAsNew.Count().Should().Be(4, because: "the two categories shouldn't match any existing category in each of the locations as they do not have ProductGroupId 100");
        }


        [Test]
        public void StoreFixtureOverWriteShouldBePopulatedGivenSelectedCategoriesArePresentInStore()
        {
            //Arrange
            TestModel.AddedLocations.Clear();
            TestModel.AddedLocations = TestModel.MasterLocationList.Take(2).ToObservableCollection();

            //Act
            TestModel.CreateStoreFixtureRelationships();

            //Assert
            TestModel.StoreFixtureOverwrite.Count().Should().Be(2, because: "one category should be present in each Location");
        }


        [Test]
        public void AllLocationSpaceTobeCreatedAsNewShouldBePopulatedGivenSelectedCategoriesAreNotPresentInStore()
        {
            //Arrange
            TestModel.AddedLocations.Clear();
            TestModel.AddedLocations = TestModel.MasterLocationList.Take(2).ToObservableCollection();
            TestModel.SelectedLocationSpaceCategory[0].LocationSpaceProductGroup.ProductGroupId = 100;
            TestModel.SelectedLocationSpaceCategory[1].LocationSpaceProductGroup.ProductGroupId = 100;

            //Act
            TestModel.CreateStoreFixtureRelationships();

            //Assert
            TestModel.AllLocationSpaceToBeCreatedAsNew.Count().Should().Be(2, because: "2 locations will be edited as there were 2 locations selected which do not have the selected categories");
        }


        [Test]
        public void AllLocationSpaceTobeOverwrittenShouldBePopulatedGivenSelectedCategoriesArePresentInStore()
        {
            //Arrange
            TestModel.AddedLocations.Clear();
            TestModel.AddedLocations = TestModel.MasterLocationList.Take(2).ToObservableCollection();

            //Act
            TestModel.CreateStoreFixtureRelationships();

            //Assert
            TestModel.AllLocationSpaceToBeOverwritten.Count().Should().Be(2, because: "2 locations will be edited as there were 2 locations selected which do have the selected categories");
        }



        [Test]
        public void ApplyFixtureRelationShipsShouldAddNewCategories()
        {
            //Arrange
            TestModel.AddedLocations.Clear();
            TestModel.AddedLocations = TestModel.MasterLocationList.Take(2).ToObservableCollection();
            TestModel.SelectedLocationSpaceCategory[0].LocationSpaceProductGroup.ProductGroupId = 100;
            TestModel.SelectedLocationSpaceCategory[1].LocationSpaceProductGroup.ProductGroupId = 102;

            //Act
            TestModel.CreateStoreFixtureRelationships();
            TestModel.AllLocationSpaceToBeCreatedAsNew[0].LocationSpaceProductGroups.Count.Should().Be(1, because: "Initilally the location should have one category in product Groups");
            TestModel.AllLocationSpaceToBeCreatedAsNew[1].LocationSpaceProductGroups.Count.Should().Be(1, because: "Initilally the location should have one category in product Groups");

            //Act
            TestModel.ApplyStoreFixtureRelationships();

            //Assert
            TestModel.AllLocationSpaceToBeCreatedAsNew[0].LocationSpaceProductGroups.Count.Should()
                .Be(3, because: "Two more categories should have been added");
            TestModel.AllLocationSpaceToBeCreatedAsNew[1].LocationSpaceProductGroups.Count.Should()
             .Be(3, because: "Two more categories should have been added");

        }


        [Test]
        public void ApplyFixtureRelationShipsShouldOverwriteExistingCategories()
        {
            //Arrange
            TestModel.AddedLocations.Clear();
            TestModel.AddedLocations = TestModel.MasterLocationList.Take(1).ToObservableCollection();

            //Act
            TestModel.CreateStoreFixtureRelationships();
            TestModel.AllLocationSpaceToBeOverwritten[0].LocationSpaceProductGroups.Count.Should().Be(1, because: "Initilally the location should have one category in product Groups");

            //Act
            TestModel.ApplyStoreFixtureRelationships();

            //Assert
            TestModel.AllLocationSpaceToBeOverwritten[0].LocationSpaceProductGroups.Count.Should()
                .Be(1, because: "The same category should be overwritten");

        }
    }
    #endregion 
}
