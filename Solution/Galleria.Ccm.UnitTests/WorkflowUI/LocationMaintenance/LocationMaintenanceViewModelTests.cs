﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LocationMaintenance
{
    [TestFixture]
    public class LocationMaintenanceViewModelTests : TestBase<LocationMaintenanceViewModel>
    {
        #region Test Fixture Helpers

        private const Int32 _numLocationsInserted = 10;

        public override void  Setup()
        {
            base.Setup();


            Helpers.TestDataHelper.InsertLocationDtos(this.DalFactory, _numLocationsInserted, 1);

            this.TestModel = new LocationMaintenanceViewModel();
        }


        #endregion

        #region Properties

        [Test]
        public void Property_AvailableLocations()
        {
            String propertyName = "AvailableLocations";
            Assert.AreEqual(propertyName, LocationMaintenanceViewModel.AvailableLocationsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.AvailableLocations);

            LocationInfoList locInfos = LocationInfoList.FetchByEntityId(this.EntityId);
            Assert.AreEqual(locInfos.Count, this.TestModel.AvailableLocations.Count);
        }

        [Test]
        public void Property_SelectedLocation()
        {
            String propertyName = "SelectedLocation";
            Assert.AreEqual(propertyName, LocationMaintenanceViewModel.SelectedLocationProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_SelectedLocationGroup()
        {
            String propertyName = "SelectedLocationGroup";
            Assert.AreEqual(propertyName, LocationMaintenanceViewModel.SelectedLocationGroupProperty.Path);
        }

        [Test]
        public void Property_ManagerEmail()
        {
            String propertyName = "ManagerEmail";
            Assert.AreEqual(propertyName, LocationMaintenanceViewModel.ManagerEmailProperty.Path);
        }

        [Test]
        public void Property_RegionalManagerEmail()
        {
            String propertyName = "RegionalManagerEmail";
            Assert.AreEqual(propertyName, LocationMaintenanceViewModel.RegionalManagerEmailProperty.Path);
        }

        [Test]
        public void Property_DivisionalManagerEmail()
        {
            String propertyName = "DivisionalManagerEmail";
            Assert.AreEqual(propertyName, LocationMaintenanceViewModel.DivisionalManagerEmailProperty.Path);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {

            this.TestModel = new LocationMaintenanceViewModel();

            //we should start with a new store
            Assert.IsTrue(this.TestModel.SelectedLocation.IsNew);

            //the info list model should also contain existing stores
            Assert.AreEqual(_numLocationsInserted, this.TestModel.AvailableLocations.Count());
        }

        #endregion

        #region Commands

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;

            //check command is registered and value are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            Location preExecuteItem = this.TestModel.SelectedLocation;

            cmd.Execute();

            Assert.Contains(LocationMaintenanceViewModel.SelectedLocationProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedLocation, "The item should have changed.");
            Assert.IsTrue(this.TestModel.SelectedLocation.IsNew, "The item should be new");
            Assert.IsTrue(this.TestModel.SelectedLocation.IsInitialized, "The item should be marked initialized");

            Assert.AreEqual(0, this.TestModel.SelectedLocation.LocationGroupId, "No location group should have been assigned.");
        }

        [Test]
        public void Command_Open()
        {
            RelayCommand<Int16?> cmd = this.TestModel.OpenCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            OpenCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            //check execute
            base.PropertyChangedNotifications.Clear();
            Location preExecuteItem = this.TestModel.SelectedLocation;

            LocationInfo loadInfo = this.TestModel.AvailableLocations.Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedLocation, "Item should have changed");
            Assert.AreEqual(loadInfo.Id, this.TestModel.SelectedLocation.Id, "The location with the info id should ahve been loaded");
            Assert.Contains(LocationMaintenanceViewModel.SelectedLocationProperty.Path, base.PropertyChangedNotifications, "SelectedStore should have change notification");
        }

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedLocation.Name = "st1";
            this.TestModel.SelectedLocation.Code = "code1";
            LocationGroupInfo newLocationGroup = LocationGroupInfo.NewLocationGroupInfo(LocationHierarchy.FetchByEntityId(this.EntityId).FetchAllGroups().Last());
            this.TestModel.SelectedLocationGroup = newLocationGroup;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected store has been updated
            Assert.IsFalse(this.TestModel.SelectedLocation.IsNew, "The store should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedLocation.IsDirty, "The store should have been saved");
            Assert.AreEqual("st1", this.TestModel.SelectedLocation.Name, "The store should be the same");
            Assert.AreNotEqual(0, this.TestModel.SelectedLocation.Id, "The store should have been given a valid id");
            Assert.AreEqual(newLocationGroup.Id, this.TestModel.SelectedLocationGroup.Id);
            Assert.Contains(LocationMaintenanceViewModel.SelectedLocationProperty.Path, base.PropertyChangedNotifications);

            //check an info has been added for the new store
            LocationInfo addedInfo = this.TestModel.AvailableLocations.FirstOrDefault(l => l.Id == this.TestModel.SelectedLocation.Id);
            Assert.IsNotNull(addedInfo, "An info for the new store should have been added");

            //check another store with the same code cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.SelectedLocation.Name = "st2";
            this.TestModel.SelectedLocation.Code = "code1";
            this.TestModel.SelectedLocationGroup = newLocationGroup;
            cmd.Execute();
            Assert.AreNotEqual("code1", this.TestModel.SelectedLocation.Code);
        }

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAsCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedLocation.Name = "st1";
            this.TestModel.SelectedLocation.Code = "code1";
            LocationGroupInfo newLocationGroup = LocationGroupInfo.NewLocationGroupInfo(LocationHierarchy.FetchByEntityId(this.EntityId).FetchAllGroups().Last());
            this.TestModel.SelectedLocationGroup = newLocationGroup;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();

            //load an existing item
            LocationInfo loadInfo = this.TestModel.AvailableLocations.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Location preExecuteItem = this.TestModel.SelectedLocation;

            cmd.Execute();

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedLocation, "Item should have changed");
            Assert.AreNotEqual(preExecuteItem.Id, this.TestModel.SelectedLocation.Id, "Id should be different");
            Assert.AreNotEqual(preExecuteItem.Code, this.TestModel.SelectedLocation.Code, "Code should be different");
            Assert.Contains(LocationMaintenanceViewModel.SelectedLocationProperty.Path, base.PropertyChangedNotifications, "should have change notification");

            //check infos are correct
            LocationInfo oldItemInfo = this.TestModel.AvailableLocations.FirstOrDefault(l => l.Id == preExecuteItem.Id);
            Assert.IsNotNull(oldItemInfo, "An info for the old item should still exist");

            LocationInfo newItemInfo = this.TestModel.AvailableLocations.FirstOrDefault(l => l.Id == this.TestModel.SelectedLocation.Id);
            Assert.IsNotNull(newItemInfo, "An info for the new item should have been added");
        }

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            //check execution
            base.PropertyChangedNotifications.Clear();
            String locationName = "st1";

            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedLocation.Name = locationName;
            this.TestModel.SelectedLocation.Code = "code1";
            LocationGroupInfo newLocationGroup = LocationGroupInfo.NewLocationGroupInfo(LocationHierarchy.FetchByEntityId(this.EntityId).FetchAllGroups().Last());
            this.TestModel.SelectedLocationGroup = newLocationGroup;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execution
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedLocation.IsNew, "A new store should have been loaded");
            Assert.AreEqual(locationName, this.TestModel.AvailableLocations.Last().Name, "The saved store should be in the available list");
            Assert.Contains(LocationMaintenanceViewModel.SelectedLocationProperty.Path, base.PropertyChangedNotifications, "SelectedStore should have change notification");
        }

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedLocation.Name = "st1";
            this.TestModel.SelectedLocation.Code = "code1";
            LocationGroupInfo newLocationGroup = LocationGroupInfo.NewLocationGroupInfo(LocationHierarchy.FetchByEntityId(this.EntityId).FetchAllGroups().Last());
            this.TestModel.SelectedLocationGroup = newLocationGroup;
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execution
            //nb cannot test the the window closed.
            Int32 preLocationCount = this.TestModel.AvailableLocations.Count;

            cmd.Execute();

            Assert.AreEqual(preLocationCount + 1, this.TestModel.AvailableLocations.Count);
        }

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = this.TestModel.DeleteCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new store");

            //add a store and open
            LocationInfo loadInfo = this.TestModel.AvailableLocations.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing store");

            //check execute
            Int16 oldLocationId = this.TestModel.SelectedLocation.Id;
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedLocation.IsNew, "A new store should have been loaded");

            LocationInfo oldInfo = this.TestModel.AvailableLocations.FirstOrDefault(l => l.Id == oldLocationId);
            Assert.IsNull(oldInfo, "The deleted store should have been removed from the available list");


            Location oldLocation = Location.FetchById(oldLocationId);
            Assert.IsNotNull(oldLocation.DateDeleted, "Should be marked deleted");
        }

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", LocationMaintenanceViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_SelectLocationGroup()
        {
            RelayCommand cmd = this.TestModel.SelectLocationGroupCommand;
            Assert.AreEqual("SelectLocationGroupCommand", LocationMaintenanceViewModel.SelectLocationGroupCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //opens new window
        }

        #endregion

        #region Others

        [Test]
        public void TestIsEmailValid()
        {
            String validEmail = "valid@email.org";
            String inValidEmail1 = "invalid.email.com";
            String inValidEmail2 = "invalid.@email.com";
            String inValidEmail3 = "this isnot\avalid@email.com";
            String inValidEmail4 = "ab(c)d,e:f;g<h>i[jk]l@email.com";
            String inValidEmail5 = "Abc..123@email.com";

            Assert.IsTrue(this.TestModel.IsEmailValid(validEmail));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail1));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail2));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail3));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail4));
            Assert.IsFalse(this.TestModel.IsEmailValid(inValidEmail5));
        }

        #endregion
    }
}
