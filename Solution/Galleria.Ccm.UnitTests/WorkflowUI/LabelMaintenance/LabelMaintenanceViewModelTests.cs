﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27940 : L.Luong 
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LabelMaintenance;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.WorkflowUI.LabelMaintenance
{
    [TestFixture]
    public class LabelMaintenanceViewModelTests : TestBase<LabelMaintenanceViewModel>
    {
        #region Test Fixture Helpers
        
        private LabelMaintenanceViewModel _labelMaintenanceViewModel;

        public override void Setup()
        {
            base.Setup();

            Helpers.TestDataHelper.InsertLabelDtos(this.DalFactory, 5, 1);

            _labelMaintenanceViewModel = new LabelMaintenanceViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_SelectedLabel()
        {
            const string expected = "SelectedLabel";

            var result = LabelMaintenanceViewModel.SelectedLabelProperty.Path;

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Property_AvailableLabels()
        {
            const String expected = "AvailableLabels";

            var result = LabelMaintenanceViewModel.AvailableLabelsProperty.Path;

            Assert.AreEqual(expected, result);
            AssertHelper.AssertPropertyIsReadOnly(_labelMaintenanceViewModel.GetType(), expected);
        }

        [Test]
        public void AvailableFontsProperty()
        {
            Assert.IsNotEmpty(_labelMaintenanceViewModel.AvailableFonts);
        }

        [Test]
        public void DisplayTextProperty()
        {
            _labelMaintenanceViewModel.SelectedLabel.Text = "TestText";

            Assert.AreEqual("TestText", _labelMaintenanceViewModel.DisplayText);
        }

        #endregion

        #region Commands

        #region NewCommand

        [Test]
        public void NewCommand_Executed()
        {
            // new command
            var cmd = _labelMaintenanceViewModel.NewCommand;

            // new command exists in view model
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);

            // new command should able to be excuted
            Assert.IsTrue(cmd.CanExecute());

            // selected label should not be declared
            var notExpected = _labelMaintenanceViewModel.SelectedLabel;

            // execute command
            cmd.Execute();
            var result = _labelMaintenanceViewModel.SelectedLabel;

            // check results
            Assert.AreNotEqual(notExpected, result, "The model was not changed after executing New Command.");
            Assert.IsTrue(result.IsNew, "The model was not marked as new after executing New Command.");
        }

        #endregion

        #region OpenCommand

        [Test]
        public void OpenCommand_Executed()
        {
            // open command
            var cmd = _labelMaintenanceViewModel.OpenCommand;

            // Open command exists in view model
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            OpenCommandCheckExpectedValues(cmd);

            // check CanExecute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            // selected label should not be declared
            Label preExecuteItem = _labelMaintenanceViewModel.SelectedLabel;

            // execute command
            LabelInfo loadInfo = _labelMaintenanceViewModel.AvailableLabels.Last();
            cmd.Execute(loadInfo.Id);

            // check results
            Assert.AreNotEqual(preExecuteItem, _labelMaintenanceViewModel.SelectedLabel, "The model was not changed after executing Open Command.");
            Assert.AreEqual(loadInfo.Id, _labelMaintenanceViewModel.SelectedLabel.Id, "The label with the info id should have been loaded");
        }

        #endregion

        #region SaveCommand

        [Test]
        public void SaveCommand_Executed()
        {
            Int32 labelCount = _labelMaintenanceViewModel.AvailableLabels.Count;

            // save command
            var cmd = _labelMaintenanceViewModel.SaveCommand;

            // save command exists in view model
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            // check CanExecute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            _labelMaintenanceViewModel.SelectedLabel.Name = "testLabel";
            Assert.IsTrue(cmd.CanExecute(), "Save should be valid");

            // execute command
            cmd.Execute();
            var result = _labelMaintenanceViewModel.SelectedLabel;

            // check results
            Assert.IsFalse(result.IsNew, "The label should no longer be new");
            Assert.IsFalse(result.IsDirty, "The label should have been saved");
            Assert.AreEqual("testLabel", result.Name, "The label should be the same");
            Assert.AreNotEqual(0, result.Id, "The label should have been given a valid id");
            Assert.AreEqual(labelCount + 1, _labelMaintenanceViewModel.AvailableLabels.Count, "The labels available should have increased");
            
        }

        #endregion

        #region SaveAsCommand

        [Test]
        public void SaveAsCommand_Executed()
        {
            // saveAs command
            var cmd = _labelMaintenanceViewModel.SaveAsCommand;

            // saveAs command exists in view model
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            SaveAsCommandCheckExpectedValues(cmd);

            // check CanExecute
            Assert.IsTrue(cmd.CanExecute(), "Save should be valid");

            // open a label command
            LabelInfo loadInfo = _labelMaintenanceViewModel.AvailableLabels.First();
            _labelMaintenanceViewModel.OpenCommand.Execute(loadInfo.Id);
            Label openedLabel = _labelMaintenanceViewModel.SelectedLabel;
            String openedLabelName = openedLabel.Name;

            // new name for new label
            _labelMaintenanceViewModel.SelectedLabel.Name = "testLabel";

            // execute command
            cmd.Execute();
            Label result = _labelMaintenanceViewModel.SelectedLabel;

            // the two labels should be different now
            Assert.AreNotEqual(openedLabel, result, "Item should have changed");
            Assert.AreNotEqual(openedLabel.Id, result.Id, "Id should be different");

            //check infos are correct
            LabelInfo oldLabelInfo = _labelMaintenanceViewModel.AvailableLabels.First();
            Assert.IsNotNull(oldLabelInfo, "An info for the old item should still exist");
            Assert.AreEqual(openedLabel.Id, oldLabelInfo.Id, "Id should equal");
            Assert.AreEqual(openedLabelName, oldLabelInfo.Name, "Name should equal");

            LabelInfo newLabelInfo = _labelMaintenanceViewModel.AvailableLabels.Last();
            Assert.IsNotNull(newLabelInfo, "An info for the new item should have been added");
            Assert.AreEqual(result.Id, newLabelInfo.Id, "Id should equal");
            Assert.AreEqual(result.Name, newLabelInfo.Name, "Name should equal");
        }

        #endregion

        #region SaveAndNewCommand

        [Test]
        public void SaveAndNewCommand_Executed()
        {
            Int32 labelCount = _labelMaintenanceViewModel.AvailableLabels.Count;

            // save command
            var cmd = _labelMaintenanceViewModel.SaveAndNewCommand;

            // save command exists in view model
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            // check CanExecute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            _labelMaintenanceViewModel.SelectedLabel.Name = "testLabel";
            Assert.IsTrue(cmd.CanExecute(), "Save should be valid");

            // execute command
            cmd.Execute();
            var result = _labelMaintenanceViewModel.SelectedLabel;

            // check that it has loaded new label
            Assert.IsTrue(result.IsNew, "The selected label opened as new label");
            Assert.AreEqual("", result.Name, "The selected label name should be blank");
            Assert.AreEqual(labelCount + 1, _labelMaintenanceViewModel.AvailableLabels.Count, "The labels available should have increased");

            // check that label save correctly
            LabelInfo newLabelInfo = _labelMaintenanceViewModel.AvailableLabels.Last();
            Assert.IsNotNull(newLabelInfo, "An info for the new item should have been added");
            Assert.AreEqual("testLabel", newLabelInfo.Name, "Name should equal");

        }
        
        #endregion

        #region SaveAndCloseCommand

        [Test]
        public void SaveAndCloseCommand_Executed()
        {
            Int32 labelCount = _labelMaintenanceViewModel.AvailableLabels.Count;

            // save command
            var cmd = _labelMaintenanceViewModel.SaveAndCloseCommand;

            // save command exists in view model
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            // check CanExecute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            _labelMaintenanceViewModel.SelectedLabel.Name = "testLabel";
            Assert.IsTrue(cmd.CanExecute(), "Save should be valid");

            // execute command
            cmd.Execute();

            // check that label save correctly
            // Cannot check if he window closed
            LabelInfo newLabelInfo = _labelMaintenanceViewModel.AvailableLabels.Last();
            Assert.IsNotNull(newLabelInfo, "An info for the new item should have been added");
            Assert.AreEqual("testLabel", newLabelInfo.Name, "Name should equal");

        }

        #endregion

        #region DeleteCommand

        [Test]
        public void Command_Delete()
        {
            RelayCommand cmd = _labelMaintenanceViewModel.DeleteCommand;

            // save command exists in view model
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            // check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new label");

            // add a store and open
            LabelInfo loadInfo = _labelMaintenanceViewModel.AvailableLabels.Last();
            _labelMaintenanceViewModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing label");

            //  execute
            Object deletedLabelId = _labelMaintenanceViewModel.SelectedLabel.Id;
            cmd.Execute();

            // check execute
            Assert.IsTrue(_labelMaintenanceViewModel.SelectedLabel.IsNew, "A new label should have been loaded");
            Assert.AreEqual("", _labelMaintenanceViewModel.SelectedLabel.Name, "The new label name should be blank");

            LabelInfo deletedInfo = _labelMaintenanceViewModel.AvailableLabels.FirstOrDefault(l => l.Id == deletedLabelId);
            Assert.IsNull(deletedInfo, "The deleted store should have been removed from the available list");

            Label deletedLabel = Label.FetchById(deletedLabelId);
            Assert.IsNotNull(deletedLabel.DateDeleted, "Should be marked deleted");
        }

        #endregion

        #region CloseCommand

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = _labelMaintenanceViewModel.CloseCommand;

            //check command is registered
            Assert.Contains(cmd, _labelMaintenanceViewModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #endregion
    }
}
