﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26950 : A.Kuszyk
//  Created.
#endregion
#region Version History: (CCM 811)
// V8-30486 : M.Pettit
//  Added parent WorkpackagePlanogram for each parameter
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard;
using Galleria.Ccm.Model;
using Galleria.Ccm.Engine;
using Moq;

namespace Galleria.Ccm.UnitTests.WorkflowUI.WorkPackages
{
    [TestFixture]
    public class WorkpackageWizardParameterTests : TestBase
    {
        private IEnumerable<TaskParameterType> _parameterTypes
        {
            get { return Enum.GetValues(typeof(TaskParameterType)).Cast<TaskParameterType>(); }
        }

        [Test]
        [TestCaseSource("_parameterTypes")]
        public void SetValuesFromContentLookup_SetsMatchingValue(TaskParameterType parameterType)
        {
            var testParameter = GetTestParameter(parameterType);
            var expectedString = parameterType.ToString();
            var contentLookup = Galleria.Ccm.Model.ContentLookup.NewContentLookup(1);
            var property = typeof(Galleria.Ccm.Model.ContentLookup).GetProperty(expectedString.TrimEnd("Single".ToCharArray()));
            if (property == null)
            {
                Assert.Pass();
                return;
            }
            property.SetValue(contentLookup, expectedString, null);

            testParameter.SetValuesFromContentLookup(contentLookup);

            Assert.AreEqual(expectedString, testParameter.Value);
        }

        private WorkpackageWizardParameter GetTestParameter(TaskParameterType parameterType)
        {
            var engineTaskInfo = EngineTaskInfo.GetEngineTaskInfo(typeof(Galleria.Ccm.Engine.Tasks.GetBlockingByName.v1.Task).AssemblyQualifiedName);
            var workflowTask = WorkflowTask.NewWorkflowTask(engineTaskInfo, 0);
            var taskParameter = new TaskParameter(1, String.Empty, String.Empty, String.Empty, parameterType, String.Empty, false, false);
            var engineTaskParameterInfo = EngineTaskParameterInfo.GetEngineTaskParameterInfo(taskParameter);
            var parameter = WorkflowTaskParameter.NewWorkflowTaskParameter(engineTaskInfo, engineTaskParameterInfo);
            workflowTask.Parameters.Add(parameter);
            var planogram = WorkpackagePlanogram.NewWorkpackagePlanogram();
            var model = WorkpackagePlanogramParameter.NewWorkpackagePlanogramParameter(parameter);
            planogram.Parameters.Add(model);
            return new WorkpackageWizardParameter(model, parameter);
        }
    }
}
