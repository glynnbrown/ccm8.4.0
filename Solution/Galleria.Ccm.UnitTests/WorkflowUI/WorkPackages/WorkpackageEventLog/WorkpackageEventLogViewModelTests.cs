﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 810)
// V8-29590 : A.Probyn
//  Created
// V8-29861 : A.Probyn
//  ~ Updated inline with performance changes
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageEventLog;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.WorkflowUI.WorkPackages.WorkpackageEventLog
{
    [TestFixture]
    public class WorkpackageEventLogViewModelTests : TestBase
    {
        #region TestFixture Helpers

        private Workpackage _testModel = null;

        private void InsertTestData()
        {
            List<Int32> packageIds = TestDataHelper.InsertPlanograms(5);

            //add a workflow
            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();

            Ccm.Model.Workflow flow1 = Ccm.Model.Workflow.NewWorkflow(this.EntityId);
            flow1.Name = Guid.NewGuid().ToString();
            flow1.Tasks.Add(engineTasks[6]);
            flow1 = flow1.Save();

            PlanogramInfoList plans = PlanogramInfoList.FetchByIds(packageIds);

            //add some workpackages
            Workpackage package1 = Workpackage.NewWorkpackage(this.EntityId);
            package1.WorkflowId = flow1.Id;
            package1.Name = Guid.NewGuid().ToString();
            package1.Planograms.Add(plans[0]);
            package1.Planograms.Add(plans[1]);
            package1 = package1.Save();
            _testModel = package1;

            //Add some event logs
            EventLog.CreateEventLog(null,
                            null,
                            "32769",
                            32769,
                            System.Diagnostics.EventLogEntryType.Error,
                            null,
                            "Process 1",
                            null,
                            "1",
                            "2",
                            "3",
                            "4",
                            package1.Name,
                            package1.Id);

            EventLog.CreateEventLog(null,
                            null,
                            "32769",
                            32769,
                            System.Diagnostics.EventLogEntryType.Warning,
                            null,
                            "Process 2",
                            null,
                            "5",
                            "6",
                            "7",
                            "8",
                            package1.Name,
                            package1.Id);
                            
        }

        public override void Setup()
        {
            base.Setup();

            InsertTestData();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailableWorkpackageEventLogs()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            Assert.AreEqual(2, view.AvailableWorkpackageEventLogs.Count);
        }

        [Test]
        public void Property_AvailablePlanogramEventLogs()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            Assert.AreEqual(5, view.AvailablePlanogramEventLogs.Count);
        }

        [Test]
        public void Property_SelectedViewType()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            Assert.AreEqual(WorkpackageEventLogViewType.Planograms, view.SelectedViewType);

            //Update
            view.SelectedViewType = WorkpackageEventLogViewType.Workpackages;

            Assert.AreEqual(WorkpackageEventLogViewType.Workpackages, view.SelectedViewType);
        }

        [Test]
        public void Property_SelectedWorkpackageEventLog()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            //Should be blank on load
            Assert.AreEqual(null, view.SelectedWorkpackageEventLog);

            //Check counts
            Assert.AreEqual(0, view.FilteredWorkpackageEventLogs.Count);

            //Change view type
            view.SelectedViewType = WorkpackageEventLogViewType.Workpackages;

            //Update to show warnings
            view.IsWarningVisible = true;

            //Check counts
            Assert.AreEqual(2, view.FilteredWorkpackageEventLogs.Count);

            //Check first row is selected
            Assert.AreEqual(view.FilteredWorkpackageEventLogs.First(), view.SelectedWorkpackageEventLog);
        }

        [Test]
        public void Property_SelectedPlanogramEventLog()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            //Should have first selected on load
            Assert.AreEqual(view.FilteredPlanogramEventLogs.First(), view.SelectedPlanogramEventLog);

            //Check counts
            Assert.AreEqual(5, view.FilteredPlanogramEventLogs.Count);

            //Update to show information
            view.IsInformationVisible = false;
            view.SelectedViewType = WorkpackageEventLogViewType.Planograms;

            //Check counts
            Assert.AreEqual(0, view.FilteredPlanogramEventLogs.Count);

            //Check no row is seected
            Assert.AreEqual(null, view.SelectedPlanogramEventLog);
        }

        #region FilteredWorkpackageEventLogs

        [Test]
        public void Property_FilteredWorkpackageEventLogs()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            Assert.AreEqual(0, view.FilteredWorkpackageEventLogs.Count);
        }

        [Test]
        public void Property_FilteredWorkpackageEventLogs_ChangedByFilter()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            //Check counts
            Assert.AreEqual(0, view.FilteredWorkpackageEventLogs.Count);

            //Change view type
            view.SelectedViewType = WorkpackageEventLogViewType.Workpackages;

            //Update to show warnings
            view.IsWarningVisible = true;
                       
            //Check counts
            Assert.AreEqual(2, view.FilteredWorkpackageEventLogs.Count);
            
        }

        #endregion

        #region FilteredWorkpackageEventLogs

        [Test]
        public void Property_FilteredPlanogramEventLogs()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            view.SelectedViewType = WorkpackageEventLogViewType.Planograms;

            Assert.AreEqual(5, view.FilteredPlanogramEventLogs.Count);
        }

        [Test]
        public void Property_FilteredPlanogramEventLogs_ChangedByFilter()
        {
            WorkpackageEventLogViewModel view = new WorkpackageEventLogViewModel(_testModel);

            //Check counts
            Assert.AreEqual(5, view.FilteredPlanogramEventLogs.Count);

            //Update to not show information
            view.IsInformationVisible = false;
            view.SelectedViewType = WorkpackageEventLogViewType.Planograms;

            //Check counts
            Assert.AreEqual(0, view.FilteredPlanogramEventLogs.Count);

            //Update to show information
            view.IsInformationVisible = true;

            //Check counts
            Assert.AreEqual(5, view.FilteredPlanogramEventLogs.Count);
        }

        #endregion
              

        #endregion
        
        #region Commands

        #endregion
    }
}
