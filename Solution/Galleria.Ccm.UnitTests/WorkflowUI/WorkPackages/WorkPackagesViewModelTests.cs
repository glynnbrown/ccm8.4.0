﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25460 : L.Hodson
//  Created
// V8-27186 : A.Silva   ~ Amended to use PlanogramRepositoryRow instead of PlanogramInfo.
// V8-28444 : M.Shelley
//  Fix failing unit tests : modified anonymous lambdas for ShowWindowRequest to use a delegate, so that these 
//  can be un-registered to prevent a test affecting another that uses the window helper (namely methods 
//  Command_OpenWorkpackage_Execute and Command_NewWorkpackage_Execute)
//  Also modified a number of test cases to set the SelectedProcessingStatusFilterType to "All" to ensure that
//  AvailableWorkpackageRows is populated
#endregion
#region Version History: (CCM 8.0.1)
// V8-28690 : D.Pleasance
//  Added test for new workpackage filtering, Property_AvailableWorkpackages_ChangedBySelectedWorkpackageProcessingStatusFilterType.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Added missing WindowService responses
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.UnitTests.WorkflowUI.WorkPackages
{
    [TestFixture]
    public sealed class WorkPackagesViewModelTests : TestBase
    {
        #region TestFixture Helpers

        private void InsertTestData()
        {
            List<Int32> packageIds = TestDataHelper.InsertPlanograms(5);

            //add a workflow
            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();

            Ccm.Model.Workflow flow1 = Ccm.Model.Workflow.NewWorkflow(this.EntityId);
            flow1.Name = Guid.NewGuid().ToString();
            flow1.Tasks.Add(engineTasks[6]);
            flow1 = flow1.Save();

            PlanogramInfoList plans = PlanogramInfoList.FetchByIds(packageIds);

            //add some workpackages
            Workpackage package1 = Workpackage.NewWorkpackage(this.EntityId);
            package1.WorkflowId = flow1.Id;
            package1.Name = Guid.NewGuid().ToString();
            package1.Planograms.Add(plans[0]);
            package1.Planograms.Add(plans[1]);
            package1 = package1.Save();

            Workpackage package2 = Workpackage.NewWorkpackage(this.EntityId);
            package2.WorkflowId = flow1.Id;
            package2.Name = Guid.NewGuid().ToString();
            package2.Planograms.Add(plans[2]);
            package2.Planograms.Add(plans[3]);
            package2.Planograms.Add(plans[4]);
            package2 = package2.Save();
        }

        public override void Setup()
        {
            base.Setup();

            InsertTestData();
        }

        #endregion

        #region Properties

        #region AvailableWorkpackages

        [Test]
        public void Property_AvailableWorkpackages()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            Assert.AreNotEqual(0, view.AvailableWorkpackages.Count);

			view.StopPolling();
        }

        [Test]
        public void Property_AvailableWorkpackages_ChangedBySelectedWorkpackageProcessingStatusFilterType()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();
            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();

            Int32 workpackageCount = view.AvailableWorkpackageRows.Count;
            Int32 workpackageId = view.SelectedWorkpackageRow.Info.Id;
            view.SelectedWorkpackageProcessingStatusFilterType = ProcessingStatusFilterType.All;

            //update available workpackages
            view.RefreshAvailableWorkpackages();

            Assert.IsNotNull(view.SelectedWorkpackageRow);
            Assert.AreEqual(workpackageId, view.SelectedWorkpackageRow.Info.Id);
            Assert.AreEqual(workpackageCount, view.AvailableWorkpackages.Count);

            view.SelectedWorkpackageProcessingStatusFilterType = ProcessingStatusFilterType.Complete;
            // filter shouldnt return any available workpackage rows as they are all created as pending.
            Assert.AreEqual(0, view.AvailableWorkpackageRows.Count);

            view.StopPolling();
        }

        #endregion

        #region SelectedWorkpackageInfo

        [Test]
        public void Property_SelectedWorkpackageInfo_IsInitialised()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();
            Assert.IsNotNull(view.SelectedWorkpackageRow);
            Assert.IsNotNull(view.SelectedWorkpackageRow.Info);
            Assert.Contains(view.SelectedWorkpackageRow, view.AvailableWorkpackageRows);
            Assert.Contains(view.SelectedWorkpackageRow.Info, view.AvailableWorkpackages);

			view.StopPolling();
        }

        [Test]
        public void Property_SelectedWorkpackageRow_Set()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            base.PropertyChangedNotifications.Clear();
            view.PropertyChanged += base.TestModel_PropertyChanged;

            //check set
            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();
            Assert.AreEqual(view.AvailableWorkpackageRows.Last(), view.SelectedWorkpackageRow);
            Assert.Contains(WorkPackagesViewModel.SelectedWorkpackageRowProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;

			view.StopPolling();
        }

        [Test]
        public void Property_SelectedWorkpackageInfo_NotLostOnAvailableWorkpackagesChanged()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();
            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();

            Int32 workpackageId = view.SelectedWorkpackageRow.Info.Id;

            //update available workpackages
            view.RefreshAvailableWorkpackages();

            Assert.IsNotNull(view.SelectedWorkpackageRow);
            Assert.AreEqual(workpackageId, view.SelectedWorkpackageRow.Info.Id);

			view.StopPolling();
        }

        #endregion

        #region CurrentWorkpackageTasks


        [Test]
        public void Property_CurrentWorkpackageTasks_IsInitialized()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;
            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.FirstOrDefault();

            Assert.AreNotEqual(0, view.CurrentWorkpackageTasks.Count());
            
            view.StopPolling();
        }

        [Test]
        public void Property_CurrentWorkpackageTasks_ChangedBySelectedWorkpackageInfo()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();

            Workpackage package = Workpackage.FetchById(view.SelectedWorkpackageRow.Info.Id);
            Ccm.Model.Workflow workflow = Ccm.Model.Workflow.FetchById(package.WorkflowId);

            //check tasks
            Assert.AreEqual(workflow.Tasks.Count, view.CurrentWorkpackageTasks.Count());

            for (Int32 i = 0; i < workflow.Tasks.Count; i++)
            {
                var viewTask = view.CurrentWorkpackageTasks.OrderBy(t => t.SequenceId).ElementAt(i);
                var workflowTask = workflow.Tasks.OrderBy(t => t.SequenceId).ElementAt(i);

                //Assert.AreEqual(workflowTask.Details.Name, viewTask.Name);
                Assert.AreEqual(workflowTask.SequenceId, viewTask.SequenceId);
            }

			view.StopPolling();
        }

        #endregion

        #region CurrentWorkpackagePlans

        [Test]
        public void Property_CurrentWorkpackagePlans_IsInitialized()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            Assert.AreNotEqual(0, view.PlanogramRows.Count());

			view.StopPolling();
        }

        [Test]
        public void Property_CurrentWorkpackagePlans_ChangedBySelectedWorkpackageInfo()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();

            Workpackage package = Workpackage.FetchById(view.SelectedWorkpackageRow.Info.Id);

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            Assert.AreEqual(package.Planograms.Count, view.PlanogramRows.Count);

            for (Int32 i = 0; i < package.Planograms.Count; i++)
            {
                var packagePlan = package.Planograms.OrderBy(p => p.DestinationPlanogramId).ElementAt(i);
                var viewPlan = view.PlanogramRows.OrderBy(p => p.Info.Id).ElementAt(i);

                Assert.AreEqual(packagePlan.DestinationPlanogramId, viewPlan.Info.Id);
            }

			view.StopPolling();
        }

        #endregion


        #region SelectedPlanogramInfo

        [Test]
        public void Property_SelectedPlanogramInfo_IsInitialized()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            Assert.IsNotNull(view.SelectedPlanogramRow);

			view.StopPolling();
        }

        [Test]
        public void Property_SelectedPlanogramInfo_ChangedBySelectedWorkpackageInfo()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            base.PropertyChangedNotifications.Clear();
            view.PropertyChanged += base.TestModel_PropertyChanged;

            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();
            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            Assert.Contains(WorkPackagesViewModel.SelectedPlanogramRowProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(view.SelectedPlanogramRow, view.PlanogramRows);

            view.PropertyChanged -= base.TestModel_PropertyChanged;

			view.StopPolling();
        }

        [Test]
        public void Property_SelectedPlanogramInfo_Set()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            base.PropertyChangedNotifications.Clear();
            view.PropertyChanged += base.TestModel_PropertyChanged;

            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            view.SelectedPlanogramRow = view.PlanogramRows.Last();
            Assert.AreEqual(view.PlanogramRows.Last(), view.SelectedPlanogramRow);
            Assert.Contains(WorkPackagesViewModel.SelectedPlanogramRowProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;

			view.StopPolling();
        }

        [Test]
        public void Property_SelectedPlanogramInfo_NotLostOnAvailableWorkpackagesChanged()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();
            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.Last();

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            view.SelectedPlanogramRow = view.PlanogramRows.Last();

            Int32 planId = view.SelectedPlanogramRow.Info.Id;

            //update available workpackages
            view.RefreshAvailableWorkpackages();

            Assert.IsNotNull(view.SelectedPlanogramRow);
            Assert.AreEqual(planId, view.SelectedPlanogramRow.Info.Id);

			view.StopPolling();
        }

        #endregion

        //#region SelectedPlanogramThumbnail

        //[Test]
        //public void Property_SelectedPlanogramThumbnail_ChangedBySelectedPlanogramInfo()
        //{
        //    WorkPackagesViewModel view = new WorkPackagesViewModel();

        //    base.PropertyChangedNotifications.Clear();
        //    view.PropertyChanged += base.TestModel_PropertyChanged;

        //    view.SelectedPlanogramRow = view.PlanogramRows.Last();
        //    Assert.Contains(WorkPackagesViewModel.SelectedPlanogramThumbnailProperty.Path, base.PropertyChangedNotifications);

        //    view.PropertyChanged -= base.TestModel_PropertyChanged;
        //}

        //#endregion
        
        #endregion

        #region Commands

        #region NewWorkpackage

        [Test]
        public void Command_NewWorkpackage_Execute()
        {
            EventHandler<WindowHelper.ShowWindowRequestArgs> showWindowDelegate = null;

            WorkPackagesViewModel view = new WorkPackagesViewModel();

            Boolean winRequestMade = false;

            showWindowDelegate = delegate(object s, WindowHelper.ShowWindowRequestArgs e)
            {
                winRequestMade = true;
                WizardViewModel viewModel = e.Args[0] as WizardViewModel;
                Assert.IsNotNull(viewModel);

                Assert.IsTrue(viewModel.WorkpackageData.Workpackage.IsNew);
            };

            WindowHelper.ShowWindowRequest += showWindowDelegate;

            view.NewWorkpackageCommand.Execute();

            Assert.IsTrue(winRequestMade);

            WindowHelper.ShowWindowRequest -= showWindowDelegate;
			view.StopPolling();
        }

        #endregion

        #region OpenWorkpackage

        [Test]
        //[STAThread]
        public void Command_OpenWorkpackage_Execute()
        {
            EventHandler<WindowHelper.ShowWindowRequestArgs> showWindowDelegate = null;

            WorkPackagesViewModel view = new WorkPackagesViewModel();

            Boolean winRequestMade = false;

            showWindowDelegate = delegate(object s, WindowHelper.ShowWindowRequestArgs e)
            {
                winRequestMade = true;
                WizardViewModel viewModel = e.Args[0] as WizardViewModel;
                Assert.IsNotNull(viewModel);

                Assert.IsFalse(viewModel.WorkpackageData.Workpackage.IsNew);
                Assert.AreEqual(view.SelectedWorkpackageRow.Info.Id, viewModel.WorkpackageData.Workpackage.Id);
            };

            WindowHelper.ShowWindowRequest += showWindowDelegate;
            view.OpenWorkpackageCommand.Execute();

            Assert.IsTrue(winRequestMade);

            WindowHelper.ShowWindowRequest -= showWindowDelegate;
            view.StopPolling();
        }

        #endregion

        #region Delete Workpackage

        [Test]
        public void Command_DeleteWorkpackage_DisabledWhenNoWorkpackage()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            RelayCommand cmd = view.DeleteWorkpackageCommand;

            view.SelectedWorkpackageRow = null;

            Assert.IsNull(view.SelectedWorkpackageRow);
            Assert.IsFalse(cmd.CanExecute());
            Assert.IsNotNull(cmd.DisabledReason);

            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.First();
            Assert.IsTrue(cmd.CanExecute());

			view.StopPolling();
        }

        [Test]
        public void Command_DeleteWorkpackage_Execute()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            RelayCommand cmd = view.DeleteWorkpackageCommand;

            Assert.IsNotNull(cmd.Icon);
            Assert.IsNotNull(cmd.FriendlyName);
            Assert.IsNotNull(cmd.FriendlyDescription);

            WorkpackageInfo selectedWorkpackage = view.SelectedWorkpackageRow.Info;

            //delete it
            WindowService.AddResponseResult(NUnitWindowService.ShowMessageMethod, ModalMessageResult.Button1);
            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod);//display busy
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //display busy
            cmd.Execute();

            Assert.AreNotEqual(selectedWorkpackage, view.SelectedWorkpackageRow.Info);
            Assert.IsFalse(view.AvailableWorkpackages.Any(w => w.Id == selectedWorkpackage.Id));

            //refetch
            WorkpackageInfoList workPackageInfos = WorkpackageInfoList.FetchByEntityId(App.ViewState.EntityId);
            Assert.IsFalse(workPackageInfos.Any(w => w.Id == selectedWorkpackage.Id));

			view.StopPolling();
        }

        #endregion

        #region RunWorkpackage

        [Test]
        public void Command_RunWorkpackage_Execute()
        {
            throw new InconclusiveException("TODO");
        }

        #endregion

        #region OpenPlan

        [Test]
        public void Command_OpenPlan_Execute()
        {
            throw new InconclusiveException("TODO");
        }

        #endregion

        #region Delete Plan

        [Test]
        public void Command_DeletePlan_DisabledWhenNoPlanogram()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();

            RelayCommand cmd = view.DeletePlanCommand;

            view.SelectedPlanogramRow = null;
            Assert.IsNull(view.SelectedPlanogramRow);
            Assert.IsFalse(cmd.CanExecute());
            Assert.IsNotNull(cmd.DisabledReason);

			view.StopPolling();
        }

        [Test]
        public void Command_DeletePlan_EnabledWhenPlanogramSelected()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();
            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.FirstOrDefault();

            RelayCommand cmd = view.DeletePlanCommand;

            Assert.IsNotNull(view.SelectedWorkpackageRow);

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            // Select a plan
            Assert.IsTrue(view.PlanogramRows.Count() > 0, "There are no planograms for this package");

            view.SelectedPlanogramRow = view.PlanogramRows.FirstOrDefault();
            Assert.IsNotNull(view.SelectedPlanogramRow);

            Assert.IsTrue(cmd.CanExecute());

			view.StopPolling();
        }

        [Test]
        public void Command_DeletePlan_Execute()
        {
            WorkPackagesViewModel view = new WorkPackagesViewModel();
            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.FirstOrDefault();

            RelayCommand cmd = view.DeletePlanCommand;
            Assert.IsNotNull(cmd.Icon);
            Assert.IsNotNull(cmd.FriendlyName);
            Assert.IsNotNull(cmd.FriendlyDescription);

            // Set the processing status filter type
            view.SelectedProcessingStatusFilterType = ProcessingStatusFilterType.All;

            // Select a plan
            Assert.IsTrue(view.PlanogramRows.Count() > 0, "There are no planograms for this package");

            //select a plan
            view.SelectedPlanogramRow = view.PlanogramRows.FirstOrDefault();

            PlanogramInfo planToDelete = view.SelectedPlanogramRow.Info;

            WindowService.AddResponseResult(NUnitWindowService.ShowMessageMethod, ModalMessageResult.Button1);
            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod);//display busy
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //display busy
            cmd.Execute();

            view.SelectedWorkpackageRow = view.AvailableWorkpackageRows.FirstOrDefault();

            Assert.AreNotEqual(planToDelete, view.SelectedPlanogramRow);
            Assert.IsFalse(view.PlanogramRows.Any(w => w.Info.Id == planToDelete.Id));

			view.StopPolling();
        }

        #endregion

        #endregion
    }
}
