﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27095 : I.George
//  Created
#endregion

#region Version History: (CCM 8.1.1)
// V8-30602 : A.Probyn
//  Updated references to WorkpackageWizardData constructor
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.WorkflowUI.WorkPackages
{
    [TestFixture]
    public class SelectPlanogramStepTests : TestBase
    {
        public void InsertTestData()
        {
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;

            //Create a planogram hierarchy
            TestDataHelper.PopulatePlanogramHierarchy(entityId);

            //create some plans
            TestDataHelper.InsertPlanograms(5);
        }

        [Test]
        public void UpdateOnPlanogramSearchCriteriaChanged()
        {
            InsertTestData();

            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;

            var package = Workpackage.NewWorkpackage(entityId);

            var data = new WorkpackageWizardData(package, false);

            var viewModel = new SelectPlanogramsStepViewModel(data);

            viewModel.PlanogramSearchCriteria = "PPP";
            Assert.AreEqual(0, viewModel.PlanogramRows.Count);
        }

        [Test]
        public void UpdatesOnSelectedPlanogramGroupChanged()
        {
            InsertTestData();
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            var package = Workpackage.NewWorkpackage(entityId);

            var data = new WorkpackageWizardData(package, false);

            var viewModel = new SelectPlanogramsStepViewModel(data);

            //change group
            viewModel.PlanogramGroupSearchCriteria = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            Assert.AreEqual(0, viewModel.PlanogramRows.Count);
        }


        [Test]
        public void Property_PlanogramHierarchyView()
        {
            InsertTestData();
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            var package = Workpackage.NewWorkpackage(entityId);
            var data = new WorkpackageWizardData(package, false);

            var viewModel = new SelectPlanogramsStepViewModel(data);
            Assert.IsNotNull(viewModel.PlanogramHierarchyView.Model);
        }

        [Test]
        public void Property_PlanogramSearchCriteria()
        {
            InsertTestData();
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            var package = Workpackage.NewWorkpackage(entityId);
            var data = new WorkpackageWizardData(package, false);

            var viewModel = new SelectPlanogramsStepViewModel(data);
            Assert.IsNull(viewModel.PlanogramSearchCriteria);
        }


        [Test]
        public void Property_SelectedPlanogramRows()
        {
            InsertTestData();
            //create the new package from the existing
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            var package = Workpackage.NewWorkpackage(entityId);

            var data = new WorkpackageWizardData(package, false);

            var viewModel = new SelectPlanogramsStepViewModel(data);
            Assert.IsNotNull(viewModel.AssignedPlanograms);
        }

        [Test]
        public void Property_UserFavouriteGroupsView_Initialised()
        {
            InsertTestData();
            
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            var package = Workpackage.NewWorkpackage(entityId);

            var data = new WorkpackageWizardData(package, false);

            var viewModel = new SelectPlanogramsStepViewModel(data);
            Assert.IsEmpty(viewModel.UserFavouriteGroupsView.Model);

            Assert.IsNotNull(viewModel.UserFavouriteGroupsView.Model);
        }

        [Test]
        public void property_ShowAllAssigned()
        {
            InsertTestData();
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            var package = Workpackage.NewWorkpackage(entityId);

            var data = new WorkpackageWizardData(package, false);

            var viewModel = new SelectPlanogramsStepViewModel(data);
            int availableRows = viewModel.AssignedPlanograms.Count();
            Assert.AreEqual(0, availableRows);

        }
    }
}
