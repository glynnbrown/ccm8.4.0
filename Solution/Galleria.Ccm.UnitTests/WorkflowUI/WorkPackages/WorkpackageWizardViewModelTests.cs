﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Hodson
//  Created
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteriaAsync
// V8-28444 : M.Shelley
//  Fix broken unit tests
#endregion
#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Added StoreSpecificWorkpackage
#endregion
#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Updated StoreSpecificWorkpackage as NextCommand can be executed on load due to defaults.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.WorkflowUI.WorkPackages
{
    [TestFixture]
    public class WorkpackageWizardViewModelTests : TestBase
    {
        #region TestFixture Helpers

        private void InsertTestData()
        {
            //Create a planogram hierarchy
            TestDataHelper.PopulatePlanogramHierarchy(this.EntityId);

            //create some plans
            TestDataHelper.InsertPlanograms(20);
            
            //Add some workflows
            TestDataHelper.InsertWorkflows(this.EntityId);

            //add some products
            TestDataHelper.InsertProductDtos(base.DalFactory, 5, this.EntityId);
            TestDataHelper.InsertLocationDtos(base.DalFactory, 5, this.EntityId);

            //add some location plan assignments
            TestDataHelper.InsertLocationPlanAssignment(this.EntityId);
        }

        #endregion

        [Test]
        public void CopyFromExisting()
        {
            InsertTestData();
            var viewmodel = new WizardViewModel();

            //set the creation type
            Assert.AreEqual(WizardStep.SelectWorkpackageType, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var createStep = (SelectCreationTypeStepViewModel)viewmodel.CurrentStepViewModel;

            createStep.SelectedWorkpackageType = WorkpackageType.NewFromExisting;
            Assert.IsTrue(viewmodel.NextCommand.CanExecute());

            viewmodel.NextCommand.Execute();

            //select plans
            Assert.AreEqual(WizardStep.SelectPlanograms, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var selectPlansStep = (SelectPlanogramsStepViewModel)viewmodel.CurrentStepViewModel;

            selectPlansStep.PlanogramGroupSearchCriteria = selectPlansStep.PlanogramHierarchyView.Model.RootGroup;
            selectPlansStep.PlanogramSearchCriteria = "Plan";
            Assert.AreNotEqual(0, selectPlansStep.PlanogramRows.Count);

            Assert.IsFalse(viewmodel.NextCommand.CanExecute());
            selectPlansStep.PlanogramRows[0].IsSelected = true;

            viewmodel.NextCommand.Execute();

            //select workflow
            Assert.AreEqual(WizardStep.SelectWorkflow, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var selectWorkflowStep = (SelectWorkflowStepViewModel)viewmodel.CurrentStepViewModel;

            Assert.IsFalse(viewmodel.NextCommand.CanExecute());
            Assert.IsFalse(viewmodel.FinishCommand.CanExecute());

            selectWorkflowStep.SelectedWorkflow = selectWorkflowStep.AvailableWorkflowsAsync.First(w => w.Name == "Add Product");
            Int32 workflowId = selectWorkflowStep.SelectedWorkflow.Id;

            Assert.IsFalse(viewmodel.FinishCommand.CanExecute());
            viewmodel.NextCommand.Execute();

            //set parameters
            Assert.AreEqual(WizardStep.SetTaskParameters, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var setParamsStep = (SetTaskParametersStepViewModel)viewmodel.CurrentStepViewModel;

            Assert.IsTrue(viewmodel.NextCommand.CanExecute());
            Assert.IsFalse(viewmodel.FinishCommand.CanExecute());

            String paramValue = ProductInfoList.FetchByEntityId(this.EntityId).First().Gtin;
            setParamsStep.SelectedCells.Add(new WorkpackageWizardCellInfo(setParamsStep.PlanogramRows.First(), "Parameters[0].Value"));
            setParamsStep.SelectedCells[0].SetCellValue(paramValue);

            Assert.IsFalse(viewmodel.FinishCommand.CanExecute());
            viewmodel.NextCommand.Execute();

            //summary
            Assert.AreEqual(WizardStep.Summary, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var summaryStep = (SummaryStepViewModel)viewmodel.CurrentStepViewModel;

            Assert.IsFalse(viewmodel.NextCommand.CanExecute());
            Assert.IsFalse(viewmodel.FinishCommand.CanExecute());

            String workpackageName = Guid.NewGuid().ToString();
            summaryStep.WorkpackageName = workpackageName;
            summaryStep.RunNow = false;

            viewmodel.FinishCommand.Execute();
            Assert.IsTrue((Boolean)viewmodel.DialogResult);

            //check the workpackage was created
            WorkpackageInfoList infoList = WorkpackageInfoList.FetchByEntityId(this.EntityId);
            Assert.Contains(workpackageName, infoList.Select(w => w.Name).ToList());

            Workpackage savedPackage = Workpackage.FetchById(infoList.First(w => w.Name == workpackageName).Id);

            Assert.AreEqual(workflowId, savedPackage.WorkflowId);
            Assert.AreEqual(1, savedPackage.Planograms.Count);

            Assert.AreEqual(paramValue, savedPackage.Planograms[0].Parameters[0].Values[0].Value1);

        }

        [Test]
        public void LoadFromExistingWorkpackage()
        {
            InsertTestData();

            //Insert a test workpackage to copy from
            WorkflowInfoList workflows = WorkflowInfoList.FetchByEntityId(this.EntityId);
            Ccm.Model.Workflow flow1 = Ccm.Model.Workflow.FetchById(workflows.First(w => w.Name == "Add Product").Id);

            Workpackage package = Workpackage.NewWorkpackage(this.EntityId);
            package.Name = Guid.NewGuid().ToString();
            package.WorkflowId = flow1.Id;

            PlanogramInfoList plans = PlanogramInfoList.FetchBySearchCriteria("Plan", null, App.ViewState.EntityId);

            package.Planograms.Add(plans.ElementAt(0));
            package.Planograms[0].Parameters.Add(flow1.Tasks[0].Parameters[0]);
            package.Planograms[0].Parameters[0].Value = ProductInfoList.FetchByEntityId(this.EntityId).ElementAt(0).Gtin;

            package.Planograms.Add(plans.ElementAt(1));
            package.Planograms[1].Parameters.Add(flow1.Tasks[0].Parameters[0]);
            package.Planograms[1].Parameters[0].Value = ProductInfoList.FetchByEntityId(this.EntityId).ElementAt(1).Gtin;

            package = package.Save();


            //create the new package from the existing
            var viewmodel = new WizardViewModel();

            //set the creation type
            Assert.AreEqual(WizardStep.SelectWorkpackageType, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var createStep = (SelectCreationTypeStepViewModel)viewmodel.CurrentStepViewModel;

            createStep.SelectedWorkpackageType = WorkpackageType.LoadFromWorkpackage;
            Assert.IsTrue(viewmodel.NextCommand.CanExecute());

            viewmodel.NextCommand.Execute();


            //select the workpackage
            Assert.AreEqual(WizardStep.LoadFromOtherWorkpackage, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var workpackageStep = (LoadFromOtherWorkpackageStepViewModel)viewmodel.CurrentStepViewModel;

            Assert.IsFalse(viewmodel.NextCommand.CanExecute());

            workpackageStep.SelectedWorkpackage = workpackageStep.AvailableWorkpackages.First(w => w.Name == package.Name);

            Assert.IsTrue(viewmodel.NextCommand.CanExecute());
            viewmodel.NextCommand.Execute();

            Assert.AreEqual(WizardStep.SelectWorkflow, viewmodel.CurrentWizardStep, "Unexpected wizard step.");

            // Set the selected work flow
            var selectWorkflowStep = (SelectWorkflowStepViewModel)viewmodel.CurrentStepViewModel;

            // First check there is a workflow to set
            Assert.IsTrue(selectWorkflowStep.AvailableWorkflowsAsync != null, "AvailableWorkflowsAsync unexpectedly null.");
            Assert.IsTrue(selectWorkflowStep.AvailableWorkflowsAsync.Count() > 0, "AvailableWorkflowsAsync contains no items.");

            selectWorkflowStep.SelectedWorkflow = selectWorkflowStep.AvailableWorkflowsAsync.Where(x => x.Id == package.WorkflowId).FirstOrDefault();
            viewmodel.NextCommand.Execute();

            Assert.AreEqual(WizardStep.SetTaskParameters, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var setParamsStep = (SetTaskParametersStepViewModel)viewmodel.CurrentStepViewModel;
            viewmodel.NextCommand.Execute();

            Assert.AreEqual(WizardStep.Summary, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var summaryStep = (SummaryStepViewModel)viewmodel.CurrentStepViewModel;

            String newPackageName = Guid.NewGuid().ToString();
            summaryStep.WorkpackageName = newPackageName;
            summaryStep.RunNow = false;

            viewmodel.FinishCommand.Execute();


            //check the new workpackage
            WorkpackageInfoList infoList = WorkpackageInfoList.FetchByEntityId(this.EntityId);
            Workpackage newPackage = Workpackage.FetchById(infoList.First(w => w.Name == newPackageName).Id);

            Assert.AreEqual(package.WorkflowId, newPackage.WorkflowId);
            Assert.AreEqual(2, newPackage.Planograms.Count);

            // No longer valid
            //Assert.AreEqual(package.Planograms[0].Parameters[0].Value, newPackage.Planograms[0].Parameters[0].Value);
            //Assert.AreEqual(package.Planograms[1].Parameters[0].Value, newPackage.Planograms[0].Parameters[0].Value);
        }

        [Test]
        public void StoreSpecificWorkpackage()
        {
            InsertTestData();

            //Insert a test workpackage to copy from
            WorkflowInfoList workflows = WorkflowInfoList.FetchByEntityId(this.EntityId);
            Ccm.Model.Workflow flow1 = Ccm.Model.Workflow.FetchById(workflows.First(w => w.Name == "Add Product").Id);

            Workpackage package = Workpackage.NewWorkpackage(this.EntityId);
            package.Name = Guid.NewGuid().ToString();
            package.WorkflowId = flow1.Id;

            PlanogramInfoList plans = PlanogramInfoList.FetchBySearchCriteria("Plan", null, App.ViewState.EntityId);

            package.Planograms.Add(plans.ElementAt(0));
            package.Planograms[0].Parameters.Add(flow1.Tasks[0].Parameters[0]);
            package.Planograms[0].Parameters[0].Value = ProductInfoList.FetchByEntityId(this.EntityId).ElementAt(0).Gtin;

            package.Planograms.Add(plans.ElementAt(1));
            package.Planograms[1].Parameters.Add(flow1.Tasks[0].Parameters[0]);
            package.Planograms[1].Parameters[0].Value = ProductInfoList.FetchByEntityId(this.EntityId).ElementAt(1).Gtin;

            package = package.Save();


            //create the new package from the existing
            var viewmodel = new WizardViewModel();

            //set the creation type
            Assert.AreEqual(WizardStep.SelectWorkpackageType, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var createStep = (SelectCreationTypeStepViewModel)viewmodel.CurrentStepViewModel;

            createStep.SelectedWorkpackageType = WorkpackageType.StoreSpecific;
            Assert.IsTrue(viewmodel.NextCommand.CanExecute());

            viewmodel.NextCommand.Execute();


            //select the stores \ planograms
            Assert.AreEqual(WizardStep.SelectStoreSpecific, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var workpackageStep = (SelectStoreSpecificPlanogramsStepViewModel)viewmodel.CurrentStepViewModel;

            Assert.IsFalse(viewmodel.NextCommand.CanExecute());

            workpackageStep.SelectedAvailableRows.Add(workpackageStep.AvailableRows.First());
            workpackageStep.AddSelectedCommand.Execute();

            Assert.IsTrue(viewmodel.NextCommand.CanExecute());
            viewmodel.NextCommand.Execute();


            //select the planogram name template
            Assert.AreEqual(WizardStep.SelectPlanogramNameTemplate, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var selectPlanogramNameTemplateStep = (SelectPlanogramNameTemplateStepViewModel)viewmodel.CurrentStepViewModel;
            ((SelectPlanogramNameTemplateStepViewModel)viewmodel.CurrentStepViewModel).SelectedPlanogramNameTemplate.BuilderFormula = "Test formulae";

            //Default are already applied
            Assert.IsTrue(viewmodel.NextCommand.CanExecute());

            selectPlanogramNameTemplateStep.SelectedField = selectPlanogramNameTemplateStep.SelectedGroupFields.First();
            selectPlanogramNameTemplateStep.AddFieldToText(selectPlanogramNameTemplateStep.SelectedField);

            Assert.IsTrue(viewmodel.NextCommand.CanExecute());
            viewmodel.NextCommand.Execute();

            Assert.AreEqual(WizardStep.SelectWorkflow, viewmodel.CurrentWizardStep, "Unexpected wizard step.");

            // Set the selected work flow
            var selectWorkflowStep = (SelectWorkflowStepViewModel)viewmodel.CurrentStepViewModel;

            // First check there is a workflow to set
            Assert.IsTrue(selectWorkflowStep.AvailableWorkflowsAsync != null, "AvailableWorkflowsAsync unexpectedly null.");
            Assert.IsTrue(selectWorkflowStep.AvailableWorkflowsAsync.Count() > 0, "AvailableWorkflowsAsync contains no items.");

            selectWorkflowStep.SelectedWorkflow = selectWorkflowStep.AvailableWorkflowsAsync.Where(x => x.Id == package.WorkflowId).FirstOrDefault();
            viewmodel.NextCommand.Execute();

            Assert.AreEqual(WizardStep.SetTaskParameters, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var setParamsStep = (SetTaskParametersStepViewModel)viewmodel.CurrentStepViewModel;
            viewmodel.NextCommand.Execute();

            Assert.AreEqual(WizardStep.Summary, viewmodel.CurrentWizardStep, "Unexpected wizard step.");
            var summaryStep = (SummaryStepViewModel)viewmodel.CurrentStepViewModel;

            String newPackageName = Guid.NewGuid().ToString();
            summaryStep.WorkpackageName = newPackageName;
            summaryStep.RunNow = false;

            viewmodel.FinishCommand.Execute();


            //check the new workpackage
            WorkpackageInfoList infoList = WorkpackageInfoList.FetchByEntityId(this.EntityId);
            Workpackage newPackage = Workpackage.FetchById(infoList.First(w => w.Name == newPackageName).Id);

            Assert.AreEqual(package.WorkflowId, newPackage.WorkflowId);
            Assert.AreEqual(5, newPackage.Planograms.Count);

            // No longer valid
            //Assert.AreEqual(package.Planograms[0].Parameters[0].Value, newPackage.Planograms[0].Parameters[0].Value);
            //Assert.AreEqual(package.Planograms[1].Parameters[0].Value, newPackage.Planograms[0].Parameters[0].Value);
        }
    }
}
