﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-26159 : L.Ineson
//	Created
// CCM-26953 : I.George
// Added the Property_AvailableMetricRows
#endregion

#region Version History: (CCM 8.0.1)
// V8-28444 : M.Shelley
//  Fix broken unit test : ContinueWithItemChange_WarningForChangedOldItemTimelineGroups
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance;
using Galleria.Framework.Enums;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Moq;
using System.Windows.Media.Animation;
using Galleria.Ccm.Workflow.Client.Wpf;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PerformanceSelectionMaintenance
{
    [TestFixture]
    public class PerformanceSelectionMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private GFSModel.Entity _gfsEntity;
        private Mock<Services.Entity.EntityService> _entityMock;
        private Mock<Services.Performance.PerformanceService> _performanceMock;
        private Mock<Services.Timeline.TimelineService> _timelineMock;

        public override void Setup()
        {
            base.Setup();

            //create mocks
            _entityMock = MockServiceHelper.CreateMock<Services.Entity.EntityService>();
            _performanceMock = MockServiceHelper.CreateMock<Services.Performance.PerformanceService>();
            _timelineMock = MockServiceHelper.CreateMock<Services.Timeline.TimelineService>();

            //set the default entity response.
            MockServiceHelper.Entity_DefaultGetEntitiesResponse(_entityMock);
            _gfsEntity = GFSModel.Entity.FetchByCcmEntity(App.ViewState.CurrentEntityView.Model);

            //set the performance sources
            List<Services.Performance.PerformanceSource> gfsSources = new List<Services.Performance.PerformanceSource>();
            gfsSources.Add(MockServiceHelper.NewPerformanceSource(1, "Performance Source 1","Weeks",_gfsEntity.Name,GFSModel.PerformanceSourceType.Advanced));
            gfsSources.Add(MockServiceHelper.NewPerformanceSource(2, "Performance Source 2", "Months", _gfsEntity.Name, GFSModel.PerformanceSourceType.Advanced));
            MockServiceHelper.Performance_GetPerformanceSourceByEntityNameResponse(_performanceMock, _gfsEntity.Name, gfsSources);


            //set the gfs timeline groups.
            List<Services.Timeline.TimelineGroup> source1TimelineGroups = new List<Services.Timeline.TimelineGroup>();
            source1TimelineGroups.Add(MockServiceHelper.NewTimelineGroup("Week1",2012010101,"Weeks"));
            source1TimelineGroups.Add(MockServiceHelper.NewTimelineGroup("Week2",2012010102,"Weeks"));
            MockServiceHelper.Timeline_GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeResponse(
                _timelineMock, _gfsEntity.Name, gfsSources[0].Name, source1TimelineGroups);

            //source 2 groups
            List<Services.Timeline.TimelineGroup> source2TimelineGroups = new List<Services.Timeline.TimelineGroup>();
            source2TimelineGroups.Add(MockServiceHelper.NewTimelineGroup("Month1",20120101,"Months"));
            MockServiceHelper.Timeline_GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeResponse(
                _timelineMock, _gfsEntity.Name, gfsSources[1].Name, source2TimelineGroups);

           
            //Insert some test performance selections
            PerformanceSelection selection1 = PerformanceSelection.NewPerformanceSelection(this.EntityId);
            selection1.Name = "Selection 1";
            selection1.SelectionType = PerformanceSelectionType.Fixed;
            selection1.TimeType = PerformanceSelectionTimeType.Week;
            selection1.GFSPerformanceSourceId = gfsSources[0].Id;

            PerformanceSelectionTimelineGroup group1 = PerformanceSelectionTimelineGroup.NewPerformanceSelectionTimelineGroup();
            group1.Code = 2012010101;
            selection1.TimelineGroups.Add(group1);

            PerformanceSelectionMetric metric = PerformanceSelectionMetric.NewPerformanceSelectionMetric();
            metric.AggregationType = AggregationType.Sum;
            selection1.Metrics.Add(metric);

            PerformanceSelectionMetric metric2 = PerformanceSelectionMetric.NewPerformanceSelectionMetric();
            metric.AggregationType = AggregationType.Min;
            selection1.Metrics.Add(metric2);

            PerformanceSelectionTimelineGroup group2 = PerformanceSelectionTimelineGroup.NewPerformanceSelectionTimelineGroup();
            group2.Code = 2012010102;
            selection1.TimelineGroups.Add(group2);

            selection1.Save();

            PerformanceSelection selection2 = PerformanceSelection.NewPerformanceSelection(this.EntityId);
            selection2.Name = "Selection 2";
            selection2.SelectionType = PerformanceSelectionType.Dynamic;
            selection2.TimeType = PerformanceSelectionTimeType.Month;
            selection2.GFSPerformanceSourceId = gfsSources[1].Id;

            PerformanceSelectionTimelineGroup group3 = PerformanceSelectionTimelineGroup.NewPerformanceSelectionTimelineGroup();
            group3.Code = 20120101;
            selection2.TimelineGroups.Add(group3);

            PerformanceSelectionTimelineGroup group4 = PerformanceSelectionTimelineGroup.NewPerformanceSelectionTimelineGroup();
            group4.Code = 20120102;
            selection2.TimelineGroups.Add(group4);

            PerformanceSelectionMetric metric3 = PerformanceSelectionMetric.NewPerformanceSelectionMetric();
            metric.AggregationType = AggregationType.Sum;
            selection2.Metrics.Add(metric3);

            PerformanceSelectionMetric metric4 = PerformanceSelectionMetric.NewPerformanceSelectionMetric();
            metric.AggregationType = AggregationType.Min;
            selection2.Metrics.Add(metric4);


            selection2.Save();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailableItems_FetchedOnStartup()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableItems.Count);
        }

        [Test]
        public void Property_AvailableItems_UpdatedOnItemInsert()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            Int32 count = viewModel.AvailableItems.Count;

            //save the new workflow
            viewModel.CurrentItem.Name = "w1";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailableItems.Count);
        }

      
        [Test]
        public void Property_AvailableItems_UpdatedOnItemDelete()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            Int32 count = viewModel.AvailableItems.Count;

            //open and delete a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailableItems.Count);
        }

        [Test]
        public void Property_CurrentItem_InitializedAsNew()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.IsNullOrEmpty(viewModel.CurrentItem.Name);
        }

        [Test]
        public void Property_PerformanceSources_FetchedOnStartup()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.PerformanceSources.Count);
        }

        [Test]
        public void Property_SelectedPerformanceSource_SetOnStartup()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            Assert.IsNotNull(viewModel.SelectedPerformanceSource);
            Assert.Contains(viewModel.SelectedPerformanceSource, viewModel.PerformanceSources);
        }

        [Test]
        public void Property_SelectedPerformanceSource_UpdatedOnItemOpened()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.OpenCommand.Execute(viewModel.AvailableItems[0].Id);
            Assert.AreEqual(viewModel.CurrentItem.GFSPerformanceSourceId, viewModel.SelectedPerformanceSource.Id);

            viewModel.OpenCommand.Execute(viewModel.AvailableItems[1].Id);
            Assert.AreEqual(viewModel.CurrentItem.GFSPerformanceSourceId, viewModel.SelectedPerformanceSource.Id);
        }

        [Test]
        public void Property_SelectedPerformanceSource_SetUpdatesCurrentItem()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.SelectedPerformanceSource = viewModel.PerformanceSources[0];
            Assert.AreEqual(viewModel.CurrentItem.GFSPerformanceSourceId, viewModel.SelectedPerformanceSource.Id);
            Assert.AreEqual(viewModel.CurrentItem.TimeType, viewModel.TimelineGroups[0].TimelineFrequency);

            viewModel.SelectedPerformanceSource = viewModel.PerformanceSources[1];
            Assert.AreEqual(viewModel.CurrentItem.GFSPerformanceSourceId, viewModel.SelectedPerformanceSource.Id);
            Assert.AreEqual(viewModel.CurrentItem.TimeType, viewModel.TimelineGroups[0].TimelineFrequency);
        }

        [Test]
        public void Property_TimelineGroups_UpdatedOnSelectedPerformanceSourceSet()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            GFSTimelineGroupListViewModel groupListView = new GFSTimelineGroupListViewModel();

            viewModel.SelectedPerformanceSource = viewModel.PerformanceSources[0];
            groupListView.BeginFetchByPerformanceSource(_gfsEntity, viewModel.SelectedPerformanceSource);
            Assert.AreEqual(groupListView.Model.Count, viewModel.TimelineGroups.Count);

            //select the next source
            viewModel.SelectedPerformanceSource = viewModel.PerformanceSources[1];
            groupListView.BeginFetchByPerformanceSource(_gfsEntity, viewModel.SelectedPerformanceSource);
            Assert.AreEqual(groupListView.Model.Count, viewModel.TimelineGroups.Count);
        }

        [Test]
        public void Property_SelectedTimelineGroupsCount_UpdatedOnSelectionChange()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();
            viewModel.TimelineGroups[0].IsSelected = true;

            Assert.Contains(PerformanceSelectionMaintenanceViewModel.SelectedTimelineGroupsCountProperty.Path, 
                base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            Assert.Contains(PerformanceSelectionMaintenanceViewModel.SelectedTimelineGroupsCountProperty.Path,
               base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_AvailableMetricRows()
        {
            String propertyName = "AvailableMetricRows";
            var ViewModel = new PerformanceSelectionMaintenanceViewModel();
            Assert.AreEqual(propertyName,PerformanceSelectionMaintenanceViewModel.AvailableMetricRowsProperty.Path);
            AssertHelper.AssertPropertyIsReadOnly(ViewModel.GetType(), propertyName);
            Assert.IsNotNull(ViewModel.AvailableMetricRows);
        }
        #endregion

        #region Commands

        [Test]
        public void Command_New_Execution()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            var oldModel = viewModel.CurrentItem;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.CurrentItem);
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
        }

        [Test]
        public void Command_Open_Execution()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            foreach (var info in viewModel.AvailableItems)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.CurrentItem.Id);
                Assert.IsFalse(viewModel.CurrentItem.IsDirty);
            }
        }

        [Test]
        public void Command_Save_DisabledWhenInvalid()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            Assert.IsFalse(viewModel.CurrentItem.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.CurrentItem.Name = "Test1";

            Assert.IsTrue(viewModel.CurrentItem.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void Command_Save_Execution()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.CurrentItem.Name = "Test1";

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.CurrentItem.Name);
            Assert.IsFalse(viewModel.CurrentItem.IsDirty);
        }

        [Test]
        public void Command_SaveAs_Execution()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            Int32 preCount = PerformanceSelectionInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            String newName = "SaveAsName";

            viewModel.SaveAsCommand.Execute(newName);

            Assert.AreEqual(newName, viewModel.CurrentItem.Name);
            Assert.IsFalse(viewModel.CurrentItem.IsDirty);

            Assert.AreEqual(preCount + 1, PerformanceSelectionInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void Command_SaveAndNew_Execution()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.CurrentItem.Name = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.CurrentItem.Name);
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
        }

        [Test]
        public void Command_Delete_DisabledForNewItem()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            Assert.IsFalse(viewModel.CurrentItem.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void Command_Delete_Execution()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            Int32 preCount = PerformanceSelectionInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.AreEqual(preCount - 1, PerformanceSelectionInfoList.FetchByEntityId(this.EntityId).Count);
        }

        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            viewModel.OpenCommand.Execute(1);
            Assert.IsFalse(base.PropertyChangedNotifications.Contains("ContinueWithItemChange"));

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();
            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(base.PropertyChangedNotifications.Contains("ContinueWithItemChange"));

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            viewModel.CurrentItem.Name = "Name";

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            viewModel.OpenCommand.Execute(1);
            Assert.Contains("ContinueWithItemChange", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.CurrentItem.Name = "Name";

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            viewModel.OpenCommand.Execute(2);
            Assert.Contains("ContinueWithItemChange", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItemTimelineGroups()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.CurrentItem.Name = "Name";
            viewModel.TimelineGroups[0].IsSelected = !viewModel.TimelineGroups[0].IsSelected;

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            viewModel.OpenCommand.Execute(2);
            Assert.Contains("ContinueWithItemChange", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }
        #endregion

        #region Other Tests

        [Test]
        public void Other_CreateSaveOpenFixedSelection()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            //create a new fixed selection
            viewModel.CurrentItem.Name = "FixedTimeline";
            viewModel.SelectedPerformanceSource = viewModel.PerformanceSources.First();
            viewModel.CurrentItem.SelectionType = PerformanceSelectionType.Fixed;

            //select a timeline
            Int64 timelineCode = viewModel.TimelineGroups[0].Code;
            viewModel.TimelineGroups[0].IsSelected = true;

            //save the item
            viewModel.SaveCommand.Execute();

            Int32 itemId = viewModel.CurrentItem.Id;


            //new viewmodel and open
            viewModel.Dispose();
            viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.OpenCommand.Execute(itemId);

            Assert.AreEqual(viewModel.CurrentItem.Name, "FixedTimeline", "Name not set");
            Assert.AreEqual(viewModel.CurrentItem.SelectionType, PerformanceSelectionType.Fixed, "selection type not set");
            Assert.AreEqual(viewModel.SelectedPerformanceSource, viewModel.PerformanceSources.First(), "selected source incorrect");
            Assert.AreEqual(1, viewModel.CurrentItem.TimelineGroups.Count, "timeline groups count wrong");
            Assert.AreEqual(timelineCode, viewModel.CurrentItem.TimelineGroups[0].Code, "group code wrong");
            Assert.IsTrue(viewModel.TimelineGroups[0].IsSelected, "group not selected in viewmodel");
        }

        [Test]
        public void Other_CreateSaveOpenDynamicSelection()
        {
            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            //create a new fixed selection
            viewModel.CurrentItem.Name = "DynamicTimeline";
            viewModel.SelectedPerformanceSource = viewModel.PerformanceSources.First();
            viewModel.CurrentItem.SelectionType = PerformanceSelectionType.Dynamic;

            //set the number of weeks
            viewModel.CurrentItem.DynamicValue = 3;

            //select a timeline to exclude
            Int64 timelineCode = viewModel.TimelineGroups[0].Code;
            viewModel.TimelineGroups[0].IsSelected = true;

            //save the item
            viewModel.SaveCommand.Execute();

            Int32 itemId = viewModel.CurrentItem.Id;


            //new viewmodel and open
            viewModel.Dispose();
            viewModel = new PerformanceSelectionMaintenanceViewModel();

            viewModel.OpenCommand.Execute(itemId);

            Assert.AreEqual(viewModel.CurrentItem.Name, "DynamicTimeline");
            Assert.AreEqual(viewModel.CurrentItem.SelectionType, PerformanceSelectionType.Dynamic);
            Assert.AreEqual(viewModel.SelectedPerformanceSource, viewModel.PerformanceSources.First());
            Assert.AreEqual(3, viewModel.CurrentItem.DynamicValue);
            Assert.AreEqual(1, viewModel.CurrentItem.TimelineGroups.Count);
            Assert.AreEqual(timelineCode, viewModel.CurrentItem.TimelineGroups[0].Code, "group code wrong");
            Assert.IsTrue(viewModel.TimelineGroups[0].IsSelected, "group not selected in viewmodel");
        }

        [Test]
        public void Other_PerformanceSourceIsUnavailable()
        {
            PerformanceSelection selection = PerformanceSelection.FetchById(1);
            Assert.AreEqual(1, selection.GFSPerformanceSourceId);

            //make the first source unavailable
            List<Services.Performance.PerformanceSource> gfsSources = new List<Services.Performance.PerformanceSource>();
            gfsSources.Add(MockServiceHelper.NewPerformanceSource(2, "Performance Source 2", "Months", _gfsEntity.Name, GFSModel.PerformanceSourceType.Advanced));
            MockServiceHelper.Performance_GetPerformanceSourceByEntityNameResponse(_performanceMock, _gfsEntity.Name, gfsSources);

            //load the related performance selection
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Assert.AreNotEqual(1, viewModel.SelectedPerformanceSource.Id, "unavailable source should not be selected");
            Assert.IsNotNull(viewModel.WarningText, "Warning text should have been set.");


            //save
            viewModel.SaveCommand.Execute();
            Assert.IsNull(viewModel.WarningText, "Warning text should have been cleared.");
        }

        [Test]
        public void Other_TimelineGroupIsUnavailable()
        {
            //make a timeline group unavailable
            List<Services.Timeline.TimelineGroup> source1TimelineGroups = new List<Services.Timeline.TimelineGroup>();
            source1TimelineGroups.Add(MockServiceHelper.NewTimelineGroup("Week2", 2012010102, "Weeks"));
            MockServiceHelper.Timeline_GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeResponse(
                _timelineMock, _gfsEntity.Name, "Performance Source 1", source1TimelineGroups);


            //load the related performance selection
            var viewModel = new PerformanceSelectionMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            Assert.IsFalse(viewModel.TimelineGroups.Any(t => t.Code == 2012010101), "Unavailable group should not be set");
            Assert.IsNotNull(viewModel.WarningText, "Warning text should have been set.");

            //save
            viewModel.SaveCommand.Execute();
            Assert.IsNull(viewModel.WarningText, "Warning text should have been cleared.");
            Assert.IsFalse(viewModel.CurrentItem.TimelineGroups.Any(t => t.Code == 2012010101), "Unavailable group should not be set");
        }

        [Test]
        public void Other_SaveAsIncludesWeekSelections()
        {
            //31041

            var viewModel = new PerformanceSelectionMaintenanceViewModel();

            //create a new fixed selection
            viewModel.CurrentItem.Name = Guid.NewGuid().ToString();
            viewModel.SelectedPerformanceSource = viewModel.PerformanceSources.First();
            viewModel.CurrentItem.SelectionType = PerformanceSelectionType.Fixed;

            //select a timeline
            Int64 timelineCode = viewModel.TimelineGroups[0].Code;
            viewModel.TimelineGroups[0].IsSelected = true;

            //save
            viewModel.SaveCommand.Execute();
            Int32 itemId = viewModel.CurrentItem.Id;

            //now save as
            viewModel.SaveAsCommand.Execute(Guid.NewGuid().ToString());
            Int32 itemId2 = viewModel.CurrentItem.Id;

            //new
            viewModel.NewCommand.Execute();
            Assert.IsFalse(viewModel.TimelineGroups[0].IsSelected);

            //open ver 2
            viewModel.OpenCommand.Execute(itemId2);
            Assert.IsTrue(viewModel.TimelineGroups[0].IsSelected);
        }

        #endregion
    }
}
