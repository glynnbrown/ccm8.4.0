﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26124 : I.George
//	Initial version
#endregion

#region Version History: (CCM 8.0)
// V8-30290 : A.Probyn
//  Resolved unit test fails
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.InventoryProfileMaintenance;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.UnitTests.WorkflowUI.InventoryProfile
{
    [TestFixture]
    class InventoryProfileMaintenanceViewModelTest : TestBase<InventoryProfileMaintenanceViewModel>
    {
        #region Test Help Fixture

        private const Int32 _numberInserted = 20;

        public override void Setup()
        {
            base.Setup();

            Helpers.TestDataHelper.InsertInventoryProfileDtos(this.DalFactory, this.EntityId, _numberInserted);
            this.TestModel = new InventoryProfileMaintenanceViewModel();
        }
        #endregion

        #region Properties

        [Test]
        public void Property_MasterInventoryProfileList()
        {
            String propertyName = "MasterInventoryProfileList";
            Assert.AreEqual(propertyName, InventoryProfileMaintenanceViewModel.MasterInventoryProfileListProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyObservable(this.TestModel.MasterInventoryProfileList);

            InventoryProfileInfoList invProInfo = InventoryProfileInfoList.FetchByEntityId(this.EntityId);
            Assert.AreEqual(invProInfo.Count, this.TestModel.MasterInventoryProfileList.Count);
        }

        [Test]
        public void Property_SelectedInventoryProfile()
        {
            String propertyName = "SelectedInventoryProfile";
            Assert.AreEqual(propertyName, InventoryProfileMaintenanceViewModel.SelectedInventoryProfileProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);

            //check this starts with a new rule 
            Assert.IsNotNull(this.TestModel.SelectedInventoryProfile);
        }

        [Test]
        public void Property_ForceValue()
        {
            String propertyName = "ForceValue";
            Assert.AreEqual(propertyName, InventoryProfileMaintenanceViewModel.ForceValueProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.ForceValue);

            //check setter

            //force type facings
            this.TestModel.SelectedForceType = InventoryRuleForceType.Facings;

            base.PropertyChangedNotifications.Clear();
            Int32 value = 4;
            this.TestModel.ForceValue = value;
            Assert.AreEqual(value, this.TestModel.ForceValue);
            Assert.AreEqual(value, this.TestModel.SelectedInventoryProfile.MinFacings);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.MinUnits);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);

            //force type units
            this.TestModel.SelectedForceType = InventoryRuleForceType.Units;
            Assert.AreEqual(value, this.TestModel.ForceValue);

            base.PropertyChangedNotifications.Clear();
            value = 5;
            this.TestModel.ForceValue = value;
            Assert.AreEqual(value, this.TestModel.ForceValue);
            Assert.AreEqual(value, this.TestModel.SelectedInventoryProfile.MinUnits);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.MinFacings);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);

            //check that we can create and re-open a rule with force facings
            this.TestModel.ForceValue = 4;
            this.TestModel.SelectedForceType = InventoryRuleForceType.Facings;
            this.TestModel.OpenCommand.Execute();

            Assert.AreEqual(4, this.TestModel.SelectedInventoryProfile.MinFacings);
            Assert.AreEqual(4, this.TestModel.ForceValue);
            Assert.AreEqual(InventoryRuleForceType.Facings, this.TestModel.SelectedForceType);
        }

        [Test]
        public void Property_SelectedForceType()
        {
            String propertyName = "SelectedForceType";
            Assert.AreEqual(propertyName, InventoryProfileMaintenanceViewModel.SelectedForceTypeProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.SelectedForceType);

            //check setter

            Int32 forceValue = 4;
            this.TestModel.SelectedForceType = InventoryRuleForceType.Facings;
            this.TestModel.ForceValue = forceValue;

            //change type to units
            base.PropertyChangedNotifications.Clear();
            this.TestModel.SelectedForceType = InventoryRuleForceType.Units;
            Assert.AreEqual(InventoryRuleForceType.Units, this.TestModel.SelectedForceType);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(forceValue, this.TestModel.SelectedInventoryProfile.MinUnits);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.MinFacings);

            //change back to facings
            base.PropertyChangedNotifications.Clear();
            this.TestModel.SelectedForceType = InventoryRuleForceType.Facings;
            Assert.AreEqual(InventoryRuleForceType.Facings, this.TestModel.SelectedForceType);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(forceValue, this.TestModel.SelectedInventoryProfile.MinFacings);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.MinUnits);
        }

        [Test]
        public void Property_WasteHurdleValue()
        {
            String propertyName = "WasteHurdleValue";
            Assert.AreEqual(propertyName, InventoryProfileMaintenanceViewModel.WasteHurdleValueProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.WasteHurdleValue);

            //check setter

            //force type facings
            this.TestModel.SelectedWasteHurdleType = InventoryRuleWasteHurdleType.CasePacks;

            base.PropertyChangedNotifications.Clear();
            Int32 value = 4;
            this.TestModel.WasteHurdleValue = value;
            Assert.AreEqual(value, this.TestModel.WasteHurdleValue);
            Assert.AreEqual(value, this.TestModel.SelectedInventoryProfile.WasteHurdleCasePack);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.WasteHurdleUnits);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);

            //force type units
            this.TestModel.SelectedWasteHurdleType = InventoryRuleWasteHurdleType.Units;
            Assert.AreEqual(value, this.TestModel.WasteHurdleValue);

            base.PropertyChangedNotifications.Clear();
            value = 5;
            this.TestModel.WasteHurdleValue = value;
            Assert.AreEqual(value, this.TestModel.WasteHurdleValue);
            Assert.AreEqual(value, this.TestModel.SelectedInventoryProfile.WasteHurdleUnits);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.WasteHurdleCasePack);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);

            //check that we can create and re-open a rule with waste hurdle case packs
            this.TestModel.WasteHurdleValue = 4;
            this.TestModel.SelectedWasteHurdleType = InventoryRuleWasteHurdleType.CasePacks;
            this.TestModel.OpenCommand.Execute();

            Assert.AreEqual(4, this.TestModel.WasteHurdleValue);
            Assert.AreEqual(InventoryRuleWasteHurdleType.CasePacks, this.TestModel.SelectedWasteHurdleType);
        }

        [Test]
        public void Property_SelectedWasteHurdleType()
        {
            String propertyName = "SelectedWasteHurdleType";
            Assert.AreEqual(propertyName, InventoryProfileMaintenanceViewModel.SelectedWasteHurdleTypeProperty.Path);

            //check initial value
            Assert.IsNotNull(this.TestModel.SelectedWasteHurdleType);

            //check setter

            Int32 wasteHurdleValue = 4;
            this.TestModel.SelectedWasteHurdleType = InventoryRuleWasteHurdleType.CasePacks;
            this.TestModel.WasteHurdleValue = wasteHurdleValue;

            //change type to units
            base.PropertyChangedNotifications.Clear();
            this.TestModel.SelectedWasteHurdleType = InventoryRuleWasteHurdleType.Units;
            Assert.AreEqual(InventoryRuleWasteHurdleType.Units, this.TestModel.SelectedWasteHurdleType);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(wasteHurdleValue, this.TestModel.SelectedInventoryProfile.WasteHurdleUnits);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.WasteHurdleCasePack);

            //change back to case pack
            base.PropertyChangedNotifications.Clear();
            this.TestModel.SelectedWasteHurdleType = InventoryRuleWasteHurdleType.CasePacks;
            Assert.AreEqual(InventoryRuleWasteHurdleType.CasePacks, this.TestModel.SelectedWasteHurdleType);
            Assert.Contains(propertyName, base.PropertyChangedNotifications);
            Assert.AreEqual(wasteHurdleValue, this.TestModel.SelectedInventoryProfile.WasteHurdleCasePack);
            Assert.AreEqual(0, this.TestModel.SelectedInventoryProfile.WasteHurdleUnits);
        }
        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            this.TestModel = new InventoryProfileMaintenanceViewModel();

            //start with a new store
            Assert.IsTrue(this.TestModel.SelectedInventoryProfile.IsNew);

            //the info list model should also contain existing stores (+1 for default inventory profile)
            Assert.AreEqual(_numberInserted + 1, this.TestModel.MasterInventoryProfileList.Count());
        }
        #endregion

        #region Command

        #region NewCommand

        [Test]
        public void Command_New()
        {
            RelayCommand cmd = this.TestModel.NewCommand;

            //check command is registered and value are correct
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            NewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());

            //check execute
            base.PropertyChangedNotifications.Clear();
            Galleria.Ccm.Model.InventoryProfile preExecuteItem = this.TestModel.SelectedInventoryProfile;

            cmd.Execute();

            Assert.Contains(InventoryProfileMaintenanceViewModel.SelectedInventoryProfileProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedInventoryProfile, "this Item should have changed");
            Assert.IsTrue(this.TestModel.SelectedInventoryProfile.IsNew, "the item should be new");
            Assert.IsTrue(this.TestModel.SelectedInventoryProfile.IsInitialized, "this item should be marked initialized");
        }

        #endregion

        #region SaveCommand

        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as an invalid object");
            this.TestModel.SelectedInventoryProfile.Name = "IP1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as store is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();
            cmd.Execute();

            //check the selected store has been updated
            Assert.IsFalse(this.TestModel.SelectedInventoryProfile.IsNew, "The IP should no longer be new");
            Assert.IsFalse(this.TestModel.SelectedInventoryProfile.IsDirty, "The IP should have been saved");
            Assert.AreEqual("IP1", this.TestModel.SelectedInventoryProfile.Name, "The IP should be the same");
            Assert.AreNotEqual(0, this.TestModel.SelectedInventoryProfile.Id, "The IP should have been given a valid id");
            Assert.Contains(InventoryProfileMaintenanceViewModel.SelectedInventoryProfileProperty.Path, base.PropertyChangedNotifications);
        
            //check that another IP with the same name cannot be saved
            this.TestModel.NewCommand.Execute();
            this.TestModel.SelectedInventoryProfile.Name = "IP1";
            cmd.Execute();
            Assert.AreNotEqual("IP1", this.TestModel.SelectedInventoryProfile.Name);
        }

        #endregion

        #region OpenCommand

        [Test]
        public void Commannd_Open()
        {
            RelayCommand<Int32?> cmd = this.TestModel.OpenCommand;
            Assert.AreEqual("OpenCommand", InventoryProfileMaintenanceViewModel.OpenCommandProperty.Path);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(null));
            Assert.IsTrue(cmd.CanExecute(1));

            base.PropertyChangedNotifications.Clear();
            Galleria.Ccm.Model.InventoryProfile preExecuteItem = this.TestModel.SelectedInventoryProfile;

            InventoryProfileInfo loadInfo = InventoryProfileInfoList.FetchByEntityId(1).Last();
            cmd.Execute(loadInfo.Id);

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedInventoryProfile, "Item should have changed");
            Assert.AreEqual(loadInfo.Id, this.TestModel.SelectedInventoryProfile.Id, "The location with the info id should ahve been loaded");
            Assert.Contains(InventoryProfileMaintenanceViewModel.SelectedInventoryProfileProperty.Path, base.PropertyChangedNotifications, "Selected item should have change notification");
        }


        #endregion

        #region SaveAndNewCommand

        [Test]
        public void Command_SaveAndNew()
        {
            RelayCommand cmd = this.TestModel.SaveAndNewCommand;
            Assert.AreEqual("SaveAndNewCommand", InventoryProfileMaintenanceViewModel.SaveAndNewCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndNewCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedInventoryProfile.Name = "IP1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as IP is valid");

            //check execution
            //nb cannot test the window closed.
            IEnumerable<InventoryProfileInfo> availableInfoList = InventoryProfileInfoList.FetchByEntityId(this.EntityId);
            Int32 preItemCount = availableInfoList.Count();

            cmd.Execute();

            availableInfoList = InventoryProfileInfoList.FetchByEntityId(this.EntityId);
            Assert.AreEqual(preItemCount + 1, availableInfoList.Count());

            Assert.IsTrue(TestModel.SelectedInventoryProfile.IsNew);
        }

         
        #endregion

        #region SaveAndCloseCommand

        [Test]
        public void Command_SaveAndClose()
        {
            RelayCommand cmd = this.TestModel.SaveAndCloseCommand;
            Assert.AreEqual("SaveAndCloseCommand", InventoryProfileMaintenanceViewModel.SaveAndCloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAndCloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedInventoryProfile.Name = "IP1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as IP is valid");

            //check execution
            //nb cannot test the window closed.
            IEnumerable<InventoryProfileInfo> availableInfoList = InventoryProfileInfoList.FetchByEntityId(this.EntityId);
            Int32 preItemCount = availableInfoList.Count();

            cmd.Execute();

            availableInfoList = InventoryProfileInfoList.FetchByEntityId(this.EntityId);
            Assert.AreEqual(preItemCount + 1, availableInfoList.Count());
        }
        #endregion

        #region SavaAsCommand

        [Test]
        public void Command_SaveAs()
        {
            RelayCommand cmd = this.TestModel.SaveAsCommand;
            Assert.AreEqual("SaveAsCommand", InventoryProfileMaintenanceViewModel.SaveAsCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            SaveAsCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Save should be disabled as invalid object");
            this.TestModel.SelectedInventoryProfile.Name = "IP1";
            Assert.IsTrue(cmd.CanExecute(), "save enabled as IP is valid");

            //check execute
            base.PropertyChangedNotifications.Clear();

            //load an existing item
            InventoryProfileInfoList availableInfoList = InventoryProfileInfoList.FetchByEntityId(1);

            InventoryProfileInfo loadInfo = availableInfoList.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Galleria.Ccm.Model.InventoryProfile preExecuteItem = this.TestModel.SelectedInventoryProfile;

            cmd.Execute();

            Assert.AreNotEqual(preExecuteItem, this.TestModel.SelectedInventoryProfile, "Item should have changed");
            Assert.AreNotEqual(preExecuteItem.Id, this.TestModel.SelectedInventoryProfile.Id, "Id should be different");
            Assert.AreNotEqual(preExecuteItem.Name, this.TestModel.SelectedInventoryProfile.Name, "Code should be different");
            Assert.Contains(InventoryProfileMaintenanceViewModel.SelectedInventoryProfileProperty.Path, base.PropertyChangedNotifications, "should have change notification");

            //check infos are correct
            availableInfoList = InventoryProfileInfoList.FetchByEntityId(1);

            InventoryProfileInfo oldItemInfo = availableInfoList.FirstOrDefault(l => l.Id == preExecuteItem.Id);
            Assert.IsNotNull(oldItemInfo, "An info for the old item should still exist");

            InventoryProfileInfo newItemInfo = availableInfoList.FirstOrDefault(l => l.Id == this.TestModel.SelectedInventoryProfile.Id);
            Assert.IsNotNull(newItemInfo, "An info for the new item should have been added");
        }
        #endregion

        #region DeleteCommand

        [Test]
        public void Command_Delete()
        {
            
            RelayCommand cmd = this.TestModel.DeleteCommand;
            Assert.AreEqual("DeleteCommand", InventoryProfileMaintenanceViewModel.DeleteCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            DeleteCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to delete a new item");

            //add an IP and open
            InventoryProfileInfo loadInfo = this.TestModel.MasterInventoryProfileList.Last();
            this.TestModel.OpenCommand.Execute(loadInfo.Id);
            Assert.IsTrue(cmd.CanExecute(), "It should be possible to delete an existing item");

            //check execute
            Int32 oldItemId = this.TestModel.SelectedInventoryProfile.Id;
            cmd.Execute();

            Assert.IsTrue(this.TestModel.SelectedInventoryProfile.IsNew, "A new item should have been loaded");

            InventoryProfileInfo oldInfo = this.TestModel.MasterInventoryProfileList.FirstOrDefault(l => l.Id == oldItemId);
            Assert.IsNull(oldInfo, "The deleted item should have been removed from the available list");

            Galleria.Ccm.Model.InventoryProfile oldIP = Galleria.Ccm.Model.InventoryProfile.FetchById(oldItemId);
            Assert.IsNotNull(oldIP.DateDeleted, "Should be marked deleted");
        }
        
        #endregion

        #region CloseCommand

        [Test]
        public void Command_Close()
        {
            RelayCommand cmd = this.TestModel.CloseCommand;
            Assert.AreEqual("CloseCommand", InventoryProfileMaintenanceViewModel.CloseCommandProperty.Path);

            //check command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);
            CloseCommandCheckExpectedValues(cmd);

            //check can execute
            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        #endregion
    }
}
