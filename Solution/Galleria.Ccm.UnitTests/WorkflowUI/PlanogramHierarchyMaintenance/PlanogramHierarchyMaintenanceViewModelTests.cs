﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25871 : A.Kuszyk
//  Created.
// V8-27964 : A.Silva
//      Renamed RenameGroup Tests to EditGroup.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramHierarchyMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.UnitTests.WorkflowUI.PlanogramHierarchyMaintenance
{
    [TestFixture]
    public class PlanogramHierarchyMaintenanceViewModelTests : TestBase<PlanogramHierarchyMaintenanceViewModel>
    {
        private PlanogramHierarchySelectorViewModel _treeViewSelectorViewModel;

        [SetUp]
        [STAThread]
        public new void Setup()
        {
            base.Setup();

            var planogramHierarchy = PlanogramHierarchy.FetchByEntityId(base.EntityId);;

            for (Int32 i = 1; i <= 5; i++)
            {
                var tempPlanGroup = PlanogramGroup.NewPlanogramGroup();
                tempPlanGroup.Name = String.Format("PlanogramGroup{0}", i);
                tempPlanGroup.Id = i;
                planogramHierarchy.RootGroup.ChildList.Add(tempPlanGroup);
            }

            planogramHierarchy.Save();

            var planHierarchyViewModel = new PlanogramHierarchyViewModel();
            planHierarchyViewModel.FetchForCurrentEntity();

            UserPlanogramGroupListViewModel userGroups = new UserPlanogramGroupListViewModel();

            _treeViewSelectorViewModel = new PlanogramHierarchySelectorViewModel(planHierarchyViewModel, userGroups);

            TestModel = new PlanogramHierarchyMaintenanceViewModel();
        }

        #region Commands

        #region CanExecute
        [Test]
        public void NewGroup_CanExecute_ReturnsTrueWhenGroupSelected()
        {
            TestModel.SelectedPlanogramGroup = PlanogramGroup.NewPlanogramGroup();
            Assert.That(TestModel.NewGroup_CanExecute());
        }

        [Test]
        public void NewGroup_CanExecute_ReturnsFalseWhenNoGroupSelected()
        {
            TestModel.SelectedPlanogramGroup = null;
            Assert.IsFalse(TestModel.NewGroup_CanExecute());
        }

        [Test]
        public void DeleteGroup_CanExecute_ReturnsTrueWhenGroupSelected()
        {
            PlanogramGroup parentGroup = PlanogramGroup.NewPlanogramGroup();
            parentGroup.ChildList.Add(PlanogramGroup.NewPlanogramGroup());
            TestModel.SelectedPlanogramGroup = parentGroup.ChildList[0];
            Assert.That(TestModel.DeleteGroup_CanExecute());
        }

        [Test]
        public void DeleteGroup_CanExecute_ReturnsFalseWhenNoGroupSelected()
        {
            TestModel.SelectedPlanogramGroup = null;
            Assert.IsFalse(TestModel.DeleteGroup_CanExecute());
        }

        [Test]
        public void EditGroup_CanExecute_ReturnsTrueWhenGroupSelected()
        {
            TestModel.SelectedPlanogramGroup = PlanogramGroup.NewPlanogramGroup();
            Assert.That(TestModel.EditGroup_CanExecute());
        }

        [Test]
        public void EditGroup_CanExecute_ReturnsFalseWhenNoGroupSelected()
        {
            TestModel.SelectedPlanogramGroup = null;
            Assert.IsFalse(TestModel.EditGroup_CanExecute());
        }
        #endregion 

        #region Executed

        [Test]
        public void ExpandAll_Executed_ExpandsAllGroups()
        {
            var allGroups = _treeViewSelectorViewModel.AvailableGroups;
            TestModel.ExpandAllCommand.Execute();
            throw new InconclusiveException("Cannot test because it depends on the attached control");
            Assert.That(allGroups.All(group => group.IsExpanded == true));
        }

        [Test]
        public void CollapseAll_Executed_CollapsesAllGroups()
        {
            var allGroups = _treeViewSelectorViewModel.AvailableGroups;
            TestModel.CollapseAllCommand.Execute();
            Assert.That(allGroups.All(group => group.IsExpanded == false));
        }

        #endregion

        #endregion
    }
}
