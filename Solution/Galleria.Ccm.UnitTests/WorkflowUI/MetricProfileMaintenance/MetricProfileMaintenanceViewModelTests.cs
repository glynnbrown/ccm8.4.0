﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.MetricProfileMaintenance;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Moq;
using System.Windows.Media.Animation;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.WorkflowUI.MetricProfileMaintenance
{
    [TestFixture]
    public class MetricProfileMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private List<MetricProfile> InsertTestData()
        {
            List<MetricProfile> profileList = new List<MetricProfile>();

            List<MetricDto> metrics = TestDataHelper.InsertMetricDtos(base.DalFactory, this.EntityId, 5);

            MetricProfile profile = MetricProfile.NewMetricProfile(this.EntityId);
            profile.SetMetric(1, metrics[0].Id, 1);
            profile.SetMetric(2, metrics[1].Id, 1);
            profile.SetMetric(3, metrics[2].Id, 2);
            profile.Name = "Profile1";
            profileList.Add(profile.Save());

            profile = MetricProfile.NewMetricProfile(this.EntityId);
            profile.SetMetric(1, metrics[3].Id, 2);
            profile.SetMetric(2, metrics[4].Id, 1);
            profile.Name = "Profile2";
            profileList.Add(profile.Save());

            return profileList;
        }

        public override void Setup()
        {
            base.Setup();

            InsertTestData();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailableItems_FetchedOnStartup()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableItems.Count);
        }

        [Test]
        public void Property_AvailableItems_UpdatedOnItemInsert()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Int32 count = viewModel.AvailableItems.Count;

            //save the new workflow
            viewModel.CurrentItem.Name = "w1";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailableItems.Count);
        }

        [Test]
        public void Property_AvailableItems_UpdatedOnItemDelete()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Int32 count = viewModel.AvailableItems.Count;

            //open and delete a workflow
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailableItems.Count);
        }

        [Test]
        public void Property_CurrentItem_InitializedAsNew()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.IsNullOrEmpty(viewModel.CurrentItem.Name);
        }

        [Test]
        public void Property_AvailableMetrics_FetchedOnStartup()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Assert.AreNotEqual(0, viewModel.AvailableMetrics.Count);
        }

        [Test]
        public void Property_AvailableMetrics_UpdatedOnItemLoad()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Int32 count = viewModel.AvailableMetrics.Count;

            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            Assert.AreNotEqual(count, viewModel.AvailableMetrics.Count);
            Assert.AreEqual(count - viewModel.MetricRows.Count, viewModel.AvailableMetrics.Count);

            viewModel.NewCommand.Execute();
            Assert.AreEqual(count, viewModel.AvailableMetrics.Count);
        }


        [Test]
        public void Property_SelectedMetric_Initialized()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Assert.IsNotNull(viewModel.SelectedMetric);
        }

        [Test]
        public void Property_MetricRows_UpdatedOnItemLoad()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Assert.AreEqual(0, viewModel.MetricRows.Count);

            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            Assert.AreNotEqual(0, viewModel.MetricRows.Count);

            viewModel.NewCommand.Execute();
            Assert.AreEqual(0, viewModel.MetricRows.Count);
        }

        [Test]
        public void Property_SelectedMetricRow_UpdatedOnItemLoad()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            Assert.IsNull(viewModel.SelectedMetricRow);

            viewModel.OpenCommand.Execute(viewModel.AvailableMetrics.First().Id);
            Assert.IsNotNull(viewModel.SelectedMetricRow);

            viewModel.NewCommand.Execute();
            Assert.IsNull(viewModel.SelectedMetricRow);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_New_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            var oldModel = viewModel.CurrentItem;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.CurrentItem);
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
        }

        [Test]
        public void Command_Open_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            foreach (var info in viewModel.AvailableItems)
            {
                viewModel.OpenCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.CurrentItem.Id);
                Assert.IsFalse(viewModel.CurrentItem.IsDirty);
            }
        }

        [Test]
        public void Command_Save_DisabledWhenInvalid()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            Assert.IsFalse(viewModel.CurrentItem.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.CurrentItem.Name = "Test1";

            Assert.IsTrue(viewModel.CurrentItem.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void Command_Save_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            viewModel.CurrentItem.Name = "Test1";

            //set a couple of metrics
            Metric m1 = viewModel.SelectedMetric;
            viewModel.AddMetricCommand.Execute();

            Metric m2 = viewModel.SelectedMetric;
            viewModel.AddMetricCommand.Execute();

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.CurrentItem.Name);
            Assert.IsFalse(viewModel.CurrentItem.IsDirty);
            Assert.AreEqual(2, viewModel.MetricRows.Count);

            //fetch back
            MetricProfile savedProfile = MetricProfile.FetchById(viewModel.CurrentItem.Id);
            Assert.AreEqual(savedProfile.Metric1MetricId, m1.Id);
            Assert.AreEqual(viewModel.MetricRows[0].Ratio, savedProfile.Metric1Ratio);
            Assert.AreEqual(savedProfile.Metric2MetricId, m2.Id);
            Assert.AreEqual(viewModel.MetricRows[1].Ratio, savedProfile.Metric2Ratio);
        }

        [Test]
        public void Command_SaveAs_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            Int32 preCount = MetricProfileInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            String newName = "SaveAsName";

            viewModel.SaveAsCommand.Execute(newName);

            Assert.AreEqual(newName, viewModel.CurrentItem.Name);
            Assert.IsFalse(viewModel.CurrentItem.IsDirty);

            Assert.AreEqual(preCount + 1, MetricProfileInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void Command_SaveAndNew_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            viewModel.CurrentItem.Name = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.CurrentItem.Name);
            Assert.IsTrue(viewModel.CurrentItem.IsNew);
        }

        [Test]
        public void Command_Delete_DisabledForNewItem()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);
            Assert.IsFalse(viewModel.CurrentItem.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void Command_Delete_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            Int32 preCount = MetricProfileInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.CurrentItem.IsNew);
            Assert.AreEqual(preCount - 1, MetricProfileInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void Command_AddMetric_DisabledWhenNoAvailable()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            while (viewModel.AvailableMetrics.Count > 0)
            {
                Assert.IsTrue(viewModel.AddMetricCommand.CanExecute());
                viewModel.AddMetricCommand.Execute();
            }

            Assert.IsFalse(viewModel.AddMetricCommand.CanExecute());
        }

        [Test]
        public void Command_AddMetric_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            viewModel.SelectedMetric = viewModel.AvailableMetrics.Last();

            Metric metricToAdd = viewModel.SelectedMetric;

            viewModel.AddMetricCommand.Execute();

            Assert.AreEqual(1, viewModel.MetricRows.Count, "A metric row should have been added");
            Assert.IsFalse(viewModel.AvailableMetrics.Contains(metricToAdd), "Metric should have been removed from available list");
            Assert.IsNotNull(viewModel.SelectedMetric, "A metric should be selected");
            Assert.AreNotEqual(metricToAdd, viewModel.SelectedMetric, "Metric should not still be selected");
            Assert.AreEqual(metricToAdd, viewModel.MetricRows[0].Metric, "Row should be for added metric");
            Assert.AreNotEqual(0, viewModel.MetricRows[0].Ratio, "Row should have value");
            Assert.AreEqual(viewModel.MetricRows[0], viewModel.SelectedMetricRow, "Row should be selected");
        }

        [Test]
        public void Command_DeleteMetric_Execution()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            viewModel.OpenCommand.Execute(viewModel.AvailableItems.First().Id);

            Metric selectedMetric = viewModel.SelectedMetricRow.Metric;
            Int32 count = viewModel.MetricRows.Count;

            viewModel.DeleteMetricCommand.Execute();

            Assert.AreNotEqual(count, viewModel.MetricRows.Count);
            Assert.IsFalse(viewModel.MetricRows.Any(m => m.Metric == selectedMetric));
        }

        #endregion

        #region ContinueWithItemChange

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedNewItem()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            viewModel.OpenCommand.Execute(1);
            Assert.IsFalse(base.PropertyChangedNotifications.Contains("ContinueWithItemChange"));

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void ContinueWithItemChange_NoWarningForUnchangedOldItem()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();
            viewModel.OpenCommand.Execute(2);

            Assert.IsFalse(base.PropertyChangedNotifications.Contains("ContinueWithItemChange"));

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedNewItem()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            viewModel.CurrentItem.Name = "Name";

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            viewModel.OpenCommand.Execute(1);
            Assert.Contains("ContinueWithItemChange", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void ContinueWithItemChange_WarningForChangedOldItem()
        {
            var viewModel = new MetricProfileMaintenanceViewModel();
            viewModel.OpenCommand.Execute(1);
            viewModel.CurrentItem.Name = "Name";

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            viewModel.OpenCommand.Execute(2);
            Assert.Contains("ContinueWithItemChange", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion


    }
}
