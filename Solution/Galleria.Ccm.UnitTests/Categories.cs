﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.UnitTests
{
    internal static class Categories
    {
        public const String Smoke = "Smoke";
        public const String SmokeDal = "Smoke Dal";
        public const String ClientRaised = "Client Raised";
        public const String MerchandisingTasks = "Merchandising Tasks";
        public const String MerchandisePlanogramTask = "Merchandise Planogram Task";
        public const String PlanogramDal_A = "Planogram Dal A";
        public const String PlanogramDal_B = "Planogram Dal B";
    }
}
