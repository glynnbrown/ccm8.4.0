﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31804 : D.Pleasance
//  Created.
// V8-32825 : A.Silva
//  Some refactoring to help with the tiquet.
// CCM-18440 : A.Kuszyk
//  Added test to ensure position attributes are preserved.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Enums;
using Task = Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task;
using ProcessingOrderType = Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProcessingOrderType;
using ProductSelectionType = Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProductSelectionType;
using FluentAssertions;

namespace Galleria.Ccm.UnitTests.EngineTasks.MerchandiseIncreaseProductInventory.v1
{
    [TestFixture, Category(Categories.MerchandisingTasks)]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Constants

        private const Int32 SpaceConstraintParameterId = 0;
        private const Int32 ProcessingOrderParameterId = 1;
        private const Int32 ProductSelectionParameterId = 2;
        private const Int32 PercentageOfRankedProductsParameterId = 3;
        private const Int32 CarParkComponentNameParameterId = 4;

        #endregion

        #region Fields

        Task _task;

        #endregion

        #region Test Helper Methods

        [SetUp]
        public void SetUp()
        {
            _task = new Task();
            TestPlanogram.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            TestPlanogram.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            TestPlanogram.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
        }

        private void SetTaskParameters(SpaceConstraintType spaceConstraintType, ProcessingOrderType processingOrderType, ProductSelectionType productSelectionType, Single percentageOfRankedProducts)
        {
            SetTaskParameterValues(SpaceConstraintParameterId, new Object[] { spaceConstraintType });
            SetTaskParameterValues(ProcessingOrderParameterId, new Object[] { processingOrderType });
            SetTaskParameterValues(ProductSelectionParameterId, new Object[] { productSelectionType });
            SetTaskParameterValues(PercentageOfRankedProductsParameterId, new Object[] { percentageOfRankedProducts });
        }

        private static void AssertFacings(PlanogramPosition position, Int16? wide, Int16? high, Int16? deep, String message = null, Boolean assertEquality = true)
        {
            if (assertEquality)
            {
                if (deep.HasValue) Assert.AreEqual(deep.Value, position.FacingsDeep, message ?? String.Empty);
                if (high.HasValue) Assert.AreEqual(high.Value, position.FacingsHigh, message ?? String.Empty);
                if (wide.HasValue) Assert.AreEqual(wide.Value, position.FacingsWide, message ?? String.Empty);
            }
            else
            {
                if (deep.HasValue) Assert.AreNotEqual(deep.Value, position.FacingsDeep, message ?? String.Empty);
                if (high.HasValue) Assert.AreNotEqual(high.Value, position.FacingsHigh, message ?? String.Empty);
                if (wide.HasValue) Assert.AreNotEqual(wide.Value, position.FacingsWide, message ?? String.Empty);
            }
        }

        #endregion

        #region Parameters

        [Test]
        public void PercentageOfRankedProductsIsNotRequiredFor([Values(ProductSelectionType.AllProducts, ProductSelectionType.ProductsBelowAverageDos)] ProductSelectionType productSelectionType)
        {
            const String percentageNotRequiredFor = "Percentage of ranked products is not required for {0}";
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, productSelectionType, 0.0F);

            String isRequired = Context.WorkflowTask.Parameters[PercentageOfRankedProductsParameterId].IsRequired();

            Assert.That(isRequired, Is.Not.Null, String.Format(percentageNotRequiredFor, ProductSelectionTypeHelper.FriendlyNames[productSelectionType]));
        }

        [Test]
        public void PercentageOfRankedProductsIsRequiredFor([Values(ProductSelectionType.TopRankedProducts)] ProductSelectionType productSelectionType)
        {
            const String percentageRequiredFor = "Percentage of ranked products is required for {0}";
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, productSelectionType, 0.0F);

            String isRequired = Context.WorkflowTask.Parameters[PercentageOfRankedProductsParameterId].IsRequired();

            Assert.That(isRequired, Is.Null, String.Format(percentageRequiredFor, ProductSelectionTypeHelper.FriendlyNames[productSelectionType]));
        }

        [Test]
        public void NoPercentageOfRankedProductsIsValidFor([Values(ProductSelectionType.AllProducts, ProductSelectionType.ProductsBelowAverageDos)] ProductSelectionType productSelectionType)
        {
            const String noPercentageIsValidFor = "No percentage of ranked products is valid for {0}";
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, productSelectionType, 0.0F);

            String isValid = Context.WorkflowTask.Parameters[PercentageOfRankedProductsParameterId].IsValid();

            Assert.That(isValid, Is.Null, String.Format(noPercentageIsValidFor, ProductSelectionTypeHelper.FriendlyNames[productSelectionType]));
        }

        [Test]
        public void NoPercentageOfRankedProductsIsNotValidFor([Values(ProductSelectionType.TopRankedProducts)] ProductSelectionType productSelectionType)
        {
            const String noPercentageIsNotValidFor = "No percentage of ranked products is not valid for {0}";
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, productSelectionType, 0.0F);

            String isValid = Context.WorkflowTask.Parameters[PercentageOfRankedProductsParameterId].IsValid();

            Assert.That(isValid, Is.Not.Null, String.Format(noPercentageIsNotValidFor, ProductSelectionTypeHelper.FriendlyNames[productSelectionType]));
        }

        [Test]
        public void SomePercentageOfRankedProductsIsValidForTopRangedProducts([Values(ProductSelectionType.TopRankedProducts)] ProductSelectionType productSelectionType)
        {
            const String somePercentageIsValidFor = "Some percentage of ranked products is valid for {0}";
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, productSelectionType, 1.0F);

            String isValid = Context.WorkflowTask.Parameters[PercentageOfRankedProductsParameterId].IsValid();

            Assert.That(isValid, Is.Null, String.Format(somePercentageIsValidFor, ProductSelectionTypeHelper.FriendlyNames[productSelectionType]));
        }

        #endregion

        #region Processing Order

        public enum Order
        {
            ObserveOrder,
            ReverseOrder
        }

        [Test]
        public void ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst()
        {
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProductSelectionType.AllProducts });            
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 1;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);

            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_Before");
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_After");

            TestPlanogram.Positions.First(p => p.GetPlanogramProduct().Gtin.Equals(prod1.Gtin))
                .TotalUnits.Should().Be(1, "because prod1 has a high DOS");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct().Gtin.Equals(prod2.Gtin))
                .TotalUnits.Should().Be(2, "because prod2 has a relatively high DOS");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct().Gtin.Equals(prod3.Gtin))
                .TotalUnits.Should().Be(9, "because prod3 has a low DOS");
        }

        [Test]
        public void ExecuteTask_WhenByLowestDosAndAllProductsHaveZeroDos_ShouldNotDoAnything()
        {
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 0;
            prod2.GetPlanogramPerformanceData().P2 = 0f;
            prod3.GetPlanogramPerformanceData().P2 = 0f;
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);

            _task.ExecuteTask(Context);

            TestPlanogram.Positions.Should().OnlyContain(
                p => p.TotalUnits == 1, "because none of the products should have been increased");
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsInRankOrder_IntoBlockSpace([Values(Order.ObserveOrder, Order.ReverseOrder)] Order order)
        {
            Boolean reverseOrder = order == Order.ReverseOrder;
            const Single shelfWidth = 120;
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, ProductSelectionType.AllProducts, 0.0F);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));

            Single[] sizes =
                reverseOrder ?
                new Single[] { shelfWidth / 4f, shelfWidth / 8f, shelfWidth / 32f } :
                new Single[] { shelfWidth / 32f, shelfWidth / 8f, shelfWidth / 4f };
            foreach (var s in sizes)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(s, 196, 75)));
            }
            MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            
            _task.ExecuteTask(Context);

            AssertFacings(TestPlanogram.Positions[reverseOrder ? 2 : 0], 4, null, null);
            AssertFacings(TestPlanogram.Positions[1], 3, null, null);
            AssertFacings(TestPlanogram.Positions[reverseOrder ? 0 : 2], 2, null, null);
        }
        
        [Test]
        public void ExecuteTask_MultiSitedProductsAcrossSequenceGroups([Values(Order.ObserveOrder, Order.ReverseOrder)] Order order)
        {
            Boolean reverseOrder = order == Order.ReverseOrder;
            const Single shelfWidth = 120;
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, ProductSelectionType.AllProducts, 0.0F);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));

            shelf1.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 999;

            Single[] sizes =
                reverseOrder ?
                new Single[] { shelfWidth / 4f, shelfWidth / 8f, shelfWidth / 32f } :
                new Single[] { shelfWidth / 32f, shelfWidth / 8f, shelfWidth / 4f };
            foreach (var s in sizes)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(s, 30, 75)));
            }
            Int32 p = 0;
            foreach (var s in sizes)
            {
                shelf1.AddPosition(bay, TestPlanogram.Products[p]);
                shelf2.AddPosition(bay, TestPlanogram.Products[p]);
                p++;
            }

            MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.33f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.66f, PlanogramBlockingDividerType.Horizontal);

            //using (var mgs = TestPlanogram.GetMerchandisingGroups())
            //{
            //    TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            //}

            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            //AssertFacings(TestPlanogram.Positions[reverseOrder ? 2 : 0], 4, null, null);
            //AssertFacings(TestPlanogram.Positions[1], 3, null, null);
            //AssertFacings(TestPlanogram.Positions[reverseOrder ? 0 : 2], 2, null, null);
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsInRankOrder_BeyondBlockSpace([Values(Order.ObserveOrder, Order.ReverseOrder)] Order order)
        {
            Boolean reverseOrder = order == Order.ReverseOrder;
            const Single shelfWidth = 120;
            SetTaskParameters(SpaceConstraintType.BeyondBlockSpace, ProcessingOrderType.ByRank, ProductSelectionType.AllProducts, 0.0F);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));

            Single[] sizes =
                reverseOrder ?
                new Single[] { shelfWidth / 4f, shelfWidth / 8f, shelfWidth / 32f } :
                new Single[] { shelfWidth / 32f, shelfWidth / 8f, shelfWidth / 4f };
            foreach (var s in sizes)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(s, 196, 75)));
            }
            MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            AssertFacings(TestPlanogram.Positions[reverseOrder ? 2 : 0], 6, null, null, assertEquality: !reverseOrder);
            AssertFacings(TestPlanogram.Positions[1], 3, null, null, assertEquality: !reverseOrder);
            AssertFacings(TestPlanogram.Positions[reverseOrder ? 0 : 2], 2, null, null, assertEquality: !reverseOrder);
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsInAchievedDosOrder_IntoBlockSpace([Values(Order.ObserveOrder, Order.ReverseOrder)] Order order)
        {
            Boolean reverseOrder = order == Order.ReverseOrder;
            const Single shelfWidth = 120;
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByAchievedDos, ProductSelectionType.AllProducts, 0);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));
            Single[] sizes =
                reverseOrder ?
                new Single[] { shelfWidth / 4f, shelfWidth / 8f, shelfWidth / 32f } :
                new Single[] { shelfWidth / 32f, shelfWidth / 8f, shelfWidth / 4f };
            foreach (var s in sizes)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(s, 196, 75)));
            }
            // Achieved DOS is total position units for product / units sold per day.
            // Units sold per day is regSalesMetricId performance data value / DaysOfPerformance (7 in this example).
            TestPlanogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            TestPlanogram.Inventory.DaysOfPerformance = 10;
            Byte regSalesMetricId = TestPlanogram.Performance.Metrics.First(m => m.SpecialType == MetricSpecialType.RegularSalesUnits).MetricId;
            TestPlanogram.Products[0].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[1].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 10);
            TestPlanogram.Products[2].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 1);

            MerchandisePlanogram();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            AssertFacings(TestPlanogram.Positions[reverseOrder ? 2 : 0], 4, null, null);
            AssertFacings(TestPlanogram.Positions[1], 3, null, null);
            AssertFacings(TestPlanogram.Positions[reverseOrder ? 0 : 2], 2, null, null);
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsInAchievedDosOrder_BeyondBlockSpace([Values(Order.ObserveOrder, Order.ReverseOrder)] Order order)
        {
            Boolean reverseOrder = order == Order.ReverseOrder;
            const Single shelfWidth = 120;
            SetTaskParameters(SpaceConstraintType.BeyondBlockSpace, ProcessingOrderType.ByAchievedDos, ProductSelectionType.AllProducts, 0);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));
            Single[] sizes =
                reverseOrder ?
                new Single[] { shelfWidth / 4f, shelfWidth / 8f, shelfWidth / 32f } :
                new Single[] { shelfWidth / 32f, shelfWidth / 8f, shelfWidth / 4f };
            foreach (var s in sizes)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(s, 196, 75)));
            }
            // Achieved DOS is total position units for product / units sold per day.
            // Units sold per day is regSalesMetricId performance data value / DaysOfPerformance (7 in this example).
            TestPlanogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            TestPlanogram.Inventory.DaysOfPerformance = 10;
            Byte regSalesMetricId = TestPlanogram.Performance.Metrics.First(m => m.SpecialType == MetricSpecialType.RegularSalesUnits).MetricId;
            TestPlanogram.Products[0].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[1].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 10);
            TestPlanogram.Products[2].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 1);

            MerchandisePlanogram();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            AssertFacings(TestPlanogram.Positions[reverseOrder ? 2 : 0], 6, null, null, assertEquality: !reverseOrder);
            AssertFacings(TestPlanogram.Positions[1], 3, null, null, assertEquality: !reverseOrder);
            AssertFacings(TestPlanogram.Positions[reverseOrder ? 0 : 2], 2, null, null, assertEquality: !reverseOrder);
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsInRankOrderWhenTwoBlocksShareMerchandisingGroup()
        {
            const Single shelfWidth = 120;
            SetTaskParameters(SpaceConstraintType.BeyondBlockSpace, ProcessingOrderType.ByRank, ProductSelectionType.AllProducts, 0.0F);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));
            for (int i = 1; i <= 4; i++)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(20, 196, 75)));
            }
            MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            TestPlanogram.Assortment.Products[0].Rank = 1; // should be faced up
            TestPlanogram.Assortment.Products[1].Rank = 3;
            TestPlanogram.Assortment.Products[2].Rank = 2; // should be faced up
            TestPlanogram.Assortment.Products[3].Rank = 4;
            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            TestPlanogram.AddSequenceGroup(leftBlock, TestPlanogram.Products.Take(2));
            TestPlanogram.AddSequenceGroup(rightBlock, TestPlanogram.Products.Skip(2).Take(2));
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_IncreasesUnitsInRankOrderWhenTwoBlocksShareMerchandisingGroup");

            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == TestPlanogram.Products[0]).FacingsWide
                .Should().Be(2, "because the first product (rank 1) should get two facings");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == TestPlanogram.Products[1]).FacingsWide
                .Should().Be(1, "because the second product (rank 3) should get one facing");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == TestPlanogram.Products[2]).FacingsWide
                .Should().Be(2, "because the first product (rank 1) should get two facings");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == TestPlanogram.Products[3]).FacingsWide
                .Should().Be(1, "because the second product (rank 3) should get one facing");
        }

        #endregion

        #region Product Selection

        [Test]
        public void ExecuteTask_IncreasesUnitsOnAllProducts_WhenSufficientSpace()
        {
            const Int32 noOfProducts = 3;
            SetTaskParameters(SpaceConstraintType.BeyondBlockSpace, ProcessingOrderType.ByRank, ProductSelectionType.AllProducts, 0.0F);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 196, 75));
                shelf.AddPosition(bay, product);
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            
            MerchandisePlanogram();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            foreach (var position in TestPlanogram.Positions)
            {
                AssertFacings(position, 4, 1, 1);
            }
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsOnTopRankedProducts_WhenSufficientSpace()
        {
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByRank, ProductSelectionType.TopRankedProducts, 0.5f);
            const Int32 noOfProducts = 6;
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 45, 75));
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            // Add top ranking products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[0]);
            shelf2.AddPosition(bay, TestPlanogram.Products[1]);
            shelf3.AddPosition(bay, TestPlanogram.Products[2]);
            // Add bottom ranking products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[3]);
            shelf2.AddPosition(bay, TestPlanogram.Products[4]);
            shelf3.AddPosition(bay, TestPlanogram.Products[5]);

            MerchandisePlanogram();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            foreach (var position in TestPlanogram.Positions)
            {
                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                if (planogramProduct.Id.Equals(TestPlanogram.Products[0].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[1].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[2].Id))
                {
                    AssertFacings(position, 2, 1, 1, String.Format("Top ranking products should be two facings wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
                }
            }

            foreach (var position in TestPlanogram.Positions)
            {
                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                if (planogramProduct.Id.Equals(TestPlanogram.Products[3].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[4].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[5].Id))
                {
                    AssertFacings(position, 1, 1, 1, String.Format("Bottom ranking products should be one facing wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
                }
            }
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsOnBelowAverageDos_WhenSufficientSpace()
        {
            SetTaskParameters(SpaceConstraintType.BlockSpace, ProcessingOrderType.ByAchievedDos, ProductSelectionType.ProductsBelowAverageDos, 0.0F);
            const Int32 noOfProducts = 6;
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 45, 75));
            }
            // Achieved DOS is total position units for product / units sold per day.
            // Units sold per day is regSalesMetricId performance data value / DaysOfPerformance (7 in this example).
            TestPlanogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            TestPlanogram.Inventory.DaysOfPerformance = 7;
            Byte regSalesMetricId = TestPlanogram.Performance.Metrics.First(m => m.SpecialType == MetricSpecialType.RegularSalesUnits).MetricId;
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 45, 75));
            }
            // Set performance data for products that will be below average dos.
            TestPlanogram.Products[0].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[1].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[2].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            // Set performance data for products that will be above average dos.
            TestPlanogram.Products[3].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 0.1f);
            TestPlanogram.Products[4].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 0.1f);
            TestPlanogram.Products[5].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 0.1f);
            // Add low dos products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[0]);
            shelf2.AddPosition(bay, TestPlanogram.Products[1]);
            shelf3.AddPosition(bay, TestPlanogram.Products[2]);
            // Add high dos products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[3]);
            shelf2.AddPosition(bay, TestPlanogram.Products[4]);
            shelf3.AddPosition(bay, TestPlanogram.Products[5]);

            MerchandisePlanogram();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            foreach (var position in TestPlanogram.Positions)
            {
                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                if (planogramProduct.Id.Equals(TestPlanogram.Products[0].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[1].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[2].Id))
                {
                    AssertFacings(position, 2, 1, 1, String.Format("Products below average Dos should be two facings wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
                }
            }

            foreach (var position in TestPlanogram.Positions)
            {
                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                if (planogramProduct.Id.Equals(TestPlanogram.Products[3].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[4].Id) || planogramProduct.Id.Equals(TestPlanogram.Products[5].Id))
                {
                    AssertFacings(position, 1, 1, 1, String.Format("Products above average Dos should be one facing wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
                }
            }
        }

        #endregion

        [Test]
        public void ExecuteTask_PreservesPositionAttributes()
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var prod1 = TestPlanogram.AddProduct();
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            var pos1 = shelf1.AddPosition(bay, prod1);
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Case;
            var prod2 = TestPlanogram.AddProduct();
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            var pos2 = shelf2.AddPosition(bay, prod2);
            pos2.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Case;
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.ReprocessAllMerchandisingGroups();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            TestPlanogram.Products.Should().HaveCount(2, "because all products should be placed");
            TestPlanogram.Positions.Should().OnlyContain(
                p => p.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Case,
                "because the merchandising style of the positions should be preserved");
        }

        [Test]
        public void ExecuteTask_WhenTopRankedAndLowestDos_ShouldIncreaseLowestOfTopRankedProducts()
        {
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.TopRankedProducts });
            base.SetTaskParameterValues(3, new Object[] { 0.666666f });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 1;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenTopRankedAndLowestDos_ShouldIncreaseLowestOfTopRankedProducts");

            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod1)
                .TotalUnits.Should().Be(4, "because prod1 was in the top ranking products, despite having a high dos.");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod2)
                .TotalUnits.Should().Be(7, "because prod2 has a is lowest dos in the high ranking products");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod3)
                .TotalUnits.Should().Be(1, "because prod3 was not in the top ranking products, despite the fact that it had the lowest dos");
        }

        [Test]
        public void ExecuteTask_WhenBelowAverageDosAndLowestDos_ShouldIncreaseLowestOfBelowAverage()
        {
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.ProductsBelowAverageDos });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 1;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenBelowAverageDosAndLowestDos_ShouldIncreaseLowestOfBelowAverage");

            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod1)
                .TotalUnits.Should().Be(1, "because prod1 is above average dos");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod2)
                .TotalUnits.Should().Be(2, "because prod2 is above average dos for most of the increases");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod3)
                .TotalUnits.Should().Be(9, "because prod3 is below average dos");
        }

        [Test]
        public void ExecuteTask_WhenLowestDosDoesntHaveEnoughSpace_ShouldIncreaseOtherProducts()
        {
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(60, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 0.1f;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenLowestDosDoesntHaveEnoughSpace_ShouldIncreaseOtherProducts");

            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod1)
                .TotalUnits.Should().Be(1, "because prod1 has the highest dos");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod2)
                .TotalUnits.Should().Be(5, "because prod2 was the lowest dos that could fit units");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod3)
                .TotalUnits.Should().Be(1, "because prod3 can't fit any additional units");
        }

        [Test]
        public void ExecuteTask_WhenInventoryChangeIsByUnits_UnitsAreIncreased()
        {
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            base.SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            base.SetTaskParameterValues(5, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByUnits });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 60));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);

            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenInventoryChangeIsByUnits_UnitsAreIncreased_Before");
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenInventoryChangeIsByUnits_UnitsAreIncreased_After");


            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod1)
                .TotalUnits.Should().Be(10, "because the 10 facings should have been added to pos 1, as units");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod2)
                .TotalUnits.Should().Be(14, "because the 2 facings should have been added to pos 2, as 14 units");
        }

        [Test]
        public void ExecuteTask_WhenInventoryChangeIsByFacings_FacingsAreIncreased()
        {
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            base.SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            base.SetTaskParameterValues(5, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByFacings });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 60));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);

            _task.ExecuteTask(Context);

            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod1)
                .TotalUnits.Should().Be(6, "because the 6 facings should have been added to pos 1, as units");
            TestPlanogram.Positions.First(p => p.GetPlanogramProduct() == prod2)
                .TotalUnits.Should().Be(6, "because the 6 facings should have been added to pos 2, as 6 units");
        }
    }
}