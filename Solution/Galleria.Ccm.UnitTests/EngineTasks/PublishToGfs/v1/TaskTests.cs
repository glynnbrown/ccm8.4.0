﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811

// V8-30291 : A.Silva
//  Created.

#endregion

#endregion

using Galleria.Ccm.Engine.Tasks.PublishToGfs.v1;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Helpers;

namespace Galleria.Ccm.UnitTests.EngineTasks.PublishToGfs.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        /// <summary>
        ///     Instance of the task that is being tested.
        /// </summary>
        private Task _task;

        #endregion

        #region Helper Methods

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        #endregion

        [Test]
        public void PublishComponentAnnotation()
        {
            //create plan
            Package pk = Package.NewPackage(null, PackageLockType.Unknown);
            Planogram plan = pk.Planograms.Add();
            PlanogramFixture fixture = plan.Fixtures.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add(fixture);

            //add component
            PlanogramFixtureComponent shelfFC = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent component = plan.Components.FindById(shelfFC.PlanogramComponentId);

            //add annotation
            PlanogramAnnotation annotation = plan.Annotations.Add(shelfFC, fixtureItem, PlanogramSettings.NewPlanogramSettings());
            annotation.Text = "test";

            //convert to dc
            var planDc = CcmToGfsPlanogramConverter.ConvertToPlanogramDc(plan);

            //check the annotation is correct
            Assert.AreEqual(1, planDc.Annotations.Count, "Should have 1 annotation");

            var annotationDc = planDc.Annotations[0];
            Assert.AreEqual(annotation.Text, annotationDc.Text, "Text should match");
            Assert.AreEqual(annotation.PlanogramFixtureItemId, annotationDc.PlanogramFixtureItemId);
            Assert.IsNotNull(annotationDc.PlanogramFixtureAssemblyId);
            Assert.AreEqual(annotation.PlanogramFixtureComponentId, annotationDc.PlanogramAssemblyComponentId, "fixture component id should have gone into assembly.");
            Assert.IsNull(annotationDc.PlanogramSubComponentId);
            Assert.IsNull(annotationDc.PlanogramPositionId);

        }
    }
}
