﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31284 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM830
// V8-31807 : D.Pleasance
//  Task changes to become block aware.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32519 : L.Ineson
//  Added AddCarParkTextBox parameter
// V8-31682 : A.Silva
//  Added TaskShouldNeverInsertNonSequencedProductsWhenTheirSequenceGroupIsNotInPresentBlocking.
// V8-32882 : A.Kuszyk
//  Added test for sequence sub-group behaviour.
// V8-32787 : A.Silva
//  Added tests for Assortment Rule Priority enforcement.
// V8-32932 : A.Kuszyk
//  Added defensive check to sequence group/blocking group matching.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingAssortment.v1;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System.Diagnostics;
using FluentAssertions;

namespace Galleria.Ccm.UnitTests.EngineTasks.MerchandiseProductsUsingAssortment.v1
{
    [TestFixture]
    [Category(Categories.MerchandisingTasks)]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        private TaskBase _task;
        private List<ProductDto> _startingProducts;
        private List<ProductDto> _largeProductDtos;
        private List<ProductDto> _productsToInsert;
        private List<ProductDto> _outOfSequenceProduct;
        private List<ProductDto> _nonBlockedSequenceProducts;

        private readonly List<Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>>
            _worseBeforeBetterTreatmentTypes =
                new List<Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>>
                {
                    new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>(
                        PlanogramAssortmentProductTreatmentType.Optional,
                        PlanogramAssortmentProductTreatmentType.Normal),
                    new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>(
                        PlanogramAssortmentProductTreatmentType.Optional,
                        PlanogramAssortmentProductTreatmentType.Inheritance),
                    new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>(
                        PlanogramAssortmentProductTreatmentType.Optional,
                        PlanogramAssortmentProductTreatmentType.Distribution),
                    new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>(
                        PlanogramAssortmentProductTreatmentType.Normal,
                        PlanogramAssortmentProductTreatmentType.Inheritance),
                    new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>(
                        PlanogramAssortmentProductTreatmentType.Normal,
                        PlanogramAssortmentProductTreatmentType.Distribution),
                    new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>(
                        PlanogramAssortmentProductTreatmentType.Inheritance,
                        PlanogramAssortmentProductTreatmentType.Distribution)
                };

        private readonly List<PlanogramAssortmentProductTreatmentType> _productTreatmentTypesWithLowerPriorityThanProductRule =
            new List<PlanogramAssortmentProductTreatmentType>
            {
                PlanogramAssortmentProductTreatmentType.Optional,
                PlanogramAssortmentProductTreatmentType.Normal,
                PlanogramAssortmentProductTreatmentType.Inheritance
            };

        private readonly List<PlanogramAssortmentProductTreatmentType> _productTreatmentTypesWithHigherPriorityThanProductRule =
            new List<PlanogramAssortmentProductTreatmentType>
            {
                PlanogramAssortmentProductTreatmentType.Distribution
            };

        #endregion

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _task = new Task();
            _startingProducts = PlanogramTestHelper.CreateProductDtos(24, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _startingProducts);
            _largeProductDtos = PlanogramTestHelper.CreateProductDtos(24, Context.EntityId, 24, new WidthHeightDepthValue(35, 10, 10)).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _largeProductDtos);
            _productsToInsert = new[] { "AA", "BB", "CC", "DD", "EE" }.Select(gtin => PlanogramTestHelper.NewInsertedProductDto(gtin, Context.EntityId)).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _productsToInsert);
            _outOfSequenceProduct = PlanogramTestHelper.CreateProductDtos(1, Context.EntityId, 54).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _outOfSequenceProduct);
            _nonBlockedSequenceProducts = PlanogramTestHelper.CreateProductDtos(1, Context.EntityId, 55).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _nonBlockedSequenceProducts);
        }

        #region Behavior Tests

        [Test]
        public void ExecuteTask_WhenProductToInsertCanBePlaced_ShouldPlace()
        {
            const String expectation = "The Planogram's Positions should contain one placement for the product to be inserted.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _productsToInsert.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(4, new Object[] { SpaceConstraintType.BeyondBlockSpace });
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            if (positionsContainOnlyOnePlacementForProduct) Assert.Inconclusive("The product to be inserted should not have already a placement.");

            _task.ExecuteTask(Context);

            positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            Assert.IsTrue(positionsContainOnlyOnePlacementForProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenPlacedProductIsNew_ShouldAddToProductList()
        {
            const String expectation = "The Planogram's Product List should contain the newly placed product.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _productsToInsert.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            String expectedGtin = productsToInsert.First().Gtin;
            Boolean productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            if (productListContainsNewPlacedProduct) Assert.Inconclusive("The product to be inserted should not exist previously in the Planogram's Product List.");

            _task.ExecuteTask(Context);

            productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            Assert.IsTrue(productListContainsNewPlacedProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertIsNotPlaced_ShouldNotAddToProductList()
        {
            const String expectation = "The Planogram's Product List should not contain the non placed new product.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _outOfSequenceProduct.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            String expectedGtin = productsToInsert.First().Gtin;
            Boolean productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            if (productListContainsNewPlacedProduct) Assert.Inconclusive("The product to be inserted should not exist previously in the Planogram's Product List.");

            _task.ExecuteTask(Context);

            productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            Assert.IsFalse(productListContainsNewPlacedProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertIsNotPlacedButPreviouslyOnProductList_ShouldNotRemoveFromProductList()
        {
            const String expectation = "The Planogram's Product List should still contain the non placed new product as it was there previously.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _outOfSequenceProduct.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            String expectedGtin = productsToInsert.First().Gtin;
            TestPlanogram.AddProduct(productsToInsert.First());
            Boolean productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            if (!productListContainsNewPlacedProduct) Assert.Inconclusive("The product to be inserted should exist previously in the Planogram's Product List.");

            _task.ExecuteTask(Context);

            productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            Assert.IsTrue(productListContainsNewPlacedProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertIsNotSequenced_ShouldNotPlace()
        {
            const String expectation = "The Planogram's Positions should not contain placements for the out of sequence product to be inserted.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _outOfSequenceProduct.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            if (positionsContainOnlyOnePlacementForProduct) Assert.Inconclusive("The product to be inserted should not have already a placement.");

            _task.ExecuteTask(Context);

            positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            Assert.IsFalse(positionsContainOnlyOnePlacementForProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertIsAlreadyPlaced_ShouldNotPlace()
        {
            const String expectation = "The Planogram's Positions should contain only one placement for the product to be inserted.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _startingProducts.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            if (!positionsContainOnlyOnePlacementForProduct) Assert.Inconclusive("The product to be inserted should have already a placement.");

            _task.ExecuteTask(Context);

            positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            Assert.IsTrue(positionsContainOnlyOnePlacementForProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenValidAndInvalidComponentsForProductToInsert_ShouldPlaceOnValidOne()
        {
            const String expectation = "The product to be inserted should be on the valid component.";
            SetUpTestPlanogramTwoShelvesWithProducts();
            IEnumerable<ProductDto> productsToInsert = _productsToInsert.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            Object expectedComponentId = TestPlanogram.GetPlanogramSubComponentPlacements().Last().SubComponent.Id;
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionIsOnExpectedComponent =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin && p.PlanogramSubComponentId == expectedComponentId) != null;
            if (positionIsOnExpectedComponent) Assert.Inconclusive("The product to be inserted should not have already a placement.");

            _task.ExecuteTask(Context);

            positionIsOnExpectedComponent =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin && p.PlanogramSubComponentId == expectedComponentId) != null;
            Assert.IsTrue(positionIsOnExpectedComponent, expectation);
        }

        #endregion

        [Test]
        public void ExecuteTask_WhenBlockingDoesNotMatchSequencing_ShouldComplete()
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            TestPlanogram.AddProduct();
            TestPlanogram.AddBlocking().Dividers.Add(0.5f,0,PlanogramBlockingDividerType.Vertical);
            TestPlanogram.AddSequenceGroup(TestPlanogram.Blocking[0].Groups[1], TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();

            _task.ExecuteTask(Context);

            TestPlanogram.EventLogs.Should().NotContain(
                e => e.EntryType == PlanogramEventLogEntryType.Error,
                "because blocking groups without matching sequence groups should be ignored");
        }

        [Test]
        public void ExecuteTask_WhenAlwaysAddAndInsufficientSpaceForSubGroupProduct_ProductIsStillAddedToSubGroupComponent()
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            var prod0 = TestPlanogram.AddProduct(new WidthHeightDepthValue(110, 10, 10));
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(110, 10, 10));
            shelf1.AddPosition(bay, prod0);
            shelf1.AddPosition(bay, prod1);
            shelf2.AddPosition(bay, prod3);
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.Sequence.Groups[0].SubGroups.Add(
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(
                    TestPlanogram.Sequence.Groups[0].Products.Take(3)));
            TestPlanogram.ReprocessAllMerchandisingGroups();
            TestPlanogram.UpdatePositionSequenceData();
            base.SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });

            _task.ExecuteTask(Context);

            GetShelfGtins(shelf1).Should().Equal(TestPlanogram.Products.Take(3).Select(p => p.Gtin), "the first three products (in a sub group) should be placed on the first shelf");
            GetShelfGtins(shelf2).Should().Equal(TestPlanogram.Products.Skip(3).Select(p => p.Gtin), "the last product should be placed by itself on the second shelf, despite it having spare white space");
        }

        private static IEnumerable<String> GetShelfGtins(PlanogramFixtureComponent shelf)
        {
            return shelf.GetPlanogramComponent().GetSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin);
        }

        [Test]
        public void TaskExecution_WhenProductDoesNotFit_ShouldNotPlace()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(3).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);

            var block = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramSequenceGroup greenSequence = TestPlanogram.AddSequenceGroup(block, new List<PlanogramProduct>());

            greenSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0],
                                         _largeProductDtos[1], 
                                         _largeProductDtos[2], 
                                         _largeProductDtos[3], 
                                         _largeProductDtos[4], 
                                         _largeProductDtos[5], 
                                         _largeProductDtos[6], 
                                         _largeProductDtos[7], 
                                         _largeProductDtos[8], 
                                         _largeProductDtos[9] 
                                     });

            TestPlanogram.UpdatePositionSequenceData();

            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(3).Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            _task.ExecuteTask(Context);
            IEnumerable<String> actual = TestPlanogram.GetSubComponentPlacementsFor(productsToInsert).Select(o => o.Component.Name).ToList();

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void TaskExecution_WhenAddToCarparkShelf_ShouldAddDroppedToCarpark()
        {
            const String carParkComponentName = "TestCarPark";
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(3).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);

            var block = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramSequenceGroup greenSequence = TestPlanogram.AddSequenceGroup(block, new List<PlanogramProduct>());

            greenSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0],
                                         _largeProductDtos[1], 
                                         _largeProductDtos[2], 
                                         _largeProductDtos[3], 
                                         _largeProductDtos[4], 
                                         _largeProductDtos[5], 
                                         _largeProductDtos[6], 
                                         _largeProductDtos[7], 
                                         _largeProductDtos[8], 
                                         _largeProductDtos[9] 
                                     });

            TestPlanogram.UpdatePositionSequenceData();

            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(4).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AddToCarParkComponent });
            SetTaskParameterValues(2, new Object[] { carParkComponentName });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);

            using (PlanogramMerchandisingGroupList merchandisingGroups = this.Context.Planogram.GetMerchandisingGroups())
            {
                PlanogramMerchandisingGroup carparkMerchGroup = merchandisingGroups
                    .FirstOrDefault(merchandisingGroup =>
                                    merchandisingGroup.SubComponentPlacements
                                                      .Any(subComponentPlacement =>
                                                           subComponentPlacement.SubComponent.Name == carParkComponentName));
                Assert.IsNotNull(carparkMerchGroup, "The planogram should contain a Car Park Shelf.");

                List<String> actual = carparkMerchGroup.PositionPlacements.Select(p => p.Product.Gtin).ToList();
                List<Single> xPositions = carparkMerchGroup.PositionPlacements.Select(pp => pp.Position.X).OrderBy(p => p).ToList();
                AssertInsert(actual, xPositions, expected);
            }
        }

        [Test]
        public void TaskExecution_WhenAlwaysAdd_ShouldAddDroppedAnyway()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            TestPlanogram.AddBlocking(1, true);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            PlanogramSubComponentPlacement lowerShelf = Context.Planogram.GetPlanogramSubComponentPlacements().First();
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = lowerShelf.GetPlanogramPositions().Select(p => p.GetPlanogramProduct().Gtin).ToList();

            CollectionAssert.IsSubsetOf(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNotRanged_ShouldNotInsert()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;

            _task.ExecuteTask(Context);

            Assert.IsFalse(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenProductRanged_ShouldInsert()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            TestPlanogram.Assortment.Products.First().IsRanged = false;
            PlanogramAssortmentProduct rangedProduct = TestPlanogram.Assortment.Products.Last();

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == rangedProduct.Gtin));
        }

        [Test]
        public void TaskShouldInsertNonRangedProductsWhenPlacementIsNotRangedAndInRankOrder()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;
            SetTaskParameterValues(3, new Object[] { PlacementOrderType.NotRangedInRankOrder });

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }

        [Test]
        public void TaskShouldNeverInsertNonSequencedProductsWhenTheirSequenceGroupIsNotInPresentBlocking()
        {
            SetTestPlanogram();
            IEnumerable<ProductDto> dtos = _nonBlockedSequenceProducts.Take(1);
            TestPlanogram.Assortment.AddAssortmentProducts(dtos);
            PlanogramAssortmentProduct nonBlockedSequencedProduct = TestPlanogram.Assortment.Products.First();
            nonBlockedSequencedProduct.IsRanged = true;
            SetTaskParameterValues(3, new Object[] { PlacementOrderType.RangedInRankOrder });

            _task.ExecuteTask(Context);

            Assert.IsFalse(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonBlockedSequencedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenProductIsOptionalAndNotRanged_ShouldInsert()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;
            nonRangedProduct.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Optional;
            SetTaskParameterValues(3, new Object[] { PlacementOrderType.IsOptionalAndNotRangedInRankOrder });

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenProductNotRanged_ShouldInsert2()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;
            SetTaskParameterValues(3, new Object[] { PlacementOrderType.InAssortmentRankOrder });

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenSingleFacing_ShouldInsertOneFacing()
        {
            const String expectation = "There should be the right number of facings.";
            const Int16 expected = 1;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = 10 * expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleUnit });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            if (insertedPosition == null) Assert.Inconclusive("There should be at least a position inserted.");

            Int16 actual = insertedPosition.FacingsWide;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void TaskExecution_WhenAssortmentFacing_ShouldInsertAssortmentFacings()
        {
            const String expectation = "There should be the right number of facings.";
            const Int32 expected = 14;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.AssortmentUnits });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            if (insertedPosition == null) Assert.Inconclusive("There should be at least a position inserted.");

            Int32 actual = insertedPosition.TotalUnits;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void TaskExecution_WhenAssortmentFacingButDoesNotFit_ShouldNotInsert()
        {
            const String expectation = "There should be no insert if the right number of facings does not fit.";
            const Int16 expected = 300;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.AssortmentUnits });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            Assert.IsNull(insertedPosition, expectation);
        }

        [Test]
        public void TaskExecution_WhenCloseAssortmentFacingButDoesNotFit_ShouldInsertCloseToAssortmentFacing()
        {
            const String expectation = "There should be close to assortment facings insert if the right number of facings does not fit.";
            const Int32 expected = 168;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = 5 * expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.CloseToAssortmentUnits });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            if (insertedPosition == null) Assert.Inconclusive("There should be at least a position inserted.");

            Int32 actual = insertedPosition.TotalUnits;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void TaskExecution_PegboardDoesNotOverfill()
        {
            SetProductSequences();
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent pegboard = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            PlanogramSubComponent pegboardSubComponent = pegboard.GetPlanogramComponent().SubComponents.First();
            pegboardSubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboardSubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            const Int16 rows = 4;
            const Int16 columns = 6;
            for (Int16 sequenceY = 1; sequenceY < columns; sequenceY++)
            {
                for (Int16 sequenceX = 1; sequenceX < rows; sequenceX++)
                {
                    Int16 current = Convert.ToInt16(sequenceX + rows * (sequenceY - 1));
                    PlanogramPosition position = pegboard.AddPosition(bay, TestPlanogram.AddProduct(_startingProducts[Convert.ToInt32(current)]));
                    position.Sequence = current;
                    position.SequenceX = sequenceX;
                    position.SequenceY = sequenceY;
                    position.SequenceZ = 1;
                }
            }
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.CloseToAssortmentUnits });

            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            Boolean atLeastOneGroupOverfills;
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                atLeastOneGroupOverfills = merchandisingGroups.Any(
                    merchandisingGroup => merchandisingGroup.PositionPlacements.First().GetAvailableSpaceOnAxis(AxisType.X).LessThan(0));
            }
            Assert.IsFalse(atLeastOneGroupOverfills);
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(5, new Object[] { AddCarParkTextBoxType.Yes });



            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(3).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);

            var block = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramSequenceGroup greenSequence = TestPlanogram.AddSequenceGroup(block, new List<PlanogramProduct>());

            greenSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0],
                                         _largeProductDtos[1], 
                                         _largeProductDtos[2], 
                                         _largeProductDtos[3], 
                                         _largeProductDtos[4], 
                                         _largeProductDtos[5], 
                                         _largeProductDtos[6], 
                                         _largeProductDtos[7], 
                                         _largeProductDtos[8], 
                                         _largeProductDtos[9] 
                                     });

            TestPlanogram.UpdatePositionSequenceData();

            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(4).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AddToCarParkComponent });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();



            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(5, new Object[] { AddCarParkTextBoxType.No });

            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(3).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);

            var block = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramSequenceGroup greenSequence = TestPlanogram.AddSequenceGroup(block, new List<PlanogramProduct>());

            greenSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0],
                                         _largeProductDtos[1], 
                                         _largeProductDtos[2], 
                                         _largeProductDtos[3], 
                                         _largeProductDtos[4], 
                                         _largeProductDtos[5], 
                                         _largeProductDtos[6], 
                                         _largeProductDtos[7], 
                                         _largeProductDtos[8], 
                                         _largeProductDtos[9] 
                                     });

            TestPlanogram.UpdatePositionSequenceData();

            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(4).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AddToCarParkComponent });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();


            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }

        #region Assortment Rule Priority Enforcement

        [Test]
        public void BetterTreatmentProductWithLowerRankIsAddedInsteadOfWorseTreatmentProductWithHigherRank(
            [ValueSource("_worseBeforeBetterTreatmentTypes")] Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>
                treatmentTypeTuple)
        {
            List<ProductDto> startingProducts = _startingProducts.Take(11).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>> {startingProducts});
            SetTaskParameterValues(0, new Object[] {DroppedProductBehaviourType.DoNotAdd});
            SetTaskParameterValues(1, new Object[] {PlacementType.SingleUnit});
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_startingProducts);
            IEnumerable<ProductDto> assortmentProducts = _startingProducts.Skip(11).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(assortmentProducts);
            PlanogramAssortmentProductList assortmentProductList = TestPlanogram.Assortment.Products;
            PlanogramAssortmentProduct worseHighRankProduct = assortmentProductList.First();
            PlanogramAssortmentProduct betterLowRankProduct = assortmentProductList.Last();
            foreach (PlanogramAssortmentProduct product in assortmentProductList)
            {
                product.ProductTreatmentType = treatmentTypeTuple.Item1;
            }
            betterLowRankProduct.ProductTreatmentType = treatmentTypeTuple.Item2;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionsGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionsGtins.Should()
                          .ContainInOrder(startingProducts.Select(product => product.Gtin),
                                          "original products should not be affected")
                          .And.NotContain(worseHighRankProduct.Gtin,
                                          "a high ranking product should be skipped in favor of a low ranking product with better ProductTreatmentType")
                          .And.Contain(betterLowRankProduct.Gtin,
                                       "a low rank product should force skipping other high rank products with worse ProductTreatmentTypes");
        }

        [Test]
        public void WorseTreatmentProductIsNotPlacedBeforeBetterTreatmentProductWithHigherRank([ValueSource("_worseBeforeBetterTreatmentTypes")] Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> treatmentTypeTuple)
        {
            List<ProductDto> startingProducts = _startingProducts.Take(11).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>> { startingProducts });
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleUnit });
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_startingProducts);
            IEnumerable<ProductDto> assortmentProducts = _startingProducts.Skip(11).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(assortmentProducts);
            PlanogramAssortmentProductList assortmentProductList = TestPlanogram.Assortment.Products;
            PlanogramAssortmentProduct betterHighRankProduct = assortmentProductList.First();
            PlanogramAssortmentProduct worseLowRankProduct = assortmentProductList.Last();
            foreach (PlanogramAssortmentProduct product in assortmentProductList)
            {
                product.ProductTreatmentType = treatmentTypeTuple.Item2;
            }
            worseLowRankProduct.ProductTreatmentType = treatmentTypeTuple.Item1;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(startingProducts.Select(product => product.Gtin),
                                         "original products should not be affected")
                         .And.NotContain(worseLowRankProduct.Gtin,
                                         "a low ranking product should never force skipping a high ranking product of equal or better ProductTreatmentType")
                         .And.Contain(betterHighRankProduct.Gtin,
                                      "a high rank product with better treatment should not be skipped for a worse one with lower rank");
        }

        [Test]
        public void HighRankProductWithLowerPriorityThanProductRuleIsDroppedForLowRankProductWithProductRule([ValueSource("_productTreatmentTypesWithLowerPriorityThanProductRule")] PlanogramAssortmentProductTreatmentType worseProductTreatmentType)
        {
            List<ProductDto> startingProducts = _startingProducts.Take(11).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>> { startingProducts });
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleUnit });
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_startingProducts);
            IEnumerable<ProductDto> assortmentProducts = _startingProducts.Skip(11).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(assortmentProducts);
            PlanogramAssortmentProductList assortmentProductList = TestPlanogram.Assortment.Products;
            foreach (PlanogramAssortmentProduct product in assortmentProductList)
            {
                product.ProductTreatmentType = worseProductTreatmentType;
            }
            PlanogramAssortmentProduct highRankProductWithLowerPriority = assortmentProductList.First();
            PlanogramAssortmentProduct lowRankProductWithHigherPriority = assortmentProductList.Last();
            lowRankProductWithHigherPriority.MinListUnits = 2;
            lowRankProductWithHigherPriority.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Optional;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(startingProducts.Take(11).Select(product => product.Gtin),
                                         "original products should not be affected")
                         .And.NotContain(highRankProductWithLowerPriority.Gtin,
                                         "a high ranking product with Treatment Type lower in priority than Product Rules should be skipped in favor of a low ranking product with a Product Rule")
                         .And.Contain(lowRankProductWithHigherPriority.Gtin,
                                      "a low rank product with product rules should force skipping other high rank products with Treatment Type lower in priority than Product Rules");
        }

        [Test]
        public void LowRankProductWithProductRuleDoesNotForceDropHighRankProductWithHigherPriorityThanProductRule([ValueSource("_productTreatmentTypesWithHigherPriorityThanProductRule")] PlanogramAssortmentProductTreatmentType betterProductTreatmentType)
        {
            List<ProductDto> startingProducts = _startingProducts.Take(11).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>> { startingProducts });
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleUnit });
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_startingProducts);
            IEnumerable<ProductDto> assortmentProducts = _startingProducts.Skip(11).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(assortmentProducts);
            PlanogramAssortmentProductList assortmentProductList = TestPlanogram.Assortment.Products;
            foreach (PlanogramAssortmentProduct product in assortmentProductList)
            {
                product.ProductTreatmentType = betterProductTreatmentType;
            }
            PlanogramAssortmentProduct highRankProductWithHigherPriority = assortmentProductList.First();
            PlanogramAssortmentProduct lowRankProductWithLowerPriority = assortmentProductList.Last();
            lowRankProductWithLowerPriority.MinListUnits = 2;
            lowRankProductWithLowerPriority.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Optional;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(startingProducts.Take(11).Select(product => product.Gtin),
                                         "original products should not be affected")
                         .And.NotContain(lowRankProductWithLowerPriority.Gtin,
                                         "a low rank product with Product Rule should not force dropping of a high rank product with Treatment Type higher in priority than Product Rules")
                         .And.Contain(highRankProductWithHigherPriority.Gtin,
                                      "a high rank product with Treatment Type higher in priority than Product Rules should not be dropped for a low rank product with Product Rule");
        }

        [Test]
        public void HighRankProductWithFamilyRuleIsDroppedForLowRankProductWithProductRule()
        {
            List<ProductDto> startingProducts = _startingProducts.Take(11).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>> { startingProducts });
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleUnit });
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_startingProducts);
            IEnumerable<ProductDto> assortmentProducts = _startingProducts.Skip(11).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(assortmentProducts);
            PlanogramAssortmentProductList assortmentProductList = TestPlanogram.Assortment.Products;
            foreach (PlanogramAssortmentProduct product in assortmentProductList.Take(11))
            {
                product.FamilyRuleName = "First 12 family";
                product.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.MinimumProductCount;
                product.FamilyRuleValue = 2;
            }
            PlanogramAssortmentProduct highRankProductWithFamilyRule = assortmentProductList.First();
            PlanogramAssortmentProduct lowRankProductWithProductRule = assortmentProductList.Last();
            lowRankProductWithProductRule.MinListUnits = 2;

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("FamilyRuleDroppedForProductRule");

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(startingProducts.Take(11).Select(product => product.Gtin),
                                         "original products should not be affected")
                         .And.NotContain(highRankProductWithFamilyRule.Gtin,
                                         "a high ranking product with Family Rules should be dropped in favor of a low ranking product with Product Rules")
                         .And.Contain(lowRankProductWithProductRule.Gtin,
                                      "a low rank product with Product Rules should force dropping other high rank product with Family Rules");
        }

        #endregion

        #region Task Logging Unit Tests

        [Test]
        public void TaskShouldLogInsufficientContentErrorWhenBlockingIsMissing()
        {
            SetTestPlanogram();
            TestPlanogram.Blocking.Clear();
            IEnumerable<ProductDto> dtos = _nonBlockedSequenceProducts.Take(1);
            TestPlanogram.Assortment.AddAssortmentProducts(dtos);
            PlanogramAssortmentProduct nonBlockedSequencedProduct = TestPlanogram.Assortment.Products.First();
            nonBlockedSequencedProduct.IsRanged = true;
            SetTaskParameterValues(3, new Object[] {PlacementOrderType.RangedInRankOrder});

            _task.ExecuteTask(Context);

            List<PlanogramEventLog> errorEventLogs = TestPlanogram.EventLogs.Where(log => log.EntryType == PlanogramEventLogEntryType.Error).ToList();
            Assert.That(errorEventLogs, Has.Count.EqualTo(1), "There should be one Error event log, and 0 were found.");
            Assert.That(errorEventLogs.First().Description,
                        Is.EqualTo("Insufficient Planogram content (Blocking Strategy)"),
                        "The Error event description was not the expected one.");
        }

        #endregion

        #region Case Tests

        [Test]
        public void TaskExecution_InsertProductA()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            SetTaskParameterValues(4, new Object[] { SpaceConstraintType.BeyondBlockSpace });
            //TestPlanogram.TakeSnapshot("TaskExecution_InsertProductA_Before");
            _task.ExecuteTask(Context);

            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _startingProducts[9].Gtin, _startingProducts[17].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductB()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductC()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(3));

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            // both green and yellow block
            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin, _productsToInsert[2].Gtin, _startingProducts[3].Gtin, _startingProducts[4].Gtin, _startingProducts[5].Gtin, _startingProducts[6].Gtin, _startingProducts[7].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductD()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(4));

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            // green and orange
            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour || p.SequenceColour == TestPlanogram.Sequence.Groups[1].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin, _startingProducts[18].Gtin, _startingProducts[19].Gtin, _startingProducts[12].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            // green and orange
            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour || p.SequenceColour == TestPlanogram.Sequence.Groups[1].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin, _startingProducts[11].Gtin, _startingProducts[10].Gtin, _productsToInsert[3].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            // both green and yellow block
            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin, _productsToInsert[2].Gtin, _startingProducts[3].Gtin, _startingProducts[4].Gtin, _startingProducts[5].Gtin, _startingProducts[6].Gtin, _startingProducts[7].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductE()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(5));

            SetTaskParameterValues(4, new Object[] { SpaceConstraintType.BeyondBlockSpace });
            _task.ExecuteTask(Context);
            
            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            // green and orange, blue
            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin, _startingProducts[18].Gtin, _startingProducts[19].Gtin, _startingProducts[12].Gtin, _startingProducts[23].Gtin, _startingProducts[22].Gtin, _startingProducts[21].Gtin, _startingProducts[20].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            // green and orange, blue
            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin, _startingProducts[11].Gtin, _startingProducts[10].Gtin, _productsToInsert[3].Gtin, _startingProducts[13].Gtin, _startingProducts[14].Gtin, _productsToInsert[4].Gtin, _startingProducts[15].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            // green and yellow block
            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin, _productsToInsert[2].Gtin, _startingProducts[3].Gtin, _startingProducts[4].Gtin, _startingProducts[5].Gtin, _startingProducts[6].Gtin, _startingProducts[7].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertInCorrectSequence_Shelf(
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType ySequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 50, 75));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramPosition position = shelf1.AddPosition(bay,
                                                               TestPlanogram.Products[0]);

            PlanogramPosition position2 = shelf2.AddPosition(bay,
                                                               TestPlanogram.Products[1]);

            foreach (PlanogramSubComponent sub in TestPlanogram.Components.SelectMany(c => c.SubComponents))
            {
                sub.MerchandisingStrategyX = merchStrategy;
            }

            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });

            TestPlanogram.Blocking.First().Groups.First()
                .SetBlockingGroupPlacementTypes(xSequenceDirection,
                                                ySequenceDirection,
                                                PlanogramBlockingGroupPlacementType.FrontToBack);

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(TestPlanogram.Products);

            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>();
            if (xSequenceDirection.IsPositive() && !ySequenceDirection.IsPositive()) // LtR TtB
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }
            if (!xSequenceDirection.IsPositive() && !ySequenceDirection.IsPositive()) // RtL TtB
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (xSequenceDirection.IsPositive() && ySequenceDirection.IsPositive()) // LtR BtT
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (!xSequenceDirection.IsPositive() && ySequenceDirection.IsPositive()) // RtL BtT
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            // How you would visually see the sequence as...
            // LeftToRight, BottomToTop  (4,3,2)
            // LeftToRight, TopToBottom  (2,3,4)
            // RightToLeft, BottomToTop  (2,3,4)
            // RightToLeft, TopToBottom  (4,3,2)

            CollectionAssert.AreEqual(expected, actual);
        }

        private void WriteList(String caption, IEnumerable<String> items)
        {
            Debug.WriteLine(caption);
            foreach (String item in items)
            {
                Debug.WriteLine(item);
            }
        }

        [Test]
        public void TaskExecution_InsertInCorrectSequence_Peg(
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirectionSecondary)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            PlanogramSubComponent pegboardSubComponent = peg.GetPlanogramComponent().SubComponents.First();
            pegboardSubComponent.MerchandisingStrategyX = merchStrategy;
            pegboardSubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(110, 50, 10));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(30, 30, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(30, 30, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(30, 30, 10));

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                        PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(TestPlanogram.Products[1], planogramPositionPlacement, sequenceDirection == PlanogramBlockingGroupPlacementType.BottomToTop ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }

            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });

            TestPlanogram.Blocking.First().Groups.First()
                .SetBlockingGroupPlacementTypes(sequenceDirection,
                                                sequenceDirectionSecondary,
                                                PlanogramBlockingGroupPlacementType.FrontToBack);

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(TestPlanogram.Products);

            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = peg.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceY).ThenBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();

            List<String> expected = new List<String>();
            if (sequenceDirection.IsPositive() && !sequenceDirectionSecondary.IsPositive()) // BtT RtL
            {
                expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct4.Gtin, planogramProduct3.Gtin };
            }
            if (!sequenceDirection.IsPositive() && !sequenceDirectionSecondary.IsPositive()) // TtB RtL
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin, planogramProduct1.Gtin };
            }
            if (sequenceDirection.IsPositive() && sequenceDirectionSecondary.IsPositive()) // BtT LtR
            {
                expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }
            if (!sequenceDirection.IsPositive() && sequenceDirectionSecondary.IsPositive()) // TtB LtR
            {
                expected = new List<String>() { planogramProduct3.Gtin, planogramProduct4.Gtin, planogramProduct2.Gtin, planogramProduct1.Gtin };
            }

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_InsertInCorrectSequence_Chest(
            [Values(PlanogramBlockingGroupPlacementType.FrontToBack, PlanogramBlockingGroupPlacementType.BackToFront)] PlanogramBlockingGroupPlacementType zSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);

            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 65));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }

            foreach (PlanogramSubComponent sub in TestPlanogram.Components.SelectMany(c => c.SubComponents))
            {
                sub.MerchandisingStrategyX = merchStrategy;
            }

            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });

            TestPlanogram.Blocking.First().Groups.First()
                .SetBlockingGroupPlacementTypes(zSequenceDirection,
                                                xSequenceDirection,
                                                PlanogramBlockingGroupPlacementType.BottomToTop);

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(TestPlanogram.Products);

            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var positions = chest.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions();
            var actual = chest.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Where(p => p.SequenceX == ((xSequenceDirection == PlanogramBlockingGroupPlacementType.LeftToRight) ? 2 : 1)).OrderBy(p => p.SequenceZ).Select(p => p.GetPlanogramProduct().Gtin).ToList();

            List<String> expected = new List<String>();
            if (zSequenceDirection.IsPositive() && !xSequenceDirection.IsPositive()) // BtF RtL
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (!zSequenceDirection.IsPositive() && !xSequenceDirection.IsPositive()) // FtB RtL
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }
            if (zSequenceDirection.IsPositive() && xSequenceDirection.IsPositive()) // BtF LtR
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (!zSequenceDirection.IsPositive() && xSequenceDirection.IsPositive()) // FtB LtR
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequence_ShouldPlaceOnePosition()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;

            PlanogramPosition position1 = shelf1.AddPosition(bay,
                                                               TestPlanogram.Products[0]);

            PlanogramPosition position2 = shelf2.AddPosition(bay,
                                                               TestPlanogram.Products[1]);
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            TestPlanogram.CreateAssortmentFromProducts();
            
            MerchandisePlanogram();

            Planogram plan = TestPlanogram;

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);
            
            var bottomBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.25F)).GetPlanogramBlockingGroup();
            
            PlanogramSequenceGroup bottomSequence = TestPlanogram.AddSequenceGroup(bottomBlock, new List<PlanogramProduct>());

            bottomSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            bottomBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup topSequence = TestPlanogram.AddSequenceGroup(topBlock, new List<PlanogramProduct>());

            topSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            topBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);
            
            plan.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);
                        
            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct2.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        #endregion

        #region Test Helper Methods

        private void SetUpTestPlanogramOneShelfWithProducts()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            const Int16 columns = 6;
            for (Int16 sequenceX = 0; sequenceX < columns; sequenceX++)
            {
                Int16 current = sequenceX;
                PlanogramPosition position = shelf.AddPosition(bay,
                                                               TestPlanogram.AddProduct(
                                                                   _startingProducts[Convert.ToInt32(current)]));
                position.Sequence = current;
                position.SequenceX = sequenceX;
                position.SequenceY = 1;
                position.SequenceZ = 1;
            }
            SetProductSequences();
            MerchandisePlanogram();
        }

        private void SetUpTestPlanogramTwoShelvesWithProducts()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelfA = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            const Int16 columns = 6;
            for (Int16 sequenceX = 0; sequenceX < columns; sequenceX++)
            {
                Int16 current = sequenceX;
                PlanogramPosition position = shelfA.AddPosition(bay,
                                                               TestPlanogram.AddProduct(
                                                                   _startingProducts[Convert.ToInt32(current)]));
                position.Sequence = current;
                position.SequenceX = sequenceX;
                position.SequenceY = 1;
                position.SequenceZ = 1;
            }
            PlanogramFixtureComponent shelfB = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 30, 0));
            for (Int16 sequenceX = 0; sequenceX < columns; sequenceX++)
            {
                Int16 current = Convert.ToInt16(sequenceX + 6);
                PlanogramPosition position = shelfB.AddPosition(bay,
                                                               TestPlanogram.AddProduct(
                                                                   _startingProducts[Convert.ToInt32(current)]));
                position.Sequence = current;
                position.SequenceX = sequenceX;
                position.SequenceY = 1;
                position.SequenceZ = 1;
            }
            SetProductSequences();
            MerchandisePlanogram();
        }

        /// <summary>
        ///     A basic planogram with three shelves and 8 products each. They are all sequenced.
        /// </summary>
        private void SetTestPlanogram()
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                _startingProducts.Skip(16).Take(8),
                _startingProducts.Skip(8).Take(8),
                _startingProducts.Take(8)
            });
            SetProductSequences();
        }

        private void SetProductSequences()
        {
            Planogram plan = TestPlanogram;

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0.25f, 0, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.65f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);

            var greenBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup(); // Green
            var orangeBlock = blocking.Locations.First(l => l.X.EqualTo(0.25f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup(); // Orange
            var blueBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup(); // Blue
            var yellowBlock = blocking.Locations.First(l => l.X.EqualTo(0.25f) && l.Y.EqualTo(0.65f)).GetPlanogramBlockingGroup(); // Yellow

            // get products for green block
            PlanogramSequenceGroup greenSequence = TestPlanogram.AddSequenceGroup(greenBlock, new List<PlanogramProduct>());

            greenSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0], // non fitting product
                                         _startingProducts[16], // green 17
                                         _productsToInsert[0], // green A
                                         _startingProducts[17], // green 18
                                         _startingProducts[9], // green 10
                                         _productsToInsert[1], // green B
                                         _startingProducts[8], // green 9
                                         _startingProducts[0], // green 1
                                         _startingProducts[1], // green 2
                                         _startingProducts[2] // green 3
                                     });

            greenBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup orangeSequence = TestPlanogram.AddSequenceGroup(orangeBlock, new List<PlanogramProduct>());

            orangeSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[18], // orange 19
                                         _startingProducts[19], // orange 20
                                         _startingProducts[12], // orange 13
                                         _startingProducts[11], // orange 12
                                         _startingProducts[10], // orange 11
                                         _productsToInsert[3] // orange D
                                     });

            orangeBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup blueSequence = TestPlanogram.AddSequenceGroup(blueBlock, new List<PlanogramProduct>());

            blueSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[15], // blue 16
                                         _productsToInsert[4], // blue E
                                         _startingProducts[14], // blue 15
                                         _startingProducts[13], // blue 14
                                         _startingProducts[20], // blue 21
                                         _startingProducts[21], // blue 22
                                         _startingProducts[22], // blue 23
                                         _startingProducts[23] // blue 24                                     
                                     });

            blueBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.TopToBottom,
                                                        PlanogramBlockingGroupPlacementType.RightToLeft,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup yellowSequence = TestPlanogram.AddSequenceGroup(yellowBlock, new List<PlanogramProduct>());

            yellowSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[7], // yellow 8
                                         _startingProducts[6], // yellow 7
                                         _startingProducts[5], // yellow 6
                                         _startingProducts[4], // yellow 5
                                         _startingProducts[3], // yellow 4
                                         _productsToInsert[2] // yellow C
                                     });

            yellowBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.RightToLeft,
                                                        PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup nonBlockedSequence = TestPlanogram.AddSequenceGroup(null, new List<PlanogramProduct>());
            nonBlockedSequence.AddSequenceProducts(new List<ProductDto>
                                                   {
                                                       _startingProducts[15],
                                                       _nonBlockedSequenceProducts[0],
                                                       _startingProducts[7],
                                                       _productsToInsert[2]
                                                   });
            plan.UpdatePositionSequenceData();
        }

        private void AssertInsert(List<String> actual, List<Single> xPositions, List<String> expected)
        {
            foreach (PlanogramEventLog eventLog in TestPlanogram.EventLogs)
            {
                Debug.WriteLine(eventLog.Content);
            }
            Debug.Write(actual.First());
            foreach (String s in actual.Skip(1))
                Debug.Write(String.Format(@", {0}", s));
            Debug.WriteLine(String.Empty);
            Debug.Write(xPositions.First());
            foreach (Single s in xPositions.Skip(1))
                Debug.Write(String.Format(@", {0}", s));
            Debug.WriteLine(String.Empty);
            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AllItemsAreUnique(xPositions);
        }

        #endregion
    }
}