﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30730 : A.Kuszyk
//  Created.
// V8-30907 : A.Kuszyk
//  Added additional tests for place unplaced products parameter.
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added AddCarParkTextBox parameter
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using testingNs = Galleria.Ccm.Engine.Tasks.AddProductsUsingProductUniverse.v1;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.EngineTasks.AddProductsUsingProductUniverse.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<testingNs.Task>
    {
        private testingNs.Task _task;
        private ProductUniverse _productUniverse;

        [SetUp]
        public new void Setup()
        {
            base.Setup();
            _task = new testingNs.Task();
            Int32 hierarchyId = TestDataHelper.InsertProductHierarchyDtos(DalFactory, 1, Context.EntityId).First().Id;
            List<ProductLevelDto> levelList = TestDataHelper.InsertProductLevelDtos(DalFactory, hierarchyId, 10);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(DalFactory, levelList, 10);
            ProductUniverseDto productUniverseDto = TestDataHelper.InsertProductUniverseDtos(
                base.DalFactory, new List<Int32>() { Context.EntityId }, productGroupDtoList.Select(d => d.Id).ToList()).First();

            List<ProductDto> productDtos = TestDataHelper.InsertProductDtos(base.DalFactory, 20, Context.EntityId);
            TestDataHelper.InsertProductUniverseProductDtos(base.DalFactory, new List<ProductUniverseDto> { productUniverseDto }, productDtos);

            _productUniverse = ProductUniverse.FetchById(productUniverseDto.Id);

            Assert.AreNotEqual(0, _productUniverse.Products.Count, "Universe should have products.."); 

            // Product Universe parameter.
            base.SetTaskParameterValues(0, new Object[] { productUniverseDto.Name, productUniverseDto.Id });
            
            // Add to car park parameter.
            base.SetTaskParameterValues(1, new Object[] { testingNs.AddToCarParkType.No });

            // Add to car park component name parameter.
            base.SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });

            //Add carpark textbox
            SetTaskParameterValues(3, new Object[] { testingNs.AddCarParkTextBoxType.No });
        }

        [Test]
        public void ExecuteTask_AddsProductsFromUniverse()
        {
            _task.ExecuteTask(Context);

            CollectionAssert.AreEquivalent(
                _productUniverse.Products.Select(p => p.Gtin).ToList(),
                TestPlanogram.Products.Select(p => p.Gtin).ToList());
        }

        [Test]
        public void ExecuteTask_DoesntAddPositions_WhenAddCarParkIsNo()
        {
            _task.ExecuteTask(Context);

            CollectionAssert.IsEmpty(TestPlanogram.Positions);
        }

        [Test]
        public void ExecuteTask_AddsPositions_WhenAddCarParkIsYes()
        {
            base.SetTaskParameterValues(1, new Object[] { testingNs.AddToCarParkType.Yes });

            _task.ExecuteTask(Context);

            CollectionAssert.AreEquivalent(
                _productUniverse.Products.Select(p => p.Gtin).ToList(),
                TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList());
        }

        [Test]
        public void ExecuteTask_AddsPositionsForUnplacedProductsAlreadyInPlan_WhenPlaceUnplacedIsTrue()
        {
            base.SetTaskParameterValues(1, new Object[] { testingNs.AddToCarParkType.Yes });
            base.SetTaskParameterValues(3, new Object[] { testingNs.PlaceUnPlacedOnCarParkComponentType.Yes });
            IEnumerable<String> gtinsToAdd = _productUniverse.Products.Take(3).Select(p => p.Gtin).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.Contains(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_DoesntAddPositionsForPlacedProductsAlreadyInPlan_WhenPlaceUnplacedIsTrue()
        {
            base.SetTaskParameterValues(1, new Object[] { testingNs.AddToCarParkType.Yes });
            base.SetTaskParameterValues(3, new Object[] { testingNs.PlaceUnPlacedOnCarParkComponentType.Yes });
            IEnumerable<String> gtinsToAdd = _productUniverse.Products.Take(3).Select(p => p.Gtin).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            foreach (var p in TestPlanogram.Products) { shelf.AddPosition(bay, p); }
            var existingPositions = TestPlanogram.Positions.ToList();

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Except(existingPositions).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.DoesNotContain(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_DoesntAddPositionsForUnplacedProductsAlreadyInPlan_WhenPlaceUnplacedIsFalse()
        {
            base.SetTaskParameterValues(1, new Object[] { testingNs.AddToCarParkType.Yes });
            base.SetTaskParameterValues(3, new Object[] { testingNs.PlaceUnPlacedOnCarParkComponentType.No });
            IEnumerable<String> gtinsToAdd = _productUniverse.Products.Take(3).Select(p => p.Gtin).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.DoesNotContain(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameterValues(1, new Object[] { testingNs.AddToCarParkType.Yes });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(3, new Object[] { testingNs.PlaceUnPlacedOnCarParkComponentType.Yes });
            SetTaskParameterValues(4, new Object[] { testingNs.AddCarParkTextBoxType.Yes });

            TestPlanogram.Positions.Clear();
            TestPlanogram.Products.Clear();


            _task.ExecuteTask(Context);

            //check carpark shelf exists
            Assert.IsNotNull(TestPlanogram.Components.FirstOrDefault(c => c.Name == "Car Park Shelf"), "Should have a car park shelf.");

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameterValues(1, new Object[] { testingNs.AddToCarParkType.Yes });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(3, new Object[] { testingNs.PlaceUnPlacedOnCarParkComponentType.Yes });
            SetTaskParameterValues(4, new Object[] { testingNs.AddCarParkTextBoxType.No });

            IEnumerable<String> gtinsToAdd = _productUniverse.Products.Take(3).Select(p => p.Gtin).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }
    }
}
