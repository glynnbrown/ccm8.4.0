﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-13996 : A.Kuszyk
//  Created.
#endregion

#endregion

using FluentAssertions;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.UnitTests.EngineTasks.GetBlockingFromPlanogram.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.GetBlockingFromPlanogram.v1.Task>
    {
        private Galleria.Ccm.Engine.Tasks.GetBlockingFromPlanogram.v1.Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Ccm.Engine.Tasks.GetBlockingFromPlanogram.v1.Task();
        }

        [Test]
        public void ExecuteTask_CorrectlyNormalisesMetricProfile()
        {
            var entityId = Context.EntityId;
            var sourcePlan = "Package".CreatePackage().AddPlanogram();
            var sourceBlocking = sourcePlan.AddBlocking();
            sourceBlocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var sourceLeftBlock = sourceBlocking.Locations.First(l => l.X.EqualTo(0f)).GetPlanogramBlockingGroup();
            var sourceRightBlock = sourceBlocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            sourcePlan = sourcePlan.Parent.Save().Planograms.First();

            var prod1Performance = TestPlanogram.AddProduct().GetPlanogramPerformanceData();
            prod1Performance.P1 = 5;
            prod1Performance.P2 = 1;
            var prod2Performance = TestPlanogram.AddProduct().GetPlanogramPerformanceData();
            prod2Performance.P1 = 5;
            prod2Performance.P2 = 3;
            TestPlanogram.AddSequenceGroup(sourceLeftBlock, TestPlanogram.Products.Take(1));
            TestPlanogram.AddSequenceGroup(sourceRightBlock, TestPlanogram.Products.Skip(1));
            TestPlanogram.CreateAssortmentFromProducts();

            var metric1 = Metric.NewMetric(entityId);
            metric1.Name = "Metric1";
            metric1.Description = "Description";
            metric1.DataModelType = MetricElasticityDataModelType.None;
            metric1.SpecialType = MetricSpecialType.None;
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            var metric2 = Metric.NewMetric(entityId);
            metric2.Name = "Metric2";
            metric2.Description = "Description";
            metric2.DataModelType = MetricElasticityDataModelType.None;
            metric2.SpecialType = MetricSpecialType.None;
            metric2.Direction = MetricDirectionType.MaximiseIncrease;
            var metrics = MetricList.NewMetricList();
            metrics.AddRange(new[] { metric1, metric2 });
            metrics = metrics.Save();
            metric1 = metrics.First(m => m.Name.Equals(metric1.Name));
            metric2 = metrics.First(m => m.Name.Equals(metric2.Name));

            var planMetric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            planMetric1.Name = metric2.Name;
            var planMetric2 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(2);
            planMetric2.Name = metric1.Name;

            TestPlanogram.Performance.Metrics.Clear();
            TestPlanogram.Performance.Metrics.AddRange(new[] { planMetric1, planMetric2 });

            var metricProfile = MetricProfile.NewMetricProfile(entityId);
            metricProfile.Name = "Metric Profile";
            metricProfile.Metric1MetricId = metric1.Id;
            metricProfile.Metric1Ratio = 0f;
            metricProfile.Metric2MetricId = metric2.Id;
            metricProfile.Metric2Ratio = 1f;
            metricProfile.Save();

            SetTaskParameterValues(0,new Object[] { new TaskParameterValue(sourcePlan.Name, sourcePlan.Id) },true);
            SetTaskParameterValues(1, new Object[] { metricProfile.Name });

            _task.ExecuteTask(Context);

            TestPlanogram.Blocking.First(b => b.Type == PlanogramBlockingType.PerformanceApplied).Dividers.First().X
                .Should().Be(0.5f, "because the metric profile should be correctly mapped to the planogram metrics");
                
        }
    }
}
