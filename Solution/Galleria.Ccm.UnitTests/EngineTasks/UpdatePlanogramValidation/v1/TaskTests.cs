﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811

// V8-30255 : A.Silva
//  Created

#endregion

#endregion

using Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdatePlanogramValidation.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        /// <summary>
        ///     Task being tested.
        /// </summary>
        private Task _task;

        #endregion

        #region Test Helper Methods

        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        #endregion
    }
}