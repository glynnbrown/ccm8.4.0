﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-29281 : A.Silva
//      Created
// V8-29422 : A.Silva
//      Amended tests to account for IsRanged requisite. Removed obsolete tests.
// V8-29419 : A.Silva
//      Added TaskExecution_WhenAlwaysAdd_ShouldAddDroppedAnyway test.
// V8-29499 : A.Silva
//      Added ExecuteTask_WhenProductToInsertIsNotPlaced_ShouldNotAddToProductList
//            ExecuteTask_WhenProductToInsertIsNotSequenced_ShouldNotPlace
//            ExecuteTask_WhenProductToInsertIsNotPlacedButPreviouslyOnProductList_ShouldNotRemoveFromProductList
  
#endregion

#region Version History: CCM810

// V8-29741 : A.Silva
//  Extracted CreateProductDtos and NewDefaultProductDto to PlanogramTestHelper.
// V8-29902 : A.Silva
//  Refactored and amended for the changes to Car Park Shelf and Increase Units.
// V8-30003 : A.Silva
//  Refactored tests to test all components for overfill.
// V8-30141 : A.Silva
//  Refactored use of GetAvailableSpaceOnAxis.

#endregion

#region Version History: CCM820

// V8-30760 : D.Pleasance
//  Added new tests for task parameter PlacementOrderType
// V8-31176 : D.Pleasance
//  Added TaskExecution_InsertMultisited

#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32519 : L.Ineson
//  Added AddCarParkTextBox parameter
// V8-32882 : A.Kuszyk
//  Added test for sequence sub-group behaviour.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.InsertProductFromAssortment.v1;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using FluentAssertions;

namespace Galleria.Ccm.UnitTests.EngineTasks.InsertProductFromAssortment.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        private TaskBase _task;
        private List<ProductDto> _startingProducts;
        private List<ProductDto> _largeProductDtos;
        private List<ProductDto> _productsToInsert;
        private List<ProductDto> _outOfSequenceProduct;
        private List<ProductDto> _expectedTopShelf;
        private List<ProductDto> _expectedMiddleShelf;
        private List<ProductDto> _expectedLowerShelf;

        #endregion

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _task = new Task();
            _startingProducts = PlanogramTestHelper.CreateProductDtos(24, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _startingProducts);
            _largeProductDtos = PlanogramTestHelper.CreateProductDtos(24, Context.EntityId, 24, new WidthHeightDepthValue(35, 10, 10)).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _largeProductDtos);
            _productsToInsert = new[] {"AA", "BB", "CC", "DD", "EE"}.Select(gtin => PlanogramTestHelper.NewInsertedProductDto(gtin, Context.EntityId)).ToList();
            _outOfSequenceProduct = PlanogramTestHelper.CreateProductDtos(1, Context.EntityId, 30).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _productsToInsert);
        }

        #region Behavior Tests

        [Test]
        public void ExecuteTask_WhenAlwaysAddAndInsufficientSpaceForSubGroupProduct_ProductIsStillAddedToSubGroupComponent()
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var prod0 = TestPlanogram.AddProduct(new WidthHeightDepthValue(110, 10, 10));
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(110, 10, 10));
            shelf1.AddPosition(bay, prod0);
            shelf1.AddPosition(bay, prod1);
            shelf2.AddPosition(bay, prod3);
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.Sequence.Groups[0].SubGroups.Add(
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(
                    TestPlanogram.Sequence.Groups[0].Products.Take(3)));
            TestPlanogram.ReprocessAllMerchandisingGroups();
            TestPlanogram.UpdatePositionSequenceData();
            base.SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });

            _task.ExecuteTask(Context);

            GetShelfGtins(shelf1).Should().Equal(TestPlanogram.Products.Take(3).Select(p => p.Gtin), "the first three products (in a sub group) should be placed on the first shelf");
            GetShelfGtins(shelf2).Should().Equal(TestPlanogram.Products.Skip(3).Select(p => p.Gtin), "the last product should be placed by itself on the second shelf, despite it having spare white space");
        }

        private static IEnumerable<String> GetShelfGtins(PlanogramFixtureComponent shelf)
        {
            return shelf.GetPlanogramComponent().GetSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertCanBePlaced_ShouldPlace()
        {
            const String expectation = "The Planogram's Positions should contain one placement for the product to be inserted.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _productsToInsert.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            if (positionsContainOnlyOnePlacementForProduct) Assert.Inconclusive("The product to be inserted should not have already a placement.");

            _task.ExecuteTask(Context);

            positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            Assert.IsTrue(positionsContainOnlyOnePlacementForProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenPlacedProductIsNew_ShouldAddToProductList()
        {
            const String expectation = "The Planogram's Product List should contain the newly placed product.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _productsToInsert.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            String expectedGtin = productsToInsert.First().Gtin;
            Boolean productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            if (productListContainsNewPlacedProduct) Assert.Inconclusive("The product to be inserted should not exist previously in the Planogram's Product List.");

            _task.ExecuteTask(Context);

            productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            Assert.IsTrue(productListContainsNewPlacedProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertIsNotPlacedButPreviouslyOnProductList_ShouldNotRemoveFromProductList()
        {
            const String expectation = "The Planogram's Product List should still contain the non placed new product as it was there previously.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _outOfSequenceProduct.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            String expectedGtin = productsToInsert.First().Gtin;
            TestPlanogram.AddProduct(productsToInsert.First());
            Boolean productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            if (!productListContainsNewPlacedProduct) Assert.Inconclusive("The product to be inserted should exist previously in the Planogram's Product List.");

            _task.ExecuteTask(Context);

            productListContainsNewPlacedProduct = TestPlanogram.Products.Any(p => p.Gtin == expectedGtin);
            Assert.IsTrue(productListContainsNewPlacedProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertIsNotSequenced_ShouldNotPlace()
        {
            const String expectation = "The Planogram's Positions should not contain placements for the out of sequence product to be inserted.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _outOfSequenceProduct.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            if (positionsContainOnlyOnePlacementForProduct) Assert.Inconclusive("The product to be inserted should not have already a placement.");

            _task.ExecuteTask(Context);

            positionsContainOnlyOnePlacementForProduct =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            Assert.IsFalse(positionsContainOnlyOnePlacementForProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenProductToInsertIsAlreadyPlaced_ShouldNotPlace()
        {
            const String expectation = "The Planogram's Positions should contain only one placement for the product to be inserted.";
            SetUpTestPlanogramOneShelfWithProducts();
            IEnumerable<ProductDto> productsToInsert = _startingProducts.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionsContainOnlyOnePlacementForProduct = 
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            if (!positionsContainOnlyOnePlacementForProduct) Assert.Inconclusive("The product to be inserted should have already a placement.");

            _task.ExecuteTask(Context);

            positionsContainOnlyOnePlacementForProduct = 
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin) != null;
            Assert.IsTrue(positionsContainOnlyOnePlacementForProduct, expectation);
        }

        [Test]
        public void ExecuteTask_WhenValidAndInvalidComponentsForProductToInsert_ShouldPlaceOnValidOne()
        {
            const String expectation = "The product to be inserted should be on the valid component.";
            SetUpTestPlanogramTwoShelvesWithProducts();
            IEnumerable<ProductDto> productsToInsert = _productsToInsert.Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            Object expectedComponentId = TestPlanogram.GetPlanogramSubComponentPlacements().Last().SubComponent.Id;
            String placedGtin = productsToInsert.First().Gtin;
            Boolean positionIsOnExpectedComponent =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin && p.PlanogramSubComponentId == expectedComponentId) != null;
            if (positionIsOnExpectedComponent) Assert.Inconclusive("The product to be inserted should not have already a placement.");

            _task.ExecuteTask(Context);

            positionIsOnExpectedComponent =
                TestPlanogram.Positions.SingleOrDefault(p => p.GetPlanogramProduct().Gtin == placedGtin && p.PlanogramSubComponentId == expectedComponentId) != null;
            Assert.IsTrue(positionIsOnExpectedComponent, expectation);
        }

        #endregion

        [Test]
        public void TaskExecution_WhenProductNotPlaced_ShouldPlaceOnMerchandisingGroupWithMostSpace()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _startingProducts.Take(6).ToList();
            IEnumerable<ProductDto> productsOnSecondShelf = _startingProducts.Skip(7).Take(2).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf,
                productsOnSecondShelf
            });
            TestPlanogram.AddBlocking(1, true);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_startingProducts);
            IEnumerable<ProductDto> productsToInsert = _startingProducts.Skip(6).Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            IEnumerable<String> expected = TestPlanogram.GetSubComponentPlacementsFor(productsOnSecondShelf).Select(o => o.Component.Name).ToList();
            if (!expected.Any()) Assert.Inconclusive("Could not find the expected Merchandising Group");

            _task.ExecuteTask(Context);
            IEnumerable<String> actual = TestPlanogram.GetSubComponentPlacementsFor(productsToInsert).Select(o => o.Component.Name).ToList();

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductDoesNotFit_ShouldNotPlace()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            TestPlanogram.AddBlocking(1, true);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] {DroppedProductBehaviourType.DoNotAdd});

            _task.ExecuteTask(Context);
            IEnumerable<String> actual = TestPlanogram.GetSubComponentPlacementsFor(productsToInsert).Select(o => o.Component.Name).ToList();

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void TaskExecution_WhenAddToCarparkShelf_ShouldAddDroppedToCarpark()
        {
            const String carParkComponentName = "TestCarPark";
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            TestPlanogram.AddBlocking(1, true);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AddToCarParkComponent });
            SetTaskParameterValues(2, new Object[] { carParkComponentName });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);

            using (PlanogramMerchandisingGroupList merchandisingGroups = this.Context.Planogram.GetMerchandisingGroups())
            {
                PlanogramMerchandisingGroup carparkMerchGroup = merchandisingGroups
                    .FirstOrDefault(merchandisingGroup =>
                                    merchandisingGroup.SubComponentPlacements
                                                      .Any(subComponentPlacement =>
                                                           subComponentPlacement.SubComponent.Name == carParkComponentName));
                Assert.IsNotNull(carparkMerchGroup, "The planogram should contain a Car Park Shelf.");

                List<String> actual = carparkMerchGroup.PositionPlacements.Select(p => p.Product.Gtin).ToList();
                List<Single> xPositions = carparkMerchGroup.PositionPlacements.Select(pp => pp.Position.X).OrderBy(p => p).ToList();
                AssertInsert(actual, xPositions, expected);
            }
        }

        [Test]
        public void TaskExecution_WhenAlwaysAdd_ShouldAddDroppedAnyway()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            TestPlanogram.AddBlocking(1, true);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            PlanogramSubComponentPlacement lowerShelf = Context.Planogram.GetPlanogramSubComponentPlacements().First();
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = lowerShelf.GetPlanogramPositions().Select(p => p.GetPlanogramProduct().Gtin).ToList();

            CollectionAssert.IsSubsetOf(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNotRanged_ShouldNotInsert()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;

            _task.ExecuteTask(Context);

            Assert.IsFalse(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenProductRanged_ShouldInsert()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            TestPlanogram.Assortment.Products.First().IsRanged = false;
            PlanogramAssortmentProduct rangedProduct = TestPlanogram.Assortment.Products.Last();

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == rangedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenProductNotRanged_ShouldInsert()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;
            SetTaskParameterValues(3, new Object[] { PlacementOrderType.NotRangedInRankOrder });

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenProductIsOptionalAndNotRanged_ShouldInsert()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;
            nonRangedProduct.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Optional;
            SetTaskParameterValues(3, new Object[] { PlacementOrderType.IsOptionalAndNotRangedInRankOrder });

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }

        [Test]
        public void TaskExecution_WhenProductNotRanged_ShouldInsert2()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            PlanogramAssortmentProduct nonRangedProduct = TestPlanogram.Assortment.Products.First();
            nonRangedProduct.IsRanged = false;
            SetTaskParameterValues(3, new Object[] { PlacementOrderType.InAssortmentRankOrder });

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.Positions.Any(p => p.GetPlanogramProduct().Gtin == nonRangedProduct.Gtin));
        }
        
        [Test]
        public void TaskExecution_WhenSingleFacing_ShouldInsertOneFacing()
        {
            const String expectation = "There should be the right number of facings.";
            const Int16 expected = 1;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = 10*expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleUnit });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            if (insertedPosition == null) Assert.Inconclusive("There should be at least a position inserted.");

            Int16 actual = insertedPosition.FacingsWide;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void TaskExecution_WhenAssortmentFacing_ShouldInsertAssortmentFacings()
        {
            const String expectation = "There should be the right number of facings.";
            const Int32 expected = 14;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.AssortmentUnits });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            if (insertedPosition == null) Assert.Inconclusive("There should be at least a position inserted.");

            Int32 actual = insertedPosition.TotalUnits;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void TaskExecution_WhenAssortmentFacingButDoesNotFit_ShouldNotInsert()
        {
            const String expectation = "There should be no insert if the right number of facings does not fit.";
            const Int16 expected = 300;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.AssortmentUnits });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            Assert.IsNull(insertedPosition, expectation);
        }

        [Test]
        public void TaskExecution_WhenCloseAssortmentFacingButDoesNotFit_ShouldInsertCloseToAssortmentFacing()
        {
            const String expectation = "There should be close to assortment facings insert if the right number of facings does not fit.";
            const Int32 expected = 168;
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            TestPlanogram.Assortment.Products.First().Units = 5*expected;
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.CloseToAssortmentUnits });

            _task.ExecuteTask(Context);

            PlanogramPosition insertedPosition = TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin == _productsToInsert.First().Gtin);
            if (insertedPosition == null) Assert.Inconclusive("There should be at least a position inserted.");

            Int32 actual = insertedPosition.TotalUnits;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void TaskExecution_PegboardDoesNotOverfill()
        {
            SetProductSequences();
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent pegboard = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            PlanogramSubComponent pegboardSubComponent = pegboard.GetPlanogramComponent().SubComponents.First();
            pegboardSubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboardSubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            const Int16 rows = 4;
            const Int16 columns = 6;
            for (Int16 sequenceY = 1; sequenceY < columns; sequenceY++)
            {
                for (Int16 sequenceX = 1; sequenceX < rows; sequenceX++)
                {
                    Int16 current = Convert.ToInt16(sequenceX + rows * (sequenceY - 1));
                    PlanogramPosition position = pegboard.AddPosition(bay, TestPlanogram.AddProduct(_startingProducts[Convert.ToInt32(current)]));
                    position.Sequence = current;
                    position.SequenceX = sequenceX;
                    position.SequenceY = sequenceY;
                    position.SequenceZ = 1;
                }
            }
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert);
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            SetTaskParameterValues(1, new Object[] { PlacementType.CloseToAssortmentUnits });

            MerchandisePlanogram();
            
            _task.ExecuteTask(Context);

            Boolean atLeastOneGroupOverfills;
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                atLeastOneGroupOverfills = merchandisingGroups.Any(
                    merchandisingGroup => merchandisingGroup.PositionPlacements.First().GetAvailableSpaceOnAxis(AxisType.X).LessThan(0));
            }
            Assert.IsFalse(atLeastOneGroupOverfills);
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AddToCarParkComponent });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.Yes });


            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            TestPlanogram.AddBlocking(1, true);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();


            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameterValues(0, new Object[] { DroppedProductBehaviourType.AddToCarParkComponent });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.No });

            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            TestPlanogram.AddBlocking(1, true);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(_largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }

        #region Case Tests

        [Test]
        public void TaskExecution_InsertProductA()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            List<String> expected = _expectedLowerShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual =
                _productsToInsert.Take(1)
                                 .Select(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(
                                             p =>
                                             p.GetPlanogramPositions()
                                              .OrderBy(pp => pp.Sequence)
                                              .Select(pp => pp.GetPlanogramProduct().Gtin)
                                              .ToList())
                                         .ToList())
                                 .First();
            List<Single> xPositions =
                _productsToInsert.Take(1)
                                 .SelectMany(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(p => p.GetPlanogramPositions())
                                         .Select(pp => pp.X)
                                         .ToList())
                                 .OrderBy(p => p)
                                 .ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_InsertProductB()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            IEnumerable<ProductDto> expectedMiddleShelf =
                _expectedMiddleShelf.Except(new List<ProductDto> {_productsToInsert[3], _productsToInsert[4]});
            List<String> expected = expectedMiddleShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual =
                _productsToInsert.Skip(1)
                                 .Take(1)
                                 .Select(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(
                                             p =>
                                             p.GetPlanogramPositions()
                                              .OrderBy(pp => pp.Sequence)
                                              .Select(pp => pp.GetPlanogramProduct().Gtin)
                                              .ToList())
                                         .ToList())
                                 .First();
            List<Single> xPositions =
                _productsToInsert.Skip(1)
                                 .Take(1)
                                 .SelectMany(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(p => p.GetPlanogramPositions())
                                         .Select(pp => pp.X)
                                         .ToList())
                                 .OrderBy(p => p)
                                 .ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_InsertProductC()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(3));
            List<String> expected = _expectedTopShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual =
                _productsToInsert.Skip(2)
                                 .Take(1)
                                 .Select(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(
                                             p =>
                                             p.GetPlanogramPositions()
                                              .OrderBy(pp => pp.Sequence)
                                              .Select(pp => pp.GetPlanogramProduct().Gtin)
                                              .ToList())
                                         .ToList())
                                 .First();
            List<Single> xPositions =
                _productsToInsert.Skip(2)
                                 .Take(1)
                                 .SelectMany(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(p => p.GetPlanogramPositions())
                                         .Select(pp => pp.X)
                                         .ToList())
                                 .OrderBy(p => p)
                                 .ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_InsertProductD()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(4));
            List<String> expected =
                _expectedMiddleShelf.Except(new List<ProductDto> {_productsToInsert[4]})
                                    .Select(dto => dto.Gtin)
                                    .ToList();

            _task.ExecuteTask(Context);
            List<String> actual =
                _productsToInsert.Skip(3)
                                 .Take(1)
                                 .Select(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(
                                             p =>
                                             p.GetPlanogramPositions()
                                              .OrderBy(pp => pp.Sequence)
                                              .Select(pp => pp.GetPlanogramProduct().Gtin)
                                              .ToList())
                                         .ToList())
                                 .First();
            List<Single> xPositions =
                _productsToInsert.Skip(3)
                                 .Take(1)
                                 .SelectMany(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(p => p.GetPlanogramPositions())
                                         .Select(pp => pp.X)
                                         .ToList())
                                 .OrderBy(p => p)
                                 .ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_InsertProductE()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(5));
            List<String> expected = _expectedMiddleShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual =
                _productsToInsert.Skip(4)
                                 .Take(1)
                                 .Select(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(
                                             p =>
                                             p.GetPlanogramPositions()
                                              .OrderBy(pp => pp.Sequence)
                                              .Select(pp => pp.GetPlanogramProduct().Gtin)
                                              .ToList())
                                         .ToList())
                                 .First();
            List<Single> xPositions =
                _productsToInsert.Skip(4)
                                 .Take(1)
                                 .SelectMany(
                                     o => TestPlanogram.GetSubComponentPlacementsFor(new[] {o})
                                         .SelectMany(p => p.GetPlanogramPositions())
                                         .Select(pp => pp.X)
                                         .ToList())
                                 .OrderBy(p => p)
                                 .ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_IgnoresBlocking_Shelf(
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType ySequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60,0));
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(120,50,75));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramPosition position = shelf1.AddPosition(bay,TestPlanogram.Products[0]);
            PlanogramPosition position2 = shelf2.AddPosition(bay,TestPlanogram.Products[1]);
            foreach (var s in TestPlanogram.Components.SelectMany(c => c.SubComponents)) { s.MerchandisingStrategyX = merchStrategy; }
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f)});
            TestPlanogram.Blocking.First().Groups.First().SetBlockingGroupPlacementTypes(
                xSequenceDirection,ySequenceDirection,PlanogramBlockingGroupPlacementType.FrontToBack);
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = shelf2.GetPlanogramSubComponentPlacements()
                .First()
                .GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            List<String> expected = new List<String>();
            expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);
            
            // How you would visually see the sequence as...
            // LeftToRight, BottomToTop  (4,3,2)
            // LeftToRight, TopToBottom  (2,3,4)
            // RightToLeft, BottomToTop  (2,3,4)
            // RightToLeft, TopToBottom  (4,3,2)
            
            CollectionAssert.AreEqual(expected, actual);
        }

        private void WriteList(String caption, IEnumerable<String> items)
        {
            Debug.WriteLine(caption);
            foreach (String item in items)
            {
                Debug.WriteLine(item);
            }
        }

        [Test]
        public void TaskExecution_IgnoresBlocking_Peg(
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentYMerchStrategyType.BottomStacked, PlanogramSubComponentYMerchStrategyType.TopStacked)] PlanogramSubComponentYMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirectionSecondary)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            peg.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = merchStrategy;
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 50, 10));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(TestPlanogram.Products[1], planogramPositionPlacement, sequenceDirection == PlanogramBlockingGroupPlacementType.BottomToTop ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below);
                mg.Process(); mg.ApplyEdit();
            }
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Blocking.First().Groups.First().SetBlockingGroupPlacementTypes(
                sequenceDirection,sequenceDirectionSecondary,PlanogramBlockingGroupPlacementType.FrontToBack);
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = peg.GetPlanogramSubComponentPlacements()
                .First()
                .GetPlanogramPositions()
                .Where(p => p.SequenceY == (sequenceDirection == PlanogramBlockingGroupPlacementType.BottomToTop ? (Int16)2 : (Int16)1))
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            List<String> expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_IgnoresBlocking_Chest(
            [Values(PlanogramBlockingGroupPlacementType.FrontToBack, PlanogramBlockingGroupPlacementType.BackToFront)] PlanogramBlockingGroupPlacementType zSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 65));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                        PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(TestPlanogram.Products[1], planogramPositionPlacement, PlanogramPositionAnchorDirection.ToRight);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }
            foreach (PlanogramSubComponent sub in TestPlanogram.Components.SelectMany(c => c.SubComponents))
            {
                sub.MerchandisingStrategyX = merchStrategy;
            }
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Blocking.First().Groups.First().SetBlockingGroupPlacementTypes(
                zSequenceDirection, xSequenceDirection, PlanogramBlockingGroupPlacementType.BottomToTop);
            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = chest.GetPlanogramSubComponentPlacements()
                .First()
                .GetPlanogramPositions()
                .Where(p => p.SequenceX == 2)
                .OrderBy(p => p.SequenceZ)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            List<String> expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequence_ShouldPlaceOnePosition()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;

            PlanogramPosition position1 = shelf1.AddPosition(bay,
                                                               TestPlanogram.Products[0]);

            PlanogramPosition position2 = shelf2.AddPosition(bay,
                                                               TestPlanogram.Products[1]);
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            MerchandisePlanogram();

            TestPlanogram.CreateAssortmentFromProducts();
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequence_ShouldPlaceOnePositionBasedOnAvailableSpace()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0), new WidthHeightDepthValue(200, 4, 75));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;
            foreach (var shelf in new[] { shelf1, shelf2 })
            {
                shelf.AddPosition(bay, TestPlanogram.Products[0]);
                shelf.AddPosition(bay, TestPlanogram.Products[1]);
            }
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            MerchandisePlanogram();

            TestPlanogram.CreateAssortmentFromProducts();
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequenceAndAnchorUsesUpSpace_ShouldPlaceOnePositionBasedOnAvailableSpace()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;
            foreach (var shelf in new[] { shelf1, shelf2 })
            {
                shelf.AddPosition(bay, TestPlanogram.Products[0]);
                shelf.AddPosition(bay, TestPlanogram.Products[1]);
            }
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            MerchandisePlanogram();

            TestPlanogram.CreateAssortmentFromProducts();
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequenceAndAnchorDoesntUseUpSpace_ShouldPlaceOnePositionBasedOnAvailableSpace()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;
            foreach (var shelf in new[] { shelf1, shelf2 })
            {
                shelf.AddPosition(bay, TestPlanogram.Products[0]);
                shelf.AddPosition(bay, TestPlanogram.Products[1]);
            }
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[2], 
                                         products[1],
                                         products[0],
                                         products[3]
                                     });

            MerchandisePlanogram();

            TestPlanogram.CreateAssortmentFromProducts();
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct1.Gtin, planogramProduct4.Gtin, planogramProduct2.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        #endregion

        #region Test Helper Methods

        private void SetUpTestPlanogramOneShelfWithProducts()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            const Int16 columns = 6;
            for (Int16 sequenceX = 0; sequenceX < columns; sequenceX++)
            {
                Int16 current = sequenceX;
                PlanogramPosition position = shelf.AddPosition(bay,
                                                               TestPlanogram.AddProduct(
                                                                   _startingProducts[Convert.ToInt32(current)]));
                position.Sequence = current;
                position.SequenceX = sequenceX;
                position.SequenceY = 1;
                position.SequenceZ = 1;
            }
            SetProductSequences();
            MerchandisePlanogram();
        }

        private void SetUpTestPlanogramTwoShelvesWithProducts()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelfA = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            const Int16 columns = 6;
            for (Int16 sequenceX = 0; sequenceX < columns; sequenceX++)
            {
                Int16 current = sequenceX;
                PlanogramPosition position = shelfA.AddPosition(bay,
                                                               TestPlanogram.AddProduct(
                                                                   _startingProducts[Convert.ToInt32(current)]));
                position.Sequence = current;
                position.SequenceX = sequenceX;
                position.SequenceY = 1;
                position.SequenceZ = 1;
            }
            PlanogramFixtureComponent shelfB = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,30,0));
            for (Int16 sequenceX = 0; sequenceX < columns; sequenceX++)
            {
                Int16 current = Convert.ToInt16(sequenceX + 6);
                PlanogramPosition position = shelfB.AddPosition(bay,
                                                               TestPlanogram.AddProduct(
                                                                   _startingProducts[Convert.ToInt32(current)]));
                position.Sequence = current;
                position.SequenceX = sequenceX;
                position.SequenceY = 1;
                position.SequenceZ = 1;
            }
            SetProductSequences();
            MerchandisePlanogram();
        }

        private void SetOriginalTestPlanogram()
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                _startingProducts.Skip(16).Take(8),
                _startingProducts.Skip(8).Take(8),
                _startingProducts.Take(8)
            });
            TestPlanogram.AddBlocking(4, true);
            PlanogramBlockingGroupList blockingGroups = TestPlanogram.Blocking.First().Groups;
            blockingGroups[0].SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.FrontToBack);
            blockingGroups[1].SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.RightToLeft, PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.FrontToBack);
            blockingGroups[2].SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.FrontToBack);
            blockingGroups[3].SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.TopToBottom, PlanogramBlockingGroupPlacementType.RightToLeft, PlanogramBlockingGroupPlacementType.FrontToBack);
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(new List<ProductDto>
                                                  {
                                                      _startingProducts[16], // green 17
                                                      _productsToInsert[0], // green A
                                                      _startingProducts[17], // green 18
                                                      _startingProducts[9], // green 10
                                                      _productsToInsert[1], // green B
                                                      _startingProducts[8], // green 9
                                                      _startingProducts[0], // green 1
                                                      _startingProducts[1], // green 2
                                                      _startingProducts[2] // green 3
                                                  });
            TestPlanogram.Sequence.Groups[1].AddSequenceProducts(new List<ProductDto>
                                                  {
                                                      _startingProducts[7], // yellow 8
                                                      _startingProducts[6], // yellow 7
                                                      _startingProducts[5], // yellow 6
                                                      _startingProducts[4], // yellow 5
                                                      _startingProducts[3], // yellow 4
                                                      _productsToInsert[2] // yellow 3
                                                  });
            TestPlanogram.Sequence.Groups[2].AddSequenceProducts(new List<ProductDto>
                                                  {
                                                      _startingProducts[18], // orange 19
                                                      _startingProducts[10], // orange 11
                                                      _productsToInsert[3], // orange D
                                                      _startingProducts[11], // orange 12
                                                      _startingProducts[19], // orange 20
                                                      _startingProducts[12] // orange 13
                                                  });
            TestPlanogram.Sequence.Groups[3].AddSequenceProducts(new List<ProductDto>
                                                  {
                                                      _startingProducts[15], // blue 16
                                                      _startingProducts[23], // blue 24
                                                      _productsToInsert[4], // blue E
                                                      _startingProducts[22], // blue 23
                                                      _startingProducts[14], // blue 15
                                                      _startingProducts[11], // blue 12
                                                      _startingProducts[20] // blue 21
                                                  });
            _expectedTopShelf = new List<ProductDto>
            {
                _startingProducts[0], // green 1
                _startingProducts[1], // green 2
                _startingProducts[2], // green 3
                _productsToInsert[3], // orange D
                _productsToInsert[2], // yellow C
                _startingProducts[3], // yellow 4
                _startingProducts[4], // yellow 5
                _startingProducts[5], // yellow 6
                _startingProducts[6], // yellow 7
                _startingProducts[7] // yellow 8
            };
            _expectedMiddleShelf = new List<ProductDto>
            {
                _startingProducts[8], // green 9
                _startingProducts[9], // green 10
                _productsToInsert[1], // green B
                _startingProducts[10], // orange 11
                _startingProducts[11], // orange 12
                _startingProducts[12], // orange 13
                _startingProducts[13], // blue 14
                _startingProducts[14], // blue 15
                _productsToInsert[4], // blue E
                _startingProducts[15] // blue 16
            };
            _expectedLowerShelf = new List<ProductDto>
            {
                _startingProducts[16], // green 17
                _productsToInsert[0], // green A
                _startingProducts[17], // green 18
                _startingProducts[18], // orange 19
                _startingProducts[19], // orange 20
                _startingProducts[20], // blue 21
                _startingProducts[21], // blue 22
                _startingProducts[22], // blue 23
                _startingProducts[23] // blue 24
            };
        }

        private void SetTestPlanogram()
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                _startingProducts.Skip(16).Take(8),
                _startingProducts.Skip(8).Take(8),
                _startingProducts.Take(8)
            });
            SetProductSequences();
            _expectedTopShelf = new List<ProductDto>
            {
                _startingProducts[0],   // green 1
                _startingProducts[1],   // green 2
                _startingProducts[2],   // green 3
                _startingProducts[3],   // yellow 4
                _productsToInsert[2],   // yellow C
                _startingProducts[4],   // yellow 5
                _startingProducts[5],   // yellow 6
                _startingProducts[6],   // yellow 7
                _startingProducts[7]    // yellow 8
            };
            _expectedMiddleShelf = new List<ProductDto>
            {
                _startingProducts[8],   // green 9
                _startingProducts[9],   // green 10
                _productsToInsert[1],   // green B
                _startingProducts[10],  // orange 11
                _productsToInsert[3],   // orange D
                _startingProducts[11],  // orange 12
                _startingProducts[12],  // orange 13
                _startingProducts[13],  // blue 14
                _startingProducts[14],  // blue 15
                _startingProducts[15],  // blue 16
                _productsToInsert[4]    // blue E
            };
            _expectedLowerShelf = new List<ProductDto>
            {
                _startingProducts[16],  // green 17
                _productsToInsert[0],   // green A
                _startingProducts[17],  // green 18
                _startingProducts[18],  // orange 19
                _startingProducts[19],  // orange 20
                _startingProducts[20],  // blue 21
                _startingProducts[21],  // blue 22
                _startingProducts[22],  // blue 23
                _startingProducts[23]   // blue 24
            };
        }

        private void SetProductSequences()
        {
            TestPlanogram.AddBlocking(4, true);
            PlanogramBlockingGroupList blockingGroups = TestPlanogram.Blocking.First().Groups;
            blockingGroups[0]
                .SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.LeftToRight,
                                                PlanogramBlockingGroupPlacementType.BottomToTop,
                                                PlanogramBlockingGroupPlacementType.FrontToBack);
            blockingGroups[1]
                .SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.RightToLeft,
                                                PlanogramBlockingGroupPlacementType.BottomToTop,
                                                PlanogramBlockingGroupPlacementType.FrontToBack);
            blockingGroups[2]
                .SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.BottomToTop,
                                                PlanogramBlockingGroupPlacementType.LeftToRight,
                                                PlanogramBlockingGroupPlacementType.FrontToBack);
            blockingGroups[3]
                .SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.TopToBottom,
                                                PlanogramBlockingGroupPlacementType.RightToLeft,
                                                PlanogramBlockingGroupPlacementType.FrontToBack);
            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0], // non fitting product
                                         _startingProducts[16], // green 17
                                         _productsToInsert[0], // green A
                                         _startingProducts[17], // green 18
                                         _startingProducts[9], // green 10
                                         _productsToInsert[1], // green B
                                         _startingProducts[8], // green 9
                                         _startingProducts[0], // green 1
                                         _startingProducts[1], // green 2
                                         _startingProducts[2] // green 3
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[7], // yellow 8
                                         _startingProducts[6], // yellow 7
                                         _startingProducts[5], // yellow 6
                                         _startingProducts[4], // yellow 5
                                         _startingProducts[3], // yellow 4
                                         _productsToInsert[2] // yellow C
                                     });
            TestPlanogram.Sequence.Groups[2]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[18], // orange 19
                                         _startingProducts[19], // orange 20
                                         _startingProducts[12], // orange 13
                                         _startingProducts[11], // orange 12
                                         _startingProducts[10], // orange 11
                                         _productsToInsert[3] // orange D
                                     });
            TestPlanogram.Sequence.Groups[3]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[15], // blue 16
                                         _productsToInsert[4], // blue E
                                         _startingProducts[14], // blue 15
                                         _startingProducts[13], // blue 14
                                         _startingProducts[20], // blue 21
                                         _startingProducts[21], // blue 22
                                         _startingProducts[22], // blue 23
                                         _startingProducts[23] // blue 24
                                     });
        }

        private void AssertInsert(List<String> actual, List<Single> xPositions, List<String> expected)
        {
            foreach (PlanogramEventLog eventLog in TestPlanogram.EventLogs)
            {
                Debug.WriteLine(eventLog.Content);
            }
            Debug.Write(actual.First());
            foreach (String s in actual.Skip(1))
                Debug.Write(String.Format(@", {0}", s));
            Debug.WriteLine(String.Empty);
            Debug.Write(xPositions.First());
            foreach (Single s in xPositions.Skip(1))
                Debug.Write(String.Format(@", {0}", s));
            Debug.WriteLine(String.Empty);
            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AllItemsAreUnique(xPositions);
        }

        #endregion
    }
}