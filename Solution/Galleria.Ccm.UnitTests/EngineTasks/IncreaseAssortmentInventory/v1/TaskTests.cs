﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27946 : A.Kuszyk
//  Created.
#endregion

#region Version History: CCM803

// V8-29502 : A.Silva
//      Refactored the test to use up to date methods and extensions.
// V8-29280 : A.Silva
//      Rewriting of all tests.

#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Engine.Tasks.IncreaseAssortmentInventory.v1;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using FluentAssertions;

namespace Galleria.Ccm.UnitTests.EngineTasks.IncreaseAssortmentInventory.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        private Task _task;

        private readonly List<PlanogramComponentType> _componentTypes =
            Enum.GetValues(typeof(PlanogramComponentType)).Cast<PlanogramComponentType>().ToList().Where(p => p != PlanogramComponentType.Custom).ToList();

        private IEnumerable<ProcessingMethodType> _processingMethods =
            new List<ProcessingMethodType>()
            {
                ProcessingMethodType.SingleIncrementPerPass,
                ProcessingMethodType.MaximumIncreasePerPass
            };

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _task = new Task();
        }

        [Test]
        public void ExecuteTask_WhenEmptyPlanogram_ShouldNotChangePlanogram([ValueSource("_componentTypes")]PlanogramComponentType componentType)
        {
            TestPlanogram.AddFixtureItem().AddFixtureComponent(componentType);

            _task.ExecuteTask(Context);

            Assert.AreEqual(0, TestPlanogram.Positions.Sum(p => p.TotalUnits));
        }

        [SetUp]
        public void SetUp()
        {
            _task = new Task();
        }

        [Test]
        public void ExecuteTask_ShouldProcessBothProcessingMethods()
        {
            SetTaskParameterValues(0, new Object[] { 0.5f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.MaximumIncreasePerPass }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 4; i++)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.ReprocessAllMerchandisingGroups();
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            TestPlanogram.Positions.Should().OnlyContain(p => p.TotalUnits == 2, "because all products should have had their units increased");
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsToAssortmentUnits_WithOneBlock(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70)));
            shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70)));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 5;
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True, "All products should be placed");
            foreach (var position in TestPlanogram.Positions)
            {
                Assert.That(position.TotalUnits, Is.EqualTo(TestPlanogram.Assortment.Products.First(p => p.Gtin == position.GetPlanogramProduct().Gtin).Units));
            }
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsToAssortmentUnits_WithMultipleBlocks(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 6;
            TestPlanogram.AddBlocking().Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True, "All products should be placed");
            foreach (var position in TestPlanogram.Positions)
            {
                Assert.That(
                    position.TotalUnits,
                    Is.EqualTo(TestPlanogram.Assortment.Products.First(p => p.Gtin == position.GetPlanogramProduct().Gtin).Units),
                    String.Format("{0} did not achieve assortment inventory", position.GetPlanogramProduct().Gtin));
            }
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsCloseToAssortmentUnits_WithMultipleBlocks(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 100;
            TestPlanogram.AddBlocking().Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Positions.Count, Is.EqualTo(TestPlanogram.Products.Count), "There should be the same number of positions as products.");
            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True, "All products should be placed");
            foreach (var position in TestPlanogram.Positions)
            {
                if (processingMethod == ProcessingMethodType.SingleIncrementPerPass)
                {
                    Assert.That(position.TotalUnits, Is.EqualTo(8)); // 4 facings wide, 1 high, 2 deep.
                }
                else
                {
                    Assert.That(position.TotalUnits, Is.GreaterThanOrEqualTo(1));
                }
            }
        }

        [Test]
        public void ExecuteTask_DoesNotCorrectBadLayout(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(15, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True, "All products should be placed");
            foreach (var p in TestPlanogram.Positions) Assert.That(p.TotalUnits, Is.EqualTo(2), String.Format("{0} should have 2 units", p.GetPlanogramProduct().Gtin));
            var actualShelf1Products = shelf1.GetPlanogramSubComponentPlacements().First()
                .GetPlanogramPositions(orderBySequence: true).Select(p => p.GetPlanogramProduct()).ToList();
            var expectedShelf1Products = TestPlanogram.Products.Take(4).ToList();
            CollectionAssert.AreEqual(expectedShelf1Products, actualShelf1Products);
            var actualShelf2Products = shelf2.GetPlanogramSubComponentPlacements().First()
                .GetPlanogramPositions(orderBySequence: true).Select(p => p.GetPlanogramProduct()).ToList();
            var expectedShelf2Products = TestPlanogram.Products.Skip(4).ToList();
            CollectionAssert.AreEqual(expectedShelf2Products, actualShelf2Products);
        }

        [Test]
        public void ExecuteTask_PreservesPositions_WithGroupedBlocks(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 10, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 1;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).PlanogramBlockingGroupId = leftBlock.Id;
            TestPlanogram.Blocking.First().Groups.RemoveList(TestPlanogram.Blocking.First().Groups.Where(g => !g.GetBlockingLocations().Any()).ToList());
            var middleBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.3f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(2).Union(TestPlanogram.Products.Skip(4)));
            var middleSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(middleBlock);
            middleSequence.AddSequenceProducts(TestPlanogram.Products.Skip(2).Take(2));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, middleSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            Assert.That(TestPlanogram.Positions.All(p => p.TotalUnits.Equals(1)), "All positions should have one unit");
        }

        [Test]
        public void ExecuteTask_PreservesPositions_WithOnlyOneIncrease(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(15, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 1;
            TestPlanogram.Assortment.Products.First(ap => ap.Gtin.Equals(TestPlanogram.Products.First().Gtin)).Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Positions)
            {
                if (p.GetPlanogramProduct() == TestPlanogram.Products.First())
                {
                    Assert.That(p.TotalUnits, Is.EqualTo(2));
                }
                else
                {
                    Assert.That(p.TotalUnits, Is.EqualTo(1));
                }
            }
        }

        [Test]
        public void ExecuteTask_IncreasesInventoryIntoWhiteSpaceFromOneBlock(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 70);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            foreach (var ap in TestPlanogram.Assortment.Products.Take(4)) ap.Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Products.Take(4)) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.EqualTo(2), p.Gtin);
            foreach (var p in TestPlanogram.Products.Skip(4)) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.EqualTo(1), p.Gtin);
        }

        [Test]
        public void ExecuteTask_IncreasesInventoryIntoWhiteSpaceFromAllBlocks(
            [ValueSource("_processingMethods")] 
            ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 70);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Products) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.EqualTo(2), p.Gtin);
        }
    }
}
