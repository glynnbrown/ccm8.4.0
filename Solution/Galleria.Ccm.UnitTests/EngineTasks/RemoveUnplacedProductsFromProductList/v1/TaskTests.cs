﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27688 : J.Pickup
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.RemoveUnplacedProductsFromProductList.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Task = Galleria.Ccm.Engine.Tasks.RemoveUnplacedProductsFromProductList.v1.Task;

namespace Galleria.Ccm.UnitTests.EngineTasks.RemoveUnplacedProductsFromProductList.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        private TaskBase _task;

        #endregion

        [SetUp]
        public void SetUp()
        {
            CreatePlanogram(TestPlanogram);
            _task = new Task();
        }

        [Test]
        public void RemoveUnplacedProductsFromProductList_WhenContainsUnplacedProductsTheyAreRemoved()
        {
            Assert.IsTrue(TestPlanogram.Products.Count() == 2);
            _task.ExecuteTask(Context);
            Assert.IsTrue(TestPlanogram.Products.Count() == 1);
            Assert.IsTrue(TestPlanogram.Products.First().Gtin == "1");
        }


        public static void CreatePlanogram(Planogram testPlanogram)
        {
            var planogram = testPlanogram;
            planogram.Components.Add(PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, 120, 10, 75));
            planogram.Fixtures.Add(PlanogramFixture.NewPlanogramFixture());
            planogram.Fixtures.First().Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogram.Components.First()));
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(planogram.Fixtures.First()));

            PlanogramProduct prodA = PlanogramProduct.NewPlanogramProduct();
            prodA.Gtin = "1";
            PlanogramProduct prodB = PlanogramProduct.NewPlanogramProduct();
            prodB.Gtin = "2";

            planogram.Products.Add(prodA);
            planogram.Products.Add(prodB);

            PlanogramPosition positionA = PlanogramPosition.NewPlanogramPosition();
            positionA.PlanogramProductId = prodA.Id;

            planogram.Positions.Add(positionA);
        }

    }
}
