﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32522 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.RemoveComponentsOutsideOfFixtureArea.v1;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.EngineTasks.RemoveComponentsOutsideOfFixtureArea.v1
{
    /// <summary>
    ///     Unit Tests for the RemoveComponentsOutsideOfFixtureArea <see cref="Task"/>.
    /// </summary>
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        /// <summary>
        ///     Task being tested.
        /// </summary>
        private Task _task;

        private PlanogramFixtureComponent _outOfFixtureComponent;
        private PlanogramFixtureComponent _inFixtureComponent;
        private PlanogramFixtureAssembly _outOfFixtureAssembly;
        private PlanogramFixtureAssembly _inFixtureAssembly;

        #endregion

        #region Test Helper Methods

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        /// <summary>
        ///     Sets up a plan to test the use cases for the RemoveComponentsOutsideOfFixtureArea task.
        /// </summary>
        private void SetUpTestPlan()
        {
            _outOfFixtureComponent = TestPlanogram.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(-130, 10, 0));
            _outOfFixtureComponent.MetaIsOutsideOfFixtureArea = true;
            
            _inFixtureComponent = TestPlanogram.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
        
            _outOfFixtureAssembly = TestPlanogram.AddFixtureItem().AddFixtureAssembly();
            _outOfFixtureAssembly.AddAssemblyComponent(PlanogramComponentType.Shelf, new PointValue(-130, 10, 0)).MetaIsOutsideOfFixtureArea = true;
            _outOfFixtureAssembly.AddAssemblyComponent(PlanogramComponentType.Shelf, new PointValue(-130, 10, 0)).MetaIsOutsideOfFixtureArea = true;

            _inFixtureAssembly = TestPlanogram.AddFixtureItem().AddFixtureAssembly();
            _inFixtureAssembly.AddAssemblyComponent(PlanogramComponentType.Shelf).MetaIsOutsideOfFixtureArea = true;
            _inFixtureAssembly.AddAssemblyComponent(PlanogramComponentType.Shelf);
        }

        /// <summary>
        ///     Enumerates all the current fixture components in the planogram.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PlanogramFixtureComponent> GetActualFixtureComponents()
        {
            return TestPlanogram.Fixtures.SelectMany(fixture => fixture.Components).ToList();
        }

        private List<PlanogramFixtureAssembly> GetActualFixtureAssemblies()
        {
            return TestPlanogram.Fixtures.SelectMany(fixture => fixture.Assemblies).ToList();
        }

        #endregion

        [Test]
        public void RemoveComponentsOutsideOfFixtureAreaRemovesAllComponentsOutsideTheirFixtureArea()
        {
            const String expectation = "All components flagged as IsOutsideOfFixtureArea should have been removed.";
            SetUpTestPlan();

            _task.ExecuteTask(Context);

            CollectionAssert.DoesNotContain(GetActualFixtureComponents(), _outOfFixtureComponent, expectation);
        }

        [Test]
        public void RemoveComponentsOutsideOfFixtureAreaDoesNotRemoveAnyComponentInsideTheirFixtureArea()
        {
            const String expectation = "All components not flagged as IsOutsideOfFixtureArea should have been kept.";
            SetUpTestPlan();

            _task.ExecuteTask(Context);

            CollectionAssert.Contains(GetActualFixtureComponents(), _inFixtureComponent, expectation);
        }

        [Test]
        public void RemoveComponentsOutsideOfFixtureAreaRemovesAssembliesWithAllComponentsOutsideTheirFixtureArea()
        {
            const String expectation = "All assemblies with all components flagged as IsOutsideOfFixtureArea should have been removed.";
            SetUpTestPlan();

            _task.ExecuteTask(Context);

            CollectionAssert.DoesNotContain(GetActualFixtureAssemblies(), _outOfFixtureAssembly, expectation);
        }

        [Test]
        public void RemoveComponentsOutsideOfFixtureAreaDoesNotRemoveAssembliesWithSomeComponentsOutsideTheirFixtureArea()
        {
            const String expectation = "Assemblies with only some components flagged as IsOutsideOfFixtureArea should not have been removed.";
            SetUpTestPlan();

            _task.ExecuteTask(Context);

            CollectionAssert.Contains(GetActualFixtureAssemblies(), _inFixtureAssembly, expectation);
        }

        [Test]
        public void RemoveComponentsOutsideOfFixtureAreaLogsRemovedComponentsCount()
        {
            const String expectation = "There should be an event with the correct number of removed components.";
            SetUpTestPlan();

            _task.ExecuteTask(Context);

            PlanogramEventLog eventLog = TestPlanogram.EventLogs.FirstOrDefault(log => log.EventType == PlanogramEventLogEventType.Removal);
            Assert.IsNotNull(eventLog, "There should be an event of type Removal.");
            Assert.AreEqual(eventLog.Content, "2 Component(s) were removed from the Planogram.", expectation);
        }
    }
}