﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30733 : L.Luong
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.UpdateLocationProductAttributes.v1;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdateLocationProductAttributes.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        private Task _task;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        [Test]
        public void ExecuteTask_UpdatesAllLocationProductAttributes()
        {
            // Create master data and planogram product.
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, 1, Context.EntityId);

            List<Galleria.Ccm.Model.Product> products = Galleria.Ccm.Model.ProductList.FetchByEntityId(Context.EntityId).ToList();
            List<LocationProductAttributeDto> locationProduct = TestDataHelper.InsertLocationProductAttributeDtos(DalFactory, locationDtos, products, Context.EntityId);

            // add location
            Context.Planogram.LocationCode = locationDtos.FirstOrDefault().Code;
            Context.Planogram.LocationName = locationDtos.FirstOrDefault().Name;

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            SetTaskParameterValues(0, productDtos.Select(p => p.Gtin));

            foreach (PlanogramProduct planProduct in Context.Planogram.Products)
            {
                Assert.IsFalse(EqualsCheck(locationProduct.FirstOrDefault(l => l.GTIN == planProduct.Gtin), planProduct));
            }

            _task.ExecuteTask(Context);

            foreach (PlanogramProduct planProduct in Context.Planogram.Products)
            {
                Assert.IsTrue(EqualsCheck(locationProduct.FirstOrDefault(l => l.GTIN == planProduct.Gtin), planProduct));
            }
        }

        #region Test Helpers

        public Boolean EqualsCheck(LocationProductAttributeDto locationProductAttribute, PlanogramProduct product)
        {
            if (!product.CaseCost.Equals(locationProductAttribute.CaseCost)) return false;
            if (!product.CaseDeep.Equals(locationProductAttribute.CaseDeep.HasValue ? locationProductAttribute.CaseDeep.Value : (Byte)0)) return false;
            if (!product.CaseDepth.Equals(locationProductAttribute.CaseDepth.HasValue ? locationProductAttribute.CaseDepth.Value : 0)) return false;
            if (!product.CaseHeight.Equals(locationProductAttribute.CaseHeight.HasValue ? locationProductAttribute.CaseHeight.Value : 0)) return false;
            if (!product.CaseHigh.Equals(locationProductAttribute.CaseHigh.HasValue ? locationProductAttribute.CaseHigh.Value : (Byte)0)) return false;
            if (!product.CasePackUnits.Equals(locationProductAttribute.CasePackUnits.HasValue ? locationProductAttribute.CasePackUnits.Value : (Int16)0)) return false;
            if (!product.CaseWide.Equals(locationProductAttribute.CaseWide.HasValue ? locationProductAttribute.CaseWide.Value : (Byte)0)) return false;
            if (!product.CaseWidth.Equals(locationProductAttribute.CaseWidth.HasValue ? locationProductAttribute.CaseWidth.Value : 0)) return false;
            if (!product.ConsumerInformation.Equals(locationProductAttribute.ConsumerInformation != null ? locationProductAttribute.ConsumerInformation : "")) return false;
            if (!product.CorporateCode.Equals(locationProductAttribute.CorporateCode != null ? locationProductAttribute.CorporateCode : "")) return false;
            if (!product.CostPrice.Equals(locationProductAttribute.CostPrice)) return false;
            if (!product.DeliveryFrequencyDays.Equals(locationProductAttribute.DeliveryFrequencyDays)) return false;
            if (!product.DeliveryMethod.Equals(locationProductAttribute.DeliveryMethod != null ? locationProductAttribute.DeliveryMethod : "")) return false;
            if (!product.Depth.Equals(locationProductAttribute.Depth.HasValue ? locationProductAttribute.Depth.Value : 0)) return false;
            if (!product.ShortDescription.Equals(locationProductAttribute.Description != null ? locationProductAttribute.Description : "")) return false;
            if (!product.Gtin.Equals(locationProductAttribute.GTIN)) return false;
            if (!product.Height.Equals(locationProductAttribute.Height.HasValue ? locationProductAttribute.Height.Value : 0)) return false;
            if (!product.Manufacturer.Equals(locationProductAttribute.Manufacturer != null ? locationProductAttribute.Manufacturer : "")) return false;
            if (!product.ManufacturerCode.Equals(locationProductAttribute.ManufacturerCode != null ? locationProductAttribute.ManufacturerCode : "")) return false;
            if (!product.MaxDeep.Equals(locationProductAttribute.MaxDeep.HasValue ? locationProductAttribute.MaxDeep.Value : (Byte)0)) return false;
            if (!product.MinDeep.Equals(locationProductAttribute.MinDeep.HasValue ? locationProductAttribute.MinDeep.Value : (Byte)0)) return false;
            if (!product.Model.Equals(locationProductAttribute.Model != null ? locationProductAttribute.Model : "")) return false;
            if (!product.Pattern.Equals(locationProductAttribute.Pattern != null ? locationProductAttribute.Pattern : "")) return false;
            if (!product.RecommendedRetailPrice.Equals(locationProductAttribute.RecommendedRetailPrice)) return false;
            if (!product.SellPackCount.Equals(locationProductAttribute.SellPackCount)) return false;
            if (!product.SellPackDescription.Equals(locationProductAttribute.SellPackDescription != null ? locationProductAttribute.SellPackDescription : "")) return false;
            if (!product.SellPrice.Equals(locationProductAttribute.SellPrice)) return false;
            if (!product.ShelfLife.Equals(locationProductAttribute.ShelfLife.HasValue ? locationProductAttribute.ShelfLife.Value : (Int16)0)) return false;
            if (!product.Size.Equals(locationProductAttribute.Size != null ? locationProductAttribute.Size : "")) return false;
            if (!product.StatusType.Equals(locationProductAttribute.StatusType.HasValue ? (PlanogramProductStatusType)locationProductAttribute.StatusType.Value : (PlanogramProductStatusType)0)) return false;
            if (!product.TrayDeep.Equals(locationProductAttribute.TrayDeep.HasValue ? locationProductAttribute.TrayDeep.Value : (Byte)0)) return false;
            if (!product.TrayDepth.Equals(locationProductAttribute.TrayDepth.HasValue ? locationProductAttribute.TrayDepth.Value : 0)) return false;
            if (!product.TrayHeight.Equals(locationProductAttribute.TrayHeight.HasValue ? locationProductAttribute.TrayHeight.Value : 0)) return false;
            if (!product.TrayHigh.Equals(locationProductAttribute.TrayHigh.HasValue ? locationProductAttribute.TrayHigh.Value : (Byte)0)) return false;
            if (!product.TrayPackUnits.Equals(locationProductAttribute.TrayPackUnits.HasValue ? locationProductAttribute.TrayPackUnits.Value : (Int16)0)) return false;
            if (!product.TrayThickDepth.Equals(locationProductAttribute.TrayThickDepth.HasValue ? locationProductAttribute.TrayThickDepth.Value : 0)) return false;
            if (!product.TrayThickHeight.Equals(locationProductAttribute.TrayThickHeight.HasValue ? locationProductAttribute.TrayThickHeight.Value : 0)) return false;
            if (!product.TrayThickWidth.Equals(locationProductAttribute.TrayThickWidth.HasValue ? locationProductAttribute.TrayThickWidth.Value : 0)) return false;
            if (!product.TrayWide.Equals(locationProductAttribute.TrayWide.HasValue ? locationProductAttribute.TrayWide.Value : (Byte)0)) return false;
            if (!product.TrayWidth.Equals(locationProductAttribute.TrayWidth.HasValue ? locationProductAttribute.TrayWidth.Value : 0)) return false;
            if (!product.UnitOfMeasure.Equals(locationProductAttribute.UnitOfMeasure != null ? locationProductAttribute.UnitOfMeasure : "")) return false;
            if (!product.Vendor.Equals(locationProductAttribute.Vendor != null ? locationProductAttribute.Vendor : "")) return false;
            if (!product.VendorCode.Equals(locationProductAttribute.VendorCode != null ? locationProductAttribute.VendorCode : "")) return false;
            if (!product.Width.Equals(locationProductAttribute.Width.HasValue ? locationProductAttribute.Width.Value : 0)) return false;
          
            return true;
        }

        #endregion
    }
}
