﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31562 : D.Pleasance
//  Created.
#endregion

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdatePlanogramLocationSpace.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1.Task>
    {
        #region Fields

        Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1.Task _task;
        List<LocationSpaceBayDto> _locSpaceBayDtos = new List<LocationSpaceBayDto>();
        List<LocationSpaceElementDto> _locSpaceElementDtos = new List<LocationSpaceElementDto>();

        #endregion

        [SetUp]
        public void SetUp()
        {
            _task = new Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1.Task();
        }

        #region Task tests


        [Test]
        public void ExecuteTask_FailIfLocationSpaceElementDataMissing()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(1);

            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { true }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.Fail }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.EventLogs.Where(p => p.EventId == (Int32)EventLogEvent.LocationSpaceBayElementDataMissing).Count(),
                            "The event log should contain error that the location space element data is missing.");
        }

        [Test]
        public void ExecuteTask_FailIfBayProfileDoesNotMatch()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(2);
            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { false }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.Fail }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.EventLogs.Where(p => p.EventId == (Int32)EventLogEvent.PlanogramFixtureBayMismatchBetweenLocationSpaceData).Count(),
                            "The event log should contain error that the planogram fixture bay is not the same as the location space bay data.");
        }

        [Test]
        public void ExecuteTask_EnforceLocationSpace_CreateBays()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(2);
            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { false }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.EnforceLocationSpace }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            Assert.AreEqual(2, TestPlanogram.FixtureItems.Count, "There should be 2 fixture bays created");
            Int32 bayOrder = 1;
            foreach (PlanogramFixtureItem fixtureItem in TestPlanogram.FixtureItems.OrderBy(p => p.X))
            {
                PlanogramFixture bayFixture = fixtureItem.GetPlanogramFixture();
                LocationSpaceBayDto locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p => p.Order == bayOrder);

                ValidateFixtureBay(bayFixture, locationSpaceBayDto);
                bayOrder++;
            }
        }

        [Test]
        public void ExecuteTask_FailIfBayProfileDoesNotMatch_EnforceAndRemoveBays()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            var bay2 = TestPlanogram.AddFixtureItem();
            bay2.X = bay.GetPlanogramFixture().Width;
            var bay2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(1);
            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { false }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.EnforceLocationSpace }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.FixtureItems.Count, "There should only be 1 fixture bay");

            Int32 bayOrder = 1;
            foreach (PlanogramFixtureItem fixtureItem in TestPlanogram.FixtureItems.OrderBy(p => p.X))
            {
                PlanogramFixture bayFixture = fixtureItem.GetPlanogramFixture();
                LocationSpaceBayDto locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p => p.Order == bayOrder);

                ValidateFixtureBay(bayFixture, locationSpaceBayDto);
                bayOrder++;
            }
        }

        [Test]
        public void ExecuteTask_FailIfElementProfileDoesNotMatch()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            var bay2 = TestPlanogram.AddFixtureItem();
            bay2.X = bay.GetPlanogramFixture().Width;
            var bay2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(2, 2);
            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { true }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.Fail }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            // both bays have element mismatch
            Assert.AreEqual(2, TestPlanogram.EventLogs.Where(p => p.EventId == (Int32)EventLogEvent.PlanogramFixtureBayElementMismatchBetweenLocationSpaceData).Count(),
                            "The event log should contain error that the planogram fixture bay element is not the same as the location space bay element data.");
        }

        [Test]
        public void ExecuteTask_EnforceLocationSpace_CreateBaysAndElements()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            var bay2 = TestPlanogram.AddFixtureItem();
            bay2.X = bay.GetPlanogramFixture().Width;
            var bay2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(2, 5);
            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { true }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.EnforceLocationSpace }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            Assert.AreEqual(2, TestPlanogram.FixtureItems.Count, "There should be 2 fixture bays created");
            Int32 bayOrder = 1;

            foreach (PlanogramFixtureItem fixtureItem in TestPlanogram.FixtureItems.OrderBy(p => p.X))
            {
                PlanogramFixture bayFixture = fixtureItem.GetPlanogramFixture();
                LocationSpaceBayDto locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p => p.Order == bayOrder);

                ValidateFixtureBay(bayFixture, locationSpaceBayDto);
                bayOrder++;
                Int32 elementOrder = 1;

                foreach (PlanogramSubComponentPlacement planogramSubComponentPlacement in fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf).OrderBy(p => p.FixtureComponent.Y))
                {
                    LocationSpaceElementDto locationSpaceElementDto = _locSpaceElementDtos.FirstOrDefault(p => p.Order == elementOrder);

                    ValidateFixtureElement(planogramSubComponentPlacement, locationSpaceElementDto);
                    elementOrder++;
                }
            }
        }

        [Test]
        public void ExecuteTask_EnforceLocationSpace_CreateBaysAndElementsFromPreviousBay()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            
            CreateLocationSpaceData(2, 0);
            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { true }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.EnforceLocationSpace }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            Assert.AreEqual(2, TestPlanogram.FixtureItems.Count, "There should be 2 fixture bays created");
            Int32 bayOrder = 1;

            foreach (PlanogramFixtureItem fixtureItem in TestPlanogram.FixtureItems.OrderBy(p => p.X))
            {
                PlanogramFixture bayFixture = fixtureItem.GetPlanogramFixture();
                LocationSpaceBayDto locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p => p.Order == bayOrder);

                ValidateFixtureBay(bayFixture, locationSpaceBayDto);
                bayOrder++;

                Assert.AreEqual(3, fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf).Count(), 
                    "Although there was no location space element data there should be 3 shelves created from the previous bay.");
            }
        }

        [Test]
        public void ExecuteTask_EnforceLocationSpace_EnforceBaysAndRemoveElements()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            var bay2 = TestPlanogram.AddFixtureItem();
            bay2.X = bay.GetPlanogramFixture().Width;
            var bay2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(2, 1);
            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { true }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.EnforceLocationSpace }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            Assert.AreEqual(2, TestPlanogram.FixtureItems.Count, "There should be 2 fixture bays created");
            Int32 bayOrder = 1;

            foreach (PlanogramFixtureItem fixtureItem in TestPlanogram.FixtureItems.OrderBy(p => p.X))
            {
                PlanogramFixture bayFixture = fixtureItem.GetPlanogramFixture();
                LocationSpaceBayDto locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p => p.Order == bayOrder);

                ValidateFixtureBay(bayFixture, locationSpaceBayDto);
                bayOrder++;
                Int32 elementOrder = 1;

                Assert.AreEqual(1, fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf).Count(), "There should only be 1 element for each bay");

                foreach (PlanogramSubComponentPlacement planogramSubComponentPlacement in fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf).OrderBy(p => p.FixtureComponent.Y))
                {
                    LocationSpaceElementDto locationSpaceElementDto = _locSpaceElementDtos.FirstOrDefault(p => p.Order == elementOrder);

                    ValidateFixtureElement(planogramSubComponentPlacement, locationSpaceElementDto);
                    elementOrder++;
                }
            }
        }
                
        [Test]
        public void ExecuteTask_UpdateBayProfile_1Bay()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            
            CreateLocationSpaceData(1);

            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { false }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.Fail }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            PlanogramFixture bayFixture = bay.GetPlanogramFixture();
            LocationSpaceBayDto locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p=> p.Order==1);
            
            ValidateFixtureBay(bayFixture, locationSpaceBayDto);
        }

        [Test]
        public void ExecuteTask_UpdateBayProfile_2Bay()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            var bay2 = TestPlanogram.AddFixtureItem();
            bay2.X = bay.GetPlanogramFixture().Width;
            var bay2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bay2Shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(2);

            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { false }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.Fail }); // profile mis match behavior 
            _task.ExecuteTask(Context);

            PlanogramFixture bayFixture = bay.GetPlanogramFixture();
            LocationSpaceBayDto locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p => p.Order == 1);

            ValidateFixtureBay(bayFixture, locationSpaceBayDto);

            bayFixture = bay2.GetPlanogramFixture();
            locationSpaceBayDto = _locSpaceBayDtos.FirstOrDefault(p => p.Order == 2);

            ValidateFixtureBay(bayFixture, locationSpaceBayDto);
        }

        [Test]
        public void ExecuteTask_UpdateBayProfileAndShelfProfile_IgnoreShelfProfileAttributes()
        {
            //Validate that the shelf attributes were ignored when updating bays and shelfs. I.e.
            // the width should be the same as the bay parent width
            // and the other attributes should be the same as the original shelf

            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            PlanogramFixtureItem bay2 = TestPlanogram.AddFixtureItem();
            bay2.X = bay.GetPlanogramFixture().Width;
            PlanogramFixtureComponent bay2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent bay2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent bay2Shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);

            CreateLocationSpaceData(2, 3);

            SetTaskParameterValues(0, new Object[] { true }); // update bay profile
            SetTaskParameterValues(1, new Object[] { true }); // update element profile
            SetTaskParameterValues(2, new Object[] { FixtureProfileMismatchBehaviourType.Fail }); // profile mis match behavior 

            // Ignore Shelf Profile Attributes
            List<string> shelfProfileAttributes = new List<string>();

            foreach (ObjectFieldInfo attribute in LocationSpaceElement.EnumerateDisplayableFieldInfos(true).ToList())
            {
                shelfProfileAttributes.Add(attribute.PropertyName);
            }

            SetTaskParameterValues(3, shelfProfileAttributes); 

            Planogram testPlanogramCopy = TestPlanogram.Copy();

            _task.ExecuteTask(Context);

            Int32 bayOrder = 0;

            foreach (PlanogramFixtureItem fixtureItem in TestPlanogram.FixtureItems.OrderBy(p => p.X))
            {
                PlanogramFixture bayFixture = fixtureItem.GetPlanogramFixture();
   
                Int32 elementOrder = 0;

                foreach (PlanogramSubComponentPlacement planogramSubComponentPlacement in fixtureItem.GetPlanogramSubComponentPlacements().Where(p => p.Component.ComponentType == PlanogramComponentType.Shelf).OrderBy(p => p.FixtureComponent.Y))
                {
                    PlanogramComponent originalPlanogramComponent = testPlanogramCopy.Components[bayOrder * elementOrder];
                    PlanogramFixtureComponent originalFixtureComponent = testPlanogramCopy.Fixtures[bayOrder].Components[elementOrder];
                   
                    if (originalPlanogramComponent != null && originalFixtureComponent != null)
                    {
                        Assert.AreEqual(bayFixture.Width, planogramSubComponentPlacement.Component.Width, "The element component width should have been the same as the bay width.");
                        Assert.AreEqual(originalPlanogramComponent.Height, planogramSubComponentPlacement.Component.Height, "The element component height should have been ignored.");
                        Assert.AreEqual(originalPlanogramComponent.Depth, planogramSubComponentPlacement.Component.Depth, "The element component depth should have been ignored.");
                        //Assert.AreNotEqual(bayCopy.MetaTotalComponentsOverMerchandisedHeight.Value, fixtureItem.MetaTotalComponentsOverMerchandisedHeight.Value, "The element component depth should have been updated.");
                        Assert.AreEqual(originalFixtureComponent.Y, planogramSubComponentPlacement.FixtureItem.Y, "The element component depth should have been ignored.");
                    }

                    elementOrder++;
                }

                bayOrder++;

            }

        }
        #endregion

        #region Methods

        private void CreateLocationSpaceData(Int32 bayCount, Int32 elementCount = 0)
        {
            _locSpaceBayDtos.Clear();
            _locSpaceElementDtos.Clear();

            //merch hierarchy
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(DalFactory, 1, Context.EntityId);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(DalFactory, 1, 1);
            List<ProductGroupDto> prodGroupDtos = TestDataHelper.InsertProductGroupDtos(DalFactory, hierarchy1levelDtos, 1);

            //location hierarchy
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(DalFactory, 1, Context.EntityId);
            List<LocationLevelDto> locLevelsDtos = TestDataHelper.InsertLocationLevelDtos(DalFactory, locHierarchyDtos[0].Id, 1);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(DalFactory, locLevelsDtos, 1);
            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(DalFactory, Context.EntityId, locGroupDtos, 1);

            //location space
            List<LocationSpaceDto> locSpaceDtos = TestDataHelper.InsertLocationSpaceDtos(DalFactory, locDtos, Context.EntityId);

            List<LocationSpaceProductGroupDto> locSpaceProductGroupDtos = new List<LocationSpaceProductGroupDto>();
            foreach (LocationSpaceDto locationSpaceDto in locSpaceDtos)
            {
                locSpaceProductGroupDtos.AddRange(
                    TestDataHelper.InsertLocationSpaceProductGroupDtos(DalFactory, locationSpaceDto.Id, prodGroupDtos, bayCount));
            }

            foreach (LocationSpaceProductGroupDto locationSpaceProductGroupDto in locSpaceProductGroupDtos)
            {
                _locSpaceBayDtos.AddRange(TestDataHelper.InsertLocationSpaceBayDtos(DalFactory, locationSpaceProductGroupDto.Id, bayCount, 100));
            }

            if (elementCount > 0)
            {
                foreach (LocationSpaceBayDto locationSpaceBayDto in _locSpaceBayDtos)
                {
                    _locSpaceElementDtos.AddRange(TestDataHelper.InsertLocationSpaceElementDtos(DalFactory, locationSpaceBayDto.Id, elementCount, 100));
                }
            }

            TestPlanogram.LocationCode = locDtos.First().Code;
            TestPlanogram.CategoryCode = prodGroupDtos.First().Code;
        }

        private static void ValidateFixtureBay(PlanogramFixture bay1Fixture, LocationSpaceBayDto locationSpaceBayDto)
        {
            Assert.AreEqual(locationSpaceBayDto.Width, bay1Fixture.Width, "The fixture width should have been updated.");
            Assert.AreEqual(locationSpaceBayDto.Height, bay1Fixture.Height, "The fixture height should have been updated.");
            Assert.AreEqual(locationSpaceBayDto.Depth, bay1Fixture.Depth, "The fixture depth should have been updated.");
        }

        private void ValidateFixtureElement(PlanogramSubComponentPlacement planogramSubComponentPlacement, LocationSpaceElementDto locationSpaceElementDto)
        {
            PlanogramComponent shelfComponent = planogramSubComponentPlacement.Component;
            PlanogramSubComponent shelfSubComponent = planogramSubComponentPlacement.SubComponent;
            PlanogramFixtureComponent shelfFixtureComponent = planogramSubComponentPlacement.FixtureComponent;

            Assert.AreEqual(locationSpaceElementDto.Width, shelfComponent.Width, "The element component width should have been updated.");
            Assert.AreEqual(locationSpaceElementDto.Height, shelfComponent.Height, "The element component  height should have been updated.");
            Assert.AreEqual(locationSpaceElementDto.Depth, shelfComponent.Depth, "The element component depth should have been updated.");

            Assert.AreEqual(locationSpaceElementDto.Width, shelfSubComponent.Width, "The element sub component width should have been updated.");
            Assert.AreEqual(locationSpaceElementDto.Height, shelfSubComponent.Height, "The element sub component height should have been updated.");
            Assert.AreEqual(locationSpaceElementDto.Depth, shelfSubComponent.Depth, "The element sub component depth should have been updated.");
            
            // position the shelf
            //shelfFixtureComponent.X = xPosition;
            //shelfFixtureComponent.Y = locationSpaceElement.Y == 0 ? yPosition : locationSpaceElement.Y;
            //shelfFixtureComponent.Z = locationSpaceElement.Z;
        }

        #endregion
    }
}