﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.Processes.ProductAttributeValidation;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using ProductAttributeComparisonResult = Galleria.Framework.Planograms.Model.ProductAttributeComparisonResult;
using Task = Galleria.Ccm.Engine.Tasks.ProductAttrbiuteValidation.V1.Task;

namespace Galleria.Ccm.UnitTests.EngineTasks.ProductAttributeValidation.V1
{
    [TestFixture]
    internal class TaskTests : TaskTestBase<Task>
    {
        [SetUp]
        public void SetUp()
        {
            List<string> productGtins = TestDataHelper.InsertProductDtos(DalFactory, 5, Context.EntityId).Select(dto => dto.Gtin).ToList();

            // Add all the Products to the assortment.
            TestPlanogram.Products.AddRange(
               ProductList.FetchByEntityIdProductGtins(Context.EntityId, productGtins)
               .Select(PlanogramProduct.NewPlanogramProduct));

            foreach (var p in TestPlanogram.Products)
            {
                p.Brand = "XXX";
            }
        }

        [Test]
        public void CheckThatEngineValidatesProductAttributes()
        {
            // Arrange
            var engineToTest = new AttributeValidationEngine();
            var testEntity = Entity.FetchById(Context.EntityId);
            var ExpectedPlanogram = "Test Planogram".CreatePackage().AddPlanogram();
            ExpectedPlanogram.Id = TestPlanogram.Id;

            var testAttribute = EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Brand]", "Brand of product", PlanogramItemType.Product);
            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute) testAttribute);
            ExpectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Select(product => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(product.Gtin, "[PlanogramProduct.Brand]", "text", "XXX")).ToList());

            // Act
            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);

            // Assert
            CollectionAssert.AreEqual(ExpectedPlanogram.ProductAttributeComparisonResult, TestPlanogram.ProductAttributeComparisonResult);
        }

        [Test]
        public void CheckThatEngineCanValidateWithMultipleAttributes()
        {
            // Arrange
            var engineToTest = new AttributeValidationEngine();
            var testEntity = Entity.FetchById(Context.EntityId);
            var expectedPlanogram = "Test Planogram".CreatePackage().AddPlanogram();
            expectedPlanogram.Id = TestPlanogram.Id;

            PlanogramProduct productToChange = TestPlanogram.Products.FirstOrDefault(p => p.Gtin.Equals("GTIN1", StringComparison.InvariantCultureIgnoreCase));
            productToChange.Height += 20;

            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Brand]", "Brand of product", PlanogramItemType.Product));
            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Height]", "Height of product", PlanogramItemType.Product));

            expectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Select(product => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(product.Gtin, "[PlanogramProduct.Brand]", "text", "XXX")).ToList());
            expectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Where(p => p.Gtin.Equals("GTIN1", StringComparison.OrdinalIgnoreCase)).Select(prod => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(prod.Gtin, "[PlanogramProduct.Height]", "101", "121")));

            // Act
            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);

            // Assert
            CollectionAssert.AreEquivalent(expectedPlanogram.ProductAttributeComparisonResult, TestPlanogram.ProductAttributeComparisonResult);
            
        }

        [Test]
        public void CheckThatEngineProducesTheRightResultsAfterAnComparisonAttributeChanges()
        {
            // Arrange
            var engineToTest = new AttributeValidationEngine();
            var testEntity = Entity.FetchById(Context.EntityId);
            var expectedPlanogram = "Test Planogram".CreatePackage().AddPlanogram();
            expectedPlanogram.Id = TestPlanogram.Id;

            PlanogramProduct productToChange = TestPlanogram.Products.FirstOrDefault(p => p.Gtin.Equals("GTIN1", StringComparison.InvariantCultureIgnoreCase));
            productToChange.Height += 20;
            productToChange.Width += 20;

            var brandAttribute =
                (EntityComparisonAttribute)
                    EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Brand]",
                        "Brand of product", PlanogramItemType.Product);

            var heightAttribute =
                (EntityComparisonAttribute)
                    EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Height]",
                        "Height of product", PlanogramItemType.Product);

            testEntity.ComparisonAttributes.Add(brandAttribute);
            testEntity.ComparisonAttributes.Add(heightAttribute);
            expectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Select(product => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(product.Gtin, "[PlanogramProduct.Brand]", "text", "XXX")).ToList());
            expectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Where(p => p.Gtin.Equals("GTIN1", StringComparison.OrdinalIgnoreCase)).Select(prod => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(prod.Gtin, "[PlanogramProduct.Width]", "102", "122")));

            // Act
            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);
            testEntity.ComparisonAttributes.Remove(heightAttribute);
            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Width]", "Width of product", PlanogramItemType.Product));
            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);

            // Assert
            CollectionAssert.AreEquivalent(expectedPlanogram.ProductAttributeComparisonResult, TestPlanogram.ProductAttributeComparisonResult);

        }

        [Test]
        public void CheckThatTaskCanRun()
        {
            TestDataHelper.InsertEntityComparisonAttributeDtos(DalFactory, 1, "[PlanogramProduct.Brand]", (Byte)PlanogramItemType.Product, "Brand Of Product");

            var expectedPlanogram = "Test Planogram".CreatePackage().AddPlanogram();
            expectedPlanogram.Id = TestPlanogram.Id;
            expectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Select(product => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(product.Gtin, "[PlanogramProduct.Brand]", "text", product.Brand)).ToList());

            var taskToTest = new Task();
            taskToTest.Processor = new AttributeValidationEngine();
            
            taskToTest.ExecuteTask(Context);
            CollectionAssert.AreEquivalent(expectedPlanogram.ProductAttributeComparisonResult, TestPlanogram.ProductAttributeComparisonResult);

        }

        [Test]
        public void CheckThatEngineReplacesTheExistingResultWhenRanMultipleTimes()
        {
            // Arrange
            var engineToTest = new AttributeValidationEngine();
            var testEntity = Entity.FetchById(Context.EntityId);
            var expectedPlanogram = "Test Planogram".CreatePackage().AddPlanogram();
            expectedPlanogram.Id = TestPlanogram.Id;

            PlanogramProduct productToChange = TestPlanogram.Products.FirstOrDefault(p => p.Gtin.Equals("GTIN1", StringComparison.InvariantCultureIgnoreCase));
            productToChange.Height += 20;

            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Brand]", "Brand of product", PlanogramItemType.Product));
            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Height]", "Height of product", PlanogramItemType.Product));

            expectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Select(product => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(product.Gtin, "[PlanogramProduct.Brand]", "text", "XXX")).ToList());
            expectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Where(p => p.Gtin.Equals("GTIN1", StringComparison.OrdinalIgnoreCase)).Select(prod => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(prod.Gtin, "[PlanogramProduct.Height]", "101", "121")));

            // Act
            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);

            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);

            // Assert
            CollectionAssert.AreEquivalent(expectedPlanogram.ProductAttributeComparisonResult, TestPlanogram.ProductAttributeComparisonResult);
        }

        [Test]
        public void CheckThatEngineIgnoreAttributesThatAreNotPresentInTheMasterData()
        {
            // Arrange
            var engineToTest = new AttributeValidationEngine();
            var testEntity = Entity.FetchById(Context.EntityId);
            var ExpectedPlanogram = "Test Planogram".CreatePackage().AddPlanogram();
            ExpectedPlanogram.Id = TestPlanogram.Id;

            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Brand]", "Brand of product", PlanogramItemType.Product));
            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.PerformanceData.P1]", "Product Performance Data 1", PlanogramItemType.Product));

            ExpectedPlanogram.ProductAttributeComparisonResult.AddList(TestPlanogram.Products.Select(product => ProductAttributeComparisonResult.NewEntityComparisonAttributeResult(product.Gtin, "[PlanogramProduct.Brand]", "text", "XXX")).ToList());

            // Act
            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);

            // Assert
            CollectionAssert.AreEqual(ExpectedPlanogram.ProductAttributeComparisonResult, TestPlanogram.ProductAttributeComparisonResult);
        }

        [Test]
        public void CheckThatEngineHandlesAttributesThatHaveANullValue()
        {
            // Arrange
            var engineToTest = new AttributeValidationEngine();
            var testEntity = Entity.FetchById(Context.EntityId);
            var ExpectedPlanogram = "Test Planogram".CreatePackage().AddPlanogram();
            ExpectedPlanogram.Id = TestPlanogram.Id;

            testEntity.ComparisonAttributes.Add((EntityComparisonAttribute)EntityComparisonAttribute.NewEntityComparisonAttribute("[PlanogramProduct.Brand]", "Brand of product", PlanogramItemType.Product));

            foreach (var p in TestPlanogram.Products)
            {
                p.Brand = null;
            }

            
            var products = ProductList.FetchByEntityIdProductGtins(Context.EntityId,
                TestPlanogram.Products.Select(p => p.Gtin).ToList());

            Mapper.CreateMap<Product, ProductDto>()
                .ForMember(dest => dest.MerchandisingStyle, opt => opt.MapFrom(src => (byte) src.MerchandisingStyle))
                .ForMember(dest => dest.FillPatternType, opt => opt.MapFrom(src => (Byte) src.FillPatternType))
                .ForMember(dest => dest.OrientationType, opt => opt.MapFrom(src => (Byte) src.OrientationType))
                .ForMember(dest => dest.ShapeType, opt => opt.MapFrom(src => (Byte) src.ShapeType))
                .ForMember(dest => dest.StatusType, opt => opt.MapFrom(src => (Byte) src.StatusType));


            var productstoUpdate = new List<ProductDto>();
            foreach (var p in products)
            {
                p.Brand = null;
                productstoUpdate.Add(Mapper.Map<ProductDto>(p));
            }
            TestDataHelper.UpdateProductDtos(DalFactory, productstoUpdate);

            // Act
            engineToTest.ValidateProductAttributes(TestPlanogram, testEntity);

            // Assert
            CollectionAssert.IsEmpty(TestPlanogram.ProductAttributeComparisonResult);
        }
    }
}

