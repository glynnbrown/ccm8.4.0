﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31560 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.UnitTests.EngineTasks.DecreaseProductInventory.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.DecreaseProductInventory.v1.Task>
    {
        Galleria.Ccm.Engine.Tasks.DecreaseProductInventory.v1.Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Galleria.Ccm.Engine.Tasks.DecreaseProductInventory.v1.Task();
            TestPlanogram.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            TestPlanogram.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            TestPlanogram.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
        }
        
        #region Task tests

        [Test]
        public void ExecuteTask_Decrease_ReduceFacingsBy()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);

                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                PlanogramSubComponentPlacement planogramSubComponentPlacement = position.GetPlanogramSubComponentPlacement();

                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(3, TestPlanogram.Positions[0].FacingsWide, "Pre Facing Wide should be 3.");
            Assert.AreEqual(3, TestPlanogram.Positions[1].FacingsWide, "Pre Facing Wide should be 3.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)1));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)1));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(2, TestPlanogram.Positions[0].FacingsWide, "Post Facing Wide should be 2.");
            Assert.AreEqual(2, TestPlanogram.Positions[1].FacingsWide, "Post Facing Wide should be 2.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceFacingsBy_ValueExceedsFacingsOnThePlan()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);

                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                PlanogramSubComponentPlacement planogramSubComponentPlacement = position.GetPlanogramSubComponentPlacement();

                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(3, TestPlanogram.Positions[0].FacingsWide, "Pre Facing Wide should be 3.");
            Assert.AreEqual(3, TestPlanogram.Positions[1].FacingsWide, "Pre Facing Wide should be 3.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)99));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)99));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Positions[0].FacingsWide, "Post Facing Wide should be 1.");
            Assert.AreEqual(1, TestPlanogram.Positions[1].FacingsWide, "Post Facing Wide should be 1.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceFacingsBy_ValueLessThanFacingsOnThePlan()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);

                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                PlanogramSubComponentPlacement planogramSubComponentPlacement = position.GetPlanogramSubComponentPlacement();
                position.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(1, TestPlanogram.Positions[0].FacingsWide, "Pre Facing Wide should be 3.");
            Assert.AreEqual(1, TestPlanogram.Positions[1].FacingsWide, "Pre Facing Wide should be 3.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)1));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)1));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Positions[0].FacingsWide, "Post Facing Wide should be 1.");
            Assert.AreEqual(1, TestPlanogram.Positions[1].FacingsWide, "Post Facing Wide should be 1.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceFacingsBy_MultisitedProducts()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramProduct planogramProduct = TestPlanogram.AddProduct();

                PlanogramPosition position1 = shelf1.AddPosition(bay, planogramProduct);
                PlanogramPosition position2 = shelf2.AddPosition(bay, planogramProduct);

                PlanogramSubComponentPlacement planogramSubComponentPlacement = position1.GetPlanogramSubComponentPlacement();

                position1.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position1.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position1.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);

                planogramSubComponentPlacement = position2.GetPlanogramSubComponentPlacement();

                position2.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position2.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            List<PlanogramPosition> shelf1Positions = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();
            List<PlanogramPosition> shelf2Positions = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();

            Assert.AreEqual(3, shelf1Positions[0].FacingsWide, "Pre Facing Wide should be 3.");
            Assert.AreEqual(2, shelf2Positions[0].FacingsWide, "Pre Facing Wide should be 2.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)2));
            //test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)1));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            shelf1Positions = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();
            shelf2Positions = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();

            Assert.AreEqual(2, shelf1Positions[0].FacingsWide, "Post Facing Wide should be 2.");
            Assert.AreEqual(1, shelf2Positions[0].FacingsWide, "Post Facing Wide should be 1.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceFacingsBy_MultisitedProducts_ExceedsFacingsOnThePlan()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramProduct planogramProduct = TestPlanogram.AddProduct();

                PlanogramPosition position1 = shelf1.AddPosition(bay, planogramProduct);
                PlanogramPosition position2 = shelf2.AddPosition(bay, planogramProduct);

                PlanogramSubComponentPlacement planogramSubComponentPlacement = position1.GetPlanogramSubComponentPlacement();

                position1.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position1.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position1.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);

                planogramSubComponentPlacement = position2.GetPlanogramSubComponentPlacement();

                position2.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position2.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            List<PlanogramPosition> shelf1Positions = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();
            List<PlanogramPosition> shelf2Positions = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();

            Assert.AreEqual(3, shelf1Positions[0].FacingsWide, "Pre Facing Wide should be 3.");
            Assert.AreEqual(2, shelf2Positions[0].FacingsWide, "Pre Facing Wide should be 2.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)99));
            //test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceFacingsBy, (Int32)1));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            shelf1Positions = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();
            shelf2Positions = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();

            Assert.AreEqual(1, shelf1Positions[0].FacingsWide, "Post Facing Wide should be 1.");
            Assert.AreEqual(1, shelf2Positions[0].FacingsWide, "Post Facing Wide should be 1.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceFacingsTo()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);

                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                PlanogramSubComponentPlacement planogramSubComponentPlacement = position.GetPlanogramSubComponentPlacement();

                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(3, TestPlanogram.Positions[0].FacingsWide, "Pre Facing Wide should be 3.");
            Assert.AreEqual(3, TestPlanogram.Positions[1].FacingsWide, "Pre Facing Wide should be 3.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceFacingsTo, (Int32)1));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceFacingsTo, (Int32)1));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Positions[0].FacingsWide, "Post Facing Wide should be 1.");
            Assert.AreEqual(1, TestPlanogram.Positions[1].FacingsWide, "Post Facing Wide should be 1.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceFacingsTo_ValueExceedsFacingsOnThePlan()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);

                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                PlanogramSubComponentPlacement planogramSubComponentPlacement = position.GetPlanogramSubComponentPlacement();

                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.IncreaseFacings(planogramProduct, planogramSubComponentPlacement);
                position.RecalculateUnits(planogramProduct, planogramSubComponentPlacement);
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(3, TestPlanogram.Positions[0].FacingsWide, "Pre Facing Wide should be 3.");
            Assert.AreEqual(3, TestPlanogram.Positions[1].FacingsWide, "Pre Facing Wide should be 3.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceFacingsTo, (Int32)99));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceFacingsTo, (Int32)99));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(3, TestPlanogram.Positions[0].FacingsWide, "Post Facing Wide should be 3.");
            Assert.AreEqual(3, TestPlanogram.Positions[1].FacingsWide, "Post Facing Wide should be 3.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceUnitsTo()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);
                position.FacingsWide = 50;
                position.RecalculateUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement());
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(50, TestPlanogram.Positions[0].TotalUnits, "Pre Total Units should be 50.");
            Assert.AreEqual(50, TestPlanogram.Positions[1].TotalUnits, "Pre Total Units should be 50.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)1));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)2));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Positions[0].TotalUnits, "Post Total Units should be 1.");
            Assert.AreEqual(2, TestPlanogram.Positions[1].TotalUnits, "Post Total Units should be 2.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceUnitsTo_ValueExceedsUnitsOnThePlan()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);
                position.FacingsWide = 50;
                position.RecalculateUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement());
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(50, TestPlanogram.Positions[0].TotalUnits, "Pre Total Units should be 50.");
            Assert.AreEqual(50, TestPlanogram.Positions[1].TotalUnits, "Pre Total Units should be 50.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)99));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)99));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(50, TestPlanogram.Positions[0].TotalUnits, "Post Total Units should be 50.");
            Assert.AreEqual(50, TestPlanogram.Positions[1].TotalUnits, "Post Total Units should be 50.");
        }


        [Test]
        public void ExecuteTask_Decrease_ReduceUnitsTo_MultisitedProducts()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramProduct planogramProduct = TestPlanogram.AddProduct();

                PlanogramPosition position1 = shelf1.AddPosition(bay, planogramProduct);
                PlanogramPosition position2 = shelf2.AddPosition(bay, planogramProduct);

                position1.FacingsWide = 30;
                position1.RecalculateUnits(planogramProduct, position1.GetPlanogramSubComponentPlacement());

                position2.FacingsWide = 20;
                position2.RecalculateUnits(planogramProduct, position2.GetPlanogramSubComponentPlacement());
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            List<PlanogramPosition> shelf1Positions = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();
            List<PlanogramPosition> shelf2Positions = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();

            Assert.AreEqual(30, shelf1Positions[0].TotalUnits, "Pre Total Units should be 30.");
            Assert.AreEqual(20, shelf2Positions[0].TotalUnits, "Pre Total Units should be 20.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)30));
            
            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            shelf1Positions = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();
            shelf2Positions = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().ToList();

            Assert.AreEqual(20, shelf1Positions[0].FacingsWide, "Post Total Units should be 20.");
            Assert.AreEqual(10, shelf2Positions[0].FacingsWide, "Post Total Units should be 10.");
        }
        
        [Test]
        public void ExecuteTask_Decrease_ReduceToCasePackMultiplier()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);
                position.FacingsWide = 50;

                PlanogramProduct planogramProduct = position.GetPlanogramProduct();
                planogramProduct.CasePackUnits = 20;

                position.RecalculateUnits(planogramProduct, position.GetPlanogramSubComponentPlacement());
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(50, TestPlanogram.Positions[0].TotalUnits, "Pre Total Units should be 50.");
            Assert.AreEqual(50, TestPlanogram.Positions[1].TotalUnits, "Pre Total Units should be 50.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceToCasePackMultiplier, (Int32)1));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceToCasePackMultiplier, (Int32)2));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(20, TestPlanogram.Positions[0].TotalUnits, "Post Total Units should be 20.");
            Assert.AreEqual(40, TestPlanogram.Positions[1].TotalUnits, "Post Total Units should be 40.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceUnitsTo_WithRightCap()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);
                position.FacingsWide = 50;
                position.FacingsXWide = 10;
                position.FacingsXDeep = 1;
                position.FacingsXHigh = 1;
                position.RecalculateUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement());
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(60, TestPlanogram.Positions[0].TotalUnits, "Pre Total Units should be 60.");
            Assert.AreEqual(60, TestPlanogram.Positions[1].TotalUnits, "Pre Total Units should be 60.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)1));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)2));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Positions[0].TotalUnits, "Post Total Units should be 1.");
            Assert.AreEqual(2, TestPlanogram.Positions[1].TotalUnits, "Post Total Units should be 2.");
        }

        [Test]
        public void ExecuteTask_Decrease_ReduceUnitsTo_HasTopCaps()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);
                position.FacingsHigh = 6;
                position.FacingsYHigh = 5;
                position.FacingsYWide = 1;
                position.FacingsYDeep = 1;
                position.GetPlanogramProduct().MaxTopCap = 99;

                //position.IncreaseUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement(), null);
                position.RecalculateUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement());
            }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            Assert.AreEqual(11, TestPlanogram.Positions[0].TotalUnits, "Pre Total Units should be 11.");
            Assert.AreEqual(11, TestPlanogram.Positions[1].TotalUnits, "Pre Total Units should be 11.");

            test.Add(new TaskParameterValue(TestPlanogram.Products[0].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)1));
            test.Add(new TaskParameterValue(TestPlanogram.Products[1].Gtin, ProductDecreaseType.ReduceUnitsTo, (Int32)3));

            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Positions[0].TotalUnits, "Post Total Units should be 1.");
            Assert.AreEqual(3, TestPlanogram.Positions[1].TotalUnits, "Post Total Units should be 3.");
        }

        [Test]
        public void ExecuteTask_DecreaseProductDoesntExist()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            for (Int32 i = 1; i <= 5; i++)
            {
                PlanogramPosition position = shelf.AddPosition(bay);
                position.IncreaseUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement(), null);
                position.RecalculateUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement());
            }

            Assert.AreEqual(0, TestPlanogram.EventLogs.Count, "There should be any event log messages");

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            test.Add(new TaskParameterValue("999999", ProductDecreaseType.ReduceFacingsBy, (Int32)1));            
            
            SetTaskParameterValues(0, test, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.EventLogs.Where(p => p.EventId == (Int32)EventLogEvent.PlanogramProductInventoryDecreasedNoProductPositions).Count(), 
                            "The event log should contain warning that the decrease product did not exist on plan.");
        }

        #endregion        
    }
}