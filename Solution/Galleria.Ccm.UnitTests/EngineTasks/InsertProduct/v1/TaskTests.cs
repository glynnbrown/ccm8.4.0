﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-29105 : A.Silva
//      Created
// V8-29422 : A.Silva
//      Removed obsolete tests.
// V8-29419 : A.Silva
//      Added TaskExecution_WhenAlwaysAdd_ShouldAddDroppedAnyway test.

#endregion

#region Version History : CCM810

// V8-29902 : A.Silva
//  Refactored TaskExecution_WhenAddToCarparkShelf_ShouldAddDroppedToCarpark to account for the CarParkShelf changes.
// V8-29902 : A.Silva
//  Refactored and amended for the changes to Car Park Shelf and Increase Units.
// V8-30003 : A.Silva
//  Refactored tests to test all components for overfill.

#endregion

#region Version History: CCM820

// V8-31176 : D.Pleasance
//  Added TaskExecution_InsertMultisited

#endregion

#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added AddCarParkTextBox parameter
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.InsertProduct.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Task = Galleria.Ccm.Engine.Tasks.InsertProduct.v1.Task;

namespace Galleria.Ccm.UnitTests.EngineTasks.InsertProduct.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        private TaskBase _task;
        private List<ProductDto> _startingProducts;
        private List<ProductDto> _largeProductDtos;
        private List<ProductDto> _productsToInsert;
        private List<ProductDto> _expectedTopShelf;
        private List<ProductDto> _expectedMiddleShelf;
        private List<ProductDto> _expectedLowerShelf;

        #endregion

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _task = new Task();
            _startingProducts = GetProductDtos(24, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _startingProducts);
            _largeProductDtos = GetProductDtos(24, Context.EntityId, 24, new WidthHeightDepthValue(35,10,10)).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _largeProductDtos);
            _productsToInsert = new[] { "AA", "BB", "CC", "DD", "EE" }.Select(NewInsertedProductDto).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _productsToInsert);
        }

        [Test]
        public void ExecuteTask_AddsProductsInParameterOrder([Values(true, false)] Boolean reverse)
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(100, 4, 75));
            shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(50, 10, 10)));
            for (int i = 0; i < 5; i++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            }
            var orderedGtins = 
                reverse ? 
                TestPlanogram.Products.Skip(1).Reverse().Select(p => p.Gtin).ToList() : 
                TestPlanogram.Products.Skip(1).Select(p => p.Gtin).ToList();
            SetTaskParameterValues(0, orderedGtins);
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var positionGtins = TestPlanogram.Positions.Skip(1).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEqual(orderedGtins, positionGtins);
        }

        [Test]
        public void TaskExecution_IgnoresBlocking_Shelf(
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType ySequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 50, 75));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramPosition position = shelf1.AddPosition(bay, TestPlanogram.Products[0]);
            PlanogramPosition position2 = shelf2.AddPosition(bay, TestPlanogram.Products[1]);
            foreach (var s in TestPlanogram.Components.SelectMany(c => c.SubComponents)) { s.MerchandisingStrategyX = merchStrategy; }
            base.SetTaskParameterValues(0, TestPlanogram.Products.Select(p => p.Gtin));
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Blocking.First().Groups.First().SetBlockingGroupPlacementTypes(
                xSequenceDirection, ySequenceDirection, PlanogramBlockingGroupPlacementType.FrontToBack);
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = shelf2.GetPlanogramSubComponentPlacements()
                .First()
                .GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            List<String> expected = new List<String>();
            expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            // How you would visually see the sequence as...
            // LeftToRight, BottomToTop  (4,3,2)
            // LeftToRight, TopToBottom  (2,3,4)
            // RightToLeft, BottomToTop  (2,3,4)
            // RightToLeft, TopToBottom  (4,3,2)

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_IgnoresBlocking_Peg(
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentYMerchStrategyType.BottomStacked, PlanogramSubComponentYMerchStrategyType.TopStacked)] PlanogramSubComponentYMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirectionSecondary)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            peg.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = merchStrategy;
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 50, 10));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(TestPlanogram.Products[1], planogramPositionPlacement, sequenceDirection == PlanogramBlockingGroupPlacementType.BottomToTop ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below);
                mg.Process(); mg.ApplyEdit();
            }
            base.SetTaskParameterValues(0, TestPlanogram.Products.Select(p => p.Gtin));
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Blocking.First().Groups.First().SetBlockingGroupPlacementTypes(
                sequenceDirection, sequenceDirectionSecondary, PlanogramBlockingGroupPlacementType.FrontToBack);
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = peg.GetPlanogramSubComponentPlacements()
                .First()
                .GetPlanogramPositions()
                .Where(p => p.SequenceY == (sequenceDirection == PlanogramBlockingGroupPlacementType.BottomToTop ? (Int16)2 : (Int16)1))
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            List<String> expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_IgnoresBlocking_Chest(
            [Values(PlanogramBlockingGroupPlacementType.FrontToBack, PlanogramBlockingGroupPlacementType.BackToFront)] PlanogramBlockingGroupPlacementType zSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 65));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                        PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(TestPlanogram.Products[1], planogramPositionPlacement, PlanogramPositionAnchorDirection.ToRight);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }
            foreach (PlanogramSubComponent sub in TestPlanogram.Components.SelectMany(c => c.SubComponents))
            {
                sub.MerchandisingStrategyX = merchStrategy;
            }
            base.SetTaskParameterValues(0, TestPlanogram.Products.Select(p => p.Gtin));
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Blocking.First().Groups.First().SetBlockingGroupPlacementTypes(
                zSequenceDirection, xSequenceDirection, PlanogramBlockingGroupPlacementType.BottomToTop);
            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = chest.GetPlanogramSubComponentPlacements()
                .First()
                .GetPlanogramPositions()
                .Where(p => p.SequenceX == 2)
                .OrderBy(p => p.SequenceZ)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            List<String> expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNew_ShouldAddToPlanogram()
        {
            LoadBasicPlanogram(_startingProducts.Take(4));
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            SetTaskParameterValues(0, _startingProducts.Skip(4).Select(o => o.Gtin));
            ICollection<String> expected = _startingProducts.Select(o => o.Gtin).ToList();

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Products.Select(o => o.Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductPlaced_ShouldNotPlace()
        {
            LoadBasicPlanogram(_startingProducts.Take(4));
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            SetTaskParameterValues(0, _startingProducts.Take(4).Select(o => o.Gtin));
            ICollection<String> expected = _startingProducts.Take(4).Select(o => o.Gtin).ToList();

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Positions.Select(o => o.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNotPlaced_ShouldPlace()
        {
            LoadBasicPlanogram(_startingProducts.Take(4));
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            SetTaskParameterValues(0, _startingProducts.Skip(4).Take(1).Select(o => o.Gtin));
            ICollection<String> expected = _startingProducts.Take(5).Select(o => o.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = TestPlanogram.Positions.Select(o => o.GetPlanogramProduct().Gtin).ToList();

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNotPlaced_ShouldPlaceOnValidMerchandisingGroup()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _startingProducts.Take(1).Union(_startingProducts.Skip(2).Take(2)).ToList();
            IEnumerable<ProductDto> productsOnSecondShelf = _startingProducts.Skip(4).Take(4);
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf,
                productsOnSecondShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            IEnumerable<ProductDto> productsToInsert = _startingProducts.Skip(1).Take(1).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            IEnumerable<String> expected = GetSubComponentPlacementsFor(productsOnFirstShelf).Select(o => o.Component.Name).ToList();
            if (!expected.Any()) Assert.Inconclusive("Could not find the expected Merchandising Group");

            _task.ExecuteTask(Context);
            IEnumerable<String> actual = GetSubComponentPlacementsFor(productsToInsert).Select(o => o.Component.Name).ToList();

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNotPlaced_ShouldPlaceOnMerchandisingGroupWithMostSpace()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _startingProducts.Take(6).ToList();
            IEnumerable<ProductDto> productsOnSecondShelf = _startingProducts.Skip(7).Take(2).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf,
                productsOnSecondShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            IEnumerable<ProductDto> productsToInsert = _startingProducts.Skip(6).Take(1).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            IEnumerable<String> expected = GetSubComponentPlacementsFor(productsOnSecondShelf).Select(o => o.Component.Name).ToList();
            if (!expected.Any()) Assert.Inconclusive("Could not find the expected Merchandising Group");

            _task.ExecuteTask(Context);
            IEnumerable<String> actual = GetSubComponentPlacementsFor(productsToInsert).Select(o => o.Component.Name).ToList();

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductDoesNotFit_ShouldNotPlace()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(1).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] {DroppedProductBehaviourType.DoNotAdd});

            _task.ExecuteTask(Context);
            IEnumerable<String> actual = GetSubComponentPlacementsFor(productsToInsert).Select(o => o.Component.Name).ToList();

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void TaskExecution_WhenAddToCarparkShelf_ShouldAddDroppedToCarpark()
        {
            const String carParkComponentName = "TestCarPark";
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.AddToCarparkShelf });
            SetTaskParameterValues(2, new Object[] { carParkComponentName });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);

            using (PlanogramMerchandisingGroupList merchandisingGroups = this.Context.Planogram.GetMerchandisingGroups())
            {
                PlanogramMerchandisingGroup carparkMerchGroup = merchandisingGroups
                    .FirstOrDefault(merchandisingGroup =>
                                    merchandisingGroup.SubComponentPlacements
                                                      .Any(subComponentPlacement =>
                                                           subComponentPlacement.SubComponent.Name == carParkComponentName));
                Assert.IsNotNull(carparkMerchGroup, "The planogram should contain a Car Park Shelf.");

                List<String> actual = carparkMerchGroup.PositionPlacements.Select(p => p.Product.Gtin).ToList();
                List<Single> xPositions = carparkMerchGroup.PositionPlacements.Select(pp => pp.Position.X).OrderBy(p => p).ToList();
                AssertInsert(actual, xPositions, expected);
            }
        }

        [Test]
        public void TaskExecution_WhenAlwaysAdd_ShouldAddDroppedAnyway()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            PlanogramSubComponentPlacement lowerShelf = Context.Planogram.GetPlanogramSubComponentPlacements().First();
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = lowerShelf.GetPlanogramPositions().Select(p => p.GetPlanogramProduct().Gtin).ToList();

            CollectionAssert.IsSubsetOf(expected, actual);
        }

        [Test]
        public void TaskExecution_InsertProductA()
        {
            SetTestPlanogram();
            SetTaskParameterValues(0, _productsToInsert.Take(1).Select(o => o.Gtin));
            List<String> expected = _expectedLowerShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = _productsToInsert.Take(1).Select(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions().OrderBy(pp => pp.Sequence).Select(pp => pp.GetPlanogramProduct().Gtin).ToList()).ToList()).First();
            List<Single> xPositions = _productsToInsert.Take(1).SelectMany(o => GetSubComponentPlacementsFor(new[] {o}).SelectMany(p => p.GetPlanogramPositions()).Select(pp => pp.X).ToList()).OrderBy(p => p).ToList();

            AssertInsert(actual, xPositions, expected);
        }

        private void AssertInsert(List<String> actual, List<Single> xPositions, IEnumerable<String> expected)
        {
            foreach (PlanogramEventLog eventLog in TestPlanogram.EventLogs)
            {
                Debug.WriteLine(eventLog.Content);
            }

            Debug.Write(actual.First());
            foreach (String s in actual.Skip(1))
            {
                Debug.Write(String.Format(@", {0}", s));
            }
            Debug.WriteLine(String.Empty);
            Debug.Write(xPositions.First());
            foreach (Single s in xPositions.Skip(1))
            {
                Debug.Write(String.Format(@", {0}", s));
            }
            Debug.WriteLine(String.Empty);
            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AllItemsAreUnique(xPositions);
        }

        [Test]
        public void TaskExecution_InsertProductB()
        {
            SetTestPlanogram();
            SetTaskParameterValues(0, _productsToInsert.Take(2).Select(o => o.Gtin));
            IEnumerable<ProductDto> expectedMiddleShelf = _expectedMiddleShelf.Except(new List<ProductDto>{_productsToInsert[3], _productsToInsert[4]});
            List<String> expected = expectedMiddleShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = _productsToInsert.Skip(1).Take(1).Select(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions().OrderBy(pp => pp.Sequence).Select(pp => pp.GetPlanogramProduct().Gtin).ToList()).ToList()).First();
            List<Single> xPositions = _productsToInsert.Skip(1).Take(1).SelectMany(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions()).Select(pp => pp.X).ToList()).OrderBy(p => p).ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_InsertProductC()
        {
            SetTestPlanogram();
            SetTaskParameterValues(0, _productsToInsert.Take(3).Select(o => o.Gtin));
            List<String> expected = _expectedTopShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = _productsToInsert.Skip(2).Take(1).Select(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions().OrderBy(pp => pp.Sequence).Select(pp => pp.GetPlanogramProduct().Gtin).ToList()).ToList()).First();
            List<Single> xPositions = _productsToInsert.Skip(2).Take(1).SelectMany(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions()).Select(pp => pp.X).ToList()).OrderBy(p => p).ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_InsertProductD()
        {
            SetTestPlanogram();
            SetTaskParameterValues(0, _productsToInsert.Take(4).Select(o => o.Gtin));
            List<String> expected = _expectedMiddleShelf.Except(new List<ProductDto>{_productsToInsert[4]}).Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = _productsToInsert.Skip(3).Take(1).Select(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions().OrderBy(pp => pp.Sequence).Select(pp => pp.GetPlanogramProduct().Gtin).ToList()).ToList()).First();
            List<Single> xPositions = _productsToInsert.Skip(3).Take(1).SelectMany(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions()).Select(pp => pp.X).ToList()).OrderBy(p => p).ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_InsertProductE()
        {
            SetTestPlanogram();
            SetTaskParameterValues(0, _productsToInsert.Take(5).Select(o => o.Gtin));
            List<String> expected = _expectedMiddleShelf.Select(dto => dto.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = _productsToInsert.Skip(4).Take(1).Select(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions().OrderBy(pp => pp.Sequence).Select(pp => pp.GetPlanogramProduct().Gtin).ToList()).ToList()).First();
            List<Single> xPositions = _productsToInsert.Skip(4).Take(1).SelectMany(o => GetSubComponentPlacementsFor(new[] { o }).SelectMany(p => p.GetPlanogramPositions()).Select(pp => pp.X).ToList()).OrderBy(p => p).ToList();

            AssertInsert(actual, xPositions, expected);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequence_ShouldPlaceOnePosition()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;

            PlanogramPosition position1 = shelf1.AddPosition(bay,
                                                               TestPlanogram.Products[0]);

            PlanogramPosition position2 = shelf2.AddPosition(bay,
                                                               TestPlanogram.Products[1]);
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            MerchandisePlanogram();

            SetTaskParameterValues(0, products.Skip(2).Take(2).Select(o => o.Gtin));
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequence_ShouldPlaceOnePositionBasedOnAvailableSpace()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0), new WidthHeightDepthValue(200, 4, 75));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;
            foreach (var shelf in new[] { shelf1, shelf2 })
            {
                shelf.AddPosition(bay, TestPlanogram.Products[0]);
                shelf.AddPosition(bay, TestPlanogram.Products[1]);
            }
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            MerchandisePlanogram();

            SetTaskParameterValues(0, products.Skip(2).Take(2).Select(o => o.Gtin));
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequenceAndAnchorUsesUpSpace_ShouldPlaceOnePositionBasedOnAvailableSpace()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;
            foreach (var shelf in new[] { shelf1, shelf2 })
            {
                shelf.AddPosition(bay, TestPlanogram.Products[0]);
                shelf.AddPosition(bay, TestPlanogram.Products[1]);
            }
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });

            MerchandisePlanogram();

            SetTaskParameterValues(0, products.Skip(2).Take(2).Select(o => o.Gtin));
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequenceAndAnchorDoesntUseUpSpace_ShouldPlaceOnePositionBasedOnAvailableSpace()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            planogramProduct1.Gtin = products[0].Gtin;
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct2.Gtin = products[1].Gtin;
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct3.Gtin = products[2].Gtin;
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            planogramProduct4.Gtin = products[3].Gtin;
            foreach (var shelf in new[] { shelf1, shelf2 })
            {
                shelf.AddPosition(bay, TestPlanogram.Products[0]);
                shelf.AddPosition(bay, TestPlanogram.Products[1]);
            }
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                TestPlanogram.Sequence.ApplyFromMerchandisingGroups(mgs); // Create basis for sequence groups which we add to to introduce the new items.
            }

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[0], 
                                         products[1],
                                         products[2],
                                         products[3]
                                     });
            TestPlanogram.Sequence.Groups[1]
                .AddSequenceProducts(new List<ProductDto>
                                     {
                                         products[2], 
                                         products[1],
                                         products[0],
                                         products[3]
                                     });

            MerchandisePlanogram();

            SetTaskParameterValues(0, products.Skip(2).Take(2).Select(o => o.Gtin));
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct1.Gtin, planogramProduct4.Gtin, planogramProduct2.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.AddToCarparkShelf });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();


            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(3, new Object[] { AddCarParkTextBoxType.Yes });


            _task.ExecuteTask(Context);

            //check carpark shelf exists
            Assert.IsNotNull(TestPlanogram.Components.FirstOrDefault(c => c.Name == "Car Park Shelf"), "Should have a car park shelf.");

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.AddToCarparkShelf });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(3, new Object[] { AddCarParkTextBoxType.No });


            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }



        #region Test Helper Methods

        private void WriteList(String caption, IEnumerable<String> items)
        {
            Debug.WriteLine(caption);
            foreach (String item in items)
            {
                Debug.WriteLine(item);
            }
        }

        private void SetOriginalTestPlanogram()
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                _startingProducts.Skip(16).Take(8),
                _startingProducts.Skip(8).Take(8),
                _startingProducts.Take(8)
            });
            AddBlocking(itemCount: 4, includeSequence: true);
            PlanogramBlockingGroupList blockingGroups = TestPlanogram.Blocking.First().Groups;
            SetBlockingGroupPlacementTypes(blockingGroups[0], 
                PlanogramBlockingGroupPlacementType.LeftToRight,
                PlanogramBlockingGroupPlacementType.BottomToTop, 
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[1], 
                PlanogramBlockingGroupPlacementType.RightToLeft,
                PlanogramBlockingGroupPlacementType.BottomToTop, 
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[2], 
                PlanogramBlockingGroupPlacementType.BottomToTop,
                PlanogramBlockingGroupPlacementType.LeftToRight,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[3], 
                PlanogramBlockingGroupPlacementType.TopToBottom,
                PlanogramBlockingGroupPlacementType.RightToLeft, 
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SequenceProducts(TestPlanogram.Sequence.Groups[0], new List<ProductDto>
            {
                _startingProducts[16],  // green 17
                _productsToInsert[0],   // green A
                _startingProducts[17],  // green 18
                _startingProducts[9],   // green 10
                _productsToInsert[1],   // green B
                _startingProducts[8],   // green 9
                _startingProducts[0],   // green 1
                _startingProducts[1],   // green 2
                _startingProducts[2]    // green 3
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[1], new List<ProductDto>
            {
                _startingProducts[7],   // yellow 8
                _startingProducts[6],   // yellow 7
                _startingProducts[5],   // yellow 6
                _startingProducts[4],   // yellow 5
                _startingProducts[3],   // yellow 4
                _productsToInsert[2]    // yellow 3
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[2], new List<ProductDto>
            {
                _startingProducts[18],  // orange 19
                _startingProducts[10],  // orange 11
                _productsToInsert[3],   // orange D
                _startingProducts[11],  // orange 12
                _startingProducts[19],  // orange 20
                _startingProducts[12]   // orange 13
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[3], new List<ProductDto>
            {
                _startingProducts[15],  // blue 16
                _startingProducts[23],  // blue 24
                _productsToInsert[4],   // blue E
                _startingProducts[22],  // blue 23
                _startingProducts[14],  // blue 15
                _startingProducts[11],  // blue 12
                _startingProducts[20]   // blue 21
            });
            _expectedTopShelf = new List<ProductDto>
            {
                _startingProducts[0],   // green 1
                _startingProducts[1],   // green 2
                _startingProducts[2],   // green 3
                _productsToInsert[3],   // orange D
                _productsToInsert[2],   // yellow C
                _startingProducts[3],   // yellow 4
                _startingProducts[4],   // yellow 5
                _startingProducts[5],   // yellow 6
                _startingProducts[6],   // yellow 7
                _startingProducts[7]    // yellow 8
            };
            _expectedMiddleShelf = new List<ProductDto>
            {
                _startingProducts[8],   // green 9
                _startingProducts[9],   // green 10
                _productsToInsert[1],   // green B
                _startingProducts[10],  // orange 11
                _startingProducts[11],  // orange 12
                _startingProducts[12],  // orange 13
                _startingProducts[13],  // blue 14
                _startingProducts[14],  // blue 15
                _productsToInsert[4],   // blue E
                _startingProducts[15]   // blue 16
            };
            _expectedLowerShelf = new List<ProductDto>
            {
                _startingProducts[16],  // green 17
                _productsToInsert[0],   // green A
                _startingProducts[17],  // green 18
                _startingProducts[18],  // orange 19
                _startingProducts[19],  // orange 20
                _startingProducts[20],  // blue 21
                _startingProducts[21],  // blue 22
                _startingProducts[22],  // blue 23
                _startingProducts[23]   // blue 24
            };
        }

        private void SetTestPlanogram()
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                _startingProducts.Skip(16).Take(8),
                _startingProducts.Skip(8).Take(8),
                _startingProducts.Take(8)
            });
            AddBlocking(itemCount: 4, includeSequence: true);
            PlanogramBlockingGroupList blockingGroups = TestPlanogram.Blocking.First().Groups;
            SetBlockingGroupPlacementTypes(blockingGroups[0],
                PlanogramBlockingGroupPlacementType.LeftToRight,
                PlanogramBlockingGroupPlacementType.BottomToTop,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[1],
                PlanogramBlockingGroupPlacementType.RightToLeft,
                PlanogramBlockingGroupPlacementType.BottomToTop,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[2],
                PlanogramBlockingGroupPlacementType.BottomToTop,
                PlanogramBlockingGroupPlacementType.LeftToRight,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[3],
                PlanogramBlockingGroupPlacementType.TopToBottom,
                PlanogramBlockingGroupPlacementType.RightToLeft,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SequenceProducts(TestPlanogram.Sequence.Groups[0], new List<ProductDto>
            {
                _largeProductDtos[0],   // non fitting product
                _startingProducts[16],  // green 17
                _productsToInsert[0],   // green A
                _startingProducts[17],  // green 18
                _startingProducts[9],   // green 10
                _productsToInsert[1],   // green B
                _startingProducts[8],   // green 9
                _startingProducts[0],   // green 1
                _startingProducts[1],   // green 2
                _startingProducts[2]    // green 3
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[1], new List<ProductDto>
            {
                _startingProducts[7],   // yellow 8
                _startingProducts[6],   // yellow 7
                _startingProducts[5],   // yellow 6
                _startingProducts[4],   // yellow 5
                _startingProducts[3],   // yellow 4
                _productsToInsert[2]    // yellow C
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[2], new List<ProductDto>
            {
                _startingProducts[18],  // orange 19
                _startingProducts[19],  // orange 20
                _startingProducts[12],  // orange 13
                _startingProducts[11],  // orange 12
                _startingProducts[10],  // orange 11
                _productsToInsert[3]    // orange D
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[3], new List<ProductDto>
            {
                _startingProducts[15],  // blue 16
                _productsToInsert[4],   // blue E
                _startingProducts[14],  // blue 15
                _startingProducts[13],  // blue 14
                _startingProducts[20],  // blue 21
                _startingProducts[21],  // blue 22
                _startingProducts[22],  // blue 23
                _startingProducts[23]   // blue 24
            });
            _expectedTopShelf = new List<ProductDto>
            {
                _startingProducts[0],   // green 1
                _startingProducts[1],   // green 2
                _startingProducts[2],   // green 3
                _startingProducts[3],   // yellow 4
                _productsToInsert[2],   // yellow C
                _startingProducts[4],   // yellow 5
                _startingProducts[5],   // yellow 6
                _startingProducts[6],   // yellow 7
                _startingProducts[7]    // yellow 8
            };
            _expectedMiddleShelf = new List<ProductDto>
            {
                _startingProducts[8],   // green 9
                _startingProducts[9],   // green 10
                _productsToInsert[1],   // green B
                _startingProducts[10],  // orange 11
                _productsToInsert[3],   // orange D
                _startingProducts[11],  // orange 12
                _startingProducts[12],  // orange 13
                _startingProducts[13],  // blue 14
                _startingProducts[14],  // blue 15
                _startingProducts[15],  // blue 16
                _productsToInsert[4]    // blue E
            };
            _expectedLowerShelf = new List<ProductDto>
            {
                _startingProducts[16],  // green 17
                _productsToInsert[0],   // green A
                _startingProducts[17],  // green 18
                _startingProducts[18],  // orange 19
                _startingProducts[19],  // orange 20
                _startingProducts[20],  // blue 21
                _startingProducts[21],  // blue 22
                _startingProducts[22],  // blue 23
                _startingProducts[23]   // blue 24
            };
        }

        private static void SetBlockingGroupPlacementTypes(PlanogramBlockingGroup blockingGroup, PlanogramBlockingGroupPlacementType primary, PlanogramBlockingGroupPlacementType secondary, PlanogramBlockingGroupPlacementType tertiary)
        {
            blockingGroup.BlockPlacementPrimaryType = primary;
            blockingGroup.BlockPlacementSecondaryType = secondary;
            blockingGroup.BlockPlacementTertiaryType = tertiary;
        }

        private ProductDto NewInsertedProductDto(String gtin)
        {
            ProductList existingProducts = ProductList.FetchByEntityId(Context.EntityId);
            Int32 lastId = existingProducts.Max(o => o.Id);
            ProductDto dto = NewDefaultProductDto(++lastId, Context.EntityId, gtin, gtin);
            dto.Width = 10;
            dto.Height = 10;
            dto.Depth = 10;
            return dto;
        }



        private static IEnumerable<ProductDto> GetProductDtos(Int16 itemCount, Int32 entityId, Int16 startingId = 0, WidthHeightDepthValue size = default(WidthHeightDepthValue))
        {
            if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(10, 10, 10);

            for (var i = (Int16)(startingId + 1); i < startingId + itemCount + 1; i++)
            {
                ProductDto dto = NewDefaultProductDto(i, entityId, "Product{0}");
                dto.Width = size.Width;
                dto.Height = size.Height;
                dto.Depth = size.Depth;
                yield return dto;
            }
        }

        private static ProductDto NewDefaultProductDto(Int32 id, Int32 entityId, String nameMask = "name{0}", String gtinMask = "gtin{0}")
        {
            return new ProductDto
            {
                Id = id,
                EntityId = entityId,
                ReplacementProductId = id,
                Gtin = String.Format(gtinMask, id),
                Name = String.Format(nameMask, id),
                IsNewProduct = true,
                DateCreated = DateTime.Now,
                DateLastModified = DateTime.Now,
                DateDeleted = null,
            };
        }

        private IEnumerable<PlanogramSubComponentPlacement> GetSubComponentPlacementsFor(IEnumerable<ProductDto> productsToInsert)
        {
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                IEnumerable<String> gtinsToInsert = productsToInsert.Select(d => d.Gtin);
                PlanogramMerchandisingGroup merchandisingGroup = merchandisingGroups.FirstOrDefault(o => 
                    !gtinsToInsert.Except(o.PositionPlacements.Select(p => p.Product.Gtin)).Any());
                return merchandisingGroup != null ? merchandisingGroup.SubComponentPlacements : new List<PlanogramSubComponentPlacement>();
            }
        }

        /// <summary>
        ///     Adds Blocking Groups and optionally Sequence Groups.
        /// </summary>
        /// <param name="itemCount">The number of groups to add.</param>
        /// <param name="includeSequence">Whether to add corresponding Sequence Groups for each Blocking Group.</param>
        private void AddBlocking(Int32 itemCount, Boolean includeSequence)
        {
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            TestPlanogram.Blocking.Add(blocking);
            for (var i = 0; i < itemCount; i++)
            {
                PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
                blockingGroup.Colour = blocking.GetNextBlockingGroupColour();
                blocking.Groups.Add(blockingGroup);
                if (!includeSequence) continue;

                PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = blockingGroup.Colour;
                TestPlanogram.Sequence.Groups.Add(sequenceGroup);
            }
        }

        /// <summary>
        ///     Adds the products created from <paramref name="productDtos"/> to the provided <paramref name="sequenceGroup"/>.
        /// </summary>
        /// <param name="sequenceGroup"></param>
        /// <param name="productDtos"></param>
        private static void SequenceProducts(PlanogramSequenceGroup sequenceGroup, IEnumerable<ProductDto> productDtos)
        {
            PlanogramSequenceGroupProductList products = sequenceGroup.Products;
            products.Clear();
            foreach (PlanogramSequenceGroupProduct sequenceProduct in productDtos.Select(dto => PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(PlanogramTestHelper.NewPlanogramProduct(dto))))
            {
                sequenceProduct.SequenceNumber = products.Count + 1;
                products.Add(sequenceProduct);
            }
        }

        #endregion
    }
}