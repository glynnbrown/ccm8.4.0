﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30907 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added AddCarParkTextBox parameter
// V8-32068 : A.Silva
//  Amended ExecuteTask_DoesntAddPositionsForUnplacedProductsAlreadyInPlan_WhenPlaceUnplacedIsFalse.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.AddProductManual.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Task = Galleria.Ccm.Engine.Tasks.AddProductManual.v1.Task;

namespace Galleria.Ccm.UnitTests.EngineTasks.AddProductManual.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        private const Int32 AddToCarParkComponentParameterId = 1;
        private const Int32 PlaceUnPlacedOnCarParkComponentParameterId = 3;
        private Task _task;
        private IEnumerable<String> _productGtins;

        [SetUp]
        public void SetUp()
        {
            _task = new Task();
            _productGtins = TestDataHelper.InsertProductDtos(DalFactory, 10, Context.EntityId).Select(dto => dto.Gtin);
            // Product Gtins.
            SetTaskParameterValues(0, _productGtins);
            // Add to car park shelf.
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.No });
            // Car park component name.
            SetTaskParameterValues(2, new Object[] { "Car Park" });
            // Place unplaced products on car park component
            SetTaskParameterValues(3, new Object[] { PlaceUnPlacedOnCarParkComponentType.No });
            //Add carpark textbox
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.No });
        }

        [Test]
        public void ExecuteTask_AddsProductsInParameterOrder([Values(true, false)] Boolean reverse)
        {
            var orderedGtins = reverse ? _productGtins.Reverse().ToList() : _productGtins.ToList();
            SetTaskParameterValues(0, orderedGtins);
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });

            _task.ExecuteTask(Context);

            var actualGtins = TestPlanogram.Positions
                .Where(p => orderedGtins.Contains(p.GetPlanogramProduct().Gtin))
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();

            WriteList("Expected", orderedGtins);
            WriteList("Actual", actualGtins);
            CollectionAssert.AreEqual(orderedGtins, actualGtins);
        }

        private void WriteList(String desc, IEnumerable<String> items)
        {
            Debug.WriteLine(desc);

            foreach (var item in items)
            {
                Debug.WriteLine(item);
            }
        }

        [Test]
        public void ExecuteTask_AddsPositionsForUnplacedProductsAlreadyInPlan_WhenPlaceUnplacedIsTrue()
        {
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });
            SetTaskParameterValues(3, new Object[] { PlaceUnPlacedOnCarParkComponentType.Yes });
            IEnumerable<String> gtinsToAdd = _productGtins.Take(3).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.Contains(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_DoesntAddPositionsForPlacedProductsAlreadyInPlan_WhenPlaceUnplacedIsTrue()
        {
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });
            SetTaskParameterValues(3, new Object[] { PlaceUnPlacedOnCarParkComponentType.Yes });
            IEnumerable<String> gtinsToAdd = _productGtins.Take(3).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            foreach (var p in TestPlanogram.Products) { shelf.AddPosition(bay, p); }
            var existingPositions = TestPlanogram.Positions.ToList();

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Except(existingPositions).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.DoesNotContain(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_DoesntAddPositionsForUnplacedProductsAlreadyInPlan_WhenPlaceUnplacedIsFalse()
        {
            const String expectation = "Non placed products should not have been placed on the Car Park Component when PlaceUnPlacedOnCarParkComponent is true.";
            SetTaskParameterValues(AddToCarParkComponentParameterId, new Object[] { AddToCarParkType.Yes });
            SetTaskParameterValues(PlaceUnPlacedOnCarParkComponentParameterId, new Object[] { PlaceUnPlacedOnCarParkComponentType.No });
            IEnumerable<String> gtinsToAdd = _productGtins.Take(3).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd).Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.IsNotSubsetOf(gtinsToAdd, positionGtins, expectation);
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.Yes });

            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, _productGtins.Take(1).ToList())
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.No });

            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, _productGtins.Take(1).ToList())
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }
    }
}