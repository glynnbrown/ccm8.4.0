﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32520 : N.Haywood
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Engine.Tasks.AddComponent.v1;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.EngineTasks.AddComponent.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.AddComponent.v1.Task>
    {
        private Galleria.Ccm.Engine.Tasks.AddComponent.v1.Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Galleria.Ccm.Engine.Tasks.AddComponent.v1.Task();
            // Product placement
            SetTaskParameterValues(0, new Object[] { "Car Park" });
            // Add linked label
            SetTaskParameterValues(1, new Object[] { AddLinkedTextboxType.No });
        }


        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameterValues(1, new Object[] { AddLinkedTextboxType.Yes });

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameterValues(1, new Object[] { AddLinkedTextboxType.No });

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddComponent()
        {
            Int32 originalNumberOfComponents = TestPlanogram.Components.Count();

            _task.ExecuteTask(Context);

            Assert.AreEqual(originalNumberOfComponents + 1, TestPlanogram.Components.Count(),
                    "Should have 1 more component added item");

            _task.ExecuteTask(Context);

            Assert.AreEqual(originalNumberOfComponents + 1, TestPlanogram.Components.Count(),
                    "Should not have added another component");
        }
    }
}
