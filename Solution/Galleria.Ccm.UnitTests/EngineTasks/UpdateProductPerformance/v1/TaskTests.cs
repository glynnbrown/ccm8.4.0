﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811

// V8-30526 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using NUnit.Framework;
using Task = Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdateProductPerformance.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        /// <summary>
        ///     Instance of the task that is being tested.
        /// </summary>
        private Task _task;

        #endregion

        #region Parameter Tests

        [Test]
        public void ExecuteTask_WhenPerformanceSelectionNameIsMissing_ShouldLogInformation()
        {
            const EventLogEvent expectedEventLogEvent = EventLogEvent.PlanogramPerformanceDataNoPerformanceSelection;
            String expectedDescription = EventLogEventHelper.FriendlyDescriptions[expectedEventLogEvent];
            NullifyTaskParameter(0);

            _task.ExecuteTask(Context);

            Assert.IsTrue(
                Context.Planogram.EventLogs.Any(log => log.EventId == (Int32) expectedEventLogEvent && log.Description == expectedDescription));
        }

        [Test]
        public void ExecuteTask_WhenPerformanceSelectionNameIsNullOrEmpty_ShouldLogInformation()
        {
            const EventLogEvent expectedEventLogEvent = EventLogEvent.PlanogramPerformanceDataNoPerformanceSelection;
            String expectedDescription = EventLogEventHelper.FriendlyDescriptions[expectedEventLogEvent];
            SetTaskParameterValues(0, new Object[] {null});

            _task.ExecuteTask(Context);

            Assert.IsTrue(
                Context.Planogram.EventLogs.Any(log => log.EventId == (Int32) expectedEventLogEvent && log.Description == expectedDescription));
        }

        [Test]
        public void ExecuteTask_WhenEnterprisePerformanceWeightingIsMissing_ShouldLogError()
        {
            const EventLogEvent expectedEventLogEvent = EventLogEvent.InvalidTaskParameterValue;
            String expectedDescription = String.Format(EventLogEventHelper.FriendlyDescriptions[expectedEventLogEvent], Message.Error_NoValue);
            NullifyTaskParameter(1);

            _task.ExecuteTask(Context);

            Assert.IsTrue(
                Context.Planogram.EventLogs.Any(log => log.EventId == (Int32) expectedEventLogEvent && log.Description == expectedDescription));
        }

        [Test]
        public void ExecuteTask_WhenClusterPerformanceWeightingIsMissing_ShouldLogError()
        {
            const EventLogEvent expectedEventLogEvent = EventLogEvent.InvalidTaskParameterValue;
            String expectedDescription = String.Format(EventLogEventHelper.FriendlyDescriptions[expectedEventLogEvent], Message.Error_NoValue);
            NullifyTaskParameter(2);

            _task.ExecuteTask(Context);

            Assert.IsTrue(
                Context.Planogram.EventLogs.Any(log => log.EventId == (Int32) expectedEventLogEvent && log.Description == expectedDescription));
        }

        [Test]
        public void ExecuteTask_WhenStorePerformanceWeightingIsMissing_ShouldLogInformation()
        {
            const EventLogEvent expectedEventLogEvent = EventLogEvent.InvalidTaskParameterValue;
            String expectedDescription = String.Format(EventLogEventHelper.FriendlyDescriptions[expectedEventLogEvent], Message.Error_NoValue);
            NullifyTaskParameter(3);

            _task.ExecuteTask(Context);

            Assert.IsTrue(
                Context.Planogram.EventLogs.Any(log => log.EventId == (Int32) expectedEventLogEvent && log.Description == expectedDescription));
        }

        #endregion

        [Test]
        public void ExecuteTask_WhenEnterpriseFull_ShouldGetCorrectPerformance()
        {
            SetTaskParameterValues(0, new Object[] {"TestPerformanceSelection"});

            _task.OnPreExecute(Context);
        }

        #region Helper Methods

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        #endregion
    }
}