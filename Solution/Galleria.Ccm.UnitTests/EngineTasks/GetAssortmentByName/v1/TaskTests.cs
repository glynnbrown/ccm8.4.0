﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810

// V8-29741 : A.Silva
//  Created.

#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.EngineTasks.GetAssortmentByName.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        private Task _task;
        private List<ProductDto> _testProducts;
        private String _testAssortmentName;
        private List<AssortmentLocalProductDto> _assortmentLocalProductDtos;
        private List<AssortmentProductDto> _assortmentProductDtos;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _task = new Task();
            _testProducts = PlanogramTestHelper.CreateProductDtos(10, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _testProducts);
            int entityId = Context.EntityId;
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, 1, entityId);
            int hierarchyId = TestDataHelper.InsertProductHierarchyDtos(DalFactory, 1, entityId).First().Id;
            List<ProductLevelDto> levelDtos = TestDataHelper.InsertProductLevelDtos(DalFactory, hierarchyId, 1);
            int productGroupId = TestDataHelper.InsertProductGroupDtos(DalFactory, levelDtos, 1).First().Id;
            AssortmentDto assortmentDto = TestDataHelper.InsertAssortmentDtos(DalFactory, 1, entityId, productGroupId).First();
            _testAssortmentName = assortmentDto.Name;
            int assortmentId = assortmentDto.Id;
            _assortmentLocalProductDtos = TestDataHelper.InsertAssortmentLocalProducts(
                DalFactory,
                locationDtos.Select(o => new Tuple<Int16, String>(o.Id, o.Code)),
                _testProducts.Select(o => new Tuple<Int32, String>(o.Id, o.Gtin)),
                assortmentId);
            _assortmentProductDtos = TestDataHelper.InsertAssortmentProductDtos(DalFactory, _testProducts, assortmentId);
        }

        [Test]
        public void ExecuteTask_ShouldUpdateAssortmentName()
        {
            const String expectation = "The Planogram's Assortment name should have been updated.";
            String expected = _testAssortmentName;
            SetTaskParameterValues(0, new Object[] {expected});

            _task.ExecuteTask(Context);

            String actual = TestPlanogram.Assortment.Name;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ExecuteTask_ShouldUpdateAssortmentLocalProducts()
        {
            const String expectation = "The Planogram's Assortment local products should have been updated.";
            IEnumerable<String> expected = _assortmentLocalProductDtos.Select(dto => dto.ProductGTIN).ToList();
            SetTaskParameterValues(0, new Object[] { _testAssortmentName });

            _task.ExecuteTask(Context);

            IEnumerable<String> actual = TestPlanogram.Assortment.LocalProducts.Select(product => product.ProductGtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual, expectation);
        }

        [Test]
        public void ExecuteTask_ShouldUpdateAssortmentProducts()
        {
            const String expectation = "The Planogram's Assortment local products should have been updated.";
            IEnumerable<String> expected = _assortmentProductDtos.Select(dto => dto.GTIN).ToList();
            SetTaskParameterValues(0, new Object[] { _testAssortmentName });

            _task.ExecuteTask(Context);

            IEnumerable<String> actual = TestPlanogram.Assortment.Products.Select(product => product.Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual, expectation);
        }

        [Test]
        public void ExecuteTask_ShouldAddProductsToPlanogram()
        {
            const String expectation = "The Planogram's products should have been updated.";
            IEnumerable<String> expected = _assortmentProductDtos.Select(dto => dto.GTIN).ToList();
            SetTaskParameterValues(0, new Object[] { _testAssortmentName });

            _task.ExecuteTask(Context);

            IEnumerable<String> actual = TestPlanogram.Products.Select(product => product.Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual, expectation);
        }

        [Test]
        public void ExecuteTask_WhenAddingProductsToPlanogram_ShouldAddOnlyOnce()
        {
            const String expectation = "The Planogram's products should have each GTIN only once.";
            SetTaskParameterValues(0, new Object[] { _testAssortmentName });

            _task.ExecuteTask(Context);

            IEnumerable<String> actual = TestPlanogram.Products.Select(product => product.Gtin).ToList();
            CollectionAssert.AllItemsAreUnique(actual, expectation);
        }

        [Test]
        public void ExecuteTask_WhenAddingExistingProductsToPlanogram_ShouldNotAddAgain()
        {
            const String expectation = "The Planogram's products should have each GTIN only once.";
            TestPlanogram.AddProduct(_testProducts.First());
            SetTaskParameterValues(0, new Object[] { _testAssortmentName });

            _task.ExecuteTask(Context);

            IEnumerable<String> actual = TestPlanogram.Products.Select(product => product.Gtin).ToList();
            CollectionAssert.AllItemsAreUnique(actual, expectation);
        }
    }
}
