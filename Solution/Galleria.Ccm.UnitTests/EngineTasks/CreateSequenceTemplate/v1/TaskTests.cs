﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30729 : D.Pleasance
// Created.
// V8-30920 : A.Kuszyk
//  Added tests for modified task behaviour to place shelves in subsequent bays once the first bay is full.
// V8-31172 : A.Kuszyk
//  Added tests for multi-sited products.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Engine.Tasks.CreateSequenceTemplate.v1;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.UnitTests.EngineTasks.CreateSequenceTemplate.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Task();
            base.SetTaskParameterValues(0, new Object[] { SequenceType.ProductColor });
            base.SetTaskParameterValues(1, new Object[] { SequencePlacementPrimaryType.LeftToRight });
            base.SetTaskParameterValues(2, new Object[] { SequencePlacementSecondaryType.BottomToTop });
            base.SetTaskParameterValues(3, new Object[] { SequencePlacementPrimaryType.FrontToBack });
            base.SetTaskParameterValues(4, new Object[] { UnplacedProductBehaviourType.DoNotAdd });
        }

        [Test]
        public void ExecuteTask_MultisitedProductsAreSequencedIntoOneSequenceGroup_WithProductColours()
        {
            const Int32 numberOfPlacements = 4;
            base.SetTaskParameterValues(0, new Object[] { SequenceType.ProductColor });
            var plan = TestPlanogram;
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            for (Int32 i = 0; i < numberOfPlacements; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,(bay.GetPlanogramFixture().Height / (Single)numberOfPlacements) * i, 0));
                shelf.AddPosition(bay, product);
            }

            _task.ExecuteTask(Context);

            Assert.AreEqual(plan.Sequence.Groups.Count, 1);
            var expectedGtins = new List<String> { product.Gtin };
            var actualGtins = plan.Sequence.Groups.SelectMany(g => g.Products.Select(p => p.Gtin)).ToList();
            CollectionAssert.AreEqual(expectedGtins, actualGtins);
        }

        [Test]
        public void ExecuteTask_MultisitedProductsAreLogged_WithProductColours()
        {
            const Int32 numberOfPlacements = 4;
            base.SetTaskParameterValues(0, new Object[] { SequenceType.ProductColor });
            var plan = TestPlanogram;
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            for (Int32 i = 0; i < numberOfPlacements; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, (bay.GetPlanogramFixture().Height / (Single)numberOfPlacements) * i, 0));
                shelf.AddPosition(bay, product);
            }

            _task.ExecuteTask(Context);

            Int32 expected = 1;
            Int32 actual = TestPlanogram.EventLogs.Count(e => e.EventId == (Int32)Galleria.Ccm.Model.EventLogEvent.MultisitedProductNotSequenced);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ExecuteTask_MultisitedProductsAreSequencedOncePerSequenceGroup_WithBlockingColours()
        {
            const Int32 numberOfPlacements = 4;
            base.SetTaskParameterValues(0, new Object[] { SequenceType.BlockingGroupings});
            var plan = TestPlanogram;
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            for (Int32 i = 0; i < numberOfPlacements; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, (bay.GetPlanogramFixture().Height / (Single)numberOfPlacements) * i, 0));
                shelf.AddPosition(bay, product);
                shelf.AddPosition(bay, product);
            }
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.75f, PlanogramBlockingDividerType.Horizontal);

            _task.ExecuteTask(Context);

            Assert.AreEqual(plan.Sequence.Groups.Count, 4);
            foreach (var group in plan.Sequence.Groups)
            {
                var expectedGtins = new List<String> { product.Gtin };
                var actualGtins = group.Products.Select(p => p.Gtin).ToList();
                CollectionAssert.AreEqual(expectedGtins, actualGtins); 
            }
        }

        [Test]
        public void ExecuteTask_MultisitedProductsAreLogged_WithBlockingColours()
        {
            const Int32 numberOfPlacements = 4;
            base.SetTaskParameterValues(0, new Object[] { SequenceType.BlockingGroupings });
            var plan = TestPlanogram;
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            for (Int32 i = 0; i < numberOfPlacements; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, (bay.GetPlanogramFixture().Height / (Single)numberOfPlacements) * i, 0));
                shelf.AddPosition(bay, product);
                shelf.AddPosition(bay, product);
            }
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.75f, PlanogramBlockingDividerType.Horizontal);

            _task.ExecuteTask(Context);

            Int32 expected = 1;
            Int32 actual = TestPlanogram.EventLogs.Count(e => e.EventId == (Int32)Galleria.Ccm.Model.EventLogEvent.MultisitedProductNotSequenced);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ExecuteTask_MultipleBaysAreNotCreated_WhenTooManyShelvesForOne()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (Int32 i = 0; i < 4; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10,50,10));
                product.FillColour = i;
                shelf.AddPosition(bay, product);
            }

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Fixtures.Count);
        }

        [Test]
        public void ExecuteTask_EmptyBaysAreRemoved_WhenCarParkBaysAreEmpty()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (Int32 i = 0; i < 8; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                product.FillColour = i;
                shelf.AddPosition(bay, product);
            }

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Fixtures.Count);
        }

        [Test]
        public void ExecuteTask_CarParkProductsAreAddedToCarParkShelfInOwnBay_WhenAddToCarPark()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            using (var merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                var carParkMerchGroup = Context.GetCarParkMerchandisingGroup("Car Park", merchandisingGroups, /*addTextBox*/false); 
                for (Int32 i = 0; i < 4; i++)
                {
                    var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    product.FillColour = i;
                    shelf.AddPosition(bay, product);

                    var carParkProduct = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    carParkProduct.FillColour = i;
                    carParkMerchGroup.InsertPositionPlacement(carParkProduct);
                }
                carParkMerchGroup.Process();
                carParkMerchGroup.ApplyEdit();
            }

            _task.ExecuteTask(Context);

            Assert.AreEqual(2, TestPlanogram.Fixtures.Count);
            Boolean isCarPark = false;
            foreach (var fixture in TestPlanogram.Fixtures)
            {
                foreach(var component in fixture.Components.Select(fc => fc.GetPlanogramComponent()))
                {
                    if(isCarPark)
                    {
                        Assert.IsTrue(component.Name.Contains("Car Park"));
                    }
                    else
                    {
                        Assert.IsFalse(component.Name.Contains("Car Park"));
                    }
                }
                isCarPark = !isCarPark;
            }
        }

        [Test]
        public void ExecuteTask_CarParkShelvesAreAsWideAsProducts()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            using (var merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                var carParkMerchGroup = Context.GetCarParkMerchandisingGroup("Car Park", merchandisingGroups, /*addTextBox*/false);
                for (Int32 i = 0; i < 4; i++)
                {
                    var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    product.FillColour = i;
                    shelf.AddPosition(bay, product);

                    var carParkProduct = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    carParkProduct.FillColour = i;
                    carParkMerchGroup.InsertPositionPlacement(carParkProduct);
                }
                carParkMerchGroup.Process();
                carParkMerchGroup.ApplyEdit();
            }

            _task.ExecuteTask(Context);

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (!mg.SubComponentPlacements.First().Component.Name.Contains("Car Park")) continue;
                    Single width = mg.PositionPlacements.GetPositionDetails().TotalSize.Width;
                    AssertHelper.AreSinglesEqual(width, mg.SubComponentPlacements.First().Component.Width);
                    AssertHelper.AreSinglesEqual(width, mg.SubComponentPlacements.First().SubComponent.Width);
                }
            }
        }
        
        [Test]
        public void ExecuteTask_GroupIsNamedCorrectly_WhenOneShelfInLastCarParkBay()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            using (var merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                var carParkMerchGroup = Context.GetCarParkMerchandisingGroup("Car Park", merchandisingGroups, /*addTextBox*/false);
                for (Int32 i = 0; i < 3; i++)
                {
                    var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    product.FillColour = i;
                    product.ColourGroupValue = i.ToString();
                    shelf.AddPosition(bay, product);

                    var carParkProduct = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    carParkProduct.FillColour = i;
                    carParkProduct.ColourGroupValue = i.ToString();
                    carParkMerchGroup.InsertPositionPlacement(carParkProduct);
                }
                carParkMerchGroup.Process();
                carParkMerchGroup.ApplyEdit();
            }

            _task.ExecuteTask(Context);

            var expected = new List<String> { "0", "1", "2", "Car Park" };
            var actual = TestPlanogram.Sequence.Groups.Select(g => g.Name).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void ExecuteTask_PlanIsValid_WhenGroupNamesAreEmpty()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            using (var merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                var carParkMerchGroup = Context.GetCarParkMerchandisingGroup("Car Park", merchandisingGroups, /*addTextBox*/false);
                for (Int32 i = 0; i < 4; i++)
                {
                    var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    product.FillColour = i;
                    product.ColourGroupValue = " ";
                    shelf.AddPosition(bay, product);

                    var carParkProduct = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    carParkProduct.FillColour = i;
                    carParkProduct.ColourGroupValue = String.Empty;
                    carParkMerchGroup.InsertPositionPlacement(carParkProduct);
                }
                carParkMerchGroup.Process();
                carParkMerchGroup.ApplyEdit();
            }

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.IsValid);
        }

        [Test]
        public void ExecuteTask_AllComponentsHavePositions()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            using (var merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                var carParkMerchGroup = Context.GetCarParkMerchandisingGroup("Car Park", merchandisingGroups, /*addTextBox*/false);
                for (Int32 i = 0; i < 4; i++)
                {
                    var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    product.FillColour = i;
                    shelf.AddPosition(bay, product);

                    if (i % 2 == 0)
                    {
                        var carParkProduct = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                        carParkProduct.FillColour = i;
                        carParkMerchGroup.InsertPositionPlacement(carParkProduct); 
                    }
                }
                carParkMerchGroup.Process();
                carParkMerchGroup.ApplyEdit();
            }

            _task.ExecuteTask(Context);

            Assert.AreEqual(2, TestPlanogram.Fixtures.Count);
            Assert.AreEqual(7, TestPlanogram.Components.Count); 
            Assert.AreEqual(6, TestPlanogram.Positions.Count);
            foreach (var position in TestPlanogram.Positions)
            {
                var subComp = position.GetPlanogramSubComponent();
                Assert.IsNotNull(subComp);
                Assert.IsFalse(subComp.IsDeleted);
            }
            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var m in mgs)
                {
                    Assert.AreEqual(1, m.PositionPlacements.Count());
                    Assert.IsTrue(m.PositionPlacements.Any());
                }
            }
        }

        [Test]
        public void ExecuteTask_FirstBayIsNotCarPark()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            using (var merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                var carParkMerchGroup = Context.GetCarParkMerchandisingGroup("Car Park", merchandisingGroups, /*addTextBox*/false);
                for (Int32 i = 0; i < 4; i++)
                {
                    var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    product.FillColour = i;
                    shelf.AddPosition(bay, product);

                    var carParkProduct = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
                    carParkProduct.FillColour = i;
                    carParkMerchGroup.InsertPositionPlacement(carParkProduct);
                }
                carParkMerchGroup.Process();
                carParkMerchGroup.ApplyEdit();
            }

            _task.ExecuteTask(Context);

            var firstSequencedBay = TestPlanogram.FixtureItems.OrderBy(f => f.BaySequenceNumber).First();
            var firstPositionedBay = TestPlanogram.FixtureItems.OrderBy(f => f.X).First();
            Assert.AreEqual(firstSequencedBay, firstPositionedBay);
            Assert.IsTrue(firstSequencedBay.GetPlanogramFixture().Components
                .Select(fc => fc.GetPlanogramComponent())
                .Any(c => c.ComponentType == PlanogramComponentType.Backboard));
        }

        [Test]
        public void ExecuteTask_CreateSequenceFromProductColour_ForAllProducts()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product1 = TestPlanogram.AddProduct();
            product1.Colour = "0";
            product1.ColourGroupValue = "GALLERIA";
            var product2 = TestPlanogram.AddProduct();
            product2.Colour = "0";
            product2.ColourGroupValue = "GALLERIA";
            shelf.AddPosition(bay, product1);
            shelf.AddPosition(bay, product2);

            _task.ExecuteTask(Context);

            CollectionAssert.IsEmpty(Context.Planogram.Blocking);
            Assert.AreEqual(Context.Planogram.Products.Count, 2, "there should be 2 products on the sequence template");
            Assert.AreEqual(Context.Planogram.PlanogramType, PlanogramType.SequenceTemplate, "The planogram should be of template type");
        }

        [Test]
        public void ExecuteTask_CreateSequenceFromProductColour_ForPlacedProducts()
        {
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product1 = TestPlanogram.AddProduct();
            product1.Colour = "0";
            product1.ColourGroupValue = "GALLERIA";
            var product2 = TestPlanogram.AddProduct();
            product2.Colour = "0";
            product2.ColourGroupValue = "GALLERIA";
            shelf.AddPosition(bay, product1);

            _task.ExecuteTask(Context);

            CollectionAssert.IsEmpty(Context.Planogram.Blocking);
            Assert.AreEqual(Context.Planogram.Products.Count, 1, "there should be 1 products on the sequence template");
            Assert.AreEqual(Context.Planogram.PlanogramType, PlanogramType.SequenceTemplate, "The planogram should be of template type");
        }

        [Test]
        public void ExecuteTask_CreateSequenceFromProductColour_ForAllProductsRegardlessOfPlaced()
        {
            base.SetTaskParameterValues(4, new Object[] { UnplacedProductBehaviourType.AddToCarParkComponent });
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product1 = TestPlanogram.AddProduct();
            product1.Colour = "0";
            product1.ColourGroupValue = "GALLERIA";
            var product2 = TestPlanogram.AddProduct();
            product2.Colour = "0";
            product2.ColourGroupValue = "GALLERIA";
            shelf.AddPosition(bay, product1);

            _task.ExecuteTask(Context);

            CollectionAssert.IsEmpty(Context.Planogram.Blocking);
            Assert.AreEqual(Context.Planogram.Products.Count, 2, "there should be 2 products on the sequence template");
            Assert.AreEqual(Context.Planogram.PlanogramType, PlanogramType.SequenceTemplate, "The planogram should be of template type");
        }
    }
}