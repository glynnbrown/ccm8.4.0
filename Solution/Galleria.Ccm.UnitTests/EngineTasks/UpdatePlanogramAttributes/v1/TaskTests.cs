﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 802
// V8-29078: A.Kuszyk
//  Created.
#endregion
#region Version History : CCM 803
// V8-29643 : D.Pleasance
//  Updated following changes to task to allow list of properties to be updated. 
#endregion
#region Version History: CCM 840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Model;
using Moq;
using Galleria.Ccm.Engine;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using UpdatePlanogramAttributesv1 = Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributes.v1;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdatePlanogramAttributes.v1
{
    [TestFixture]
    public class TaskTests : TestBase
    {
        #region UpdatePlanogramAttributes

        [Test]
        public void UpdatePlanogramAttributes_UpdateGeneral()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos().Where(p => p.GroupName.Equals(
                                                    Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_General)).
                                                    Select(f =>
                                                        new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(
                                                            PlanogramAttributeSourceType.General, f.PropertyName, f.PropertyName)).ToList();


            // Create Entity
            List<EntityDto> entityDtos = Helpers.TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            //Create Hierrarchies
            var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(DalFactory, 1, entityDtos.First().Id);
            var productLevel = TestDataHelper.InsertProductLevelDtos(DalFactory, hierrarchy.First().Id, 1);
            var productGroup = TestDataHelper.InsertProductGroupDtos(DalFactory, productLevel, 1);

            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);

            Assert.AreEqual("CategoryCode", plan.CategoryCode);
            Assert.AreEqual("CategoryName", plan.CategoryName);
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdateCustomDataText()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos().Where(p => p.GroupName.Equals(
                                                    Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataText)).
                                                    Select(f =>
                                                        new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(
                                                            PlanogramAttributeSourceType.CustomDataText, f.PropertyName, f.PropertyName)).ToList();
            
            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);

            Assert.AreEqual("CustomAttributes.Text1", plan.CustomAttributes.Text1);
            Assert.AreEqual("CustomAttributes.Text2", plan.CustomAttributes.Text2);
            Assert.AreEqual("CustomAttributes.Text3", plan.CustomAttributes.Text3);
            Assert.AreEqual("CustomAttributes.Text4", plan.CustomAttributes.Text4);
            Assert.AreEqual("CustomAttributes.Text5", plan.CustomAttributes.Text5);
            Assert.AreEqual("CustomAttributes.Text6", plan.CustomAttributes.Text6);
            Assert.AreEqual("CustomAttributes.Text7", plan.CustomAttributes.Text7);
            Assert.AreEqual("CustomAttributes.Text8", plan.CustomAttributes.Text8);
            Assert.AreEqual("CustomAttributes.Text9", plan.CustomAttributes.Text9);
            Assert.AreEqual("CustomAttributes.Text10", plan.CustomAttributes.Text10);
            Assert.AreEqual("CustomAttributes.Text11", plan.CustomAttributes.Text11);
            Assert.AreEqual("CustomAttributes.Text12", plan.CustomAttributes.Text12);
            Assert.AreEqual("CustomAttributes.Text13", plan.CustomAttributes.Text13);
            Assert.AreEqual("CustomAttributes.Text14", plan.CustomAttributes.Text14);
            Assert.AreEqual("CustomAttributes.Text15", plan.CustomAttributes.Text15);
            Assert.AreEqual("CustomAttributes.Text16", plan.CustomAttributes.Text16);
            Assert.AreEqual("CustomAttributes.Text17", plan.CustomAttributes.Text17);
            Assert.AreEqual("CustomAttributes.Text18", plan.CustomAttributes.Text18);
            Assert.AreEqual("CustomAttributes.Text19", plan.CustomAttributes.Text19);
            Assert.AreEqual("CustomAttributes.Text20", plan.CustomAttributes.Text20);
            Assert.AreEqual("CustomAttributes.Text21", plan.CustomAttributes.Text21);
            Assert.AreEqual("CustomAttributes.Text22", plan.CustomAttributes.Text22);
            Assert.AreEqual("CustomAttributes.Text23", plan.CustomAttributes.Text23);
            Assert.AreEqual("CustomAttributes.Text24", plan.CustomAttributes.Text24);
            Assert.AreEqual("CustomAttributes.Text25", plan.CustomAttributes.Text25);
            Assert.AreEqual("CustomAttributes.Text26", plan.CustomAttributes.Text26);
            Assert.AreEqual("CustomAttributes.Text27", plan.CustomAttributes.Text27);
            Assert.AreEqual("CustomAttributes.Text28", plan.CustomAttributes.Text28);
            Assert.AreEqual("CustomAttributes.Text29", plan.CustomAttributes.Text29);
            Assert.AreEqual("CustomAttributes.Text30", plan.CustomAttributes.Text30);
            Assert.AreEqual("CustomAttributes.Text31", plan.CustomAttributes.Text31);
            Assert.AreEqual("CustomAttributes.Text32", plan.CustomAttributes.Text32);
            Assert.AreEqual("CustomAttributes.Text33", plan.CustomAttributes.Text33);
            Assert.AreEqual("CustomAttributes.Text34", plan.CustomAttributes.Text34);
            Assert.AreEqual("CustomAttributes.Text35", plan.CustomAttributes.Text35);
            Assert.AreEqual("CustomAttributes.Text36", plan.CustomAttributes.Text36);
            Assert.AreEqual("CustomAttributes.Text37", plan.CustomAttributes.Text37);
            Assert.AreEqual("CustomAttributes.Text38", plan.CustomAttributes.Text38);
            Assert.AreEqual("CustomAttributes.Text39", plan.CustomAttributes.Text39);
            Assert.AreEqual("CustomAttributes.Text40", plan.CustomAttributes.Text40);
            Assert.AreEqual("CustomAttributes.Text41", plan.CustomAttributes.Text41);
            Assert.AreEqual("CustomAttributes.Text42", plan.CustomAttributes.Text42);
            Assert.AreEqual("CustomAttributes.Text43", plan.CustomAttributes.Text43);
            Assert.AreEqual("CustomAttributes.Text44", plan.CustomAttributes.Text44);
            Assert.AreEqual("CustomAttributes.Text45", plan.CustomAttributes.Text45);
            Assert.AreEqual("CustomAttributes.Text46", plan.CustomAttributes.Text46);
            Assert.AreEqual("CustomAttributes.Text47", plan.CustomAttributes.Text47);
            Assert.AreEqual("CustomAttributes.Text48", plan.CustomAttributes.Text48);
            Assert.AreEqual("CustomAttributes.Text49", plan.CustomAttributes.Text49);
            Assert.AreEqual("CustomAttributes.Text50", plan.CustomAttributes.Text50);
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdateCustomDataValue()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create master data and planogram product.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();

            // Create product attribute selections
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos().Where(p => p.GroupName.Equals(
                                                    Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataValue)).
                                                    Select(f =>
                                                        new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(
                                                            PlanogramAttributeSourceType.CustomDataValue, f.PropertyName, 0)).ToList();
            
            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);

            Assert.AreEqual(0, plan.CustomAttributes.Value1);
            Assert.AreEqual(0, plan.CustomAttributes.Value2);
            Assert.AreEqual(0, plan.CustomAttributes.Value3);
            Assert.AreEqual(0, plan.CustomAttributes.Value4);
            Assert.AreEqual(0, plan.CustomAttributes.Value5);
            Assert.AreEqual(0, plan.CustomAttributes.Value6);
            Assert.AreEqual(0, plan.CustomAttributes.Value7);
            Assert.AreEqual(0, plan.CustomAttributes.Value8);
            Assert.AreEqual(0, plan.CustomAttributes.Value9);
            Assert.AreEqual(0, plan.CustomAttributes.Value10);
            Assert.AreEqual(0, plan.CustomAttributes.Value11);
            Assert.AreEqual(0, plan.CustomAttributes.Value12);
            Assert.AreEqual(0, plan.CustomAttributes.Value13);
            Assert.AreEqual(0, plan.CustomAttributes.Value14);
            Assert.AreEqual(0, plan.CustomAttributes.Value15);
            Assert.AreEqual(0, plan.CustomAttributes.Value16);
            Assert.AreEqual(0, plan.CustomAttributes.Value17);
            Assert.AreEqual(0, plan.CustomAttributes.Value18);
            Assert.AreEqual(0, plan.CustomAttributes.Value19);
            Assert.AreEqual(0, plan.CustomAttributes.Value20);
            Assert.AreEqual(0, plan.CustomAttributes.Value21);
            Assert.AreEqual(0, plan.CustomAttributes.Value22);
            Assert.AreEqual(0, plan.CustomAttributes.Value23);
            Assert.AreEqual(0, plan.CustomAttributes.Value24);
            Assert.AreEqual(0, plan.CustomAttributes.Value25);
            Assert.AreEqual(0, plan.CustomAttributes.Value26);
            Assert.AreEqual(0, plan.CustomAttributes.Value27);
            Assert.AreEqual(0, plan.CustomAttributes.Value28);
            Assert.AreEqual(0, plan.CustomAttributes.Value29);
            Assert.AreEqual(0, plan.CustomAttributes.Value30);
            Assert.AreEqual(0, plan.CustomAttributes.Value31);
            Assert.AreEqual(0, plan.CustomAttributes.Value32);
            Assert.AreEqual(0, plan.CustomAttributes.Value33);
            Assert.AreEqual(0, plan.CustomAttributes.Value34);
            Assert.AreEqual(0, plan.CustomAttributes.Value35);
            Assert.AreEqual(0, plan.CustomAttributes.Value36);
            Assert.AreEqual(0, plan.CustomAttributes.Value37);
            Assert.AreEqual(0, plan.CustomAttributes.Value38);
            Assert.AreEqual(0, plan.CustomAttributes.Value39);
            Assert.AreEqual(0, plan.CustomAttributes.Value40);
            Assert.AreEqual(0, plan.CustomAttributes.Value41);
            Assert.AreEqual(0, plan.CustomAttributes.Value42);
            Assert.AreEqual(0, plan.CustomAttributes.Value43);
            Assert.AreEqual(0, plan.CustomAttributes.Value44);
            Assert.AreEqual(0, plan.CustomAttributes.Value45);
            Assert.AreEqual(0, plan.CustomAttributes.Value46);
            Assert.AreEqual(0, plan.CustomAttributes.Value47);
            Assert.AreEqual(0, plan.CustomAttributes.Value48);
            Assert.AreEqual(0, plan.CustomAttributes.Value49);
            Assert.AreEqual(0, plan.CustomAttributes.Value50);
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdateCustomDataFlag()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos().Where(p => p.GroupName.Equals(
                                                    Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataFlag)).
                                                    Select(f =>
                                                        new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(
                                                            PlanogramAttributeSourceType.CustomDataFlag, f.PropertyName, true)).ToList();
            
            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);

            Assert.AreEqual(true, plan.CustomAttributes.Flag1);
            Assert.AreEqual(true, plan.CustomAttributes.Flag2);
            Assert.AreEqual(true, plan.CustomAttributes.Flag3);
            Assert.AreEqual(true, plan.CustomAttributes.Flag4);
            Assert.AreEqual(true, plan.CustomAttributes.Flag5);
            Assert.AreEqual(true, plan.CustomAttributes.Flag6);
            Assert.AreEqual(true, plan.CustomAttributes.Flag7);
            Assert.AreEqual(true, plan.CustomAttributes.Flag8);
            Assert.AreEqual(true, plan.CustomAttributes.Flag9);
            Assert.AreEqual(true, plan.CustomAttributes.Flag10);            
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdateCustomDataDate()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            DateTime dateTime = DateTime.UtcNow;

            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos().Where(p => p.GroupName.Equals(
                                                    Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataDate)).
                                                    Select(f =>
                                                        new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(
                                                            PlanogramAttributeSourceType.CustomDataDate, f.PropertyName, dateTime)).ToList();

            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);

            Assert.AreEqual(dateTime, plan.CustomAttributes.Date1);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date2);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date3);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date4);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date5);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date6);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date7);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date8);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date9);
            Assert.AreEqual(dateTime, plan.CustomAttributes.Date10);
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdateCustomDataNote()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos().Where(p => p.GroupName.Equals(
                                                    Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_CustomDataNote)).
                                                    Select(f =>
                                                        new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(
                                                            PlanogramAttributeSourceType.CustomDataNote, f.PropertyName, f.PropertyName)).ToList();

            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);

            Assert.AreEqual("CustomAttributes.Note1", plan.CustomAttributes.Note1);
            Assert.AreEqual("CustomAttributes.Note2", plan.CustomAttributes.Note2);
            Assert.AreEqual("CustomAttributes.Note3", plan.CustomAttributes.Note3);
            Assert.AreEqual("CustomAttributes.Note4", plan.CustomAttributes.Note4);
            Assert.AreEqual("CustomAttributes.Note5", plan.CustomAttributes.Note5);
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdateSettings()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos().Where(p => p.GroupName.Equals(
                                                    Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_Settings)).
                                                    Select(f =>
                                                        new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(
                                                            PlanogramAttributeSourceType.Settings, f.PropertyName, 0)).ToList();
            
            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);

            Assert.AreEqual(PlanogramLengthUnitOfMeasureType.Unknown, plan.LengthUnitsOfMeasure);
            Assert.AreEqual(PlanogramAreaUnitOfMeasureType.Unknown, plan.AreaUnitsOfMeasure);
            Assert.AreEqual(PlanogramVolumeUnitOfMeasureType.Unknown, plan.VolumeUnitsOfMeasure);
            Assert.AreEqual(PlanogramWeightUnitOfMeasureType.Unknown, plan.WeightUnitsOfMeasure);
            Assert.AreEqual(PlanogramCurrencyUnitOfMeasureType.Unknown, plan.CurrencyUnitsOfMeasure);
            Assert.AreEqual(PlanogramProductPlacementXType.Manual, plan.ProductPlacementX);
            Assert.AreEqual(PlanogramProductPlacementYType.Manual, plan.ProductPlacementY);
            Assert.AreEqual(PlanogramProductPlacementZType.Manual, plan.ProductPlacementZ);            
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdatingCatgeoryCodeShouldUpdateCategoryName()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos()
                                                    .Where(p => p.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_General))
                                                    .Where(c => c.PropertyName == Planogram.CategoryCodeProperty.Name)
                                                    .Select(f => new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(PlanogramAttributeSourceType.General, f.PropertyName, "code1")).ToList();


            // Create Entity
            List<EntityDto> entityDtos = Helpers.TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            //Create Hierrarchies
            var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(DalFactory, 1, entityDtos.First().Id);
            var productLevel = TestDataHelper.InsertProductLevelDtos(DalFactory, hierrarchy.First().Id, 1);
            var productGroup = TestDataHelper.InsertProductGroupDtos(DalFactory, productLevel, 1);

            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);
            
            Assert.AreEqual("name1", plan.CategoryName);
        }

        [Test]
        public void UpdatePlanogramAttributes_UpdatingCatgeoryCodeShouldNotUpdateCategoryNameWhenManuallySpecified()
        {
            Mock<ITaskContext> contextMock = new Mock<ITaskContext>();
            Planogram plan = Planogram.NewPlanogram();
            contextMock.Setup(c => c.Planogram).Returns(plan);

            // Create planogram attributes.
            IEnumerable<ObjectFieldInfo> planogramAttributes = Planogram.EnumerateAttributeFieldInfos();
            List<UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection> planogramGeneralAttributeSelections = Planogram.EnumerateAttributeFieldInfos()
                                                    .Where(p => p.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.Planogram_PropertyGroup_General))
                                                    .Where(c => c.PropertyName == Planogram.CategoryCodeProperty.Name || c.PropertyName == Planogram.CategoryNameProperty.Name)
                                                    .Select(f => new UpdatePlanogramAttributesv1.Task.PlanogramAttributeSelection(PlanogramAttributeSourceType.General, f.PropertyName, "code1")).ToList();


            // Create Entity
            List<EntityDto> entityDtos = Helpers.TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            //Create Hierrarchies
            var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(DalFactory, 1, entityDtos.First().Id);
            var productLevel = TestDataHelper.InsertProductLevelDtos(DalFactory, hierrarchy.First().Id, 1);
            var productGroup = TestDataHelper.InsertProductGroupDtos(DalFactory, productLevel, 1);

            UpdatePlanogramAttributesv1.Task.UpdatePlanogramAttributes(plan, planogramGeneralAttributeSelections, 1);
            
            Assert.IsTrue(plan.CategoryName == "code1");
        }

        #endregion
    }
}
