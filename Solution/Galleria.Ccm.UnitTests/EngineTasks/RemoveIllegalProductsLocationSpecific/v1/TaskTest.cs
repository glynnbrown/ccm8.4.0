﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30736 : L.Luong
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.RemoveIllegalProductsLocationSpecific.v1;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.EngineTasks.RemoveIllegalProductsLocationSpecific.v1
{
    [TestFixture]
    public class TaskTest : TaskTestBase<Task>
    {
        private Task _task;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        [Test]
        public void ExecuteTask_RemoveIllegalProductsFromLocationSpecificPlanogram()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, 1, Context.EntityId);
            List<ProductDto> removeProductsDtos = new List<ProductDto>();
            removeProductsDtos.Add(productDtos[0]);
            removeProductsDtos.Add(productDtos[1]);

            List<LocationProductIllegalDto> locationProductIllegalDtos = TestDataHelper.InsertLocationProductIllegalDtos(DalFactory, locationDtos.FirstOrDefault().Id, removeProductsDtos, Context.EntityId);

            // add location
            Context.Planogram.LocationCode = locationDtos.FirstOrDefault().Code;
            Context.Planogram.LocationName = locationDtos.FirstOrDefault().Name;

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            SetTaskParameterValues(0, new Object[] { RemoveActionType.RemovePositionsAndProducts });

            // check products are in planogram
            Assert.AreEqual(Context.Planogram.Products.Count(), 4, "Should have all the products before task execute");

            // execute
            _task.ExecuteTask(Context);
            Assert.AreEqual(Context.Planogram.Products.Count(), 2, "Products in the LocationProductIllegal should have been removed");
        }

        [Test]
        public void ExecuteTask_RemoveIllegalProductsFromNonLocationSpecificPlanogram()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, 1, Context.EntityId);
            List<ProductDto> removeProductsDtos = new List<ProductDto>();
            removeProductsDtos.Add(productDtos[0]);
            removeProductsDtos.Add(productDtos[1]);

            List<LocationProductIllegalDto> locationProductIllegalDtos = TestDataHelper.InsertLocationProductIllegalDtos(DalFactory, locationDtos.FirstOrDefault().Id, removeProductsDtos, Context.EntityId);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            // set parameters
            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            test.Add(new TaskParameterValue(productDtos.First().Gtin, productDtos.Last().Gtin));

            SetTaskParameterValues(1, new Object[] { RemoveActionType.RemovePositionsAndProducts });

            // check products are in planogram
            Assert.AreEqual(Context.Planogram.Products.Count(), 4, "Should have all the products before task execute");

            // execute
            _task.ExecuteTask(Context);
            Assert.AreEqual(Context.Planogram.Products.Count(), 4, "Products should stay the same as the planogram is non location Specific");
        }
    }
}
