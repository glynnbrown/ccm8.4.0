﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30761 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using FluentAssertions;

namespace Galleria.Ccm.UnitTests.EngineTasks.IncreaseProductInventory.v1
{
    [TestFixture]
    [Category(Categories.MerchandisingTasks)]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.Task>
    {
        Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.Task();
            TestPlanogram.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            TestPlanogram.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            TestPlanogram.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
        }

        #region Parameters

        [Test]
        public void Parameter_PercentageOfRankedProducts_IsRequiredWhenProductSelectionIsTopRankedProducts()
        {
            // Set processing order and number of ranked products values. These are irrelevant.
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            base.SetTaskParameterValues(2, new Object[] { 0 });

            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            Assert.IsNotNullOrEmpty(Context.WorkflowTask.Parameters[2].IsRequired(), "Number of ranked products should be not required when the product selection is all products");

            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.ProductsBelowAverageDos });
            Assert.IsNotNullOrEmpty(Context.WorkflowTask.Parameters[2].IsRequired(), "Number of ranked products should be not required when the product selection is below average Dos");

            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.TopRankedProducts });
            Assert.IsNull(Context.WorkflowTask.Parameters[2].IsRequired(), "Number of ranked products should be required when the product selection is top ranked products");
        }

        [Test]
        public void Parameter_PercentageOfRankedProducts_IsValidWhenProductSelectionIsTopRankedProductsAndValueIsGreaterThanZero()
        {
            // Set processing order and number of ranked products values. These are irrelevant.
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            base.SetTaskParameterValues(2, new Object[] { 0 });

            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            Assert.IsNull(Context.WorkflowTask.Parameters[2].IsValid(), "Number of ranked products should be valid when the product selection is all products");

            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.ProductsBelowAverageDos });
            Assert.IsNull(Context.WorkflowTask.Parameters[2].IsValid(), "Number of ranked products should be valid when the product selection is below average Dos");

            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.TopRankedProducts });
            Assert.IsNotNullOrEmpty(Context.WorkflowTask.Parameters[2].IsValid(), "Number of ranked products should be not valid when the product selection is top ranked products and the value is zero");

            base.SetTaskParameterValues(2, new Object[] { 1 });
            Assert.IsNull(Context.WorkflowTask.Parameters[2].IsValid(), "Number of ranked products should be not valid when the product selection is top ranked products and the value is greater than zero");
        }

        #endregion

        #region Processing Order

        public enum Order
        {
            ObserveOrder,
            ReverseOrder
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsInRankOrder([Values(Order.ObserveOrder, Order.ReverseOrder)] Order order)
        {
            Boolean reverseOrder = order == Order.ReverseOrder;
            const Single shelfWidth = 120;
            SetupParametersForAllProductsByRank();
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(shelfWidth * (34f / 32f), 4, 75));

            Single[] sizes =
                reverseOrder ?
                new Single[] { shelfWidth / 4f, shelfWidth / 8f, shelfWidth / 32f } :
                new Single[] { shelfWidth / 32f, shelfWidth / 8f, shelfWidth / 4f };
            foreach (var s in sizes) 
            { 
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(s, 196, 75))); 
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);

            _task.ExecuteTask(Context);

            AssertFacings(TestPlanogram.Positions[reverseOrder ? 2 : 0], 6, null, null, assertEquality: !reverseOrder);
            AssertFacings(TestPlanogram.Positions[1], 3, null, null, assertEquality: !reverseOrder);
            AssertFacings(TestPlanogram.Positions[reverseOrder ? 0 : 2], 2, null, null, assertEquality: !reverseOrder);
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsInAchievedDosOrder([Values(Order.ObserveOrder, Order.ReverseOrder)] Order order)
        {
            Boolean reverseOrder = order == Order.ReverseOrder;
            const Single shelfWidth = 120;
            SetupParametersForAllProductsByAchievedDos();
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(shelfWidth * (34f/32f), 4, 75));
            Single[] sizes =
                reverseOrder ?
                new Single[] { shelfWidth / 4f, shelfWidth / 8f, shelfWidth / 32f } :
                new Single[] { shelfWidth / 32f, shelfWidth / 8f, shelfWidth / 4f };
            foreach (var s in sizes)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(s, 196, 75)));
            }
            // Achieved DOS is total position units for product / units sold per day.
            // Units sold per day is regSalesMetricId performance data value / DaysOfPerformance (7 in this example).
            TestPlanogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            TestPlanogram.Inventory.DaysOfPerformance = 10;
            Byte regSalesMetricId = TestPlanogram.Performance.Metrics.First(m => m.SpecialType == MetricSpecialType.RegularSalesUnits).MetricId;
            TestPlanogram.Products[0].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[1].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 10);
            TestPlanogram.Products[2].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 1);

            _task.ExecuteTask(Context);

            AssertFacings(TestPlanogram.Positions[reverseOrder ? 2 : 0], 6, null, null, assertEquality:!reverseOrder);
            AssertFacings(TestPlanogram.Positions[1], 3, null, null, assertEquality: !reverseOrder);
            AssertFacings(TestPlanogram.Positions[reverseOrder ? 0 : 2], 2, null, null, assertEquality: !reverseOrder);
        }

        #endregion

        #region Product Selection

        [Test]
        public void ExecuteTask_IncreasesUnitsOnAllProducts_WhenSufficientSpace()
        {
            const Int32 noOfProducts = 3;
            SetupParametersForAllProductsByRank();
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 196, 75));
                shelf.AddPosition(bay, product);
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);

            _task.ExecuteTask(Context);

            foreach (var position in TestPlanogram.Positions)
            {
                AssertFacings(position, 4, 1, 1);
            }
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsOnTopRankedProducts_WhenSufficientSpace()
        {
            SetupParametersForTopRankingProductsByRank(0.5f);
            const Int32 noOfProducts = 6;
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 45, 75));
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            // Add top ranking products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[0]);
            shelf2.AddPosition(bay, TestPlanogram.Products[1]);
            shelf3.AddPosition(bay, TestPlanogram.Products[2]);
            // Add bottom ranking products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[3]);
            shelf2.AddPosition(bay, TestPlanogram.Products[4]);
            shelf3.AddPosition(bay, TestPlanogram.Products[5]);

            _task.ExecuteTask(Context);

            foreach (var position in TestPlanogram.Positions.Take(3))
            {
                AssertFacings(position, 2, 1, 1, String.Format("Top ranking products should be two facings wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
            }

            foreach (var position in TestPlanogram.Positions.Skip(3))
            {
                AssertFacings(position, 1, 1, 1, String.Format("Bottom ranking products should be one facing wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
            }
        } 

        [Test]
        public void ExecuteTask_IncreasesUnitsOnBelowAverageDos_WhenSufficientSpace()
        {
            SetupParametersForBelowAverageDosByAchievedDos();
            const Int32 noOfProducts = 6;
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 45, 75));
            }
            // Achieved DOS is total position units for product / units sold per day.
            // Units sold per day is regSalesMetricId performance data value / DaysOfPerformance (7 in this example).
            TestPlanogram.Inventory.InventoryMetricType = PlanogramInventoryMetricType.RegularSalesUnits;
            TestPlanogram.Inventory.DaysOfPerformance = 7;
            Byte regSalesMetricId = TestPlanogram.Performance.Metrics.First(m => m.SpecialType == MetricSpecialType.RegularSalesUnits).MetricId;
            for (int i = 0; i < noOfProducts; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 45, 75));
            }
            // Set performance data for products that will be below average dos.
            TestPlanogram.Products[0].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[0].GetPlanogramPerformanceData().AchievedDos = 3.4f;
            TestPlanogram.Products[1].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[1].GetPlanogramPerformanceData().AchievedDos = 3.5f;
            TestPlanogram.Products[2].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 100);
            TestPlanogram.Products[2].GetPlanogramPerformanceData().AchievedDos = 3.6f;

            // Set performance data for products that will be above average dos.
            TestPlanogram.Products[3].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 0.1f);
            TestPlanogram.Products[3].GetPlanogramPerformanceData().AchievedDos = 3.4f;
            TestPlanogram.Products[4].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 0.1f);
            TestPlanogram.Products[4].GetPlanogramPerformanceData().AchievedDos = 3.5f;
            TestPlanogram.Products[5].GetPlanogramPerformanceData().SetPerformanceData(regSalesMetricId, 0.1f);
            TestPlanogram.Products[5].GetPlanogramPerformanceData().AchievedDos = 3.6f;

            // Add low dos products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[0]);
            shelf2.AddPosition(bay, TestPlanogram.Products[1]);
            shelf3.AddPosition(bay, TestPlanogram.Products[2]);
      
            // Add high dos products, one to each shelf.
            shelf1.AddPosition(bay, TestPlanogram.Products[3]);
            shelf2.AddPosition(bay, TestPlanogram.Products[4]);
            shelf3.AddPosition(bay, TestPlanogram.Products[5]);

            _task.ExecuteTask(Context);

            foreach (var position in TestPlanogram.Positions.Take(3))
            {
                AssertFacings(position, 2, 1, 1, String.Format("Products below average Dos should be two facings wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
            }

            foreach (var position in TestPlanogram.Positions.Skip(3))
            {
                AssertFacings(position, 1, 1, 1, String.Format("Products above average Dos should be one facing wide. Gtin: {0}", position.GetPlanogramProduct().Gtin));
            }
        }

        #endregion

        [Test]
        public void ExecuteTask_WhenInventoryChangeIsByUnits_UnitsAreIncreased()
        {
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            base.SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByUnits});
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            var pos1 = shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 60)));
            var pos2 = shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            TestPlanogram.CreateAssortmentFromProducts();

            _task.ExecuteTask(Context);

            pos1.TotalUnits.Should().Be(10, "because the 10 facings should have been added to pos 1, as units");
            pos2.TotalUnits.Should().Be(14, "because the 2 facings should have been added to pos 2, as 14 units");
        }

        [Test]
        public void ExecuteTask_WhenInventoryChangeIsByFacings_FacingsAreIncreased()
        {
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            base.SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByFacings });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            var pos1 = shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 60)));
            var pos2 = shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            TestPlanogram.CreateAssortmentFromProducts();

            _task.ExecuteTask(Context);

            pos1.TotalUnits.Should().Be(6, "because the 6 facings should have been added to pos 1, as units");
            pos2.TotalUnits.Should().Be(6, "because the 6 facings should have been added to pos 2, as 6 units");
        }

        [Test]
        public void ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst()
        {
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos});
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            base.SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByFacings });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10,10,70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10,10,70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10,10,70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 1;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();

            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_Before");
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_After");

            pos1.TotalUnits.Should().Be(1, "because prod1 has a high DOS");
            pos2.TotalUnits.Should().Be(2, "because prod2 has a relatively high DOS");
            pos3.TotalUnits.Should().Be(9, "because prod3 has a low DOS");
        }

        [Test]
        public void ExecuteTask_WhenTopRankedAndLowestDos_ShouldIncreaseLowestOfTopRankedProducts()
        {
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.TopRankedProducts });
            base.SetTaskParameterValues(2, new Object[] { 0.666666f });
            base.SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByFacings });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 1;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();

            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_Before");
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_After");

            pos1.TotalUnits.Should().Be(4, "because prod1 was in the top ranking products, despite having a high dos.");
            pos2.TotalUnits.Should().Be(7, "because prod2 has a is lowest dos in the high ranking products");
            pos3.TotalUnits.Should().Be(1, "because prod3 was not in the top ranking products, despite the fact that it had the lowest dos");
        }

        [Test]
        public void ExecuteTask_WhenBelowAverageDosAndLowestDos_ShouldIncreaseLowestOfBelowAverage()
        {
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.ProductsBelowAverageDos});
            base.SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByFacings });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 1;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();

            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_Before");
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenProcessingOrderIsByLowestDos_ShouldIncreaseLowestDosFirst_After");

            pos1.TotalUnits.Should().Be(1, "because prod1 is above average dos");
            pos2.TotalUnits.Should().Be(2, "because prod2 is above average dos for most of the increases");
            pos3.TotalUnits.Should().Be(9, "because prod3 is below average dos");
        }

        [Test]
        public void ExecuteTask_WhenLowestDosDoesntHaveEnoughSpace_ShouldIncreaseOtherProducts()
        {
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            base.SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.InventoryChangeType.ByFacings });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(60, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 0.1f;
            prod2.GetPlanogramPerformanceData().P2 = 2f;
            prod3.GetPlanogramPerformanceData().P2 = 10f;
            TestPlanogram.CreateAssortmentFromProducts();

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_WhenLowestDosDoesntHaveEnoughSpace_ShouldIncreaseOtherProducts");

            pos1.TotalUnits.Should().Be(1, "because prod1 has the highest dos");
            pos2.TotalUnits.Should().Be(5, "because prod2 was the lowest dos that could fit units");
            pos3.TotalUnits.Should().Be(1, "because prod3 can't fit any additional units");
        }

        [Test]
        public void ExecuteTask_WhenByLowestDosAndAllProductsHaveZeroDos_ShouldNotDoAnything()
        {
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProcessingOrderType.ByLowestDos });
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 11;
            TestPlanogram.Inventory.DaysOfPerformance = 1;
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70));
            var pos1 = shelf.AddPosition(bay, prod1);
            var pos2 = shelf.AddPosition(bay, prod2);
            var pos3 = shelf.AddPosition(bay, prod3);
            prod1.GetPlanogramPerformanceData().P2 = 0;
            prod2.GetPlanogramPerformanceData().P2 = 0f;
            prod3.GetPlanogramPerformanceData().P2 = 0f;
            TestPlanogram.CreateAssortmentFromProducts();

            _task.ExecuteTask(Context);

            TestPlanogram.Positions.Should().OnlyContain(
                p => p.TotalUnits == 1, "because none of the products should have been increased");
        }

        #region Helpers
        private static void AssertFacings(PlanogramPosition position, Int16? wide, Int16? high, Int16? deep, String message = null, Boolean assertEquality = true)
        {
            if (assertEquality)
            {
                if(deep.HasValue) Assert.AreEqual(deep.Value, position.FacingsDeep, message ?? String.Empty);
                if(high.HasValue) Assert.AreEqual(high.Value, position.FacingsHigh, message ?? String.Empty);
                if(wide.HasValue) Assert.AreEqual(wide.Value, position.FacingsWide, message ?? String.Empty); 
            }
            else
            {
                if (deep.HasValue) Assert.AreNotEqual(deep.Value, position.FacingsDeep, message ?? String.Empty);
                if (high.HasValue) Assert.AreNotEqual(high.Value, position.FacingsHigh, message ?? String.Empty);
                if (wide.HasValue) Assert.AreNotEqual(wide.Value, position.FacingsWide, message ?? String.Empty); 
            }
        }

        private void SetupParametersForAllProductsByRank()
        {
            // Processing order.
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            // Product selection.
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            // Number of ranked products.
            base.SetTaskParameterValues(2, new Object[] { 0 });
        }

        private void SetupParametersForAllProductsByAchievedDos()
        {
            // Processing order.
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByAchievedDos });
            // Product selection.
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.AllProducts });
            // Number of ranked products.
            base.SetTaskParameterValues(2, new Object[] { 0 });
        }

        private void SetupParametersForTopRankingProductsByRank(Single percentageOfRankedProducts)
        {
            // Processing order.
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByRank });
            // Product selection.
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.TopRankedProducts });
            // Number of ranked products.
            base.SetTaskParameterValues(2, new Object[] { percentageOfRankedProducts });
        }

        private void SetupParametersForBelowAverageDosByAchievedDos()
        {
            // Processing order.
            base.SetTaskParameterValues(0, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProcessingOrderType.ByAchievedDos });
            // Product selection.
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.IncreaseProductInventory.v1.ProductSelectionType.ProductsBelowAverageDos });
            // Number of ranked products.
            base.SetTaskParameterValues(2, new Object[] { 0 });
        } 
        #endregion
    }
}
