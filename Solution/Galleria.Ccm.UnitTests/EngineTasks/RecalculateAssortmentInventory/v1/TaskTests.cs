﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-27949 : A.Kuszyk
//  Created.

#endregion

#region Version History: CCM810

// V8-28878 : A.Silva
//  Amended CalculateUnitsAndLogOnError_MaxShelfLifeValueIsNotExceeded

#endregion

#region Version History: CCM820
// V8-30705 : A.Probyn
//  Added CalculateUnitsAndLogOnError_WhenAssortmentRuleProductTreatmentTypeSetup and CalculateUnitsAndLogOnError_WhenAssortmentProductRuleIsSetup
#endregion

#region Version History: CCM830
// V8-31551 : A.Probyn
//  Added CalculateUnitsAndLogOnError_WhenAssortmentProductInventoryRuleIsSetup
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-13996 : J.Pickup
//  normalise metrics test
#endregion


#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Moq;
using NUnit.Framework;
using Task = Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Enums;
using Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1;
using FluentAssertions;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.UnitTests.EngineTasks.RecalculateAssortmentInventory.v1
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task>
    {
        private const Byte NumberOfProducts = 10;
        private Planogram _planogram;
        private Dictionary<String, PlanogramProduct> _productsByGtin;
        private Dictionary<String, PlanogramAssortmentProduct> _assortmentProductsByGtin;
        private Dictionary<Object, PlanogramPerformanceData> _performanceDataByProductId;
        private Dictionary<String, PlanogramAssortmentInventoryRule> _assortmentInventoryRulesByGtin;
        private Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task();
       
            _planogram = Planogram.NewPlanogram();
            
            // Products and Assortment Products
            for (Byte i = 0; i < NumberOfProducts; i++)
			{
                var product = PlanogramProduct.NewPlanogramProduct();
                product.Gtin = String.Format("Product{0}", i);
                _planogram.Products.Add(product);
                _planogram.Assortment.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(product));
			}
            _productsByGtin = _planogram.Products.ToDictionary(p => p.Gtin, p => p);
            _assortmentProductsByGtin = _planogram.Assortment.Products.ToDictionary(p => p.Gtin, p => p);

            // Performance data
            Single performanceValue = 1;
            foreach (var product in _planogram.Products)
            {
                var performanceData = _planogram.Performance.PerformanceData.FirstOrDefault(p => p.PlanogramProductId == product.Id);
                if (performanceData == null)
                {
                    performanceData = PlanogramPerformanceData.NewPlanogramPerformanceData(product);
                    _planogram.Performance.PerformanceData.Add(performanceData);
                }
                for (Byte i = 1; i <= 20; i++)
                {
                    var propertyInfo = typeof(PlanogramPerformanceData).GetProperty(String.Format("P{0}", i));
                    propertyInfo.SetValue(performanceData, performanceValue, null);
                }
                performanceValue++;
            }
            _performanceDataByProductId = _planogram.Performance.PerformanceData.ToDictionary(p => p.PlanogramProductId, p => p);
            Assert.AreEqual(NumberOfProducts, _performanceDataByProductId.Count());

            //Inventory rules
            _assortmentInventoryRulesByGtin = new Dictionary<String, PlanogramAssortmentInventoryRule>(); //TODO
        }

        #region CalculateRanksByProduct
        [Test]
        public void CalculateRanksByProduct_RanksCorrectly()
        {
            MetricList globalMetricList = MetricList.NewMetricList();

            var metricProfile = Galleria.Ccm.Model.MetricProfile.NewMetricProfile(1);
            for (Byte i = 1; i <= 20; i++)
            {
                //add to global
                Metric m1 = Metric.NewMetric(Context.EntityId);
                m1.Name = "Metric" + i;
                m1.Description = "metric desc";
                globalMetricList.Add(m1);

                //add to metric profile
                var propertyInfo = typeof(Galleria.Ccm.Model.MetricProfile).GetProperty(String.Format("Metric{0}MetricId", i));
                propertyInfo.SetValue(metricProfile, m1.Id, null);

                propertyInfo = typeof(Galleria.Ccm.Model.MetricProfile).GetProperty(String.Format("Metric{0}Ratio", i));
                propertyInfo.SetValue(metricProfile, (Single?)1, null);

                //add plan metrics
                var metric = _planogram.Performance.Metrics.FirstOrDefault(m => m.MetricId == i);
                if (metric == null)
                {
                    metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(i);
                    _planogram.Performance.Metrics.Add(metric);
                }
                metric.Name = m1.Name;
                metric.Direction = MetricDirectionType.MaximiseIncrease;
            }

            //create the context
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);


            IPlanogramMetricProfile profile = metricProfile.ToPlanogramMetricProfile(context.Planogram.Performance, globalMetricList);

            var ranksByProduct = Task.CalculateRanksByProduct(
                context, profile, _productsByGtin, _assortmentProductsByGtin, _performanceDataByProductId);

            var actual = _planogram.Assortment.Products.OrderBy(p => ranksByProduct[p]);
            var expected = _planogram.Assortment.Products.Reverse();
            CollectionAssert.AreEqual(expected, actual);
        }
        #endregion

        #region CalculateUnitsAndLogOnError
        [Test]
        public void CalculateUnitsAndLogOnError_MinUnitsValueIsTakenFromInventoryProfile()
        {
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);

            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.MinUnits = 10;
            inventoryProfile.ShelfLife = 1;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            Assert.AreEqual(inventoryProfile.MinUnits, units.Value);
        }

        [Test]
        public void CalculateUnitsAndLogOnError_CasePackValueIsMultipliedByProductCasePackUnits()
        {
            const Int32 casePackUnits = 5;

            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 2;
            inventoryProfile.ShelfLife = 1;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            Assert.AreEqual(inventoryProfile.CasePack * casePackUnits, units.Value);
        }

        [Test]
        public void CalculateUnitsAndLogOnError_MaxShelfLifeValueIsNotExceeded()
        {
            const Int32 casePackUnits = 100;

            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);

            InventoryProfile inventoryProfile = InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 100;
            inventoryProfile.ShelfLife = 0.1f;
            PlanogramAssortmentProduct assortmentProduct = _planogram.Assortment.Products.First();
            PlanogramProduct product = _productsByGtin[assortmentProduct.Gtin];
            product.ShelfLife = 1;
            product.CasePackUnits = casePackUnits;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct,
                context,
                _productsByGtin,
                _performanceDataByProductId,
                _assortmentInventoryRulesByGtin,
                inventoryProfile);
            if (!units.HasValue) Assert.Inconclusive("There should have been a Units value calculated.");
            Single? unitsSoldPerDay = _performanceDataByProductId[product.Id].UnitsSoldPerDay;
            if (!unitsSoldPerDay.HasValue) Assert.Inconclusive("Units Sold Per Day should have a valid value for the product.");

            Int16 expected = Convert.ToInt16(Math.Floor(inventoryProfile.ShelfLife*product.ShelfLife*unitsSoldPerDay.Value));
            if (expected == 0) expected = Convert.ToInt16(inventoryProfile.CasePack*casePackUnits);
            Assert.AreEqual(expected, units.Value);
        }

        [Test]
        public void CalculateUnitsAndLogOnError_DaysOfSupplyValueIsMultipliedByUnitsSoldPerDay()
        {
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);

            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.DaysOfSupply = 2;
            inventoryProfile.ShelfLife = 1;
            var assortmentProduct = _planogram.Assortment.Products.First();
            var product = _productsByGtin[assortmentProduct.Gtin];
            product.ShelfLife = 100;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            Int16 expected = Convert.ToInt16(Math.Ceiling(inventoryProfile.DaysOfSupply * _performanceDataByProductId[product.Id].UnitsSoldPerDay.Value));
            Assert.AreEqual(expected, units.Value);
        }

        [Test]
        public void CalculateUnitsAndLogOnError_ReplenishmentDaysIsMultipliedByDeliveryFrequenceAndUnitsPerDay()
        {
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);

            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.ShelfLife = 1;
            inventoryProfile.ReplenishmentDays = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            var product = _productsByGtin[assortmentProduct.Gtin];
            product.ShelfLife = 100;
            product.DeliveryFrequencyDays = 7;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            Int16 expected = Convert.ToInt16(Math.Ceiling(
                inventoryProfile.ReplenishmentDays * product.DeliveryFrequencyDays.Value * _performanceDataByProductId[product.Id].UnitsSoldPerDay.Value));
            Assert.AreEqual(expected, units.Value);
        }

        [Test]
        public void CalculateUnitsAndLogOnError_ReturnsZero_WhenWasteHurdleUnitsMoreThanUnitsPerDayTimesSeven()
        {
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);

            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.ShelfLife = 1;
            inventoryProfile.WasteHurdleUnits = 50;
            var assortmentProduct = _planogram.Assortment.Products.First();
            var product = _productsByGtin[assortmentProduct.Gtin];
            product.ShelfLife = 100;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            Assert.AreEqual(0, units.Value);
        }
        
        [Test]
        public void CalculateUnitsAndLogOnError_ReturnsZero_WhenWasteHurdleCasePacksMoreThanUnitsPerDayTimesSevenTimesCasePackUnits()
        {
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);

            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.ShelfLife = 1;
            inventoryProfile.WasteHurdleCasePack = 50;
            var assortmentProduct = _planogram.Assortment.Products.First();
            var product = _productsByGtin[assortmentProduct.Gtin];
            product.ShelfLife = 100;
            product.CasePackUnits = 5;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            Assert.AreEqual(0, units.Value);
        }
        
        [Test]
        public void CalculateUnitsAndLogOnError_WhenAssortmentRuleProductTreatmentTypeSetup()
        {
            const Int32 casePackUnits = 0;

            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);

            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 0;
            inventoryProfile.ShelfLife = 0;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 0;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;

            //Make product a core product
            assortmentProduct.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Core;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure 1 unit is set, rather than 0
            Assert.AreEqual(1, units.Value);

            //Make product a core product
            assortmentProduct.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Inheritance;

            units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure 1 unit is set, rather than 0
            Assert.AreEqual(1, units.Value);

            //Make product a core product
            assortmentProduct.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Distribution;

            units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure 1 unit is set, rather than 0
            Assert.AreEqual(1, units.Value);
        }

        [Test]
        public void CalculateUnitsAndLogOnError_WhenAssortmentProductRuleIsSetup()
        {
            const Int32 casePackUnits = 7;

            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 1;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;

            //Apply force product rule
            assortmentProduct.ClearRule();
            assortmentProduct.ExactListUnits = 25;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure 25 unit is set, rather than 0
            Assert.AreEqual(25, units.Value);

            //Apply delist product rule
            assortmentProduct.ClearRule();
            assortmentProduct.ExactListUnits = 0;

            units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure product is not given any units due to delist
            Assert.IsNull(units);

            //Apply minimum hurdle rule
            assortmentProduct.ClearRule();
            assortmentProduct.MinListUnits = 500;

            units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure minimum value is met
            Assert.AreEqual(500, units.Value);

            //Apply preserve product min rule
            assortmentProduct.ClearRule();
            assortmentProduct.PreserveListUnits = 400;

            units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure minimum value is met
            Assert.AreEqual(400, units.Value);

            //max value preserve/minimum hurdle rule
            assortmentProduct.ClearRule();
            assortmentProduct.PreserveListUnits = 20;
            assortmentProduct.MaxListUnits = 26;
            inventoryProfile.CasePack = 700;
            inventoryProfile.ShelfLife = 2;

            units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure maxium value is kept when exceeded
            Assert.AreEqual(26, units.Value, "28 was calculated, but max value should set it to 26");
                        
            //local product

            //Setup local product with different location to plan location for this product
            _planogram.LocationCode = "402";
            PlanogramAssortmentLocalProduct localProduct = PlanogramAssortmentLocalProduct.NewPlanogramAssortmentLocalProduct();
            localProduct.LocationCode = "401";
            localProduct.ProductGtin = assortmentProduct.Gtin;
            _planogram.Assortment.LocalProducts.Add(localProduct);

            units = Task.CalculateUnitsAndLogOnError(
               assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure maxium value is kept when exceeded
            Assert.IsNull(units, "product should have 0 units as its not local to plans location");

            //Set local product to match this location
            _planogram.Assortment.LocalProducts[0].LocationCode = "402";
            assortmentProduct.ClearRule();

            units = Task.CalculateUnitsAndLogOnError(
               assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            Assert.AreNotEqual(0, units.Value, "product should have units calculated as its now local to this plans location");
            
            //regional product

            //create region and set product as primary product
            PlanogramAssortmentRegion testRegion = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();
            PlanogramAssortmentRegionProduct testRegionProduct = PlanogramAssortmentRegionProduct.NewPlanogramAssortmentRegionProduct();
            testRegionProduct.PrimaryProductGtin = assortmentProduct.Gtin;
            testRegionProduct.RegionalProductGtin = "Test";
            PlanogramAssortmentRegionLocation testRegionLocation = PlanogramAssortmentRegionLocation.NewPlanogramAssortmentRegionLocation();
            testRegionLocation.LocationCode = "402";
            testRegion.Products.Add(testRegionProduct);
            testRegion.Name = "Region 1";
            testRegion.Locations.Add(testRegionLocation);

            _planogram.Assortment.Regions.Add(testRegion);
            assortmentProduct.IsPrimaryRegionalProduct = true;

            units = Task.CalculateUnitsAndLogOnError(
               assortmentProduct, context, _productsByGtin, _performanceDataByProductId, _assortmentInventoryRulesByGtin, inventoryProfile);

            //Ensure maxium value is kept when exceeded
            Assert.IsNull(units, "product should have 0 units as its a regional product");

        }

        [Test]
        public void CalculateUnitsAndLogOnError_MinimumCasePacksAreSet()
        {
            const Int32 casePackUnits = 7;
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 1;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;
            PlanogramAssortmentInventoryRule inventoryRule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(assortmentProduct);
            PlanogramProduct planProduct = _planogram.Products.First(p => p.Gtin.Equals(assortmentProduct.Gtin));
            inventoryRule.CasePack = 5;
            _planogram.Assortment.InventoryRules.Add(inventoryRule);
            _assortmentInventoryRulesByGtin.Add(assortmentProduct.Gtin, inventoryRule);

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct, 
                context, 
                _productsByGtin, 
                _performanceDataByProductId, 
                _assortmentInventoryRulesByGtin, 
                inventoryProfile);

            units.Should().Be(35, "because there should be 5 case packs at 7 units each.");
        }

        [Test]
        public void CalculateUnitsAndLogOnError_MaxUnitsIsEnforced()
        {
            const Int32 casePackUnits = 7;
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 5;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;
            PlanogramProduct planProduct = _planogram.Products.First(p => p.Gtin.Equals(assortmentProduct.Gtin));
            assortmentProduct.MaxListUnits = 20;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct,
                context,
                _productsByGtin,
                _performanceDataByProductId,
                _assortmentInventoryRulesByGtin,
                inventoryProfile);

            units.Should().Be(20, "because the max list units should restrict the units calculated");
        }

        [Test]
        public void CalculateUnitsAndLogOnError_MaxShelfLifeIsEnforced()
        {
            const Int32 casePackUnits = 7;
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 1;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;
            PlanogramAssortmentInventoryRule inventoryRule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(assortmentProduct);
            PlanogramProduct planProduct = _planogram.Products.First(p => p.Gtin.Equals(assortmentProduct.Gtin));
            inventoryRule.CasePack = 5;
            _planogram.Assortment.InventoryRules.Add(inventoryRule);
            _assortmentInventoryRulesByGtin.Add(assortmentProduct.Gtin, inventoryRule);
            _assortmentInventoryRulesByGtin.First().Value.ShelfLife = 0.50f;
            _performanceDataByProductId[_productsByGtin[assortmentProduct.Gtin].Id].UnitsSoldPerDay = 0.143f;
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 50;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct,
                context,
                _productsByGtin,
                _performanceDataByProductId,
                _assortmentInventoryRulesByGtin,
                inventoryProfile);

            units.Should().Be(3, "because more than 3 units would exceed the maximum shelf life.");
        }

        [Test]
        public void CalculateUnitsAndLogOnError_MinDosIsEnforced()
        {
            const Int32 casePackUnits = 7;
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 1;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;
            PlanogramAssortmentInventoryRule inventoryRule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(assortmentProduct);
            PlanogramProduct planProduct = _planogram.Products.First(p => p.Gtin.Equals(assortmentProduct.Gtin));
            _planogram.Assortment.InventoryRules.Add(inventoryRule);
            _performanceDataByProductId[_productsByGtin[assortmentProduct.Gtin].Id].UnitsSoldPerDay = 1;
            _assortmentInventoryRulesByGtin.Add(assortmentProduct.Gtin, inventoryRule);
            _assortmentInventoryRulesByGtin.First().Value.DaysOfSupply = 10;


            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct,
                context,
                _productsByGtin,
                _performanceDataByProductId,
                _assortmentInventoryRulesByGtin,
                inventoryProfile);

            units.Should().Be(10, "because there should be a minimum of 10 days of supply");
        }

        [Test]
        public void CalculateUnitsAndLogOnError_MinDeliveryDaysIsEnforced()
        {
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 1;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = 1;
            PlanogramAssortmentInventoryRule inventoryRule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(assortmentProduct);
            PlanogramProduct planProduct = _planogram.Products.First(p => p.Gtin.Equals(assortmentProduct.Gtin));
            _planogram.Assortment.InventoryRules.Add(inventoryRule);
            _performanceDataByProductId[_productsByGtin[assortmentProduct.Gtin].Id].UnitsSoldPerDay = 0.143f;
            _assortmentInventoryRulesByGtin.Add(assortmentProduct.Gtin, inventoryRule);
            _assortmentInventoryRulesByGtin.First().Value.ReplenishmentDays = 20;
            _productsByGtin[assortmentProduct.Gtin].DeliveryFrequencyDays = 2;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct,
                context,
                _productsByGtin,
                _performanceDataByProductId,
                _assortmentInventoryRulesByGtin,
                inventoryProfile);

            units.Should().Be(6, "because there should be 20 replenishment days of units");
        }

        [Test]
        public void CalculateUnitsAndLogOnError_MinUnitsIsEnforced()
        {
            const Int32 casePackUnits = 7;
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 1;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;
            PlanogramAssortmentInventoryRule inventoryRule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(assortmentProduct);
            PlanogramProduct planProduct = _planogram.Products.First(p => p.Gtin.Equals(assortmentProduct.Gtin));
            _planogram.Assortment.InventoryRules.Add(inventoryRule);
            _performanceDataByProductId[_productsByGtin[assortmentProduct.Gtin].Id].UnitsSoldPerDay = 0.143f;
            _assortmentInventoryRulesByGtin.Add(assortmentProduct.Gtin, inventoryRule);
            _assortmentInventoryRulesByGtin.First().Value.MinUnits = 80;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct,
                context,
                _productsByGtin,
                _performanceDataByProductId,
                _assortmentInventoryRulesByGtin,
                inventoryProfile);

            units.Should().Be(80, "because there is a minimum units rule of 80");
        }

        [Test]
        public void CalculateUnitsAndLogOnError_WasteHurdleUnitsIsEnforced()
        {
            const Int32 casePackUnits = 7;
            var contextMock = new Mock<ITaskContext>();
            contextMock.Setup(c => c.Planogram).Returns(_planogram);
            var context = new Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.TaskContext((ITaskContext)contextMock.Object);
            var inventoryProfile = Galleria.Ccm.Model.InventoryProfile.NewInventoryProfile(1);
            inventoryProfile.CasePack = 1;
            inventoryProfile.ShelfLife = 2;
            var assortmentProduct = _planogram.Assortment.Products.First();
            _productsByGtin[assortmentProduct.Gtin].ShelfLife = 100;
            _productsByGtin[assortmentProduct.Gtin].CasePackUnits = casePackUnits;
            PlanogramAssortmentInventoryRule inventoryRule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(assortmentProduct);
            PlanogramProduct planProduct = _planogram.Products.First(p => p.Gtin.Equals(assortmentProduct.Gtin));
            _planogram.Assortment.InventoryRules.Add(inventoryRule);
            _performanceDataByProductId[_productsByGtin[assortmentProduct.Gtin].Id].UnitsSoldPerDay = 0.143f;
            _assortmentInventoryRulesByGtin.Add(assortmentProduct.Gtin, inventoryRule);
            _assortmentInventoryRulesByGtin.First().Value.WasteHurdleUnits = 100;

            Int16? units = Task.CalculateUnitsAndLogOnError(
                assortmentProduct,
                context,
                _productsByGtin,
                _performanceDataByProductId,
                _assortmentInventoryRulesByGtin,
                inventoryProfile);

            units.Should().Be(0, "because there is a waste hurdle units rule in force.");
        }

        #endregion

        #region GetUnNormalisedPsi
        private Byte[] _oneToTwenty
        {
            get
            {
                return new Byte[]
                {
                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
                };
            }
        }

        [Test]
        public void GetUnNormalisedPsi_ReturnsNull_WhenNoMatchingPerformanceData()
        {
            //create the global metrics list
            MetricList globalMetricList = MetricList.NewMetricList();
            Metric globalMetric1 = Metric.NewMetric(0);
            globalMetric1.Name = "Metric1";
            globalMetricList.Add(globalMetric1);

            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            planogram.Products.Add(product);
            planogram.Performance.PerformanceData.Clear();
            var performanceDataByProductId = planogram.Performance.PerformanceData.ToDictionary(p => p.PlanogramProductId, p => p);

            //create the metric profile
            var metricProfile = Galleria.Ccm.Model.MetricProfile.NewMetricProfile(1);
            metricProfile.Metric1MetricId = globalMetric1.Id;
            metricProfile.Metric1Ratio = 1;

            //nb - dont add metric to plan metrics.

            var performanceDataTotals = new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData);


            var actual = Task.GetUnNormalisedPsi(product, null, performanceDataTotals, performanceDataByProductId);

            Assert.IsNull(actual);
        }

        [Test]
        [TestCaseSource("_oneToTwenty")]
        public void GetUnNormalisedPsi_MetricCalculated_WhenMaximumIncrease(Byte metricNumber)
        {
            //create global metrics
            MetricList globalMetricList = MetricList.NewMetricList();
            Metric globalMetric = Metric.NewMetric(Context.EntityId);
            globalMetric.Name = "Metric"+ metricNumber;
            globalMetricList.Add(globalMetric);


            var random = new Random();
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            planogram.Products.Add(product);

            var performanceData = planogram.Performance.PerformanceData.First();
            var performanceDataByProductId = planogram.Performance.PerformanceData.ToDictionary(p => p.PlanogramProductId, p => p);
            typeof(PlanogramPerformanceData).GetProperty(String.Format("P{0}", metricNumber)).SetValue(performanceData, (Single?)random.Next(1, 10), null);


            //add plan metrics
            var metric = planogram.Performance.Metrics.FirstOrDefault(m => m.MetricId == metricNumber);
            if (metric == null)
            {
                metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(metricNumber);
                planogram.Performance.Metrics.Add(metric);
            }
            metric.Name = globalMetric.Name;
            metric.Direction = MetricDirectionType.MaximiseIncrease;


            //create the metric profile
            var metricProfile = Galleria.Ccm.Model.MetricProfile.NewMetricProfile(1);
            metricProfile.GetType().GetProperty(String.Format("Metric{0}MetricId", metricNumber)).SetValue(metricProfile, globalMetric.Id, null);
            metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).SetValue(metricProfile, (Single?)random.Next(1, 100) / 100, null);

            var performanceDataTotals = new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData);

            //resolve plan metrics
            IPlanogramMetricProfile profile = metricProfile.ToPlanogramMetricProfile(planogram.Performance, globalMetricList);

            var metricRatio = (Single)metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).GetValue(metricProfile, null);
            var dataValue = (Single)performanceData.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceData, null);
            var dataValueTotal = (Single)performanceDataTotals.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceDataTotals, null);
            var expected = metricRatio * (dataValue / dataValueTotal);
            var actual = Task.GetUnNormalisedPsi(product, profile, performanceDataTotals, performanceDataByProductId);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource("_oneToTwenty")]
        public void GetUnNormalisedPsi_MetricCalculated_WhenMaximumDecrease(Byte metricNumber)
        {
            //create global metrics
            MetricList globalMetricList = MetricList.NewMetricList();
            Metric globalMetric = Metric.NewMetric(0);
            globalMetric.Name = "Metric" + metricNumber;
            globalMetricList.Add(globalMetric);

            var random = new Random();
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            planogram.Products.Add(product);
            var product2 = PlanogramProduct.NewPlanogramProduct();
            planogram.Products.Add(product2);

           
            var performanceData = planogram.Performance.PerformanceData.First();
            var performanceDataByProductId = planogram.Performance.PerformanceData.ToDictionary(p => p.PlanogramProductId, p => p);
            foreach (var performanceDataItem in planogram.Performance.PerformanceData)
            {
                typeof(PlanogramPerformanceData).
                    GetProperty(String.Format("P{0}", metricNumber)).
                    SetValue(performanceDataItem, (Single?)random.Next(1, 10), null);
            }

            //add plan metrics
            var metric = planogram.Performance.Metrics.FirstOrDefault(m=>m.MetricId == metricNumber);
            if (metric == null)
            {
                metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(metricNumber);
                planogram.Performance.Metrics.Add(metric);
            }
            metric.Name = globalMetric.Name;
            metric.Direction = MetricDirectionType.MaximiseDecrease;

            var metricProfile = Galleria.Ccm.Model.MetricProfile.NewMetricProfile(1);
            metricProfile.GetType().GetProperty(String.Format("Metric{0}MetricId", metricNumber)).SetValue(metricProfile, globalMetric.Id, null);
            metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).SetValue(metricProfile, (Single?)((Single)random.Next(1, 100) / 100f), null);
            var performanceDataTotals = new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData);

            //resolve plan metrics
            IPlanogramMetricProfile profile = metricProfile.ToPlanogramMetricProfile(planogram.Performance, globalMetricList);

            var metricRatio = (Single)metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).GetValue(metricProfile, null);
            var dataValue = (Single)performanceData.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceData, null);
            var dataValueTotal = (Single)performanceDataTotals.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceDataTotals, null);
            var expected = metricRatio * (1f - (dataValue / dataValueTotal));
            var actual = Task.GetUnNormalisedPsi(product, profile, performanceDataTotals, performanceDataByProductId);

            actual.Should().BeApproximately(expected, 0.01f);
        }
        #endregion

        #region PerformanceDataIsEmpty

        [Test]
        public void ExecuteTask_WhenPerformanceDataIsEmpty_UnitsShouldRemainAsBefore()
        {
            Assert.Inconclusive("This behaviour is unconfirmed at present (AK - 12/10/15)");
            TestPlanogram.AddProduct();
            TestPlanogram.CreateAssortmentFromProducts();
            var assortmentProduct = TestPlanogram.Assortment.Products.First();
            assortmentProduct.Units = 10;
            var task = new Task();
            var metricDtos = TestDataHelper.InsertMetricDtos(DalFactory, Context.EntityId, 10);
            var metricProfileName = TestDataHelper.InsertMetricProfileDtos(base.DalFactory, 1, Context.EntityId, metricDtos).First().Name;
            var inventoryProfileName = TestDataHelper.InsertInventoryProfileDtos(DalFactory, Context.EntityId, 1).First().Name;
            base.SetTaskParameterValues(0, new Object[] { metricProfileName });
            base.SetTaskParameterValues(1, new Object[] { inventoryProfileName });

            task.ExecuteTask(Context);

            Assert.That(assortmentProduct.Units, Is.EqualTo(10));
        }

        #endregion

        #region NormaliseMetrics
        [Test]
        public void ExecuteTask_CorrectlyNormalisesMetricProfile()
        {
            var entityId = Context.EntityId;           

            var prod1 = TestPlanogram.AddProduct();
            prod1.Gtin = "1";
            var prod2 = TestPlanogram.AddProduct();
            prod2.Gtin = "2";
            
            var prod1Performance = prod1.GetPlanogramPerformanceData();
            prod1Performance.P1 = 5;
            prod1Performance.P2 = 1;
            var prod2Performance = prod2.GetPlanogramPerformanceData();
            prod2Performance.P1 = 5;
            prod2Performance.P2 = 3;
            TestPlanogram.CreateAssortmentFromProducts();

            var metric1 = Metric.NewMetric(entityId);
            metric1.Name = "Metric1";
            metric1.Description = "Description";
            metric1.DataModelType = MetricElasticityDataModelType.None;
            metric1.SpecialType = MetricSpecialType.None;
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            var metric2 = Metric.NewMetric(entityId);
            metric2.Name = "Metric2";
            metric2.Description = "Description";
            metric2.DataModelType = MetricElasticityDataModelType.None;
            metric2.SpecialType = MetricSpecialType.None;
            metric2.Direction = MetricDirectionType.MaximiseIncrease;
            var metrics = MetricList.NewMetricList();
            metrics.AddRange(new[] { metric1, metric2 });
            metrics = metrics.Save();
            metric1 = metrics.First(m => m.Name.Equals(metric1.Name));
            metric2 = metrics.First(m => m.Name.Equals(metric2.Name));

            var planMetric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            planMetric1.Name = metric2.Name;
            var planMetric2 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(2);
            planMetric2.Name = metric1.Name;

            TestPlanogram.Performance.Metrics.Clear();
            TestPlanogram.Performance.Metrics.AddRange(new[] { planMetric1, planMetric2 });

            var metricProfile = MetricProfile.NewMetricProfile(entityId);
            metricProfile.Name = "Metric Profile";
            metricProfile.Metric1MetricId = metric1.Id;
            metricProfile.Metric1Ratio =0f;
            metricProfile.Metric2MetricId = metric2.Id;
            metricProfile.Metric2Ratio = 1f;
            metricProfile.Save();

            var inventoryProfile = InventoryProfile.NewInventoryProfile(entityId);
            inventoryProfile.Name = "Inventory Profile";
            inventoryProfile.EntityId = entityId;
            inventoryProfile.CasePack = 1;
            inventoryProfile.DaysOfSupply = 1;
            inventoryProfile.ShelfLife = 1;
            inventoryProfile.ReplenishmentDays = 1;
            inventoryProfile.WasteHurdleUnits = 1;
            inventoryProfile.WasteHurdleCasePack = 1;
            inventoryProfile.MinUnits = 1;
            inventoryProfile.MinFacings = 1;
            inventoryProfile.DateCreated = DateTime.UtcNow;
            inventoryProfile.DateLastModified = DateTime.UtcNow;
            inventoryProfile.DateDeleted = null;
            inventoryProfile.Save();

            // Set Task Parameters : MetricProfileName, InventoryProfileName, CalculationOptions, RangeOptions
            SetTaskParameterValues(0, new Object[] { metricProfile.Name });
            SetTaskParameterValues(1, new Object[] { inventoryProfile.Name });
            SetTaskParameterValues(2, new Object[] { CalculationOptionsType.RankAndUnits });
            SetTaskParameterValues(3, new Object[] { RangeOptionsType.AssortmentProductsOnly });


            _task.ExecuteTask(Context);


            PlanogramAssortment assortment = TestPlanogram.Assortment;

            short prod1Rank = assortment.Products.First().Rank;
            short prod2Rank = assortment.Products.Last().Rank;

            //If we have normalised it will work prod 1 = rank 1. If not normalised it would be rank 2.
            Assert.AreEqual(prod1Rank, 1, "Rank was not the same as expected");
            Assert.AreEqual(prod2Rank, 2, "Rank was not the same as expected");


        }

        #endregion

        [Test, Category(Categories.Smoke)]
        public void ExecuteTask_FailsWhenMetricsNotInPlan()
        {
            //CCM-18670 - Checks scenario where both plan and profile metrics are in
            // global but do not actually match up with each other.

            Int32 entityId = Context.EntityId;

            //start with 6 global metrics
            MetricList globalMetricList = MetricList.NewMetricList();
            for (Byte i = 1; i <= 6; i++)
            {
                //add to global
                Metric m1 = Metric.NewMetric(entityId);
                m1.Name = "Metric" + i;
                m1.Description = "metric desc";
                globalMetricList.Add(m1);
            }
            globalMetricList = globalMetricList.Save();

            //add the first 3 to the profile
            var metricProfile = Galleria.Ccm.Model.MetricProfile.NewMetricProfile(entityId);
            metricProfile.Name = "FailsWhenMetricsNotInPlan";
            for (Byte i = 1; i <= 3; i++)
            {
                Metric m1 = globalMetricList[i - 1];
                var propertyInfo = typeof(Galleria.Ccm.Model.MetricProfile).GetProperty(String.Format("Metric{0}MetricId", i));
                propertyInfo.SetValue(metricProfile, m1.Id, null);

                propertyInfo = typeof(Galleria.Ccm.Model.MetricProfile).GetProperty(String.Format("Metric{0}Ratio", i));
                propertyInfo.SetValue(metricProfile, (Single?)1, null);
            }
            metricProfile = metricProfile.Save();

            //add the final 3 to the plan
            TestPlanogram.Performance.Metrics.Clear();
            for (Byte i = 4; i <= 6; i++)
            {
                Metric m1 = globalMetricList[i - 1];
                var metric = _planogram.Performance.Metrics.FirstOrDefault(m => m.MetricId == i);
                if (metric == null)
                {
                    metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(i);
                    TestPlanogram.Performance.Metrics.Add(metric);
                }
                metric.Name = m1.Name;
                metric.Direction = MetricDirectionType.MaximiseIncrease;
            }


            var inventoryProfile = InventoryProfile.NewInventoryProfile(entityId);
            inventoryProfile.Name = "Inventory Profile";
            inventoryProfile.EntityId = entityId;
            inventoryProfile.CasePack = 1;
            inventoryProfile.DaysOfSupply = 1;
            inventoryProfile.ShelfLife = 1;
            inventoryProfile.ReplenishmentDays = 1;
            inventoryProfile.WasteHurdleUnits = 1;
            inventoryProfile.WasteHurdleCasePack = 1;
            inventoryProfile.MinUnits = 1;
            inventoryProfile.MinFacings = 1;
            inventoryProfile.DateCreated = DateTime.UtcNow;
            inventoryProfile.DateLastModified = DateTime.UtcNow;
            inventoryProfile.DateDeleted = null;
            inventoryProfile.Save();


            // Set Task Parameters : MetricProfileName, InventoryProfileName, CalculationOptions, RangeOptions
            SetTaskParameterValues(0, new Object[] { metricProfile.Name });
            SetTaskParameterValues(1, new Object[] { inventoryProfile.Name });
            SetTaskParameterValues(2, new Object[] { CalculationOptionsType.RankAndUnits });
            SetTaskParameterValues(3, new Object[] { RangeOptionsType.AssortmentProductsOnly });


            //Execute
            _task.ExecuteTask(Context);

            //check that the error was logged.
            AssertLogError(EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile);
        }


        [Test, Category(Categories.Smoke)]
        public void ExecuteTask_CorrectWhenPlanMetricsDoNotStartInP1()
        {
            //CCM-18670 - Checks scenario where metric match up but the profile only
            // has 1 metric and the plan performance data sits in p4.


            Int32 entityId = Context.EntityId;

            //add a global metric
            MetricList globalMetricList = MetricList.NewMetricList();
            Metric m1 = Metric.NewMetric(entityId);
            m1.Name = "Metric1";
            m1.Description = "metric desc";
            globalMetricList.Add(m1);
            globalMetricList = globalMetricList.Save();
            m1 = globalMetricList[0];


            //metric profile:
            var metricProfile = Galleria.Ccm.Model.MetricProfile.NewMetricProfile(entityId);
            metricProfile.Name = "FailsWhenMetricsNotInPlan";
            metricProfile.Metric1MetricId = m1.Id;
            metricProfile.Metric1Ratio = 1;
            metricProfile = metricProfile.Save();

            //add the plan metric in p4.
            TestPlanogram.Performance.Metrics.Clear();
            var metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(4);
            metric.Name = m1.Name;
            metric.Direction = MetricDirectionType.MaximiseIncrease;
            TestPlanogram.Performance.Metrics.Add(metric);

            //set perf values to p4 only
            Random rand = new Random();
            foreach (var perfData in TestPlanogram.Performance.PerformanceData)
            {
                perfData.ClearAllValues();
                perfData.P4 = (Single)rand.Next();
            }

            var inventoryProfile = InventoryProfile.NewInventoryProfile(entityId);
            inventoryProfile.Name = "Inventory Profile";
            inventoryProfile.EntityId = entityId;
            inventoryProfile.CasePack = 1;
            inventoryProfile.DaysOfSupply = 1;
            inventoryProfile.ShelfLife = 1;
            inventoryProfile.ReplenishmentDays = 1;
            inventoryProfile.WasteHurdleUnits = 1;
            inventoryProfile.WasteHurdleCasePack = 1;
            inventoryProfile.MinUnits = 1;
            inventoryProfile.MinFacings = 1;
            inventoryProfile.DateCreated = DateTime.UtcNow;
            inventoryProfile.DateLastModified = DateTime.UtcNow;
            inventoryProfile.DateDeleted = null;
            inventoryProfile.Save();


            // Set Task Parameters : MetricProfileName, InventoryProfileName, CalculationOptions, RangeOptions
            SetTaskParameterValues(0, new Object[] { metricProfile.Name });
            SetTaskParameterValues(1, new Object[] { inventoryProfile.Name });
            SetTaskParameterValues(2, new Object[] { CalculationOptionsType.RankAndUnits });
            SetTaskParameterValues(3, new Object[] { RangeOptionsType.AssortmentProductsOnly });

            //Execute
            _task.ExecuteTask(Context);


            List<Object> expectedProductOrder = 
                TestPlanogram.Products
                .Where(p=> p.GetPlanogramAssortmentProduct() != null)
                .OrderBy(p => p.GetPlanogramPerformanceData().P4).Select(p=> p.Id).ToList();

            List<Object> actualOrder =
                TestPlanogram.Products
                .Where(p => p.GetPlanogramAssortmentProduct() != null)
                .OrderBy(p => p.GetPlanogramAssortmentProduct().Rank).Select(p => p.Id).ToList();

            Assert.IsTrue(expectedProductOrder.SequenceEqual(actualOrder), "order should match");

        }
    }
}
