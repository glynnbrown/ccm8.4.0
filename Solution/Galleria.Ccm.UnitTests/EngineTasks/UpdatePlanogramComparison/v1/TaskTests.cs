﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-31909 : A.Silva
//  Created

#endregion

#endregion

using Galleria.Ccm.Engine.Tasks.UpdatePlanogramComparison.v1;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdatePlanogramComparison.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        /// <summary>
        ///     The <see cref="Task"/> being tested.
        /// </summary>
        private Task _task;

        #endregion

        #region Test Helper Methods

        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        #endregion
    }
}