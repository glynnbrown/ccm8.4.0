﻿using System;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.AddProductsUsingProductUniverse.v1;
using Galleria.Ccm.Engine.Tasks.ProductBuddyAssignment.V1;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog.Implementation;
using Galleria.Framework.Planograms.Interfaces;
using Task = Galleria.Ccm.Engine.Tasks.ProductBuddyAssignment.V1.Task;

namespace Galleria.Ccm.UnitTests.EngineTasks.ProductBuddyAssignment.V1
{
    [TestFixture]
    internal class TaskTests : TaskTestBase<Task>
    {
        [SetUp]
        public void SetUp()
        {
            List<string> productGtins = TestDataHelper.InsertProductDtos(DalFactory, 40, Context.EntityId).Select(dto => dto.Gtin).ToList();

            // Add all the Products to the assortment.
            TestPlanogram.Assortment.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, productGtins)
                .Cast<IPlanogramProductInfo>()
                .Select(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct));
            foreach (var p in TestPlanogram.Assortment.Products) { p.IsRanged = true; }
            
        }
        
        [Test]
        public void CheckThatANewBuddyCanBeAddedAfterTheExisitngBuddiesHasBeenRemoved()
        {
            var taskToTest = new Task();

            // Due to CSLA saving from the root of an object, we have to retrieve the specific object, add the custom attributes and save it.
            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1"});
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin20";
            customAttributeData.Text24 = "50%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            var expectedProductBuddies = CreatedProductBuddies(new string[] {"gtin1|gitn20|50"});
            
            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.ClearAllBuddiesFromAssortment });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(1));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.50F));
            
        }

        [Test]
        public void CheckThatANewBuddyCanBeAddedAfterTheExisitngBuddiesHasBeenRemovedWithNoPercentage()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin20";
            customAttributeData.Text24 = "";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.ClearAllBuddiesFromAssortment });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(1));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(1.00F));

        }

        [Test]
        public void CheckThatANewBuddyCanBeAddedAfterTheExisitngBuddiesHasBeenRemovedWithInvalidPrecentage()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin20";
            customAttributeData.Text24 = "AA";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.ClearAllBuddiesFromAssortment });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(1));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(1.00F));

        }

        [Test]
        public void CheckThatANewBuddyAreNotAddedWhenModeIsMissing()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin20";
            customAttributeData.Text24 = "20%";
            customAttributeData.Text25 = "";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { null });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(2));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.20F));

            Assert.That(productBuddies[1].S1ProductGtin, Is.EqualTo("gtin21").IgnoreCase);
            Assert.That(productBuddies[1].S1Percentage, Is.EqualTo(0.50F));

        }

        
        [Test]
        public void CheckThatBuddyIsNotCreatedWhenTheSourceProductIsNotInTheAssortment()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin40";
            customAttributeData.Text24 = "20%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { null });

            TestPlanogram.Assortment.Products.Clear();
            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(2));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.20F));

            Assert.That(productBuddies[1].S1ProductGtin, Is.EqualTo("gtin21").IgnoreCase);
            Assert.That(productBuddies[1].S1Percentage, Is.EqualTo(0.50F));
        }

        [Test]
        public void CheckThatBuddyIsNotCreatedWhenProductBuddyGTINIsEmpty()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "";
            customAttributeData.Text24 = "20%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { null });

            TestPlanogram.Assortment.Products.Clear();
            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(2));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.20F));

            Assert.That(productBuddies[1].S1ProductGtin, Is.EqualTo("gtin21").IgnoreCase);
            Assert.That(productBuddies[1].S1Percentage, Is.EqualTo(0.50F));
        }

        [Test]
        public void CheckThatErrorIsProducedWhenInvalidBuddyGTINIsEntered()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "cat1";
            customAttributeData.Text24 = "20%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { null });
            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(2));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.20F));

            Assert.That(productBuddies[1].S1ProductGtin, Is.EqualTo("gtin21").IgnoreCase);
            Assert.That(productBuddies[1].S1Percentage, Is.EqualTo(0.50F));
        }

        [Test]
        public void CheckThatErrorIsProducedWhenMultpleBuddyGTINAreEntered()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin20,gtin21,gtin22";
            customAttributeData.Text24 = "20%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20",
                "gtin1|gtin21|50",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { null });
            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(2));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.20F));

            Assert.That(productBuddies[1].S1ProductGtin, Is.EqualTo("gtin21").IgnoreCase);
            Assert.That(productBuddies[1].S1Percentage, Is.EqualTo(0.50F));
        }
        

        [Test]
        public void CheckThatWhenBuddyIsMergedInExisitngHasPriortyOverExistingBuddyItIsAddedWhenBuddiesDoNotExists()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin22";
            customAttributeData.Text24 = "10%";
            product.Save();
            
            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.MergeBuddiesExistingHavePriority });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(TestPlanogram.Assortment.ProductBuddies.Count, Is.EqualTo(1));

            Assert.That(TestPlanogram.Assortment.ProductBuddies[0].S1ProductGtin, Is.EqualTo("gtin22").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[0].S1Percentage, Is.EqualTo(0.10F));
        }

        [Test]
        public void CheckThatWhenBuddyIsMergedInExisitngHasPriortyOverExistingBuddyItIsNotAddedWhenBuddiesExists()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin22";
            customAttributeData.Text24 = "10%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20"
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.MergeBuddiesExistingHavePriority });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(TestPlanogram.Assortment.ProductBuddies.Count, Is.EqualTo(1));

            Assert.That(TestPlanogram.Assortment.ProductBuddies[0].S1ProductGtin, Is.EqualTo("gtin20").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[0].S1Percentage, Is.EqualTo(0.20F));
        }

        [Test]
        public void CheckThatWhenBuddyIsMergedInItHasPriortyOverExistingBuddyItIsAddedWhenBuddiesDoNotExists()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin22";
            customAttributeData.Text24 = "10%";
            product.Save();

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.MergeBuddiesNewHavePriority });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(1));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin22").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.10F));
            
        }

        [Test]
        public void CheckThatWhenBuddyIsMergedInItHasPriortyOverExistingBuddyWhenBuddiesExists()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin22";
            customAttributeData.Text24 = "10%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|20"
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.MergeBuddiesNewHavePriority });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            List<PlanogramAssortmentProductBuddy> productBuddies =
                TestPlanogram.Assortment.ProductBuddies.Where(b => b.ProductGtin.Equals(testPlanogramProduct.Gtin, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Assert.IsNotNull(productBuddies);
            Assert.That(productBuddies.Count, Is.EqualTo(1));

            Assert.That(productBuddies[0].S1ProductGtin, Is.EqualTo("gtin22").IgnoreCase);
            Assert.That(productBuddies[0].S1Percentage, Is.EqualTo(0.10F));
        }

        [Test]
        public void CheckThatWhenBuddyIsMergedInItHasPriortyOverExistingBuddyWhenOtherBuddiesExists()
        {
            var taskToTest = new Task();

            PlanogramAssortmentProduct testPlanogramProduct = TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals("gtin1", StringComparison.InvariantCultureIgnoreCase));
            ProductList products = ProductList.FetchByEntityIdProductGtins(Context.EntityId, new List<String> { "gtin1" });
            Product product = Product.FetchById(products.First().Id);
            CustomAttributeData customAttributeData = product.CustomAttributes;
            customAttributeData.Text23 = "gtin22";
            customAttributeData.Text24 = "10%";
            product.Save();

            // Added buddies in to test the remove
            var buddiestoAdd = new List<string>()
            {
                "gtin1|gtin20|10",
                "gtin2|gtin30|20",
                "gtin3|gtin40|30",
            };

            CreatedProductBuddies(buddiestoAdd);

            SetTaskParameterValues((int)Task.Parameter.BuddyProductGTIN, new Object[] { new TaskParameterValue(Task.Parameter.BuddyProductGTIN, "Text23") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyPercentage, new Object[] { new TaskParameterValue(Task.Parameter.BuddyPercentage, "Text24") }, true);
            SetTaskParameterValues((int)Task.Parameter.BuddyCreationType, new object[] { BuddyCreationType.MergeBuddiesNewHavePriority });

            taskToTest.ExecuteTask(Context);

            Assert.That(TestPlanogram.Assortment, Is.Not.Null);
            Assert.That(testPlanogramProduct, Is.Not.Null);

            Assert.IsNotNull(TestPlanogram.Assortment.ProductBuddies);
            Assert.That(TestPlanogram.Assortment.ProductBuddies.Count, Is.EqualTo(3));

            Assert.That(TestPlanogram.Assortment.ProductBuddies[0].ProductGtin, Is.EqualTo("gtin2").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[0].S1ProductGtin, Is.EqualTo("gtin30").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[0].S1Percentage, Is.EqualTo(0.20F));

            Assert.That(TestPlanogram.Assortment.ProductBuddies[1].ProductGtin, Is.EqualTo("gtin3").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[1].S1ProductGtin, Is.EqualTo("gtin40").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[1].S1Percentage, Is.EqualTo(0.30F));

            Assert.That(TestPlanogram.Assortment.ProductBuddies[2].ProductGtin, Is.EqualTo("gtin1").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[2].S1ProductGtin, Is.EqualTo("gtin22").IgnoreCase);
            Assert.That(TestPlanogram.Assortment.ProductBuddies[2].S1Percentage, Is.EqualTo(0.10F));
        }

        private List<PlanogramAssortmentProductBuddy> CreatedProductBuddies(IEnumerable<String> buddiestoAdd)
        {
            var buddiesToReturn = new List<PlanogramAssortmentProductBuddy>();
            foreach (String buddyToAdd in buddiestoAdd)
            {
                var values = buddyToAdd.Split('|');
                var buddy = PlanogramAssortmentProductBuddy.NewPlanogramAssortmentProductBuddy(values[0]);
                buddy.S1ProductGtin = values[1];

                var percent = 0F;
                Single.TryParse(values[2], out percent);

                buddy.S1Percentage = (Single?)((percent * 0.01) <= 0.01 ? 1 : percent * 0.01);
                TestPlanogram.Assortment.ProductBuddies.Add(buddy);

                buddiesToReturn.Add(buddy);
            }

            return buddiesToReturn;
        }
    }

    

}