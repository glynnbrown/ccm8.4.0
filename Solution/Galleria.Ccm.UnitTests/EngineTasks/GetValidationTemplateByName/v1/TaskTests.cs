﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811

// V8-30255 : A.Silva
//  Created

#endregion

#endregion

using System;
using Galleria.Ccm.Engine.Tasks.Resources.Language;
using Galleria.Ccm.Model;
using NUnit.Framework;
using Task = Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task;

namespace Galleria.Ccm.UnitTests.EngineTasks.GetValidationTemplateByName.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        /// <summary>
        ///     The Id for the parameter ValidationTemplateName.
        /// </summary>
        private const Int32 ValidationTemplateNameParameterId = 0;

        /// <summary>
        ///     Task being tested.
        /// </summary>
        private Task _task;

        #endregion

        #region Parameter testing

        [Test]
        public void ExecuteTask_WhenMissingValidationTemplateName_ShouldLogError()
        {
            const EventLogEvent expectedEventLogEvent = EventLogEvent.InvalidTaskParameterValue;
            SetTaskParameterValues(ValidationTemplateNameParameterId, new Object[] {null});
            NullifyTaskParameter(ValidationTemplateNameParameterId);

            _task.ExecuteTask(Context);

            AssertLogError(expectedEventLogEvent, Message.GetValidationTemplate_Parameter_ValidationTemplateName_Name, Message.Error_NoValue);
        }

        [Test]
        public void ExecuteTask_WhenNullOrEmptyValidationTemplateName_ShouldLogWarning([Values(null, "")] String testValue)
        {
            const EventLogEvent expectedEventLogEvent = EventLogEvent.InvalidTaskParameterValue;
            SetTaskParameterValues(ValidationTemplateNameParameterId, new[] {testValue});

            _task.ExecuteTask(Context);

            AssertLogWarning(expectedEventLogEvent, Message.GetValidationTemplate_Parameter_ValidationTemplateName_Name, Message.Error_NoValue);
        }

        #endregion

        #region Test Helper Methods

        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        #endregion
    }
}