﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-27494 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM820
// V8-30988 : D.Pleasance
//  Amended to also update assortment product Name
#endregion
#region Version History : CCM830
// V8-31648 : A.Kuszyk
//  Fixed custom attributes test.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Model;
using Moq;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;
using UpdateProductAttributesv1 = Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdateProductAttributes.v1
{
    [TestFixture]
    public class TaskTests : TestBase
    {
        [Test]
        public void UpdateProducts_UpdatesAllProductAttributes()
        {
            // Create master data and planogram product.
            Product source = Product.NewProduct();
            TestDataHelper.SetPropertyValues(source);
            PlanogramProduct planProduct = PlanogramProduct.NewPlanogramProduct();
            planProduct.Gtin = source.Gtin;

            PlanogramAssortmentProduct assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            assortmentProduct.Gtin = source.Gtin;

            // Create product attribute selections
            IEnumerable<UpdateProductAttributesv1.Task.ProductAttributeSelection> productAttributeSelections = PlanogramProduct.
                EnumerateDisplayableFieldInfos(false, false, false).
                Select(f =>
                    new UpdateProductAttributesv1.Task.ProductAttributeSelection(
                        ProductAttributeSourceType.ProductAttributes, f.PropertyName));

            UpdateProductAttributesv1.Task.UpdateProducts(
                new[] { planProduct },
                new[] { assortmentProduct },
                productAttributeSelections,
                new[] { source }.ToDictionary(p => p.Gtin, p => p));

            AssertHelper.AreEqual<IPlanogramProduct>(source, planProduct, new[] { "Id" });
            Assert.AreEqual(source.Name, assortmentProduct.Name);
        }

        [Test]
        public void UpdateProducts_UpdatesAllCustomAttributes()
        {
            // Create master data and planogram product.
            Product source = Product.NewProduct();
            TestDataHelper.SetPropertyValues(source);
            TestDataHelper.SetPropertyValues(source.CustomAttributes);
            PlanogramProduct planProduct = PlanogramProduct.NewPlanogramProduct();
            planProduct.Gtin = source.Gtin;

            PlanogramAssortmentProduct assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            assortmentProduct.Gtin = source.Gtin;

            // Create product attribute selections
            IEnumerable<UpdateProductAttributesv1.Task.ProductAttributeSelection> productAttributeSelections = CustomAttributeData.
                EnumerateDisplayablePropertyInfos().
                Select(p =>
                    new UpdateProductAttributesv1.Task.ProductAttributeSelection(
                        ProductAttributeSourceType.ProductCustomAttributes, String.Format("CustomAttributes.{0}", p.Name)));

            UpdateProductAttributesv1.Task.UpdateProducts(
                new[] { planProduct },
                new[] { assortmentProduct },
                productAttributeSelections,
                new[] { source }.ToDictionary(p => p.Gtin, p => p));

            AssertHelper.AreEqual<ICustomAttributeData>(source.CustomAttributes, planProduct.CustomAttributes, new[] { "Id" });
            Assert.IsTrue(String.IsNullOrEmpty( assortmentProduct.Name), "Assortment Product name should not have been updated.");
        }
    }
}
