﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 830

// V8-32359 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Engine;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Engine.Tasks.UpdateProductAttributesUsingList.v1;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdateProductAttributesUsingList.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        private const Int32 ProductCodesParameterId = 0;
        private const Int32 ProductAttributeUpdateValueParameterId = 1;

        private Task _task;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        [Test]
        public void UpdateProductAttributesUsingListUpdatesCommonAttributes()
        {
            const Int32 expectedFillColour = 1;
            const String expectedShortDescription = "ShortDescription";
            List<PlanogramProduct> products = CreateDefaultPlanogramProducts();
            SetTaskParameterValues(ProductAttributeUpdateValueParameterId, new Object[] { new TaskParameterValue(PlanogramProduct.FillColourProperty.Name, expectedFillColour), new TaskParameterValue(PlanogramProduct.ShortDescriptionProperty.Name, expectedShortDescription) }, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(expectedFillColour, products.First().FillColour);
            Assert.AreEqual(expectedShortDescription, products.Last().ShortDescription);
        }

        private List<PlanogramProduct> CreateDefaultPlanogramProducts()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var products = new List<PlanogramProduct>() {shelf.AddPosition(bay).GetPlanogramProduct(), shelf.AddPosition(bay).GetPlanogramProduct()};
            return products;
        }

        [Test]
        public void UpdateProductAttributesUsingListUpdatesCustomAttributeText()
        {
            const Int32 expectedFillColour = 1;
            const String expectedCustomText10 = "CustomAttributes.Text10";
            List<PlanogramProduct> products = CreateDefaultPlanogramProducts();
            SetTaskParameterValues(ProductAttributeUpdateValueParameterId, new Object[] { new TaskParameterValue(PlanogramProduct.FillColourProperty.Name, expectedFillColour), new TaskParameterValue(PlanogramProduct.CustomAttributesProperty.Name + "." + CustomAttributeData.Text10Property.Name, expectedCustomText10) }, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(expectedCustomText10, products.First().CustomAttributes.Text10);
            Assert.AreEqual(expectedCustomText10, products.Last().CustomAttributes.Text10);
        }

        [Test]
        public void UpdateProductAttributesUsingListUpdatesCustomAttributeValues()
        {
            const Int32 expectedFillColour = 1;
            const Single expectedCustomValue11 = 10.6F;
            List<PlanogramProduct> products = CreateDefaultPlanogramProducts();
            SetTaskParameterValues(ProductAttributeUpdateValueParameterId, new Object[] { new TaskParameterValue(PlanogramProduct.FillColourProperty.Name, expectedFillColour), new TaskParameterValue(PlanogramProduct.CustomAttributesProperty.Name + "." + CustomAttributeData.Value11Property.Name, expectedCustomValue11) }, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(expectedCustomValue11, products.First().CustomAttributes.Value11);
            Assert.AreEqual(expectedCustomValue11, products.Last().CustomAttributes.Value11);
        }

        [Test]
        public void UpdateProductAttributesUsingListUpdatesCustomAttributeFlags()
        {
            const Int32 expectedFillColour = 1;
            const Boolean expectedCustomFlag04 = true;
            List<PlanogramProduct> products = CreateDefaultPlanogramProducts();
            SetTaskParameterValues(ProductAttributeUpdateValueParameterId, new Object[] { new TaskParameterValue(PlanogramProduct.FillColourProperty.Name, expectedFillColour), new TaskParameterValue(PlanogramProduct.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag4Property.Name, expectedCustomFlag04) }, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(expectedCustomFlag04, products.First().CustomAttributes.Flag4);
            Assert.AreEqual(expectedCustomFlag04, products.Last().CustomAttributes.Flag4);
        }

        [Test]
        public void UpdateProductAttributesUsingListUpdatesCustomAttributeDates()
        {
            const Int32 expectedFillColour = 1;
            DateTime customattributesDate10 = DateTime.UtcNow;
            List<PlanogramProduct> products = CreateDefaultPlanogramProducts();
            SetTaskParameterValues(ProductAttributeUpdateValueParameterId, new Object[] { new TaskParameterValue(PlanogramProduct.FillColourProperty.Name, expectedFillColour), new TaskParameterValue(PlanogramProduct.CustomAttributesProperty.Name + "." + CustomAttributeData.Date10Property.Name, customattributesDate10) }, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(customattributesDate10, products.First().CustomAttributes.Date10);
            Assert.AreEqual(customattributesDate10, products.Last().CustomAttributes.Date10);
        }

        [Test]
        public void UpdateProductAttributesUsingListUpdatesOnlyExplicitProductsWhenAppropriate()
        {
            const Int32 expectedFillColour = 1;
            const String expectedShortDescription = "ShortDescription";
            List<PlanogramProduct> products = CreateDefaultPlanogramProducts();
            SetTaskParameterValues(ProductCodesParameterId, new Object[] { products.First().Gtin });
            SetTaskParameterValues(ProductAttributeUpdateValueParameterId, new Object[] { new TaskParameterValue(PlanogramProduct.FillColourProperty.Name, expectedFillColour), new TaskParameterValue(PlanogramProduct.ShortDescriptionProperty.Name, expectedShortDescription) }, true);

            _task.ExecuteTask(Context);

            Assert.AreEqual(expectedFillColour, products.First().FillColour);
            Assert.AreNotEqual(expectedShortDescription, products.Last().ShortDescription);
        }

        [Test]
        public void UpdateProductAttributesUsingListWillNotExecuteWhenAllAttributeParametersIncorrect()
        {
            const Int32 expectedFillColour = 1;
            const String expectedShortDescription = "ShortDescription";
            List<PlanogramProduct> products = CreateDefaultPlanogramProducts();
            SetTaskParameterValues(ProductCodesParameterId, new Object[] { products.First().Gtin });
            SetTaskParameterValues(ProductAttributeUpdateValueParameterId, new Object[] { new TaskParameterValue("Non existing Property", expectedFillColour) }, true);

            _task.ExecuteTask(Context);

            Assert.AreNotEqual(expectedShortDescription, products.First().ShortDescription);
            Assert.AreNotEqual(expectedShortDescription, products.Last().ShortDescription);
            Assert.IsTrue(TestPlanogram.EventLogs.Any(log => log.EntryType == PlanogramEventLogEntryType.Error));
        }
    }
}
