﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29723 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using Galleria.Ccm.Engine.Tasks.ReplaceProductManual.v1;
using Galleria.Ccm.Engine;
using Galleria.Framework.Planograms.Merchandising;
using ccmModel = Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.EngineTasks.ReplaceProductManual.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        private Task _task;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesntExistInProductList()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });
            
            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            test.Add(new TaskParameterValue(productDtos.First().Gtin, productDtos.Last().Gtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            
            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p=> p.Gtin == productDtos.First().Gtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == productDtos.Last().Gtin).FirstOrDefault();
            Assert.IsNull(replacementProduct, "Replacement product should not exist as part of the planogram product list.");
            
            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == productDtos.Last().Gtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replaceSequenceX, replacePosition.SequenceX);
            Assert.IsTrue((replacePosition.FacingsWide == 1));
        }

        [Test]
        public void ExecuteTask_ReplaceMultipleProductsThatDontExistInProductList()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(6, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            String replaceProductGtin1 = productDtos[0].Gtin;
            String replaceProductGtin2 = productDtos[2].Gtin;
            String replacementProductGtin1 = productDtos[5].Gtin;
            String replacementProductGtin2 = productDtos[4].Gtin;

            test.Add(new TaskParameterValue(replaceProductGtin1, replacementProductGtin1));
            test.Add(new TaskParameterValue(replaceProductGtin2, replacementProductGtin2));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replaceProduct1, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replaceProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replaceProduct2, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct1, "Replacement product should not exist as part of the planogram product list.");

            PlanogramProduct replacementProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct2, "Replacement product should not exist as part of the planogram product list.");

            PlanogramPosition replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct1.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition1, "Replace position should exist as part of the planograms positions.");

            PlanogramPosition replacePosition2 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct2.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition2, "Replace position should exist as part of the planograms positions.");

            Int16 replace1SequenceX = replacePosition1.SequenceX;
            Int16 replace2SequenceX = replacePosition2.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replacementProduct1, "Replacement product should exist as part of the planogram product list.");

            replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct1.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition1, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replace1SequenceX, replacePosition1.SequenceX);
            Assert.IsTrue((replacePosition1.FacingsWide == 1));

            replaceProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replaceProduct1, "Replace product should exist as part of the planogram product list.");

            replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct1.Id)).FirstOrDefault();
            Assert.IsNull(replacePosition1, "Replace position should not exist as part of the planograms positions.");

            replacementProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replacementProduct2, "Replacement product should exist as part of the planogram product list.");

            replacePosition2 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct2.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition2, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replace2SequenceX, replacePosition2.SequenceX);
            Assert.IsTrue((replacePosition2.FacingsWide == 1));

            replaceProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replaceProduct2, "Replace product should exist as part of the planogram product list.");

            replacePosition2 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct2.Id)).FirstOrDefault();
            Assert.IsNull(replacePosition2, "Replace position should not exist as part of the planograms positions.");
        }

        [Test]
        public void ExecuteTask_ReplaceMultipleProductsThatDontExistInProductListOnePlacedOtherDoesntFit1()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).Union(PlanogramTestHelper.CreateProductDtos(1, Context.EntityId, 5, size: new WidthHeightDepthValue(120, 10, 10))).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            String replaceProductGtin1 = productDtos[0].Gtin;
            String replaceProductGtin2 = productDtos[2].Gtin;
            String replacementProductGtin1 = productDtos[4].Gtin;
            String replacementProductGtin2 = productDtos[5].Gtin;

            test.Add(new TaskParameterValue(replaceProductGtin1, replacementProductGtin1));
            test.Add(new TaskParameterValue(replaceProductGtin2, replacementProductGtin2));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replaceProduct1, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replaceProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replaceProduct2, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct1, "Replacement product should not exist as part of the planogram product list.");

            PlanogramProduct replacementProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct2, "Replacement product should not exist as part of the planogram product list.");

            PlanogramPosition replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct1.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition1, "Replace position should exist as part of the planograms positions.");

            PlanogramPosition replacePosition2 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct1.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition2, "Replace position should exist as part of the planograms positions.");

            Int16 replace1SequenceX = replacePosition1.SequenceX;
            Int16 replace2SequenceX = replacePosition2.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replacementProduct1, "Replacement product should exist as part of the planogram product list.");

            replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct1.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition1, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replace1SequenceX, replacePosition1.SequenceX);
            Assert.IsTrue((replacePosition1.FacingsWide == 1));

            replaceProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replaceProduct1, "Replace product should exist as part of the planogram product list.");

            replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct1.Id)).FirstOrDefault();
            Assert.IsNull(replacePosition1, "Replace position should not exist as part of the planograms positions.");

            replacementProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin2).FirstOrDefault();
            Assert.IsNull(replacementProduct2, "Replacement product should not exist as part of the planogram product list.");
            
            replaceProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replaceProduct2, "Replace product should exist as part of the planogram product list.");

            replacePosition2 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct2.Id)).FirstOrDefault();
            Assert.IsNull(replacePosition2, "Replace position should not exist as part of the planograms positions.");
        }

        [Test]
        public void ExecuteTask_ReplaceMultipleProductsThatDontExistInProductListOnePlacedOtherDoesntFit2()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).Union(PlanogramTestHelper.CreateProductDtos(1, Context.EntityId, 5, size: new WidthHeightDepthValue(120, 10, 10))).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();

            String replaceProductGtin1 = productDtos[0].Gtin;
            String replaceProductGtin2 = productDtos[2].Gtin;
            String replacementProductGtin1 = productDtos[5].Gtin;
            String replacementProductGtin2 = productDtos[4].Gtin;

            test.Add(new TaskParameterValue(replaceProductGtin1, replacementProductGtin1));
            test.Add(new TaskParameterValue(replaceProductGtin2, replacementProductGtin2));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replaceProduct1, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replaceProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replaceProduct2, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct1, "Replacement product should not exist as part of the planogram product list.");

            PlanogramProduct replacementProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct2, "Replacement product should not exist as part of the planogram product list.");

            PlanogramPosition replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct1.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition1, "Replace position should exist as part of the planograms positions.");

            PlanogramPosition replacePosition2 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct2.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition2, "Replace position should exist as part of the planograms positions.");

            Int16 replace1SequenceX = replacePosition1.SequenceX;
            Int16 replace2SequenceX = replacePosition2.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct1, "Replacement product should not exist as part of the planogram product list.");
            
            replaceProduct1 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin1).FirstOrDefault();
            Assert.IsNotNull(replaceProduct1, "Replace product should exist as part of the planogram product list.");

            replacePosition1 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct1.Id)).FirstOrDefault();
            Assert.IsNull(replacePosition1, "Replace position should not exist as part of the planograms positions.");

            replacementProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replacementProduct2, "Replacement product should exist as part of the planogram product list.");

            replaceProduct2 = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin2).FirstOrDefault();
            Assert.IsNotNull(replaceProduct2, "Replace product should exist as part of the planogram product list.");

            replacePosition2 = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct2.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition2, "Replacement position should exist as part of the planograms positions.");
            
            Assert.IsTrue((replacePosition2.FacingsWide == 1));
        }

        [Test]
        public void ExecuteTask_ReplaceProductReplaceDoesntExist()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            String replaceProductGtin1 = "Test";

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            test.Add(new TaskParameterValue(replaceProductGtin1, productDtos.Last().Gtin));
            
            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == replaceProductGtin1).FirstOrDefault();
            Assert.IsNull(replaceProduct, "Replace product doesnt exist.");

            _task.ExecuteTask(Context);

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == productDtos.Last().Gtin).FirstOrDefault();
            Assert.IsNull(replacementProduct, "Replacement product should not exist as part of the planogram product list.");
        }

        [Test]
        public void ExecuteTask_ReplaceProductReplacementDoesntExist()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(5, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            String replacementProductGtin1 = "Test";

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            test.Add(new TaskParameterValue(productDtos.First().Gtin, replacementProductGtin1));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == productDtos.First().Gtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct, "Replacement product should not exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");
            
            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementProductGtin1).FirstOrDefault();
            Assert.IsNull(replacementProduct, "Replacement product should not exist as part of the planogram product list.");
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesntExistInProductListAndProductTooBig()
        {
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).Union(PlanogramTestHelper.CreateProductDtos(1, Context.EntityId, 4, size: new WidthHeightDepthValue(120, 10, 10))).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productDtos.Take(4)
            });

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            test.Add(new TaskParameterValue(productDtos.First().Gtin, productDtos.Last().Gtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == productDtos.First().Gtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == productDtos.Last().Gtin).FirstOrDefault();
            Assert.IsNull(replacementProduct, "Replacement product should not exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == productDtos.Last().Gtin).FirstOrDefault();
            Assert.IsNull(replacementProduct, "Replacement product should not exist as part of the planogram product list.");
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesExistInProductList1()
        {
            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products.First().Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == replaceGtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replaceSequenceX, replacePosition.SequenceX);
            Assert.IsTrue((replacePosition.FacingsWide == 1));
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesExistInProductList2()
        {
            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products[2].Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == replaceGtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replaceSequenceX, replacePosition.SequenceX);
            Assert.IsTrue((replacePosition.FacingsWide == 1));
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesExistInProductListFillAvailableSpace()
        {
            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products.First().Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.FillAvailableSpace });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == replaceGtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replaceSequenceX, replacePosition.SequenceX);
            Assert.IsTrue((replacePosition.FacingsWide > 1));
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesExistInProductListButTooBig()
        {
            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(allowSqueeze:false); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 10, 10),false);

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products.First().Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == replaceGtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            // The position should have been too big to place on the shelf
            replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct.Id)).FirstOrDefault();
            Assert.IsNull(replacePosition, "Replacement position should not exist as part of the planograms positions.");
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesExistInProductListButTooBigButAlwaysAdd()
        {
            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(allowSqueeze: false); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 10, 10), false);

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products.First().Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.AlwaysAdd });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == replaceGtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;

            _task.ExecuteTask(Context);

            replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            // The position should have been too big to place on the shelf but we forced the add.
            replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replacementProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replacement position should exist as part of the planograms positions.");

            Assert.AreEqual(replaceSequenceX, replacePosition.SequenceX);
        }

        [Test]
        public void ExecuteTask_ReplaceProductThatDoesExistInProductListButTooBigPlaceOnCarParkShelf()
        {
            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(allowSqueeze: false); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 10, 10), false);

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products.First().Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.AddToCarparkShelf });

            PlanogramProduct replaceProduct = TestPlanogram.Products.Where(p => p.Gtin == replaceGtin).FirstOrDefault();
            Assert.IsNotNull(replaceProduct, "Replace product should exist as part of the planogram product list.");

            PlanogramProduct replacementProduct = TestPlanogram.Products.Where(p => p.Gtin == replacementGtin).FirstOrDefault();
            Assert.IsNotNull(replacementProduct, "Replacement product should exist as part of the planogram product list.");

            PlanogramPosition replacePosition = TestPlanogram.Positions.Where(p => p.PlanogramProductId.Equals(replaceProduct.Id)).FirstOrDefault();
            Assert.IsNotNull(replacePosition, "Replace position should exist as part of the planograms positions.");

            Int16 replaceSequenceX = replacePosition.SequenceX;
            List<String> expected = new List<String>() { replacementGtin };

            _task.ExecuteTask(Context);

            PlanogramSubComponentPlacement carParkShelf = Context.Planogram.GetPlanogramSubComponentPlacements().Where(p => p.Component.Name == "Car Park").FirstOrDefault();
            List<String> actual = carParkShelf.GetPlanogramPositions().Select(p => p.GetPlanogramProduct().Gtin).ToList();
            
            Assert.AreEqual(expected, actual, "Replacement should have been added to carpark shelf.");
        }

        [Test]
        public void ExecuteTask_WhenReplaceProductIsntPositioned_Should(
            [Values(ProductNotFoundActionType.AddReplacementToCarParkComponent, ProductNotFoundActionType.AddReplacementToProductList)]
            ProductNotFoundActionType productNotFoundAction)
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.GetPlanogramFixture().Height = 1000;
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(1000,4,500));
            var productDtos = TestDataHelper.InsertProductDtos(DalFactory, 4, Context.EntityId);
            ccmModel.ProductList products = ccmModel.ProductList.FetchByEntityId(Context.EntityId);
            TestPlanogram.Products.Add(PlanogramProduct.NewPlanogramProduct(products.First()));
            shelf.AddPosition(bay, TestPlanogram.Products.ElementAt(0));

            base.SetTaskParameterValues(
                0,
                new TaskParameterValue[] 
                { 
                    new TaskParameterValue(productDtos.ElementAt(0).Gtin, productDtos.ElementAt(2).Gtin),
                    new TaskParameterValue(productDtos.ElementAt(1).Gtin, productDtos.ElementAt(3).Gtin),
                },
                isTaskParameterType: true);
            base.SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });
            base.SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.DoNotAdd });
            base.SetTaskParameterValues(3, new Object[] { "Car Park" });
            base.SetTaskParameterValues(4, new Object[] { productNotFoundAction });

            _task.ExecuteTask(Context);

            var positionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var expected =
                productNotFoundAction == ProductNotFoundActionType.AddReplacementToCarParkComponent ?
                new List<String> { productDtos.ElementAt(2).Gtin, productDtos.ElementAt(3).Gtin } :
                new List<String> { productDtos.ElementAt(2).Gtin };
            CollectionAssert.AreEquivalent(expected, positionGtins);

            Int32 expectedComponents =
                productNotFoundAction == ProductNotFoundActionType.AddReplacementToCarParkComponent ?
                2 :
                1;
            Assert.AreEqual(expectedComponents, TestPlanogram.Components.Where(c => c.IsMerchandisable).Count());
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.AddToCarparkShelf });
            SetTaskParameterValues(3, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(5, new Object[] { AddCarParkTextBoxType.Yes });


            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(allowSqueeze: false); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 10, 10), false);

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products.First().Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });


            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameterValues(2, new Object[] { DroppedProductBehaviourType.AddToCarparkShelf });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.No });


            var fixItem = TestPlanogram.AddFixtureItem();
            var shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++) { TestPlanogram.AddProduct(allowSqueeze: false); }
            foreach (var product in TestPlanogram.Products.Take(4)) { shelf1.AddPosition(fixItem, product); }

            TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 10, 10), false);

            using (var m = TestPlanogram.GetMerchandisingGroups()) { m.ForEach(g => { g.Process(); g.ApplyEdit(); }); }

            List<ITaskParameterValue> test = new List<ITaskParameterValue>();
            String replaceGtin = TestPlanogram.Products.First().Gtin;
            String replacementGtin = TestPlanogram.Products.Last().Gtin;
            test.Add(new TaskParameterValue(replaceGtin, replacementGtin));

            SetTaskParameterValues(0, test, true);
            SetTaskParameterValues(1, new Object[] { PlacementType.SingleFacing });

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }
    }
}
