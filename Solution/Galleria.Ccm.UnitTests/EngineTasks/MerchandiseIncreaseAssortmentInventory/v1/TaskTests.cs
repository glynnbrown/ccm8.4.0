﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : A.Kuszyk
//  Created.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// CCM-18440 : A.Kuszyk
//  Added test to ensure position attributes are preserved.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using FluentAssertions;

namespace Galleria.Ccm.UnitTests.EngineTasks.MerchandiseIncreaseAssortmentInventory.v1
{
    [TestFixture]
    [Category(Categories.MerchandisingTasks)]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.Task>
    {
        private Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.Task _task;
        private IEnumerable<Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType> _processingMethods =
            new List<Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType>()
            {
                Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass,
                Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.MaximumIncreasePerPass
            };

        [SetUp]
        public void SetUp()
        {
            _task = new Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.Task();
        }

        [Test]
        public void ExecuteTask_ShouldProcessBothProcessingMethods()
        {
            SetTaskParameterValues(0, new Object[] { 0.5f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.MaximumIncreasePerPass }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for(int i = 0; i < 4; i++)
            {
                shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.ReprocessAllMerchandisingGroups();
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            TestPlanogram.Positions.Should().OnlyContain(p => p.TotalUnits == 2, "because all products should have had their units increased");
        }

        [Test]
        public void ExecuteTask_PreservesPositionAttributes()
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var prod1 = TestPlanogram.AddProduct();
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            var pos1 = shelf1.AddPosition(bay, prod1);
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Case;
            var prod2 = TestPlanogram.AddProduct();
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            var pos2 = shelf2.AddPosition(bay, prod2);
            pos2.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Case;
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.ReprocessAllMerchandisingGroups();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            TestPlanogram.Products.Should().HaveCount(2, "because all products should be placed");
            TestPlanogram.Positions.Should().OnlyContain(
                p => p.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Case,
                "because the merchandising style of the positions should be preserved");
        }

        [Test]
        public void ExecuteTask_WhenLotsOfWhiteSpace_ProductsAreIncreasedCorrectly()
        {
            var productSize = new WidthHeightDepthValue(10, 80, 70);
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass}); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            shelf2.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            TestPlanogram.AddSequenceGroup(leftBlock, TestPlanogram.Products.Take(2));
            TestPlanogram.AddSequenceGroup(rightBlock, TestPlanogram.Products.Skip(2));
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var p in TestPlanogram.Assortment.Products) { p.Units = 6; }
            TestPlanogram.ReprocessAllMerchandisingGroups();
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            shelf1.GetPositionGtins(true).Should().Equal(
                new[] { TestPlanogram.Products[0].Gtin, TestPlanogram.Products[2].Gtin },
                "because the products originally on the bottom shelf should stay on the bottom shelf");
            shelf2.GetPositionGtins(true).Should().Equal(
                new[] { TestPlanogram.Products[1].Gtin, TestPlanogram.Products[3].Gtin },
                "because the products originally on the top shelf should stay on the top shelf");
            TestPlanogram.Positions.Should().OnlyContain(
                p => p.TotalUnits == 6,
                "because all products should be faced up to 6 units");
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsToAssortmentUnits_WithOneBlock(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay,TestPlanogram.AddProduct(new WidthHeightDepthValue(10,10,70)));
            shelf.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 70)));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach(var ap in TestPlanogram.Assortment.Products) ap.Units = 5;
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True, "All products should be placed");
            foreach (var position in TestPlanogram.Positions)
            {
                Assert.That(position.TotalUnits, Is.EqualTo(TestPlanogram.Assortment.Products.First(p => p.Gtin == position.GetPlanogramProduct().Gtin).Units));
            }
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsToAssortmentUnits_WithMultipleBlocks(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 6;
            TestPlanogram.AddBlocking().Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True, "All products should be placed");
            foreach (var position in TestPlanogram.Positions)
            {
                Assert.That(
                    position.TotalUnits,
                    Is.EqualTo(TestPlanogram.Assortment.Products.First(p => p.Gtin == position.GetPlanogramProduct().Gtin).Units),
                    String.Format("{0} did not achieve assortment inventory", position.GetPlanogramProduct().Gtin));
            }
        }

        [Test]
        public void ExecuteTask_IncreasesUnitsCloseToAssortmentUnits_WithMultipleBlocks(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 100;
            TestPlanogram.AddBlocking().Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            //TestPlanogram.Parent.SaveAs(0, @"c:\users\usr145\before.pog");
            _task.ExecuteTask(Context);
            //TestPlanogram.Parent.SaveAs(0, @"c:\users\usr145\after.pog");

            Assert.That(TestPlanogram.Positions.Count, Is.EqualTo(TestPlanogram.Products.Count), "There should be the same number of positions as products.");
            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True,"All products should be placed");
            foreach (var position in TestPlanogram.Positions)
            {
                if (processingMethod == Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass)
                {
                    Assert.That(position.TotalUnits, Is.EqualTo(8)); // 4 facings wide, 1 high, 2 deep.
                }
                else
                {
                    Assert.That(position.TotalUnits, Is.GreaterThanOrEqualTo(1));
                }
            }
        }

        [Test]
        public void ExecuteTask_LeavesProductsInPlace_IfCannotCorrectLayout(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(15, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for(Int32 i = 0; i < 4; i++) shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            for (Int32 i = 0; i < 4; i++) shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();
            //TestPlanogram.TakeSnapshot("ExecuteTask_LeavesProductsInPlace_IfCannotCorrectLayout_Before");

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_LeavesProductsInPlace_IfCannotCorrectLayout_After");

            TestPlanogram.Products.Should().OnlyContain(p => p.GetPlanogramPositions().Any(), "because all products should be placed");
            TestPlanogram.Positions.Should().OnlyContain(p => p.TotalUnits == 2, "because all positions should have 2 units");
            shelf1.GetPositionGtins(orderXAsc: true).Should().Equal(
                TestPlanogram.Products.Take(2).Union(TestPlanogram.Products.Skip(4).Take(2)).Select(p => p.Gtin),
                "because shelf1 should have the first two products of each sequence group on");
            shelf2.GetPositionGtins(orderXAsc: true).Should().Equal(
                TestPlanogram.Products.Skip(2).Take(2).Reverse().Union(TestPlanogram.Products.Skip(6).Take(2).Reverse()).Select(p => p.Gtin),
                "because shelf2 should have the last two products of each sequence group on");
        }

        [Test]
        public void ExecuteTask_PopulatesPositionSequenceNumber(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(15, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < 4; i++) shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            for (Int32 i = 0; i < 4; i++) shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();
            List<Int32> expectedSequenceNumbers = new List<Int32>() { 1, 2, 3, 4 };
            //TestPlanogram.Parent.SaveAs(0, @"c:\users\usr145\merchassortplan_original.pog");

            _task.ExecuteTask(Context);
            //TestPlanogram.Parent.SaveAs(0, @"c:\users\usr145\merchassortplan.pog");

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), Is.True, "All products should be placed");
            foreach (var p in TestPlanogram.Positions) Assert.That(p.TotalUnits, Is.EqualTo(2), String.Format("{0} should have 2 units", p.GetPlanogramProduct().Gtin));
            Assert.That(
                TestPlanogram.Products.Take(4).Select(p => p.GetPlanogramPositions().First().SequenceNumber).ToList(), 
                Is.EqualTo(expectedSequenceNumbers));
            Assert.That(
                TestPlanogram.Products.Skip(4).Select(p => p.GetPlanogramPositions().First().SequenceNumber).ToList(), 
                Is.EqualTo(expectedSequenceNumbers));
        }

        [Test]
        public void ExecuteTask_PreservesPositions_WithGroupedBlocks(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 10, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 1;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).PlanogramBlockingGroupId = leftBlock.Id;
            TestPlanogram.Blocking.First().Groups.RemoveList(TestPlanogram.Blocking.First().Groups.Where(g => !g.GetBlockingLocations().Any()).ToList());
            var middleBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.3f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(2).Union(TestPlanogram.Products.Skip(4)));
            var middleSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(middleBlock);
            middleSequence.AddSequenceProducts(TestPlanogram.Products.Skip(2).Take(2));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, middleSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            Assert.That(TestPlanogram.Positions.All(p => p.TotalUnits.Equals(1)), "All positions should have one unit");
        }

        [Test]
        public void ExecuteTask_PreservesPositions_WithOnlyOneIncrease(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(15, 90, 30);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 1;
            TestPlanogram.Assortment.Products.First(ap => ap.Gtin.Equals(TestPlanogram.Products.First().Gtin)).Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Positions)
            {
                if (p.GetPlanogramProduct() == TestPlanogram.Products.First())
                {
                    Assert.That(p.TotalUnits, Is.EqualTo(2));
                }
                else
                {
                    Assert.That(p.TotalUnits, Is.EqualTo(1));
                }
            }
        }

        [Test]
        public void ExecuteTask_IncreasesInventoryIntoWhiteSpaceFromOneBlock_WhenBeyondBlockSpace(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BeyondBlockSpace}); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 70);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            foreach (var ap in TestPlanogram.Assortment.Products.Take(4)) ap.Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(2).Union(TestPlanogram.Products.Skip(2).Take(2).Reverse()));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4).Take(2).Union(TestPlanogram.Products.Skip(6).Reverse()));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();

            //TestPlanogram.Parent.SaveAs(0, @"c:\users\usr145\before.pog");
            _task.ExecuteTask(Context);
            //TestPlanogram.Parent.SaveAs(0, @"c:\users\usr145\after.pog");

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Products.Take(4)) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.EqualTo(2), p.Gtin);
            foreach (var p in TestPlanogram.Products.Skip(4)) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.EqualTo(1), p.Gtin);
        }

        [Test]
        public void ExecuteTask_IncreasesInventoryIntoWhiteSpaceFromAllBlocks_WhenBeyondBlockSpace(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BeyondBlockSpace }); // Space constraint.

            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 90, 70);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 2;
            TestPlanogram.AddBlocking().Dividers.Add(0.33f, 0f, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Blocking.First().Dividers.Add(0.66f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.GreaterThan(0.65f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Products) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.EqualTo(2), p.Gtin);
        }

        [Test]
        public void ExecuteTask_ShouldNotDropProducts(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace}); // Space constraint.

            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(25,19,5)));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(23.2f,29.5f,7)));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(25,19,10)));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19,25,10)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19.4f,29.2f,7.3f)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(22,30,7.5f)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19,25,5)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19.1f,30.1f,7)));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 10;
            TestPlanogram.AddBlocking().Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            var leftSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(leftBlock);
            leftSequence.AddSequenceProducts(TestPlanogram.Products.Take(4));
            var rightSequence = PlanogramSequenceGroup.NewPlanogramSequenceGroup(rightBlock);
            rightSequence.AddSequenceProducts(TestPlanogram.Products.Skip(4));
            TestPlanogram.Sequence.Groups.AddRange(new[] { leftSequence, rightSequence });
            using (var mgs = TestPlanogram.GetMerchandisingGroups()) { foreach (var mg in mgs) { mg.Process(); mg.ApplyEdit(); } }
            TestPlanogram.UpdatePositionSequenceData();
            //TestPlanogram.TakeSnapshot("ExecuteTask_ShouldNotDropProducts_Before");

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_ShouldNotDropProducts_After");

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Products) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.GreaterThanOrEqualTo(10), p.Gtin);
        }

        [Test]
        public void ExecuteTask_ShouldPreservePresentation(
            [ValueSource("_processingMethods")] 
            Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType processingMethod)
        {
            SetTaskParameterValues(0, new Object[] { 1f }); // Top percentage of products.
            SetTaskParameterValues(1, new Object[] { processingMethod }); // Processing method one.
            SetTaskParameterValues(2, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.ProcessingMethodType.SingleIncrementPerPass }); // Processing method two.
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.SpaceConstraintType.BlockSpace }); // Space constraint.

            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,14,0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 106, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(25, 19, 5)));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19.4f, 29.2f, 7.3f)));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(22, 30, 7.5f)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19, 25, 10)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(25, 19, 10)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(23.2f, 29.5f, 7)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19.1f, 30.1f, 7)));
            shelf2.AddPosition(bay, TestPlanogram.AddProduct(new WidthHeightDepthValue(19, 25, 5)));
            base.MerchandisePlanogram();
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            foreach (var ap in TestPlanogram.Assortment.Products) ap.Units = 20;
            TestPlanogram.AddBlocking().Dividers.Add(0.493f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            leftBlock.Name = "Left";
            var rightBlock = TestPlanogram.Blocking.First().Locations.First(l => l.X.EqualTo(0.493f)).GetPlanogramBlockingGroup();
            rightBlock.Name = "Right";
            TestPlanogram.AddSequenceGroup(leftBlock,TestPlanogram.Products.Take(1).Union(TestPlanogram.Products.Skip(3).Take(3).Reverse()));
            TestPlanogram.AddSequenceGroup(rightBlock, TestPlanogram.Products.Skip(1).Take(2).Union(TestPlanogram.Products.Skip(6).Take(2).Reverse()));
            TestPlanogram.UpdatePositionSequenceData();

            //TestPlanogram.TakeSnapshot("ExecuteTask_ShouldPreservePresentation_before");
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_ShouldPreservePresentation_after");

            Assert.That(TestPlanogram.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
            foreach (var p in TestPlanogram.Products) Assert.That(p.GetPlanogramPositions().First().TotalUnits, Is.GreaterThanOrEqualTo(20), p.Gtin);
            Assert.That(shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(3));
            Assert.That(shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(5));
        }
    }
}
