﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-31284 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM830
// V8-31807 : D.Pleasance
//  Task changes to become block aware.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.InsertProduct.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Task = Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingList.v1.Task;
using Galleria.Framework.Helpers;
using System.Diagnostics;

namespace Galleria.Ccm.UnitTests.EngineTasks.MerchandiseProductsUsingList.v1
{
    [TestFixture]
    [Category(Categories.MerchandisingTasks)]
    public class TaskTests : TaskTestBase<Task>
    {
        #region Fields

        private TaskBase _task;
        private List<ProductDto> _startingProducts;
        private List<ProductDto> _largeProductDtos;
        private List<ProductDto> _productsToInsert;

        #endregion

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _task = new Task();
            _startingProducts = GetProductDtos(24, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _startingProducts);
            _largeProductDtos = GetProductDtos(24, Context.EntityId, 24, new WidthHeightDepthValue(35, 10, 10)).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _largeProductDtos);
            _productsToInsert = new[] { "AA", "BB", "CC", "DD", "EE" }.Select(NewInsertedProductDto).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, _productsToInsert);
        }

        [Test]
        public void TaskExecution_WhenProductNew_ShouldAddToPlanogram()
        {
            LoadBasicPlanogram(_startingProducts.Take(4));
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            SetTaskParameterValues(0, _startingProducts.Skip(4).Select(o => o.Gtin));
            ICollection<String> expected = _startingProducts.Select(o => o.Gtin).ToList();

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Products.Select(o => o.Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductPlaced_ShouldNotPlace()
        {
            LoadBasicPlanogram(_startingProducts.Take(4));
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            SetTaskParameterValues(0, _startingProducts.Take(4).Select(o => o.Gtin));
            ICollection<String> expected = _startingProducts.Take(4).Select(o => o.Gtin).ToList();

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Positions.Select(o => o.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNotPlaced_ShouldPlace()
        {
            LoadBasicPlanogram(_startingProducts.Take(4));
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _startingProducts);
            SetTaskParameterValues(0, _startingProducts.Skip(4).Take(1).Select(o => o.Gtin));
            ICollection<String> expected = _startingProducts.Take(5).Select(o => o.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = TestPlanogram.Positions.Select(o => o.GetPlanogramProduct().Gtin).ToList();

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductNotPlaced_ShouldPlaceOnValidMerchandisingGroup()
        {
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            TestPlanogram.AddProduct(productSize);
            for (Int32 i = 0; i < 2; i++) shelf1.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            for (Int32 i = 0; i < 4; i++) shelf2.AddPosition(bay, TestPlanogram.AddProduct(productSize));
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            SetTaskParameterValues(0, TestPlanogram.Products.Skip(1).Take(1).Select(p => p.Gtin));
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingList.v1.SpaceConstraintType.BeyondBlockSpace });

            //TestPlanogram.TakeSnapshot("TaskExecution_WhenProductNotPlaced_ShouldPlaceOnValidMerchandisingGroup_Before2");
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("TaskExecution_WhenProductNotPlaced_ShouldPlaceOnValidMerchandisingGroup_After2");

            Assert.That(TestPlanogram.Positions, Has.Count.EqualTo(TestPlanogram.Products.Count));
            Assert.That(TestPlanogram.Products.Take(4), Has.All.Matches<PlanogramProduct>(p => On(shelf1, p)));
            Assert.That(TestPlanogram.Products.Skip(4), Has.All.Matches<PlanogramProduct>(p => On(shelf2, p)));
        }

        private Boolean On(PlanogramFixtureComponent fc, PlanogramProduct prod)
        {
            if (!prod.GetPlanogramPositions().Any()) return false;
            return prod.GetPlanogramPositions().All(p => p.GetPlanogramSubComponentPlacement().FixtureComponent == fc);
        }

        [Test]
        public void TaskExecution_WhenProductDoesNotFit_ShouldNotPlace()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(3).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);

            var block = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramSequenceGroup greenSequence = TestPlanogram.AddSequenceGroup(block, new List<PlanogramProduct>());

            greenSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0],
                                         _largeProductDtos[1], 
                                         _largeProductDtos[2], 
                                         _largeProductDtos[3], 
                                         _largeProductDtos[4], 
                                         _largeProductDtos[5], 
                                         _largeProductDtos[6], 
                                         _largeProductDtos[7], 
                                         _largeProductDtos[8], 
                                         _largeProductDtos[9] 
                                     });

            TestPlanogram.UpdatePositionSequenceData();

            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(3).Take(1).ToList();
            TestPlanogram.Assortment.AddAssortmentProducts(productsToInsert);
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.DoNotAdd });

            _task.ExecuteTask(Context);
            IEnumerable<String> actual = TestPlanogram.GetSubComponentPlacementsFor(productsToInsert).Select(o => o.Component.Name).ToList();

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void TaskExecution_WhenAddToCarparkShelf_ShouldAddDroppedToCarpark()
        {
            const String carParkComponentName = "TestCarPark";
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.AddToCarparkShelf });
            SetTaskParameterValues(2, new Object[] { carParkComponentName });
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);

            using (PlanogramMerchandisingGroupList merchandisingGroups = this.Context.Planogram.GetMerchandisingGroups())
            {
                PlanogramMerchandisingGroup carparkMerchGroup = merchandisingGroups
                    .FirstOrDefault(merchandisingGroup =>
                                    merchandisingGroup.SubComponentPlacements
                                                      .Any(subComponentPlacement =>
                                                           subComponentPlacement.SubComponent.Name == carParkComponentName));
                Assert.IsNotNull(carparkMerchGroup, "The planogram should contain a Car Park Shelf.");

                List<String> actual = carparkMerchGroup.PositionPlacements.Select(p => p.Product.Gtin).ToList();
                List<Single> xPositions = carparkMerchGroup.PositionPlacements.Select(pp => pp.Position.X).OrderBy(p => p).ToList();
                AssertInsert(actual, xPositions, expected);
            }
        }

        [Test]
        public void TaskExecution_WhenAlwaysAdd_ShouldAddDroppedAnyway()
        {
            IEnumerable<ProductDto> productsOnFirstShelf = _largeProductDtos.Take(4).ToList();
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                productsOnFirstShelf
            });
            AddBlocking(itemCount: 1, includeSequence: true);
            SequenceProducts(TestPlanogram.Sequence.Groups.First(), _largeProductDtos);
            IEnumerable<ProductDto> productsToInsert = _largeProductDtos.Skip(5).Take(2).ToList();
            SetTaskParameterValues(0, productsToInsert.Select(o => o.Gtin));
            SetTaskParameterValues(1, new Object[] { DroppedProductBehaviourType.AlwaysAdd });
            PlanogramSubComponentPlacement lowerShelf = Context.Planogram.GetPlanogramSubComponentPlacements().First();
            List<String> expected = productsToInsert.Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);
            List<String> actual = lowerShelf.GetPlanogramPositions().Select(p => p.GetPlanogramProduct().Gtin).ToList();

            CollectionAssert.IsSubsetOf(expected, actual);
        }

        private void AssertInsert(List<String> actual, List<Single> xPositions, IEnumerable<String> expected)
        {
            foreach (PlanogramEventLog eventLog in TestPlanogram.EventLogs)
            {
                Debug.WriteLine(eventLog.Content);
            }

            Debug.Write(actual.First());
            foreach (String s in actual.Skip(1))
            {
                Debug.Write(String.Format(@", {0}", s));
            }
            Debug.WriteLine(String.Empty);
            Debug.Write(xPositions.First());
            foreach (Single s in xPositions.Skip(1))
            {
                Debug.Write(String.Format(@", {0}", s));
            }
            Debug.WriteLine(String.Empty);
            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AllItemsAreUnique(xPositions);
        }

        [Test]
        public void TaskExecution_InsertProductA()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(1));
            SetTaskParameterValues(0, _productsToInsert.Take(1).Select(o => o.Gtin));
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingList.v1.SpaceConstraintType.BeyondBlockSpace });

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _startingProducts[9].Gtin, _startingProducts[17].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductB()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(2));
            SetTaskParameterValues(0, _productsToInsert.Take(2).Select(o => o.Gtin));

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductC()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(3));
            SetTaskParameterValues(0, _productsToInsert.Take(3).Select(o => o.Gtin));

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            // both green and yellow block
            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin, _productsToInsert[2].Gtin, _startingProducts[3].Gtin, _startingProducts[4].Gtin, _startingProducts[5].Gtin, _startingProducts[6].Gtin, _startingProducts[7].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductD()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(4));
            SetTaskParameterValues(0, _productsToInsert.Take(4).Select(o => o.Gtin));

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            // green and orange
            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour || p.SequenceColour == TestPlanogram.Sequence.Groups[1].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin, _startingProducts[18].Gtin, _startingProducts[19].Gtin, _startingProducts[12].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            // green and orange
            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .Where(p => p.SequenceColour == TestPlanogram.Sequence.Groups[0].Colour || p.SequenceColour == TestPlanogram.Sequence.Groups[1].Colour)
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin, _startingProducts[11].Gtin, _startingProducts[10].Gtin, _productsToInsert[3].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            // both green and yellow block
            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin, _productsToInsert[2].Gtin, _startingProducts[3].Gtin, _startingProducts[4].Gtin, _startingProducts[5].Gtin, _startingProducts[6].Gtin, _startingProducts[7].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertProductE()
        {
            SetTestPlanogram();
            TestPlanogram.Assortment.AddAssortmentProducts(_productsToInsert.Take(5));
            SetTaskParameterValues(0, _productsToInsert.Take(5).Select(o => o.Gtin));
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingList.v1.SpaceConstraintType.BeyondBlockSpace });

            _task.ExecuteTask(Context);

            List<PlanogramSubComponentPlacement> shelves = TestPlanogram.GetPlanogramSubComponentPlacements().ToList();

            // green and orange, blue
            var actualShelf1Gtins = shelves[0].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { _startingProducts[16].Gtin, _productsToInsert[0].Gtin, _startingProducts[17].Gtin, _startingProducts[18].Gtin, _startingProducts[19].Gtin, _startingProducts[12].Gtin, _startingProducts[23].Gtin, _startingProducts[22].Gtin, _startingProducts[21].Gtin, _startingProducts[20].Gtin };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            // green and orange, blue
            var actualShelf2Gtins = shelves[1].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { _startingProducts[8].Gtin, _productsToInsert[1].Gtin, _startingProducts[9].Gtin, _startingProducts[11].Gtin, _startingProducts[10].Gtin, _productsToInsert[3].Gtin, _startingProducts[13].Gtin, _startingProducts[14].Gtin, _productsToInsert[4].Gtin, _startingProducts[15].Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            // green and yellow block
            var actualShelf3Gtins = shelves[2].GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { _startingProducts[0].Gtin, _startingProducts[1].Gtin, _startingProducts[2].Gtin, _productsToInsert[2].Gtin, _startingProducts[3].Gtin, _startingProducts[4].Gtin, _startingProducts[5].Gtin, _startingProducts[6].Gtin, _startingProducts[7].Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);

            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void TaskExecution_InsertInCorrectSequence_Shelf(
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType ySequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 50, 75));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            base.SetTaskParameterValues(0, TestPlanogram.Products.Select(p => p.Gtin));
            PlanogramPosition position = shelf1.AddPosition(bay, TestPlanogram.Products[0]);
            PlanogramPosition position2 = shelf2.AddPosition(bay, TestPlanogram.Products[1]);
            foreach (var s in TestPlanogram.Components.SelectMany(c => c.SubComponents)) { s.MerchandisingStrategyX = merchStrategy; }
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            TestPlanogram.Blocking.First().Groups.First().SetBlockingGroupPlacementTypes(
                xSequenceDirection, ySequenceDirection, PlanogramBlockingGroupPlacementType.FrontToBack);
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = shelf2.GetPlanogramSubComponentPlacements()
                .First()
                .GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            List<String> expected = new List<String>();
            if (xSequenceDirection.IsPositive() && !ySequenceDirection.IsPositive()) // LtR TtB
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }
            if (!xSequenceDirection.IsPositive() && !ySequenceDirection.IsPositive()) // RtL TtB
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (xSequenceDirection.IsPositive() && ySequenceDirection.IsPositive()) // LtR BtT
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (!xSequenceDirection.IsPositive() && ySequenceDirection.IsPositive()) // RtL BtT
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            // How you would visually see the sequence as...
            // LeftToRight, BottomToTop  (4,3,2)
            // LeftToRight, TopToBottom  (2,3,4)
            // RightToLeft, BottomToTop  (2,3,4)
            // RightToLeft, TopToBottom  (4,3,2)

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_InsertInCorrectSequence_Peg(
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirectionSecondary)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            PlanogramSubComponent pegboardSubComponent = peg.GetPlanogramComponent().SubComponents.First();
            pegboardSubComponent.MerchandisingStrategyX = merchStrategy;
            pegboardSubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;

            List<ProductDto> products = PlanogramTestHelper.CreateProductDtos(4, Context.EntityId).ToList();
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(110, 50, 10));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(30, 30, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(30, 30, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(30, 30, 10));

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                        PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(TestPlanogram.Products[1], planogramPositionPlacement, sequenceDirection == PlanogramBlockingGroupPlacementType.BottomToTop ? PlanogramPositionAnchorDirection.Above : PlanogramPositionAnchorDirection.Below);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }

            base.SetTaskParameterValues(0, TestPlanogram.Products.Select(p => p.Gtin));
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });

            TestPlanogram.Blocking.First().Groups.First()
                .SetBlockingGroupPlacementTypes(sequenceDirection,
                                                sequenceDirectionSecondary,
                                                PlanogramBlockingGroupPlacementType.FrontToBack);

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(TestPlanogram.Products);

            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var actual = peg.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceY).ThenBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();

            List<String> expected = new List<String>();
            if (sequenceDirection.IsPositive() && !sequenceDirectionSecondary.IsPositive()) // BtT RtL
            {
                expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct4.Gtin, planogramProduct3.Gtin };
            }
            if (!sequenceDirection.IsPositive() && !sequenceDirectionSecondary.IsPositive()) // TtB RtL
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin, planogramProduct1.Gtin };
            }
            if (sequenceDirection.IsPositive() && sequenceDirectionSecondary.IsPositive()) // BtT LtR
            {
                expected = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }
            if (!sequenceDirection.IsPositive() && sequenceDirectionSecondary.IsPositive()) // TtB LtR
            {
                expected = new List<String>() { planogramProduct3.Gtin, planogramProduct4.Gtin, planogramProduct2.Gtin, planogramProduct1.Gtin };
            }

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_InsertInCorrectSequence_Chest(
            [Values(PlanogramBlockingGroupPlacementType.FrontToBack, PlanogramBlockingGroupPlacementType.BackToFront)] PlanogramBlockingGroupPlacementType zSequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType merchStrategy,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType xSequenceDirection)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);

            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 65));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            using (var mgs = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(TestPlanogram.Products[0]);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }

            foreach (PlanogramSubComponent sub in TestPlanogram.Components.SelectMany(c => c.SubComponents))
            {
                sub.MerchandisingStrategyX = merchStrategy;
            }

            base.SetTaskParameterValues(0, TestPlanogram.Products.Select(p => p.Gtin));
            TestPlanogram.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });

            TestPlanogram.Blocking.First().Groups.First()
                .SetBlockingGroupPlacementTypes(zSequenceDirection,
                                                xSequenceDirection,
                                                PlanogramBlockingGroupPlacementType.BottomToTop);

            TestPlanogram.Sequence.Groups[0]
                .AddSequenceProducts(TestPlanogram.Products);

            MerchandisePlanogram();

            _task.ExecuteTask(Context);

            var positions = chest.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions();
            var actual = chest.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Where(p => p.SequenceX == ((xSequenceDirection == PlanogramBlockingGroupPlacementType.LeftToRight) ? 2 : 1)).OrderBy(p => p.SequenceZ).Select(p => p.GetPlanogramProduct().Gtin).ToList();

            List<String> expected = new List<String>();
            if (zSequenceDirection.IsPositive() && !xSequenceDirection.IsPositive()) // BtF RtL
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (!zSequenceDirection.IsPositive() && !xSequenceDirection.IsPositive()) // FtB RtL
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }
            if (zSequenceDirection.IsPositive() && xSequenceDirection.IsPositive()) // BtF LtR
            {
                expected = new List<String>() { planogramProduct4.Gtin, planogramProduct3.Gtin, planogramProduct2.Gtin };
            }
            if (!zSequenceDirection.IsPositive() && xSequenceDirection.IsPositive()) // FtB LtR
            {
                expected = new List<String>() { planogramProduct2.Gtin, planogramProduct3.Gtin, planogramProduct4.Gtin };
            }

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void TaskExecution_WhenProductIsMultisitedInSequence_ShouldPlaceOnePosition()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 60, 0));

            PlanogramProduct planogramProduct0 = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 50, 75));
            PlanogramProduct planogramProduct1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramPosition position1 = shelf1.AddPosition(bay,
                                                               TestPlanogram.Products[0]);

            PlanogramPosition position2 = shelf2.AddPosition(bay,
                                                               TestPlanogram.Products[1]);
            shelf2.GetPlanogramComponent().SubComponents.First().FillColourFront = 888;
            
            MerchandisePlanogram();

            Planogram plan = TestPlanogram;

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);

            var bottomBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.25F)).GetPlanogramBlockingGroup();

            PlanogramSequenceGroup bottomSequence = TestPlanogram.AddSequenceGroup(bottomBlock, new List<PlanogramProduct>());

            bottomSequence.AddSequenceProducts(new List<PlanogramProduct>
                                     {
                                         planogramProduct0, 
                                         planogramProduct1,
                                         planogramProduct2,
                                         planogramProduct3
                                     });

            bottomBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup topSequence = TestPlanogram.AddSequenceGroup(topBlock, new List<PlanogramProduct>());

            topSequence.AddSequenceProducts(new List<PlanogramProduct>
                                     {
                                         planogramProduct1,
                                         planogramProduct2,
                                         planogramProduct3
                                     });

            topBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            plan.UpdatePositionSequenceData();

            SetTaskParameterValues(0, TestPlanogram.Products.Skip(2).Take(2).Select(o => o.Gtin));
            _task.ExecuteTask(Context);

            // Should have inserted multisited items Gtin 3 \ 4
            var actual = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            var actual2 = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<String> expected = new List<String>() { planogramProduct0.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin };
            List<String> expected2 = new List<String>() { planogramProduct1.Gtin };

            WriteList("Expected: ", expected);
            WriteList("Actual: ", actual);

            WriteList("Expected 2: ", expected2);
            WriteList("Actual 2: ", actual2);

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(expected2, actual2);
        }

        private void WriteList(String caption, IEnumerable<String> items)
        {
            Debug.WriteLine(caption);
            foreach (String item in items)
            {
                Debug.WriteLine(item);
            }
        }

        private void SetOriginalTestPlanogram()
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                _startingProducts.Skip(16).Take(8),
                _startingProducts.Skip(8).Take(8),
                _startingProducts.Take(8)
            });
            AddBlocking(itemCount: 4, includeSequence: true);
            PlanogramBlockingGroupList blockingGroups = TestPlanogram.Blocking.First().Groups;
            SetBlockingGroupPlacementTypes(blockingGroups[0],
                PlanogramBlockingGroupPlacementType.LeftToRight,
                PlanogramBlockingGroupPlacementType.BottomToTop,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[1],
                PlanogramBlockingGroupPlacementType.RightToLeft,
                PlanogramBlockingGroupPlacementType.BottomToTop,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[2],
                PlanogramBlockingGroupPlacementType.BottomToTop,
                PlanogramBlockingGroupPlacementType.LeftToRight,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SetBlockingGroupPlacementTypes(blockingGroups[3],
                PlanogramBlockingGroupPlacementType.TopToBottom,
                PlanogramBlockingGroupPlacementType.RightToLeft,
                PlanogramBlockingGroupPlacementType.FrontToBack);
            SequenceProducts(TestPlanogram.Sequence.Groups[0], new List<ProductDto>
            {
                _startingProducts[16],  // green 17
                _productsToInsert[0],   // green A
                _startingProducts[17],  // green 18
                _startingProducts[9],   // green 10
                _productsToInsert[1],   // green B
                _startingProducts[8],   // green 9
                _startingProducts[0],   // green 1
                _startingProducts[1],   // green 2
                _startingProducts[2]    // green 3
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[1], new List<ProductDto>
            {
                _startingProducts[7],   // yellow 8
                _startingProducts[6],   // yellow 7
                _startingProducts[5],   // yellow 6
                _startingProducts[4],   // yellow 5
                _startingProducts[3],   // yellow 4
                _productsToInsert[2]    // yellow 3
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[2], new List<ProductDto>
            {
                _startingProducts[18],  // orange 19
                _startingProducts[10],  // orange 11
                _productsToInsert[3],   // orange D
                _startingProducts[11],  // orange 12
                _startingProducts[19],  // orange 20
                _startingProducts[12]   // orange 13
            });
            SequenceProducts(TestPlanogram.Sequence.Groups[3], new List<ProductDto>
            {
                _startingProducts[15],  // blue 16
                _startingProducts[23],  // blue 24
                _productsToInsert[4],   // blue E
                _startingProducts[22],  // blue 23
                _startingProducts[14],  // blue 15
                _startingProducts[11],  // blue 12
                _startingProducts[20]   // blue 21
            });
        }

        private void SetTestPlanogram()
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>>
            {
                _startingProducts.Skip(16).Take(8),
                _startingProducts.Skip(8).Take(8),
                _startingProducts.Take(8)
            });
            SetProductSequences();
        }

        private void SetProductSequences()
        {
            Planogram plan = TestPlanogram;

            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0.25f, 0, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.65f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);

            var greenBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup(); // Green
            var orangeBlock = blocking.Locations.First(l => l.X.EqualTo(0.25f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup(); // Orange
            var blueBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup(); // Blue
            var yellowBlock = blocking.Locations.First(l => l.X.EqualTo(0.25f) && l.Y.EqualTo(0.65f)).GetPlanogramBlockingGroup(); // Yellow

            // get products for green block
            PlanogramSequenceGroup greenSequence = TestPlanogram.AddSequenceGroup(greenBlock, new List<PlanogramProduct>());

            greenSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _largeProductDtos[0], // non fitting product
                                         _startingProducts[16], // green 17
                                         _productsToInsert[0], // green A
                                         _startingProducts[17], // green 18
                                         _startingProducts[9], // green 10
                                         _productsToInsert[1], // green B
                                         _startingProducts[8], // green 9
                                         _startingProducts[0], // green 1
                                         _startingProducts[1], // green 2
                                         _startingProducts[2] // green 3
                                     });

            greenBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup orangeSequence = TestPlanogram.AddSequenceGroup(orangeBlock, new List<PlanogramProduct>());

            orangeSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[18], // orange 19
                                         _startingProducts[19], // orange 20
                                         _startingProducts[12], // orange 13
                                         _startingProducts[11], // orange 12
                                         _startingProducts[10], // orange 11
                                         _productsToInsert[3] // orange D
                                     });

            orangeBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.LeftToRight,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup blueSequence = TestPlanogram.AddSequenceGroup(blueBlock, new List<PlanogramProduct>());

            blueSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[15], // blue 16
                                         _productsToInsert[4], // blue E
                                         _startingProducts[14], // blue 15
                                         _startingProducts[13], // blue 14
                                         _startingProducts[20], // blue 21
                                         _startingProducts[21], // blue 22
                                         _startingProducts[22], // blue 23
                                         _startingProducts[23] // blue 24
                                     });

            blueBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.TopToBottom,
                                                        PlanogramBlockingGroupPlacementType.RightToLeft,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            PlanogramSequenceGroup yellowSequence = TestPlanogram.AddSequenceGroup(yellowBlock, new List<PlanogramProduct>());

            yellowSequence.AddSequenceProducts(new List<ProductDto>
                                     {
                                         _startingProducts[7], // yellow 8
                                         _startingProducts[6], // yellow 7
                                         _startingProducts[5], // yellow 6
                                         _startingProducts[4], // yellow 5
                                         _startingProducts[3], // yellow 4
                                         _productsToInsert[2] // yellow C
                                     });

            yellowBlock.SetBlockingGroupPlacementTypes(PlanogramBlockingGroupPlacementType.RightToLeft,
                                                        PlanogramBlockingGroupPlacementType.BottomToTop,
                                                        PlanogramBlockingGroupPlacementType.FrontToBack);

            plan.UpdatePositionSequenceData();
        }

        private static void SetBlockingGroupPlacementTypes(PlanogramBlockingGroup blockingGroup, PlanogramBlockingGroupPlacementType primary, PlanogramBlockingGroupPlacementType secondary, PlanogramBlockingGroupPlacementType tertiary)
        {
            blockingGroup.BlockPlacementPrimaryType = primary;
            blockingGroup.BlockPlacementSecondaryType = secondary;
            blockingGroup.BlockPlacementTertiaryType = tertiary;
        }

        private ProductDto NewInsertedProductDto(String gtin)
        {
            ProductList existingProducts = ProductList.FetchByEntityId(Context.EntityId);
            Int32 lastId = existingProducts.Max(o => o.Id);
            ProductDto dto = NewDefaultProductDto(++lastId, Context.EntityId, gtin, gtin);
            dto.Width = 10;
            dto.Height = 10;
            dto.Depth = 10;
            return dto;
        }

        #region Test Helper Methods

        private static IEnumerable<ProductDto> GetProductDtos(Int16 itemCount, Int32 entityId, Int16 startingId = 0, WidthHeightDepthValue size = default(WidthHeightDepthValue))
        {
            if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(10, 10, 10);

            for (var i = (Int16)(startingId + 1); i < startingId + itemCount + 1; i++)
            {
                ProductDto dto = NewDefaultProductDto(i, entityId, "Product{0}");
                dto.Width = size.Width;
                dto.Height = size.Height;
                dto.Depth = size.Depth;
                yield return dto;
            }
        }

        private static ProductDto NewDefaultProductDto(Int32 id, Int32 entityId, String nameMask = "name{0}", String gtinMask = "gtin{0}")
        {
            return new ProductDto
            {
                Id = id,
                EntityId = entityId,
                ReplacementProductId = id,
                Gtin = String.Format(gtinMask, id),
                Name = String.Format(nameMask, id),
                IsNewProduct = true,
                DateCreated = DateTime.Now,
                DateLastModified = DateTime.Now,
                DateDeleted = null,
            };
        }

        private IEnumerable<PlanogramSubComponentPlacement> GetSubComponentPlacementsFor(IEnumerable<ProductDto> productsToInsert)
        {
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                IEnumerable<String> gtinsToInsert = productsToInsert.Select(d => d.Gtin);
                PlanogramMerchandisingGroup merchandisingGroup = merchandisingGroups.FirstOrDefault(o =>
                    !gtinsToInsert.Except(o.PositionPlacements.Select(p => p.Product.Gtin)).Any());
                return merchandisingGroup != null ? merchandisingGroup.SubComponentPlacements : new List<PlanogramSubComponentPlacement>();
            }
        }

        /// <summary>
        ///     Adds Blocking Groups and optionally Sequence Groups.
        /// </summary>
        /// <param name="itemCount">The number of groups to add.</param>
        /// <param name="includeSequence">Whether to add corresponding Sequence Groups for each Blocking Group.</param>
        private void AddBlocking(Int32 itemCount, Boolean includeSequence)
        {
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            TestPlanogram.Blocking.Add(blocking);
            for (var i = 0; i < itemCount; i++)
            {
                PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
                blockingGroup.Name = String.Format("Group{0}", i);
                blockingGroup.Colour = blocking.GetNextBlockingGroupColour();
                blocking.Groups.Add(blockingGroup);
                if (!includeSequence) continue;

                PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                TestPlanogram.Sequence.Groups.Add(sequenceGroup);
            }
        }

        /// <summary>
        ///     Adds the products created from <paramref name="productDtos"/> to the provided <paramref name="sequenceGroup"/>.
        /// </summary>
        /// <param name="sequenceGroup"></param>
        /// <param name="productDtos"></param>
        private static void SequenceProducts(PlanogramSequenceGroup sequenceGroup, IEnumerable<ProductDto> productDtos)
        {
            PlanogramSequenceGroupProductList products = sequenceGroup.Products;
            products.Clear();
            foreach (PlanogramSequenceGroupProduct sequenceProduct in productDtos.Select(dto => PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(PlanogramTestHelper.NewPlanogramProduct(dto))))
            {
                sequenceProduct.SequenceNumber = products.Count + 1;
                products.Add(sequenceProduct);
            }
        }

        #endregion
    }
}