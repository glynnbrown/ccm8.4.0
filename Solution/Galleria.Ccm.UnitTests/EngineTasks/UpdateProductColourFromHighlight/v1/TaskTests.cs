﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30728 : A.Kuszyk
// Created.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Engine.Tasks.UpdateProductColourFromHighlight.v1;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.EngineTasks.UpdateProductColourFromHighlight.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Task>
    {
        Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Task();
            base.SetTaskParameterValues(0, new Object[] { String.Empty });
            base.SetTaskParameterValues(1, new Object[] { ProductFilterType.All });
            base.SetTaskParameterValues(2, new Object[] { } );
        }

        [Test]
        public void ExecuteTask_SetsPlanogramHighlightSequenceStrategy()
        {
            var highlight = Galleria.Ccm.Model.Highlight.NewHighlight(Context.EntityId);
            highlight.Name = "test highlight";
            highlight.Save();
            base.SetTaskParameterValues(0, new Object[] { highlight.Name });
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product = TestPlanogram.AddProduct();
            product.Brand = "GALLERIA";
            shelf.AddPosition(bay, product);

            _task.ExecuteTask(Context);

            Assert.AreEqual(highlight.Name, TestPlanogram.HighlightSequenceStrategy);
        }

        [Test]
        public void ExecuteTask_SetsProductColour()
        {
            var highlight = CreateBrandHighlight(Context.EntityId);
            highlight.Save();
            base.SetTaskParameterValues(0, new Object[] { highlight.Name });
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product = TestPlanogram.AddProduct();
            product.Brand = "GALLERIA";
            shelf.AddPosition(bay, product);

            _task.ExecuteTask(Context);

            Assert.AreEqual(highlight.Groups.First().FillColour, product.FillColour);
        }

        [Test]
        public void ExecuteTask_SetsProductColour_ForAllProducts()
        {
            var highlight = CreateBrandHighlight(Context.EntityId);
            highlight.Save();
            base.SetTaskParameterValues(0, new Object[] { highlight.Name });
            base.SetTaskParameterValues(1, new Object[] { ProductFilterType.All });
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product1 = TestPlanogram.AddProduct();
            product1.Brand = "GALLERIA";
            var product2 = TestPlanogram.AddProduct();
            product2.Brand = "GALLERIA";
            shelf.AddPosition(bay, product1);
            shelf.AddPosition(bay, product2);

            _task.ExecuteTask(Context);

            Assert.AreEqual(highlight.Groups.First().FillColour, product1.FillColour);
            Assert.AreEqual(highlight.Groups.First().FillColour, product2.FillColour);
            Assert.AreEqual(highlight.Groups.First().Name, product1.ColourGroupValue);
            Assert.AreEqual(highlight.Groups.First().Name, product2.ColourGroupValue);
        }

        [Test]
        public void ExecuteTask_SetsProductColour_ForRangedProducts()
        {
            var highlight = CreateBrandHighlight(Context.EntityId);
            highlight.Save();
            base.SetTaskParameterValues(0, new Object[] { highlight.Name });
            base.SetTaskParameterValues(1, new Object[] { ProductFilterType.RangedInAssortment});
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product1 = TestPlanogram.AddProduct();
            product1.Brand = "GALLERIA";
            var product2 = TestPlanogram.AddProduct();
            product2.Brand = "GALLERIA";
            shelf.AddPosition(bay, product1);
            shelf.AddPosition(bay, product2);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals(product1.Gtin)).IsRanged = true;
            TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals(product2.Gtin)).IsRanged = false;

            _task.ExecuteTask(Context);

            Assert.AreEqual(highlight.Groups.First().FillColour, product1.FillColour);
            Assert.AreNotEqual(highlight.Groups.First().FillColour, product2.FillColour);
            Assert.AreEqual(highlight.Groups.First().Name, product1.ColourGroupValue);
            Assert.AreNotEqual(highlight.Groups.First().Name, product2.ColourGroupValue);
        }

        [Test]
        public void ExecuteTask_SetsProductColour_ForUnRangedProducts()
        {
            var highlight = CreateBrandHighlight(Context.EntityId);
            highlight.Save();
            base.SetTaskParameterValues(0, new Object[] { highlight.Name });
            base.SetTaskParameterValues(1, new Object[] { ProductFilterType.NotRangedInAssortment});
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product1 = TestPlanogram.AddProduct();
            product1.Brand = "GALLERIA";
            var product2 = TestPlanogram.AddProduct();
            product2.Brand = "GALLERIA";
            shelf.AddPosition(bay, product1);
            shelf.AddPosition(bay, product2);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals(product1.Gtin)).IsRanged = true;
            TestPlanogram.Assortment.Products.First(p => p.Gtin.Equals(product2.Gtin)).IsRanged = false;

            _task.ExecuteTask(Context);

            Assert.AreNotEqual(highlight.Groups.First().FillColour, product1.FillColour);
            Assert.AreEqual(highlight.Groups.First().FillColour, product2.FillColour);
            Assert.AreNotEqual(highlight.Groups.First().Name, product1.ColourGroupValue);
            Assert.AreEqual(highlight.Groups.First().Name, product2.ColourGroupValue);
        }

        [Test]
        public void ExecuteTask_SetsProductColour_ForSelectedGtins()
        {
            var highlight = CreateBrandHighlight(Context.EntityId);
            highlight.Save();
            base.SetTaskParameterValues(0, new Object[] { highlight.Name });
            base.SetTaskParameterValues(1, new Object[] { ProductFilterType.Gtin });
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product1 = TestPlanogram.AddProduct();
            product1.Brand = "GALLERIA";
            var product2 = TestPlanogram.AddProduct();
            product2.Brand = "GALLERIA";
            base.SetTaskParameterValues(2, new Object[] { product1.Gtin });
            shelf.AddPosition(bay, product1);
            shelf.AddPosition(bay, product2);

            _task.ExecuteTask(Context);

            Assert.AreEqual(highlight.Groups.First().FillColour, product1.FillColour);
            Assert.AreNotEqual(highlight.Groups.First().FillColour, product2.FillColour);
            Assert.AreEqual(highlight.Groups.First().Name, product1.ColourGroupValue);
            Assert.AreNotEqual(highlight.Groups.First().Name, product2.ColourGroupValue);
        }

        [Test]
        public void ExecuteTask_SetsProductColourGroupValue()
        {
            var highlight = CreateBrandHighlight(Context.EntityId);
            highlight.Save();
            base.SetTaskParameterValues(0, new Object[] { highlight.Name });
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var product = TestPlanogram.AddProduct();
            product.Brand = "GALLERIA";
            shelf.AddPosition(bay, product);

            _task.ExecuteTask(Context);

            Assert.AreEqual(highlight.Groups.First().Name, product.ColourGroupValue);
        }

        private Galleria.Ccm.Model.Highlight CreateBrandHighlight(Int32 entityId)
        {
            Galleria.Ccm.Model.Highlight brand = Galleria.Ccm.Model.Highlight.NewHighlight(entityId);

            brand.Name = "04 Brand";
            brand.Type = Galleria.Ccm.Model.HighlightType.Position;
            brand.MethodType = Galleria.Ccm.Model.HighlightMethodType.Group;
            brand.IsFilterEnabled = false;
            brand.IsPreFilter = true;
            brand.IsAndFilter = true;
            brand.Field1 = "[PlanogramProduct.Brand]";
            brand.Field2 = "";
            brand.QuadrantXType = Galleria.Ccm.Model.HighlightQuadrantType.Mean;
            brand.QuadrantXConstant = 0F;
            brand.QuadrantYType = Galleria.Ccm.Model.HighlightQuadrantType.Mean;
            brand.QuadrantYConstant = 0F;

            #region Highlight Groups

            var brandGroup01 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup01.Order = 1;
            brandGroup01.Name = "GALLERIA";
            brandGroup01.DisplayName = "";
            brandGroup01.FillColour = -65536;
            brandGroup01.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup01);

            var brandGroup02 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup02.Order = 2;
            brandGroup02.Name = "Kelloggs";
            brandGroup02.DisplayName = "";
            brandGroup02.FillColour = -16711936;
            brandGroup02.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup02);

            var brandGroup03 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup03.Order = 3;
            brandGroup03.Name = "Nestle";
            brandGroup03.DisplayName = "";
            brandGroup03.FillColour = -16776961;
            brandGroup03.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup03);

            var brandGroup04 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup04.Order = 4;
            brandGroup04.Name = "Weetabix";
            brandGroup04.DisplayName = "";
            brandGroup04.FillColour = -256;
            brandGroup04.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup04);

            #endregion

            return brand;
        }
    }
}
