#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-29105 : A.Silva
//  Created

#endregion

#region Version History: CCM803

// V8-29420 : A.Silva
//  Amended Setup to add a WorkPackage fake to the mock context.

#endregion

#region Version History: CCM810

// V8-30003 : A.Silva
//  Amended LoadBasicPlanogram to use the common TestPlanogram and not create a new, disconnected, one.

#endregion

#region Version History: CCM811

// V8-30255 : A.Silva
//  Added Assertion Helpers for logging.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.UnitTests.Imports.Mappings;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Moq;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Mock;
using System.IO;
using Galleria.Ccm.Dal.Mock.Implementation;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.UnitTests.EngineTasks
{
    public abstract class TaskTestBase<T>
    {
        #region Fields

        private readonly Mock<ITaskContext> _contextMock = new Mock<ITaskContext>(MockBehavior.Loose);
        private String _taskType;

        #endregion

        #region Properties

        private WorkflowTask TestWorkflowTask { get; set; }
        protected Planogram TestPlanogram { get; private set; }
        protected TaskContext Context { get; private set; }
        
        public DalFactory DalFactory { get; private set; }

        #endregion

        [SetUp]
        public virtual void Setup()
        {
            //  Register the tasks.
            Galleria.Ccm.Engine.TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");

            // create a new, in memory dal factory
            DalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();

            // register this factory within the dal container
            String dalName = Guid.NewGuid().ToString();
            DalContainer.RegisterFactory(dalName, DalFactory);


            // set this factory to be the default of the container
            DalContainer.DalName = dalName;

            DalFactory.CreateDatabase();

            //Set global context entity id to be 1 for the tests;
            Csla.ApplicationContext.GlobalContext["EntityId"] = 1;

            bool authenticated = Galleria.Ccm.Security.DomainPrincipal.Authenticate();

            //  Setup the EntityId in ContextMock.
            Int32 entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id;
            _contextMock.Setup(o => o.EntityId).Returns(entityId);
            
            //  Setup the WorkflowPackage in the ContextMock.
            Workpackage testWorkPackage = Workpackage.NewWorkpackage(entityId);
            testWorkPackage.Name = "MockWorkPackage";
            _contextMock.Setup(o => o.Workpackage).Returns(testWorkPackage);

            //  Setup the WorkflowTask in ContextMock.
            _taskType = typeof(T).ToString();
            EngineTaskInfo taskInfo = EngineTaskInfo.GetEngineTaskInfo(_taskType);
            TestWorkflowTask = WorkflowTask.NewWorkflowTask(taskInfo, 1);
            _contextMock.Setup(o => o.WorkflowTask).Returns(TestWorkflowTask);

            //  Setup the Planogram in ContextMock.
            TestPlanogram = "Package".CreatePackage().AddPlanogram();
            TestPlanogram.Name = "Test Planogram";
            _contextMock.Setup(o => o.Planogram).Returns(TestPlanogram);
            
            //  Get the mock object.
            Context = new TaskContext(_contextMock.Object);
        }

        [Test]
        public void Task_ShouldBeRegistered()
        {
            const String expectation = "TaskContainer should be able to create the task.";

            TaskBase actual = TaskContainer.CreateTask(_taskType);

            Assert.IsNotNull(actual, expectation);
        }

        #region Methods

        protected void LoadBasicPlanogram(IEnumerable<ProductDto> productDtos)
        {
            LoadBasicPlanogram(new List<IEnumerable<ProductDto>> {productDtos});
        }

        protected void LoadBasicPlanogram(ICollection<IEnumerable<ProductDto>> shelves)
        {
            //TestPlanogram = Planogram.NewPlanogram();

            //  Add a single bay.
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();

            Single shelfSeparation = bay.GetPlanogramFixture().Height/shelves.Count;
            Single shelfHeightOffset = 0;

            //  Add the shelves and their products.
            foreach (IEnumerable<ProductDto> productDtos in shelves)
            {
                //  Add the shelf.
                var shelfOrigin = new PointValue(0, shelfHeightOffset, 0);
                PlanogramFixtureComponent shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, shelfOrigin);
                shelfHeightOffset += shelfSeparation;

                // Place the products.
                PlanogramTestHelper.PlaceProductsOnComponent(shelf, productDtos);
            }

            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchandisingGroups)
                {
                    merchandisingGroup.Process();
                    merchandisingGroup.ApplyEdit();
                }
            }

            //_contextMock.Setup(o => o.Planogram).Returns(TestPlanogram);
        }

        /// <summary>
        ///     Process the <see cref="TestPlanogram"/> merchandising groups
        ///     and apply the changes.
        /// </summary>
        protected void MerchandisePlanogram()
        {
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
            {
                merchandisingGroups.ForEach(merchandisingGroup =>
                {
                    merchandisingGroup.Process();
                    merchandisingGroup.ApplyEdit();
                });
            }
        }

        /// <summary>
        ///     Adds the given <paramref name="values"/> as to the task parameter with the matching <paramref name="parameterId"/>.
        /// </summary>
        /// <param name="parameterId">The Id for the parameter that will get set.</param>
        /// <param name="values">The enumeration of values to set the parameter to.</param>
        /// <param name="isTaskParameterType">Determines if the values parameter is of type ITaskParameterValue</param>
        protected void SetTaskParameterValues(Int32 parameterId, IEnumerable<Object> values, Boolean isTaskParameterType = false)
        {
            WorkflowTaskParameter taskParameter = TestWorkflowTask.Parameters.FirstOrDefault(o => o.ParameterDetails.Id == parameterId);
            if (taskParameter == null)
                Assert.Inconclusive("Could not find the task parameter {0} for task {1}", parameterId,
                    (typeof (T).FullName));

            WorkflowTaskParameterValueList taskParameterValues = taskParameter.Values;
            taskParameterValues.Clear();

            if (!isTaskParameterType)
            {
                taskParameterValues.AddRange(values.Select(o =>
                    WorkflowTaskParameterValue.NewWorkflowTaskParameterValue(new TaskParameterValue(o))));
            }
            else
            {
                taskParameterValues.AddRange(values.Select(o =>
                    WorkflowTaskParameterValue.NewWorkflowTaskParameterValue((ITaskParameterValue)o)));
            }
        }

        protected void NullifyTaskParameter(Int32 parameterId)
        {
            WorkflowTaskParameter taskParameter = TestWorkflowTask.Parameters.FirstOrDefault(o => o.ParameterDetails.Id == parameterId);
            if (taskParameter == null)
                Assert.Inconclusive("Could not find the task parameter {0} for task {1}", parameterId,
                    (typeof(T).FullName));

            WorkflowTaskParameterValueList taskParameterValues = taskParameter.Values;
            taskParameterValues.Clear();
            //taskParameterValues.Add(null);
        }

        #endregion

        #region Assertion Helpers

        protected void AssertLogError(EventLogEvent expectedEvent, params Object[] args)
        {
            AssertLogEntry(PlanogramEventLogEntryType.Error, expectedEvent, args);
        }

        protected void AssertLogWarning(EventLogEvent expectedEvent, params Object[] args)
        {
            AssertLogEntry(PlanogramEventLogEntryType.Warning, expectedEvent, args);
        }

        protected void AssertLogInformation(EventLogEvent expectedEvent, params Object[] args)
        {
            AssertLogEntry(PlanogramEventLogEntryType.Information, expectedEvent, args);
        }

        private void AssertLogEntry(PlanogramEventLogEntryType expectedEntryType, EventLogEvent expectedEvent, params Object[] args)
        {
            List<PlanogramEventLog> correctEntryTypeEventLogs = Context.Planogram.EventLogs.Where(log => log.EntryType == expectedEntryType).ToList();
            Assert.IsTrue(correctEntryTypeEventLogs.Count > 0, "There were no event logs for the Entry Type {0}", expectedEntryType);

            List<PlanogramEventLog> correctEventIdEventLogs = correctEntryTypeEventLogs.Where(log => log.EventId == Convert.ToInt32(expectedEvent)).ToList();
            Assert.IsTrue(correctEventIdEventLogs.Count > 0, "There were no event logs for the Event Id {0}", expectedEvent);

            String expectedDescription = args.Length > 0 ? String.Format(EventLogEventHelper.FriendlyDescriptions[expectedEvent], args) : EventLogEventHelper.FriendlyDescriptions[expectedEvent];
            List<PlanogramEventLog> correctDescriptionEventLogs = correctEventIdEventLogs.Where(log => log.Description == expectedDescription).ToList();
            Assert.IsTrue(correctDescriptionEventLogs.Count > 0, "There were no event logs with the description '{0}'", expectedDescription);

            String expectedContent = args.Length > 0 ? String.Format(EventLogEventHelper.FriendlyContents[expectedEvent], args) : EventLogEventHelper.FriendlyContents[expectedEvent];
            List<PlanogramEventLog> correctContentEventLogs = correctDescriptionEventLogs.Where(log => log.Content == expectedContent).ToList();
            Assert.IsTrue(correctContentEventLogs.Count > 0, "There were no event logs with the content '{0}'", expectedContent);
        }

        #endregion
    }
}