﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27687 : A.Kuszyk
//  Created.
#endregion
#region Version History : CCM820
// V8-30907 : A.Kuszyk
//  Added additional tests.
#endregion
#region Version History: CCM830
// V8-32519 : L.Ineson
//  Added AddCarParkTextBox parameter
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.UnitTests.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Moq;
using Galleria.Ccm.Engine;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.UnitTests.EngineTasks.AddProductFromAssortment.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1.Task>
    {
        private Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1.Task _task;
        private IEnumerable<String> _productGtins;

        [SetUp]
        public void SetUp()
        {
            _task = new Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1.Task();
            _productGtins = TestDataHelper.InsertProductDtos(DalFactory, 10, Context.EntityId).Select(dto => dto.Gtin).ToList();
            // Product placement
            SetTaskParameterValues(0, new Object[] { PlacementType.SingleFacing });
            // Add To car park
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.No });
            // Car park component name.
            SetTaskParameterValues(2, new Object[] { "Car Park" });
            // Place unplaced products on car park component
            SetTaskParameterValues(3, new Object[] { PlaceUnPlacedOnCarParkComponentType.No });
            //Add carpark textbox
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.No });

            TestPlanogram.Assortment.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, _productGtins)
                .Cast<IPlanogramProductInfo>()
                .Select(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct));
            foreach (var p in TestPlanogram.Assortment.Products) { p.IsRanged = true; }

        }

        [Test]
        public void ExecuteTask_AddsPositionsForUnplacedProductsAlreadyInPlan_WhenPlaceUnplacedIsTrue()
        {
            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.AddProductManual.v1.AddToCarParkType.Yes });
            SetTaskParameterValues(3, new Object[] { PlaceUnPlacedOnCarParkComponentType.Yes });
            IEnumerable<String> gtinsToAdd = _productGtins.Take(3).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.Contains(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_DoesntAddPositionsForPlacedProductsAlreadyInPlan_WhenPlaceUnplacedIsTrue()
        {
            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.AddProductManual.v1.AddToCarParkType.Yes });
            SetTaskParameterValues(3, new Object[] { PlaceUnPlacedOnCarParkComponentType.Yes });
            IEnumerable<String> gtinsToAdd = _productGtins.Take(3).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));
            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            foreach (var p in TestPlanogram.Products) { shelf.AddPosition(bay, p); }
            var existingPositions = TestPlanogram.Positions.ToList();

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Except(existingPositions).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.DoesNotContain(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_DoesntAddPositionsForUnplacedProductsAlreadyInPlan_WhenPlaceUnplacedIsFalse()
        {
            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.AddProductManual.v1.AddToCarParkType.Yes });
            SetTaskParameterValues(3, new Object[] { PlaceUnPlacedOnCarParkComponentType.No });
            IEnumerable<String> gtinsToAdd = _productGtins.Take(3).ToList();
            TestPlanogram.Products.AddRange(
                ProductList.FetchByEntityIdProductGtins(Context.EntityId, gtinsToAdd)
                .Select(PlanogramProduct.NewPlanogramProduct));

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            foreach (String gtin in gtinsToAdd)
            {
                CollectionAssert.DoesNotContain(positionGtins, gtin);
            }
        }

        [Test]
        public void ExecuteTask_AddsProductsToListOnly_WhenAddToCarParkShelfIsNo()
        {
            _task.ExecuteTask(Context);

            IEnumerable<String> planogramProductGtins = TestPlanogram.Products.Select(p => p.Gtin).ToList();
            CollectionAssert.AreEquivalent(_productGtins, planogramProductGtins);
            CollectionAssert.IsEmpty(TestPlanogram.Positions);
        }

        [Test]
        public void ExecuteTask_AddsProductsToListAndCarParkShelf_WhenAddToCarParkShelfIsYes()
        {
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });

            _task.ExecuteTask(Context);

            IEnumerable<String> planogramProductGtins = TestPlanogram.Products.Select(p => p.Gtin).ToList();
            IEnumerable<String> planogramPositionGtins = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(_productGtins, planogramProductGtins);
            CollectionAssert.AreEquivalent(_productGtins, planogramPositionGtins);
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.Yes });

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameterValues(1, new Object[] { AddToCarParkType.Yes });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(4, new Object[] { AddCarParkTextBoxType.No });

             _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }
    }
}
