﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31559 : A.Kuszyk
//  Created.
// V8-31977 : A.Kuszyk
//  Added test for all fields.
// V8-31977 : A.Kuszyk
//  Ensured that test for enum fields simulates how parameters will be passed through the database.
// V8-32519 : L.Ineson
//  Added AddCarParkTextBox parameter
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Engine;
using Galleria.Framework.ViewModel;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.UnitTests.EngineTasks.RemoveProductsUsingAttribute.v1
{
    [TestFixture]
    public class TaskTests : TaskTestBase<Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.Task>
    {
        private const String _carParkName = "Car Park";
        private Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.Task _task;

        [SetUp]
        public void SetUp()
        {
            _task = new Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.Task();
            base.SetTaskParameterValues(2, new Object[] { _carParkName }); // Car park name
        }

        [Test]
        public void ExecuteTask_RemovesMatching_WhenRemoveActionIs_RemovePositions()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositions);
            CreateTestPlanogram(base.TestPlanogram);

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.Where(p => !p.IsActive).Any(p => p.GetPlanogramPositions().Any()), Is.False);
        }

        [Test]
        public void ExecuteTask_RemovesMatching_WhenRemoveActionIs_RemovePositionsAndProducts()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndProducts);
            CreateTestPlanogram(base.TestPlanogram);

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.Any(p => !p.IsActive), Is.False);
            Assert.That(TestPlanogram.Products.Where(p => !p.IsActive).Any(p => p.GetPlanogramPositions().Any()), Is.False);
        }

        [Test]
        public void ExecuteTask_RemovesMatching_WhenRemoveActionIs_RemovePositionsAndAddToCarPark()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndAddToCarPark);
            CreateTestPlanogram(base.TestPlanogram);

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram
                .Products
                .Where(p => !p.IsActive)
                .All(p => p.GetPlanogramPositions()
                    .All(pos => pos.GetPlanogramSubComponent().Name.Equals(_carParkName))), 
                Is.True);
        }

        [Test]
        public void ExecuteTask_DoesNotRemoveNonMatching_WhenRemoveActionIs_RemovePositions()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositions);
            CreateTestPlanogram(base.TestPlanogram);

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.Where(p => p.IsActive).Any(), Is.True);
            Assert.That(TestPlanogram.Products.Where(p => p.IsActive).All(p => p.GetPlanogramPositions().Any()), Is.True);
        }

        [Test]
        public void ExecuteTask_DoesNotRemoveNonMatching_WhenRemoveActionIs_RemovePositionsAndProducts()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndProducts);
            CreateTestPlanogram(base.TestPlanogram);

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.Any(p => p.IsActive), Is.True);
            Assert.That(TestPlanogram.Products.Where(p => p.IsActive).All(p => p.GetPlanogramPositions().Any()), Is.True);
        }

        [Test]
        public void ExecuteTask_DoesNotRemoveNonMatching_WhenRemoveActionIs_RemovePositionsAndAddToCarPark()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndAddToCarPark);
            CreateTestPlanogram(base.TestPlanogram);

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.Any(p => p.IsActive), Is.True);
            Assert.That(TestPlanogram
                .Products
                .Where(p => p.IsActive)
                .All(p => p.GetPlanogramPositions()
                    .All(pos => !pos.GetPlanogramSubComponent().Name.Equals(_carParkName))),
                Is.True);
        }

        IEnumerable<PropertyInfo> _productProperties = typeof(PlanogramProduct).GetProperties();
        IEnumerable<PropertyInfo> _assortmentProductProperties = typeof(PlanogramAssortmentProduct).GetProperties();
        IEnumerable<PropertyInfo> _performanceDataProperties = typeof(PlanogramPerformanceData).GetProperties();
        IEnumerable<PropertyInfo> _customAttributesProperties = typeof(Galleria.Framework.Planograms.Model.CustomAttributeData).GetProperties();
        IEnumerable<ObjectFieldInfo> _displayableFieldInfos = PlanogramProduct.EnumerateDisplayableFieldInfos(includeMetadata: true, includeCustom: true, includePerformanceData: true).ToList();

        [Test]
        public void ExecuteTask_RemovesProducts_BasedOnAttribute([ValueSource("_displayableFieldInfos")] ObjectFieldInfo field)
        {
            Object fieldValue = GetDefaultFieldValue(field);
            base.SetTaskParameterValues(
                0,
                new Object[] { new TaskParameterValue(
                    field.FieldPlaceholder,
                    Galleria.Ccm.Engine.TaskParameterFilterOperandType.Equals, 
                    fieldValue is Boolean || fieldValue is Enum || fieldValue is DateTime ? fieldValue : Convert.ToString(fieldValue)) },
                true); // Criteria
            base.SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndProducts }); // Remove action type

            var bay = TestPlanogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 4; i++) shelf.AddPosition(bay);
            TestPlanogram.CreateAssortmentFromProducts();
            foreach (var ap in TestPlanogram.Assortment.Products) ap.IsRanged = false;
            foreach (var prod in TestPlanogram.Products.Take(2))
            {
                if(field.PropertyName.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                {
                    var customAttributesProperty = _productProperties.First(p => p.Name.Equals(PlanogramProduct.CustomAttributesProperty.Name));
                    var customAttributes = customAttributesProperty.GetValue(prod, null) as Galleria.Framework.Planograms.Model.CustomAttributeData;
                    var customPropertyName = Regex.Match(field.PropertyName,String.Format(@"{0}\.(\w+)",PlanogramProduct.CustomAttributesProperty.Name)).Groups[1].Value;
                    var property = _customAttributesProperties.First(p => p.Name.Equals(customPropertyName));
                    property.SetValue(customAttributes, fieldValue, null);
                }
                else if (field.PropertyName.Contains(PlanogramProduct.AssortmentFieldPrefix))
                {
                    var property = _assortmentProductProperties.First(p => p.Name.Equals(
                        Regex.Match(field.PropertyName, String.Format(@"{0}(\w+)", PlanogramProduct.AssortmentFieldPrefix)).Groups[1].Value));
                    var assortmentProduct = prod.GetPlanogramAssortmentProduct();
                    if(assortmentProduct != null) property.SetValue(assortmentProduct, fieldValue, null);
                }
                else if(field.PropertyName.Contains(PlanogramPerformance.PerformanceDataProperty.Name))
                {
                    var property = _performanceDataProperties.First(p => p.Name.Equals(
                            Regex.Match(field.PropertyName, String.Format(@"{0}\.(\w+)", PlanogramPerformance.PerformanceDataProperty.Name)).Groups[1].Value));
                    var performanceData = prod.GetPlanogramPerformanceData();
                    if (performanceData != null) property.SetValue(performanceData, fieldValue, null);
                }
                else
                {
                    var property = _productProperties.First(p => p.Name.Equals(field.PropertyName));
                    property.SetValue(prod, fieldValue, null);
                }
            }
            var expectedGtins = TestPlanogram.Products.Skip(2).Select(p => p.Gtin).ToList();

            _task.ExecuteTask(Context);

            Assert.That(TestPlanogram.Products.Select(p => p.Gtin).ToList(), Is.EquivalentTo(expectedGtins));
        }

        private Object GetDefaultFieldValue(ObjectFieldInfo field)
        {
            if (field.PropertyType.IsEnum)
            {
                return Convert.ToByte(Enum.Parse(field.PropertyType, "1"));
            }
            else if(field.PropertyType.Equals(typeof(String)))
            {
                return "HelloWorld";
            }
            else if (field.PropertyType.Equals(typeof(Boolean)) || field.PropertyType.Equals(typeof(Boolean?)))
            {
                return true;
            }
            else if (field.PropertyType.Equals(typeof(DateTime)) || field.PropertyType.Equals(typeof(DateTime?)))
            {
                return DateTime.Now;
            }
            else if (field.PropertyType.Equals(typeof(Single)) || field.PropertyType.Equals(typeof(Single?)))
            {
                return Convert.ToSingle(Math.PI);
            }
            else if (field.PropertyType.Equals(typeof(Double)) || field.PropertyType.Equals(typeof(Double?)))
            {
                return Math.PI;
            }
            else if (field.PropertyType.Equals(typeof(Int32)) || field.PropertyType.Equals(typeof(Int32?)))
            {
                return 10;
            }
            else if (field.PropertyType.Equals(typeof(Int16)) || field.PropertyType.Equals(typeof(Int16?)))
            {
                return (Int16)10;
            }
            else if (field.PropertyType.Equals(typeof(Byte)) || field.PropertyType.Equals(typeof(Byte?)))
            {
                return (Byte)10;
            }
            return null;
        }

        private void SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType removeActionType)
        {
            base.SetTaskParameterValues(
                0,
                new Object[] 
                { 
                    new TaskParameterValue("[PlanogramProduct.IsActive]",Galleria.Ccm.Engine.TaskParameterFilterOperandType.Equals, false) 
                },
                true); // Criteria
            base.SetTaskParameterValues(1, new Object[] { removeActionType }); // Remove action type
        }

        private void CreateTestPlanogram(Planogram planogram)
        {
            var bay = planogram.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 4; i++)
            {
                var product = TestPlanogram.AddProduct();
                product.IsActive = i < 2;
                shelf.AddPosition(bay, product);
            }
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenYes()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndAddToCarPark);
            CreateTestPlanogram(base.TestPlanogram);

            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndAddToCarPark });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.AddCarParkTextBoxType.Yes });

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(1, annotations.Count, "Should have 1 linked annotation.");
        }

        [Test]
        public void ExecuteTask_AddCarParkTextBoxParameter_WhenNo()
        {
            SetTaskParameters(Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndAddToCarPark);
            CreateTestPlanogram(base.TestPlanogram);

            SetTaskParameterValues(1, new Object[] { Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.RemoveActionType.RemovePositionsAndAddToCarPark });
            SetTaskParameterValues(2, new Object[] { "Car Park Shelf" });
            SetTaskParameterValues(3, new Object[] { Galleria.Ccm.Engine.Tasks.RemoveProductsUsingAttribute.v1.AddCarParkTextBoxType.No });

            _task.ExecuteTask(Context);

            //check textbox
            List<PlanogramAnnotation> annotations = TestPlanogram.Annotations.Where(a =>
                a.GetPlanogramFixtureComponent() != null && a.GetPlanogramFixtureComponent().GetPlanogramComponent().Name == "Car Park Shelf")
                .ToList();
            Assert.AreEqual(0, annotations.Count, "Should not have a linked annotation.");
        }
    }
}
