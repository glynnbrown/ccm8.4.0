﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27688 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.RemoveProductsFromAssortment.v1;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.UnitTests.Model;
using Galleria.Framework.Planograms.Model;
using Moq;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.EngineTasks.RemoveProductsFromAssortment.v1
{
    [TestFixture]
    public class TaskTests : TestBase
    {
        private Planogram _planogram;
        private IEnumerable<ProductDto> _productDtos;
        private IEnumerable<String> _assortmentProductGtins;

        [SetUp]
        public void SetUp()
        {
            _planogram = CreatePlanogram();
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;
            _productDtos = TestDataHelper.InsertProductDtos(base.DalFactory, 10, entityId);
            _planogram.Products.AddRange(Galleria.Ccm.Model.ProductList.FetchByEntityId(entityId).Select(p => PlanogramProduct.NewPlanogramProduct(p)));
            foreach (var product in _planogram.Products)
            {
                _planogram.Positions.Add(product, _planogram.Components.First().SubComponents.First().GetPlanogramSubComponentPlacement());
            }
            _planogram.Assortment.Products.AddRange(_productDtos.Take(5).Select(dto =>
            {
                var assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
                assortmentProduct.Gtin = dto.Gtin;
                return assortmentProduct;
            }));

            _assortmentProductGtins = _planogram.Assortment.Products.Select(ap => ap.Gtin);
        }

        //[Test]
        //public void RemoveProductsFromAssortment_WhenRemovePositions_RemovesPositions()
        //{
        //    var contextMock = new Mock<ITaskContext>();
        //    contextMock.Setup(c => c.Planogram).Returns(_planogram);

        //    Task.RemoveProductsFromAssortment(RemoveActionType.RemovePositions, contextMock.Object);

        //    Assert.That(_planogram.Positions.All(p => _assortmentProductGtins.Contains(p.GetPlanogramProduct().Gtin)));
        //}

        //[Test]
        //public void RemoveProductsFromAssortment_WhenRemovePositions_LeavesProducts()
        //{
        //    var contextMock = new Mock<ITaskContext>();
        //    contextMock.Setup(c => c.Planogram).Returns(_planogram);

        //    Task.RemoveProductsFromAssortment(RemoveActionType.RemovePositions, contextMock.Object);

        //    CollectionAssert.AreEquivalent(
        //        _productDtos.Select(dto => dto.Gtin).ToList(),
        //        _planogram.Products.Select(p => p.Gtin).ToList());
        //}

        //[Test]
        //public void RemoveProductsFromAssortment_WhenRemovePositionsAndAddToCarPark_RemovesPositionsAndAddsToCarPark()
        //{
        //    var contextMock = new Mock<ITaskContext>();
        //    contextMock.Setup(c => c.Planogram).Returns(_planogram);

        //    Task.RemoveProductsFromAssortment(RemoveActionType.RemovePositionsAndAddToCarPark, contextMock.Object);

        //    Assert.IsFalse(_planogram.Positions.
        //        Where(p=>!_assortmentProductGtins.Contains(p.GetPlanogramProduct().Gtin)).
        //        All(p => p.GetPlanogramSubComponent().Parent.IsCarPark));
        //}

        //[Test]
        //public void RemoveProductsFromAssortment_WhenRemovePositionsAndAddToCarPark_LeavesProducts()
        //{
        //    var contextMock = new Mock<ITaskContext>();
        //    contextMock.Setup(c => c.Planogram).Returns(_planogram);

        //    Task.RemoveProductsFromAssortment(RemoveActionType.RemovePositionsAndAddToCarPark, contextMock.Object);

        //    CollectionAssert.AreEquivalent(
        //        _productDtos.Select(dto => dto.Gtin).ToList(),
        //        _planogram.Products.Select(p => p.Gtin).ToList());
        //}

        //[Test]
        //public void RemoveProductsFromAssortment_WhenRemovePositionsAndProducts_RemovesPositions()
        //{
        //    var contextMock = new Mock<ITaskContext>();
        //    contextMock.Setup(c => c.Planogram).Returns(_planogram);

        //    Task.RemoveProductsFromAssortment(RemoveActionType.RemovePositionsAndProducts, contextMock.Object);

        //    foreach (var gtin in _productDtos.Where(dto => !_assortmentProductGtins.Contains(dto.Gtin)).Select(dto=>dto.Gtin))
        //    {
        //        Assert.IsFalse(_planogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).Contains(gtin));
        //    }
        //}

        //[Test]
        //public void RemoveProductsFromAssortment_WhenRemovePositionsAndProducts_RemovesProducts()
        //{
        //    var contextMock = new Mock<ITaskContext>();
        //    contextMock.Setup(c => c.Planogram).Returns(_planogram);

        //    Task.RemoveProductsFromAssortment(RemoveActionType.RemovePositionsAndProducts, contextMock.Object);

        //    foreach (var gtin in _productDtos.Where(dto => !_assortmentProductGtins.Contains(dto.Gtin)).Select(dto => dto.Gtin))
        //    {
        //        Assert.IsFalse(_planogram.Products.Select(p => p.Gtin).Contains(gtin));
        //    }
        //}

        private static Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Components.Add(PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, 120, 10, 75));
            planogram.Fixtures.Add(PlanogramFixture.NewPlanogramFixture());
            planogram.Fixtures.First().Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogram.Components.First()));
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(planogram.Fixtures.First()));
            return planogram;
        }
    }
}
