﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802
// V8-29137 : A.Kuszyk
//  Created.
#endregion

#region Version History: CCM803

// V8-29420 : A.Silva
//  Amended tests to use planograms containing an Assortment.

#endregion

#region Version History: CCM810

// V8-29709 : A.Silva
//  Added tests for the drop worst performing functionality.
// V8-29805 : A.Silva
//  Amended tests to account for first product being skipped.
// V8-29585 : A.Silva
//  Refactored IsReversed so that !IsPositive is used instead.
// V8-29769 : A.Kuszyk
//  Added tests for the application of full squeeze.
// V8-29768 : A.Silva
//  Added Place Correct Units tests.
// V8-29772 : A.Silva
//  Added Compact Block starting tests.
// V8-30052 : A.Silva
//  Added Unit Tests for First on Block behavior.

#endregion

#region Version History : CCM811
// V8-30114 : A.Kuszyk
//  Added additional test for complex plan.
#endregion

#region Version History : CCM820
// V8-30760 : D.Pleasance
//  Added tests for task parameter Placement Order type.
// V8-30705 : A.Probyn
//  Added new ExecuteTask_WhenNotAllProductsFit_ShouldDropLowestPriorityAssortmentRuleProducts test
// V8-30997 : D.Pleasance
//  Added ExecuteTask_OneShelf_ValidateOverfilledMerchandisingGroup.
// V8-31243 : D.Pleasance
//  Amended so that each test, tests both ProductComparisonType types.
#endregion

#region Version History : CCM830
// V8-31677 : A.Kuszyk
//  Added additional test for compacting block space.
// V8-32505 : A.Kuszyk
//  Prototyped sequence sub group changes.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32787 : A.Silva
//  Refactored Drop products in order of assortment rules tests.
// V8-33026 : A.Kuszyk
//  Added test to ensure that merchandising group positions are cleared at the same time as planogram positions
// CCM-18440 : A.Kuszyk
//  Added test to ensure position attributes are preserved.
// CCM-18547 : A.Silva
//  Amended ExecuteTask_PlacesInSequenceForComplexPlan test to account for the change in default value for the Ignore Merchandise Dimensions parameter.
// CCM-18786 : M.Pettit
//  Added ExecuteTask_WhenProductDoesNotFitAsFront0ButPositionIsOrientated_ProductShouldStillbeMerchandised test
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using FluentAssertions;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Ccm.UnitTests.EngineTasks.MerchandisePlanogram.v1
{
    [TestFixture]
    [Category(Categories.MerchandisePlanogramTask)]
    public class TaskTests : TaskTestBase<Task>
    {
        private const Boolean IgnoreMerchandisingDimensions = true;
        private const Int32 IgnoreMerchandisingDimensionsParameterId = 1;
        private const Int32 ProductComparisonTypeParameterId = 2;
        private const Int32 PlacedPositionsCount = 12;
        private const Int32 PlacedPositionsMinusLastCount = PlacedPositionsCount - 1;
        private Task _task;

        #region Test Helper Methods

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _task = new Task();
        }

        private void CreateOneBlockPlanWithAssortmentThatFits12OutOf20()
        {
            TestPlanogram.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(20, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> {new Tuple<Single, Single>(0.0F, 0.0F)});
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
        }

        private readonly List<Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>> _worseBeforeBetterTreatmentTypes =
            new List<Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType>>
            {
                new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> (PlanogramAssortmentProductTreatmentType.Optional, PlanogramAssortmentProductTreatmentType.Normal),
                new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> (PlanogramAssortmentProductTreatmentType.Optional, PlanogramAssortmentProductTreatmentType.Inheritance),
                new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> (PlanogramAssortmentProductTreatmentType.Optional, PlanogramAssortmentProductTreatmentType.Distribution),
                new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> (PlanogramAssortmentProductTreatmentType.Normal, PlanogramAssortmentProductTreatmentType.Inheritance),
                new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> (PlanogramAssortmentProductTreatmentType.Normal, PlanogramAssortmentProductTreatmentType.Distribution),
                new Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> (PlanogramAssortmentProductTreatmentType.Inheritance, PlanogramAssortmentProductTreatmentType.Distribution)
            };

        private readonly List<PlanogramAssortmentProductTreatmentType> _productTreatmentTypesWithLowerPriorityThanProductRule =
            new List<PlanogramAssortmentProductTreatmentType>
            {
                PlanogramAssortmentProductTreatmentType.Optional,
                PlanogramAssortmentProductTreatmentType.Normal,
                PlanogramAssortmentProductTreatmentType.Inheritance
            };

        private readonly List<PlanogramAssortmentProductTreatmentType> _productTreatmentTypesWithHigherPriorityThanProductRule =
            new List<PlanogramAssortmentProductTreatmentType>
            {
                PlanogramAssortmentProductTreatmentType.Distribution
            };

        #endregion

        #region Sequence Sub Groups
        [Test]
        public void ExecuteTask_SubGroupedProducts_ShouldStickTogether()
        {
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(25, 10, 10);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            for (Int32 i = 0; i < 6; i++) TestPlanogram.AddProduct(productSize);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            var sequenceGroup = TestPlanogram.Sequence.Groups.First();
            sequenceGroup.AddSequenceProducts(TestPlanogram.Products);
            sequenceGroup.SubGroups.Add(
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(
                sequenceGroup.Products.Skip(2)));

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_SubGroupedProducts_ShouldStickTogether");

            shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin)
                    .Should().Equal(
                        TestPlanogram.Products.Take(2).Select(p => p.Gtin),
                        "shelf1 should have the first two products on");
            shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderByDescending(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin)
                .Should().Equal(
                    TestPlanogram.Products.Skip(2).Take(2).Select(p => p.Gtin),
                    "shelf2 should have the two of the last four products on, because the other two were low ranking and were dropped");
        }

        [Test]
        public void ExecuteTask_ConflictingSubGroupedProducts_ShouldStickTogether()
        {
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(25, 10, 10);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < 16; i++) TestPlanogram.AddProduct(productSize);
            TestPlanogram.CreateAssortmentFromProducts(new[] 
                {
                    TestPlanogram.Products[0],
                    TestPlanogram.Products[4],
                    TestPlanogram.Products[8],
                    TestPlanogram.Products[12],
                    TestPlanogram.Products[1],
                    TestPlanogram.Products[5],
                    TestPlanogram.Products[9],
                    TestPlanogram.Products[13],
                    TestPlanogram.Products[2],
                    TestPlanogram.Products[6],
                    TestPlanogram.Products[10],
                    TestPlanogram.Products[14],
                    TestPlanogram.Products[3],
                    TestPlanogram.Products[7],
                    TestPlanogram.Products[11],
                    TestPlanogram.Products[15],
                });
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            for (int i = 0; i < 4; i++)
            {
                var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup();
                TestPlanogram.Sequence.Groups.First().SubGroups.Add(subGroup);
                foreach (var sp in TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Skip(4 * i).Take(4))
                {
                    sp.PlanogramSequenceGroupSubGroupId = subGroup.Id;
                } 
            }

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_ConflictingSubGroupedProducts_ShouldStickTogether");

            CollectionAssert.AreEqual(
                new[] { TestPlanogram.Products[0], TestPlanogram.Products[1], TestPlanogram.Products[4], TestPlanogram.Products[5] }.Select(p => p.Gtin).ToList(),
                shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList());
            CollectionAssert.AreEqual(
                new[] { TestPlanogram.Products[8], TestPlanogram.Products[9], TestPlanogram.Products[12], TestPlanogram.Products[13] }.Select(p => p.Gtin).ToList(),
                shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderByDescending(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList());
        }

        [Test]
        public void ExecuteTask_EntireSequenceGroupInTwoSubGroups_BothSubGroupsShouldBeRepresented()
        {
            var productSize = new WidthHeightDepthValue(25, 20, 10);
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            for (var i = 0; i < 10; i++) TestPlanogram.AddProduct(productSize);
            TestPlanogram.CreateAssortmentFromProducts(new[]
                {
                    TestPlanogram.Products[0],
                    TestPlanogram.Products[1],
                    TestPlanogram.Products[2],
                    TestPlanogram.Products[3],
                    TestPlanogram.Products[5],
                    TestPlanogram.Products[6],
                    TestPlanogram.Products[7],
                    TestPlanogram.Products[8],
                    TestPlanogram.Products[4],
                    TestPlanogram.Products[9],
                });
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.Sequence.Groups[0].SubGroups.Add(PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(TestPlanogram.Sequence.Groups[0].Products.Take(5)));
            TestPlanogram.Sequence.Groups[0].SubGroups.Add(PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(TestPlanogram.Sequence.Groups[0].Products.Skip(5)));

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_EntireSequenceGroupInTwoSubGroups_BothSubGroupsShouldBeRepresented");

            CollectionAssert.AreEqual(TestPlanogram.Products.Take(4).Select(p => p.Gtin).ToList(), GetGtinsInXOrder(shelf1));
            CollectionAssert.AreEqual(TestPlanogram.Products.Skip(5).Take(4).Reverse().Select(p => p.Gtin).ToList(), GetGtinsInXOrder(shelf2));
        }

        private static List<string> GetGtinsInXOrder(PlanogramFixtureComponent shelf1)
        {
            return shelf1.GetPlanogramComponent().GetSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
        }
        #endregion

        #region Places at Full Squeeze

        [Test]
        public void ExecuteTask_PlacesAtFullSqueeze_OnShelf(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            var bay1 = TestPlanogram.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (Int16 i = 1; i <= 20; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(120, 10, 10));
                product.SqueezeWidth = 0.1f;
                var position = shelf1.AddPosition(bay1, product);
                position.SequenceX = i;
            }
            base.MerchandisePlanogram();
            TestPlanogram.AddBlocking(1, true).Groups.First().AddLocation(0, 1, 0, 1);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            List<String> expected = TestPlanogram.Products.Take(10).Select(p => p.Gtin).ToList();
            List<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEqual(expected, actual);

            expected = TestPlanogram.Products.Skip(10).Select(p => p.Gtin).ToList();
            actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.IsNotSubsetOf(expected, actual);
        }

        [Test]
        public void ExecuteTask_PlacesAtFullSqueeze_OnPegboard(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            var bay1 = TestPlanogram.AddFixtureItem();
            var peg1 = bay1.AddFixtureComponent(PlanogramComponentType.Peg);
            peg1.GetPlanogramComponent().SubComponents.First().MerchConstraintRow1SpacingX = 3;
            for (Int16 i = 1; i <= 20; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(100, 110, 10));
                product.SqueezeWidth = 0.1f;
                var position = peg1.AddPosition(bay1, product);
                position.SequenceX = i;
            }
            base.MerchandisePlanogram();
            TestPlanogram.AddBlocking(1, true).Groups.First().AddLocation(0, 1, 0, 1);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            IEnumerable<String> expected = TestPlanogram.Products.Take(10).Select(p => p.Gtin).ToList();
            IEnumerable<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            WriteList("Expected:", expected);
            WriteList("Actual:", actual);
            CollectionAssert.AreEqual(expected, actual);

            expected = TestPlanogram.Products.Skip(10).Select(p => p.Gtin).ToList();
            actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.IsNotSubsetOf(expected, actual);
        }

        [Test]
        public void ExecuteTask_PlacesAtFullSqueeze_OnChest(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            var bay1 = TestPlanogram.AddFixtureItem();
            var chest1 = bay1.AddFixtureComponent(PlanogramComponentType.Chest);
            for (Int16 i = 1; i <= 20; i++)
            {
                var product = TestPlanogram.AddProduct(new WidthHeightDepthValue(110, 10, 65));
                product.SqueezeWidth = 0.1f;
                var position = chest1.AddPosition(bay1, product);
                position.SequenceX = i;
            }
            base.MerchandisePlanogram();
            TestPlanogram.AddBlocking(1, true).Groups.First().AddLocation(0, 1, 0, 1);
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            IEnumerable<String> expected = TestPlanogram.Products.Take(10).Select(p => p.Gtin).ToList();
            IEnumerable<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEqual(expected, actual);

            expected = TestPlanogram.Products.Skip(10).Select(p => p.Gtin).ToList();
            actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.IsNotSubsetOf(expected, actual);
        }

        #endregion

        #region Simple Plan behavior

        [Test]
        public void ExecuteTask_ForSimplePlan_ProductsAreReplacedInOriginalLocations(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem fixItem = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                           new PointValue(0, 0, 0));

            for (Byte i = 0; i < 5; i++)
            {
                PlanogramPosition position = shelf1.AddPosition(fixItem, TestPlanogram.AddProduct());
                position.SequenceX = Convert.ToInt16(i + 1);
                position.SequenceY = position.SequenceZ = 1;
            }

            using (PlanogramMerchandisingGroupList m = TestPlanogram.GetMerchandisingGroups())
            {
                m.ForEach(g =>
                          {
                              g.Process();
                              g.ApplyEdit();
                          });
            }
            TestPlanogram.AddBlocking().AddGroup().AddLocation(0, 1, 0, 1);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            TestPlanogram.CreateAssortmentFromProducts();
            Dictionary<int, PointValue> coordinatesByPosition =
                TestPlanogram.Positions.ToDictionary(p => Convert.ToInt32(p.PlanogramProductId),
                                                     p => new PointValue(p.X, p.Y, p.Z));

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            AssertPositions(coordinatesByPosition);
        }

        [Test]
        public void ExecuteTask_ForSimplePlan_ProductsAreAddedInCorrectLocations(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem fixItem = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                           new PointValue(0, 0, 0));
            for (Byte i = 0; i < 5; i++)
            {
                TestPlanogram.AddProduct();
            }
            foreach (PlanogramProduct product in TestPlanogram.Products)
            {
                shelf1.AddPosition(fixItem, product);
            }

            using (PlanogramMerchandisingGroupList m = TestPlanogram.GetMerchandisingGroups())
            {
                m.ForEach(g =>
                          {
                              g.Process();
                              g.ApplyEdit();
                          });
            }
            TestPlanogram.AddBlocking().AddGroup().AddLocation(0, 1, 0, 1);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            Dictionary<int, PointValue> coordinatesByPosition =
                TestPlanogram.Positions.ToDictionary(p => Convert.ToInt32(p.PlanogramProductId),
                                                     p => new PointValue(p.X, p.Y, p.Z));
            TestPlanogram.Positions.Clear();
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            AssertPositions(coordinatesByPosition);
        }

        [Test]
        public void ExecuteTask_ForSimplePlan_WithTwoBlocks_ProductsAreReplacedInOriginalLocations(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem fixItem = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                           new PointValue(0, 0, 0));
            PlanogramFixtureComponent shelf2 = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                           new PointValue(0,
                                                                                          fixItem.GetPlanogramFixture()
                                                                                                 .Height/2,
                                                                                          0));
            for (Byte i = 0; i < 10; i++)
            {
                TestPlanogram.AddProduct();
            }
            foreach (PlanogramProduct product in TestPlanogram.Products.Take(5))
            {
                shelf1.AddPosition(fixItem, product);
            }
            foreach (PlanogramProduct product in TestPlanogram.Products.Skip(5).Take(5))
            {
                shelf2.AddPosition(fixItem, product);
            }

            using (PlanogramMerchandisingGroupList m = TestPlanogram.GetMerchandisingGroups())
            {
                m.ForEach(g =>
                          {
                              g.Process();
                              g.ApplyEdit();
                          });
            }
            PlanogramBlocking blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.4f, PlanogramBlockingDividerType.Horizontal);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            Dictionary<int, PointValue> coordinatesByPosition =
                TestPlanogram.Positions.ToDictionary(p => Convert.ToInt32(p.PlanogramProductId),
                                                     p => new PointValue(p.X, p.Y, p.Z));
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            AssertPositions(coordinatesByPosition);
        }

        #endregion

        #region Shelves behavior

        [Test]
        public void ExecuteTask_OneShelf_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType, 
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,
                PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even,
                PlanogramSubComponentXMerchStrategyType.Manual)] PlanogramSubComponentXMerchStrategyType merchStrategy)
        {
            // Products on one shelf, left stacked, placed in sequence order.
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 100, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            for (Int16 i = 1; i <= 20; i++)
            {
                shelf1.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            MerchandisePlanogram();
            TestPlanogram.AddBlocking().AddGroup().BlockPlacementPrimaryType = sequenceDirection;
            TestPlanogram.Blocking.First().Groups.First().AddLocation(0, 1, 0, 1);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> gtinsOrderedBySequenceDirection =
                sequenceDirection.IsPositive()
                    ? TestPlanogram.Positions.OrderBy(p => p.X).Select(p => p.GetPlanogramProduct().Gtin).ToList()
                    : TestPlanogram.Positions.OrderByDescending(p => p.X)
                                   .Select(p => p.GetPlanogramProduct().Gtin)
                                   .ToList();
            Int32 seqProductIndex = 0;
            IOrderedEnumerable<PlanogramSequenceGroupProduct> sequenceProducts =
                TestPlanogram.Sequence.Groups.First().Products.OrderBy(g => g.SequenceNumber);
            Assert.AreEqual(sequenceProducts.Count(), gtinsOrderedBySequenceDirection.Count());
            foreach (PlanogramSequenceGroupProduct seqProduct in sequenceProducts)
            {
                Assert.AreEqual(seqProduct.Gtin, gtinsOrderedBySequenceDirection.ElementAt(seqProductIndex));
                seqProductIndex++;
            }
        }

        [Test]
        public void ExecuteTask_TwoCombinedShelves_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,
                PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even)] PlanogramSubComponentXMerchStrategyType merchStrategy)
        {
            // Products across two combined shelves, left stacked, placed in sequence order.
            const Int16 productsPerShelf = 10;
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureItem bay2 = TestPlanogram.AddFixtureItem(); bay2.X = bay1.GetPlanogramFixture().Width;
            PlanogramFixtureComponent shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            PlanogramFixtureComponent shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf2.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            for (Int16 i = 1; i <= productsPerShelf; i++)
            {
                shelf1.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            for (Int16 i = 1; i <= productsPerShelf; i++)
            {
                shelf2.AddPosition(bay2, TestPlanogram.AddProduct()).SequenceX = i;
            }
            MerchandisePlanogram();
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirection;
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            using (PlanogramMerchandisingGroupList merchGroups = TestPlanogram.GetMerchandisingGroups())
            {
                var expected = TestPlanogram.Sequence.Groups
                    .First().Products.OrderBy(g => g.SequenceNumber).Select(p => p.Gtin).ToList();

                var shelf1Placements = merchGroups.First().SubComponentPlacements.First().GetPlanogramPositions();
                var shelf2Placements = merchGroups.First().SubComponentPlacements.Last().GetPlanogramPositions();
                var actual =
                    (sequenceDirection.IsPositive() ?
                    shelf1Placements.OrderBy(p => p.X).Union(shelf2Placements.OrderBy(p => p.X)) :
                    shelf2Placements.OrderByDescending(p => p.X).Union(shelf1Placements.OrderByDescending(p => p.X)))
                    .Select(p => p.GetPlanogramProduct().Gtin).ToList();
                WriteList("Expected:", expected);
                WriteList("Actual:", actual);
                CollectionAssert.AreEqual(expected, actual);
            }
        }

        [Test]
        public void ExecuteTask_TwoShelves_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,
                PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even,
                PlanogramSubComponentXMerchStrategyType.Manual)] PlanogramSubComponentXMerchStrategyType merchStrategy)
        {
            const Int32 n = 24; // number of products per shelf
            // Products on two shelves, left stacked, placed in sequence order (snaking).
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 50, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            PlanogramFixtureComponent shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 150, 0));
            shelf2.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            for (Int16 i = 1; i <= n; i++)
            {
                shelf1.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            for (Int16 i = 1; i <= n; i++)
            {
                shelf2.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            using (PlanogramMerchandisingGroupList m = TestPlanogram.GetMerchandisingGroups())
            {
                m.ForEach(g =>
                          {
                              g.Process();
                              g.ApplyEdit();
                          });
            }
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirection;
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            IEnumerable<String> gtinsOrderedBySequenceDirection =
                sequenceDirection.IsPositive()
                    ? TestPlanogram.Positions.Take(n)
                                   .OrderBy(p => p.X)
                                   .Union(TestPlanogram.Positions.Skip(n).Take(n).OrderByDescending(p => p.X))
                                   .Select(p => p.GetPlanogramProduct().Gtin)
                    : TestPlanogram.Positions.Take(n)
                                   .OrderByDescending(p => p.X)
                                   .Union(TestPlanogram.Positions.Skip(n).Take(n).OrderBy(p => p.X))
                                   .Select(p => p.GetPlanogramProduct().Gtin);
            IEnumerable<string> gtinsOrderedBySequenceValue =
                TestPlanogram.Sequence.Groups.First().Products.OrderBy(g => g.SequenceNumber).Select(p => p.Gtin);
            Int32 seqProductIndex = 0;
            foreach (string seqProductGtin in gtinsOrderedBySequenceValue)
            {
                Assert.AreEqual(seqProductGtin,
                                gtinsOrderedBySequenceDirection.ElementAt(seqProductIndex),
                                String.Format("Sequence index: {0}", seqProductIndex));
                seqProductIndex++;
            }
        }

        [Test]
        public void ExecuteTask_TwoShelves_TwoBlocks(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,
                PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even,
                PlanogramSubComponentXMerchStrategyType.Manual)] PlanogramSubComponentXMerchStrategyType merchStrategy)
        {
            // Products on two shelves, left stacked, block separated, placed in sequence order.
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent bottomShelf = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                             new PointValue(0, 50, 0));
            bottomShelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            PlanogramFixtureComponent topShelf = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                          new PointValue(0, 150, 0));
            topShelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            for (Int16 i = 1; i <= 20; i++)
            {
                bottomShelf.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            for (Int16 i = 1; i <= 20; i++)
            {
                topShelf.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            using (PlanogramMerchandisingGroupList m = TestPlanogram.GetMerchandisingGroups())
            {
                m.ForEach(g =>
                          {
                              g.Process();
                              g.ApplyEdit();
                          });
            }
            TestPlanogram.AddBlocking().Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            foreach (PlanogramBlockingGroup g in TestPlanogram.Blocking.First().Groups)
            {
                g.BlockPlacementPrimaryType = sequenceDirection;
            }
            int bottomColour =
                TestPlanogram.Blocking.First().Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup().Colour;
            int topColour =
                TestPlanogram.Blocking.First().Locations.First(l => l.Y.EqualTo(0.5)).GetPlanogramBlockingGroup().Colour;
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            IEnumerable<String> gtinsOrderedBySequenceDirection =
                sequenceDirection.IsPositive()
                    ? TestPlanogram.Positions.Take(20)
                                   .OrderBy(p => p.X)
                                   .Union(TestPlanogram.Positions.Skip(20).Take(20).OrderBy(p => p.X))
                                   .Select(p => p.GetPlanogramProduct().Gtin)
                    : TestPlanogram.Positions.Take(20)
                                   .OrderByDescending(p => p.X)
                                   .Union(TestPlanogram.Positions.Skip(20).Take(20).OrderByDescending(p => p.X))
                                   .Select(p => p.GetPlanogramProduct().Gtin);
            IEnumerable<string> gtinsOrderedBySequenceValue =
                TestPlanogram.Sequence.Groups.First(g => g.Colour == bottomColour)
                             .Products.OrderBy(g => g.SequenceNumber)
                             .Select(p => p.Gtin)
                             .Union(
                                 TestPlanogram.Sequence.Groups.First(g => g.Colour == topColour)
                                              .Products.OrderBy(g => g.SequenceNumber)
                                              .Select(p => p.Gtin));
            Int32 seqProductIndex = 0;
            foreach (string seqProductGtin in gtinsOrderedBySequenceValue)
            {
                Assert.AreEqual(seqProductGtin,
                                gtinsOrderedBySequenceDirection.ElementAt(seqProductIndex),
                                String.Format("Sequence index: {0}", seqProductIndex));
                seqProductIndex++;
            }
        }

        [Test]
        public void ExecuteTask_OneShelf_TwoBlocks(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirection,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,
                PlanogramSubComponentXMerchStrategyType.RightStacked)] PlanogramSubComponentXMerchStrategyType
                merchStrategy)
        {
            // Products placed on one shelf, with block divider half way, don't appear outside of block.
            const Int16 products = 12;
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 50, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            for (Int16 i = 1; i <= products; i++)
            {
                shelf1.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            MerchandisePlanogram();
            TestPlanogram.AddBlocking().Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            foreach (PlanogramBlockingGroup g in TestPlanogram.Blocking.First().Groups)
            {
                g.BlockPlacementPrimaryType = sequenceDirection;
            }
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            foreach (PlanogramProduct p in TestPlanogram.Products)
            {
                p.Width = p.Width*2;
            } // move them out of the blocking space.
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            Assert.AreEqual(6, TestPlanogram.Positions.Count);
            if (merchStrategy == PlanogramSubComponentXMerchStrategyType.LeftStacked)
            {
                Assert.That(TestPlanogram.Positions.All(p => p.X < 60f));
            }
            else
            {
                Assert.That(TestPlanogram.Positions.All(p => p.X >= 60f));
            }
        }

        [Test]
        public void ExecuteTask_ThreeShelves_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirectionX,
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType sequenceDirectionY,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,
                PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even,
                PlanogramSubComponentXMerchStrategyType.Manual)] PlanogramSubComponentXMerchStrategyType merchStrategy)
        {
            const Int32 n = 24; // Number of products per shelf
            // Products on three shelves, one block, left/right stacked, sequence direction combinations, products appear in sequence order.
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 10, 0));
            PlanogramFixtureComponent shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 110, 0));
            PlanogramFixtureComponent shelf3 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 160, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategy;
            for (Int16 i = 1; i <= n; i++)
            {
                shelf1.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            for (Int16 i = 1; i <= n; i++)
            {
                shelf2.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            for (Int16 i = 1; i <= n; i++)
            {
                shelf3.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            List<string> shelf1Gtins =
                TestPlanogram.Positions.Take(n).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<string> shelf2Gtins =
                TestPlanogram.Positions.Skip(n).Take(n).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            List<string> shelf3Gtins =
                TestPlanogram.Positions.Skip(n*2).Take(n).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            using (PlanogramMerchandisingGroupList m = TestPlanogram.GetMerchandisingGroups())
            {
                m.ForEach(g =>
                          {
                              g.Process();
                              g.ApplyEdit();
                          });
            }
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirectionX;
            blockingGroup.BlockPlacementSecondaryType = sequenceDirectionY;
            blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            TestPlanogram.CreateAssortmentFromProducts();
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            IEnumerable<PlanogramPosition> shelf1Positions =
                shelf1Gtins.Select(
                    g => TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin.Equals(g)));
            IEnumerable<PlanogramPosition> shelf2Positions =
                shelf2Gtins.Select(
                    g => TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin.Equals(g)));
            IEnumerable<PlanogramPosition> shelf3Positions =
                shelf3Gtins.Select(
                    g => TestPlanogram.Positions.FirstOrDefault(p => p.GetPlanogramProduct().Gtin.Equals(g)));

            IEnumerable<IEnumerable<PlanogramPosition>> shelfPositionsInYOrder =
                sequenceDirectionY.IsPositive()
                    ? new[] {shelf1Positions, shelf2Positions, shelf3Positions}
                    : new[] {shelf3Positions, shelf2Positions, shelf1Positions};

            IEnumerable<PlanogramPosition> shelfPositionsInSequenceOrder =
                sequenceDirectionX.IsPositive()
                    ? shelfPositionsInYOrder.ElementAt(0).OrderBy(p => p.X)
                                            .Union(shelfPositionsInYOrder.ElementAt(1).OrderByDescending(p => p.X))
                                            .Union(shelfPositionsInYOrder.ElementAt(2).OrderBy(p => p.X))
                    : shelfPositionsInYOrder.ElementAt(0).OrderByDescending(p => p.X)
                                            .Union(shelfPositionsInYOrder.ElementAt(1).OrderBy(p => p.X))
                                            .Union(shelfPositionsInYOrder.ElementAt(2).OrderByDescending(p => p.X));

            Debug.WriteLine("Expected:");
            foreach (
                string s in
                    TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin))
            {
                Debug.WriteLine(s);
            }
            Debug.WriteLine("Actual:");
            foreach (string s in shelfPositionsInSequenceOrder.Select(p => p.GetPlanogramProduct().Gtin))
            {
                Debug.WriteLine(s);
            }

            foreach (
                PlanogramSequenceGroupProduct sequenceProd in
                    TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber))
            {
                String expected = sequenceProd.Gtin;
                String actual =
                    shelfPositionsInSequenceOrder.ElementAt(sequenceProd.SequenceNumber - 1).GetPlanogramProduct().Gtin;
                Assert.AreEqual(expected, actual, String.Format("Sequence number: {0}", sequenceProd.SequenceNumber));
            }
        }

        #endregion

        [Test]
        public void ExecuteTask_PreservesPositionAttributes()
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            var prod1 = TestPlanogram.AddProduct();
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            var pos1 = shelf1.AddPosition(bay, prod1);
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Case;
            var prod2 = TestPlanogram.AddProduct();
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            var pos2 = shelf2.AddPosition(bay, prod2);
            pos2.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Case;
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            TestPlanogram.Products.Should().HaveCount(2, "because all products should be placed");
            TestPlanogram.Positions.Should().OnlyContain(
                p => p.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Case,
                "because the merchandising style of the positions should be preserved");
        }

        [Test]
        public void ExecuteTask_WhenPositionsAlreadyExist_TaskShouldStillMerchandise()
        {
            SetTaskParameterValues(2, new Object[] { ProductComparisonType.Rank });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10,10,10);
            for(int i = 0; i < 4; i++) shelf.AddPosition(bay, TestPlanogram.AddProduct(dimensions: productSize));
            TestPlanogram.CreateAssortmentFromProducts();
            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            TestPlanogram.AddSequenceGroup(leftBlock, TestPlanogram.Products.Take(2));
            TestPlanogram.AddSequenceGroup(rightBlock, TestPlanogram.Products.Skip(2));

            _task.ExecuteTask(Context);

            TestPlanogram.Positions.Should().HaveCount(4, "because the original positions should be re-merchandised");
        }

        [Test]
        public void ExecuteTask_WhenProductDoesNotFitAsFront0ButPositionIsOrientated_ProductShouldStillbeMerchandised()
        {
            //tests that a product is not removed  from a plan if the product does not fit at Front0 orientation
            // but does fit using the position's preferred orientation

            SetTaskParameterValues(2, new Object[] { ProductComparisonType.Rank });
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelfSubComponent = shelf.GetSubComp();
            //create a shelf with limited merch height
            shelfSubComponent.MerchandisableHeight = 10;
            //create a product that is taller than the merch hieght
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 15, 9);
            //add it to the shelf
            PlanogramPosition position = shelf.AddPosition(bay, TestPlanogram.AddProduct(dimensions: productSize));
            //orientate it so that it fits
            position.OrientationType = PlanogramPositionOrientationType.Bottom0;
            TestPlanogram.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups[0].AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.UpdatePositionSequenceData();

            _task.ExecuteTask(Context);

            TestPlanogram.Products.Should().HaveCount(1, "because the product should be placed");
            TestPlanogram.Positions.Should().HaveCount(1, "because the position should exist");
        }

        [Test]
        public void ExecuteTask_ManualComponent_NoExceptionIsThrownAndComponentEndsAsManual(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureComponent shelf1 = TestPlanogram.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponent subComponent = shelf1.GetPlanogramComponent().SubComponents.First();
            subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            TestPlanogram.AddBlocking();
            TestPlanogram.CreateAssortmentFromProducts();

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            TestDelegate execute = () => _task.ExecuteTask(Context);

            Assert.DoesNotThrow(execute);
            Assert.AreEqual(PlanogramSubComponentXMerchStrategyType.Manual, subComponent.MerchandisingStrategyX);
            Assert.AreEqual(PlanogramSubComponentYMerchStrategyType.Manual, subComponent.MerchandisingStrategyY);
            Assert.AreEqual(PlanogramSubComponentZMerchStrategyType.Manual, subComponent.MerchandisingStrategyZ);
        }

        [Test]
        public void ExecuteTask_PlacesInSequenceForComplexPlan(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(true, false)] Boolean reverseRankOrder)
        {
            const Int32 numberOfProducts = 300;
            const Single productSize = 10f;

            #region Bays and Components

            #region Bay 1
		    // Bay 1
            /*       ___________
             *      |+---------+|
             *      || : : : : ||
             *      || : : : : ||
             *      || : : : : ||
             *      |+---------+|
             *      |===========|
             *      |           |
             *      |===========|
             *      |___________|
             */
            var bay1 = TestPlanogram.AddFixtureItem();
            bay1.BaySequenceNumber = 1;
            var b1Shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0));
            var b1Shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 80, 0));
            var b1Peg1 = bay1.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 100, 0), new WidthHeightDepthValue(120, 100, 4));

            b1Shelf1.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            b1Shelf1.GetSubComp().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            b1Shelf2.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            b1Shelf2.GetSubComp().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            b1Peg1.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            b1Peg1.GetSubComp().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked; 
	        #endregion

            #region Bay 2
            // Bay 2
            /*       ___________
             *      |           |
             *      |===========|
             *      |           |
             *      |           |
             *      |===========|
             *      |           |
             *      | _________ |
             *      ||         ||
             *      ||_________||
             */
            var bay2 = TestPlanogram.AddFixtureItem();
            bay2.BaySequenceNumber = 2;
            bay2.Angle = Convert.ToSingle(Math.PI / 2f);
            bay2.X = 195;
            var b2Chest1 = bay2.AddFixtureComponent(PlanogramComponentType.Chest);
            var b2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var b2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));

            b2Chest1.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            b2Chest1.GetSubComp().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
            b2Shelf1.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            b2Shelf2.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual; 
            #endregion

            #region Bay 3
            // Bay 3
            /*       ___________
             *      |+---------+|  
             *      |           |
             *      |+---------+|
             *      |           |
             *      |  o     o  |
             *      |           |
             *      |  o     o  |
             *      |           |
             *      |___________|
             */
            var bay3 = TestPlanogram.AddFixtureItem();
            bay3.BaySequenceNumber = 3;
            bay3.X = 240;
            var b3Rod1 = bay3.AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(30, 50, 0));
            var b3Rod2 = bay3.AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(90, 50, 0));
            var b3Rod3 = bay3.AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(30, 100, 0));
            var b3Rod4 = bay3.AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(90, 100, 0));
            var b3Bar1 = bay3.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 148, 0));
            var b3Bar2 = bay3.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 198, 0));

            b3Rod1.GetSubComp().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;
            b3Rod2.GetSubComp().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
            b3Rod3.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            b3Rod4.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            b3Bar1.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            b3Bar2.GetSubComp().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked; 
            #endregion

            #endregion

            #region Blocking
            /*      (Group A)                  (Group B)
             *         _ _ _ _ _ _ _ _ _|_ _ _ _ _ _ _ _ _
             *        !...........!     |     !...........!
             *      __!:_________:!_._._|_._._!._._._._._.!__
             *        !:         :!     |     !     |     !
             * (Group !:.........:!.....|.....!     |     !
             *    F)  !...........!     |     !  c  |  c  !  (Group C)
             *        !           !.....|.....!     |     !
             *      __!._._._._._.!:____|    :!  c  |  c  !
             *        !_ _ _ _ _ _!:_ _ | _ _:!_ _ _|_ _ _!
             *                          |           |  
             *        (Group E)            (Group D)
             */
            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0f, 175f / 225f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 35f / 225f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 175f / 225f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.85f, 0, PlanogramBlockingDividerType.Vertical);

            var groupA = blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0) && g.GetBlockingLocations().First().Y.EqualTo(175f / 225f));
            var groupB = blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0.5f) && g.GetBlockingLocations().First().Y.EqualTo(175f / 225f));
            var groupC = blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0.85f) && g.GetBlockingLocations().First().Y.EqualTo(0));
            var groupD = blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0.5f) && g.GetBlockingLocations().First().Y.EqualTo(0));
            var groupE = blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0) && g.GetBlockingLocations().First().Y.EqualTo(0));
            var groupF = blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0) && g.GetBlockingLocations().First().Y.EqualTo(35f / 225f));

            groupA.Name = "Group A";
            groupB.Name = "Group B";
            groupC.Name = "Group C";
            groupD.Name = "Group D";
            groupE.Name = "Group E";
            groupF.Name = "Group F";

            foreach (var blockingGroup in blocking.Groups)
            {
                blockingGroup.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
                blockingGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BackToFront;
                blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.TopToBottom;
            }
            #endregion

            #region Products and Sequence
		    
            TestPlanogram.Sequence.ApplyFromBlocking(blocking);

            TestPlanogram.AddProducts(new WidthHeightDepthValue(productSize, productSize, productSize), iterations: numberOfProducts);

            TestPlanogram.Sequence.Groups
                .First(g => g.GetBlockingGroup(blocking) == groupA)
                .AddSequenceProducts(TestPlanogram.Products.Skip(0).Take(40));
            TestPlanogram.Sequence.Groups
                .First(g => g.GetBlockingGroup(blocking) == groupB)
                .AddSequenceProducts(TestPlanogram.Products.Skip(40).Take(30));
            TestPlanogram.Sequence.Groups
                .First(g => g.GetBlockingGroup(blocking) == groupC)
                .AddSequenceProducts(TestPlanogram.Products.Skip(70).Take(30));
            TestPlanogram.Sequence.Groups
                .First(g => g.GetBlockingGroup(blocking) == groupD)
                .AddSequenceProducts(TestPlanogram.Products.Skip(100).Take(60));
            TestPlanogram.Sequence.Groups
                .First(g => g.GetBlockingGroup(blocking) == groupE)
                .AddSequenceProducts(TestPlanogram.Products.Skip(160).Take(20));
            TestPlanogram.Sequence.Groups
                .First(g => g.GetBlockingGroup(blocking) == groupF)
                .AddSequenceProducts(TestPlanogram.Products.Skip(180).Take(100));

            IOrderedEnumerable<PlanogramProduct> orderedProducts = TestPlanogram.Products.OrderBy(p => p.Gtin);
            TestPlanogram.CreateAssortmentFromProducts(reverseRankOrder ? orderedProducts.Reverse() : orderedProducts);
	        #endregion

            SetTaskParameterValues(IgnoreMerchandisingDimensionsParameterId, new Object[] { IgnoreMerchandisingDimensionsType.Yes });
            SetTaskParameterValues(ProductComparisonTypeParameterId, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("ExecuteTask_PlacesInSequenceForComplexPlan");

            // This dictionary contains the actual sequence of gtins that appear on the components. In case it looks
            // like gobbledegook, uncomment the line above to save and open the plan. I transcribed the positions
            // into this dictionary so that we can assert that the positions end up where we can visually see that they
            // should.
            Boolean groupAReverse = false;
            Boolean groupDReverse = false;
            Boolean groupEReverse = true;
            Boolean groupFReverse = true;
            Boolean groupFChestReverse = false;
            var actualGtinsByBlockingGroupColour = new Dictionary<Int32, IEnumerable<String>>()
            {
                #region Group A
                {
                    groupA.Colour,
                    b2Shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                        .OrderByDescending(p => p.SequenceX)
                        .Skip(6)
                        .Select(p => p.GetPlanogramProduct().Gtin)
                        .Union(
                        b1Peg1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                            .GroupBy(p => p.SequenceY)
                            .OrderBy(g => g.Key)
                            .Skip(4)
                            .Reverse()
                            .SelectMany(g => 
                                {
                                    IEnumerable<PlanogramPosition> returnValue;
                                    if(groupAReverse)
                                    {
                                        returnValue = g.OrderByDescending(p => p.SequenceX);
                                    }
                                    else
                                    {
                                        returnValue = g.OrderBy(p => p.SequenceX);
                                    }
                                    groupAReverse = !groupAReverse;
                                    return returnValue;
                                })
                            .Select(p => p.GetPlanogramProduct().Gtin))
                },
                #endregion

                #region Group B
                {
                    groupB.Colour,
                    b3Bar2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                        .OrderByDescending(p => p.SequenceX)
                        .Select(p => p.GetPlanogramProduct().Gtin)
                        .Union(
                        b2Shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                            .OrderBy(p => p.SequenceX)
                            .Skip(6)
                            .Select(p => p.GetPlanogramProduct().Gtin))
                },
                #endregion
            
                #region Group C
                {
                    groupC.Colour,
                    b3Bar1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                        .OrderByDescending(p => p.SequenceX)
                        .Take(5)
                        .Select(p => p.GetPlanogramProduct().Gtin)
                        .Union(
                        b3Rod4.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                            .OrderByDescending(p => p.SequenceZ)
                            .Select(p => p.GetPlanogramProduct().Gtin))
                            .Union(
                            b3Rod2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                                .OrderBy(p => p.SequenceZ)
                                .Select(p => p.GetPlanogramProduct().Gtin))
                },
                #endregion

                #region Group D
		        {
                    groupD.Colour,
                    b3Bar1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                        .OrderByDescending(p => p.SequenceX)
                        .Skip(5)
                        .Select(p => p.GetPlanogramProduct().Gtin)
                        .Union(
                        b2Shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                            .OrderBy(p => p.SequenceX)
                            .Skip(6)
                            .Select(p => p.GetPlanogramProduct().Gtin))
                            .Union(
                            b3Rod3.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                                .OrderByDescending(p => p.SequenceZ)
                                .Select(p => p.GetPlanogramProduct().Gtin))
                                .Union(
                                b3Rod1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                                    .OrderBy(p => p.SequenceZ)
                                    .Select(p => p.GetPlanogramProduct().Gtin))
                                    .Union(
                                    b2Chest1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                                        .GroupBy(p => p.SequenceX)
                                        .OrderBy(p => p.Key)
                                        .Skip(5)
                                        .SelectMany(g => g)
                                        .GroupBy(p => p.SequenceZ)
                                        .OrderByDescending(g => g.Key)
                                        .SelectMany(g =>
                                            {
                                                IEnumerable<PlanogramPosition> returnValue;
                                                if (groupDReverse)
                                                {
                                                    returnValue = g.OrderByDescending(p => p.SequenceX);
                                                }
                                                else
                                                {
                                                    returnValue = g.OrderBy(p => p.SequenceX);
                                                }
                                                groupDReverse = !groupDReverse;
                                                return returnValue;
                                            })
                                        .Select(p => p.GetPlanogramProduct().Gtin))
                },
                #endregion

                #region Group E
                {
                    groupE.Colour,
                    b2Chest1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                        .GroupBy(p => p.SequenceX)
                        .OrderBy(g => g.Key)
                        .Take(5)
                        .SelectMany(g => g)
                        .GroupBy(p => p.SequenceZ)
                        .OrderBy(g => g.Key)
                        .Skip(3)
                        .SelectMany(g =>
                            {
                                IEnumerable<PlanogramPosition> returnValue;
                                if (groupEReverse)
                                {
                                    returnValue = g.OrderByDescending(p => p.SequenceX);
                                }
                                else
                                {
                                    returnValue = g.OrderBy(p => p.SequenceX);
                                }
                                groupEReverse = !groupEReverse;
                                return returnValue;
                            })
                        .Select(p => p.GetPlanogramProduct().Gtin)
                },
                #endregion

                #region Group F
                {
                    groupF.Colour,
                    b2Shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                        .OrderByDescending(p => p.SequenceX)
                        .Skip(6)
                        .Select(p => p.GetPlanogramProduct().Gtin)
                        .Union(
                        b1Peg1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                            .GroupBy(p => p.SequenceY)
                            .OrderByDescending(g => g.Key)
                            .Skip(4)
                            .SelectMany(g =>
                                {
                                    IEnumerable<PlanogramPosition> returnValue;
                                    if(groupFReverse)
                                    {
                                        returnValue = g.OrderByDescending(p => p.SequenceX);
                                    }
                                    else
                                    {
                                        returnValue = g.OrderBy(p => p.SequenceX);
                                    }
                                    groupFReverse = !groupFReverse;
                                    return returnValue;
                                })
                            .Select(p => p.GetPlanogramProduct().Gtin))
                            .Union(
                            b1Shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                                .OrderBy(p => p.SequenceX)
                                .Select(p => p.GetPlanogramProduct().Gtin))
                                .Union(
                                b1Shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                                    .OrderByDescending(p => p.SequenceX)
                                    .Select(p => p.GetPlanogramProduct().Gtin))
                                    .Union(
                                    b2Chest1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                                        .GroupBy(p => p.SequenceX)
                                        .OrderBy(g => g.Key)
                                        .Take(5)
                                        .SelectMany(g => g)
                                        .GroupBy(p => p.SequenceZ)
                                        .OrderByDescending(g => g.Key)
                                        .Skip(3)
                                        .SelectMany(g =>
                                            {
                                                IEnumerable<PlanogramPosition> returnValue;
                                                if(groupFChestReverse)
                                                {
                                                    returnValue = g.OrderByDescending(p => p.SequenceX);
                                                }
                                                else
                                                {
                                                    returnValue = g.OrderBy(p => p.SequenceX);
                                                }
                                                groupFChestReverse = !groupFChestReverse;
                                                return returnValue;
                                            })
                                        .Select(p => p.GetPlanogramProduct().Gtin))
                }
                #endregion
            };


            // Check that all the products from the sequence were added and that they were added in the right order.
            foreach (var sequenceGroup in TestPlanogram.Sequence.Groups)
            { 
                IEnumerable<String> actualEnumeration;
                if (!actualGtinsByBlockingGroupColour.TryGetValue(sequenceGroup.Colour, out actualEnumeration)) continue;
                var actual = actualEnumeration.ToList();
                IEnumerable<PlanogramSequenceGroupProduct> groupProducts = sequenceGroup.Products.OrderBy(p => p.SequenceNumber);
                groupProducts = reverseRankOrder ? groupProducts.Skip(Math.Max(0, groupProducts.Count() - actual.Count)) : groupProducts.Take(actual.Count);
                actual.Should().Equal(
                    groupProducts.Select(p => p.Gtin), 
                    $"because the products should be placed in sequence order for block {sequenceGroup.GetBlockingGroup(blocking).Name}");
            }

        }

        [Test]
        public void ExecuteTask_RespectsComponentSpaceWithinBlockSpace_WithBars(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            var bay1 = TestPlanogram.AddFixtureItem();
            var bay2 = TestPlanogram.AddFixtureItem();
            bay2.X = 120;
            var bar = bay2.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 100, 0));
            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0.55f, 0, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 10; i++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            }

            TestPlanogram.Sequence.ApplyFromBlocking(blocking);
            TestPlanogram.Sequence.Groups
                .First(g => g.GetBlockingGroup(blocking).GetBlockingLocations().First().X.EqualTo(0))
                .AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products.OrderBy(p => p.Gtin));
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            Assert.AreEqual(1, TestPlanogram.Positions.Count);
        }

        #region Pegboard behavior

        [Test]
        public void ExecuteTask_OnePegboard_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] PlanogramBlockingGroupPlacementType sequenceDirectionX,
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] PlanogramBlockingGroupPlacementType sequenceDirectionY,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,
                PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even)] PlanogramSubComponentXMerchStrategyType xMerchStrategy,
            [Values(PlanogramSubComponentYMerchStrategyType.BottomStacked,
                PlanogramSubComponentYMerchStrategyType.TopStacked, PlanogramSubComponentYMerchStrategyType.Even)] PlanogramSubComponentYMerchStrategyType yMerchStrategy)
        {
            const Int16 products = 5; // number of products per row
            const Int16 rows = 5; // number of rows.
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent pegBoard = bay1.AddFixtureComponent(PlanogramComponentType.Peg);
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = xMerchStrategy;
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = yMerchStrategy;

            for (Int16 p = 1; p <= rows * products; p++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(10f, 10f, 5), false);
            }
            MerchandisePlanogram();
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirectionX;
            blockingGroup.BlockPlacementSecondaryType = sequenceDirectionY;
            blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();

            Stopwatch timer = new Stopwatch(); timer.Start();
            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);
            timer.Stop(); Debug.WriteLine("Task execution took: {0} ms", timer.ElapsedMilliseconds);

            IEnumerable<IEnumerable<PlanogramPosition>> rowsOfPositions =
                sequenceDirectionY.IsPositive() ?
                TestPlanogram.Positions.OrderBy(p => p.SequenceY).GroupBy(p => p.SequenceY) :
                TestPlanogram.Positions.OrderByDescending(p => p.SequenceY).GroupBy(p => p.SequenceY).OrderByDescending(p => p.Key);
            List<IEnumerable<PlanogramPosition>> sequencedRowsOfPositions = new List<IEnumerable<PlanogramPosition>>();
            Boolean reverseRow = false;
            foreach (IEnumerable<PlanogramPosition> row in rowsOfPositions)
            {
                IEnumerable<PlanogramPosition> orderedRow =
                    !sequenceDirectionX.IsPositive() ?
                    row.OrderByDescending(p => p.SequenceX) :
                    row.OrderBy(p => p.SequenceX);
                if (reverseRow) orderedRow = orderedRow.Reverse();
                reverseRow = !reverseRow;
                sequencedRowsOfPositions.Add(orderedRow);
            }
            IEnumerable<PlanogramPosition> flattenedPositions = sequencedRowsOfPositions.SelectMany(r => r);

            var expected = TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
            var actual = flattenedPositions.Select(p => p.GetPlanogramProduct().Gtin).ToList();

            WriteList("Expected:", expected);
            WriteList("Actual:", actual);

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void ExecuteTask_OnePegboard_TwoBlocks(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionX,
            [Values(PlanogramBlockingGroupPlacementType.BottomToTop, PlanogramBlockingGroupPlacementType.TopToBottom)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionY,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even)] 
            PlanogramSubComponentXMerchStrategyType xMerchStrategy,
            [Values(PlanogramSubComponentYMerchStrategyType.BottomStacked, PlanogramSubComponentYMerchStrategyType.TopStacked, PlanogramSubComponentYMerchStrategyType.Even)] 
            PlanogramSubComponentYMerchStrategyType yMerchStrategy)
        {
            const Int16 noOfProducts = 36;
            const Single productSize = 10f;
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            bay1.GetPlanogramFixture().Height = 120;
            PlanogramFixtureComponent pegBoard = bay1.AddFixtureComponent(PlanogramComponentType.Peg, size: new WidthHeightDepthValue(120, 120, 4));
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = xMerchStrategy;
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = yMerchStrategy;
            for (Int16 r = 1; r <= noOfProducts; r++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(productSize, productSize, 5));
            }
            TestPlanogram.AddBlocking().Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            Int32 skip = 0;
            foreach (PlanogramBlockingGroup blockingGroup in TestPlanogram.Blocking.First().Groups)
            {
                blockingGroup.BlockPlacementPrimaryType = sequenceDirectionX;
                blockingGroup.BlockPlacementSecondaryType = sequenceDirectionY;
                blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
                var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                TestPlanogram.Sequence.Groups.Add(sequenceGroup);
                sequenceGroup.AddSequenceProducts(TestPlanogram.Products.Skip(skip).Take(noOfProducts / 2));
                skip += noOfProducts / 2;
            }
            TestPlanogram.CreateAssortmentFromProducts();
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            foreach (var sequenceGroup in TestPlanogram.Sequence.Groups)
            {
                var expected = sequenceGroup.Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
                WriteList("Expected:", expected);
                var groupPositions = TestPlanogram.Positions
                    .Where(p => sequenceGroup.Products.Select(s => s.Gtin).Contains(p.GetPlanogramProduct().Gtin));
                var actual =
                    sequenceDirectionY.IsPositive() ?
                    groupPositions.OrderBy(p => p.SequenceY).Select(p => p.GetPlanogramProduct().Gtin).ToList() :
                    groupPositions.OrderByDescending(p => p.SequenceY).Select(p => p.GetPlanogramProduct().Gtin).ToList();
                WriteList("Actual:", actual);
                CollectionAssert.AreEqual(expected, actual);
            }
        }
        #endregion

        #region Chest behavior

        [Test]
        public void ExecuteTask_OneChest_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionX,
            [Values(PlanogramBlockingGroupPlacementType.FrontToBack, PlanogramBlockingGroupPlacementType.BackToFront)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionZ,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even)] 
            PlanogramSubComponentXMerchStrategyType xMerchStrategy,
            [Values(PlanogramSubComponentZMerchStrategyType.BackStacked,PlanogramSubComponentZMerchStrategyType.FrontStacked, PlanogramSubComponentZMerchStrategyType.Even)] 
            PlanogramSubComponentZMerchStrategyType zMerchStrategy)
        {
            const Int16 products = 5; // number of products per row
            const Int16 rows = 3; // number of rows.
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            bay1.GetPlanogramFixture().Height = 120;
            PlanogramFixtureComponent pegBoard = bay1.AddFixtureComponent(PlanogramComponentType.Chest);
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = xMerchStrategy;
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyZ = zMerchStrategy;
            for (Int16 r = 1; r <= rows; r++)
            {
                for (Int16 p = 1; p <= products; p++)
                {
                    TestPlanogram.AddProduct(new WidthHeightDepthValue(20, 20, 20));
                }
            }
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirectionX;
            blockingGroup.BlockPlacementSecondaryType = sequenceDirectionZ;
            blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            IEnumerable<IEnumerable<PlanogramPosition>> rowsOfPositions =
                SequenceRows(
                sequenceDirectionX, 
                sequenceDirectionZ.IsPositive()
                    ? TestPlanogram.Positions.GroupBy(p => p.SequenceZ).OrderBy(p => p.Key)
                    : TestPlanogram.Positions.GroupBy(p => p.SequenceZ).OrderByDescending(p => p.Key));
            Int32 row = 1;
            foreach (var rowOfPositions in rowsOfPositions)
            {
                WriteList(
                    String.Format("Row {0} at Z sequence {1}:", row, rowOfPositions.First().SequenceZ), 
                    rowOfPositions.Select(p => p.GetPlanogramProduct().Gtin));
                row++;
            }

            IEnumerable<String> actual =
                rowsOfPositions.SelectMany(r => r).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            IEnumerable<String> expected =
                TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();

            WriteList("Expected:", expected);
            WriteList("Actual:", actual);
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void ExecuteTask_OneChest_TwoVerticalBlocks(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionX,
            [Values(PlanogramBlockingGroupPlacementType.FrontToBack, PlanogramBlockingGroupPlacementType.BackToFront)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionZ,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even)] 
            PlanogramSubComponentXMerchStrategyType xMerchStrategy,
            [Values(PlanogramSubComponentZMerchStrategyType.BackStacked,PlanogramSubComponentZMerchStrategyType.FrontStacked, PlanogramSubComponentZMerchStrategyType.Even)] 
            PlanogramSubComponentZMerchStrategyType zMerchStrategy)
        {
            const Int16 products = 5; // number of products per row
            const Int16 rows = 4; // number of rows.
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            bay1.GetPlanogramFixture().Height = 50;
            PlanogramFixtureComponent pegBoard = bay1.AddFixtureComponent(PlanogramComponentType.Chest);
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = xMerchStrategy;
            pegBoard.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyZ = zMerchStrategy;
            for (Int16 r = 1; r <= rows; r++)
            {
                for (Int16 p = 1; p <= products; p++)
                {
                    TestPlanogram.AddProduct(new WidthHeightDepthValue(20, 20, 15));
                }
            }
            MerchandisePlanogram();
            TestPlanogram.AddBlocking().Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            foreach (PlanogramBlockingGroup g in TestPlanogram.Blocking.First().Groups)
            {
                g.BlockPlacementPrimaryType = sequenceDirectionX;
                g.BlockPlacementSecondaryType = sequenceDirectionZ;
                g.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            }
            PlanogramBlockingGroup frontGroup =
                TestPlanogram.Blocking.First().Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0f));
            frontGroup.Name = "Front";
            PlanogramBlockingGroup backGroup =
                TestPlanogram.Blocking.First().Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.5f));
            backGroup.Name = "Back";
            TestPlanogram.Sequence.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup(frontGroup));
            TestPlanogram.Sequence.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup(backGroup));
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products.Take(Convert.ToInt32(Math.Floor(rows * products / 2f))));
            TestPlanogram.Sequence.Groups.Last().AddSequenceProducts(TestPlanogram.Products.Skip(Convert.ToInt32(Math.Floor(rows * products / 2f))));
            TestPlanogram.CreateAssortmentFromProducts();
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            IEnumerable<IEnumerable<PlanogramPosition>> rowsOfPositions =
                sequenceDirectionZ.IsPositive()
                    ? TestPlanogram.Positions.GroupBy(p => p.SequenceZ).OrderBy(p => p.Key)
                    : TestPlanogram.Positions.GroupBy(p => p.SequenceZ).OrderByDescending(p => p.Key);

            IEnumerable<IEnumerable<PlanogramPosition>> frontRows =
                sequenceDirectionZ.IsPositive()
                    ? rowsOfPositions.Skip(rows / 2)
                    : rowsOfPositions.Take(rows / 2);

            IEnumerable<IEnumerable<PlanogramPosition>> backRows =
                sequenceDirectionZ.IsPositive()
                    ? rowsOfPositions.Take(rows/2)
                    : rowsOfPositions.Skip(rows/2);

            IEnumerable<PlanogramPosition> sequencedFrontPositions =
                SequenceRows(sequenceDirectionX, frontRows).SelectMany(r => r);
            IEnumerable<PlanogramPosition> sequencedBackPositions =
                SequenceRows(sequenceDirectionX, backRows).SelectMany(r => r);

            Int32 frontGroupColour = frontGroup.Colour;
            Int32 backGroupColour = backGroup.Colour;
            AssertSequencedProducts(frontGroupColour, sequencedFrontPositions, "Front");
            AssertSequencedProducts(backGroupColour, sequencedBackPositions, "Back");
        }

        #endregion

        #region Bar behaviour

        [Test]
        public void ExecuteTask_OneBar_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionX,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked,PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even)] 
            PlanogramSubComponentXMerchStrategyType xMerchStrategy,
            [Values(IgnoreMerchandisingDimensionsType.Yes, IgnoreMerchandisingDimensionsType.No)]
            IgnoreMerchandisingDimensionsType ignoreMerchandisingDimensions)
        {
            if (ignoreMerchandisingDimensions == IgnoreMerchandisingDimensionsType.No) Assert.Ignore("There is currently a known issue with products not placing correctly on bars, see CCM-18567");
            const Int32 noOfProducts = 10;
            const Single productSize = 10;

            var bay = TestPlanogram.AddFixtureItem();
            var bar = bay.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 150, 0));
            bar.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = xMerchStrategy;
            for (Int32 p = 0; p <= noOfProducts; p++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(productSize, productSize, productSize));
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirectionX;
            blockingGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            SetTaskParameterValues(1, new Object[] { ignoreMerchandisingDimensions });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            var expected = TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
            WriteList("Expected:", expected);
            var actual =
                sequenceDirectionX.IsPositive() ?
                TestPlanogram.Positions.OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList() :
                TestPlanogram.Positions.OrderByDescending(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            WriteList("Actual:", actual);
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void ExecuteTask_OneBar_TwoBlocks(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionX,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked, PlanogramSubComponentXMerchStrategyType.Even)] 
            PlanogramSubComponentXMerchStrategyType xMerchStrategy,
            [Values(IgnoreMerchandisingDimensionsType.Yes, IgnoreMerchandisingDimensionsType.No)]
            IgnoreMerchandisingDimensionsType ignoreMerchandisingDimensions)
        {
            if (ignoreMerchandisingDimensions == IgnoreMerchandisingDimensionsType.No) Assert.Ignore("There is currently a known issue with products not placing correctly on bars, see CCM-18567");
            const Int32 noOfProducts = 10;
            const Single productSize = 10;

            var bay = TestPlanogram.AddFixtureItem();
            var bar = bay.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 150, 0));
            bar.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = xMerchStrategy;
            for (Int32 p = 0; p <= noOfProducts; p++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(productSize, productSize, productSize));
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            TestPlanogram.AddBlocking().Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            Int32 skip = 0;
            foreach (var blockingGroup in TestPlanogram.Blocking.First().Groups)
            {
                blockingGroup.BlockPlacementPrimaryType = sequenceDirectionX;
                blockingGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
                blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
                var newSequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                TestPlanogram.Sequence.Groups.Add(newSequenceGroup);
                newSequenceGroup.AddSequenceProducts(TestPlanogram.Products.Skip(skip).Take(5));
                skip += 5;
            }
            SetTaskParameterValues(1, new Object[] { ignoreMerchandisingDimensions });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            foreach(var sequenceGroup in TestPlanogram.Sequence.Groups)
            {
                var expected = sequenceGroup.Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
                WriteList("Expected:", expected);
                var groupPositions = TestPlanogram.Positions
                    .Where(p => sequenceGroup.Products.Select(s => s.Gtin).Contains(p.GetPlanogramProduct().Gtin));
                var actual =
                    sequenceDirectionX.IsPositive() ?
                    groupPositions.OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList() :
                    groupPositions.OrderByDescending(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList();
                WriteList("Actual:", actual);
                CollectionAssert.AreEqual(expected, actual);
            }
        }

        #endregion

        #region Rod behaviour

        [Test]
        public void ExecuteTask_OneRod_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.FrontToBack, PlanogramBlockingGroupPlacementType.BackToFront)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionZ,
            [Values(PlanogramSubComponentZMerchStrategyType.FrontStacked, PlanogramSubComponentZMerchStrategyType.BackStacked, PlanogramSubComponentZMerchStrategyType.Even)] 
            PlanogramSubComponentZMerchStrategyType zMerchStrategy)
        {
            const Int32 noOfProducts = 7;
            const Single productSize = 10;

            var bay = TestPlanogram.AddFixtureItem();
            var rod = bay.AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(60, 150, 0));
            rod.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyZ = zMerchStrategy;
            for (Int32 p = 0; p < noOfProducts; p++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(productSize, productSize, productSize));
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirectionZ;
            blockingGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            var expected = TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
            WriteList("Expected:", expected);
            var actual =
                sequenceDirectionZ.IsPositive() ?
                TestPlanogram.Positions.OrderBy(p => p.SequenceZ).Select(p => p.GetPlanogramProduct().Gtin).ToList() :
                TestPlanogram.Positions.OrderByDescending(p => p.SequenceZ).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            WriteList("Actual:", actual);
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion

        #region ClipStrip behaviour

        [Test]
        public void ExecuteTask_OneClipStrip_OneBlock(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.TopToBottom, PlanogramBlockingGroupPlacementType.BottomToTop)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionY,
            [Values(PlanogramSubComponentYMerchStrategyType.BottomStacked, PlanogramSubComponentYMerchStrategyType.TopStacked, PlanogramSubComponentYMerchStrategyType.Even)] 
            PlanogramSubComponentYMerchStrategyType yMerchStrategy,
            [Values(IgnoreMerchandisingDimensionsType.Yes, IgnoreMerchandisingDimensionsType.No)]
            IgnoreMerchandisingDimensionsType ignoreMerchandisingDimensions)
        {
            const Int32 noOfProducts = 5;
            const Single productSize = 10;

            var bay = TestPlanogram.AddFixtureItem();
            var clip = bay.AddFixtureComponent(PlanogramComponentType.ClipStrip, new PointValue(60, 0, 0));
            clip.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = yMerchStrategy;
            for (Int32 p = 0; p < noOfProducts; p++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(productSize, productSize, productSize));
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            PlanogramBlockingGroup blockingGroup = TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) }).Groups.First();
            blockingGroup.BlockPlacementPrimaryType = sequenceDirectionY;
            blockingGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(TestPlanogram.Products);
            SetTaskParameterValues(1, new Object[] { ignoreMerchandisingDimensions });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            var expected = TestPlanogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
            WriteList("Expected:", expected);
            var actual =
                sequenceDirectionY.IsPositive() ?
                TestPlanogram.Positions.OrderBy(p => p.SequenceY).Select(p => p.GetPlanogramProduct().Gtin).ToList() :
                TestPlanogram.Positions.OrderByDescending(p => p.SequenceY).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            WriteList("Actual:", actual);
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void ExecuteTask_OneClipStrip_TwoBlocks(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
            [Values(PlanogramBlockingGroupPlacementType.TopToBottom, PlanogramBlockingGroupPlacementType.BottomToTop)] 
            PlanogramBlockingGroupPlacementType sequenceDirectionY,
            [Values(PlanogramSubComponentYMerchStrategyType.BottomStacked, PlanogramSubComponentYMerchStrategyType.TopStacked, PlanogramSubComponentYMerchStrategyType.Even)] 
            PlanogramSubComponentYMerchStrategyType yMerchStrategy)
        {
            const Int32 noOfProducts = 6;
            const Single productSize = 5;

            var bay = TestPlanogram.AddFixtureItem();
            var clip = bay.AddFixtureComponent(PlanogramComponentType.ClipStrip, new PointValue(60, 0, 0));
            clip.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = yMerchStrategy;
            for (Int32 p = 0; p < noOfProducts; p++)
            {
                TestPlanogram.AddProduct(new WidthHeightDepthValue(productSize, productSize, productSize));
            }
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            TestPlanogram.AddBlocking().Dividers.Add(0f, 0.25f, PlanogramBlockingDividerType.Horizontal);
            Int32 skip = 0;
            foreach (var blockingGroup in TestPlanogram.Blocking.First().Groups)
            {
                blockingGroup.BlockPlacementPrimaryType = sequenceDirectionY;
                blockingGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
                blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
                var newGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                TestPlanogram.Sequence.Groups.Add(newGroup);
                newGroup.AddSequenceProducts(TestPlanogram.Products.Skip(skip).Take(noOfProducts/2));
                skip += noOfProducts/2;
            }
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            foreach (var sequenceGroup in TestPlanogram.Sequence.Groups)
            {
                var expected = sequenceGroup.Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
                WriteList("Expected:", expected);
                var groupPositions = TestPlanogram.Positions
                    .Where(p => sequenceGroup.Products.Select(s => s.Gtin).Contains(p.GetPlanogramProduct().Gtin));
                var actual =
                    sequenceDirectionY.IsPositive() ?
                    groupPositions.OrderBy(p => p.SequenceY).Select(p => p.GetPlanogramProduct().Gtin).ToList() :
                    groupPositions.OrderByDescending(p => p.SequenceY).Select(p => p.GetPlanogramProduct().Gtin).ToList();
                WriteList("Actual:", actual);
                CollectionAssert.AreEqual(expected, actual);
            }
        }

        #endregion

        #region Combined behaviour
        #endregion

        #region Merchandising Dimensions behavior

        [Test]
        public void ExecuteTask_WhenFirstProductInPlacementCannotFitBlockButDoesFitComponent_ShouldBePlaced(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            const String expectation = "When the first product on a placement cannot be placed due to block constraints but fits the component, should be placed.";
            PlanogramFixtureItem fixtureItem = TestPlanogram.AddFixtureItem();
            var shelfCoordinates = new List<PointValue>
                               {
                                   new PointValue(0, 0, 0),
                                   new PointValue(0, 20, 0),
                                   new PointValue(0, 40, 0),
                                   new PointValue(0, 80, 0)
                               };
            foreach (PointValue coordinate in shelfCoordinates)
            {
                fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, coordinate);
            }
            List<String> expected = fixtureItem.GetPlanogramFixture().Components.Select(c => c.GetPlanogramComponent().Name).ToList();
            var productSizes = new List<WidthHeightDepthValue>
                                         {
                                             new WidthHeightDepthValue(35, 10, 10),
                                             new WidthHeightDepthValue(10, 10, 10),
                                             new WidthHeightDepthValue(35, 10, 10),
                                             new WidthHeightDepthValue(20, 10, 10)
                                         };
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(productSizes, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.25F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] { PlacementUnitsType.SingleUnit });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Products.Select(p => p.GetPlanogramPositions().Single().GetPlanogramSubComponent().GetPlanogramSubComponentPlacement().Component.Name).ToList();
            CollectionAssert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ExecuteTask_WhenFirstProductIsTooTallAndIgnoreMerchandisingDimensionsIsTrue_ShouldAddAnyway(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem fixtureItem = TestPlanogram.AddFixtureItem();
            fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf);
            fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 20, 10));
            var sizes = new List<WidthHeightDepthValue>
                        {
                            new WidthHeightDepthValue(10, 30, 10),
                            new WidthHeightDepthValue(10, 10, 10),
                            new WidthHeightDepthValue(10, 10, 10)
                        };
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(sizes, Context.EntityId, 0).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] {PlacementUnitsType.SingleUnit});
            SetTaskParameterValues(1, new Object[] { IgnoreMerchandisingDimensions });
            List<String> expected = TestPlanogram.Assortment.Products.OrderBy(p => p.Rank).Select(p => p.Gtin).ToList();
            if (TestPlanogram.Positions.Any())
                Assert.Inconclusive("The planogram should have no positions before running the task.");

            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void ExecuteTask_WhenFirstProductIsTooDeepAndIgnoreMerchandisingDimensionsIsTrue_ShouldAddAnyway(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Shelf)
               .GetPlanogramComponent().SubComponents.First().Depth = 20;
            bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 20, 10));
            var sizes = new List<WidthHeightDepthValue>
                        {
                            new WidthHeightDepthValue(10, 10, 30),
                            new WidthHeightDepthValue(10, 10, 10),
                            new WidthHeightDepthValue(10, 10, 10)
                        };
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(sizes, Context.EntityId, 0).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] {PlacementUnitsType.SingleUnit});
            SetTaskParameterValues(1, new Object[] { IgnoreMerchandisingDimensions });
            List<String> expected = TestPlanogram.Assortment.Products.OrderBy(p => p.Rank).Select(p => p.Gtin).ToList();
            if (TestPlanogram.Positions.Any())
                Assert.Inconclusive("The planogram should have no positions before running the task.");
            
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void ExecuteTask_WhenFirstProductIsTooWideAndIgnoreMerchandisingDimensionsIsTrue_ShouldAddAnyway(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Shelf)
               .GetPlanogramComponent().SubComponents.First().Width = 20;
            bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 20, 10));
            var sizes = new List<WidthHeightDepthValue>
                        {
                            new WidthHeightDepthValue(30, 10, 10),
                            new WidthHeightDepthValue(10, 10, 10),
                            new WidthHeightDepthValue(10, 10, 10)
                        };
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(sizes, Context.EntityId, 0).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] {PlacementUnitsType.SingleUnit});
            SetTaskParameterValues(1, new Object[] { IgnoreMerchandisingDimensions });
            List<String> expected = TestPlanogram.Assortment.Products.OrderBy(p => p.Rank).Select(p => p.Gtin).ToList();
            if (TestPlanogram.Positions.Any())
                Assert.Inconclusive("The planogram should have no positions before running the task.");

            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        #endregion

        #region Assortment behavior

        [Test]
        public void ExecuteTask_WhenAssortmentProductIsMissingFromPlanogram_ShouldBeAdded(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            const String expectation =
                "The merchandised product missing from the planogram should have been added to the Product List.";
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(1, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> {new Tuple<Single, Single>(0.0F, 0.0F)});
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            List<String> expected = TestPlanogram.Assortment.Products.Select(p => p.Gtin).ToList();

            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Products.Select(p => p.Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual, expectation);
        }

        [Test]
        public void ExecuteTask_WhenAssortmentProductExistsInPlanogram_ShouldNotBeAddedAgain(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            const String expectation =
                "The merchandised product previously existing in the planogram should not have been added again to the Product List.";
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(1, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            TestPlanogram.AddProduct(productDtos.First());
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Products.Select(p => p.Gtin).ToList();
            CollectionAssert.AllItemsAreUnique(actual, expectation);
        }

        #endregion

        #region Drop worse performing, readd better performing behaviors

        [Test]
        public void ExecuteTask_WhenNotAllProductsFit_ShouldPlaceBestRankingProducts(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(20, Context.EntityId).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos.Skip(10).Union(productDtos.Take(10)));
            List<String> expected =
                TestPlanogram.Assortment.Products.OrderBy(p => p.Rank).Select(p => p.Gtin).Take(12).ToList();
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void ExecuteTask_WhenBestRankingProductCannotBeReAdded_ShouldStopReAddingWorseProducts(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            List<WidthHeightDepthValue> sizes = new List<WidthHeightDepthValue>
                                                {
                                                    new WidthHeightDepthValue(10, 10, 10),
                                                    new WidthHeightDepthValue(40, 10, 10),
                                                    new WidthHeightDepthValue(40, 10, 10),
                                                    new WidthHeightDepthValue(50, 10, 10),
                                                    new WidthHeightDepthValue(10, 10, 10)
                                                };
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(sizes, Context.EntityId, 0).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos.Skip(10).Union(productDtos.Take(10)));
            List<String> expected =
                TestPlanogram.Assortment.Products.OrderBy(p => p.Rank).Take(3).Select(p => p.Gtin).ToList();
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            List<String> actual = TestPlanogram.Positions.Select(p => p.GetPlanogramProduct().Gtin).ToList();
            CollectionAssert.AreEquivalent(expected, actual);
        }

        #endregion

        #region Drop products in order of assortment rules

        [Test]
        public void OneBlockPlanWithAssortmentThatFits12OutOf20DoesWhatItSays()
        {
            CreateOneBlockPlanWithAssortmentThatFits12OutOf20();

            _task.ExecuteTask(Context);

            PlanogramPositionList placedPositions = TestPlanogram.Positions;
            placedPositions.Should()
                           .HaveCount(PlacedPositionsCount,
                                      String.Format("OneBlockPlanWithAssortmentThatFits12OutOf20 should have placed exactly {0} positions.", PlacedPositionsCount));
        }

        [Test]
        public void WorseTreatmentProductIsDroppedForBetterTreatmentProductWithLowerRank([ValueSource("_worseBeforeBetterTreatmentTypes")] Tuple<PlanogramAssortmentProductTreatmentType,PlanogramAssortmentProductTreatmentType> treatmentTypeTuple)
        {
            CreateOneBlockPlanWithAssortmentThatFits12OutOf20();
            IOrderedEnumerable<PlanogramAssortmentProduct> assortmentProducts = TestPlanogram.Assortment.Products.OrderBy(product => product.Rank);
            foreach (PlanogramAssortmentProduct product in assortmentProducts)
            {
                product.ProductTreatmentType = treatmentTypeTuple.Item1;
            }
            PlanogramAssortmentProduct worseHighRankProduct = assortmentProducts.Skip(PlacedPositionsMinusLastCount).First();
            PlanogramAssortmentProduct betterLowRankProduct = assortmentProducts.Skip(PlacedPositionsCount).First();
            betterLowRankProduct.ProductTreatmentType = treatmentTypeTuple.Item2;
            Context.WorkflowTask.Parameters[ProductComparisonTypeParameterId].Value = ProductComparisonType.AssortmentRules;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionsGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionsGtins.Should()
                          .ContainInOrder(assortmentProducts.Take(PlacedPositionsMinusLastCount).Select(product => product.Gtin),
                                          "there is no better ranked product for these")
                          .And.NotContain(worseHighRankProduct.Gtin,
                                          "a high ranking product should be dropped in favor of a low ranking product with better ProductTreatmentType")
                          .And.Contain(betterLowRankProduct.Gtin,
                                       "a low rank product should force dropping other high rank products with worse ProductTreatmentTypes");
        }

        [Test]
        public void WorseTreatmentProductIsNotPlacedBeforeBetterTreatmentProductWithHigherRank([ValueSource("_worseBeforeBetterTreatmentTypes")] Tuple<PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductTreatmentType> treatmentTypeTuple)
        {
            CreateOneBlockPlanWithAssortmentThatFits12OutOf20();
            IOrderedEnumerable<PlanogramAssortmentProduct> assortmentProducts = TestPlanogram.Assortment.Products.OrderBy(product => product.Rank);
            foreach (PlanogramAssortmentProduct product in assortmentProducts)
            {
                product.ProductTreatmentType = treatmentTypeTuple.Item2;
            }
            PlanogramAssortmentProduct betterHighRankProduct = assortmentProducts.Skip(PlacedPositionsMinusLastCount).First();
            PlanogramAssortmentProduct worseLowRankProduct = assortmentProducts.Skip(PlacedPositionsCount).First();
            worseLowRankProduct.ProductTreatmentType = treatmentTypeTuple.Item1;
            Context.WorkflowTask.Parameters[ProductComparisonTypeParameterId].Value = ProductComparisonType.AssortmentRules;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(assortmentProducts.Take(11).Select(product => product.Gtin),
                                         "there is no better ranked product for these")
                         .And.NotContain(worseLowRankProduct.Gtin,
                                         "a low ranking product should never force dropping a high ranking product of equal or better ProductTreatmentType")
                         .And.Contain(betterHighRankProduct.Gtin,
                                      "a high rank product with better treatment should not be dropped for a worse one with lower rank");
        }

        [Test]
        public void HighRankProductWithLowerPriorityThanProductRuleIsDroppedForLowRankProductWithProductRule([ValueSource("_productTreatmentTypesWithLowerPriorityThanProductRule")] PlanogramAssortmentProductTreatmentType worseProductTreatmentType)
        {
            CreateOneBlockPlanWithAssortmentThatFits12OutOf20();
            IOrderedEnumerable<PlanogramAssortmentProduct> assortmentProducts = TestPlanogram.Assortment.Products.OrderBy(product => product.Rank);
            foreach (PlanogramAssortmentProduct product in assortmentProducts)
            {
                product.ProductTreatmentType = worseProductTreatmentType;
            }
            PlanogramAssortmentProduct highRankProductWithLowerPriority = assortmentProducts.Skip(PlacedPositionsMinusLastCount).First();
            PlanogramAssortmentProduct lowRankProductWithHigherPriority = assortmentProducts.Skip(PlacedPositionsCount).First();
            lowRankProductWithHigherPriority.MinListUnits = 2;
            lowRankProductWithHigherPriority.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Optional;
            Context.WorkflowTask.Parameters[ProductComparisonTypeParameterId].Value = ProductComparisonType.AssortmentRules;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(assortmentProducts.Take(PlacedPositionsMinusLastCount).Select(product => product.Gtin),
                                         "there is no better ranked product for these")
                         .And.NotContain(highRankProductWithLowerPriority.Gtin,
                                         "a high ranking product with Treatment Type lower in priority than Product Rules should be dropped in favor of a low ranking product with a Product Rule")
                         .And.Contain(lowRankProductWithHigherPriority.Gtin,
                                      "a low rank product with product rules should force dropping other high rank products with Treatment Type lower in priority than Product Rules");
        }

        [Test]
        public void LowRankProductWithProductRuleDoesNotForceDropHighRankProductWithHigherPriorityThanProductRule([ValueSource("_productTreatmentTypesWithHigherPriorityThanProductRule")] PlanogramAssortmentProductTreatmentType betterProductTreatmentType)
        {
            CreateOneBlockPlanWithAssortmentThatFits12OutOf20();
            IOrderedEnumerable<PlanogramAssortmentProduct> assortmentProducts = TestPlanogram.Assortment.Products.OrderBy(product => product.Rank);
            foreach (PlanogramAssortmentProduct product in assortmentProducts)
            {
                product.ProductTreatmentType = betterProductTreatmentType;
            }
            PlanogramAssortmentProduct highRankProductWithHigherPriority = assortmentProducts.Skip(PlacedPositionsMinusLastCount).First();
            PlanogramAssortmentProduct lowRankProductWithLowerPriority = assortmentProducts.Skip(PlacedPositionsCount).First();
            lowRankProductWithLowerPriority.MinListUnits = 2;
            lowRankProductWithLowerPriority.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Optional;
            Context.WorkflowTask.Parameters[ProductComparisonTypeParameterId].Value = ProductComparisonType.AssortmentRules;

            _task.ExecuteTask(Context);

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(assortmentProducts.Take(PlacedPositionsMinusLastCount).Select(product => product.Gtin),
                                         "there is no better ranked product for these")
                         .And.NotContain(lowRankProductWithLowerPriority.Gtin,
                                         "a low rank product with Product Rule should not force dropping of a high rank product with Treatment Type higher in priority than Product Rules")
                         .And.Contain(highRankProductWithHigherPriority.Gtin,
                                      "a high rank product with Treatment Type higher in priority than Product Rules should not be dropped for a low rank product with Product Rule");
        }

        [Test]
        public void HighRankProductWithFamilyRuleIsDroppedForLowRankProductWithProductRule()
        {
            CreateOneBlockPlanWithAssortmentThatFits12OutOf20();
            IOrderedEnumerable<PlanogramAssortmentProduct> assortmentProducts = TestPlanogram.Assortment.Products.OrderBy(product => product.Rank);
            foreach (PlanogramAssortmentProduct product in assortmentProducts.Take(PlacedPositionsCount))
            {
                product.FamilyRuleName = "First 12 family";
                product.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.MinimumProductCount;
                product.FamilyRuleValue = 2;
            }
            PlanogramAssortmentProduct highRankProductWithFamilyRule = assortmentProducts.Skip(PlacedPositionsMinusLastCount).First();
            PlanogramAssortmentProduct lowRankProductWithProductRule = assortmentProducts.Skip(PlacedPositionsCount).First();
            lowRankProductWithProductRule.MinListUnits = 2;
            Context.WorkflowTask.Parameters[ProductComparisonTypeParameterId].Value = ProductComparisonType.AssortmentRules;

            _task.ExecuteTask(Context);
            //TestPlanogram.TakeSnapshot("FamilyRuleDroppedForProductRule");

            IEnumerable<String> positionGtins = TestPlanogram.Positions.Select(position => position.GetPlanogramProduct().Gtin);
            positionGtins.Should()
                         .ContainInOrder(assortmentProducts.Take(PlacedPositionsMinusLastCount).Select(product => product.Gtin),
                                         "there is no better ranked product for these")
                         .And.NotContain(highRankProductWithFamilyRule.Gtin,
                                         "a high ranking product with Family Rules should be dropped in favor of a low ranking product with Product Rules")
                         .And.Contain(lowRankProductWithProductRule.Gtin,
                                      "a low rank product with Product Rules should force dropping other high rank product with Family Rules");
        }

        #endregion

        #region Place correct units

        [Test]
        public void ExecuteTask_WhenSingleUnit_ShouldAddSingleUnit(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            NewPlaceCorrectUnitsTestPlanogram();
            const Int32 expected = 1;
            TestPlanogram.Assortment.Products.First().Units = 10*expected;
            SetTaskParameterValues(0, new Object[] {PlacementUnitsType.SingleUnit});
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            AssertTotalUnitsEquals(expected);
        }

        [Test]
        public void ExecuteTask_WhenAssortmentUnits_ShouldAddAssortmentUnits(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureComponent shelf = NewPlaceCorrectUnitsTestPlanogram();
            Int16 expected = Convert.ToInt16(2*Math.Truncate(shelf.GetPlanogramComponent().Depth/10));
            TestPlanogram.Assortment.Products.First().Units = expected;
            SetTaskParameterValues(0, new Object[] {PlacementUnitsType.AssortmentUnits});
            SetTaskParameterValues(2, new Object[] { productComparisonType });
            
            _task.ExecuteTask(Context);

            AssertTotalUnitsEquals(expected);
        }

        #endregion

        #region Compact Block Space

        [Test]
        public void ExecuteTask_ShouldCompactSpace(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            var shelfCoordinates = new List<PointValue>
                               {
                                   new PointValue(0, 0, 0),
                                   new PointValue(0, 20, 0),
                                   new PointValue(0, 40, 0),
                                   new PointValue(0, 80, 0)
                               };
            foreach (PointValue coordinate in shelfCoordinates)
            {
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, coordinate);
            }

            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(12, Context.EntityId, size: new WidthHeightDepthValue(25, 10, 10)).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] {PlacementUnitsType.SingleUnit});
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            Assert.IsTrue(TestPlanogram.GetPlanogramSubComponentPlacements().All(p => p.GetPlanogramPositions().Count() == 3), "There should be three products on each one of the shelves");
        }

        [Test]
        public void ExecuteTask_WhenCompactingMakesWorse_ShouldNotCompactSpace(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            var shelfCoordinates = new List<PointValue>
                               {
                                   new PointValue(0, 0, 0),
                                   new PointValue(0, 20, 0),
                                   new PointValue(0, 40, 0)
                               };
            foreach (PointValue coordinate in shelfCoordinates)
            {
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, coordinate);
            }

            var sizes = new List<WidthHeightDepthValue>
                        {
                            new WidthHeightDepthValue(50, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(40, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(10, 10, 10),
                            new WidthHeightDepthValue(110, 10, 10)
                        };
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(sizes, Context.EntityId, 0).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] { PlacementUnitsType.SingleUnit });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);


            Int32[] expected = {3, 5, 1}; // Three products on the first shelf, five on the second and 1 on the third.
            Assert.IsTrue(TestPlanogram.GetPlanogramSubComponentPlacements().Select((p,i) => p.GetPlanogramPositions().Count() == expected[i]).All(b => b), "There should be the correct number of products on each one of the shelves");
        }

        [Test]
        public void ExecuteTask_WhenCompactingIncreasesRange_ShouldCompactSpace(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            SetTaskParameterValues(0, new Object[] { PlacementUnitsType.SingleUnit });
            SetTaskParameterValues(1, new Object[] { false});
            SetTaskParameterValues(2, new Object[] { productComparisonType });
            var bay = TestPlanogram.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var prod0 = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod3 = TestPlanogram.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod4 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
            TestPlanogram.CreateAssortmentFromProducts(TestPlanogram.Products);
            TestPlanogram.AddBlocking(new [] { new Tuple<Single, Single>(0, 0) });
            TestPlanogram.Sequence.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup(TestPlanogram.Blocking.First().Groups.First()));
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(new[] 
            {
                prod0, prod1, prod2, prod4, prod3
            });

            _task.ExecuteTask(Context);

            var actualShelf1Gtins = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { prod0.Gtin, prod1.Gtin, };
            WriteList("Expected on shelf 1", expectedShelf1Gtins);
            WriteList("Actual on shelf 1", actualShelf1Gtins);

            var actualShelf2Gtins = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .OrderByDescending(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { prod2.Gtin };
            WriteList("Expected on shelf 2", expectedShelf2Gtins);
            WriteList("Actual on shelf 2", actualShelf2Gtins);

            var actualShelf3Gtins = shelf3.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { prod4.Gtin, prod3.Gtin };
            WriteList("Expected on shelf 3", expectedShelf3Gtins);
            WriteList("Actual on shelf 3", actualShelf3Gtins);
            
            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        #endregion

        #region First On Block

        [Test]
        public void ExecuteTask_WhenWouldGetNoRepresentationDueToBlockSpace_ShouldPlaceOneProductPerPlacement(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            const String expectation = "A block too small to fit any products should still get at least one product place per placement.";
            PlanogramFixtureItem fixtureItem = TestPlanogram.AddFixtureItem();
            var shelfCoordinates = new List<PointValue>
                               {
                                   new PointValue(0, 0, 0),
                                   new PointValue(0, 20, 0),
                                   new PointValue(0, 40, 0),
                                   new PointValue(0, 80, 0)
                               };
            foreach (PointValue coordinate in shelfCoordinates)
            {
                fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, coordinate);
            }
            var firstBlockProductSizes = new List<WidthHeightDepthValue>
                                         {
                                             new WidthHeightDepthValue(35, 10, 10),
                                             new WidthHeightDepthValue(10, 10, 10),
                                             new WidthHeightDepthValue(22, 10, 10),
                                             new WidthHeightDepthValue(20, 10, 10)
                                         };
            var secondBlockProductSizes = new List<WidthHeightDepthValue>
                                          {
                                              new WidthHeightDepthValue(35, 10, 10),
                                              new WidthHeightDepthValue(35, 10, 10),
                                              new WidthHeightDepthValue(10, 10, 10),
                                              new WidthHeightDepthValue(10, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(20, 10, 10)
                                          };
            ICollection<ProductDto> firstBlockProductDtos = PlanogramTestHelper.CreateProductDtos(firstBlockProductSizes, Context.EntityId).ToList();
            ICollection<ProductDto> secondBlockProductDtos = PlanogramTestHelper.CreateProductDtos(secondBlockProductSizes, Context.EntityId, firstBlockProductDtos.Count + 1).ToList();
            List<ProductDto> productDtos = firstBlockProductDtos.Union(secondBlockProductDtos).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> {new Tuple<Single, Single>(0.25F, 0.0F)});
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(firstBlockProductDtos);
            TestPlanogram.Sequence.Groups.Last().AddSequenceProducts(secondBlockProductDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] { PlacementUnitsType.SingleUnit });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            IEnumerable<String> expectedGtins = firstBlockProductDtos.Select(dto => dto.Gtin).ToList();
            Boolean hasPlacementsOnAllComponents;
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
                hasPlacementsOnAllComponents = merchandisingGroups.All(g => g.SubComponentPlacements.Any(p => p.GetPlanogramPositions().Any(pp => expectedGtins.Contains(pp.GetPlanogramProduct().Gtin))));
            Assert.IsTrue(hasPlacementsOnAllComponents, expectation);
        }

        [Test]
        public void ExecuteTask_WhenWouldGetNoRepresentationDueToPhysicalConstraints_ShouldSkipIt(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            const String expectation = "A products does not fit physically and Ignore Merchandising Dimensions is off should not place but skip.";
            PlanogramFixtureItem fixtureItem = TestPlanogram.AddFixtureItem();
            var shelfCoordinates = new List<PointValue>
                               {
                                   new PointValue(0, 0, 0),
                                   new PointValue(0, 20, 0),
                                   new PointValue(0, 40, 0),
                                   new PointValue(0, 80, 0)
                               };
            foreach (PointValue coordinate in shelfCoordinates)
            {
                fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, coordinate);
            }
            var firstBlockProductSizes = new List<WidthHeightDepthValue>
                                         {
                                             new WidthHeightDepthValue(35, 30, 10),
                                             new WidthHeightDepthValue(10, 10, 10),
                                             new WidthHeightDepthValue(10, 10, 10),
                                             new WidthHeightDepthValue(22, 10, 10),
                                             new WidthHeightDepthValue(20, 10, 10)
                                         };
            var secondBlockProductSizes = new List<WidthHeightDepthValue>
                                          {
                                              new WidthHeightDepthValue(35, 10, 10),
                                              new WidthHeightDepthValue(35, 10, 10),
                                              new WidthHeightDepthValue(10, 10, 10),
                                              new WidthHeightDepthValue(10, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(20, 10, 10)
                                          };
            ICollection<ProductDto> firstBlockProductDtos = PlanogramTestHelper.CreateProductDtos(firstBlockProductSizes, Context.EntityId).ToList();
            ICollection<ProductDto> secondBlockProductDtos = PlanogramTestHelper.CreateProductDtos(secondBlockProductSizes, Context.EntityId, firstBlockProductDtos.Count + 1).ToList();
            List<ProductDto> productDtos = firstBlockProductDtos.Union(secondBlockProductDtos).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.25F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(firstBlockProductDtos);
            TestPlanogram.Sequence.Groups.Last().AddSequenceProducts(secondBlockProductDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] { PlacementUnitsType.SingleUnit });
            SetTaskParameterValues(1, new Object[] { IgnoreMerchandisingDimensionsType.Yes });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            IEnumerable<String> expectedGtins = firstBlockProductDtos.Select(dto => dto.Gtin).ToList();
            Boolean hasPlacementsOnAllComponents;
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
                hasPlacementsOnAllComponents = merchandisingGroups.All(g => g.SubComponentPlacements.Any(p => p.GetPlanogramPositions().Any(pp => expectedGtins.Contains(pp.GetPlanogramProduct().Gtin))));
            Assert.IsTrue(hasPlacementsOnAllComponents, expectation);
        }

        [Test]
        public void ExecuteTask_WhenWouldGetNoRepresentationDueToPhysicalConstraintsButIgnoreDimensions_ShouldPlaceOneProductPerPlacement(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            const String expectation = "A no products fit physically and Ignore Merchandising Dimensions is on should still get at least one product place per placement.";
            PlanogramFixtureItem fixtureItem = TestPlanogram.AddFixtureItem();
            var shelfCoordinates = new List<PointValue>
                               {
                                   new PointValue(0, 0, 0),
                                   new PointValue(0, 20, 0),
                                   new PointValue(0, 40, 0),
                                   new PointValue(0, 80, 0)
                               };
            foreach (PointValue coordinate in shelfCoordinates)
            {
                fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, coordinate);
            }
            var firstBlockProductSizes = new List<WidthHeightDepthValue>
                                         {
                                             new WidthHeightDepthValue(35, 30, 10),
                                             new WidthHeightDepthValue(10, 10, 10),
                                             new WidthHeightDepthValue(22, 10, 10),
                                             new WidthHeightDepthValue(20, 10, 10)
                                         };
            var secondBlockProductSizes = new List<WidthHeightDepthValue>
                                          {
                                              new WidthHeightDepthValue(35, 10, 10),
                                              new WidthHeightDepthValue(35, 10, 10),
                                              new WidthHeightDepthValue(10, 10, 10),
                                              new WidthHeightDepthValue(10, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(22, 10, 10),
                                              new WidthHeightDepthValue(20, 10, 10)
                                          };
            ICollection<ProductDto> firstBlockProductDtos = PlanogramTestHelper.CreateProductDtos(firstBlockProductSizes, Context.EntityId).ToList();
            ICollection<ProductDto> secondBlockProductDtos = PlanogramTestHelper.CreateProductDtos(secondBlockProductSizes, Context.EntityId, firstBlockProductDtos.Count + 1).ToList();
            List<ProductDto> productDtos = firstBlockProductDtos.Union(secondBlockProductDtos).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.25F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(firstBlockProductDtos);
            TestPlanogram.Sequence.Groups.Last().AddSequenceProducts(secondBlockProductDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            SetTaskParameterValues(0, new Object[] { PlacementUnitsType.SingleUnit });
            SetTaskParameterValues(1, new Object[] { IgnoreMerchandisingDimensions });
            SetTaskParameterValues(2, new Object[] { productComparisonType });

            _task.ExecuteTask(Context);

            IEnumerable<String> expectedGtins = firstBlockProductDtos.Select(dto => dto.Gtin).ToList();
            Boolean hasPlacementsOnAllComponents;
            using (PlanogramMerchandisingGroupList merchandisingGroups = TestPlanogram.GetMerchandisingGroups())
                hasPlacementsOnAllComponents = merchandisingGroups.All(g => g.SubComponentPlacements.Any(p => p.GetPlanogramPositions().Any(pp => expectedGtins.Contains(pp.GetPlanogramProduct().Gtin))));
            Assert.IsTrue(hasPlacementsOnAllComponents, expectation);
        }

        #endregion

        #region Placement Types

        [Test]
        public void ExecuteTask_PlacementOrderTypes(
                            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType,
                            [Values(PlacementOrderType.NotRangedInRankOrder, PlacementOrderType.InAssortmentRankOrder)] 
                                PlacementOrderType placementOrderType)
        {
            // Products on one shelf, left stacked, placed in sequence order.
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 100, 0));            
            for (Int16 i = 1; i <= 20; i++)
            {
                shelf1.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            MerchandisePlanogram();
            TestPlanogram.AddBlocking().AddGroup().BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            TestPlanogram.Blocking.First().Groups.First().AddLocation(0, 1, 0, 1);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());
            TestPlanogram.CreateAssortmentFromProducts();

            foreach (PlanogramAssortmentProduct planogramAssortmentProduct in TestPlanogram.Assortment.Products)
            {
                planogramAssortmentProduct.IsRanged = false;
            }

            SetTaskParameterValues(2, new Object[] { productComparisonType });
            SetTaskParameterValues(3, new Object[] { placementOrderType });

            _task.ExecuteTask(Context);

            List<String> gtinsOrderedBySequenceDirection =
                TestPlanogram.Positions.OrderBy(p => p.X).Select(p => p.GetPlanogramProduct().Gtin).ToList();
            Int32 seqProductIndex = 0;
            IOrderedEnumerable<PlanogramSequenceGroupProduct> sequenceProducts =
                TestPlanogram.Sequence.Groups.First().Products.OrderBy(g => g.SequenceNumber);
            Assert.AreEqual(sequenceProducts.Count(), gtinsOrderedBySequenceDirection.Count());
            foreach (PlanogramSequenceGroupProduct seqProduct in sequenceProducts)
            {
                Assert.AreEqual(seqProduct.Gtin, gtinsOrderedBySequenceDirection.ElementAt(seqProductIndex));
                seqProductIndex++;
            }
        }

        #endregion
        
        [Test]
        public void ExecuteTask_OneShelf_ValidateOverfilledMerchandisingGroup(
            [Values(ProductComparisonType.Rank, ProductComparisonType.AssortmentRules)] ProductComparisonType productComparisonType)
        {
            // create a plan with 4 products, each product will be assigned to a blocking group.

            const Int16 products = 4;
            PlanogramFixtureItem bay1 = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                        new PointValue(0, 50, 0));

            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            for (Int16 i = 1; i <= products; i++)
            {
                shelf1.AddPosition(bay1, TestPlanogram.AddProduct()).SequenceX = i;
            }
            MerchandisePlanogram();

            PlanogramBlocking blockingGroup = TestPlanogram.AddBlocking();
            blockingGroup.Dividers.Add(0.25F, 0F, PlanogramBlockingDividerType.Vertical);
            blockingGroup.Dividers.Add(0.5F, 0F, PlanogramBlockingDividerType.Vertical);
            blockingGroup.Dividers.Add(0.75F, 0F, PlanogramBlockingDividerType.Vertical);
            TestPlanogram.Sequence.ApplyFromBlocking(TestPlanogram.Blocking.First());

            foreach (PlanogramProduct p in TestPlanogram.Products)
            {
                p.Depth = 20;
            }

            TestPlanogram.CreateAssortmentFromProducts();

            foreach (PlanogramAssortmentProduct planogramAssortmentProduct in TestPlanogram.Assortment.Products)
            {
                planogramAssortmentProduct.Units = 999;
            }

            SetTaskParameterValues(1, new Object[] { true });
            SetTaskParameterValues(2, new Object[] { productComparisonType });
            _task.ExecuteTask(Context);

            using (PlanogramMerchandisingGroupList merchGroups = TestPlanogram.GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchGroups)
                {
                    Assert.IsFalse(merchandisingGroup.IsOverfilled(), "The merchandising group should not be overfilled.");
                }
            }
        }

        [Test]
        public void ExecuteTask_WhenProductSizeTooBigButSqueezedSizeFits_ShouldBePlaced()
        {
            var bay = TestPlanogram.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 40, 0));
            var blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0.1f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            bottomBlock.Name = "Bottom";
            var topBlock = blocking.Locations.First(l => !l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            topBlock.Name = "Top";
            var prod1 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
            var prod2 = TestPlanogram.AddProduct(new WidthHeightDepthValue(10, 50, 10));
            foreach (var p in TestPlanogram.Products) p.SqueezeHeight = 0.1f;
            TestPlanogram.AddSequenceGroup(bottomBlock, TestPlanogram.Products.Take(1));
            TestPlanogram.AddSequenceGroup(topBlock, TestPlanogram.Products.Skip(1));
            TestPlanogram.CreateAssortmentFromProducts();
            SetTaskParameterValues(1, new Object[] { false }); // Do not ignore merchandising dimensions.

            _task.ExecuteTask(Context);

            TestPlanogram.Positions.Should().HaveCount(2);
            shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .Should().ContainSingle(p => p.GetPlanogramProduct() == prod1, "product 1 should be squeezed onto shelf 1");
            shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .Should().ContainSingle(p => p.GetPlanogramProduct() == prod2, "product 2 should be placed onto shelf 2 anyway");
        }

        [Test]
        public void TaskKeepsOriginalPlacementMerchandisingStyleWhenExecuted()
        {
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();
            PlanogramFixtureComponent shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramBlocking blocking = TestPlanogram.AddBlocking();
            blocking.Dividers.Add(0, 0, PlanogramBlockingDividerType.Horizontal);
            PlanogramProduct product = TestPlanogram.AddProduct();
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            PlanogramPosition planogramPosition = shelf.AddPosition(bay, product);
            planogramPosition.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            PlanogramBlockingGroup topBlock = blocking.Locations.OrderBy(location => location.Y).Last().GetPlanogramBlockingGroup();
            topBlock.Name = "Top";
            TestPlanogram.AddSequenceGroup(topBlock, TestPlanogram.Products);
            TestPlanogram.CreateAssortmentFromProducts();
            TestPlanogram.UpdatePositionSequenceData();
            IEnumerable<Tuple<String, PlanogramPositionMerchandisingStyle>> expectedMerchStylesByGtin =
                TestPlanogram.Positions
                             .Select(position =>
                                     new Tuple<String, PlanogramPositionMerchandisingStyle>(position.GetPlanogramProduct().Gtin,
                                                                                            position.MerchandisingStyle))
                             .ToList();

            _task.ExecuteTask(Context);

            IEnumerable<Tuple<String, PlanogramPositionMerchandisingStyle>> actualMerchStylesByGtin =
                TestPlanogram.Positions
                             .Select(position =>
                                     new Tuple<String, PlanogramPositionMerchandisingStyle>(position.GetPlanogramProduct().Gtin,
                                                                                            position.MerchandisingStyle))
                             .ToList();
            actualMerchStylesByGtin
                .Should().BeEquivalentTo(expectedMerchStylesByGtin,
                                         because: "the original styles for each gtin should have been preserved when merchandising");
        }

        #region Test Helper Methods

        /// <summary>
        ///     Simple planogram to test placing correct units. 
        ///     It has one bay, one shelf, simple blocking strategy, 
        ///     product sequence and assortment, a 10x10x10 product.
        /// </summary>
        /// <returns></returns>
        private PlanogramFixtureComponent NewPlaceCorrectUnitsTestPlanogram()
        {
            //  One default Bay.
            PlanogramFixtureItem bay = TestPlanogram.AddFixtureItem();

            //  One default Shelf.
            PlanogramFixtureComponent shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            //  One simple 10x10x10 product.
            var sizes = new List<WidthHeightDepthValue>
                        {
                            new WidthHeightDepthValue(10, 10, 10)
                        };
            List<ProductDto> productDtos = PlanogramTestHelper.CreateProductDtos(sizes, Context.EntityId, 0).ToList();
            TestDataHelper.InsertProductDtos(DalFactory, productDtos);

            //  Basic Blocking Strategy, Product Sequence and Assortment.
            TestPlanogram.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            TestPlanogram.Sequence.Groups.First().AddSequenceProducts(productDtos);
            TestPlanogram.Assortment.AddAssortmentProducts(productDtos);
            foreach (ProductDto dto in productDtos)
            {
                TestPlanogram.AddProduct(dto);
            }
            return shelf;
        }

        private void WriteList(String caption, IEnumerable<String> items)
        {
            Debug.WriteLine(caption);
            foreach (String item in items)
            {
                Debug.WriteLine(item);
            }
        }

        private static List<IEnumerable<PlanogramPosition>> SequenceRows(
            PlanogramBlockingGroupPlacementType sequenceDirectionX,
            IEnumerable<IEnumerable<PlanogramPosition>> rowsOfPositions)
        {
            List<IEnumerable<PlanogramPosition>> sequencedRowsOfPositions = new List<IEnumerable<PlanogramPosition>>();
            Boolean reverseRow = false;
            foreach (IEnumerable<PlanogramPosition> row in rowsOfPositions)
            {
                IEnumerable<PlanogramPosition> orderedRow =
                    sequenceDirectionX.IsPositive()
                        ? row.OrderBy(p => p.SequenceX)
                        : row.OrderByDescending(p => p.SequenceX);
                if (reverseRow) orderedRow = orderedRow.Reverse();
                reverseRow = !reverseRow;
                sequencedRowsOfPositions.Add(orderedRow);
            }
            return sequencedRowsOfPositions;
        }

        #region Assertion Helpers

        /// <summary>
        ///     Helper to assert that the final number of units 
        ///     for the only product in the place correct units test planogram 
        ///     is correct.
        /// </summary>
        /// <param name="expected">Expected number of units.</param>
        private void AssertTotalUnitsEquals(int expected)
        {
            PlanogramPosition position = TestPlanogram.Positions.First();
            position.RecalculateUnits(position.GetPlanogramProduct(), position.GetPlanogramSubComponentPlacement());
            Int32 actual = position.TotalUnits;
            Assert.AreEqual(expected, actual);
        }

        private void AssertPositions(Dictionary<Int32, PointValue> coordinatesByPosition)
        {
            Debug.WriteLine("Positions added for the following products:");
            foreach (PlanogramPosition p in TestPlanogram.Positions)
            {
                Debug.WriteLine(p.GetPlanogramProduct().Gtin);
            }
            Assert.AreEqual(coordinatesByPosition.Count,
                            TestPlanogram.Positions.Count,
                            "Different number of positions added than were present originally");
            foreach (PlanogramPosition position in TestPlanogram.Positions)
            {
                PointValue coords = coordinatesByPosition[Convert.ToInt32(position.PlanogramProductId)];
                Assert.AreEqual(coords.X,
                                position.X,
                                String.Format("{0} X expected: {1}, actual: {2}",
                                              position.GetPlanogramProduct().Gtin,
                                              coords.X,
                                              position.X));
                Assert.AreEqual(coords.Y,
                                position.Y,
                                String.Format("{0} Y expected: {1}, actual: {2}",
                                              position.GetPlanogramProduct().Gtin,
                                              coords.Y,
                                              position.Y));
                Assert.AreEqual(coords.Z,
                                position.Z,
                                String.Format("{0} Z expected: {1}, actual: {2}",
                                              position.GetPlanogramProduct().Gtin,
                                              coords.Z,
                                              position.Z));
            }
        }

        private void AssertSequencedProducts(Int32 groupColour,
                                             IEnumerable<PlanogramPosition> sequencedPositions,
                                             String description = "")
        {
            IOrderedEnumerable<PlanogramSequenceGroupProduct> sequencedProducts =
                TestPlanogram.Sequence.Groups.First(g => g.Colour == groupColour)
                             .Products.OrderBy(p => p.SequenceNumber);
            WriteList(String.Format("{0} Expected:", description), sequencedProducts.Select(p => p.Gtin));
            WriteList(String.Format("{0} Actual:", description),
                      sequencedPositions.Select(p => p.GetPlanogramProduct().Gtin));
            Assert.AreEqual(sequencedProducts.Count(), sequencedPositions.Count());
            foreach (PlanogramSequenceGroupProduct seqProduct in sequencedProducts)
            {
                Assert.AreEqual(
                    seqProduct.Gtin,
                    sequencedPositions.ElementAt(seqProduct.SequenceNumber - 1).GetPlanogramProduct().Gtin);
            }
        }

        #endregion

        #endregion
    }

    internal static class HelperExtensions
    {
        public static PlanogramSubComponent GetSubComp(this PlanogramFixtureComponent fixComp)
        {
            return fixComp.GetPlanogramComponent().SubComponents.First();
        }

    }
}
