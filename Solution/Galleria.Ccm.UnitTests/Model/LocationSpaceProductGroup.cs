﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationSpaceProductGroup : TestBase
    {
        #region Test Helper Methods
        private void AssertDtoAndModelAreEqual(LocationSpaceProductGroupDto dto, Galleria.Ccm.Model.LocationSpaceProductGroup model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
            Assert.AreEqual(dto.LocationSpaceId, model.Parent.Id);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.LocationSpaceProductGroup model =
                Galleria.Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            Random random = new Random();

            // create a new model for rand values
            Galleria.Ccm.Model.LocationSpaceProductGroup valueModel =
                Galleria.Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();

            //Test Id
            Int32 testId = 1;
            valueModel.Id = testId;
            Assert.AreEqual(testId, valueModel.Id);

            //Test Product Group Id
            Int32 testProductGroupId = random.Next(1, 500);
            valueModel.ProductGroupId = testProductGroupId;
            Assert.AreEqual(testProductGroupId, valueModel.ProductGroupId);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationSpaceProductGroup()
        {
            TestCreateNew<Ccm.Model.LocationSpaceProductGroup>(
                /*Child*/true,
                new String[] { },
                new Object[] { true },
                new String[] { "Parent" });
        }

        #endregion

        #region Data Access

        #region Insert
        [Test]
        public void DataAccess_Insert()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Call save on the parent object
            model = model.Save();

            //Get product group we want to check has been inserted
            Ccm.Model.LocationSpaceProductGroup testModel = model.LocationSpaceProductGroups.First();

            //Fetch saved product group
            LocationSpaceProductGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);

        }

        #endregion

        #region Update

        [Test]
        public void DataAccess_Update()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Call save on the parent object
            model = model.Save();

            //Get product group we want to check has been inserted
            Ccm.Model.LocationSpaceProductGroup testModel = model.LocationSpaceProductGroups.First();

            //Call save on the parent object again
            model = model.Save();

            //Reselect the product group from the parent object
            testModel = model.LocationSpaceProductGroups.First();

            //Fetch saved product group
            LocationSpaceProductGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);
        }

        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Call save on the parent object
            model = model.Save();

            //Get product group we want to check has been inserted
            Ccm.Model.LocationSpaceProductGroup testModel = model.LocationSpaceProductGroups.First();
            Assert.IsNotNull(testModel, "Product group should have been inserted, and returned");

            //Fetch saved product group to ensure it exists in the database
            LocationSpaceProductGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);

            //Delete product group from parent object
            model.LocationSpaceProductGroups.Remove(testModel);

            //Call save on the parent object
            model = model.Save();

            //try to retrieve again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(testModel.Id);
                        });
                }
            }
        }
        #endregion

        #endregion
    }
}
