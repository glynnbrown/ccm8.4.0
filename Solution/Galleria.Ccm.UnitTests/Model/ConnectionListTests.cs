﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConnectionListTests : TestBase
    {
        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            ConnectionList model =  ConnectionList.NewConnectionList();

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewConnectionList()
        {
            ConnectionList model =
                ConnectionList.NewConnectionList();

            Assert.IsTrue(model.IsChild, " Model should be marked as a child");
            Assert.IsEmpty(model, "the model should be empty");

            //Call method to add new 
            model.AddNew();
        }

        [Test]
        public void Fetch()
        {
            throw new InconclusiveException("TODO");
            //ConnectionDto connectionDto = new ConnectionDto()
            //{
            //    DatabaseName = "TestDB",
            //    ServerName = "TestServer",
            //    PortNumber = 5,
            //    IsAutoConnect = false,
            //    IsCurrentConnection = false,
            //    Type = (Byte)ConnectionType.Database
            //};

            //UserSettings modelSettings =
            //    UserSettings.FetchUserSettings(true);

            //Connection modelConn =
            //    Connection.NewConnection(connectionDto);

            //modelSettings.Connections.Add(modelConn);
            //modelSettings = modelSettings.Save();

            //UserSettings dalSettings =
            //    UserSettings.FetchUserSettings(false);

            //Assert.IsNotEmpty(dalSettings.Connections, "settings should contain the added connection");

            //Connection dalConn =
            //    dalSettings.Connections[0];

            //AssertHelper.AssertModelObjectsAreEqual(connectionDto, dalConn);

        }

        [Test]
        public void GetCurrentConnection()
        {
            throw new InconclusiveException("TODO");
            //UserSettings modelSettings =
            //    UserSettings.FetchUserSettings(true);

            ////Add a non-current connection
            //ConnectionDto connectionDto = new ConnectionDto()
            //{
            //    DatabaseName = "TestDB",
            //    ServerName = "TestServer",
            //    PortNumber = 5,
            //    IsAutoConnect = false,
            //    IsCurrentConnection = false,
            //    Type = (Byte)ConnectionType.Database
            //};
            //Connection modelConn =
            //    Connection.NewConnection(connectionDto);
            //modelSettings.Connections.Add(modelConn);

            ////Add a current connection
            //ConnectionDto currentConnectionDto = new ConnectionDto()
            //{
            //    DatabaseName = "TestDB2",
            //    ServerName = "TestServer",
            //    PortNumber = 5,
            //    IsAutoConnect = false,
            //    IsCurrentConnection = true,
            //    Type = (Byte)ConnectionType.Database
            //};
            //modelConn = Connection.NewConnection(currentConnectionDto);
            //modelSettings.Connections.Add(modelConn);
            ////Save down
            //modelSettings = modelSettings.Save();

            ////Refetch the settings list
            //UserSettings dalSettings =
            //    UserSettings.FetchUserSettings(false);

            //Assert.IsNotEmpty(dalSettings.Connections, "settings should contain the added connection");

            //Connection dalConn = dalSettings.GetCurrentConnection();

            //AssertHelper.AssertModelObjectsAreEqual(currentConnectionDto, dalConn);
        }

        #endregion
    }
}
