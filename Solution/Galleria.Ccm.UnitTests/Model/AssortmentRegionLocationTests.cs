﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26704 : A.Kuszyk
//	Created.
// V8-27056 : J.Pickup
//		Reviewed.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class AssortmentRegionLocationTests : TestBase<AssortmentRegionLocation, AssortmentRegionLocationDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataPortalFetch_GetsLocationCode()
        {
            var entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;
            var locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, 1, entityId);
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId).First().Id;
            var levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var productGroupId = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelDtos, 1).First().Id;
            var assortmentId = TestDataHelper.InsertAssortmentDtos(base.DalFactory, 1, entityId, productGroupId).First().Id;
            var regionId = TestDataHelper.InsertAssortmentRegionDtos(DalFactory, assortmentId, 1).First().Id;
            TestDataHelper.InsertAssortmentRegionLocationDtos(DalFactory, regionId, locationDtos.Select(o => new Tuple<Int16, String>(o.Id, o.Code)));

            var model = AssortmentRegionLocationList.FetchByRegionId(regionId).First();

            Assert.AreEqual(locationDtos.First().Code, model.LocationCode);
        }

        #endregion
    }
}
