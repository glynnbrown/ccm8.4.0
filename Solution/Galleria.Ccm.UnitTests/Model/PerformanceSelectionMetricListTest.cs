﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PerformanceSelectionMetricListTest : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            PerformanceSelectionMetricList model = PerformanceSelectionMetricList.NewPerformanceSelectionMetricList();
            Serialize(model);
        }
        #endregion
    }
}
