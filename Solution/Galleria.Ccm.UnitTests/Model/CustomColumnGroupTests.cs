﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnGroupTests : TestBase
    {
        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumnGroup testModel = CustomColumnGroup.NewCustomColumnGroup(null);

            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<CustomColumnGroup>(CustomColumnGroup.NewCustomColumnGroup(null));
        }

        [Test]
        public void NewCustomColumnGroupt()
        {
            TestDelegate code = () => CustomColumnGroup.NewCustomColumnGroup(null);

            Assert.DoesNotThrow(code);
        }

        #endregion
    }
}
