﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-30738 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PrintTemplateComponentDetailTests : TestBase<PrintTemplateComponentDetail, PrintTemplateComponentDetailDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out PrintTemplateComponentDetail model)
        {
            PrintTemplate template = PrintTemplate.NewPrintTemplate();
            template.Name = Guid.NewGuid().ToString();

            PrintTemplateSectionGroup group = template.SectionGroups.AddNewSectionGroup(1);
            group.Name = Guid.NewGuid().ToString();

            PrintTemplateSection section = group.Sections.AddNewSection();

            PrintTemplateComponent component = section.Components.AddNewComponent(PrintTemplateComponentType.DataSheet);

            root = template;
            model = component.ComponentDetails;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Assert.Ignore("TODO");
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            Assert.Ignore("TODO");
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            Assert.Ignore("TODO");
            TestNewFactoryMethod();
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            Assert.Ignore("TODO");
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            Assert.Ignore("TODO");
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<PrintTemplate>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            Assert.Ignore("TODO");
            TestInsert<PrintTemplate>();
        }

        [Test]
        public void Update()
        {
            Assert.Ignore("TODO");
            TestUpdate<PrintTemplate>();
        }

        [Test]
        public void Delete()
        {
            Assert.Ignore("TODO");
            TestDelete<PrintTemplate>();
        }

        #endregion
    }
}
