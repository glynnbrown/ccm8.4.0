﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM 8.0)
// V8-25669 : A.Kuszyk
//  Created (adapted from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class MetricProfileInfoListTests : TestBase
    {
        private List<MetricProfileDto> InsertTestDtos()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<MetricDto> metricDtoList = TestDataHelper.InsertMetricDtos(base.DalFactory, 50, entityList[0].Id);
            List<MetricProfileDto> metricProfileDtoList = TestDataHelper.InsertMetricProfileDtos(base.DalFactory, 50, entityList[0].Id, metricDtoList);

            return metricProfileDtoList;
        }

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<MetricProfileDto> ent1MetricProfileDtoList = InsertTestDtos();

            var model = MetricProfileInfoList.FetchByEntityId(ent1MetricProfileDtoList[0].EntityId);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods



        #endregion
    }
}
