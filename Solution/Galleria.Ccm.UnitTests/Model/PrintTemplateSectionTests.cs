﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-30738 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PrintTemplateSectionTests : TestBase<PrintTemplateSection, PrintTemplateSectionDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out PrintTemplateSection model)
        {
            PrintTemplate template = PrintTemplate.NewPrintTemplate();
            template.Name = Guid.NewGuid().ToString();

            PrintTemplateSectionGroup group = template.SectionGroups.AddNewSectionGroup(1);
            group.Name = Guid.NewGuid().ToString();

            PrintTemplateSection section = group.Sections.AddNewSection();

            root = template;
            model = section;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<PrintTemplate>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<PrintTemplate>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<PrintTemplate>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<PrintTemplate>();
        }

        #endregion
    }
}
