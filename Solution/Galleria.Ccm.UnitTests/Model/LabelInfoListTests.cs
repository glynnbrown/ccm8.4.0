﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class LabelInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Label model1 = Label.NewLabel(LabelType.Product);
            PopulatePropertyValues1(model1, new List<String> { "Id" });

            model1 = model1.Save();
            model1.Dispose();

            Label model2 = Label.NewLabel(LabelType.Product);
            PopulatePropertyValues2(model2, new List<String> { "Id" });

            model2 = model1.Save();
            model2.Dispose();

            IDalFactory dalFactory = DalContainer.GetDalFactory();

            LabelInfoList infoList = LabelInfoList.FetchByIds(new List<Object>() { (Int16)1, (Int16)2 });

            Serialize(infoList);
        }
        #endregion


        #region FactoryMethods

        [Test]
        public void FetchLabelInfo()
        {
            Label model1 = Label.NewLabel(LabelType.Product);
            PopulatePropertyValues1(model1, new List<String> { "Id" });

            model1 = model1.Save();
            model1.Dispose();

            Label model2 = Label.NewLabel(LabelType.Product);
            PopulatePropertyValues2(model2, new List<String> { "Id" });

            model2 = model2.Save();
            model2.Dispose();

            LabelInfo modelInfo;
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            LabelInfoList infoList = LabelInfoList.FetchByIds(new List<Object>() { model1.Id, model2.Id });
            modelInfo = infoList[0];

            //Check properties
            Assert.AreEqual(model1.Id, modelInfo.Id);
            Assert.AreEqual(model1.Name, modelInfo.Name);

            modelInfo = infoList[1];
            Assert.AreEqual(model2.Id, modelInfo.Id);
            Assert.AreEqual(model2.Name, modelInfo.Name);
        }

        #endregion

    }
}
