﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System.Collections;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class RenumberingStrategyInfoListTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id;
            TestDataHelper.InsertRenumberingStrategyDtos(DalFactory, 20, entityId);
            var model = RenumberingStrategyInfoList.FetchByEntityId(entityId);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }

        #endregion

        #region FactoryMethods

        [Test]
        public void FetchByEntityId()
        {
            var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id;
            var expected = TestDataHelper.InsertRenumberingStrategyDtos(DalFactory, 20, entityId).ToList();
            var actual = RenumberingStrategyInfoList.FetchByEntityId(entityId);

            Assert.IsTrue(actual.All(info => expected.Any(dto => dto.Id == info.Id)));
        }

        #endregion
    }
}