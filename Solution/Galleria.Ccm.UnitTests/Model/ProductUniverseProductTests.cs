﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

using NUnit.Framework;

using Csla;

using Galleria.Framework.Dal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;

using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductUniverseProduct : TestBase
    {
        #region Helpers

        public static void AssertDtoAndModelAreEqual(ProductUniverseProductDto dto,
            Galleria.Ccm.Model.ProductUniverseProduct model)
        {
            Assert.AreEqual(6, typeof(ProductUniverseProductDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.ProductId, model.ProductId);
            Assert.AreEqual(dto.Gtin, model.Gtin);
            Assert.AreEqual(dto.Name, model.Name);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Ensures that the model object is serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Insert some test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductDto> productDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 1, entityDtoList[0].Id);

            //Fetch product model object
            Galleria.Ccm.Model.Product product = Galleria.Ccm.Model.Product.FetchById(productDtoList[0].Id);

            Galleria.Ccm.Model.ProductUniverseProduct model = Galleria.Ccm.Model.ProductUniverseProduct.NewProductUniverseProduct(product);

            Serialize(model);
        }
        #endregion

        #region Property Setters
        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.Product product =
                Galleria.Ccm.Model.Product.NewProduct();

            // create a new model
            Galleria.Ccm.Model.ProductUniverseProduct model =
                Galleria.Ccm.Model.ProductUniverseProduct.NewProductUniverseProduct(product);

            TestPropertySetters<Galleria.Ccm.Model.ProductUniverseProduct>(model);
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Test NewProductUniverseProduct From Product
        /// </summary>
        [Test]
        public void NewProductUniverseProductFromProduct()
        {
            //Insert some test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductDto> productDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 1, entityDtoList[0].Id);

            //Fetch product model object
            Galleria.Ccm.Model.Product product = Galleria.Ccm.Model.Product.FetchById(1);//productDtoList[0].Id);

            Galleria.Ccm.Model.ProductUniverseProduct model = Galleria.Ccm.Model.ProductUniverseProduct.NewProductUniverseProduct(product);

            //Check they match up
            Assert.AreEqual(product.Id, model.ProductId);
            Assert.AreEqual(product.Gtin, model.Gtin);
            Assert.AreEqual(product.Name, model.Name);
        }

        /// <summary>
        /// Test NewProductUniverseProduct From ProductInfo
        /// </summary>
        [Test]
        public void NewProductUniverseProductFromProductInfo()
        {
            //Insert some test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductDto> productDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 5, entityDtoList[0].Id);

            //Setup model object to hold data
            Galleria.Ccm.Model.ProductInfo productInfo;

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    //Fetch product info dtos
                    List<ProductInfoDto> productInfoDtoList = dal.FetchByProductIds(productDtoList.Select(d=>d.Id)).ToList();

                    //Fetch product info model object
                    productInfo = Galleria.Ccm.Model.ProductInfo.GetProductInfo(dalContext, productInfoDtoList[0]);
                }
            }

            //Call factory method
            Galleria.Ccm.Model.ProductUniverseProduct model = Galleria.Ccm.Model.ProductUniverseProduct.NewProductUniverseProduct(productInfo);

            //Check they match up
            Assert.AreEqual(productInfo.Id, model.ProductId);
            Assert.AreEqual(productInfo.Gtin, model.Gtin);
            Assert.AreEqual(productInfo.Name, model.Name);
        }

        /// <summary>
        /// Test FetchProductUniverseProduct
        /// </summary>
        [Test]
        public void FetchProductUniverseProduct()
        {
            //Insert some test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductDto> productDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 25, entityDtoList[0].Id);
            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtoList[0].Id);
            List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDtoList[0].Id, 4);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelDtoList, 10);
            List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, productGroupDtoList);
            List<ProductUniverseProductDto> productUniverseProductDtoList = TestDataHelper.InsertProductUniverseProductDtos(base.DalFactory, productUniverseDtoList, productDtoList);

            //Select 1 product universe product for use in test
            ProductUniverseProductDto productUniverseProductDto = productUniverseProductDtoList[0];


            Galleria.Ccm.Model.ProductUniverseProduct model =
                        Galleria.Ccm.Model.ProductUniverseProduct.FetchProductUniverseProduct(base.DalFactory.CreateContext(), productUniverseProductDto);

            AssertDtoAndModelAreEqual(productUniverseProductDto, model);
        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_ChildInsertUpdateDelete()
        {
            //insert entities and locations
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            ProductDto prodDto = TestDataHelper.InsertProductDtos(base.DalFactory, 1, entityId)[0];

            Galleria.Ccm.Model.ProductUniverse parentUniverse =
                Galleria.Ccm.Model.ProductUniverse.NewProductUniverse(entityId);
            parentUniverse.Name = "uni1";

            //create a new item
            Galleria.Ccm.Model.Product product = Galleria.Ccm.Model.Product.FetchById(prodDto.Id);
            Galleria.Ccm.Model.ProductUniverseProduct item =
                Galleria.Ccm.Model.ProductUniverseProduct.NewProductUniverseProduct(product);
            parentUniverse.Products.Add(item);

            //insert
            parentUniverse = parentUniverse.Save();
            item = parentUniverse.Products[0];

            ProductUniverseProductDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductUniverseProductDal dal = dalContext.GetDal<IProductUniverseProductDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            // no updatable properties

            //remove from the parent
            Int32 itemId = item.Id;
            parentUniverse.Products.Remove(item);
            parentUniverse = parentUniverse.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductUniverseProductDal dal = dalContext.GetDal<IProductUniverseProductDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }

        }


        #endregion
    }
}
