﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25669 : A.Kuszyk
//  Created (adapted from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductInfoTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(ProductDto dto, ProductInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Gtin, model.Gtin);
            Assert.AreEqual(dto.Name, model.Name);
        }

        private void AssertModelandModelAreEqual(ProductInfo model1,ProductInfo model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.EntityId, model2.EntityId);
            Assert.AreEqual(model1.Gtin, model2.Gtin);
            Assert.AreEqual(model1.Name, model2.Name);
        }

        private List<ProductDto> InsertTestDtos()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);

            return ent1ProductDtoList;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            var dtos = InsertTestDtos();
            ProductInfoList infoList = ProductInfoList.FetchByProductIds(dtos.Select(d=>d.Id));

            ProductInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchProductInfo()
        {
            List<ProductDto> dtoList = InsertTestDtos();

            ProductInfoList infoList = ProductInfoList.FetchByProductIds(dtoList.Select(d=>d.Id));
            foreach (var prodDto in infoList)
            {
                ProductInfo dalLocInfo =
                    infoList.First(l => l.Id == prodDto.Id);
                AssertDtoAndModelAreEqual(prodDto, dalLocInfo);
            }
        }

        #endregion
    }
}
