﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (adapted from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductUniverseInfo : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(
            ProductUniverseDto dto,
            Galleria.Ccm.Model.ProductUniverseInfo model)
        {
            Assert.AreEqual(11, typeof(ProductUniverseInfoDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.UniqueContentReference, model.UniqueContentReference);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
            Assert.AreEqual(dto.IsMaster, model.IsMaster);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.DateCreated, model.DateCreated);
            Assert.AreEqual(dto.DateLastModified, model.DateLastModified);

            //productgroupcode - from productgroup
            //productgroupname - from productgroup
        }
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtoList[0].Id);
            List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDtoList[0].Id, 4);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelDtoList, 10);
            List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, productGroupDtoList);

            Galleria.Ccm.Model.ProductUniverseInfoList infoList = Galleria.Ccm.Model.ProductUniverseInfoList.FetchByEntityId(entityDtoList[0].Id);

            Galleria.Ccm.Model.ProductUniverseInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchProductUniverseInfo()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtoList[0].Id);
            List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDtoList[0].Id, 4);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelDtoList, 10);
            List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, productGroupDtoList);

            Galleria.Ccm.Model.ProductUniverseInfoList infoList = Galleria.Ccm.Model.ProductUniverseInfoList.FetchByEntityId(entityDtoList[0].Id);

            foreach (ProductUniverseDto dto in productUniverseDtoList)
            {
                Galleria.Ccm.Model.ProductUniverseInfo dalInfo =
                    infoList.First(l => l.Id == dto.Id);
                AssertDtoAndModelAreEqual(dto, dalInfo);
            }
        }

        #endregion
    }
}
