﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductUniverseTests : TestBase<ProductUniverse, ProductUniverseDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out ProductUniverse model)
        {
            ProductUniverse blocking = ProductUniverse.NewProductUniverse(1);
            blocking.Name = Guid.NewGuid().ToString();

            root = blocking;
            model = blocking;
        }


        public static void AssertDtoAndModelAreEqual(ProductUniverseDto dto, Galleria.Ccm.Model.ProductUniverse model)
        {
            Assert.AreEqual(11, typeof(ProductUniverseDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            //dtokey - not in model.
            Assert.AreEqual(dto.UniqueContentReference, model.UniqueContentReference);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
            Assert.AreEqual(dto.IsMaster, model.IsMaster);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            //DateCreated - not in model
            //DateLastModified - not in model
            //DateDeleted - not in model.
        }


        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_EntityId()
        {
            TestNewFactoryMethod(new String[] { "EntityId" });
        }


        //[Test]
        //public void NewFactoryMethod_EntityIdProductGroupId()
        //{
        //    TestNewFactoryMethod(new String[] { "EntityId", "ProductGroupId" });
        //}
       

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void FetchById()
        {
            TestFetch<ProductUniverse>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void FetchByEntityIdName()
        {
            TestFetch<ProductUniverse>("FetchByEntityIdName", new String[] { "EntityId", "Name" });
        }


        [Test]
        public void FetchProductUniverse()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<ProductLevelDto> levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 2);
            List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelDtos, 1);

            List<ProductUniverseDto> universeDtos = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtos, groupDtos);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (ProductUniverseDto dto in universeDtos)
                {
                    var model = Ccm.Model.ProductUniverse.FetchProductUniverse(dalContext, dto);
                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }

        [Test]
        public void Insert()
        {
            TestInsert<ProductUniverse>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<ProductUniverse>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<ProductUniverse>();
        }

        #endregion

    }
}
