﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25587 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramGroupListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            PlanogramGroupList model = PlanogramGroupList.NewPlanogramGroupList();
            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewPlanogramGroupList()
        {
            PlanogramGroupList model = PlanogramGroupList.NewPlanogramGroupList();

            Assert.IsTrue(model.IsChild, "the model should be marked a child");
            Assert.IsEmpty(model, "the model should be empty");
        }

        [Test]
        public void FetchByParentPlanogramGroupId()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<PlanogramHierarchyDto> hierarchyDtos = PlanogramHierarchyTests.InsertPlanogramHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<PlanogramGroupDto> groupDtos = PlanogramHierarchyTests.InsertPlanogramGroupDtos(base.DalFactory, hierarchyDtos[0].Id, 5, 4);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (PlanogramGroupDto dto in groupDtos)
                {
                    IEnumerable<PlanogramGroupDto> expectedChildren = groupDtos.Where(g => g.ParentGroupId == dto.Id);
                    var model = PlanogramGroupList.FetchByParentPlanogramGroupId(dalContext, dto.Id, groupDtos);

                    Assert.AreEqual(expectedChildren.Count(), model.Count);

                    foreach (PlanogramGroupDto childDto in expectedChildren)
                    {
                        var item = model.First(m => m.Id == childDto.Id);
                        PlanogramGroupTests.AssertDtoAndModelAreEqual(childDto, item);
                    }

                }

            }

        }

        #endregion
    }
}
