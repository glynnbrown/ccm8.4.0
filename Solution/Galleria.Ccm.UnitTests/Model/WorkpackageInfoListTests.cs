﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-25757 : L.Ineson
//		Created
#endregion
#endregion


using System.Linq;
using Galleria.Ccm.Model;
using NUnit.Framework;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class WorkpackageInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            WorkpackageInfoList model = WorkpackageInfoList.FetchByEntityId(1);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            
           //insert a workflow
            TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");
            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();

            Entity entity1 = Entity.NewEntity();
            entity1.Name = "e1";
            entity1 = entity1.Save();

            var flow1 = Galleria.Ccm.Model.Workflow.NewWorkflow(entity1.Id);
            flow1.Name = "Workflow 1";
            flow1.Tasks.Add(engineTasks.First());
            flow1.Tasks.Add(engineTasks.First());
            flow1 = flow1.Save();

            Workpackage package = Workpackage.NewWorkpackage(entity1.Id);
            package.Name = "package1";
            package.WorkflowId = flow1.Id;
            package = package.Save();


            Entity entity2 = Entity.NewEntity();
            entity2.Name = "e2";
            entity2 = entity2.Save();

            var flow2 = Galleria.Ccm.Model.Workflow.NewWorkflow(entity2.Id);
            flow2.Name = "Workflow 2";
            flow2.Tasks.Add(engineTasks.First());
            flow2.Tasks.Add(engineTasks.First());
            flow2 = flow2.Save();

            Workpackage package2 = Workpackage.NewWorkpackage(entity2.Id);
            package2.Name = "package2";
            package2.WorkflowId = flow2.Id;
            package2 = package2.Save();


            WorkpackageInfoList list= WorkpackageInfoList.FetchByEntityId(entity1.Id);
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual("package1", list[0].Name);

            list = WorkpackageInfoList.FetchByEntityId(entity2.Id);
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual("package2", list[0].Name);
        }

        #endregion
    }
}
