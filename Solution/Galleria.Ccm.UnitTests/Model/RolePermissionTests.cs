﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26124 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class RolePermissionTests : TestBase
    {
        #region Test Helper Methods

        private void AssertDtoAndModelAreEqual(RolePermissionDto dto, RolePermission model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.PermissionType, model.PermissionType);
        }

        private void AssertModelAndModelAreEqual(RolePermission model1, RolePermission model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.PermissionType, model2.PermissionType);
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            RolePermission model =
                RolePermission.NewRolePermission(0);
            Serialize(model);
        }

        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            RolePermission model = RolePermission.NewRolePermission(0);
            TestBase.TestPropertySetters<RolePermission>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewRoleEntity()
        {
            RolePermission newRolePermission =
                RolePermission.NewRolePermission(456);

            Assert.AreEqual(456, newRolePermission.PermissionType);

            Assert.IsTrue(newRolePermission.IsNew);
        }

        #endregion

        #region Data Access

        #region Insert

        [Test]
        public void DataAccess_Insert()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;

            //Create parent Role
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 1, entityId, false);

            //Create the RoleEntity
            RolePermission rolePermission =
                RolePermission.NewRolePermission(1);
            Role role = Role.FetchById(roleList[0].Id);
            role.Permissions.Add(rolePermission);

            //save
            role = role.Save();
            rolePermission = role.Permissions[0];

            //Fetch the dto back
            RolePermissionDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRolePermissionDal dal = dalContext.GetDal<IRolePermissionDal>())
                {
                    dto = dal.FetchById(rolePermission.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, rolePermission);
        }

        #endregion

        #region Update

        [Test]
        public void DataAccess_Update()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            //Create parent Role
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 2, entityList[0].Id, false);

            //Create the RoleEntity
            RolePermission rolePermission =
                RolePermission.NewRolePermission(1);

            Role role = Role.FetchById(roleList[0].Id);
            role.Permissions.Add(rolePermission);

            //save
            role = role.Save();
            rolePermission = role.Permissions[0];

            //Update
            rolePermission = RolePermission.NewRolePermission(2);

            //save
            role = role.Save();
            rolePermission = role.Permissions[0];

            //Fetch the dto back
            RolePermissionDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRolePermissionDal dal = dalContext.GetDal<IRolePermissionDal>())
                {
                    dto = dal.FetchById(rolePermission.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, rolePermission);
        }

        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;

            //Create parent Role
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 1, entityId, false);

            //Create the RoleEntity
            RolePermission rolePermission =
                RolePermission.NewRolePermission(1);
            Role role = Role.FetchById(roleList[0].Id);
            role.Permissions.Add(rolePermission);

            //save
            role = role.Save();
            rolePermission = role.Permissions[0];

            //Fetch the dto back
            RolePermissionDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRolePermissionDal dal = dalContext.GetDal<IRolePermissionDal>())
                {
                    dto = dal.FetchById(rolePermission.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, rolePermission);

            //Delete the item
            Int32 fetchId = rolePermission.Id;
            role.Permissions.Remove(rolePermission);
            role.Save();

            //Try to fetch the item again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRolePermissionDal dal = dalContext.GetDal<IRolePermissionDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(fetchId);
                        });
                }
            }
        }

        #endregion

        #endregion
    }
}
