﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
// V8-27710 : A.Kuszyk
//  Added HasIdValueAfterCreation test.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security;
using Csla.Core;
using Csla.Rules;
using Galleria.Framework.Dal;
using NUnit.Framework;
using System.Collections;
using System.Diagnostics;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CodingStandards : TestBase
    {
        #region Properties
        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<Type> TestTypes
        {
            get
            {
                // a list of types to exlude from this test
                // defined as a string as we need to be able
                // to exlude some private command classed as well
                List<String> exclusions = new List<String>()
                {
                    "ColumnLayoutSetting",
                    "ColumnLayoutSettingList",
                    "Connection",
                    "ConnectionList",
                    "CustomColumn",
                    "CustomColumnFilter",
                    "CustomColumnFilterList",
                    "CustomColumnGroup",
                    "CustomColumnGroupList",
                    "CustomColumnLayout",
                    "CustomColumnLayoutInfo",
                    "CustomColumnLayoutInfoList",
                    "CustomColumnList",
                    "CustomColumnSort",
                    "CustomColumnSortList",
                    "Fixture",
                    "FixtureAssembly",
                    "FixtureAssemblyList",
                    "FixtureAssemblyComponent",
                    "FixtureAssemblyComponentList",
                    "FixtureAssemblyItem",
                    "FixtureAssemblyItemList",
                    "FixtureComponent",
                    "FixtureComponentItem",
                    "FixtureComponentItemList",
                    "FixtureComponentList",
                    "FixtureItem",
                    "FixtureItemList",
                    "FixtureImage",
                    "FixtureImageList",
                    "FixtureList",
                    "FixturePackage",
                    "FixturePackageInfo",
                    "FixturePackageInfoList",
                    "FixtureSubComponent",
                    "FixtureSubComponentList",
                    "Folder",
                    "FolderList",
                    "Highlight",
                    "HighlightCharacteristic",
                    "HighlightCharacteristicList",
                    "HighlightCharacteristicRule",
                    "HighlightCharacteristicRuleList",
                    "HighlightFilter",
                    "HighlightFilterList",
                    "HighlightGroup",
                    "HighlightGroupList",
                    "HighlightInfo",
                    "HighlightInfoList",
                    "Label",
                    "LabelInfo",
                    "LabelInfoList",
                    "LockCustomColumnLayoutCommand",
                    "LockFixturePackageCommand",
                    "LockHighlightCommand",
                    "LockLabelCommand",
                    "LockPlanogramComparisonTemplateCommand",
                    "LockPlanogramExportTemplateCommand",
                    "LockPlanogramImportTemplateCommand",
                    "LockPrintTemplateCommand",
                    "LockProductLibraryCommand",
                    "LockValidationTemplateCommand",
                    "PlanogramComparisonTemplate",
                    "PlanogramComparisonTemplateField",
                    "PlanogramComparisonTemplateFieldList",
                    "PlanogramComparisonTemplateInfo",
                    "PlanogramComparisonTemplateInfoList",
                    "PlanogramExportTemplate",
                    "PlanogramExportTemplateFieldInfo",
                    "PlanogramExportTemplateFieldInfoList",
                    "PlanogramExportTemplateInfo",
                    "PlanogramExportTemplateInfoList",
                    "PlanogramExportTemplateMapping",
                    "PlanogramExportTemplateMappingList",
                    "PlanogramExportTemplatePerformanceMetric",
                    "PlanogramExportTemplatePerformanceMetricList",
                    "PlanogramImportTemplate",
                    "PlanogramImportTemplateFieldInfo",
                    "PlanogramImportTemplateFieldInfoList",
                    "PlanogramImportTemplateInfo",
                    "PlanogramImportTemplateInfoList",
                    "PlanogramImportTemplateMapping",
                    "PlanogramImportTemplateMappingList",
                    "PlanogramImportTemplatePerformanceMetric",
                    "PlanogramImportTemplatePerformanceMetricList",
                    "PrintTemplate",
                    "PrintTemplateComponent",
                    "PrintTemplateComponentDetail",
                    "PrintTemplateComponentList",
                    "PrintTemplateInfo",
                    "PrintTemplateInfoList",
                    "PrintTemplateSection",
                    "PrintTemplateSectionGroup",
                    "PrintTemplateSectionGroupList",
                    "PrintTemplateSectionList",
                    "Product",
                    "ProductLibrary",
                    "ProductLibraryColumnMapping",
                    "ProductLibraryColumnMappingList",
                    "ProductLibraryInfo",
                    "ProductLibraryInfoList",
                    "ProductLibraryProduct",
                    "ProductLibraryProductPerformanceData",
                    "RecentPlanogram",
                    "RecentPlanogramList",
                    "SetFolderIdCommand",
                    "SyncSource",
                    "SyncSourceList",
                    "SyncStatus",
                    "SyncTarget",
                    "SyncTargetGfsPerformance",
                    "SyncTargetGfsPerformanceList",
                    "SyncTargetList",
                    "UnlockByIdCommand",
                    "UnlockCustomColumnLayoutCommand",
                    "UnlockFixturePackageCommand",
                    "UnlockHighlightCommand",
                    "UnlockLabelCommand",
                    "UnlockPlanogramComparisonTemplateCommand",
                    "UnlockPlanogramExportTemplateCommand",
                    "UnlockPlanogramImportTemplateCommand",
                    "UnlockPrintTemplateCommand",
                    "UnlockValidationTemplateCommand",
                    "UpdatePlanogramGroupCommand",
                    "UserEditorSettings",
                    "UserEditorSettingsColor",
                    "UserEditorSettingsColorList",
                    "UserEditorSettingsRecentDatasheet",
                    "UserEditorSettingsRecentDatasheetList",
                    "UserEditorSettingsRecentHighlight",
                    "UserEditorSettingsRecentHighlightList",
                    "UserEditorSettingsRecentLabel",
                    "UserEditorSettingsRecentLabelList",
                    "UserEditorSettingsSelectedColumn",
                    "UserEditorSettingsSelectedColumnList",
                    "UserSystemSetting",
                    "ValidationTemplate",
                    "ValidationTemplateList"
                };

                List<Type> testTypes = new List<Type>();
                foreach (Type testType in Assembly.GetAssembly(typeof(Galleria.Ccm.Constants)).GetTypes())
                {
                    if (testType.Namespace != null)
                    {
                        if ((testType.Namespace.Contains("Galleria.Ccm.Model")) && (!testType.IsAbstract))
                        {
                            if ((testType.GetInterfaces().Contains(typeof(ISavable))) ||
                                (testType.GetInterfaces().Contains(typeof(ICommandObject))) ||
                                (testType.GetInterfaces().Contains(typeof(IReadOnlyObject))) ||
                                (testType.GetInterfaces().Contains(typeof(IReadOnlyCollection))))
                            {
                                if (!exclusions.Contains(testType.Name))
                                {
                                    testTypes.Add(testType);
                                }
                            }
                        }
                    }
                }

                testTypes.Sort(delegate(Type x, Type y)
                {
                    return x.Name.CompareTo(y.Name);
                });

                return testTypes;
            }
        }

        public IEnumerable<Type> ModelTypes
        {
            get
            {
                List<Type> modelTypes = new List<Type>();

                foreach (Type modelType in Assembly.GetAssembly(typeof(Galleria.Ccm.Constants)).GetTypes())
                {
                    if (modelType.Namespace != null)
                    {
                        if ((modelType.Namespace.Contains("Galleria.Ccm.Model")) && (!modelType.IsAbstract))
                        {
                            if ((modelType.GetInterfaces().Contains(typeof(ISavable))) ||
                                (modelType.GetInterfaces().Contains(typeof(IReadOnlyObject))))
                            {
                                if ((!modelType.GetInterfaces().Contains(typeof(IEnumerable))) &&
                                    (modelType.GetMethods().Where(m => m.Name == String.Format("New{0}", modelType.Name)).Where(m => m.GetParameters().Count() == 0).Count() > 0))
                                {
                                    modelTypes.Add(modelType);
                                }
                            }
                        }
                    }
                }

                modelTypes.Sort(delegate(Type x, Type y)
                {
                    return x.Name.CompareTo(y.Name);
                });

                return modelTypes;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public CodingStandards()
        {

        }
        #endregion

        #region Methods

        [Test]
        [TestCaseSource("ModelTypes")]
        [Category(Categories.Smoke)]
        public void HasIdValueAfterCreation(Type modelType)
        {
            var factoryMethods = modelType.GetMethods().Where(m => m.Name == String.Format("New{0}", modelType.Name));
            var parameterlessFactoryMethods = factoryMethods.Where(m => m.GetParameters().Count() == 0);
            if (parameterlessFactoryMethods.Count() == 0)
            {
                Assert.Inconclusive("No parameterless constructor for this type");
            }

            var model = parameterlessFactoryMethods.First().Invoke(null, null);
            var idProperty = model.GetType().GetProperties().FirstOrDefault(p=>p.Name=="Id");
            if (idProperty == null)
            {
                Assert.Inconclusive("No Id property on model");
            }

            var idValue = idProperty.GetValue(model, null) as Int32?;
            if (idValue == null)
            {
                Assert.Inconclusive("Id was not of type Int32");

            }
            Assert.That(idValue != 0, String.Format("Id property should not be zero, but was {0}", idValue));
        }

        /// <summary>
        /// Tests that the object has the correct authorization rules.
        /// </summary>
        [Test, TestCaseSource("TestTypes")]
        public void HasCorrectAuthorizationRules(Type testType)
        {
            // if an principal is already set then this test has been run after others.
            // Reset the principal to a generic one so that it does not fail.
            var principal = Csla.ApplicationContext.User as Galleria.Ccm.Security.DomainPrincipal;
            if (principal != null)
            {
                Csla.ApplicationContext.User = new System.Security.Principal.GenericPrincipal(
                    new System.Security.Principal.GenericIdentity("test"), new String[] { });
            }

            if (testType.IsGenericType) throw new InconclusiveException("Cannot test generic types");


            //((Galleria.Ccm.Security.DomainIdentity)Csla.ApplicationContext.User.Identity).Roles.Clear();
            Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.CreateObject, testType));
            Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.GetObject, testType));
            Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.EditObject, testType));
            Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.DeleteObject, testType));

            if (principal != null)
            {
                //put the authenticated principal back again.
                Csla.ApplicationContext.User = principal;
            }
        }


        //[Test, TestCaseSource("TestTypes")]
        //public void MatchesDto(Type testType)
        //{
        //    Type dtoType = Assembly.GetAssembly(typeof(Galleria.Ccm.Constants)).GetType(
        //        String.Format("Galleria.Ccm.Dal.DataTransferObjects.{0}Dto", testType.Name));
        //    if (dtoType != null)
        //    {
        //        List<String> modelObjectProperties = new List<String>();
        //        List<String> dtoProperties = new List<String>();

        //        foreach (PropertyInfo property in testType.GetProperties().Where(p => p.CanWrite || p.Name == "Id"))
        //        {
        //            modelObjectProperties.Add(property.Name);
        //        }
        //        //modelObjectProperties.Add("ExtendedData");
        //        PropertyInfo parentProperty = testType.GetProperty(
        //            "Parent",
        //            BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
        //        if (parentProperty != null)
        //        {
        //            modelObjectProperties.Add(String.Format("{0}Id", parentProperty.PropertyType.Name));
        //        }
        //        modelObjectProperties = modelObjectProperties.OrderBy(p => p).ToList();
        //        foreach (PropertyInfo property in dtoType.GetProperties().Where(p => p.CanWrite).OrderBy(p => p.Name))
        //        {
        //            dtoProperties.Add(property.Name);
        //        }
        //        if (dtoProperties.Count != modelObjectProperties.Count)
        //        {
        //            for (Int32 i = 0; i < Math.Max(dtoProperties.Count, modelObjectProperties.Count); i++)
        //            {
        //                String dtoProperty = "";
        //                String modelObjectProperty = "";
        //                if (i < dtoProperties.Count)
        //                {
        //                    dtoProperty = dtoProperties[i];
        //                }
        //                if (i < modelObjectProperties.Count)
        //                {
        //                    modelObjectProperty = modelObjectProperties[i];
        //                }
        //                Debug.WriteLine(String.Format("{0}\t{1}", dtoProperty, modelObjectProperty));
        //            }
        //        }

        //        Assert.AreEqual(dtoProperties.Count, modelObjectProperties.Count);
        //        for (Int32 i = 0; i < dtoProperties.Count; i++)
        //        {
        //            Assert.AreEqual(dtoProperties[i], modelObjectProperties[i]);
        //        }
        //    }
        //}


        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public void Setup()
        {
            // authenticate
            Galleria.Ccm.Security.DomainPrincipal.Authenticate();
        }
        #endregion
    }
}
