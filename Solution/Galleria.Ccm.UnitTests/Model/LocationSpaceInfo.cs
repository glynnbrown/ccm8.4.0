﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    class LocationSpaceInfo : TestBase
    {
        #region Test Helper Methods

        public static void AssertDtoAndModelAreEqual(LocationSpaceDto dto, Galleria.Ccm.Model.LocationSpaceInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.LocationId, model.LocationId);
        }
        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entityDtoList[0].Id;

            List<ProductHierarchyDto> hierarchy1Dtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchy1Dtos[0].Id, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            List<LocationDto> sourceLocationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId);

            List<LocationSpaceDto> sourceLocationSpaceDto = TestDataHelper.InsertLocationSpaceDtos(base.DalFactory, sourceLocationDtoList, entityId);

            Galleria.Ccm.Model.LocationSpaceInfoList infoList = Galleria.Ccm.Model.LocationSpaceInfoList.FetchByEntityId(entityId);

            Galleria.Ccm.Model.LocationSpaceInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void GetLocationSpaceInfo()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entityDtoList[0].Id;

            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            List<LocationDto> sourceLocationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId);

            List<LocationSpaceDto> sourceLocationSpaceDtoList = TestDataHelper.InsertLocationSpaceDtos(base.DalFactory, sourceLocationDtoList, entityId);

            Galleria.Ccm.Model.LocationSpaceInfoList infoList = Galleria.Ccm.Model.LocationSpaceInfoList.FetchByEntityId(entityId);


            foreach (LocationSpaceDto LocationSpaceDto in sourceLocationSpaceDtoList)
            {
                if (LocationSpaceDto.EntityId == entityId)
                {
                    Galleria.Ccm.Model.LocationSpaceInfo dalInfo = infoList.First(l => l.Id == LocationSpaceDto.Id);

                    AssertDtoAndModelAreEqual(LocationSpaceDto, dalInfo);
                }
            }
        }

        #endregion
    }
}
