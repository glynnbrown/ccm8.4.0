﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27720 : I.George
//		Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    class AssortmentMinorRevisionInfoListTests : TestBase
    {
        #region TestMethod Helpers
        private EntityDto _entityDto;
        private List<AssortmentMinorRevisionDto> _assortmentMinorRevisionDtos;

        private void InitializeTestData()
        {
            _entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).First();
            _assortmentMinorRevisionDtos = TestDataHelper.InsertAssortmentMinorRevisionDtos(DalFactory, 5, _entityDto.Id);
        }
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Entity entity = Entity.NewEntity();

            Galleria.Ccm.Model.AssortmentMinorRevisionInfoList model =
                Galleria.Ccm.Model.AssortmentMinorRevisionInfoList.FetchByEntityId(entity.Id);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods
        [Test]
        public void FetchByEntityId()
        {
            _entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).First();
            var expected = TestDataHelper.InsertAssortmentMinorRevisionDtos(DalFactory, 5, _entityDto.Id);
            var actual = AssortmentMinorRevisionInfoList.FetchByEntityId(_entityDto.Id);
            foreach (AssortmentMinorRevisionDto assortDto in expected)
            {
               var info = actual.FirstOrDefault(c => c.Id == assortDto.Id);
               Assert.IsNotNull(info, "Matching dtos should have been returned");
            }
           
        }

        [Test]
        public void FetchByEntityIdAssortmentMinorRevisionSearchCriteria()
        {
            _entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).First();
            var expected = TestDataHelper.InsertAssortmentMinorRevisionDtos(DalFactory, 5, _entityDto.Id);
           string seaechCriteria = "";
            var actual = AssortmentMinorRevisionInfoList.FetchByEntityIdAssortmentMinorRevisionSearchCriteria(_entityDto.Id, seaechCriteria);

            foreach (AssortmentMinorRevisionDto assortDto in expected)
            {
               actual.FirstOrDefault(c => c.Id == assortDto.Id);
            }
        }

        [Test]
        public void FetchByProductGroupIdCriteria()
        {
            _entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).First();
            var expected = TestDataHelper.InsertAssortmentMinorRevisionDtos(DalFactory, 5, _entityDto.Id);
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _entityDto.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);

            var actual = AssortmentMinorRevisionInfoList.FetchByProductGroupId(ent1Hierarchy1GroupDtos[1].Id);

            foreach (AssortmentMinorRevisionDto assortDto in expected)
            {
               actual.FirstOrDefault(c => c.Id == assortDto.Id);
            }
        }
        #endregion
    }
}
