﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class FixtureImageTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(FixtureImage.NewFixtureImage());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<FixtureImage>(FixtureImage.NewFixtureImage());
        }

        #endregion

        #region Planogram Model Check Tests

        [Test]
        public void MatchesPlanogramImage()
        {
            Type planType = typeof(PlanogramImage);
            Type fixtureType = typeof(FixtureImage);

            foreach (PropertyInfo planProperty in planType.GetProperties())
            {
                if (planProperty.DeclaringType == planType && planProperty.Name != "Parent")
                {
                    PropertyInfo fixtureProperty = fixtureType.GetProperty(planProperty.Name);

                    Assert.IsNotNull(fixtureProperty, "Property missing: " + planProperty.Name);

                    if (!fixtureProperty.PropertyType.GetInterfaces().Contains(typeof(Galleria.Framework.Planograms.Model.IModelList))
                        && !fixtureProperty.PropertyType.GetInterfaces().Contains(typeof(Galleria.Framework.Planograms.Model.IModelObject)))
                    {
                        if (!planProperty.PropertyType.IsEnum)
                        {
                            Assert.AreEqual(planProperty.PropertyType, fixtureProperty.PropertyType, "Incorrect type: " + planProperty.Name);
                        }
                        else
                        {
                            Assert.IsFalse(fixtureProperty.PropertyType == typeof(Byte), "Incorrect type: " + planProperty.Name);
                        }
                    }
                }

            }
        }


        #endregion
    }
}
