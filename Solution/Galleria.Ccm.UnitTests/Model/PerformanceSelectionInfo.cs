﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26159 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PerformanceSelectionInfoTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            //Insert some test performance selections
            PerformanceSelection selection1 = PerformanceSelection.NewPerformanceSelection(1);
            selection1.Name = "Selection 1";
            selection1.SelectionType = PerformanceSelectionType.Fixed;
            selection1.TimeType = PerformanceSelectionTimeType.Week;
            selection1.GFSPerformanceSourceId = 1;
            selection1.Save();

            PerformanceSelection selection2 = PerformanceSelection.NewPerformanceSelection(1);
            selection2.Name = "Selection 2";
            selection2.SelectionType = PerformanceSelectionType.Dynamic;
            selection2.TimeType = PerformanceSelectionTimeType.Month;
            selection2.GFSPerformanceSourceId = 2;
            selection2.Save();

            PerformanceSelectionInfo model = PerformanceSelectionInfoList.FetchByEntityId(1)[0];
            Serialize(model);
        }
        #endregion

        #region Methods

        [Test]
        public void FetchChild()
        {
            TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            //Insert some test performance selections
            PerformanceSelection selection1 = PerformanceSelection.NewPerformanceSelection(1);
            selection1.Name = "Selection 1";
            selection1.SelectionType = PerformanceSelectionType.Fixed;
            selection1.TimeType = PerformanceSelectionTimeType.Week;
            selection1.GFSPerformanceSourceId = 1;
            selection1 = selection1.Save();

            PerformanceSelection selection2 = PerformanceSelection.NewPerformanceSelection(1);
            selection2.Name = "Selection 2";
            selection2.SelectionType = PerformanceSelectionType.Dynamic;
            selection2.TimeType = PerformanceSelectionTimeType.Month;
            selection2.GFSPerformanceSourceId = 2;
            selection2 = selection2.Save();

            PerformanceSelectionInfoList modelList = PerformanceSelectionInfoList.FetchByEntityId(1);

            Assert.AreEqual(selection1.Id, modelList[0].Id);
            Assert.AreEqual(selection1.Name, modelList[0].Name);

            Assert.AreEqual(selection2.Id, modelList[1].Id);
            Assert.AreEqual(selection2.Name, modelList[1].Name);
        }

        #endregion
    }
}
