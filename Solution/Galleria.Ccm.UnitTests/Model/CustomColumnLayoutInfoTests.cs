﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnLayoutInfoTests : TestBase
    {
        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumnLayoutInfo testModel = CustomColumnLayoutInfo.GetCustomColumnLayoutInfo(null, new CustomColumnLayoutInfoDto());

            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<CustomColumnLayoutInfo>(CustomColumnLayoutInfo.GetCustomColumnLayoutInfo(null, new CustomColumnLayoutInfoDto()));
        }

        [Test]
        public void GetCustomColumnLayoutInfo()
        {
            TestDelegate code = () => CustomColumnLayoutInfo.GetCustomColumnLayoutInfo(null, new CustomColumnLayoutInfoDto());

            Assert.DoesNotThrow(code);
        }

        #endregion
    }
}
