﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Security.Principal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserPlanogramGroupTests : TestBase
    {
        private EntityDto _entityDto;

        [Test]
        public void Serializable()
        {
            var model = UserPlanogramGroup.NewUserPlanogramGroup(1, 1);
            TestDelegate serialize = () => Serialize(model);
            Assert.DoesNotThrow(serialize);
        }

        [Test]
        public void PropertySetters()
        {
            var model = UserPlanogramGroup.NewUserPlanogramGroup(1,1);
            TestPropertySetters<UserPlanogramGroup>(model);
        }

        [Test]
        public void NewUserPlanogramGroup()
        {
            var model = UserPlanogramGroup.NewUserPlanogramGroup(1, 1);
            Assert.IsTrue(model.IsNew, "User should be marked as a new object");
        }

        private UserPlanogramGroupDto InsertUserPlanogramGroupWithDefaults()
        {
            _entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).FirstOrDefault();
            var planHierarchy = PlanogramHierarchy.NewPlanogramHierarchy(_entityDto.Id);
            planHierarchy.Name = "PlanHierarchy";
            planHierarchy.Save();
            var modelDto = new UserPlanogramGroupDto
            {
                UserId = InsertDtoForCurrentUser().Id,
                PlanogramGroupId = planHierarchy.RootGroup.Id
            };
            using (var dalContext = DalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IUserPlanogramGroupDal>())
            {
                dal.Insert(modelDto);
            }
            return modelDto;
        }

        private UserDto InsertDtoForCurrentUser()
        {
            var dto = new UserDto()
            {
                UserName = WindowsIdentity.GetCurrent().Name
            };

            try
            {
                using (var dalContext = this.DalFactory.CreateContext())
                {
                    using (var dal = dalContext.GetDal<IUserDal>())
                    {
                        dal.Insert(dto);
                    }
                }
            }
            catch (Exception)
            {
                // DTO already exists
            }
            return dto;
        }
    }
}
