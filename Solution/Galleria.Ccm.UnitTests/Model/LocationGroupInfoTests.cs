﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationGroupInfoTests : TestBase
    {
        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(LocationGroupDto dto, LocationGroupInfo model)
        {
            Assert.AreEqual(4, typeof(LocationGroupInfoDto).GetProperties().Count(), "Number of properties has changed");

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Code, model.Code);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.DateDeleted, model.DateDeleted);

        }


        private List<LocationGroupDto> InsertTestDtos()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            LocationHierarchyDto hierarchyDto = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id)[0];

            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDto.Id, 3);
            List<LocationGroupDto> returnList = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 5);

            return returnList;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            LocationHierarchy hierarchy = LocationHierarchy.FetchById(InsertTestDtos()[0].LocationHierarchyId);
            hierarchy.RootGroup.ChildList.Remove(hierarchy.RootGroup.ChildList[0]);
            hierarchy = hierarchy.Save();

            LocationGroupInfoList infoList = LocationGroupInfoList.FetchDeletedByLocationHierarchyId(hierarchy.Id);

            LocationGroupInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchLocationGroupInfo()
        {
            LocationGroupInfoDto dto =
                new LocationGroupInfoDto()
                {
                    Code = "c1",
                    DateDeleted = DateTime.UtcNow,
                    Name = "C1Name",
                    Id = 22
                };

            LocationGroupInfo model = LocationGroupInfo.Fetch(null, dto);


            AssertDtoAndModelAreEqual(dto, model);
        }

        #endregion


    }
}
