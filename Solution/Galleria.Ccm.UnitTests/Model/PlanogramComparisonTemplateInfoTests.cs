﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramComparisonTemplateInfoTests : TestBase
    {
        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId;
            List<PlanogramComparisonTemplateDto> dtos;
            PlanogramComparisonTemplateInfoList models;
            SetupTestModels(out entityId, out dtos, out models);
            PlanogramComparisonTemplateInfo mut = models.FirstOrDefault();
            if (mut == null) Assert.Inconclusive("There should be a model to serialize.");

            Serialize(mut);
        }

        private void SetupTestModels(out Int32 entityId, out List<PlanogramComparisonTemplateDto> dtos, out PlanogramComparisonTemplateInfoList models)
        {
            IDalContext dalContext = DalFactory.CreateContext();
            entityId = TestDataHelper.InsertDefaultTestEntity(DalFactory);
            dtos = TestDataHelper.InsertPlanogramComparisonTemplateDtos(dalContext, 1, entityId);
            models = PlanogramComparisonTemplateInfoList.FetchByEntityId(entityId);
        }

        #endregion

        #region FactoryMethods

        [Test]
        public void FetchEntityInfo()
        {
            Int32 entityId;
            List<PlanogramComparisonTemplateDto> dtos;
            PlanogramComparisonTemplateInfoList models;
            SetupTestModels(out entityId, out dtos, out models);

            foreach (PlanogramComparisonTemplateDto dto in dtos)
            {
                PlanogramComparisonTemplateInfo mut = models.First(l => Equals(l.Id, dto.Id));
                AssertDtoAndModelAreEqual(dto, mut);
            }
        }

        #endregion
    }
}
