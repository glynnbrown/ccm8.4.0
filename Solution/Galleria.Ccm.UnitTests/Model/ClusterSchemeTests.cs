﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Csla;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ClusterSchemeTests : TestBase
    {
        #region Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(ClusterSchemeDto dto, Galleria.Ccm.Model.ClusterScheme model)
        {
            Assert.AreEqual(11, typeof(ClusterSchemeDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            //dtoKey - not in model
            Assert.AreEqual(dto.UniqueContentReference, model.UniqueContentReference);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.IsDefault, model.IsDefault);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
            //DateCreated - not in model
            //DateLastModified - not in model
            //DateDeleted - not in model.
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            ClusterScheme model =
                ClusterScheme.NewClusterScheme(1);
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model
            ClusterScheme model = ClusterScheme.NewClusterScheme(1);

            TestPropertySetters<ClusterScheme>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewClusterScheme()
        {
            // create a new model
            ClusterScheme model = ClusterScheme.NewClusterScheme(1);

            //amend details
            model.Name = Guid.NewGuid().ToString();

            //take copy of cluster scheme
            ClusterScheme copy = model.Clone();

            //call factory method on original model
            model = ClusterScheme.NewClusterScheme(1);

            Assert.AreNotEqual(model.Name, copy.Name, "Names should not match");
        }

        [Test]
        public void FetchById()
        {
            // insert into the data source
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                dalContext.Begin();

                //Create a locations
                List<LocationDto> locationDtoList = new List<LocationDto>();
                for (int i = 0; i < 5; i++)
                {
                    locationDtoList.Add(new LocationDto()
                    {
                        Name = "name" + i,
                        Address1 = "address1" + i,
                        Address2 = "address2" + i,
                        City = "city" + i,
                        Code = "code" + i
                    });
                }

                //Insert cluster scheme dto
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    foreach (LocationDto dto in locationDtoList)
                    {
                        dal.Insert(dto);
                    }
                }

                //Insert cluster data for these stores
                ClusterSchemeDto clusterDto = TestDataHelper.InsertSingeClusterSchemeData(this.DalFactory, locationDtoList);

                dalContext.Commit();

                // fetch the model
                ClusterScheme model = ClusterScheme.FetchById(clusterDto.Id);

                // assert they are different
                AssertDtoAndModelAreEqual(clusterDto, model);
            }
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// NameRequired
        /// </summary>
        [Test]
        public void NameRequired()
        {
            // create a new model
            ClusterScheme model = ClusterScheme.NewClusterScheme(1);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = string.Empty;
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        /// <summary>
        /// NamePropertyMaxLength
        /// </summary>
        [Test]
        public void NamePropertyMaxLength()
        {
            // create a new model
            ClusterScheme model = ClusterScheme.NewClusterScheme(1);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = new String('*', 256);
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        #endregion

        #region Methods

        [Test]
        public void FetchAllocatedLocations()
        {
            //insert test data
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, entityDtos);
            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 2);
            List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 1);

            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(base.DalFactory, entityDtos[0].Id, groupDtos, 25);

            List<ClusterSchemeDto> clusterSchemeDtos = TestDataHelper.InsertClusterSchemeDtos(base.DalFactory, entityDtos[0].Id, 1);
            List<ClusterDto> clusterDtos = TestDataHelper.InsertClusterDtos(base.DalFactory, clusterSchemeDtos, 5);
            List<ClusterLocationDto> clusterLocDtos = TestDataHelper.InsertClusterLocationDtos(base.DalFactory, clusterDtos, locDtos);

            //fetch back the cluster scheme
            Galleria.Ccm.Model.ClusterScheme model = ClusterScheme.FetchById(clusterSchemeDtos[0].Id);

            List<ClusterLocation> methodResult = model.FetchAllocatedLocations();

            foreach (ClusterLocationDto dto in clusterLocDtos)
            {
                var methodResultItem = methodResult.First(c => c.Id == dto.Id);
                ClusterLocationTests.AssertDtoAndModelAreEqual(dto, methodResultItem);
            }

        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_InsertUpdateDelete()
        {
            //insert entities and locations
            Entity entity = Entity.NewEntity();
            entity.Name = "ent";
            entity = entity.Save();
            Int32 entityId = entity.Id;
            TestDataHelper.PopulateLocationHierarchy(base.DalFactory, entityId);

            //create a new item
            ClusterScheme item =
                ClusterScheme.NewClusterScheme(entityId);
            TestDataHelper.SetPropertyValues(item);

            //insert
            item = item.Save();

            ClusterSchemeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //update
            TestDataHelper.SetPropertyValues(item);
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);


            //update again to check concurrency
            TestDataHelper.SetPropertyValues(item);
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //delete
            Int32 itemId = item.Id;
            item.Delete();
            item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    Assert.Throws(typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }

        }

        #endregion
    }
}
