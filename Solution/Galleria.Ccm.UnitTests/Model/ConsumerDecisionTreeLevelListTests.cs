﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeLevelListTests : TestBase
    {
        #region Serializable
        [Test]
        public void Serializable()
        {
            var model = ConsumerDecisionTreeLevelList.NewConsumerDecisionTreeLevelList();
            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTreeLevelList()
        {
            var model = ConsumerDecisionTreeLevelList.NewConsumerDecisionTreeLevelList();

            Assert.IsTrue(model.IsChild, "the model should be a child");
            Assert.IsEmpty(model, "the model should be empty");
        }

        #endregion
    }
}
