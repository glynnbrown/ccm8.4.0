﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Csla.Core;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeTests : TestBase
    {
        #region Test Helpers

        public static void AssertDtoAndModelAreEqual(ConsumerDecisionTreeDto dto,
            Ccm.Model.ConsumerDecisionTree model)
        {
            Assert.AreEqual(11, typeof(ConsumerDecisionTreeDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            //dtokey
            Assert.AreEqual(dto.UniqueContentReference, model.UniqueContentReference);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
            Assert.AreEqual(dto.Type, (Byte)model.Type);
            //datecreated
            //datelastmodified
            //date deleted
        }

        #endregion

        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.ConsumerDecisionTree model =
                Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);
            Serialize(model);
        }

        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.ConsumerDecisionTree model =
                Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);

            TestBase.TestPropertySetters<Galleria.Ccm.Model.ConsumerDecisionTree>(model);
        }

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTree()
        {
            Galleria.Ccm.Model.ConsumerDecisionTree model =
               Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);

            Assert.IsTrue(model.IsNew);
            Assert.IsFalse(model.IsChild, "should be a root object");

            Assert.IsNotNull(model.RootNode);

            //check there is a default name
            Assert.IsNotNull(model.Name);
        }

        [Test]
        public void FetchById()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<ConsumerDecisionTreeDto> cdtDtos = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 15);

            foreach (ConsumerDecisionTreeDto dto in cdtDtos)
            {
                Galleria.Ccm.Model.ConsumerDecisionTree model
                   = Galleria.Ccm.Model.ConsumerDecisionTree.FetchById(dto.Id);

                AssertDtoAndModelAreEqual(dto, model);
            }
        }

        [Test]
        public void FetchByEntityIdName()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<ConsumerDecisionTreeDto> cdtDtos = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 15);

            foreach (ConsumerDecisionTreeDto dto in cdtDtos)
            {
                Galleria.Ccm.Model.ConsumerDecisionTree model
                   = Galleria.Ccm.Model.ConsumerDecisionTree.FetchByEntityIdName(dto.EntityId, dto.Name);

                AssertDtoAndModelAreEqual(dto, model);
            }
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// NameRequired
        /// </summary>
        [Test]
        public void NameRequired()
        {
            // get groupId
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            Int32 hierarchy = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0].Id;
            List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchy, 1);
            Int32 groupId = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levels, 1)[0].Id;

            // create a new model
            Galleria.Ccm.Model.ConsumerDecisionTree model =
               Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);
            model.ProductGroupId = groupId;

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = String.Empty;
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        /// <summary>
        /// NamePropertyMaxLength
        /// </summary>
        [Test]
        public void NamePropertyMaxLength()
        {
            // get groupId
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            Int32 hierarchy = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0].Id;
            List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchy, 1);
            Int32 groupId = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levels, 1)[0].Id;

            // create a new model
            Galleria.Ccm.Model.ConsumerDecisionTree model =
               Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);
            model.ProductGroupId = groupId;

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = new String('*', 101);
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        /// <summary>
        /// GroupIdPropertyRequired
        /// </summary>
        [Test]
        public void GroupIdPropertyRequired()
        {
            // get groupId
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            Int32 hierarchy = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0].Id;
            List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchy, 1);
            Int32 groupId = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levels, 1)[0].Id;

            // create a new model
            Galleria.Ccm.Model.ConsumerDecisionTree model =
               Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);
            model.Name = "name";

            // set a valid groupId
            model.ProductGroupId = groupId;
            Assert.AreEqual(model.IsValid, true);

            // set an invalid groupId
            model.ProductGroupId = null;
            Assert.AreEqual(model.IsValid, false);

            // set a valid groupId
            model.ProductGroupId = groupId;
            Assert.AreEqual(model.IsValid, true);
        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_InsertUpdateDelete()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            Int32 hierarchy = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0].Id;
            List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchy, 1);
            Int32 groupId = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levels, 1)[0].Id;

            //create a new item
            Galleria.Ccm.Model.ConsumerDecisionTree item =
                Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(entityId);
            item.Name = "name";
            item.ProductGroupId = groupId;

            //insert
            item = item.Save();

            ConsumerDecisionTreeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //update
            item.Name = "name1";
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);

                    dto = dal.FetchByEntityIdName(item.EntityId, item.Name);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }


            //update again to check concurrency
            item.Name = "name2";
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);

                    dto = dal.FetchByEntityIdName(item.EntityId, item.Name);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }

            //delete
            Int32 itemId = item.Id;
            item.Delete();
            item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }

        }


        #endregion

        #region Helpers

        //[Test]
        //public void GetLevelCount()
        //{
        //    //create a model
        //    Galleria.Ccm.Model.ConsumerDecisionTree model =
        //       Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);

        //    //add nodes
        //    Galleria.Ccm.Model.ConsumerDecisionTreeNode lastNode = model.RootNode;
        //    for(Int32 i=1; i <6; i++)
        //    {
        //         Galleria.Ccm.Model.ConsumerDecisionTreeNode node =
        //        Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode();

        //        lastNode.ChildList.Add(node);

        //        Assert.AreEqual(i+1, model.GetLevelCount());

        //        lastNode = node;
        //    }

        //}

        //[Test]
        //public void GetLevelLocationBreakType()
        //{
        //    //create a model
        //    Galleria.Ccm.Model.ConsumerDecisionTree model =
        //       Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);

        //    //add nodes
        //    Galleria.Ccm.Model.ConsumerDecisionTreeNode lastNode = model.RootNode;
        //    for (Int32 i = 1; i < 5; i++)
        //    {
        //        Galleria.Ccm.Model.ConsumerDecisionTreeNode node =
        //       Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode();

        //        node.ProductBreakType = 
        //            (Galleria.Ccm.Model.ConsumerDecisionTreeProductBreakType)
        //            TestDataHelper.GetRandomValue(node.ProductBreakType.GetType(), new Random());

        //        lastNode.ChildList.Add(node);

        //        Assert.AreEqual(node.ProductBreakType, model.GetLevelLocationBreakType(i));

        //        lastNode = node;
        //    }
        //}

        //[Test]
        //public void GetAssignedProductIds()
        //{
        //    List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
        //    Int32 entityId = entityDtoList[0].Id;
        //    ApplicationContext.GlobalContext[Constants.EntityId] = entityId;

        //    //product hierarchy
        //    ProductHierarchyDto prodHierarchyDto = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0];
        //    List<ProductLevelDto> levelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, prodHierarchyDto.Id, 2);
        //    List<ProductGroupDto> groupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelDtoList, 5);

        //    //products
        //    List<ProductDto> prodDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 10, entityId);

        //    //create a model
        //    Galleria.Ccm.Model.ConsumerDecisionTree model =
        //       Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(1);

        //    //add 2 child nodes
        //    model.RootNode.ChildList.Add(
        //        Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode());
        //    for (Int32 i = 0; i < 5; i++)
        //    {
        //        model.RootNode.ChildList[0].Products.Add(
        //             Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(prodDtoList[i].Id)
        //             );
        //    }

        //    model.RootNode.ChildList.Add(
        //         Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode());
        //    for (Int32 i = 5; i < 10; i++)
        //    {
        //        model.RootNode.ChildList[1].Products.Add(
        //             Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(prodDtoList[i].Id)
        //             );
        //    }


        //    //call the model
        //    List<Int32> assignedIds = model.GetAssignedProductIds().ToList();
        //    foreach (ProductDto prodDto in prodDtoList)
        //    {
        //        Assert.Contains(prodDto.Id, assignedIds);
        //    }

        //}

        #endregion
    }
}
