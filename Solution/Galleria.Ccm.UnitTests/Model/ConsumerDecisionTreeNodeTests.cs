﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeNodeTests : TestBase
    {
        #region Test Helpers

        public static void AssertDtoAndModelAreEqual(ConsumerDecisionTreeNodeDto dto,
            Ccm.Model.ConsumerDecisionTreeNode model)
        {
            Assert.AreEqual(10, typeof(ConsumerDecisionTreeNodeDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            //dtokey
            //public Int32 ConsumerDecisionTreeId { get; set; }
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.ConsumerDecisionTreeLevelId, model.ConsumerDecisionTreeLevelId);
            //public Int32? ParentNodeId { get; set; }
            //public DateTime DateCreated { get; set; }
            //public DateTime DateLastModified { get; set; }
            //public DateTime? DateDeleted { get; set; }
        }

        #endregion

        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeNode model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(1);
            Serialize(model);
        }

        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeNode model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(1);

            TestBase.TestPropertySetters<Galleria.Ccm.Model.ConsumerDecisionTreeNode>
                (model, new List<string>() { "LevelId" });
        }

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTreeNode()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeNode model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(1);

            Assert.IsTrue(model.IsNew);
            Assert.IsTrue(model.IsChild, "should be a child object");

            Assert.IsNotNull(model.Products);
            Assert.IsNotNull(model.Name);
        }

        //[Test]
        //public void NewConsumerDecisionTreeNodeFromScenarioCdtNode()
        //{
        //    List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
        //    Int32 entityId = entityDtoList[0].Id;

        //    //product hierarchy
        //    ProductHierarchyDto prodHierarchyDto = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0];
        //    List<ProductLevelDto> levelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, prodHierarchyDto.Id, 2);
        //    List<ProductGroupDto> groupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelDtoList, 5);

        //    //products
        //    List<ProductDto> prodDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 2, entityId);

        //    //product universe
        //    List<ProductUniverseDto> universeDtoList =
        //        TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, groupDtoList);
        //    List<ProductUniverseProductDto> universeProductDtoList =
        //        TestDataHelper.InsertProductUniverseProductDtos(base.DalFactory, universeDtoList, prodDtoList);

        //    //create the parent object and save
        //    Galleria.Ccm.Model.Review reviewModel = Galleria.Ccm.Model.Review.NewReview(entityId, 1);

        //    //add the universe
        //    Galleria.Ccm.Model.ProductUniverse universe =
        //        Galleria.Ccm.Model.ProductUniverse.FetchById(universeDtoList[0].Id);
        //    Galleria.Ccm.Model.ScenarioProductUniverse scenarioUniverse =
        //        Galleria.Ccm.Model.ScenarioProductUniverse.NewScenarioProductUniverse(universe);
        //    reviewModel.Scenarios[0].ScenarioProductUniverse = scenarioUniverse;

        //    //add a product to the cdt root node
        //    Galleria.Ccm.Model.ScenarioConsumerDecisionTreeNodeProduct scenarioCdtProduct =
        //        Galleria.Ccm.Model.ScenarioConsumerDecisionTreeNodeProduct.NewScenarioConsumerDecisionTreeNodeProduct
        //        (reviewModel.Scenarios[0].ScenarioProductUniverse.Products[0]);

        //    Galleria.Ccm.Model.ScenarioConsumerDecisionTreeNode scenNode = reviewModel.Scenarios[0].ConsumerDecisionTree.RootNode;

        //    Galleria.Ccm.Model.ConsumerDecisionTree consumerDecisionTree =
        //        Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(entityId);

        //    //create the cdtnode model
        //    Galleria.Ccm.Model.ConsumerDecisionTreeNode model =
        //        Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(scenNode, consumerDecisionTree.RootNode);

        //    Assert.IsTrue(model.IsNew);
        //    Assert.IsTrue(model.IsChild, "should be a child object");

        //    Assert.AreEqual(scenNode.Name, model.Name);
        //    //Assert.AreEqual(scenNode.ProductBreakType, model.ProductBreakType);

        //    Assert.AreEqual(reviewModel.Scenarios[0].ConsumerDecisionTree.RootNode.NodeRangeProducts.Count, model.Products.Count);
        //}

        [Test]
        public void FetchConsumerDecisionTreeNode()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a cdt hierarchy
            ConsumerDecisionTreeDto cdtDto = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1)[0];
            List<ConsumerDecisionTreeLevelDto> cdtLevelDtoList = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(this.DalFactory, cdtDto, 3, true);
            List<ConsumerDecisionTreeNodeDto> nodeDtosList = TestDataHelper.InsertConsumerDecisionTreeNodeDtos(base.DalFactory, cdtDto, cdtLevelDtoList, 5, /*addRoot*/true);


            //fetch back the cdt
            Galleria.Ccm.Model.ConsumerDecisionTree cdt =
                Galleria.Ccm.Model.ConsumerDecisionTree.FetchById(cdtDto.Id);

            //check the nodes match
            IEnumerable<Galleria.Ccm.Model.ConsumerDecisionTreeNode> cdtNodes =
                cdt.RootNode.FetchAllChildNodes();

            Assert.AreEqual(nodeDtosList.Count, cdtNodes.Count());

            //cycle through nodes
            foreach (ConsumerDecisionTreeNodeDto nodeDto in nodeDtosList)
            {
                Galleria.Ccm.Model.ConsumerDecisionTreeNode cdtNode =
                    cdtNodes.FirstOrDefault(c => c.Id == nodeDto.Id);

                Assert.IsNotNull(cdtNode);

                if (!cdtNode.IsRoot)
                {
                    Assert.AreEqual(cdtNode.ParentNode.Id, nodeDto.ParentNodeId);
                }

                AssertDtoAndModelAreEqual(nodeDto, cdtNode);
            }

        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_ChildInsertUpdateDelete()
        {
            //throw new NotImplementedException();

            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            Int32 hierarchy = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0].Id;
            List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchy, 1);
            Int32 groupId = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levels, 1)[0].Id;

            //create the parent cdt
            Galleria.Ccm.Model.ConsumerDecisionTree parentCdt =
                Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(entityId);
            parentCdt.Name = "name";
            parentCdt.ProductGroupId = groupId;
            parentCdt.InsertNewChildLevel(parentCdt.RootLevel);

            //create a new item
            Galleria.Ccm.Model.ConsumerDecisionTreeNode item =
                Galleria.Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(parentCdt.RootLevel.ChildLevel.Id);
            TestDataHelper.SetPropertyValues(item, new List<String>() { "LevelId" });
            parentCdt.RootNode.ChildList.Add(item);

            //insert
            parentCdt = parentCdt.Save();
            item = parentCdt.RootNode.ChildList[0];

            ConsumerDecisionTreeNodeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //update
            TestDataHelper.SetPropertyValues(item, new List<String>() { "LevelId" });
            parentCdt = parentCdt.Save();
            item = parentCdt.RootNode.ChildList[0];

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);


            //update again to check concurrency
            TestDataHelper.SetPropertyValues(item, new List<String>() { "LevelId" });
            parentCdt = parentCdt.Save();
            item = parentCdt.RootNode.ChildList[0];

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);


            //remove from the parent
            Int32 itemId = item.Id;
            parentCdt.RootNode.ChildList.Remove(item);
            parentCdt = parentCdt.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }
        }

        #endregion

        #region ConsumerDecisionTreeLevelId reference checks

        [Test]
        public void ConsumerDecisionTreeLevelId_Resolve()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            ConsumerDecisionTreeDto cdtDto = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1)[0];
            List<ConsumerDecisionTreeLevelDto> cdtLevels = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(base.DalFactory, cdtDto, 1, false);
            TestDataHelper.InsertConsumerDecisionTreeNodeDtos(base.DalFactory, cdtDto, cdtLevels, 1, false);

            var consumerDecisionTree = ConsumerDecisionTree.FetchById(cdtDto.Id);

            var rootLevel = consumerDecisionTree.RootLevel;
            Assert.AreEqual(consumerDecisionTree.RootNode.ConsumerDecisionTreeLevelId, rootLevel.Id);

            //add a new cdt level
            var targetCdtLevel = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel("testlevel");
            consumerDecisionTree.RootLevel.ChildLevel = targetCdtLevel;

            //add a node
            var node = ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(targetCdtLevel.Id);
            node.Name = "testNode";
            consumerDecisionTree.RootNode.ChildList.Add(node);

            //Assert.IsTrue(targetCdtLevel.LevelId is Guid);
            Assert.AreEqual(targetCdtLevel.Id, node.ConsumerDecisionTreeLevelId);

            //save
            consumerDecisionTree = consumerDecisionTree.Save();
            rootLevel = consumerDecisionTree.RootLevel;
            targetCdtLevel = consumerDecisionTree.RootLevel.ChildLevel;
            node = consumerDecisionTree.RootNode.ChildList[0];

            Assert.AreEqual(consumerDecisionTree.RootNode.ConsumerDecisionTreeLevelId, rootLevel.Id);

            //Assert.IsTrue(targetCdtLevel.LevelId is Int32);
            Assert.AreEqual(targetCdtLevel.Id, node.ConsumerDecisionTreeLevelId);


            //re-fetch
            ConsumerDecisionTreeNodeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                {
                    dto = dal.FetchById(node.Id);
                }
            }
            Assert.AreEqual(targetCdtLevel.Id, dto.ConsumerDecisionTreeLevelId);
        }

        [Test]
        public void ConsumerDecisionTreeLevelId_Reset()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            ConsumerDecisionTreeDto cdtDto = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1)[0];
            List<ConsumerDecisionTreeLevelDto> cdtLevels = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(base.DalFactory, cdtDto, 1, false);
            TestDataHelper.InsertConsumerDecisionTreeNodeDtos(base.DalFactory, cdtDto, cdtLevels, 1, false);

            Ccm.Model.ConsumerDecisionTree consumerDecisionTree = Ccm.Model.ConsumerDecisionTree.FetchById(cdtDto.Id);
            Ccm.Model.ConsumerDecisionTree consumerDecisionTreeCopy;

            //add a new cdt level
            Ccm.Model.ConsumerDecisionTreeLevel targetCdtLevel =
                Ccm.Model.ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel("testlevel");
            consumerDecisionTree.RootLevel.ChildLevel = targetCdtLevel;

            //add a node
            Ccm.Model.ConsumerDecisionTreeNode node =
                Ccm.Model.ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(targetCdtLevel.Id);
            node.Name = "testNode";
            consumerDecisionTree.RootNode.ChildList.Add(node);

            //save the review and reload
            consumerDecisionTree = consumerDecisionTree.Save();

            //refetch review to ensure no temp id's
            consumerDecisionTree = Ccm.Model.ConsumerDecisionTree.FetchById(consumerDecisionTree.Id);

            //take the copy
            consumerDecisionTreeCopy = consumerDecisionTree.Copy();
            consumerDecisionTreeCopy = consumerDecisionTreeCopy.Save();


            //check the item has copied correctly
            Assert.AreEqual(consumerDecisionTree.RootNode.ChildList.Count, consumerDecisionTreeCopy.RootNode.ChildList.Count);

            Ccm.Model.ConsumerDecisionTreeNode origItem = consumerDecisionTree.RootNode.ChildList[0];
            Ccm.Model.ConsumerDecisionTreeNode copyItem = consumerDecisionTreeCopy.RootNode.ChildList[0];


            //check the target loc
            Assert.AreEqual(origItem.Id, consumerDecisionTree.RootLevel.ChildLevel.Id);
            Assert.AreEqual(copyItem.Id, consumerDecisionTreeCopy.RootLevel.ChildLevel.Id);
            Assert.AreNotEqual(origItem.Id, copyItem.Id);
        }

        #endregion
    }
}
