﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-27630 : I.George
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttributeInfoList : TestBase
    {
        #region Helper methods
        private Int32 entityId;
        private List<LocationProductAttributeDto> InsertDtos()
        {
             entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId);

            List<LocationProductAttributeDto> dtoList = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtoList, prodList, entityId);
            return dtoList;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Entity entity = Entity.NewEntity();
            
            Galleria.Ccm.Model.LocationProductAttributeInfoList model =
                Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(entity.Id);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory methods

        [Test]
        public void FetchByEntityId()
        {
            List<LocationProductAttributeDto> entityDtoList1 = InsertDtos();
            Int32 entityId2 = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId2);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId2);

            List<LocationProductAttributeDto> entityDtoList2 = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtoList, prodList, entityId2);

            Galleria.Ccm.Model.LocationProductAttributeInfoList model1 =
                Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(entityId);
            foreach (LocationProductAttributeDto dto in entityDtoList1)
            {
                Ccm.Model.LocationProductAttributeInfo info = model1.
                    First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
            }

            Galleria.Ccm.Model.LocationProductAttributeInfoList model2 =
               Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(entityId2);
            foreach (LocationProductAttributeDto dto in entityDtoList2)
            {
                Ccm.Model.LocationProductAttributeInfo info = model2.
                    First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
            }

        }

        [Test]
        public void FetchByEntityIdSearchCriteria()
        {
            Int32 entityId2 = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId2);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId2);

            List<LocationProductAttributeDto> entityDtoList2 = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtoList, prodList, entityId2);

            String searchCriteria = String.Format("{0} OR {1}", prodList[0].Name, prodList[1].Name);
            Galleria.Ccm.Model.LocationProductAttributeInfoList model = Ccm.Model.LocationProductAttributeInfoList.FetchByEntityIdSearchCriteria(entityId2, searchCriteria);
            foreach (LocationProductAttributeDto dto in entityDtoList2.Where
               (a => a.ProductId == prodList[0].Id || a.ProductId == prodList[1].Id))
            {
                Galleria.Ccm.Model.LocationProductAttributeInfo info = model.
                    First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
            }
        }


        #endregion
    }
}
