﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26474 : A.Silva ~ Created
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ValidationTemplateGroupTests : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(ValidationTemplateGroup.NewValidationTemplateGroup());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters(ValidationTemplateGroup.NewValidationTemplateGroup());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewValidationTemplateGroup()
        {
            var newValidationTemplate = ValidationTemplateGroup.NewValidationTemplateGroup();

            Assert.IsTrue(newValidationTemplate.IsNew);
            Assert.IsNotNull(newValidationTemplate.Metrics);
            Assert.IsTrue(newValidationTemplate.IsChild, "A newly created Validation Template Group should be a child object.");
        }

        [Test]
        public void FetchValidationTemplateGroup()
        {
            var entityDtos = TestDataHelper.InsertEntityDtos(DalFactory, 1);
            var templateDtos = TestDataHelper.InsertValidationTemplateDtos(DalFactory, 5, entityDtos[0].Id);
            var groupDtos = TestDataHelper.InsertValidationTemplateGroupDtos(DalFactory,
                templateDtos, 5);

            using (var dalContext = DalFactory.CreateContext())
            {
                foreach (var dto in groupDtos)
                {
                    var model = ValidationTemplateGroup.Fetch(dalContext, dto);

                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }
        #endregion

        #region Data Access
        
        [Test]
        public void DataAccess_Insert()
        {
            var validationTemplate = CreateValidationTemplate("template1");
            validationTemplate.Groups.Add(CreateValidationTemplateGroup("group1"));

            // Save.
            validationTemplate = validationTemplate.Save();
            var validationTemplateGroup = validationTemplate.Groups[0];

            var dto = FetchDtoByModel(validationTemplateGroup);

            AssertHelper.AssertModelObjectsAreEqual(dto, validationTemplateGroup);
        }

        [Test]
        public void DataAccess_Update()
        {
            var validationTemplate = CreateValidationTemplate("template1");
            validationTemplate.Groups.Add(CreateValidationTemplateGroup("group1"));

            // Save.
            validationTemplate = validationTemplate.Save();
            var validationTemplateGroup = validationTemplate.Groups[0];

            // Update and save.
            validationTemplateGroup.Name = "updatedTemplate1";
            validationTemplate = validationTemplate.Save();
            validationTemplateGroup = validationTemplate.Groups[0];

            var dto = FetchDtoByModel(validationTemplateGroup);

            AssertDtoAndModelAreEqual(dto,validationTemplateGroup);
        }

        [Test]
        public void DataAccess_Delete()
        {
            var template = CreateValidationTemplate("template1");
            template.Groups.Add(CreateValidationTemplateGroup("group1"));

            // Save.
            template = template.Save();
            var group = template.Groups[0];

            var dto = FetchDtoByModel(group);

            AssertDtoAndModelAreEqual(dto, group);

            // Delete the dto.
            template.Groups.Remove(group);
            template.Save();

            // Retrieved the deleted item.
            dto = FetchDtoByModel(group);

            Assert.IsNull(dto);
        }

        #endregion

        #region IValidationTemplate Members

        [Test]
        public void IValidationTemplateMetrics_GetAccessor_GetsMetrics()
        {
            const string expectation = "When getting Metrics through the interface the actual Metrics are returned.";
            var testModel = ValidationTemplateGroup.NewValidationTemplateGroup();
            var expected = testModel.Metrics;
            expected.Add(ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric());

            var actual = ((IValidationTemplateGroup)testModel).Metrics;

            CollectionAssert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void IValidationTemplateAddNewMetric_Invoked_AddsNewMetric()
        {
            const string expectation = "When AddNewMetric is invoked the Metrics collection gets a new Metric.";
            var testModel = ValidationTemplateGroup.NewValidationTemplateGroup();

            ((IValidationTemplateGroup)testModel).AddNewMetric();

            var actual = testModel.Metrics.Any<IValidationTemplateGroupMetric>();
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void IValidationTemplateRemoveMetric_ContainsMetric_RemovesMetric()
        {
            const string expectation = "When RemoveMetric is invoked with an existing Metric the Metrics collection removes it.";
            var testModel = ValidationTemplateGroup.NewValidationTemplateGroup();
            var itemToRemove = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            testModel.Metrics.Add(itemToRemove);

            ((IValidationTemplateGroup)testModel).RemoveMetric(itemToRemove);

            var actual = !testModel.Metrics.Contains(itemToRemove);
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void IValidationTemplateRemoveMetric_DoesNotContainMetric_DoesNotChangeMetrics()
        {
            const string expectation = "When RemoveMetric is invoked with a non existing Metric the Metrics collection remains unchanged.";
            var testModel = ValidationTemplateGroup.NewValidationTemplateGroup();
            var itemToRemove = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            testModel.Metrics.Add(itemToRemove);
            var unrelatedItem = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();

            ((IValidationTemplateGroup)testModel).RemoveMetric(unrelatedItem);

            var actual = testModel.Metrics.Contains(itemToRemove);
            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region Test Helper Methods

        private static ValidationTemplate CreateValidationTemplate(String templateName)
        {
            var validationTemplate = ValidationTemplate.NewValidationTemplate(1);
            validationTemplate.Name = templateName;
            return validationTemplate;
        }

        private static ValidationTemplateGroup CreateValidationTemplateGroup(String groupName)
        {
            var validationTemplateGroup = ValidationTemplateGroup.NewValidationTemplateGroup();
            validationTemplateGroup.Name = groupName;
            return validationTemplateGroup;
        }

        private static ValidationTemplateGroupDto FetchDtoByModel(ValidationTemplateGroup model)
        {
            ValidationTemplateGroupDto dto;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IValidationTemplateGroupDal>())
            {
                dto = dal.FetchByValidationTemplateId(model.Parent.Id).FirstOrDefault(groupDto => groupDto.Id == model.Id);
            }
            return dto;
        }

        #endregion
    }
}
