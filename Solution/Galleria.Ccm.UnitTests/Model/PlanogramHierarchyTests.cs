﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25587 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramHierarchyTests : TestBase
    {
        #region Test Fixture Helpers


        internal static List<PlanogramHierarchyDto> InsertPlanogramHierarchyDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId)
        {
            List<PlanogramHierarchyDto> dtoList = new List<PlanogramHierarchyDto>();

            //insert hierarchies
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    for (int i = 1; i <= numToInsert; i++)
                    {

                        dtoList.Add(
                        new PlanogramHierarchyDto()
                        {
                            Name = "heirarchy" + i.ToString(),
                            EntityId = entityId,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        });
                    }
                    foreach (PlanogramHierarchyDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }


        internal static List<PlanogramGroupDto> InsertPlanogramGroupDtos(IDalFactory dalFactory, Int32 hierarchyId,
            Int32 levelCount, Int32 numPerLevel)
        {
            List<PlanogramGroupDto> dtoList = new List<PlanogramGroupDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    Int32 groupNum = 1;


                    //cycle through levels
                    List<PlanogramGroupDto> lastLevelDtos = new List<PlanogramGroupDto>();
                    for (Int32 levelNo = 0; levelNo < levelCount; levelNo++)
                    {
                        List<PlanogramGroupDto> levelDtos = new List<PlanogramGroupDto>();

                        for (int i = 1; i <= numPerLevel; i++)
                        {
                            PlanogramGroupDto groupDto = new PlanogramGroupDto()
                            {
                                Name = Guid.NewGuid().ToString(),
                                PlanogramHierarchyId = hierarchyId,
                                DateCreated = DateTime.Now,
                                DateLastModified = DateTime.Now
                            };

                            //pick a parent
                            Int32 parentCount = lastLevelDtos.Count;
                            if (parentCount != 0)
                            {
                                groupDto.ParentGroupId = lastLevelDtos[parentCount - 1].Id;
                            }


                            dal.Insert(groupDto);
                            groupNum++;

                            levelDtos.Add(groupDto);
                            dtoList.Add(groupDto);



                            //only add one group if this is the root level
                            if (levelNo == 0)
                            {
                                break;
                            }

                        }

                        lastLevelDtos = levelDtos;
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramHierarchy.NewPlanogramHierarchy(1));
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<PlanogramHierarchy>(PlanogramHierarchy.NewPlanogramHierarchy(1));
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewPlanogramHierarchy()
        {
            PlanogramHierarchy newHierarchy =
               PlanogramHierarchy.NewPlanogramHierarchy(1);

            Assert.IsTrue(newHierarchy.IsNew);
            Assert.IsFalse(newHierarchy.IsChild);

            Assert.IsNotNull(newHierarchy.RootGroup);

        }

        [Test]
        public void FetchById()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<PlanogramHierarchyDto> hierarchyDtos = InsertPlanogramHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<PlanogramGroupDto> groupDtos = InsertPlanogramGroupDtos(base.DalFactory, hierarchyDtos[0].Id,1, 2);

            //retrieve by id
            PlanogramHierarchy model =
                PlanogramHierarchy.FetchById(hierarchyDtos[0].Id);

            AssertDtoAndModelAreEqual(hierarchyDtos[0], model);
            Assert.AreEqual(groupDtos.Count, model.EnumerateAllGroups().Count());
        }


        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);

            foreach (EntityDto entity in entityDtos)
            {
                List<PlanogramHierarchyDto> hierarchyDtos = InsertPlanogramHierarchyDtos(base.DalFactory, 1, entity.Id);
                List<PlanogramGroupDto> groupDtos = InsertPlanogramGroupDtos(base.DalFactory, hierarchyDtos[0].Id, 1, 2);

                var model = PlanogramHierarchy.FetchByEntityId(entity.Id);
                AssertDtoAndModelAreEqual(hierarchyDtos[0], model);
                Assert.AreEqual(groupDtos.Count, model.EnumerateAllGroups().Count());
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create a new hierarchy
            PlanogramHierarchy hierarchy =
                PlanogramHierarchy.NewPlanogramHierarchy(1);

            //set properties
            hierarchy.Name = "testHierarchy";

            //save
            hierarchy = hierarchy.Save();

            //fetch back the hierarchy dto
            PlanogramHierarchyDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    dto = dal.FetchById(hierarchy.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, hierarchy);


            //remember to ensure all level id references have been updated
        }

        [Test]
        public void DataAccess_Update()
        {
            //create a new hierarchy
            PlanogramHierarchy hierarchy =
                PlanogramHierarchy.NewPlanogramHierarchy(1);

            //set properties
            hierarchy.Name = "testHierarchy";

            //save
            hierarchy = hierarchy.Save();

            //change a property
            hierarchy.Name = "updatedHierarchy";

            //save
            hierarchy = hierarchy.Save();


            //fetch back the hierarchy dto
            PlanogramHierarchyDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    dto = dal.FetchById(hierarchy.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, hierarchy);


            //remember to ensure all level id references have been updated
        }

       //cant delete

        #endregion

        #region Methods

        [Test]
        public void EnumerateAllGroups()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<PlanogramHierarchyDto> hierarchyDtos = InsertPlanogramHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<PlanogramGroupDto> groupDtos = InsertPlanogramGroupDtos(base.DalFactory, hierarchyDtos[0].Id, 1, 2);

            //retrieve by id
            PlanogramHierarchy model =
                PlanogramHierarchy.FetchById(hierarchyDtos[0].Id);

            List<PlanogramGroup> methodResults = model.EnumerateAllGroups().ToList();

            foreach (PlanogramGroupDto dto in groupDtos)
            {
                var item = methodResults.First(l => l.Id == dto.Id);
                PlanogramGroupTests.AssertDtoAndModelAreEqual(dto, item);
            }
        }

        #endregion
    }
}
