﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using System.Security.Principal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserTests : TestBase
    {
        [Test]
        public void Serializable()
        {
            var user = User.NewUser();
            TestDelegate serialize = ()=> Serialize(user);
            Assert.DoesNotThrow(serialize);
        }

        [Test]
        public void PropertySetters()
        {
            var user = User.NewUser();
            TestPropertySetters<User>(user);
        }

        [Test]
        public void NewUser()
        {
            var user = User.NewUser();
            Assert.IsTrue(user.IsNew, "User should be marked as a new object");
            Assert.IsFalse(user.IsChild, "User should not be marked as a child");
        }

        [Test]
        public void GetCurrentUser()
        {
            InsertDtoForCurrentUser();

            var model = User.GetCurrentUser();

            Assert.AreEqual(WindowsIdentity.GetCurrent().Name, model.UserName);
        }

        [Test]
        public void FetchById()
        {
            var dto = InsertDtoForCurrentUser();

            var model = User.FetchById(dto.Id);

            AssertDtoAndModelAreEqual(dto, model);
        }

        [Test]
        public void DataPortal_InsertUpdateDelete()
        {
            var item = User.NewUser();
            TestDataHelper.SetPropertyValues(item);
            item = item.Save();

            UserDto dto;
            using (var dalContext = base.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IUserDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);

            //update
            TestDataHelper.SetPropertyValues(item);
            item = item.Save();

            using (var dalContext = base.DalFactory.CreateContext())
            {
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);


            //update again to check concurrency
            TestDataHelper.SetPropertyValues(item);
            item = item.Save();

            using (var dalContext = base.DalFactory.CreateContext())
            {
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);

            //delete
            Int32 itemId = item.Id;
            item.Delete();
            item.Save();

            using (var dalContext = base.DalFactory.CreateContext())
            {
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    dto = dal.FetchById(itemId);
                    Assert.IsNotNull(dto.DateDeleted, "Date deleted should be set");
                }
            }
        }

        private UserDto InsertDtoForCurrentUser()
        {
            var dto = new UserDto()
            {
                UserName = WindowsIdentity.GetCurrent().Name
            };

            using (var dalContext = this.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IUserDal>())
                {
                    try
                    {
                        dal.Insert(dto);
                    }
                    catch
                    {
                        dto = dal.FetchByUserName(dto.UserName);
                    }
                }
            }
            return dto;
        }
    }
}
