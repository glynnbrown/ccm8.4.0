﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//		Created
#endregion
#endregion


using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationGroupInfoListTests : TestBase
    {
        private List<LocationGroupDto> InsertTestDtos()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);


            LocationHierarchyDto hierarchyDto = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id)[0];

            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDto.Id, 3);
            List<LocationGroupDto> returnList = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 5);

            return returnList;
        }

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            LocationGroupInfoList model = LocationGroupInfoList.FetchDeletedByLocationHierarchyId(1);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods


        [Test]
        public void FetchDeletedByLocationHierarchyId()
        {
            List<LocationGroupDto> dtoList = InsertTestDtos();

            LocationHierarchy hierarchy = LocationHierarchy.FetchById(dtoList.First().LocationHierarchyId);
            LocationGroup group = hierarchy.RootGroup.ChildList.First();
            hierarchy.RootGroup.ChildList.Remove(group);
            hierarchy = hierarchy.Save();

            LocationGroupInfoList model1 = LocationGroupInfoList.FetchDeletedByLocationHierarchyId(hierarchy.Id);
            Assert.AreEqual(1, model1.Count);
            Assert.AreEqual(group.Code, model1[0].Code);

        }



        #endregion
    }
}
