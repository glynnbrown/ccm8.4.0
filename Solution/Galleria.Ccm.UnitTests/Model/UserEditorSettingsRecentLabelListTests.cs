﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//   Created.
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserEditorSettingsRecentLabelListTests
    {
        [Test]
        public void Add_WhenLessThan10Items_ShouldAddItem(
            [Values(LabelType.Product, LabelType.Fixture)] LabelType labelType)
        {
            UserEditorSettingsRecentLabelList list = UserEditorSettingsRecentLabelList.NewList();
            Label label = Label.NewLabel(labelType);

            list.Add(label, maxRecentItems:10);

            Assert.IsTrue(list.Any(rl => rl.LabelId.Equals(label.Id)));
        }

        [Test]
        public void Add_When10Items_RemovesFirstAndAddsLabel(
            [Values(LabelType.Product, LabelType.Fixture)] LabelType labelType)
        {
            UserEditorSettingsRecentLabelList list = UserEditorSettingsRecentLabelList.NewList();
            for (int i = 0; i < 10; i++)
            {
                list.Add(Label.NewLabel(labelType), maxRecentItems: 10);
            }
            Label label = Label.NewLabel(labelType);
            var firstItem = list.First();

            list.Add(label, maxRecentItems: 10);

            Assert.That(list, Has.Count.EqualTo(10));
            Assert.IsTrue(list.Any(rl => rl.LabelId.Equals(label.Id)));
            CollectionAssert.DoesNotContain(list, firstItem);
        }

        [Test]
        public void Add_When10ProductLabels_AddingFixture_AddsItem()
        {
            UserEditorSettingsRecentLabelList list = UserEditorSettingsRecentLabelList.NewList();
            for (int i = 0; i < 10; i++)
            {
                list.Add(Label.NewLabel(LabelType.Product), maxRecentItems: 10);
            }
            Label fixtureLabel = Label.NewLabel(LabelType.Fixture);

            list.Add(fixtureLabel, maxRecentItems: 10);

            Assert.That(list.Count(l => l.LabelType == LabelType.Product), Is.EqualTo(10));
            Assert.IsTrue(list.Any(rl => rl.LabelId.Equals(fixtureLabel.Id)));
        }

        [Test]
        public void Add_When10FixtureLabels_AddingProduct_AddsItem()
        {
            UserEditorSettingsRecentLabelList list = UserEditorSettingsRecentLabelList.NewList();
            for (int i = 0; i < 10; i++)
            {
                list.Add(Label.NewLabel(LabelType.Fixture), maxRecentItems: 10);
            }
            Label productLabel = Label.NewLabel(LabelType.Product);

            list.Add(productLabel, maxRecentItems: 10);

            Assert.That(list.Count(l => l.LabelType == LabelType.Fixture), Is.EqualTo(10));
            Assert.IsTrue(list.Any(rl => rl.LabelId.Equals(productLabel.Id)));
        }

        [Test]
        public void Add_NullIsIgnored_WhenAddingAddingNull()
        {
            UserEditorSettingsRecentLabelList list = UserEditorSettingsRecentLabelList.NewList();

            list.Add(null, maxRecentItems:10);

            Assert.IsTrue(list.All(i => i.LabelId != null));
        }
    }
}
