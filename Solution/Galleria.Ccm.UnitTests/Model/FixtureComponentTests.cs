﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class FixtureComponentTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(FixtureComponent.NewFixtureComponent());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<FixtureComponent>(FixtureComponent.NewFixtureComponent());
        }

        #endregion

        #region Planogram Model Check Tests
        [Test]
        public void MatchesPlanogramComponent()
        {
            Type planType = typeof(PlanogramComponent);
            Type fixtureType = typeof(FixtureComponent);

            foreach (PropertyInfo planProperty in planType.GetProperties())
            {
                if (planProperty.Name == "IsCarPark") continue;
                if (planProperty.Name == "SubComponents") continue;
                if (planProperty.Name == "SubComponentsAsync") continue;
                if (planProperty.Name.StartsWith("Meta")) continue;

                if (planProperty.DeclaringType == planType && planProperty.Name != "Parent")
                {
                    PropertyInfo fixtureProperty = fixtureType.GetProperty(planProperty.Name);

                    Assert.IsNotNull(fixtureProperty, "Property missing: " + planProperty.Name);

                    if (!fixtureProperty.PropertyType.GetInterfaces().Contains(typeof(Galleria.Framework.Planograms.Model.IModelList))
                        && !fixtureProperty.PropertyType.GetInterfaces().Contains(typeof(Galleria.Framework.Planograms.Model.IModelObject))
                        && !planProperty.Name.Contains("Id"))
                    {
                        if (!planProperty.PropertyType.IsEnum)
                        {
                            Assert.AreEqual(planProperty.PropertyType, fixtureProperty.PropertyType, "Incorrect type: " + planProperty.Name);
                        }
                        else
                        {
                            Assert.IsFalse(fixtureProperty.PropertyType == typeof(Byte), "Incorrect type: " + planProperty.Name);
                        }
                    }
                }

            }
        }
       



        #endregion
    }
}
