﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25669 : A.Kuszyk
//      Created.
// V8-26041 : A.Kuszyk
//      Added tests for GetImageData.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductTests : TestBase
    {
        private List<String> _propertiesToExclude = new List<String>() 
            {
                "PlanogramImageIdFront",
                "PlanogramImageIdBack",
                "PlanogramImageIdTop",
                "PlanogramImageIdBottom",
                "PlanogramImageIdLeft",
                "PlanogramImageIdRight",
                "PlanogramImageIdDisplayFront",
                "PlanogramImageIdDisplayBack",
                "PlanogramImageIdDisplayTop",
                "PlanogramImageIdDisplayBottom",
                "PlanogramImageIdDisplayLeft",
                "PlanogramImageIdDisplayRight",
                "PlanogramImageIdTrayFront",
                "PlanogramImageIdTrayBack",
                "PlanogramImageIdTrayTop",
                "PlanogramImageIdTrayBottom",
                "PlanogramImageIdTrayLeft",
                "PlanogramImageIdTrayRight",
                "PlanogramImageIdPointOfPurchaseFront",
                "PlanogramImageIdPointOfPurchaseBack",
                "PlanogramImageIdPointOfPurchaseTop",
                "PlanogramImageIdPointOfPurchaseBottom",
                "PlanogramImageIdPointOfPurchaseLeft",
                "PlanogramImageIdPointOfPurchaseRight",
                "PlanogramImageIdAlternateFront",
                "PlanogramImageIdAlternateBack",
                "PlanogramImageIdAlternateTop",
                "PlanogramImageIdAlternateBottom",
                "PlanogramImageIdAlternateLeft",
                "PlanogramImageIdAlternateRight",
                "PlanogramImageIdCaseFront",
                "PlanogramImageIdCaseBack",
                "PlanogramImageIdCaseTop",
                "PlanogramImageIdCaseBottom",
                "PlanogramImageIdCaseLeft",
                "PlanogramImageIdCaseRight",
                "ShapeType"
            };

        [Test]
        public void Serializable()
        {
            var product = Product.NewProduct();
            Serialize(product);
        }

        [Test]
        public void PropertySetters()
        {
            var product = Product.NewProduct();
            
            TestPropertySetters<Product>(product,_propertiesToExclude);
        }

        #region Factory Methods
        [Test]
        public void NewProduct()
        {
            var newProduct = Product.NewProduct(1);

            Assert.AreEqual(newProduct.EntityId, 1);

            Assert.IsTrue(newProduct.IsNew);
            Assert.IsFalse(newProduct.IsChild, "A newly created product should be a root object");
        }

        [Test]
        public void FetchById()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityList[0].Id);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);
            List<ProductDto> productDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entityList[0].Id);

            //fetch each product by id
            foreach (ProductDto prodDto in productDtoList)
            {
                var dalItem = Product.FetchById(prodDto.Id);
                AssertHelper.AssertModelObjectsAreEqual(prodDto, dalItem,_propertiesToExclude);

                Assert.IsFalse(dalItem.IsChild, "Should have been fetched as a root object");
            }
        } 
        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            Random rand = new Random();

            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityList[0].Id);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //create a new item 
            var prod = Product.NewProduct(entityList[0].Id);

            //set properties & correct so they are valid
            prod.Name = "prod1";
            prod.Gtin = Guid.NewGuid().ToString().Substring(0, 10);

            Single squeezeHeight = (Single)rand.NextDouble();
            while (squeezeHeight < 0.01)
            {
                squeezeHeight = (Single)rand.NextDouble();
            }
            prod.SqueezeHeight = squeezeHeight;

            Single squeezeWidth = (Single)rand.NextDouble();
            while (squeezeWidth < 0.01)
            {
                squeezeWidth = (Single)rand.NextDouble();
            }
            prod.SqueezeWidth = squeezeWidth;

            Single squeezeDepth = (Single)rand.NextDouble();
            while (squeezeDepth < 0.01)
            {
                squeezeDepth = (Single)rand.NextDouble();
            }
            prod.SqueezeDepth = squeezeDepth;

            //save
            prod = prod.Save();

            //fetch back the dto
            ProductDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dto = dal.FetchById(prod.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, prod,_propertiesToExclude);
        }

        [Test]
        public void DataAccess_Update()
        {
            Random rand = new Random();

            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityList[0].Id);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //create a new item 
            var prod = Product.NewProduct(entityList[0].Id);

            //set properties
            prod.Name = "prod1";
            prod.Gtin = Guid.NewGuid().ToString().Substring(0, 10);

            Single squeezeHeight = (Single)rand.NextDouble();
            while (squeezeHeight < 0.01)
            {
                squeezeHeight = (Single)rand.NextDouble();
            }
            prod.SqueezeHeight = squeezeHeight;

            Single squeezeWidth = (Single)rand.NextDouble();
            while (squeezeWidth < 0.01)
            {
                squeezeWidth = (Single)rand.NextDouble();
            }
            prod.SqueezeWidth = squeezeWidth;

            Single squeezeDepth = (Single)rand.NextDouble();
            while (squeezeDepth < 0.01)
            {
                squeezeDepth = (Single)rand.NextDouble();
            }
            prod.SqueezeDepth = squeezeDepth;

            //save
            prod = prod.Save();

            //update
            prod.Name = "prod2";
            prod.Gtin = Guid.NewGuid().ToString().Substring(0, 10);

            squeezeHeight = (Single)rand.NextDouble();
            while (squeezeHeight < 0.01)
            {
                squeezeHeight = (Single)rand.NextDouble();
            }
            prod.SqueezeHeight = squeezeHeight;

            squeezeWidth = (Single)rand.NextDouble();
            while (squeezeWidth < 0.01)
            {
                squeezeWidth = (Single)rand.NextDouble();
            }
            prod.SqueezeWidth = squeezeWidth;

            squeezeDepth = (Single)rand.NextDouble();
            while (squeezeDepth < 0.01)
            {
                squeezeDepth = (Single)rand.NextDouble();
            }
            prod.SqueezeDepth = squeezeDepth;

            //save
            prod = prod.Save();

            //fetch back the dto
            ProductDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dto = dal.FetchById(prod.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(
                dto, 
                prod, 
                _propertiesToExclude.Union(new List<String> { "DateCreated", "DateLastModified", "DateDeleted" }).ToList());
        }

        [Test]
        public void DataAccess_Delete()
        {
            Random rand = new Random();

            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityList[0].Id);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //create a new item 
            var prod = Product.NewProduct(entityList[0].Id);

            //set properties
            prod.Name = "prod1";
            prod.Gtin = Guid.NewGuid().ToString().Substring(0, 10);

            Single squeezeHeight = (Single)rand.NextDouble();
            while (squeezeHeight < 0.01)
            {
                squeezeHeight = (Single)rand.NextDouble();
            }
            prod.SqueezeHeight = squeezeHeight;

            Single squeezeWidth = (Single)rand.NextDouble();
            while (squeezeWidth < 0.01)
            {
                squeezeWidth = (Single)rand.NextDouble();
            }
            prod.SqueezeWidth = squeezeWidth;

            Single squeezeDepth = (Single)rand.NextDouble();
            while (squeezeDepth < 0.01)
            {
                squeezeDepth = (Single)rand.NextDouble();
            }
            prod.SqueezeDepth = squeezeDepth;

            //save
            prod = prod.Save();


            //fetch back the dto
            ProductDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dto = dal.FetchById(prod.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, prod,_propertiesToExclude);

            //delete it
            Int32 itemId = prod.Id;
            prod.Delete();
            prod.Save();


            //try to retrieve again 
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dto = dal.FetchById(itemId);
                    Assert.IsNotNull(dto.DateDeleted);
                    //Assert.Throws(
                    //    typeof(DtoDoesNotExistException),
                    //    () =>
                    //    {
                    //        dto = dal.FetchById(itemId);
                    //    });
                }
            }
        }

        #endregion

        [Test]
        public void GetImageData_ThrowsWhenArgumentsNotMatchProductEnum()
        {
            var product = Product.NewProduct();
            foreach (var imageTypeName in Enum.GetNames(typeof(PlanogramImageType)))
            {
                foreach (var facingTypeName in Enum.GetNames(typeof(PlanogramImageFacingType)))
                {
                    ProductImageType imageType;
                    ProductImageFacing facingType;
                    if (!Enum.TryParse<ProductImageType>(imageTypeName, out imageType) ||
                        !Enum.TryParse<ProductImageFacing>(facingTypeName, out facingType))
                    {
                        Assert.IsNull(product.GetImageData(
                            (PlanogramImageType)Enum.Parse(typeof(PlanogramImageType), imageTypeName),
                            (PlanogramImageFacingType)Enum.Parse(typeof(PlanogramImageFacingType), facingTypeName)));
                    }
                }
            }
        }

        [Test]
        public void GetImageData_ReturnsCorrectData()
        {
            var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).FirstOrDefault().Id;
            var product = Product.NewProduct(entityId);
            var file = File.NewFile(entityId, System.IO.Path.Combine("Environment","TestImage.png"), null);
            var image = Image.NewImage(file);
            var originalImageData = file.FileBlob.Data;

            foreach (var imageTypeName in Enum.GetNames(typeof(PlanogramImageType)))
            {
                foreach (var facingTypeName in Enum.GetNames(typeof(PlanogramImageFacingType)))
                {
                    product.ImageList.Clear();
                    
                    // Convert the combination of valid PlanogramImage enums to a combination of 
                    // ProductImage enums, continuing if a match is not available.
                    ProductImageType imageType;
                    ProductImageFacing facingType;
                    if (!Enum.TryParse<ProductImageType>(imageTypeName, out imageType)) continue;
                    if (!Enum.TryParse<ProductImageFacing>(facingTypeName, out facingType)) continue;
                    var productImageImageFacingType = new ProductImageImageFacingType
                        {
                            FacingType = facingType,
                            ImageType = imageType
                        };

                    // Add the valid Image to product.
                    var productImage = ProductImage.NewProductImage(image,productImageImageFacingType,product.Id);
                    product.ImageList.Add(productImage);

                    // Attempt to retrieve image.
                    var imageData = product.GetImageData(
                        (PlanogramImageType)Enum.Parse(typeof(PlanogramImageType),imageTypeName),
                        (PlanogramImageFacingType)Enum.Parse(typeof(PlanogramImageFacingType),facingTypeName));                    
                    CollectionAssert.AreEqual(originalImageData, imageData);
                }
            }
        }

        private Product CreateProduct(ProductImageFacing facingType, ProductImageType imageType)
        {
            var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).FirstOrDefault().Id;
            var product = Product.NewProduct(entityId);
            product.Name = "ProductName";
            product.Gtin = "ProductGtin";
            var file = File.NewFile(entityId, System.IO.Path.Combine("Environment","TestImage.png"), null);

            product.ImageList.Add(ProductImage.NewProductImage(
                Image.NewImage(file),
                new ProductImageImageFacingType()
                {
                    FacingType = facingType,
                    ImageType = imageType
                },
                product.Id));
            product.Save();
            return product;
        }
    }
}
