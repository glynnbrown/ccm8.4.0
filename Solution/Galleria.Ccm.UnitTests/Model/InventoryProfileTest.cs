﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26124 : I.George
//		Created
// CCM-30759 : J.Pickup
//		InventoryProfileType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class InventoryProfileTest : TestBase
    {
        #region Helper Methods
        /// <summary>
        /// Assert that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">the dto to compare</param>
        /// <param name="model">the model to compare</param>
        public static void AssertDtoAndModelAreEqual(InventoryProfileDto dto, InventoryProfile model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.CasePack, model.CasePack);
            Assert.AreEqual(dto.DaysOfSupply, model.DaysOfSupply);
            Assert.AreEqual(dto.ShelfLife, model.ShelfLife);
            Assert.AreEqual(dto.ReplenishmentDays, model.ReplenishmentDays);
            Assert.AreEqual(dto.WasteHurdleUnits, model.WasteHurdleUnits);
            Assert.AreEqual(dto.WasteHurdleCasePack, model.WasteHurdleCasePack);
            Assert.AreEqual(dto.MinUnits, model.MinUnits);
            Assert.AreEqual(dto.MinFacings, model.MinFacings);
            Assert.AreEqual((Byte)dto.InventoryProfileType, (Byte)model.InventoryProfileType);
        }
        /// <summary>
        /// Assert that the properties of a model 
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">the dto to compare</param>
        /// <param name="model">the model to compare</param>
        public static void AssertDtoAndModelAreEqual(InventoryProfile dto, InventoryProfile model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.CasePack, model.CasePack);
            Assert.AreEqual(dto.DaysOfSupply, model.DaysOfSupply);
            Assert.AreEqual(dto.ShelfLife, model.ShelfLife);
            Assert.AreEqual(dto.ReplenishmentDays, model.ReplenishmentDays);
            Assert.AreEqual(dto.WasteHurdleUnits, model.WasteHurdleUnits);
            Assert.AreEqual(dto.WasteHurdleCasePack, model.WasteHurdleCasePack);
            Assert.AreEqual(dto.MinUnits, model.MinUnits);
            Assert.AreEqual(dto.MinFacings, model.MinFacings);
            Assert.AreEqual((Byte)dto.InventoryProfileType, (Byte)model.InventoryProfileType);
        }
     
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            InventoryProfile model = InventoryProfile.NewInventoryProfile(1);
            Serialize(model);
        }

        #endregion

        #region PropertySetters

        /// <summary>
        /// Property setters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            //model to set on
            InventoryProfile model = InventoryProfile.NewInventoryProfile(1);
            TestPropertySetters<InventoryProfile>(model);
        }

        #endregion

        #region Business Rule
        /// <summary>
        /// Name Required
        /// </summary>
        [Test]
        public void NameRequired()
        {
            //create a new model
            InventoryProfile model = InventoryProfile.NewInventoryProfile(1);

            //set a valid name
            model.Name = "Valid Name";
            Assert.AreEqual(model.IsValid, true);

            //set an invalid name
            model.Name = String.Empty;
            Assert.AreEqual(model.IsValid, false);


            //set a valid name
            model.Name = "Valid Name";
            Assert.AreEqual(model.IsValid, true);
        }

        /// <summary>
        /// Name Property Maximum length
        /// </summary>
        [Test]
        public void NamePropertyMaxLength()
        {
            //create a new model
            InventoryProfile model = InventoryProfile.NewInventoryProfile(1);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = new String('*', 101);
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }
        #endregion

        #region Factory Methods
        [Test]
        public void NewInventoryProfile()
        {
            Int32 entityId = 1;
            InventoryProfile newInventoryProfile = InventoryProfile.NewInventoryProfile(entityId);
            Assert.AreEqual(newInventoryProfile.EntityId, entityId);
            Assert.IsTrue(newInventoryProfile.IsNew);
        }

        [Test]
        public void FetchById()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<InventoryProfileDto> inventoryProfileDtos = TestDataHelper.InsertInventoryProfileDtos(base.DalFactory, entityId, 15);

            foreach (InventoryProfileDto dto in inventoryProfileDtos)
            {
                InventoryProfile model = InventoryProfile.FetchById(dto.Id);

                AssertDtoAndModelAreEqual(dto, model);
            }
        }

        [Test]
        public void FetchByEntityIdName()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<InventoryProfileDto> inventoryProfileDtos = TestDataHelper.InsertInventoryProfileDtos(base.DalFactory, entityId, 15);

            foreach (InventoryProfileDto dto in inventoryProfileDtos)
            {
                InventoryProfile model = InventoryProfile.FetchByEntityIdName(dto.EntityId, dto.Name);

                AssertDtoAndModelAreEqual(dto, model);
            }
        }
        #endregion

        #region DataAccess
        [Test]
        public void DataPortal_InsertUpdateDelete()
        {
            List<EntityDto> entities = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entities[0].Id;

            InventoryProfile item = InventoryProfile.NewInventoryProfile(entityId);
            item.Name = "name";

            //insert
            item = item.Save();

            InventoryProfileDto dto;

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //update
            item.Name = "name1";
            item = item.Save();

            using(IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //update again to check concurrency
            item.Name = "name2";
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);
            //delete
            Int32 itemId = item.Id;
            item.Delete();
            item.Save();

            using(IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    dto = dal.FetchById(itemId);

                    Assert.IsNotNull(dto.DateDeleted, "date should be marked as deleted");
                }
            }
        }
        #endregion
    }
}
