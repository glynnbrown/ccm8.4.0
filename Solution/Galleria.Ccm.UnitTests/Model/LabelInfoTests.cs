﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class LabelInfoTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Assert that the properties of a model 
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">the dto to compare</param>
        /// <returns>The model to compare</returns>
        public static LabelInfoDto GetInfoDto(LabelDto dto)
        {
            return new LabelInfoDto()
            {
                Name = dto.Name,
                Id = dto.Id
            };
        }

        public static void AssertDtoAndModelAreEqual(LabelInfoDto dto, LabelInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
        }

        public static void AssertDtoAndModelAreEqual(LabelDto dto, LabelInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<Object> ids = TestDataHelper.InsertLabelDtos(base.DalFactory, 20, entityId).Select(o => o.Id).ToList();

            //fetch for the list
            LabelInfoList infoList = LabelInfoList.FetchByIds(ids);

            LabelInfo model = infoList[0];

            Serialize(model);
        }
        #endregion


        #region FactoryMethods

        [Test]
        public void FetchLabelInfo()
        {
            
            Label model1 = Label.NewLabel(LabelType.Product);
            PopulatePropertyValues1(model1, new List<String> { "Id" });

            model1 = model1.Save();
            model1.Dispose();

            LabelInfo model2;
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            LabelInfoList infoList = LabelInfoList.FetchByIds(new List<Object>() { model1.Id });
            model2 = infoList[0];

            //Check properties
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.Name, model2.Name);
        }

        #endregion

    }
}
