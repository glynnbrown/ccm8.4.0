﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25395 A.Probyn
//  Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ProductLibraryTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(ProductLibrary.NewProductLibrary());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<ProductLibrary>(ProductLibrary.NewProductLibrary());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void CreateNew()
        {
            TestCreateNew<ProductLibrary>(
                /*child*/false,
                null,
                new Object[] { ProductLibraryDalFactoryType.Unknown },
                null);
        }

        [Test]
        public void Fetch()
        {
            //insert a dto to fetch
            ProductLibraryDto dto = new ProductLibraryDto();
            PopulatePropertyValues1<ProductLibraryDto>(dto);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                {
                    dal.Insert(dto);
                }
            }

            //fetch
            ProductLibrary model = ProductLibrary.FetchById(dto.Id);

            //check details
            AssertDtoAndModelAreEqual<ProductLibraryDto, ProductLibrary>(dto, model);
        }

        #endregion

        #region Data Access

        [Test]
        public void Insert()
        {
            ProductLibrary model1 = ProductLibrary.NewProductLibrary();
            PopulatePropertyValues1(model1, new List<String> { "Id" });

            model1 = model1.Save();
            model1.Dispose();

            //refetch
            ProductLibrary model2 = ProductLibrary.FetchById(model1.Id);

            AssertModelsAreEqual<ProductLibrary>(model1, model2);

        }

        [Test]
        public void Update()
        {
            ProductLibrary model = ProductLibrary.NewProductLibrary();
            PopulatePropertyValues1<ProductLibrary>(model, new List<String> { "Id" });

            model = model.Save();

            PopulatePropertyValues2<ProductLibrary>(model, new List<String> { "Id" });

            model = model.Save();
            model.Dispose();

            //refetch
            ProductLibrary model2 = ProductLibrary.FetchById(model.Id);

            AssertModelsAreEqual<ProductLibrary>(model, model2);
        }

        [Test]
        public void Delete()
        {
            ProductLibrary model = ProductLibrary.NewProductLibrary();
            PopulatePropertyValues1<ProductLibrary>(model, new List<String> { "Id" });

            model = model.Save();

            //refetch
            model = ProductLibrary.FetchById(model.Id);

            //delete
            model.Delete();
            model.Save();

            //refetch
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                {
                    ProductLibraryDto dto;
                    Assert.Throws(typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(model.Id);
                        },
                        "model should no longer exist"
                    );
                }
            }
        }

        #endregion

    }
}
