﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
// CCM-25449 : N.Haywood
//      Fixed broken test
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Csla;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ClusterLocationTests : TestBase
    {
        #region Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(
            ClusterLocationDto clusterLocDto,
            Galleria.Ccm.Model.ClusterLocation model)
        {
            Assert.AreEqual(14, typeof(ClusterLocationDto).GetProperties().Count());

            Assert.AreEqual(clusterLocDto.Id, model.Id);
            //dtokey - not in model
            //clusterid - not in model
            Assert.AreEqual(clusterLocDto.LocationId, model.LocationId);
            Assert.AreEqual(clusterLocDto.Address1, model.Address1);
            Assert.AreEqual(clusterLocDto.Address2, model.Address2);
            Assert.AreEqual(clusterLocDto.City, model.City);
            Assert.AreEqual(clusterLocDto.Code, model.Code);
            Assert.AreEqual(clusterLocDto.Country, model.Country);
            Assert.AreEqual(clusterLocDto.County, model.County);
            Assert.AreEqual(clusterLocDto.Name, model.Name);
            Assert.AreEqual(clusterLocDto.PostalCode, model.PostalCode);
            Assert.AreEqual(clusterLocDto.Region, model.Region);
            Assert.AreEqual(clusterLocDto.TVRegion, model.TVRegion);
            //DateCreated - not in model
            //DateLastModified - not in model
            //DateDeleted - not in model
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //insert entities and locations
            Galleria.Ccm.Model.Entity entity = Galleria.Ccm.Model.Entity.NewEntity();
            entity.Name = "ent";
            entity = entity.Save();
            Int32 entityId = entity.Id;
            //ApplicationContext.GlobalContext[Constants.EntityId] = entityId;
            TestDataHelper.PopulateLocationHierarchy(base.DalFactory, entityId);
            Galleria.Ccm.Model.LocationInfoList locationInfos =
                Galleria.Ccm.Model.LocationInfoList.FetchByEntityId(entityId);

            Galleria.Ccm.Model.ClusterLocation model =
                Galleria.Ccm.Model.ClusterLocation.NewClusterLocation(locationInfos[0]);
            Serialize(model);
        }
        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            //insert entities and locations
            Galleria.Ccm.Model.Entity entity = Galleria.Ccm.Model.Entity.NewEntity();
            entity.Name = "ent";
            entity = entity.Save();
            Int32 entityId = entity.Id;
            //ApplicationContext.GlobalContext[Constants.EntityId] = entityId;
            TestDataHelper.PopulateLocationHierarchy(base.DalFactory, entityId);
            Galleria.Ccm.Model.LocationInfoList locationInfos =
                Galleria.Ccm.Model.LocationInfoList.FetchByEntityId(entityId);

            // create a new model
            Galleria.Ccm.Model.ClusterLocation model =
                Galleria.Ccm.Model.ClusterLocation.NewClusterLocation(locationInfos[0]);

            TestPropertySetters<Galleria.Ccm.Model.ClusterLocation>(model);
        }

        #endregion

        #region Factory Methods


        [Test]
        public void NewClusterLocation()
        {
            //FromLocationInfo

            Galleria.Ccm.Model.Location loc =
                Galleria.Ccm.Model.Location.NewLocation( 1);
            loc.Name = "loc1";
            loc.Code = "loccode";
            loc.Address1 = "address1";
            loc.Address2 = "address2";
            loc.City = "city";
            loc.County = "country";
            loc.PostalCode = "postalcode";
            loc.TVRegion = "tvregion";
            loc.LocationGroupId = 1;


            loc = loc.Save();

            Galleria.Ccm.Model.LocationInfoList infoList =
                Galleria.Ccm.Model.LocationInfoList.FetchByEntityId(1);
            Galleria.Ccm.Model.LocationInfo locInfo = infoList[0];

            // create a new model
            Galleria.Ccm.Model.ClusterLocation model =
                Galleria.Ccm.Model.ClusterLocation.NewClusterLocation(locInfo);

            //check the model has been created
            Assert.IsTrue(model.IsChild);

            Assert.AreEqual(locInfo.Id, model.LocationId);
            Assert.AreEqual(locInfo.Name, model.Name);
            Assert.AreEqual(locInfo.Code, model.Code);
            Assert.AreEqual(locInfo.Address1, model.Address1);
            Assert.AreEqual(locInfo.Address2, model.Address2);
            Assert.AreEqual(locInfo.City, model.City);
            Assert.AreEqual(locInfo.Country, model.Country);
            Assert.AreEqual(locInfo.PostalCode, model.PostalCode);
            Assert.AreEqual(locInfo.TVRegion, model.TVRegion);
        }

        [Test]
        public void FetchClusterLocation()
        {
            //insert entities and locations
            Galleria.Ccm.Model.Entity entity = Galleria.Ccm.Model.Entity.NewEntity();
            entity.Name = "ent";
            entity = entity.Save();
            Int32 entityId = entity.Id;
            //ApplicationContext.GlobalContext[Constants.EntityId] = entityId;
            TestDataHelper.PopulateLocationHierarchy(base.DalFactory, entityId);

            Galleria.Ccm.Model.ClusterScheme parentScheme =
                Galleria.Ccm.Model.ClusterScheme.NewClusterScheme(1);
            parentScheme.Name = "parentScheme";

            Galleria.Ccm.Model.Cluster parentCluster =
                Galleria.Ccm.Model.Cluster.NewCluster();
            parentCluster.Name = "parentCluster";
            parentScheme.Clusters.Add(parentCluster);

            Galleria.Ccm.Model.LocationInfoList locationInfos =
                Galleria.Ccm.Model.LocationInfoList.FetchByEntityId(entityId);

            //create a new item
            Galleria.Ccm.Model.ClusterLocation item =
                Galleria.Ccm.Model.ClusterLocation.NewClusterLocation(locationInfos[0]);
            parentCluster.Locations.Add(item);

            //insert
            parentScheme = parentScheme.Save();
            item = parentScheme.Clusters[0].Locations[0];

            //refetch
            parentScheme = Galleria.Ccm.Model.ClusterScheme.FetchById(parentScheme.Id);
            item = parentScheme.Clusters[0].Locations[0];

            ClusterLocationDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);
        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_ChildInsertUpdateDelete()
        {
            //insert entities and locations
            Galleria.Ccm.Model.Entity entity = Galleria.Ccm.Model.Entity.NewEntity();
            entity.Name = "ent";
            entity = entity.Save();
            Int32 entityId = entity.Id;
            TestDataHelper.PopulateLocationHierarchy(base.DalFactory, entityId);

            Galleria.Ccm.Model.ClusterScheme parentScheme =
                Galleria.Ccm.Model.ClusterScheme.NewClusterScheme(1);
            parentScheme.Name = "parentScheme";

            Galleria.Ccm.Model.Cluster parentCluster =
                Galleria.Ccm.Model.Cluster.NewCluster();
            parentCluster.Name = "parentCluster";
            parentScheme.Clusters.Add(parentCluster);

            Galleria.Ccm.Model.LocationInfoList locationInfos =
                Galleria.Ccm.Model.LocationInfoList.FetchByEntityId(entityId);

            //create a new item
            Galleria.Ccm.Model.ClusterLocation item =
                Galleria.Ccm.Model.ClusterLocation.NewClusterLocation(locationInfos[0]);
            parentCluster.Locations.Add(item);

            //insert
            parentScheme = parentScheme.Save();
            item = parentScheme.Clusters[0].Locations[0];

            ClusterLocationDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            // no properties to update


            //remove from the parent
            Int32 itemId = item.Id;
            parentScheme.Clusters[0].Locations.Remove(item);
            parentScheme = parentScheme.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }

        }

        #endregion
    }
}
