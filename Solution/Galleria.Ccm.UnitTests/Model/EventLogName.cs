﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class EventLogName : TestBase
    {
        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(EventLogNameDto dto, Galleria.Ccm.Model.EventLogName model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
        }

        private void AssertModelandModelAreEqual(Galleria.Ccm.Model.EventLogName model1, Galleria.Ccm.Model.EventLogName model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.Name, model2.Name);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.EventLogName model =
                Galleria.Ccm.Model.EventLogName.NewEventLogName();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySettersGeneric()
        {
            // create a new model for rand values
            Galleria.Ccm.Model.EventLogName model = Galleria.Ccm.Model.EventLogName.NewEventLogName();

            TestBase.TestPropertySetters<Galleria.Ccm.Model.EventLogName>(model);
        }

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model for rand values
            Galleria.Ccm.Model.EventLogName valueModel =
                Galleria.Ccm.Model.EventLogName.NewEventLogName();

            //name
            String testName = Guid.NewGuid().ToString();
            valueModel.Name = testName;
            Assert.AreEqual(testName, valueModel.Name);

        }
        #endregion

        #region FactoryMethods

        [Test]
        public void NewEventLogName()
        {

            Galleria.Ccm.Model.EventLogName newEventLogName =
                Galleria.Ccm.Model.EventLogName.NewEventLogName();

            Assert.IsTrue(newEventLogName.IsNew);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_InsertFetchById()
        {

            //create a new item 
            Galleria.Ccm.Model.EventLogName eventLogName =
                Galleria.Ccm.Model.EventLogName.NewEventLogName();

            eventLogName.Name = "NameTest";

            //save
            eventLogName = eventLogName.Save();

            //fetch back the dto
            EventLogNameDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                {
                    dto = dal.FetchById(eventLogName.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, eventLogName);
        }

        [Test]
        public void DataAccess_InsertFetchByName()
        {
            //create a new item 
            Galleria.Ccm.Model.EventLogName eventLogName =
                Galleria.Ccm.Model.EventLogName.NewEventLogName();

            eventLogName.Name = "NameTest";

            //save
            eventLogName = eventLogName.Save();

            //fetch back the dto
            EventLogNameDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                {
                    dto = dal.FetchByName(eventLogName.Name);
                }
            }
            Assert.AreEqual(dto.Name, eventLogName.Name);
        }

        #endregion
    }
}
