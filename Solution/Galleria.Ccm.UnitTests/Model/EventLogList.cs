﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion
#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName, WorkpackageId property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class EventLogList : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.EventLogList model =
                Galleria.Ccm.Model.EventLogList.NewList();
            Serialize(model);
        }
        #endregion

        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(EventLogDto dto, Galleria.Ccm.Model.EventLog model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.EventLogNameId, model.EventLogNameId);
            Assert.AreEqual(dto.Source, model.Source);
            Assert.AreEqual(dto.EventId, model.EventId);
            Assert.AreEqual(dto.EntryType, (Int16)model.EntryType);
            Assert.AreEqual(dto.UserId, model.UserId);
            Assert.AreEqual(dto.Process, model.Process);
            Assert.AreEqual(dto.ComputerName, model.ComputerName);
            Assert.AreEqual(dto.URLHelperLink, model.UrlHelperLink);
            Assert.AreEqual(dto.Description, model.Description);
            Assert.AreEqual(dto.Content, model.Content);
            Assert.AreEqual(dto.WorkpackageId, model.WorkpackageId);
            Assert.AreEqual(dto.WorkpackageName, model.WorkpackageName);
        }

        private void AssertModelandModelAreEqual(Galleria.Ccm.Model.EventLog model1, Galleria.Ccm.Model.EventLog model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.EntityId, model2.EntityId);
            Assert.AreEqual(model1.EventLogNameId, model2.EventLogNameId);
            Assert.AreEqual(model1.Source, model2.Source);
            Assert.AreEqual(model1.EventId, model2.EventId);
            Assert.AreEqual(model1.EntryType, model2.EntryType);
            Assert.AreEqual(model1.UserId, model2.UserId);
            Assert.AreEqual(model1.Process, model2.Process);
            Assert.AreEqual(model1.ComputerName, model2.ComputerName);
            Assert.AreEqual(model1.UrlHelperLink, model2.UrlHelperLink);
            Assert.AreEqual(model1.Description, model2.Description);
            Assert.AreEqual(model1.Content, model2.Content);
            Assert.AreEqual(model1.WorkpackageId, model2.WorkpackageId);
            Assert.AreEqual(model1.WorkpackageName, model2.WorkpackageName);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewList()
        {
            Galleria.Ccm.Model.EventLogList model = Galleria.Ccm.Model.EventLogList.NewList();

            Assert.IsFalse(model.IsChild, "the model should be a root");
            Assert.IsEmpty(model, "the model should be empty");
        }

        [Test]
        public void FetchAll()
        {
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);
                List<EventLogNameDto> eventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 5);
                List<EventLogDto> eventLogHelperList = TestDataHelper.InsertEventLogDtos(dalContext, 5, entityList, eventLogNameList);
                Galleria.Ccm.Model.EventLogList eventLogList = Galleria.Ccm.Model.EventLogList.FetchAll();

                Assert.AreEqual(eventLogHelperList.Count, eventLogList.Count);

                foreach (EventLogDto EventLogDto in eventLogHelperList)
                {
                    Ccm.Model.EventLog eventLog = eventLogList.FirstOrDefault(i => i.Id == EventLogDto.Id);
                    AssertDtoAndModelAreEqual(EventLogDto, eventLog);
                }
            }
        }

        [Test]
        public void FetchByEventLogNameId()
        {
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                Int32 currentEventLogId = 1;
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);
                List<EventLogNameDto> eventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 5);
                List<EventLogDto> eventLogHelperList = TestDataHelper.InsertEventLogDtos(dalContext, 5, entityList, eventLogNameList);
                Galleria.Ccm.Model.EventLogList eventLogList = Galleria.Ccm.Model.EventLogList.FetchByEventLogNameId(currentEventLogId);

                Assert.AreEqual(eventLogList.Count, 25);

                foreach (EventLogDto EventLogDto in eventLogHelperList)
                {
                    if (EventLogDto.Id == currentEventLogId)
                    {
                        Ccm.Model.EventLog eventLog = eventLogList.FirstOrDefault(i => i.Id == EventLogDto.Id);
                        AssertDtoAndModelAreEqual(EventLogDto, eventLog);
                    }
                }
            }

        }

        [Test]
        public void FetchByEventLogNameIdEntryTypeEntityId()
        {
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                Int32 currentEntityID = 1;
                Int32 currentEventLogId = 1;

                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);
                List<EventLogNameDto> eventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 5);
                List<EventLogDto> eventLogHelperList = TestDataHelper.InsertEventLogDtos(dalContext, 5, entityList, eventLogNameList);

                Galleria.Ccm.Model.EventLogList eventLogList = Galleria.Ccm.Model.EventLogList.FetchByEventLogNameIdEntryTypeEntityId(100, currentEventLogId, null, currentEntityID);

                Assert.AreEqual(eventLogList.Count, 5);

                foreach (EventLogDto EventLogDto in eventLogHelperList)
                {
                    if (EventLogDto.Id == currentEventLogId)
                    {
                        Ccm.Model.EventLog eventLog = eventLogList.FirstOrDefault(i => i.Id == EventLogDto.Id);
                        AssertDtoAndModelAreEqual(EventLogDto, eventLog);
                    }
                }
            }
        }


        [Test]
        public void FetchByMixedCriteria()
        {
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                Int32 currentEntityID = 1;
                Int32 currentEventLogId = 1;
                String currentProcess = "ProcessTest3";

                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);
                List<EventLogNameDto> eventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 5);
                List<EventLogDto> eventLogHelperList = TestDataHelper.InsertEventLogDtos(dalContext, 5, entityList, eventLogNameList);

                Galleria.Ccm.Model.EventLogList eventLogList
                    = Galleria.Ccm.Model.EventLogList.FetchByMixedCriteria(100, currentEventLogId, null, null, null, null, currentEntityID, null, currentProcess, null, null, null, null, null, DateTime.UtcNow.AddYears(-1), DateTime.UtcNow, null, null);

                Assert.AreEqual(eventLogList.Count, 5);

                foreach (EventLogDto EventLogDto in eventLogHelperList)
                {
                    if (EventLogDto.Id == currentEventLogId)
                    {
                        Ccm.Model.EventLog eventLog = eventLogList.FirstOrDefault(i => i.Id == EventLogDto.Id);
                        AssertDtoAndModelAreEqual(EventLogDto, eventLog);
                    }
                }
            }
        }

        [Test]
        public void FetchByWorkpackageId()
        {
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                Int32 currentEventLogId = 1;
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);
                List<EventLogNameDto> eventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 2);
                var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First();
                List<WorkflowDto> workflowList = TestDataHelper.InsertWorkflowDtos(dalContext, entityList, 2);
                List<WorkpackageDto> workpackageList = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowList, userName);
                List<EventLogDto> eventLogHelperList = TestDataHelper.InsertEventLogDtos(dalContext, 5, entityList, eventLogNameList, workpackageList);
                Assert.AreEqual(80, eventLogHelperList.Count);
                
                Galleria.Ccm.Model.EventLogList eventLogList = Galleria.Ccm.Model.EventLogList.FetchByWorkPackageId(workflowList[0].Id);
                Assert.AreEqual(eventLogList.Count, 20);

                foreach (EventLogDto EventLogDto in eventLogHelperList.Where(p => p.WorkpackageId == workflowList[0].Id))
                {
                    if (EventLogDto.Id == currentEventLogId)
                    {
                        Ccm.Model.EventLog eventLog = eventLogList.FirstOrDefault(i => i.Id == EventLogDto.Id);
                        AssertDtoAndModelAreEqual(EventLogDto, eventLog);
                    }
                }

                eventLogList = Galleria.Ccm.Model.EventLogList.FetchByWorkPackageId(workflowList[1].Id);
                Assert.AreEqual(eventLogList.Count, 20);

                foreach (EventLogDto EventLogDto in eventLogHelperList.Where(p => p.WorkpackageId == workflowList[1].Id))
                {
                    if (EventLogDto.Id == currentEventLogId)
                    {
                        Ccm.Model.EventLog eventLog = eventLogList.FirstOrDefault(i => i.Id == EventLogDto.Id);
                        AssertDtoAndModelAreEqual(EventLogDto, eventLog);
                    }
                }
            }

        }

        #endregion
    }
}
