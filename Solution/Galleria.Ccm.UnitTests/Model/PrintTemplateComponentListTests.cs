﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-30738 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PrintTemplateComponentListTests : TestBase<PrintTemplateComponent, PrintTemplateComponentDto>
    {
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerializeList();
        }

        [Test]
        public void NewFactoryMethod()
        {
            TestNewListFactoryMethod<PrintTemplateComponentList>();
        }


    }
}
