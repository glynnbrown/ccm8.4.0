﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-25628 : A.Kuszyk
//		Created (Copied from SA)
// V8-25748 : K.Pickup
//      Changed data type of LocationId to Int16.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationTests : TestBase
    {
        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(LocationDto dto, Location model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.LocationGroupId, model.LocationGroupId);
            Assert.AreEqual(dto.Code, model.Code);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.Region, model.Region);
            Assert.AreEqual(dto.County, model.County);
            Assert.AreEqual(dto.TVRegion, model.TVRegion);
            Assert.AreEqual(dto.State, model.State);
            Assert.AreEqual(dto.Location, model.LocationAttribute);
            Assert.AreEqual(dto.DefaultClusterAttribute, model.DefaultClusterAttribute);
            Assert.AreEqual(dto.Address1, model.Address1);
            Assert.AreEqual(dto.Address2, model.Address2);
            Assert.AreEqual(dto.City, model.City);
            Assert.AreEqual(dto.PostalCode, model.PostalCode);
            Assert.AreEqual(dto.Country, model.Country);
            Assert.AreEqual(dto.Latitude, model.Latitude);
            Assert.AreEqual(dto.Longitude, model.Longitude);
            Assert.AreEqual(dto.DateOpen, model.DateOpen);
            Assert.AreEqual(dto.DateLastRefitted, model.DateLastRefitted);
            Assert.AreEqual(dto.DateClosed, model.DateClosed);
            Assert.AreEqual(dto.CarParkSpaces, model.CarParkSpaces);
            Assert.AreEqual(dto.CarParkManagement, model.CarParkManagement);
            Assert.AreEqual(dto.PetrolForecourtType, model.PetrolForecourtType);
            Assert.AreEqual(dto.Restaurant, model.Restaurant);
            Assert.AreEqual(dto.SizeGrossFloorArea, model.SizeGrossFloorArea);
            Assert.AreEqual(dto.SizeNetSalesArea, model.SizeNetSalesArea);
            Assert.AreEqual(dto.SizeMezzSalesArea, model.SizeMezzSalesArea);
            Assert.AreEqual(dto.TelephoneCountryCode, model.TelephoneCountryCode);
            Assert.AreEqual(dto.TelephoneAreaCode, model.TelephoneAreaCode);
            Assert.AreEqual(dto.TelephoneNumber, model.TelephoneNumber);
            Assert.AreEqual(dto.FaxCountryCode, model.FaxCountryCode);
            Assert.AreEqual(dto.FaxAreaCode, model.FaxAreaCode);
            Assert.AreEqual(dto.FaxNumber, model.FaxNumber);
            Assert.AreEqual(dto.OpeningHours, model.OpeningHours);
            Assert.AreEqual(dto.AverageOpeningHours, model.AverageOpeningHours);
            Assert.AreEqual(dto.ManagerName, model.ManagerName);
            Assert.AreEqual(dto.RegionalManagerName, model.RegionalManagerName);
            Assert.AreEqual(dto.DivisionalManagerName, model.DivisionalManagerName);
            Assert.AreEqual(dto.ManagerEmail, model.ManagerEmail);
            Assert.AreEqual(dto.RegionalManagerEmail, model.RegionalManagerEmail);
            Assert.AreEqual(dto.DivisionalManagerEmail, model.DivisionalManagerEmail);
            Assert.AreEqual(dto.AdvertisingZone, model.AdvertisingZone);
            Assert.AreEqual(dto.DistributionCentre, model.DistributionCentre);
            Assert.AreEqual(dto.NoOfCheckouts, model.NoOfCheckouts);
            Assert.AreEqual(dto.IsMezzFitted, model.IsMezzFitted);
            Assert.AreEqual(dto.IsFreehold, model.IsFreehold);
            Assert.AreEqual(dto.Is24Hours, model.Is24Hours);
            Assert.AreEqual(dto.IsOpenMonday, model.IsOpenMonday);
            Assert.AreEqual(dto.IsOpenTuesday, model.IsOpenTuesday);
            Assert.AreEqual(dto.IsOpenWednesday, model.IsOpenWednesday);
            Assert.AreEqual(dto.IsOpenThursday, model.IsOpenThursday);
            Assert.AreEqual(dto.IsOpenFriday, model.IsOpenFriday);
            Assert.AreEqual(dto.IsOpenSaturday, model.IsOpenSaturday);
            Assert.AreEqual(dto.IsOpenSunday, model.IsOpenSunday);
            Assert.AreEqual(dto.HasPetrolForecourt, model.HasPetrolForecourt);
            Assert.AreEqual(dto.HasNewsCube, model.HasNewsCube);
            Assert.AreEqual(dto.HasAtmMachines, model.HasAtmMachines);
            Assert.AreEqual(dto.HasCustomerWC, model.HasCustomerWC);
            Assert.AreEqual(dto.HasBabyChanging, model.HasBabyChanging);
            Assert.AreEqual(dto.HasInStoreBakery, model.HasInStoreBakery);
            Assert.AreEqual(dto.HasHotFoodToGo, model.HasHotFoodToGo);
            Assert.AreEqual(dto.HasRotisserie, model.HasRotisserie);
            Assert.AreEqual(dto.HasFishmonger, model.HasFishmonger);
            Assert.AreEqual(dto.HasButcher, model.HasButcher);
            Assert.AreEqual(dto.HasPizza, model.HasPizza);
            Assert.AreEqual(dto.HasDeli, model.HasDeli);
            Assert.AreEqual(dto.HasSaladBar, model.HasSaladBar);
            Assert.AreEqual(dto.HasOrganic, model.HasOrganic);
            Assert.AreEqual(dto.HasGrocery, model.HasGrocery);
            Assert.AreEqual(dto.HasMobilePhones, model.HasMobilePhones);
            Assert.AreEqual(dto.HasDryCleaning, model.HasDryCleaning);
            Assert.AreEqual(dto.HasHomeShoppingAvailable, model.HasHomeShoppingAvailable);
            Assert.AreEqual(dto.HasOptician, model.HasOptician);
            Assert.AreEqual(dto.HasPharmacy, model.HasPharmacy);
            Assert.AreEqual(dto.HasTravel, model.HasTravel);
            Assert.AreEqual(dto.HasPhotoDepartment, model.HasPhotoDepartment);
            Assert.AreEqual(dto.HasCarServiceArea, model.HasCarServiceArea);
            Assert.AreEqual(dto.HasGardenCentre, model.HasGardenCentre);
            Assert.AreEqual(dto.HasClinic, model.HasClinic);
            Assert.AreEqual(dto.HasAlcohol, model.HasAlcohol);
            Assert.AreEqual(dto.HasFashion, model.HasFashion);
            Assert.AreEqual(dto.HasCafe, model.HasCafe);
            Assert.AreEqual(dto.HasRecycling, model.HasRecycling);
            Assert.AreEqual(dto.HasPhotocopier, model.HasPhotocopier);
            Assert.AreEqual(dto.HasLottery, model.HasLottery);
            Assert.AreEqual(dto.HasPostOffice, model.HasPostOffice);
            Assert.AreEqual(dto.HasMovieRental, model.HasMovieRental);
            Assert.AreEqual(dto.HasJewellery, model.HasJewellery);
            Assert.AreEqual(dto.LocationType, model.LocationType);

        }

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="m2">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertModelAndModelAreEqual(Location m2, Location model)
        {
            Assert.AreEqual(m2.Id, model.Id);
            Assert.AreEqual(m2.RowVersion, model.RowVersion);
            Assert.AreEqual(m2.EntityId, model.EntityId);
            Assert.AreEqual(m2.LocationGroupId, model.LocationGroupId);
            Assert.AreEqual(m2.Code, model.Code);
            Assert.AreEqual(m2.Name, model.Name);
            Assert.AreEqual(m2.Region, model.Region);
            Assert.AreEqual(m2.County, model.County);
            Assert.AreEqual(m2.TVRegion, model.TVRegion);
            Assert.AreEqual(m2.State, model.State);
            Assert.AreEqual(m2.LocationAttribute, model.LocationAttribute);
            Assert.AreEqual(m2.DefaultClusterAttribute, model.DefaultClusterAttribute);
            Assert.AreEqual(m2.Address1, model.Address1);
            Assert.AreEqual(m2.Address2, model.Address2);
            Assert.AreEqual(m2.City, model.City);
            Assert.AreEqual(m2.PostalCode, model.PostalCode);
            Assert.AreEqual(m2.Country, model.Country);
            Assert.AreEqual(m2.Latitude, model.Latitude);
            Assert.AreEqual(m2.Longitude, model.Longitude);
            Assert.AreEqual(m2.DateOpen, model.DateOpen);
            Assert.AreEqual(m2.DateLastRefitted, model.DateLastRefitted);
            Assert.AreEqual(m2.DateClosed, model.DateClosed);
            Assert.AreEqual(m2.CarParkSpaces, model.CarParkSpaces);
            Assert.AreEqual(m2.CarParkManagement, model.CarParkManagement);
            Assert.AreEqual(m2.PetrolForecourtType, model.PetrolForecourtType);
            Assert.AreEqual(m2.Restaurant, model.Restaurant);
            Assert.AreEqual(m2.SizeGrossFloorArea, model.SizeGrossFloorArea);
            Assert.AreEqual(m2.SizeNetSalesArea, model.SizeNetSalesArea);
            Assert.AreEqual(m2.SizeMezzSalesArea, model.SizeMezzSalesArea);
            Assert.AreEqual(m2.TelephoneCountryCode, model.TelephoneCountryCode);
            Assert.AreEqual(m2.TelephoneAreaCode, model.TelephoneAreaCode);
            Assert.AreEqual(m2.TelephoneNumber, model.TelephoneNumber);
            Assert.AreEqual(m2.FaxCountryCode, model.FaxCountryCode);
            Assert.AreEqual(m2.FaxAreaCode, model.FaxAreaCode);
            Assert.AreEqual(m2.FaxNumber, model.FaxNumber);
            Assert.AreEqual(m2.OpeningHours, model.OpeningHours);
            Assert.AreEqual(m2.AverageOpeningHours, model.AverageOpeningHours);
            Assert.AreEqual(m2.ManagerName, model.ManagerName);
            Assert.AreEqual(m2.RegionalManagerName, model.RegionalManagerName);
            Assert.AreEqual(m2.DivisionalManagerName, model.DivisionalManagerName);
            Assert.AreEqual(m2.ManagerEmail, model.ManagerEmail);
            Assert.AreEqual(m2.RegionalManagerEmail, model.RegionalManagerEmail);
            Assert.AreEqual(m2.DivisionalManagerEmail, model.DivisionalManagerEmail);
            Assert.AreEqual(m2.AdvertisingZone, model.AdvertisingZone);
            Assert.AreEqual(m2.DistributionCentre, model.DistributionCentre);
            Assert.AreEqual(m2.NoOfCheckouts, model.NoOfCheckouts);
            Assert.AreEqual(m2.IsMezzFitted, model.IsMezzFitted);
            Assert.AreEqual(m2.IsFreehold, model.IsFreehold);
            Assert.AreEqual(m2.Is24Hours, model.Is24Hours);
            Assert.AreEqual(m2.IsOpenMonday, model.IsOpenMonday);
            Assert.AreEqual(m2.IsOpenTuesday, model.IsOpenTuesday);
            Assert.AreEqual(m2.IsOpenWednesday, model.IsOpenWednesday);
            Assert.AreEqual(m2.IsOpenThursday, model.IsOpenThursday);
            Assert.AreEqual(m2.IsOpenFriday, model.IsOpenFriday);
            Assert.AreEqual(m2.IsOpenSaturday, model.IsOpenSaturday);
            Assert.AreEqual(m2.IsOpenSunday, model.IsOpenSunday);
            Assert.AreEqual(m2.HasPetrolForecourt, model.HasPetrolForecourt);
            Assert.AreEqual(m2.HasNewsCube, model.HasNewsCube);
            Assert.AreEqual(m2.HasAtmMachines, model.HasAtmMachines);
            Assert.AreEqual(m2.HasCustomerWC, model.HasCustomerWC);
            Assert.AreEqual(m2.HasBabyChanging, model.HasBabyChanging);
            Assert.AreEqual(m2.HasInStoreBakery, model.HasInStoreBakery);
            Assert.AreEqual(m2.HasHotFoodToGo, model.HasHotFoodToGo);
            Assert.AreEqual(m2.HasRotisserie, model.HasRotisserie);
            Assert.AreEqual(m2.HasFishmonger, model.HasFishmonger);
            Assert.AreEqual(m2.HasButcher, model.HasButcher);
            Assert.AreEqual(m2.HasPizza, model.HasPizza);
            Assert.AreEqual(m2.HasDeli, model.HasDeli);
            Assert.AreEqual(m2.HasSaladBar, model.HasSaladBar);
            Assert.AreEqual(m2.HasOrganic, model.HasOrganic);
            Assert.AreEqual(m2.HasGrocery, model.HasGrocery);
            Assert.AreEqual(m2.HasMobilePhones, model.HasMobilePhones);
            Assert.AreEqual(m2.HasDryCleaning, model.HasDryCleaning);
            Assert.AreEqual(m2.HasHomeShoppingAvailable, model.HasHomeShoppingAvailable);
            Assert.AreEqual(m2.HasOptician, model.HasOptician);
            Assert.AreEqual(m2.HasPharmacy, model.HasPharmacy);
            Assert.AreEqual(m2.HasTravel, model.HasTravel);
            Assert.AreEqual(m2.HasPhotoDepartment, model.HasPhotoDepartment);
            Assert.AreEqual(m2.HasCarServiceArea, model.HasCarServiceArea);
            Assert.AreEqual(m2.HasGardenCentre, model.HasGardenCentre);
            Assert.AreEqual(m2.HasClinic, model.HasClinic);
            Assert.AreEqual(m2.HasAlcohol, model.HasAlcohol);
            Assert.AreEqual(m2.HasFashion, model.HasFashion);
            Assert.AreEqual(m2.HasCafe, model.HasCafe);
            Assert.AreEqual(m2.HasRecycling, model.HasRecycling);
            Assert.AreEqual(m2.HasPhotocopier, model.HasPhotocopier);
            Assert.AreEqual(m2.HasLottery, model.HasLottery);
            Assert.AreEqual(m2.HasPostOffice, model.HasPostOffice);
            Assert.AreEqual(m2.HasMovieRental, model.HasMovieRental);
            Assert.AreEqual(m2.HasJewellery, model.HasJewellery);
            Assert.AreEqual(m2.LocationType, model.LocationType);
        }

        private void SetValues(Location model, Int32 setNum)
        {
            model.Code = "code" + setNum;
            model.Name = "name" + setNum;
            model.Region = "region" + setNum;
            model.County = "county" + setNum;
            model.TVRegion = "TVRegion" + setNum;
            model.LocationAttribute = "locationAttribute" + setNum;
            model.DefaultClusterAttribute = "DefaultClusterAttribute" + setNum;
            model.Address1 = "DefaultClusterAttribute" + setNum;
            model.Address2 = "DefaultClusterAttribute" + setNum;
            model.City = "City" + setNum;
            model.PostalCode = "PostalCode" + setNum;
            model.Country = "Country" + setNum;
            model.City = "City" + setNum;
            model.Latitude = setNum;
            model.Longitude = setNum;
            model.DateOpen = DateTime.Now.AddMinutes(setNum);
            model.DateLastRefitted = DateTime.Now.AddMinutes(setNum);
            model.DateClosed = DateTime.Now.AddMinutes(setNum);
            model.CarParkSpaces = null;
            model.CarParkManagement = "CarParkManagement" + setNum;
            model.PetrolForecourtType = "PetrolForecourtType" + setNum;
            model.Restaurant = "Restaurant" + setNum;
            model.SizeGrossFloorArea = setNum;
            model.SizeNetSalesArea = setNum;
            model.SizeMezzSalesArea = setNum;
            model.TelephoneCountryCode = "TelephoneCountryCode" + setNum;
            model.TelephoneAreaCode = "TelephoneAreaCode" + setNum;
            model.TelephoneNumber = "TelephoneNumber" + setNum;
            model.FaxCountryCode = "FaxCountryCode" + setNum;
            model.FaxAreaCode = "FaxAreaCode" + setNum;
            model.FaxNumber = "FaxNumber" + setNum;
            model.OpeningHours = "OpeningHours" + setNum;
            model.AverageOpeningHours = 46487F;
            model.ManagerName = "ManagerName" + setNum;
            model.RegionalManagerName = "RegionalManagerName" + setNum;
            model.DivisionalManagerName = "DivisionalManagerName" + setNum;
            model.ManagerEmail = "ManagerEmail" + setNum;
            model.RegionalManagerEmail = "RegionalManagerEmail" + setNum;
            model.DivisionalManagerEmail = "DivisionalManagerEmail" + setNum;
            model.AdvertisingZone = "AdvertisingZone" + setNum;
            model.DistributionCentre = "DistributionCentre" + setNum;
            model.NoOfCheckouts = null;
            model.IsMezzFitted = (setNum % 2 == 0);
            model.IsFreehold = (setNum % 2 == 0);
            model.Is24Hours = (setNum % 2 == 0);
            model.IsOpenMonday = (setNum % 2 == 0);
            model.IsOpenTuesday = (setNum % 2 == 0);
            model.IsOpenWednesday = (setNum % 2 == 0);
            model.IsOpenThursday = (setNum % 2 == 0);
            model.IsOpenFriday = (setNum % 2 == 0);
            model.IsOpenSaturday = (setNum % 2 == 0);
            model.IsOpenSunday = (setNum % 2 == 0);
            model.HasPetrolForecourt = (setNum % 2 == 0);
            model.HasNewsCube = (setNum % 2 == 0);
            model.HasAtmMachines = (setNum % 2 == 0);
            model.HasCustomerWC = (setNum % 2 == 0);
            model.HasBabyChanging = (setNum % 2 == 0);
            model.HasInStoreBakery = (setNum % 2 == 0);
            model.HasHotFoodToGo = (setNum % 2 == 0);
            model.HasRotisserie = (setNum % 2 == 0);
            model.HasFishmonger = (setNum % 2 == 0);
            model.HasButcher = (setNum % 2 == 0);
            model.HasPizza = (setNum % 2 == 0);
            model.HasDeli = (setNum % 2 == 0);
            model.HasSaladBar = (setNum % 2 == 0);
            model.HasOrganic = (setNum % 2 == 0);
            model.HasGrocery = (setNum % 2 == 0);
            model.HasMobilePhones = (setNum % 2 == 0);
            model.HasDryCleaning = (setNum % 2 == 0);
            model.HasHomeShoppingAvailable = (setNum % 2 == 0);
            model.HasOptician = (setNum % 2 == 0);
            model.HasPharmacy = (setNum % 2 == 0);
            model.HasTravel = (setNum % 2 == 0);
            model.HasPhotoDepartment = (setNum % 2 == 0);
            model.HasCarServiceArea = (setNum % 2 == 0);
            model.HasGardenCentre = (setNum % 2 == 0);
            model.HasClinic = (setNum % 2 == 0);
            model.HasAlcohol = (setNum % 2 == 0);
            model.HasFashion = (setNum % 2 == 0);
            model.HasCafe = (setNum % 2 == 0);
            model.HasRecycling = (setNum % 2 == 0);
            model.HasPhotocopier = (setNum % 2 == 0);
            model.HasLottery = (setNum % 2 == 0);
            model.HasPostOffice = (setNum % 2 == 0);
            model.HasMovieRental = (setNum % 2 == 0);
            model.HasJewellery = (setNum % 2 == 0);
            model.LocationType = "LocationType" + setNum;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Location model = Location.NewLocation(1);
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            //model to set on
            Location model = Location.NewLocation(1);

            TestPropertySetters<Location>(model);

        }
        #endregion

        #region FactoryMethods

        [Test]
        public void NewLocation()
        {
            Int32 entityId = 1;

            Location newLocation =Location.NewLocation(entityId);

            Assert.AreEqual(newLocation.EntityId, entityId);

            Assert.IsTrue(newLocation.IsNew);
        }

        [Test]
        public void FetchById()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, entityDtos);
            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 2);
            List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 1);
            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(base.DalFactory, entityDtos[0].Id, groupDtos, 5);

            foreach (LocationDto dto in locDtos)
            {
                var model = Location.FetchById(dto.Id);
                AssertDtoAndModelAreEqual(dto, model);
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataPortal_InsertUpdateDelete()
        {
            List<EntityDto> entities = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entities[0].Id;
            List<LocationHierarchyDto> locHierarchies = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, entities);
            List<LocationLevelDto> locLevels = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, locHierarchies[0].Id, 3);
            List<LocationGroupDto> locGroups = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, locLevels, 1);

            //create a new item 
            Location item = Location.NewLocation(locGroups[0].Id, entityId);

            //set properties
            SetValues(item, 1);

            //insert
            item = item.Save();

            LocationDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);

            //update
            SetValues(item, 2);
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);


            //update again to check concurrency
            SetValues(item, 3);
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);

            //delete
            Int16 itemId = item.Id;
            item.Delete();
            item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dto = dal.FetchById(itemId);

                    Assert.IsNotNull(dto.DateDeleted, "DateDeleted should be marked.");
                }
            }

        }

        #endregion
    }
}
