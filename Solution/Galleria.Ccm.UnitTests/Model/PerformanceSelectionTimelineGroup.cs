﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26159 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PerformanceSelectionTimelineGroupTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            PerformanceSelectionTimelineGroup model = PerformanceSelectionTimelineGroup.NewPerformanceSelectionTimelineGroup();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            //model to set on
            PerformanceSelectionTimelineGroup model = PerformanceSelectionTimelineGroup.NewPerformanceSelectionTimelineGroup();

            TestPropertySetters<PerformanceSelectionTimelineGroup>(model);

        }
        #endregion
    }
}
