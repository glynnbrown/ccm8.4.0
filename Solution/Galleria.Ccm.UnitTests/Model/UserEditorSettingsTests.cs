﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM800)

// V8-28676 : A.Silva
//		Created

#endregion

#region Version History: (CCM CCM802)

// V8-27828 : J.Pickup
//		Introduced new test IsShowSelectionAsBlinkingEnabled_WhenInitialized_ShouldDefaultToTrue

#endregion

#region Version History: CCM810

// V8-30282: A.Silva
//  Amended ProductImageAttribute_WhenInitialized_ShouldDefaultToPlanogramProductGtin Unit Test.

#endregion

#region Version History: CCM820

// V8-29439: J.Pickup
//  InstantNotificationValidationProperties_WhenInitialized_ShouldDefaultToFalse test created.

#endregion



#endregion

using System;
using System.IO;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserEditorSettingsTests : TestBase
    {
        [Test]
        public void Serializable()
        {
            Serialize(UserEditorSettings.NewUserEditorSettings());
        }

        #region Default property values

        private const RealImageProviderType ExpectedDefaultSource = RealImageProviderType.Repository;
        private const String ExpectedDefaultLocation = "Images";
        private const Boolean ExpectedDefaultIgnoreLeadingZeros = false;
        private const Byte ExpectedDefaultMinFieldLength = 10;
        private const RealImageCompressionType ExpectedDefaultImageCompression = RealImageCompressionType.None;
        private const RealImageColourDepthType ExpectedDefaultImageColourDepth = RealImageColourDepthType.Bit8;
        private const bool ExpectedDefaultTransparency = false;

        private const Boolean ShowInstantComponentHasCollisionsWarnings = false;
        private const Boolean ShowInstantComponentIsOverfilledWarnings = false;
        private const Boolean ShowInstantComponentSlopeWithNoRiserWarnings = false;
        private const Boolean ShowInstantPegIsOverfilledWarnings = false;
        private const Boolean ShowInstantPositionCannotBreakTrayBackWarnings = false;
        private const Boolean ShowInstantPositionCannotBreakTrayDownWarnings = false;
        private const Boolean ShowInstantPositionCannotBreakTrayTopWarnings = false;
        private const Boolean ShowInstantPositionCannotBreakTrayUpWarnings = false;
        private const Boolean ShowInstantPositionDoesNotAchieveMinDeepWarnings = false;
        private const Boolean ShowInstantPositionExceedsMaxDeepWarnings = false;
        private const Boolean ShowInstantPositionExceedsMaxRightCapWarnings = false;
        private const Boolean ShowInstantPositionExceedsMaxStackWarnings = false;
        private const Boolean ShowInstantPositionExceedsMaxTopCapWarnings = false;
        private const Boolean ShowInstantPositionHasCollisionsWarnings = false;
        private const Boolean ShowInstantPositionHasInvalidMerchStyleSettingsWarnings = false;
        private const Boolean ShowInstantPositionIsHangingTrayWarnings = false;
        private const Boolean ShowInstantPositionIsHangingWithoutPegWarnings = false;
        private const Boolean ShowInstantPositionIsOutsideMerchandisingSpaceWarnings = false;
        private const Boolean ShowInstantProductDoesNotAchieveMinCasesWarnings = false;
        private const Boolean ShowInstantProductDoesNotAchieveMinDeliveriesWarnings = false;
        private const Boolean ShowInstantProductDoesNotAchieveMinDosWarnings = false;
        private const Boolean ShowInstantProductDoesNotAchieveMinShelfLifeWarnings = false;
        private const Boolean ShowInstantProductHasDuplicateGtinWarnings = false;
        private const Boolean ShowInstantAssortmentLocalLineRulesWarnings = false;
        private const Boolean ShowInstantAssortmentDistributionRulesWarnings = false;
        private const Boolean ShowInstantAssortmentProductRulesWarnings = false;
        private const Boolean ShowInstantAssortmentFamilyRulesWarnings = false;
        private const Boolean ShowInstantAssortmentInheritanceRulesWarnings = false;
        private const Boolean ShowInstantAssortmentCoreRulesWarnings = false;
        public const Boolean ShowInstantPositionPegHolesAreInvalidWarnings = false;

        [Test]
        public void ProductImageSource_WhenInitialized_ShouldDefaultToRepository()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            RealImageProviderType actual = editorSettings.ProductImageSource;

            Assert.AreEqual(ExpectedDefaultSource, actual);
        }

        [Test]
        public void ProductImageLocation_WhenInitialized_ShouldDefaultToDocumentsImages()
        {
            //The default location of documents will be defined by the type of install chosen.
            string myPublicDocsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments),
                Constants.MyDocsAppFolderName);

            string calculatedDefaultDocsFolder;
            if (Directory.Exists(myPublicDocsFolder))
            {
                calculatedDefaultDocsFolder = myPublicDocsFolder;
            }
            else
            {
                calculatedDefaultDocsFolder = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Constants.MyDocsAppFolderName);
            }

            if (!Directory.Exists(calculatedDefaultDocsFolder))
            {
                Assert.Inconclusive("Cannot find a documents folder to test the property against.");
            }

            string expected = Path.Combine(calculatedDefaultDocsFolder, ExpectedDefaultLocation);

            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageLocation;

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///     When initializing a new instance of <see cref="UserEditorSettings" />,
        ///     <see cref="UserEditorSettings.ProductImageAttribute" /> should default to
        ///     <see cref="PlanogramProduct.GtinProperty" />.
        /// </summary>
        [Test]
        public void ProductImageAttribute_WhenInitialized_ShouldDefaultToPlanogramProductGtin()
        {
            const String expectation = "ProductImageAttribute should default to PlanogramProduct.Gtin";
            const String requisiteExpectedProperty = "Cannot test without the field for the expected property ({0})";
            String expectedAttributePropertyName = PlanogramProduct.GtinProperty.Name;
            ObjectFieldInfo objectFieldInfo = PlanogramProduct
                .EnumerateDisplayableFieldInfos(false, true, false)
                .FirstOrDefault(o => o.PropertyName == expectedAttributePropertyName);
            if (objectFieldInfo == null) Assert.Inconclusive(requisiteExpectedProperty, expectedAttributePropertyName);
            String expected = objectFieldInfo.FieldPlaceholder;

            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();

            String actual = editorSettings.ProductImageAttribute;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageIgnoreLeadingZeros_WhenInitialized_ShouldDefaultToFalse()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            bool actual = editorSettings.ProductImageIgnoreLeadingZeros;

            Assert.AreEqual(ExpectedDefaultIgnoreLeadingZeros, actual);
        }

        [Test]
        public void ProductImageMinFieldLength_WhenInitialized_ShouldDefaultToTen()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            byte actual = editorSettings.ProductImageMinFieldLength;

            Assert.AreEqual(ExpectedDefaultMinFieldLength, actual);
        }

        [Test]
        public void ProductImageFacingPostfixFront_WhenInitialized_ShouldDefaultToEnumValue()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageFacingPostfixFront;

            Assert.AreEqual(((Int32)PlanogramImageFacingType.Front).ToString(), actual);
        }

        [Test]
        public void ProductImageFacingPostfixLeft_WhenInitialized_ShouldDefaultToEnumValue()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageFacingPostfixLeft;

            Assert.AreEqual(((Int32)PlanogramImageFacingType.Left).ToString(), actual);
        }

        [Test]
        public void ProductImageFacingPostfixTop_WhenInitialized_ShouldDefaultToEnumValue()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageFacingPostfixTop;

            Assert.AreEqual(((Int32)PlanogramImageFacingType.Top).ToString(), actual);
        }

        [Test]
        public void ProductImageFacingPostfixBack_WhenInitialized_ShouldDefaultToEnumValue()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageFacingPostfixBack;

            Assert.AreEqual(((Int32)PlanogramImageFacingType.Back).ToString(), actual);
        }

        [Test]
        public void ProductImageFacingPostfixRight_WhenInitialized_ShouldDefaultToEnumValue()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageFacingPostfixRight;

            Assert.AreEqual(((Int32)PlanogramImageFacingType.Right).ToString(), actual);
        }

        [Test]
        public void ProductImageFacingPostfixBottom_WhenInitialized_ShouldDefaultToEnumValue()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageFacingPostfixBottom;

            Assert.AreEqual(((Int32)PlanogramImageFacingType.Bottom).ToString(), actual);
        }

        [Test]
        public void ProductImageCompression_WhenInitialized_ShouldDefaultToNone()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            RealImageCompressionType actual = editorSettings.ProductImageCompression;

            Assert.AreEqual(ExpectedDefaultImageCompression, actual);
        }

        [Test]
        public void ProductImageColourDepth_WhenInitialized_ShouldDefaultToBit8()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            RealImageColourDepthType actual = editorSettings.ProductImageColourDepth;

            Assert.AreEqual(ExpectedDefaultImageColourDepth, actual);
        }

        [Test]
        public void ProductImageTransparency_WhenInitialized_ShouldDefaultToFalse()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            bool actual = editorSettings.ProductImageTransparency;

            Assert.AreEqual(ExpectedDefaultTransparency, actual);
        }

        [Test]
        public void IsShowSelectionAsBlinkingEnabled_WhenInitialized_ShouldDefaultToTrue()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            Boolean actual = editorSettings.IsShowSelectionAsBlinkingEnabled;

            Assert.AreEqual(true, actual);
        }

        [Test]
        public void InstantNotificationValidationProperties_WhenInitialized_ShouldDefaultToFalse()
        {
            UserEditorSettings editorSettings = UserEditorSettings.NewUserEditorSettings();
            string actual = editorSettings.ProductImageFacingPostfixLeft;

            Assert.AreEqual(ShowInstantComponentHasCollisionsWarnings, editorSettings.ShowInstantComponentHasCollisionsWarnings);
            Assert.AreEqual(ShowInstantComponentIsOverfilledWarnings, editorSettings.ShowInstantComponentIsOverfilledWarnings);
            Assert.AreEqual(ShowInstantComponentSlopeWithNoRiserWarnings, editorSettings.ShowInstantComponentSlopeWithNoRiserWarnings);
            Assert.AreEqual(ShowInstantPegIsOverfilledWarnings, editorSettings.ShowInstantPegIsOverfilledWarnings);
            Assert.AreEqual(ShowInstantPositionCannotBreakTrayBackWarnings, editorSettings.ShowInstantPositionCannotBreakTrayBackWarnings);
            Assert.AreEqual(ShowInstantPositionCannotBreakTrayDownWarnings, editorSettings.ShowInstantPositionCannotBreakTrayDownWarnings);
            Assert.AreEqual(ShowInstantPositionCannotBreakTrayTopWarnings, editorSettings.ShowInstantPositionCannotBreakTrayTopWarnings);
            Assert.AreEqual(ShowInstantPositionCannotBreakTrayUpWarnings, editorSettings.ShowInstantPositionCannotBreakTrayUpWarnings);
            Assert.AreEqual(ShowInstantPositionDoesNotAchieveMinDeepWarnings, editorSettings.ShowInstantPositionDoesNotAchieveMinDeepWarnings);
            Assert.AreEqual(ShowInstantPositionExceedsMaxDeepWarnings, editorSettings.ShowInstantPositionExceedsMaxDeepWarnings);
            Assert.AreEqual(ShowInstantPositionExceedsMaxRightCapWarnings, editorSettings.ShowInstantPositionExceedsMaxRightCapWarnings);
            Assert.AreEqual(ShowInstantPositionExceedsMaxStackWarnings, editorSettings.ShowInstantPositionExceedsMaxStackWarnings);
            Assert.AreEqual(ShowInstantPositionExceedsMaxTopCapWarnings, editorSettings.ShowInstantPositionExceedsMaxTopCapWarnings);
            Assert.AreEqual(ShowInstantPositionHasCollisionsWarnings, editorSettings.ShowInstantPositionHasCollisionsWarnings);
            Assert.AreEqual(ShowInstantPositionHasInvalidMerchStyleSettingsWarnings, editorSettings.ShowInstantPositionHasInvalidMerchStyleSettingsWarnings);
            Assert.AreEqual(ShowInstantPositionIsHangingTrayWarnings, editorSettings.ShowInstantPositionIsHangingTrayWarnings);
            Assert.AreEqual(ShowInstantPositionIsHangingWithoutPegWarnings, editorSettings.ShowInstantPositionIsHangingWithoutPegWarnings);
            Assert.AreEqual(ShowInstantPositionIsOutsideMerchandisingSpaceWarnings, editorSettings.ShowInstantPositionIsOutsideMerchandisingSpaceWarnings);
            Assert.AreEqual(ShowInstantProductDoesNotAchieveMinCasesWarnings, editorSettings.ShowInstantProductDoesNotAchieveMinCasesWarnings);
            Assert.AreEqual(ShowInstantProductDoesNotAchieveMinDeliveriesWarnings, editorSettings.ShowInstantProductDoesNotAchieveMinDeliveriesWarnings);
            Assert.AreEqual(ShowInstantProductDoesNotAchieveMinDosWarnings, editorSettings.ShowInstantProductDoesNotAchieveMinDosWarnings);
            Assert.AreEqual(ShowInstantProductDoesNotAchieveMinShelfLifeWarnings, editorSettings.ShowInstantProductDoesNotAchieveMinShelfLifeWarnings);
            Assert.AreEqual(ShowInstantProductHasDuplicateGtinWarnings, editorSettings.ShowInstantProductHasDuplicateGtinWarnings);
            Assert.AreEqual(ShowInstantAssortmentLocalLineRulesWarnings, editorSettings.ShowInstantAssortmentLocalLineRulesWarnings);
            Assert.AreEqual(ShowInstantAssortmentDistributionRulesWarnings, editorSettings.ShowInstantAssortmentDistributionRulesWarnings);
            Assert.AreEqual(ShowInstantAssortmentProductRulesWarnings, editorSettings.ShowInstantAssortmentProductRulesWarnings);
            Assert.AreEqual(ShowInstantAssortmentFamilyRulesWarnings, editorSettings.ShowInstantAssortmentFamilyRulesWarnings);
            Assert.AreEqual(ShowInstantAssortmentInheritanceRulesWarnings, editorSettings.ShowInstantAssortmentInheritanceRulesWarnings);
            Assert.AreEqual(ShowInstantAssortmentCoreRulesWarnings, editorSettings.ShowInstantAssortmentCoreRulesWarnings);
            Assert.AreEqual(ShowInstantPositionPegHolesAreInvalidWarnings, editorSettings.ShowInstantPositionPegHolesAreInvalidWarnings);
        }

        #endregion

        [Test]
        public void PlanogramComparisonTemplateLocationHasModelPropertyInfo()
        {
            const String expectation = "UserEditorSettings.PlanogramComparisonTemplateLocation should have a registered ModelPropertyInfo.";
            const String expected = "PlanogramComparisonTemplateLocation";

            String actual = UserEditorSettings.PlanogramComparisonTemplateLocationProperty.Name;

            Assert.AreEqual(expected, actual, expectation);
        }
    }
}