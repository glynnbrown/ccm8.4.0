﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationGroupTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        internal static void AssertDtoAndModelAreEqual(LocationGroupDto dto, LocationGroup model)
        {
            Assert.AreEqual(dto.Code, model.Code);
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.LocationLevelId, model.LocationLevelId);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.ParentGroupId, (model.ParentGroup != null) ? (int?)model.ParentGroup.Id : null);
        }
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            LocationGroup model =
                LocationGroup.NewLocationGroup(1);
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySettersGeneric()
        {
            // create a new model for rand values
            LocationGroup model = LocationGroup.NewLocationGroup(1);

            List<String> propertiesToComplexToDealWith = new List<String>();
            propertiesToComplexToDealWith.Add("LocationLevelId");
            propertiesToComplexToDealWith.Add("AssignedLocationsCount");
            TestBase.TestPropertySetters<LocationGroup>(model, propertiesToComplexToDealWith);
        }

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model
            LocationGroup model =
                LocationGroup.NewLocationGroup(1);

            // create a random number generator
            Random random = new Random(1);

            // Name
            string name = Convert.ToString(random.Next());
            model.Name = name;
            Assert.AreEqual(model.Name, name);

            //code 
            string code = Convert.ToString(random.Next());
            model.Code = code;
            Assert.AreEqual(model.Code, code);

        }
        #endregion

        #region FactoryMethods

        [Test]
        public void NewLocationGroup()
        {
            Int32 levelId = 6;

            LocationGroup newGroup =
                LocationGroup.NewLocationGroup(6);

            Assert.IsTrue(newGroup.IsNew);
            Assert.IsTrue(newGroup.IsChild);


            Assert.IsNotNull(newGroup.ChildList);
            Assert.AreEqual(newGroup.LocationLevelId, levelId);
        }

        [Test]
        public void FetchLocationGroup()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root group)
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(entityId);
            LocationGroup rootGroup = hierarchy.RootGroup;


            //set properties
            rootGroup.Name = "test";
            rootGroup.Code = "testCode";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            LocationHierarchy returnedHierarchy =
                LocationHierarchy.FetchById(hierarchy.Id);
            LocationGroup returnedGroup = returnedHierarchy.RootGroup;


            Assert.AreEqual(returnedGroup.Name, rootGroup.Name);
            Assert.AreEqual(returnedGroup.Code, rootGroup.Code);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root group)
            LocationHierarchy hierarchy = LocationHierarchy.NewLocationHierarchy(entityId);
            LocationGroup rootGroup = hierarchy.RootGroup;

            //set properties
            rootGroup.Name = "test";
            rootGroup.Code = "testCode";


            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //fetch back the level dto
            LocationGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dto = dal.FetchById(rootGroup.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, rootGroup);
        }

        [Test]
        public void DataAccess_Update()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root group)
            LocationHierarchy hierarchy = LocationHierarchy.NewLocationHierarchy(entityId);
            LocationGroup rootGroup = hierarchy.RootGroup;

            //set properties
            rootGroup.Name = "test";
            rootGroup.Code = "testCode";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //update
            rootGroup.Name = "test#";
            rootGroup.Code = "testCode#";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //fetch back the level dto
            LocationGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dto = dal.FetchById(rootGroup.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, rootGroup);
        }

        [Test]
        public void DataAccess_Delete()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root group)
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(entityId);
            LocationLevel level = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup group = LocationGroup.NewLocationGroup(level.Id);
            hierarchy.RootGroup.ChildList.Add(group);

            //set properties
            group.Name = "test";
            group.Code = "testCode";

            //save
            hierarchy = hierarchy.Save();
            group = hierarchy.RootGroup.ChildList[0];

            //fetch back the level dto
            LocationGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dto = dal.FetchById(group.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, group);

            //delete it
            Int32 itemId = group.Id;
            hierarchy.RootGroup.ChildList.Remove(group);
            hierarchy = hierarchy.Save();


            //try to retrieve again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dto = dal.FetchById(itemId);
                    Assert.IsNotNull(dto.DateDeleted, "Item should be marked as deleted");
                }
            }
            AssertDtoAndModelAreEqual(dto, group);
        }

        #endregion

        #region Helper Methods

        [Test]
        public void Methods_GetAllChildGroups()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root group)
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(entityId);
            LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup group1 = LocationGroup.NewLocationGroup(level1.Id);
            LocationGroup group2 = LocationGroup.NewLocationGroup(level1.Id);
            hierarchy.RootGroup.ChildList.Add(group1);
            hierarchy.RootGroup.ChildList.Add(group2);
            LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            LocationGroup group3 = LocationGroup.NewLocationGroup(level2.Id);
            LocationGroup group4 = LocationGroup.NewLocationGroup(level2.Id);
            group1.ChildList.Add(group3);
            group2.ChildList.Add(group4);

            List<LocationGroup> groupList = hierarchy.RootGroup.EnumerateAllChildGroups().ToList();
            Assert.AreEqual(5, groupList.Count());

            Assert.Contains(hierarchy.RootGroup, groupList);
            Assert.Contains(group1, groupList);
            Assert.Contains(group2, groupList);
            Assert.Contains(group3, groupList);
            Assert.Contains(group4, groupList);
        }

        [Test]
        public void Methods_GetParentPath()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root group)
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(entityId);
            LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup group1 = LocationGroup.NewLocationGroup(level1.Id);
            LocationGroup group2 = LocationGroup.NewLocationGroup(level1.Id);
            hierarchy.RootGroup.ChildList.Add(group1);
            hierarchy.RootGroup.ChildList.Add(group2);
            LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            LocationGroup group3 = LocationGroup.NewLocationGroup(level2.Id);
            LocationGroup group4 = LocationGroup.NewLocationGroup(level2.Id);
            group1.ChildList.Add(group3);
            group2.ChildList.Add(group4);

            List<LocationGroup> parentPathList = group4.GetParentPath();
            Assert.AreEqual(2, parentPathList.Count);

            Assert.Contains(group2, parentPathList);
            Assert.Contains(hierarchy.RootGroup, parentPathList);
        }

        [Test]
        public void Methods_GetAssociatedLevel()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root group)
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(entityId);
            LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup group1 = LocationGroup.NewLocationGroup(level1.Id);
            LocationGroup group2 = LocationGroup.NewLocationGroup(level1.Id);
            hierarchy.RootGroup.ChildList.Add(group1);
            hierarchy.RootGroup.ChildList.Add(group2);
            LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            LocationGroup group3 = LocationGroup.NewLocationGroup(level2.Id);
            LocationGroup group4 = LocationGroup.NewLocationGroup(level2.Id);
            group1.ChildList.Add(group3);
            group2.ChildList.Add(group4);

            Assert.AreEqual(hierarchy.RootLevel, hierarchy.RootGroup.GetAssociatedLevel());
            Assert.AreEqual(level1, group1.GetAssociatedLevel());
            Assert.AreEqual(level1, group2.GetAssociatedLevel());
            Assert.AreEqual(level2, group3.GetAssociatedLevel());
            Assert.AreEqual(level2, group4.GetAssociatedLevel());
        }


        #endregion

        #region Other Scenarios

        [Test]
        public void MoveItemFromOneCollectionToAnother()
        {
            //create a hierarchy with 2 areas & a dept under each
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            LocationHierarchy hierarchy =
               LocationHierarchy.NewLocationHierarchy(entityId);

            LocationLevel arealevel = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup area1 = LocationGroup.NewLocationGroup(arealevel.Id);
            area1.Name = "area1";
            area1.Code = "1";
            LocationGroup area2 = LocationGroup.NewLocationGroup(arealevel.Id);
            area2.Name = "area2";
            area2.Code = "2";
            hierarchy.RootGroup.ChildList.Add(area1);
            hierarchy.RootGroup.ChildList.Add(area2);

            LocationLevel deptLevel = hierarchy.InsertNewChildLevel(arealevel);
            LocationGroup dept1 = LocationGroup.NewLocationGroup(deptLevel.Id);
            dept1.Name = "dept1";
            dept1.Code = "3";
            LocationGroup dept2 = LocationGroup.NewLocationGroup(deptLevel.Id);
            dept2.Name = "dept2";
            dept2.Code = "4";
            area1.ChildList.Add(dept1);
            area2.ChildList.Add(dept2);

            //save
            hierarchy = hierarchy.Save();

            //check area1 has 1 child & area 2 has 1 child
            area1 = hierarchy.RootGroup.ChildList[0];
            Assert.AreEqual(1, area1.ChildList.Count);

            area2 = hierarchy.RootGroup.ChildList[1];
            Assert.AreEqual(1, area2.ChildList.Count);


            //get the dept ids
            dept1 = hierarchy.RootGroup.ChildList[0].ChildList[0];
            Int32 dept1Id = dept1.Id;
            dept2 = hierarchy.RootGroup.ChildList[1].ChildList[0];
            Int32 dept2Id = dept2.Id;

            //move dept1 form area1 to area2
            area2.ChildList.Add(dept1);
            area1.ChildList.Remove(dept1);
            Assert.AreEqual(0, area1.ChildList.Count);

            //save
            hierarchy = hierarchy.Save();

            //refetch the hierarchy from scratch
            hierarchy = LocationHierarchy.FetchById(hierarchy.Id);

            //check area1 has no children
            area1 = hierarchy.RootGroup.ChildList[0];
            Assert.AreEqual(0, area1.ChildList.Count);

            //check area2 has 2 children
            area2 = hierarchy.RootGroup.ChildList[1];
            Assert.AreEqual(2, area2.ChildList.Count);

            //check the dept ids have not changed
            Assert.AreEqual(dept2Id, area2.ChildList[0].Id);
            Assert.AreEqual(dept1Id, area2.ChildList[1].Id);
        }

        [Test]
        public void GEM14222_DeletingMiddleLevelMovesChildGroups()
        {
            //create a hierarchy with 3 levels and a group at each
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            LocationHierarchy hierarchy =
               LocationHierarchy.NewLocationHierarchy(entityId);

            //level 1
            LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup group1 = LocationGroup.NewLocationGroup(level1.Id);
            group1.Name = "group1";
            group1.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            hierarchy.RootGroup.ChildList.Add(group1);

            //level 2
            LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            LocationGroup group2 = LocationGroup.NewLocationGroup(level2.Id);
            group2.Name = "group2";
            group2.Code = group1.Code + 1;
            group1.ChildList.Add(group2);

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;
            group1 = hierarchy.RootGroup.ChildList[0];
            group2 = group1.ChildList[0];

            //remove level 1
            hierarchy.RemoveLevel(level1);

            //check that group 2 is now the child of the rootGroup
            Assert.AreEqual(group2.Id, hierarchy.RootGroup.ChildList[0].Id);

            //save
            hierarchy = hierarchy.Save();

            //check the hierarchy returned now only has 2 levels and 2 groups
            Assert.AreEqual((Int32)level2.Id, (Int32)hierarchy.RootLevel.ChildLevel.Id);
            Assert.AreEqual((Int32)group2.LocationLevelId, hierarchy.RootGroup.ChildList[0].Id);

            //try to fetch back group 1 - should fail
            LocationGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {

                    dto = dal.FetchById(group1.Id);
                    Assert.IsNotNull(dto.DateDeleted, "Group1 item should be marked as deleted");
                }
            }

        }

        [Test]
        public void GEM14222_DeletingParentGroupDeletesChildren()
        {
            //create a hierarchy with 3 levels and a group at each
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            LocationHierarchy hierarchy =
               LocationHierarchy.NewLocationHierarchy(entityId);

            //level 1
            LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup group1 = LocationGroup.NewLocationGroup(level1.Id);
            group1.Name = "group1";
            group1.Name = "group1"; group1.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            hierarchy.RootGroup.ChildList.Add(group1);

            //level 2
            LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            LocationGroup group2 = LocationGroup.NewLocationGroup(level2.Id);
            group2.Name = "group2";
            group2.Code = group1.Code + 1;
            group1.ChildList.Add(group2);

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;
            group1 = hierarchy.RootGroup.ChildList[0];
            group2 = group1.ChildList[0];

            //delete group1
            Assert.AreEqual(1, hierarchy.RootGroup.ChildList.Count());
            hierarchy.RootGroup.ChildList.Remove(group1);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count());

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;

            //check the returned hierarchy only has a root group
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count());

            //try to fetch group 1 and 2 - should both fail
            LocationGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dto = dal.FetchById(group1.Id);
                    Assert.IsNotNull(dto.DateDeleted, "Group1 item should be marked as deleted");

                    dto = dal.FetchById(group2.Id);
                    Assert.IsNotNull(dto.DateDeleted, "Group2 item should be marked as deleted");
                }
            }

        }

        [Test]
        public void DeleteMiddleThenLastLevel()
        {
            //create a hierarchy with 3 levels and a group at each
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            LocationHierarchy hierarchy =
               LocationHierarchy.NewLocationHierarchy(entityId);

            //level 1
            LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            LocationGroup group1 = LocationGroup.NewLocationGroup(level1.Id);
            group1.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            hierarchy.RootGroup.ChildList.Add(group1);

            level1.Name = "level1";
            group1.Name = "group1";

            //level 2
            LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            LocationGroup group2 = LocationGroup.NewLocationGroup(level2.Id);
            group2.Code = group1.Code + 1;
            group1.ChildList.Add(group2);

            level2.Name = "level2";
            group2.Name = "group2";

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;
            group1 = hierarchy.RootGroup.ChildList[0];
            group2 = group1.ChildList[0];

            //remove level 1
            hierarchy.RemoveLevel(level1);
            level2 = hierarchy.RootLevel.ChildLevel;

            //check that group 2 is now the child of the rootGroup
            Assert.AreEqual(group2.Id, hierarchy.RootGroup.ChildList[0].Id);

            //remove level 2
            hierarchy.RemoveLevel(level2);
            Assert.IsNull(hierarchy.RootLevel.ChildLevel);

            //save
            hierarchy = hierarchy.Save();

            //check the hierarchy returned now only has 1 levels and 1 groups
            Assert.IsNull(hierarchy.RootLevel.ChildLevel);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count);

            //try to fetch back group 1 - should fail
            LocationGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {

                    dto = dal.FetchById(group1.Id);
                    Assert.IsNotNull(dto.DateDeleted, "Item should be marked as deleted");
                }
            }

            //try to fetch back group 2 - should fail
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {

                    dto = dal.FetchById(group2.Id);
                    Assert.IsNotNull(dto.DateDeleted, "Item should be marked as deleted");
                }
            }

            //try to fetch back level 1 - should fail
            LocationLevelDto levelDto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {

                    levelDto = dal.FetchById((Int32)level1.Id);
                    Assert.IsNotNull(levelDto.DateDeleted, "level 1 should be marked as deleted");
                }
            }

            //try to fetch back level 2 - should fail
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {

                    levelDto = dal.FetchById((Int32)level2.Id);
                    Assert.IsNotNull(levelDto.DateDeleted, "level 2 should be marked as deleted");
                }
            }
        }


        [Test]
        public void FrameworkBehaviourCheckHierarchy()
        {
            //create a hierarchy with one level and one group
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            LocationHierarchy hierarchy =
               LocationHierarchy.NewLocationHierarchy(entityId);
            hierarchy = hierarchy.Save();

            //get the rowversion of the header
            RowVersion preHeaderRowVersion = hierarchy.RowVersion;

            //check the isdirty flags
            Assert.IsFalse(hierarchy.IsDirty);
            Assert.IsFalse(hierarchy.IsSelfDirty);
            Assert.IsFalse(hierarchy.RootGroup.IsDirty);
            Assert.IsFalse(hierarchy.RootGroup.IsSelfDirty);

            //edit the root group
            hierarchy.RootGroup.Name = "edited";

            //check the flags are as expected
            Assert.IsTrue(hierarchy.IsDirty);
            Assert.IsFalse(hierarchy.IsSelfDirty);
            Assert.IsTrue(hierarchy.RootGroup.IsDirty);
            Assert.IsTrue(hierarchy.RootGroup.IsSelfDirty);

            //save
            hierarchy = hierarchy.Save();

            //check the header rowversion has changed
            Assert.AreNotEqual(preHeaderRowVersion.Value, hierarchy.RowVersion.Value, "Expected the header rowversion to have changed");
        }

        #endregion
    }
}
