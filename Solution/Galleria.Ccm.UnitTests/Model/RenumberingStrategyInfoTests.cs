﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class RenumberingStrategyInfoTests : TestBase
    {
        #region Test Helper Methods

        #region fields

        private EntityDto _entityDto;
        private IEnumerable<RenumberingStrategyDto> _modelDtos;

        #endregion

        private void InitializeTestData()
        {
            _entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).First();
            _modelDtos = TestDataHelper.InsertRenumberingStrategyDtos(DalFactory, 5, _entityDto.Id);
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            InitializeTestData();

            var model = RenumberingStrategyInfoList.FetchByEntityId(_entityDto.Id).First();

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void FetchRenumberingStrategyInfo()
        {
            InitializeTestData();

            var testModels = RenumberingStrategyInfoList.FetchByEntityId(_entityDto.Id);
            foreach (var expected in _modelDtos)
            {
                var dtoId = expected.Id;
                var actual = testModels.FirstOrDefault(info => info.Id == dtoId);

                AssertDtoAndModelAreEqual(expected, actual);
            }
        }
        #endregion
    }
}
