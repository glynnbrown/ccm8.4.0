﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeLevelTests : TestBase
    {
        #region Serializable
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeLevel model =
                Galleria.Ccm.Model.ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel();
            Serialize(model);
        } 
        #endregion

        #region Property Setters
        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeLevel model =
                Galleria.Ccm.Model.ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel();

            TestBase.TestPropertySetters<Galleria.Ccm.Model.ConsumerDecisionTreeLevel>(model);
        } 
        #endregion

        #region Factory Methods
        
        [Test]
        public void NewConsumerDecisionTreeLevel()
        {
            var model = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel();
            Assert.That(model.IsChild);

            model = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel("name");
            Assert.That(model.IsChild);
            Assert.IsNotNull(model.Name);
        }

        [Test]
        public void FetchConsumerDecisionTreeLevel()
        {
            //create a cdt hierarchy
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            ConsumerDecisionTreeDto cdtDto = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1)[0];
            List<ConsumerDecisionTreeLevelDto> cdtLevelDtoList = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(this.DalFactory, cdtDto, 3, true);
            List<ConsumerDecisionTreeNodeDto> nodeDtosList = TestDataHelper.InsertConsumerDecisionTreeNodeDtos(base.DalFactory, cdtDto, cdtLevelDtoList, 5, /*addRoot*/true);
            
            //fetch back the cdt
            Galleria.Ccm.Model.ConsumerDecisionTree cdt =
                Galleria.Ccm.Model.ConsumerDecisionTree.FetchById(cdtDto.Id);

            using (var dalContext = DalFactory.CreateContext())
            {
                foreach (var cdtLevelDto in cdtLevelDtoList)
                {
                    AssertDtoAndModelAreEqual(
                        cdtLevelDto,
                        ConsumerDecisionTreeLevel.FetchConsumerDecisionTreeLevel(dalContext, cdtLevelDto, cdtLevelDtoList));
                }
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataPortal_ChildInsertUpdateDelete()
        {
            //create a cdt hierarchy
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            
            // Prepare CDT and Levels.
            var parentCdtDto = TestDataHelper.InsertConsumerDecisionTreeDtos(DalFactory, entityId, 1)[0];
            var cdtLevelDtos = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(DalFactory, parentCdtDto, 10, true);
            var cdtNodes = TestDataHelper.InsertConsumerDecisionTreeNodeDtos(DalFactory, parentCdtDto, cdtLevelDtos, 10, true);
            var productDtos = TestDataHelper.InsertProductDtos(DalFactory, 10, entityId);
            TestDataHelper.InsertConsumerDecisionTreeNodeProductDtos(DalFactory, cdtNodes, productDtos, 1);

            // Get CDT from DTO.
            var parentCdt = ConsumerDecisionTree.FetchById(parentCdtDto.Id);

            //insert
            parentCdt = parentCdt.Save();
            var level= parentCdt.RootLevel;
            ConsumerDecisionTreeLevelDto dto;

            dto = FetchDtoFromLevel(level);
            AssertDtoAndModelAreEqual(dto, level);

            //update
            TestDataHelper.SetPropertyValues(level, new List<String>() { "LevelId", "ChildLevel", "ChildLevelList" });
            parentCdt = parentCdt.Save();
            level = parentCdt.RootLevel;

            dto = FetchDtoFromLevel(level);
            AssertDtoAndModelAreEqual(dto, level);

            //update again to test concurrency
            TestDataHelper.SetPropertyValues(level, new List<String>() { "LevelId", "ChildLevel", "ChildLevelList" });
            parentCdt = parentCdt.Save();
            level = parentCdt.RootLevel;

            dto = FetchDtoFromLevel(level);
            AssertDtoAndModelAreEqual(dto, level);

            //remove from the parent
            var levels = parentCdt.FetchAllLevels();
            level = levels[levels.Count - 1];
            Int32 levelId = (Int32)level.Id;
            parentCdt.RemoveLevel(level,true);
            parentCdt = parentCdt.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById((Int32)level.Id);
                        });
                }
            }
        }

        private ConsumerDecisionTreeLevelDto FetchDtoFromLevel(ConsumerDecisionTreeLevel level)
        {
            ConsumerDecisionTreeLevelDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
                {
                    dto = dal.FetchById((Int32)level.Id);
                }
            }
            return dto;
        }

        #endregion
    }
}
