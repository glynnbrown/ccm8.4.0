﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628 : A.Kuszyk
//		Created (Copied from SA)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<LocationDto> locationList =
            TestDataHelper.InsertLocationDtos(base.DalFactory, 20, entityId);

            LocationInfoList model = LocationInfoList.FetchByEntityId(1);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            List<LocationDto> locationList1 =
            TestDataHelper.InsertLocationDtos(base.DalFactory, 20, entityList[0].Id);

            List<LocationDto> locationList2 =
            TestDataHelper.InsertLocationDtos(base.DalFactory, 20, entityList[1].Id);

            LocationInfoList model1 = LocationInfoList.FetchByEntityId(entityList[0].Id);
            foreach (LocationDto locDto in locationList1)
            {
                LocationInfo info = model1.First(i => i.Id == locDto.Id);
            }

            LocationInfoList model2 = LocationInfoList.FetchByEntityId(entityList[1].Id);
            foreach (LocationDto locDto in locationList2)
            {
                LocationInfo info = model2.First(i => i.Id == locDto.Id);
            }
        }

        [Test]
        public void FetchByEntityIdLocationCodes()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, entityDtos);
            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 2);
            List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 1);

            List<LocationDto> locDtos1 = TestDataHelper.InsertLocationDtos(base.DalFactory, entityDtos[0].Id, groupDtos, 5);

            List<LocationDto> locDtos2 = TestDataHelper.InsertLocationDtos(base.DalFactory, entityDtos[0].Id, groupDtos, 5);


            LocationInfoList model1 = LocationInfoList.FetchByEntityIdLocationCodes(entityDtos[0].Id, locDtos1.Select(l => l.Code));
            foreach (LocationDto locDto in locDtos1)
            {
                LocationInfo info = model1.First(i => i.Id == locDto.Id);
            }

            LocationInfoList model2 = LocationInfoList.FetchByEntityIdLocationCodes(entityDtos[0].Id, locDtos2.Select(l => l.Code));
            foreach (LocationDto locDto in locDtos2)
            {
                LocationInfo info = model2.First(i => i.Id == locDto.Id);
            }
        }

        #endregion
    }
}
