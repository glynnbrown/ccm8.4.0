﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<UserDto> userList = TestDataHelper.InsertUserDtos(base.DalFactory, 5);
            UserInfoList infoList = UserInfoList.FetchAll();

            Serialize(infoList);

            Assert.IsTrue(infoList.IsReadOnly);
        }
        #endregion

        #region Factory Methods


        [Test]
        public void FetchAll()
        {
            List<UserDto> userList = TestDataHelper.InsertUserDtos(base.DalFactory, 5);
            UserInfoList infoList = UserInfoList.FetchAll();

            foreach (UserDto userDto in userList)
            {
                UserInfo info = infoList.First(i => i.Id == userDto.Id);
            }

        }

        #endregion

    }
}
