﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27720 : I.George
//		Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class AssortmentMinorRevisionInfoTests : TestBase
    {
        #region TestMethod Helpers
        private EntityDto _entityDto;
        private List<AssortmentMinorRevisionDto> _assortmentMinorRevisionDtos;

        private void InitializeTestData()
        {
            _entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).First();
            _assortmentMinorRevisionDtos = TestDataHelper.InsertAssortmentMinorRevisionDtos(DalFactory, 5, _entityDto.Id);
        }
        #endregion

        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            InitializeTestData();
            Entity entity = Entity.NewEntity();


            Galleria.Ccm.Model.AssortmentMinorRevisionInfoList model =
                Galleria.Ccm.Model.AssortmentMinorRevisionInfoList.FetchByEntityId(entity.Id);

            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void GethAssortmentMinorRevisionInfo()
        {
            InitializeTestData();

            var testModels = AssortmentMinorRevisionInfoList.FetchByEntityId(_entityDto.Id);
            foreach (var expected in _assortmentMinorRevisionDtos)
            {
                var dtoId = expected.Id;
                var actual = testModels.FirstOrDefault(info => info.Id == dtoId);

                AssertDtoAndModelAreEqual(expected, actual);
            }
        }
        #endregion
    }
}
