﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32524 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class FixtureAnnotationTests : TestBase<FixtureAnnotation, FixtureAnnotationDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out FixtureAnnotation model)
        {
            FixturePackage package = FixturePackage.NewFixturePackage();
            package.Name = Guid.NewGuid().ToString();
            package.Description = "Test";

            FixtureAnnotation anno = NewModel();
            anno.Text = "Test";
            package.Annotations.Add(anno);

            root = package;
            model = anno;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }


        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<FixturePackage>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<FixturePackage>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<FixturePackage>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<FixturePackage>();
        }

        #endregion

    }
}
