﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26124 : I.George
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class InventoryProfileInfoTest : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Assert that the properties of a model 
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">the dto to compare</param>
        /// <returns>The model to compare</returns>
        public static InventoryProfileInfoDto GetInfoDto(InventoryProfileDto dto)
        {
            return new InventoryProfileInfoDto()
            {
                Name = dto.Name,
                Id = dto.Id
            };
        }

        public static void AssertDtoAndModelAreEqual(InventoryProfileInfoDto dto, InventoryProfileInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
        }

        public static void AssertDtoAndModelAreEqual(InventoryProfileDto dto, InventoryProfileInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<InventoryProfileDto> dtoList = TestDataHelper.InsertInventoryProfileDtos(base.DalFactory, entityId, 20);

            //fetch for the list
            InventoryProfileInfoList infoList = InventoryProfileInfoList.FetchByEntityId(entityId);

            InventoryProfileInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods
        [Test]
        public void FetchInventoryProfileInfo()
        {
            //create an entity with required dto
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<InventoryProfileDto> dtoList = TestDataHelper.InsertInventoryProfileDtos(base.DalFactory, entityId, 20);

            //fetch the info list
            List<InventoryProfileInfoDto> infoList = new List<InventoryProfileInfoDto>();
            dtoList.ForEach(c => infoList.Add(GetInfoDto(c)));

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (InventoryProfileInfoDto dto in infoList)
                {
                    InventoryProfileInfo model = InventoryProfileInfo.FetchInventoryProfileInfo(dalContext, dto);
                    AssertDtoAndModelAreEqual(dto, model);
                }

            }

        }
        #endregion
    }
}
