﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM 8.0)
// V8-25669 : A.Kuszyk
//  Created (adapted from SA).
// V8-26099 : I.George
//  Added FetchByMerchandisingGroupId
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);

            var model = ProductInfoList.FetchByProductIds(ent1ProductDtoList.Select(d=>d.Id));

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityIdProductGtins()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);

            var model1 = ProductInfoList.FetchByEntityIdProductGtins(entity1.Id, ent1ProductDtoList.Select(p => p.Gtin).Distinct());

            //Ensure correct number are returned
            Assert.AreEqual(ent1ProductDtoList.Count, model1.Count);

            foreach (ProductDto prodDto in ent1ProductDtoList)
            {
                var info = model1.FirstOrDefault(i => i.Id == prodDto.Id);
                Assert.IsNotNull(info, "Matching product should have been returned");
            }
        }

        [Test]
        public void FetchByEntityIdProductIds()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);

            var model1 = ProductInfoList.FetchByEntityIdProductIds(
                entity1.Id, ent1ProductDtoList.Select(p => p.Id).Distinct());

            //Ensure correct number are returned
            Assert.AreEqual(ent1ProductDtoList.Count, model1.Count);

            foreach (ProductDto prodDto in ent1ProductDtoList)
            {
                var info = model1.FirstOrDefault(i => i.Id == prodDto.Id);
                Assert.IsNotNull(info, "Matching product should have been returned");
            }
        }

        [Test]
        public void FetchByProductIds()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);

            var model1 = ProductInfoList.FetchByProductIds(ent1ProductDtoList.Select(p => p.Id).Distinct());

            //Ensure correct number are returned
            Assert.AreEqual(ent1ProductDtoList.Count, model1.Count);

            foreach (ProductDto prodDto in ent1ProductDtoList)
            {
                var info = model1.FirstOrDefault(i => i.Id == prodDto.Id);
                Assert.IsNotNull(info, "Matching product should have been returned");
            }
        }

        [Test]
        public void FetchByEntityIdSearchCriteria()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);

            var model1 = ProductInfoList.FetchByEntityIdSearchCriteria(entity1.Id, " ");

            //Ensure correct number are returned
            Assert.AreEqual(ent1ProductDtoList.Count, model1.Count);

            foreach (ProductDto prodDto in ent1ProductDtoList)
            {
                var info = model1.FirstOrDefault(i => i.Id == prodDto.Id);
                Assert.IsNotNull(info, "Matching product should have been returned");
            }
        }

        [Test]
        public void FetchByProductUniverseId()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);
            var prodUnivList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, new List<EntityDto> { entity1 }, ent1Hierarchy1GroupDtos.Select(d => d.Id).ToList());
            TestDataHelper.InsertProductUniverseProductDtos(base.DalFactory, prodUnivList, ent1ProductDtoList);
            
            var model1 = ProductInfoList.FetchByProductUniverseId(prodUnivList[0].Id);

            //Ensure correct number are returned
            Assert.AreEqual(ent1ProductDtoList.Count, model1.Count);

            foreach (ProductDto prodDto in ent1ProductDtoList)
            {
                var info = model1.FirstOrDefault(i => i.Id == prodDto.Id);
                Assert.IsNotNull(info, "Matching product should have been returned");
            }
        }

        [Test]
        public void FetchByMerchandisingGroupId()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<ProductHierarchyDto> ent1HierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entity1.Id);
            List<ProductLevelDto> ent1Hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, ent1HierarchyDtos[0].Id, 3);
            List<ProductGroupDto> ent1Hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, ent1Hierarchy1levelDtos, 5);
            List<ProductDto> ent1ProductDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 50, entity1.Id);
            List<ProductUniverseDto> ent1ProductUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, new List<EntityDto> { entity1 }, ent1Hierarchy1GroupDtos.Select(d => d.Id).ToList());
            TestDataHelper.InsertProductUniverseProductDtos(base.DalFactory, ent1ProductUniverseDtoList, ent1ProductDtoList);

         
            ProductInfoList model1 = ProductInfoList.FetchByMerchandisingGroupId(ent1Hierarchy1GroupDtos[0].Id);
            //Ensure the correct number are returned
            foreach (ProductDto prodDto in ent1ProductDtoList)
            {
                ProductInfo info = model1.FirstOrDefault( i=> i.Id == prodDto.Id);
                Assert.AreEqual(prodDto.Id, info.Id);
                Assert.AreEqual(prodDto.Gtin, info.Gtin);
                Assert.AreEqual(prodDto.EntityId, info.EntityId);
                Assert.AreEqual(prodDto.Name, info.Name);
            }

            foreach (ProductDto productDto in ent1ProductDtoList)
            {
                var info = model1.FirstOrDefault(i => i.Id == productDto.Id);
                Assert.IsNotNull(info, "Matching product should have been returned");
            }
        }
        #endregion
    }
}
