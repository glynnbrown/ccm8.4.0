﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class BlockingLocationTests : TestBase<BlockingLocation, BlockingLocationDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out BlockingLocation model)
        {
            Blocking blocking = Blocking.NewBlocking(1);
            blocking.Name = Guid.NewGuid().ToString();
            blocking.Description = "Test";
            blocking.ProductGroupId = 1;

            BlockingLocation loc = NewModel();
            blocking.Locations.Add(loc);

            root = blocking;
            model = loc;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        [Test]
        public void Property_CalculatedProperties_UpdatedOnDividerMove()
        {
            Blocking item = Blocking.NewBlocking(1);
            item.Name = Guid.NewGuid().ToString();

            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            BlockingLocation loc1 = item.Locations[0];


            Boolean allocatedSpacePropertyChangedFired = false;

            loc1.PropertyChanged +=
                (s, e) =>
                {
                     if (e.PropertyName == BlockingLocation.SpacePercentageProperty.Name) allocatedSpacePropertyChangedFired = true;
                };

            //move the first divider
            item.Dividers[0].MoveTo(0.3F);
            Assert.IsTrue(allocatedSpacePropertyChangedFired, "allocated space Property change not fired");
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_BlockingGroup()
        {
            Blocking blocking = Blocking.NewBlocking(1);
            BlockingGroup g1 = blocking.Groups.AddNew();

            BlockingLocation loc = BlockingLocation.NewBlockingLocation(g1);

            Assert.AreEqual(loc.BlockingGroupId, g1.Id);
            Assert.IsTrue(loc.IsNew);
            Assert.IsTrue(loc.IsInitialized);
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<Blocking>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<Blocking>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<Blocking>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<Blocking>();
        }

        #endregion
    }
}
