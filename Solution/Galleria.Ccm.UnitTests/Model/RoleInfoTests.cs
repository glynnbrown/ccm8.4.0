﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26124 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class RoleInfoTests : TestBase
    {
        #region Test Helper Methods

        private void AssertDtoAndModelAreEqual(RoleDto dto, RoleInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.Description, model.Description);
        }

        private void AssertModelandModelAreEqual(RoleInfo model1, RoleInfo model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.Name, model2.Name);
            Assert.AreEqual(model1.Description, model2.Description);
        }

        private List<RoleDto> InsertTestDtos()
        {
            return TestDataHelper.InsertRoleDtos(base.DalFactory, 20, 1);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            InsertTestDtos();
            RoleInfoList infoList = RoleInfoList.FetchAll();
            RoleInfo model = infoList[0];
            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchRoleInfo()
        {
            List<RoleDto> dtoList = InsertTestDtos();
            RoleInfoList infoList = RoleInfoList.FetchAll();
            foreach (RoleDto distributionCentreDto in dtoList)
            {
                RoleInfo distCentreInfo = infoList.First(l => l.Id == distributionCentreDto.Id);
                AssertDtoAndModelAreEqual(distributionCentreDto, distCentreInfo);
            }
        }
        #endregion
    }
}
