﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PerformanceSelectionMetricTest : TestBase
    {
        #region Test Helper

        public static void AssertDtoAndModelAreEqual(PerformanceSelectionMetricDto dto, PerformanceSelectionMetric model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.GFSPerformanceSourceMetricId, model.GFSPerformanceSourceMetricId);
            Assert.AreEqual(dto.AggregationType, model.AggregationType);
            Assert.AreEqual(dto.PerformanceSelectionId, model.Parent.Id);
        }

        public static void AssertModelAndModelAreEqual(PerformanceSelectionMetric model, PerformanceSelectionMetric m2)
        {
            Assert.AreEqual(model.Id, m2.Id);
            Assert.AreEqual(model.GFSPerformanceSourceMetricId, m2.GFSPerformanceSourceMetricId);
            Assert.AreEqual(model.AggregationType, model.AggregationType);
            Assert.AreEqual(model.Parent.Id, model.Parent.Id);

        }
        #endregion 

        #region Serializable
        /// <summary>
        /// Serialize
        /// </summary>
        [Test]
        public void Serializable()
        {
            PerformanceSelectionMetric model = PerformanceSelectionMetric.NewPerformanceSelectionMetric();
            Serialize(model);
        }
        #endregion

        #region PropertySetters
        [Test]
        public void PropertySetters()
        {
            //model set on
            PerformanceSelectionMetric model = PerformanceSelectionMetric.NewPerformanceSelectionMetric();

            TestPropertySetters<PerformanceSelectionMetric>(model);
        }
        #endregion

        #region FactoryMethod

        [Test]
        public void NewPerformanceSelectionMetric()
        {
            TestCreateNew<Ccm.Model.PerformanceSelectionMetric>(true, new String[] { }, null, new String[] { "Parent" });
        }
        #endregion

        #region Methods
        [Test]
        public void FetchChild()
        {
            //fetch saved elements
            List<PerformanceSelectionMetricDto> metricList = new List<PerformanceSelectionMetricDto>();
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (PerformanceSelectionMetricDto dto in metricList)
                {
                    PerformanceSelectionMetric model = PerformanceSelectionMetric.Fetch(dalContext, dto);
                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }
        #endregion

    }
}
