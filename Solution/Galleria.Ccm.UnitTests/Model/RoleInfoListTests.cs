﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26124 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class RoleInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<RoleDto> RoleList =
            TestDataHelper.InsertRoleDtos(base.DalFactory, 20, entityId);

            RoleInfoList model = RoleInfoList.FetchAll();

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchAll()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            List<RoleDto> RoleList1 =
            TestDataHelper.InsertRoleDtos(base.DalFactory, 20, entityList[0].Id);

            RoleInfoList model = RoleInfoList.FetchAll();
            foreach (RoleDto roleDto in RoleList1)
            {
                RoleInfo info = model.First(i => i.Id == roleDto.Id);
            }
        }

        #endregion
    }
}
