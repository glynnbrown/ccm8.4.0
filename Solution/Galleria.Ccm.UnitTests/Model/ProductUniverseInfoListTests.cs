﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
// V8-26560 : A.Probyn
//  Added FetchByProductUniverseIds
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductUniverseInfoList : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtoList[0].Id);
            List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDtoList[0].Id, 4);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelDtoList, 10);
            List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, productGroupDtoList);

            Galleria.Ccm.Model.ProductUniverseInfoList infoList = Galleria.Ccm.Model.ProductUniverseInfoList.FetchByEntityId(entityDtoList[0].Id);

            Serialize(infoList);

            Assert.IsTrue(infoList.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtoList[0].Id);
            List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDtoList[0].Id, 4);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelDtoList, 10);
            List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, productGroupDtoList);

            foreach (IGrouping<Int32, ProductUniverseDto> entityGroup in productUniverseDtoList.GroupBy(p => p.EntityId))
            {
                Galleria.Ccm.Model.ProductUniverseInfoList infoList = Galleria.Ccm.Model.ProductUniverseInfoList.FetchByEntityId(entityDtoList[0].Id);
                foreach (ProductUniverseDto dto in entityGroup)
                {
                    Ccm.Model.ProductUniverseInfo info = infoList.First(i => i.Id == dto.Id);
                }
            }
        }

        [Test]
        public void FetchByProductUniverseIds()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtoList[0].Id);
            List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDtoList[0].Id, 4);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelDtoList, 10);
            List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, productGroupDtoList);

            foreach (IGrouping<Int32, ProductUniverseDto> entityGroup in productUniverseDtoList.GroupBy(p => p.EntityId))
            {
                Galleria.Ccm.Model.ProductUniverseInfoList infoList = Galleria.Ccm.Model.ProductUniverseInfoList.FetchByProductUniverseIds(productUniverseDtoList.Select(p => p.Id));
                Assert.AreEqual(productUniverseDtoList.Count, infoList.Count);
                foreach (ProductUniverseDto dto in productUniverseDtoList)
                {
                    Assert.IsNotNull(infoList.FirstOrDefault(p => p.Id == dto.Id));
                }
            }
        }

        #endregion
    }
}
