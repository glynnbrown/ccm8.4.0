﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26474 : A.Silva ~ Created
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using NUnit.Framework;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ValidationTemplateTests : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(ValidationTemplate.NewValidationTemplate(1));
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters(ValidationTemplate.NewValidationTemplate(1));
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewValidationTemplate()
        {
            var newValidationTemplate = ValidationTemplate.NewValidationTemplate(1);

            Assert.AreEqual(newValidationTemplate.EntityId, 1);
            Assert.IsTrue(newValidationTemplate.IsNew);
            Assert.IsFalse(newValidationTemplate.IsChild, "A newly created Validation Template should be a root object.");
        }

        [Test]
        public void FetchById()
        {
            var entityDtos = TestDataHelper.InsertEntityDtos(DalFactory, 1);
            var validationTemplateDtos = TestDataHelper.InsertValidationTemplateDtos(DalFactory, 5, entityDtos[0].Id);

            foreach (var dto in validationTemplateDtos)
            {
                var validationTemplate = ValidationTemplate.FetchById(dto.Id);

                AssertHelper.AssertModelObjectsAreEqual(dto, validationTemplate);
                Assert.IsFalse(validationTemplate.IsChild, "Should have been fetched as a root object.");
            }
        }

        [Test]
        public void FetchByEntityIdName()
        {
            var entityDtos = TestDataHelper.InsertEntityDtos(DalFactory, 1);
            var validationTemplateDtos = TestDataHelper.InsertValidationTemplateDtos(DalFactory, 5, entityDtos[0].Id);

            foreach (var dto in validationTemplateDtos)
            {
                var validationTemplate = ValidationTemplate.FetchByEntityIdName(dto.EntityId, dto.Name);

                AssertHelper.AssertModelObjectsAreEqual(dto, validationTemplate);
                Assert.IsFalse(validationTemplate.IsChild, "Should have been fetched as a root object.");
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            var validationTemplate = SaveNewValidationTemplate();

            var dto = FetchValidationTemplateDto(validationTemplate.Id);

            AssertHelper.AssertModelObjectsAreEqual(dto, validationTemplate);
        }

        [Test]
        public void DataAccess_Update()
        {
            var validationTemplate = SaveNewValidationTemplate();

            // Update.
            validationTemplate.Name = "validationTemplate2";
            validationTemplate = validationTemplate.Save();

            var dto = FetchValidationTemplateDto(validationTemplate.Id);

            AssertHelper.AssertModelObjectsAreEqual(dto,validationTemplate, new List<String>{"DateLastModified"});
        }

        [Test]
        public void DataAccess_Delete()
        {
            var validationTemplate = SaveNewValidationTemplate();
            var dto = FetchValidationTemplateDto(validationTemplate.Id);
            AssertHelper.AssertModelObjectsAreEqual(dto, validationTemplate);

            // Delete it.
            var itemId = validationTemplate.Id;
            validationTemplate.Delete();
            validationTemplate.Save();

            Assert.Throws(typeof(DtoDoesNotExistException),
                () =>
                {
                    dto = FetchValidationTemplateDto(validationTemplate.Id);
                });
        }

        #endregion

        #region IValidationTemplate Members

        [Test]
        public void IValidationTemplateGroups_GetAccessor_GetsGroups()
        {
            const string expectation = "When getting Groups through the interface the actual groups are returned.";
            var testModel = ValidationTemplate.NewValidationTemplate(1);
            var expected = testModel.Groups;
            expected.Add(ValidationTemplateGroup.NewValidationTemplateGroup());

            var actual = ((IValidationTemplate)testModel).Groups;

            CollectionAssert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void IValidationTemplateAddNewGroup_Invoked_AddsNewGroup()
        {
            const string expectation = "When AddNewGroup is invoked the Groups collection gets a new group.";
            var testModel = ValidationTemplate.NewValidationTemplate(1);

            ((IValidationTemplate)testModel).AddNewGroup("TEST");

            var actual = testModel.Groups.Any();
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void IValidationTemplateRemoveGroup_ContainsGroup_RemovesGroup()
        {
            const string expectation = "When RemoveGroup is invoked with an existing group the groups collection removes it.";
            var testModel = ValidationTemplate.NewValidationTemplate(1);
            var itemToRemove = ValidationTemplateGroup.NewValidationTemplateGroup();
            testModel.Groups.Add(itemToRemove);

            ((IValidationTemplate)testModel).RemoveGroup(itemToRemove);

            var actual = !testModel.Groups.Contains(itemToRemove);
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void IValidationTemplateRemoveGroup_DoesNotContainGroup_DoesNotChangeGroups()
        {
            const string expectation = "When RemoveGroup is invoked with a non existing group the groups collection remains unchanged.";
            var testModel = ValidationTemplate.NewValidationTemplate(1);
            var itemToRemove = ValidationTemplateGroup.NewValidationTemplateGroup();
            testModel.Groups.Add(itemToRemove);
            var unrelatedItem = ValidationTemplateGroup.NewValidationTemplateGroup();

            ((IValidationTemplate)testModel).RemoveGroup(unrelatedItem);

            var actual = testModel.Groups.Contains(itemToRemove);
            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region Test Helper Methods

        private ValidationTemplate SaveNewValidationTemplate()
        {
            var entityDtos = TestDataHelper.InsertEntityDtos(DalFactory, 1);

            // Create the new item.
            var validationTemplate = ValidationTemplate.NewValidationTemplate(entityDtos[0].Id);

            // Set properties so they are valid.
            validationTemplate.Name = "validationTemplate1";

            // Save.
            validationTemplate = validationTemplate.Save();
            return validationTemplate;
        }

        private ValidationTemplateDto FetchValidationTemplateDto(Object validationTemplateId)
        {
            ValidationTemplateDto dto;
            using (var dalContext = DalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IValidationTemplateDal>())
            {
                dto = dal.FetchById(validationTemplateId);
            }
            return dto;
        }

        #endregion
    }
}