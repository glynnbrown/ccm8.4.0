﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ClusterSchemeInfoTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Lack of NewClusterSchemeInfo factory method
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            //No property setters
        }

        #endregion

        #region Factory Methods

        [Test]
        public void FetchClusterSchemeInfo()
        {
            //Insert location dtos
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Inset cluster scheme data
            ClusterSchemeDto clusterSchemeDto = TestDataHelper.InsertSingeClusterSchemeData(this.DalFactory, locationDtos);

            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                // fetch the model (use infolist to test as this utilises the internal method)
                Galleria.Ccm.Model.ClusterSchemeInfoList modelList = Galleria.Ccm.Model.ClusterSchemeInfoList.FetchByEntityId(1);

                Assert.AreEqual(1, modelList.Count, "1 cluster scheme should have been returned");

                //check correct attributes have been returned
                Galleria.Ccm.Model.ClusterSchemeInfo model = modelList[0];
                Assert.AreEqual(clusterSchemeDto.Id, model.Id, "Id should match");
                Assert.AreEqual(clusterSchemeDto.Name, model.Name, "Name should match");
                Assert.AreEqual(clusterSchemeDto.EntityId, model.EntityId, "Entity Id should match");
                Assert.AreEqual(clusterSchemeDto.IsDefault, model.IsDefault, "Id should match");
                Assert.AreEqual(locationDtos.Count(), model.ClusterCount, "Cluster count should be 10");
            }
        }

        #endregion
    }
}
