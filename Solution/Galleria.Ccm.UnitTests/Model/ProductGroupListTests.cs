﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ProductGroupListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            ProductGroupList model =
                ProductGroupList.NewProductGroupList();
            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewProductGroupList()
        {
            ProductGroupList model = ProductGroupList.NewProductGroupList();

            Assert.IsTrue(model.IsChild, "the model should be marked a child");
            Assert.IsEmpty(model, "the model should be empty");
        }

        [Test]
        public void FetchByParentProductGroupId()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<ProductLevelDto> levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 5);
            List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelDtos, 4);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (ProductGroupDto dto in groupDtos)
                {
                    IEnumerable<ProductGroupDto> expectedChildren = groupDtos.Where(g => g.ParentGroupId == dto.Id);
                    var model = ProductGroupList.FetchByParentProductGroupId(dalContext, dto.Id, groupDtos);

                    Assert.AreEqual(expectedChildren.Count(), model.Count);

                    foreach (ProductGroupDto childDto in expectedChildren)
                    {
                        var item = model.First(m => m.Id == childDto.Id);
                        ProductGroupTests.AssertDtoAndModelAreEqual(childDto, item);
                    }

                }

            }

        }

        #endregion
    }
}
