﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConnectionTests : TestBase
    {

        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Connection model =
                Connection.NewConnection();
            Serialize(model);
        }

        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            Connection model =
                Connection.NewConnection();

            TestBase.TestPropertySetters<Connection>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewConnection()
        {
            Connection model =
                Connection.NewConnection();

            Assert.IsTrue(model.IsChild, " Model should be marked as a child");
            Assert.IsTrue(model.IsNew, " Model should be marked as a new object");
        }

        [Test]
        public void GetConnection()
        {
            UserSystemSetting modelSettings =
                UserSystemSetting.FetchUserSettings(true);

            //Add a non-current connection
            ConnectionDto connectionDto = new ConnectionDto()
            {
                DatabaseName = "TestDB",
                ServerName = "TestServer",
                PortNumber = 5,
                IsAutoConnect = false,
                IsCurrentConnection = false,
                DatabaseFileName = String.Empty,
                //Type = (Byte)ConnectionType.Database
            };
            Connection modelConn = Connection.NewConnection();
            modelConn.DatabaseName = "TestDB";
            modelConn.ServerName = "TestServer";
            modelConn.PortNumber = 5;
            modelConn.DatabaseFileName = String.Empty;
            modelConn.IsAutoConnect = false;
            modelConn.IsCurrentConnection = false;
            modelSettings.Connections.Add(modelConn);

            modelSettings = modelSettings.Save();

            //Refetch the settings list
            UserSystemSetting dalSettings =   UserSystemSetting.FetchUserSettings(false);

            Assert.IsNotEmpty(dalSettings.Connections, "settings should contain the added connection");

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                Connection dalConn = Connection.GetConnection(dalContext, connectionDto);
                AssertHelper.AssertModelObjectsAreEqual(connectionDto, dalConn);
            }
        }

        #endregion

        #region DataAccess


        [Test]
        public void Child_Insert()
        {
            throw new InconclusiveException("TODO");
            //UserSettings modelSettings =
            //    UserSettings.FetchUserSettings(true);

            ////Add a non-current connection
            //ConnectionDto connectionDto = new ConnectionDto()
            //{
            //    DatabaseName = "TestDB",
            //    ServerName = "TestServer",
            //    PortNumber = 5,
            //    IsAutoConnect = false,
            //    IsCurrentConnection = false,
            //    //Type = (Byte)ConnectionType.Database
            //};
            //Connection modelConn = Connection.NewConnection(connectionDto);
            //modelSettings.Connections.Add(modelConn);

            ////Save down
            //modelSettings = modelSettings.Save();

            ////Refetch the settings list
            //UserSettings dalSettings =
            //    UserSettings.FetchUserSettings(false);

            //Assert.IsNotEmpty(dalSettings.Connections, "settings should contain the added connection");

            //using (IDalContext dalContext = base.DalFactory.CreateContext())
            //{
            //    Connection dalConn =
            //        Connection.GetConnection(dalContext, connectionDto);
            //    AssertHelper.AssertModelObjectsAreEqual(connectionDto, dalConn);
            //}
        }

        [Test]
        public void Child_Update()
        {
            throw new InconclusiveException("TODO");
            //UserSettings modelSettings =
            //    UserSettings.FetchUserSettings(true);

            ////Add a non-current connection
            //ConnectionDto connectionDto = new ConnectionDto()
            //{
            //    DatabaseName = "TestDB",
            //    ServerName = "TestServer",
            //    PortNumber = 5,
            //    IsAutoConnect = false,
            //    IsCurrentConnection = false,
            //    //ype = (Byte)ConnectionType.Database
            //};
            //Connection modelConn =
            //    Connection.NewConnection(connectionDto);
            //modelSettings.Connections.Add(modelConn);

            ////Save down
            //modelSettings = modelSettings.Save();
            //Assert.IsNotEmpty(modelSettings.Connections, "settings should contain the added connection");
            //modelConn = modelSettings.Connections[0];

            ////edit and re-save
            ////Note: We cannot update the ServerName, Databasename, PortNumber and DatabaseFilename
            ////as thse (in the absence of an Id field) are used to identify the existing connection
            ////to update. If no matching connections are found, nothing is updated.
            ////modelConn.DatabaseName = "TestDB2";
            ////modelConn.ServerName = "TestServer2";
            ////modelConn.PortNumber = 4;
            //modelConn.IsAutoConnect = true;
            //modelConn.IsCurrentConnection = true;

            //modelSettings = modelSettings.Save();

            ////Refetch the settings list
            //UserSettings dalSettings =
            //    UserSettings.FetchUserSettings(false);
            //Assert.IsNotEmpty(dalSettings.Connections, "settings should contain the updated connection");

            //Connection dalConn = dalSettings.Connections[0];

            ////Check the newly fetched item matchees the updated model item
            //AssertHelper.AssertModelObjectsAreEqual(modelConn, dalConn);
        }

        #endregion

    }
}
