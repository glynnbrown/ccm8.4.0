﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttributeInfo : TestBase
    {
        //private Int32 _entityId;

        //#region Helpers

        //private List<LocationProductAttributeDto> InsertDtos()
        //{
        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();
        //    _entityId = entity.Id;
        //    //Add Products
        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productDtoList = TestDataHelper.PopulateProductHierarchyWithProducts(hierarchy, 5);
        //    //Add Locations
        //    List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, _entityId, new List<Int32> { 1, 2 }, 2);

        //    List<LocationProductAttributeDto> dtosList =
        //        TestDataHelper.InsertLocationProductAttributeDtos(this.DalFactory, locationDtoList, productDtoList, _entityId);

        //    return dtosList;
        //}

        //#endregion

        //#region Serializable
        ///// <summary>
        ///// Serializable
        ///// </summary>
        //[Test]
        //public void Serializable()
        //{
        //    InsertDtos();

        //    Galleria.Ccm.Model.LocationProductAttributeInfoList infoList =
        //        Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(_entityId);

        //    Serialize(infoList[0]);
        //}
        //#endregion

        //#region Factory Methods

        //[Test]
        //public void GetLocationProductAttributeInfo()
        //{
        //    List<LocationProductAttributeDto> dtosList = InsertDtos();

        //    Galleria.Ccm.Model.LocationProductAttributeInfoList infoList =
        //        Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(_entityId);

        //    foreach (LocationProductAttributeDto dto in dtosList)
        //    {
        //        Galleria.Ccm.Model.LocationProductAttributeInfo info =
        //            infoList.FirstOrDefault(i =>
        //                i.ProductId == dto.ProductId &&
        //                i.LocationId == dto.LocationId);

        //        AssertHelper.AssertModelObjectsAreEqual(info, dto);
        //    }
        //}

        //#endregion
    }
}
