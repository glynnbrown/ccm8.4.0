﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ClusterSchemeInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster scheme data
            ClusterSchemeDto clusterSchemeDto = TestDataHelper.InsertSingeClusterSchemeData(this.DalFactory, locationDtos);

            //Load cluster scheme model object
            Ccm.Model.ClusterSchemeInfoList clusterSchemeInfoList = Ccm.Model.ClusterSchemeInfoList.FetchByEntityId(1);

            Serialize(clusterSchemeInfoList);

            Assert.IsTrue(clusterSchemeInfoList.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster scheme data
            ClusterSchemeDto clusterSchemeDto = TestDataHelper.InsertSingeClusterSchemeData(this.DalFactory, locationDtos);

            //Load cluster scheme model object
            Ccm.Model.ClusterSchemeInfoList clusterSchemeInfoList = Ccm.Model.ClusterSchemeInfoList.FetchByEntityId(1);

            //Check clusters have been fetched correctly (1 cluster per location)
            Assert.AreEqual(1, clusterSchemeInfoList.Count, "1 cluster scheme info should have been returned for this entity");

            //check correct attributes have been returned
            Galleria.Ccm.Model.ClusterSchemeInfo model = clusterSchemeInfoList[0];
            Assert.AreEqual(clusterSchemeDto.Id, model.Id, "Id should match");
            Assert.AreEqual(clusterSchemeDto.Name, model.Name, "Name should match");
            Assert.AreEqual(clusterSchemeDto.EntityId, model.EntityId, "Entity Id should match");
            Assert.AreEqual(clusterSchemeDto.IsDefault, model.IsDefault, "Id should match");
            Assert.AreEqual(locationDtos.Count(), model.ClusterCount, "Cluster count should be 10");
        }

        #endregion
    }
}
