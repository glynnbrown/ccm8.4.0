﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class LabelTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(Label.NewLabel(LabelType.Product));
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<Label>(Label.NewLabel(LabelType.Product));
        }

        #endregion

        #region Factory Methods

        [Test]
        public void CreateNew()
        {
            var newLabel = Label.NewLabel(LabelType.Product);

            Assert.IsTrue(newLabel.IsNew);
            Assert.IsFalse(newLabel.IsChild, "A newly created Label should be a root object.");
        }

        [Test]
        public void Fetch()
        {
            //insert a dto to fetch
            LabelDto dto = new LabelDto();
            PopulatePropertyValues1<LabelDto>(dto);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dal.Insert(dto);
                }
            }

            //fetch
            Label Label = Label.FetchById(dto.Id);

            Label model = Label;
            AssertDtoAndModelAreEqual<LabelDto, Label>(dto, model);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            Label label = Label.NewLabel(LabelType.Product);
            label.Name = "l1";

            //save
            label = label.Save();
            
            LabelDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dto = dal.FetchById(label.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, label);
        }

        [Test]
        public void DataAccess_Update()
        {
            Label label = Label.NewLabel(LabelType.Product);
            label.Name = "l1";

            //save
            label = label.Save();

            LabelDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dto = dal.FetchById(label.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, label);

            //change the name
            label.Name = "l1#2";

            label.Save();


            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dto = dal.FetchById(label.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, label);
        }

        [Test]
        public void DataAccess_Delete()
        {
            Label label = Label.NewLabel(LabelType.Product);
            label.Name = "l1";

            //save
            label = label.Save();

            LabelDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dto = dal.FetchById(label.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, label);

            label.Delete();
            label.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dto = dal.FetchById(label.Id);

                    Assert.IsNotNull(dto.DateDeleted, "DateDeleted should be marked.");

                    //Assert.Throws(typeof(DtoDoesNotExistException),
                    //    () =>
                    //    {
                    //        dto = dal.FetchById(1);
                    //    },
                    //    "model should no longer exist"
                    //);
                }
            }
        }

        #endregion
    }
}
