﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserInfoTests : TestBase
    {
        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(UserDto dto, UserInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.UserName, model.UserName);
            Assert.AreEqual(dto.FirstName, model.FirstName);
            Assert.AreEqual(dto.LastName, model.LastName);
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            List<UserDto> locDtos = TestDataHelper.InsertUserDtos(base.DalFactory, 5);
            UserInfoList infoList = UserInfoList.FetchAll();

            UserInfo model = infoList[0];

            Serialize(model);
        }

        [Test]
        public void FetchUserInfo()
        {
            List<UserDto> dtoList = TestDataHelper.InsertUserDtos(base.DalFactory, 5);
            UserInfoList infoList = UserInfoList.FetchAll();

            foreach (UserDto UserDto in dtoList)
            {
                UserInfo dalLocInfo = infoList.First(l => l.Id == UserDto.Id);
                AssertDtoAndModelAreEqual(UserDto, dalLocInfo);
            }
        }

        #endregion

    }
}
