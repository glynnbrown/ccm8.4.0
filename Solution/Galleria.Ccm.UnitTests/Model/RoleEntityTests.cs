﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26124 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class RoleEntityTests : TestBase
    {
        #region Test Helper Methods

        private void AssertDtoAndModelAreEqual(RoleEntityDto dto, RoleEntity model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
        }

        private void AssertModelAndModelAreEqual(RoleEntity model1, RoleEntity model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.EntityId, model2.EntityId);
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            RoleEntity model =
                RoleEntity.NewRoleEntity(0);
            Serialize(model);
        }

        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            RoleEntity model = RoleEntity.NewRoleEntity(0);
            TestBase.TestPropertySetters<RoleEntity>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewRoleEntity()
        {
            RoleEntity newRoleEntity =
                RoleEntity.NewRoleEntity(123);

            Assert.AreEqual(123, newRoleEntity.EntityId);

            Assert.IsTrue(newRoleEntity.IsNew);
        }

        #endregion

        #region Data Access

        #region Insert

        [Test]
        public void DataAccess_Insert()
        {
            // get the entity id
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;

            //Create parent Role
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 1, entityId, false);

            //Create the RoleEntity
            RoleEntity roleEntity =
                RoleEntity.NewRoleEntity(entityId);
            Role role = Role.FetchById(roleList[0].Id);
            role.Entities.Add(roleEntity);

            //save
            role = role.Save();

            // fetch the role entity dtos
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                using (IRoleEntityDal dal = dalContext.GetDal<IRoleEntityDal>())
                {
                    IEnumerable<RoleEntityDto> dtoList = dal.FetchByRoleId(role.Id);
                    foreach (RoleEntityDto dto in dtoList)
                    {
                        RoleEntity model = role.Entities.First(item => item.Id == dto.Id);
                        AssertDtoAndModelAreEqual(dto, model);
                    }
                }
            }
        }
        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            // get the entity id
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;

            //Create parent Role
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 1, entityId, false);

            //Create the RoleEntity
            RoleEntity roleEntity =
                RoleEntity.NewRoleEntity(entityId);
            Role role = Role.FetchById(roleList[0].Id);
            role.Entities.Add(roleEntity);

            // save
            role = role.Save();
            roleEntity = role.Entities[0];

            // delete the item
            role.Entities.Remove(roleEntity);
            role.Save();

            // verify that the role entity does not exist
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                using (IRoleEntityDal dal = dalContext.GetDal<IRoleEntityDal>())
                {
                    Assert.AreEqual(0, dal.FetchByRoleId(role.Id).Count());
                }
            }
        }
        #endregion

        #endregion
    }
}
