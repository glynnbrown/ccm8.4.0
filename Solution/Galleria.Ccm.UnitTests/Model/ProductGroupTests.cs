﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ProductGroupTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(ProductGroupDto dto, ProductGroup model)
        {
            Assert.AreEqual(10, typeof(ProductGroupDto).GetProperties().Count());

            Assert.AreEqual(dto.Code, model.Code);
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.ProductLevelId, model.ProductLevelId);
            Assert.AreEqual(dto.Name, model.Name);
            //ParentGroupid
            //dto key
            //datecreated
            //datelastmodified
            //datedeleted
        }
        #endregion

        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(ProductGroup.NewProductGroup(1));
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<ProductGroup>(ProductGroup.NewProductGroup(1));
        }

        #endregion

        #region FactoryMethods

        [Test]
        public void NewProductGroup()
        {
            Int32 levelId = 1;

            ProductGroup newGroup =
                ProductGroup.NewProductGroup(levelId);

            Assert.IsTrue(newGroup.IsNew);
            Assert.IsTrue(newGroup.IsChild);

            Assert.IsNotNull(newGroup.ChildList);
            Assert.AreEqual(newGroup.ProductLevelId, levelId);
        }

        [Test]
        public void FetchProductGroup()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<ProductLevelDto> levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 1);
            List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelDtos, 2);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (ProductGroupDto dto in groupDtos)
                {
                    var model = ProductGroup.Fetch(dalContext, dto, groupDtos);
                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }

        #endregion

        #region Data Access


        [Test]
        public void DataAccess_Insert()
        {
            //create a new hierarchy (will have root group)
            ProductHierarchy hierarchy =
                ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";
            ProductGroup rootGroup = hierarchy.RootGroup;

            //set properties
            rootGroup.Name = "test";
            rootGroup.Code = "testCode";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //fetch back the level dto
            ProductGroupDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(rootGroup.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, rootGroup);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create a new hierarchy (will have root group)
            ProductHierarchy hierarchy =
                ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";
            ProductGroup rootGroup = hierarchy.RootGroup;

            //set properties
            rootGroup.Name = "test";
            rootGroup.Code = "testCode";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //update
            rootGroup.Name = "test#";
            rootGroup.Code = "testCode#";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //fetch back the level dto
            ProductGroupDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(rootGroup.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, rootGroup);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create a new hierarchy (will have root group)
            ProductHierarchy hierarchy =
                ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";
            ProductLevel level = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup group = ProductGroup.NewProductGroup(level.Id);
            hierarchy.RootGroup.ChildList.Add(group);

            //set properties
            group.Name = "test";
            group.Code = "testCode";

            //save
            hierarchy = hierarchy.Save();
            group = hierarchy.RootGroup.ChildList[0];

            //fetch back the level dto
            ProductGroupDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(group.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, group);

            //delete it
            Int32 itemId = group.Id;
            hierarchy.RootGroup.ChildList.Remove(group);
            hierarchy = hierarchy.Save();


            //try to retrieve again
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(itemId);
                    Assert.IsNotNull(dto.DateDeleted);
                }
            }
            AssertDtoAndModelAreEqual(dto, group);
        }

        #endregion

        #region Helper Methods

        [Test]
        public void FetchAllChildGroups()
        {
            //create a new hierarchy (will have root group)
            ProductHierarchy hierarchy =
                ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";
            ProductLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup group1 = ProductGroup.NewProductGroup(level1.Id);
            ProductGroup group2 = ProductGroup.NewProductGroup(level1.Id);
            hierarchy.RootGroup.ChildList.Add(group1);
            hierarchy.RootGroup.ChildList.Add(group2);
            ProductLevel level2 = hierarchy.InsertNewChildLevel(level1);
            ProductGroup group3 = ProductGroup.NewProductGroup(level2.Id);
            ProductGroup group4 = ProductGroup.NewProductGroup(level2.Id);
            group1.ChildList.Add(group3);
            group2.ChildList.Add(group4);

            List<ProductGroup> groupList = hierarchy.RootGroup.EnumerateAllChildGroups().ToList();
            Assert.AreEqual(5, groupList.Count());

            Assert.Contains(hierarchy.RootGroup, groupList);
            Assert.Contains(group1, groupList);
            Assert.Contains(group2, groupList);
            Assert.Contains(group3, groupList);
            Assert.Contains(group4, groupList);
        }

        [Test]
        public void FetchParentPath()
        {
            //create a new hierarchy (will have root group)
            ProductHierarchy hierarchy =
                ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";
            ProductLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup group1 = ProductGroup.NewProductGroup(level1.Id);
            ProductGroup group2 = ProductGroup.NewProductGroup(level1.Id);
            hierarchy.RootGroup.ChildList.Add(group1);
            hierarchy.RootGroup.ChildList.Add(group2);
            ProductLevel level2 = hierarchy.InsertNewChildLevel(level1);
            ProductGroup group3 = ProductGroup.NewProductGroup(level2.Id);
            ProductGroup group4 = ProductGroup.NewProductGroup(level2.Id);
            group1.ChildList.Add(group3);
            group2.ChildList.Add(group4);

            List<ProductGroup> parentPathList = group4.GetParentPath();
            Assert.AreEqual(2, parentPathList.Count);

            Assert.Contains(group2, parentPathList);
            Assert.Contains(hierarchy.RootGroup, parentPathList);
        }

        [Test]
        public void FetchAssociatedLevel()
        {
            //create a new hierarchy (will have root group)
            ProductHierarchy hierarchy =
                ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";
            ProductLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup group1 = ProductGroup.NewProductGroup(level1.Id);
            ProductGroup group2 = ProductGroup.NewProductGroup(level1.Id);
            hierarchy.RootGroup.ChildList.Add(group1);
            hierarchy.RootGroup.ChildList.Add(group2);
            ProductLevel level2 = hierarchy.InsertNewChildLevel(level1);
            ProductGroup group3 = ProductGroup.NewProductGroup(level2.Id);
            ProductGroup group4 = ProductGroup.NewProductGroup(level2.Id);
            group1.ChildList.Add(group3);
            group2.ChildList.Add(group4);

            Assert.AreEqual(hierarchy.RootLevel, hierarchy.RootGroup.GetAssociatedLevel());
            Assert.AreEqual(level1, group1.GetAssociatedLevel());
            Assert.AreEqual(level1, group2.GetAssociatedLevel());
            Assert.AreEqual(level2, group3.GetAssociatedLevel());
            Assert.AreEqual(level2, group4.GetAssociatedLevel());
        }

        #endregion

        #region Other Scenarios

        [Test]
        public void MoveItemFromOneCollectionToAnother()
        {
            //create a hierarchy with 2 areas & a dept under each
            ProductHierarchy hierarchy =
               ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";

            ProductLevel arealevel = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup area1 = ProductGroup.NewProductGroup(arealevel.Id);
            area1.Name = "area1";
            area1.Code = "aCode1";
            ProductGroup area2 = ProductGroup.NewProductGroup(arealevel.Id);
            area2.Name = "area2";
            area2.Code = "aCode2";
            hierarchy.RootGroup.ChildList.Add(area1);
            hierarchy.RootGroup.ChildList.Add(area2);

            ProductLevel deptLevel = hierarchy.InsertNewChildLevel(arealevel);
            ProductGroup dept1 = ProductGroup.NewProductGroup(deptLevel.Id);
            dept1.Name = "dept1";
            dept1.Code = "dCode1";
            ProductGroup dept2 = ProductGroup.NewProductGroup(deptLevel.Id);
            dept2.Name = "dept2";
            dept2.Code = "dCode2";
            area1.ChildList.Add(dept1);
            area2.ChildList.Add(dept2);

            //save
            hierarchy = hierarchy.Save();

            //check area1 has 1 child & area 2 has 1 child
            area1 = hierarchy.RootGroup.ChildList.FirstOrDefault(g => g.Name == "area1");
            Assert.AreEqual(1, area1.ChildList.Count);

            area2 = hierarchy.RootGroup.ChildList.FirstOrDefault(g => g.Name == "area2");
            Assert.AreEqual(1, area2.ChildList.Count);


            //get the dept ids
            dept1 = hierarchy.RootGroup.ChildList[0].ChildList.FirstOrDefault(g => g.Name == "dept1");
            Object dept1Id = dept1.Id;
            dept2 = hierarchy.RootGroup.ChildList[1].ChildList.FirstOrDefault(g => g.Name == "dept2");
            Object dept2Id = dept2.Id;

            //move dept1 form area1 to area2
            area2.ChildList.Add(dept1);
            area1.ChildList.Remove(dept1);
            Assert.AreEqual(0, area1.ChildList.Count);

            //save
            hierarchy = hierarchy.Save();

            //refetch the hierarchy from scratch
            hierarchy = ProductHierarchy.FetchById(hierarchy.Id);

            //check area1 has no children
            area1 = hierarchy.RootGroup.ChildList.FirstOrDefault(g => g.Name == "area1");
            Assert.AreEqual(0, area1.ChildList.Count);

            //check area2 has 2 children
            area2 = hierarchy.RootGroup.ChildList.FirstOrDefault(g => g.Name == "area2");
            Assert.AreEqual(2, area2.ChildList.Count);

            //check the dept ids have not changed
            Assert.AreEqual(dept2Id, area2.ChildList[0].Id);
            Assert.AreEqual(dept1Id, area2.ChildList[1].Id);
        }

        [Test]
        public void GEM14222_DeletingMiddleLevelMovesChildGroups()
        {
            //create a hierarchy with 3 levels and a group at each
            ProductHierarchy hierarchy =
               ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";

            //level 1
            ProductLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup group1 = ProductGroup.NewProductGroup(level1.Id);
            group1.Name = "group1";
            group1.Code = "code1";
            hierarchy.RootGroup.ChildList.Add(group1);

            //level 2
            ProductLevel level2 = hierarchy.InsertNewChildLevel(level1);
            ProductGroup group2 = ProductGroup.NewProductGroup(level2.Id);
            group2.Name = "group2";
            group2.Code = "code2";
            group1.ChildList.Add(group2);

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;
            group1 = hierarchy.RootGroup.ChildList[0];
            group2 = group1.ChildList[0];

            //remove level 1
            hierarchy.RemoveLevel(level1);

            //check that group 2 is now the child of the rootGroup
            Assert.AreEqual(group2.Id, hierarchy.RootGroup.ChildList[0].Id);

            //save
            hierarchy = hierarchy.Save();

            //check the hierarchy returned now only has 2 levels and 2 groups
            Assert.AreEqual(level2.Id, hierarchy.RootLevel.ChildLevel.Id);
            Assert.AreEqual(group2.Id, hierarchy.RootGroup.ChildList[0].Id);

            //try to fetch back group 1 - should fail
            ProductGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(group1.Id);
                    Assert.IsNotNull(dto.DateDeleted, "group 1 should no longer exist");
                }
            }

        }

        [Test]
        public void GEM14222_DeletingParentGroupDeletesChildren()
        {
            //create a hierarchy with 3 levels and a group at each
            ProductHierarchy hierarchy =
               ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";

            //level 1
            ProductLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup group1 = ProductGroup.NewProductGroup(level1.Id);
            group1.Name = "group1";
            group1.Code = "code1";
            hierarchy.RootGroup.ChildList.Add(group1);

            //level 2
            ProductLevel level2 = hierarchy.InsertNewChildLevel(level1);
            ProductGroup group2 = ProductGroup.NewProductGroup(level2.Id);
            group2.Name = "group2";
            group2.Code = "code2";
            group1.ChildList.Add(group2);

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;
            group1 = hierarchy.RootGroup.ChildList[0];
            group2 = group1.ChildList[0];

            //delete group1
            Assert.AreEqual(1, hierarchy.RootGroup.ChildList.Count());
            hierarchy.RootGroup.ChildList.Remove(group1);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count());

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;

            //check the returned hierarchy only has a root group
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count());

            //try to fetch group 1 and 2 - should both fail
            ProductGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(group1.Id);
                    Assert.IsNotNull(dto.DateDeleted, "group 1 should no longer exist");

                    dto = dal.FetchById(group2.Id);
                    Assert.IsNotNull(dto.DateDeleted, "group 2 should no longer exist");
                }
            }

        }

        [Test]
        public void DeleteMiddleThenLastLevel()
        {
            //create a hierarchy with 3 levels and a group at each
            ProductHierarchy hierarchy =
               ProductHierarchy.NewProductHierarchy(1);
            hierarchy.Name = "h1";

            //level 1
            ProductLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            ProductGroup group1 = ProductGroup.NewProductGroup(level1.Id);
            group1.Name = "group1";
            group1.Code = "code1";
            hierarchy.RootGroup.ChildList.Add(group1);

            //level 2
            ProductLevel level2 = hierarchy.InsertNewChildLevel(level1);
            ProductGroup group2 = ProductGroup.NewProductGroup(level2.Id);
            group2.Name = "group2";
            group2.Code = "code2";
            group1.ChildList.Add(group2);

            //save
            hierarchy = hierarchy.Save();
            level1 = hierarchy.RootLevel.ChildLevel;
            level2 = level1.ChildLevel;
            group1 = hierarchy.RootGroup.ChildList[0];
            group2 = group1.ChildList[0];

            //remove level 1
            hierarchy.RemoveLevel(level1);
            level2 = hierarchy.RootLevel.ChildLevel;

            //check that group 2 is now the child of the rootGroup
            Assert.AreEqual(group2.Id, hierarchy.RootGroup.ChildList[0].Id);

            //remove level 2
            hierarchy.RemoveLevel(level2);
            Assert.IsNull(hierarchy.RootLevel.ChildLevel);

            //save
            hierarchy = hierarchy.Save();

            //check the hierarchy returned now only has 1 levels and 1 groups
            Assert.IsNull(hierarchy.RootLevel.ChildLevel);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count);

            //try to fetch back group 1 - should fail
            ProductGroupDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(group1.Id);
                    Assert.IsNotNull(dto.DateDeleted, "group 1 should no longer exist");
                }
            }

            //try to fetch back group 2 - should fail
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dto = dal.FetchById(group2.Id);
                    Assert.IsNotNull(dto.DateDeleted, "group 2 should no longer exist");
                }
            }

            //try to fetch back level 1 - should fail
            ProductLevelDto levelDto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    levelDto = dal.FetchById(level1.Id);
                    Assert.IsNotNull(dto.DateDeleted, "level 1 should no longer exist");
                }
            }

            //try to fetch back level 2 - should fail
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    levelDto = dal.FetchById(level2.Id);
                    Assert.IsNotNull(dto.DateDeleted, "level 2 should no longer exist");
                }
            }
        }

        #endregion
    }
}
