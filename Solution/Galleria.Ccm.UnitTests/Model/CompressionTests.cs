﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CompressionTests : TestBase
    {
        #region Test Helper Methods

        private void AssertDtoAndModelAreEqual(CompressionDto dto, Compression model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.Width, model.Width);
            Assert.AreEqual(dto.Height, model.Height);
            Assert.AreEqual(dto.ColourDepth, (Int32)model.ColourDepth);
            Assert.AreEqual(dto.MaintainAspectRatio, model.MaintainAspectRatio);
            Assert.AreEqual(dto.Enabled, model.Enabled);
        }

        /// <summary>
        /// Insert some test dtos
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <returns></returns>
        private List<CompressionDto> InsertTestDtos()
        {
            TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            List<CompressionDto> dtoList = new List<CompressionDto>();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
                {
                    for (int i = 1; i <= 5; i++)
                    {
                        dtoList.Add(
                        new CompressionDto()
                        {
                            //Id = i,                            
                            Name = "name" + i.ToString(),
                            Width = i,
                            Height = i,
                            MaintainAspectRatio = true,
                            Enabled = true,
                            ColourDepth = i
                        });
                    }

                    // insert into the dal
                    foreach (CompressionDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        [Test]
        public void Serializable()
        {
            CompressionDto inserted = InsertTestDtos().FirstOrDefault();
            Compression model = Compression.GetCompressionById(inserted.Id);

            Serialize(model);
        }

        [Test]
        public void FetchCompression()
        {
            List<CompressionDto> dtoList = InsertTestDtos();

            foreach (CompressionDto Dto in dtoList)
            {
                Compression model = Compression.GetCompressionById(Dto.Id);
                AssertDtoAndModelAreEqual(Dto, model);
            }
        }

        [Test]
        public void PropertySetters()
        {
            Compression model = Compression.NewCompression();

            TestBase.TestPropertySetters<Compression>(model);
        }
    }
}
