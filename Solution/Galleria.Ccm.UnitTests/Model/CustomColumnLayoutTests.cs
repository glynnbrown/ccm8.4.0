﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnLayoutTests : TestBase
    {
        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumnLayout testModel = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.Undefined, CustomColumnList.NewCustomColumnList());

            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<CustomColumnLayout>(CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.Undefined, CustomColumnList.NewCustomColumnList()));
        }

        [Test]
        public void NewCustomColumnLayout()
        {
            TestDelegate code = () => CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.Undefined, CustomColumnList.NewCustomColumnList());

            Assert.DoesNotThrow(code);
        }

        #endregion
    }
}
