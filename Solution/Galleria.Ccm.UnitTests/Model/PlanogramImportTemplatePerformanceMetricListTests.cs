﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-28878 : D.Pleasance
//  Created.
#endregion
#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramImportTemplatePerformanceMetricListTests : TestBase<PlanogramImportTemplatePerformanceMetric, PlanogramImportTemplatePerformanceMetricDto>
    {
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerializeList();
        }

        [Test]
        public void NewFactoryMethod()
        {
            TestNewListFactoryMethod<PlanogramImportTemplatePerformanceMetricList>();
        }
    }
}