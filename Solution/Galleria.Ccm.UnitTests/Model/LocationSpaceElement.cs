﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationSpaceElement : TestBase
    {
        #region Test Helper Methods
        private void AssertDtoAndModelAreEqual(LocationSpaceElementDto dto, Galleria.Ccm.Model.LocationSpaceElement model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.LocationSpaceBayId, model.Parent.Id);
            Assert.AreEqual(dto.Order, model.Order);
            Assert.AreEqual(dto.X, model.X);
            Assert.AreEqual(dto.Y, model.Y);
            Assert.AreEqual(dto.Z, model.Z);
            Assert.AreEqual(dto.MerchandisableHeight, model.MerchandisableHeight);
            Assert.AreEqual(dto.Height, model.Height);
            Assert.AreEqual(dto.Width, model.Width);
            Assert.AreEqual(dto.Depth, model.Depth);
            Assert.AreEqual(dto.FaceThickness, model.FaceThickness);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.LocationSpaceElement model =
                Galleria.Ccm.Model.LocationSpaceElement.NewLocationSpaceElement();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model for rand values
            Galleria.Ccm.Model.LocationSpaceElement valueModel =
                Galleria.Ccm.Model.LocationSpaceElement.NewLocationSpaceElement();

            //Test Id
            Int32 testId = 1;
            valueModel.Id = testId;
            Assert.AreEqual(testId, valueModel.Id);

            //Test Order
            byte testOrder = 1;
            valueModel.Order = testOrder;
            Assert.AreEqual(testOrder, valueModel.Order);

            //Test X
            Single testX = 1;
            valueModel.X = testX;
            Assert.AreEqual(testX, valueModel.X);

            //Test Y
            Single testY = 1;
            valueModel.Y = testY;
            Assert.AreEqual(testY, valueModel.Y);

            //Test Z
            Single testZ = 1;
            valueModel.Z = testZ;
            Assert.AreEqual(testZ, valueModel.Z);

            //Test Merchandisable Height
            Single testMerchandisableHeight = 1;
            valueModel.MerchandisableHeight = testMerchandisableHeight;
            Assert.AreEqual(testMerchandisableHeight, valueModel.MerchandisableHeight);

            //Test Height
            Single testHeight = 1;
            valueModel.Height = testHeight;
            Assert.AreEqual(testHeight, valueModel.Height);

            //Test Width
            Single testWidth = 1;
            valueModel.Width = testWidth;
            Assert.AreEqual(testWidth, valueModel.Width);

            //Test Depth
            Single testDepth = 1;
            valueModel.Depth = testDepth;
            Assert.AreEqual(testDepth, valueModel.Depth);

            //Test Face Thickness
            Single testFaceThickness = 1;
            valueModel.FaceThickness = testFaceThickness;
            Assert.AreEqual(testFaceThickness, valueModel.FaceThickness);

        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationSpaceElement()
        {
            TestCreateNew<Ccm.Model.LocationSpaceElement>(
                /*isChild*/true,
                new String[] { },
                null,
                new String[] { "Parent" });
        }

        #endregion

        #region Data Access

        #region Insert
        [Test]
        public void DataAccess_Insert()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Select default element in default bay
            Ccm.Model.LocationSpaceElement modelElement = modelProductGroup.Bays.First().Elements.First();

            //Set indentifiable property value
            Byte randomOrder = Convert.ToByte(random.Next(1, 255));
            modelElement.Order = randomOrder;

            //Call save on the parent object
            model = model.Save();

            //Get bay we want to check has been inserted
            Ccm.Model.LocationSpaceElement testModel = model.LocationSpaceProductGroups.First().Bays.First().Elements.First();

            //Fetch saved Element
            LocationSpaceElementDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);

        }

        #endregion

        #region Update

        [Test]
        public void DataAccess_Update()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Select default bay
            Ccm.Model.LocationSpaceElement modelElement = modelProductGroup.Bays.First().Elements.First();

            //Set indentifiable property value
            Byte randomOrder = Convert.ToByte(random.Next(1, 255));
            modelElement.Order = randomOrder;

            //Call save on the parent object
            model = model.Save();

            //Get bay we want to check has been inserted
            Ccm.Model.LocationSpaceElement testModel = model.LocationSpaceProductGroups.First().Bays.First().Elements.First();

            //Set indentifiable property value
            Byte updateOrderValue = Convert.ToByte(random.Next(1, 255));
            testModel.Order = updateOrderValue;

            //Call save on the parent object again
            model = model.Save();

            //Reselect the bay from the parent object
            testModel = model.LocationSpaceProductGroups.First().Bays.First().Elements.First();

            //Fetch saved Element
            LocationSpaceElementDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);
        }

        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Select default bay
            Ccm.Model.LocationSpaceElement modelElement = modelProductGroup.Bays.First().Elements.First();

            //Call save on the parent object
            model = model.Save();

            //Get bay we want to check has been inserted
            Ccm.Model.LocationSpaceElement testModel = model.LocationSpaceProductGroups.First().Bays.First().Elements.FirstOrDefault();
            Assert.IsNotNull(testModel, "Element should have been inserted, and returned");

            //Fetch saved Bay to ensure it exists in the database
            LocationSpaceElementDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);

            //Delete bay from parent object
            model.LocationSpaceProductGroups.First().Bays.First().Elements.Remove(testModel);

            //Call save on the parent object
            model = model.Save();

            //try to retrieve again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(testModel.Id);
                        });
                }
            }
        }
        #endregion

        #endregion
    }
}
