﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628 : A.Kuszyk
//		Created, adapted from SA.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<LocationDto> locationList =
            TestDataHelper.InsertLocationDtos(base.DalFactory, 20, entityId);

            LocationList model = LocationList.FetchByEntityId(1);

            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            List<LocationDto> locationList1 =
            TestDataHelper.InsertLocationDtos(base.DalFactory, 20, entityList[0].Id);

            List<LocationDto> locationList2 =
            TestDataHelper.InsertLocationDtos(base.DalFactory, 20, entityList[1].Id);

            LocationList model1 = LocationList.FetchByEntityId(entityList[0].Id);
            foreach (LocationDto locDto in locationList1)
            {
                Location info = model1.First(i => i.Id == locDto.Id);
            }

            LocationList model2 = LocationList.FetchByEntityId(entityList[1].Id);
            foreach (LocationDto locDto in locationList2)
            {
                Location info = model2.First(i => i.Id == locDto.Id);
            }
        }

        #endregion

    }
}
