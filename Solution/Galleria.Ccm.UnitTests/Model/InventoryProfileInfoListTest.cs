﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26124 : I.George
//		Created
#endregion
#endregion

using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class InventoryProfileInfoListTest : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            InventoryProfileInfoList model = InventoryProfileInfoList.FetchByEntityId(1);

            Serialize(model);
            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 20);

            foreach (EntityDto entity in entityDtos)
            {
                List<InventoryProfileDto> IpDtoList = TestDataHelper.InsertInventoryProfileDtos(base.DalFactory, entity.Id, 20);
                //Fetch the info list
                InventoryProfileInfoList modelList = InventoryProfileInfoList.FetchByEntityId(entity.Id);
                //check the infos
                foreach (InventoryProfileDto dto in IpDtoList)
                {
                    InventoryProfileInfo model = modelList.FirstOrDefault(p => p.Id == dto.Id);
                    InventoryProfileInfoTest.AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }
        #endregion
    }
}
