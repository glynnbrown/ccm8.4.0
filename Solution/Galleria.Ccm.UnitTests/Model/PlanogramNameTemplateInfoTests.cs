﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM8.0.2
// V8-29010 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramNameTemplateInfoTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<PlanogramNameTemplateDto> planogramNameTemplateList =
            TestDataHelper.InsertPlanogramNameTemplateDtos(base.DalFactory, 20, entityId);

            PlanogramNameTemplateInfoList model = PlanogramNameTemplateInfoList.FetchByEntityId(entityId);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            List<PlanogramNameTemplateDto> planogramNameTemplateList1 =
            TestDataHelper.InsertPlanogramNameTemplateDtos(base.DalFactory, 20, entityList[0].Id);

            List<PlanogramNameTemplateDto> planogramNameTemplateList2 =
            TestDataHelper.InsertPlanogramNameTemplateDtos(base.DalFactory, 20, entityList[1].Id);

            PlanogramNameTemplateInfoList model1 = PlanogramNameTemplateInfoList.FetchByEntityId(entityList[0].Id);
            foreach (PlanogramNameTemplateDto impDto in planogramNameTemplateList1)
            {
                PlanogramNameTemplateInfo info = model1.First(i => i.Id.Equals(impDto.Id));
            }

            PlanogramNameTemplateInfoList model2 = PlanogramNameTemplateInfoList.FetchByEntityId(entityList[1].Id);
            foreach (PlanogramNameTemplateDto impDto in planogramNameTemplateList2)
            {
                PlanogramNameTemplateInfo info = model2.First(i => i.Id.Equals(impDto.Id));
            }
        }

        #endregion
    }
}