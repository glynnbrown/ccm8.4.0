﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Reflection;
using Csla.Core;
using System.Collections;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class BlockingGroupListTests : TestBase<BlockingGroup, BlockingGroupDto>
    {
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerializeList();
        }

        [Test]
        public void NewFactoryMethod()
        {
            TestNewListFactoryMethod<BlockingGroupList>();
        }


    }
}
