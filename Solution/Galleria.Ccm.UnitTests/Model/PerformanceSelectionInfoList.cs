﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26159 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PerformanceSelectionInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            PerformanceSelectionInfoList model = PerformanceSelectionInfoList.FetchByEntityId(1);
            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            //Insert some test performance selections
            PerformanceSelection selection1 = PerformanceSelection.NewPerformanceSelection(1);
            selection1.Name = "Selection 1";
            selection1.SelectionType = PerformanceSelectionType.Fixed;
            selection1.TimeType = PerformanceSelectionTimeType.Week;
            selection1.GFSPerformanceSourceId = 1;
            selection1.Save();

            PerformanceSelection selection2 = PerformanceSelection.NewPerformanceSelection(1);
            selection2.Name = "Selection 2";
            selection2.SelectionType = PerformanceSelectionType.Dynamic;
            selection2.TimeType = PerformanceSelectionTimeType.Month;
            selection2.GFSPerformanceSourceId = 2;
            selection2.Save();


            PerformanceSelectionInfoList model = PerformanceSelectionInfoList.FetchByEntityId(1);
            Assert.AreEqual(2, model.Count);

        }

        #endregion
    }
}
