﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
#endregion
#region Version History: CCM830
// V8-13996 : J.Pickup
//  TestNormaliseMetricOrderToPlanogramPerformance && TestGetRatioForMetricId
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class MetricProfileTests : TestBase
    {
        [Test]
        public void Serializable()
        {
            var product = MetricProfile.NewMetricProfile(1);
            Serialize(product);
        }

        [Test]
        public void PropertySetters()
        {
            var product = MetricProfile.NewMetricProfile(1);

            TestPropertySetters<MetricProfile>(product);
        }

        #region Factory Methods
        [Test]
        public void NewMetricProfile()
        {
            var newProduct = MetricProfile.NewMetricProfile(1);

            Assert.AreEqual(newProduct.EntityId, 1);

            Assert.IsTrue(newProduct.IsNew);
            Assert.IsFalse(newProduct.IsChild, "A newly created product should be a root object");
        }

        [Test]
        public void FetchById()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<MetricDto> metricDtoList = TestDataHelper.InsertMetricDtos(base.DalFactory, 50, entityList[0].Id);
            List<MetricProfileDto> metricProfileDtoList = TestDataHelper.InsertMetricProfileDtos(base.DalFactory, 50, entityList[0].Id, metricDtoList);

            //fetch each by id
            foreach (MetricProfileDto metricProfDto in metricProfileDtoList)
            {
                var dalItem = MetricProfile.FetchById(metricProfDto.Id);
                AssertHelper.AssertModelObjectsAreEqual(metricProfDto, dalItem);

                Assert.IsFalse(dalItem.IsChild, "Should have been fetched as a root object");
            }
        }

        [Test]
        public void FetchByEntityIdName()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<MetricDto> metricDtoList = TestDataHelper.InsertMetricDtos(base.DalFactory, 50, entityList[0].Id);
            List<MetricProfileDto> metricProfileDtoList = TestDataHelper.InsertMetricProfileDtos(base.DalFactory, 50, entityList[0].Id, metricDtoList);

            //fetch each by id
            foreach (MetricProfileDto metricProfDto in metricProfileDtoList)
            {
                var dalItem = MetricProfile.FetchByEntityIdName(metricProfDto.EntityId, metricProfDto.Name);
                AssertHelper.AssertModelObjectsAreEqual(metricProfDto, dalItem);

                Assert.IsFalse(dalItem.IsChild, "Should have been fetched as a root object");
            }
        }
        #endregion

        #region Methods

        [Test]
        public void TestNormaliseMetricOrderToPlanogramPerformance()
        {
            //Tests that the order of metric profile can be normalised against the order within planogram performance, including ratio.

            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //Create basic metricprofile
            List<MetricDto> metrics = TestDataHelper.InsertMetricDtos(base.DalFactory, entityId, 5);
            MetricList globalMetrics = MetricList.FetchByEntityId(entityId);

            MetricProfile profile = MetricProfile.NewMetricProfile(entityId);
            profile.SetMetric(1, metrics[0].Id, 10);
            profile.SetMetric(2, metrics[1].Id, 20);
            profile.SetMetric(3, metrics[2].Id, 30);
            profile.Name = "Profile1";
            profile.Save();
            
            //Create planogram and associated performance.
            var planogram = Planogram.NewPlanogram();

            // Create product
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Gtin = "Gtin";
            planogramProduct.ShelfLife = 20;
            planogramProduct.DeliveryFrequencyDays = 3;
            planogramProduct.CasePackUnits = 5;

            // Add product to planogram
            planogram.Products.Add(planogramProduct);

            // create positions
            planogram.Positions.Add();
            planogram.Positions[0].PlanogramProductId = planogramProduct.Id;
            planogram.Positions[0].TotalUnits = 10;

            planogram.Positions.Add();
            planogram.Positions[1].PlanogramProductId = planogramProduct.Id;
            planogram.Positions[1].TotalUnits = 20;

            // Set performance data
            planogram.Performance.PerformanceData[0].P1 = 88;

            // Create metrics
            planogram.Performance.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(50));
            planogram.Performance.Metrics[0].Name = metrics[2].Name;
            planogram.Performance.Metrics[0].SpecialType = MetricSpecialType.RegularSalesUnits;

            planogram.Performance.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(60));
            planogram.Performance.Metrics[1].Name = metrics[1].Name;
            planogram.Performance.Metrics[1].SpecialType = MetricSpecialType.RegularSalesUnits;

            planogram.Performance.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(70));
            planogram.Performance.Metrics[2].Name = metrics[0].Name;
            planogram.Performance.Metrics[2].SpecialType = MetricSpecialType.RegularSalesUnits;


            //At this point we can check the inital order > call normalise > then check the orders normalised
            Assert.IsTrue(profile.Metric1Ratio == 10);
            Assert.IsTrue(profile.Metric2Ratio == 20);
            Assert.IsTrue(profile.Metric3Ratio == 30);

            IPlanogramMetricProfile iPlanMetricProfileBeforeChange = profile.ToPlanogramMetricProfile(planogram.Performance, globalMetrics);
            Boolean normalisedSuccessfullyBeforeChange = (iPlanMetricProfileBeforeChange != null);
            Assert.IsFalse(normalisedSuccessfullyBeforeChange, "The metrics don't match and therefore method call should return null");

            //Remove metrics so that everything should be fine.
            IEnumerable<PlanogramPerformanceMetric> metricsToRemove = planogram.Performance.Metrics.Where(m => m.Name == String.Empty).ToList();
            planogram.Performance.Metrics.RemoveList(metricsToRemove);


            //Assert we have ratios in correct order
            IPlanogramMetricProfile iPlanMetricProfileAfterChange = profile.ToPlanogramMetricProfile(planogram.Performance, globalMetrics);
            Boolean normalisedSuccessfully = (iPlanMetricProfileAfterChange != null);
            Assert.IsTrue(normalisedSuccessfully, "The metrics now match and therefore method call should return true");

            Assert.IsTrue(iPlanMetricProfileAfterChange.Metric1Ratio == 30);
            Assert.IsTrue(iPlanMetricProfileAfterChange.Metric2Ratio == 20);
            Assert.IsTrue(iPlanMetricProfileAfterChange.Metric3Ratio == 10);
        }



        [Test]
        public void TestGetRatioForMetricId()
        {
            //Randomly generate ratio values and assign them to metrics, test correct ratio is returned for given id.

            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //Create basic metricprofile
            List<MetricDto> metrics = TestDataHelper.InsertMetricDtos(base.DalFactory, entityId, 5);

            Random rnd = new Random();
            Int32 ratio1 = rnd.Next();
            Int32 ratio2= rnd.Next();
            Int32 ratio3 = rnd.Next();
            Int32 ratio4 = rnd.Next();
        
            MetricProfile profile = MetricProfile.NewMetricProfile(entityId);
            profile.SetMetric(1, metrics[0].Id, ratio1);
            profile.SetMetric(2, metrics[1].Id, ratio2);
            profile.SetMetric(3, metrics[2].Id, ratio3);
            profile.SetMetric(4, metrics[3].Id, ratio4);
            profile.Name = "Profile1";

            //Check method returns expected ratios...
            Assert.AreEqual(ratio1, profile.GetRatioForMetricId(metrics[0].Id), "Ratio returned was not the expected value for metric 1");
            Assert.AreEqual(ratio2, profile.GetRatioForMetricId(metrics[1].Id), "Ratio returned was not the expected value for metric 2");
            Assert.AreEqual(ratio3, profile.GetRatioForMetricId(metrics[2].Id), "Ratio returned was not the expected value for metric 3");
            Assert.AreEqual(ratio4, profile.GetRatioForMetricId(metrics[3].Id), "Ratio returned was not the expected value for metric 4");
        }

        #endregion
    }
}
