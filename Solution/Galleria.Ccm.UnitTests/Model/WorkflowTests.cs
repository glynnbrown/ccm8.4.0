﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31584 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;
using System.Diagnostics;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class WorkflowTests : TestBase
    {
        //TraceListener[] _originalListeners;
        private Galleria.Ccm.Model.Workflow _workflow;
        private EntityDto _entityDto;

        [SetUp]
        public void SetUp()
        {
            //_originalListeners = new TraceListener[Trace.Listeners.Count];
            //Trace.Listeners.CopyTo(_originalListeners, 0);
            //Trace.Listeners.Clear();
            //Trace.Listeners.Add(new ConsoleTraceListener());
            _entityDto = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First();
            _workflow = Galleria.Ccm.Model.Workflow.NewWorkflow(Entity.FetchById(_entityDto.Id));

        }

        //[TearDown]
        //public void TearDown()
        //{
        //    Trace.Listeners.Clear();
        //    Trace.Listeners.AddRange(_originalListeners);
        //}

        [Test]
        public void TaskParameterDataCache_GetValidationTemplateInfos_FetchesData()
        {
            TestDataHelper.InsertValidationTemplateDtos(base.DalFactory, 10, _workflow.EntityId);
            
            var actual = _workflow.TaskParameterDataCache.GetValidationTemplateInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetValidationTemplateInfos_GetsCachedData()
        {
            TestDataHelper.InsertValidationTemplateDtos(base.DalFactory, 10, _workflow.EntityId);
            var expected = _workflow.TaskParameterDataCache.GetValidationTemplateInfos();

            var actual = _workflow.TaskParameterDataCache.GetValidationTemplateInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetRenumberingStrategyInfos_FetchesData()
        {
            TestDataHelper.InsertRenumberingStrategyDtos(base.DalFactory, 10, _workflow.EntityId);

            var actual = _workflow.TaskParameterDataCache.GetRenumberingStrategyInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetRenumberingStrategyInfos_GetsCachedData()
        {
            TestDataHelper.InsertRenumberingStrategyDtos(base.DalFactory, 10, _workflow.EntityId);
            var expected = _workflow.TaskParameterDataCache.GetRenumberingStrategyInfos();

            var actual = _workflow.TaskParameterDataCache.GetRenumberingStrategyInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetProductUniverseInfos_FetchesData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var productGroupIds = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).Select(dto => dto.Id).ToList();
            var expected = TestDataHelper.InsertProductUniverseDtos(base.DalFactory,new List<Int32>() { _workflow.EntityId }, productGroupIds).Count;

            var actual = _workflow.TaskParameterDataCache.GetProductUniverseInfos();

            Assert.That(actual, Has.Count.EqualTo(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetProductUniverseInfos_GetsCachedData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var productGroupIds = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).Select(dto => dto.Id).ToList();
            TestDataHelper.InsertProductUniverseDtos(base.DalFactory, new List<Int32>() { _workflow.EntityId }, productGroupIds);
            var expected = _workflow.TaskParameterDataCache.GetProductUniverseInfos();

            var actual = _workflow.TaskParameterDataCache.GetProductUniverseInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetPlanogramImportTemplate_FetchesData()
        {
            var expected = TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 10, _workflow.EntityId).First();

            var actual = _workflow.TaskParameterDataCache.GetPlanogramImportTemplate(expected.Id);

            Assert.That(actual.Id, Is.EqualTo(expected.Id));
        }

        [Test]
        public void TaskParameterDataCache_GetPlanogramImportTemplate_GetsCachedData()
        {
            var dto = TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 10, _workflow.EntityId).First();
            var expected= _workflow.TaskParameterDataCache.GetPlanogramImportTemplate(dto.Id);

            var actual = _workflow.TaskParameterDataCache.GetPlanogramImportTemplate(expected.Id);

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetPlanogramImportTemplateInfos_FetchesData()
        {
            TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 10, _workflow.EntityId);

            var actual = _workflow.TaskParameterDataCache.GetPlanogramImportTemplateInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetPlanogramImportTemplateInfos_GetsCachedData()
        {
            TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 10, _workflow.EntityId);
            var expected = _workflow.TaskParameterDataCache.GetPlanogramImportTemplateInfos();

            var actual = _workflow.TaskParameterDataCache.GetPlanogramImportTemplateInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetMetricProfileInfos_FetchesData()
        {
            var metricDtos = TestDataHelper.InsertMetricDtos(base.DalFactory, _workflow.EntityId, 10);
            TestDataHelper.InsertMetricProfileDtos(base.DalFactory, 10, _workflow.EntityId, metricDtos);

            var actual = _workflow.TaskParameterDataCache.GetMetricProfileInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetMetricProfileInfos_GetsCachedData()
        {
            var metricDtos = TestDataHelper.InsertMetricDtos(base.DalFactory, _workflow.EntityId, 10);
            TestDataHelper.InsertMetricProfileDtos(base.DalFactory, 10, _workflow.EntityId, metricDtos);
            var expected = _workflow.TaskParameterDataCache.GetMetricProfileInfos();

            var actual = _workflow.TaskParameterDataCache.GetMetricProfileInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetLocationHierarchy_FetchesData()
        {
            var dto = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First();

            var actual = _workflow.TaskParameterDataCache.GetLocationHierarchy();

            Assert.That(actual.Id, Is.EqualTo(dto.Id));
        }

        [Test]
        public void TaskParameterDataCache_GetLocationHierarchy_GetsCachedData()
        {
            TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, _workflow.EntityId);
            var expected = _workflow.TaskParameterDataCache.GetLocationHierarchy();

            var actual = _workflow.TaskParameterDataCache.GetLocationHierarchy();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetLocationInfos_FetchesData()
        {
            var expected = TestDataHelper.InsertLocationDtos(base.DalFactory, 10, _workflow.EntityId).Count;

            var actual = _workflow.TaskParameterDataCache.GetLocationInfos();

            Assert.That(actual, Has.Count.EqualTo(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetLocationInfos_GetsCachedData()
        {
            TestDataHelper.InsertLocationDtos(base.DalFactory, 10, _workflow.EntityId);
            var expected = _workflow.TaskParameterDataCache.GetLocationInfos();

            var actual = _workflow.TaskParameterDataCache.GetLocationInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetPlanogramInfo_FetchesData()
        {
            using (var dalContext = base.DalFactory.CreateContext())
            {
                var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, "User", _workflow.EntityId, 10);
                var planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, "User", 1, packageDtos); 

                var actual = _workflow.TaskParameterDataCache.GetPlanogramInfo(
                    Convert.ToInt32(planogramDtos.First().Id), planogramDtos.Select(dto => Convert.ToInt32(dto.Id)));

                Assert.That(actual.Id, Is.EqualTo(planogramDtos.First().Id));
            }
        }

        [Test]
        public void TaskParameterDataCache_GetPlanogramInfo_GetsCachedData()
        {
            using (var dalContext = base.DalFactory.CreateContext())
            {
                var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, "User", _workflow.EntityId, 10);
                var planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, "User", 1, packageDtos);
                var expected = _workflow.TaskParameterDataCache.GetPlanogramInfo(
                    Convert.ToInt32(planogramDtos.First().Id), planogramDtos.Select(dto => Convert.ToInt32(dto.Id)));

                var actual = _workflow.TaskParameterDataCache.GetPlanogramInfo(
                    Convert.ToInt32(planogramDtos.First().Id), planogramDtos.Select(dto => Convert.ToInt32(dto.Id)));

                Assert.That(actual, Is.SameAs(expected));
            }
        }

        [Test]
        public void TaskParameterDataCache_GetLocationPlanAssignments_FetchesData()
        {
            using (var dalContext = DalFactory.CreateContext())
            {
                var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, "User", _workflow.EntityId, 10);
                var planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, "User", 10, packageDtos);
                var locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, 10, _workflow.EntityId);
                var userDto = TestDataHelper.InsertUserDtos(dalContext, 1).First();
                var workflowDtos = TestDataHelper.InsertWorkflowDtos(dalContext, new List<EntityDto>() { _entityDto }, 1);
                var workpackageDtos = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtos, userDto);
                var workpackagePlanogramDtos = TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext, workpackageDtos, planogramDtos);
                var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
                var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
                var productGroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).ToList();
                var dto = TestDataHelper.InsertLocationPlanAssignmentDtos(
                    base.DalFactory, locationDtos, workpackagePlanogramDtos, productGroupDtos, 10, _workflow.EntityId).First();

                var actual = _workflow.TaskParameterDataCache.GetLocationPlanAssignments(dto.PlanogramId).First();

                Assert.That(actual.Id, Is.EqualTo(dto.Id)); 
            }
        }

        [Test]
        public void TaskParameterDataCache_GetLocationPlanAssignments_GetsCachedData()
        {
            using (var dalContext = DalFactory.CreateContext())
            {
                var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, "User", _workflow.EntityId, 10);
                var planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, "User", 10, packageDtos);
                var locationDtos = TestDataHelper.InsertLocationDtos(DalFactory, 10, _workflow.EntityId);
                var userDto = TestDataHelper.InsertUserDtos(dalContext, 1).First();
                var workflowDtos = TestDataHelper.InsertWorkflowDtos(dalContext, new List<EntityDto>() { _entityDto }, 1);
                var workpackageDtos = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtos, userDto);
                var workpackagePlanogramDtos = TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext, workpackageDtos, planogramDtos);
                var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
                var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
                var productGroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).ToList();
                var dto = TestDataHelper.InsertLocationPlanAssignmentDtos(
                    base.DalFactory, locationDtos, workpackagePlanogramDtos, productGroupDtos, 10, _workflow.EntityId).First();
                var expected = _workflow.TaskParameterDataCache.GetLocationPlanAssignments(dto.PlanogramId).First();

                var actual = _workflow.TaskParameterDataCache.GetLocationPlanAssignments(dto.PlanogramId).First();

                Assert.That(actual, Is.SameAs(expected));
            }
        }

        [Test]
        public void TaskParameterDataCache_GetProductGroupInfo_FetchesData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var dto = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).First();

            var actual = _workflow.TaskParameterDataCache.GetProductGroupInfo(dto.Id);

            Assert.That(actual.Id, Is.EqualTo(dto.Id));
        }

        [Test]
        public void TaskParameterDataCache_GetProductGroupInfo_GetsCachedData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var dto = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).First();
            var expected = _workflow.TaskParameterDataCache.GetProductGroupInfo(dto.Id);

            var actual = _workflow.TaskParameterDataCache.GetProductGroupInfo(dto.Id);

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetProjectInfoByProductGroupCode()
        {
            Assert.Inconclusive("Project fetches cannot be tested, because the fetch code is tightly coupled to a concrete implementation of the GFS webservices.");
        }

        [Test]
        public void TaskParameterDataCache_GetProjectInfos()
        {
            Assert.Inconclusive("Project fetches cannot be tested, because the fetch code is tightly coupled to a concrete implementation of the GFS webservices.");
        }

        [Test]
        public void TaskParameterDataCache_GetProjectInfosWithNoProductGroupAssigned()
        {
            Assert.Inconclusive("Project fetches cannot be tested, because the fetch code is tightly coupled to a concrete implementation of the GFS webservices.");
        }

        [Test]
        public void TaskParameterDataCache_GetAssortmentInfos_FetchesData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var productGroupDto = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).First();
            TestDataHelper.InsertAssortmentDtos(DalFactory, 10, _workflow.EntityId, productGroupDto.Id);

            var actual = _workflow.TaskParameterDataCache.GetAssortmentInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetAssortmentInfos_GetsCachedData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var productGroupDto = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).First();
            TestDataHelper.InsertAssortmentDtos(DalFactory, 10, _workflow.EntityId, productGroupDto.Id);
            var expected = _workflow.TaskParameterDataCache.GetAssortmentInfos();

            var actual = _workflow.TaskParameterDataCache.GetAssortmentInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetBlockingInfos_FetchesData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var productGroupDto = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).First();
            var blocking = Blocking.NewBlocking(_workflow.EntityId);
            blocking.ProductGroupId = productGroupDto.Id;
            blocking.Name = "Blocking";
            blocking = blocking.Save();

            var actual = _workflow.TaskParameterDataCache.GetBlockingInfos();

            Assert.That(actual, Has.Count.EqualTo(1));
        }

        [Test]
        public void TaskParameterDataCache_GetBlockingInfos_GetsCachedData()
        {
            var hierarchyId = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, _workflow.EntityId).First().Id;
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyId, 1);
            var productGroupDto = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 10).First();
            var blocking = Blocking.NewBlocking(_workflow.EntityId);
            blocking.ProductGroupId = productGroupDto.Id;
            blocking.Name = "Blocking";
            blocking = blocking.Save();
            var expected = _workflow.TaskParameterDataCache.GetBlockingInfos();

            var actual = _workflow.TaskParameterDataCache.GetBlockingInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetClusterSchemeInfos_FetchesData()
        {
            TestDataHelper.InsertClusterSchemeDtos(DalFactory, _workflow.EntityId, 10);

            var actual = _workflow.TaskParameterDataCache.GetClusterSchemeInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetClusterSchemeInfos_GetsCachedData()
        {
            TestDataHelper.InsertClusterSchemeDtos(DalFactory, _workflow.EntityId, 10);
            var expected = _workflow.TaskParameterDataCache.GetClusterSchemeInfos();

            var actual = _workflow.TaskParameterDataCache.GetClusterSchemeInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetConsumerDecisionTreeInfos_FetchesData()
        {
            TestDataHelper.InsertConsumerDecisionTreeDtos(DalFactory, _workflow.EntityId, 10);

            var actual = _workflow.TaskParameterDataCache.GetConsumerDecisionTreeInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetConsumerDecisionTreeInfos_GetsCachedData()
        {
            TestDataHelper.InsertConsumerDecisionTreeDtos(DalFactory, _workflow.EntityId, 10);
            var expected = _workflow.TaskParameterDataCache.GetConsumerDecisionTreeInfos();

            var actual = _workflow.TaskParameterDataCache.GetConsumerDecisionTreeInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetHighlightInfos_FetchesData()
        {
            var highlight = Highlight.NewHighlight(_workflow.EntityId);
            highlight.Name = "Highlight";
            highlight = highlight.Save();

            var actual = _workflow.TaskParameterDataCache.GetHighlightInfos();

            Assert.That(actual, Has.Count.EqualTo(1));
        }

        [Test]
        public void TaskParameterDataCache_GetHighlightInfos_GetsCachedData()
        {
            var highlight = Highlight.NewHighlight(_workflow.EntityId);
            highlight.Name = "Highlight";
            highlight = highlight.Save();
            var expected = _workflow.TaskParameterDataCache.GetHighlightInfos();

            var actual = _workflow.TaskParameterDataCache.GetHighlightInfos();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetEntity_FetchesData()
        {
            var actual = _workflow.TaskParameterDataCache.GetEntity();

            Assert.That(actual.Id, Is.EqualTo(_workflow.EntityId));
        }

        [Test]
        public void TaskParameterDataCache_GetEntity_GetsCachedData()
        {
            var expected = _workflow.TaskParameterDataCache.GetEntity();

            var actual = _workflow.TaskParameterDataCache.GetEntity();

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void TaskParameterDataCache_GetInventoryProfileInfos_FetchesData()
        {
            TestDataHelper.InsertInventoryProfileDtos(DalFactory, _workflow.EntityId, 10);

            var actual = _workflow.TaskParameterDataCache.GetInventoryProfileInfos();

            Assert.That(actual, Has.Count.EqualTo(10));
        }

        [Test]
        public void TaskParameterDataCache_GetInventoryProfileInfos_GetsCachedData()
        {
            TestDataHelper.InsertInventoryProfileDtos(DalFactory, _workflow.EntityId, 10);
            var expected = _workflow.TaskParameterDataCache.GetInventoryProfileInfos();

            var actual = _workflow.TaskParameterDataCache.GetInventoryProfileInfos();

            Assert.That(actual, Is.SameAs(expected));
        }
    }
}


