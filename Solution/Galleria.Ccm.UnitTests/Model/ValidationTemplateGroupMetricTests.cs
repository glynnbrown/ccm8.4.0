﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26474 : A.Silva ~ Created
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ValidationTemplateGroupMetricTests : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters(ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewValidationTemplateGroupMetric()
        {
            var newModel = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();

            Assert.IsTrue(newModel.IsNew);
            Assert.IsTrue(newModel.IsChild, "A newly created Validation Template Group Metric should be a child object.");
        }

        [Test]
        public void FetchValidationTemplateGroupMetric()
        {
            var entityDtos = TestDataHelper.InsertEntityDtos(DalFactory, 1);
            var templateDtos = TestDataHelper.InsertValidationTemplateDtos(DalFactory, 5, entityDtos[0].Id);
            var groupDtos = TestDataHelper.InsertValidationTemplateGroupDtos(DalFactory,
                templateDtos, 5);
            var metricDtos = TestDataHelper.InsertValidationTemplateGroupMetricDtos(DalFactory, groupDtos, 10);

            using (var dalContext = DalFactory.CreateContext())
            {
                foreach (var dto in metricDtos)
                {
                    var model = ValidationTemplateGroupMetric.Fetch(dalContext, dto);

                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }
        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            var template = CreateValidationTemplate("template1");
            var group = CreateValidationTemplateGroup("group1");
            group.Metrics.Add(CreateValidationTemplateGroupMetric("metric1"));
            template.Groups.Add(group);

            // Save.
            template = template.Save();
            var model = template.Groups[0].Metrics[0];

            var dto = FetchDtoByModel(model);

            AssertHelper.AssertModelObjectsAreEqual(dto, model);
        }

        [Test]
        public void DataAccess_Update()
        {
            var template = CreateValidationTemplate("template1");
            var group = CreateValidationTemplateGroup("group1");
            group.Metrics.Add(CreateValidationTemplateGroupMetric("metric1"));
            template.Groups.Add(group);

            // Save.
            template = template.Save();
            var model = template.Groups[0].Metrics[0];

            // Update and save.
            model.Field = "updatedMetric1";
            template = template.Save();
            model = template.Groups[0].Metrics[0];

            var dto = FetchDtoByModel(model);

            AssertDtoAndModelAreEqual(dto, model);
        }

        [Test]
        public void DataAccess_Delete()
        {
            var template = CreateValidationTemplate("template1");
            var group = CreateValidationTemplateGroup("group1");
            group.Metrics.Add(CreateValidationTemplateGroupMetric("metric1"));
            template.Groups.Add(group);

            // Save.
            template = template.Save();
            var model = template.Groups[0].Metrics[0];

            var dto = FetchDtoByModel(model);

            AssertDtoAndModelAreEqual(dto, model);

            // Delete the dto.
            template.Groups[0].Metrics.Remove(model);
            template.Save();

            // Retrieved the deleted item.
            dto = FetchDtoByModel(model);

            Assert.IsNull(dto);
        }

        #endregion

        #region IValidationTemplateGroupMetric Members

        [Test]
        public void IValidationTemplateGroupMetricParent_Invoked_ReturnsParent()
        {
            const string expectation = "When Parent is invoked the actual parent is returned.";
            var expected = ValidationTemplateGroup.NewValidationTemplateGroup();
            var testModel = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            expected.Metrics.Add(testModel);

            var actual = ((IValidationTemplateGroupMetric)testModel).Parent;

            Assert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region Test Helper Methods

        private static ValidationTemplate CreateValidationTemplate(String templateName)
        {
            var model = ValidationTemplate.NewValidationTemplate(1);
            model.Name = templateName;
            return model;
        }

        private static ValidationTemplateGroup CreateValidationTemplateGroup(String groupName)
        {
            var model = ValidationTemplateGroup.NewValidationTemplateGroup();
            model.Name = groupName;
            return model;
        }

        private static ValidationTemplateGroupMetric CreateValidationTemplateGroupMetric(String metricName)
        {
            var model = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            model.Field = metricName;
            return model;
        }

        private static ValidationTemplateGroupMetricDto FetchDtoByModel(ValidationTemplateGroupMetric model)
        {
            ValidationTemplateGroupMetricDto dto;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IValidationTemplateGroupMetricDal>())
            {
                dto = dal.FetchByValidationTemplateGroupId(model.Parent.Id).FirstOrDefault(metricDto => metricDto.Id == model.Id);
            }
            return dto;
        }

        #endregion
    }
}