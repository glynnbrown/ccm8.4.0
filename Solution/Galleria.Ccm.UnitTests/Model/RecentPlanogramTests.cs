﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24261 : L.Hodson
//  Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class RecentPlanogramTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(RecentPlanogram.NewRecentPlanogram());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<RecentPlanogram>(RecentPlanogram.NewRecentPlanogram());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void CreateNew()
        {
            TestCreateNew<RecentPlanogram>(true);
        }

        [Test]
        public void Fetch()
        {
            //insert a dto to fetch
            RecentPlanogramDto dto = new RecentPlanogramDto();
            PopulatePropertyValues1<RecentPlanogramDto>(dto);

            using (IDalContext dalContext = base.UserDalFactory.CreateContext())
            {
                using (IRecentPlanogramDal dal = dalContext.GetDal<IRecentPlanogramDal>())
                {
                    dal.Insert(dto);
                }
            }

            //fetch
            RecentPlanogramList list = RecentPlanogramList.FetchAll();
            Assert.AreEqual(1, list.Count);

            RecentPlanogram model = list[0];
            AssertDtoAndModelAreEqual<RecentPlanogramDto, RecentPlanogram>(dto, model);
        }

        #endregion

        #region Data Access

        [Test]
        public void Insert()
        {
            RecentPlanogramList list = RecentPlanogramList.FetchAll();

            RecentPlanogram model = RecentPlanogram.NewRecentPlanogram();
            PopulatePropertyValues1<RecentPlanogram>(model, new List<String> { "Id" });
            list.Add(model);

            list = list.Save();
            model = list[0];

            //refetch
            list = RecentPlanogramList.FetchAll();

            AssertModelsAreEqual<RecentPlanogram>(model, list[0]);

        }

        [Test]
        public void Update()
        {
            RecentPlanogramList list = RecentPlanogramList.FetchAll();

            RecentPlanogram model = RecentPlanogram.NewRecentPlanogram();
            PopulatePropertyValues1<RecentPlanogram>(model, new List<String> { "Id" });
            list.Add(model);

            list = list.Save();

            model = list[0];
            PopulatePropertyValues2<RecentPlanogram>(model, new List<String> { "Id" });

            list = list.Save();
            model = list[0];

            //refetch
            list = RecentPlanogramList.FetchAll();

            AssertModelsAreEqual<RecentPlanogram>(model, list[0]);
        }

        [Test]
        public void Delete()
        {
            RecentPlanogramList list = RecentPlanogramList.FetchAll();

            RecentPlanogram model = RecentPlanogram.NewRecentPlanogram();
            PopulatePropertyValues1<RecentPlanogram>(model, new List<String> { "Id" });
            list.Add(model);

            list = list.Save();

            //refetch
            list = RecentPlanogramList.FetchAll();
            model = list[0];

            //delete
            list.Remove(model);
            list = list.Save();

            //refetch
            list = RecentPlanogramList.FetchAll();
            Assert.AreEqual(0, list.Count);
        }

        #endregion
    }
}
