﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 : L.Hodson
//		Created
// V8-25436 : L.Luong
//      Changed to match new design
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class HighlightTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(Highlight.NewHighlight());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<Highlight>(Highlight.NewHighlight());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void CreateNew()
        {
            Int32 entityId = 1;

            Highlight newHighlight = Highlight.NewHighlight(entityId);

            Assert.AreEqual(newHighlight.EntityId, entityId);

            Assert.IsTrue(newHighlight.IsNew);
        }

        [Test]
        public void Fetch()
        {
            //insert a dto to fetch
            HighlightDto dto = new HighlightDto();
            PopulatePropertyValues1<HighlightDto>(dto);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dal.Insert(dto);
                }
            }

            //fetch
            Highlight highlight = Highlight.FetchById(dto.Id, HighlightDalFactoryType.Unknown);

            Highlight model = highlight;
            AssertDtoAndModelAreEqual<HighlightDto, Highlight>(dto, model);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create highlight
            Highlight highlight = Highlight.NewHighlight();
            highlight.Name = "h1";

            //save
            highlight = highlight.Save();

            HighlightDto dto;

            //fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, highlight);

            
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new highlight
            Highlight highlight = Highlight.NewHighlight();
            highlight.Name = "h1";

            //save
            highlight = highlight.Save();

            HighlightDto dto;

            //fetch highlight
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, highlight);

            //change the name
            highlight.Name = "h1.1";

            //save new highlight
            highlight = highlight.Save();

            //re-fetch highlight
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, highlight);

            
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new highlight
            Highlight highlight = Highlight.NewHighlight();
            highlight.Name = "h1";

            //save
            highlight = highlight.Save();

            HighlightDto dto;
            //fetch highlight
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, highlight);

            //delete label
            highlight.Delete();
            highlight.Save();

            //check if highlight has beem deleted
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {

                    dto = dal.FetchById(highlight.Id);

                    Assert.IsNotNull(dto.DateDeleted, "DateDeleted should be marked.");

                    //Assert.Throws(typeof(DtoDoesNotExistException),
                    //    () =>
                    //    {
                    //        dto = dal.FetchById(highlight.Id);
                    //    },
                    //    "model should no longer exist"
                    //);
                }
            }
        }

        #endregion

    }
}
