﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class EngineTaskInfoListTests : TestBase
    {
        #region Factory Methods
        /// <summary>
        /// Tests that we can return all registered tasks
        /// </summary>
        [Test]
        public void FetchAll()
        {
            // register the task assembly
            TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");

            // fetch the list of engine task infos
            EngineTaskInfoList tasks = EngineTaskInfoList.FetchAll();

            // assert that we found some tasks
            Assert.IsTrue(tasks.Count > 0);
        }
        #endregion
    }
}
