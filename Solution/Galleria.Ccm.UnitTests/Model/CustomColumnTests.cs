﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.
// V8-27504 : A.Silva
//      Amended to account for changes in CustomColumn.NewCustomColumn signature.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnTests : TestBase
    {
        #region Test Fields

        private const String CorrectPath = "CorrectPath";
        private const Int32 CorrectNumber = 1;
        private const Int32 CorrectHeaderGroupNumber = 1;

        private readonly CustomColumnDto _testCustomColumnDto = new CustomColumnDto
        {
            CustomColumnLayoutId = "CustomColumnLayoutId",
            IsVisibleOnLoad = true,
            Number = CorrectNumber,
            Path = CorrectPath,
            TotalType = (Byte) CustomColumnTotalType.StandardDeviation
        };

        #endregion

        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumn testModel = CustomColumn.NewCustomColumn(null, 0);
            
            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<CustomColumn>(CustomColumn.NewCustomColumn(null, 0));
        }

        #endregion

        #region Factory Methods

        #region NewCustomColumn

        [Test]
        public void NewCustomColumn_ShouldBeChildObject()
        {
            CustomColumn testModel = CustomColumn.NewCustomColumn(null, 0);

            Assert.IsTrue(testModel.IsChild, "A Custom Column should be created as a child object.");
        }

        [Test]
        public void NewCustomColumn_ShouldHaveCorrectPath()
        {
            CustomColumn testModel = CustomColumn.NewCustomColumn(CorrectPath, 0);

            Assert.AreEqual(CorrectPath, testModel.Path, "A new Custom Column should have the provided path.");
        }

        [Test]
        public void NewCustomColumn_ShouldHaveCorrectNumber()
        {
            CustomColumn testModel = CustomColumn.NewCustomColumn(null, CorrectNumber);

            Assert.AreEqual(CorrectNumber, testModel.Number, "A new Custom Column should have the provided order number.");
        }


        [Test]
        public void NewCustomColumn_ShouldHaveColumnTotalTypeNone()
        {
            CustomColumn testModel = CustomColumn.NewCustomColumn(null, 0);

            Assert.AreEqual(CustomColumnTotalType.None, testModel.TotalType,
                "A new Custom Column should be have None as Column Total Type.");
        }

        #endregion

        [Test]
        public void FetchCustomcolumn()
        {
            CustomColumn actual = CustomColumn.FetchCustomColumn(null, _testCustomColumnDto);

            AssertDtoAndModelAreEqual(_testCustomColumnDto, actual);
        }

        #endregion
    }
}