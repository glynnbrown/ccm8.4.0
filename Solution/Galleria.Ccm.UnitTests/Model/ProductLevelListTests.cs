﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ProductLevelListTests : TestBase
    {
        [Test]
        public void Serializable()
        {
            //Fetch product list
            ProductLevelList model = ProductLevelList.NewProductLevelList();

            //Serializable
            Serialize(model);
        }

        [Test]
        public void NewProductLevelList()
        {
            //Fetch product list
            ProductLevelList model = ProductLevelList.NewProductLevelList();

            Assert.IsEmpty(model);
        }
    }
}
