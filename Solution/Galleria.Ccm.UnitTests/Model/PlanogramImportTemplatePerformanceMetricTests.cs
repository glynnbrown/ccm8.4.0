﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramImportTemplatePerformanceMetricTests : TestBase<PlanogramImportTemplatePerformanceMetric, PlanogramImportTemplatePerformanceMetricDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }


        protected override void CreateObjectTree(out object root, out PlanogramImportTemplatePerformanceMetric model)
        {
            PlanogramImportTemplate item = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1");
            item.Name = Guid.NewGuid().ToString();

            item.PerformanceMetrics.Clear();

            PlanogramImportTemplatePerformanceMetric metric = NewModel();
            metric.Name = "Test";
            metric.MetricId = 1;
            metric.ExternalField = "Test";

            item.PerformanceMetrics.Add(metric);

            root = item;
            model = metric;
        }


        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod(new String[] { "MetricId" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<PlanogramImportTemplate>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<PlanogramImportTemplate>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<PlanogramImportTemplate>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<PlanogramImportTemplate>();
        }

        #endregion
    }
}