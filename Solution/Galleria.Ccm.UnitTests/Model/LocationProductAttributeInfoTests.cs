﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-27630 : I.George
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttributeInfo : TestBase
    {
        #region Helper methods
        
        private List<LocationProductAttributeDto> InsertDtos()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId);

            List<LocationProductAttributeDto> dtoList = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtoList, prodList, entityId);
            return dtoList;
        }
        #endregion
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
          InsertDtos();
           Entity entity = Entity.NewEntity();
           

            Galleria.Ccm.Model.LocationProductAttributeInfoList model =
                Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(entity.Id);

            Serialize(model);
        }

        [Test]
        public void GetLocationProductAttributeInfo()
        {
           
            List<LocationProductAttributeDto> dtosList = InsertDtos();
           

            Galleria.Ccm.Model.LocationProductAttributeInfoList infoList =
                Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(1);

            foreach (LocationProductAttributeDto dto in dtosList)
            {
                Galleria.Ccm.Model.LocationProductAttributeInfo info =
                    infoList.FirstOrDefault(i =>
                        i.ProductId == dto.ProductId &&
                        i.LocationId == dto.LocationId);

                AssertHelper.AssertModelObjectsAreEqual(info, dto);
            }
        }
        #endregion

    }
}
