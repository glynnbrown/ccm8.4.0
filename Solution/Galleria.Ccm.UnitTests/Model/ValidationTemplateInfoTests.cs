﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24761 : A.Silva ~ Created

#endregion

#endregion

using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ValidationTemplateInfoTests : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            var model = GetInfoList()[0];
            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void FetchValidationTemplateInfo()
        {
            using (var dalContext = DalFactory.CreateContext())
            {
                var infoList = GetInfoList();
                foreach (var dto in infoList)
                {
                    var dtoId = dto.Id;
                    var model = infoList.FirstOrDefault(info => info.Id == dtoId);

                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }
        #endregion

        #region Test Helper Methods

        private ValidationTemplateInfoList GetInfoList()
        {
            var entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1)[0];
            var templateDtos = TestDataHelper.InsertValidationTemplateDtos(DalFactory, 5, entityDto.Id);
            var groupDtos = TestDataHelper.InsertValidationTemplateGroupDtos(DalFactory, templateDtos, 5);
            TestDataHelper.InsertValidationTemplateGroupMetricDtos(DalFactory, groupDtos, 10);

            var infoList = ValidationTemplateInfoList.FetchByEntityId(entityDto.Id);
            return infoList;
        }

        #endregion
    }
}
