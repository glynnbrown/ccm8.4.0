﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
#endregion

#region Version History: (CCM v8.1.0)
// V8-29746 : A.Probyn
//  Needs resolving once V8-29847 test data is fixed.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ContentLookupListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Assert.Ignore();
            
            //Insert location data
            IEnumerable<ContentLookupDto> clusterDtos = TestDataHelper.InsertContentLookupDtos(this.DalFactory, 1);

            //Call factory method
            Ccm.Model.ContentLookupList contentLookupList = Ccm.Model.ContentLookupList.FetchByEntityId(1);

            Serialize(contentLookupList);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewClusterList()
        {
            Assert.Ignore();
            
            //Insert location data
            IEnumerable<ContentLookupDto> contentLookupDtos = TestDataHelper.InsertContentLookupDtos(this.DalFactory, 1);

            //Call factory method
            Ccm.Model.ContentLookupList contentLookupList = Ccm.Model.ContentLookupList.FetchByEntityId(1);

            foreach (ContentLookup currentLookup in contentLookupList)
            {
                Assert.IsTrue(currentLookup.IsChild);
            }
        }

        [Test]
        public void FetchByEntityId()
        {
            Assert.Ignore();
            
            //Insert location data
            IEnumerable<ContentLookupDto> contentLookupDtos = TestDataHelper.InsertContentLookupDtos(this.DalFactory, 1);

            //Call factory method
            Ccm.Model.ContentLookupList contentLookupList = Ccm.Model.ContentLookupList.FetchByEntityId(1);

            //Check clusters have been fetched correctly (1 cluster per location)
            Assert.AreEqual(contentLookupList.Count(), contentLookupDtos.Count(), "Expect same amount of items");
        }

        #endregion
    }
}
