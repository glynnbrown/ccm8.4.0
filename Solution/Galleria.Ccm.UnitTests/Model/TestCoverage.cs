﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Linq;
using System.Reflection;

using NUnit.Framework;
using System.Diagnostics;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class TestCoverage
    {
        [Test]
        public void VerifyTestCoverage()
        {
            Boolean foundAllClasses = true;
            String errorMessage = "";

            foreach (Type dtoClass in
                Assembly.GetAssembly(typeof(Galleria.Ccm.Constants)).GetTypes().Where(
                t => t.Namespace == "Galleria.Ccm.Model" &&
                    !t.IsInterface &&
                    (t.BaseType.Name.StartsWith("ModelObject") ||
                    t.BaseType.Name.StartsWith("ModelReadOnlyObject") ||
                    t.BaseType.Name.StartsWith("ModelList") ||
                    t.BaseType.Name.StartsWith("ModelReadOnlyList"))
                    && !t.Name.StartsWith("ModelObject")
                    && !t.Name.StartsWith("ModelList"))
                    .OrderBy(t=> t.Name)) 
            {

                String testClassName = String.Format(
                    "Galleria.Ccm.UnitTests.Model.{0}Tests",
                    dtoClass.Name);
                Type testClass = Type.GetType(testClassName);
                Boolean foundThisMethod = false;
                if (testClass != null)
                {
                    // We've found the test method--check that it is decorated with the [Test] attribute:
                    foreach (Object attribute in testClass.GetCustomAttributes(false))
                    {
                        if (attribute is TestFixtureAttribute)
                        {
                            foundThisMethod = true;
                            break;
                        }
                    }
                }
                if (!foundThisMethod)
                {
                    // Output the names of missing methods:
                    foundAllClasses = false;
                    errorMessage = String.Format("{0}{1}No {2} test class found.", errorMessage, Environment.NewLine, testClassName);
                }
            }
            // Assert that all classes were found.
            if (!foundAllClasses)
            {
                Debug.WriteLine(errorMessage);
                throw new InconclusiveException(errorMessage);
            }
            //Assert.IsTrue(foundAllClasses, errorMessage);
        }
    }
}
