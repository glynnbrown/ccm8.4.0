﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Security.Principal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserPlanogramGroupListTests : TestBase
    {
        [Test]
        public void Serializable()
        {
            var model = UserPlanogramGroupList.NewUserPlanogramGroupList();
            TestDelegate serialize = () => Serialize(model);
            Assert.DoesNotThrow(serialize);
        }

        [Test]
        public void NewUserPlanogramGroupList()
        {
            var model = UserPlanogramGroupList.NewUserPlanogramGroupList();
            Assert.IsEmpty(model);
        }

        [Test]
        public void FetchByUserId()
        {
            var userDto = InsertDtoForCurrentUser();
            InsertTwoUserPlanogramGroups(userDto);

            var list = UserPlanogramGroupList.FetchByUserId(userDto.Id);

            Assert.AreEqual(2, list.Count);
        }

        [Test]
        public void Add()
        {
            var userDto = InsertDtoForCurrentUser();
            InsertTwoUserPlanogramGroups(userDto);
            var list = UserPlanogramGroupList.FetchByUserId(userDto.Id);
            var oldCount = list.Count;
            var newUserPlanogramGroup = UserPlanogramGroup.NewUserPlanogramGroup(1, 1);
            newUserPlanogramGroup.UserId = userDto.Id;
            newUserPlanogramGroup.PlanogramGroupId = 2;

            list.Add(newUserPlanogramGroup);
            list.Save();

            list = UserPlanogramGroupList.FetchByUserId(userDto.Id);
            Assert.AreEqual(oldCount + 1, list.Count);
        }

        [Test]
        public void Edit()
        {
            var userDto = InsertDtoForCurrentUser();
            InsertTwoUserPlanogramGroups(userDto);
            var list = UserPlanogramGroupList.FetchByUserId(userDto.Id);

            list.Where(o => o.PlanogramGroupId == 1).FirstOrDefault().PlanogramGroupId = 2;
            list.Save();

            list = UserPlanogramGroupList.FetchByUserId(userDto.Id);
            Assert.That(list.Any(o=>o.PlanogramGroupId==2));
        }

        [Test]
        public void Remove()
        {
            var userDto = InsertDtoForCurrentUser();
            InsertTwoUserPlanogramGroups(userDto);
            var list = UserPlanogramGroupList.FetchByUserId(userDto.Id);
            var oldCount = list.Count();

            list.Remove(list.FirstOrDefault());
            list.Save();

            list = UserPlanogramGroupList.FetchByUserId(userDto.Id);
            Assert.AreEqual(oldCount - 1, list.Count());
        }

        private void InsertTwoUserPlanogramGroups(UserDto userDto)
        {
            var entityDto = TestDataHelper.InsertEntityDtos(DalFactory, 1).FirstOrDefault();
            var planHierarchy = PlanogramHierarchy.NewPlanogramHierarchy(entityDto.Id);
            planHierarchy.Name = "PlanHierarchy";
            var planGroup = PlanogramGroup.NewPlanogramGroup();
            planGroup.Name = "PlanGroupName";
            planGroup.Id = 1;
            planHierarchy.RootGroup.ChildList.Add(planGroup);
            var planGroup2 = PlanogramGroup.NewPlanogramGroup();
            planGroup2.Name = "PlanGroupName2";
            planGroup2.Id = 2;
            planHierarchy.RootGroup.ChildList.Add(planGroup2);
            planHierarchy.Save();

            using(var dalContext = DalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IUserPlanogramGroupDal>())
            {
                var dto1 = new UserPlanogramGroupDto
                {
                    UserId = userDto.Id,
                    PlanogramGroupId = planHierarchy.RootGroup.Id
                };
                var dto2 = new UserPlanogramGroupDto
                {
                    UserId = userDto.Id,
                    PlanogramGroupId = planGroup.Id
                };
                dal.Insert(dto1);
                dal.Insert(dto2);
            }
        }

        private UserDto InsertDtoForCurrentUser()
        {
            var dto = new UserDto()
            {
                UserName = WindowsIdentity.GetCurrent().Name
            };

            try
            {
                using (var dalContext = this.DalFactory.CreateContext())
                {
                    using (var dal = dalContext.GetDal<IUserDal>())
                    {
                        dal.Insert(dto);
                    }
                }
            }
            catch (Exception)
            {
                // DTO already exists
            }
            return dto;
        }
    }
}
