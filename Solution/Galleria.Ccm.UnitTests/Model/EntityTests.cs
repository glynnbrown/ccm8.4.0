﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created
// V8-27964 : A.Silva
//      Added SyncPlanHierarchy method tests.

#endregion
#endregion

using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class EntityTests : TestBase
    {
        #region Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(EntityDto dto, Entity model)
        {
            Assert.AreEqual(11, typeof(EntityDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.Description, model.Description);
            Assert.AreEqual(dto.GFSId, model.GFSId);
            //dtokey
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Entity model = Entity.NewEntity();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model
            Entity model = Entity.NewEntity();

            TestPropertySetters<Entity>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewEntity()
        {
            // create a new model
            Entity model = Entity.NewEntity();

            //amend details
            model.Name = Guid.NewGuid().ToString();

            //take copy of cluster scheme
            Entity copy = model.Clone();

            // create a new model
            model = Entity.NewEntity();

            Assert.AreNotEqual(model.Name, copy.Name, "Names should not match");
        }

        [Test]
        public void FetchById()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 25);

            foreach (EntityDto dto in entityDtos)
            {
                Entity model =
                    Entity.FetchById(dto.Id);

                AssertDtoAndModelAreEqual(dto, model);
            }

        }

        #endregion

        #region Business Rules

        /// <summary>
        /// NameRequired
        /// </summary>
        [Test]
        public void NameRequired()
        {
            // create a new model
            Entity model = Entity.NewEntity();

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = string.Empty;
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        /// <summary>
        /// NamePropertyMaxLength
        /// </summary>
        [Test]
        public void NamePropertyMaxLength()
        {
            // create a new model
            Entity model = Entity.NewEntity();

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = new String('*', 101);
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_InsertUpdateDelete()
        {
            //create a new item
            Entity item =  Entity.NewEntity();
            item.Name = "NEW";

            //insert
            item = item.Save();

            EntityDto entityDto;
            
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    entityDto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(entityDto, item);

            //update
            item.Name = "UPDATED";
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    entityDto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(entityDto, item);
            

            //update again to check concurrency
            item.Name = "UPDATED2";
            item = item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    entityDto = dal.FetchById(item.Id);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(entityDto, item);
            

            //delete
            Object itemId = item.Id;
            item.Delete();
            item.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    entityDto = dal.FetchById(item.Id);
                    Assert.IsNotNull(entityDto.DateDeleted);

                }
            }
        }

        /// <summary>
        /// Checks that the GetDto and load dto methods are correct
        /// </summary>
        [Test]
        public void DataTransferObject()
        {
            //create a dto
            EntityDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    Assert.AreEqual(11, typeof(EntityDto).GetProperties().Count());

                    dto = new EntityDto()
                    {
                        Id = 1,
                        Description = "description",
                        GFSId = 1,
                        Name = "name",
                        //DtoKey,
                        //RowVersion
                    };

                    dal.Insert(dto);
                }
            }

            //fetch back as a model
            Entity model =
                Entity.FetchById(dto.Id);

            AssertDtoAndModelAreEqual(dto, model);

            //update the model properties
            model.Name = "name2";
            model.Description = "description2";
            model.GFSId = 2;

            model = model.Save();

            //reget the saved dto and check
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    dto = dal.FetchById(model.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, model);
        }

        #endregion

        #region Methods

        #region SyncPlanHierarchy

        [Test]
        public void SyncPlanHierarchy_WhenProductLevelIdNotSet_ShouldNotSetDateLastMerchSynced()
        {
            Entity entity = CreateEntity();

            entity.SyncPlanHierarchy();

            Assert.AreEqual(null, entity.DateLastMerchSynced);
        }

        [Test]
        public void SyncPlanHierarchy_WhenSuccesful_ShouldSetDateLastMerchSynced()
        {
            Entity entity = CreateEntity();

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);

            SetProductLevel(ref entity, productHierarchy.RootLevel.Id);

            entity.SyncPlanHierarchy();

            Assert.AreNotEqual(null, entity.DateLastMerchSynced);
        }

        [Test]
        public void SyncPlanHierarchy_WhenHierarchyNotModified_ShouldNotSetDateLastMerchSynced()
        {
            Entity entity = CreateEntity();

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);

            DateTime? expected = productHierarchy.DateLastModified;

            SetProductLevel(ref entity, productHierarchy.RootLevel.Id, expected);

            entity.SyncPlanHierarchy();

            Assert.AreEqual(expected, entity.DateLastMerchSynced);
        }

        [Test]
        public void SyncPlanHierarchy_WhenHierarchyModified_ShouldSetDateLastMerchSynced()
        {
            Entity entity = CreateEntity();

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);
            ProductLevel rootLevel = productHierarchy.RootLevel;
            DateTime? expected = productHierarchy.DateLastModified;

            SetProductLevel(ref entity, rootLevel.Id, expected);

            //  Modify the product hierarchy.
            AddNewProductGroup(ref productHierarchy, productHierarchy.RootGroup);

            entity.SyncPlanHierarchy();

            Assert.AreNotEqual(expected, entity.DateLastMerchSynced);
        }

        [Test]
        public void SyncPlanHierarchy_WhenHierarchyModified_ShouldSyncPlanHierarchy()
        {
            Entity entity = CreateEntity();

            List<PlanogramGroup> originalGroups = GetAllPlanogramGroups(entity);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);
            ProductLevel rootLevel = productHierarchy.RootLevel;

            SetProductLevel(ref entity, rootLevel.Id, productHierarchy.DateLastModified);

            //  Modify the product hierarchy.
            AddNewProductGroup(ref productHierarchy, productHierarchy.RootGroup);

            entity.SyncPlanHierarchy();

            List<PlanogramGroup> expected = GetAllPlanogramGroups(entity);
            CollectionAssert.AreNotEqual(expected.Select(o => o.Name), originalGroups.Select(o => o.Name));
        }

        [Test]
        public void SyncPlanHierarchy_WhenGroupExists_ShouldSyncPlanHierarchy()
        {
            Entity entity = CreateEntity();

            PlanogramHierarchy planogramHierarchy = PlanogramHierarchy.FetchByEntityId(entity.Id);
            PlanogramGroup newPlanogramGroup = AddNewPlanogramGroup(ref planogramHierarchy, planogramHierarchy.RootGroup);
            List<PlanogramGroup> originalGroups = GetAllPlanogramGroups(entity);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);
            ProductLevel rootLevel = productHierarchy.RootLevel;

            SetProductLevel(ref entity, rootLevel.Id, productHierarchy.DateLastModified);

            //  Modify the product hierarchy.
            AddNewProductGroup(ref productHierarchy, productHierarchy.RootGroup, newPlanogramGroup.Name);

            entity.SyncPlanHierarchy();

            List<PlanogramGroup> expected = GetAllPlanogramGroups(entity);
            CollectionAssert.AreEqual(expected.Select(o => o.Name), originalGroups.Select(o => o.Name));
        }

        [Test]
        public void SyncPlanHierarchy_WhenGroupAssociated_ShouldSyncPlanHierarchy()
        {
            Entity entity = CreateEntity();

            PlanogramHierarchy planogramHierarchy = PlanogramHierarchy.FetchByEntityId(entity.Id);
            PlanogramGroup newPlanogramGroup = AddNewPlanogramGroup(ref planogramHierarchy, planogramHierarchy.RootGroup);
            List<PlanogramGroup> originalGroups = GetAllPlanogramGroups(entity);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);
            ProductLevel rootLevel = productHierarchy.RootLevel;

            SetProductLevel(ref entity, rootLevel.Id, productHierarchy.DateLastModified);

            //  Modify the product hierarchy.
            ProductGroup existingProductGroup = AddNewProductGroup(ref productHierarchy, productHierarchy.RootGroup,
                newPlanogramGroup.Name);
            AddNewProductGroup(ref productHierarchy, existingProductGroup);

            entity.SyncPlanHierarchy();

            List<PlanogramGroup> expected = GetAllPlanogramGroups(entity);
            CollectionAssert.AreNotEqual(expected.Select(o => o.Name), originalGroups.Select(o => o.Name));
        }

        [Test]
        public void SyncPlanHierarchy_WhenGroupAssociatedToAnother_ShouldNotSyncPlanHierarchy()
        {
            Entity entity = CreateEntity();

            PlanogramHierarchy planogramHierarchy = PlanogramHierarchy.FetchByEntityId(entity.Id);
            PlanogramGroup newPlanogramGroup = AddNewPlanogramGroup(ref planogramHierarchy, planogramHierarchy.RootGroup,
                productGroupId: 0);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);
            ProductLevel rootLevel = productHierarchy.RootLevel;

            SetProductLevel(ref entity, rootLevel.Id, productHierarchy.DateLastModified);

            //  Modify the product hierarchy.
            AddNewProductGroup(ref productHierarchy, productHierarchy.RootGroup, newPlanogramGroup.Name);

            entity.SyncPlanHierarchy();

            Assert.AreEqual(0, GetAllPlanogramGroups(entity).First(o => o.Name == newPlanogramGroup.Name).ProductGroupId);
        }

        [Test]
        public void SyncPlanHierarchy_WhenGroupAssociated_ShouldNotSyncPlanHierarchyBelow()
        {
            Entity entity = CreateEntity();

            PlanogramHierarchy planogramHierarchy = PlanogramHierarchy.FetchByEntityId(entity.Id);
            PlanogramGroup newPlanogramGroup = AddNewPlanogramGroup(ref planogramHierarchy, planogramHierarchy.RootGroup,
                productGroupId: 0);
            List<PlanogramGroup> originalGroups = GetAllPlanogramGroups(entity);

            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entity.Id);
            ProductLevel rootLevel = productHierarchy.RootLevel;

            SetProductLevel(ref entity, rootLevel.Id, productHierarchy.DateLastModified);

            //  Modify the product hierarchy.
            ProductGroup existingProductGroup = AddNewProductGroup(ref productHierarchy, productHierarchy.RootGroup,
                newPlanogramGroup.Name);
            AddNewProductGroup(ref productHierarchy, existingProductGroup);

            entity.SyncPlanHierarchy();

            List<PlanogramGroup> expected = GetAllPlanogramGroups(entity);
            CollectionAssert.AreEqual(expected.Select(o => o.Name), originalGroups.Select(o => o.Name));
        }

        private static List<PlanogramGroup> GetAllPlanogramGroups(Entity entity)
        {
            return PlanogramHierarchy.FetchByEntityId(entity.Id).EnumerateAllGroups().ToList();
        }

        private static PlanogramGroup AddNewPlanogramGroup(ref PlanogramHierarchy planogramHierarchy,
            PlanogramGroup parentGroup, String name = null, Int32? productGroupId = null)
        {
            PlanogramGroup newItem = PlanogramGroup.NewPlanogramGroup();
            newItem.Name = name ?? String.Format("{0} {1}", parentGroup.Name, parentGroup.ChildList.Count);
            newItem.ProductGroupId = productGroupId;
            parentGroup.ChildList.Add(newItem);
            planogramHierarchy = planogramHierarchy.Save();
            return
                planogramHierarchy.EnumerateAllGroups()
                    .FirstOrDefault(o => o.Name == newItem.Name && o.ProductGroupId == newItem.ProductGroupId);
        }

        private static ProductGroup AddNewProductGroup(ref ProductHierarchy productHierarchy,
            ProductGroup parentProductGroup, String name = null)
        {
            ProductGroup newItem = ProductGroup.NewProductGroup(parentProductGroup.GetAssociatedLevel().ChildLevel.Id);
            newItem.Code = productHierarchy.GetNextAvailableDefaultGroupCode();
            newItem.Name = name ?? String.Format("{0} {1}", parentProductGroup.Name, parentProductGroup.ChildList.Count);
            parentProductGroup.ChildList.Add(newItem);
            productHierarchy = productHierarchy.Save();
            return
                productHierarchy.EnumerateAllGroups()
                    .FirstOrDefault(o => o.Name == newItem.Name && o.Code == newItem.Code);
        }

        private static ProductLevel AddNewProductLevel(ref ProductHierarchy productHierarchy, ProductLevel rootLevel,
            String name = null)
        {
            ProductLevel newProductLevel = ProductLevel.NewProductLevel();
            newProductLevel.Name = name ?? productHierarchy.GetNextAvailableLevelName(rootLevel);
            productHierarchy.AddLevel(newProductLevel, rootLevel);
            productHierarchy = productHierarchy.Save();
            return newProductLevel;
        }

        private static Entity CreateEntity()
        {
            Entity entity = Entity.NewEntity();
            entity.Name = "DefaultEntity";
            entity = entity.Save();
            return entity;
        }

        private static void SetProductLevel(ref Entity entity, Int32? productLevelId = null,
            DateTime? dateLastMerchSynced = null)
        {
            entity.ProductLevelId = productLevelId;
            entity.DateLastMerchSynced = dateLastMerchSynced;
            entity = entity.Save();
        }

        #endregion

        #endregion
    }
}
