﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (SA 1.0)
// V8-24779 : L.Luong
//		Created 

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramImportTemplateInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<PlanogramImportTemplateDto> planogramImportTemplateList =
            TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 20, entityId);

            PlanogramImportTemplateInfoList model = PlanogramImportTemplateInfoList.FetchByEntityId(1);

            Serialize(model);

            Assert.IsTrue(model.IsReadOnly);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            List<PlanogramImportTemplateDto> planogramImportTemplateList1 =
            TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 20, entityList[0].Id);

            List<PlanogramImportTemplateDto> planogramImportTemplateList2 =
            TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 20, entityList[1].Id);

            PlanogramImportTemplateInfoList model1 = PlanogramImportTemplateInfoList.FetchByEntityId(entityList[0].Id);
            foreach (PlanogramImportTemplateDto impDto in planogramImportTemplateList1)
            {
                PlanogramImportTemplateInfo info = model1.First(i => i.Id.Equals(impDto.Id));
            }

            PlanogramImportTemplateInfoList model2 = PlanogramImportTemplateInfoList.FetchByEntityId(entityList[1].Id);
            foreach (PlanogramImportTemplateDto impDto in planogramImportTemplateList2)
            {
                PlanogramImportTemplateInfo info = model2.First(i => i.Id.Equals(impDto.Id));
            }
        }

        #endregion
    }
}
