﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class MetricProfileInfoTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(MetricProfileDto dto, MetricProfileInfo model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
        }

        private void AssertModelandModelAreEqual(MetricProfileInfo model1, MetricProfileInfo model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.Name, model2.Name);
        }

        private List<MetricProfileDto> InsertTestDtos()
        {
            EntityDto entity1 = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0];
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<MetricDto> metricDtoList = TestDataHelper.InsertMetricDtos(base.DalFactory, 50, entityList[0].Id);
            List<MetricProfileDto> metricProfileDtoList = TestDataHelper.InsertMetricProfileDtos(base.DalFactory, 50, entityList[0].Id, metricDtoList);

            return metricProfileDtoList;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            var dtos = InsertTestDtos();
            MetricProfileInfoList infoList = MetricProfileInfoList.FetchByEntityId(dtos[0].EntityId);

            MetricProfileInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchMetricProfileInfo()
        {
            List<MetricProfileDto> dtoList = InsertTestDtos();

            MetricProfileInfoList infoList = MetricProfileInfoList.FetchByEntityId(1);
            foreach (var prodDto in infoList)
            {
                MetricProfileInfo dalLocInfo =
                    infoList.First(l => l.Id == prodDto.Id);
                AssertDtoAndModelAreEqual(prodDto, dalLocInfo);
            }
        }

        #endregion
    }
}
