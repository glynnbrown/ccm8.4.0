﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27804 : L.Ineson
//  Created.
#endregion
#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramImportTemplateMappingListTests : TestBase<PlanogramImportTemplateMapping, PlanogramImportTemplateMappingDto>
    {
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerializeList();
        }

        [Test]
        public void NewFactoryMethod()
        {
            TestNewListFactoryMethod<PlanogramImportTemplateMappingList>();
        }


    }
}
