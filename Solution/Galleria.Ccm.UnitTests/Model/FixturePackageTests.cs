﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#region Version History: (CCM CCM800)
// CCM-31280  : J.Pickup
//		GetPlanogramImageId
#endregion

#region Version History: (CCM CCM830)
// V8-32582 : L.Ineson
//  Added more tests.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;

using NUnit.Framework;

using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using System.Reflection;
using System.Linq;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class FixturePackageTests : TestBase
    {

        [Test]
        public void EnumsMatchPlanogramVersions()
        {
            foreach (Type fixtureEnumType in
                Assembly.GetAssembly(typeof(Galleria.Ccm.Constants)).GetTypes()
                .Where(t => t.Namespace == "Galleria.Ccm.Model" && t.IsEnum && t.Name.StartsWith("Fixture")))
            {
                String planEnumName = fixtureEnumType.Name.Replace("Fixture", "Planogram");

                Type planEnumType = Assembly.GetAssembly(typeof(Galleria.Framework.Planograms.Model.Planogram))
                    .GetType("Galleria.Framework.Planograms.Model."+planEnumName);
                if(planEnumType != null)
                {
                    Object[] planEnumValues = Enum.GetValues(planEnumType).Cast<Object>().ToArray();
                    Object[] fixtureEnumValues = Enum.GetValues(fixtureEnumType).Cast<Object>().ToArray();

                    Assert.AreEqual(planEnumValues.Count(), fixtureEnumValues.Count(),
                        "Incorrect count: " + fixtureEnumType.Name);

                    for(Int32 i =0; i< planEnumValues.Count(); i++)
                    {
                        Assert.AreEqual(planEnumValues[i].ToString(), fixtureEnumValues[i].ToString(),
                            "Incorrect value: " + fixtureEnumType.Name);
                    }
                }
            }
        }

        [Test]
        public void SaveFixtureWithAssemblies()
        {
            //GEM26182

            Galleria.Ccm.Model.FixturePackage package = Galleria.Ccm.Model.FixturePackage.NewFixturePackage();
            package.Name = "Test";

            Fixture fixture1 = Fixture.NewFixture();
            fixture1.Name = "fx1";
            package.Fixtures.Add(fixture1);

            FixtureAssembly assembly1 = FixtureAssembly.NewFixtureAssembly();
            assembly1.Name = "assem1";
            package.Assemblies.Add(assembly1);

            FixtureAssemblyItem fAssembly1 = FixtureAssemblyItem.NewFixtureAssemblyItem();
            fAssembly1.FixtureAssemblyId = assembly1.Id;
            fixture1.Assemblies.Add(fAssembly1);

            FixtureComponent component1 = FixtureComponent.NewFixtureComponent();
            component1.Name = "c1";
            package.Components.Add(component1);

            FixtureAssemblyComponent cItem1 = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
            cItem1.FixtureComponentId = component1.Id;
            assembly1.Components.Add(cItem1);

            //save
            Galleria.Ccm.Model.FixturePackage savedPackage = null;
            savedPackage = package.Save();
            savedPackage = Galleria.Ccm.Model.FixturePackage.FetchById(savedPackage.Id);

            //check
            Assert.AreEqual(1, package.Fixtures.Count, "Fixture count wrong");
            Assert.AreEqual(1, package.Fixtures[0].Assemblies.Count, "FixtureAssemblyItem count wrong");
            Assert.AreEqual(1, package.Assemblies.Count, "FixtureAssembly count wrong");
            Assert.AreEqual(1, package.Assemblies[0].Components.Count, "FixtureComponent count wrong.");
        }


        [Test]
        public void ConvertFixtureComponentWithLinkedAnnotation()
        {
            //V8-32524 - Checks that it is possible to create a fixture package
            // from a planogram fixture component with a linked annotation

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);

            PlanogramAnnotation linkedAnno = PlanogramAnnotation.NewPlanogramAnnotation(shelf, fixtureItem, PlanogramSettings.NewPlanogramSettings());
            linkedAnno.Height = 20;
            linkedAnno.Width = 100;
            linkedAnno.Depth = 1;
            linkedAnno.Text = "LINKED ANNO";
            plan.Annotations.Add(linkedAnno);

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(shelf);

            Assert.AreEqual(1, fixturePk.Components.Count, "Should have one component");
            Assert.AreEqual(1, fixturePk.Annotations.Count, "Should have one annotation");

            Assert.AreEqual(linkedAnno.Height, fixturePk.Annotations[0].Height);
            Assert.AreEqual(linkedAnno.Width, fixturePk.Annotations[0].Width);
            Assert.AreEqual(linkedAnno.Depth, fixturePk.Annotations[0].Depth);
            Assert.AreEqual(linkedAnno.Text, fixturePk.Annotations[0].Text);


            //convert back
            pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            plan = pk.Planograms.Add();
            fixtureItem = plan.FixtureItems.Add();
            fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            FixturePackageHelper.AddToPlanogram(fixturePk, plan, fixtureItem);

            //check 
            Assert.AreEqual(1, fixture.Components.Count, "Should have one component");
            Assert.AreEqual(1, plan.Annotations.Count, "Should have one annotation");

            Assert.AreEqual(fixtureItem.Id, plan.Annotations[0].PlanogramFixtureItemId);
            Assert.AreEqual(fixture.Components[0].Id, plan.Annotations[0].PlanogramFixtureComponentId);
            Assert.IsNull(plan.Annotations[0].PlanogramFixtureAssemblyId);
            Assert.IsNull(plan.Annotations[0].PlanogramAssemblyComponentId);
            Assert.IsNull(plan.Annotations[0].PlanogramSubComponentId);
            Assert.IsNull(plan.Annotations[0].PlanogramPositionId);

            Assert.AreNotEqual(linkedAnno, plan.Annotations[0]);
            Assert.AreEqual(linkedAnno.Height, plan.Annotations[0].Height);
            Assert.AreEqual(linkedAnno.Width, plan.Annotations[0].Width);
            Assert.AreEqual(linkedAnno.Depth, plan.Annotations[0].Depth);
            Assert.AreEqual(linkedAnno.Text, plan.Annotations[0].Text);
        }

        [Test]
        public void ConvertSubComponentImages()
        {
            //V8-32524 - Checks that it is possible to create a fixture package
            // from a planogram fixture component with subcomponent images

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);

            PlanogramImage img1 = PlanogramImage.NewPlanogramImage();
            img1.FileName = "Test1";
            img1.Description = "Test1 description";
            img1.ImageData = new Byte[] { 1, 1, 1 };
            plan.Images.Add(img1);

            PlanogramImage img2 = PlanogramImage.NewPlanogramImage();
            img2.FileName = "Test2";
            img2.Description = "Test2 description";
            img2.ImageData = new Byte[] { 2, 2, 2 };
            plan.Images.Add(img2);

            shelfComponent.SubComponents[0].ImageIdFront = img1.Id;
            shelfComponent.SubComponents[0].ImageIdBack = img2.Id;

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(shelf);

            //check images
            Assert.AreEqual(2, fixturePk.Images.Count, "Should have 2 images.");
            Assert.AreEqual(fixturePk.Images[0].Id, fixturePk.Components[0].SubComponents[0].ImageIdFront);
            Assert.AreEqual(fixturePk.Images[1].Id, fixturePk.Components[0].SubComponents[0].ImageIdBack);

            //convert back
            pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            plan = pk.Planograms.Add();
            fixtureItem = plan.FixtureItems.Add();
            fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            FixturePackageHelper.AddToPlanogram(fixturePk, plan, fixtureItem);

            //check
            Assert.AreEqual(2, plan.Images.Count, "Should have 2 images.");
            Assert.AreEqual(plan.Images[0].Id, plan.Components[0].SubComponents[0].ImageIdFront);
            Assert.AreEqual(plan.Images[1].Id, plan.Components[0].SubComponents[0].ImageIdBack);
        }

        [Test]
        public void ConvertFixtureItemWithLinkedAnnotation()
        {
            //V8-32524 - Checks that it is possible to create a fixture package
            // from a planogram fixture item with a linked annotation

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);

            PlanogramAnnotation linkedAnno = PlanogramAnnotation.NewPlanogramAnnotation(fixtureItem, PlanogramSettings.NewPlanogramSettings());
            linkedAnno.Text = "LINKED ANNO";
            plan.Annotations.Add(linkedAnno);

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(fixtureItem);

            Assert.AreEqual(1, fixturePk.Fixtures.Count, "Should have one fixture");
            Assert.AreEqual(1, fixturePk.Fixtures[0].Components.Count, "Should have one fixture component");
            Assert.AreEqual(1, fixturePk.Components.Count, "Should have one component");
            Assert.AreEqual(1, fixturePk.Annotations.Count, "Should have one annotation");


            //convert back
            pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            plan = pk.Planograms.Add();
            fixtureItem = plan.FixtureItems.Add();
            fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            FixturePackageHelper.AddToPlanogram(fixturePk, plan, fixtureItem);

            //check 
            Assert.AreEqual(2, plan.FixtureItems.Count, "Should have 2 fixtures now");
            Assert.AreEqual(2, plan.Fixtures.Count, "Should have 2 fixtures now");
            Assert.AreEqual(1, plan.Fixtures[1].Components.Count, "Should have one component");
            Assert.AreEqual(1, plan.Annotations.Count, "Should have one annotation");


            Assert.AreEqual(plan.FixtureItems[1].Id, plan.Annotations[0].PlanogramFixtureItemId);
            Assert.IsNull(plan.Annotations[0].PlanogramFixtureComponentId, "Should be a fixture level annotation");
            Assert.IsNull(plan.Annotations[0].PlanogramFixtureAssemblyId);
            Assert.IsNull(plan.Annotations[0].PlanogramAssemblyComponentId);
            Assert.IsNull(plan.Annotations[0].PlanogramSubComponentId);
            Assert.IsNull(plan.Annotations[0].PlanogramPositionId);
        }

        [Test]
        public void ConvertAssembly()
        {
            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf1 = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramFixtureComponent shelf2 = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());

            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add(
                new Tuple<PlanogramFixtureComponent, PlanogramFixtureItem>[]{
                    new Tuple<PlanogramFixtureComponent, PlanogramFixtureItem>(shelf1,fixtureItem),
                    new Tuple<PlanogramFixtureComponent, PlanogramFixtureItem>(shelf2,fixtureItem)},
                    fixtureItem);

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(fixtureAssembly);

            Assert.AreEqual(0, fixturePk.FixtureItems.Count);
            Assert.AreEqual(0, fixturePk.Fixtures.Count);
            Assert.AreEqual(1, fixturePk.Assemblies.Count);
            Assert.AreEqual(2, fixturePk.Components.Count);
            Assert.AreEqual(2, fixturePk.Assemblies[0].Components.Count);

            //convert back
            pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            plan = pk.Planograms.Add();
            fixtureItem = plan.FixtureItems.Add();
            fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            FixturePackageHelper.AddToPlanogram(fixturePk, plan, fixtureItem);
            Assert.AreEqual(1, plan.Assemblies.Count);
            Assert.AreEqual(2, plan.Assemblies[0].Components.Count);
            Assert.AreEqual(2, plan.Components.Count);
            Assert.AreEqual(2, plan.Assemblies[0].Components.Count);
        }


        [Test]
        public void ChangePlanogramComponent_ChangeSubComponentProperty()
        {
            //V8-32524 - Checks the behaviour of editing a component
            // in the component editor and updating changes back to the planogram.

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);
            PlanogramSubComponent shelfSubComponent = shelfComponent.SubComponents[0];

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(shelf);

            //change a property
            fixturePk.Components[0].SubComponents[0].Name = "SUB NAME";

            //convert back
            fixturePk.Components[0].AddOrUpdatePlanogramComponent(plan);

            //Check
            Assert.AreEqual("SUB NAME", shelfSubComponent.Name);
            Assert.IsFalse(shelfSubComponent.IsDeleted);
        }

        [Test]
        public void ChangePlanogramComponent_AddImage()
        {
            //V8-32524 - Checks the behaviour of editing a component
            // in the component editor and updating changes back to the planogram.

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);
            PlanogramSubComponent shelfSubComponent = shelfComponent.SubComponents[0];

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(shelf);

            //Add an image
            FixtureImage fixtureImg = FixtureImage.NewFixtureImage();
            fixtureImg.FileName = "Test";
            fixtureImg.ImageData = new Byte[]{1,2,3};
            fixturePk.Images.Add(fixtureImg);

            fixturePk.Components[0].SubComponents[0].ImageIdFront = fixtureImg.Id;

            //convert back
            fixturePk.Components[0].AddOrUpdatePlanogramComponent(plan);

            //Check
            Assert.AreEqual(1, plan.Images.Count);
            Assert.AreEqual(plan.Images[0].Id, shelfSubComponent.ImageIdFront);
            Assert.AreEqual("Test", plan.Images[0].FileName);
        }

        [Test]
        public void ChangePlanogramComponent_RemoveImage()
        {
            //V8-32524 - Checks the behaviour of editing a component
            // in the component editor and updating changes back to the planogram.

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);
            PlanogramSubComponent shelfSubComponent = shelfComponent.SubComponents[0];

            //Add an image
            PlanogramImage fixtureImg = PlanogramImage.NewPlanogramImage();
            fixtureImg.FileName = "Test";
            fixtureImg.ImageData = new Byte[] { 1, 2, 3 };
            plan.Images.Add(fixtureImg);

            shelfSubComponent.ImageIdFront = fixtureImg.Id;


            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(shelf);

            //check
            Assert.AreEqual(1, fixturePk.Images.Count);
            Assert.AreEqual(fixturePk.Images[0].Id, fixturePk.Components[0].SubComponents[0].ImageIdFront);

            //remove it
            fixturePk.Components[0].SubComponents[0].ImageIdFront = null;

            //convert back
            fixturePk.Components[0].AddOrUpdatePlanogramComponent(plan);

            //check
            Assert.IsNull(shelfSubComponent.ImageIdFront);

            //nb - its ok that the image has not been removed from the  plan,
            // this will be done by cleanup if its not in use elsewhere.

        }

        [Test]
        public void ChangePlanogramComponent_AddSubComponent()
        {
            //V8-32524 - Checks the behaviour of editing a component
            // in the component editor and updating changes back to the planogram.

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);
            PlanogramSubComponent shelfSubComponent = shelfComponent.SubComponents[0];

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(shelf);

            FixtureSubComponent sub1 = FixtureSubComponent.NewFixtureSubComponent();
            sub1.Name = "TEST SUB";
            fixturePk.Components[0].SubComponents.Add(sub1);

            //convert back
            fixturePk.Components[0].AddOrUpdatePlanogramComponent(plan);

            Assert.AreEqual(2, shelfComponent.SubComponents.Count);
        }

        [Test]
        public void ChangePlanogramComponent_RemoveSubComponent()
        {
            //V8-32524 - Checks the behaviour of editing a component
            // in the component editor and updating changes back to the planogram.

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.User);
            Planogram plan = pk.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent shelf = fixture.Components.Add(PlanogramComponentType.Shelf, PlanogramSettings.NewPlanogramSettings());
            PlanogramComponent shelfComponent = plan.Components.FindById(shelf.PlanogramComponentId);
            PlanogramSubComponent shelfSubComponent = shelfComponent.SubComponents[0];

            PlanogramSubComponent sub1 = PlanogramSubComponent.NewPlanogramSubComponent();
            sub1.Name = "TEST SUB";
            shelfComponent.SubComponents.Add(sub1);

            //create fixture package
            FixturePackage fixturePk = FixturePackage.NewFixturePackage(shelf);

            //remove it
            fixturePk.Components[0].SubComponents.Remove(fixturePk.Components[0].SubComponents.Last());


            //convert back
            fixturePk.Components[0].AddOrUpdatePlanogramComponent(plan);

            Assert.AreEqual(1, shelfComponent.SubComponents.Count);
        }
    }
}
