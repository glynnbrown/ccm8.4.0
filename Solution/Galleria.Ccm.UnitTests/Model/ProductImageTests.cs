﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;
using System.IO;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductImageTests : TestBase
    {
        #region Fields
        private String testPath;
        private Int32 imageWidth = 300;
        private Int32 imageHeight = 100;
        private List<EntityDto> entityList;
        private String imageName = "gtin1_1.bmp";
        private Int32 _entityId;
        #endregion

        #region Test Helper Methods

        private static readonly Dictionary<String, ProductImageImageFacingType> postFixList = new Dictionary<String, ProductImageImageFacingType>()
        {
           {"_1", new ProductImageImageFacingType() { ImageType = ProductImageType.Unit, FacingType = ProductImageFacing.Front}},
           {"_2", new ProductImageImageFacingType() { ImageType = ProductImageType.Unit, FacingType = ProductImageFacing.Back}},
           {"_3", new ProductImageImageFacingType() { ImageType = ProductImageType.Unit, FacingType = ProductImageFacing.Right}},
           {"_4", new ProductImageImageFacingType() { ImageType = ProductImageType.Unit, FacingType = ProductImageFacing.Left}},
           {"_5", new ProductImageImageFacingType() { ImageType = ProductImageType.Unit, FacingType = ProductImageFacing.Top}},
           {"_6", new ProductImageImageFacingType() { ImageType = ProductImageType.Unit, FacingType = ProductImageFacing.Bottom}},
           {"_11", new ProductImageImageFacingType() { ImageType = ProductImageType.Display, FacingType = ProductImageFacing.Front}},
           {"_12", new ProductImageImageFacingType() { ImageType = ProductImageType.Display, FacingType = ProductImageFacing.Back}},
           {"_13", new ProductImageImageFacingType() { ImageType = ProductImageType.Display, FacingType = ProductImageFacing.Right}},
           {"_14", new ProductImageImageFacingType() { ImageType = ProductImageType.Display, FacingType = ProductImageFacing.Left}},
           {"_15", new ProductImageImageFacingType() { ImageType = ProductImageType.Display, FacingType = ProductImageFacing.Top}},
           {"_16", new ProductImageImageFacingType() { ImageType = ProductImageType.Display, FacingType = ProductImageFacing.Bottom}},
           {"_21", new ProductImageImageFacingType() { ImageType = ProductImageType.PointOfPurchase, FacingType = ProductImageFacing.Front}},
           {"_22", new ProductImageImageFacingType() { ImageType = ProductImageType.PointOfPurchase, FacingType = ProductImageFacing.Back}},
           {"_23", new ProductImageImageFacingType() { ImageType = ProductImageType.PointOfPurchase, FacingType = ProductImageFacing.Right}},
           {"_24", new ProductImageImageFacingType() { ImageType = ProductImageType.PointOfPurchase, FacingType = ProductImageFacing.Left}},
           {"_25", new ProductImageImageFacingType() { ImageType = ProductImageType.PointOfPurchase, FacingType = ProductImageFacing.Top}},
           {"_26", new ProductImageImageFacingType() { ImageType = ProductImageType.PointOfPurchase, FacingType = ProductImageFacing.Bottom}}
        };
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(ProductImageDto dto, ProductImage model)
        {
            Assert.AreEqual(dto.Id, model.Id, "Id Check");
            Assert.AreEqual(dto.RowVersion, model.RowVersion, "RowVersion Check");
            Assert.AreEqual(dto.ProductId, model.ProductId, "ProductId Check");
            Assert.AreEqual(dto.ImageId, model.ImageId, "ImageId Check");
            Assert.AreEqual(dto.ImageId, model.ProductImageData.Id, "ImageId Check from product image data");
            Assert.AreEqual(dto.EntityId, model.EntityId, "Id Check");
            Assert.AreEqual(dto.FacingType, (Byte)model.FacingType, "FacingType Check");
            Assert.AreEqual(dto.ImageType, (Byte)model.ImageType, "ImageType Check");
        }

        /// <summary>
        /// Loads products
        /// </summary>
        private List<ProductDto> LoadProductData()
        {
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1,_entityId);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);
            return TestDataHelper.InsertProductDtos(DalFactory, 5, _entityId);
        }
        #endregion

        #region Setup/Breakdown
        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public void ImageSetup()
        {
            TestDataHelper.InsertCompressionDtos(base.DalFactory, 1);

            const String cImageModelTest = "ImageModelTest";
            System.Drawing.Bitmap newImage = new System.Drawing.Bitmap(imageWidth, imageHeight);
            String appPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            testPath = Path.Combine(appPath, cImageModelTest);
            Directory.CreateDirectory(testPath);
            newImage.Save(Path.Combine(testPath, imageName), System.Drawing.Imaging.ImageFormat.Bmp);

            using(var dalContext = DalFactory.CreateContext())
            {
                _entityId = TestDataHelper.InsertEntityDtos(dalContext, 1).FirstOrDefault().Id;
            }
        }

        /// <summary>
        /// Called when a test is completed
        /// </summary>
        [TearDown]
        public void ImageTeardown()
        {
            Directory.Delete(testPath, true);
        }
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            LoadProductData();
            ProductImage model =
                ProductImage.NewProductImage(
                    Image.NewImage(_entityId, Path.Combine(testPath, imageName)),
                    new ProductImageImageFacingType()
                    {
                        FacingType = ProductImageFacing.Front,
                        ImageType = ProductImageType.Unit
                    }, 0);
            Serialize(model);
        }
        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            ProductImage model = ProductImage.NewProductImage(
                Image.NewImage(_entityId, Path.Combine(testPath,imageName)),
                new ProductImageImageFacingType(),
                1);
            TestPropertySetters<ProductImage>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewProductImage()
        {
            LoadProductData();

            Image img = Image.NewImage(_entityId, Path.Combine(testPath, imageName));
            img.CompressionId = 1;
            img = img.Save();

            ProductImage newItem =
                ProductImage.NewProductImage(img,
                    new ProductImageImageFacingType()
                    {
                        FacingType = ProductImageFacing.Front,
                        ImageType = ProductImageType.Unit
                    }, 0);

            Assert.IsTrue(newItem.IsNew);
            Assert.IsTrue(newItem.IsChild);
        }

        #endregion

        #region Data Access
        [Test]
        public void DataAccess_Insert()
        {
            // insert some products
            List<ProductDto> productList = LoadProductData();
            var productModel = Product.FetchById(productList.First().Id);

            // build the product image list
            Image img = Image.NewImage(_entityId, Path.Combine(testPath, imageName));
            img.CompressionId = 1;
            img = img.Save();

            productModel.ImageList.Add(ProductImage.NewProductImage(img,
                    new ProductImageImageFacingType()
                    {
                        FacingType = ProductImageFacing.Front,
                        ImageType = ProductImageType.Unit
                    },
                    productModel.Id));
            productModel = productModel.Save();

            foreach (ProductImage pImage in productModel.ImageList)
            {
                ProductImageDto dto;
                using (IDalContext dalContext = base.DalFactory.CreateContext())
                {
                    using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
                    {
                        dto = dal.FetchById(pImage.Id);
                        AssertDtoAndModelAreEqual(dto, pImage);
                    }
                }
            }
        }

        [Test]
        public void DataAccess_Delete()
        {
            // insert some products
            List<ProductDto> productList = LoadProductData();
            var productModel = Product.FetchById(productList.First().Id);

            // build the product image list
            Image img = Image.NewImage(_entityId, Path.Combine(testPath, imageName));
            img.CompressionId = 1;
            img = img.Save();

            productModel.ImageList.Add(ProductImage.NewProductImage(img,
                    new ProductImageImageFacingType()
                    {
                        FacingType = ProductImageFacing.Front,
                        ImageType = ProductImageType.Unit
                    },
                    productModel.Id));
            productModel = productModel.Save();

            foreach (ProductImage pImage in productModel.ImageList)
            {
                ProductImageDto dalDto;
                using (IDalContext dalContext = base.DalFactory.CreateContext())
                {
                    using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
                    {
                        dalDto = dal.FetchById(pImage.Id);
                        AssertDtoAndModelAreEqual(dalDto, pImage);

                        Int32 dtoId = dalDto.Id;

                        dal.DeleteById(dtoId);

                        //GFS-21757 : Ensure deleted item is deleted correctly
                        Assert.Throws(typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dalDto = dal.FetchById(dtoId);
                        }, "Child MasterData objects are permanently deleted and so should throw an exception");
                    }
                }
            }
        }

        #endregion
    }
}
