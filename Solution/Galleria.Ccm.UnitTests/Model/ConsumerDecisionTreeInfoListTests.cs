﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeInfoListTests : TestBase
    {
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeInfoList model =
                Galleria.Ccm.Model.ConsumerDecisionTreeInfoList.FetchByEntityId(1);
            Serialize(model);
        }

        #region Factory Methods

        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);

            foreach (EntityDto entity in entityDtos)
            {
                List<ConsumerDecisionTreeDto> cdtDtoList =
                    TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entity.Id, 5);

                //fetch the info list
                Galleria.Ccm.Model.ConsumerDecisionTreeInfoList modelList =
                    Galleria.Ccm.Model.ConsumerDecisionTreeInfoList.FetchByEntityId(entity.Id);

                //check the infos
                foreach (ConsumerDecisionTreeDto dto in cdtDtoList)
                {
                    Galleria.Ccm.Model.ConsumerDecisionTreeInfo model =
                        modelList.FirstOrDefault(p => p.Id == dto.Id);

                    ConsumerDecisionTreeInfoTests.AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }

        #endregion

    }
}
