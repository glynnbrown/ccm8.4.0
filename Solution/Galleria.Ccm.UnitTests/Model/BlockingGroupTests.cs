﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Reflection;
using Csla.Core;
using System.Collections;
using Galleria.Framework.Planograms.Model;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class BlockingGroupTests : TestBase<BlockingGroup, BlockingGroupDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out BlockingGroup model)
        {
            Blocking blocking = Blocking.NewBlocking(1);
            blocking.Name = Guid.NewGuid().ToString();
            blocking.Description = "Test";
            blocking.ProductGroupId = 1;

            BlockingGroup group = NewModel();
            group.Name = Guid.NewGuid().ToString();


            blocking.Groups.Add(group);

            root = blocking;
            model = group;
        }


        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        [Test]
        public void Property_TotalAllocatedSpace_UpdatedOnLocationCombine()
        {
            Blocking item = Blocking.NewBlocking(1);
            item.Name = Guid.NewGuid().ToString();

            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            BlockingGroup group1 = item.Groups[0];

            Boolean propertyChangedFired = false;

            group1.PropertyChanged +=
                (s, e) =>
                {
                    if (e.PropertyName == BlockingGroup.TotalSpacePercentageProperty.Name)
                    {
                        propertyChangedFired = true;
                    }
                };
            
            //combine the locations
            item.Locations[1].CombineWith(item.Locations[0]);
            Assert.IsTrue(propertyChangedFired, "Property change not fired");
        }

        [Test]
        public void Property_TotalAllocatedSpace_UpdatedLocationAllocatedSpaceChanged()
        {
            Blocking item = Blocking.NewBlocking(1);
            item.Name = Guid.NewGuid().ToString();

            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            BlockingGroup group1 = item.Groups[0];

            Boolean propertyChangedFired = false;

            group1.PropertyChanged +=
                (s, e) =>
                {
                    if (e.PropertyName == BlockingGroup.TotalSpacePercentageProperty.Name)
                    {
                        propertyChangedFired = true;
                    }
                };

            //move the first divider
            item.Dividers[0].MoveTo(0.3F);
            Assert.IsTrue(propertyChangedFired, "Property change not fired");
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_NameColour()
        {
            TestNewFactoryMethod(new String[] { "Name", "Colour" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<Blocking>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<Blocking>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<Blocking>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<Blocking>();
        }

        #endregion

    }
}
