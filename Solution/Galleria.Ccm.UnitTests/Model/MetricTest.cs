﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class MetricTest : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Assert that property of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">the dto to compare</param>
        /// <param name="model">the model to compare</param>
        public static void AssertDtoAndModelAreEqual(MetricDto dto, Metric model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.Description, model.Description);
            Assert.AreEqual(dto.DataModelType, model.DataModelType);
            Assert.AreEqual(dto.Direction, model.Direction);
            Assert.AreEqual(dto.SpecialType, model.SpecialType);
            Assert.AreEqual(dto.DateCreated, model.DateCreated);
            Assert.AreEqual(dto.DateDeleted, model.DateDeleted);
            Assert.AreEqual(dto.DateLastModified, model.DateLastModified);
        }

        public static void AssertModelAndModelAreEqual(Metric m2, Metric model)
        {
            Assert.AreEqual(m2.Id, model.Id);
            Assert.AreEqual(m2.RowVersion, model.RowVersion);
            Assert.AreEqual(m2.EntityId, model.EntityId);
            Assert.AreEqual(m2.Name, model.Name);
            Assert.AreEqual(m2.Description, model.Description);
            Assert.AreEqual(m2.SpecialType, model.SpecialType);
            Assert.AreEqual(m2.DataModelType, model.DataModelType);
            Assert.AreEqual(m2.Direction, model.Direction);
            Assert.AreEqual(m2.DateCreated, model.DateCreated);
            Assert.AreEqual(m2.DateDeleted, model.DateDeleted);
            Assert.AreEqual(m2.DateLastModified, model.DateLastModified);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Metric model = Metric.NewMetric(1);
            Serialize(model);
        }
        #endregion

        #region Property Setters
        [Test]
        public void PropertySetters()
        {
            //model to set on
            Metric model = Metric.NewMetric(1);
            TestPropertySetters<Metric>(model);
        }
        #endregion

        #region Factory
        [Test]
        public void NewMetric()
        {
            Int32 entityId = 1;
            Metric newMetric = Metric.NewMetric(entityId);
            Assert.AreEqual(newMetric.EntityId, entityId);
            Assert.IsTrue(newMetric.IsNew);
        }
        #endregion

        #region DataAccess

        [Test]
        public void Data_Portal_InsertUpdateDelete()
        {
            List<EntityDto> entities = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entities[0].Id;

            //create a new item
            Metric item = Metric.NewMetric(entityId);
 
        }
        #endregion

    }
}
