﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using System.ComponentModel;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnSortTests : TestBase
    {
        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumnSort testModel = CustomColumnSort.NewCustomColumnSort(null, ListSortDirection.Ascending);

            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<CustomColumnSort>(CustomColumnSort.NewCustomColumnSort(null, ListSortDirection.Ascending));
        }

        #endregion

        [Test]
        public void NewCustomColumnSort()
        {
            TestDelegate code = () => CustomColumnSort.NewCustomColumnSort(null, ListSortDirection.Ascending);

            Assert.DoesNotThrow(code);
        }
    }
}
