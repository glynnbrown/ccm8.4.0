﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnListTests : TestBase
    {
        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumnList testModel = CustomColumnList.NewCustomColumnList();

            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        // Only property is the collection itself, which the test base can't test as CustomColumn does not have a parameterless factory method or constructor.

        [Test]
        public void NewCustomColumnList()
        {
            TestDelegate code = () => CustomColumnList.NewCustomColumnList();

            Assert.DoesNotThrow(code);
        }

        #endregion
    }
}