﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25587 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramGroupTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(PlanogramGroupDto dto, PlanogramGroup model)
        {
            Assert.AreEqual(9, typeof(PlanogramGroupDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
            //Assert.AreEqual(dto.PlanogramHierarchyId, model.PlanogramHierarchyId);
            //ParentGroupid
            //dto key
            //datecreated
            //datelastmodified
            //datedeleted
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
        }
        #endregion

        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramGroup.NewPlanogramGroup());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<PlanogramGroup>(PlanogramGroup.NewPlanogramGroup());
        }

        #endregion

        #region FactoryMethods

        [Test]
        public void NewPlanogramGroup()
        {
            PlanogramGroup newGroup = PlanogramGroup.NewPlanogramGroup();

            Assert.IsTrue(newGroup.IsNew);
            Assert.IsTrue(newGroup.IsChild);

            Assert.IsNotNull(newGroup.ChildList);
        }

        [Test]
        public void FetchPlanogramGroup()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<PlanogramHierarchyDto> hierarchyDtos = PlanogramHierarchyTests.InsertPlanogramHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<PlanogramGroupDto> groupDtos = PlanogramHierarchyTests.InsertPlanogramGroupDtos(base.DalFactory,hierarchyDtos[0].Id,  2, 2);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (PlanogramGroupDto dto in groupDtos)
                {
                    var model = PlanogramGroup.Fetch(dalContext, dto, groupDtos);
                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }

        #endregion

        #region Data Access


        [Test]
        public void DataAccess_Insert()
        {
            //create a new hierarchy (will have root group)
            PlanogramHierarchy hierarchy =
                PlanogramHierarchy.NewPlanogramHierarchy(1);
            hierarchy.Name = "h1";
            PlanogramGroup rootGroup = hierarchy.RootGroup;

            //set properties
            rootGroup.Name = "test";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //fetch back the level dto
            PlanogramGroupDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    dto = dal.FetchById(rootGroup.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, rootGroup);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create a new hierarchy (will have root group)
            PlanogramHierarchy hierarchy =
                PlanogramHierarchy.NewPlanogramHierarchy(1);
            hierarchy.Name = "h1";
            PlanogramGroup rootGroup = hierarchy.RootGroup;

            //set properties
            rootGroup.Name = "test";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //update
            rootGroup.Name = "test#";

            //save
            hierarchy = hierarchy.Save();
            rootGroup = hierarchy.RootGroup;

            //fetch back the level dto
            PlanogramGroupDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    dto = dal.FetchById(rootGroup.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, rootGroup);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create a new hierarchy (will have root group)
            PlanogramHierarchy hierarchy =
                PlanogramHierarchy.NewPlanogramHierarchy(1);
            hierarchy.Name = "h1";
            PlanogramGroup group = PlanogramGroup.NewPlanogramGroup();
            hierarchy.RootGroup.ChildList.Add(group);

            //set properties
            group.Name = "test";

            //save
            hierarchy = hierarchy.Save();
            group = hierarchy.RootGroup.ChildList[0];

            //fetch back the level dto
            PlanogramGroupDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    dto = dal.FetchById(group.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, group);

            //delete it
            Int32 itemId = group.Id;
            hierarchy.RootGroup.ChildList.Remove(group);
            hierarchy = hierarchy.Save();


            //try to retrieve again
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    dto = dal.FetchById(itemId);
                    Assert.IsNotNull(dto.DateDeleted);
                }
            }
            AssertDtoAndModelAreEqual(dto, group);
        }

        #endregion

        #region Helper Methods

        [Test]
        public void EnumerateAllChildGroups()
        {
            //create a new hierarchy (will have root group)
            PlanogramHierarchy hierarchy =
                PlanogramHierarchy.NewPlanogramHierarchy(1);
            hierarchy.Name = "h1";
            PlanogramGroup group1 = PlanogramGroup.NewPlanogramGroup();
            PlanogramGroup group2 = PlanogramGroup.NewPlanogramGroup();
            hierarchy.RootGroup.ChildList.Add(group1);
            hierarchy.RootGroup.ChildList.Add(group2);
            PlanogramGroup group3 = PlanogramGroup.NewPlanogramGroup();
            PlanogramGroup group4 = PlanogramGroup.NewPlanogramGroup();
            group1.ChildList.Add(group3);
            group2.ChildList.Add(group4);

            List<PlanogramGroup> groupList = hierarchy.RootGroup.EnumerateAllChildGroups().ToList();
            Assert.AreEqual(5, groupList.Count());

            Assert.Contains(hierarchy.RootGroup, groupList);
            Assert.Contains(group1, groupList);
            Assert.Contains(group2, groupList);
            Assert.Contains(group3, groupList);
            Assert.Contains(group4, groupList);
        }

        #endregion

    }
}
