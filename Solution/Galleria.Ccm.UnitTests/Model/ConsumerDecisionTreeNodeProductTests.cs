﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeNodeProductTests : TestBase
    {
        #region Test Helpers

        public static void AssertDtoAndModelAreEqual(ConsumerDecisionTreeNodeProductDto dto,
            Ccm.Model.ConsumerDecisionTreeNodeProduct model)
        {
            Assert.AreEqual(8, typeof(ConsumerDecisionTreeNodeProductDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            //public ConsumerDecisionTreeNodeProductDtoKey DtoKey
            //public Int32 ConsumerDecisionTreeNodeId { get; set; }
            Assert.AreEqual(dto.ProductId, model.ProductId);
            Assert.AreEqual(dto.ProductGtin, model.ProductGtin);
            //public DateTime DateCreated { get; set; }
            //public DateTime DateLastModified { get; set; }
            //public DateTime? DateDeleted { get; set; }
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(1);
            Serialize(model);
        }

        #endregion

        #region PropertySetters

        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(1);

            TestBase.TestPropertySetters<Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTreeNodeProduct()
        {
            Int32 productId = 1;

            Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(productId);

            Assert.IsTrue(model.IsNew);
            Assert.IsTrue(model.IsChild, "should be a child object");

            Assert.AreEqual(productId, model.ProductId);
        }

        [Test]
        public void FetchConsumerDecisionTreeNodeProduct()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<ProductDto> productDtos = TestDataHelper.InsertProductDtos(base.DalFactory, 25, entityId);
            List<ConsumerDecisionTreeDto> cdtDtos = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1);
            List<ConsumerDecisionTreeLevelDto> cdtLevelDtoList = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(this.DalFactory, cdtDtos[0], 1, true);
            List<ConsumerDecisionTreeNodeDto> cdtNodeDtos =
                TestDataHelper.InsertConsumerDecisionTreeNodeDtos(base.DalFactory, cdtDtos[0], cdtLevelDtoList, 5, true);
            List<ConsumerDecisionTreeNodeProductDto> cdtNodeProdDtos =
                TestDataHelper.InsertConsumerDecisionTreeNodeProductDtos(base.DalFactory, cdtNodeDtos, productDtos, 3);

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (ConsumerDecisionTreeNodeProductDto dto in cdtNodeProdDtos)
                {
                    Ccm.Model.ConsumerDecisionTreeNodeProduct model =
                        Ccm.Model.ConsumerDecisionTreeNodeProduct.FetchConsumerDecisionTreeNodeProduct(
                        dalContext, dto);

                    AssertDtoAndModelAreEqual(dto, model);
                }

            }

        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_ChildInsertUpdateDelete()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            Int32 productId = TestDataHelper.InsertProductDtos(base.DalFactory, 1, entityId)[0].Id;
            Int32 hierarchy = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId)[0].Id;
            List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchy, 1);
            Int32 groupId = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levels, 1)[0].Id;

            //create the parent cdt
            Galleria.Ccm.Model.ConsumerDecisionTree parentCdt =
                Galleria.Ccm.Model.ConsumerDecisionTree.NewConsumerDecisionTree(entityId);
            parentCdt.Name = "name";
            parentCdt.ProductGroupId = groupId;

            //create a new item
            Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct item =
               Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(productId);
            parentCdt.RootNode.Products.Add(item);

            //insert
            parentCdt = parentCdt.Save();
            item = parentCdt.RootNode.Products[0];

            ConsumerDecisionTreeNodeProductDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //update
            //nothing to test, fail if any writable properties get added.
            IEnumerable<PropertyInfo> writeableProperties =
                typeof(Galleria.Ccm.Model.ConsumerDecisionTreeNodeProduct)
                .GetProperties().Where(p => p.DeclaringType == typeof(Galleria.Ccm.Model.ConsumerDecisionTree) && p.CanWrite);

            Assert.AreEqual(0, writeableProperties.Count());


            //remove from the parent
            Int32 itemId = item.Id;
            parentCdt.RootNode.Products.Remove(item);
            parentCdt = parentCdt.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }

        }


        #endregion

    }
}
