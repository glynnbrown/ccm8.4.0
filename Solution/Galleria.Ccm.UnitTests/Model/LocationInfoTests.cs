﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (SA 1.0)
// V8-25628 : A.Kuszyk
//		Created (Copied from SA)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationInfoTests : TestBase
    {
        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(LocationDto dto, LocationInfo model)
        {
            Assert.AreEqual(15, typeof(LocationInfoDto).GetProperties().Count(), "Number of properties has changed");

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.LocationGroupId, model.LocationGroupId);
            Assert.AreEqual(dto.Code, model.Code);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.Region, model.Region);
            Assert.AreEqual(dto.County, model.County);
            Assert.AreEqual(dto.TVRegion, model.TVRegion);
            Assert.AreEqual(dto.Address1, model.Address1);
            Assert.AreEqual(dto.Address2, model.Address2);
            Assert.AreEqual(dto.PostalCode, model.PostalCode);
            Assert.AreEqual(dto.City, model.City);
            Assert.AreEqual(dto.Country, model.Country);
            Assert.AreEqual(dto.DateDeleted, model.DateDeleted);
        }

        private List<LocationDto> InsertTestDtos()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<LocationDto> returnList =
            TestDataHelper.InsertLocationDtos(base.DalFactory, 20, entityId);

            return returnList;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<LocationDto> locDtos = InsertTestDtos();
            LocationInfoList infoList = LocationInfoList.FetchByEntityId(locDtos.First().EntityId);

            LocationInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchLocationInfo()
        {
            List<LocationDto> dtoList = InsertTestDtos();
            LocationInfoList infoList = LocationInfoList.FetchByEntityId(1);

            foreach (LocationDto locationDto in dtoList)
            {
                LocationInfo dalLocInfo = infoList.First(l => l.Id == locationDto.Id);
                AssertDtoAndModelAreEqual(locationDto, dalLocInfo);
            }
        }

        #endregion
    }
}
