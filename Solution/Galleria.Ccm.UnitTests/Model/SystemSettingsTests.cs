﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24863 : L.Hodson
//		Created
#endregion

#region Version History: CCM801

// V8-28679 : A.Silva
//      Added Real Image related tests.

#endregion

#region Version History: CCM810

// V8-30282: A.Silva
//  Amended ProductImageAttribute_WhenInitialized_ShouldDefaultToPlanogramProductGtin Unit Test.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class SystemSettingsTest : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Insert some default data
            SystemSettings model =
                TestDataHelper.InsertDefaultSettings(base.DalFactory);
            Serialize(model);
        }
        #endregion

        #region Property Setters
        [Test]
        public void PropertySetters()
        {
            //Insert some default data
            SystemSettings model =
                TestDataHelper.InsertDefaultSettings(base.DalFactory);

            TestBase.TestPropertySetters<SystemSettings>(model);
        }
        #endregion

        #region Default Property Values

        [Test]
        public void SystemSettings_ShouldImplementIRealImageProviderSettings()
        {
            const String expectation = "SystemSettings should implement IRealImageProviderSettings so that it can be used with RealImageProviders.";
            Type expected = typeof(IRealImageProviderSettings);
            SystemSettings actual = SystemSettings.NewSystemSettings();

            Assert.IsInstanceOf(expected, actual, expectation);
        }

        [Test]
        public void ProductImageSource_WhenInitialized_ShouldDefaultToFolder()
        {
            const String expectation = "ProductImageSource should default to Folder.";
            const RealImageProviderType expected = RealImageProviderType.Folder;

            RealImageProviderType actual = SystemSettings.NewSystemSettings().ProductImageSource;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageLocation_WhenInitialized_ShouldDefaultToDocumentsImages()
        {
            const String expectation = "ProductImageLocation should default to a Images folder inside the default document folders";
            //  The default location of documents is defined by the type of install chosen.
            String defaultDocsFolderPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), 
                Constants.MyDocsAppFolderName);
            if (!Directory.Exists(defaultDocsFolderPath))
            {
                //  Try the user's documents as the default location failed.
                defaultDocsFolderPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Constants.MyDocsAppFolderName);
            }
            const String imagesFolderName = "Images";
            String expected = Path.Combine(defaultDocsFolderPath, imagesFolderName);
            if (!Directory.Exists(expected))
            {
                const String requirementNotMet = "Cannot test without a default documents folder to test against (expected: {0}).";
                Assert.Inconclusive(requirementNotMet, expected);
            }

            String actual = SystemSettings.NewSystemSettings().ProductImageLocation;

            Assert.AreEqual(expected, actual,expectation);
        }

        /// <summary>
        ///     When initializing a new instance of <see cref="SystemSettings" />,
        ///     <see cref="SystemSettings.ProductImageAttribute" /> should default to
        ///     <see cref="PlanogramProduct.GtinProperty" />.
        /// </summary>
        [Test]
        public void ProductImageAttribute_WhenInitialized_ShouldDefaultToPlanogramProductGtin()
        {
            const String expectation = "ProductImageAttribute should default to PlanogramProduct.Gtin";
            const String requisiteExpectedProperty = "Cannot test without the field for the expected property ({0})";
            String defaultProductAttribute = PlanogramProduct.GtinProperty.Name;
            ObjectFieldInfo objectFieldInfo = PlanogramProduct
                .EnumerateDisplayableFieldInfos(false, false, false)
                .FirstOrDefault(o => o.PropertyName == defaultProductAttribute);
            if (objectFieldInfo == null) Assert.Inconclusive(requisiteExpectedProperty, defaultProductAttribute);
            String expected = objectFieldInfo.FieldPlaceholder;

            String actual = SystemSettings.NewSystemSettings().ProductImageAttribute;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageIgnoreLeadingZeros_WhenInitialized_ShouldBeFalse()
        {
            const String expectation = "ProductImageIgnoreLeadingZeros should default to false.";

            Boolean actual = SystemSettings.NewSystemSettings().ProductImageIgnoreLeadingZeros;

            Assert.IsFalse(actual, expectation);
        }

        [Test]
        public void ProductImageMinFieldLength_WhenInitializes_ShouldDefaultToTen()
        {
            const String expectation = "ProductImageMinFieldLength should default to 10.";
            const Byte expected = 10;

            Byte actual = SystemSettings.NewSystemSettings().ProductImageMinFieldLength;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageFacingPostfixFront_WhenInitialized_ShouldDefaultToEnumValue()
        {
            const String expectation = "ProductImageFacingPostfixFront should default to the corresponding enum value.";
            String expected = ((Int32)PlanogramImageFacingType.Front).ToString();

            String actual = SystemSettings.NewSystemSettings().ProductImageFacingPostfixFront;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageFacingPostfixLeft_WhenInitialized_ShouldDefaultToEnumValue()
        {
            const String expectation = "ProductImageFacingPostfixLeft should default to the corresponding enum value.";
            String expected = ((Int32)PlanogramImageFacingType.Left).ToString();

            String actual = SystemSettings.NewSystemSettings().ProductImageFacingPostfixLeft;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageFacingPostfixTop_WhenInitialized_ShouldDefaultToEnumValue()
        {
            const String expectation = "ProductImageFacingPostfixTop should default to the corresponding enum value.";
            String expected = ((Int32)PlanogramImageFacingType.Top).ToString();

            String actual = SystemSettings.NewSystemSettings().ProductImageFacingPostfixTop;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageFacingPostfixBack_WhenInitialized_ShouldDefaultToEnumValue()
        {
            const String expectation = "ProductImageFacingPostfixBack should default to the corresponding enum value.";
            String expected = ((Int32)PlanogramImageFacingType.Back).ToString();

            String actual = SystemSettings.NewSystemSettings().ProductImageFacingPostfixBack;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageFacingPostfixRight_WhenInitialized_ShouldDefaultToEnumValue()
        {
            const String expectation = "ProductImageFacingPostfixRight should default to the corresponding enum value.";
            String expected = ((Int32)PlanogramImageFacingType.Right).ToString();

            String actual = SystemSettings.NewSystemSettings().ProductImageFacingPostfixRight;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageFacingPostfixBottom_WhenInitialized_ShouldDefaultToEnumValue()
        {
            const String expectation = "ProductImageFacingPostfixBottom should default to the corresponding enum value.";
            String expected = ((Int32)PlanogramImageFacingType.Bottom).ToString();

            String actual = SystemSettings.NewSystemSettings().ProductImageFacingPostfixBottom;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageCompression_WhenInitialized_ShouldDefaultToNone()
        {
            const String expectation = "ProductImageCompresion should default to None.";
            const RealImageCompressionType expected = RealImageCompressionType.None;

            RealImageCompressionType actual = SystemSettings.NewSystemSettings().ProductImageCompression;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageColourDepth_WhenInitialized_ShouldDefaultToBit8()
        {
            const String expectation = "ProductImageColourDepthn should default to Bit8.";
            const RealImageColourDepthType expected = RealImageColourDepthType.Bit8;

            RealImageColourDepthType actual = SystemSettings.NewSystemSettings().ProductImageColourDepth;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void ProductImageTransparency_WhenInitialized_ShouldDefaultToFalse()
        {
            const String expectation = "ProductImageTransparency should default to False.";

            Boolean actual = SystemSettings.NewSystemSettings().ProductImageTransparency;

            Assert.IsFalse(actual, expectation);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewSystemSettings()
        {
            //Create new SystemSettings object
            SystemSettings model =
                SystemSettings.NewSystemSettings();

            Assert.IsTrue(model.IsNew);
            Assert.IsTrue(model.IsChild);

            AssertHelper.AssertChildModelObjectsAreNotNull<SystemSettings>(
                model,
                new List<String> { "ParentEntity" });
        }

        [Test]
        public void FetchSystemSettings()
        {
            List<EntityDto> entities = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entities[0].Id;

            //Insert some database info data
            SystemSettings modelSystemSettings =
                TestDataHelper.InsertDefaultSettings(base.DalFactory);
            //Fetch the model
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                SystemSettings dalSystemSettings =
                    SystemSettings.FetchSystemSettings(dalContext, entityId);
                Assert.IsNotNull(dalSystemSettings, "Dal Object must exist after Fetch");
            }
        }

        #endregion
    }
}