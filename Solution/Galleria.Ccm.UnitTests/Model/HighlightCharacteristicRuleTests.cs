﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 : L.Hodson
//		Created
// V8-25436 : L.Luong
//      Changed to match new design
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class HighlightCharacteristicRuleTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(HighlightCharacteristicRule.NewHighlightCharacteristicRule());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<HighlightCharacteristicRule>(HighlightCharacteristicRule.NewHighlightCharacteristicRule());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void CreateNew()
        {
            TestCreateNew<HighlightCharacteristicRule>(true);
        }

        [Test]
        public void Fetch()
        {
            //insert a dto to fetch
            HighlightDto highlightDto = new HighlightDto();
            highlightDto.Name = "h1";

            HighlightCharacteristicDto characteristicDto = new HighlightCharacteristicDto();
            PopulatePropertyValues1<HighlightCharacteristicDto>(characteristicDto, new List<String> { "Id", "HighlightId" });

            HighlightCharacteristicRuleDto dto = new HighlightCharacteristicRuleDto();
            PopulatePropertyValues1<HighlightCharacteristicRuleDto>(dto, new List<String> { "Id", "HighlightCharacteristicId" });

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dal.Insert(highlightDto);
                }
                using (IHighlightCharacteristicDal dal = dalContext.GetDal<IHighlightCharacteristicDal>())
                {
                    characteristicDto.HighlightId = highlightDto.Id;
                    dal.Insert(characteristicDto);
                }
                using (IHighlightCharacteristicRuleDal dal = dalContext.GetDal<IHighlightCharacteristicRuleDal>())
                {
                    dto.HighlightCharacteristicId = characteristicDto.Id;
                    dal.Insert(dto);
                }
            }

            //fetch
            Highlight highlight = Highlight.FetchById(highlightDto.Id, HighlightDalFactoryType.Unknown);
            HighlightCharacteristicRule model = highlight.Characteristics[0].Rules[0];
            AssertDtoAndModelAreEqual<HighlightCharacteristicRuleDto, HighlightCharacteristicRule>(dto, model);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create highlight
            Highlight highlight = Highlight.NewHighlight();
            highlight.Name = "h1";

            //insert a characteristic child to highlight
            HighlightCharacteristic characteristic = HighlightCharacteristic.NewHighlightCharacteristic();
            characteristic.Name = "c1";
            highlight.Characteristics.Add(characteristic);

            //insert a characteristic rule child to characteristic
            HighlightCharacteristicRule model = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            PopulatePropertyValues1<HighlightCharacteristicRule>(model, new List<String> { "Id", "HighlightId" });
            highlight.Characteristics[0].Rules.Add(model);

            //save
            highlight = highlight.Save();
            HighlightDto dto;
            model = highlight.Characteristics[0].Rules[0];

            //fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            AssertModelsAreEqual<HighlightCharacteristicRule>(model, highlight.Characteristics[0].Rules[0]);

        }

        [Test]
        public void DataAccess_Update()
        {
            //create highlight
            Highlight highlight = Highlight.NewHighlight();
            highlight.Name = "h1";

            //insert a characteristic child to highlight
            HighlightCharacteristic characteristic = HighlightCharacteristic.NewHighlightCharacteristic();
            characteristic.Name = "c1";
            highlight.Characteristics.Add(characteristic);

            //insert a characteristic rule child to characteristic
            HighlightCharacteristicRule model = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            PopulatePropertyValues1<HighlightCharacteristicRule>(model, new List<String> { "Id", "HighlightId" });
            highlight.Characteristics[0].Rules.Add(model);

            //save
            highlight = highlight.Save();
            HighlightDto dto;
            model = highlight.Characteristics[0].Rules[0];

            //change characteristic rule property
            PopulatePropertyValues2<HighlightCharacteristicRule>(model, new List<String> { "Id", "HighlightId" });

            //save again
            highlight = highlight.Save();
            model = highlight.Characteristics[0].Rules[0];

            //fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            AssertModelsAreEqual<HighlightCharacteristicRule>(model, highlight.Characteristics[0].Rules[0]);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create highlight
            Highlight highlight = Highlight.NewHighlight();
            highlight.Name = "h1";

            //insert a characteristic child to highlight
            HighlightCharacteristic characteristic = HighlightCharacteristic.NewHighlightCharacteristic();
            characteristic.Name = "c1";
            highlight.Characteristics.Add(characteristic);

            //insert a characteristic rule child to characteristic
            HighlightCharacteristicRule model = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            PopulatePropertyValues1<HighlightCharacteristicRule>(model, new List<String> { "Id", "HighlightId" });
            highlight.Characteristics[0].Rules.Add(model);

            //save
            highlight = highlight.Save();
            HighlightDto dto;
            model = highlight.Characteristics[0].Rules[0];

            //fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            model = highlight.Characteristics[0].Rules[0];

            //delete
            highlight.Characteristics[0].Rules.Remove(model);
            highlight = highlight.Save();

            //refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dto = dal.FetchById(highlight.Id);
                }
            }
            Assert.AreEqual(0, highlight.Characteristics[0].Rules.Count);
        }

        #endregion
    }
}
