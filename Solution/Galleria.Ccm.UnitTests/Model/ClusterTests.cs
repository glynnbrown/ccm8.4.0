﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629: A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;
using System.Collections.Generic;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ClusterTests : TestBase
    {
        #region Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(ClusterDto dto, Galleria.Ccm.Model.Cluster model)
        {
            Assert.AreEqual(4, typeof(ClusterDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            //dtokey
            Assert.AreEqual(dto.Name, model.Name);
            //dto.ClusterSchemeId - not in model
            //dto.DateCreated - not in model
            //dto.DateLastModified - not in model
            //dto.DateDeleted - not in model
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.Cluster model =
                Galleria.Ccm.Model.Cluster.NewCluster();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model
            Galleria.Ccm.Model.Cluster model = Galleria.Ccm.Model.Cluster.NewCluster();

            TestPropertySetters<Galleria.Ccm.Model.Cluster>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewCluster()
        {
            // create a new model
            Galleria.Ccm.Model.Cluster model =
                Galleria.Ccm.Model.Cluster.NewCluster();

            Assert.IsNullOrEmpty(model.Name, "Should not have a default name");
            Assert.IsNotNull(model.Locations, "Locations list should have been intialized");
            Assert.IsTrue(model.IsChild);
        }

        [Test]
        public void FetchCluster()
        {
            //insert entities and locations
            Galleria.Ccm.Model.Entity entity = Galleria.Ccm.Model.Entity.NewEntity();
            entity.Name = "ent";
            entity = entity.Save();
            Int32 entityId = entity.Id;
            TestDataHelper.PopulateLocationHierarchy(base.DalFactory, entityId);

            Galleria.Ccm.Model.ClusterScheme parentScheme =
                Galleria.Ccm.Model.ClusterScheme.NewClusterScheme(entityId);
            TestDataHelper.SetPropertyValues(parentScheme);

            //create a new item
            Galleria.Ccm.Model.Cluster item =
                Galleria.Ccm.Model.Cluster.NewCluster();
            TestDataHelper.SetPropertyValues(item);
            parentScheme.Clusters.Add(item);

            //insert
            parentScheme = parentScheme.Save();
            item = parentScheme.Clusters[0];

            //refetch
            parentScheme = Galleria.Ccm.Model.ClusterScheme.FetchById(parentScheme.Id);
            item = parentScheme.Clusters[0];

            //get the stored dto and check
            ClusterDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// NameRequired
        /// </summary>
        [Test]
        public void NameRequired()
        {
            // create a new model
            Galleria.Ccm.Model.Cluster model = Galleria.Ccm.Model.Cluster.NewCluster();

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = string.Empty;
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        /// <summary>
        /// NamePropertyMaxLength
        /// </summary>
        [Test]
        public void NamePropertyMaxLength()
        {
            // create a new model
            Galleria.Ccm.Model.Cluster model = Galleria.Ccm.Model.Cluster.NewCluster();

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);

            // set an invalid name
            model.Name = new String('*', 101);
            Assert.AreEqual(model.IsValid, false);

            // set a valid name
            model.Name = "Valid name";
            Assert.AreEqual(model.IsValid, true);
        }

        #endregion

        #region DataAccess

        [Test]
        public void DataPortal_ChildInsertUpdateDelete()
        {
            //insert entities and locations
            Galleria.Ccm.Model.Entity entity = Galleria.Ccm.Model.Entity.NewEntity();
            entity.Name = "ent";
            entity = entity.Save();
            Int32 entityId = entity.Id;
            TestDataHelper.PopulateLocationHierarchy(base.DalFactory, entityId);

            Galleria.Ccm.Model.ClusterScheme parentScheme =
                Galleria.Ccm.Model.ClusterScheme.NewClusterScheme(entityId);
            parentScheme.Name = "scheme";

            //create a new item
            Galleria.Ccm.Model.Cluster item =
                Galleria.Ccm.Model.Cluster.NewCluster();
            item.Name = "testCluster";
            parentScheme.Clusters.Add(item);

            //insert
            parentScheme = parentScheme.Save();
            item = parentScheme.Clusters[0];

            ClusterDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);

            //update
            TestDataHelper.SetPropertyValues(item);
            parentScheme = parentScheme.Save();
            item = parentScheme.Clusters[0];

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);


            //update again to check concurrency
            TestDataHelper.SetPropertyValues(item);
            parentScheme = parentScheme.Save();
            item = parentScheme.Clusters[0];

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    dto = dal.FetchById(item.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, item);


            //remove from the parent
            Int32 itemId = item.Id;
            parentScheme.Clusters.Remove(item);
            parentScheme = parentScheme.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }

        }



        #endregion
    }
}
