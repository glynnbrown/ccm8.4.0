﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using System.IO;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using System.Diagnostics;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ImageTests : TestBase
    {
        #region Fields
        private String _testPath;
        private Int32 _imageWidth = 300;
        private Int32 _imageHeight = 100;
        private List<EntityDto> _entityList;
        private Int32 _entityId;
        #endregion  

        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(ImageDto dto, Image model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.OriginalFileId, model.OriginalFileId);
            Assert.AreEqual(dto.BlobId, model.BlobID);
            Assert.AreEqual(dto.BlobId, model.ImageBlob.Id);
            Assert.AreEqual(dto.CompressionId, model.CompressionId);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Height, model.Height);
            Assert.AreEqual(dto.SizeInBytes, model.SizeInBytes);
            Assert.AreEqual(dto.Width, model.Width);
        }
        #endregion

        #region Setup/Breakdown
        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public void ImageSetup()
        {
            const String cImageModelTest = "ImageModelTest";
            System.Drawing.Bitmap newImage = new System.Drawing.Bitmap(_imageWidth, _imageHeight);
            String appPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            _testPath = Path.Combine(appPath, cImageModelTest);
            Directory.CreateDirectory(_testPath);
            newImage.Save(Path.Combine(_testPath, "1.bmp"), System.Drawing.Imaging.ImageFormat.Bmp);
            _entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 4);
            _entityId = _entityList.FirstOrDefault().Id;
        }

        /// <summary>
        /// Called when a test is completed
        /// </summary>
        [TearDown]
        public void ImageTeardown()
        {
            Directory.Delete(_testPath, true);
        }
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Image model = Image.NewImage(_entityId, Path.Combine(_testPath, "1.bmp"));
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySettersGeneric()
        {
            // create a new model for rand values
            Image model = Image.NewImage(_entityId, Path.Combine(_testPath, "1.bmp"));

            TestBase.TestPropertySetters<Image>(model, new List<String> { "OriginalFile" });
        }

        [Test]
        public void PropertySetters()
        {
            //create a new model
            Image model = Image.NewImage(_entityId, Path.Combine(_testPath, "1.bmp"));

            Galleria.Ccm.Model.File newFile = 
                Galleria.Ccm.Model.File.NewFile(_entityId, Path.Combine(_testPath, "1.bmp"), null).Save();
            model.OriginalFile = newFile;
            Assert.AreEqual(newFile.Id, model.OriginalFileId);
            Assert.AreEqual(newFile, model.OriginalFile);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewImage()
        {
            const Int32 targetWidth = 30, targetHeight = 10;
            Galleria.Ccm.Model.Entity a = Galleria.Ccm.Model.Entity.FetchById(1);
            Galleria.Ccm.Model.Compression comp = Ccm.Model.Compression.NewCompression();
            comp.Width = targetWidth;
            comp.Height = targetHeight;
            comp.ColourDepth = Galleria.Ccm.Model.ImagePixelFormat.pf16bit;
            comp.MaintainAspectRatio = false;

            Galleria.Ccm.Model.Image newItem =
                Galleria.Ccm.Model.Image.NewImage(_entityId, Path.Combine(_testPath, "1.bmp"), comp);

            Assert.IsTrue(newItem.IsNew);
            Assert.AreEqual(targetWidth, newItem.Width);
            Assert.AreEqual(targetHeight, newItem.Height);

            Galleria.Ccm.Model.Image entityTest =
                Galleria.Ccm.Model.Image.NewImage(2, Path.Combine(_testPath, "1.bmp"), comp);

            Assert.AreEqual(2, entityTest.EntityId);
            Assert.IsTrue(entityTest.IsNew);
            Assert.AreEqual(targetWidth, entityTest.Width);
            Assert.AreEqual(targetHeight, entityTest.Height);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            List<Galleria.Ccm.Model.Image> newItemList = new List<Ccm.Model.Image>();
            foreach (EntityDto entity in _entityList)
            {
                //create a different number of images per entity
                for (Int32 x = 0; x < entity.Id; x++)
                {
                    newItemList.Add(Galleria.Ccm.Model.Image.NewImage(entity.Id, Path.Combine(_testPath, "1.bmp"), null).Save());
                }
            }

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IImageDal dal = dalContext.GetDal<IImageDal>())
                {
                    foreach (Ccm.Model.Image image in newItemList)
                    {
                        ImageDto dto = dal.FetchById(image.Id);
                        AssertDtoAndModelAreEqual(dto, image);
                    }

                    //IEnumerable<IGrouping<Int32, Ccm.Model.Image>> imageEntityGroup = newItemList.GroupBy(i => i.EntityId);

                    //foreach (IGrouping<Int32, Ccm.Model.Image> entityGroup in imageEntityGroup)
                    //{
                    //    IEnumerable<ImageDto> returnedDtos = dal.FetchByEntityId(entityGroup.Key);
                    //    Assert.AreEqual(entityGroup.Count(), returnedDtos.Count(), "Count should be the same");
                    //}
                }
            }


        }

        [Test]
        public void DataAccess_Update()
        {
            List<CompressionDto> compressionDtoList = TestDataHelper.InsertCompressionDtos(base.DalFactory, 1);

            Galleria.Ccm.Model.Image newItem =
               Galleria.Ccm.Model.Image.NewImage(_entityId, Path.Combine(_testPath, "1.bmp"));
            newItem.CompressionId = compressionDtoList[0].Id;
            newItem = newItem.Save();

            ImageDto newDto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IImageDal dal = dalContext.GetDal<IImageDal>())
                {
                    Debug.WriteLine(newItem.Id);
                    newDto = dal.FetchById(newItem.Id);
                }
            }
            AssertDtoAndModelAreEqual(newDto, newItem);

            Galleria.Ccm.Model.File newFile = Galleria.Ccm.Model.File.NewFile(_entityId, Path.Combine(_testPath, "1.bmp"), null).Save();
            newItem.OriginalFile = newFile;
            Galleria.Ccm.Model.Image updateItem = newItem.Save();

            ImageDto updateDto;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IImageDal dal = dalContext.GetDal<IImageDal>())
                {
                    Debug.WriteLine(updateItem.Id);
                    updateDto = dal.FetchById(updateItem.Id);
                }
            }
            AssertDtoAndModelAreEqual(updateDto, updateItem);

            Assert.AreNotEqual(newDto.OriginalFileId, updateDto.OriginalFileId);
        }

        #endregion
    }
}
