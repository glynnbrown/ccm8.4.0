﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationSpaceBay : TestBase
    {
        #region Test Helper Methods
        private void AssertDtoAndModelAreEqual(LocationSpaceBayDto dto, Galleria.Ccm.Model.LocationSpaceBay model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.LocationSpaceProductGroupId, model.Parent.Id);
            Assert.AreEqual(dto.Order, model.Order);
            Assert.AreEqual(dto.Height, model.Height);
            Assert.AreEqual(dto.Width, model.Width);
            Assert.AreEqual(dto.Depth, model.Depth);
            Assert.AreEqual(dto.BaseHeight, model.BaseHeight);
            Assert.AreEqual(dto.BaseWidth, model.BaseWidth);
            Assert.AreEqual(dto.BaseDepth, model.BaseDepth);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.LocationSpaceBay model =
                Galleria.Ccm.Model.LocationSpaceBay.NewLocationSpaceBay();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model for rand values
            Galleria.Ccm.Model.LocationSpaceBay valueModel =
                Galleria.Ccm.Model.LocationSpaceBay.NewLocationSpaceBay();

            //Test Id
            Int32 testId = 1;
            valueModel.Id = testId;
            Assert.AreEqual(testId, valueModel.Id);

            //Test Order
            Byte testOrder = 1;
            valueModel.Order = testOrder;
            Assert.AreEqual(testOrder, valueModel.Order);

            //Test Height
            Single testHeight = 1;
            valueModel.Height = testHeight;
            Assert.AreEqual(testHeight, valueModel.Height);

            //Test Width
            Single testWidth = 1;
            valueModel.Width = testWidth;
            Assert.AreEqual(testWidth, valueModel.Width);

            //Test Depth
            Single testDepth = 1;
            valueModel.Depth = testDepth;
            Assert.AreEqual(testDepth, valueModel.Depth);

            //Test Base Height
            Single testBaseHeight = 1;
            valueModel.BaseHeight = testBaseHeight;
            Assert.AreEqual(testBaseHeight, valueModel.BaseHeight);

            //Test Base Width
            Single testBaseWidth = 1;
            valueModel.BaseWidth = testBaseWidth;
            Assert.AreEqual(testBaseWidth, valueModel.BaseWidth);

            //Test Base Depth
            Single testBaseDepth = 1;
            valueModel.BaseDepth = testBaseDepth;
            Assert.AreEqual(testBaseDepth, valueModel.BaseDepth);

        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationSpaceBay()
        {
            TestCreateNew<Ccm.Model.LocationSpaceBay>(
                /*child*/true,
                null,
                new Object[] { true },
                new String[] { "ParentLocationSpaceProductGroup", "Parent" });
        }

        #endregion

        #region Data Access

        #region Insert
        [Test]
        public void DataAccess_Insert()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Select default bay
            Ccm.Model.LocationSpaceBay modelBay = modelProductGroup.Bays.First();

            //Set indentifiable property value
            Byte randomOrder = Convert.ToByte(random.Next(1, 255));
            modelBay.Order = randomOrder;

            //Call save on the parent object
            model = model.Save();

            //Get bay we want to check has been inserted
            Ccm.Model.LocationSpaceBay testModel = model.LocationSpaceProductGroups.First().Bays.First();

            //Fetch saved Bay
            LocationSpaceBayDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);
        }

        #endregion

        #region Update

        [Test]
        public void DataAccess_Update()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Select default bay
            Ccm.Model.LocationSpaceBay modelBay = modelProductGroup.Bays.First();

            //Set indentifiable property value
            Byte randomOrder = Convert.ToByte(random.Next(1, 255));
            modelBay.Order = randomOrder;

            //Call save on the parent object
            model = model.Save();

            //Get bay we want to check has been inserted
            Ccm.Model.LocationSpaceBay testModel = model.LocationSpaceProductGroups.First().Bays.First();

            //Set indentifiable property value
            Byte updateOrderValue = Convert.ToByte(random.Next(1, 255));
            testModel.Order = updateOrderValue;

            //Call save on the parent object again
            model = model.Save();

            //Reselect the bay from the parent object
            testModel = model.LocationSpaceProductGroups.First().Bays.First();

            //Fetch saved Bay
            LocationSpaceBayDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);
        }

        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            Random random = new Random();

            //Create Test data
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, 1);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, 1, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

            //Setup parent model object
            Galleria.Ccm.Model.LocationSpace model = Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            model.LocationId = locationDtoList.First().Id;

            //Insert group
            Ccm.Model.LocationSpaceProductGroup modelProductGroup = Ccm.Model.LocationSpaceProductGroup.NewLocationSpaceProductGroup();
            model.LocationSpaceProductGroups.Add(modelProductGroup);

            //Select insert group
            modelProductGroup = model.LocationSpaceProductGroups.First();
            modelProductGroup.ProductGroupId = hierarchy1GroupDtos.Last().Id;

            //Select default bay
            Ccm.Model.LocationSpaceBay modelBay = modelProductGroup.Bays.First();

            //Call save on the parent object
            model = model.Save();

            //Get bay we want to check has been inserted
            Ccm.Model.LocationSpaceBay testModel = model.LocationSpaceProductGroups.First().Bays.FirstOrDefault();
            Assert.IsNotNull(testModel, "Bay should have been inserted, and returned");

            //Fetch saved Bay to ensure it exists in the database
            LocationSpaceBayDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                {
                    dto = dal.FetchById(testModel.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, testModel);

            //Delete bay from parent object
            model.LocationSpaceProductGroups.First().Bays.Remove(testModel);

            //Call save on the parent object
            model = model.Save();

            //try to retrieve again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(testModel.Id);
                        });
                }
            }

        }
        #endregion

        #endregion

    }
}
