﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class LocationHierarchyTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(LocationHierarchy.NewLocationHierarchy(1));
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<LocationHierarchy>(LocationHierarchy.NewLocationHierarchy(1));
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationHierarchy()
        {
            LocationHierarchy newHierarchy =
               LocationHierarchy.NewLocationHierarchy(1);

            Assert.IsTrue(newHierarchy.IsNew);
            Assert.IsFalse(newHierarchy.IsChild);

            Assert.IsNotNull(newHierarchy.RootLevel);
            Assert.IsNotNull(newHierarchy.RootGroup);

        }

        [Test]
        public void FetchById()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 1);
            List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 2);

            //retrieve by id
            LocationHierarchy model =
                LocationHierarchy.FetchById(hierarchyDtos[0].Id);

            AssertDtoAndModelAreEqual(hierarchyDtos[0], model);
            Assert.AreEqual(levelDtos.Count, model.EnumerateAllLevels().Count());
            Assert.AreEqual(groupDtos.Count, model.EnumerateAllGroups().Count());
        }


        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);

            foreach (EntityDto entity in entityDtos)
            {
                List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, entity.Id);
                List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 1);
                List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 2);

                var model = LocationHierarchy.FetchByEntityId(entity.Id);
                AssertDtoAndModelAreEqual(hierarchyDtos[0], model);
                Assert.AreEqual(levelDtos.Count, model.EnumerateAllLevels().Count());
                Assert.AreEqual(groupDtos.Count, model.EnumerateAllGroups().Count());
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create a new hierarchy
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(1);

            //set properties
            hierarchy.Name = "testHierarchy";

            //save
            hierarchy = hierarchy.Save();

            //fetch back the hierarchy dto
            LocationHierarchyDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    dto = dal.FetchById(hierarchy.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, hierarchy);


            //remember to ensure all level id references have been updated
        }

        [Test]
        public void DataAccess_Update()
        {
            //create a new hierarchy
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(1);

            //set properties
            hierarchy.Name = "testHierarchy";

            //save
            hierarchy = hierarchy.Save();

            //change a property
            hierarchy.Name = "updatedHierarchy";

            //save
            hierarchy = hierarchy.Save();


            //fetch back the hierarchy dto
            LocationHierarchyDto dto;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    dto = dal.FetchById(hierarchy.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, hierarchy);


            //remember to ensure all level id references have been updated
        }

        //cant delete

        #endregion

        #region Methods

        [Test]
        public void FetchAllLevels()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 1);
            List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 2);

            //retrieve by id
            LocationHierarchy model =
                LocationHierarchy.FetchById(hierarchyDtos[0].Id);

            List<LocationLevel> methodResults = model.EnumerateAllLevels().ToList();

            foreach (LocationLevelDto dto in levelDtos)
            {
                var item = methodResults.First(l => l.Id.Equals(dto.Id));
                LocationLevelTests.AssertDtoAndModelAreEqual(dto, item);
            }
        }

        [Test]
        public void FetchAllGroups()
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, entityDtos[0].Id);
            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 1);
            List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 2);

            //retrieve by id
            LocationHierarchy model =
                LocationHierarchy.FetchById(hierarchyDtos[0].Id);

            List<LocationGroup> methodResults = model.EnumerateAllGroups().ToList();

            foreach (LocationGroupDto dto in groupDtos)
            {
                var item = methodResults.First(l => l.Id == dto.Id);
                LocationGroupTests.AssertDtoAndModelAreEqual(dto, item);
            }
        }

        #endregion
    }
}
