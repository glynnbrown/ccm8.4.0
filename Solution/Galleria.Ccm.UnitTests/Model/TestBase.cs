﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Linq;
using System.Reflection;

using NUnit.Framework;
using Galleria.Framework.Dal;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.UnitTesting.TestBases;
using Galleria.Framework.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    public abstract class TestBase
    {
        #region Fields
        private String _originalDalName; // stores the original dal name
        private String _dalName; // the dal name
        private IDalFactory _dalFactory; // the dal factory to use for testing
        private IDalFactory _userDalFactory;

        #endregion

        #region Properties
        /// <summary>
        /// The dal factory to use for testing
        /// </summary>
        protected IDalFactory DalFactory
        {
            get { return _dalFactory; }
        }

        protected IDalFactory UserDalFactory
        {
            get { return _userDalFactory; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public void Setup()
        {
            DalContainer.Reset();

            // store the original dal name
            _originalDalName = DalContainer.DalName;

            // generate a new dal name
            _dalName = Guid.NewGuid().ToString();

            // create a new, in memory dal factory
            _dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();
            DalContainer.RegisterFactory(_dalName, _dalFactory);
            _dalFactory.CreateDatabase();

            //User
            String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                   Galleria.Ccm.Constants.AppDataFolderName, "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (string file in Directory.GetFiles(templateDir))
                {
                    //if (!Path.GetFileName(file).StartsWith(templateName))
                    //{
                    try
                    {
                        File.Delete(file);
                    }
                    catch
                    {
                        // No point in crying over spilt milk
                    }
                    //}
                }
            }





            String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
            var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
            userFactory.UnitTestFolder = templateDir;
            _userDalFactory = userFactory;
            DalContainer.RegisterFactory(Constants.UserDal, _userDalFactory);


            //authenticate
            DomainPrincipal.Authenticate();
        }

        /// <summary>
        /// Called when a test is completed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            //Remove the User DalFactory
            DalContainer.RemoveFactory(Constants.UserDal);
            _userDalFactory.Dispose();

            //Delete the user file
            String factoryFilePath = Path.Combine(this.ExecutingAssemblyEnvironmentsPath(), Galleria.Ccm.Constants.AppDataFolderName);
            if (Directory.Exists(factoryFilePath))
            {
                Directory.Delete(factoryFilePath, true);
            }

            // remove the registered dal
            DalContainer.RemoveFactory(_dalName);

            // and restore the original dal name
            DalContainer.DalName = _originalDalName;
        }

        /// <summary>
        /// Gets the root path where our DBMaintenance tests will store template and test databases
        /// </summary>
        /// <returns></returns>
        protected String ExecutingAssemblyEnvironmentsPath()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

        /// <summary>
        /// Creates a collection of model objects of a particular type and sets all settable properties on each one
        /// except references to IDs of other model objects.  The number of model objects returned will be one more 
        /// than the number of properties set so that each one can be toggled separately, yielding a set of objects
        /// that, if successfully saved and reloaded, proves that the save and load code is working correctly for each
        /// property.
        /// </summary>
        /// <typeparam name="T">The type of model object to create.</typeparam>
        /// <param name="ids">A dictionary mapping Guid ids of objects to the integer ids they'll get when they're
        /// saved using the Mock Dal.  This method adds the ids for any objects it creates to this dictionary, based on
        /// the value of id.</param>
        /// <param name="id">The number of model objects of type T that have been created already.</param>
        /// <returns></returns>
        protected List<T> CreateModelObjects<T>(Dictionary<Object, Object> ids, ref Int32 id)
        {
            List<T> returnValue = new List<T>();
            Type type = typeof(T);

            List<PropertyInfo> properties = new List<PropertyInfo>();
            properties.AddRange(type.GetProperties().Where(p => p.CanWrite && p.PropertyType != typeof(Object)));
            //properties.Add(type.GetProperty("ExtendedData", BindingFlags.Public | BindingFlags.Instance));

            PropertyInfo idProperty = type.GetProperty("Id");

            for (Int32 i = 0; i <= properties.Count; i++)
            {
                T t = (T)type.GetMethod(String.Format("New{0}", type.Name)).Invoke(null, null);
                id++;
                ids[idProperty.GetValue(t, null)] = id;
                for (Int32 j = 0; j < properties.Count; j++)
                {
                    if (i == j)
                    {
                        properties[j].SetValue(t, TestDataHelper.GetValue2(properties[j].PropertyType), null);
                    }
                    else
                    {
                        properties[j].SetValue(t, TestDataHelper.GetValue2(properties[j].PropertyType), null);
                    }
                }
                returnValue.Add(t);
            }

            return returnValue;
        }

        /// <summary>
        /// Compares two collections of model objects of a certain type.
        /// </summary>
        /// <typeparam name="T">The type of model objects we're comparing.</typeparam>
        /// <param name="collection1">A collection of model objects that haven't been saved yet.</param>
        /// <param name="collection2">A collection of model objects that have been saved.</param>
        /// <param name="ids">A dictionary mapping Guid ids of unsaved model objects to integer ids of saved model
        /// objects.  This will be used to determine whether properties that represent ids of linked objects have 
        /// "matching" values.</param>
        protected void CompareModelObjects<T>(
            IEnumerable<T> collection1,
            IEnumerable<T> collection2,
            Dictionary<Object, Object> ids)
        {
            List<T> list1 = new List<T>(collection1);
            List<T> list2 = new List<T>(collection2);
            Assert.AreEqual(list1.Count, list2.Count);
            for (Int32 i = 0; i < list1.Count; i++)
            {
                CompareModelObjects<T>(list1[i], list2[i], ids);
            }
        }

        /// <summary>
        /// Compares two model objects of a certain type.
        /// </summary>
        /// <typeparam name="T">The type of model objects we're comparing.</typeparam>
        /// <param name="collection1">A model object that hasn't been saved yet.</param>
        /// <param name="collection2">A model object that has been saved.</param>
        /// <param name="ids">A dictionary mapping Guid ids of unsaved model objects to integer ids of saved model
        /// objects.  This will be used to determine whether properties that represent ids of linked objects have 
        /// "matching" values.</param>
        protected void CompareModelObjects<T>(T object1, T object2, Dictionary<Object, Object> ids)
        {
            Type type = typeof(T);

            List<PropertyInfo> properties = new List<PropertyInfo>();
            properties.AddRange(type.GetProperties().Where(p => p.CanWrite));
            //properties.Add(type.GetProperty("ExtendedData", BindingFlags.Public | BindingFlags.Instance));

            foreach (PropertyInfo property in properties)
            {
                if ((property.Name.EndsWith("Id") || property.Name.Contains("ImageId")) &&
                    property.PropertyType == typeof(Object) &&
                    property.Name != "Mesh3DId")
                {
                    Assert.AreEqual(ids[property.GetValue(object1, null)], property.GetValue(object2, null), property.Name);
                }
                else
                {
                    Assert.AreEqual(property.GetValue(object1, null), property.GetValue(object2, null), property.Name);
                }
            }
        }

        #endregion

        #region Test Helpers

        /// <summary>
        /// Tests that a dto is serializable
        /// </summary>
        protected static void Serialize(object obj)
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
            }
        }

        protected static void TestPropertySetters<T>(T modelObject)
        {
            TestPropertySetters<T>(modelObject, new List<String>());
        }

        protected static void TestPropertySetters<T>(T modelObject, List<String> propertiesToExclude)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(typeof(T).GetProperties().Where(p => p.CanWrite));
            foreach (String propertyName in propertiesToExclude)
            {
                properties.RemoveAll(p => p.Name == propertyName);
            }
            foreach (PropertyInfo property in properties)
            {
                Object value1 = TestDataHelper.GetValue1(property.PropertyType);
                property.SetValue(modelObject, value1, null);
                Assert.AreEqual(value1, property.GetValue(modelObject, null));
            }
            // OK, so setting each individual property seemed to work.  But how do we know that setting one property
            // didn't inadvertantly affect another?  Let's double-check...
            foreach (PropertyInfo property in properties)
            {
                Object value1 = TestDataHelper.GetValue1(property.PropertyType);
                Object value2 = TestDataHelper.GetValue2(property.PropertyType);
                property.SetValue(modelObject, value2, null);
                Assert.AreEqual(value2, property.GetValue(modelObject, null));
                // This property is now set to value 2--check that all other properties aren't:
                foreach (PropertyInfo otherProperty in properties)
                {
                    if (otherProperty.Name != property.Name)
                    {
                        Assert.AreNotEqual(value2, otherProperty.GetValue(modelObject, null));
                    }
                }
                // Revert back to value 1 before moving on the next property.
                property.SetValue(modelObject, value1, null);
            }
        }

        protected void AssertDtoAndModelAreEqual<TDTO, T>(TDTO dto, T model, List<String> propertiesToExclude = null)
        {
            PropertyInfo[] dtoProperties = typeof(TDTO).GetProperties();

            foreach (PropertyInfo dtoProperty in dtoProperties)
            {
                if (propertiesToExclude == null
                    || !propertiesToExclude.Contains(dtoProperty.Name))
                {
                    PropertyInfo modelProperty = typeof(T).GetProperty(dtoProperty.Name);
                    if (modelProperty != null)
                    {
                        Object dtoValue = dtoProperty.GetValue(dto, null);
                        Object modelValue = modelProperty.GetValue(model, null);
                        if (modelProperty.PropertyType.IsEnum && modelValue != null)
                        {
                            modelValue = Convert.ToByte(modelValue);
                        }

                        Assert.AreEqual(dtoValue, modelValue, dtoProperty.Name);
                    }
                }
            }
        }

        protected void AssertModelsAreEqual<T>(T model1, T model2, List<String> propertiesToExclude = null)
        {
            PropertyInfo[] dtoProperties = typeof(T).GetProperties()
                .Where(p=> p.DeclaringType == typeof(T)).ToArray();

            foreach (PropertyInfo modelProperty in dtoProperties)
            {
                if (propertiesToExclude == null
                    || !propertiesToExclude.Contains(modelProperty.Name))
                {
                    String baseTypeName =
                        (modelProperty.PropertyType.BaseType != null)?
                    modelProperty.PropertyType.BaseType.Name : null;
                    Boolean isModelObjectType = baseTypeName != null &&
                             (baseTypeName.StartsWith("ModelObject") ||
                            baseTypeName.StartsWith("ModelReadOnlyObject") ||
                            baseTypeName.StartsWith("ModelList") ||
                            baseTypeName.StartsWith("ModelReadOnlyList"));

                    if (!isModelObjectType)
                    {
                        Object model1Value = modelProperty.GetValue(model1, null);
                        Object model2Value = modelProperty.GetValue(model2, null);

                        Assert.AreEqual(model1Value, model2Value, modelProperty.Name);
                    }
                }
            }
        }

        protected static void TestCreateNew<T>(Boolean isChildObject)
        {
            TestCreateNew<T>(isChildObject, new String[] { }, null, new String[] { });
        }
        protected static void TestCreateNew<T>(Boolean isChildObject,
            String[] parameterPropertyNames, Object[] parameters, String[] excludeProperties)
        {
            Type type = typeof(T);
            String createMethodName = "New" + type.Name;

            MethodInfo createMethod =
                type.GetMethod(createMethodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic);

            Object modelObject = createMethod.Invoke(null, parameters);
            Assert.IsNotNull(modelObject, "Could not create model.");

            //check isChild status
            PropertyInfo property = type.GetProperty("IsChild");
            Assert.AreEqual(isChildObject, property.GetValue(modelObject, null));

            //isnew
            property = type.GetProperty("IsNew");
            if (property != null)
            {
                Assert.IsTrue((Boolean)property.GetValue(modelObject, null));
            }

            //isinitialized
            property = type.GetProperty("IsInitialized");
            if (property != null)
            {
                Assert.IsTrue((Boolean)property.GetValue(modelObject, null));
            }

            //check parameter properties 
            if (parameterPropertyNames != null)
            {
                for (Int32 i = 0; i < parameterPropertyNames.Count(); i++)
                {
                    if (!String.IsNullOrEmpty(parameterPropertyNames[i]))
                    {
                        Assert.AreEqual(parameters[i], type.GetProperty(parameterPropertyNames[i]).GetValue(modelObject, null),
                            "Parameter property was not set.");
                    }
                }
            }

            if (type.Name.EndsWith("List"))
            {
                //check list starts empty.
                Assert.IsEmpty((System.Collections.ICollection)modelObject, "Lists should start empty.");
            }
            else
            {
                //check that child objects are not null

                List<PropertyInfo> properties = new List<PropertyInfo>(type.GetProperties().Where(p => p.DeclaringType == type));
                foreach (PropertyInfo childProperty in properties)
                {
                    if (excludeProperties == null || !excludeProperties.Contains(childProperty.Name))
                    {
                        //if the property represents a child model object
                        if (childProperty.PropertyType.BaseType != null && !childProperty.Name.StartsWith("Parent"))
                        {
                            String baseTypeName = childProperty.PropertyType.BaseType.Name;
                            if (baseTypeName.StartsWith("ModelObject") ||
                            baseTypeName.StartsWith("ModelReadOnlyObject") ||
                            baseTypeName.StartsWith("ModelList") ||
                            baseTypeName.StartsWith("ModelReadOnlyList"))
                            {
                                //check it is not null
                                Object value = childProperty.GetValue(modelObject, null);
                                Assert.IsNotNull(value, String.Format("Child model object {0} is null", childProperty.Name));
                            }
                        }
                    }
                }
            }
        }

        protected static void PopulatePropertyValues1<T>(T modelObject, List<String> propertiesToExclude = null)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(typeof(T).GetProperties().Where(p => p.CanWrite));
            if (propertiesToExclude != null)
            {
                foreach (String propertyName in propertiesToExclude)
                {
                    properties.RemoveAll(p => p.Name == propertyName);
                }
            }
            foreach (PropertyInfo property in properties)
            {
                Object value1 = TestDataHelper.GetValue1(property.PropertyType);
                property.SetValue(modelObject, value1, null);
                Assert.AreEqual(value1, property.GetValue(modelObject, null));
            }
        }
        protected static void PopulatePropertyValues2<T>(T modelObject, List<String> propertiesToExclude = null)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(typeof(T).GetProperties().Where(p => p.CanWrite));
            if (propertiesToExclude != null)
            {
                foreach (String propertyName in propertiesToExclude)
                {
                    properties.RemoveAll(p => p.Name == propertyName);
                }
            }
            foreach (PropertyInfo property in properties)
            {
                Object value1 = TestDataHelper.GetValue2(property.PropertyType);
                property.SetValue(modelObject, value1, null);
                Assert.AreEqual(value1, property.GetValue(modelObject, null));
            }
        }

        #endregion

    }

    public abstract class TestBase<TMODEL, TDTO> : ModelTestBase<TMODEL, TDTO>
        where TMODEL : ModelObject<TMODEL>
    {
        #region Fields
        private String _originalDalName; // stores the original dal name
        private String _dalName; // the dal name
        private IDalFactory _userDalFactory;

        #endregion

        #region Properties

        protected IDalFactory UserDalFactory
        {
            get { return _userDalFactory; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public void Setup()
        {
            DalContainer.Reset();

            // store the original dal name
            _originalDalName = DalContainer.DalName;

            // generate a new dal name
            _dalName = Guid.NewGuid().ToString();

            // create a new, in memory dal factory
            base.DalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();
            DalContainer.RegisterFactory(_dalName, base.DalFactory);
            base.DalFactory.CreateDatabase();

            //User
            String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName, "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (string file in Directory.GetFiles(templateDir))
                {
                    //if (!Path.GetFileName(file).StartsWith(templateName))
                    //{
                    try
                    {
                        File.Delete(file);
                    }
                    catch
                    {
                        // No point in crying over spilt milk
                    }
                    //}
                }
            }





            String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
            var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
            userFactory.UnitTestFolder = templateDir;
            _userDalFactory = userFactory;
            DalContainer.RegisterFactory(Constants.UserDal, _userDalFactory);


            //authenticate
            DomainPrincipal.Authenticate();
        }

        /// <summary>
        /// Called when a test is completed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            //Remove the User DalFactory
            DalContainer.RemoveFactory(Constants.UserDal);
            _userDalFactory.Dispose();

            //Delete the user file
            String factoryFilePath = Path.Combine(this.ExecutingAssemblyEnvironmentsPath(), Galleria.Ccm.Constants.AppDataFolderName);
            if (Directory.Exists(factoryFilePath))
            {
                Directory.Delete(factoryFilePath, true);
            }

            // remove the registered dal
            DalContainer.RemoveFactory(_dalName);

            // and restore the original dal name
            DalContainer.DalName = _originalDalName;
        }

        /// <summary>
        /// Gets the root path where our DBMaintenance tests will store template and test databases
        /// </summary>
        /// <returns></returns>
        private String ExecutingAssemblyEnvironmentsPath()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

        #endregion


    }
}
