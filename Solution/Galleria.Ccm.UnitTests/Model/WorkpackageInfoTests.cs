﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-25757 : L.Ineson
//		Created
#endregion
#region Version History: (CCM810)
// V8-29811 : A.Probyn
//  Updated inline with changes for PlanogramoLocationType
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class WorkpackageInfoTests : TestBase
    {
        #region TestFixtureHelpers

        private void InsertWorkpackages()
        {
            //insert a workflow
            TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");
            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();

            var flow1 = Galleria.Ccm.Model.Workflow.NewWorkflow(1);
            flow1.Name = Guid.NewGuid().ToString();
            flow1.Tasks.Add(engineTasks.First());
            flow1.Tasks.Add(engineTasks.First());
            flow1 = flow1.Save();

            Workpackage package = Workpackage.NewWorkpackage(1);
            package.Name = Guid.NewGuid().ToString();
            package.WorkflowId = flow1.Id;
            package = package.Save();

        }

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(WorkpackageInfoDto dto, WorkpackageInfo model)
        {
            Assert.AreEqual(22, typeof(WorkpackageInfoDto).GetProperties().Count(), "Number of properties has changed");

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.UserId, model.UserId);
            Assert.AreEqual(dto.UserFullName, model.UserFullName);
            Assert.AreEqual(dto.PlanogramCount, model.PlanogramCount);
            Assert.AreEqual(dto.PlanogramPendingCount, model.PlanogramPendingCount);
            Assert.AreEqual(dto.PlanogramQueuedCount, model.PlanogramQueuedCount);
            Assert.AreEqual(dto.PlanogramProcessingCount, model.PlanogramProcessingCount);
            Assert.AreEqual(dto.PlanogramCompleteCount, model.PlanogramCompleteCount);
            Assert.AreEqual(dto.PlanogramFailedCount, model.PlanogramFailedCount);
            Assert.AreEqual(dto.DateCreated, model.DateCreated);
            Assert.AreEqual(dto.DateLastModified, model.DateLastModified);
            Assert.AreEqual(dto.DateCompleted, model.DateCompleted);
            Assert.AreEqual(dto.ProcessingStatus, (Byte)model.ProcessingStatus);
            Assert.AreEqual(dto.ProcessingStatusDescription, model.ProcessingStatusDescription);
            Assert.AreEqual(dto.ProgressMax, model.ProgressMax);
            Assert.AreEqual(dto.ProgressCurrent, model.ProgressCurrent);
            Assert.AreEqual(dto.ProgressDateStarted, model.ProgressDateStarted);
            Assert.AreEqual(dto.ProgressDateLastUpdated, model.ProgressDateLastUpdated);

        }

        #endregion


        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            InsertWorkpackages();

            WorkpackageInfoList infoList = WorkpackageInfoList.FetchByEntityId(1);
            WorkpackageInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchWorkpackageInfo()
        {
            WorkpackageInfoDto dto =
                new WorkpackageInfoDto()
                {
                    Name = "C1Name",
                    Id = 22
                };

            WorkpackageInfo model = WorkpackageInfo.GetWorkpackageInfo(null, dto);


            AssertDtoAndModelAreEqual(dto, model);
        }

        #endregion
    }
}
