﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    class AssortmentMinorRevisionReplaceActionTests : TestBase
    {

        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.AssortmentMinorRevisionReplaceAction model =
                Galleria.Ccm.Model.AssortmentMinorRevisionReplaceAction.NewAssortmentMinorRevisionReplaceAction();
            Serialize(model);
        }

        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.AssortmentMinorRevisionReplaceAction model =
                Galleria.Ccm.Model.AssortmentMinorRevisionReplaceAction.NewAssortmentMinorRevisionReplaceAction();
            TestBase.TestPropertySetters<Galleria.Ccm.Model.AssortmentMinorRevisionReplaceAction>(model);
        }

    }
}
