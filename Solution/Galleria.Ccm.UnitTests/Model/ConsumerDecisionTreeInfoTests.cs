﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion

#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Updated AssertDtoAndModelAreEqual expected properties from 7 to 8 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeInfoTests : TestBase
    {
        #region TestHelpers

        public static ConsumerDecisionTreeInfoDto GetInfoDto(ConsumerDecisionTreeDto dto)
        {
            return new ConsumerDecisionTreeInfoDto()
            {
                EntityId = dto.EntityId,
                Name = dto.Name,
                Id = dto.Id,
                UniqueContentReference = dto.UniqueContentReference,
                ProductGroupId = dto.ProductGroupId
            };
        }

        public static void AssertDtoAndModelAreEqual(ConsumerDecisionTreeInfoDto dto,
            Ccm.Model.ConsumerDecisionTreeInfo model)
        {
            Assert.AreEqual(8, typeof(ConsumerDecisionTreeInfoDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.UniqueContentReference, model.UniqueContentReference);
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
        }

        public static void AssertDtoAndModelAreEqual(ConsumerDecisionTreeDto dto,
            Ccm.Model.ConsumerDecisionTreeInfo model)
        {
            Assert.AreEqual(8, typeof(ConsumerDecisionTreeInfoDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.UniqueContentReference, model.UniqueContentReference);
            Assert.AreEqual(dto.ProductGroupId, model.ProductGroupId);
            Assert.AreEqual(dto.ParentUniqueContentReference, model.ParentUniqueContentReference);
        }

        #endregion

        [Test]
        public void Serializable()
        {
            //create an entity & required dtos
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<ConsumerDecisionTreeDto> cdtDtoList = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1);

            //fetch the info list
            Galleria.Ccm.Model.ConsumerDecisionTreeInfoList infoList =
                Galleria.Ccm.Model.ConsumerDecisionTreeInfoList.FetchByEntityId(entityId);

            Galleria.Ccm.Model.ConsumerDecisionTreeInfo model = infoList[0];

            Serialize(model);
        }

        #region Factory Methods

        [Test]
        public void FetchConsumerDecisionTreeInfo()
        {
            //create an entity & required dtos
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<ConsumerDecisionTreeDto> cdtDtoList = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityId, 1);

            List<ConsumerDecisionTreeInfoDto> infoDtoList = new List<ConsumerDecisionTreeInfoDto>();
            cdtDtoList.ForEach(c => infoDtoList.Add(GetInfoDto(c)));


            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {

                foreach (ConsumerDecisionTreeInfoDto dto in infoDtoList)
                {
                    Galleria.Ccm.Model.ConsumerDecisionTreeInfo model =
                        Galleria.Ccm.Model.ConsumerDecisionTreeInfo.FetchConsumerDecisionTreeInfo(dalContext, dto);

                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }


        #endregion
    }
}
