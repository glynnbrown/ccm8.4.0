﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27804 : L.Ineson
//  Created.
// V8-28219 : N.Haywood
//  changed PlanogramImportFileType.Spaceman to SpacemanV9
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using System.Windows.Media;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramImportTemplateTests : TestBase<PlanogramImportTemplate, PlanogramImportTemplateDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out PlanogramImportTemplate model)
        {
            PlanogramImportTemplate item = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1");
            item.Name = Guid.NewGuid().ToString();

            root = item;
            model = item;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_FileTypeFileVersion()
        {
            TestNewFactoryMethod(new String[] { "FileType", "FileVersion" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void FetchByFilename()
        {
            throw new InconclusiveException("TODO");

            //PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.Spaceman, "1");
            //template.Name = Guid.NewGuid().ToString();
            //template.SaveAsFile(Path.Com

            //TestFetch<PlanogramImportTemplate>("FetchByFilename", new String[] { "Id" });
        }

        [Test]
        public void FetchById()
        {
            TestFetch<PlanogramImportTemplate>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<PlanogramImportTemplate>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<PlanogramImportTemplate>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<PlanogramImportTemplate>();
        }

        #endregion

    }
}
