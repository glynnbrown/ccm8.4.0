﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28258 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.IO;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class TaskLibraryTests : TestBase
    {
        #region RegisterValidAssembly
        /// <summary>
        /// Tests that a valid assembly can be registered
        /// </summary>
        [Test]
        public void RegisterValidAssembly()
        {
            // select the assembly to register
            String filePath = Path.Combine(this.ExecutingAssemblyEnvironmentsPath(), "Environment", "Galleria.Ccm.Engine.Tasks.dll");

            // register the library
            TaskLibrary.TaskLibraryRegisterResult result = TaskLibrary.Register(filePath);
            Assert.AreEqual(TaskLibrary.TaskLibraryRegisterResult.Successful, result);

            // register the library again
            result = TaskLibrary.Register(filePath);
            Assert.AreEqual(TaskLibrary.TaskLibraryRegisterResult.AssemblyOutOfDate, result);
        }
        #endregion

        #region RegisterInvalidAssembly
        /// <summary>
        /// Tests that a valid assembly can be registered
        /// </summary>
        [Test]
        public void RegisterInvalidAssembly()
        {
            // select the assembly to register
            String filePath = Path.Combine(this.ExecutingAssemblyEnvironmentsPath(), "Environment", "Galleria.Ccm.Engine.Tasks.txt");

            // register the library
            TaskLibrary.TaskLibraryRegisterResult result = TaskLibrary.Register(filePath);
            Assert.AreEqual(TaskLibrary.TaskLibraryRegisterResult.InvalidAssembly, result);
        }
        #endregion
    }
}
