﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24261 : L.Hodson
//  Created
#endregion
#endregion


using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class RecentPlanogramListTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(RecentPlanogramList.NewRecentPlanogramList());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void CreateNew()
        {
            TestCreateNew<RecentPlanogramList>(false);
        }

        [Test]
        public void FetchAll()
        {
            Int32 expectedCount = 10;

            //insert a few dtos
            using (IDalContext dalContext = base.UserDalFactory.CreateContext())
            {
                using (IRecentPlanogramDal dal = dalContext.GetDal<IRecentPlanogramDal>())
                {
                    for (Int32 i = 0; i < expectedCount; i++)
                    {
                        RecentPlanogramDto dto = new RecentPlanogramDto();
                        PopulatePropertyValues1<RecentPlanogramDto>(dto);
                        dal.Insert(dto);
                    }
                }
            }


            //fetch
            RecentPlanogramList list = RecentPlanogramList.FetchAll();
            Assert.AreEqual(expectedCount, list.Count);

        }

        #endregion
    }
}
