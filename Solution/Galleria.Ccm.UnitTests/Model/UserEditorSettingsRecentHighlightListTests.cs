﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//   Created.
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserEditorSettingsRecentHighlightListTests
    {
        [Test]
        public void Add_WhenLessThan10Items_ShouldAddItem()
            {
            UserEditorSettingsRecentHighlightList list = UserEditorSettingsRecentHighlightList.NewList();
            Highlight highlight = Highlight.NewHighlight();
            list.AddHighlightId(highlight.Id);

            Assert.IsTrue(list.Any(rh => rh.HighlightId.Equals(highlight.Id)));
            }

        [Test]
        public void Add_When10Items_RemovesFirstAndAddsLabel()
            
        {
            UserEditorSettingsRecentHighlightList list = UserEditorSettingsRecentHighlightList.NewList();
            for (int i = 0; i < 10; i++)
            {
                list.AddHighlightId(Highlight.NewHighlight().Id, maxRecentItems: 10);
            }
            Highlight highlight = Highlight.NewHighlight();
            var firstItem = list.First();

            list.AddHighlightId(highlight.Id, maxRecentItems: 10);

            Assert.That(list, Has.Count.EqualTo(10));
            Assert.IsTrue(list.Any(rl => rl.HighlightId.Equals(highlight.Id)));
            CollectionAssert.DoesNotContain(list, firstItem);
        }

        [Test]
        public void Add_NullIsIgnored_WhenAddingAddingNull()
        {
            UserEditorSettingsRecentHighlightList list = UserEditorSettingsRecentHighlightList.NewList();

            list.AddHighlightId(null, maxRecentItems: 10);

            Assert.IsTrue(list.All(i => i.HighlightId != null));
        }
    }
}
