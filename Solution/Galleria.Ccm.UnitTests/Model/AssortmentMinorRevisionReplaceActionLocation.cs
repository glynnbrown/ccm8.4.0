﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    class AssortmentMinorRevisionReplaceActionLocation : TestBase
    {

        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.AssortmentMinorRevisionReplaceActionLocation model =
                Galleria.Ccm.Model.AssortmentMinorRevisionReplaceActionLocation.NewAssortmentMinorRevisionReplaceActionLocation();
            Serialize(model);
        }

        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.AssortmentMinorRevisionReplaceActionLocation model =
                Galleria.Ccm.Model.AssortmentMinorRevisionReplaceActionLocation.NewAssortmentMinorRevisionReplaceActionLocation();
            TestBase.TestPropertySetters<Galleria.Ccm.Model.AssortmentMinorRevisionReplaceActionLocation>(model);
        }

    }
}
