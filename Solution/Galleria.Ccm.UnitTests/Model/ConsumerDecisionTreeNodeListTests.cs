﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeNodeListTests : TestBase
    {
        #region Serializable
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeNodeList model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNodeList.NewConsumerDecisionTreeNodeList();
            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTreeNodeList()
        {
            Galleria.Ccm.Model.ConsumerDecisionTreeNodeList model =
                Galleria.Ccm.Model.ConsumerDecisionTreeNodeList.NewConsumerDecisionTreeNodeList();

            Assert.IsTrue(model.IsChild, "the model should be a child");
            Assert.IsEmpty(model, "the model should be empty");
        }

        [Test]
        public void FetchListByParentNodeId()
        {
            //create an entity & required dtos
            Galleria.Ccm.Model.Entity entity1 = Galleria.Ccm.Model.Entity.NewEntity();
            entity1.Name = "entity1";
            entity1 = entity1.Save();

            List<ConsumerDecisionTreeDto> cdtDtoList = TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entity1.Id, 1);
            List<ConsumerDecisionTreeLevelDto> cdtLevelDtoList = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(this.DalFactory, cdtDtoList[0], 3, true);
            List<ConsumerDecisionTreeNodeDto> cdtNodeDtoList = TestDataHelper.InsertConsumerDecisionTreeNodeDtos(base.DalFactory, cdtDtoList[0], cdtLevelDtoList, 5, true);

            Galleria.Ccm.Model.ConsumerDecisionTree parentCdt =
                    Galleria.Ccm.Model.ConsumerDecisionTree.FetchById(cdtDtoList[0].Id);

            //cycle through parent dtos
            foreach (Galleria.Ccm.Model.ConsumerDecisionTreeNode node
                in parentCdt.RootNode.FetchAllChildNodes())
            {
                IEnumerable<Int32> expectedChildIds = cdtNodeDtoList.Where(n => n.ParentNodeId == node.Id).Select(n => n.Id);
                IEnumerable<Int32> actualChildIds = node.ChildList.Select(n => n.Id);

                Assert.AreEqual(expectedChildIds.Count(), actualChildIds.Count());

                foreach (Int32 expectedId in expectedChildIds)
                {
                    Assert.IsTrue(actualChildIds.Contains(expectedId));
                }

            }
        }

        #endregion
    }
}
