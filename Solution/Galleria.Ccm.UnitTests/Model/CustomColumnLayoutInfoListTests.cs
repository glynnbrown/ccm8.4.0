﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnLayoutInfoListTests : TestBase
    {
        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumnLayoutInfoList testModel = CustomColumnLayoutInfoList.FetchByIds(new List<Object>());

            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        // Only property is the collection itself, which the test base can't test as CustomColumnLayoutInfoList does not have a parameterless factory method or constructor.

        [Test]
        public void FetchByIds()
        {
            TestDelegate code = () => CustomColumnLayoutInfoList.FetchByIds(new List<Object>());

            Assert.DoesNotThrow(code);
        }

        #endregion
    }
}
