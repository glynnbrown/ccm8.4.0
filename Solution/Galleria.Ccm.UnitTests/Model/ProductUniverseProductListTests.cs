﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//      Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

using NUnit.Framework;

using Csla;

using Galleria.Framework.Dal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;

using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductUniverseProductList : TestBase
    {
        #region Serializable

        [Test]
        public void Serializable()
        {
            var model = Ccm.Model.ProductUniverseProductList.NewProductUniverseProductList();
            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewProductUniverseProductList()
        {
            var model = Ccm.Model.ProductUniverseProductList.NewProductUniverseProductList();
            Assert.IsEmpty(model);
        }

        [Test]
        public void FetchByProductUniverseId()
        {
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            List<ProductDto> productDtoList = TestDataHelper.InsertProductDtos(base.DalFactory, 25, entityDtoList[0].Id);
            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtoList[0].Id);
            List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, productHierarchyDtoList[0].Id, 4);
            List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(base.DalFactory, productLevelDtoList, 10);
            List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(base.DalFactory, entityDtoList, productGroupDtoList);
            List<ProductUniverseProductDto> productUniverseProductDtoList = TestDataHelper.InsertProductUniverseProductDtos(base.DalFactory, productUniverseDtoList, productDtoList);

            //Fetch product list
            Galleria.Ccm.Model.ProductUniverseProductList model = Galleria.Ccm.Model.ProductUniverseProductList.FetchByProductUniverseId(base.DalFactory.CreateContext(), productUniverseDtoList[0].Id);

            //This universe should have 25 products
            Assert.AreEqual(25, model.Count, "25 products should exist");
        }

        #endregion
    }
}
