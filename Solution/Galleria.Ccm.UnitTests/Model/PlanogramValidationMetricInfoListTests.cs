﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27004 : A.Silva ~ Created.
// V8-26322 : A.Silva
//      Amended how User records are inserted to avoid random duplicates.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramValidationMetricInfoListTests : TestBase
    {
        #region Serializable

        [Test]
        public void Serializable()
        {
            List<Int32> planogramIds;
            InsertTestDtos(out planogramIds);
            var validationInfoList = PlanogramValidationInfoList.FetchByPlanogramIds(planogramIds);
            var model = validationInfoList[0].Groups[0].Metrics;

            Serialize(model);
            Assert.IsTrue(model.IsReadOnly);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void FetchByPlanogramIds()
        {
            // Not sure how to test. Will need to get back to this later.
        }

        #endregion

        #region Test Helper Methods

        /// <summary>
        ///     Asserts the propertiesi in both the given <paramref name="dto"/> and <paramref name="model"/> are the same.
        /// </summary>
        /// <param name="dto">The original test dto.</param>
        /// <param name="model">The final model generated from the dto.</param>
        private static void AssertDtoAndModelAreEqual(PlanogramValidationTemplateGroupMetricDto dto, PlanogramValidationMetricInfoDto model)
        {
            Assert.AreEqual(4, typeof(PlanogramValidationMetricInfoDto).GetProperties().Count(), "Incorrect number of properties being tested.");

            Assert.AreEqual(Convert.ToInt32(dto.Id), model.Id);
            Assert.AreEqual(Convert.ToInt32(dto.PlanogramValidationTemplateGroupId), model.PlanogramValidationGroupId);
            Assert.AreEqual(dto.Field, model.Field);
            Assert.AreEqual(dto.ResultType, model.ResultType);
        }

        private List<PlanogramValidationTemplateGroupMetricDto> InsertTestDtos(out List<Int32> planogramIds)
        {
            var dalContext = base.DalFactory.CreateContext();
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 5);
            var planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos);
            planogramIds = planogramDtos.Select(dto => Convert.ToInt32(dto.Id)).ToList();
            var validationTemplateDtos = TestDataHelper.InsertPlanogramValidationTemplateDtos(dalContext, planogramDtos);
            var validationTemplateGroupDtos = TestDataHelper.InsertPlanogramValidationTemplateGroupDtos(dalContext, validationTemplateDtos);
            var validationTemplateMetricDtos = TestDataHelper.InsertPlanogramValidationTemplateGroupMetricDtos(dalContext, validationTemplateGroupDtos);
            return validationTemplateMetricDtos;
        }

        #endregion
    }
}
