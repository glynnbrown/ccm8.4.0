﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-27630 : I.George
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttributeListTests : TestBase
    {
        #region Helper methods
        private Int32 entityId;
        private List<LocationProductAttributeDto> InsertDtos()
        {
            entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId);

            List<LocationProductAttributeDto> dtoList = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtoList, prodList, entityId);
            return dtoList;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {

            Galleria.Ccm.Model.LocationProductAttributeList model =
                Galleria.Ccm.Model.LocationProductAttributeList.NewList();
            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewList()
        {
            Galleria.Ccm.Model.LocationProductAttributeList model = Galleria.Ccm.Model.LocationProductAttributeList.NewList();

            Assert.IsFalse(model.IsChild, "the model should be a root");
            Assert.IsEmpty(model, "the model should be empty");
        }

        [Test]
        public void FetchByEntityId()
        {
            List<LocationProductAttributeDto> entityDtoList1 = InsertDtos();
            Int32 entityId2 = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId2);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId2);

            List<LocationProductAttributeDto> entityDtoList2 = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtoList, prodList, entityId2);

            Galleria.Ccm.Model.LocationProductAttributeInfoList model1 =
                Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(entityId);
            foreach (LocationProductAttributeDto dto in entityDtoList1)
            {
                Ccm.Model.LocationProductAttributeInfo info = model1.
                    First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
            }

            Galleria.Ccm.Model.LocationProductAttributeInfoList model2 =
               Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(entityId2);
            foreach (LocationProductAttributeDto dto in entityDtoList2)
            {
                Ccm.Model.LocationProductAttributeInfo info = model2.
                    First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
            }
        }

        [Test]
        public void FetchByLocationIdProductIdCombinations()
        {
            List<LocationProductAttributeDto> entityDtoList1 = InsertDtos();
            Int32 entityId2 = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId2);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId2);

            List<LocationProductAttributeDto> entityDtoList2 = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtoList, prodList, entityId2);
           
            //compile a combination list
            List<Tuple<Int16, Int32>> combinationsList = new List<Tuple<short, int>>();
            foreach (LocationDto locDto in locationDtoList)
            {
                foreach (Galleria.Ccm.Model.Product prodDto in prodList)
                {
                    combinationsList.Add(new Tuple<short, int>(locDto.Id, prodDto.Id));
                }
            }

            //add a random combination to the end
            combinationsList.Add(new Tuple<short, int>(199, 198));

            //fetch back dtos for the combinations
            Galleria.Ccm.Model.LocationProductAttributeList dalList =
                Galleria.Ccm.Model.LocationProductAttributeList.FetchByLocationIdProductIdCombinations(entityId2, combinationsList);

            //check each combination has been provided for
            Assert.AreEqual(combinationsList.Count, dalList.Count);
            foreach (Tuple<Int16, Int32> combi in combinationsList)
            {
                Galleria.Ccm.Model.LocationProductAttribute dalType = dalList.First(l => l.LocationId == combi.Item1 && l.ProductId == combi.Item2);
            }

        }

        #endregion
    }
}
