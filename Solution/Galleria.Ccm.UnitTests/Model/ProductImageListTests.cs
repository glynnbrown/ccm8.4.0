﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ProductImageListTests : TestBase
    {
        [Test]
        public void Serializable()
        {
            var productImageList = ProductImageList.NewList();
            Serialize(productImageList);
        }

        [Test]
        public void NewList()
        {
            var model = ProductImageList.NewList();

            Assert.IsTrue(model.IsChild, "the model should be marked a child");
            Assert.IsEmpty(model, "the model should be empty");
        }

        [Test]
        public void GetListByProductId()
        {
            using(var dalContext = DalFactory.CreateContext())
            {
                var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).FirstOrDefault().Id;
                var productDtos = TestDataHelper.InsertProductDtos(DalFactory, 10, entityId);
                var compressionDtos = TestDataHelper.InsertCompressionDtos(DalFactory,10);
                var productImageDtos = TestDataHelper.InsertProductImageDtos(dalContext,entityId,productDtos,compressionDtos);

                foreach(var productDto in productDtos)
                {
                    var matchingImages = productImageDtos.Where(p=>p.ProductId==productDto.Id);
                    var imageList = ProductImageList.GetListByProductId(productDto.Id);
                    Assert.AreEqual(matchingImages.Count(),imageList.Count);
                }
            }
        }

        [Test]
        public void GetListByEntityId()
        {
            using (var dalContext = DalFactory.CreateContext())
            {
                var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).FirstOrDefault().Id;
                var productDtos = TestDataHelper.InsertProductDtos(DalFactory, 10, entityId);
                var compressionDtos = TestDataHelper.InsertCompressionDtos(DalFactory, 10);
                var productImageDtos = TestDataHelper.InsertProductImageDtos(dalContext, entityId, productDtos, compressionDtos);

                foreach (var productDto in productDtos)
                {
                    var matchingImages = productImageDtos.Where(p => p.EntityId == entityId);
                    var imageList = ProductImageList.GetListByEntityId(entityId);
                    Assert.AreEqual(matchingImages.Count(), imageList.Count);
                }
            }
        }

        [Test]
        public void GetListByProductIdImageType()
        {
            using (var dalContext = DalFactory.CreateContext())
            {
                var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).FirstOrDefault().Id;
                var productDtos = TestDataHelper.InsertProductDtos(DalFactory, 10, entityId);
                var compressionDtos = TestDataHelper.InsertCompressionDtos(DalFactory, 10);
                var productImageDtos = TestDataHelper.InsertProductImageDtos(dalContext, entityId, productDtos, compressionDtos);

                foreach (var productDto in productDtos)
                {
                    var matchingImages = productImageDtos.Where(p => p.ProductId == productDto.Id);
                    foreach(var imageType in matchingImages.GroupBy(p=>p.ImageType))
                    {
                        var imageList = ProductImageList.GetListByProductIdImageType(productDto.Id,(ProductImageType)imageType.Key);
                        Assert.AreEqual(imageType.Count(), imageList.Count);
                    }
                }
            }
        }
    }
}
