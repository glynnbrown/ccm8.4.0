﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (SA 1.0)
// V8-24779 : L.Luong
//		Created 

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramImportTemplateInfoTests: TestBase
    {
        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(PlanogramImportTemplateDto dto, PlanogramImportTemplateInfo model)
        {
            Assert.AreEqual(4, typeof(PlanogramImportTemplateInfoDto).GetProperties().Count(), "Number of properties has changed");

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            Assert.AreEqual(dto.Name, model.Name);
        }

        private List<PlanogramImportTemplateDto> InsertTestDtos()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<PlanogramImportTemplateDto> returnList =
            TestDataHelper.InsertPlanogramImportTemplateDtos(base.DalFactory, 20, entityId);

            return returnList;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<PlanogramImportTemplateDto> ImpDtos = InsertTestDtos();
            PlanogramImportTemplateInfoList infoList = PlanogramImportTemplateInfoList.FetchByEntityId(ImpDtos.First().EntityId);

            PlanogramImportTemplateInfo model = infoList[0];

            Serialize(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void FetchPlanogramImportTemplateInfo()
        {
            List<PlanogramImportTemplateDto> dtoList = InsertTestDtos();
            PlanogramImportTemplateInfoList infoList = PlanogramImportTemplateInfoList.FetchByEntityId(1);

            foreach (PlanogramImportTemplateDto planogramImportTemplateDto in dtoList)
            {
                PlanogramImportTemplateInfo dalImpInfo = infoList.First(l => l.Id.Equals(planogramImportTemplateDto.Id));
                AssertDtoAndModelAreEqual(planogramImportTemplateDto, dalImpInfo);
            }
        }

        #endregion
    }
}
