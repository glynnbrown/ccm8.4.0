﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class MetricListTest : TestBase
    {
      #region Serializable
        [Test]
        public void Serializable()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            List<MetricDto> metricList = TestDataHelper.InsertMetricDtos(base.DalFactory, entityId, 20);

            MetricList model = MetricList.FetchByEntityId(entityId);
            Serialize(model);
        }
    
    #endregion

      #region Factory Methods
        [Test]
        public void FetchByEntityId()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            List<MetricDto> metricList1 =
            TestDataHelper.InsertMetricDtos(base.DalFactory, entityList[0].Id, 20);

            List<MetricDto> metricList2 =
            TestDataHelper.InsertMetricDtos(base.DalFactory, entityList[1].Id, 20);

            MetricList model1 = MetricList.FetchByEntityId(entityList[0].Id);
            foreach (MetricDto metDto in metricList1)
            {
                Metric info = model1.First(i => i.Id == metDto.Id);
            }

            MetricList model2 = MetricList.FetchByEntityId(entityList[1].Id);
            foreach (MetricDto metDto in metricList2)
            {
                Metric info = model2.First(i => i.Id == metDto.Id);
            }
        }
 
    #endregion
    }
}