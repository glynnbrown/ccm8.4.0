﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26124 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class RoleTests : TestBase
    {
        #region Test Helper Methods

        private void AssertDtoAndModelAreEqual(RoleDto dto, Role model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.Description, model.Description);
            Assert.AreEqual(dto.IsAdministrator, model.IsAdministratorRole);
        }

        private void AssertModelAndModelAreEqual(Role model1, Role model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.RowVersion, model2.RowVersion);
            Assert.AreEqual(model1.Name, model2.Name);
            Assert.AreEqual(model1.Description, model2.Description);
            Assert.AreEqual(model1.IsAdministratorRole, model2.IsAdministratorRole);
        }

        private void SetRandomPropertyValues(Role model)
        {
            model.Name = Guid.NewGuid().ToString();
            model.Description = Guid.NewGuid().ToString();
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            Role model =
                Role.NewRole();
            Serialize(model);
        }

        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            Role model = Role.NewRole();
            TestBase.TestPropertySetters<Role>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewRole()
        {
            Role newRole =
                Role.NewRole();

            Assert.IsTrue(newRole.IsNew);
        }

        [Test]
        public void FetchDistributionCentre()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 15, entityId);

            //fetch each Distribution Centre by ID
            foreach (RoleDto roleDto in roleList)
            {
                Role dalRole =
                    Role.FetchById(roleDto.Id);

                AssertDtoAndModelAreEqual(roleDto, dalRole);
            }
        }

        #endregion

        #region Data Access

        #region Insert

        [Test]
        public void DataAccess_Insert()
        {
            TestDataHelper.InsertEntityDtos(base.DalFactory, 1);

            //Create new item and save
            Role role = Role.NewRole();
            SetRandomPropertyValues(role);
            role = role.Save();

            //Fetch the dto back
            RoleDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    dto = dal.FetchById(role.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, role);
        }

        #endregion

        #region Update

        [Test]
        public void DataAccess_Update()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //Create new item and save
            Role role = Role.NewRole();
            SetRandomPropertyValues(role);
            role = role.Save();

            //Update and save
            SetRandomPropertyValues(role);
            role = role.Save();

            //Fetch the dto back
            RoleDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    dto = dal.FetchById(role.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, role);
        }

        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //Create new item and save
            Role role = Role.NewRole();
            SetRandomPropertyValues(role);
            role = role.Save();

            //Fetch the dto back
            RoleDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    dto = dal.FetchById(role.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, role);

            //Delete the item
            Int32 itemId = role.Id;
            role.Delete();
            role.Save();

            //Try to fetch the item again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(itemId);
                        });
                }
            }
        }

        #endregion

        #endregion
    }
}
