﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM8.0.2
// V8-29010 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using System.Windows.Media;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramNameTemplateTests : TestBase<PlanogramNameTemplate, PlanogramNameTemplateDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out PlanogramNameTemplate model)
        {
            PlanogramNameTemplate item = PlanogramNameTemplate.NewPlanogramNameTemplate(1);
            item.Name = Guid.NewGuid().ToString();

            root = item;
            model = item;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_EntityId()
        {
            TestNewFactoryMethod(new String[] { "EntityId" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void FetchById()
        {
            TestFetch<PlanogramNameTemplate>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<PlanogramNameTemplate>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<PlanogramNameTemplate>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<PlanogramNameTemplate>();
        }

        #endregion

    }
}