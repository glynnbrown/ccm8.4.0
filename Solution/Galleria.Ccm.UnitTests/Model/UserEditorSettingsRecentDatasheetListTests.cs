﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//   Created.
#endregion
#endregion 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserEditorSettingsRecentDatasheetListTests
    {
        [Test]
        public void Add_WhenLessThan10Items_ShouldAddItem()
        {
            UserEditorSettingsRecentDatasheetList list = UserEditorSettingsRecentDatasheetList.NewList();
            CustomColumnLayout datasheet = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            list.Add(datasheet);

            Assert.IsTrue(list.Any(rh => rh.DatasheetId.Equals(datasheet.Id)));
        }

         [Test]
        public void Add_When10Items_RemovesFirstAndAddsLabel()
            
        {
            UserEditorSettingsRecentDatasheetList list = UserEditorSettingsRecentDatasheetList.NewList();
            for (int i = 0; i < 10; i++)
            {
                list.Add(CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet), maxRecentItems: 10);
            }
            CustomColumnLayout datasheet = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            var firstItem = list.First();

            list.Add(datasheet, maxRecentItems: 10);

            Assert.That(list, Has.Count.EqualTo(10));
            Assert.IsTrue(list.Any(rl => rl.DatasheetId.Equals(datasheet.Id)));
            CollectionAssert.DoesNotContain(list, firstItem);
        }

        [Test]
        public void Add_NullIsIgnored_WhenAddingAddingNull()
        {
            UserEditorSettingsRecentDatasheetList list = UserEditorSettingsRecentDatasheetList.NewList();

            list.Add(null, maxRecentItems: 10);

            Assert.IsTrue(list.All(i => i.DatasheetId != null));
        }
    }
}

    

