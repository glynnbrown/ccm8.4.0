﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttributeList : TestBase
    {
        //#region Serializable
        ///// <summary>
        ///// Serializable
        ///// </summary>
        //[Test]
        //public void Serializable()
        //{
        //    Galleria.Ccm.Model.LocationProductAttributeList model =
        //        Galleria.Ccm.Model.LocationProductAttributeList.NewList();
        //    Serialize(model);
        //}
        //#endregion

        //#region Factory Methods

        //[Test]
        //public void NewList()
        //{
        //    Galleria.Ccm.Model.LocationProductAttributeList model = Galleria.Ccm.Model.LocationProductAttributeList.NewList();

        //    Assert.IsFalse(model.IsChild, "the model should be a root");
        //    Assert.IsEmpty(model, "the model should be empty");
        //}

        //[Test]
        //public void FetchByEntityId()
        //{
        //    Random rand = new Random(238742);
        //    List<Ccm.Model.Entity> entityList = new List<Ccm.Model.Entity>();
        //    for (int i = 1; i <= 5; i++)
        //    {
        //        Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //        entity.Name = Guid.NewGuid().ToString();
        //        entityList.Add(entity.Save());
        //    }

        //    //insert dtos for each entity
        //    List<LocationProductAttributeDto> typeList = new List<LocationProductAttributeDto>();
        //    foreach (Ccm.Model.Entity entity in entityList)
        //    {
        //        Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //        List<Ccm.Model.Product> productList = TestDataHelper.PopulateProductHierarchyWithProducts(hierarchy, 5);

        //        List<LocationDto> locationList = TestDataHelper.InsertLocationDtos(
        //            base.DalFactory, entity.Id, new List<Int32> { 1, 2 }, 2);

        //        typeList =
        //            typeList.Union(
        //            TestDataHelper.InsertLocationProductAttributeDtos(
        //            base.DalFactory, locationList, productList, entity.Id))
        //            .ToList();
        //    }

        //    //fetch back dtos for each entity
        //    foreach (Ccm.Model.Entity entity in entityList)
        //    {
        //        Ccm.Model.LocationProductAttributeList dalList =
        //            Ccm.Model.LocationProductAttributeList.FetchByEntityId(entity.Id);
        //        IEnumerable<LocationProductAttributeDto> sourcelist = typeList.Where(t => t.EntityId == entity.Id);

        //        Assert.AreEqual(sourcelist.Count(), dalList.Count);
        //        foreach (LocationProductAttributeDto sourceDto in sourcelist)
        //        {
        //            Ccm.Model.LocationProductAttribute dalType = dalList
        //                .First(l => l.LocationId == sourceDto.LocationId && l.ProductId == sourceDto.ProductId);
        //        }

        //    }
        //}

        //[Test]
        //public void FetchByLocationIdProductIdCombinations()
        //{
        //    Random rand = new Random(238742);
        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();

        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productList = TestDataHelper.PopulateProductHierarchyWithProducts(entity.Id, hierarchy, 5);

        //    List<LocationDto> locationList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, entity.Id, new List<Int32> { 1, 2 }, 2);

        //    List<LocationProductAttributeDto> sourceList = TestDataHelper.InsertLocationProductAttributeDtos(
        //            base.DalFactory, locationList, productList, entity.Id);


        //    //compile a list of combinations
        //    List<Tuple<Int16, Int32>> combinationsList = new List<Tuple<short, int>>();
        //    foreach (LocationDto locDto in locationList)
        //    {
        //        foreach (Ccm.Model.Product prodDto in productList)
        //        {
        //            combinationsList.Add(new Tuple<short, int>(locDto.Id, prodDto.Id));
        //        }
        //    }
        //    //add a random combination to the end
        //    combinationsList.Add(new Tuple<short, int>(199, 198));

        //    //fetch back dtos for the combinations
        //    Ccm.Model.LocationProductAttributeList dalList =
        //        Ccm.Model.LocationProductAttributeList.FetchByLocationIdProductIdCombinations(entity.Id, combinationsList);

        //    //check each combination has been provided for
        //    Assert.AreEqual(combinationsList.Count, dalList.Count);
        //    foreach (Tuple<Int16, Int32> combi in combinationsList)
        //    {
        //        Ccm.Model.LocationProductAttribute dalType = dalList.First(l => l.LocationId == combi.Item1 && l.ProductId == combi.Item2);
        //    }

        //}

        //#endregion
    }
}
