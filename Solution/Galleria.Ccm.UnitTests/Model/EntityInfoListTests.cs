﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class EntityInfoListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);
            EntityInfoList infoList = EntityInfoList.FetchAllEntityInfos();

            Serialize(infoList);

            Assert.IsTrue(infoList.IsReadOnly);
        }
        #endregion

        #region Factory Methods


        [Test]
        public void FetchAllEntityInfos()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 5);
            EntityInfoList infoList = EntityInfoList.FetchAllEntityInfos();

            foreach (EntityDto entityDto in entityList)
            {
                EntityInfo info = infoList.First(i => i.Id == entityDto.Id);
            }

        }


        #endregion
    }
}
