﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttributeInfoList : TestBase
    {
        //private Int32 _entityId;

        //#region Helpers

        //private List<LocationProductAttributeDto> InsertDtos()
        //{
        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();
        //    _entityId = entity.Id;
        //    //Add Products
        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productDtoList = TestDataHelper.PopulateProductHierarchyWithProducts(hierarchy, 5);
        //    //Add Locations
        //    List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, _entityId, new List<Int32> { 1, 2 }, 2);

        //    List<LocationProductAttributeDto> dtosList =
        //        TestDataHelper.InsertLocationProductAttributeDtos(this.DalFactory, locationDtoList, productDtoList, _entityId);

        //    return dtosList;
        //}

        //#endregion

        //#region Serializable
        ///// <summary>
        ///// Serializable
        ///// </summary>
        //[Test]
        //public void Serializable()
        //{
        //    InsertDtos();

        //    Galleria.Ccm.Model.LocationProductAttributeInfoList model =
        //        Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(_entityId);

        //    Serialize(model);

        //    Assert.IsTrue(model.IsReadOnly);
        //}
        //#endregion

        //#region Factory Methods


        //[Test]
        //public void FetchByEntityId()
        //{
        //    List<LocationProductAttributeDto> entity1DtosList = InsertDtos();

        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();
        //    Int32 entity2Id = entity.Id;
        //    //Add Products
        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productDtoList = TestDataHelper.PopulateProductHierarchyWithProducts(entity2Id, hierarchy, 5);
        //    Ccm.Model.LocationTypeList types = Ccm.Model.LocationTypeList.FetchByEntityId(entity2Id);
        //    //Add Locations
        //    List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, entity2Id, new List<Int32> { 1, 2 }, types.Select(t => t.Id), 2);

        //    List<LocationProductAttributeDto> entity2DtosList =
        //        TestDataHelper.InsertLocationProductAttributeDtos(this.DalFactory, locationDtoList, productDtoList, entity2Id);


        //    Galleria.Ccm.Model.LocationProductAttributeInfoList model1 =
        //        Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(_entityId);
        //    foreach (LocationProductAttributeDto dto in entity1DtosList)
        //    {
        //        Ccm.Model.LocationProductAttributeInfo info = model1.
        //            First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
        //    }

        //    Galleria.Ccm.Model.LocationProductAttributeInfoList model2 =
        //        Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityId(entity2Id);
        //    foreach (LocationProductAttributeDto dto in entity2DtosList)
        //    {
        //        Ccm.Model.LocationProductAttributeInfo info = model2.
        //            First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
        //    }
        //}

        //[Test]
        //public void FetchByEntityIdCriteria()
        //{
        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();
        //    Int32 entity2Id = entity.Id;
        //    //Add Products
        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productDtoList = TestDataHelper.PopulateProductHierarchyWithProducts(entity2Id, hierarchy, 5);
        //    //Add Locations
        //    List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, entity2Id, new List<Int32> { 1, 2 }, 2);

        //    List<LocationProductAttributeDto> entity2DtosList =
        //        TestDataHelper.InsertLocationProductAttributeDtos(this.DalFactory, locationDtoList, productDtoList, entity2Id);

        //    String searchCriteria = String.Format("{0} OR {1}", productDtoList[0].Name, productDtoList[1].Name);
        //    Galleria.Ccm.Model.LocationProductAttributeInfoList model =
        //        Galleria.Ccm.Model.LocationProductAttributeInfoList.FetchByEntityIdSearchCriteria(entity2Id, searchCriteria);
        //    foreach (LocationProductAttributeDto dto in entity2DtosList.Where
        //        (a => a.ProductId == productDtoList[0].Id || a.ProductId == productDtoList[1].Id))
        //    {
        //        Ccm.Model.LocationProductAttributeInfo info = model.
        //            First(i => i.ProductId == dto.ProductId && i.LocationId == dto.LocationId);
        //    }

        //}


        //#endregion
    }
}
