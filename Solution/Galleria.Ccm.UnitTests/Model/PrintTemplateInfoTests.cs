﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-30738 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PrintTemplateInfoTests : TestBase//<PrintTemplateInfo, PrintTemplateInfoDto>
    {
        #region Helpers

        ///// <summary>
        ///// Returns the name of all properties that can be loaded from the dto.
        ///// </summary>
        //private IEnumerable<String> DtoPropertyNames
        //{
        //    get { return base.GetMatchingDtoPropertyNames(); }
        //}

        //protected override void CreateObjectTree(out object root, out PrintTemplateInfo model)
        //{
        //    PrintTemplate template = PrintTemplate.NewPrintTemplate();
        //    template.Name = Guid.NewGuid().ToString();

        //    PrintTemplateInfoGroup group = PrintTemplateInfoGroup.NewPrintTemplateInfoGroup();
        //    group.Name = Guid.NewGuid().ToString();
        //    template.InfoGroups.Add(group);

        //    PrintTemplateInfo section = PrintTemplateInfo.NewPrintTemplateInfo();
        //    group.Infos.Add(section);

        //    root = template;
        //    model = section;
        //}

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Assert.Ignore("TODO");
            //TestSerialize();
        }
        #endregion

        #region Dto

        //[Test, TestCaseSource("DtoPropertyNames")]
        //public void LoadDataTransferObjectProperty(String propertyName)
        //{
        //    Assert.Ignore("TODO");
        //    //TestLoadDataTransferObjectProperty(propertyName);
        //}

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            Assert.Ignore("TODO");
            //TestFetch<PrintTemplate>("FetchById", new String[] { "Id" });
        }

        #endregion
    }
}
