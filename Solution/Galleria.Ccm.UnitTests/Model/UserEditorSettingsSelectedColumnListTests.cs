﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{   [TestFixture]
    public class UserEditorSettingsSelectedColumnListTests
    {
        [Test]
        public void Add_To_List([Values] String fieldplaceholder, SearchColumnType columntype)
        {
            UserEditorSettingsSelectedColumnList list = UserEditorSettingsSelectedColumnList.NewList();
            UserEditorSettingsSelectedColumn column = UserEditorSettingsSelectedColumn.NewUserEditorSettingsSelectedColumn(fieldplaceholder, columntype);
            list.Add(column);
            Assert.IsTrue(list.Any(c => c.Id.Equals(column.Id)));
        }

    }
}
