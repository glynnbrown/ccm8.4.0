﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ConsumerDecisionTreeList : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Setup entity
            Galleria.Ccm.Model.Entity entity = Galleria.Ccm.Model.Entity.NewEntity();
            entity.Name = "entity";
            entity = entity.Save();
            Int32 entityId = entity.Id;

            //insert cdts
            IEnumerable<ConsumerDecisionTreeDto> cdtDtos = TestDataHelper.InsertConsumerDecisionTreeDtos(this.DalFactory, entityId, 10);

            //Fetch cdt list
            Galleria.Ccm.Model.ConsumerDecisionTreeList model =
                Galleria.Ccm.Model.ConsumerDecisionTreeList.FetchByEntityIdConsumerDecisionTreeIds(entityId, cdtDtos.Select(p => p.Id));

            Serialize(model);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void FetchByEntityIdConsumerDecisionTreeIds()
        {
            //Insert data
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 4);

            //Populate with consumer decision trees
            List<ConsumerDecisionTreeDto> consumerDecisionTreeList = new List<ConsumerDecisionTreeDto>();
            consumerDecisionTreeList.AddRange(TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityDtos[0].Id, 5));
            consumerDecisionTreeList.AddRange(TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityDtos[1].Id, 5));
            consumerDecisionTreeList.AddRange(TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityDtos[2].Id, 5));
            consumerDecisionTreeList.AddRange(TestDataHelper.InsertConsumerDecisionTreeDtos(base.DalFactory, entityDtos[3].Id, 5));

            foreach (IGrouping<Int32, ConsumerDecisionTreeDto> group in consumerDecisionTreeList.GroupBy(p => p.EntityId))
            {
                Galleria.Ccm.Model.ConsumerDecisionTreeList returnedList =
                    Galleria.Ccm.Model.ConsumerDecisionTreeList.FetchByEntityIdConsumerDecisionTreeIds(group.Key, group.Select(p => p.Id));

                //Check the correct amount was returned
                Assert.AreEqual(group.Count(), returnedList.Count);

                //Check fetching the first of the group
                returnedList =
                    Galleria.Ccm.Model.ConsumerDecisionTreeList.FetchByEntityIdConsumerDecisionTreeIds(group.Key, new List<Int32>() { group.First().Id });
                Assert.IsNotEmpty(returnedList);

                Assert.AreEqual(group.First().Id, returnedList.First().Id);
                Assert.AreEqual(group.First().UniqueContentReference, returnedList.First().UniqueContentReference);
                Assert.AreEqual(group.First().Name, returnedList.First().Name);
            }
        }

        #endregion
    }
}
