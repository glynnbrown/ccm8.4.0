﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    class blobTests : TestBase
    {

        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.Blob model =
                Galleria.Ccm.Model.Blob.NewBlob();
            Serialize(model);
        }

        [Test]
        public void PropertySetters()
        {
            Galleria.Ccm.Model.Blob model =
                Galleria.Ccm.Model.Blob.NewBlob();

            TestBase.TestPropertySetters<Galleria.Ccm.Model.Blob>(model);
        }

    }
}
