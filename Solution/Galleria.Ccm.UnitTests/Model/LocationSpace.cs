﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion
#region Version History: (CCM 8.0.3)
// V8-29223 : D.Pleasance
//      Removed DateDeleted
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationSpace : TestBase
    {
        #region Test Helper Methods
        private void AssertDtoAndModelAreEqual(LocationSpaceDto dto, Galleria.Ccm.Model.LocationSpace model)
        {
            Assert.AreEqual(dto.LocationId, model.LocationId);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            List<EntityDto> entities = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entities[0].Id;

            Galleria.Ccm.Model.LocationSpace model =
                Galleria.Ccm.Model.LocationSpace.NewLocationSpace(entityId);
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            List<EntityDto> entities = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            Int32 entityId = entities[0].Id;

            // create a new model for rand values
            Galleria.Ccm.Model.LocationSpace valueModel =
                Galleria.Ccm.Model.LocationSpace.NewLocationSpace(entityId);

            //Test Product Group Id
            Int16 testLocationId = 1;
            valueModel.LocationId = testLocationId;
            Assert.AreEqual(testLocationId, valueModel.LocationId);
        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationSpace()
        {
            TestCreateNew<Ccm.Model.LocationSpace>(
                /*isChildObject*/false,
                new String[] { "EntityId" },
                new Object[] { 1 },
                new String[] { });
        }

        #endregion

        #region Data Access

        #region Insert
        [Test]
        public void DataAccess_Insert()
        {
            //Create Test data (2 entitys and 5  for entity 1 and 5  for entity 2)
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            //create a new  list (root object)
            Galleria.Ccm.Model.LocationSpace locationSpace = Galleria.Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);
            locationSpace.LocationId = 1;

            //save object
            locationSpace = locationSpace.Save();

            //Fetch saved 
            LocationSpaceDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    dto = dal.FetchById(locationSpace.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, locationSpace);

        }

        #endregion

        #region Update

        [Test]
        public void DataAccess_Update()
        {
            //Create Test data (2 entitys and 5  for entity 1 and 5  for entity 2)
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            //create a new  list (root object)
            Galleria.Ccm.Model.LocationSpace locationSpace = Galleria.Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);

            //set location id
            locationSpace.LocationId = 1;

            //save 
            locationSpace = locationSpace.Save();

            //Update 
            locationSpace.LocationId = 2;

            //save
            locationSpace = locationSpace.Save();

            //Fetch saved 
            LocationSpaceDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    dto = dal.FetchById(locationSpace.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, locationSpace);
        }

        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            //Create Test data (2 entitys and 5  for entity 1 and 5  for entity 2)
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            //create a new  list (root object)
            Galleria.Ccm.Model.LocationSpace locationSpace = Galleria.Ccm.Model.LocationSpace.NewLocationSpace(entityDtoList[0].Id);

            locationSpace.LocationId = 1;

            //save list
            locationSpace = locationSpace.Save();

            //Fetch saved 
            LocationSpaceDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    dto = dal.FetchById(locationSpace.Id);
                }
            }
            AssertDtoAndModelAreEqual(dto, locationSpace);

            //delete it
            Int32 itemId = locationSpace.Id;
            locationSpace.Delete();
            locationSpace.Save();

            //try to retrieve again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    Assert.Throws<DtoDoesNotExistException>(() => dal.FetchById(itemId));
                }
            }

        }
        #endregion

        #endregion

    }
}
