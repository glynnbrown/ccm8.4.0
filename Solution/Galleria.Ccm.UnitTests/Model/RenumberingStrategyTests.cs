﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class RenumberingStrategyTests : TestBase
    {
        #region Test HelperMethods

        /// <summary>
        ///     Gets the collection of test dtos.
        /// </summary>
        /// <returns>New enumeration of <see cref="RenumberingStrategyDto"/> instances to test.</returns>
        private IEnumerable<RenumberingStrategyDto> GetTestDtos()
        {
            Int32 entityId;
            return GetTestDtos(out entityId);
        }

        /// <summary>
        ///     Gets the collection of test dtos.
        /// </summary>
        /// <param name="entityId"><c>Out</c> The Id of the entity this test instances belong to.</param>
        /// <returns>New enumeration of <see cref="RenumberingStrategyDto"/> instances to test.</returns>
        private IEnumerable<RenumberingStrategyDto> GetTestDtos(out Int32 entityId)
        {
            var entityDtos = TestDataHelper.InsertEntityDtos(DalFactory, 1);
            entityId = entityDtos.First().Id;
            var renumberingStrategyDtos = TestDataHelper.InsertRenumberingStrategyDtos(DalFactory, 5, entityId);
            return renumberingStrategyDtos;
        }

        private RenumberingStrategy SaveNewTestModel()
        {
            var entityDtos = TestDataHelper.InsertEntityDtos(DalFactory, 1);
            var newTestModel = RenumberingStrategy.NewRenumberingStrategy(entityDtos.First().Id);
            newTestModel.Name = "TestModel";

            return newTestModel.Save();
        }

        private RenumberingStrategyDto FetchTestDtoById(int testId)
        {
            RenumberingStrategyDto actual;
            using (var dalContext = DalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IRenumberingStrategyDal>())
            {
                actual = dal.FetchById(testId);
            }
            return actual;
        }

        #endregion
     
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(RenumberingStrategy.NewRenumberingStrategy());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters(RenumberingStrategy.NewRenumberingStrategy());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewRenumberingStrategy()
        {
            const Int32 expected = 0;
            var newTestModel = RenumberingStrategy.NewRenumberingStrategy();

            Assert.AreEqual(expected, newTestModel.EntityId);
            Assert.IsTrue(newTestModel.IsNew);
            Assert.IsFalse(newTestModel.IsChild, "A newly created Renumbering Strategy should be a root object.");
        }

        [Test]
        public void NewRenumberingStrategy_WithEntityId()
        {
            const Int32 expected = 1;
            var newTestModel = RenumberingStrategy.NewRenumberingStrategy(expected);

            Assert.AreEqual(expected, newTestModel.EntityId);
            Assert.IsTrue(newTestModel.IsNew);
            Assert.IsFalse(newTestModel.IsChild, "A newly created Renumbering Strategy should be a root object.");
        }

        [Test]
        public void FetchById()
        {
            foreach (var expected in GetTestDtos())
            {
                var actual = RenumberingStrategy.FetchById(expected.Id);

                AssertHelper.AssertModelObjectsAreEqual(expected, actual);
                Assert.IsFalse(actual.IsChild, "RenumberingStrategy is a root object but was retrieved as a child.");
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            var expected = SaveNewTestModel();

            var actual = FetchTestDtoById(expected.Id);

            AssertHelper.AssertModelObjectsAreEqual(expected, actual);
        }

        [Test]
        public void DataAccess_Update()
        {
            var expected = SaveNewTestModel();

            // Update.
            expected.Name = "UpdatedName";
            expected = expected.Save();
            var actual = FetchTestDtoById(expected.Id);

            AssertHelper.AssertModelObjectsAreEqual(expected, actual);
        }

        [Test]
        public void DataAccess_Delete()
        {
            var expected = SaveNewTestModel();
            var actual = FetchTestDtoById(expected.Id);
            AssertHelper.AssertModelObjectsAreEqual(expected, actual);

            // Delete.
            var deletedId = actual.Id;
            expected.Delete();
            expected.Save();

            actual = FetchTestDtoById(deletedId);

            Assert.IsNotNull(actual.DateDeleted);
        }

        #endregion
    }
}
