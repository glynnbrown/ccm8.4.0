﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26671 : A.Silva 
//      Created.

#endregion

#endregion

using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class CustomColumnFilterTests : TestBase
    {
        #region Code Standards tests

        [Test]
        public void Serializable()
        {
            CustomColumnFilter testModel = CustomColumnFilter.NewCustomColumnFilter(null, null, null);

            TestDelegate code = () => Serialize(testModel);

            Assert.DoesNotThrow(code);
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<CustomColumnFilter>(CustomColumnFilter.NewCustomColumnFilter(null, null, null));
        }

        [Test]
        public void NewCustomColumnFilter()
        {
            TestDelegate code = () => CustomColumnFilter.NewCustomColumnFilter(null, null, null);

            Assert.DoesNotThrow(code);
        }

        #endregion
    }
}