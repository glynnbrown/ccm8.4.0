﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramComparisonTemplateFieldTests : TestBase<PlanogramComparisonTemplateField,PlanogramComparisonTemplateFieldDto>
    {
        #region Helpers

        /// <summary>
        ///     Get all the names of all properties that can be loaded from a dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region General Model Tests

        [Test]
        public void Serializable()
        {
            TestSerialize();
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Data Access

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion
    }
}
