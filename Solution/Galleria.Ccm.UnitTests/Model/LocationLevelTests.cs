﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class LocationLevelTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(LocationLevelDto dto, LocationLevel model)
        {
            Assert.AreEqual(10, typeof(LocationLevelDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.ShapeNo, model.ShapeNo);
            //ParentLevel
            Assert.AreEqual(dto.Colour, model.Colour);
            //dtokey
            //datecreated
            //datelastmodified
            //datedeleted
        }
        #endregion


        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(LocationLevel.NewLocationLevel());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<LocationLevel>(LocationLevel.NewLocationLevel(), new List<String> { "ResolvedColour" });
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationLevel()
        {
            LocationLevel newLevel = LocationLevel.NewLocationLevel();

            Assert.IsTrue(newLevel.IsNew);
            Assert.IsTrue(newLevel.IsChild);
        }

        [Test]
        public void FetchLocationLevel()
        {
            List<LocationLevelDto> dtoList = new List<LocationLevelDto>();

            dtoList.Add(
                new LocationLevelDto()
                {
                    Id = 1,
                    Name = "Test",
                    DateCreated = DateTime.UtcNow,
                    DateLastModified = DateTime.UtcNow,
                    DateDeleted = null,
                    Colour = 2,
                    ParentLevelId = 3,
                    LocationHierarchyId = 4,
                    ShapeNo = 6
                });

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (LocationLevelDto dto in dtoList)
                {
                    var model = LocationLevel.Fetch(dalContext, dto, dtoList);
                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_InsertUpdateDelete()
        {

            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<LocationHierarchyDto> hierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, 1, entityId);
            List<LocationLevelDto> levelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 1);
            List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, levelDtos, 1);

            LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);
            LocationLevel item = LocationLevel.NewLocationLevel();
            item.Name = "testLevel";
            item.Colour = 25;
            item.ShapeNo = 1;
            hierarchy.RootLevel.ChildLevel = item;

            //insert
            hierarchy = hierarchy.Save();
            item = hierarchy.RootLevel.ChildLevel;

            LocationLevelDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }

            //update
            item.Name = "testLevel1";
            item.Colour = 26;
            item.ShapeNo = 2;
            hierarchy = hierarchy.Save();
            item = hierarchy.RootLevel.ChildLevel;

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }

            //update again for concurrency
            item.Name = "testLevel2";
            item.Colour = 27;
            item.ShapeNo = 3;
            hierarchy = hierarchy.Save();
            item = hierarchy.RootLevel.ChildLevel;

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }

            //delete
            hierarchy.RootLevel.ChildLevel = null;
            hierarchy = hierarchy.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    Assert.IsNotNull(dto.DateDeleted);
                }
            }
        }

        #endregion

        #region Other Scenarios

        [Test]
        public void GEM14222_DeletingParentLevelRemovesDeletedChildren()
        {
            //Create a hierarchy with 3 levels
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;

            //create a new hierarchy (will have root level)
            LocationHierarchy hierarchy =
                LocationHierarchy.NewLocationHierarchy(entityId);

            //set level properties
            hierarchy.RootLevel.Name = "testLevel";
            hierarchy.RootLevel.ShapeNo = 2;
            hierarchy.RootLevel.Colour = 22;

            if (hierarchy.RootLevel.ChildLevel == null)
            {
                hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            }
            if (hierarchy.RootLevel.ChildLevel.ChildLevel == null)
            {
                hierarchy.InsertNewChildLevel(hierarchy.RootLevel.ChildLevel);
            }

            Assert.AreEqual(3, hierarchy.EnumerateAllLevels().Count());

            //save and refetch
            hierarchy = hierarchy.Save();
            hierarchy = LocationHierarchy.FetchById(hierarchy.Id);

            //remove level 3 then level 2
            hierarchy.RemoveLevel(hierarchy.RootLevel.ChildLevel.ChildLevel);
            hierarchy.RemoveLevel(hierarchy.RootLevel.ChildLevel);

            //save and refetch
            hierarchy = hierarchy.Save();
            hierarchy = LocationHierarchy.FetchById(hierarchy.Id);

            //check that the hierarchy only has 1 level
            Assert.AreEqual(1, hierarchy.EnumerateAllLevels().Count());
        }

        #endregion
    }
}
