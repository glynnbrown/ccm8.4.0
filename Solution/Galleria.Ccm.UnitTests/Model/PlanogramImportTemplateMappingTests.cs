﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27804 : L.Ineson
//  Created.
// V8-28219 : N.Haywood
//  changed PlanogramImportFileType.Spaceman to SpacemanV9
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramImportTemplateMappingTests : TestBase<PlanogramImportTemplateMapping, PlanogramImportTemplateMappingDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }


        protected override void CreateObjectTree(out object root, out PlanogramImportTemplateMapping model)
        {
            PlanogramImportTemplate item = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1");
            item.Name = Guid.NewGuid().ToString();

            item.Mappings.Clear();

            PlanogramImportTemplateMapping mapping = NewModel();
            mapping.FieldType = PlanogramFieldMappingType.Product;
            mapping.Field = Product.NameProperty.Name;
            mapping.ExternalField = "Field1";

            item.Mappings.Add(mapping);

            root = item;
            model = mapping;
        }


        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_Field_FieldType()
        {
            TestNewFactoryMethod(new String[]{"Field", "FieldType"});
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<PlanogramImportTemplate>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<PlanogramImportTemplate>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<PlanogramImportTemplate>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<PlanogramImportTemplate>();
        }

        #endregion

    }
}
