﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-27630 : I.George
//  Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttributeTests : TestBase<LocationProductAttribute , LocationProductAttributeDto>
    {
        #region Helpers

        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out LocationProductAttribute model)
        {
            LocationProductAttribute locationProductAttribute = LocationProductAttribute.NewLocationProductAttribute(1, 1, 1);
            locationProductAttribute.Description = "Test";

            root = locationProductAttribute;
            model = locationProductAttribute;
        }
     
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }
        #endregion

        #region Factory Methods
        [Test]
        public void NewFactoryMethod_EntityId()
        {
            TestNewFactoryMethod(new String[] {"LocationId","ProductId","EntityId"});
        }
        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }
        #endregion 

        #region Data Access

        [Test]
        public void FetchByLocationIdProductId()
        {
            TestFetch<LocationProductAttribute>("FetchByLocationIdProductId", new String[] { "LocationId", "ProductId" });
        }

        [Test]
        public void Insert()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;
                       
            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId);
            
            LocationProductAttribute item = LocationProductAttribute.NewLocationProductAttribute(locationDtoList[0].Id, prodList[0].Id, entityId);
            item = item.Save();
           
            
            //fetch back the dto
            LocationProductAttributeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    dto = dal.FetchByLocationIdProductId(item.LocationId,item.ProductId);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);
         }
       
        [Test]
        public void Update()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 5)[0].Id;

            // Add product
            ProductHierarchy productHierarchy = ProductHierarchy.NewProductHierarchy(entityId);
            List<Product> prodList = TestDataHelper.PopulateProductHierarchyWithProducts(productHierarchy, 4);
            //Add location
            var locationDtoList = TestDataHelper.InsertLocationDtos(base.DalFactory, 5, entityId);

            LocationProductAttribute item = LocationProductAttribute.NewLocationProductAttribute(locationDtoList[0].Id, prodList[0].Id, entityId);
            item = item.Save();

            //fetch back the dto
            LocationProductAttributeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    dto = dal.FetchByLocationIdProductId(item.LocationId, item.ProductId);
                }
            }
            AssertHelper.AssertModelObjectsAreEqual(dto, item);
        }

        [Test]
        public void Delete()
        {
            TestDelete<LocationProductAttribute>();
        }

        #endregion

        
    }

    
}