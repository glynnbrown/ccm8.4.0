﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM800)
// V8-26159 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PerformanceSelectionTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            PerformanceSelection model = PerformanceSelection.NewPerformanceSelection(1);
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            //model to set on
            PerformanceSelection model = PerformanceSelection.NewPerformanceSelection(1);

            TestPropertySetters<PerformanceSelection>(model);

        }
        #endregion

        #region Factory Methods

        [Test]
        public void NewPerformanceSelection()
        {
            PerformanceSelection model = PerformanceSelection.NewPerformanceSelection(5);
            Assert.AreEqual(5, model.EntityId);
            Assert.IsTrue(model.IsNew);
            Assert.IsTrue(model.IsInitialized);
        }

        #endregion
    }
}
