﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//		Created
// V8-27720 : I.George
// changed the test format and added more tests
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    class AssortmentMinorRevisionTests : TestBase<AssortmentMinorRevision, AssortmentMinorRevisionDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out AssortmentMinorRevision model)
        {
            AssortmentMinorRevision assortmentMinorRevision = AssortmentMinorRevision.NewAssortmentMinorRevision(1);
            assortmentMinorRevision.Name = Guid.NewGuid().ToString();

            IEnumerable<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
            var hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityDtos.First().Id);
            var levelList = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos.First().Id, 1);
            var productGroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelList, 1);
            var productGroup = productGroupDtos.First();
            assortmentMinorRevision.ProductGroupId = productGroup.Id;
            
            root = assortmentMinorRevision;
            model = assortmentMinorRevision;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_EntityId()
        {
            TestNewFactoryMethod(new String[] { "EntityId" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void FetchByEntityIdName()
        {
            TestFetch<AssortmentMinorRevision>("FetchByEntityIdName", new String[] { "EntityId", "Name" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<AssortmentMinorRevision>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<AssortmentMinorRevision>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<AssortmentMinorRevision>();
        }

        #endregion

    }
}
