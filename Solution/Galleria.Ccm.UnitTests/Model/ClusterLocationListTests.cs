﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25692 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ClusterLocationListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster data
            ClusterDto clusterDto = TestDataHelper.InsertSingeClusterData(this.DalFactory, locationDtos);

            //Load cluster scheme model object
            Ccm.Model.ClusterLocationList clusterLocationList = Ccm.Model.ClusterLocationList.FetchByClusterId(clusterDto.Id);

            Serialize(clusterLocationList);
        }
        #endregion

        #region Methods

        [Test]
        public void Add()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster data
            ClusterDto clusterDto = TestDataHelper.InsertSingeClusterData(this.DalFactory, locationDtos);

            //Load cluster scheme model object
            Ccm.Model.ClusterLocationList clusterLocationList = Ccm.Model.ClusterLocationList.FetchByClusterId(clusterDto.Id);

            //Check clusters have been fetched correctly (1 cluster per location)
            Assert.AreEqual(locationDtos.Count(), clusterLocationList.Count, "cluster locations should have been returned for this cluster");

            //Create locationInfo model object
            Ccm.Model.LocationInfoList modelList = Ccm.Model.LocationInfoList.FetchByEntityId(1);
            Ccm.Model.LocationInfo model = modelList[0];

            //Call method
            clusterLocationList.Add(model);

            //Check clusters location has been added
            Assert.AreEqual(locationDtos.Count() + 1, clusterLocationList.Count, "11 cluster locations should now exist in this cluster location list");
        }

        [Test]
        public void Remove()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster data
            ClusterDto clusterDto = TestDataHelper.InsertSingeClusterData(this.DalFactory, locationDtos);

            //Load cluster scheme model object
            Ccm.Model.ClusterLocationList clusterLocationList = Ccm.Model.ClusterLocationList.FetchByClusterId(clusterDto.Id);

            //Check clusters have been fetched correctly (1 cluster per location)
            Assert.AreEqual(locationDtos.Count(), clusterLocationList.Count, "10 cluster locations should have been returned for this cluster");

            //Create locationInfo model object
            Ccm.Model.LocationList modelList = Ccm.Model.LocationList.FetchByEntityId(1);
            Ccm.Model.Location model = modelList[0];

            //Call method
            clusterLocationList.Remove(model);

            //Check clusters location has been removed
            Assert.AreEqual(locationDtos.Count() - 1, clusterLocationList.Count, "9 cluster locations should now exist in this cluster location list");
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewClusterLocationList()
        {
            Ccm.Model.ClusterLocationList model =
                Ccm.Model.ClusterLocationList.NewClusterLocationList();

            Assert.IsEmpty(model);
        }

        [Test]
        public void FetchByClusterId()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster data
            ClusterDto clusterDto = TestDataHelper.InsertSingeClusterData(this.DalFactory, locationDtos);

            //Load cluster scheme model object
            Ccm.Model.ClusterLocationList clusterLocationList = Ccm.Model.ClusterLocationList.FetchByClusterId(clusterDto.Id);

            //Check clusters have been fetched correctly (1 cluster per location)
            Assert.AreEqual(locationDtos.Count(), clusterLocationList.Count, "10 cluster locations should have been returned for this cluster");
        }

        #endregion
    }
}
