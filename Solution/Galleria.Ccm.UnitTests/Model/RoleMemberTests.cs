﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26124 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class RoleMemberTests : TestBase
    {
        #region Test Helper Methods

        private void AssertDtoAndModelAreEqual(RoleMemberDto dto, RoleMember model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.RowVersion, model.RowVersion);
            Assert.AreEqual(dto.UserId, model.UserId);
        }

        private void AssertModelAndModelAreEqual(RoleMember model1, RoleMember model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.RowVersion, model2.RowVersion);
            Assert.AreEqual(model1.UserId, model2.UserId);
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            RoleMember model =
                RoleMember.NewRoleMember(1, 1);
            Serialize(model);
        }

        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            RoleMember model =RoleMember.NewRoleMember(1, 1);
            TestBase.TestPropertySetters<RoleMember>(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewRoleMember()
        {
            RoleMember newRoleMember =
                RoleMember.NewRoleMember(321, 123);

            Assert.AreEqual(321, newRoleMember.UserId);
            Assert.AreEqual(123, newRoleMember.RoleId);

            Assert.IsTrue(newRoleMember.IsNew);
        }

        #endregion

        #region Data Access

        #region Insert

        [Test]
        public void DataAccess_Insert()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;

            //Create parent Role and User
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 1, entityId, false);
            List<UserDto> userList = TestDataHelper.InsertUserDtos(base.DalFactory, 1);

            //Create the RoleMember
            Role role = Role.FetchById(roleList[0].Id);
            RoleMember roleMember =
                RoleMember.NewRoleMember(userList[0].Id, role.Id);
            role.Members.Add(roleMember);

            //save
            role = role.Save();
            roleMember = role.Members[0];

            //Fetch the dto back
            RoleMemberDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
                {
                    dto = dal.FetchById(roleMember.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, roleMember);
        }

        #endregion

        #region Update

        [Test]
        public void DataAccess_Update()
        {
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 2);

            //Create parent Role
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 2, entityList[0].Id, false);
            List<UserDto> userList = TestDataHelper.InsertUserDtos(base.DalFactory, 2);

            //Create the RoleMember
            Role role = Role.FetchById(roleList[0].Id);
            RoleMember roleMember =
                RoleMember.NewRoleMember(userList[0].Id, role.Id);

            role.Members.Add(roleMember);

            //save
            role = role.Save();
            roleMember = role.Members[0];

            //Fetch the dto back
            RoleMemberDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
                {
                    dto = dal.FetchById(roleMember.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, roleMember);
        }

        #endregion

        #region Delete

        [Test]
        public void DataAccess_Delete()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;

            //Create parent Role
            List<RoleDto> roleList = TestDataHelper.InsertRoleDtos(base.DalFactory, 1, entityId, false);
            List<UserDto> userList = TestDataHelper.InsertUserDtos(base.DalFactory, 1);

            //Create the RoleMember
            Role role = Role.FetchById(roleList[0].Id);
            RoleMember roleMember =
                RoleMember.NewRoleMember(userList[0].Id, role.Id);
            role.Members.Add(roleMember);

            //save
            role = role.Save();
            roleMember = role.Members[0];

            //Fetch the dto back
            RoleMemberDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
                {
                    dto = dal.FetchById(roleMember.Id);
                }
            }

            AssertDtoAndModelAreEqual(dto, roleMember);

            //Delete the item
            Int32 Id = roleMember.Id;
            role.Members.Remove(roleMember);
            role.Save();

            //Try to fetch the item again
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
                {
                    Assert.Throws(
                        typeof(DtoDoesNotExistException),
                        () =>
                        {
                            dto = dal.FetchById(Id);
                        });
                }
            }
        }

        #endregion

        #endregion
    }
}
