﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27059 : J.Pickup
//  Created.
// V8-27056 : J.Pickup
//		Reviewed.
#endregion
#endregion

using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Collections.Generic;
using System;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class AssortmentTests : TestBase<Assortment, AssortmentDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_EntityId()
        {
            TestNewFactoryMethod(new String[] { "EntityId" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion
    }
}