﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class PlanogramComparisonTemplateTests : TestBase<PlanogramComparisonTemplate, PlanogramComparisonTemplateDto>
    {
        private readonly List<String> _propertiesToExclude = new List<String> {"RowVersion"};

        #region Helpers

        /// <summary>
        ///     Get all the names of all properties that can be loaded from a dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return GetMatchingDtoPropertyNames().Except(_propertiesToExclude); }
        }

        #endregion

        #region General Model Tests

        [Test]
        public void Serializable()
        {
            TestSerialize();
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters(_propertiesToExclude);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            const Int32 entityId = 1;

            PlanogramComparisonTemplate newItem = PlanogramComparisonTemplate.NewPlanogramComparisonTemplate(entityId);

            Assert.AreEqual(newItem.EntityId, entityId);

            Assert.IsTrue(newItem.IsNew);
        }

        #endregion

        #region Data Access

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion
    }
}
