﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629: A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class ClusterListTests : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster scheme data
            ClusterSchemeDto clusterSchemeDto = TestDataHelper.InsertSingeClusterSchemeData(this.DalFactory, locationDtos);

            //Call factory method
            Ccm.Model.ClusterList clusterList = Ccm.Model.ClusterList.FetchByClusterSchemeId(clusterSchemeDto.Id);

            Serialize(clusterList);
        }
        #endregion


        #region Factory Methods

        [Test]
        public void NewClusterList()
        {
            //Cluster scheme is created with a new cluster list
            Ccm.Model.ClusterScheme clusterScheme = Ccm.Model.ClusterScheme.NewClusterScheme(1);

            Assert.IsTrue(clusterScheme.Clusters.IsChild);
        }

        [Test]
        public void FetchByClusterSchemeId()
        {
            //Insert location data
            IEnumerable<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Insert cluster scheme data
            ClusterSchemeDto clusterSchemeDto = TestDataHelper.InsertSingeClusterSchemeData(this.DalFactory, locationDtos);

            //Call factory method
            Ccm.Model.ClusterList clusterList = Ccm.Model.ClusterList.FetchByClusterSchemeId(clusterSchemeDto.Id);

            //Check clusters have been fetched correctly (1 cluster per location)
            Assert.AreEqual(locationDtos.Count(), clusterList.Count, "clusters should have been returned for this cluster scheme");
        }

        #endregion
    }
}
