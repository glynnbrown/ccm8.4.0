﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ProductLevelTests : TestBase
    {
        #region Test Helper Methods
        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        public static void AssertDtoAndModelAreEqual(ProductLevelDto dto, ProductLevel model)
        {
            Assert.AreEqual(10, typeof(ProductLevelDto).GetProperties().Count());

            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.ShapeNo, model.ShapeNo);
            //ParentLevel
            Assert.AreEqual(dto.Colour, model.Colour);
            //dtokey
            //datecreated
            //datelastmodified
            //datedeleted
        }
        #endregion


        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(ProductLevel.NewProductLevel());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<ProductLevel>(ProductLevel.NewProductLevel(), new List<String>{"ResolvedColour"});
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewProductLevel()
        {
            ProductLevel newLevel = ProductLevel.NewProductLevel();

            Assert.IsTrue(newLevel.IsNew);
            Assert.IsTrue(newLevel.IsChild);
        }

        [Test]
        public void FetchProductLevel()
        {
            List<ProductLevelDto> dtoList = new List<ProductLevelDto>();

            dtoList.Add(
                new ProductLevelDto()
                {
                    Id = 1,
                    Name = "Test",
                    DateCreated = DateTime.UtcNow,
                    DateLastModified = DateTime.UtcNow,
                    DateDeleted = null,
                    Colour = 2,
                    ParentLevelId = 3,
                    ProductHierarchyId = 4,
                    ShapeNo = 6
                });

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                foreach (ProductLevelDto dto in dtoList)
                {
                    var model = ProductLevel.Fetch(dalContext, dto, dtoList);
                    AssertDtoAndModelAreEqual(dto, model);
                }
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_InsertUpdateDelete()
        {

            Int32 entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 1, entityId);
            List<ProductLevelDto> levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 1);
            List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, levelDtos, 1);

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(entityId);
            ProductLevel item = ProductLevel.NewProductLevel();
            item.Name = "testLevel";
            item.Colour = 25;
            item.ShapeNo = 1;
            hierarchy.RootLevel.ChildLevel = item;

            //insert
            hierarchy = hierarchy.Save();
            item = hierarchy.RootLevel.ChildLevel;

            ProductLevelDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }

            //update
            item.Name = "testLevel1";
            item.Colour = 26;
            item.ShapeNo = 2;
            hierarchy = hierarchy.Save();
            item = hierarchy.RootLevel.ChildLevel;

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }

            //update again for concurrency
            item.Name = "testLevel2";
            item.Colour = 27;
            item.ShapeNo = 3;
            hierarchy = hierarchy.Save();
            item = hierarchy.RootLevel.ChildLevel;

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    AssertDtoAndModelAreEqual(dto, item);
                }
            }

            //delete
            hierarchy.RootLevel.ChildLevel = null;
            hierarchy = hierarchy.Save();

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    dto = dal.FetchById(item.Id);
                    Assert.IsNotNull(dto.DateDeleted);
                }
            }
        }

        #endregion
    }
}
