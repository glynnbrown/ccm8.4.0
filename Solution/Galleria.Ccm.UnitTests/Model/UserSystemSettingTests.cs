﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created
#endregion
#endregion


using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class UserSystemSettingTests : TestBase
    {
        #region Serializable

        [Test]
        public void Serializable()
        {
            UserSystemSetting model = UserSystemSetting.NewUserSystemSetting();

            Serialize(model);
        }

        #endregion

        #region Property Setters

        [Test]
        public void PropertySetters()
        {
            //Create model
            UserSystemSetting model = UserSystemSetting.NewUserSystemSetting();

            //DisplayLanguage
            String language = "en-us";
            model.DisplayLanguage = language;
            Assert.AreEqual(language, model.DisplayLanguage);

            //DisplayCEIPWindow
            Boolean displayCEIP = false;
            model.DisplayCEIPWindow = displayCEIP;
            Assert.AreEqual(displayCEIP, model.DisplayCEIPWindow);

            //SendCEIPInformation
            Boolean sendCEIP = false;
            model.SendCEIPInformation = sendCEIP;
            Assert.AreEqual(sendCEIP, model.SendCEIPInformation);

            //IsDatabaseSelectionRemembered
            Boolean databaseRemembered = true;
            model.IsDatabaseSelectionRemembered = databaseRemembered;
            Assert.AreEqual(databaseRemembered, model.IsDatabaseSelectionRemembered);

            //IsEntitySelectionRemembered
            Boolean entityRemembered = true;
            model.IsEntitySelectionRemembered = entityRemembered;
            Assert.AreEqual(entityRemembered, model.IsEntitySelectionRemembered);

            // PlanAssignmentViewBy
            PlanAssignmentViewType viewByType = PlanAssignmentViewType.ViewCategory;
            model.PlanAssignmentViewBy = viewByType;
            Assert.AreEqual(viewByType, model.PlanAssignmentViewBy);
        }

        #endregion
    }
}
