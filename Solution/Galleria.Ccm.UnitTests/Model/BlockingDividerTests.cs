﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class BlockingDividerTests : TestBase<BlockingDivider, BlockingDividerDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out BlockingDivider model)
        {
            Blocking blocking = Blocking.NewBlocking(1);
            blocking.Name = Guid.NewGuid().ToString();
            blocking.Description = "Test";
            blocking.ProductGroupId = 1;

            BlockingDivider divider = NewModel();
            blocking.Dividers.Add(divider);
            divider.Level = 1;

            root = blocking;
            model = divider;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        [Test]
        public void NewFactoryMethod_LevelTypeXYLength()
        {
            TestNewFactoryMethod(new String[] { "Level", "Type", "X", "Y", "Length" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void Fetch()
        {
            TestFetch<Blocking>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<Blocking>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<Blocking>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<Blocking>();
        }

        #endregion
    }
}
