﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using System.Windows.Media;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class BlockingTests : TestBase<Blocking, BlockingDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        protected override void CreateObjectTree(out object root, out Blocking model)
        {
            Blocking blocking = Blocking.NewBlocking(1);
            blocking.Name = Guid.NewGuid().ToString();
            blocking.Description = "Test";
            blocking.ProductGroupId = 1;

            root = blocking;
            model = blocking;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_EntityId()
        {
            TestNewFactoryMethod(new String[] { "EntityId" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Data Access

        [Test]
        public void FetchById()
        {
            TestFetch<Blocking>("FetchById", new String[] { "Id" });
        }

        [Test]
        public void FetchByEntityIdName()
        {
            TestFetch<Blocking>("FetchByEntityIdName", new String[] { "EntityId", "Name" });
        }

        [Test]
        public void Insert()
        {
            TestInsert<Blocking>();
        }

        [Test]
        public void Update()
        {
            TestUpdate<Blocking>();
        }

        [Test]
        public void Delete()
        {
            TestDelete<Blocking>();
        }

        #endregion

        #region Blocking Behaviours

        private Blocking CreateBehaviourTestData()
        {
            Blocking item = Blocking.NewBlocking(1);
            item.Name = "Test";
            item.ProductGroupId = 1;

            item.Height = 200;
            item.Width = 400;

            //add groups
            BlockingGroup group1 = item.Groups.AddNew();
            group1.Colour = CommonHelper.ColorToInt(Colors.Orange);

            BlockingGroup group2 = item.Groups.AddNew();
            group2.Colour = CommonHelper.ColorToInt(Colors.Green);

            BlockingGroup group3 = item.Groups.AddNew();
            group3.Colour = CommonHelper.ColorToInt(Colors.Red);

            BlockingGroup group4 = item.Groups.AddNew();
            group4.Colour = CommonHelper.ColorToInt(Colors.Purple);

            //add dividers
            BlockingDivider divider1 = BlockingDivider.NewBlockingDivider((Byte)1, PlanogramBlockingDividerType.Vertical, 0.45F, 0, 1);
            item.Dividers.Add(divider1);

            BlockingDivider divider2 = BlockingDivider.NewBlockingDivider((Byte)1, PlanogramBlockingDividerType.Vertical, 0.80F, 0, 1);
            item.Dividers.Add(divider2);

            BlockingDivider divider3 = BlockingDivider.NewBlockingDivider((Byte)2, PlanogramBlockingDividerType.Horizontal, 0.80F, 0.50F, 0.2F);
            item.Dividers.Add(divider3);

            //add locations
            BlockingLocation loc1 = item.Locations.Add(group1);
            loc1.BlockingDividerRightId = divider1.Id;

            BlockingLocation loc2 = item.Locations.Add(group2);
            loc2.BlockingDividerLeftId = divider1.Id;
            loc2.BlockingDividerRightId = divider2.Id;

            BlockingLocation loc3 = item.Locations.Add(group3);
            loc3.BlockingDividerLeftId = divider2.Id;
            loc3.BlockingDividerBottomId = divider3.Id;

            BlockingLocation loc4 = item.Locations.Add(group4);
            loc4.BlockingDividerLeftId = divider2.Id;
            loc4.BlockingDividerTopId = divider3.Id;

            return item;
        }

        [Test]
        public void Behaviour_CreateFullAndSave()
        {
            Blocking item = Blocking.NewBlocking(1);
            item.Name = Guid.NewGuid().ToString();
            item.ProductGroupId = 1;

            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            Action<Blocking> checkBlocking =
            (b) =>
            {
                Assert.AreEqual(3, b.Dividers.Count);

                BlockingDivider divider1 = b.Dividers[0];
                BlockingDivider divider2 = b.Dividers[1];
                BlockingDivider divider3 = b.Dividers[2];

                //check dividers
                Assert.AreEqual(0.45F, divider1.X);
                Assert.AreEqual(0F, divider1.Y);
                Assert.AreEqual(1F, divider1.Length);

                Assert.AreEqual(0.8F, divider2.X);
                Assert.AreEqual(0F, divider2.Y);
                Assert.AreEqual(1F, divider2.Length);

                Assert.AreEqual(0.8F, divider3.X);
                Assert.AreEqual(0.5F, divider3.Y);
                Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(divider3.Length, 5)));

                //check groups
                Assert.AreEqual(4, b.Groups.Count, "Should have 4 groups");
                Assert.AreEqual(4, b.Groups.Select(g => g.Colour).Distinct().Count(),
                    "Each group should have a distinct colour");

                //check locations.
                Assert.AreEqual(4, b.Locations.Count, "Should have 4 locations");

                BlockingLocation loc1 = b.Locations[0];
                BlockingLocation loc2 = b.Locations[1];
                BlockingLocation loc3 = b.Locations[2];
                BlockingLocation loc4 = b.Locations[3];

                Assert.AreEqual(b.Groups[0].Id, loc1.BlockingGroupId);
                Assert.AreEqual(divider1.Id, loc1.BlockingDividerRightId);
                Assert.AreEqual(0, loc1.X);
                Assert.AreEqual(0, loc1.Y);
                Assert.AreEqual(0.45F, loc1.Width);
                Assert.AreEqual(1, loc1.Height);

                Assert.AreEqual(b.Groups[1].Id, loc2.BlockingGroupId);
                Assert.AreEqual(divider1.Id, loc2.BlockingDividerLeftId);
                Assert.AreEqual(divider2.Id, loc2.BlockingDividerRightId);
                Assert.AreEqual(0.45F, loc2.X);
                Assert.AreEqual(0, loc2.Y);
                Assert.AreEqual(0.35F, Convert.ToSingle(Math.Round(loc2.Width, 5)));
                Assert.AreEqual(1, Convert.ToSingle(Math.Round(loc2.Height, 5)));

                Assert.AreEqual(b.Groups[2].Id, loc3.BlockingGroupId);
                Assert.AreEqual(divider2.Id, loc3.BlockingDividerLeftId);
                Assert.AreEqual(divider3.Id, loc3.BlockingDividerTopId);
                Assert.AreEqual(0.8F, loc3.X);
                Assert.AreEqual(0F, loc3.Y);
                Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc3.Width, 5)));
                Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc3.Height, 5)));

                Assert.AreEqual(b.Groups[3].Id, loc4.BlockingGroupId);
                Assert.AreEqual(divider2.Id, loc4.BlockingDividerLeftId);
                Assert.AreEqual(divider3.Id, loc4.BlockingDividerBottomId);
                Assert.AreEqual(0.8F, loc4.X);
                Assert.AreEqual(0.5, loc4.Y);
                Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc4.Width, 5)));
                Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc4.Height, 5)));
            };

            //execute the check
            checkBlocking(item);


            //Save
            item = item.Save();

            //check
            checkBlocking(item);

            //re-fetch
            item = Blocking.FetchById(item.Id);

            //check
            checkBlocking(item);
        }

        [Test]
        public void Behaviour_MoveDividerVertical()
        {
            Blocking blocking = CreateBehaviourTestData();

            //SIMPLE MOVING:

            //move the first divider left
            BlockingDivider div = blocking.Dividers[0];
            Assert.AreEqual(PlanogramBlockingDividerType.Vertical, div.Type);
            div.MoveTo(0.40F);

            //check 
            Assert.AreEqual(0.40F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);

            //move right
            div.MoveTo(0.50F);
            Assert.AreEqual(0.50F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);

            //HIT LIMITS:

            //try to move beyond next divider - should get stopped.
            div.MoveTo(0.9F);
            Assert.AreEqual(0.79F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);

            //check the divider we hit has not moved.
            BlockingDivider divider2 = blocking.Dividers[1];
            Assert.AreEqual(0.8F, divider2.X);
            Assert.AreEqual(0, divider2.Y);
            Assert.AreEqual(1F, divider2.Length);

            //try to move to 0 - should get stopped
            div.MoveTo(0);
            Assert.AreEqual(0.01F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);
        }

        [Test]
        public void Behaviour_MoveDividerHorizontal()
        {
            Blocking blocking = CreateBehaviourTestData();

            //move the divider down
            BlockingDivider div = blocking.Dividers[2];
            Assert.AreEqual(PlanogramBlockingDividerType.Horizontal, div.Type);

            //SIMPLE MOVING:
            div.MoveTo(0.40F);

            //check 
            Assert.AreEqual(0.40F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);

            //check that the adjacent vert divider was not affected
            BlockingDivider adjDiv = blocking.Dividers[1];
            Assert.AreEqual(0.8F, adjDiv.X);
            Assert.AreEqual(0F, adjDiv.Y);
            Assert.AreEqual(1F, adjDiv.Length);

            //move up
            div.MoveTo(0.70F);
            Assert.AreEqual(0.70F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);

            //check that the adjacent vert divider was not affected
            Assert.AreEqual(0.8F, adjDiv.X);
            Assert.AreEqual(0F, adjDiv.Y);
            Assert.AreEqual(1F, adjDiv.Length);


            //HIT LIMITS:

            //move to 0 - should get stopped
            div.MoveTo(0);
            Assert.AreEqual(0.01F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);

            //move to bottom - should get stopped
            div.MoveTo(1);
            Assert.AreEqual(0.99F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);
        }

        [Test]
        public void Behaviour_MoveDividerDifferentVerticalBands()
        {
            Blocking blocking = CreateBehaviourTestData();

            BlockingDivider div1 = blocking.Dividers[0];
            BlockingDivider div2 = blocking.Dividers[1];
            BlockingDivider div3 = blocking.Dividers[2];

            //add a new horizontal divider in the vert block before
            BlockingDivider div4 = blocking.Dividers.Add(div1.X, 0.6F, PlanogramBlockingDividerType.Horizontal);

            //try to move div 4 past div 3
            // should be allowed as div 3 is not in same vert band
            div4.MoveTo(0.3F);
            Assert.AreEqual(0.3F, div4.Y);

            //div 3 should be unaffected
            Assert.AreEqual(0.5F, div3.Y);
        }

        [Test]
        public void Behaviour_AddDividerHorizontal()
        {
            Blocking blocking = CreateBehaviourTestData();
            BlockingDivider div1 = blocking.Dividers[0];
            BlockingDivider div2 = blocking.Dividers[1];
            BlockingDivider div3 = blocking.Dividers[2];

            //add a new horizontal divider
            BlockingDivider div4 = blocking.Dividers.Add(blocking.Dividers[0].X, 0.6F, PlanogramBlockingDividerType.Horizontal);
            Assert.AreEqual((Byte)2, div4.Level);

            Assert.AreEqual(blocking.Groups.Count, 5, "A new group should have been added");
            Assert.AreEqual(blocking.Locations.Count, 5, "A new loc should have been added");

            //check the new loc
            BlockingLocation newLoc = blocking.Locations[4];
            Assert.AreEqual(blocking.Groups[4].Id, newLoc.BlockingGroupId);
            Assert.AreEqual(div1.Id, newLoc.BlockingDividerLeftId);
            Assert.AreEqual(div2.Id, newLoc.BlockingDividerRightId);
            Assert.AreEqual(null, newLoc.BlockingDividerTopId);
            Assert.AreEqual(div4.Id, newLoc.BlockingDividerBottomId);

            //check the existing loc
            BlockingLocation loc2 = blocking.Locations[1];
            Assert.AreEqual(blocking.Groups[1].Id, loc2.BlockingGroupId);
            Assert.AreEqual(div1.Id, loc2.BlockingDividerLeftId);
            Assert.AreEqual(div2.Id, loc2.BlockingDividerRightId);
            Assert.AreEqual(div4.Id, loc2.BlockingDividerTopId);
            Assert.AreEqual(null, loc2.BlockingDividerBottomId);
        }

        [Test]
        public void Behaviour_AddDividerVertical()
        {
            Blocking blocking = CreateBehaviourTestData();
            BlockingDivider div1 = blocking.Dividers[0];
            BlockingDivider div2 = blocking.Dividers[1];
            BlockingDivider div3 = blocking.Dividers[2];

            //Add a new vertical divider
            BlockingDivider div4 = blocking.Dividers.Add(0.6F, 0, PlanogramBlockingDividerType.Vertical);
            Assert.AreEqual((Byte)1, div4.Level);

            Assert.AreEqual(blocking.Groups.Count, 5, "A new group should have been added");
            Assert.AreEqual(blocking.Locations.Count, 5, "A new loc should have been added");

            //check the new loc
            BlockingLocation newLoc = blocking.Locations[4];
            Assert.AreEqual(blocking.Groups[4].Id, newLoc.BlockingGroupId);
            Assert.AreEqual(div4.Id, newLoc.BlockingDividerLeftId);
            Assert.AreEqual(div2.Id, newLoc.BlockingDividerRightId);
            Assert.AreEqual(null, newLoc.BlockingDividerTopId);
            Assert.AreEqual(null, newLoc.BlockingDividerBottomId);

            //check the existing loc
            BlockingLocation loc2 = blocking.Locations[1];
            Assert.AreEqual(blocking.Groups[1].Id, loc2.BlockingGroupId);
            Assert.AreEqual(div1.Id, loc2.BlockingDividerLeftId);
            Assert.AreEqual(div4.Id, loc2.BlockingDividerRightId);
            Assert.AreEqual(null, loc2.BlockingDividerTopId);
            Assert.AreEqual(null, loc2.BlockingDividerBottomId);
        }

        [Test]
        public void Behaviour_RemoveDivider1()
        {
            Blocking b = Blocking.NewBlocking(1);
            b.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            BlockingDivider div1 = b.Dividers[0];
            BlockingDivider div2 = b.Dividers[1];
            BlockingDivider div3 = b.Dividers[2];

            BlockingLocation loc1 = b.Locations[0];
            BlockingLocation loc2 = b.Locations[1];
            BlockingLocation loc3 = b.Locations[2];
            BlockingLocation loc4 = b.Locations[3];

            //check initial state
            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.45F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);

            Assert.AreEqual(0.45F, loc2.X);
            Assert.AreEqual(0, loc2.Y);
            Assert.AreEqual(0.35F, loc2.Width);
            Assert.AreEqual(1, loc2.Height);

            Assert.AreEqual(0.8F, loc3.X);
            Assert.AreEqual(0F, loc3.Y);
            Assert.AreEqual(0.2F, loc3.Width);
            Assert.AreEqual(0.5F, loc3.Height);

            Assert.AreEqual(0.8F, loc4.X);
            Assert.AreEqual(0.5, loc4.Y);
            Assert.AreEqual(0.2F, loc4.Width);
            Assert.AreEqual(0.5F, loc4.Height);


            //Remove divider 1
            b.Dividers.Remove(div1);
            Assert.AreEqual(2, b.Dividers.Count);
            Assert.AreEqual(3, b.Groups.Count);
            Assert.AreEqual(3, b.Locations.Count);

            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.8F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);
            Assert.AreEqual(div2.Id, loc1.BlockingDividerRightId, "div2 should not be loc1 right divider");

            //loc2 should have been removed.

            Assert.AreEqual(0.8F, loc3.X);
            Assert.AreEqual(0F, loc3.Y);
            Assert.AreEqual(0.2F, loc3.Width);
            Assert.AreEqual(0.5F, loc3.Height);

            Assert.AreEqual(0.8F, loc4.X);
            Assert.AreEqual(0.5, loc4.Y);
            Assert.AreEqual(0.2F, loc4.Width);
            Assert.AreEqual(0.5F, loc4.Height);
        }

        [Test]
        public void Behaviour_RemoveDivider2()
        {
            Blocking b = Blocking.NewBlocking(1);
            b.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            BlockingDivider div1 = b.Dividers[0];
            BlockingDivider div2 = b.Dividers[1];
            BlockingDivider div3 = b.Dividers[2];

            BlockingLocation loc1 = b.Locations[0];
            BlockingLocation loc2 = b.Locations[1];
            BlockingLocation loc3 = b.Locations[2];
            BlockingLocation loc4 = b.Locations[3];

            //remove divider 2
            b.Dividers.Remove(b.Dividers[1]);
            Assert.AreEqual(1, b.Dividers.Count);
            Assert.AreEqual(2, b.Groups.Count);
            Assert.AreEqual(2, b.Locations.Count);

            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.45F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);

            Assert.AreEqual(0.45F, loc2.X);
            Assert.AreEqual(0, loc2.Y);
            Assert.AreEqual(0.55F, loc2.Width);
            Assert.AreEqual(1, loc2.Height);
            Assert.AreEqual(loc2.BlockingDividerRightId, null, "loc2 should not be at the edge");

            //loc3 - removed
            //loc4 - removed
        }

        [Test]
        public void Behaviour_RemoveDivider3()
        {
            Blocking b = Blocking.NewBlocking(1);
            b.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            BlockingDivider div1 = b.Dividers[0];
            BlockingDivider div2 = b.Dividers[1];
            BlockingDivider div3 = b.Dividers[2];

            BlockingLocation loc1 = b.Locations[0];
            BlockingLocation loc2 = b.Locations[1];
            BlockingLocation loc3 = b.Locations[2];
            BlockingLocation loc4 = b.Locations[3];

            //remove divider 3
            b.Dividers.Remove(b.Dividers[2]);
            Assert.AreEqual(b.Dividers.Count, 2);
            Assert.AreEqual(3, b.Groups.Count);
            Assert.AreEqual(3, b.Locations.Count);

            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.45F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);

            Assert.AreEqual(0.45F, loc2.X);
            Assert.AreEqual(0, loc2.Y);
            Assert.AreEqual(0.35F, loc2.Width);
            Assert.AreEqual(1, loc2.Height);

            Assert.AreEqual(0.8F, loc3.X);
            Assert.AreEqual(0F, loc3.Y);
            Assert.AreEqual(0.2F, loc3.Width);
            Assert.AreEqual(1F, loc3.Height);
            Assert.AreEqual(loc3.BlockingDividerTopId, null, "loc 3 should now hit the top");

            //loc 4 removed
        }

        [Test]
        public void Behaviour_MoveL2HorizResizesL3Vert()
        {
            Blocking b = Blocking.NewBlocking(1);
            b.Name = Guid.NewGuid().ToString();

            b.Dividers.Add(0.50F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.50F, 0.6F, PlanogramBlockingDividerType.Horizontal);
            b.Dividers.Add(0.70F, 0, PlanogramBlockingDividerType.Vertical);

            BlockingDivider divider1 = b.Dividers[0];
            BlockingDivider divider2 = b.Dividers[1];
            BlockingDivider divider3 = b.Dividers[2];

            Assert.AreEqual(1, divider1.Level);
            Assert.AreEqual(2, divider2.Level);
            Assert.AreEqual(3, divider3.Level);

            //move div2 down
            divider2.MoveTo(0.5F);

            Assert.AreEqual(0.7F, divider3.X, "Divider 3 should not have moved.");
            Assert.AreEqual(0.5F, divider3.Length, "Divider 3 should have resized");

        }

        [Test]
        public void Behaviour_MoveVertResizesAdjacent()
        {
            Blocking b = Blocking.NewBlocking(1);
            b.Name = Guid.NewGuid().ToString();


            //V30, V60, h0 75, h0 50
            b.Dividers.Add(0.30F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.60F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0, 0.75F, PlanogramBlockingDividerType.Horizontal);
            b.Dividers.Add(0.3F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            BlockingDivider divider1 = b.Dividers[0];
            BlockingDivider divider2 = b.Dividers[1];
            BlockingDivider divider3 = b.Dividers[2];
            BlockingDivider divider4 = b.Dividers[3];

            //move v1 to 70
            divider1.MoveTo(0.7F);

            Assert.AreEqual(0.59F, divider3.Length);
            Assert.AreEqual(0.59F, divider4.X, 5);
            Assert.AreEqual(0.01F, divider4.Length);


            //move v1 to 50
            divider1.MoveTo(0.5F);

            Assert.AreEqual(0.5F, divider3.Length);
            Assert.AreEqual(0.5F, divider4.X);
            Assert.AreEqual(0.1F, divider4.Length);
        }

        [Test]
        public void Behaviour_GEM27422DeleteTShape()
        {
            Blocking b = Blocking.NewBlocking(1);
            b.Name = Guid.NewGuid().ToString();

            //add dividers to create the shape
            BlockingDivider d1 = b.Dividers.Add(0.25F, 0, PlanogramBlockingDividerType.Vertical);
            BlockingDivider d2 = b.Dividers.Add(0.25F, 0.75F, PlanogramBlockingDividerType.Horizontal);
            BlockingDivider d3 = b.Dividers.Add(0.25F, 0.5F, PlanogramBlockingDividerType.Horizontal);
            BlockingDivider d4 = b.Dividers.Add(0.6F, 0, PlanogramBlockingDividerType.Vertical);

            BlockingLocation l1 = b.Locations[0];
            BlockingLocation l2 = b.Locations[1];
            BlockingLocation l3 = b.Locations[2];
            BlockingLocation l4 = b.Locations[3];
            BlockingLocation l5 = b.Locations[4];


            //remove divider 3
            b.Dividers.Remove(d3);

            //check d4
            Assert.AreEqual(0.75F, d4.Length, "The length of d4 should have increased");

            Assert.AreEqual(4, b.Locations.Count, "A location should have been removed");
            Assert.AreEqual(d2, l2.GetTopDivider());
            Assert.AreEqual(d2, l5.GetTopDivider());
        }

        #endregion
    }
}
