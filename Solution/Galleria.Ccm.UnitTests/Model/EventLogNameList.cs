﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class EventLogNameList : TestBase
    {
        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.EventLogNameList model =
                Galleria.Ccm.Model.EventLogNameList.NewList();
            Serialize(model);
        }
        #endregion

        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(EventLogNameDto dto, Galleria.Ccm.Model.EventLogName model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.Name, model.Name);
        }

        private void AssertModelandModelAreEqual(Galleria.Ccm.Model.EventLogName model1, Galleria.Ccm.Model.EventLogName model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.Name, model2.Name);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewList()
        {
            Galleria.Ccm.Model.EventLogNameList model = Galleria.Ccm.Model.EventLogNameList.NewList();

            Assert.IsFalse(model.IsChild, "the model should be a root");
            Assert.IsEmpty(model, "the model should be empty");
        }

        [Test]
        public void FetchAll()
        {
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                List<EventLogNameDto> eventLogNameHelperList = TestDataHelper.InsertEventLogNameDtos(dalContext, 5);
                Galleria.Ccm.Model.EventLogNameList eventLogNameList = Galleria.Ccm.Model.EventLogNameList.FetchAll();

                Assert.AreEqual(eventLogNameHelperList.Count, eventLogNameList.Count);

                foreach (EventLogNameDto eventLogNameDto in eventLogNameHelperList)
                {
                    Ccm.Model.EventLogName eventLog = eventLogNameList.First(i => i.Id == eventLogNameDto.Id);
                    AssertDtoAndModelAreEqual(eventLogNameDto, eventLog);
                }
            }

        }

        #endregion
    }
}
