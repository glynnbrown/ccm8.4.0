﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class LocationProductAttribute : TestBase
    {
        //#region Test Helper Methods

        ///// <summary>
        ///// Asserts that the properties of a model
        ///// match the dto it has been loaded from
        ///// </summary>
        ///// <param name="dto">The dto to compare</param>
        ///// <param name="model">The model to compare</param>
        //private void AssertDtoAndModelAreEqual(LocationProductAttributeDto dto, Galleria.Ccm.Model.LocationProductAttribute model)
        //{
        //    Assert.AreEqual(dto.ProductId, model.ProductId);
        //    Assert.AreEqual(dto.LocationId, model.LocationId);
        //    Assert.AreEqual(dto.EntityId, model.EntityId);
        //    Assert.AreEqual(dto.GTIN, model.GTIN);
        //    Assert.AreEqual(dto.Description, model.Description);
        //    Assert.AreEqual(dto.Height, model.Height);
        //    Assert.AreEqual(dto.Width, model.Width);
        //    Assert.AreEqual(dto.Depth, model.Depth);
        //    Assert.AreEqual(dto.CasePackUnits, model.CasePackUnits);
        //    Assert.AreEqual(dto.CaseHigh, model.CaseHigh);
        //    Assert.AreEqual(dto.CaseWide, model.CaseWide);
        //    Assert.AreEqual(dto.CaseDeep, model.CaseDeep);
        //    Assert.AreEqual(dto.CaseHeight, model.CaseHeight);
        //    Assert.AreEqual(dto.CaseDepth, model.CaseDepth);
        //    Assert.AreEqual(dto.CaseWidth, model.CaseWidth);
        //    Assert.AreEqual(dto.DaysOfSupply, model.DaysOfSupply);
        //    Assert.AreEqual(dto.MinDeep, model.MinDeep);
        //    Assert.AreEqual(dto.MaxDeep, model.MaxDeep);
        //    Assert.AreEqual(dto.TrayPackUnits, model.TrayPackUnits);
        //    Assert.AreEqual(dto.TrayHigh, model.TrayHigh);
        //    Assert.AreEqual(dto.TrayWide, model.TrayWide);
        //    Assert.AreEqual(dto.TrayDeep, model.TrayDeep);
        //    Assert.AreEqual(dto.TrayHeight, model.TrayHeight);
        //    Assert.AreEqual(dto.TrayDepth, model.TrayDepth);
        //    Assert.AreEqual(dto.TrayWidth, model.TrayWidth);
        //    Assert.AreEqual(dto.TrayThickHeight, model.TrayThickHeight);
        //    Assert.AreEqual(dto.TrayThickDepth, model.TrayThickDepth);
        //    Assert.AreEqual(dto.TrayThickWidth, model.TrayThickWidth);
        //    if (model.StatusType != null)
        //    {
        //        Assert.AreEqual((ProductStatusType)dto.StatusType, model.StatusType);
        //    }
        //    Assert.AreEqual(dto.ShelfLife, model.ShelfLife);
        //    Assert.AreEqual(dto.DeliveryFrequencyDays, model.DeliveryFrequencyDays);
        //    Assert.AreEqual(dto.DeliveryMethod, model.DeliveryMethod);
        //    Assert.AreEqual(dto.VendorCode, model.VendorCode);
        //    Assert.AreEqual(dto.Vendor, model.Vendor);
        //    Assert.AreEqual(dto.ManufacturerCode, model.ManufacturerCode);
        //    Assert.AreEqual(dto.Manufacturer, model.Manufacturer);
        //    Assert.AreEqual(dto.Size, model.Size);
        //    Assert.AreEqual(dto.UnitOfMeasure, model.UnitOfMeasure);
        //    Assert.AreEqual(dto.SellPrice, model.SellPrice);
        //    Assert.AreEqual(dto.SellPackCount, model.SellPackCount);
        //    Assert.AreEqual(dto.SellPackDescription, model.SellPackDescription);
        //    Assert.AreEqual(dto.RecommendedRetailPrice, model.RecommendedRetailPrice);
        //    Assert.AreEqual(dto.CostPrice, model.CostPrice);
        //    Assert.AreEqual(dto.CaseCost, model.CaseCost);
        //    Assert.AreEqual(dto.ConsumerInformation, model.ConsumerInformation);
        //    Assert.AreEqual(dto.Pattern, model.Pattern);
        //    Assert.AreEqual(dto.Model, model.Model);
        //    Assert.AreEqual(dto.CorporateCode, model.CorporateCode);

        //    //#region Custom Attributes
        //    //Assert.AreEqual(dto.CustomAttribute01, model.CustomAttribute01);
        //    //Assert.AreEqual(dto.CustomAttribute02, model.CustomAttribute02);
        //    //Assert.AreEqual(dto.CustomAttribute03, model.CustomAttribute03);
        //    //Assert.AreEqual(dto.CustomAttribute04, model.CustomAttribute04);
        //    //Assert.AreEqual(dto.CustomAttribute05, model.CustomAttribute05);
        //    //Assert.AreEqual(dto.CustomAttribute06, model.CustomAttribute06);
        //    //Assert.AreEqual(dto.CustomAttribute07, model.CustomAttribute07);
        //    //Assert.AreEqual(dto.CustomAttribute08, model.CustomAttribute08);
        //    //Assert.AreEqual(dto.CustomAttribute09, model.CustomAttribute09);
        //    //Assert.AreEqual(dto.CustomAttribute10, model.CustomAttribute10);
        //    //Assert.AreEqual(dto.CustomAttribute11, model.CustomAttribute11);
        //    //Assert.AreEqual(dto.CustomAttribute12, model.CustomAttribute12);
        //    //Assert.AreEqual(dto.CustomAttribute13, model.CustomAttribute13);
        //    //Assert.AreEqual(dto.CustomAttribute14, model.CustomAttribute14);
        //    //Assert.AreEqual(dto.CustomAttribute15, model.CustomAttribute15);
        //    //Assert.AreEqual(dto.CustomAttribute16, model.CustomAttribute16);
        //    //Assert.AreEqual(dto.CustomAttribute17, model.CustomAttribute17);
        //    //Assert.AreEqual(dto.CustomAttribute18, model.CustomAttribute18);
        //    //Assert.AreEqual(dto.CustomAttribute19, model.CustomAttribute19);
        //    //Assert.AreEqual(dto.CustomAttribute20, model.CustomAttribute20);
        //    //Assert.AreEqual(dto.CustomAttribute21, model.CustomAttribute21);
        //    //Assert.AreEqual(dto.CustomAttribute22, model.CustomAttribute22);
        //    //Assert.AreEqual(dto.CustomAttribute23, model.CustomAttribute23);
        //    //Assert.AreEqual(dto.CustomAttribute24, model.CustomAttribute24);
        //    //Assert.AreEqual(dto.CustomAttribute25, model.CustomAttribute25);
        //    //Assert.AreEqual(dto.CustomAttribute26, model.CustomAttribute26);
        //    //Assert.AreEqual(dto.CustomAttribute27, model.CustomAttribute27);
        //    //Assert.AreEqual(dto.CustomAttribute28, model.CustomAttribute28);
        //    //Assert.AreEqual(dto.CustomAttribute29, model.CustomAttribute29);
        //    //Assert.AreEqual(dto.CustomAttribute30, model.CustomAttribute30);
        //    //Assert.AreEqual(dto.CustomAttribute31, model.CustomAttribute31);
        //    //Assert.AreEqual(dto.CustomAttribute32, model.CustomAttribute32);
        //    //Assert.AreEqual(dto.CustomAttribute33, model.CustomAttribute33);
        //    //Assert.AreEqual(dto.CustomAttribute34, model.CustomAttribute34);
        //    //Assert.AreEqual(dto.CustomAttribute35, model.CustomAttribute35);
        //    //Assert.AreEqual(dto.CustomAttribute36, model.CustomAttribute36);
        //    //Assert.AreEqual(dto.CustomAttribute37, model.CustomAttribute37);
        //    //Assert.AreEqual(dto.CustomAttribute38, model.CustomAttribute38);
        //    //Assert.AreEqual(dto.CustomAttribute39, model.CustomAttribute39);
        //    //Assert.AreEqual(dto.CustomAttribute40, model.CustomAttribute40);
        //    //Assert.AreEqual(dto.CustomAttribute41, model.CustomAttribute41);
        //    //Assert.AreEqual(dto.CustomAttribute42, model.CustomAttribute42);
        //    //Assert.AreEqual(dto.CustomAttribute43, model.CustomAttribute43);
        //    //Assert.AreEqual(dto.CustomAttribute44, model.CustomAttribute44);
        //    //Assert.AreEqual(dto.CustomAttribute45, model.CustomAttribute45);
        //    //Assert.AreEqual(dto.CustomAttribute46, model.CustomAttribute46);
        //    //Assert.AreEqual(dto.CustomAttribute47, model.CustomAttribute47);
        //    //Assert.AreEqual(dto.CustomAttribute48, model.CustomAttribute48);
        //    //Assert.AreEqual(dto.CustomAttribute49, model.CustomAttribute49);
        //    //Assert.AreEqual(dto.CustomAttribute50, model.CustomAttribute50);

        //    //#endregion
        //}

        //private void AssertModelandModelAreEqual(Galleria.Ccm.Model.LocationProductAttribute model1, Galleria.Ccm.Model.LocationProductAttribute model2)
        //{
        //    Assert.AreEqual(model1.ProductId, model2.ProductId);
        //    Assert.AreEqual(model1.LocationId, model2.LocationId);
        //    Assert.AreEqual(model1.EntityId, model2.EntityId);
        //    Assert.AreEqual(model1.GTIN, model2.GTIN);
        //    Assert.AreEqual(model1.Description, model2.Description);
        //    Assert.AreEqual(model1.Height, model2.Height);
        //    Assert.AreEqual(model1.Width, model2.Width);
        //    Assert.AreEqual(model1.Depth, model2.Depth);
        //    Assert.AreEqual(model1.CasePackUnits, model2.CasePackUnits);
        //    Assert.AreEqual(model1.CaseHigh, model2.CaseHigh);
        //    Assert.AreEqual(model1.CaseWide, model2.CaseWide);
        //    Assert.AreEqual(model1.CaseDeep, model2.CaseDeep);
        //    Assert.AreEqual(model1.CaseHeight, model2.CaseHeight);
        //    Assert.AreEqual(model1.CaseDepth, model2.CaseDepth);
        //    Assert.AreEqual(model1.CaseWidth, model2.CaseWidth);
        //    Assert.AreEqual(model1.DaysOfSupply, model2.DaysOfSupply);
        //    Assert.AreEqual(model1.MinDeep, model2.MinDeep);
        //    Assert.AreEqual(model1.MaxDeep, model2.MaxDeep);
        //    Assert.AreEqual(model1.TrayPackUnits, model2.TrayPackUnits);
        //    Assert.AreEqual(model1.TrayHigh, model2.TrayHigh);
        //    Assert.AreEqual(model1.TrayWide, model2.TrayWide);
        //    Assert.AreEqual(model1.TrayDeep, model2.TrayDeep);
        //    Assert.AreEqual(model1.TrayHeight, model2.TrayHeight);
        //    Assert.AreEqual(model1.TrayDepth, model2.TrayDepth);
        //    Assert.AreEqual(model1.TrayWidth, model2.TrayWidth);
        //    Assert.AreEqual(model1.TrayThickHeight, model2.TrayThickHeight);
        //    Assert.AreEqual(model1.TrayThickDepth, model2.TrayThickDepth);
        //    Assert.AreEqual(model1.TrayThickWidth, model2.TrayThickWidth);
        //    Assert.AreEqual(model1.StatusType, model2.StatusType);
        //    Assert.AreEqual(model1.ShelfLife, model2.ShelfLife);
        //    Assert.AreEqual(model1.DeliveryFrequencyDays, model2.DeliveryFrequencyDays);
        //    Assert.AreEqual(model1.DeliveryMethod, model2.DeliveryMethod);
        //    Assert.AreEqual(model1.VendorCode, model2.VendorCode);
        //    Assert.AreEqual(model1.Vendor, model2.Vendor);
        //    Assert.AreEqual(model1.ManufacturerCode, model2.ManufacturerCode);
        //    Assert.AreEqual(model1.Manufacturer, model2.Manufacturer);
        //    Assert.AreEqual(model1.Size, model2.Size);
        //    Assert.AreEqual(model1.UnitOfMeasure, model2.UnitOfMeasure);
        //    Assert.AreEqual(model1.SellPrice, model2.SellPrice);
        //    Assert.AreEqual(model1.SellPackCount, model2.SellPackCount);
        //    Assert.AreEqual(model1.SellPackDescription, model2.SellPackDescription);
        //    Assert.AreEqual(model1.RecommendedRetailPrice, model2.RecommendedRetailPrice);
        //    Assert.AreEqual(model1.CostPrice, model2.CostPrice);
        //    Assert.AreEqual(model1.CaseCost, model2.CaseCost);
        //    Assert.AreEqual(model1.ConsumerInformation, model2.ConsumerInformation);
        //    Assert.AreEqual(model1.Pattern, model2.Pattern);
        //    Assert.AreEqual(model1.Model, model2.Model);
        //    Assert.AreEqual(model1.CorporateCode, model2.CorporateCode);

        //    //#region Custom Attributes
        //    //Assert.AreEqual(model1.CustomAttribute01, model2.CustomAttribute01);
        //    //Assert.AreEqual(model1.CustomAttribute02, model2.CustomAttribute02);
        //    //Assert.AreEqual(model1.CustomAttribute03, model2.CustomAttribute03);
        //    //Assert.AreEqual(model1.CustomAttribute04, model2.CustomAttribute04);
        //    //Assert.AreEqual(model1.CustomAttribute05, model2.CustomAttribute05);
        //    //Assert.AreEqual(model1.CustomAttribute06, model2.CustomAttribute06);
        //    //Assert.AreEqual(model1.CustomAttribute07, model2.CustomAttribute07);
        //    //Assert.AreEqual(model1.CustomAttribute08, model2.CustomAttribute08);
        //    //Assert.AreEqual(model1.CustomAttribute09, model2.CustomAttribute09);
        //    //Assert.AreEqual(model1.CustomAttribute10, model2.CustomAttribute10);
        //    //Assert.AreEqual(model1.CustomAttribute11, model2.CustomAttribute11);
        //    //Assert.AreEqual(model1.CustomAttribute12, model2.CustomAttribute12);
        //    //Assert.AreEqual(model1.CustomAttribute13, model2.CustomAttribute13);
        //    //Assert.AreEqual(model1.CustomAttribute14, model2.CustomAttribute14);
        //    //Assert.AreEqual(model1.CustomAttribute15, model2.CustomAttribute15);
        //    //Assert.AreEqual(model1.CustomAttribute16, model2.CustomAttribute16);
        //    //Assert.AreEqual(model1.CustomAttribute17, model2.CustomAttribute17);
        //    //Assert.AreEqual(model1.CustomAttribute18, model2.CustomAttribute18);
        //    //Assert.AreEqual(model1.CustomAttribute19, model2.CustomAttribute19);
        //    //Assert.AreEqual(model1.CustomAttribute20, model2.CustomAttribute20);
        //    //Assert.AreEqual(model1.CustomAttribute21, model2.CustomAttribute21);
        //    //Assert.AreEqual(model1.CustomAttribute22, model2.CustomAttribute22);
        //    //Assert.AreEqual(model1.CustomAttribute23, model2.CustomAttribute23);
        //    //Assert.AreEqual(model1.CustomAttribute24, model2.CustomAttribute24);
        //    //Assert.AreEqual(model1.CustomAttribute25, model2.CustomAttribute25);
        //    //Assert.AreEqual(model1.CustomAttribute26, model2.CustomAttribute26);
        //    //Assert.AreEqual(model1.CustomAttribute27, model2.CustomAttribute27);
        //    //Assert.AreEqual(model1.CustomAttribute28, model2.CustomAttribute28);
        //    //Assert.AreEqual(model1.CustomAttribute29, model2.CustomAttribute29);
        //    //Assert.AreEqual(model1.CustomAttribute30, model2.CustomAttribute30);
        //    //Assert.AreEqual(model1.CustomAttribute31, model2.CustomAttribute31);
        //    //Assert.AreEqual(model1.CustomAttribute32, model2.CustomAttribute32);
        //    //Assert.AreEqual(model1.CustomAttribute33, model2.CustomAttribute33);
        //    //Assert.AreEqual(model1.CustomAttribute34, model2.CustomAttribute34);
        //    //Assert.AreEqual(model1.CustomAttribute35, model2.CustomAttribute35);
        //    //Assert.AreEqual(model1.CustomAttribute36, model2.CustomAttribute36);
        //    //Assert.AreEqual(model1.CustomAttribute37, model2.CustomAttribute37);
        //    //Assert.AreEqual(model1.CustomAttribute38, model2.CustomAttribute38);
        //    //Assert.AreEqual(model1.CustomAttribute39, model2.CustomAttribute39);
        //    //Assert.AreEqual(model1.CustomAttribute40, model2.CustomAttribute40);
        //    //Assert.AreEqual(model1.CustomAttribute41, model2.CustomAttribute41);
        //    //Assert.AreEqual(model1.CustomAttribute42, model2.CustomAttribute42);
        //    //Assert.AreEqual(model1.CustomAttribute43, model2.CustomAttribute43);
        //    //Assert.AreEqual(model1.CustomAttribute44, model2.CustomAttribute44);
        //    //Assert.AreEqual(model1.CustomAttribute45, model2.CustomAttribute45);
        //    //Assert.AreEqual(model1.CustomAttribute46, model2.CustomAttribute46);
        //    //Assert.AreEqual(model1.CustomAttribute47, model2.CustomAttribute47);
        //    //Assert.AreEqual(model1.CustomAttribute48, model2.CustomAttribute48);
        //    //Assert.AreEqual(model1.CustomAttribute49, model2.CustomAttribute49);
        //    //Assert.AreEqual(model1.CustomAttribute50, model2.CustomAttribute50);
        //    //#endregion

        //}

        //private void SetRandomPropertyValues(Galleria.Ccm.Model.LocationProductAttribute model)
        //{
        //    Random rand = new Random(2184762);

        //    model.GTIN = Guid.NewGuid().ToString().Substring(0, 10);
        //    model.Description = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.Height = Convert.ToSingle(rand.Next(0, 100));
        //    model.Width = Convert.ToSingle(rand.Next(0, 100));
        //    model.Depth = Convert.ToSingle(rand.Next(0, 100));
        //    model.CasePackUnits = Convert.ToInt16(rand.Next(0, 100));
        //    model.CaseHigh = 1;
        //    model.CaseWide = 1;
        //    model.CaseDeep = 1;
        //    model.CaseHeight = Convert.ToSingle(rand.Next(0, 100));
        //    model.CaseDepth = Convert.ToSingle(rand.Next(0, 100));
        //    model.CaseWidth = Convert.ToSingle(rand.Next(0, 100));
        //    model.DaysOfSupply = 1;
        //    model.MinDeep = 1;
        //    model.MaxDeep = 1;
        //    model.TrayPackUnits = Convert.ToInt16(rand.Next(0, 100));
        //    model.TrayHigh = 1;
        //    model.TrayWide = 1;
        //    model.TrayDeep = 1;
        //    model.TrayHeight = Convert.ToSingle(rand.Next(0, 100));
        //    model.TrayDepth = Convert.ToSingle(rand.Next(0, 100));
        //    model.TrayWidth = Convert.ToSingle(rand.Next(0, 100));
        //    model.TrayThickHeight = Convert.ToSingle(rand.Next(0, 100));
        //    model.TrayThickDepth = Convert.ToSingle(rand.Next(0, 100));
        //    model.TrayThickWidth = Convert.ToSingle(rand.Next(0, 100));
        //    model.StatusType = ProductStatusType.Active;
        //    model.ShelfLife = Convert.ToInt16(rand.Next(0, 100));
        //    model.DeliveryFrequencyDays = Convert.ToSingle(rand.Next(0, 100));
        //    model.DeliveryMethod = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.VendorCode = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.Vendor = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.ManufacturerCode = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.Manufacturer = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.Size = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.UnitOfMeasure = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.SellPrice = Convert.ToSingle(rand.Next(0, 100));
        //    model.SellPackCount = Convert.ToInt16(rand.Next(0, 100));
        //    model.SellPackDescription = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.RecommendedRetailPrice = Convert.ToSingle(rand.Next(0, 100));
        //    model.CostPrice = Convert.ToSingle(rand.Next(0, 100));
        //    model.CaseCost = Convert.ToSingle(rand.Next(0, 100));
        //    model.ConsumerInformation = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.Pattern = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.Model = Guid.NewGuid().ToString().Substring(0, 15);
        //    model.CorporateCode = Guid.NewGuid().ToString().Substring(0, 15);

        //    //#region Custom Attributes
        //    //model.CustomAttribute01 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute02 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute03 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute04 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute05 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute06 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute07 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute08 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute09 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute10 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute11 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute12 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute13 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute14 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute15 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute16 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute17 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute18 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute19 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute20 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute21 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute22 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute23 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute24 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute25 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute26 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute27 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute28 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute29 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute30 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute31 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute32 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute33 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute34 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute35 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute36 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute37 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute38 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute39 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute40 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute41 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute42 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute43 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute44 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute45 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute46 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute47 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute48 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute49 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());
        //    //model.CustomAttribute50 = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString());

        //    //#endregion
        //}

        //#endregion

        //#region Serializable
        ///// <summary>
        ///// Serializable
        ///// </summary>
        //[Test]
        //public void Serializable()
        //{
        //    Galleria.Ccm.Model.LocationProductAttribute model =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(1, 1, 1);
        //    Serialize(model);
        //}
        //#endregion

        //#region Property Setters

        //[Test]
        //public void PropertySettersGeneric()
        //{
        //    Galleria.Ccm.Model.LocationProductAttribute model =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(1, 1, 1);

        //    TestBase.TestPropertySetters<Galleria.Ccm.Model.LocationProductAttribute>(model);
        //}

        ///// <summary>
        ///// PropertySetters
        ///// </summary>
        //[Test]
        //public void PropertySetters()
        //{
        //    // create a new model for rand values
        //    Galleria.Ccm.Model.LocationProductAttribute valueModel =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(1, 1, 1);
        //    SetRandomPropertyValues(valueModel);

        //    //model to set on
        //    Galleria.Ccm.Model.LocationProductAttribute model =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(1, 1, 1);

        //    //set and check each property
        //    model.GTIN = valueModel.GTIN;
        //    model.Description = valueModel.Description;
        //    model.Height = valueModel.Height;
        //    model.Width = valueModel.Width;
        //    model.Depth = valueModel.Depth;
        //    model.CasePackUnits = valueModel.CasePackUnits;
        //    model.CaseHigh = valueModel.CaseHigh;
        //    model.CaseWide = valueModel.CaseWide;
        //    model.CaseDeep = valueModel.CaseDeep;
        //    model.CaseHeight = valueModel.CaseHeight;
        //    model.CaseDepth = valueModel.CaseDepth;
        //    model.CaseWidth = valueModel.CaseWidth;
        //    model.DaysOfSupply = valueModel.DaysOfSupply;
        //    model.MinDeep = valueModel.MinDeep;
        //    model.MaxDeep = valueModel.MaxDeep;
        //    model.TrayPackUnits = valueModel.TrayPackUnits;
        //    model.TrayHigh = valueModel.TrayHigh;
        //    model.TrayWide = valueModel.TrayWide;
        //    model.TrayDeep = valueModel.TrayDeep;
        //    model.TrayHeight = valueModel.TrayHeight;
        //    model.TrayDepth = valueModel.TrayDepth;
        //    model.TrayWidth = valueModel.TrayWidth;
        //    model.TrayThickHeight = valueModel.TrayThickHeight;
        //    model.TrayThickDepth = valueModel.TrayThickDepth;
        //    model.TrayThickWidth = valueModel.TrayThickWidth;
        //    model.StatusType = valueModel.StatusType;
        //    model.ShelfLife = valueModel.ShelfLife;
        //    model.DeliveryFrequencyDays = valueModel.DeliveryFrequencyDays;
        //    model.DeliveryMethod = valueModel.DeliveryMethod;
        //    model.VendorCode = valueModel.VendorCode;
        //    model.Vendor = valueModel.Vendor;
        //    model.ManufacturerCode = valueModel.ManufacturerCode;
        //    model.Manufacturer = valueModel.Manufacturer;
        //    model.Size = valueModel.Size;
        //    model.UnitOfMeasure = valueModel.UnitOfMeasure;
        //    model.SellPrice = valueModel.SellPrice;
        //    model.SellPackCount = valueModel.SellPackCount;
        //    model.SellPackDescription = valueModel.SellPackDescription;
        //    model.RecommendedRetailPrice = valueModel.RecommendedRetailPrice;
        //    model.CostPrice = valueModel.CostPrice;
        //    model.CaseCost = valueModel.CaseCost;
        //    model.ConsumerInformation = valueModel.ConsumerInformation;
        //    model.Pattern = valueModel.Pattern;
        //    model.Model = valueModel.Model;
        //    model.CorporateCode = valueModel.CorporateCode;

        //    //#region Custom Attributes
        //    //model.CustomAttribute01 = valueModel.CustomAttribute01;
        //    //model.CustomAttribute02 = valueModel.CustomAttribute02;
        //    //model.CustomAttribute03 = valueModel.CustomAttribute03;
        //    //model.CustomAttribute04 = valueModel.CustomAttribute04;
        //    //model.CustomAttribute05 = valueModel.CustomAttribute05;
        //    //model.CustomAttribute06 = valueModel.CustomAttribute06;
        //    //model.CustomAttribute07 = valueModel.CustomAttribute07;
        //    //model.CustomAttribute08 = valueModel.CustomAttribute08;
        //    //model.CustomAttribute09 = valueModel.CustomAttribute09;
        //    //model.CustomAttribute10 = valueModel.CustomAttribute10;
        //    //model.CustomAttribute11 = valueModel.CustomAttribute11;
        //    //model.CustomAttribute12 = valueModel.CustomAttribute12;
        //    //model.CustomAttribute13 = valueModel.CustomAttribute13;
        //    //model.CustomAttribute14 = valueModel.CustomAttribute14;
        //    //model.CustomAttribute15 = valueModel.CustomAttribute15;
        //    //model.CustomAttribute16 = valueModel.CustomAttribute16;
        //    //model.CustomAttribute17 = valueModel.CustomAttribute17;
        //    //model.CustomAttribute18 = valueModel.CustomAttribute18;
        //    //model.CustomAttribute19 = valueModel.CustomAttribute19;
        //    //model.CustomAttribute20 = valueModel.CustomAttribute20;
        //    //model.CustomAttribute21 = valueModel.CustomAttribute21;
        //    //model.CustomAttribute22 = valueModel.CustomAttribute22;
        //    //model.CustomAttribute23 = valueModel.CustomAttribute23;
        //    //model.CustomAttribute24 = valueModel.CustomAttribute24;
        //    //model.CustomAttribute25 = valueModel.CustomAttribute25;
        //    //model.CustomAttribute26 = valueModel.CustomAttribute26;
        //    //model.CustomAttribute27 = valueModel.CustomAttribute27;
        //    //model.CustomAttribute28 = valueModel.CustomAttribute28;
        //    //model.CustomAttribute29 = valueModel.CustomAttribute29;
        //    //model.CustomAttribute30 = valueModel.CustomAttribute30;
        //    //model.CustomAttribute31 = valueModel.CustomAttribute31;
        //    //model.CustomAttribute32 = valueModel.CustomAttribute32;
        //    //model.CustomAttribute33 = valueModel.CustomAttribute33;
        //    //model.CustomAttribute34 = valueModel.CustomAttribute34;
        //    //model.CustomAttribute35 = valueModel.CustomAttribute35;
        //    //model.CustomAttribute36 = valueModel.CustomAttribute36;
        //    //model.CustomAttribute37 = valueModel.CustomAttribute37;
        //    //model.CustomAttribute38 = valueModel.CustomAttribute38;
        //    //model.CustomAttribute39 = valueModel.CustomAttribute39;
        //    //model.CustomAttribute40 = valueModel.CustomAttribute40;
        //    //model.CustomAttribute41 = valueModel.CustomAttribute41;
        //    //model.CustomAttribute42 = valueModel.CustomAttribute42;
        //    //model.CustomAttribute43 = valueModel.CustomAttribute43;
        //    //model.CustomAttribute44 = valueModel.CustomAttribute44;
        //    //model.CustomAttribute45 = valueModel.CustomAttribute45;
        //    //model.CustomAttribute46 = valueModel.CustomAttribute46;
        //    //model.CustomAttribute47 = valueModel.CustomAttribute47;
        //    //model.CustomAttribute48 = valueModel.CustomAttribute48;
        //    //model.CustomAttribute49 = valueModel.CustomAttribute49;
        //    //model.CustomAttribute50 = valueModel.CustomAttribute50;
        //    //#endregion

        //    AssertModelandModelAreEqual(valueModel, model);

        //}
        //#endregion

        //#region FactoryMethods

        //[Test]
        //public void NewLocationProductAttribute()
        //{

        //    Galleria.Ccm.Model.LocationProductAttribute newLocationProductAttribute =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(1, 2, 3);

        //    Assert.AreEqual(1, newLocationProductAttribute.LocationId);
        //    Assert.AreEqual(2, newLocationProductAttribute.ProductId);
        //    Assert.AreEqual(3, newLocationProductAttribute.EntityId);

        //    Assert.IsTrue(newLocationProductAttribute.IsNew);
        //    Assert.IsTrue(newLocationProductAttribute.IsChild);

        //}

        //#endregion

        //#region Data Access

        //[Test]
        //public void DataAccess_Insert()
        //{
        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();
        //    Int32 entityId = entity.Id;

        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productList = TestDataHelper.PopulateProductHierarchyWithProducts(hierarchy, 5);

        //    List<LocationDto> locationList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, entity.Id, new List<Int32> { 1, 2 }, 2);

        //    //List<LocationProductCustomAttributeDto> locationProductCustomAttribtues = TestDataHelper.InsertLocationProductCustomAttributeDtos(base.DalFactory, 50, entityId);

        //    //create a new item 
        //    Galleria.Ccm.Model.LocationProductAttribute prodAttribute =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(locationList[0].Id, productList.Last().Id, entityId);
        //    Galleria.Ccm.Model.LocationProductAttributeList list = Galleria.Ccm.Model.LocationProductAttributeList.NewList();
        //    list.Add(prodAttribute);

        //    //set properties
        //    SetRandomPropertyValues(prodAttribute);

        //    prodAttribute.TrayThickDepth = 10;
        //    prodAttribute.TrayThickHeight = 10;
        //    prodAttribute.TrayThickWidth = 10;

        //    //prodAttribute.CustomAttribute01 = 1;
        //    //prodAttribute.CustomAttribute02 = 2;
        //    //prodAttribute.CustomAttribute03 = 3;
        //    //prodAttribute.CustomAttribute04 = 4;
        //    //prodAttribute.CustomAttribute05 = 5;
        //    //prodAttribute.CustomAttribute06 = 6;
        //    //prodAttribute.CustomAttribute07 = 7;
        //    //prodAttribute.CustomAttribute08 = 8;
        //    //prodAttribute.CustomAttribute09 = 9;
        //    //prodAttribute.CustomAttribute10 = 10;
        //    //prodAttribute.CustomAttribute11 = 11;
        //    //prodAttribute.CustomAttribute12 = 12;
        //    //prodAttribute.CustomAttribute13 = 13;
        //    //prodAttribute.CustomAttribute14 = 14;
        //    //prodAttribute.CustomAttribute15 = 15;
        //    //prodAttribute.CustomAttribute16 = 16;
        //    //prodAttribute.CustomAttribute17 = 17;
        //    //prodAttribute.CustomAttribute18 = 18;
        //    //prodAttribute.CustomAttribute19 = 19;
        //    //prodAttribute.CustomAttribute20 = 20;
        //    //prodAttribute.CustomAttribute21 = 21;
        //    //prodAttribute.CustomAttribute22 = 22;
        //    //prodAttribute.CustomAttribute23 = 23;
        //    //prodAttribute.CustomAttribute24 = 24;
        //    //prodAttribute.CustomAttribute25 = 25;
        //    //prodAttribute.CustomAttribute26 = 26;
        //    //prodAttribute.CustomAttribute27 = 27;
        //    //prodAttribute.CustomAttribute28 = 28;
        //    //prodAttribute.CustomAttribute29 = 29;
        //    //prodAttribute.CustomAttribute30 = 30;
        //    //prodAttribute.CustomAttribute31 = 31;
        //    //prodAttribute.CustomAttribute32 = 32;
        //    //prodAttribute.CustomAttribute33 = 33;
        //    //prodAttribute.CustomAttribute34 = 34;
        //    //prodAttribute.CustomAttribute35 = 35;
        //    //prodAttribute.CustomAttribute36 = 36;
        //    //prodAttribute.CustomAttribute37 = 37;
        //    //prodAttribute.CustomAttribute38 = 38;
        //    //prodAttribute.CustomAttribute39 = 39;
        //    //prodAttribute.CustomAttribute40 = 40;
        //    //prodAttribute.CustomAttribute41 = 41;
        //    //prodAttribute.CustomAttribute42 = 42;
        //    //prodAttribute.CustomAttribute43 = 43;
        //    //prodAttribute.CustomAttribute44 = 44;
        //    //prodAttribute.CustomAttribute45 = 45;
        //    //prodAttribute.CustomAttribute46 = 46;
        //    //prodAttribute.CustomAttribute47 = 47;
        //    //prodAttribute.CustomAttribute48 = 48;
        //    //prodAttribute.CustomAttribute49 = 49;
        //    //prodAttribute.CustomAttribute50 = 50;

        //    //save
        //    list = list.Save();
        //    prodAttribute = list[0];


        //    //fetch back the dto
        //    LocationProductAttributeDto dto;
        //    using (IDalContext dalContext = base.DalFactory.CreateContext())
        //    {
        //        using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
        //        {
        //            dto = dal.FetchByLocationIdProductId(prodAttribute.LocationId, prodAttribute.ProductId);
        //        }
        //    }
        //    AssertDtoAndModelAreEqual(dto, prodAttribute);
        //}

        //[Test]
        //public void DataAccess_Update()
        //{
        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();
        //    Int32 entityId = entity.Id;

        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productList = TestDataHelper.PopulateProductHierarchyWithProducts(hierarchy, 5);
        //    List<LocationDto> locationList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, entity.Id, new List<Int32> { 1, 2 }, 2);

        //    //List<LocationProductCustomAttributeDto> locationProductCustomAttribtues = TestDataHelper.InsertLocationProductCustomAttributeDtos(base.DalFactory, 50, entityId);

        //    //create a new item 
        //    Galleria.Ccm.Model.LocationProductAttribute prodAttribute =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(locationList[0].Id, productList.Last().Id, entityId);
        //    Galleria.Ccm.Model.LocationProductAttributeList list = Galleria.Ccm.Model.LocationProductAttributeList.NewList();
        //    list.Add(prodAttribute);


        //    //set properties
        //    SetRandomPropertyValues(prodAttribute);

        //    prodAttribute.TrayThickDepth = 10;
        //    prodAttribute.TrayThickHeight = 10;
        //    prodAttribute.TrayThickWidth = 10;

        //    //prodAttribute.CustomAttribute01 = 1;
        //    //prodAttribute.CustomAttribute02 = 2;
        //    //prodAttribute.CustomAttribute03 = 3;
        //    //prodAttribute.CustomAttribute04 = 4;
        //    //prodAttribute.CustomAttribute05 = 5;
        //    //prodAttribute.CustomAttribute06 = 6;
        //    //prodAttribute.CustomAttribute07 = 7;
        //    //prodAttribute.CustomAttribute08 = 8;
        //    //prodAttribute.CustomAttribute09 = 9;
        //    //prodAttribute.CustomAttribute10 = 10;
        //    //prodAttribute.CustomAttribute11 = 11;
        //    //prodAttribute.CustomAttribute12 = 12;
        //    //prodAttribute.CustomAttribute13 = 13;
        //    //prodAttribute.CustomAttribute14 = 14;
        //    //prodAttribute.CustomAttribute15 = 15;
        //    //prodAttribute.CustomAttribute16 = 16;
        //    //prodAttribute.CustomAttribute17 = 17;
        //    //prodAttribute.CustomAttribute18 = 18;
        //    //prodAttribute.CustomAttribute19 = 19;
        //    //prodAttribute.CustomAttribute20 = 20;
        //    //prodAttribute.CustomAttribute21 = 21;
        //    //prodAttribute.CustomAttribute22 = 22;
        //    //prodAttribute.CustomAttribute23 = 23;
        //    //prodAttribute.CustomAttribute24 = 24;
        //    //prodAttribute.CustomAttribute25 = 25;
        //    //prodAttribute.CustomAttribute26 = 26;
        //    //prodAttribute.CustomAttribute27 = 27;
        //    //prodAttribute.CustomAttribute28 = 28;
        //    //prodAttribute.CustomAttribute29 = 29;
        //    //prodAttribute.CustomAttribute30 = 30;
        //    //prodAttribute.CustomAttribute31 = 31;
        //    //prodAttribute.CustomAttribute32 = 32;
        //    //prodAttribute.CustomAttribute33 = 33;
        //    //prodAttribute.CustomAttribute34 = 34;
        //    //prodAttribute.CustomAttribute35 = 35;
        //    //prodAttribute.CustomAttribute36 = 36;
        //    //prodAttribute.CustomAttribute37 = 37;
        //    //prodAttribute.CustomAttribute38 = 38;
        //    //prodAttribute.CustomAttribute39 = 39;
        //    //prodAttribute.CustomAttribute40 = 40;
        //    //prodAttribute.CustomAttribute41 = 41;
        //    //prodAttribute.CustomAttribute42 = 42;
        //    //prodAttribute.CustomAttribute43 = 43;
        //    //prodAttribute.CustomAttribute44 = 44;
        //    //prodAttribute.CustomAttribute45 = 45;
        //    //prodAttribute.CustomAttribute46 = 46;
        //    //prodAttribute.CustomAttribute47 = 47;
        //    //prodAttribute.CustomAttribute48 = 48;
        //    //prodAttribute.CustomAttribute49 = 49;
        //    //prodAttribute.CustomAttribute50 = 50;

        //    //save
        //    list = list.Save();
        //    prodAttribute = list[0];

        //    //update
        //    SetRandomPropertyValues(prodAttribute);

        //    prodAttribute.TrayThickDepth = 10;
        //    prodAttribute.TrayThickHeight = 10;
        //    prodAttribute.TrayThickWidth = 10;

        //    //save
        //    list = list.Save();
        //    prodAttribute = list[0];

        //    //fetch back the dto
        //    LocationProductAttributeDto dto;
        //    using (IDalContext dalContext = base.DalFactory.CreateContext())
        //    {
        //        using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
        //        {
        //            dto = dal.FetchByLocationIdProductId(prodAttribute.LocationId, prodAttribute.ProductId);
        //        }
        //    }
        //    AssertDtoAndModelAreEqual(dto, prodAttribute);
        //}

        //[Test]
        //public void DataAccess_Delete()
        //{
        //    Ccm.Model.Entity entity = Ccm.Model.Entity.NewEntity();
        //    entity.Name = Guid.NewGuid().ToString();
        //    entity = entity.Save();
        //    Int32 entityId = entity.Id;

        //    Ccm.Model.ProductHierarchy hierarchy = TestDataHelper.PopulateProductHierarchy(entity.MerchandisingHierarchyId);
        //    List<Ccm.Model.Product> productList = TestDataHelper.PopulateProductHierarchyWithProducts(hierarchy, 5);
        //    List<LocationDto> locationList = TestDataHelper.InsertLocationDtos(
        //        base.DalFactory, entity.Id, new List<Int32> { 1, 2 }, 2);


        //    //create a new item 
        //    Galleria.Ccm.Model.LocationProductAttribute prodAttribute =
        //        Galleria.Ccm.Model.LocationProductAttribute.NewLocationProductAttributeAsChild(locationList[0].Id, productList.Last().Id, entityId);
        //    Galleria.Ccm.Model.LocationProductAttributeList list = Galleria.Ccm.Model.LocationProductAttributeList.NewList();
        //    list.Add(prodAttribute);

        //    //set properties
        //    SetRandomPropertyValues(prodAttribute);

        //    prodAttribute.TrayThickDepth = 10;
        //    prodAttribute.TrayThickHeight = 10;
        //    prodAttribute.TrayThickWidth = 10;


        //    //prodAttribute.CustomAttribute01 = 1;
        //    //prodAttribute.CustomAttribute02 = 2;
        //    //prodAttribute.CustomAttribute03 = 3;
        //    //prodAttribute.CustomAttribute04 = 4;
        //    //prodAttribute.CustomAttribute05 = 5;
        //    //prodAttribute.CustomAttribute06 = 6;
        //    //prodAttribute.CustomAttribute07 = 7;
        //    //prodAttribute.CustomAttribute08 = 8;
        //    //prodAttribute.CustomAttribute09 = 9;
        //    //prodAttribute.CustomAttribute10 = 10;
        //    //prodAttribute.CustomAttribute11 = 11;
        //    //prodAttribute.CustomAttribute12 = 12;
        //    //prodAttribute.CustomAttribute13 = 13;
        //    //prodAttribute.CustomAttribute14 = 14;
        //    //prodAttribute.CustomAttribute15 = 15;
        //    //prodAttribute.CustomAttribute16 = 16;
        //    //prodAttribute.CustomAttribute17 = 17;
        //    //prodAttribute.CustomAttribute18 = 18;
        //    //prodAttribute.CustomAttribute19 = 19;
        //    //prodAttribute.CustomAttribute20 = 20;
        //    //prodAttribute.CustomAttribute21 = 21;
        //    //prodAttribute.CustomAttribute22 = 22;
        //    //prodAttribute.CustomAttribute23 = 23;
        //    //prodAttribute.CustomAttribute24 = 24;
        //    //prodAttribute.CustomAttribute25 = 25;
        //    //prodAttribute.CustomAttribute26 = 26;
        //    //prodAttribute.CustomAttribute27 = 27;
        //    //prodAttribute.CustomAttribute28 = 28;
        //    //prodAttribute.CustomAttribute29 = 29;
        //    //prodAttribute.CustomAttribute30 = 30;
        //    //prodAttribute.CustomAttribute31 = 31;
        //    //prodAttribute.CustomAttribute32 = 32;
        //    //prodAttribute.CustomAttribute33 = 33;
        //    //prodAttribute.CustomAttribute34 = 34;
        //    //prodAttribute.CustomAttribute35 = 35;
        //    //prodAttribute.CustomAttribute36 = 36;
        //    //prodAttribute.CustomAttribute37 = 37;
        //    //prodAttribute.CustomAttribute38 = 38;
        //    //prodAttribute.CustomAttribute39 = 39;
        //    //prodAttribute.CustomAttribute40 = 40;
        //    //prodAttribute.CustomAttribute41 = 41;
        //    //prodAttribute.CustomAttribute42 = 42;
        //    //prodAttribute.CustomAttribute43 = 43;
        //    //prodAttribute.CustomAttribute44 = 44;
        //    //prodAttribute.CustomAttribute45 = 45;
        //    //prodAttribute.CustomAttribute46 = 46;
        //    //prodAttribute.CustomAttribute47 = 47;
        //    //prodAttribute.CustomAttribute48 = 48;
        //    //prodAttribute.CustomAttribute49 = 49;
        //    //prodAttribute.CustomAttribute50 = 50;

        //    //save
        //    list = list.Save();
        //    prodAttribute = list[0];

        //    //fetch back the dto
        //    LocationProductAttributeDto dto;
        //    using (IDalContext dalContext = base.DalFactory.CreateContext())
        //    {
        //        using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
        //        {
        //            dto = dal.FetchByLocationIdProductId(prodAttribute.LocationId, prodAttribute.ProductId);
        //        }
        //    }
        //    AssertDtoAndModelAreEqual(dto, prodAttribute);

        //    //delete it
        //    Int16 itemLocId = prodAttribute.LocationId;
        //    Int32 itemProdId = prodAttribute.ProductId;
        //    list.Remove(prodAttribute);
        //    list.Save();


        //    //try to retrieve again
        //    using (IDalContext dalContext = base.DalFactory.CreateContext())
        //    {
        //        using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
        //        {
        //            //GFS-21757 Ensure it is still returned even after deletion
        //            LocationProductAttributeDto dalDto;
        //            Assert.DoesNotThrow(() => { dalDto = dal.FetchByLocationIdProductId(itemLocId, itemProdId); },
        //                "Deleted items should be returned when fetched by Id");
        //            dalDto = dal.FetchByLocationIdProductId(itemLocId, itemProdId);
        //            Assert.IsNotNull(dalDto.DateDeleted, "DateDeleted Field should be set");
        //        }
        //    }
        //}

        //[Test]
        //public void DataAccess_BatchDeleteCustomAttributeValueByLocationIds()
        //{

        //    List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(base.DalFactory, 1);
        //    //Ccm.Model.LocationHierarchy locationHierarchy = Ccm.Model.LocationHierarchy.FetchByEntityId(entityList[0].Id);
        //    List<LocationHierarchyDto> locationHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(base.DalFactory, entityList);
        //    List<LocationLevelDto> hierarchyLevelDtos = TestDataHelper.InsertLocationLevelDtos(base.DalFactory, locationHierarchyDtos[0].Id, 3);
        //    List<LocationGroupDto> hierarchyGroupDtos = TestDataHelper.InsertLocationGroupDtos(base.DalFactory, hierarchyLevelDtos, 5);
        //    List<LocationTypeDto> locationTypeDtos = TestDataHelper.InsertLocationTypeDtos(base.DalFactory, 3, 1);

        //    //create a new item 
        //    Galleria.Ccm.Model.Location location = Galleria.Ccm.Model.Location.NewLocation(1, entityList[0].Id);
        //    location.Name = "location1";
        //    location.Code = "code1";
        //    location.LocationGroupId = hierarchyGroupDtos[0].Id;
        //    //Galleria.Ccm.Model.LocationCustomAttributeData locationAttribute = location.AttributeData;

        //    //set properties
        //    //SetRandomPropertyValues(locationAttribute);
        //    String customAttribute01Value = "12345Test";
        //    //locationAttribute.CustomAttribute01 = customAttribute01Value;

        //    //save
        //    location = location.Save();
        //    //locationAttribute = location.AttributeData;

        //    List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(base.DalFactory, 2, 1);
        //    List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(base.DalFactory, hierarchyDtos[0].Id, 3);
        //    List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(base.DalFactory, hierarchy1levelDtos, 5);

        //    //create a new item 
        //    Galleria.Ccm.Model.Product product = Galleria.Ccm.Model.Product.NewProduct(1, entityList[0].Id);
        //    product.Name = "product1";
        //    product.GTIN = "gtinX";
        //    product.ProductGroupId = hierarchy1GroupDtos[0].Id;
        //    Galleria.Ccm.Model.ProductAttributeData productAttribute = product.AttributeData;

        //    //set properties
        //    SetRandomPropertyValues(productAttribute);
        //    productAttribute.CustomAttribute01 = customAttribute01Value;

        //    //save
        //    product = product.Save();
        //    productAttribute = product.AttributeData;


        //    using (IDalContext dalContext = base.DalFactory.CreateContext())
        //    {
        //        dalContext.Begin();
        //        using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
        //        {
        //            LocationProductAttributeDto dto = new LocationProductAttributeDto()
        //            {
        //                EntityId = entityList[0].Id,
        //                LocationId = location.Id,
        //                ProductId = product.Id,
        //                GTIN = product.GTIN,
        //                CasePackUnits = product.CasePackUnits,
        //                DaysOfSupply = 0,
        //                DeliveryFrequencyDays = 0,
        //                DateCreated = DateTime.UtcNow,
        //                DateLastModified = DateTime.UtcNow,
        //                CustomAttribute01 = customAttribute01Value
        //            };
        //            dal.Insert(dto);
        //        }
        //        dalContext.Commit();
        //    }

        //    List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(
        //            base.DalFactory, entityList[0].Id, new List<Int32> { 1, 2 }, 2);
        //    //List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(base.DalFactory, 100, entityList[0].Id, true);
        //    List<ProductDto> productDtos = TestDataHelper.InsertProductDtos(base.DalFactory, 5, entityList[0].Id);

        //    List<LocationProductAttributeDto> locationProductAttributeDtos = TestDataHelper.InsertLocationProductAttributeDtos(base.DalFactory, locationDtos, productDtos, entityList[0].Id);

        //    Int32 batchSize = 10;
        //    List<Int16> locationDtoIds = locationDtos.Select(l => l.Id).ToList();

        //    for (int i = 0; i < locationDtos.Count(); i += batchSize)
        //    {
        //        String attributeColumnNames = "LocationProductAttribute_CustomAttribute09,LocationProductAttribute_CustomAttribute50";

        //        List<Int16> locationIdsBatch = locationDtoIds.Skip(i).Take(batchSize).ToList();
        //        Galleria.Ccm.Model.LocationProductAttribute.ExecuteDeleteCustomAttributeValueByLocationIds(locationIdsBatch, attributeColumnNames);
        //    }

        //    //fetch back the dto
        //    LocationProductAttributeDto locationProductAttributeDto;
        //    using (IDalContext dalContext = base.DalFactory.CreateContext())
        //    {
        //        using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
        //        {
        //            locationProductAttributeDto = dal.FetchByLocationIdProductId(location.Id, product.Id);
        //        }
        //    }
        //    // this location has not been passed to the batch delete so the value should be preserved
        //    Assert.AreEqual(customAttribute01Value, locationProductAttributeDto.CustomAttribute01);

        //    Galleria.Ccm.Model.LocationProductAttributeList locationProductAttributesAfterDelete
        //        = Galleria.Ccm.Model.LocationProductAttributeList.FetchByEntityId(entityList[0].Id);

        //    foreach (Galleria.Ccm.Model.LocationProductAttribute modelLocationProductAttribute in locationProductAttributesAfterDelete.Where(l => l.LocationId != location.Id && l.ProductId != product.Id))
        //    {
        //        LocationProductAttributeDto foundlocationProductAttributeDto = null;
        //        // fetch attributes which belong to the Location
        //        using (IDalContext dalContext = base.DalFactory.CreateContext())
        //        {
        //            using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
        //            {
        //                foundlocationProductAttributeDto = dal.FetchByLocationIdProductId(modelLocationProductAttribute.LocationId, modelLocationProductAttribute.ProductId);
        //            }
        //        }
        //        AssertDtoAndModelAreEqual(foundlocationProductAttributeDto, modelLocationProductAttribute);
        //        Assert.AreEqual(null, modelLocationProductAttribute.CustomAttribute09);
        //        Assert.AreEqual(null, modelLocationProductAttribute.CustomAttribute50);
        //    }
        //}


        //#endregion
    }
}
