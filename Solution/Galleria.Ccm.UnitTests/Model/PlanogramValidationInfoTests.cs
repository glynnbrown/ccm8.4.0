﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27004 : A.Silva ~ Created.
// V8-26322 : A.Silva
//      Amended how User records are inserted to avoid random duplicates.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    /// <summary>
    ///     Test Fixture for <see cref="PlanogramValidationInfo"/>.
    /// </summary>
    [TestFixture]
    public class PlanogramValidationInfoTests : TestBase
    {
        #region Serializable

        [Test]
        public void Serializable()
        {
            IEnumerable<Int32> planogramIds;
            InsertTestDtos(out planogramIds);
            var infoList = PlanogramValidationInfoList.FetchByPlanogramIds(planogramIds);
            var model = infoList[0];

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void FetchPlanogramValidationInfo()
        {
            IEnumerable<Int32> planogramIds;
            var testDtos = InsertTestDtos(out planogramIds);
            var infoList = PlanogramValidationInfoList.FetchByPlanogramIds(planogramIds);

            foreach (var testDto in testDtos)
            {
                var planogramValidationInfo = infoList.First(info => info.Id == Convert.ToInt32(testDto.Id));
                PlanogramValidationInfoTests.AssertDtoAndModelAreEqual(testDto, planogramValidationInfo);
            }
        }

        #endregion

        #region Test Helper Methods

        /// <summary>
        ///     Asserts the propertiesi in both the given <paramref name="dto"/> and <paramref name="model"/> are the same.
        /// </summary>
        /// <param name="dto">The original test dto.</param>
        /// <param name="model">The final model generated from the dto.</param>
        private static void AssertDtoAndModelAreEqual(PlanogramValidationTemplateDto dto, PlanogramValidationInfo model)
        {
            Assert.AreEqual(3,typeof(PlanogramValidationInfoDto).GetProperties().Count(), "Incorrect number of properties being tested.");

            Assert.AreEqual(dto.Id,model.Id);
            Assert.AreEqual(dto.Name, model.Name);
            Assert.AreEqual(dto.PlanogramId,model.PlanogramId);
        }

        private List<PlanogramValidationTemplateDto> InsertTestDtos(out IEnumerable<Int32> planogramIds)
        {
            var dalContext = base.DalFactory.CreateContext();
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 5);
            var planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos);
            planogramIds = planogramDtos.Select(dto => Convert.ToInt32(dto.Id));
            var planogramValidationDtos = TestDataHelper.InsertPlanogramValidationTemplateDtos(dalContext, planogramDtos);
            return planogramValidationDtos;
        }

        #endregion
    }
}
