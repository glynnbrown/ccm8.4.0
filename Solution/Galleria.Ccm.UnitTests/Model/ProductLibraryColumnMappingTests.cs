﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25395 A.Probyn
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public sealed class ProductLibraryColumnMappingTests : TestBase
    {
        #region General Model Tests

        [Test]
        public void Serializable()
        {
            Serialize(ProductLibraryColumnMapping.NewProductLibraryColumnMapping());
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters<ProductLibraryColumnMapping>(ProductLibraryColumnMapping.NewProductLibraryColumnMapping());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void CreateNew()
        {
            TestCreateNew<ProductLibraryColumnMapping>(
                /*child*/true,
                null,
                new Object[] { 1 },
                null);
        }

        [Test]
        public void Fetch()
        {
            //insert a dto to fetch
            ProductLibraryDto libraryDto = new ProductLibraryDto();
            PopulatePropertyValues1<ProductLibraryDto>(libraryDto);

            ProductLibraryColumnMappingDto dto = new ProductLibraryColumnMappingDto();
            PopulatePropertyValues1<ProductLibraryColumnMappingDto>(dto, new List<String> { "Id", "ProductLibraryId" });

            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                {
                    dal.Insert(libraryDto);
                }
                using (IProductLibraryColumnMappingDal dal = dalContext.GetDal<IProductLibraryColumnMappingDal>())
                {
                    dto.ProductLibraryId = libraryDto.Id;
                    dal.Insert(dto);
                }
            }

            //fetch
            ProductLibrary productLibrary = ProductLibrary.FetchById(dto.ProductLibraryId);
            ProductLibraryColumnMapping model = productLibrary.ColumnMappings[0];
            AssertDtoAndModelAreEqual<ProductLibraryColumnMappingDto, ProductLibraryColumnMapping>(dto, model);
        }

        #endregion

        #region Data Access

        [Test]
        public void Insert()
        {
            ProductLibrary productLibrary = ProductLibrary.NewProductLibrary();
            PopulatePropertyValues1<ProductLibrary>(productLibrary);

            ProductLibraryColumnMapping model = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            PopulatePropertyValues1<ProductLibraryColumnMapping>(model, new List<String> { "Id", "ProductLibraryId" });
            productLibrary.ColumnMappings.Add(model);

            productLibrary = productLibrary.Save();
            model = productLibrary.ColumnMappings[0];
            productLibrary.Dispose();

            //refetch
            productLibrary = ProductLibrary.FetchById(productLibrary.Id);
            AssertModelsAreEqual<ProductLibraryColumnMapping>(model, productLibrary.ColumnMappings[0]);

        }

        [Test]
        public void Update()
        {
            ProductLibrary productLibrary = ProductLibrary.NewProductLibrary();
            PopulatePropertyValues1<ProductLibrary>(productLibrary);

            ProductLibraryColumnMapping model = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            PopulatePropertyValues1<ProductLibraryColumnMapping>(model, new List<String> { "Id", "ProductLibraryId" });
            productLibrary.ColumnMappings.Add(model);

            productLibrary = productLibrary.Save();

            model = productLibrary.ColumnMappings[0];

            PopulatePropertyValues2<ProductLibraryColumnMapping>(model, new List<String> { "Id", "ProductLibraryId" });

            productLibrary = productLibrary.Save();
            model = productLibrary.ColumnMappings[0];
            productLibrary.Dispose();

            //refetch
            productLibrary = ProductLibrary.FetchById(productLibrary.Id);
            AssertModelsAreEqual<ProductLibraryColumnMapping>(model, productLibrary.ColumnMappings[0]);
        }

        [Test]
        public void Delete()
        {
            ProductLibrary productLibrary = ProductLibrary.NewProductLibrary();
            PopulatePropertyValues1<ProductLibrary>(productLibrary);

            ProductLibraryColumnMapping model = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            PopulatePropertyValues1<ProductLibraryColumnMapping>(model, new List<String> { "Id", "ProductLibraryId" });
            productLibrary.ColumnMappings.Add(model);

            productLibrary = productLibrary.Save();
            model = productLibrary.ColumnMappings[0];
            productLibrary.Dispose();

            //refetch
            productLibrary = ProductLibrary.FetchById(productLibrary.Id);
            model = productLibrary.ColumnMappings[0];

            //delete
            productLibrary.ColumnMappings.Remove(model);
            productLibrary = productLibrary.Save();

            //refetch
            productLibrary = ProductLibrary.FetchById(productLibrary.Id);
            Assert.AreEqual(0, productLibrary.ColumnMappings.Count);
        }

        #endregion
    }
}
