﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion
#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName, WorkpackageId property
//  Corrected EventLogNameName to be EventLogName
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Model
{
    [TestFixture]
    public class EventLog : TestBase
    {
        #region Test Helper Methods

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(EventLogDto dto, Galleria.Ccm.Model.EventLog model)
        {
            Assert.AreEqual(dto.Id, model.Id);
            Assert.AreEqual(dto.EntityId, model.EntityId);
            //Assert.AreEqual(dto.EventLogName_Id, model.EventLogNameId);
            Assert.AreEqual(dto.EventLogName, model.EventLogName);
            Assert.AreEqual(dto.Source, model.Source);
            Assert.AreEqual(dto.EventId, model.EventId);
            Assert.AreEqual(dto.EntryType, (Int16)model.EntryType);
            Assert.AreEqual(dto.UserId, model.UserId);
            Assert.AreEqual(dto.Process, model.Process);
            Assert.AreEqual(dto.ComputerName, model.ComputerName);
            Assert.AreEqual(dto.URLHelperLink, model.UrlHelperLink);
            Assert.AreEqual(dto.Description, model.Description);
            Assert.AreEqual(dto.Content, model.Content);
        }

        private void AssertModelandModelAreEqual(Galleria.Ccm.Model.EventLog model1, Galleria.Ccm.Model.EventLog model2)
        {
            Assert.AreEqual(model1.Id, model2.Id);
            Assert.AreEqual(model1.EntityId, model2.EntityId);
            Assert.AreEqual(model1.EventLogNameId, model2.EventLogNameId);
            Assert.AreEqual(model1.Source, model2.Source);
            Assert.AreEqual(model1.EventId, model2.EventId);
            Assert.AreEqual(model1.EntryType, model2.EntryType);
            Assert.AreEqual(model1.UserId, model2.UserId);
            Assert.AreEqual(model1.Process, model2.Process);
            Assert.AreEqual(model1.ComputerName, model2.ComputerName);
            Assert.AreEqual(model1.UrlHelperLink, model2.UrlHelperLink);
            Assert.AreEqual(model1.Description, model2.Description);
            Assert.AreEqual(model1.Content, model2.Content);
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            Galleria.Ccm.Model.EventLog model =
                Galleria.Ccm.Model.EventLog.NewEventLog();
            Serialize(model);
        }
        #endregion

        #region Property Setters

        /// <summary>
        /// PropertySetters
        /// </summary>
        [Test]
        public void PropertySetters()
        {
            // create a new model for rand values
            Galleria.Ccm.Model.EventLog model =
                Galleria.Ccm.Model.EventLog.NewEventLog();

            TestBase.TestPropertySetters<Galleria.Ccm.Model.EventLog>(model);
        }
        #endregion

        #region FactoryMethods

        [Test]
        public void NewEventLog()
        {

            Galleria.Ccm.Model.EventLog newEventLog =
                Galleria.Ccm.Model.EventLog.NewEventLog();

            Assert.IsTrue(newEventLog.IsNew);
        }

        [Test]
        public void DeleteAllEventLogs()
        {
            EventLogNameDto eventLogNameDto = new EventLogNameDto()
            {
                Name = "Test1"
            };
            
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                EntityDto entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1)[0];

                using (IEventLogNameDal eventLogNameDal = dalContext.GetDal<IEventLogNameDal>())
                {
                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        // Insert event log name
                        eventLogNameDal.Insert(eventLogNameDto);
                        Assert.AreNotEqual(0, eventLogNameDto.Id);

                        List<EventLogDto> dtoEventLogList = new List<EventLogDto>();

                        EventLogDto dto1 = new EventLogDto()
                        {
                            EntityId = entityDto.Id,
                            EntityName = entityDto.Name,
                            EventLogNameId = eventLogNameDto.Id,
                            EventLogName = eventLogNameDto.Name,
                            Source = "1",
                            EventId = 32770,
                            EntryType = 256,
                            UserId = 1,
                            UserName = Galleria.Ccm.Model.User.FetchById(1).FirstName + " " + Galleria.Ccm.Model.User.FetchById(1).LastName,
                            DateTime = DateTime.UtcNow,
                            Process = "2",
                            ComputerName = "3",
                            URLHelperLink = "4",
                            Description = "5",
                            Content = "6",
                            GibraltarSessionId = "7"
                        };
                        dtoEventLogList.Add(dto1);

                        EventLogDto dto2 = new EventLogDto()
                        {
                            EntityId = null,
                            EntityName = null,
                            EventLogNameId = 32769,
                            EventLogName = "Test2",
                            Source = "1",
                            EventId = 32770,
                            EntryType = 256,
                            UserId = null,
                            DateTime = DateTime.UtcNow,
                            Process = "2",
                            ComputerName = "3",
                            URLHelperLink = "4",
                            Description = "5",
                            Content = "6",
                            GibraltarSessionId = "7"
                        };
                        dtoEventLogList.Add(dto2);

                        // insert into the dal
                        foreach (EventLogDto dto in dtoEventLogList)
                        {
                            dal.Insert(dto);
                        }

                        Assert.AreEqual(dal.FetchAll().Count(), 2);

                        // delete logs 
                        Galleria.Ccm.Model.EventLog.DeleteAllEventLogs();

                        Assert.AreEqual(dal.FetchAll().Count(), 0);
                    }
                }
            }
        }

        [Test]
        public void CreateEventLog()
        {
            EventLogNameDto eventLogNameDto = new EventLogNameDto()
            {
                Name = "Test1"
            };
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                UserDto userDto = TestDataHelper.InsertUserDtos(DalFactory, 1)[0];
                EntityDto entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1)[0];
                using (IEventLogNameDal eventLogNameDal = dalContext.GetDal<IEventLogNameDal>())
                {
                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        // Insert event log name
                        eventLogNameDal.Insert(eventLogNameDto);
                        Assert.AreNotEqual(0, eventLogNameDto.Id);

                        EventLogDto dto1 = new EventLogDto()
                        {
                            EntityId = entityDto.Id,
                            EntityName = entityDto.Name,
                            EventLogNameId = eventLogNameDto.Id,
                            EventLogName = eventLogNameDto.Name,
                            Source = "1",
                            EventId = 32770,
                            EntryType = 256,
                            UserId = 1,
                            UserName = Galleria.Ccm.Model.User.FetchById(1).FirstName + " " + Galleria.Ccm.Model.User.FetchById(1).LastName,
                            DateTime = DateTime.UtcNow,
                            Process = "2",
                            ComputerName = "3",
                            URLHelperLink = "4",
                            Description = "5",
                            Content = "6",
                            GibraltarSessionId = "7"
                        };
                        EventLogDto dto2 = new EventLogDto()
                        {
                            EntityId = null,
                            EntityName = null,
                            EventLogNameId = 32769,
                            EventLogName = "Test2",
                            Source = "1",
                            EventId = 32770,
                            EntryType = 256,
                            UserId = null,
                            DateTime = DateTime.UtcNow,
                            Process = "2",
                            ComputerName = "3",
                            URLHelperLink = "4",
                            Description = "5",
                            Content = "6",
                            GibraltarSessionId = "7"
                        };

                        
                        // Insert event log with existing name
                        Galleria.Ccm.Model.EventLog.CreateEventLog(
                            dto1.EntityId,
                            dto1.Source,
                            dto1.EventLogName,
                            dto1.EventId,
                            (EventLogEntryType)dto1.EntryType,
                            dto1.UserId,
                            dto1.Process,
                            dto1.ComputerName,
                            dto1.URLHelperLink,
                            dto1.Description,
                            dto1.Content,
                            dto1.GibraltarSessionId,
                            null,
                            null);

                        EventLogDto returnedDto = dal.FetchAll().ToList()[0];
                        // One minute's grace:
                        Assert.GreaterOrEqual(60000, Math.Abs((returnedDto.DateTime - dto1.DateTime).TotalMilliseconds));
                        dto1.DateTime = returnedDto.DateTime;
                        dto1.Id = returnedDto.Id;
                        Assert.AreEqual(dto1, returnedDto);

                        // Insert event log without existing name
                        Galleria.Ccm.Model.EventLog.CreateEventLog(
                            dto2.EntityId,
                            dto2.Source,
                            dto2.EventLogName,
                            dto2.EventId,
                            (EventLogEntryType)dto2.EntryType,
                            dto2.UserId,
                            dto2.Process,
                            dto2.ComputerName,
                            dto2.URLHelperLink,
                            dto2.Description,
                            dto2.Content,
                            dto2.GibraltarSessionId,
                            null,
                            null);
                        eventLogNameDto = eventLogNameDal.FetchByName("Test2");
                        dto2.EventLogNameId = eventLogNameDto.Id;
                        returnedDto = dal.FetchAll().OrderByDescending(d => d.Id).First();
                        Assert.GreaterOrEqual(60000, (returnedDto.DateTime - dto2.DateTime).TotalMilliseconds);
                        dto2.DateTime = returnedDto.DateTime;
                        dto2.Id = returnedDto.Id;
                        Assert.AreEqual(dto2, returnedDto);
                    }
                }
            }
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_InsertFetchById()
        {
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                EntityDto entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1)[0];
                //create a new item 
                Galleria.Ccm.Model.EventLog eventLog =
                    Galleria.Ccm.Model.EventLog.NewEventLog();

                eventLog.EntityId = entityDto.Id;
                //eventLog.EventLogNameId = 1;
                eventLog.EventLogName = "EventLogName1";
                eventLog.Source = "SourceTest";
                eventLog.EventId = 1;
                eventLog.EntryType = EventLogEntryType.Error;
                eventLog.UserId = 1;
                eventLog.Process = "ProcessTest";
                eventLog.ComputerName = "ComputerNameTest";
                eventLog.UrlHelperLink = "URLHelperLinkTest";
                eventLog.Description = "DescriptionTest";
                eventLog.Content = "ContentTest";

                //save
                eventLog = eventLog.Save();

                //fetch back the dto
                EventLogDto dto;

                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    dto = dal.FetchById(eventLog.Id);
                }
                AssertDtoAndModelAreEqual(dto, eventLog);
            }
        }
        #endregion
    }
}
