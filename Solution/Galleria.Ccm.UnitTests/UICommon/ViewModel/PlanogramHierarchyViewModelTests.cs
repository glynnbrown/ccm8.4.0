﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26284 : A.Kuszyk
//  Created.
// V8-27280 : A.Silva
//      Added tests for GetRecentWork method.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.UICommon.ViewModel
{
    [TestFixture]
    public class PlanogramHierarchyViewModelTests : TestBase
    {
        private PlanogramHierarchyViewModel _testModel;
        private static int _lastCreatedPlanogramGroup = 0;

        public override void Setup()
        {
            base.Setup();
            _testModel = new PlanogramHierarchyViewModel();
            _testModel.FetchForCurrentEntity();
        }

     

        #region GetRecentWork method

        [Test]
        public void GetRecentWork_WhenNoRecentWork_ShouldReturnEmptyList()
        {
            IEnumerable<PlanogramGroupViewModel> actual = _testModel.GetRecentWork();

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void GetRecentWork_WhenRecentWork_ShouldReturnRecentWorkList()
        {
            var newPlanogramGroup = CreateNewPlanogramGroup();
            UpdatePlanogramHierarchy(newPlanogramGroup, _testModel);

            IEnumerable<PlanogramGroupViewModel> actual = _testModel.GetRecentWork();

            CollectionAssert.IsNotEmpty(actual);
        }

        [Test]
        public void GetRecentWork_WhenRecentWork_ShouldReturnUpToRequiredRecentWorkCount()
        {
            const Int32 expected = 15;
            const Int32 planogramGroupCount = 20;
            BatchUpdatePlanogramHierarchy(planogramGroupCount, _testModel);

            IEnumerable<PlanogramGroupViewModel> actual = _testModel.GetRecentWork(expected);

            Assert.AreEqual(expected, actual.Count());
        }

        [Test]
        public void GetRecentWork_WhenRecentWork_ShouldReturnOnlyLatestItems()
        {
            const Int32 planogramGroupCount = 20;
            BatchUpdatePlanogramHierarchy(planogramGroupCount, _testModel);
            List<String> expected = BatchUpdatePlanogramHierarchy(planogramGroupCount, _testModel).Select(o => o.Name).ToList();
            expected.Reverse();
            expected = expected.Take(10).ToList();

            IEnumerable<PlanogramGroupViewModel> recentWork = _testModel.GetRecentWork();

            List<String> actual = recentWork.Select(o => o.PlanogramGroup.Name).ToList();
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetRecentWork_WhenRecentWork_ShouldReturnIsRecentWorkTrue()
        {
            var newPlanogramGroup = CreateNewPlanogramGroup();
            UpdatePlanogramHierarchy(newPlanogramGroup, _testModel);

            IEnumerable<PlanogramGroupViewModel> actual = _testModel.GetRecentWork();

            Assert.IsTrue(actual.First().IsRecentWorkGroup);
        }

        #endregion

        #region Test Helper Methods

        private void UpdatePlanogramHierarchy(PlanogramGroup newPlanogramGroup, PlanogramHierarchyViewModel testModel)
        {
            PlanogramHierarchy currentPlanogramHierarchy = PlanogramHierarchy.FetchByEntityId(EntityId);
            currentPlanogramHierarchy.RootGroup.ChildList.Add(newPlanogramGroup);
            currentPlanogramHierarchy.Save();
            if (testModel != null) testModel.FetchForCurrentEntity();
        }

        private List<PlanogramGroup> BatchUpdatePlanogramHierarchy(Int32 planogramGroupCount, PlanogramHierarchyViewModel testModel)
        {
            List<PlanogramGroup> batch = new List<PlanogramGroup>();
            for (Int32 i = _lastCreatedPlanogramGroup; i < _lastCreatedPlanogramGroup + planogramGroupCount; i++)
            {
                PlanogramGroup item = CreateNewPlanogramGroup(String.Format("PlanogramGroup {0}", i));
                UpdatePlanogramHierarchy(item, testModel);
                batch.Add(item);
            }
            _lastCreatedPlanogramGroup += planogramGroupCount;
            return batch;
        }

        private static PlanogramGroup CreateNewPlanogramGroup(String name = null)
        {
            PlanogramGroup newPlanogramGroup = PlanogramGroup.NewPlanogramGroup();
            newPlanogramGroup.Name = name ?? "TestPlanogramGroup";
            return newPlanogramGroup;
        }

        #endregion
    }
}
