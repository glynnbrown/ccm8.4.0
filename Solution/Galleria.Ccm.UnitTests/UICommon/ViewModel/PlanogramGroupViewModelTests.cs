﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26284 : A.Kuszyk
//  Created.
// V8-28498 : A.Silva
//      Reviewed failing tests.

#endregion

#endregion

using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.UICommon.ViewModel
{
    [TestFixture]
    public class PlanogramGroupViewModelTests : TestBase
    {
        #region Fields
        private PlanogramGroupViewModel _testModel;
        private PlanogramGroup _oldParentModel;
        private PlanogramGroup _newParentModel;
        private PlanogramGroup _moveModel;
        private PlanogramGroupViewModel _oldParentView;
        private PlanogramGroupViewModel _newParentView;
        private PlanogramGroupViewModel _moveView;
        #endregion

        public override void Setup()
        {
            base.Setup();
            var planogramGroup = PlanogramGroup.NewPlanogramGroup();
            _testModel = new PlanogramGroupViewModel(planogramGroup, false);

            _oldParentModel = PlanogramGroup.NewPlanogramGroup();
            _newParentModel = PlanogramGroup.NewPlanogramGroup();
            _moveModel = PlanogramGroup.NewPlanogramGroup();
            _oldParentModel.ChildList.Add(_moveModel);
            _oldParentView = new PlanogramGroupViewModel(_oldParentModel, true);
            _newParentView = new PlanogramGroupViewModel(_newParentModel, true);
            _moveView = _oldParentView.ChildViews.FirstOrDefault();
        }

        [Test]
        public void Move_MovesModel()
        {
            _moveView.Move(_oldParentView, _newParentView);

            Assert.That(_newParentModel.ChildList.Any(g => g.Id == _moveModel.Id));
            Assert.IsFalse(_oldParentModel.ChildList.Any(g => g.Id == _moveModel.Id));
        }

        [Test]
        public void Move_MovesView()
        {
            _moveView.Move(_oldParentView, _newParentView);

            Assert.That(_newParentView.ChildViews.Any(g => g.Id == _moveView.Id));
            Assert.IsFalse(_oldParentView.ChildViews.Any(g => g.Id == _moveView.Id));
        }

        [Test]
        public void AddGroup_AddsView()
        {
            var newModel = PlanogramGroup.NewPlanogramGroup();
            var newView = new PlanogramGroupViewModel(newModel, false);

            _newParentView.AddGroup(newView);

            CollectionAssert.Contains(_newParentView.ChildViews, newView);
        }

        [Test]
        public void AddGroup_AddsModel()
        {
            var newModel = PlanogramGroup.NewPlanogramGroup();
            var newView = new PlanogramGroupViewModel(newModel, false);

            _newParentView.AddGroup(newView);

            CollectionAssert.Contains(_newParentView.PlanogramGroup.ChildList, newModel);
        }

        [Test]
        public void RemoveGroup_RemovesView()
        {
            _oldParentView.RemoveGroup(_moveView);

            CollectionAssert.DoesNotContain(_oldParentView.ChildViews, _moveView);
        }

        [Test]
        public void RemoveGroup_RemovesModel()
        {
            _oldParentView.RemoveGroup(_moveView);

            CollectionAssert.DoesNotContain(_oldParentView.PlanogramGroup.ChildList, _moveModel);
        }
    }
}
