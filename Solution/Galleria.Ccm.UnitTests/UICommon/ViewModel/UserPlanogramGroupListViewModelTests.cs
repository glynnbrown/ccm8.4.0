﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26284 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.UICommon.ViewModel
{
    [TestFixture]
    public class UserPlanogramGroupListViewModelTests : TestBase
    {
        private UserPlanogramGroupListViewModel _testModel;

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            _testModel = new UserPlanogramGroupListViewModel();
            _testModel.FetchForCurrentUserAndEntity();
        }

        //[Test]
        //public void GetFavourites_Throws_WhenPlanHierarchyIsNull()
        //{
        //    TestDelegate test = () => _testModel.GetFavourites(null);
        //    Assert.Throws<ArgumentNullException>(test);
        //}

        //[Test]
        //public void GetFavourites_ReturnsListOfViews()
        //{
        //    InsertTestData();
        //    var planHierarchy = new PlanogramHierarchyViewModel();
        //    planHierarchy.FetchForCurrentEntity();
        //    var models = planHierarchy.Model.EnumerateAllGroups();
        //    planHierarchy.AllGroups.ForEach(g => _testModel.AddGroupToFavourites(g));

        //    var views = _testModel.GetFavourites(planHierarchy);

        //    var modelIds = models.Select(m => m.Id).ToList();
        //    var viewIds = views.Select(v => v.Id).ToList();
        //    CollectionAssert.AreEquivalent(modelIds, viewIds);
        //}

        //[Test]
        //public void AddGroupToFavourites_Throws_WhenGroupIsNull()
        //{
        //    TestDelegate test = () => _testModel.AddGroupToFavourites(null);
        //    Assert.Throws<ArgumentNullException>(test);
        //}

        //[Test]
        //public void AddPlanogramGroupToFavourites_AddsGroupToModel()
        //{
        //    var groupModel = PlanogramGroup.NewPlanogramGroup();

        //    _testModel.AddGroupToFavourites(groupModel);

        //    Assert.That(_testModel.Model.Any(g => g.Id == groupModel.Id));
        //}

        //[Test]
        //public void RemoveGroupFromFavourites_Throws_WhenGroupIsNull()
        //{
        //    TestDelegate test = () => _testModel.RemoveGroupFromFavourites(null);
        //    Assert.Throws<ArgumentNullException>(test);
        //}

        [Test]
        public void RemovePlanogramGroupFromFavourites_RemovesGroup()
        {
            InsertTestData();

            PlanogramHierarchy planHierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);

            _testModel.AddGroupToFavourites(planHierarchy.RootGroup.ChildList[0]);

            _testModel.RemoveGroupFromFavourites(planHierarchy.RootGroup.ChildList[0]);

            Assert.IsFalse(_testModel.Model.Any(g => g.Id == planHierarchy.RootGroup.ChildList[0].Id));
        }

        private void InsertTestData()
        {
            //Create a planogram hierarchy
            PlanogramHierarchy planHierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
            planHierarchy.RootGroup.ChildList.Clear();

            PlanogramGroup g1 = PlanogramGroup.NewPlanogramGroup();
            g1.Name = "Canned Foods";
            planHierarchy.RootGroup.ChildList.Add(g1);

            PlanogramGroup g2 = PlanogramGroup.NewPlanogramGroup();
            g2.Name = "Tinned Veg";
            g1.ChildList.Add(g2);

            PlanogramGroup g3 = PlanogramGroup.NewPlanogramGroup();
            g3.Name = "Tinned Fruit";
            g1.ChildList.Add(g3);

            PlanogramGroup g4 = PlanogramGroup.NewPlanogramGroup();
            g4.Name = "Condiments";
            planHierarchy.RootGroup.ChildList.Add(g4);

            PlanogramGroup g5 = PlanogramGroup.NewPlanogramGroup();
            g5.Name = "Sweets";
            g4.ChildList.Add(g5);

            PlanogramGroup g6 = PlanogramGroup.NewPlanogramGroup();
            g6.Name = "Chocolate";
            g4.ChildList.Add(g6);

            PlanogramGroup g7 = PlanogramGroup.NewPlanogramGroup();
            g7.Name = "Dressings";
            planHierarchy.RootGroup.ChildList.Add(g7);

            planHierarchy = planHierarchy.Save();
        }
    }
}
