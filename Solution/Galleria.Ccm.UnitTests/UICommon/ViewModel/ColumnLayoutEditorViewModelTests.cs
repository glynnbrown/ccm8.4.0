﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM v8.0)

//// V8-26513 : A.Silva
////      Created.
//// V8-28058 : A.Silva
////      Added missing code standard tests.

//#endregion

//#region Version History: (CCM v8.2.0)
////V8-30927 : L.Ineson
////  Removed old tests as they are out of date and brought nothing to the table.
//#endregion

//#endregion

//using System;
//using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
//using Galleria.Ccm.UnitTests.UI;
//using Galleria.Framework.ViewModel;
//using NUnit.Framework;

//namespace Galleria.Ccm.UnitTests.UICommon.ViewModel
//{
    

//    [TestFixture]
//    public class ColumnLayoutEditorViewModelTests : TestBase
//    {
//        //TODO: Add some tests that are actually worthwhile!!
//    }
//}