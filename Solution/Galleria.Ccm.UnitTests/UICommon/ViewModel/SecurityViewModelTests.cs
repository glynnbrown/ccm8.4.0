﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27528 : M.Shelley
//  Created Desaware product licensing viewmodel unit tests
#endregion

#endregion

using System;
using Galleria.Ccm.Common.Wpf.Licensing;
using Galleria.Ccm.Security;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.UI;

namespace Galleria.Ccm.UnitTests.UICommon.ViewModel
{
    [TestFixture]
    public class SecurityViewModelTests : TestBase<SecurityViewModel>
    {
        #region Setup / Teardown

        [SetUp]
        public void Setup()
        {
            this.TestModel = new SecurityViewModel(true);
        }

        #endregion

        #region Properties

        [Test]
        public void Property_SecurityInputKey()
        {
            String propertyName = "SecurityInputKey";
            Assert.AreEqual(propertyName, SecurityViewModel.SecurityInputKeyProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "TestInputKey";
            this.TestModel.SecurityInputKey = testValue;
            Assert.AreEqual(testValue, this.TestModel.SecurityInputKey);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_SecurityCompanyAddress()
        {
            String propertyName = "SecurityCompanyAddress";
            Assert.AreEqual(propertyName, SecurityViewModel.SecurityCompanyAddressProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "TestAddress";
            this.TestModel.SecurityCompanyAddress = testValue;
            Assert.AreEqual(testValue, this.TestModel.SecurityCompanyAddress);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_SecurityCompanyName()
        {
            String propertyName = "SecurityCompanyName";
            Assert.AreEqual(propertyName, SecurityViewModel.SecurityCompanyNameProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "TestCompanyName";
            this.TestModel.SecurityCompanyName = testValue;
            Assert.AreEqual(testValue, this.TestModel.SecurityCompanyName);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_SecurityCompanyPhone()
        {
            String propertyName = "SecurityCompanyPhone";
            Assert.AreEqual(propertyName, SecurityViewModel.SecurityCompanyPhoneProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "TestCompanyPhone";
            this.TestModel.SecurityCompanyPhone = testValue;
            Assert.AreEqual(testValue, this.TestModel.SecurityCompanyPhone);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_SecurityUserName()
        {
            String propertyName = "SecurityUserName";
            Assert.AreEqual(propertyName, SecurityViewModel.SecurityUserNameProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "TestUserName";
            this.TestModel.SecurityUserName = testValue;
            Assert.AreEqual(testValue, this.TestModel.SecurityUserName);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_LicenseMsg()
        {
            String propertyName = "LicenseMsg";
            Assert.AreEqual(propertyName, SecurityViewModel.LicenseMsgProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "TestLicenseMsg";
            this.TestModel.LicenseMsg = testValue;
            Assert.AreEqual(testValue, this.TestModel.LicenseMsg);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_SecurityInstallType()
        {
            String propertyName = "SecurityInstallType";
            Assert.AreEqual(propertyName, SecurityViewModel.SecurityInstallTypeProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            InstallType testValue = InstallType.Demo;
            this.TestModel.SecurityInstallType = testValue;
            Assert.AreEqual(testValue, this.TestModel.SecurityInstallType);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }

        [Test]
        public void Property_IsValid()
        {
            String propertyName = "IsValid";
            Assert.AreEqual(propertyName, SecurityViewModel.IsValidProperty.Path);

            Assert.IsNotNull(this.TestModel.IsValid);
            Assert.IsTrue(this.TestModel.IsValid, "All properties should be valid");
        }

        [Test]
        public void Property_SecurityCompanyEmail()
        {
            String propertyName = "SecurityCompanyEmail";
            Assert.AreEqual(propertyName, SecurityViewModel.securityCompanyEmailProperty.Path);

            // check setter
            this.PropertyChangedNotifications.Clear();
            String testValue = "TestEmail";
            this.TestModel.SecurityCompanyEmail = testValue;
            Assert.AreEqual(testValue, this.TestModel.SecurityCompanyEmail);
            Assert.Contains(propertyName, this.PropertyChangedNotifications);
        }
        #endregion

        #region Constructor
        [Test]
        public void Constructor()
        {
        }

        #endregion

        #region Commands
        [Test]
        public void Command_Save()
        {
            RelayCommand cmd = this.TestModel.SaveCommand;

            //check the command is registered
            Assert.Contains(cmd, this.TestModel.ViewModelCommands, "Command should be registered with the viewmodel base");

            //check the command properties
            Assert.IsTrue(!String.IsNullOrEmpty(cmd.FriendlyName), "Friendly name should be populated");

            //Check can execute
            Assert.IsFalse(cmd.CanExecute(), "Should be false as not all input fields set");
            this.TestModel.SecurityCompanyAddress = "TestAddress";
            Assert.IsFalse(cmd.CanExecute(), "Should be false as not all input fields set");
            this.TestModel.SecurityCompanyEmail = "test@email.com";
            Assert.IsFalse(cmd.CanExecute(), "Should be false as not all input fields set");
            this.TestModel.SecurityCompanyName = "TestCompanyName";
            Assert.IsFalse(cmd.CanExecute(), "Should be false as not all input fields set");
            this.TestModel.SecurityCompanyPhone = "TestCompanyPhone";
            Assert.IsFalse(cmd.CanExecute(), "Should be false as not all input fields set");
            this.TestModel.SecurityUserName = "TestUserName";
            Assert.IsTrue(cmd.CanExecute(), "Should be true as all input fields set");

            //execute the command
            cmd.Execute();

            //ensure a demo license is issued 
            Assert.AreEqual(this.TestModel.SecurityInstallType, InstallType.Demo, "Should have issued a demo license");
        }
        #endregion

        #region Methods

        [Test]
        public void Method_ValidateProperties()
        {
            //ensure all properties are valid 
            Assert.AreEqual(this.TestModel.ValidateProperties(), true, "All properties should be valid");
        }

        [Test]
        public void Method_SecurityWindowRequired()
        {
            //ensure security window is required 
            Assert.AreEqual(this.TestModel.SecurityWindowRequired(), true, "Security window should be required");
        }

        [Test]
        public void Method_LoadRegistrationDetails()
        {
            //load registration details
            this.TestModel.LoadRegistrationDetails();

            //ensure the correct details are loaded
            Assert.AreEqual(this.TestModel.SecurityUserName, this.TestModel.defaultSecurityUserName, "Default security user name should be used");
            Assert.AreEqual(this.TestModel.SecurityCompanyName, this.TestModel.defaultSecurityCompanyName, "Default security company name should be used");
            Assert.AreEqual(this.TestModel.SecurityCompanyAddress, this.TestModel.defaultSecurityCompanyAddress, "Default security company address should be used");
            Assert.AreEqual(this.TestModel.SecurityCompanyPhone, this.TestModel.defaultSecurityCompanyPhone, "Default security company phone should be used");
            Assert.AreEqual(this.TestModel.SecurityCompanyEmail, this.TestModel.defaultSecurityCompanyEmail, "Default security company email should be used");
        }

        [Test]
        public void Method_IsEmailValid()
        {
            string emailAddress;

            //create a valid email
            emailAddress = "user@test.com";
            //Test that email is valid
            Assert.IsTrue(this.TestModel.IsEmailValid(emailAddress), "Email should have been valid");

            //create a invalid email
            emailAddress = "test.com";
            //Test that email is valid
            Assert.IsFalse(this.TestModel.IsEmailValid(emailAddress), "Email should have been invalid");
        }

        #endregion
    }
}
