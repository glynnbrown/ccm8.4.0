﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28864 : A.Silva
//      Created

#endregion

#endregion

using System;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ValidationRules;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UICommon.ValidationRules
{
    [TestFixture]
    public class ByteValidationRuleTests
    {
        #region Constants

        private static readonly String ExpectedErrorContent = String.Format(Message.ByteValidationRule_OutOfRangeError,
            Byte.MinValue, Byte.MaxValue);

        #endregion

        [Test]
        public void Validate_WhenNull_ShouldFail()
        {
            const String expectation = "ByteValidationRule should fail when a null value is passed.";
            ByteValidationRule validationRule = new ByteValidationRule();

            ValidationResult validationResult = validationRule.Validate(null, null);

            Assert.IsFalse(validationResult.IsValid, expectation);
            Assert.AreEqual(validationResult.ErrorContent, ExpectedErrorContent);
        }

        [Test]
        public void Validate_WhenNonNumeric_ShouldFail()
        {
            const String expectation = "ByteValidationRule should fail when a non numeric value is passed.";
            ByteValidationRule validationRule = new ByteValidationRule();

            ValidationResult validationResult = validationRule.Validate("invalidStringValue", null);

            Assert.IsFalse(validationResult.IsValid, expectation);
            Assert.AreEqual(validationResult.ErrorContent, ExpectedErrorContent);
        }

        [Test]
        public void Validate_WhenNumericTooLarge_ShouldFail()
        {
            const String expectation = "ByteValidationRule should fail when a too large value is passed.";
            ByteValidationRule validationRule = new ByteValidationRule();

            ValidationResult validationResult = validationRule.Validate(Byte.MaxValue + 1, null);

            Assert.IsFalse(validationResult.IsValid, expectation);
            Assert.AreEqual(validationResult.ErrorContent, ExpectedErrorContent);
        }

        [Test]
        public void Validate_WhenNumericTooSmall_ShouldFail()
        {
            const String expectation = "ByteValidationRule should fail when a too small value is passed.";
            ByteValidationRule validationRule = new ByteValidationRule();

            ValidationResult validationResult = validationRule.Validate(Byte.MinValue - 1, null);

            Assert.IsFalse(validationResult.IsValid, expectation);
            Assert.AreEqual(validationResult.ErrorContent, ExpectedErrorContent);
        }

        [Test]
        public void Validate_WhenNumericInRange_ShouldPass()
        {
            const String expectation = "ByteValidationRule should pass when valid value is passed.";
            ByteValidationRule validationRule = new ByteValidationRule();

            ValidationResult validationResult = validationRule.Validate(Byte.MinValue + 1, null);

            Assert.IsTrue(validationResult.IsValid, expectation);
            Assert.IsNull(validationResult.ErrorContent, "ByteValidationRule should return an empty ErrorContent is");
        }
    }
}
