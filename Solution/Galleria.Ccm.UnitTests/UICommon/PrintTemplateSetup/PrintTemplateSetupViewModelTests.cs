﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2)
// V8-30738.Ineson
//  Created
#endregion


#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.PrintTemplateSetup;

namespace Galleria.Ccm.UnitTests.UICommon.PrintTemplateSetup
{
    [TestFixture]
    public class PrintTemplateSetupViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private void InsertExistingItems()
        {
            for (Int32 i = 0; i < 5; i++)
            {
                PrintTemplate template = PrintTemplate.NewPrintTemplate(this.EntityId);
                template.Name = "template" + i;

               PrintTemplateSectionGroup group1 =  template.SectionGroups.AddNewSectionGroup();

               foreach (PrintTemplateComponentType compType in
                   Enum.GetValues(typeof(PrintTemplateComponentType)).Cast<PrintTemplateComponentType>())
               {
                   //planogram
                   PrintTemplateSection sect1 = group1.Sections.AddNewSection();
                   PrintTemplateComponent comp=  sect1.Components.AddNewComponent(compType);
                   comp.Height = 100;
                   comp.Width = 100;
               }

               template.SaveAsToRepository(this.EntityId);
            }
        }

        #endregion

        #region Properties

        [Test]
        public void AvailablePrintTemplates_FetchedOnStartup()
        {
            //insert some workflows.
            InsertExistingItems();

            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.AreNotEqual(0, viewModel.AvailablePrintTemplates.Count);
        }

        [Test]
        public void AvailablePrintTemplates_UpdatedOnPrintTemplateInsert()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Int32 count = viewModel.AvailablePrintTemplates.Count;

            //save the new workflow
            viewModel.CurrentPrintTemplate.Name = "w1";
            viewModel.SaveCommand.Execute();

            Assert.AreEqual(count + 1, viewModel.AvailablePrintTemplates.Count);
        }

        [Test]
        public void AvailablePrintTemplates_UpdatedOnPrintTemplateDelete()
        {
            InsertExistingItems();

            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Int32 count = viewModel.AvailablePrintTemplates.Count;

            //open and delete a workflow
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);
            viewModel.DeleteCommand.Execute();

            Assert.AreEqual(count - 1, viewModel.AvailablePrintTemplates.Count);
        }

        [Test]
        public void CurrentPrintTemplate_InitializedAsNew()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.IsTrue(viewModel.CurrentPrintTemplate.IsNew);
            Assert.IsNullOrEmpty(viewModel.CurrentPrintTemplate.Name);
        }

        [Test]
        public void SelectedSectionGroup_Setter()
        {
            Assert.Ignore();
        }

        [Test]
        public void SelectedSection_Setter()
        {
            Assert.Ignore();
        }

        [Test]
        public void SelectedComponent_Setter()
        {
            Assert.Ignore();
        }

        [Test]
        public void AvailableFontSizes_IsInitialized()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.AreNotEqual(0, viewModel.AvailableFontSizes.Count);
        }

        [Test]
        public void AvailableFonts_IsInitialized()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.AreNotEqual(0, viewModel.AvailableFonts.Count);
        }

        [Test]
        public void AvailableInsertFields_IsInitialized()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.AreNotEqual(0, viewModel.AvailableInsertFields.Count);
        }

        [Test]
        public void SelectedComponentToolType_Setter()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            //check it starts on select
            Assert.AreEqual(PrintTemplateComponentToolType.Select, viewModel.SelectedComponentToolType);

           //change it
            base.PropertyChangedNotifications.Clear();
            viewModel.PropertyChanged += base.TestModel_PropertyChanged;
            viewModel.SelectedComponentToolType = PrintTemplateComponentToolType.Planogram;
            Assert.Contains("SelectedComponentToolType", base.PropertyChangedNotifications);
            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region Commands

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            var oldModel = viewModel.CurrentPrintTemplate;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.CurrentPrintTemplate);
            Assert.IsTrue(viewModel.CurrentPrintTemplate.IsNew);
        }

        [Test]
        public void NewCommand_EnabledWhenNoCreatePerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PrintTemplateCreate.ToString());

            //check
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.IsTrue(viewModel.NewCommand.CanExecute());

            //nb - this is different to other models as this allows file saves too.
        }

        [Test]
        public void NewCommand_HasExpectedProperties()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            RelayCommand cmd = viewModel.NewCommand;
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_New, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_New_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.ImageResources.New_32.ToString(), cmd.Icon.ToString());
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.ImageResources.New_16.ToString(), cmd.SmallIcon.ToString());
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.N, cmd.InputGestureKey);
        }

        [Test]
        public void OpenFromRepositoryCommand_Execution()
        {
            InsertExistingItems();
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            foreach (var info in viewModel.AvailablePrintTemplates)
            {
                viewModel.OpenFromRepositoryCommand.Execute(info.Id);
                Assert.AreEqual(info.Id, viewModel.CurrentPrintTemplate.Id);
                Assert.IsTrue(viewModel.CurrentPrintTemplate.IsInitialized);
            }
        }

        [Test]
        public void OpenFromFileCommand_Execution()
        {
            InsertExistingItems();
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            //create a new item
            String itemName = "Test1";
            viewModel.CurrentPrintTemplate.Name = itemName;
            String filePath = System.IO.Path.ChangeExtension(System.IO.Path.Combine(this.TestDir, itemName), PrintTemplate.FileExtension);
            viewModel.SaveAsToFileCommand.Execute(filePath);

            viewModel.NewCommand.Execute();
            Assert.AreNotEqual(itemName, viewModel.CurrentPrintTemplate.Name);

            viewModel.OpenFromFileCommand.Execute(filePath);

            Assert.AreEqual(itemName, viewModel.CurrentPrintTemplate.Name);
        }

        [Test]
        public void SaveCommand_DisabledWhenInvalid()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.IsFalse(viewModel.CurrentPrintTemplate.IsValid);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());

            viewModel.CurrentPrintTemplate.Name = "Test1";

            Assert.IsTrue(viewModel.CurrentPrintTemplate.IsValid);
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenNewItemNoCreatePerm()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            viewModel.CurrentPrintTemplate.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PrintTemplateCreate.ToString());

            viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenOldItemNoCreatePerm()
        {
            InsertExistingItems();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PrintTemplateCreate.ToString());

            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);
            viewModel.CurrentPrintTemplate.Name = "NewName";

            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_EnabledWhenNewItemNoEditPerm()
        {
            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PrintTemplateEdit.ToString());

            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            viewModel.CurrentPrintTemplate.Name = "Test1";
            Assert.IsTrue(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_DisabledWhenOldItemNoEditPerm()
        {
            InsertExistingItems();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PrintTemplateEdit.ToString());

            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);
            viewModel.CurrentPrintTemplate.Name = "NewName";

            Assert.IsFalse(viewModel.SaveCommand.CanExecute());
        }

        [Test]
        public void SaveCommand_HasExpectedProperties()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            RelayCommand cmd = viewModel.SaveCommand;
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_Save, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_Save_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.ImageResources.Save_32.ToString(), cmd.Icon.ToString());
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.ImageResources.Save_16.ToString(), cmd.SmallIcon.ToString());
            Assert.AreEqual(System.Windows.Input.ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(System.Windows.Input.Key.S, cmd.InputGestureKey);
        }

        [Test]
        public void SaveCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            viewModel.CurrentPrintTemplate.Name = "Test1";

            viewModel.SaveCommand.Execute();

            Assert.AreEqual("Test1", viewModel.CurrentPrintTemplate.Name);
            Assert.IsFalse(viewModel.CurrentPrintTemplate.IsDirty);
        }

        [Test]
        public void SaveAsToRepositoryCommand_DisabledWhenNoCreatePerm()
        {
            InsertExistingItems();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PrintTemplateCreate.ToString());

            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);
            viewModel.CurrentPrintTemplate.Name = "NewName";

            Assert.IsFalse(viewModel.SaveAsToRepositoryCommand.CanExecute());
        }

        [Test]
        public void SaveAsToRepositoryCommand_Execution()
        {
            InsertExistingItems();
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Int32 preCount = PrintTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);

            String newName = "SaveAsName";

            WindowService.PromptForSaveAsNameSetReponse(true, newName);
            viewModel.SaveAsToRepositoryCommand.Execute();

            Assert.AreEqual(newName, viewModel.CurrentPrintTemplate.Name);
            Assert.IsFalse(viewModel.CurrentPrintTemplate.IsDirty);

            Assert.AreEqual(preCount + 1, PrintTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void SaveAsToFile_Execution()
        {
            InsertExistingItems();
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            //create a new item
            String itemName = "Test1";
            viewModel.CurrentPrintTemplate.Name =itemName;
            String filePath = System.IO.Path.ChangeExtension(System.IO.Path.Combine(this.TestDir, itemName), PrintTemplate.FileExtension);
            viewModel.SaveAsToFileCommand.Execute(filePath);

            Assert.IsTrue(System.IO.File.Exists(filePath));

            Assert.IsFalse(viewModel.CurrentPrintTemplate.IsDirty);
        }

        [Test]
        public void SaveAndNewCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            viewModel.CurrentPrintTemplate.Name = "Test1";

            viewModel.SaveAndNewCommand.Execute();

            Assert.AreNotEqual("Test1", viewModel.CurrentPrintTemplate.Name);
            Assert.IsTrue(viewModel.CurrentPrintTemplate.IsNew);
        }

        [Test]
        public void SaveAndNewCommand_HasExpectedProperties()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            RelayCommand cmd = viewModel.SaveAndNewCommand;
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_SaveAndNew, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_SaveAndNew_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.ImageResources.SaveAndNew_16.ToString(), cmd.SmallIcon.ToString());
        }

        [Test]
        public void SaveAndCloseCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            viewModel.CurrentPrintTemplate.Name = "Test1";

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.SaveAndCloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");

            //check that the item was saved
            Assert.AreEqual("Test1", PrintTemplateInfoList.FetchByEntityId(this.EntityId).Last().Name);
        }

        [Test]
        public void SaveAndCloseCommand_HasExpectedProperties()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            RelayCommand cmd = viewModel.SaveAndCloseCommand;
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_SaveAndClose, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_SaveAndClose_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.ImageResources.SaveAndClose_16.ToString(), cmd.SmallIcon.ToString());
        }

        [Test]
        public void DeleteCommand_DisabledForNewItem()
        {
            InsertExistingItems();
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.IsTrue(viewModel.CurrentPrintTemplate.IsNew);
            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());

            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);
            Assert.IsFalse(viewModel.CurrentPrintTemplate.IsNew);
            Assert.IsTrue(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_DisabledWhenIsRepositoryAndNoDeletePerm()
        {
            InsertExistingItems();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PrintTemplateDelete.ToString());

            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);

            Assert.IsFalse(viewModel.DeleteCommand.CanExecute());
        }

        [Test]
        public void DeleteCommand_EnabledWhenIsFileAndNoDeletePerm()
        {
            Assert.Ignore();
        }

        [Test]
        public void DeleteCommand_Execution()
        {
            InsertExistingItems();
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Int32 preCount = PrintTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(true);
            viewModel.DeleteCommand.Execute();

            Assert.IsTrue(viewModel.CurrentPrintTemplate.IsNew);
            Assert.AreEqual(preCount - 1, PrintTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_CancelUserConfirm()
        {
            InsertExistingItems();
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Int32 preCount = PrintTemplateInfoList.FetchByEntityId(this.EntityId).Count;
            viewModel.OpenFromRepositoryCommand.Execute(viewModel.AvailablePrintTemplates.First().Id);

            WindowService.ConfirmDeleteWithUserSetResponse(false);
            viewModel.DeleteCommand.Execute();

            Assert.IsFalse(viewModel.CurrentPrintTemplate.IsNew);
            Assert.AreEqual(preCount, PrintTemplateInfoList.FetchByEntityId(this.EntityId).Count);
        }

        [Test]
        public void DeleteCommand_HasExpectedProperties()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            RelayCommand cmd = viewModel.DeleteCommand;
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.Language.Message.Generic_Delete, cmd.FriendlyName);
            Assert.AreEqual(Galleria.Ccm.Common.Wpf.Resources.ImageResources.Delete_16.ToString(), cmd.SmallIcon.ToString());
        }

        [Test]
        public void CloseCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.CloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");
        }

        [Test]
        public void AddSectionCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            //add another section.
            viewModel.AddSectionCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups[0].Sections.Count);
        }

        [Test]
        public void RemoveSectionCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            //add another section.
            viewModel.AddSectionCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups[0].Sections.Count);

            //remove the first
            var leftSection = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[1];
            viewModel.SelectedSection = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[0];

            viewModel.RemoveSectionCommand.Execute();
            Assert.AreEqual(1, viewModel.CurrentPrintTemplate.SectionGroups[0].Sections.Count);
            Assert.AreEqual(leftSection, viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[0]);
            Assert.AreEqual(leftSection, viewModel.SelectedSection);
        }

        [Test]
        public void RemoveSectionCommand_DisabledWhenLastInGroup()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            viewModel.SelectedSection = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[0];
            Assert.IsFalse(viewModel.RemoveSectionCommand.CanExecute());

            //add another section.
            viewModel.AddSectionCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups[0].Sections.Count);

            Assert.IsTrue(viewModel.RemoveSectionCommand.CanExecute());
        }

        [Test]
        public void MoveSectionUpCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

           //add another section.
            viewModel.AddSectionCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups[0].Sections.Count);

            //select and move it up
            viewModel.SelectedSection = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[1];

            var s1 = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[0];
            var s2 = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[1];
            viewModel.MoveSectionUpCommand.Execute();

            //check
            Assert.AreEqual(2, s1.Number);
            Assert.AreEqual(1, s2.Number);
        }

        [Test]
        public void MoveSectionDownCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            //add another section.
            viewModel.AddSectionCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups[0].Sections.Count);

            var s1 = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[0];
            var s2 = viewModel.CurrentPrintTemplate.SectionGroups[0].Sections[1];

            //select the first one
            viewModel.SelectedSection = s1;

            //move it down
            viewModel.MoveSectionDownCommand.Execute();
            Assert.AreEqual(2, s1.Number);
            Assert.AreEqual(1, s2.Number);
        }

        [Test]
        public void RemoveSectionPreviewCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void MoveSectionPreviewUpCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void MoveSectionPreviewDownCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void MoveSectionPreviewToSectionGroupCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void AddSectionGroupCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.AreEqual(1, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            //add another section group
            viewModel.AddSectionGroupCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups.Count);
        }

        [Test]
        public void RemoveSectionGroupCommand_DisabledWhenLastGroup()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            Assert.IsFalse(viewModel.RemoveSectionGroupCommand.CanExecute());

            //add one
            viewModel.AddSectionGroupCommand.Execute();

            //check it is now enabled
            Assert.IsTrue(viewModel.RemoveSectionGroupCommand.CanExecute());
        }

        [Test]
        public void RemoveSectionGroupCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);
            viewModel.AddSectionGroupCommand.Execute();

            //select the first one and remove it
            viewModel.SelectedSectionGroup = viewModel.CurrentPrintTemplate.SectionGroups[0];

            //remove it
            var leftOverGroup = viewModel.CurrentPrintTemplate.SectionGroups[1];
            viewModel.RemoveSectionGroupCommand.Execute();

            Assert.AreEqual(1, viewModel.CurrentPrintTemplate.SectionGroups.Count);
            Assert.AreEqual(leftOverGroup, viewModel.CurrentPrintTemplate.SectionGroups[0]);
            Assert.AreEqual(leftOverGroup, viewModel.SelectedSectionGroup);
        }

        [Test]
        public void RemoveSectionGroupAndSectionsCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void RemoveSectionGroupOnlyCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void MoveSectionGroupUpCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.AreEqual(1, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            //add another section group
            viewModel.AddSectionGroupCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            var g1 = viewModel.CurrentPrintTemplate.SectionGroups[0];
            var g2 = viewModel.CurrentPrintTemplate.SectionGroups[1];

            //select g2 and move it up
            viewModel.SelectedSectionGroup = g2;
            viewModel.MoveSectionGroupUpCommand.Execute();

            Assert.AreEqual(2, g1.Number);
            Assert.AreEqual(1, g2.Number);
        }

        [Test]
        public void MoveSectionGroupUpCommand_DisabledWhenTopGroup()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.AreEqual(1, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            //add another section group
            viewModel.AddSectionGroupCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            var g1 = viewModel.CurrentPrintTemplate.SectionGroups[0];
            var g2 = viewModel.CurrentPrintTemplate.SectionGroups[1];

            //select g1
            viewModel.SelectedSectionGroup = g1;
            Assert.IsFalse(viewModel.MoveSectionGroupUpCommand.CanExecute());

            //select g2
            viewModel.SelectedSectionGroup = g2;
            Assert.IsTrue(viewModel.MoveSectionGroupUpCommand.CanExecute());
        }

        [Test]
        public void MoveSectionGroupDownCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.AreEqual(1, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            //add another section group
            viewModel.AddSectionGroupCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            var g1 = viewModel.CurrentPrintTemplate.SectionGroups[0];
            var g2 = viewModel.CurrentPrintTemplate.SectionGroups[1];

            //select g1 and move it down
            viewModel.SelectedSectionGroup = g1;
            viewModel.MoveSectionGroupDownCommand.Execute();

            Assert.AreEqual(2, g1.Number);
            Assert.AreEqual(1, g2.Number);
        }

        [Test]
        public void MoveSectionGroupDownCommand_DisabledWhenLastGroup()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.AreEqual(1, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            //add another section group
            viewModel.AddSectionGroupCommand.Execute();
            Assert.AreEqual(2, viewModel.CurrentPrintTemplate.SectionGroups.Count);

            var g1 = viewModel.CurrentPrintTemplate.SectionGroups[0];
            var g2 = viewModel.CurrentPrintTemplate.SectionGroups[1];

            //select g1
            viewModel.SelectedSectionGroup = g1;
            Assert.IsTrue(viewModel.MoveSectionGroupDownCommand.CanExecute());

            //select g2
            viewModel.SelectedSectionGroup = g2;
            Assert.IsFalse(viewModel.MoveSectionGroupDownCommand.CanExecute());
        }

        [Test]
        public void RemoveSectionGroupPreviewCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void MoveSectionGroupPreviewUpCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void MoveSectionGroupPreviewDownCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void RemoveComponentCommand_Execution()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            //add a component
            viewModel.SelectedComponent =  viewModel.SelectedSection.Components.AddNewComponent(PrintTemplateComponentType.Planogram);
            
            //remove it
            viewModel.RemoveComponentCommand.Execute();
            Assert.AreEqual(0, viewModel.SelectedSection.Components.Count);
        }

        [Test]
        public void RemoveComponentCommand_DisabledWhenNoneSelected()
        {
            var viewModel = new PrintTemplateSetupViewModel(this.EntityId);

            Assert.IsFalse(viewModel.RemoveComponentCommand.CanExecute());

            //add a component
            viewModel.SelectedComponent = viewModel.SelectedSection.Components.AddNewComponent(PrintTemplateComponentType.Planogram);

            Assert.IsTrue(viewModel.RemoveComponentCommand.CanExecute());
        }

        [Test]
        public void CopyComponentCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void PasteComponentCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void InsertTextCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void PrintPreviewCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void SetProductLabelCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void SetFixtureLabelCommand_Execution()
        {
            Assert.Ignore();
        }

        [Test]
        public void SetHighlightCommand_Execution()
        {
            Assert.Ignore();
        }


        [Test]
        public void SetDataSheetCommand_Execution()
        {
            Assert.Ignore();
        }

        #endregion

    }
}
