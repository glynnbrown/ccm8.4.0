﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26284 : A.Kuszyk
//  Created (copied from WorkflowUI namespace).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Input;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.UICommon
{
    public abstract class TestBase
    {
        #region Fields
        private IDalFactory _planogramDalFactory; // the dal factory to use for testing
        private IDalFactory _userDalFactory; // the dal factory to use for testing
        private IDalFactory _dalFactory;
        private String _dalName;
        private String _originalDalName;
        private readonly List<String> _propertyChangedNotifications = new List<String>();
        private readonly List<String> _propertyChangingNotifications = new List<String>();
        private String _tempDir;
        #endregion

        #region Properties

        protected List<String> PropertyChangedNotifications
        {
            get { return _propertyChangedNotifications; }
        }

        protected List<String> PropertyChangingNotifications
        {
            get { return _propertyChangingNotifications; }
        }

        protected IDalFactory PlanogramDalFactory
        {
            get { return _planogramDalFactory; }
        }

        protected IDalFactory UserDalFactory
        {
            get { return _userDalFactory; }
        }

        protected IDalFactory DalFactory
        {
            get { return _dalFactory; }
        }

        protected String TestDir
        {
            get { return _tempDir; }
        }

        public Int32 EntityId { get; protected set; }

        public NUnitWindowService WindowService
        {
            get { return (NUnitWindowService)Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>(); }
        }

        #endregion

        #region Methods

        [SetUp]
        public virtual void Setup()
        {
            _originalDalName = DalContainer.DalName;

            _tempDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName,
                    "Test Files");

            if (Directory.Exists(_tempDir))
            {
                try
                {
                    Directory.Delete(_tempDir, true);
                }
                catch { }
            }
            Directory.CreateDirectory(_tempDir);


            DalContainer.Reset();
            _propertyChangedNotifications.Clear();

            #region Planogram Mock Dal
            //Register the planogram mock dal.
            String dalName = Guid.NewGuid().ToString();
            _planogramDalFactory = new Galleria.Framework.Planograms.Dal.Mock.DalFactory();
            DalContainer.RegisterFactory(dalName, _planogramDalFactory);
            #endregion

            #region User Dal
            String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName, "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (string file in Directory.GetFiles(templateDir))
                {
                    //if (!Path.GetFileName(file).StartsWith(templateName))
                    //{
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch
                    {
                        // No point in crying over spilt milk
                    }
                    //}
                }
            }


            String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
            var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
            userFactory.UnitTestFolder = templateDir;
            _userDalFactory = userFactory;
            DalContainer.RegisterFactory(Constants.UserDal, _userDalFactory);
            #endregion

            #region Mock Dal

            //Register the main mock dal
            _dalName = Guid.NewGuid().ToString();
            _dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();
            DalContainer.RegisterFactory(_dalName, _dalFactory);
            _dalFactory.CreateDatabase();

            // set this factory to be the default of the container
            DalContainer.DalName = _dalName;

            #endregion

            //register the window service
            Galleria.Ccm.Services.ServiceContainer.RegisterServiceObject<IWindowService>(new NUnitWindowService());
            Galleria.Ccm.Services.ServiceContainer.RegisterServiceObject<IModalBusyWorkerService>(new NUnitModalBusyWorkerService());

            //register the tasks assembly
            TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");

            //Authenticate the test user.
            DomainPrincipal.Authenticate();

            //Set global context entity id to be 1 for the tests;
            //insert a new default entity
            Int32 entityId = TestDataHelper.InsertDefaultTestEntity(_dalFactory);
            //Entity defaultEntity = Entity.NewEntity();
            //defaultEntity.GFSId = 1;
            //defaultEntity.Name = "Default";
            //defaultEntity = defaultEntity.Save();

            //tell the app that we are unit testing.
            App.InitialiseForUnitTest();

            App.ViewState.EntityId = entityId;
            this.EntityId = entityId;
        }

        [TearDown]
        public virtual void TearDown()
        {
            //Remove the User DalFactory
            DalContainer.RemoveFactory(_userDalFactory.DalFactoryConfig.Name);
            _userDalFactory.Dispose();

            // remove the registered dal
            DalContainer.RemoveFactory(_dalName);

            // and restore the original dal name
            DalContainer.DalName = _originalDalName;

            //delete all temp files
            if (Directory.Exists(_tempDir))
            {
                try
                {
                    Directory.Delete(_tempDir, true);
                }
                catch { }
            }
            Directory.CreateDirectory(_tempDir);

        }

        protected Object GetPropertyTestValue1(Type propertyType)
        {
            if (propertyType == typeof(Single) || propertyType == typeof(Single?))
            {
                return 2F;
            }
            else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?))
            {
                return 2;
            }
            else if (propertyType == typeof(Byte) || propertyType == typeof(Byte?))
            {
                return (Byte)2;
            }
            else if (propertyType == typeof(Int16) || propertyType == typeof(Int16?))
            {
                return (Int16)2;
            }
            else if (propertyType == typeof(Boolean) || propertyType == typeof(Boolean?))
            {
                return true;
            }
            else if (propertyType == typeof(String))
            {
                return "TEST";
            }
            else if (propertyType.IsEnum)
            {
                return Enum.Parse(propertyType, "1");
            }

            return null;
        }

        protected Object GetPropertyTestValue2(Type propertyType)
        {
            if (propertyType == typeof(Single) || propertyType == typeof(Single?))
            {
                return 3F;
            }
            else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?))
            {
                return 3;
            }
            else if (propertyType == typeof(Byte) || propertyType == typeof(Byte?))
            {
                return (Byte)3;
            }
            else if (propertyType == typeof(Int16) || propertyType == typeof(Int16?))
            {
                return (Int16)3;
            }
            else if (propertyType == typeof(Boolean) || propertyType == typeof(Boolean?))
            {
                return false;
            }
            else if (propertyType == typeof(String))
            {
                return "TEST2";
            }
            else if (propertyType.IsEnum)
            {
                return Enum.Parse(propertyType, "2");
            }

            return null;
        }

        protected String GetFixtureDirectoryPath()
        {
            String path = Path.Combine(this.TestDir, "Fixtures");

            if (Directory.Exists(path))
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch { }
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        #endregion

        #region Event Handlers

        protected void TestModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //add to the change notification collection
            _propertyChangedNotifications.Add(e.PropertyName);
        }

        protected void TestModel_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            //add to the change notification collection
            _propertyChangingNotifications.Add(e.PropertyName);
        }

        #endregion


    }

}
