﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)

//// V8-26634 : A.Silva ~ Created.
//// V8-26782 : A.Silva ~ Moved to Common.
//// V8-26684 : A.Silva ~ Reviewed tests.
//// V8-27196 : A.Silva ~ Ammended to use the new ObjectFieldInfo based ViewModel.

//#endregion

//#endregion

//using System;
//using Galleria.Ccm.Common.Wpf.Selectors;
//using Galleria.Ccm.UnitTests.Helpers;
//using NUnit.Framework;

//namespace Galleria.Ccm.UnitTests.UICommon.Selectors
//{
//    [TestFixture]
//    public class PlanItemFieldSelectorViewModelTests : TestBase
//    {

//        #region Properties

//        [Test]
//        public void Property_AvailableObjectFieldInfos()
//        {
//            const string expected = "AvailableObjectFieldInfos";

//            var result = ObjectFieldInfoSelectorViewModel.AvailableObjectFieldInfosProperty.Path;

//            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
//        }

//        [Test]
//        public void Property_AvailableOwners()
//        {
//            const String expected = "AvailableOwners";

//            var result = ObjectFieldInfoSelectorViewModel.AvailableOwnersProperty.Path;

//            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
//            AssertHelper.AssertPropertyIsReadOnly(typeof(ObjectFieldInfoSelectorViewModel), expected);
//        }

//        [Test]
//        public void Property_SelectedObjectFieldInfo()
//        {
//            const String expected = "SelectedObjectFieldInfo";

//            var result = ObjectFieldInfoSelectorViewModel.SelectedObjectFieldInfoProperty.Path;

//            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
//            AssertHelper.AssertPropertyIsReadOnly(typeof(ObjectFieldInfoSelectorViewModel), expected);
//        }

//        [Test]
//        public void Property_SelectedOwner()
//        {
//            const String expected = "SelectedOwner";

//            var result = ObjectFieldInfoSelectorViewModel.SelectedOwnerProperty.Path;

//            Assert.AreEqual(expected, result, String.Format("The property path was incorrectly set for {0}.", expected));
//        }

//        #endregion
//    }
//}
