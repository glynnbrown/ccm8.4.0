﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-32972 : A.Silva
//  Created.
//  Added SelectUsingClipboardCommandAddsPastedDataToSelection, SelectUsingClipboardCommandPromptsUserIfAnyGtinCouldNotBePasted,
//      SelectUsingClipboardCommandPastesMissingGtinsBackToClipboardWhenUserChosesTo, SelectUsingClipboardCommandRespectsExistingSelection.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using FluentAssertions;
using Galleria.Ccm.Common.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.UnitTests.UI;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using Moq;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UICommon.Selectors
{
    [TestFixture]
    public class ProductSelectorViewModelTests : TestBase<ProductSelectorViewModelTests>
    {
        /// <summary>
        ///     Given the user has one or more GTINs in the clipboard,
        ///     when pasting selections from the clipboard,
        ///     then the pasted data is <c>added</c> to the selection.
        /// </summary>
        /// <remarks>
        ///     Uses the system Clipboard so it needs to be run in the STAThread.
        /// </remarks>
        [Test, STAThread]
        public void SelectUsingClipboardCommandAddsPastedDataToSelection()
        {
            List<String> productGtins =
                TestDataHelper.InsertProductDtos(DalFactory, 10, EntityId).Select(dto => dto.Gtin).ToList();
            IEnumerable<String> existingSelection = productGtins.Take(5).ToList();
            IEnumerable<String> clipboardSelection = productGtins.Skip(5).ToList();
            Clipboard.Clear();
            Clipboard.SetText(String.Join("\r\n", clipboardSelection));
            var viewModel = new ProductSelectorViewModel();
            viewModel.AssignedProducts.AddRange(ProductList.FetchByEntityIdProductGtins(EntityId, existingSelection));
            RelayCommand sut = viewModel.SelectUsingClipboardCommand;

            sut.Execute();

            viewModel.AssignedProducts.Select(product => product.Gtin)
                     .Should().Contain(clipboardSelection,
                                       because: "the gtins in the clipboard should have been added to the selection");
        }

        /// <summary>
        ///     Given the user has one or more GTINs in the clipboard and some GTINs cannot be selected,
        ///     when pasting selections from the clipboard,
        ///     then the user is prompted to save the missing GTINs back to the clipboard.
        /// </summary>
        /// <remarks>
        ///     Uses the system Clipboard so it needs to be run in the STAThread.
        /// </remarks>
        [Test, STAThread]
        public void SelectUsingClipboardCommandPromptsUserIfAnyGtinCouldNotBePasted()
        {
            IEnumerable<String> productGtins =
                TestDataHelper.InsertProductDtos(DalFactory, 10, EntityId).Select(dto => dto.Gtin).ToList();
            IEnumerable<String> existingSelection = productGtins.Take(5).ToList();
            IEnumerable<String> clipboardSelection = productGtins.Skip(5).Union(new List<String> {"missing"}).ToList();
            Clipboard.Clear();
            Clipboard.SetText(String.Join("\r\n", clipboardSelection));
            var viewModel = new ProductSelectorViewModel();
            viewModel.AssignedProducts.AddRange(ProductList.FetchByEntityIdProductGtins(EntityId, existingSelection));
            Mock<IWindowService> mock = MockServiceHelper.CreateMock<IWindowService>();
            mock.Setup(service => service.ShowMessage(MessageWindowType.Question,
                                                      Message.ProductSelectorViewModel_ProductsNotFound_Header,
                                                      String.Format(Message.ProductSelectorViewModel_ProductsNotFound_Description, 1),
                                                      2,
                                                      Message.Generic_Ok,
                                                      Message.Generic_CopyToClipboard,
                                                      null,
                                                      ModalMessageButton.Button1,
                                                      ModalMessageButton.Button1));
            RelayCommand sut = viewModel.SelectUsingClipboardCommand;

            sut.Execute();

            mock.VerifyAll();
        }

        /// <summary>
        ///     Given the user has one or more GTINs in the clipboard and some GTINs cannot be selected,
        ///     when pasting selections from the clipboard,
        ///     then the user is prompted to save the missing GTINs back to the clipboard.
        /// </summary>
        /// <remarks>
        ///     Uses the system Clipboard so it needs to be run in the STAThread.
        /// </remarks>
        [Test, STAThread]
        public void SelectUsingClipboardCommandPastesMissingGtinsBackToClipboardWhenUserChosesTo()
        {
            IEnumerable<String> productGtins =
                TestDataHelper.InsertProductDtos(DalFactory, 10, EntityId).Select(dto => dto.Gtin).ToList();
            IEnumerable<String> existingSelection = productGtins.Take(5).ToList();
            IEnumerable<String> clipboardSelection = productGtins.Skip(5).Union(new List<String> { "missing" }).ToList();
            Clipboard.Clear();
            Clipboard.SetText(String.Join("\r\n", clipboardSelection));
            var viewModel = new ProductSelectorViewModel();
            viewModel.AssignedProducts.AddRange(ProductList.FetchByEntityIdProductGtins(EntityId, existingSelection));
            Mock<IWindowService> mock = MockServiceHelper.CreateMock<IWindowService>();
            mock.Setup(service => service.ShowMessage(It.IsAny<MessageWindowType>(),
                                                      It.IsAny<String>(),
                                                      It.IsAny<String>(),
                                                      It.IsAny<Int32>(),
                                                      It.IsAny<String>(),
                                                      It.IsAny<String>(),
                                                      It.IsAny<String>(),
                                                      It.IsAny<ModalMessageButton>(),
                                                      It.IsAny<ModalMessageButton>()))
                .Returns(ModalMessageResult.Button2);
            RelayCommand sut = viewModel.SelectUsingClipboardCommand;

            sut.Execute();

            Clipboard.GetText(TextDataFormat.Text).Should()
                     .Be("missing\r\n",
                         because: "the clipboard should contain the gtins that could not be added to the selection");
        }

        /// <summary>
        ///     Given the user has one or more GTINs in the clipboard and some GTINs selected already,
        ///     when pasting selections from the clipboard,
        ///     then the pasted data does not <c>remove</c> the existing selection.
        /// </summary>
        /// <remarks>
        ///     Uses the system Clipboard so it needs to be run in the STAThread.
        /// </remarks>
        [Test, STAThread]
        public void SelectUsingClipboardCommandRespectsExistingSelection()
        {
            IEnumerable<String> productGtins = 
                TestDataHelper.InsertProductDtos(DalFactory, 10, EntityId).Select(dto => dto.Gtin).ToList();
            IEnumerable<String> existingSelection = productGtins.Take(5).ToList();
            IEnumerable<String> clipboardSelection = productGtins.Skip(5).ToList();
            Clipboard.Clear();
            Clipboard.SetText(String.Join("\r\n", clipboardSelection));
            var viewModel = new ProductSelectorViewModel();
            viewModel.AssignedProducts.AddRange(ProductList.FetchByEntityIdProductGtins(EntityId, existingSelection));
            RelayCommand sut = viewModel.SelectUsingClipboardCommand;

            sut.Execute();

            viewModel.AssignedProducts.Select(product => product.Gtin)
                     .Should().Contain(existingSelection,
                                       because: "the gtins from the existing selection should remain selected");
        }
    }
}