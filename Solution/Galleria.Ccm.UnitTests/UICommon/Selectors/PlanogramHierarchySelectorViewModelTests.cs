﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25460 : L.Hodson
//  Created
// V8-25871 : A.Kuszyk
//  Added method tests.
// V8-26284 : A.Kuszyk
//  Created Common test.
// V8-28337 : A.Silva
//      Added tests for ProgressStatus.
// V8-28498 : A.Silva
//      Reviewed failing tests.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UICommon.Selectors
{
    [TestFixture]
    public sealed class PlanogramHierarchySelectorViewModelTests : TestBase
    {
        #region TestFixture Helpers

        private PlanogramHierarchyViewModel _hierarchyView;
        private UserPlanogramGroupListViewModel _userGroupsView;
        private PlanogramHierarchySelectorViewModel _groupSelectorViewmodel;

        private void InsertTestData()
        {
            //Create a planogram hierarchy
            PlanogramHierarchy planHierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
            planHierarchy.RootGroup.ChildList.Clear();

            PlanogramGroup g1 = PlanogramGroup.NewPlanogramGroup();
            g1.Name = "Canned Foods";
            planHierarchy.RootGroup.ChildList.Add(g1);

            PlanogramGroup g2 = PlanogramGroup.NewPlanogramGroup();
            g2.Name = "Tinned Veg";
            g1.ChildList.Add(g2);

            PlanogramGroup g3 = PlanogramGroup.NewPlanogramGroup();
            g3.Name = "Tinned Fruit";
            g1.ChildList.Add(g3);

            PlanogramGroup g4 = PlanogramGroup.NewPlanogramGroup();
            g4.Name = "Condiments";
            planHierarchy.RootGroup.ChildList.Add(g4);

            PlanogramGroup g5 = PlanogramGroup.NewPlanogramGroup();
            g5.Name = "Sweets";
            g4.ChildList.Add(g5);

            PlanogramGroup g6 = PlanogramGroup.NewPlanogramGroup();
            g6.Name = "Chocolate";
            g4.ChildList.Add(g6);

            PlanogramGroup g7 = PlanogramGroup.NewPlanogramGroup();
            g7.Name = "Dressings";
            planHierarchy.RootGroup.ChildList.Add(g7);

            planHierarchy = planHierarchy.Save();
        }

        public override void Setup()
        {
            base.Setup();
            InsertTestData();

            _hierarchyView = new PlanogramHierarchyViewModel();
            _hierarchyView.FetchForCurrentEntity();

            _userGroupsView = new UserPlanogramGroupListViewModel();
            _userGroupsView.FetchForCurrentUserAndEntity();

            _groupSelectorViewmodel = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);

            _groupSelectorViewmodel.ShowFavouriteGroups = true;
        }

        private Boolean ModelListContainsItemMeetingCriteria<T>(Collection<T> modelList, Func<T, Boolean> criteria)
        {
            var enumerator = modelList.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (criteria(enumerator.Current)) return true;
            }
            return false;
        }

        #endregion

        #region Properties

        [Test]
        public void Property_GroupSearchFilter_SetChangesAvailableGroups()
        {
            var view = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);

            //check initial values
            Assert.IsNullOrEmpty(view.GroupSearchFilter, "Should start empty");
            Assert.AreEqual(2, view.AvailableGroups.Count, "Should show all 2 root groups");

            //set value
            view.GroupSearchFilter = "Tinned Veg";

            //check output
            Assert.AreEqual(1, view.AvailableGroups.Count);

            //remove value
            view.GroupSearchFilter = null;
            Assert.AreEqual(2, view.AvailableGroups.Count, "Should show all 2 root groups");
        }


        [Test]
        public void Property_FavouriteGroups_Initialised()
        {
            throw new InconclusiveException("TODO");
        }

        [Test]
        public void Property_FavouriteGroups_UpdatesOnRepositoryChange()
        {
            throw new InconclusiveException("TODO");
        }

        [Test]
        public void Property_FavouriteGroups_UpdatesOnGroupAdded()
        {
            throw new InconclusiveException("TODO");
        }

        [Test]
        public void Property_FavouriteGroups_UpdatesOnGroupRemoved()
        {
            throw new InconclusiveException("TODO");
        }

        #region ProcessStatus Property Tests

        [Test]
        public void Property_ProgressStatus()
        {
            const String expectedBindingPropertyPath = "ProgressStatus";
            const String bindingPropertyPathFail = "The property's binding path was not the expected one.";

            Assert.AreEqual(expectedBindingPropertyPath, PlanogramHierarchySelectorViewModel.ProgressStatusProperty.Path, bindingPropertyPathFail);
        }

        [Test]
        public void ProgressStatus_WhenCurrentPropertyChanged_ShouldNotifyProgressStatusChange()
        {
            IProgressStatus progressStatus = new ProgressStatus();
            PlanogramHierarchySelectorViewModel testModel =  new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView)
            {
                ProgressStatus = progressStatus
            };
            PropertyChangedNotifications.Clear();
            testModel.PropertyChanged += TestModel_PropertyChanged;

            progressStatus.Current++;

            testModel.PropertyChanged -= TestModel_PropertyChanged;
            Assert.Contains(PlanogramHierarchySelectorViewModel.ProgressStatusProperty.Path, PropertyChangedNotifications);
        }

        [Test]
        public void ProgressStatus_WhenMaxPropertyChanged_ShouldNotifyProgressStatusChange()
        {
            IProgressStatus progressStatus = new ProgressStatus();
            PlanogramHierarchySelectorViewModel testModel =  new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView)
            {
                ProgressStatus = progressStatus
            };
            PropertyChangedNotifications.Clear();
            testModel.PropertyChanged += TestModel_PropertyChanged;

            progressStatus.Max++;

            testModel.PropertyChanged -= TestModel_PropertyChanged;
            Assert.Contains(PlanogramHierarchySelectorViewModel.ProgressStatusProperty.Path, PropertyChangedNotifications);
        }

        [Test]
        public void ProgressStatus_WhenNamePropertyChanged_ShouldNotifyProgressStatusChange()
        {
            IProgressStatus progressStatus = new ProgressStatus();
            PlanogramHierarchySelectorViewModel testModel =  new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView)
            {
                ProgressStatus = progressStatus
            };
            PropertyChangedNotifications.Clear();
            testModel.PropertyChanged += TestModel_PropertyChanged;

            progressStatus.Name += "Changed";

            testModel.PropertyChanged -= TestModel_PropertyChanged;
            Assert.Contains(PlanogramHierarchySelectorViewModel.ProgressStatusProperty.Path, PropertyChangedNotifications);
        }

        [Test]
        public void ProgressStatus_WhenDescriptionPropertyChanged_ShouldNotifyProgressStatusChange()
        {
            IProgressStatus progressStatus = new ProgressStatus();
            PlanogramHierarchySelectorViewModel testModel =  new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView)
            {
                ProgressStatus = progressStatus
            };
            PropertyChangedNotifications.Clear();
            testModel.PropertyChanged += TestModel_PropertyChanged;

            progressStatus.Description += "Changed";

            testModel.PropertyChanged -= TestModel_PropertyChanged;
            Assert.Contains(PlanogramHierarchySelectorViewModel.ProgressStatusProperty.Path, PropertyChangedNotifications);
        }

        #endregion

        #endregion

        #region Commands

        #region AddGroupToFavourites

        [Test]
        public void Command_AddGroupToFavourites_DisabledWhenNoSelectedGroup()
        {
            var viewModel = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);

            //set selected group to null
            viewModel.SelectedPlanogramGroup = null;

            Assert.IsFalse(viewModel.AddGroupToFavouritesCommand.CanExecute());
            Assert.IsNotNull(viewModel.AddGroupToFavouritesCommand.DisabledReason);

            //reselect
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

            Assert.IsTrue(viewModel.AddGroupToFavouritesCommand.CanExecute());
        }

        [Test]
        public void Command_AddGroupToFavourites_DisabledWhenGroupIsFavourite()
        {
            var viewModel = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);

            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            Assert.IsTrue(viewModel.AddGroupToFavouritesCommand.CanExecute());

            //add
            viewModel.AddGroupToFavouritesCommand.Execute();

            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            Assert.IsFalse(viewModel.AddGroupToFavouritesCommand.CanExecute());
            Assert.IsNotNull(viewModel.AddGroupToFavouritesCommand.DisabledReason);
        }

        [Test]
        public void Command_AddGroupToFavourites_Execution()
        {
            var viewModel = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);

            var userGroups = new UserPlanogramGroupListViewModel();
            userGroups.FetchForCurrentUserAndEntity();
            Assert.AreEqual(0, userGroups.Model.Count);

            //add a group to favourites
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            viewModel.AddGroupToFavouritesCommand.Execute();

            userGroups.FetchForCurrentUserAndEntity();
            Assert.AreEqual(1, userGroups.Model.Count);
        }

        #endregion

        #region RemoveGroupFromFavourites

        [Test]
        public void Command_RemoveGroupFromFavourites_DisabledWhenNoSelectedGroup()
        {
            var viewModel = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);

            //add a group to favourites
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            viewModel.AddGroupToFavouritesCommand.Execute();

            //set selected group to null
            viewModel.SelectedGroup = null;

            Assert.IsFalse(viewModel.RemoveGroupFromFavouritesCommand.CanExecute());
            Assert.IsNotNull(viewModel.RemoveGroupFromFavouritesCommand.DisabledReason);

            //reselect fav
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

            Assert.IsTrue(viewModel.RemoveGroupFromFavouritesCommand.CanExecute());

        }

        [Test]
        public void Command_RemoveGroupFromFavourites_DisabledWhenSelectedGroupNotFavourite()
        {
            var viewModel = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

            Assert.IsFalse(viewModel.RemoveGroupFromFavouritesCommand.CanExecute());
            Assert.IsNotNull(viewModel.RemoveGroupFromFavouritesCommand.DisabledReason);

            //add a group to favourites
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            viewModel.AddGroupToFavouritesCommand.Execute();

            Assert.IsTrue(viewModel.RemoveGroupFromFavouritesCommand.CanExecute());
        }

        [Test]
        public void Command_RemoveGroupFromFavourites_Execution()
        {
            var viewModel = new PlanogramHierarchySelectorViewModel(_hierarchyView, _userGroupsView);

            //add a group to favourites
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            viewModel.AddGroupToFavouritesCommand.Execute();

            var userGroups = new UserPlanogramGroupListViewModel();
            userGroups.FetchForCurrentUserAndEntity();

            Assert.AreEqual(1, userGroups.Model.Count);


            //remove it
            viewModel.SelectedPlanogramGroup = viewModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
            viewModel.RemoveGroupFromFavouritesCommand.Execute();

            userGroups.FetchForCurrentUserAndEntity();
            Assert.AreEqual(0, userGroups.Model.Count);
        }

        #endregion

        #endregion

    }
}
