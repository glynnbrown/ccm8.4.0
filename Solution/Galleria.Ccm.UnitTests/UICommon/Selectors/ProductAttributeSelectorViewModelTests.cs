﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-25460 : L.Hodson
//  Created
#endregion

#region Version History : CCM830

// V8-32560 : A.Silva
//  Removed tests related to AddAll, RemoveAll commands.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.UICommon.Selectors
{
    [TestFixture]
    public class ProductAttributeSelectorViewModelTests
    {
        #region Constructor

        [Test]
        public void Constructor_WhenCurrentSelectionNull_SelectedAttributesIsEmpty()
        {
            ProductAttributeSelectorViewModel viewModel = new ProductAttributeSelectorViewModel(false, false);

            CollectionAssert.IsEmpty(viewModel.SelectedAttributes);
        }

        [Test]
        public void Constructor_WhenCurrentSelectionNotNull_SelectedAttributesContainsAttributes()
        {
            IEnumerable<Tuple<ProductAttributeSourceType, String>> currentSelection = PlanogramProduct.
                EnumerateDisplayableFieldInfos(false, false, false).
                Select(f => new Tuple<ProductAttributeSourceType, String>(ProductAttributeSourceType.ProductAttributes, f.PropertyName)).ToList();

            ProductAttributeSelectorViewModel viewModel = new ProductAttributeSelectorViewModel(false, false, currentSelection);

            CollectionAssert.IsEmpty(viewModel.AvailableAttributes);
            Assert.AreEqual(currentSelection.Count(), viewModel.SelectedAttributes.Count);
        }

        [Test]
        public void Constructor_WhenIncludePropertiesSpecified_AvailableAttributesAreFiltered()
        {
            ICollection<Tuple<ProductAttributeSourceType, String>> includeProperties = PlanogramProduct.
                EnumerateDisplayableFieldInfos(false, false, false).
                Take(10).
                Select(f => new Tuple<ProductAttributeSourceType, String>(ProductAttributeSourceType.ProductAttributes, f.PropertyName)).ToList();

            ProductAttributeSelectorViewModel viewModel = new ProductAttributeSelectorViewModel(false, false, null,includeProperties);

            var expected = includeProperties.Select(p => p.Item2);
            var actual = viewModel.AvailableAttributes.Select(a => a.PropertyName);
            CollectionAssert.AreEquivalent(expected, actual);
        }

        #endregion

        #region AddSelectedAttributesCommand
        [Test]
        public void AddSelectedAttributesCommand_AddsToSelected()
        {
            var viewModel = new ProductAttributeSelectorViewModel(false, false);
            ProductAttributeSelectorRow selectedAttribute = viewModel.AvailableAttributes.First();
            viewModel.AvailableAttributesSelection.Add(selectedAttribute);

            viewModel.AddSelectedAttributesCommand.Execute();

            CollectionAssert.Contains(viewModel.SelectedAttributes, selectedAttribute);
        }

        [Test]
        public void AddSelectedAttributesCommand_RemovesFromAvailable()
        {
            var viewModel = new ProductAttributeSelectorViewModel(false, false);
            ProductAttributeSelectorRow selectedAttribute = viewModel.AvailableAttributes.First();
            viewModel.AvailableAttributesSelection.Add(selectedAttribute);

            viewModel.AddSelectedAttributesCommand.Execute();

            CollectionAssert.DoesNotContain(viewModel.AvailableAttributes, selectedAttribute);
        } 
        #endregion

        #region RemoveSelectedAttributesCommand
        [Test]
        public void RemoveSelectedAttributesCommand_AddsToAvailable()
        {
            var viewModel = new ProductAttributeSelectorViewModel(false, false);
            viewModel.SelectedAttributes.AddRange(viewModel.AvailableAttributes);
            viewModel.AvailableAttributes.Clear();
            ProductAttributeSelectorRow selectedAttribute = viewModel.SelectedAttributes.First();
            viewModel.SelectedAttributesSelection.Add(selectedAttribute);

            viewModel.RemoveSelectedAttributesCommand.Execute();

            CollectionAssert.Contains(viewModel.AvailableAttributes, selectedAttribute);
        }

        [Test]
        public void RemoveSelectedAttributesCommand_RemovesFromSelected()
        {
            var viewModel = new ProductAttributeSelectorViewModel(false, false);
            viewModel.SelectedAttributes.AddRange(viewModel.AvailableAttributes);
            viewModel.AvailableAttributes.Clear();
            ProductAttributeSelectorRow selectedAttribute = viewModel.SelectedAttributes.First();
            viewModel.SelectedAttributesSelection.Add(selectedAttribute);

            viewModel.RemoveSelectedAttributesCommand.Execute();

            CollectionAssert.DoesNotContain(viewModel.SelectedAttributes, selectedAttribute);
        }
        #endregion
    }
}
