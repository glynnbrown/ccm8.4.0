﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Entity;
using Galleria.Ccm.Services.Image;
using Galleria.Ccm.Services.Performance;
using Galleria.Ccm.Services.Timeline;
using Moq;

namespace Galleria.Ccm.UnitTests.Helpers
{
    /// <summary>
    /// Provides helper methods for mocking out service calls and responses.
    /// </summary>
    public static class MockServiceHelper
    {
        #region General

        /// <summary>
        /// Creates the service mock and registers its
        /// object with the ServiceContainer.
        /// </summary>
        public static Mock<T> CreateMock<T>()
            where T : class
        {
            Mock<T> mock = new Mock<T>();
            ServiceContainer.RegisterServiceObject(mock.Object);
            return mock;
        }

        /// <summary>
        /// Creates the service mock and registers its
        /// object with the ServiceContainer.
        /// </summary>
        public static Mock<T> CreateStrictMock<T>()
            where T : class
        {
            Mock<T> mock = new Mock<T>(MockBehavior.Strict);
            ServiceContainer.RegisterServiceObject(mock.Object);
            return mock;
        }

        #endregion

        #region Entity

        /// <summary>
        /// Creates a single gfs entity called "Default"
        /// </summary>
        /// <param name="mock"></param>
        internal static void Entity_DefaultGetEntitiesResponse(Mock<EntityService> mock)
        {
            Entity_GetEntitiesResponse(mock, new String[] { "Default" });
        }

        internal static void Entity_GetEntitiesResponse(Mock<EntityService> mock, String[] entityNames)
        {
            mock
                .Setup(m => m.GetAllEntities(It.IsAny<GetAllEntitiesRequest>()))
                .Returns(new GetAllEntitiesResponse{Entities = CreateEntities(entityNames)});
        }

        private static List<Entity> CreateEntities(String[] entityNames)
        {
            List<Entity> entities = new List<Entity>();

            Int32 id = 1;
            foreach(String name in entityNames)
            {
                entities.Add(
                     new Services.Entity.Entity()
                        {
                            Id = id,
                            Name = name,
                        }
                    );
                id++;
            }

            return entities;
        }

        #endregion

        #region Performance

        internal static void Performance_GetPerformanceSourceByEntityNameResponse(Mock<PerformanceService> mock, 
            String entityName, IEnumerable<PerformanceSource> sources)
        {
            mock
                .Setup(m => m.GetPerformanceSourceByEntityName(It.Is<GetPerformanceSourceByEntityNameRequest>(p => p.EntityName == entityName)))
                .Returns(new GetPerformanceSourceByEntityNameResponse() { PerformanceSources = new List<PerformanceSource>(sources) }); 

        }

        internal static PerformanceSource NewPerformanceSource(Int32 id, String name, String timelineLevelName, String entityName,
            GFSModel.PerformanceSourceType type)
        {
            return new PerformanceSource()
            {
                Id = id,
                Name = name,
                TimelineLevelName = timelineLevelName,
                EntityName =entityName,
                Type = type.ToString(),
                PerformanceSourceMetrics = new List<PerformanceSourceMetric>(),
                DateLastProcessed = DateTime.UtcNow
            };
        }

        #endregion

        #region Timeline

        internal static void Timeline_GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeResponse(
            Mock<TimelineService> mock, String entityName, String sourceName, IEnumerable<TimelineGroup> timelineGroups )
        {
            mock
                .Setup(m => m.GetTimelineGroupByEntityNamePerformanceSourceNameDateRange(It.Is<GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeRequest>
                    (p => p.EntityName == entityName && p.PerformanceSourceName == sourceName)))
                    .Returns(new GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeResponse()
                    {
                        TimelineGroups = new List<TimelineGroup>(timelineGroups)
                    });

        }

        internal static TimelineGroup NewTimelineGroup(String name, Int64 code, String timelineLevelName)
        {
            return new TimelineGroup()
                {
                    Name = name,
                    Code = code,
                    TimelineLevelName = timelineLevelName,
                    Children = new List<TimelineGroup>()
                };
        }

        #endregion

        #region Image

        /// <summary>
        ///     Setup <see cref="ImageService.GetAllCompressions"/> to retrieve a single <see cref="Compression"/> named "Default".
        /// </summary>
        /// <param name="mock">The <see cref="Mock{ImageService}"/> to setup the response for.</param>
        internal static void Image_DefaultGetAllCompressionsResponse(Mock<ImageService> mock)
        {
            Image_GetCompressionsResponse(mock, new[] {"Default"});
        }

        /// <summary>
        ///     Setup <see cref="ImageService.GetAllCompressions"/> to retrieve a single <see cref="Compression"/> named "Default".
        /// </summary>
        /// <param name="mock">The <see cref="Mock{ImageService}"/> to setup the response for.</param>
        internal static void Image_DefaultGetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameResponse(Mock<ImageService> mock)
        {
            Image_GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameResponse(mock, new Byte[] {1,1,1,1,1});
        }

        private static void Image_GetCompressionsResponse(Mock<ImageService> mock, IEnumerable<String> compressionNames)
        {
            mock.Setup(m => m.GetAllCompressions(It.IsAny<GetAllCompressionsRequest>()))
                .Returns(new GetAllCompressionsResponse {Compressions = CreateCompressions(compressionNames)});
        }

        internal static void Image_GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameResponse(Mock<ImageService> mock, Byte[] imagesBytes)
        {
            mock.Setup(m => m.GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionName(It.IsAny<GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameRequest>()))
                .Returns<GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameRequest>(o => new GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameResponse { ProductImage = CreateProductImages(o, imagesBytes) });
        }

        private static ProductImage CreateProductImages(GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameRequest request, Byte[] imagesBytes)
        {
            return new ProductImage { EntityName = request.EntityName, FacingType = request.FacingType, ImageType = request.ImageType, GTIN = request.GTIN, ProductImageData = new Image { ImageBlob = new Blob {Data = imagesBytes}}};
        }

        private static List<Compression> CreateCompressions(IEnumerable<String> compressionNames)
        {
            return compressionNames.Select(name => new Compression {Name = name}).ToList();
        }

        #endregion
    }
}
