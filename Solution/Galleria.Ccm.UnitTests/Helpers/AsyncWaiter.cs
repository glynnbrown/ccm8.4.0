﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM 8.0)
// CCM-25452 : N.Haywood
//  Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Galleria.Ccm.UnitTests.Helpers
{
    /// <summary>
    /// Class to factilitate waiting for an event to fire or for a condition to be met.
    /// </summary>
    /// <typeparam name="TInstance"></typeparam>
    /// <typeparam name="TSource"></typeparam>
    /// <remarks>
    /// USAGE eg-
    ///        var  waiter = new AsyncWaiter<BackstageSystemAdministrationViewModelTests, ExternalLinkSetupListViewModel>
    ///            (this, this.TestModel.ExternalLinkSetupListView);
    ///      this.TestModel.ExternalLinkSetupListView.ModelChanged += waiter.OnEvent;
    ///        waiter.Run(() => { cmd.Execute(); });
    /// </remarks>
    public class AsyncWaiter<TInstance, TSource> where TInstance : class
    {
        #region Fields

        private WeakReference _weakInstance;
        private WeakReference _weakSource;

        private Action<AsyncWaiter<TInstance, TSource>, TSource> _onDetachAction;
        private Boolean _eventFired;

        private Predicate<TSource> _stateChecker;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instances of the WeakEventListener class.
        /// </summary>
        /// <param name="rootInstance">Instance subscribing to the event.</param>
        public AsyncWaiter(TInstance instance, TSource source)
        {
            if (null == instance)
            {
                throw new ArgumentNullException("instance");
            }

            if (source == null)
                throw new ArgumentNullException("source");

            _weakInstance = new WeakReference(instance);
            _weakSource = new WeakReference(source);
        }

        #endregion

        #region Properties

        public Predicate<TSource> StateChecker
        {
            get { return _stateChecker; }
            set { _stateChecker = value; }
        }

        /// <summary>
        /// Gets or sets the method to call when detaching from the event.
        /// </summary>
        public Action<AsyncWaiter<TInstance, TSource>, TSource> OnDetachAction
        {
            get { return _onDetachAction; }
            set
            {

                // CHANGED: NEVER REMOVE THIS CHECK. IT CAN CAUSE A MEMORY LEAK.
                if (value != null && !value.Method.IsStatic)
                    throw new ArgumentException("OnDetachAction method must be static otherwise " +
                               "the event WeakEventListner cannot guarantee to unregister the handler.");

                _onDetachAction = value;
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Runs the async action and waits for it to be handled
        /// </summary>
        /// <param name="asyncAction">the action to perform</param>
        public void Run(Action asyncAction)
        {
            Run(asyncAction, 10);
        }

        /// <summary>
        /// Runs the async action and waits for it to be handled
        /// </summary>
        /// <param name="asyncAction">the action to perform</param>
        /// <param name="maxTries">the number of tries to allow</param>
        public void Run(Action asyncAction, int maxTries)
        {
            //reset the flag
            _eventFired = false;

            //call the action
            asyncAction.Invoke();

            //wait for the handler
            int tryCount = 0;

            if (_stateChecker != null)
            {
                TSource source = (TSource)_weakSource.Target;

                //we are checking for a condition to return true.
                while (!_stateChecker(source))
                {
                    if (tryCount <= maxTries)
                    {
                        Thread.Sleep(500);
                        tryCount++;
                    }
                    else
                    {
                        Detach();
                        throw new Exception("Condition not met");
                    }
                }

            }
            else
            {
                //we are waiting for an event
                while (!_eventFired)
                {
                    if (tryCount <= maxTries)
                    {
                        Thread.Sleep(500);
                        tryCount++;
                    }
                    else
                    {
                        Detach();
                        throw new Exception("Save event not fired");
                    }
                }
            }
            Detach();
        }


        /// <summary>
        /// Handler for the subscribed event calls OnEventAction to handle it.
        /// </summary>
        /// <param name="source">Event source.</param>
        /// <param name="eventArgs">Event arguments.</param>
        public void OnEvent(object source, EventArgs eventArgs)
        {
            TInstance target = (TInstance)_weakInstance.Target;
            if (null != target)
            {
                _eventFired = true;
            }
            else
            {
                Detach();
            }
        }

        /// <summary>
        /// Detaches from the subscribed event.
        /// </summary>
        public void Detach()
        {
            _stateChecker = null;

            TSource source = (TSource)_weakSource.Target;
            if (null != OnDetachAction && source != null)
            {
                // CHANGED: Passing the source instance also, because of static event handlers
                OnDetachAction(this, source);
                OnDetachAction = null;
            }
        }

        #endregion



    }
}
