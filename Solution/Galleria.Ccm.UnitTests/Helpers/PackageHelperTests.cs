﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.GFSModel;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Imports.Mappings;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Helpers
{
    [TestFixture]
    public class PackageHelperTests: TestBase
    {
        [Test]
        public void ShouldMarkForRepublishWhenOverridingExistingPlan()
        {
            // Overwrite a package >> Check Plan assignment for all locations for the plan have had publish status flagged as being modifed after initial publish.

     

            InsertEntity();
            List<Int32> planIds = CreateTwoPlansAndSaveThem();
            GetInfosAndCallOverwrite(planIds);
        }
        
        private static List<Int32> CreateTwoPlansAndSaveThem()
        {
            var packageA = "Package".CreatePackage();
            packageA.Name = "Package A";
            packageA.AddPlanogram();

            var packageB = "Package".CreatePackage();
            packageB.Name = "Package B";
            packageB.AddPlanogram();

            packageA = packageA.Save();
            packageB = packageB.Save();

            List<Int32> ids = new List<Int32>();
            ids.Add(Convert.ToInt32(packageA.Planograms.First().Id));
            ids.Add(Convert.ToInt32(packageB.Planograms.First().Id));

            return ids;
        }

        private void GetInfosAndCallOverwrite(List<Int32> planIds)
        {
            var infos = PlanogramInfoList.FetchByIds(planIds);

            CreateLocationAndPlanAssignent(infos.First().Id);

            PackageHelper.OverwritePackage(infos.First(), infos.Last().Id, infos.Last().Name, 1, infos, 1);

            var planogramId = infos.Last().Id + 1; //Not very accurate way of obtaining Id but will always work with mock dal and existing methods.

            var assignment = LocationPlanAssignmentList.FetchByPlanogramId(planogramId);

            Boolean changedToExpected = assignment.First().PublishStatus == PlanAssignmentPublishStatusType.PlanogramAltered;

            Assert.IsTrue(changedToExpected);
        }

        private void InsertEntity()
        {
            TestDataHelper.InsertEntityDtos(DalFactory, 1);
        }

        private void CreateLocationAndPlanAssignent(Int32 planId)
        {
            var locations = TestDataHelper.InsertLocationDtos(DalFactory, 1, 1);
            var users = TestDataHelper.InsertUserDtos(DalFactory, 1);

            var assignments = TestDataHelper.InsertLocationPlanAssignment(1);
            assignments.First().PlanogramId = planId;
            assignments.First().PublishStatus = PlanAssignmentPublishStatusType.Successful;
            assignments.Save();
        }
    }
}
