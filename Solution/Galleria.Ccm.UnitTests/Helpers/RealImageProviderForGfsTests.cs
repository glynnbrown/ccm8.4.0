﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-28964 : A.Silva
//      Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.GFSModel;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Services.Entity;
using Galleria.Ccm.Services.Image;
using Galleria.Ccm.UnitTests.Imports.Mappings;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Moq;
using NUnit.Framework;
using Entity = Galleria.Ccm.Model.Entity;

namespace Galleria.Ccm.UnitTests.Helpers
{
    [TestFixture]
    public class RealImageProviderForGfsTests : TestBase
    {
        #region Fields

        private Entity _entity;
        private static Mock<ImageService> _imageMock;
        private static Mock<EntityService> _entityMock;

        private readonly IEnumerable<PlanogramImageFacingType> _facingTypes = new List<PlanogramImageFacingType>
        {
            PlanogramImageFacingType.Front,
            PlanogramImageFacingType.Left,
            PlanogramImageFacingType.Top,
            PlanogramImageFacingType.Back,
            PlanogramImageFacingType.Right,
            PlanogramImageFacingType.Bottom
        };

        private readonly IEnumerable<PlanogramImageType> _imageTypes = new List<PlanogramImageType>
        {
            PlanogramImageType.None,
            PlanogramImageType.Display,
            PlanogramImageType.Tray,
            //PlanogramImageType.Alternate, Not in GFS
            //PlanogramImageType.Case, Not in GFS
            PlanogramImageType.PointOfPurchase
        };

        #endregion

        #region Test Helper Methods

        private static IRealImageProviderSettings MockRealImageProviderSettingsForGfs()
        {
            Mock<IRealImageProviderSettings> mock = new Mock<IRealImageProviderSettings>();
            mock.Setup(p => p.ProductImageSource).Returns(RealImageProviderType.Gfs);
            return mock.Object;
        }

        private static PlanogramProduct NewTestImageTarget(String gtin)
        {
            PlanogramProduct imageTarget = PlanogramProduct.NewPlanogramProduct();
            imageTarget.Gtin = gtin;
            return imageTarget;
        }

        private void SetupCorrectEntity()
        {
            _entity = Entity.FetchById(TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id);
            _entity.GFSId = 1; //gfsEntity.Id;
            GFSModel.Entity gfsEntity = GFSModel.Entity.FetchByCcmEntity(_entity);
            _entity.SystemSettings.CompressionName = CompressionList.FetchAll(gfsEntity).First().Name;
            _entity.Save();
        }

        private static void ConfigureGfsMock()
        {
            _entityMock = MockServiceHelper.CreateMock<EntityService>();
            MockServiceHelper.Entity_DefaultGetEntitiesResponse(_entityMock);
            _imageMock = MockServiceHelper.CreateMock<ImageService>();
            MockServiceHelper.Image_DefaultGetAllCompressionsResponse(_imageMock);
        }

        private static void ConfigureImageMock(Mock<ImageService> imageMock, Byte[] imagesBytes)
        {
            MockServiceHelper.Image_GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameResponse(
                imageMock, imagesBytes);
        }

        #endregion

        [Test]
        public void GetImage_ShouldRetrieveImage()
        {
            Byte[] expected = { 1, 1, 1, 1, 1 };
            ConfigureGfsMock();
            ConfigureImageMock(_imageMock, expected);
            SetupCorrectEntity();
            RealImageProviderForGfs instance = new RealImageProviderForGfs(_entity.Id, MockRealImageProviderSettingsForGfs());
            const String expectedGtin = "ProductGtin";

            PlanogramImage planogramImage = instance.GetImage(NewTestImageTarget(expectedGtin), PlanogramImageFacingType.Front, PlanogramImageType.None);

            Assert.AreEqual(expected, planogramImage.ImageData);
            _imageMock.Verify(m => m.GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionName(It.Is<GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameRequest>(request => request.GTIN == expectedGtin)));
        }

        [Test, TestCaseSource("_facingTypes")]
        public void GetImage_ShouldRequestCorrectFacingType(PlanogramImageFacingType facingType)
        {
            Byte[] expected = { 1, 1, 1, 1, 1 };
            ConfigureGfsMock();
            ConfigureImageMock(_imageMock, expected);
            SetupCorrectEntity();
            RealImageProviderForGfs instance = new RealImageProviderForGfs(_entity.Id, MockRealImageProviderSettingsForGfs());

            String facingTypeName = facingType.ToString();
            instance.GetImage(NewTestImageTarget("ProductGtin"), facingType, PlanogramImageType.None);

            _imageMock.Verify(m => m.GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionName(It.Is<GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameRequest>(request => request.FacingType == facingTypeName)));
        }

        [Test, TestCaseSource("_imageTypes")]
        public void GetImage_ShouldRequestCorrectImageType(PlanogramImageType imageType)
        {
            Byte[] expected = { 1, 1, 1, 1, 1 };
            ConfigureGfsMock();
            ConfigureImageMock(_imageMock, expected);
            SetupCorrectEntity();
            RealImageProviderForGfs instance = new RealImageProviderForGfs(_entity.Id, MockRealImageProviderSettingsForGfs());

            String imageTypeName = imageType == PlanogramImageType.None
                                       ? "Unit" // The None type will request a Unit type in GFS.
                                       : imageType.ToString();

            instance.GetImage(NewTestImageTarget("ProductGtin"), PlanogramImageFacingType.Front, imageType);

            _imageMock.Verify(m => m.GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionName(It.Is<GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameRequest>(request => request.ImageType == imageTypeName)));
        }
    }
}