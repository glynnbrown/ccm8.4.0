﻿//#region Header Information

//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM v8.0)

////	V8-28337 : Alvaro Silva
////		Created

//#endregion

//#endregion

//using System.Windows;
//using Galleria.Ccm.Common.Wpf.Helpers;
//using NUnit.Framework;

//namespace Galleria.Ccm.UnitTests.Helpers
//{
//    [TestFixture]
//    public class WindowHelperTests
//    {
//        #region GetWindowParent

//        [Test]
//        public void GetWindowParent_WhenDependencyWithoutParentWindow_ShouldReturnNull()
//        {
//            DependencyObject test = new DependencyObject();

//            Window actual = test.GetParentWindow();

//            Assert.IsNull(actual);
//        }

//        [Test]
//        public void GetWindowParent_WhenDependencyNull_ShouldReturnNull()
//        {
//            DependencyObject test = null;

//            Window actual = test.GetParentWindow();

//            Assert.IsNull(actual);
//        }

//        [Test]
//        public void GetWindowParent_WhenDependencyHasParentWindow_ShouldReturnParentWindow()
//        {
//            // Can't test this.
//        }

//        #endregion
//    }
//}