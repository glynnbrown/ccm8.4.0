﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (SA 1.0)
// SA-13714 : A.Probyn
//  Created (Copied from GFS)
// SA-14783 : L.Hodson
//  Added AssertChildModelObjectsAreNotNull and AssertModelObjectsAreEqual
// V8-26322 : A.Silva
//      Refactored AssertModelObjectsAreEqual to account for the case when the second object 
//          is missing properties that exist in the first. Any property to be ignored needs to be explicitly indicated.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Collections.Specialized;
using System.Collections;
using System.Reflection;
using Csla.Core;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Galleria.Framework.Helpers;
using System.Diagnostics;

namespace Galleria.Ccm.UnitTests.Helpers
{
    /// <summary>
    /// Provides helpers for repeatedly tested assertions
    /// </summary>
    public static class AssertHelper
    {
        [DebuggerStepThrough]
        public static void AreSinglesEqual(Single expected, Single actual)
        {
            if (!expected.EqualTo(actual))
            {
                Assert.Fail("Expected {0}, but was {1}", expected, actual);
            }
        }

        [DebuggerStepThrough]
        public static void AreSinglesEqual(Single expected, Single actual, String failMessage)
        {
            if (!expected.EqualTo(actual))
            {
                Assert.Fail("Expected {0}, but was {1} - {2} ",expected, actual, failMessage);
            }
        }

        /// <summary>
        /// Asserts that two objects that implement the interface T are equal by iterating over the
        /// interfaces properties only. Any properties that are IEnumerable just have their counts compared.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="excludedProperties"></param>
        public static void AreEqual<T>(T expected, T actual, params String[] excludedProperties)
        {
            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                if (excludedProperties.Contains(property.Name)) continue;

                var expectedValue = property.GetValue(expected, null);
                var actualValue = property.GetValue(actual, null);
                if (expectedValue == null && actualValue == null) continue;
                if (expectedValue == null || actualValue == null)
                {
                    throw new AssertionException(String.Format(
                            "Expected {0}, but was {1}, for property {2}",
                            expectedValue,
                            actualValue,
                            property.Name));
                }

                if (expectedValue is IEnumerable)
                {
                    var expectedValueList = ((IEnumerable)expectedValue).GetEnumerator();
                    var actualValueList = ((IEnumerable)actualValue).GetEnumerator();
                    Int32 expectedCount = 0;
                    while (expectedValueList.MoveNext()) { expectedCount++; }
                    Int32 actualCount = 0;
                    while (actualValueList.MoveNext()) { actualCount++; }
                    Assert.AreEqual(expectedCount, actualCount);
                    continue;
                }
                else if (expectedValue.GetType().Namespace.StartsWith("System"))
                {
                    if (!expectedValue.Equals(actualValue))
                    {
                        throw new AssertionException(String.Format(
                            "Expected {0}, but was {1}, for property {2}",
                            expectedValue,
                            actualValue,
                            property.Name));
                    }
                }
                else
                {
                    AreEqual(expectedValue, actualValue);
                }
            }
        }

        /// <summary>
        /// Asserts that the given collections of singles are equivilant, comparing equality to the
        /// given number of decimal places.
        /// </summary>
        /// <param name="expected">The expected collection.</param>
        /// <param name="actual">The actual collection.</param>
        /// <param name="decimalPlaces">The number of decimal places to use for comparisons.</param>
        public static void AreSinglesEquivalent(IEnumerable<Single> expected, IEnumerable<Single> actual, Int32 decimalPlaces)
        {
            Assert.AreEqual(expected.Count(), actual.Count());
            var roundedActuals = actual.Select(a => Convert.ToSingle(Math.Round(a, decimalPlaces))).ToList();
            foreach (var expectedItem in expected)
            {
                CollectionAssert.Contains(roundedActuals, Convert.ToSingle(Math.Round(expectedItem, decimalPlaces)));
            }
        }

        /// <summary>
        /// Asserts the given object is a readonly observable type
        /// </summary>
        /// <param name="propertyObj"></param>
        public static void AssertIsReadOnlyObservable(object propertyObj)
        {
            Assert.IsNotNull(propertyObj, "Property should never be null");

            bool isReadOnly = ((IList)propertyObj).IsReadOnly;
            if (!isReadOnly)
            {
                //check it is not a csla readonly
                isReadOnly = (propertyObj is IReadOnlyCollection);
            }
            Assert.IsTrue(isReadOnly, "Collection items should not be editable");
            Assert.IsNotNull(propertyObj as INotifyCollectionChanged, "Collection should output change notifications");
        }

        /// <summary>
        /// Asserts the given object is an editable observable type
        /// </summary>
        /// <param name="propertyObj"></param>
        public static void AssertIsEditableObservable(object propertyObj)
        {
            Assert.IsNotNull(propertyObj, "Property should never be null");
            Assert.IsFalse(((IList)propertyObj).IsReadOnly, "Collection items should be editable");
            Assert.IsNotNull(propertyObj as INotifyCollectionChanged, "Collection should output change notifications");
        }

        /// <summary>
        /// Asserts the property is not writable
        /// </summary>
        /// <param name="ownerClassType"></param>
        /// <param name="propertyName"></param>
        public static void AssertPropertyIsReadOnly(Type ownerClassType, String propertyName)
        {
            PropertyInfo propInfo = ownerClassType.GetProperty(propertyName);
            Assert.IsTrue(propInfo.CanRead, "Property should be readable");

            bool canWrite = propInfo.CanWrite;
            if (canWrite)
            {
                canWrite = (propInfo.GetSetMethod() != null);
            }
            Assert.IsFalse(canWrite, "Property should not be writable");
        }

        /// <summary>
        /// Checks the property is a readonly standard collection
        /// </summary>
        /// <param name="propertyObj"></param>
        public static void AssertIsReadOnlyCollection(object propertyObj)
        {
            Assert.IsNotNull(propertyObj, "Property should never be null");

            bool isReadOnly = ((IList)propertyObj).IsReadOnly;
            if (!isReadOnly)
            {
                //look for another isreadonly property (found in csla.core base)
                PropertyInfo readOnlyProperty = propertyObj.GetType().GetProperty("IsReadOnly");
                if (readOnlyProperty != null)
                {
                    isReadOnly = (bool)readOnlyProperty.GetValue(propertyObj, null);
                }

            }
            Assert.IsTrue(isReadOnly, "Collection items should not be editable");
        }

        /// <summary>
        /// Checks the given model objects are equal
        /// Can be either dto vs model or model vs model
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        public static void AssertModelObjectsAreEqual(Object obj1, Object obj2)
        {
            AssertModelObjectsAreEqual(obj1, obj2, new List<String>());
        }

        /// <summary>
        ///     Asserts that two objects are the same by comparing each of the properties in the first with those of the second.
        /// </summary>
        /// <param name="expected">Object that contains the expected values. The set of properties will be taken from this one.</param>
        /// <param name="actual">Object that contains the acual values to be asserted.</param>
        /// <param name="excludedProperties">The collection of property names that are to be ignored when comparing both objects.</param>
        /// <remarks>Both models and dtos can be compared among them or with each other.</remarks>
        public static void AssertModelObjectsAreEqual(Object expected, Object actual, IEnumerable<String> excludedProperties)
        {
            Assert.IsFalse(expected == null ^ actual == null, "Either object is null and the other is not. They are not equal.");

            if (expected == null)
            {   
                // No need to test further.
                Assert.Pass("Both objects are null."); 
                return;
            }

            var hasComparedAtLeastOneProperty = false;
            var propertiesToCompare = expected.GetType().GetProperties().Where(o => o.CanRead && excludedProperties.All(e => !String.Equals(e, o.Name, StringComparison.InvariantCultureIgnoreCase)));
            foreach (var expectedProperty in propertiesToCompare)
            {
                var propertyName = expectedProperty.Name;
                var actualProperty = actual.GetType().GetProperty(propertyName);
                // Lets not be so strict and just expect at least one property to be checked, fail if there are absolutely no properties in common.
                if (actualProperty == null) continue;
                Assert.IsNotNull(actualProperty, "The actual object was missing property {0}", propertyName);
                hasComparedAtLeastOneProperty = true;

                var expectedValue = expectedProperty.GetValue(expected, null);
                var actualValue = actualProperty.GetValue(actual, null);

                // Check the type of both properties is the same. Cast if not.
                var expectedType = expectedProperty.PropertyType;
                if (expectedType != actualProperty.PropertyType)
                {
                    if (actualValue != null)
                    {
                        if (expectedType.IsEnum)
                        {
                            actualValue = Enum.Parse(expectedType, actualValue.ToString());
                        }
                        else if (actualProperty.PropertyType.IsEnum)
                        {
                            actualValue = Convert.ToInt64(actualValue);
                        }
                        else if (expectedType == typeof(Byte[]))
                        {
                            actualValue = ConvertObjectToBytes(actualValue);
                        }
                        else
                        {
                            actualValue = Convert.ChangeType(actualValue, expectedType);
                        }
                    }
                }

                if (Equals(expectedValue, String.Empty)) expectedValue = null;
                if (Equals(actualValue, String.Empty)) actualValue = null;

                Assert.AreEqual(expectedValue, actualValue, "Actual property {0} had a different value.", propertyName);

        //    //check objects for null
        //    if (obj1 == null || obj2 == null)
        //    {
        //        Assert.AreEqual(obj1, obj2);
        //    }

        //    //check properties
        //    IEnumerable<PropertyInfo> properties = obj1.GetType().GetProperties()
        //        .Where(p => p.DeclaringType == obj1.GetType());
        //    foreach (PropertyInfo pInfo in properties)
        //    {
        //        if (!excludeProperties.Contains(pInfo.Name))
        //        {

        //            if (pInfo.CanRead)
        //            {
        //                PropertyInfo obj2PInfo =
        //                    obj2.GetType().GetProperty(pInfo.Name);

        //                if (obj2PInfo != null)
        //                {
        //                    Object obj1Val = pInfo.GetValue(obj1, null);
        //                    Object obj2Val = obj2PInfo.GetValue(obj2, null);

        //                    //convert the 2nd val if necessary
        //                    if (pInfo.PropertyType != obj2PInfo.PropertyType)
        //                    {
        //                        if (obj2Val != null)
        //                        {
        //                            if (pInfo.PropertyType.IsEnum)
        //                            {
        //                                obj2Val = Enum.Parse(pInfo.PropertyType, obj2Val.ToString());
        //                            }
        //                            else if (obj2PInfo.PropertyType.IsEnum)
        //                            {
        //                                obj2Val = Convert.ToInt64(obj2Val);
        //                            }
        //                            else if (pInfo.PropertyType == typeof(Byte[]))
        //                            {
        //                                obj2Val = ConvertObjectToBytes(obj2Val);
        //                            }
        //                            else
        //                            {

        //                                obj2Val = Convert.ChangeType(obj2Val, pInfo.PropertyType);
        //                            }
        //                        }
        //                    }

        //                    if (obj1Val == String.Empty) obj1Val = null;
        //                    if (obj2Val == String.Empty) obj2Val = null;

        //                    Assert.AreEqual(obj1Val, obj2Val);
        //                }
        //            }
        //        }
            }
            Assert.IsTrue(hasComparedAtLeastOneProperty,"There were no common properties. Unable to determine if the objects have the same values.");
        }

        /// <summary>
        /// Tests that all child model objects have a value in the given model object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="modelObject"></param>
        public static void AssertChildModelObjectsAreNotNull<T>(T modelObject)
        {
            AssertChildModelObjectsAreNotNull(modelObject, new List<String>());
        }
        public static void AssertChildModelObjectsAreNotNull<T>(T modelObject, List<String> propertiesToExclude)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(typeof(T)
                .GetProperties().Where(p => p.DeclaringType == typeof(T)));
            foreach (String propertyName in propertiesToExclude)
            {
                properties.RemoveAll(p => p.Name == propertyName);
            }
            foreach (PropertyInfo property in properties)
            {
                //if the property represents a child model object
                if (property.PropertyType.BaseType != null)
                {
                    String baseTypeName = property.PropertyType.BaseType.Name;
                    if (baseTypeName.StartsWith("ModelObject") ||
                    baseTypeName.StartsWith("ModelReadOnlyObject") ||
                    baseTypeName.StartsWith("ModelList") ||
                    baseTypeName.StartsWith("ModelReadOnlyList"))
                    {
                        if (!property.Name.StartsWith("Parent"))
                        {
                            //check it is not null
                            Object value = property.GetValue(modelObject, null);
                            Assert.IsNotNull(value, String.Format("Child model object {0} is null", property.Name));
                        }
                    }
                }
            }
        }

        private static Byte[] ConvertObjectToBytes(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            byte[] bytes;
            MemoryStream ms = new MemoryStream();

            bf.Serialize(ms, obj);
            ms.Seek(0, 0);
            bytes = ms.ToArray();

            return bytes;
        }
    }
}
