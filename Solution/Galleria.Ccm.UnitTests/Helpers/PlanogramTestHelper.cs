#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-29105 : A.Silva
//      Created

#endregion

#region Version History: CCM803

// V8-29502 : A.Silva
//  Added SetDividerSettings extension method.
// V8-29420 : A.Silva
//  Added CreateAssortmentFromProducts.

#endregion

#region Version History: CCM810

// V8-29741 : A.Silva
//  Moved in CreateProductDtos and NewDefaultProductDto from InsertProductFromAssortment.TaskTests
// V8-29709 : A.Silva
//  Added CreateProductDtos overload to accept a list of sizes for the products to be created.
// V8-29768 : A.Silva
//  Refactored AddPerformanceData to accept an optional unitsSoldPerDay value.
// V8-30052 : A.Silva
//  Added overload for AddBlocking.

#endregion

#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32498 : A.Silva
//  Added AddFixtureItems to add several in one go.
// V8-31682 : A.Silva
//  Amended AddSequenceGroup so that it is possible to add sequence groups without a blocking group.
// CCM-18505 : A.Silva
//  Added AddProducts to allow adding multiple products in one go to improve performance.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Helpers;
using NUnit.Framework;
using Galleria.Framework.Planograms.Helpers;
using System.IO;

namespace Galleria.Ccm.UnitTests.Helpers
{
    internal static class PlanogramTestHelper
    {
        private const string PlanogramNameTemplate = "{0}.Planogram[{1}]";
        private const string FixtureNameTemplate = "{0}.Fixture[{1}]";
        private const string SequenceGroupNameTemplate = "{0}.SequenceGroup[{1}]";

        internal static IEnumerable<PlanogramPosition> GetPositions(this PlanogramFixtureComponent fc, Boolean orderXAsc = true)
        {
            var positions = fc.GetPlanogramSubComponentPlacements().SelectMany(s => s.GetPlanogramPositions());
            if (orderXAsc)
            {
                return positions.OrderBy(p => p.SequenceX);
            }
            else
            {
                return positions.OrderByDescending(p => p.SequenceX);
            }
        }

        internal static IEnumerable<String> GetPositionGtins(this PlanogramFixtureComponent fc, Boolean orderXAsc)
        {
            return fc.GetPositions(orderXAsc).Select(p => p.GetPlanogramProduct().Gtin);
        }

        /// <summary>
        /// Gets all merch groups for the planogram, processes them and applies them.
        /// </summary>
        internal static void ReprocessAllMerchandisingGroups(this Planogram plan)
        {
            using (PlanogramMerchandisingGroupList merchGroupList = plan.GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup group in merchGroupList)
                {
                    group.Process();
                    group.ApplyEdit();
                }
            }
        }

        internal static String TakeSnapshot(this Planogram target, String fileName = null)
        {
            target.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            target.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            target.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            if (String.IsNullOrWhiteSpace(fileName)) fileName = target.Name;
            fileName = Path.ChangeExtension(fileName, ".pog");
            String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), target.Parent.Name, fileName);
            String directoryName = Path.GetDirectoryName(path);
            if (directoryName == null) return "Could not take snapshot, invalid directory.";
            if (!Directory.Exists(directoryName)) Directory.CreateDirectory(directoryName);
            target.Parent.SaveAs(1, path);

            return path;
        }

        internal static Package CreatePackage(this String name)
        {
            Package package = Package.NewPackage(1, PackageLockType.User);
            package.Name = name;
            return package;
        }

        internal static Planogram AddPlanogram(this Package parent)
        {
            Planogram planogram = Planogram.NewPlanogram();
            planogram.Name = String.Format(PlanogramNameTemplate, parent.Name, parent.Planograms.Count);
            parent.Planograms.Add(planogram);
            return planogram;
        }

        internal static IEnumerable<PlanogramAssortmentProduct> AddAssortmentProducts(this PlanogramAssortment parent, IEnumerable<ProductDto> productDtos)
        {

            IEnumerable<PlanogramAssortmentProduct> assortmentProducts = productDtos.Select((dto, i) =>
            {
                PlanogramAssortmentProduct assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
                assortmentProduct.Gtin = dto.Gtin;
                assortmentProduct.IsRanged = true;
                assortmentProduct.Facings = (Byte) new Random().Next(1, 10);
                assortmentProduct.Rank = (Int16) (i + 1);
                return assortmentProduct;
            }).ToList();

            parent.Products.AddRange(assortmentProducts);

            return assortmentProducts;
        }

        public static IEnumerable<PlanogramAssortmentProduct> AddAssortmentProducts(this PlanogramAssortment parent, IEnumerable<PlanogramProduct> products = null)
        {
            if (products == null) products = new List<PlanogramProduct>();
            IEnumerable<PlanogramProduct> planogramProducts = products as IList<PlanogramProduct> ?? products.ToList();
            if (!planogramProducts.Any()) planogramProducts = parent.Parent.Products;

            IEnumerable<PlanogramAssortmentProduct> assortmentProducts =
                planogramProducts.Select((planogramProduct, i) =>
                                         {
                                             PlanogramAssortmentProduct assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
                                             assortmentProduct.Gtin = planogramProduct.Gtin;
                                             assortmentProduct.IsRanged = true;
                                             assortmentProduct.Facings = (Byte) new Random().Next(1, 10);
                                             assortmentProduct.Rank = (Int16) (i + 1);
                                             return assortmentProduct;
                                         }).ToList();
            parent.Products.AddRange(assortmentProducts);

            return assortmentProducts;
        }

        internal static PlanogramFixtureItem AddFixtureItem(this Planogram parent)
        {
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = String.Format(FixtureNameTemplate, parent.Name, parent.Fixtures.Count);
            fixture.Width = 120;
            fixture.Height = 200;
            fixture.Depth = 75;
            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            parent.Fixtures.Add(fixture);
            parent.FixtureItems.Add(fixtureItem);
            return fixtureItem;
        }

        /// <summary>
        ///     Add the specified number of <see cref="PlanogramFixtureItem"/> instances to the <paramref name="parent"/>.
        /// </summary>
        /// <param name="parent">The instance of <see cref="Planogram"/> to add new child fixture items to.</param>
        /// <param name="number">The number of child instances to add.</param>
        /// <returns>A collection with the newly added instances.</returns>
        internal static List<PlanogramFixtureItem> AddFixtureItems(this Planogram parent, Int32 number)
        {
            var items = new List<PlanogramFixtureItem>();
            
            for (var i = 0; i < number; i++)
            {
                items.Add(parent.AddFixtureItem());
            }

            return items;
        }

        internal static PlanogramFixtureItem AddFixtureItem(this Planogram parent, 
            Single fixtureHeight, Single fixtureWidth, Single fixtureDepth, 
            Single backboardDepth = 1, Single baseHeight = 10)
        {
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = String.Format(FixtureNameTemplate, parent.Name, parent.Fixtures.Count);
            fixture.Width = fixtureWidth;
            fixture.Height = fixtureHeight;
            fixture.Depth = fixtureDepth;
            
            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            parent.Fixtures.Add(fixture);
            parent.FixtureItems.Add(fixtureItem);

            if (backboardDepth >0)
            {
                //add a backboard
                PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, backboardDepth);
                backboardFc.Z = -backboardDepth;
            }

            if (baseHeight > 0)
            {
                //Add a base
                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, baseHeight, fixtureDepth - backboardDepth);
            }

            return fixtureItem;
        }

        internal static PlanogramFixtureComponent AddFixtureComponent(this PlanogramFixtureItem parent, PlanogramComponentType type, PointValue origin = default(PointValue), WidthHeightDepthValue size = default(WidthHeightDepthValue))
        {
            PlanogramFixture fixture = parent.GetPlanogramFixture();
            String name = String.Format("{0}.Component[{1}]", fixture.Name, fixture.Components.Count);
            switch (type)
            {
                case PlanogramComponentType.Peg:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120,120,4);
                    break;
                case PlanogramComponentType.Rod:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(4,4,75);
                    break;
                case PlanogramComponentType.Chest:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120,50,75);
                    break;
                case PlanogramComponentType.Bar:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 2, 2);
                    break;
                case PlanogramComponentType.ClipStrip:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(4, 100, 1);
                    break;
                default:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120,4,75);
                    break;
            }
            PlanogramComponent component = PlanogramComponent.NewPlanogramComponent(name, type, size.Width, size.Height, size.Depth);
            PlanogramFixtureComponent fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.X = origin.X;
            fixtureComponent.Y = origin.Y;
            fixtureComponent.Z = origin.Z;
            parent.Parent.Components.Add(component);
            fixture.Components.Add(fixtureComponent);
            return fixtureComponent;
        }

        internal static PlanogramFixtureAssembly AddFixtureAssembly(this PlanogramFixtureItem parent, PointValue origin = default(PointValue), WidthHeightDepthValue size = default(WidthHeightDepthValue))
        {
            PlanogramFixture fixture = parent.GetPlanogramFixture();
            String name = String.Format("{0}.Assembly[{1}]", fixture.Name, fixture.Assemblies.Count);
            PlanogramAssembly assembly = PlanogramAssembly.NewPlanogramAssembly(name);
            assembly.Width = size.Width;
            assembly.Height = size.Height;
            assembly.Depth = size.Depth;
            parent.Parent.Assemblies.Add(assembly);
            PlanogramFixtureAssembly fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
            fixtureAssembly.PlanogramAssemblyId = assembly.Id;
            fixtureAssembly.X = origin.X;
            fixtureAssembly.Y = origin.Y;
            fixtureAssembly.Z = origin.Z;
            fixture.Assemblies.Add(fixtureAssembly);
            return fixtureAssembly;
        }

        internal static PlanogramAssemblyComponent AddAssemblyComponent(this PlanogramFixtureAssembly parent, PlanogramComponentType type, PointValue origin = default(PointValue), WidthHeightDepthValue size = default(WidthHeightDepthValue))
        {
            PlanogramAssembly assembly = parent.GetPlanogramAssembly();
            String name = String.Format("{0}.Component[{1}]", assembly.Name, assembly.Components.Count);
            switch (type)
            {
                case PlanogramComponentType.Peg:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 120, 4);
                    break;
                case PlanogramComponentType.Rod:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(4, 4, 75);
                    break;
                case PlanogramComponentType.Chest:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 50, 75);
                    break;
                case PlanogramComponentType.Bar:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 2, 2);
                    break;
                case PlanogramComponentType.ClipStrip:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(4, 100, 1);
                    break;
                default:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 4, 75);
                    break;
            }
            PlanogramComponent component = PlanogramComponent.NewPlanogramComponent(name, type, size.Width, size.Height, size.Depth);
            PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(component);
            assemblyComponent.X = origin.X;
            assemblyComponent.Y = origin.Y;
            assemblyComponent.Z = origin.Z;
            parent.Parent.Parent.Components.Add(component);
            assembly.Components.Add(assemblyComponent);
            // Adjust Assembly new size.
            IEnumerable<PlanogramComponent> assemblyPlanComponents = assembly.Components.Select(c => c.GetPlanogramComponent()).ToList();
            assembly.Width = assembly.Components.Min(c => c.X) + assemblyPlanComponents.Max(c => c.Width);
            assembly.Height = assembly.Components.Min(c => c.Y) + assemblyPlanComponents.Max(c => c.Height);
            assembly.Depth = assembly.Components.Min(c => c.Z) + assemblyPlanComponents.Max(c => c.Depth);
            return assemblyComponent;
        }

        internal static PlanogramPosition AddPosition(
            this PlanogramFixtureComponent parent, PlanogramFixtureItem parentFixtureItem, PlanogramProduct product = null, Boolean resequencePositions = true)
        {
            PlanogramSubComponent subComponent = parent.GetPlanogramComponent().SubComponents.First();
            Planogram planogram = parent.Parent.Parent;
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(subComponent, parentFixtureItem, parent);
            if (product == null) product = planogram.AddProduct();
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition((Int16) subComponentPlacement.GetPlanogramPositions().Count(), product, subComponentPlacement);
            var facings = new WideHighDeepValue(1, 1, 1);
            var blockFacings = new WideHighDeepValue(0, 0, 0);
            position.FacingsWide = facings.Wide;
            position.FacingsHigh = facings.High;
            position.FacingsDeep = facings.Deep;
            position.FacingsXWide = blockFacings.Wide;
            position.FacingsXHigh = blockFacings.High;
            position.FacingsXDeep = blockFacings.Deep;
            position.FacingsYWide = blockFacings.Wide;
            position.FacingsYHigh = blockFacings.High;
            position.FacingsYDeep = blockFacings.Deep;
            position.FacingsZWide = blockFacings.Wide;
            position.FacingsZHigh = blockFacings.High;
            position.FacingsZDeep = blockFacings.Deep;
            planogram.Positions.Add(position);

            return position;
        }
        /// <summary>
        ///     Sets the <c>Planogram Placement Types</c> for the <paramref name="planogram"/> on all three axes.
        /// </summary>
        /// <param name="planogram">The planogram to set the <c>Planogram Placement Types</c> for.</param>
        /// <param name="placementXType">Placement type for the X axis. Manual if not provided.</param>
        /// <param name="placementYType">Placement type for the Y axis. Manual if not provided.</param>
        /// <param name="placementZType">Placement type for the Z axis. Manual if not provided.</param>
        /// <returns>The source <paramref name="planogram"/> with its <c>Planogram Placement Types</c> set.</returns>
        internal static Planogram SetAutoFill(
            this Planogram planogram,
            PlanogramProductPlacementXType placementXType = PlanogramProductPlacementXType.Manual,
            PlanogramProductPlacementYType placementYType = PlanogramProductPlacementYType.Manual,
            PlanogramProductPlacementZType placementZType = PlanogramProductPlacementZType.Manual)
        {
            planogram.ProductPlacementX = placementXType;
            planogram.ProductPlacementY = placementYType;
            planogram.ProductPlacementZ = placementZType;
            return planogram;
        }

        internal static PlanogramPosition SetSequence(this PlanogramPosition source, Int16 x, Int16 y, Int16 z)
        {
            source.SequenceX = x;
            source.SequenceY = y;
            source.SequenceZ = z;
            return source;
        }
        #region Planogram Product

        internal static PlanogramProduct AddProduct(this Planogram parent,
            Single unitHeight, Single unitWidth, Single unitDepth)
        {
            PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
            product.Name = String.Format("Product[{0}]", parent.Products.Count);
            product.Gtin = String.Format("Product[{0}]", parent.Products.Count);
            product.Width = unitWidth;
            product.Height = unitHeight;
            product.Depth = unitDepth;
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            parent.Products.Add(product);
            return product;
        }


        internal static PlanogramProduct AddProduct(this Planogram parent,
            WidthHeightDepthValue dimensions = default(WidthHeightDepthValue), Boolean allowSqueeze = true)
        {
            if (dimensions == default(WidthHeightDepthValue)) dimensions = new WidthHeightDepthValue(5, 10, 2);
            PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
            product.Name = String.Format("Product[{0}]", parent.Products.Count);
            product.Gtin = String.Format("Product[{0}]", parent.Products.Count);
            product.Width = dimensions.Width;
            product.Height = dimensions.Height;
            product.Depth = dimensions.Depth;
            if (!allowSqueeze)
            {
                product.SqueezeWidth = 0;
                product.SqueezeHeight = 0;
                product.SqueezeDepth = 0;
            }
            parent.Products.Add(product);
            return product;
        }

        internal static PlanogramProduct AddProduct(this Planogram parent, ProductDto productDto)
        {
            PlanogramProduct product = NewPlanogramProduct(productDto);
            parent.Products.Add(product);
            return product;
        }

        internal static Planogram AddProducts(
            this Planogram parent, WidthHeightDepthValue size = default(WidthHeightDepthValue), Boolean allowSqueeze = true, Int16 iterations = 1)
        {
            if (size == default(WidthHeightDepthValue))
            {
                size = new WidthHeightDepthValue(5, 10, 2);
            }
            Int32 startingIndex = parent.Products.Count;
            var products = new List<PlanogramProduct>();
            for (var i = startingIndex; i < iterations + startingIndex; i++)
            {
                    PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
                    String idString = $"Product[{i}]";
                    product.Name = idString;
                    product.Gtin = idString;
                    product.Width = size.Width;
                    product.Height = size.Height;
                    product.Depth = size.Depth;
                    if (!allowSqueeze)
                    {
                        product.SqueezeWidth = 0;
                        product.SqueezeHeight = 0;
                        product.SqueezeDepth = 0;
                    }
                    products.Add(product);
            }
            parent.Products.AddList(products);
            return parent;
        }

        internal static List<PlanogramPosition> PlaceProductsOnComponent(PlanogramFixtureComponent component,
            IEnumerable<ProductDto> productDtos)
        {
            PlanogramFixture fixture = component.Parent;
            Planogram planogram = fixture.Parent;
            PlanogramFixtureItem fixtureItem =
                planogram.FixtureItems.FirstOrDefault(o => o.PlanogramFixtureId == fixture.Id);
            if (fixtureItem == null)
                Assert.Inconclusive("PlaceProductsOnComponent could not find the FixtureItem for the FixtureComponent.");

            return productDtos.Select(dto => component.AddPosition(fixtureItem, planogram.AddProduct(dto))).ToList();
        }

        internal static PlanogramProduct NewPlanogramProduct(ProductDto productDto)
        {
            PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
            product.Name = productDto.Name;
            product.Gtin = productDto.Gtin;
            product.Width = productDto.Width;
            product.Height = productDto.Height;
            product.Depth = productDto.Depth;
            return product;
        }

        #endregion

        internal static PlanogramSequenceGroup AddSequenceGroup(this Planogram parent, PlanogramBlockingGroup blockingGroup, IEnumerable<PlanogramProduct> productsInSequence)
        {
            PlanogramSequenceGroup planogramSequenceGroup = blockingGroup != null
                                                                ? PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup)
                                                                : PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            planogramSequenceGroup.Name = String.Format(SequenceGroupNameTemplate, parent.Name, parent.Sequence.Groups.Count);
            parent.Sequence.Groups.Add(planogramSequenceGroup);
            var sequence = 1;
            foreach (PlanogramSequenceGroupProduct sequenceProduct in productsInSequence.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct))
            {
                sequenceProduct.SequenceNumber = sequence++;
                planogramSequenceGroup.Products.Add(sequenceProduct);
            }
            return planogramSequenceGroup;
        }

        internal static IEnumerable<PlanogramAssortmentProduct> CreateAssortmentFromProducts(
            this Planogram parent, IEnumerable<PlanogramProduct> orderedProducts = null)
        {
            IEnumerable<PlanogramAssortmentProduct> assortmentProducts =
                (orderedProducts ?? parent.Products.OrderBy(p => p.Gtin))
                      .Select(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct)
                      .Select((product, i) =>
                              {
                                  product.Rank = (Int16) (i + 1);
                                  product.IsRanged = true;
                                  return product;
                              }).ToList();
            parent.Assortment.Products.AddRange(assortmentProducts);

            return assortmentProducts;
        }

        internal static PlanogramBlocking AddBlocking(this Planogram parent)
        {
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            parent.Blocking.Add(blocking);
            return blocking;
        }

        /// <summary>
        ///     Adds Blocking Groups and optionally Sequence Groups.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="itemCount">The number of groups to add.</param>
        /// <param name="includeSequence">Whether to add corresponding Sequence Groups for each Blocking Group.</param>
        internal static PlanogramBlocking AddBlocking(this Planogram parent, Int32 itemCount, Boolean includeSequence)
        {
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            parent.Blocking.Add(blocking);
            for (var i = 0; i < itemCount; i++)
            {
                PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
                blockingGroup.Colour = blocking.GetNextBlockingGroupColour();
                blockingGroup.Name = String.Format("BlockingGroup[{0}]", (i + 1));
                blocking.Groups.Add(blockingGroup);
                if (!includeSequence) continue;

                PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = blockingGroup.Colour;
                parent.Sequence.Groups.Add(sequenceGroup);
            }
            return blocking;
        }

        /// <summary>
        ///     Creates a Blocking Strategy from the collection of provided <paramref name="dividers"/>.
        /// </summary>
        /// <param name="planogram">The source planogram to add the new Blocking Strategy to.</param>
        /// <param name="dividers">The collection of (Width, Height) percentages to place each divider.</param>
        /// <remarks>
        ///     If a single divider with (0.0F, 0.0F) is provided the Blocking Strategy will be created with a single Block.
        /// </remarks>
        internal static PlanogramBlocking AddBlocking(this Planogram planogram, IEnumerable<Tuple<Single, Single>> dividers)
        {
            PlanogramBlocking blocking = planogram.AddBlocking();
            foreach (Tuple<Single, Single> divider in dividers)
            {
                Boolean isHorizontal = divider.Item1.EqualTo(0.0F);
                Boolean isVertical = divider.Item2.EqualTo(0.0F);
                Boolean isSingleBlock = isHorizontal && isVertical;
                if (isSingleBlock)
                {
                    Boolean hasBlocks = blocking.Groups.Any();
                    if (hasBlocks)
                        throw new InvalidOperationException("Cannot add a single block if there are other blocks present, review parameters.");
                    // Add a single block, sequence group and location.
                    PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
                    blockingGroup.Colour = blocking.GetNextBlockingGroupColour();
                    blockingGroup.Name = "SingleBlockingGroup";
                    blocking.Groups.Add(blockingGroup);
                    PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                    planogram.Sequence.Groups.Add(sequenceGroup);
                    PlanogramBlockingLocation blockingLocation = PlanogramBlockingLocation.NewPlanogramBlockingLocation(blockingGroup);
                    blocking.Locations.Add(blockingLocation);
                }
                else
                {
                    PlanogramBlockingDividerType dividerType = isHorizontal
                                                                   ? PlanogramBlockingDividerType.Horizontal
                                                                   : PlanogramBlockingDividerType.Vertical;
                    IEnumerable<PlanogramSequenceGroup> sequenceGroups = blocking
                        .Dividers.Add(divider.Item1, divider.Item2, dividerType)
                        .GetLocations()
                        .Select(l => l.GetPlanogramBlockingGroup())
                        .Where(blockingGroup => planogram.Sequence.Groups.All(g => !g.IsColourMatch(blockingGroup)))
                        .Select(PlanogramSequenceGroup.NewPlanogramSequenceGroup);
                    planogram.Sequence.Groups.AddRange(sequenceGroups);
                }
            }
            return blocking;
        }

        internal static PlanogramBlockingGroup AddGroup(this PlanogramBlocking blocking)
        {
            PlanogramBlockingGroup group = PlanogramBlockingGroup.NewPlanogramBlockingGroup(
                Guid.NewGuid().ToString(),BlockingHelper.GetNextBlockingGroupColour(blocking));
            blocking.Groups.Add(@group);
            return @group;
        }

        [Obsolete("To add a single block use AddBlocking(New List<Tuple<Single, Single>> {new Tuple<Single, Single>(0.0F, 0.0F)}) -- Any other Blocking Strategy can be created using other Divider collections and the previous method.")]
        internal static PlanogramBlockingLocation AddLocation(this PlanogramBlockingGroup group, Single startX, Single endX, Single startY, Single endY)
        {
            PlanogramBlockingLocation location = PlanogramBlockingLocation.NewPlanogramBlockingLocation(@group);
            @group.Parent.Locations.Add(location);
            
            // Left
            if (startX.EqualTo(0))
            {
                location.PlanogramBlockingDividerLeftId = null;
            }
            else
            {
                location.PlanogramBlockingDividerLeftId =
                    @group.Parent.Dividers.Add(startX, startY, PlanogramBlockingDividerType.Vertical).Id;
            }

            // Right
            if (endX.EqualTo(1))
            {
                location.PlanogramBlockingDividerRightId = null;
            }
            else
            {
                location.PlanogramBlockingDividerRightId =
                    @group.Parent.Dividers.Add(endX, startY, PlanogramBlockingDividerType.Vertical).Id;
            }

            // Bottom
            if (startY.EqualTo(0))
            {
                location.PlanogramBlockingDividerBottomId = null;
            }
            else
            {
                location.PlanogramBlockingDividerBottomId =
                    @group.Parent.Dividers.Add(startX, startY, PlanogramBlockingDividerType.Horizontal).Id;
            }

            // Top
            if (endY.EqualTo(1))
            {
                location.PlanogramBlockingDividerTopId = null;
            }
            else
            {
                location.PlanogramBlockingDividerTopId =
                    @group.Parent.Dividers.Add(startX, endY, PlanogramBlockingDividerType.Horizontal).Id;
            }

            return location;
        }

        /// <summary>
        ///     Creates a collection of new <paramref name="itemCount"/> <see cref="ProductDto"/> items. 
        ///     Optionally using the <paramref name="startingId"/> and with the specified <paramref name="size"/>.
        /// </summary>
        /// <param name="itemCount">The number of <see cref="ProductDto"/> to be created.</param>
        /// <param name="entityId">The Entity Id to create the products for.</param>
        /// <param name="startingId">The starting Id for the <see cref="ProductDto"/> (0 by default).</param>
        /// <param name="size">The size for each <see cref="ProductDto"/> created.</param>
        /// <remarks>
        ///     If used to create several "groups" of <see cref="ProductDto"/>, 
        ///     ensure each group will have different Ids by setting the <paramref name="startingId"/> appropriately.
        /// </remarks>
        public static IEnumerable<ProductDto> CreateProductDtos(Int32 itemCount, Int32 entityId, Int32 startingId = 0,
                                                              WidthHeightDepthValue size = default(WidthHeightDepthValue))
        {
            if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(10, 10, 10);

            for (var i = (Int16) (startingId + 1); i < startingId + itemCount + 1; i++)
            {
                ProductDto dto = NewDefaultProductDto(i, entityId, "Product{0}");
                dto.Width = size.Width;
                dto.Height = size.Height;
                dto.Depth = size.Depth;
                yield return dto;
            }
        }

        /// <summary>
        ///     Creates a collection of new <see cref="ProductDto"/> items with the provided <paramref name="itemSizes"/>
        /// </summary>
        /// <param name="itemSizes">Enumeration of the size for each individual product to get a dto for.</param>
        /// <param name="entityId">The Entity Id to create the products for.</param>
        /// <param name="startingIndex"></param>
        public static IEnumerable<ProductDto> CreateProductDtos(IEnumerable<WidthHeightDepthValue> itemSizes, Int32 entityId, Int32 startingIndex = 0)
        {
            return new List<ProductDto>(itemSizes.SelectMany((s, i) => CreateProductDtos(1, entityId, startingIndex + i, s)));
        }

        private static ProductDto NewDefaultProductDto(Int32 id, Int32 entityId, String nameMask = "name{0}",
                                                       String gtinMask = "gtin{0}")
        {
            return new ProductDto
                   {
                       Id = id,
                       EntityId = entityId,
                       ReplacementProductId = id,
                       Gtin = String.Format(gtinMask, id),
                       Name = String.Format(nameMask, id),
                       IsNewProduct = true,
                       DateCreated = DateTime.Now,
                       DateLastModified = DateTime.Now,
                       DateDeleted = null,
                   };
        }

        public static void SetBlockingGroupPlacementTypes(this PlanogramBlockingGroup blockingGroup,
                                                           PlanogramBlockingGroupPlacementType primary, PlanogramBlockingGroupPlacementType secondary,
                                                           PlanogramBlockingGroupPlacementType tertiary)
        {
            blockingGroup.BlockPlacementPrimaryType = primary;
            blockingGroup.BlockPlacementSecondaryType = secondary;
            blockingGroup.BlockPlacementTertiaryType = tertiary;
        }

        /// <summary>
        ///     Adds the products created from <paramref name="productDtos"/> to the provided <paramref name="sequenceGroup"/>.
        /// </summary>
        /// <param name="sequenceGroup"></param>
        /// <param name="productDtos"></param>
        public static void AddSequenceProducts(this PlanogramSequenceGroup sequenceGroup, IEnumerable<ProductDto> productDtos)
        {
            sequenceGroup.AddSequenceProducts(productDtos.Select(NewPlanogramProduct));
        }

        public static void AddSequenceProducts(this PlanogramSequenceGroup sequenceGroup, IEnumerable<PlanogramProduct> planogramProducts)
        {
            PlanogramSequenceGroupProductList products = sequenceGroup.Products;
            products.Clear();
            foreach (PlanogramSequenceGroupProduct sequenceProduct in
                    planogramProducts.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct))
            {
                sequenceProduct.SequenceNumber = products.Count + 1;
                products.Add(sequenceProduct);
            }
        }

        public static ProductDto NewInsertedProductDto(String gtin, Int32 entityId)
        {
            ProductList existingProducts = ProductList.FetchByEntityId(entityId);
            Int32 lastId = existingProducts.Max(o => o.Id);
            ProductDto dto = NewDefaultProductDto(++lastId, entityId, gtin, gtin);
            dto.Width = 10;
            dto.Height = 10;
            dto.Depth = 10;
            return dto;
        }

        public static IEnumerable<PlanogramSubComponentPlacement> GetSubComponentPlacementsFor(this Planogram parent,
                                                                                               IEnumerable<ProductDto> productsToInsert)
        {
            using (PlanogramMerchandisingGroupList merchandisingGroups = parent.GetMerchandisingGroups())
            {
                IEnumerable<String> gtinsToInsert = productsToInsert.Select(d => d.Gtin);
                PlanogramMerchandisingGroup merchandisingGroup =
                    merchandisingGroups.FirstOrDefault(
                        o => !gtinsToInsert.Except(o.PositionPlacements.Select(p => p.Product.Gtin)).Any());
                return merchandisingGroup != null
                           ? merchandisingGroup.SubComponentPlacements
                           : new List<PlanogramSubComponentPlacement>();
            }
        }

        public static void AddPerformanceData(this PlanogramPerformanceDataList source,
                                              PlanogramProduct product,
                                              Single performanceValue,
                                              Single unitsSoldPerDay = 1.0F)
        {
            PlanogramPerformanceData performanceData =
                source.FirstOrDefault(p => p.PlanogramProductId == product.Id);
            if (performanceData == null)
            {
                performanceData = PlanogramPerformanceData.NewPlanogramPerformanceData(product);

                source.Add(performanceData);
            }
            for (Byte i = 1; i <= 20; i++)
            {
                PropertyInfo propertyInfo = typeof (PlanogramPerformanceData).GetProperty(String.Format("P{0}", i));
                propertyInfo.SetValue(performanceData, performanceValue, null);
            }
            performanceData.UnitsSoldPerDay = unitsSoldPerDay;
        }
    }
}