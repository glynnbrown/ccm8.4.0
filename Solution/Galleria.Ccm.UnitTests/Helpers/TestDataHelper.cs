﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24801 : L.Hodson
//		Created
// V8-25478 : A.Kuszyk
//      Added TestDatabaseName.
// V8-25628 : A.Kuszyk
//      Added additional Location related DTO insert methods.
// V8-25630 : A.Kuszyk
//      Added ProductUniverse related DTO insert methods.
// V8-25632 : A.Kuszyk
//      Added Consumer Decision Tree related DTO insert methods.
// V8-25881 : A.Probyn
//      Added Planogram related DTO insert methods.
// V8-26041 : A.Kuszyk
//      Added Image related DTO insert methods.
// V8-25455 : J.Pickup
//      Added InserAssortmentMinorRevisonDto's
// V8-26217 : A.Kuszyk
//      Amended InsertProductDtos to insert a record for CustomAttributeData as well.
// V8-26270 : A.Kuszyk
//      Added PlanogramPerformance, PlanogramPerformanceData and PlanogramPerformanceMetric
//      dto insert methods.
// V8-26158 : I.George
//      Added InsertMetricDtos
// V8-26124 : I.George
//      Added the InsertInventoryProfileDtos
// V8-26426 : A.Kuszyk
//      Added Planogram Assortment related insert methods.
// V8-26474 : A.Silva ~ Added InsertValidationTemplateDtos().
// V8-26573 : L.Luong
//      Added PlanogramValidationTemplate and related methods
// V8-26706 : L.Luong
//      Added PlanogramConsumerDecisionTree and related methods
// V8-26704 : A.Kuszyk
//      Added Assortment insert methods.
// V8-26836 : L.Luong
//      Added PlanogramEventLog insert method
// V8-26911 : L.Luong
//      Added EventLog and related insert methods
// V8-26956 : L.Luong
//      Modified PlanogramConsumerDecisionTreeNodeProduct insert method
// V8-26520 : J.Pickup
//      Added ContentLookup.
// V8-27058 : A.Probyn
//      Added InsertPlanogramMetaDataImageDtos
// V8-27153 : A.Silva   ~ Added InsertRenumberingStrategyDtos.
// V8-27485 : A.Kuszyk
//  Added some PlanogramBlocking helpers.
// V8-27647 : L.Luong
//      Added PlanogramInventory insert methods 
// V8-27940 : L.Luong
//      Added Label insert methods
// V8-24779 : L.Luong
//      Added PlanogramImportTemplate insert methods
// V8-28016 : I.George
//      Added InsertWorkflowTaskDtos method
// V8-26322 : A.Silva
//      Amended InsertPackageDtos to accept an already existing userName instead of adding a new one on its own.
//      Amended InsertPlanogramDtos to accept an already existing userName instead of adding a new one on its own.

#endregion
#region Version History: (CCM 801)
// CCM-28519 :J.Pickup
//	Removed Date Deleted
// CCM-28878 : D.Pleasance
//  Added InsertPlanogramImportTemplatePerformanceMetricDtos
// CCM-29010 : D.Pleasance
//  Added InsertPlanogramNameTemplateDtos
#endregion
#region Version History : CCM 802

// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-29232 : A.Silva
//      Added overload for InsertProductDtos and refactored to allow easier customization of products in tests.

#endregion
#region Version History : CCM 803
// V8-29491 : D.Pleasance
//  Added InsertPerformanceSelection \ InsertWorkpackagePerformanceDtos
// V8-29590 : A.Probyn
//  Added new overloaded InsertEventLogDtos for workpackage error logs.
//  Extended CreatePlanCreweSparklingWater to create a PlanogramEventLog
#endregion
#region Version History: CCM810

// V8-29741: A.Silva
//  Added InsertAssortmentProductDtos using a collection of product Ids.
// V8-29746 : A.Probyn
//  Updated InsertContentLookupDtos with changes to replace Names with Ids
#endregion
#region Version History: CCM820
// V8-30736: L.Luong
//  Added InsertLocationProductIllegalDtos
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31562 : D.Pleasance
//  Added InsertLocationSpaceElementDtos()
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31831 : A.Probyn
//  Removed SeparatorType from PlanogramNameTemplate defaults, and added PlanogramAttribute
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using AutoMapper;
using Csla.Data;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Reporting.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Ccm.Dal.Mock;
using Galleria.Ccm.Engine;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.UnitTests.Helpers
{
    public static class TestDataHelper
    {
        #region General

        public static Byte[] ConvertStringToBytes(String str)
        {
            BinaryFormatter bf = new BinaryFormatter();
            byte[] bytes;
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, str);
                ms.Seek(0, 0);
                bytes = ms.ToArray();
                return bytes;
            }
        }

        /// <summary>
        /// A unique name to give template databases when running NUnit tests.  By using this static 
        /// field, it is possible to only create each database (sql server/vista db) once per whole NUnit test run
        /// rather than once per TestFixture, generating further performance gains.
        /// </summary>
        public static string TestDatabaseName = Guid.NewGuid().ToString();

        public static Object GetValue1(Type propertyType)
        {
            return GetValue1(propertyType, true);
        }

        public static Object GetValue1(Type propertyType, Boolean createChildren)
        {
            Object returnValue = null;
            Boolean typeFound = true;
            if (propertyType.IsEnum)
            {
                returnValue = Enum.GetValues(propertyType).GetValue(0);
            }
            else if (propertyType == typeof(Byte[]))
            {
                returnValue = new Byte[] { 2, 1, 9, 3, 8, 2 };
            }
            else if (propertyType == typeof(int))
            {
                returnValue = 219382;
            }
            else if (propertyType == typeof(float))
            {
                returnValue = 213.123123F;
            }
            else if (propertyType == typeof(Double))
            {
                returnValue = (Double)213.123123D;
            }
            else if (propertyType == typeof(string))
            {
                returnValue = "C4FC766A-E741";
            }
            else if (propertyType == typeof(DateTime))
            {
                returnValue = DateTime.Today;
            }
            else if (propertyType == typeof(bool))
            {
                returnValue = true;
            }
            else if (propertyType == typeof(short))
            {
                returnValue = (short)12312;
            }
            else if (propertyType == typeof(byte))
            {
                returnValue = (byte)189;
            }
            else if (propertyType == typeof(Guid))
            {
                returnValue = new Guid("738B4722-5975-42B9-B8AD-19CDE92C8C20");
            }
            else if (propertyType == typeof(Int64))
            {
                returnValue = 34543535353;
            }
            else if (propertyType == typeof(RowVersion))
            {
                returnValue = new RowVersion((Int64)2);
            }
            else if (propertyType == typeof(Object))
            {
                returnValue = 219382;
            }
            else if (propertyType.IsGenericType && (propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                returnValue = null;
            }

            else
            {
                ConstructorInfo constructor = propertyType.GetConstructor(Type.EmptyTypes);
                if (constructor != null)
                {
                    if (createChildren)
                    {
                        returnValue = constructor.Invoke(null);
                    }
                }
                else
                {
                    // Model object?
                    MethodInfo method = propertyType.GetMethod(
                        String.Format("New{0}", propertyType.Name),
                        Type.EmptyTypes);
                    if (method != null)
                    {
                        if (createChildren)
                        {
                            returnValue = method.Invoke(null, null);
                        }
                    }
                    else
                    {
                        typeFound = false;
                    }
                }
                if (typeFound && createChildren)
                {
                    foreach (PropertyInfo property in propertyType.GetProperties().Where(p => p.CanWrite))
                    {
                        property.SetValue(returnValue, GetValue1(property.PropertyType, false), null);
                    }
                }
            }
            if (!typeFound && createChildren)
            {
                throw new Exception(String.Format("Couldn't create dummy value for type {0}.", propertyType.Name));
            }
            return returnValue;
        }

        public static Object GetValue2(Type propertyType)
        {
            return GetValue2(propertyType, true);
        }

        public static Object GetValue2(Type propertyType, Boolean createChildren)
        {
            Object returnValue = null;
            Boolean typeFound = true;
            if (propertyType.IsEnum)
            {
                Array values = Enum.GetValues(propertyType);
                returnValue = values.GetValue(Math.Min(1, values.Length - 1));
            }
            else if (propertyType.IsGenericType &&
                (propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) &&
                propertyType.GetGenericArguments()[0].IsEnum)
            {
                Array values = Enum.GetValues(propertyType.GetGenericArguments()[0]);
                returnValue = values.GetValue(Math.Min(1, values.Length - 1));
            }
            else if (propertyType == typeof(Byte[]))
            {
                returnValue = new Byte[] { 2, 1, 9, 3, 8, 3 };
            }
            else if ((propertyType == typeof(int)) || (propertyType == typeof(int?)))
            {
                returnValue = 219383;
            }
            else if ((propertyType == typeof(float)) || (propertyType == typeof(float?)))
            {
                returnValue = 214.123123F;
            }
            else if ((propertyType == typeof(Double)) || (propertyType == typeof(Double?)))
            {
                returnValue = (Double)214.123123D;
            }
            else if (propertyType == typeof(string))
            {
                returnValue = "C4FC766A-E743";
            }
            else if ((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?)))
            {
                returnValue = DateTime.Today.AddDays(-1);
            }
            else if ((propertyType == typeof(bool)) || (propertyType == typeof(bool?)))
            {
                returnValue = false;
            }
            else if ((propertyType == typeof(short)) || (propertyType == typeof(short?)))
            {
                returnValue = (short)12313;
            }
            else if ((propertyType == typeof(byte)) || (propertyType == typeof(byte?)))
            {
                returnValue = (byte)190;
            }
            else if ((propertyType == typeof(Guid)) || (propertyType == typeof(Guid?)))
            {
                returnValue = new Guid("738B4722-5975-42B9-B8AD-19CDE92C8C21");
            }
            else if ((propertyType == typeof(Int64)) || (propertyType == typeof(Int64?)))
            {
                returnValue = 34543535354;
            }
            else if (propertyType == typeof(Object))
            {
                returnValue = 219383;
            }
            else if (propertyType == typeof(RowVersion))
            {
                returnValue = new RowVersion((Int64)3);
            }
            else
            {
                if (createChildren)
                {
                    ConstructorInfo constructor = propertyType.GetConstructor(Type.EmptyTypes);
                    if (constructor != null)
                    {
                        returnValue = constructor.Invoke(null);
                    }
                    else
                    {
                        // Model object?
                        Boolean valueCreated = false;

                        foreach (MethodInfo method in propertyType.GetMethods())
                        {
                            if (method.Name == String.Format("New{0}", propertyType.Name))
                            {
                                try
                                {
                                    ParameterInfo[] parameters = method.GetParameters();
                                    Object[] values = new Object[parameters.Length];
                                    for (Int32 i = 0; i < parameters.Length; i++)
                                    {
                                        values[i] = TestDataHelper.GetValue2(parameters[i].ParameterType);
                                    }
                                    returnValue = method.Invoke(null, values);


                                    valueCreated = true;
                                    break;

                                }
                                catch (Exception) { }

                            }
                        }

                        if (!valueCreated)
                        {
                            typeFound = false;
                        }

                        
                    }


                    if (typeFound)
                    {
                        foreach (PropertyInfo property in propertyType.GetProperties().Where(p => p.CanWrite))
                        {
                            property.SetValue(returnValue, GetValue2(property.PropertyType, false), null);
                        }
                    }

                }


            }
            if (!typeFound && createChildren)
            {
                throw new Exception(String.Format("Couldn't create dummy value for type {0}.", propertyType.Name));
            }
            return returnValue;
        }

        public static Object GetValue3(Type propertyType)
        {
            return GetValue3(propertyType, true);
        }

        public static Object GetValue3(Type propertyType, Boolean createChildren)
        {
            Object returnValue = null;
            Boolean typeFound = true;
            if (propertyType.IsEnum)
            {
                Array values = Enum.GetValues(propertyType);
                returnValue = values.GetValue(Math.Min(2, values.Length - 1));
            }
            else if (propertyType.IsGenericType &&
                (propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) &&
                propertyType.GetGenericArguments()[0].IsEnum)
            {
                Array values = Enum.GetValues(propertyType.GetGenericArguments()[0]);
                returnValue = values.GetValue(Math.Min(2, values.Length - 1));
            }
            else if (propertyType == typeof(Byte[]))
            {
                returnValue = new Byte[] { 2, 1, 9, 3, 8, 4 };
            }
            else if ((propertyType == typeof(int)) || (propertyType == typeof(int?)))
            {
                returnValue = 219384;
            }
            else if ((propertyType == typeof(float)) || (propertyType == typeof(float?)))
            {
                returnValue = 215.123123F;
            }
            else if ((propertyType == typeof(Double)) || (propertyType == typeof(Double?)))
            {
                returnValue = 215.123123D;
            }
            else if (propertyType == typeof(string))
            {
                returnValue = "C4FC766A-E744";
            }
            else if ((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?)))
            {
                returnValue = DateTime.Today.AddDays(-2);
            }
            else if ((propertyType == typeof(bool)) || (propertyType == typeof(bool?)))
            {
                returnValue = true;
            }
            else if ((propertyType == typeof(short)) || (propertyType == typeof(short?)))
            {
                returnValue = (short)12314;
            }
            else if ((propertyType == typeof(byte)) || (propertyType == typeof(byte?)))
            {
                returnValue = (byte)191;
            }
            else if ((propertyType == typeof(Guid)) || (propertyType == typeof(Guid?)))
            {
                returnValue = new Guid("738B4722-5975-42B9-B8AD-19CDE92C8C22");
            }
            else if ((propertyType == typeof(Int64)) || (propertyType == typeof(Int64?)))
            {
                returnValue = 34543535355;
            }
            else if (propertyType == typeof(Object))
            {
                returnValue = 219384;
            }
            else if (propertyType == typeof(RowVersion))
            {
                returnValue = new RowVersion((Int64)4);
            }
            else
            {
                ConstructorInfo constructor = propertyType.GetConstructor(Type.EmptyTypes);
                if (constructor != null)
                {
                    if (createChildren)
                    {
                        returnValue = constructor.Invoke(null);
                    }
                }
                else
                {
                    // Model object?
                    MethodInfo method = propertyType.GetMethod(
                        String.Format("New{0}", propertyType.Name),
                        Type.EmptyTypes);
                    if (method != null)
                    {
                        if (createChildren)
                        {
                            returnValue = method.Invoke(null, null);
                        }
                    }
                    else
                    {
                        typeFound = false;
                    }
                }
                if (typeFound && createChildren)
                {
                    foreach (PropertyInfo property in propertyType.GetProperties().Where(p => p.CanWrite))
                    {
                        property.SetValue(returnValue, GetValue3(property.PropertyType, false), null);
                    }
                }
            }
            if (!typeFound && createChildren)
            {
                throw new Exception(String.Format("Couldn't create dummy value for type {0}.", propertyType.Name));
            }
            return returnValue;
        }

        /// <summary>
        /// Populates the properties on the given model with values
        /// </summary>
        /// <param name="model"></param>
        public static void SetPropertyValues(Object model, Boolean createChildren = true)
        {
            SetPropertyValues(model, new List<String>(), createChildren);
        }
        public static void SetPropertyValues(Object model, List<String> excludeProperties, Boolean createChildren = true, Int32 seed=1)
        {
            PropertyInfo[] properties = model.GetType().GetProperties();
            foreach (PropertyInfo pInfo in properties)
            {
                if (excludeProperties == null || !excludeProperties.Contains(pInfo.Name))
                {
                    if (pInfo.CanWrite)
                    {
                        Object val;
                        switch (seed % 3)
                        {
                            case 0:
                                val =GetValue1(pInfo.PropertyType, false);
                                break;

                            case 1:
                                val =GetValue2(pInfo.PropertyType, false);
                                break;

                            case 2:
                                 val =GetValue3(pInfo.PropertyType, false);
                                break;

                            default: continue;
                        }
                            
                            
                        pInfo.SetValue(model, val, null);
                    }
                }
            }
        }


        /// <summary>
        /// Returns the value of the property at the given path for the given item.
        /// </summary>
        public static Object SetItemPropertyValue1(Object item, String propertyPath)
        {
            Object setValue = null;

            if (propertyPath != null)
            {
                String[] pathParts = propertyPath.Split('.');

                //follow the path down to find the property value
                Object curItem = item;

                //cycle through down the path parts.
                foreach (String propertyName in pathParts)
                {
                    PropertyInfo property = curItem.GetType().GetProperty(propertyName);
                    if (property == null) throw new ArgumentOutOfRangeException();

                    if (propertyName == pathParts.Last())
                    {
                        if (property.GetSetMethod() == null) return null;
                        setValue = GetValue1(property.PropertyType, true);
                        property.SetValue(curItem, setValue, null);
                    }
                    else
                    {
                        curItem = property.GetValue(curItem, null);
                    }
                }
            }

            return setValue;
        }

        


        #endregion

        #region AssortmentMinorRevision

        public static List<AssortmentMinorRevisionDto> InsertAssortmentMinorRevisionDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId)
        {
            List<AssortmentMinorRevisionDto> dtoList = new List<AssortmentMinorRevisionDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IAssortmentMinorRevisionDal dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    for (Int16 i = 1; i <= numToInsert; i++)
                    {
                        dtoList.Add(
                        new AssortmentMinorRevisionDto()
                        {
                            Id = i,
                            EntityId = entityId,
                            Name = "name" + i.ToString(),
                            ConsumerDecisionTreeId = i,
                            ProductGroupId = i,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now,
                            RowVersion = new RowVersion(),
                            UniqueContentReference = Guid.NewGuid(),
                        });
                    }

                    // insert into the dal
                    foreach (AssortmentMinorRevisionDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        #region Assortment

        public static List<AssortmentDto> InsertAssortmentDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId, Int32 productGroupId)
        {
            var returnList = new List<AssortmentDto>();

            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IAssortmentDal>())
            {
                for (Int32 i = 1; i <= numToInsert; i++)
                {
                    var assortmentDto = new AssortmentDto()
                    {
                        EntityId = entityId,
                        Id = i,
                        Name = String.Format("name{0}", i),
                        ProductGroupId = productGroupId,
                        UniqueContentReference = Guid.NewGuid()
                    };
                    returnList.Add(assortmentDto);
                    dal.Insert(assortmentDto);
                }
            }

            return returnList;
        }

        public static List<AssortmentRegionDto> InsertAssortmentRegionDtos(IDalFactory dalFactory, Int32 assortmentId, Int32 numToInsert)
        {
            var returnList = new List<AssortmentRegionDto>();

            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IAssortmentRegionDal>())
            {
                for (int i = 1; i <= numToInsert; i++)
                {
                    var dto = new AssortmentRegionDto
                    {
                        AssortmentId = assortmentId,
                        Id = i,
                        Name = String.Format("Name{0}", i)
                    };
                    returnList.Add(dto);
                    dal.Insert(dto);
                }
            }

            return returnList;
        }

        public static List<AssortmentProductDto> InsertAssortmentProductDtos(IDalFactory dalFactory, Int32 assortmentId, Int32 productId, Int32 numToInsert)
        {
            var returnList = new List<AssortmentProductDto>();

            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IAssortmentProductDal>())
            {
                for (int i = 1; i <= numToInsert; i++)
                {
                    var dto = new AssortmentProductDto
                    {
                        AssortmentId = assortmentId,
                        Id = i,
                        Name = String.Format("Name{0}", i),
                        ProductId = productId
                    };
                    returnList.Add(dto);
                    dal.Insert(dto);
                }
            }

            return returnList;
        }

        public static List<AssortmentProductDto> InsertAssortmentProductDtos(IDalFactory dalFactory,
                                                                             IEnumerable<ProductDto> productDtos,
                                                                             Int32 assortmentId)
        {
            var returnList = new List<AssortmentProductDto>();
            var id = 1;

            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IAssortmentProductDal>())
            {
                IEnumerable<AssortmentProductDto> dtos =
                    productDtos.Select(dto =>
                                      new AssortmentProductDto
                                      {
                                          AssortmentId = assortmentId,
                                          Id = id++,
                                          Name = dto.Name,
                                          ProductId = dto.Id,
                                          GTIN = dto.Gtin
                                      });
                foreach (AssortmentProductDto dto in dtos)
                {
                    returnList.Add(dto);
                    dal.Insert(dto);
                }
            }

            return returnList;
        }

        public static List<AssortmentRegionLocationDto> InsertAssortmentRegionLocationDtos(
            IDalFactory dalFactory, Int32 regionId, IEnumerable<Tuple<Int16, String>> locationIdCodes)
        {
            Int32 id = 0;
            var returnList = new List<AssortmentRegionLocationDto>();

            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IAssortmentRegionLocationDal>())
            {
                foreach (var locationIdCode in locationIdCodes)
                {
                    id++;
                    var dto = new AssortmentRegionLocationDto
                    {
                        Id = id,
                        LocationId = locationIdCode.Item1,
                        LocationCode = locationIdCode.Item2,
                        AssortmentRegionId = regionId
                    };
                    returnList.Add(dto);
                    dal.Insert(dto);
                }
            }

            return returnList;
        }

        public static List<AssortmentLocalProductDto> InsertAssortmentLocalProducts(
            IDalFactory dalFactory,
            IEnumerable<Tuple<Int16, String>> locationIdCodes,
            IEnumerable<Tuple<Int32, String>> productIdGtins,
            Int32 assortmentId)
        {
            Int32 id = 0;
            var returnList = new List<AssortmentLocalProductDto>();

            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IAssortmentLocalProductDal>())
            {
                foreach (var locationIdCode in locationIdCodes)
                {
                    foreach (var productIdGtin in productIdGtins)
                    {
                        id++;
                        var dto = new AssortmentLocalProductDto
                        {
                            LocationId = locationIdCode.Item1,
                            LocationCode = locationIdCode.Item2,
                            ProductId = productIdGtin.Item1,
                            ProductGTIN = productIdGtin.Item2,
                            Id = id,
                            AssortmentId = assortmentId
                        };
                        returnList.Add(dto);
                        dal.Insert(dto);
                    }
                }
            }

            return returnList;
        }

        #endregion

        #region Blob

        /// <summary>
        /// Inserts a dto into a data source
        /// </summary>
        /// <param name="dalFactory">The dal factory to use for the insertion</param>
        /// <returns>The list of inserted dtos</returns>
        public static BlobDto InsertBlobDto(IDalContext context)
        {
            using (IBlobDal dal = context.GetDal<IBlobDal>())
            {
                BlobDto dto = new BlobDto()
                {
                    Data = TestDataHelper.ConvertStringToBytes(Guid.NewGuid().ToString()),
                    DateCreated = DateTime.UtcNow,
                    DateLastModified = DateTime.UtcNow
                };
                dal.Insert(dto);
                return dto;
            }
        }

        #endregion

        #region Blocking Model Helpers

        internal static PlanogramBlockingGroup CreateBlockingGroup(Int32 numberOfItems)
        {
            return CreateBlockingGroup(numberOfItems, Guid.NewGuid().ToString());
        }

        internal static PlanogramBlockingGroup CreateBlockingGroup(Int32 numberOfItems, String name)
        {
            return CreateBlockingGroup(numberOfItems, name, true, false);
        }

        internal static PlanogramBlockingGroup CreateBlockingGroup(Int32 numberOfItems, String name, Boolean canMerge, Boolean isRestrictedByComponentType)
        {
            var group = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
            group.Name = name;
            group.CanMerge = canMerge;
            group.IsRestrictedByComponentType = isRestrictedByComponentType;
            return group;
        }

        internal static PlanogramBlockingGroup CreateBlockingGroup(IEnumerable<String> productGtins, PlanogramBlocking blocking)
        {
            Planogram plan = blocking.Parent;

            var group = PlanogramBlockingGroup.NewPlanogramBlockingGroup(Guid.NewGuid().ToString(), BlockingHelper.GetNextBlockingGroupColour(blocking));
            blocking.Groups.Add(group);

            if (plan != null && plan.Sequence != null && productGtins.Any())
            {
                var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = group.Colour;
                plan.Sequence.Groups.Add(sequenceGroup);
                foreach (String gtin in productGtins)
                {
                    var sequenceGroupProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
                    sequenceGroupProduct.Gtin = gtin;
                    sequenceGroup.Products.Add(sequenceGroupProduct);
                }
            }
            return group;
        }

        internal static PlanogramFixtureComponent CreateFixtureComponent(Single x, Single y, Single width, Single height, PlanogramComponentType type,
            PlanogramFixtureComponentList fixtureComponentList, PlanogramComponentList componentList)
        {
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(CreateComponent(width, height, type, componentList));
            fixtureComponent.X = x;
            fixtureComponent.Y = y;
            fixtureComponentList.Add(fixtureComponent);
            return fixtureComponent;
        }

        internal static PlanogramComponent CreateComponent(Single width, Single height, PlanogramComponentType type, PlanogramComponentList componentList)
        {
            var component = PlanogramComponent.NewPlanogramComponent(String.Empty, type, width, height, width);
            componentList.Add(component);
            return component;
        }

        internal static PlanogramBlockingLocation CreateBlockingLocation(
            PlanogramBlockingDivider left,
            PlanogramBlockingDivider right,
            PlanogramBlockingDivider top,
            PlanogramBlockingDivider bottom,
            PlanogramBlockingLocationList list)
        {
            var location = PlanogramBlockingLocation.NewPlanogramBlockingLocation();
            location.PlanogramBlockingDividerLeftId = left == null ? null : left.Id;
            location.PlanogramBlockingDividerRightId = right == null ? null : right.Id;
            location.PlanogramBlockingDividerTopId = top == null ? null : top.Id;
            location.PlanogramBlockingDividerBottomId = bottom == null ? null : bottom.Id;
            list.Add(location);
            return location;
        }

        internal static PlanogramBlockingLocation CreateBlockingLocation(
            PlanogramBlockingDivider left,
            PlanogramBlockingDivider right,
            PlanogramBlockingDivider top,
            PlanogramBlockingDivider bottom,
            PlanogramBlockingLocationList list,
            PlanogramBlockingGroup group)
        {
            var location = PlanogramBlockingLocation.NewPlanogramBlockingLocation(group);
            list.Add(location);
            location.PlanogramBlockingDividerLeftId = left == null ? null : left.Id;
            location.PlanogramBlockingDividerRightId = right == null ? null : right.Id;
            location.PlanogramBlockingDividerTopId = top == null ? null : top.Id;
            location.PlanogramBlockingDividerBottomId = bottom == null ? null : bottom.Id;
            return location;
        }

        internal static PlanogramBlockingDivider CreateBlockingDivider(Single x, Single y, Single length, PlanogramBlockingDividerType type, Byte level, PlanogramBlockingDividerList list)
        {
            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(level, type, x, y, length);
            divider.IsSnapped = true;
            list.Add(divider);
            return divider;
        }

        internal static PlanogramProduct CreatePlanogramProduct(Planogram planogram)
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Gtin = Guid.NewGuid().ToString();
            planogram.Products.Add(product);
            return product;
        }

        #endregion

        #region ClusterSchemes

        /// <summary>
        /// Inserts cluster scheme data into the db
        /// </summary>
        /// <param name="dalFactory"></param>
        public static ClusterSchemeDto InsertSingeClusterSchemeData(IDalFactory dalFactory, IEnumerable<LocationDto> locationsEnumerator)
        {
            List<LocationDto> locations = locationsEnumerator.ToList();

            Int32 schemeId = 0;

            #region Cluster Schemes
            List<ClusterSchemeDto> dtoList = new List<ClusterSchemeDto>();

            dtoList.Add(new ClusterSchemeDto()
            {
                Name = "Scheme 1",
                IsDefault = true,
                EntityId = 1
            });

            // insert into the data source
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    foreach (ClusterSchemeDto dto in dtoList)
                    {
                        dal.Insert(dto);
                        schemeId = dto.Id;
                    }
                }
                dalContext.Commit();
            }
            #endregion

            //create a cluster per location
            List<ClusterDto> clusterDtoList = new List<ClusterDto>();
            List<ClusterLocationDto> clusterLocationDtoList = new List<ClusterLocationDto>();

            for (int i = 0; i < locations.Count; i++)
            {

                clusterDtoList.Add(new ClusterDto()
                {
                    Name = String.Format("Sublevel {0}", i + 1),
                    ClusterSchemeId = schemeId
                });

                clusterLocationDtoList.Add(new ClusterLocationDto()
                {
                    LocationId = locations[i].Id,
                    ClusterId = i + 1
                });
            }


            // insert into the data source
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    foreach (ClusterDto dto in clusterDtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    foreach (ClusterLocationDto dto in clusterLocationDtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList[0];
        }

        /// <summary>
        /// Inserts cluster data into the db
        /// </summary>
        /// <param name="dalFactory"></param>
        public static ClusterDto InsertSingeClusterData(IDalFactory dalFactory, IEnumerable<LocationDto> locationsEnumerator)
        {

            List<LocationDto> locations = locationsEnumerator.ToList();

            //create a cluster and assign all locations to it
            List<ClusterLocationDto> clusterLocationDtoList = new List<ClusterLocationDto>();

            //Create cluster scheme
            ClusterSchemeDto clusterSchemeDto = new ClusterSchemeDto()
            {
                EntityId = 1,
                IsDefault = false,
                Name = Guid.NewGuid().ToString()
            };

            //Create cluster
            ClusterDto clusterDto = new ClusterDto()
            {
                Name = Guid.NewGuid().ToString(),
                ClusterSchemeId = 1
            };

            //create cluster locations
            for (int i = 0; i < locations.Count; i++)
            {
                clusterLocationDtoList.Add(new ClusterLocationDto()
                {
                    LocationId = locations[i].Id,
                    ClusterId = 1
                });
            }

            //insert into the data source
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Insert cluster scheme
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dal.Insert(clusterSchemeDto);
                }

                //Insert cluster
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    dal.Insert(clusterDto);
                }

                //Insert cluster locations
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    foreach (ClusterLocationDto dto in clusterLocationDtoList)
                    {
                        dal.Insert(dto);
                    }
                }

                dalContext.Commit();
            }

            return clusterDto;
        }

        public static List<ClusterSchemeDto> InsertClusterSchemeDtos(IDalFactory dalFactory, Int32 entityId, Int32 numToInsert)
        {
            List<ClusterSchemeDto> dtoList = new List<ClusterSchemeDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    Int32 existingCount = dal.FetchByEntityId(entityId).Count();

                    for (Int32 i = existingCount; i < (numToInsert + existingCount); i++)
                    {
                        ClusterSchemeDto dto = new ClusterSchemeDto()
                        {
                            EntityId = entityId,
                            UniqueContentReference = Guid.NewGuid(),
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now,
                            IsDefault = false,
                            Name = "scheme" + i
                        };
                        dal.Insert(dto);
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<ClusterDto> InsertClusterDtos(IDalFactory dalFactory, IEnumerable<ClusterSchemeDto> schemeDtos, Int32 numPerScheme)
        {
            List<ClusterDto> dtoList = new List<ClusterDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    foreach (ClusterSchemeDto scheme in schemeDtos)
                    {
                        for (Int32 i = 0; i < numPerScheme; i++)
                        {
                            ClusterDto dto = new ClusterDto()
                            {
                                Name = "cluster" + i,
                                ClusterSchemeId = scheme.Id
                            };
                            dal.Insert(dto);
                            dtoList.Add(dto);
                        }
                    }
                }
            }

            return dtoList;
        }

        public static List<ClusterLocationDto> InsertClusterLocationDtos(IDalFactory dalFactory, IEnumerable<ClusterDto> clusterDtos, IEnumerable<LocationDto> locationDtos)
        {
            List<ClusterLocationDto> dtoList = new List<ClusterLocationDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    foreach (var clusterGroup in clusterDtos.GroupBy(c => c.ClusterSchemeId))
                    {
                        Int32 locsPerCluster = locationDtos.Count() / clusterGroup.Count();
                        List<LocationDto> remainingLocs = locationDtos.ToList();

                        foreach (ClusterDto cluster in clusterGroup)
                        {
                            Int32 locCount = 0;
                            while (locCount != locsPerCluster)
                            {
                                LocationDto loc = remainingLocs.First();

                                ClusterLocationDto dto = new ClusterLocationDto()
                                {
                                    LocationId = loc.Id,
                                    Address1 = loc.Address1,
                                    Address2 = loc.Address2,
                                    City = loc.City,
                                    ClusterId = cluster.Id,
                                    Code = loc.Code,
                                    Country = loc.Country,
                                    County = loc.County,
                                    Name = loc.Name,
                                    PostalCode = loc.PostalCode,
                                    Region = loc.Region,
                                    TVRegion = loc.TVRegion
                                };
                                dal.Insert(dto);
                                dtoList.Add(dto);
                                remainingLocs.Remove(loc);

                                locCount++;
                            }

                        }
                    }
                }
            }


            return dtoList;
        }


        #endregion

        #region Consumer Decision Tree

        /// <summary>
        /// Inserts product family dtos for the given entities
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="entityDtoList"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<ConsumerDecisionTreeDto> InsertConsumerDecisionTreeDtos(IDalFactory dalFactory, Int32 entityId, IEnumerable<ProductGroupDto> productGroupDtoList, Int32 numToInsert)
        {
            List<ConsumerDecisionTreeDto> returnDtoList = new List<ConsumerDecisionTreeDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    foreach (ProductGroupDto productGroupDto in productGroupDtoList)
                    {
                        for (Int32 i = 0; i < numToInsert; i++)
                        {
                            ConsumerDecisionTreeDto cdtDto = new ConsumerDecisionTreeDto();
                            SetPropertyValues(cdtDto);
                            cdtDto.UniqueContentReference = Guid.NewGuid();
                            cdtDto.EntityId = entityId;
                            cdtDto.ProductGroupId = productGroupDto.Id;

                            dal.Insert(cdtDto);
                            returnDtoList.Add(cdtDto);
                        }
                    }
                }
            }
            return returnDtoList;
        }

        /// <summary>
        /// Inserts product family dtos for the given entities
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="entityDtoList"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<ConsumerDecisionTreeDto> InsertConsumerDecisionTreeDtos
            (IDalFactory dalFactory, Int32 entityId, Int32 numToInsert)
        {
            List<ConsumerDecisionTreeDto> returnDtoList = new List<ConsumerDecisionTreeDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //get the number of existing dtos
                Int32 existingCount = 0;
                using (IConsumerDecisionTreeInfoDal dal = dalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                {
                    existingCount = dal.FetchByEntityId(entityId).Count();
                }

                //insert
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    for (Int32 i = existingCount; i < (numToInsert + existingCount); i++)
                    {
                        ConsumerDecisionTreeDto cdtDto = new ConsumerDecisionTreeDto()
                        {
                            EntityId = entityId,
                            UniqueContentReference = Guid.NewGuid(),
                            Name = "name" + i,
                            ProductGroupId = null,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        };

                        dal.Insert(cdtDto);
                        returnDtoList.Add(cdtDto);
                    }

                }
            }
            return returnDtoList;
        }

        /// <summary>
        /// Inserts product family dtos for the given entities
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="entityDtoList"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<ConsumerDecisionTreeDto> InsertConsumerDecisionTreeDtos
            (IDalFactory dalFactory, Int32 entityId, IEnumerable<Int32> productGroupIds)
        {
            List<ConsumerDecisionTreeDto> returnDtoList = new List<ConsumerDecisionTreeDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //insert
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    Int32 cdtNumber = 1;

                    foreach (Int32 productGroupId in productGroupIds)
                    {
                        ConsumerDecisionTreeDto cdtDto = new ConsumerDecisionTreeDto()
                        {
                            EntityId = entityId,
                            UniqueContentReference = Guid.NewGuid(),
                            Name = "name" + cdtNumber,
                            ProductGroupId = productGroupId,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        };

                        dal.Insert(cdtDto);
                        returnDtoList.Add(cdtDto);

                        cdtNumber++;
                    }
                }
            }
            return returnDtoList;
        }

        /// <summary>
        /// Inserts levels against the given cdt id
        /// </summary>
        public static List<ConsumerDecisionTreeLevelDto> InsertConsumerDecisionTreeLevelDtos
            (IDalFactory dalFactory, ConsumerDecisionTreeDto cdtDto, Int32 levelCount, Boolean addRootLevel)
        {
            List<ConsumerDecisionTreeLevelDto> dtoList = new List<ConsumerDecisionTreeLevelDto>();

            ConsumerDecisionTreeLevelDto rootDto = null;

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IConsumerDecisionTreeLevelDal dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
                {
                    if (addRootLevel)
                    {
                        //add a rootLevel
                        rootDto = new ConsumerDecisionTreeLevelDto();
                        SetPropertyValues(rootDto, false);
                        rootDto.ConsumerDecisionTreeId = cdtDto.Id;
                        rootDto.ParentLevelId = null;
                        dal.Insert(rootDto);

                    }

                    //cycle through levels
                    for (int i = 1; i <= levelCount; i++)
                    {
                        ConsumerDecisionTreeLevelDto levelDto = new ConsumerDecisionTreeLevelDto();
                        SetPropertyValues(levelDto, false);
                        levelDto.Name = String.Format("level {0}", i);
                        levelDto.ConsumerDecisionTreeId = cdtDto.Id;

                        dtoList.Add(levelDto);
                    }

                    Int32? lastId = null;
                    if (addRootLevel)
                    {
                        lastId = rootDto.Id;
                    }

                    //Insert the child levels
                    foreach (ConsumerDecisionTreeLevelDto dto in dtoList)
                    {
                        dto.ParentLevelId = lastId;
                        dal.Insert(dto);
                        lastId = dto.Id;
                    }

                    if (addRootLevel)
                    {
                        dtoList.Insert(0, rootDto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        /// <summary>
        /// Inserts nodes against the given cdt id
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="cdtId"></param>
        /// <param name="levelCount"></param>
        /// <param name="numPerLevel"></param>
        /// <returns></returns>
        public static List<ConsumerDecisionTreeNodeDto> InsertConsumerDecisionTreeNodeDtos
            (IDalFactory dalFactory, ConsumerDecisionTreeDto cdtDto, List<ConsumerDecisionTreeLevelDto> levelDtos, Int32 numPerLevel, Boolean addRootNode)
        {
            List<ConsumerDecisionTreeNodeDto> dtoList = new List<ConsumerDecisionTreeNodeDto>();

            ConsumerDecisionTreeNodeDto rootDto = null;

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                {
                    if (addRootNode)
                    {
                        //add a rootnode
                        rootDto = new ConsumerDecisionTreeNodeDto();
                        SetPropertyValues(rootDto, false);
                        rootDto.ConsumerDecisionTreeId = cdtDto.Id;
                        rootDto.ParentNodeId = null;

                        ConsumerDecisionTreeLevelDto rootLevelDto = levelDtos.FirstOrDefault();
                        levelDtos.Remove(rootLevelDto);

                        if (rootLevelDto != null)
                        {
                            rootDto.ConsumerDecisionTreeLevelId = rootLevelDto.Id;
                        }

                        dal.Insert(rootDto);
                        dtoList.Add(rootDto);
                    }

                    foreach (ConsumerDecisionTreeLevelDto levelDto in levelDtos)
                    {
                        for (Int32 i = 1; i <= numPerLevel; i++)
                        {
                            ConsumerDecisionTreeNodeDto nodeDto = new ConsumerDecisionTreeNodeDto();
                            SetPropertyValues(nodeDto, false);
                            nodeDto.ConsumerDecisionTreeId = cdtDto.Id;
                            nodeDto.ConsumerDecisionTreeLevelId = levelDto.Id;
                            nodeDto.Name = String.Format("NodeDtoName{0}", i);

                            dtoList.Add(nodeDto);
                        }
                    }

                    IEnumerable<IGrouping<Int32, ConsumerDecisionTreeNodeDto>> levelNodeGroups =
                        dtoList.GroupBy(p => p.ConsumerDecisionTreeLevelId);

                    //Create the parent-child links
                    IGrouping<Int32, ConsumerDecisionTreeNodeDto> previousLevelGroup = null;
                    foreach (IGrouping<Int32, ConsumerDecisionTreeNodeDto> levelGroup in levelNodeGroups)
                    {
                        if (previousLevelGroup != null)
                        {
                            //If the root was add and this is the second level assign all nodes to the single root
                            if (addRootNode &&
                                levelGroup.Key == levelNodeGroups.First().Key + 1)
                            {
                                foreach (ConsumerDecisionTreeNodeDto dto in levelGroup)
                                {
                                    dto.ParentNodeId = rootDto.Id;
                                    dal.Insert(dto);
                                }
                            }
                            else
                            {
                                for (Int32 i = 0; i < numPerLevel; i++)
                                {
                                    ConsumerDecisionTreeNodeDto dto = levelGroup.ElementAtOrDefault(i);
                                    ConsumerDecisionTreeNodeDto previousDto = previousLevelGroup.ElementAtOrDefault(i);

                                    //Add the parent node id and insert
                                    if (dto != null && previousDto != null)
                                    {
                                        dto.ParentNodeId = previousDto.Id;
                                        dal.Insert(dto);
                                    }

                                }
                            }
                        }
                        else if (!addRootNode)
                        {
                            for (Int32 i = 0; i < numPerLevel; i++)
                            {
                                ConsumerDecisionTreeNodeDto dto = levelGroup.ElementAtOrDefault(i);

                                //Add the parent node id and insert
                                if (dto != null)
                                {
                                    dto.ParentNodeId = null;
                                    dal.Insert(dto);
                                }
                            }
                        }
                        previousLevelGroup = levelGroup;
                    }
                }
                dalContext.Commit();
            }

            //hook in the root node
            if (addRootNode)
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                    {
                        cdtDto.ProductGroupId = rootDto.Id;
                        dal.Update(cdtDto);
                    }
                    dalContext.Commit();
                }
            }

            return dtoList;
        }

        /// <summary>
        /// Inserts products for the given product families
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="cdtNodes"></param>
        /// <param name="products"></param>
        /// <param name="numPerFamily"></param>
        /// <returns></returns>
        public static List<ConsumerDecisionTreeNodeProductDto> InsertConsumerDecisionTreeNodeProductDtos
            (IDalFactory dalFactory, List<ConsumerDecisionTreeNodeDto> cdtNodes, List<ProductDto> products, Int32 numPerNode)
        {

            List<ConsumerDecisionTreeNodeProductDto> dtoList = new List<ConsumerDecisionTreeNodeProductDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                {
                    foreach (ConsumerDecisionTreeNodeDto nodeDto in cdtNodes)
                    {
                        List<ProductDto> availableProducts = products.ToList();

                        for (Int32 i = 0; i < numPerNode; i++)
                        {
                            if (availableProducts.Count != 0)
                            {
                                //get a product
                                ProductDto product = availableProducts[availableProducts.Count - 1];

                                //create dto
                                ConsumerDecisionTreeNodeProductDto familyProductDto = new ConsumerDecisionTreeNodeProductDto()
                                {
                                    ConsumerDecisionTreeNodeId = nodeDto.Id,
                                    ProductId = product.Id,
                                    DateCreated = DateTime.Now,
                                    DateLastModified = DateTime.Now
                                };
                                dal.Insert(familyProductDto);
                                dtoList.Add(familyProductDto);

                                //remove the product from the available list
                                availableProducts.Remove(product);
                            }
                        }
                    }
                }
            }
            return dtoList;
        }

        #endregion

        #region ContentLookup

        /// <summary>
        /// Helper method to insert a specified number of ContentLookup Dtos
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="numToInsert"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static List<ContentLookupDto> InsertContentLookupDtos(
            IDalFactory dalFactory, Int32 entityId)
        {
            List<ContentLookupDto> dtoList = new List<ContentLookupDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    Int32 existingContentlookupCount = dal.FetchByEntityId(entityId).Count();

                    for (Int32 i = 0; i < 10; i++)
                    {
                        ContentLookupDto dto = new ContentLookupDto()
                        {
                            AssortmentName = "AssortmentName " + (i + existingContentlookupCount),
                            BlockingName = "BlockingName " + (i + existingContentlookupCount),
                            ClusterName = "ClusterName " + (i + existingContentlookupCount),
                            ClusterSchemeName = "ClusterSchemeName " + (i + existingContentlookupCount),
                            ConsumerDecisionTreeName = "CDT " + (i + existingContentlookupCount),
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now,
                            EntityId = entityId,
                            Id = (i + existingContentlookupCount),
                            InventoryProfileName = "Inventory Profile Name " + (i + existingContentlookupCount),
                            MetricProfileName = "Metric Profile Name " + (i + existingContentlookupCount),
                            AssortmentMinorRevisionName = "Minor Assortment Name " + (i + existingContentlookupCount),
                            PerformanceSelectionName = "Performance Selection Name " + (i + existingContentlookupCount),
                            PlanogramName = "Planogram Name " + (i + existingContentlookupCount),
                            ProductUniverseName = "Product Universe Name " + (i + existingContentlookupCount),
                            SequenceName = "Sequence Name " + (i + existingContentlookupCount),
                            UserId = (i + existingContentlookupCount)
                        };

                        dtoList.Add(dto);
                        dal.Insert(dto);

                    }
                }

                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        #region Entity

        /// <summary>
        /// Inserts a number of additional entities into the db.
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<EntityDto> InsertEntityDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<EntityDto> dtoList = new List<EntityDto>();
            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                dtoList = TestDataHelper.InsertEntityDtos(dalContext, numToInsert);

                dalContext.Commit();
            }

            return dtoList;
        }

        /// <summary>
        /// Overriden method taking IDalContext as parameter
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<EntityDto> InsertEntityDtos(IDalContext dalContext, Int32 numToInsert)
        {
            List<EntityDto> dtoList = new List<EntityDto>();
            //insert entities
            Int32 existingEntityCount = 0;
            using (IEntityInfoDal dal = dalContext.GetDal<IEntityInfoDal>())
            {
                existingEntityCount = dal.FetchAll().Count();
            }

            using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
            {
                //add 5 dtos
                for (int i = existingEntityCount; i < (numToInsert + existingEntityCount); i++)
                {
                    dtoList.Add(
                    new EntityDto()
                    {
                        Name = "name" + i.ToString(),
                        Description = "description" + i.ToString(),
                        DateCreated = DateTime.UtcNow,
                        DateLastModified = DateTime.UtcNow
                    });
                }
                foreach (EntityDto dto in dtoList)
                {
                    dal.Insert(dto);
                }
            }
            return dtoList;
        }

        public static void InsertEntityDtos(IDalFactory dalFactory, List<EntityDto> entitiesToInsert)
        {
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IEntityDal>())
                {
                    foreach (EntityDto dto in entitiesToInsert)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }
        }

        public static Int32 InsertDefaultTestEntity(IDalFactory dalFactory)
        {
            Int32 entityId;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                EntityDto dto =
                    new EntityDto()
                    {
                        Name = "Default",
                        GFSId=1
                    };

                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    dal.Insert(dto);
                    entityId = dto.Id;
                }

                #region Product Hierarchy

                //ProductHierarcy
                ProductHierarchy defaultHierarchy = ProductHierarchy.NewProductHierarchy(dto.Id);
                defaultHierarchy.Name = String.Format(Galleria.Ccm.Resources.Language.Message.ProductHierarchy_DefaultName, dto.Name);
                defaultHierarchy.ConstructDefaultHierarchy();
                defaultHierarchy = defaultHierarchy.Save();

                #endregion

                #region Planogram Hierarchy

                //PlanogramHierarchy
                PlanogramHierarchy planHierarchy = PlanogramHierarchy.NewPlanogramHierarchy(dto.Id);
                planHierarchy.Name = Galleria.Ccm.Resources.Language.Message.Entity_PlanogramHierarchyDefaultName;
                planHierarchy.RootGroup.Name = Galleria.Ccm.Resources.Language.Message.Entity_PlanogramHierarchyDefaultRootName;
                planHierarchy = planHierarchy.Save();

                #endregion

                #region Location Hierarchy

                //LocationHierarchy
                LocationHierarchy defaultLocHierarchy = LocationHierarchy.NewLocationHierarchy(dto.Id);
                defaultLocHierarchy.Name = Galleria.Ccm.Resources.Language.Message.Entity_LocationHierarchyDefaultName;
                defaultLocHierarchy.ConstructDefaultHierarchy();
                defaultLocHierarchy = defaultLocHierarchy.Save();

                #endregion
            }
            return entityId;
        }

        #endregion

        #region EventLog

        public static List<EventLogDto> InsertEventLogDtos
            (IDalContext dalContext, Int32 numToInsert, List<EntityDto> entityDtoList, List<EventLogNameDto> eventLogNameDtoList)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
            {
                foreach (EntityDto entityDto in entityDtoList)
                {
                    foreach (EventLogNameDto eventLogNameDto in eventLogNameDtoList)
                    {
                        for (int i = 0; i < numToInsert; i++)
                        {
                            var dto = new EventLogDto();
                            SetPropertyValues(dto, false);
                            dto.EventLogNameId = eventLogNameDto.Id;
                            dto.EventLogName = eventLogNameDto.Name;
                            dto.EntityId = entityDto.Id;
                            dto.EntityName = entityDto.Name;
                            dto.DateTime = DateTime.UtcNow;
                            dto.UserName = null;
                            dal.Insert(dto);
                            dtoList.Add(dto);
                        }
                    }
                };
                return dtoList;
            }
        }

        public static List<EventLogDto> InsertEventLogDtos
            (IDalContext dalContext, Int32 numToInsert, List<EntityDto> entityDtoList, List<EventLogNameDto> eventLogNameDtoList,
            List<WorkpackageDto> workpackageDtoList)
        {
            List<EventLogDto> dtoList = new List<EventLogDto>();
            using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
            {
                foreach (EntityDto entityDto in entityDtoList)
                {
                    foreach (EventLogNameDto eventLogNameDto in eventLogNameDtoList)
                    {
                        foreach (WorkpackageDto workpackageDto in workpackageDtoList)
                        {
                            for (int i = 0; i < numToInsert; i++)
                            {
                                var dto = new EventLogDto();
                                SetPropertyValues(dto, false);
                                dto.EventLogNameId = eventLogNameDto.Id;
                                dto.EventLogName = eventLogNameDto.Name;
                                dto.EntityId = entityDto.Id;
                                dto.EntityName = entityDto.Name;
                                dto.DateTime = DateTime.UtcNow;
                                dto.UserName = null;
                                dto.WorkpackageId = workpackageDto.Id;
                                dto.WorkpackageName = workpackageDto.Name;
                                dal.Insert(dto);
                                dtoList.Add(dto);
                            }
                        }
                    }
                };
                return dtoList;
            }
        }

        public static List<EventLogNameDto> InsertEventLogNameDtos(IDalContext dalContext, Int32 numToInsert)
        {
            List<EventLogNameDto> dtoList = new List<EventLogNameDto>();
            using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
            {
                for (int i = 0; i < numToInsert; i++)
                {
                    var dto = new EventLogNameDto();
                    SetPropertyValues(dto, false);
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
                return dtoList;
            }
        }

        #endregion

        #region Metric

        public static List<MetricDto> InsertMetricDtos(IDalFactory dalFactory, Int32 entityId, Int32 numToInsert)
        {
            List<MetricDto> dtoList = new List<MetricDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (IMetricDal dal = dalContext.GetDal<IMetricDal>())
                {
                    for (Int32 i = 0; i < numToInsert; i++)
                    {
                        MetricDto dto = new MetricDto()
                        {
                            EntityId = entityId,
                            Name = "Metric" + i,
                            SpecialType = 1,
                            DataModelType = 2,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow,
                            DateDeleted = null,
                            Description = "MetricDesc" + i,
                            Direction = 1,
                        };

                        dal.Insert(dto);
                        dtoList.Add(dto);
                    }
                }

                dalContext.Commit();
            }


            return dtoList;
        }

        #endregion

        #region File

        /// <summary>
        /// Inserts a file dto into the data source
        /// </summary>
        /// <param name="dalFactory">The dal factory used to perform the insert</param>
        /// <returns>A list of inserted files</returns>
        public static FileDto InsertFileDto(IDalContext dalContext, Int32 entityId)
        {
            using (IFileDal dal = dalContext.GetDal<IFileDal>())
            {
                FileDto dto = new FileDto()
                {
                    EntityId = entityId,
                    Name = Guid.NewGuid().ToString(),
                    SourceFilePath = Guid.NewGuid().ToString(),
                    SizeInBytes = 1,
                    BlobId = InsertBlobDto(dalContext).Id,
                    UserId = null,
                    DateCreated = DateTime.UtcNow,
                    DateLastModified = DateTime.UtcNow
                };
                dal.Insert(dto);
                return dto;
            }
        }

        #endregion

        #region InventoryProfile
        /// <summary>
        /// insert a number of inventoery profile into the db
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="entityId"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<InventoryProfileDto> InsertInventoryProfileDtos(IDalFactory dalFactory, Int32 entityId, Int32 numToInsert)
        {
            List<InventoryProfileDto> dtoList = new List<InventoryProfileDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //dalContext.Begin();

                Int32 existingCount = 0;
                using (IInventoryProfileInfoDal dal = dalContext.GetDal<IInventoryProfileInfoDal>())
                {
                    existingCount = dal.FetchByEntityId(entityId).Count();
                }

                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    for (Int32 i = existingCount; i < (numToInsert + existingCount); i++)
                    {
                        InventoryProfileDto invProfiledto = new InventoryProfileDto()
                        {
                            EntityId = entityId,
                            Name = "InventoryProfile" + i,
                            CasePack = 1,
                            DaysOfSupply = 1,
                            ShelfLife = 1,
                            ReplenishmentDays = 1,
                            WasteHurdleUnits = 1,
                            WasteHurdleCasePack = 1,
                            MinUnits = 1,
                            MinFacings = 1,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow,
                            DateDeleted = null,
                        };
                        dal.Insert(invProfiledto);
                        dtoList.Add(invProfiledto);

                    }
                }
                // dalContext.Commit();
            }
            return dtoList;
        }
        #endregion

        #region Label

        public static IEnumerable<LabelDto> InsertLabelDtos(IDalFactory dalFactory, Int32 amountOfLabels, Int32 entityId)
        {
            List<LabelDto> dtoList = new List<LabelDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (var dal = dalContext.GetDal<ILabelDal>())
                {
                    for (var i = 0; i < amountOfLabels; i++)
                    {
                        dtoList.Add(new LabelDto()
                        {
                            DateCreated = DateTime.Now,
                            DateDeleted = null,
                            DateLastModified = DateTime.Now,
                            EntityId = entityId,
                            Id = i,
                            Name = String.Format("label{0}", i)
                        });
                    }

                    foreach (var dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }
            return dtoList;
        }

        #endregion

        #region Location

        /// <summary>
        /// Helper method to insert a specified number of Location Dtos
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="numToInsert"></param>
        /// <param name="entityId"></param>
        /// <remarks>Now assumes that a location hierarchy has been populated</remarks>
        public static List<LocationDto> InsertLocationDtos(IDalFactory dalFactory, Int32 numToInsertPerGroup, Int32 entityId)
        {
            IEnumerable<LocationGroupDto> locationGroupDtos = null;
            LocationHierarchyDto hierarchyDto = null;

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    //Fetch hierarchy
                    try
                    {
                        hierarchyDto = dal.FetchByEntityId(entityId);
                    }
                    catch (DtoDoesNotExistException) { }
                }

                if (hierarchyDto == null)
                {
                    List<EntityDto> entityDtos = new List<EntityDto>();
                    using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                    {
                        EntityDto entityDto = null;

                        Int32 intId = 1;
                        while (entityDto == null)
                        {
                            try
                            {
                                entityDto = dal.FetchById(entityId);
                            }
                            catch (DtoDoesNotExistException)
                            {
                                dal.Insert(new EntityDto()
                                {
                                    Name = "entity" + intId.ToString(),
                                    DateCreated = DateTime.UtcNow,
                                    DateLastModified = DateTime.UtcNow,
                                });
                                intId++;
                            }

                        }


                        entityDtos.Add(entityDto);
                    }

                    hierarchyDto = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos)[0];
                    List<LocationLevelDto> locLevelDtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, hierarchyDto.Id, 3);
                    TestDataHelper.InsertLocationGroupDtos(dalFactory, locLevelDtos, 3);
                }


                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    //Fetch location groups
                    locationGroupDtos = dal.FetchByLocationHierarchyId(hierarchyDto.Id);
                }

                dalContext.Commit();
            }

            return InsertLocationDtos(dalFactory, entityId, locationGroupDtos, numToInsertPerGroup);
        }

        public static List<LocationDto> InsertLocationDtos(
            IDalFactory dalFactory, Int32 entityId,
            IEnumerable<LocationGroupDto> groupDtos,
            Int32 numToInsertPerGroup)
        {
            return InsertLocationDtos(dalFactory, entityId,
                groupDtos.Select(g => g.Id),
                numToInsertPerGroup);
        }

        /// <summary>
        /// Helper method to insert a specified number of Location Dtos
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="numToInsert"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static List<LocationDto> InsertLocationDtos(
            IDalFactory dalFactory, Int32 entityId,
            IEnumerable<Int32> groupIdList,
            Int32 numToInsertPerGroup)
        {
            List<LocationDto> dtoList = new List<LocationDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    Int32 existingLocCount = dal.FetchByEntityId(entityId).Count();

                    foreach (Int32 groupId in groupIdList)
                    {
                        for (int i = 0; i < numToInsertPerGroup; i++)
                        {
                            LocationDto dto = new LocationDto()
                            {
                                Name = "locname" + (i + existingLocCount),
                                Code = "loccode" + (i + existingLocCount),
                                EntityId = entityId,
                                LocationGroupId = groupId,
                                Address1 = "address1" + (i + existingLocCount),
                                Address2 = "address2" + (i + existingLocCount),
                                AdvertisingZone = "advertzone" + (i + existingLocCount),
                                AverageOpeningHours = i,
                                CarParkManagement = "carparkmanagement" + (i + existingLocCount),
                                City = "city" + (i + existingLocCount),
                                Country = "country" + (i + existingLocCount),
                                County = "county" + (i + existingLocCount),
                                DateClosed = DateTime.Now,
                                DateCreated = DateTime.Now,
                                DateDeleted = null,
                                DateLastModified = DateTime.Now,
                                DateLastRefitted = DateTime.Now,
                                DateOpen = DateTime.Now,
                                DefaultClusterAttribute = "cluster" + (i + existingLocCount),
                                DistributionCentre = "dist" + (i + existingLocCount),
                                DivisionalManagerEmail = "divemail" + (i + existingLocCount),
                                DivisionalManagerName = "divname" + (i + existingLocCount),
                                FaxAreaCode = "faxarea" + (i + existingLocCount),
                                FaxCountryCode = "faxcountry" + (i + existingLocCount),
                                FaxNumber = "faxnum" + (i + existingLocCount),

                            };


                            dtoList.Add(dto);
                            dal.Insert(dto);
                        }

                        existingLocCount += numToInsertPerGroup;
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        /// <summary>
        /// Insert collection of product hierarchy dtos
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<LocationHierarchyDto> InsertLocationHierarchyDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId)
        {
            List<LocationHierarchyDto> dtoList = new List<LocationHierarchyDto>();

            //insert hierarchies
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    for (int i = 1; i <= numToInsert; i++)
                    {

                        dtoList.Add(
                        new LocationHierarchyDto()
                        {
                            Name = "heirarchy" + i.ToString(),
                            EntityId = entityId,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        });
                    }
                    foreach (LocationHierarchyDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<LocationHierarchyDto> InsertLocationHierarchyDtos(IDalFactory dalFactory, List<EntityDto> entityList)
        {
            List<LocationHierarchyDto> dtoList = new List<LocationHierarchyDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    foreach (EntityDto entity in entityList)
                    {
                        dtoList.Add(
                            new LocationHierarchyDto()
                            {
                                EntityId = entity.Id,
                                Name = "hierarchy for " + entity.Id.ToString(),
                                DateCreated = DateTime.Now,
                                DateLastModified = DateTime.Now
                            });
                    }

                    foreach (LocationHierarchyDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<LocationLevelDto> InsertLocationLevelDtos(IDalFactory dalFactory, Int32 hierarchyId, Int32 numToInsert)
        {
            List<LocationLevelDto> dtoList = new List<LocationLevelDto>();

            //insert hierarchies
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    //add 5 dtos
                    for (int i = 1; i <= numToInsert; i++)
                    {

                        dtoList.Add(
                        new LocationLevelDto()
                        {
                            Name = "level" + i.ToString(),
                            LocationHierarchyId = hierarchyId,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        });
                    }
                    Int32? lastId = null;
                    foreach (LocationLevelDto dto in dtoList)
                    {
                        dto.ParentLevelId = lastId;
                        dal.Insert(dto);
                        lastId = dto.Id;
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<LocationGroupDto> InsertLocationGroupDtos(IDalFactory dalFactory, List<LocationLevelDto> levelList, Int32 numPerLevel)
        {
            List<LocationGroupDto> dtoList = new List<LocationGroupDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    Int32 groupNum = 1;


                    //cycle through levels
                    List<LocationGroupDto> lastLevelDtos = new List<LocationGroupDto>();
                    foreach (LocationLevelDto levelDto in levelList)
                    {
                        List<LocationGroupDto> levelDtos = new List<LocationGroupDto>();

                        for (int i = 1; i <= numPerLevel; i++)
                        {
                            LocationGroupDto groupDto = new LocationGroupDto()
                            {
                                Name = "name" + groupNum,
                                Code = "code" + groupNum,
                                LocationHierarchyId = levelDto.LocationHierarchyId,
                                LocationLevelId = levelDto.Id,
                                DateCreated = DateTime.Now,
                                DateLastModified = DateTime.Now
                            };

                            //pick a parent
                            Int32 parentCount = lastLevelDtos.Count;
                            if (parentCount != 0)
                            {
                                groupDto.ParentGroupId = lastLevelDtos[parentCount - 1].Id;
                            }


                            dal.Insert(groupDto);
                            groupNum++;

                            levelDtos.Add(groupDto);
                            dtoList.Add(groupDto);



                            //only add one group if this is the root level
                            if (levelDto.ParentLevelId == null)
                            {
                                break;
                            }

                        }

                        lastLevelDtos = levelDtos;
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        /// <summary>
        /// Creates a fully populated location hierarchy with levels,
        /// groups and locations
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static LocationHierarchy PopulateLocationHierarchy(IDalFactory dalFactory, Int32 entityId)
        {
            LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

            //add 2 levels
            LocationLevel level1 = hierarchy.RootLevel.ChildLevel;
            //LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            //level1.Name = "level1";
            LocationLevel level2 = level1.ChildLevel;
            //LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            //level2.Name = "level2";

            //add 3 level1 groups
            LocationGroup level1Group1 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group1.Name = "level1Group1";
            level1Group1.Code = "level1Group1Code";
            LocationGroup level1Group2 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group2.Name = "level1Group2";
            level1Group2.Code = "level1Group2Code";
            LocationGroup level1Group3 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group3.Name = "level1Group3";
            level1Group3.Code = "level1Group3Code";
            hierarchy.RootGroup.ChildList.Add(level1Group1);
            hierarchy.RootGroup.ChildList.Add(level1Group2);
            hierarchy.RootGroup.ChildList.Add(level1Group3);

            //add 3 level2 groups per level1 group
            Int32 groupCode = 4;
            foreach (LocationGroup level1Group in hierarchy.RootGroup.ChildList)
            {
                for (int i = 1; i <= 3; i++)
                {
                    LocationGroup level2Group = LocationGroup.NewLocationGroup(level2.Id);
                    level2Group.Name = "level2group" + i.ToString();
                    level2Group.Code = groupCode.ToString();
                    level1Group.ChildList.Add(level2Group);

                    groupCode++;
                }
            }

            hierarchy = hierarchy.Save();

            //insert locations against the hierarchy
            IEnumerable<LocationGroup> leafGroups = hierarchy.RootGroup.FetchAllChildGroups().ToList().AsReadOnly().Where(g => g.ChildList.Count == 0);

            TestDataHelper.InsertLocationDtos(dalFactory, entityId,
                leafGroups.Select(g => g.Id),
                20);

            return hierarchy;
        }

        #endregion

        #region LocationPlanAssignment

        /// <summary>
        /// Helper method to insert a specified number of Location plan assignment Dtos. Assumes the count of Locations & Products are the same. (Requirement).
        /// </summary>
        public static List<LocationPlanAssignmentDto> InsertLocationPlanAssignmentDtos(IDalFactory dalFactory, List<LocationDto> locations, List<WorkpackagePlanogramDto> workpackagePlanograms, List<ProductGroupDto> productGroups, Int16 NumberToInsert, Int32 entityId)
        {
            var returnList = new List<LocationPlanAssignmentDto>();

            using (var dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
                {
                    for (Int32 i = 0; i < NumberToInsert; i++)
                    {
                        var locationPlanAssignmentDto = new LocationPlanAssignmentDto()
                        {
                            LocationId = locations[i].Id,
                            PlanogramId = (Int32)workpackagePlanograms[i].DestinationPlanogramId,
                            PlanogramName = workpackagePlanograms[i].Name,
                            ProductGroupId = productGroups.First().Id,
                        };
                        returnList.Add(locationPlanAssignmentDto);
                        dal.Insert(locationPlanAssignmentDto);
                    }
                }
            }

            return returnList;
        }

        public static LocationPlanAssignmentList InsertLocationPlanAssignment(Int32 entityId)
        {
            LocationInfoList locationInfoList = LocationInfoList.FetchByEntityId(entityId);
            ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(entityId);
            LocationPlanAssignmentList locationPlanAssignmentList = LocationPlanAssignmentList.FetchByEntityIdProductGroupId(entityId, productHierarchy.RootGroup.Id);
            
            foreach (LocationInfo location in locationInfoList)
            {
                locationPlanAssignmentList.Add(LocationPlanAssignment.NewLocationPlanAssignment(productHierarchy.RootGroup.Id, 1, location.Id));
            }

            locationPlanAssignmentList = locationPlanAssignmentList.Save();

            return locationPlanAssignmentList;
        }

        #endregion

        #region MetricProfile

        public static List<MetricProfileDto> InsertMetricProfileDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId, IEnumerable<MetricDto> metrics)
        {
            List<MetricProfileDto> dtoList = new List<MetricProfileDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IMetricProfileDal dal = dalContext.GetDal<IMetricProfileDal>())
                {
                    for (Int16 i = 1; i <= numToInsert; i++)
                    {
                        dtoList.Add(
                        new MetricProfileDto()
                        {
                            Id = i,
                            EntityId = entityId,
                            Name = "name" + i.ToString(),

                            Metric1MetricId = metrics.ElementAt(0).Id,
                            Metric1Ratio = 0.5F,

                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now,
                            DateDeleted = null,
                        });
                    }

                    // insert into the dal
                    foreach (MetricProfileDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        #region Image

        public static List<CompressionDto> InsertCompressionDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<CompressionDto> dtoList = new List<CompressionDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
                {
                    for (Int32 i = 0; i < numToInsert; i++)
                    {
                        CompressionDto dto = new CompressionDto()
                        {
                            Name = "Compression " + i,
                            ColourDepth = i,
                            Enabled = true,
                            Height = i,
                            MaintainAspectRatio = true,
                            Width = i
                        };
                        dal.Insert(dto);
                        dtoList.Add(dto);
                    }

                }
            }

            return dtoList;
        }

        public static List<ProductImageDto> InsertProductImageDtos(IDalContext dalContext, Int32 entityId,
            IEnumerable<ProductDto> products, IEnumerable<CompressionDto> compressionDtos)
        {
            List<ProductImageDto> dtoList = new List<ProductImageDto>();

            using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
            {
                foreach (ProductDto product in products)
                {
                    foreach (ProductImageType imageType in Enum.GetValues(typeof(ProductImageType)))
                    {
                        foreach (ProductImageFacing facingType in Enum.GetValues(typeof(ProductImageFacing)))
                        {
                            foreach (CompressionDto compression in compressionDtos)
                            {
                                ProductImageDto productImageDto =
                                    new ProductImageDto()
                                    {
                                        EntityId = entityId,
                                        ProductId = product.Id,
                                        ImageId = InsertImageDto(dalContext, entityId, compression.Id).Id,
                                        ImageType = (Byte)imageType,
                                        FacingType = (Byte)facingType,
                                        DateCreated = DateTime.UtcNow,
                                        DateLastModified = DateTime.UtcNow
                                    };
                                dal.Insert(productImageDto);
                                dtoList.Add(productImageDto);
                            }
                        }
                    }
                }
            }

            return dtoList;
        }

        public static ImageDto InsertImageDto(IDalContext dalContext, Int32 entityID, Int32 compressionId = 1)
        {
            Random rand = new Random(98374);
            using (IImageDal dal = dalContext.GetDal<IImageDal>())
            {
                ImageDto dto = new ImageDto()
                {
                    EntityId = entityID,
                    OriginalFileId = InsertFileDto(dalContext, entityID).Id,
                    CompressionId = compressionId,
                    BlobId = InsertBlobDto(dalContext).Id,
                    Height = (Int16)rand.Next(5000),
                    Width = (Int16)rand.Next(5000),
                    SizeInBytes = rand.Next(),
                    DateCreated = DateTime.UtcNow,
                    DateLastModified = DateTime.UtcNow
                };

                dal.Insert(dto);

                return dto;
            }
        }

        #endregion

        #region PerformanceSelection

        public static List<PerformanceSelectionDto> InsertPerformanceSelection(IDalContext dalContext, Int32 entityId)
        {
            List<PerformanceSelectionDto> dtoList = new List<PerformanceSelectionDto>();

            using (var dal = dalContext.GetDal<IPerformanceSelectionDal>())
            {
                //Create
                PerformanceSelectionDto dto = new PerformanceSelectionDto();
                SetPropertyValues(dto, false);

                //Set parents
                dto.EntityId = entityId;
                
                //Insert
                dal.Insert(dto);

                //Add to list
                dtoList.Add(dto);                 
            }

            return dtoList;
        }

        #endregion

        #region Planograms

        /// <summary>
        /// Creates the crewe sparkling water plan for use with tests.
        /// </summary>
        /// <returns></returns>
        public static Package CreatePlanCreweSparklingWater()
        {
            Package pk1 = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = Planogram.NewPlanogram();

            CreatePlanCreweSparklingWater(plan);
            pk1.Planograms.Add(plan);
            return pk1;
        }
        public static void CreatePlanCreweSparklingWater(Planogram plan)
        {
            if (plan.FixtureItems.Count > 0) plan.FixtureItems.Clear();
            if (plan.Fixtures.Count > 0) plan.Fixtures.Clear();
            if (plan.Assemblies.Count > 0) plan.Assemblies.Clear();
            if (plan.Components.Count > 0) plan.Components.Clear();
            if (plan.Annotations.Count > 0) plan.Annotations.Clear();
            if (plan.Positions.Count > 0) plan.Positions.Clear();
            if (plan.Products.Count > 0) plan.Products.Clear();
            if (plan.Images.Count > 0) plan.Images.Clear();
            if (plan.EventLogs.Count > 0) plan.EventLogs.Clear();


            plan.Name = "Crewe Sparkling Water";
            plan.Height = 191;
            plan.Width = 120.6F;
            plan.Depth = 153;
            plan.MetaBayCount = 1;

            #region Products

            PlanogramProduct p1 = PlanogramProduct.NewPlanogramProduct();
            p1.Gtin = "104276036";
            p1.Name = "MIN: 'M' Savers Sparkling Water 2L";
            p1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            p1.Height = 34.4F;
            p1.Width = 9.7F;
            p1.Depth = 9.7F;
            p1.Brand = "Savers";
            p1.FillColour = 12237312;
            plan.Products.Add(p1);


            PlanogramProduct prod3 = PlanogramProduct.NewPlanogramProduct();
            prod3.Gtin = "100110645";
            prod3.Name = "MIN: 'M' SPARKLING SPRING WTR 4X2LT";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod3.Height = 34.4F;
            prod3.Width = 19.5F;
            prod3.Depth = 19.5F;
            prod3.Brand = "Morrisons";
            prod3.FillColour = 12237312;
            plan.Products.Add(prod3);

            PlanogramProduct prod4 = PlanogramProduct.NewPlanogramProduct();
            prod4.Gtin = "100110629";
            prod4.Name = "MIN: H/LAND SPARKLING WTR 4X1.5L";
            prod4.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod4.Height = 32.5F;
            prod4.Width = 19F;
            prod4.Depth = 19F;
            prod4.Brand = "Highland Spring";
            prod4.FillColour = 12237312;
            plan.Products.Add(prod4);

            PlanogramProduct prod5 = PlanogramProduct.NewPlanogramProduct();
            prod5.Gtin = "100110637";
            prod5.Name = "MIN: 'M' SPARKLING SPRING WATER 2LT";
            prod5.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod5.Height = 34.4F;
            prod5.Width = 9.7F;
            prod5.Depth = 9.7F;
            prod5.FillColour = 12237312;
            prod5.Brand = "Morrisons";
            plan.Products.Add(prod5);

            PlanogramProduct prod6 = PlanogramProduct.NewPlanogramProduct();
            prod6.Gtin = "100110590";
            prod6.Name = "MIN: H/LAND SPRING SPARK WATER 1.5L";
            prod6.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod6.Height = 32.6F;
            prod6.Width = 9.5F;
            prod6.Depth = 9.5F;
            prod6.FillColour = 12237312;
            prod6.Brand = "Highland Spring";
            plan.Products.Add(prod6);

            PlanogramProduct prod7 = PlanogramProduct.NewPlanogramProduct();
            prod7.Gtin = "100110426";
            prod7.Name = "MIN: 'M'SPARKLING WATER 6X500ML";
            prod7.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod7.Height = 21.8F;
            prod7.Width = 19.5F;
            prod7.Depth = 12.3F;
            prod7.FillColour = 12237312;
            prod7.Brand = "Morrisons";
            plan.Products.Add(prod7);

            PlanogramProduct prod8 = PlanogramProduct.NewPlanogramProduct();
            prod8.Gtin = "100110477";
            prod8.Name = "MIN: BUXTON SPARKLING 8X500ML";
            prod8.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod8.Height = 23F;
            prod8.Width = 24.5F;
            prod8.Depth = 12F;
            prod8.TrayDepth = 39;
            prod8.TrayWidth = 25;
            prod8.TrayHeight = 23.2F;
            prod8.TrayHigh = 1;
            prod8.TrayWide = 1;
            prod8.TrayDeep = 3;
            prod8.FillColour = 12237312;
            prod8.Brand = "Buxton";
            plan.Products.Add(prod8);

            PlanogramProduct prod9 = PlanogramProduct.NewPlanogramProduct();
            prod9.Gtin = "100110451";
            prod9.Name = "MIN: H/LAND SPRING SPARKLING 6X500";
            prod9.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod9.Height = 21.5F;
            prod9.Width = 20F;
            prod9.Depth = 13.2F;
            prod9.FillColour = 12237312;
            prod9.Brand = "";
            plan.Products.Add(prod9);

            PlanogramProduct prod10 = PlanogramProduct.NewPlanogramProduct();
            prod10.Gtin = "100110531";
            prod10.Name = "MIN: PERRIER SPARKLING MINERAL 75CL";
            prod10.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod10.Height = 26.5F;
            prod10.Width = 8.4F;
            prod10.Depth = 8.4F;
            prod10.FillColour = 16760940;
            plan.Products.Add(prod10);

            PlanogramProduct prod11 = PlanogramProduct.NewPlanogramProduct();
            prod11.Gtin = "100110549";
            prod11.Name = "MIN: S.PELLEGRINO SPARKLING WTR 1LT";
            prod11.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod11.Height = 30.8F;
            prod11.Width = 8.5F;
            prod11.Depth = 8.5F;
            prod11.FillColour = 16760940;
            plan.Products.Add(prod11);

            PlanogramProduct prod12 = PlanogramProduct.NewPlanogramProduct();
            prod12.Gtin = "100110670";
            prod12.Name = "MIN: SAN PELLEGRINO 6X1L";
            prod12.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod12.Height = 30.9F;
            prod12.Width = 25F;
            prod12.Depth = 17F;
            prod12.FillColour = 16760940;
            plan.Products.Add(prod12);

            PlanogramProduct prod13 = PlanogramProduct.NewPlanogramProduct();
            prod13.Gtin = "10110522";
            prod13.Name = "MIN: STRATHMORE SPARKLING GLASS 1L";
            prod13.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod13.Height = 32F;
            prod13.Width = 8.5F;
            prod13.Depth = 8.5F;
            prod13.FillColour = 16760940;
            prod13.Brand = "Strathmore";
            plan.Products.Add(prod13);

            PlanogramProduct prod14 = PlanogramProduct.NewPlanogramProduct();
            prod14.Gtin = "100110493";
            prod14.Name = "MIN: BADOIT SPARKLING WATER  1LITRE";
            prod14.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod14.Height = 32.2F;
            prod14.Width = 8.2F;
            prod14.Depth = 8.2F;
            prod14.FillColour = 16760940;
            prod14.Brand = "Badoit";
            plan.Products.Add(prod14);

            #endregion

            #region Event Logs
                        
            PlanogramEventLog eventLog1 = PlanogramEventLog.NewPlanogramEventLog();
            eventLog1.EntryType = PlanogramEventLogEntryType.Information;
            eventLog1.EventType = PlanogramEventLogEventType.General;
            eventLog1.AffectedType = PlanogramEventLogAffectedType.Products;
            eventLog1.AffectedId = 1;
            eventLog1.Description = "Description1";
            eventLog1.Content = "Content1";
            eventLog1.WorkpackageSource = "WorkpackageSource1";
            eventLog1.TaskSource = "TaskSource1";
            eventLog1.EventId = 1;
            plan.EventLogs.Add(eventLog1);

            #endregion

            //Fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = "Bay 1";
            fixture.Height = 191;
            fixture.Width = 120.6F;
            fixture.Depth = 153;
            plan.Fixtures.Add(fixture);

            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = fixture.Id;
            plan.FixtureItems.Add(fixtureItem);

            //Backboard
            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            PlanogramFixtureComponent[] shelvesFC = new PlanogramFixtureComponent[5];
            PlanogramSubComponent[] shelves = new PlanogramSubComponent[5];
            PlanogramSubComponentPlacement[] subComponentPlacements = new PlanogramSubComponentPlacement[5];
            Single[] shelfYPositions = new Single[] { 10, 51.8F, 93.6F, 135.4F, 165.8F };

            for (Int32 i = 0; i < shelfYPositions.Count(); i++)
            {
                PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
                PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);
                shelf1FC.Y = shelfYPositions[i];

                PlanogramSubComponent shelfSub = shelf1C.SubComponents[0];
                shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
                shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
                shelfSub.IsProductOverlapAllowed = false;

                shelvesFC[i] = shelf1FC;
                shelves[i] = shelfSub;

                subComponentPlacements[i] = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                    shelfSub,
                    fixtureItem,
                    shelf1FC);
            }

            #region Positions

            PlanogramPosition pos1 = subComponentPlacements[0].AddPosition(p1);
            pos1.X = 6.95F;
            pos1.Y = 4;
            pos1.Z = 0;
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 11;
            pos1.FacingsDeep = 7;
            pos1.UnitsHigh = 1;
            pos1.UnitsWide = 11;
            pos1.UnitsDeep = 7;
            pos1.TotalUnits = 77;

            PlanogramPosition pos3 = subComponentPlacements[1].AddPosition(prod3);
            pos3.X = 0;
            pos3.Y = 4;
            pos3.Z = 0;
            pos3.FacingsHigh = 1;
            pos3.FacingsWide = 3;
            pos3.FacingsDeep = 3;
            pos3.TotalUnits = 1;

            PlanogramPosition pos4 = subComponentPlacements[1].AddPosition(prod4);
            pos4.X = 82.6F;
            pos4.Y = 4;
            pos4.Z = 0;
            pos4.FacingsHigh = 1;
            pos4.FacingsWide = 2;
            pos4.FacingsDeep = 4;
            pos4.TotalUnits = 1;

            PlanogramPosition pos5 = subComponentPlacements[2].AddPosition(prod5);
            pos5.X = 0;
            pos5.Y = 4;
            pos5.Z = 0;
            pos5.FacingsHigh = 1;
            pos5.FacingsWide = 8;
            pos5.FacingsDeep = 7;
            pos5.TotalUnits = 1;

            PlanogramPosition pos6 = subComponentPlacements[2].AddPosition(prod6);
            pos6.X = 101.6F;
            pos6.Y = 4;
            pos6.Z = 0;
            pos6.FacingsHigh = 1;
            pos6.FacingsWide = 2;
            pos6.FacingsDeep = 8;
            pos6.TotalUnits = 1;

            PlanogramPosition pos7 = subComponentPlacements[3].AddPosition(prod7);
            pos7.X = 0;
            pos7.Y = 4;
            pos7.Z = 0;
            pos7.FacingsHigh = 1;
            pos7.FacingsWide = 1;
            pos7.FacingsDeep = 6;
            pos7.TotalUnits = 1;

            PlanogramPosition pos8 = subComponentPlacements[3].AddPosition(prod8);
            pos8.X = 21.05F;
            pos8.Y = 4;
            pos8.OrientationType = PlanogramPositionOrientationType.Right0;
            pos8.FacingsHigh = 1;
            pos8.FacingsWide = 2;
            pos8.FacingsDeep = 3;
            pos8.TotalUnits = 1;

            PlanogramPosition pos9 = subComponentPlacements[3].AddPosition(prod9);
            pos9.X = 100.6F;
            pos9.Y = 4;
            pos9.FacingsHigh = 1;
            pos9.FacingsWide = 1;
            pos9.FacingsDeep = 5;
            pos9.TotalUnits = 1;

            PlanogramPosition pos10 = subComponentPlacements[4].AddPosition(prod10);
            pos10.X = 43.55F;
            pos10.Y = 4;
            pos10.FacingsHigh = 1;
            pos10.FacingsWide = 3;
            pos10.FacingsDeep = 9;
            pos10.TotalUnits = 1;

            PlanogramPosition pos11 = subComponentPlacements[4].AddPosition(prod11);
            pos11.X = 77.92F;
            pos11.Y = 4;
            pos11.FacingsHigh = 1;
            pos11.FacingsWide = 1;
            pos11.FacingsDeep = 9;
            pos11.TotalUnits = 1;

            PlanogramPosition pos12 = subComponentPlacements[4].AddPosition(prod12);
            pos12.X = 95.6F;
            pos12.Y = 4;
            pos12.FacingsHigh = 1;
            pos12.FacingsWide = 1;
            pos12.FacingsDeep = 2;
            pos12.TotalUnits = 1;

            PlanogramPosition pos13 = subComponentPlacements[4].AddPosition(prod13);
            pos13.X = 17.37F;
            pos13.Y = 4;
            pos13.FacingsHigh = 1;
            pos13.FacingsWide = 2;
            pos13.FacingsDeep = 9;
            pos13.TotalUnits = 1;

            PlanogramPosition pos14 = subComponentPlacements[4].AddPosition(prod14);
            pos14.X = 0F;
            pos14.Y = 4;
            pos14.FacingsHigh = 1;
            pos14.FacingsWide = 1;
            pos14.FacingsDeep = 9;
            pos14.TotalUnits = 1;

            #endregion

        }

        public static List<Int32> InsertPlanograms(Int32 count)
        {
            Entity entity = Entity.NewEntity();
            entity.Name = "Entity";

            List<Int32> plans = new List<Int32>();
            for (Int32 i = 0; i < count; i++)
            {
                Package pk1 = TestDataHelper.CreatePlanCreweSparklingWater();
                Planogram plan1 = pk1.Planograms.First();

                pk1.Name = String.Format("Plan{0}", i);
                plan1.Name = String.Format("Plan{0}", i);

                pk1 = pk1.Save();

                plans.Add((Int32)pk1.Id);

                pk1.Dispose();
                pk1 = null;

            }

            return plans;
        }

        public static List<WorkflowTaskDto> InsertWorkflowTaskDtos(IDalContext dalContext, List<WorkpackageDto> workpackageDtoList)
        {
            List<WorkflowTaskDto> dtoList = new List<WorkflowTaskDto>();
            using (var dal = dalContext.GetDal<IWorkflowTaskDal>())
            {
                foreach (WorkpackageDto workpackageDto in workpackageDtoList)
                {
                    WorkflowTaskDto workflowtaskDto = new WorkflowTaskDto();
                    SetPropertyValues(workflowtaskDto, false);
                    workflowtaskDto.WorkflowId = workpackageDto.WorkflowId;

                    dal.Insert(workflowtaskDto);
                    dtoList.Add(workflowtaskDto);
                }
            }
            return dtoList;
        }

        public static List<WorkflowDto> InsertWorkflowDtos(IDalContext dalContext, List<EntityDto> entityDtoList, Int32 numberToInsert)
        {
            List<WorkflowDto> dtoList = new List<WorkflowDto>();

            using (var dal = dalContext.GetDal<IWorkflowDal>())
            {
                foreach (EntityDto entityDto in entityDtoList)
                {
                    for (int i = 0; i < numberToInsert; i++)
                    {
                        //Create package
                        WorkflowDto workflowDto = new WorkflowDto();
                        SetPropertyValues(workflowDto, false);
                        workflowDto.EntityId = entityDto.Id;
                        workflowDto.Name = String.Format("Workflow {0}", i);

                        //Insert
                        dal.Insert(workflowDto);

                        //Add to list
                        dtoList.Add(workflowDto);
                    }
                }
            }
            return dtoList;
        }

        public static List<WorkpackageDto> InsertWorkPackageDtos(IDalContext dalContext, List<WorkflowDto> workflowDtoList, UserDto user)
        {
            List<WorkpackageDto> dtoList = new List<WorkpackageDto>();

            using (var dal = dalContext.GetDal<IWorkpackageDal>())
            {
                foreach (WorkflowDto workflowDto in workflowDtoList)
                {
                    //Create package
                    WorkpackageDto packageDto = new WorkpackageDto();
                    packageDto.Name = String.Format("Workpackage {0}", workflowDtoList.IndexOf(workflowDto));
                    packageDto.WorkflowId = workflowDto.Id;
                    packageDto.EntityId = workflowDto.EntityId;
                    packageDto.UserId = user.Id;

                    //Insert
                    dal.Insert(packageDto);

                    //Add to list
                    dtoList.Add(packageDto);
                }
            }
            return dtoList;
        }

        public static List<WorkpackageDto> InsertWorkPackageDtos(IDalContext dalContext, List<WorkflowDto> workflowDtoList, List<UserDto> userDtoList)
        {
            List<WorkpackageDto> dtoList = new List<WorkpackageDto>();

            using (var dal = dalContext.GetDal<IWorkpackageDal>())
            {
                foreach (WorkflowDto workflowDto in workflowDtoList)
                {
                    //Create package
                    WorkpackageDto packageDto = new WorkpackageDto();
                    packageDto.Name = String.Format("Workpackage {0}", workflowDtoList.IndexOf(workflowDto));
                    packageDto.WorkflowId = workflowDto.Id;
                    packageDto.EntityId = workflowDto.EntityId;
                    packageDto.UserId = userDtoList.First().Id;

                    //Insert
                    dal.Insert(packageDto);

                    //Add to list
                    dtoList.Add(packageDto);
                }
            }
            return dtoList;
        }

        public static List<PackageDto> InsertPackageDtos(IDalContext dalContext, String userName, Int32 entityId, Int32 numberToInsert)
        {
            List<PackageDto> dtoList = new List<PackageDto>();

            using (var dal = dalContext.GetDal<IPackageDal>())
            {
                for (int i = 0; i < numberToInsert; i++)
                {
                    //Create package
                    PackageDto packageDto = new PackageDto
                    {
                        Name = String.Format("Package {0}", i),
                        UserName = userName,
                        EntityId = entityId
                    };

                    //Insert
                    dal.Insert(packageDto);

                    //Add to list
                    dtoList.Add(packageDto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramPerformanceDto> InsertPlanogramPerformanceDtos(IDalContext dalContext, List<PlanogramDto> parentPlanogramDtos)
        {
            var dtoList = new List<PlanogramPerformanceDto>();
            using (var dal = dalContext.GetDal<IPlanogramPerformanceDal>())
            {
                foreach (var planogramDto in parentPlanogramDtos)
                {
                    var dto = new PlanogramPerformanceDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramId = planogramDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramAssortmentDto> InsertPlanogramAssortmentDtos(IDalContext dalContext, List<PlanogramDto> parentPlanogramDtos)
        {
            var dtoList = new List<PlanogramAssortmentDto>();
            using (var dal = dalContext.GetDal<IPlanogramAssortmentDal>())
            {
                foreach (var planogramDto in parentPlanogramDtos)
                {
                    var dto = new PlanogramAssortmentDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramId = planogramDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramAssortmentLocalProductDto> InsertPlanogramAssortmentLocalProductDtos(IDalContext dalContext, List<PlanogramAssortmentDto> parentPlanogramAssortmentDtos)
        {
            var dtoList = new List<PlanogramAssortmentLocalProductDto>();
            using (var dal = dalContext.GetDal<IPlanogramAssortmentLocalProductDal>())
            {
                foreach (var planogramAssortmentDto in parentPlanogramAssortmentDtos)
                {
                    var dto = new PlanogramAssortmentLocalProductDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramAssortmentId = planogramAssortmentDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramAssortmentProductDto> InsertPlanogramAssortmentProductDtos(IDalContext dalContext, List<PlanogramAssortmentDto> parentPlanogramAssortmentDtos)
        {
            var dtoList = new List<PlanogramAssortmentProductDto>();
            using (var dal = dalContext.GetDal<IPlanogramAssortmentProductDal>())
            {
                foreach (var planogramAssortmentDto in parentPlanogramAssortmentDtos)
                {
                    var dto = new PlanogramAssortmentProductDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramAssortmentId = planogramAssortmentDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramAssortmentRegionDto> InsertPlanogramAssortmentRegionDtos(IDalContext dalContext, List<PlanogramAssortmentDto> parentPlanogramAssortmentDtos)
        {
            var dtoList = new List<PlanogramAssortmentRegionDto>();
            using (var dal = dalContext.GetDal<IPlanogramAssortmentRegionDal>())
            {
                foreach (var planogramAssortmentDto in parentPlanogramAssortmentDtos)
                {
                    var dto = new PlanogramAssortmentRegionDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramAssortmentId = planogramAssortmentDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramAssortmentRegionLocationDto> InsertPlanogramAssortmentRegionLocationDtos(IDalContext dalContext, List<PlanogramAssortmentRegionDto> parentPlanogramAssortmentRegionDtos)
        {
            var dtoList = new List<PlanogramAssortmentRegionLocationDto>();
            using (var dal = dalContext.GetDal<IPlanogramAssortmentRegionLocationDal>())
            {
                foreach (var planogramAssortmentRegionDto in parentPlanogramAssortmentRegionDtos)
                {
                    var dto = new PlanogramAssortmentRegionLocationDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramAssortmentRegionId = planogramAssortmentRegionDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramAssortmentRegionProductDto> InsertPlanogramAssortmentRegionProductDtos(IDalContext dalContext, List<PlanogramAssortmentRegionDto> parentPlanogramAssortmentRegionDtos)
        {
            var dtoList = new List<PlanogramAssortmentRegionProductDto>();
            using (var dal = dalContext.GetDal<IPlanogramAssortmentRegionProductDal>())
            {
                foreach (var planogramAssortmentRegionDto in parentPlanogramAssortmentRegionDtos)
                {
                    var dto = new PlanogramAssortmentRegionProductDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramAssortmentRegionId = planogramAssortmentRegionDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramConsumerDecisionTreeDto> InsertPlanogramConsumerDecisionTreeDtos(IDalContext dalContext, List<PlanogramDto> parentPlanogramDtos)
        {
            var dtoList = new List<PlanogramConsumerDecisionTreeDto>();
            using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeDal>())
            {
                foreach (var planogramDto in parentPlanogramDtos)
                {
                    var dto = new PlanogramConsumerDecisionTreeDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramId = planogramDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramConsumerDecisionTreeLevelDto> InsertPlanogramConsumerDecisionTreeLevelDtos(IDalContext dalContext, PlanogramConsumerDecisionTreeDto parentPlanogramConsumerDecisionTreeDto, Int32 levelCount, Boolean addRootLevel)
        {
            var dtoList = new List<PlanogramConsumerDecisionTreeLevelDto>();

            PlanogramConsumerDecisionTreeLevelDto rootDto = null;

            using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeLevelDal>())
            {
                if (addRootLevel)
                {
                    //add a rootLevel
                    rootDto = new PlanogramConsumerDecisionTreeLevelDto();
                    SetPropertyValues(rootDto, false);
                    rootDto.PlanogramConsumerDecisionTreeId = parentPlanogramConsumerDecisionTreeDto.Id;
                    rootDto.ParentLevelId = null;
                    dal.Insert(rootDto);

                }

                //cycle through levels
                for (int i = 1; i <= levelCount; i++)
                {
                    var levelDto = new PlanogramConsumerDecisionTreeLevelDto();
                    SetPropertyValues(levelDto, false);
                    levelDto.Name = String.Format("level {0}", i);
                    levelDto.PlanogramConsumerDecisionTreeId = parentPlanogramConsumerDecisionTreeDto.Id;
                    dtoList.Add(levelDto);
                }

                Object lastId = null;
                if (addRootLevel)
                {
                    lastId = rootDto.Id;
                }

                //Insert the child levels
                foreach (PlanogramConsumerDecisionTreeLevelDto dto in dtoList)
                {
                    dto.ParentLevelId = lastId;
                    dal.Insert(dto);
                    lastId = dto.Id;
                }

                if (addRootLevel)
                {
                    dtoList.Insert(0, rootDto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramConsumerDecisionTreeNodeDto> InsertPlanogramConsumerDecisionTreeNodeDtos(IDalContext dalContext, PlanogramConsumerDecisionTreeDto parentPlanogramConsumerDecisionTreeDto,
             List<PlanogramConsumerDecisionTreeLevelDto> levelDtos, Int32 numPerLevel, Boolean addRootNode)
        {
            var dtoList = new List<PlanogramConsumerDecisionTreeNodeDto>();

            PlanogramConsumerDecisionTreeNodeDto rootDto = null;

            using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
            {
                if (addRootNode)
                {
                    //add a rootnode
                    rootDto = new PlanogramConsumerDecisionTreeNodeDto();
                    SetPropertyValues(rootDto, false);
                    rootDto.PlanogramConsumerDecisionTreeId = parentPlanogramConsumerDecisionTreeDto.Id;
                    rootDto.ParentNodeId = null;

                    PlanogramConsumerDecisionTreeLevelDto rootLevelDto = levelDtos.FirstOrDefault();
                    levelDtos.Remove(rootLevelDto);

                    if (rootLevelDto != null)
                    {
                        rootDto.PlanogramConsumerDecisionTreeLevelId = rootLevelDto.Id;
                    }

                    dal.Insert(rootDto);
                    dtoList.Add(rootDto);
                }

                foreach (PlanogramConsumerDecisionTreeLevelDto levelDto in levelDtos)
                {
                    for (Int32 i = 1; i <= numPerLevel; i++)
                    {
                        PlanogramConsumerDecisionTreeNodeDto nodeDto = new PlanogramConsumerDecisionTreeNodeDto();
                        SetPropertyValues(nodeDto, false);
                        nodeDto.PlanogramConsumerDecisionTreeId = parentPlanogramConsumerDecisionTreeDto.Id;
                        nodeDto.PlanogramConsumerDecisionTreeLevelId = levelDto.Id;
                        nodeDto.Name = String.Format("NodeDtoName{0}", i);
                        dal.Insert(nodeDto);
                        dtoList.Add(nodeDto);
                    }
                }

                IEnumerable<IGrouping<Object, PlanogramConsumerDecisionTreeNodeDto>> levelNodeGroups =
                    dtoList.GroupBy(p => p.PlanogramConsumerDecisionTreeLevelId);

                //Create the parent-child links
                IGrouping<Object, PlanogramConsumerDecisionTreeNodeDto> previousLevelGroup = null;
                foreach (IGrouping<Object, PlanogramConsumerDecisionTreeNodeDto> levelGroup in levelNodeGroups)
                {
                    if (previousLevelGroup != null)
                    {
                        //If the root was add and this is the second level assign all nodes to the single root
                        if (addRootNode &&
                            (Int32)levelGroup.Key == (Int32)levelNodeGroups.First().Key + 1)
                        {
                            foreach (PlanogramConsumerDecisionTreeNodeDto dto in levelGroup)
                            {
                                dto.ParentNodeId = rootDto.Id;
                                dal.Insert(dto);
                            }
                        }
                        else
                        {
                            for (Int32 i = 0; i < numPerLevel; i++)
                            {
                                PlanogramConsumerDecisionTreeNodeDto dto = levelGroup.ElementAtOrDefault(i);
                                PlanogramConsumerDecisionTreeNodeDto previousDto = previousLevelGroup.ElementAtOrDefault(i);

                                //Add the parent node id and insert
                                if (dto != null && previousDto != null)
                                {
                                    dto.ParentNodeId = previousDto.Id;
                                    dal.Insert(dto);
                                }

                            }
                        }
                    }
                    else if (!addRootNode)
                    {
                        for (Int32 i = 0; i < numPerLevel; i++)
                        {
                            PlanogramConsumerDecisionTreeNodeDto dto = levelGroup.ElementAtOrDefault(i);

                            //Add the parent node id and insert
                            if (dto != null)
                            {
                                dto.ParentNodeId = null;
                                dal.Insert(dto);
                            }
                        }
                    }
                    previousLevelGroup = levelGroup;
                }
            }
            return dtoList;
        }

        public static List<PlanogramConsumerDecisionTreeNodeProductDto> InsertPlanogramConsumerDecisionTreeNodeProductDtos
            (IDalContext dalContext, List<PlanogramConsumerDecisionTreeNodeDto> parentPlanogramConsumerDecisionTreeNodeDtos, List<PlanogramProductDto> products, Int32 numPerNode)
        {
            var dtoList = new List<PlanogramConsumerDecisionTreeNodeProductDto>();
            using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeProductDal>())
            {
                foreach (PlanogramConsumerDecisionTreeNodeDto nodeDto in parentPlanogramConsumerDecisionTreeNodeDtos)
                {
                    List<PlanogramProductDto> availableProducts = products.ToList();

                    for (Int32 i = 0; i < numPerNode; i++)
                    {
                        if (availableProducts.Count != 0)
                        {
                            //get a product
                            PlanogramProductDto product = availableProducts[availableProducts.Count - 1];

                            //create dto
                            PlanogramConsumerDecisionTreeNodeProductDto familyProductDto = new PlanogramConsumerDecisionTreeNodeProductDto()
                            {
                                PlanogramConsumerDecisionTreeNodeId = nodeDto.Id,
                                PlanogramProductId = product.Id,
                            };
                            dal.Insert(familyProductDto);
                            dtoList.Add(familyProductDto);

                            //remove the product from the available list
                            availableProducts.Remove(product);
                        }
                    }
                }
            }
            return dtoList;
        }

        public static List<PlanogramPerformanceMetricDto> InsertPlanogramPerformanceMetricDtos(IDalContext dalContext, List<PlanogramPerformanceDto> parentPlanogramPerformanceDtos)
        {
            var dtoList = new List<PlanogramPerformanceMetricDto>();
            using (var dal = dalContext.GetDal<IPlanogramPerformanceMetricDal>())
            {
                foreach (var planogramPerformanceDto in parentPlanogramPerformanceDtos)
                {
                    for (Int32 i = 0; i < 20; i++)
                    {
                        var dto = new PlanogramPerformanceMetricDto();
                        SetPropertyValues(dto, false);
                        dto.PlanogramPerformanceId = planogramPerformanceDto.Id;
                        dto.MetricId = (Byte)i;
                        dal.Insert(dto);
                        dtoList.Add(dto);
                    }
                }
            }
            return dtoList;
        }

        public static List<PlanogramPerformanceDataDto> InsertPlanogramPerformanceDataDtos(IDalContext dalContext, List<PlanogramPerformanceDto> parentPlanogramPerformanceDtos, List<PlanogramProductDto> planogramProductDtos)
        {
            var dtoList = new List<PlanogramPerformanceDataDto>();
            using (var dal = dalContext.GetDal<IPlanogramPerformanceDataDal>())
            {
                foreach (var planogramPerformanceDto in parentPlanogramPerformanceDtos)
                {
                    foreach (var planogramProductDto in planogramProductDtos)
                    {
                        var dto = new PlanogramPerformanceDataDto();
                        SetPropertyValues(dto, false);
                        dto.PlanogramPerformanceId = planogramPerformanceDto.Id;
                        dto.PlanogramProductId = planogramProductDto.Id;
                        dal.Insert(dto);
                        dtoList.Add(dto);
                    }
                }
            }
            return dtoList;
        }

        public static List<PlanogramDto> InsertPlanogramDtos(IDalContext dalContext, String userName, Int32 numberToInsertPerPackage, List<PackageDto> packageDtoList)
        {
            List<PlanogramDto> dtoList = new List<PlanogramDto>();

            using (var dal = dalContext.GetDal<IPlanogramDal>())
            {
                foreach (PackageDto packageDto in packageDtoList)
                {
                    for (int i = 0; i < numberToInsertPerPackage; i++)
                    {
                        //Create
                        PlanogramDto dto = new PlanogramDto();
                        SetPropertyValues(dto, false);
                        dto.UserName = userName;
                        dto.UniqueContentReference = Guid.NewGuid().ToString();

                        //Insert
                        dal.Insert(packageDto, dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<CustomAttributeDataDto> InsertPlanogramCustomAttributeDataDtos(IDalContext dalContext, List<PlanogramDto> planogramDtoList)
        {
            List<CustomAttributeDataDto> dtoList = new List<CustomAttributeDataDto>();

            using (var dal = dalContext.GetDal<ICustomAttributeDataDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    //Create
                    CustomAttributeDataDto dto = new CustomAttributeDataDto();
                    SetPropertyValues(dto, false);
                    dto.ParentId = planogramDto.Id;
                    dto.ParentType = (Byte)CustomAttributeDataPlanogramParentType.Planogram;

                    //Insert
                    dal.Insert(dto);

                    //Add to list
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }

        public static List<WorkpackagePlanogramDto> InsertWorkpackagePlanogramDtos(IDalContext dalContext, List<WorkpackageDto> workpackageDtoList, List<PlanogramDto> planogramDtoList)
        {
            List<WorkpackagePlanogramDto> dtoList = new List<WorkpackagePlanogramDto>();

            using (var dal = dalContext.GetDal<IWorkpackagePlanogramDal>())
            {
                foreach (WorkpackageDto packageDto in workpackageDtoList)
                {
                    foreach (PlanogramDto planogramDto in planogramDtoList)
                    {
                        //Create
                        WorkpackagePlanogramDto dto = new WorkpackagePlanogramDto();
                        SetPropertyValues(dto, false);
                        dto.WorkpackageId = packageDto.Id;
                        dto.UserName = planogramDto.UserName;

                        dto.DestinationPlanogramId = Convert.ToInt32(planogramDto.Id);
                        if (planogramDto.Id != null)
                        {
                            dto.SourcePlanogramId = Convert.ToInt32(planogramDto.Id);
                        }

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<WorkpackagePerformanceDto> InsertWorkpackagePerformanceDtos(IDalContext dalContext, List<WorkpackageDto> workpackageDtoList)
        {
            List<WorkpackagePerformanceDto> dtoList = new List<WorkpackagePerformanceDto>();

            using (var dal = dalContext.GetDal<IWorkpackagePerformanceDal>())
            {
                foreach (WorkpackageDto packageDto in workpackageDtoList)
                {
                    WorkpackagePerformanceDto dto = new WorkpackagePerformanceDto();
                    dto.WorkpackageId = packageDto.Id;
                        
                    //Insert
                    dal.Insert(dto);

                    //Add to list
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }

        public static List<PlanogramAssemblyDto> InsertPlanogramAssemblyDtos(IDalContext dalContext, Int32 numberToInsertPerPlanogram, List<PlanogramDto> planogramDtoList)
        {
            List<PlanogramAssemblyDto> dtoList = new List<PlanogramAssemblyDto>();

            using (var dal = dalContext.GetDal<IPlanogramAssemblyDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    for (int i = 0; i < numberToInsertPerPlanogram; i++)
                    {
                        //Create
                        PlanogramAssemblyDto dto = new PlanogramAssemblyDto();
                        SetPropertyValues(dto, false);

                        //Set parent
                        dto.PlanogramId = planogramDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramImageDto> InsertPlanogramImageDtos(IDalContext dalContext, Int32 numberToInsertPerPlanogram, List<PlanogramDto> planogramDtoList)
        {
            List<PlanogramImageDto> dtoList = new List<PlanogramImageDto>();

            using (var dal = dalContext.GetDal<IPlanogramImageDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    for (int i = 0; i < numberToInsertPerPlanogram; i++)
                    {
                        //Create
                        PlanogramImageDto dto = new PlanogramImageDto();
                        SetPropertyValues(dto, false);

                        //Set parent
                        dto.PlanogramId = planogramDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramMetadataImageDto> InsertPlanogramMetadataImageDtos(IDalContext dalContext, Int32 numberToInsertPerPlanogram, List<PlanogramDto> planogramDtoList)
        {
            List<PlanogramMetadataImageDto> dtoList = new List<PlanogramMetadataImageDto>();

            using (var dal = dalContext.GetDal<IPlanogramMetadataImageDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    for (int i = 0; i < numberToInsertPerPlanogram; i++)
                    {
                        //Create
                        PlanogramMetadataImageDto dto = new PlanogramMetadataImageDto();
                        SetPropertyValues(dto, false);

                        //Set parent
                        dto.PlanogramId = planogramDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramInventoryDto> InsertPlanogramInventoryDtos(IDalContext dalContext, List<PlanogramDto> parentPlanogramDtos)
        {
            var dtoList = new List<PlanogramInventoryDto>();
            using (var dal = dalContext.GetDal<IPlanogramInventoryDal>())
            {
                foreach (var planogramDto in parentPlanogramDtos)
                {
                    var dto = new PlanogramInventoryDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramId = planogramDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramComponentDto> InsertPlanogramComponentDtos(IDalContext dalContext, Int32 numberToInsertPerPlanogram, List<PlanogramDto> planogramDtoList)
        {
            List<PlanogramComponentDto> dtoList = new List<PlanogramComponentDto>();

            using (var dal = dalContext.GetDal<IPlanogramComponentDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    for (int i = 0; i < numberToInsertPerPlanogram; i++)
                    {
                        //Create
                        PlanogramComponentDto dto = new PlanogramComponentDto();
                        SetPropertyValues(dto, new List<String>() { "ImageIdFront",
                            "ImageIdBack",
                            "ImageIdTop",
                            "ImageIdBottom",
                            "ImageIdLeft",
                            "ImageIdRight"}, false);

                        //Set parent
                        dto.PlanogramId = planogramDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramAssemblyComponentDto> InsertPlanogramAssemblyComponentDtos(IDalContext dalContext, List<PlanogramAssemblyDto> planogramAssemblyDtoList, List<PlanogramComponentDto> planogramComponentDtoList)
        {
            List<PlanogramAssemblyComponentDto> dtoList = new List<PlanogramAssemblyComponentDto>();

            using (var dal = dalContext.GetDal<IPlanogramAssemblyComponentDal>())
            {
                foreach (PlanogramAssemblyDto planogramAssemblyDto in planogramAssemblyDtoList)
                {
                    foreach (PlanogramComponentDto planogramComponentDto in planogramComponentDtoList)
                    {
                        //Create
                        PlanogramAssemblyComponentDto dto = new PlanogramAssemblyComponentDto();
                        SetPropertyValues(dto, false);

                        //Set parent
                        dto.PlanogramAssemblyId = planogramAssemblyDto.Id;
                        dto.PlanogramComponentId = planogramComponentDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramEventLogDto> InsertPlanogramEventLogDtos(IDalContext dalContext, Int32 numberToInsertPerPlanogram, List<PlanogramDto> planogramDtoList)
        {
            List<PlanogramEventLogDto> dtoList = new List<PlanogramEventLogDto>();

            using (var dal = dalContext.GetDal<IPlanogramEventLogDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    for (int i = 0; i < numberToInsertPerPlanogram; i++)
                    {
                        //Create
                        PlanogramEventLogDto dto = new PlanogramEventLogDto();
                        SetPropertyValues(dto, false);

                        //Set parent
                        dto.PlanogramId = planogramDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }
            return dtoList;
        }

        public static List<PlanogramFixtureDto> InsertPlanogramFixtureDtos(IDalContext dalContext, Int32 numberToInsertPerPlanogram, List<PlanogramDto> planogramDtoList)
        {
            List<PlanogramFixtureDto> dtoList = new List<PlanogramFixtureDto>();

            using (var dal = dalContext.GetDal<IPlanogramFixtureDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    for (int i = 0; i < numberToInsertPerPlanogram; i++)
                    {
                        //Create
                        PlanogramFixtureDto dto = new PlanogramFixtureDto();
                        SetPropertyValues(dto, false);

                        //Set parent
                        dto.PlanogramId = planogramDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramFixtureItemDto> InsertPlanogramFixtureItemDtos(IDalContext dalContext, List<PlanogramDto> planogramDtoList, List<PlanogramFixtureDto> planogramFixtureDtoList)
        {
            List<PlanogramFixtureItemDto> dtoList = new List<PlanogramFixtureItemDto>();

            using (var dal = dalContext.GetDal<IPlanogramFixtureItemDal>())
            {
                foreach (PlanogramFixtureDto planogramFixtureDto in planogramFixtureDtoList)
                {
                    foreach (PlanogramDto planogramDto in planogramDtoList)
                    {
                        //Create
                        PlanogramFixtureItemDto dto = new PlanogramFixtureItemDto();
                        SetPropertyValues(dto, false);

                        //Set parents
                        dto.PlanogramId = planogramDto.Id;
                        dto.PlanogramFixtureId = planogramFixtureDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramFixtureAssemblyDto> InsertPlanogramFixtureAssemblyDtos(IDalContext dalContext, List<PlanogramAssemblyDto> planogramAssemblyDtoList, List<PlanogramFixtureDto> planogramFixtureDtoList)
        {
            List<PlanogramFixtureAssemblyDto> dtoList = new List<PlanogramFixtureAssemblyDto>();

            using (var dal = dalContext.GetDal<IPlanogramFixtureAssemblyDal>())
            {
                foreach (PlanogramFixtureDto planogramFixtureDto in planogramFixtureDtoList)
                {
                    foreach (PlanogramAssemblyDto planogramAssemblyDto in planogramAssemblyDtoList)
                    {
                        //Create
                        PlanogramFixtureAssemblyDto dto = new PlanogramFixtureAssemblyDto();
                        SetPropertyValues(dto, false);

                        //Set parents
                        dto.PlanogramAssemblyId = planogramAssemblyDto.Id;
                        dto.PlanogramFixtureId = planogramFixtureDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramFixtureComponentDto> InsertPlanogramFixtureComponentDtos(IDalContext dalContext, List<PlanogramComponentDto> planogramComponentDtoList, List<PlanogramFixtureDto> planogramFixtureDtoList)
        {
            List<PlanogramFixtureComponentDto> dtoList = new List<PlanogramFixtureComponentDto>();

            using (var dal = dalContext.GetDal<IPlanogramFixtureComponentDal>())
            {
                foreach (PlanogramFixtureDto planogramFixtureDto in planogramFixtureDtoList)
                {
                    foreach (PlanogramComponentDto planogramComponentDto in planogramComponentDtoList)
                    {
                        //Create
                        PlanogramFixtureComponentDto dto = new PlanogramFixtureComponentDto();
                        SetPropertyValues(dto, false);

                        //Set parents
                        dto.PlanogramComponentId = planogramComponentDto.Id;
                        dto.PlanogramFixtureId = planogramFixtureDto.Id;

                        //Insert
                        dal.Insert(dto);

                        //Add to list
                        dtoList.Add(dto);
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramAnnotationDto> InsertPlanogramAnnotationDtos(IDalContext dalContext, List<PlanogramDto> planogramDtoList,
            List<PlanogramAssemblyComponentDto> planogramAssemblyComponentDtoList,
            List<PlanogramFixtureItemDto> planogramFixtureItemDtoList)
        {
            List<PlanogramAnnotationDto> dtoList = new List<PlanogramAnnotationDto>();

            using (var dal = dalContext.GetDal<IPlanogramAnnotationDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    foreach (PlanogramAssemblyComponentDto planogramAssemblyComponentDto in planogramAssemblyComponentDtoList)
                    {
                        foreach (PlanogramFixtureItemDto planogramFixtureItemDto in planogramFixtureItemDtoList)
                        {
                            //Create
                            PlanogramAnnotationDto dto = new PlanogramAnnotationDto();
                            SetPropertyValues(dto, false);

                            //Set parents
                            dto.PlanogramId = planogramDto.Id;
                            dto.PlanogramAssemblyComponentId = planogramAssemblyComponentDto.Id;
                            dto.PlanogramFixtureItemId = planogramFixtureItemDto.Id;
                            dto.PlanogramFixtureAssemblyId = null;
                            dto.PlanogramFixtureComponentId = null;
                            dto.PlanogramPositionId = null;
                            dto.PlanogramSubComponentId = null;

                            //Insert
                            dal.Insert(dto);

                            //Add to list
                            dtoList.Add(dto);
                        }
                    }
                }
            }

            return dtoList;
        }

        public static List<PlanogramSubComponentDto> InsertPlanogramSubComponentDtos(IDalContext dalContext, List<PlanogramComponentDto> planogramComponentDtoList)
        {
            List<PlanogramSubComponentDto> dtoList = new List<PlanogramSubComponentDto>();

            using (var dal = dalContext.GetDal<IPlanogramSubComponentDal>())
            {
                foreach (PlanogramComponentDto planogramComponentDto in planogramComponentDtoList)
                {
                    //Create
                    PlanogramSubComponentDto dto = new PlanogramSubComponentDto();
                    SetPropertyValues(dto, new List<String>() { "ImageIdFront",
                            "ImageIdBack",
                            "ImageIdTop",
                            "ImageIdBottom",
                            "ImageIdLeft",
                            "ImageIdRight"}, false);

                    //Set parent
                    dto.PlanogramComponentId = planogramComponentDto.Id;

                    //Insert
                    dal.Insert(dto);

                    //Add to list
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }

        public static List<PlanogramValidationTemplateDto> InsertPlanogramValidationTemplateDtos(IDalContext dalContext, List<PlanogramDto> parentPlanogramDtos)
        {
            var dtoList = new List<PlanogramValidationTemplateDto>();
            using (var dal = dalContext.GetDal<IPlanogramValidationTemplateDal>())
            {
                foreach (var planogramDto in parentPlanogramDtos)
                {
                    var dto = new PlanogramValidationTemplateDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramId = planogramDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramValidationTemplateGroupDto> InsertPlanogramValidationTemplateGroupDtos(IDalContext dalContext, List<PlanogramValidationTemplateDto> parentPlanogramValidationTemplateDtos)
        {
            var dtoList = new List<PlanogramValidationTemplateGroupDto>();
            using (var dal = dalContext.GetDal<IPlanogramValidationTemplateGroupDal>())
            {
                foreach (var planogramValidationTemplateDto in parentPlanogramValidationTemplateDtos)
                {
                    var dto = new PlanogramValidationTemplateGroupDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramValidationTemplateId = planogramValidationTemplateDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        public static List<PlanogramValidationTemplateGroupMetricDto> InsertPlanogramValidationTemplateGroupMetricDtos(IDalContext dalContext, List<PlanogramValidationTemplateGroupDto> parentPlanogramValidationTemplateGroupDtos)
        {
            var dtoList = new List<PlanogramValidationTemplateGroupMetricDto>();
            using (var dal = dalContext.GetDal<IPlanogramValidationTemplateGroupMetricDal>())
            {
                foreach (var planogramValidationTemplateGroupDto in parentPlanogramValidationTemplateGroupDtos)
                {
                    var dto = new PlanogramValidationTemplateGroupMetricDto();
                    SetPropertyValues(dto, false);
                    dto.PlanogramValidationTemplateGroupId = planogramValidationTemplateGroupDto.Id;
                    dal.Insert(dto);
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }


        public static List<PlanogramProductDto> InsertPlanogramProductDtos(IDalContext dalContext, List<PlanogramDto> planogramDtoList)
        {
            List<PlanogramProductDto> dtoList = new List<PlanogramProductDto>();

            using (var dal = dalContext.GetDal<IPlanogramProductDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    //Create
                    PlanogramProductDto dto = new PlanogramProductDto();
                    SetPropertyValues(dto, new List<String>() { 
                            "PlanogramImageIdFront",
                            "PlanogramImageIdBack",
                            "PlanogramImageIdTop",
                            "PlanogramImageIdBottom",
                            "PlanogramImageIdLeft",
                            "PlanogramImageIdRight",
                    
                            "PlanogramImageIdAlternateFront",
                            "PlanogramImageIdAlternateBack",
                            "PlanogramImageIdAlternateTop",
                            "PlanogramImageIdAlternateBottom",
                            "PlanogramImageIdAlternateLeft",
                            "PlanogramImageIdAlternateRight",
                    
                            "PlanogramImageIdDisplayFront",
                            "PlanogramImageIdDisplayBack",
                            "PlanogramImageIdDisplayTop",
                            "PlanogramImageIdDisplayBottom",
                            "PlanogramImageIdDisplayLeft",
                            "PlanogramImageIdDisplayRight",
                    
                            "PlanogramImageIdTrayFront",
                            "PlanogramImageIdTrayBack",
                            "PlanogramImageIdTrayTop",
                            "PlanogramImageIdTrayBottom",
                            "PlanogramImageIdTrayLeft",
                            "PlanogramImageIdTrayRight",
                    
                            "PlanogramImageIdDisplayFront",
                            "PlanogramImageIdDisplayBack",
                            "PlanogramImageIdDisplayTop",
                            "PlanogramImageIdDisplayBottom",
                            "PlanogramImageIdDisplayLeft",
                            "PlanogramImageIdDisplayRight",
                                        
                            "PlanogramImageIdPointOfPurchaseFront",
                            "PlanogramImageIdPointOfPurchaseBack",
                            "PlanogramImageIdPointOfPurchaseTop",
                            "PlanogramImageIdPointOfPurchaseBottom",
                            "PlanogramImageIdPointOfPurchaseLeft",
                            "PlanogramImageIdPointOfPurchaseRight",
                    
                            "PlanogramImageIdCaseFront",
                            "PlanogramImageIdCaseBack",
                            "PlanogramImageIdCaseTop",
                            "PlanogramImageIdCaseBottom",
                            "PlanogramImageIdCaseLeft",
                            "PlanogramImageIdCaseRight"}, false);

                    //Set parent
                    dto.PlanogramId = planogramDto.Id;

                    //Insert
                    dal.Insert(dto);

                    //Add to list
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }

        public static List<PlanogramPositionDto> InsertPlanogramPositionDtos(IDalContext dalContext,
            List<PlanogramDto> planogramDtoList,
            List<PlanogramFixtureItemDto> planogramFixtureItemDtoList,
            List<PlanogramSubComponentDto> planogramSubComponentDtoList,
            List<PlanogramProductDto> planogramProductDtoList)
        {
            List<PlanogramPositionDto> dtoList = new List<PlanogramPositionDto>();

            using (var dal = dalContext.GetDal<IPlanogramPositionDal>())
            {
                foreach (PlanogramDto planogramDto in planogramDtoList)
                {
                    foreach (PlanogramFixtureItemDto planogramFixtureItemDto in planogramFixtureItemDtoList)
                    {
                        foreach (PlanogramSubComponentDto planogramSubComponentDto in planogramSubComponentDtoList)
                        {
                            foreach (PlanogramProductDto planogramProductDto in planogramProductDtoList)
                            {
                                //Create
                                PlanogramPositionDto dto = new PlanogramPositionDto();
                                SetPropertyValues(dto, false);

                                //Set parent
                                dto.PlanogramId = planogramDto.Id;
                                dto.PlanogramFixtureItemId = planogramProductDto.Id;
                                dto.PlanogramSubComponentId = planogramSubComponentDto.Id;
                                dto.PlanogramProductId = planogramProductDto.Id;
                                dto.PlanogramFixtureComponentId = null;
                                dto.PlanogramFixtureAssemblyId = null;
                                dto.PlanogramAssemblyComponentId = null;

                                //Insert
                                dal.Insert(dto);

                                //Add to list
                                dtoList.Add(dto);
                            }
                        }
                    }
                }
            }

            return dtoList;
        }

        #endregion

        #region Planogram Hierarchy

        public static PlanogramHierarchy PopulatePlanogramHierarchy(Int32 entityId)
        {
            //Create a planogram hierarchy
            PlanogramHierarchy planHierarchy = PlanogramHierarchy.FetchByEntityId(entityId);
            planHierarchy.RootGroup.ChildList.Clear();

            PlanogramGroup g1 = PlanogramGroup.NewPlanogramGroup();
            g1.Name = "Canned Foods";
            planHierarchy.RootGroup.ChildList.Add(g1);

            PlanogramGroup g2 = PlanogramGroup.NewPlanogramGroup();
            g2.Name = "Tinned Veg";
            g1.ChildList.Add(g2);

            PlanogramGroup g3 = PlanogramGroup.NewPlanogramGroup();
            g3.Name = "Tinned Fruit";
            g1.ChildList.Add(g3);

            PlanogramGroup g4 = PlanogramGroup.NewPlanogramGroup();
            g4.Name = "Condiments";
            planHierarchy.RootGroup.ChildList.Add(g4);

            PlanogramGroup g5 = PlanogramGroup.NewPlanogramGroup();
            g5.Name = "Sweets";
            g4.ChildList.Add(g5);

            PlanogramGroup g6 = PlanogramGroup.NewPlanogramGroup();
            g6.Name = "Chocolate";
            g4.ChildList.Add(g6);

            PlanogramGroup g7 = PlanogramGroup.NewPlanogramGroup();
            g7.Name = "Dressings";
            planHierarchy.RootGroup.ChildList.Add(g7);

            planHierarchy = planHierarchy.Save();

            return planHierarchy;
        }

        #endregion

        #region Product

        public static List<ProductDto> InsertProductDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId)
        {
            return InsertProductDtos(dalFactory, NewDefaultProductDtos(numToInsert, entityId)).ToList();
        }

        internal static List<ProductDto> InsertProductDtos(DalFactory dalFactory, Int32 numToInsert, string mask, int entityId)
        {
            return InsertProductDtos(dalFactory, NewDefaultProductDtos(numToInsert, entityId, mask)).ToList();
        }

        /// <summary>
        ///     Inserts the <see cref="ProductDto"/> collection obtained from <paramref name="productDtos"/> 
        /// into the DAL obtained from <paramref name="dalFactory"/> and the <paramref name="entityId"/>.
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="productDtos"></param>
        /// <returns></returns>
        public static List<ProductDto> InsertProductDtos(IDalFactory dalFactory, List<ProductDto> productDtos)
        {
            if (productDtos == null) return new List<ProductDto>();

            if (!productDtos.Any()) return productDtos;

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IProductDal>())
                using (var customAttributeDal = dalContext.GetDal<ICustomAttributeDataDal>())
                {

                    int idx = 1;
                    // insert into the dal
                    foreach (ProductDto dto in productDtos)
                    {
                        dal.Insert(dto);
                        customAttributeDal.Insert(new CustomAttributeDataDto
                        {
                            Id = idx++,
                            ParentId = dto.Id,
                            ParentType = (Byte)CustomAttributeDataParentType.Product
                        });
                    }
                }
                dalContext.Commit();
            }

            return productDtos;
        }

        /// <summary>
        ///     Updates the <see cref="ProductDto"/> collection obtained from <paramref name="productDtos"/> 
        /// into the DAL obtained from <paramref name="dalFactory"/> and the <paramref name="entityId"/>.
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="productDtos"></param>
        /// <returns></returns>
        public static List<ProductDto> UpdateProductDtos(IDalFactory dalFactory, List<ProductDto> productDtos)
        {
            if (productDtos == null) return new List<ProductDto>();

            if (!productDtos.Any()) return productDtos;

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IProductDal>())
                {

                    foreach (ProductDto dto in productDtos)
                    {
                        dal.Update(dto);
                    }
                }
                dalContext.Commit();
            }

            return productDtos;
        }

        public static List<ProductDto> NewDefaultProductDtos(Int32 numToInsert, Int32 entityId, string mask = "gtin{0}")
        {
            var productDtos = new List<ProductDto>();
            for (var i = 0; i < numToInsert; i++)
            {
                productDtos.Add(NewDefaultProductDto(i, entityId, mask, mask));
            }
            return productDtos;
        }

        /// <summary>
        ///     Instantiates a new <see cref="ProductDto"/> with placeholder values.
        /// </summary>
        /// <param name="id">The Id value for the <see cref="ProductDto"/>.</param>
        /// <param name="entityId">The EntityId the <see cref="ProductDto"/> belongs to.</param>
        /// <param name="nameMask">[Optional] The mask used to create the name for the <see cref="ProductDto"/>. Defaults to name{0}. {0} = Id.</param>
        /// <param name="gtinMask">[Optional] The mask used to create the GTIN for the <see cref="ProductDto"/>. Defaults to gtin{0}. {0} = Id.</param>
        /// <returns>A new instance of <see cref="ProductDto"/> with valuid placehoder values in its properties.</returns>
        public static ProductDto NewDefaultProductDto(Int32 id, Int32 entityId, String nameMask = "name{0}", String gtinMask = "gtin{0}")
        {
            return new ProductDto()
            {
                Id = id,
                EntityId = entityId,
                ReplacementProductId = id,
                Gtin = String.Format(gtinMask, id),
                Name = String.Format(nameMask, id),

                Height = 101,
                Width = 102,
                Depth = 103,
                DisplayHeight = 104,
                DisplayWidth = 105,
                DisplayDepth = 106,
                AlternateHeight = 107,
                AlternateWidth = 108,
                AlternateDepth = 109,

                NumberOfPegHoles = 9,
                PegX = 201,
                PegX2 = 202,
                PegX3 = 203,
                PegY = 204,
                PegY2 = 205,
                PegY3 = 206,
                PegProngOffsetX = 207,
                PegProngOffsetY = 208,
                PegDepth = 209,

                SqueezeHeight = 301,
                SqueezeWidth = 302,
                SqueezeDepth = 303,

                NestingHeight = 401,
                NestingWidth = 402,
                NestingDepth = 403,

                CasePackUnits = 22,
                CaseHigh = 1,
                CaseWide = 1,
                CaseDeep = 1,
                CaseHeight = 1,
                CaseWidth = 1,
                CaseDepth = 1,
                MaxStack = 34,
                MaxTopCap = 25,
                MaxRightCap = 36,
                MinDeep = 37,
                MaxDeep = 38,
                TrayPackUnits = 10,
                TrayHigh = 41,
                TrayWide = 42,
                TrayDeep = 43,
                TrayHeight = 1,
                TrayWidth = 1,
                TrayDepth = 1,
                TrayThickHeight = 44,
                TrayThickWidth = 45,
                TrayThickDepth = 46,

                FrontOverhang = 401,
                FingerSpaceAbove = 402,
                FingerSpaceToTheSide = 403,

                StatusType = 2,
                OrientationType = 4,
                MerchandisingStyle = 5,

                IsFrontOnly = true,
                IsPlaceHolderProduct = true,
                IsActive = false,
                CanBreakTrayUp = true,
                CanBreakTrayDown = false,
                CanBreakTrayBack = true,
                CanBreakTrayTop = false,
                ForceMiddleCap = true,
                ForceBottomCap = false,
                Shape = "text",
                ShapeType = 0,
                FillColour = 0,
                FillPatternType = 1,
                PointOfPurchaseDescription = "text",
                ShortDescription = "text",
                Subcategory = "text",
                CustomerStatus = "text",
                Colour = "text",
                Flavour = "text",
                PackagingShape = "text",
                PackagingType = "text",
                CountryOfOrigin = "text",
                CountryOfProcessing = "text",
                ShelfLife = 1,
                DeliveryFrequencyDays = null,
                DeliveryMethod = "text",
                Brand = "text",
                VendorCode = "text",
                Vendor = "text",
                ManufacturerCode = "text",
                Manufacturer = "text",
                Size = "text",
                UnitOfMeasure = "text",
                DateIntroduced = null,
                DateDiscontinued = null,
                DateEffective = null,
                Health = "text",
                CorporateCode = "text",
                Barcode = "text",
                SellPrice = null,
                SellPackCount = null,
                SellPackDescription = "text",
                RecommendedRetailPrice = null,
                ManufacturerRecommendedRetailPrice = null,
                CostPrice = null,
                CaseCost = null,
                TaxRate = null,
                ConsumerInformation = "text",
                Texture = "text",
                StyleNumber = null,
                Pattern = "text",
                Model = "text",
                GarmentType = "text",
                IsPrivateLabel = true,
                IsNewProduct = true,
                DateCreated = DateTime.Now,
                DateLastModified = DateTime.Now,
                DateDeleted = null,
            };
        }

        #endregion

        #region Product Hierarchy

        /// <summary>
        /// Insert collection of product hierarchy dtos
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="numToInsert"></param>
        /// <returns></returns>
        public static List<ProductHierarchyDto> InsertProductHierarchyDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId)
        {
            List<ProductHierarchyDto> dtoList = new List<ProductHierarchyDto>();

            //insert hierarchies
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    for (int i = 1; i <= numToInsert; i++)
                    {

                        dtoList.Add(
                        new ProductHierarchyDto()
                        {
                            Id = i,
                            Name = "heirarchy" + i.ToString(),
                            EntityId = entityId,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        });
                    }
                    foreach (ProductHierarchyDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ProductLevelDto> InsertProductLevelDtos(IDalFactory dalFactory, Int32 hierarchyId, Int32 numToInsert)
        {
            List<ProductLevelDto> dtoList = new List<ProductLevelDto>();

            //insert hierarchies
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    //add 5 dtos
                    for (int i = 1; i <= numToInsert; i++)
                    {

                        dtoList.Add(
                        new ProductLevelDto()
                        {
                            Name = "level" + i.ToString(),
                            ProductHierarchyId = hierarchyId,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now
                        });
                    }
                    Int32? lastId = null;
                    foreach (ProductLevelDto dto in dtoList)
                    {
                        dto.ParentLevelId = lastId;
                        dal.Insert(dto);
                        lastId = dto.Id;
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ProductGroupDto> InsertProductGroupDtos(IDalFactory dalFactory, List<ProductLevelDto> levelList, Int32 numPerLevel)
        {
            List<ProductGroupDto> dtoList = new List<ProductGroupDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    Int32 groupNum = 1;


                    //cycle through levels
                    List<ProductGroupDto> lastLevelDtos = new List<ProductGroupDto>();
                    foreach (ProductLevelDto levelDto in levelList)
                    {
                        List<ProductGroupDto> levelDtos = new List<ProductGroupDto>();

                        for (int i = 1; i <= numPerLevel; i++)
                        {
                            ProductGroupDto groupDto = new ProductGroupDto()
                            {
                                Name = "name" + groupNum,
                                Code = "code" + groupNum,
                                ProductHierarchyId = levelDto.ProductHierarchyId,
                                ProductLevelId = levelDto.Id,
                                DateCreated = DateTime.Now,
                                DateLastModified = DateTime.Now
                            };

                            //pick a parent
                            Int32 parentCount = lastLevelDtos.Count;
                            if (parentCount != 0)
                            {
                                groupDto.ParentGroupId = lastLevelDtos[parentCount - 1].Id;
                            }


                            dal.Insert(groupDto);
                            groupNum++;

                            levelDtos.Add(groupDto);
                            dtoList.Add(groupDto);



                            //only add one group if this is the root level
                            if (levelDto.ParentLevelId == null)
                            {
                                break;
                            }

                        }

                        lastLevelDtos = levelDtos;
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        /// <summary>
        /// Populate a product hierarchy
        /// </summary>
        /// <param name="hierarchyId"></param>
        /// <returns></returns>
        public static ProductHierarchy PopulateProductHierarchy(Int32 hierarchyId)
        {
            ProductHierarchy hierarchy = ProductHierarchy.FetchById(hierarchyId);

            //add 2 levels
            ProductLevel level1 = hierarchy.RootLevel.ChildLevel;
            if (level1 == null)
            {
                level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
                level1.Name = "level1";
            }
            ProductLevel level2 = level1.ChildLevel;
            if (level2 == null)
            {
                level2 = hierarchy.InsertNewChildLevel(level1);
                level2.Name = "level2";
            }


            //add 3 level1 groups
            ProductGroup level1Group1 = ProductGroup.NewProductGroup(level1.Id);
            level1Group1.Name = "xlevel1Group1";
            level1Group1.Code = "xlevel1Group1Code";
            ProductGroup level1Group2 = ProductGroup.NewProductGroup(level1.Id);
            level1Group2.Name = "xlevel1Group2";
            level1Group2.Code = "xlevel1Group2Code";
            ProductGroup level1Group3 = ProductGroup.NewProductGroup(level1.Id);
            level1Group3.Name = "xlevel1Group3";
            level1Group3.Code = "xlevel1Group3Code";
            hierarchy.RootGroup.ChildList.Add(level1Group1);
            hierarchy.RootGroup.ChildList.Add(level1Group2);
            hierarchy.RootGroup.ChildList.Add(level1Group3);

            //add 3 level2 groups per level1 group
            foreach (ProductGroup level1Group in hierarchy.RootGroup.ChildList)
            {
                for (int i = 1; i <= 3; i++)
                {
                    ProductGroup level2Group = ProductGroup.NewProductGroup(level2.Id);
                    level2Group.Name = "level2group" + i.ToString() + level1Group.Name;
                    level2Group.Code = "level2groupCode" + i.ToString() + level1Group.Code;
                    level1Group.ChildList.Add(level2Group);
                }
            }

            return hierarchy.Save();
        }

        /// <summary>
        /// Populate a specified hiearchy  with products
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <param name="numProductsToInsertPerGroup"></param>
        /// <returns></returns>
        public static List<Product> PopulateProductHierarchyWithProducts(ProductHierarchy hierarchy, Int32 numProductsToInsertPerGroup)
        {
            List<Product> productList = new List<Product>();

            List<ProductGroup> productGroups = hierarchy.EnumerateAllGroups().Where(g => g.ProductLevelId.Equals(hierarchy.EnumerateAllLevels().Last().Id)).ToList();

            foreach (ProductGroup group in productGroups)
            {
                for (int i = 1; i <= numProductsToInsertPerGroup; i++)
                {
                    Product product = Product.NewProduct(hierarchy.EntityId);
                    product.ProductGroupId = group.Id;
                    product.Gtin = Guid.NewGuid().ToString().Substring(0, 14);
                    product.Name = Guid.NewGuid().ToString().Substring(0, 20);
                    product.CasePackUnits = 1;
                    product = product.Save();

                    productList.Add(product);
                }
            }

            return productList;
        }


        #endregion

        #region Product Universe

        public static List<ProductUniverseDto> InsertProductUniverseDtos(IDalFactory dalFactory, List<EntityDto> entityDtoList, List<ProductGroupDto> productGroupDtoList)
        {
            return InsertProductUniverseDtos(dalFactory, entityDtoList, productGroupDtoList.Select(p => p.Id).ToList());
        }
        public static List<ProductUniverseDto> InsertProductUniverseDtos(IDalFactory dalFactory, List<EntityDto> entityDtoList, List<Int32> productGroupIdList)
        {
            return InsertProductUniverseDtos(dalFactory, entityDtoList.Select(e => e.Id).ToList(), productGroupIdList);
        }
        public static List<ProductUniverseDto> InsertProductUniverseDtos(IDalFactory dalFactory, List<Int32> entityIdList, List<Int32> productGroupIdList)
        {
            List<ProductUniverseDto> universeDtoList = new List<ProductUniverseDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                {
                    foreach (Int32 entityId in entityIdList)
                    {
                        foreach (Int32 productGroupId in productGroupIdList)
                        {
                            ProductUniverseDto universeDto = new ProductUniverseDto()
                            {
                                EntityId = entityId,
                                Name = Guid.NewGuid().ToString(),
                                ProductGroupId = productGroupId,
                                RowVersion = new RowVersion(),
                                UniqueContentReference = Guid.NewGuid()
                            };
                            dal.Insert(universeDto);
                            universeDtoList.Add(universeDto);
                        }
                    }
                }
            }
            return universeDtoList;
        }

        public static List<ProductUniverseProductDto> InsertProductUniverseProductDtos(IDalFactory dalFactory, List<ProductUniverseDto> productUniverseDtoList, List<ProductDto> productDtoList)
        {
            List<ProductUniverseProductDto> productUniverseProductDtoList = new List<ProductUniverseProductDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductUniverseProductDal dal = dalContext.GetDal<IProductUniverseProductDal>())
                {
                    foreach (ProductUniverseDto productUniverseDto in productUniverseDtoList)
                    {
                        foreach (ProductDto productDto in productDtoList)
                        {
                            ProductUniverseProductDto dto = new ProductUniverseProductDto()
                            {
                                Gtin = productDto.Gtin,
                                Name = productDto.Name,
                                ProductId = productDto.Id,
                                ProductUniverseId = productUniverseDto.Id
                            };
                            dal.Insert(dto);
                            productUniverseProductDtoList.Add(dto);
                        }
                    }
                }
            }
            return productUniverseProductDtoList;
        }



        #endregion

        #region Renumbering Strategy

        /// <summary>
        ///     Inserts the specified <paramref name="numToInsert"/> of <see cref="RenumberingStrategyDto"/> into the dal.
        /// </summary>
        /// <param name="dalFactory">Dal factory to use.</param>
        /// <param name="numToInsert">Number of dtos to insert.</param>
        /// <param name="entityId">EntityId for the dtos to be inserted.</param>
        /// <returns>An enumeration of <see cref="RenumberingStrategyDto"/> inserted into the dal.</returns>
        public static IEnumerable<RenumberingStrategyDto> InsertRenumberingStrategyDtos(IDalFactory dalFactory, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<RenumberingStrategyDto>();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IRenumberingStrategyDal>())
                {
                    for (var i = 0; i < numToInsert; i++)
                    {
                        dtos.Add(new RenumberingStrategyDto
                        {
                            DateCreated = DateTime.UtcNow,
                            DateDeleted = null,
                            DateLastModified = DateTime.UtcNow,
                            EntityId = entityId,
                            Id = i,
                            Name = String.Format("Renumbering Strategy #{0}", i)
                        });
                    }

                    dtos.ForEach(dal.Insert);
                }
                dalContext.Commit();
            }
            return dtos;
        }

        #endregion

        #region System Settings

        /// <summary>
        /// Inserts the default config set into the database
        /// </summary>
        /// <param name="dalFactory"></param>
        public static SystemSettings InsertDefaultSettings(IDalFactory dalFactory)
        {
            const int entityId = 1;
            List<SystemSettingsDto> dtoList = new List<SystemSettingsDto>();

            dtoList.Add(new SystemSettingsDto()
            {
                Key = "Compression Name",
                Value = "C1",
                UserId = null,
                EntityId = null
            });


            // insert into the data source
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ISystemSettingsDal dal = dalContext.GetDal<ISystemSettingsDal>())
                {
                    foreach (SystemSettingsDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();

                return SystemSettings.FetchSystemSettings(dalContext, entityId);
            }
        }

        #endregion

        #region Reporting

        #region Dto Inserts

        public static List<ReportDto> InsertReportDtos(IDalFactory dalFactory, Int32 numToInsert, List<DataModelInfoDto> dataModelInfos)
        {
            List<ReportDto> dtoList = new List<ReportDto>();

            Int32 lastReportTypeIndex = -1;

            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportDal dal = dalContext.GetDal<IReportDal>())
                {
                    //add some dtos
                    for (int i = 1; i <= numToInsert; i++)
                    {
                        Int32 reportTypeIndex = 0;
                        if (lastReportTypeIndex < dataModelInfos.Count - 1)
                        {
                            reportTypeIndex = lastReportTypeIndex + 1;
                        }
                        else
                        {
                            reportTypeIndex = 0;
                        }
                        lastReportTypeIndex = reportTypeIndex;

                        dtoList.Add(
                        new ReportDto()
                        {
                            Name = Guid.NewGuid().ToString(),
                            Description = "description" + i.ToString(),
                            PaperOrientation = (Byte)ReportPaperOrientation.Landscape,
                            PaperSize = (Byte)ReportPaperSize.A5,
                            PaperMarginType = (Byte)ReportPaperMargin.Wide,
                            PaperMarginTop = ReportPaperMarginHelper.GetPageMarginTop(ReportPaperMargin.Wide),
                            PaperMarginLeft = ReportPaperMarginHelper.GetPageMarginLeft(ReportPaperMargin.Wide),
                            PaperMarginBottom = ReportPaperMarginHelper.GetPageMarginBottom(ReportPaperMargin.Wide),
                            PaperMarginRight = ReportPaperMarginHelper.GetPageMarginRight(ReportPaperMargin.Wide),
                            DateLastExecuted = DateTime.UtcNow.AddDays(-2),
                            DataModelId = dataModelInfos[reportTypeIndex].Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        });
                    }
                    foreach (ReportDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportDto> InsertReportDtos(IDalFactory dalFactory, Int32 numToInsert, List<DataModelDto> dataModelDtos)
        {
            List<ReportDto> dtoList = new List<ReportDto>();

            Int32 lastReportTypeIndex = -1;

            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportDal dal = dalContext.GetDal<IReportDal>())
                {
                    //add some dtos
                    for (int i = 1; i <= numToInsert; i++)
                    {
                        Int32 reportTypeIndex = 0;
                        if (lastReportTypeIndex < dataModelDtos.Count - 1)
                        {
                            reportTypeIndex = lastReportTypeIndex + 1;
                        }
                        else
                        {
                            reportTypeIndex = 0;
                        }
                        lastReportTypeIndex = reportTypeIndex;

                        dtoList.Add(
                        new ReportDto()
                        {
                            Name = Guid.NewGuid().ToString(),
                            Description = "description" + i.ToString(),
                            PaperOrientation = (Byte)ReportPaperOrientation.Landscape,
                            PaperSize = (Byte)ReportPaperSize.A5,
                            PaperMarginType = (Byte)ReportPaperMargin.Wide,
                            PaperMarginTop = ReportPaperMarginHelper.GetPageMarginTop(ReportPaperMargin.Wide),
                            PaperMarginLeft = ReportPaperMarginHelper.GetPageMarginLeft(ReportPaperMargin.Wide),
                            PaperMarginBottom = ReportPaperMarginHelper.GetPageMarginBottom(ReportPaperMargin.Wide),
                            PaperMarginRight = ReportPaperMarginHelper.GetPageMarginRight(ReportPaperMargin.Wide),
                            DateLastExecuted = DateTime.UtcNow.AddDays(-2),
                            DataModelId = dataModelDtos[reportTypeIndex].Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        });
                    }
                    foreach (ReportDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<DataModelDto> InsertDataModelDtos(IDalFactory dalFactory)
        {
            using (IDalContext context = dalFactory.CreateContext())
            {
                using (IDataModelDal dal = context.GetDal<IDataModelDal>())
                {
                    return dal.FetchAll().ToList();
                }
            }
        }

        public static List<ReportSectionDto> InsertReportSectionDtos(IDalFactory dalFactory, Int32 numToInsertPerReport, List<ReportDto> reportDtos)
        {
            List<ReportSectionDto> dtoList = new List<ReportSectionDto>();

            //insert report sections
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportSectionDal dal = dalContext.GetDal<IReportSectionDal>())
                {
                    foreach (ReportDto reportDto in reportDtos)
                    {
                        //add some dtos
                        for (int i = 1; i <= numToInsertPerReport; i++)
                        {
                            dtoList.Add(
                            new ReportSectionDto()
                            {
                                Name = Guid.NewGuid().ToString(),
                                Description = "description" + i.ToString(),
                                ReportId = reportDto.Id,
                                Height = 1.23F
                            });
                        }
                    }

                    foreach (ReportSectionDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportComponentDto> InsertReportComponentDtos(IDalFactory dalFactory, Int32 numToInsertPerSection, List<ReportSectionDto> reportSectionDtos)
        {
            List<ReportComponentDto> dtoList = new List<ReportComponentDto>();

            //insert report components
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportComponentDal dal = dalContext.GetDal<IReportComponentDal>())
                {
                    foreach (ReportSectionDto reportSectionDto in reportSectionDtos)
                    {
                        //add some dtos
                        for (int i = 1; i <= numToInsertPerSection; i++)
                        {
                            dtoList.Add(
                            new ReportComponentDto()
                            {
                                Name = Guid.NewGuid().ToString(),
                                Description = "description" + i.ToString(),
                                ReportSectionId = reportSectionDto.Id,
                                ComponentType = (Byte)ReportComponentType.Line,
                                X = 123.45F,
                                Y = 234.56F,
                                Height = 1.23F,
                                Width = 2.34F
                            });
                        }
                    }

                    foreach (ReportComponentDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportComponentDetailDto> InsertReportComponentDetailDtos(IDalFactory dalFactory, Int32 numToInsertPerComponent, List<ReportComponentDto> reportComponentsDtos)
        {
            List<ReportComponentDetailDto> dtoList = new List<ReportComponentDetailDto>();

            //insert report components
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportComponentDetailDal dal = dalContext.GetDal<IReportComponentDetailDal>())
                {
                    foreach (ReportComponentDto reportComponentDto in reportComponentsDtos)
                    {
                        switch (reportComponentDto.ComponentType)
                        {
                            case (Byte)ReportComponentType.TextBox:
                                dtoList.Add(new ReportComponentDetailDto()
                                {
                                    Key = "TextBoxFontSize",
                                    Value = "9",
                                    ReportComponentId = reportComponentDto.Id
                                });
                                dtoList.Add(new ReportComponentDetailDto()
                                {
                                    Key = "TextBoxFont",
                                    Value = "Arial",
                                    ReportComponentId = reportComponentDto.Id
                                });
                                break;

                            case (Byte)ReportComponentType.Line:
                                dtoList.Add(new ReportComponentDetailDto()
                                {
                                    Key = "LineEndX",
                                    Value = "9",
                                    ReportComponentId = reportComponentDto.Id
                                });
                                dtoList.Add(new ReportComponentDetailDto()
                                {
                                    Key = "LineEndY",
                                    Value = "12",
                                    ReportComponentId = reportComponentDto.Id
                                });
                                break;

                            case (Byte)ReportComponentType.Field:
                                dtoList.Add(new ReportComponentDetailDto()
                                {
                                    Key = "TextBoxFontSize",
                                    Value = "9",
                                    ReportComponentId = reportComponentDto.Id
                                });
                                dtoList.Add(new ReportComponentDetailDto()
                                {
                                    Key = "TextBoxFontSize",
                                    Value = "9",
                                    ReportComponentId = reportComponentDto.Id
                                });
                                break;
                        }
                    }

                    foreach (ReportComponentDetailDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportFieldDto> InsertReportFieldDtos(IDalFactory dalFactory, Int32 numToInsertPerReport, List<ReportDto> reportDtos)
        {
            List<ReportFieldDto> dtoList = new List<ReportFieldDto>();

            //insert report sections
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportFieldDal dal = dalContext.GetDal<IReportFieldDal>())
                {
                    foreach (ReportDto reportDto in reportDtos)
                    {
                        //add some dtos
                        for (int i = 1; i <= numToInsertPerReport; i++)
                        {
                            dtoList.Add(
                            new ReportFieldDto()
                            {
                                Caption = String.Format("Field {0}", i.ToString()),
                                ReportId = reportDto.Id,
                                IsSortFieldUsed = false,
                            });
                        }
                    }

                    foreach (ReportFieldDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportGroupDto> InsertReportGroupDtos(IDalFactory dalFactory, Int32 numToInsertPerReport, List<ReportDto> reportDtos)
        {
            List<ReportGroupDto> dtoList = new List<ReportGroupDto>();

            //insert report sections
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportGroupDal dal = dalContext.GetDal<IReportGroupDal>())
                {
                    foreach (ReportDto reportDto in reportDtos)
                    {
                        //add some dtos
                        for (int i = 1; i <= numToInsertPerReport; i++)
                        {
                            dtoList.Add(
                            new ReportGroupDto()
                            {
                                Name = String.Format("Group {0}", i.ToString()),
                                ReportId = reportDto.Id,
                                Description = String.Format("Group Description {0}", i.ToString()),
                                GroupLevel = (Byte)(i - 1),
                                SortDirection = (Byte)Reporting.Model.SortDirection.Descending,
                                DataFieldId = "SomeDataField"
                            });
                        }
                    }

                    foreach (ReportGroupDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportPredicateDto> InsertReportPredicateDtos(IDalFactory dalFactory, Int32 numToInsertPerReport, List<ReportDto> reportDtos)
        {
            List<ReportPredicateDto> dtoList = new List<ReportPredicateDto>();
            Random rndm = new Random(1);

            //insert report sections
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportPredicateDal dal = dalContext.GetDal<IReportPredicateDal>())
                {
                    foreach (ReportDto reportDto in reportDtos)
                    {
                        //add some dtos
                        for (int i = 1; i <= numToInsertPerReport; i++)
                        {
                            dtoList.Add(
                            new ReportPredicateDto()
                            {
                                Type = (Byte)ReportPredicateType.None,
                                ReportId = reportDto.Id,
                                ParentId = Convert.ToInt32(rndm.Next(numToInsertPerReport))
                            });
                        }
                    }

                    foreach (ReportPredicateDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportConditionDto> InsertReportConditionDtos(IDalFactory dalFactory, Int32 numToInsertPerReport, List<ReportDto> reportDtos, List<ReportPredicateDto> reportPredicateDtos)
        {
            List<ReportConditionDto> dtoList = new List<ReportConditionDto>();
            Random rndm = new Random(1);

            //insert report sections
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportConditionDal dal = dalContext.GetDal<IReportConditionDal>())
                {
                    foreach (ReportDto reportDto in reportDtos)
                    {
                        foreach (ReportPredicateDto predicateDto in reportPredicateDtos)
                        {
                            //add some dtos
                            for (int i = 1; i <= numToInsertPerReport; i++)
                            {
                                dtoList.Add(
                                new ReportConditionDto()
                                {
                                    PredicateId = predicateDto.Id,
                                    ReportId = reportDto.Id,
                                    DataOperatorId = "EQUALS",
                                    Value = (Object)4,
                                    Value2 = null,
                                    DataFieldId = "SomeDataFieldId"
                                });
                            }
                        }
                    }

                    foreach (ReportConditionDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        public static List<ReportParameterDto> InsertReportParameterDtos(IDalFactory dalFactory, Int32 numToInsertPerReport, List<ReportDto> reportDtos)
        {
            List<ReportParameterDto> dtoList = new List<ReportParameterDto>();
            Random rndm = new Random(1);

            //insert report parameters
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IReportParameterDal dal = dalContext.GetDal<IReportParameterDal>())
                {
                    foreach (ReportDto reportDto in reportDtos)
                    {
                        //add some dtos
                        for (int i = 1; i <= numToInsertPerReport; i++)
                        {
                            dtoList.Add(
                            new ReportParameterDto()
                            {
                                ReportId = reportDto.Id,
                                DataFieldId = "SomeDataFieldID",
                                Name = String.Format("Parameter {0}", i.ToString()),
                            });
                        }
                    }

                    foreach (ReportParameterDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        #region Full Report Inserts

        public static List<ReportDto> InsertPopulatedReportDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            //Insert Datamodels
            List<DataModelDto> dataModelDtos = InsertDataModelDtos(dalFactory);

            //Insert Reports
            List<ReportDto> reportDtos = InsertReportDtos(dalFactory, numToInsert, dataModelDtos);

            //Insert ReportSections
            List<ReportSectionDto> reportSectionDtos = InsertReportSectionDtos(dalFactory, numToInsert, reportDtos);

            //Insert ReportComponents
            List<ReportComponentDto> reportComponentDtos = InsertReportComponentDtos(dalFactory, numToInsert, reportSectionDtos);

            return reportDtos;
        }

        #endregion

        #endregion

        #region Roles

        public static List<RoleDto> InsertRoleDtos(IDalFactory dalFactory, Int32 numToInsert, Int32 entityId, Boolean addRoleEntities = true)
        {
            List<RoleDto> dtoList = new List<RoleDto>();
            List<RoleEntityDto> roleEntityDtoList = new List<RoleEntityDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    for (int i = 0; i < numToInsert; i++)
                    {
                        dtoList.Add(
                            new RoleDto()
                            {
                                Name = "Distribution Centre " + i.ToString(),
                                Description = "Description " + i.ToString(),
                                IsAdministrator = false,
                            });
                    }
                    foreach (RoleDto dto in dtoList)
                    {
                        dal.Insert(dto);
                        if (addRoleEntities)
                        {
                            //Create a RoleEntity for each inserted Role
                            roleEntityDtoList.Add(
                                new RoleEntityDto()
                                {
                                    RoleId = dto.Id,
                                    EntityId = entityId,
                                });
                        }
                    }
                }
                if (addRoleEntities)
                {
                    //Insert the RoleEntities
                    using (IRoleEntityDal dal = dalContext.GetDal<IRoleEntityDal>())
                    {
                        foreach (RoleEntityDto dto in roleEntityDtoList)
                        {
                            dal.Insert(dto);
                        }
                    }
                }

                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        #region User
        /// <summary>
        /// Inserts the current user into the mock so that the app authenticates
        /// </summary>
        /// <param name="dalFactory"></param>
        public static void InsertUserData(IDalFactory dalFactory)
        {

        }

        public static List<UserDto> InsertUserDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<UserDto> dtoList = new List<UserDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                dtoList = InsertUserDtos(dalContext, numToInsert);
                dalContext.Commit();
            }
            return dtoList;
        }

        public static List<UserDto> InsertUserDtos(IDalContext dalContext, Int32 numToInsert)
        {
            List<UserDto> dtoList = new List<UserDto>();
            Random random = new Random();
            using (IUserDal dal = dalContext.GetDal<IUserDal>())
            {
                for (Int16 i = 1; i <= numToInsert; i++)
                {
                    Int32 id = i + random.Next();
                    dtoList.Add(
                    new UserDto()
                    {
                        Id = id,
                        UserName = "Username" + id.ToString(),
                        FirstName = "FirstName" + id.ToString(),
                        LastName = "LastName" + id.ToString(),
                        Description = "Description",
                        Office = "Alsager",
                        TelephoneNumber = "088",
                        EMailAddress = "Email.Email.com",
                        Title = "Mr/Mrs",
                        Department = "Department",
                        Company = "Galleria",
                        Manager = "Manager",
                        DateDeleted = null,
                    });
                }

                // insert into the dal
                foreach (UserDto dto in dtoList)
                {
                    dal.Insert(dto);
                }
            }

            return dtoList;

        }

        #endregion

        #region Location Space

        public static List<LocationSpaceDto> InsertLocationSpaceDtos(IDalFactory dalFactory, IEnumerable<LocationDto> locationDtoList, Int32 entityId)
        {

            List<LocationSpaceDto> dtoList = new List<LocationSpaceDto>();

            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    //add n dtos
                    foreach (LocationDto location in locationDtoList)
                    {

                        dtoList.Add(
                        new LocationSpaceDto()
                        {
                            RowVersion = new RowVersion(),
                            EntityId = entityId,
                            LocationId = location.Id
                        });
                    }
                    foreach (LocationSpaceDto dto in dtoList)
                    {
                        dal.Insert(dto);

                    }
                }
                dalContext.Commit();
            }
            return dtoList;
        }

        public static List<LocationSpaceProductGroupDto> InsertLocationSpaceProductGroupDtos(IDalFactory dalFactory,
            Int32 locationSpaceId, IEnumerable<ProductGroupDto> productGroupDtos, Int32 specifiedBayCount=1)
        {
            List<LocationSpaceProductGroupDto> dtoList = new List<LocationSpaceProductGroupDto>();

            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                {
                    //add n dtos
                    for (int i = 0; i < productGroupDtos.Count(); i++)
                    {

                        dtoList.Add(
                        new LocationSpaceProductGroupDto()
                        {
                            ProductGroupId = productGroupDtos.ElementAt(i).Id,
                            LocationSpaceId = locationSpaceId,
                            BayCount = specifiedBayCount,
                        });
                    }
                    foreach (LocationSpaceProductGroupDto dto in dtoList)
                    {
                        dal.Insert(dto);

                    }
                }
                dalContext.Commit();
            }
            return dtoList;
        }

        public static List<LocationSpaceBayDto> InsertLocationSpaceBayDtos(IDalFactory dalFactory, Int32 locationSpaceProductGroupId, 
                                            Int32 numToInsert, Single? baySpaceWidth=null)
        {
            List<LocationSpaceBayDto> dtoList = new List<LocationSpaceBayDto>();

            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                {
                    //add n dtos
                    for (int i = 1; i <= numToInsert; i++)
                    {
                        dtoList.Add(
                        new LocationSpaceBayDto()
                        {
                            LocationSpaceProductGroupId = locationSpaceProductGroupId,
                            Order = (Byte)i,
                            Height = (Single)i + 1.1F,
                            Width = (baySpaceWidth == null ? (Single)i + 2.2F : (Single) baySpaceWidth),
                            Depth = (Single)i + 3.3F,
                            BaseHeight = (Single)i + 4.4F,
                            BaseWidth = (Single)i + 5.5F,
                            BaseDepth = (Single)i + 6.6F

                        });
                    }
                    foreach (LocationSpaceBayDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }
            return dtoList;
        }

        public static List<LocationSpaceElementDto> InsertLocationSpaceElementDtos(IDalFactory dalFactory, Int32 locationSpaceBayId,
                                            Int32 numToInsert, Single? baySpaceWidth = null)
        {
            List<LocationSpaceElementDto> dtoList = new List<LocationSpaceElementDto>();

            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    //add n dtos
                    for (int i = 1; i <= numToInsert; i++)
                    {
                        dtoList.Add(
                        new LocationSpaceElementDto()
                        {
                            LocationSpaceBayId = locationSpaceBayId,
                            Order = (Byte)i,
                            Height = (Single)i + 1.1F,
                            Width = (baySpaceWidth == null ? (Single)i + 2.2F : (Single)baySpaceWidth),
                            Depth = (Single)i + 3.3F
                        });
                    }
                    foreach (LocationSpaceElementDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }
            return dtoList;
        }

        #endregion

        #region Validation Template

        public static IEnumerable<ValidationTemplateDto> InsertValidationTemplateDtos(IDalFactory dalFactory, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<ValidationTemplateDto>();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IValidationTemplateDal>())
                {
                    for (var i = 0; i < numToInsert; i++)
                    {
                        dtos.Add(new ValidationTemplateDto
                        {
                            DateCreated = DateTime.Now,
                            DateLastModified = DateTime.Now,
                            EntityId = entityId,
                            Id = i,
                            Name = String.Format("name{0}", i)
                        });
                    }

                    foreach (var dto in dtos)
                    {
                        dal.Insert(dto);
                    }
                }

                dalContext.Commit();
            }

            return dtos;
        }

        public static IEnumerable<ValidationTemplateGroupDto> InsertValidationTemplateGroupDtos(IDalFactory dalFactory,
            IEnumerable<ValidationTemplateDto> templateDtos, Int32 numPerTemplate)
        {
            var random = new Random();
            var dtos = new List<ValidationTemplateGroupDto>();

            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IValidationTemplateGroupDal>())
                {
                    var groupIndex = 1;

                    foreach (var templateDto in templateDtos)
                    {
                        for (var i = 0; i < numPerTemplate; i++)
                        {
                            var groupDto = new ValidationTemplateGroupDto
                            {
                                Name = String.Format("name{0}", groupIndex),
                                Threshold1 = (float)random.NextDouble() * 10,
                                Threshold2 = (float)random.NextDouble() * 10,
                                ValidationTemplateId = templateDto.Id
                            };

                            dal.Insert(groupDto);
                            groupIndex++;

                            dtos.Add(groupDto);
                        }
                    }
                }
                dalContext.Commit();
            }
            return dtos;
        }

        public static IEnumerable<ValidationTemplateGroupMetricDto> InsertValidationTemplateGroupMetricDtos(IDalFactory dalFactory,
            IEnumerable<ValidationTemplateGroupDto> groupDtos, Int32 numPerTemplate)
        {
            var random = new Random();
            var dtos = new List<ValidationTemplateGroupMetricDto>();

            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IValidationTemplateGroupMetricDal>())
                {
                    var metricIndex = 1;

                    foreach (var groupDto in groupDtos)
                    {
                        for (var i = 0; i < numPerTemplate; i++)
                        {
                            var metricDto = new ValidationTemplateGroupMetricDto
                            {
                                Field = String.Format("field{0}", metricIndex),
                                Threshold1 = (float)random.NextDouble() * 10,
                                Threshold2 = (float)random.NextDouble() * 10,
                                ValidationTemplateGroupId = groupDto.Id
                            };

                            dal.Insert(metricDto);
                            metricIndex++;

                            dtos.Add(metricDto);
                        }
                    }
                }
                dalContext.Commit();
            }
            return dtos;
        }

        #endregion

        #region Workflow

        public static List<Ccm.Model.Workflow> InsertWorkflows(Int32 entityId)
        {
            List<Ccm.Model.Workflow> returnList = new List<Ccm.Model.Workflow>();

            EngineTaskInfoList engineTasks = EngineTaskInfoList.FetchAll();

            //Add Product
            Ccm.Model.Workflow flow = Ccm.Model.Workflow.NewWorkflow(entityId);
            flow.Name = "Add Product";
            flow.Tasks.Add(engineTasks.First(e => e.TaskType == "Galleria.Ccm.Engine.Tasks.AddProductManual.v1.Task"));
            flow = flow.Save();
            returnList.Add(flow);

            //Remove Product
            flow = Ccm.Model.Workflow.NewWorkflow(entityId);
            flow.Name = "Remove Product";
            flow.Tasks.Add(engineTasks.First(e => e.TaskType == "Galleria.Ccm.Engine.Tasks.RemoveProductManual.v1.Task"));
            flow = flow.Save();
            returnList.Add(flow);

            return returnList;
        }

        #endregion

        #region LocationProductAttribute

        public static List<LocationProductAttributeDto> InsertLocationProductAttributeDtos(IDalFactory dalFactory, IEnumerable<LocationDto> locations, IEnumerable<Product> products, Int32 entityId)
        {
            List<LocationProductAttributeDto> dtoList = new List<LocationProductAttributeDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    foreach (LocationDto location in locations)
                    {
                        foreach (Product product in products)
                        {
                            LocationProductAttributeDto dto = new LocationProductAttributeDto()
                            {
                                EntityId = entityId,
                                LocationId = location.Id,
                                ProductId = product.Id,
                                GTIN = product.Gtin,
                                CasePackUnits = product.CasePackUnits,
                                DaysOfSupply = 0,
                                DeliveryFrequencyDays = 0,
                                DateCreated = DateTime.UtcNow,
                                DateLastModified = DateTime.UtcNow
                            };
                            dtoList.Add(dto);
                        }
                    }


                    foreach (LocationProductAttributeDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }


            return dtoList;
        }

        public static List<LocationProductIllegalDto> InsertLocationProductIllegalDtos(IDalFactory dalFactory, Int16 locationId, IEnumerable<ProductDto> products, Int32 entityId)
        {
            List<LocationProductIllegalDto> dtoList = new List<LocationProductIllegalDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationProductIllegalDal dal = dalContext.GetDal<ILocationProductIllegalDal>())
                {

                    foreach (ProductDto product in products)
                    {
                        LocationProductIllegalDto dto = new LocationProductIllegalDto()
                        {
                            EntityId = entityId,
                            LocationId = locationId,
                            ProductId = product.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dtoList.Add(dto);
                    }

                    foreach (LocationProductIllegalDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }
                }
                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        #region Planogram Export Template

        public static List<PlanogramExportTemplateDto> InsertPlanogramExportTemplateDtos(IDalFactory dalFactory, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<PlanogramExportTemplateDto>();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                dtos = InsertPlanogramExportTemplateDtos(dalContext, numToInsert, entityId);
                dalContext.Commit();
            }

            return dtos;
        }

        public static List<PlanogramExportTemplateDto> InsertPlanogramExportTemplateDtos(IDalContext dalContext, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<PlanogramExportTemplateDto>();

            using (var dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
            {
                for (var i = 0; i < numToInsert; i++)
                {
                    dtos.Add(new PlanogramExportTemplateDto
                    {
                        DateCreated = DateTime.Now,
                        DateLastModified = DateTime.Now,
                        EntityId = entityId,
                        Id = i,
                        Name = String.Format("name{0}", i),
                        FileVersion = "V9",
                        FileType = 0
                    });
                }

                foreach (var dto in dtos)
                {
                    dal.Insert(dto);
                }
            }

            return dtos;
        }

        #endregion


        #region Planogram Export Template Performance Metric

        public static List<PlanogramExportTemplatePerformanceMetricDto> InsertPlanogramExportTemplatePerformanceMetricDtos(
            IDalContext dalContext, List<PlanogramExportTemplateDto> parentPlanogramExportTemplateDtos)
        {
            var dtoList = new List<PlanogramExportTemplatePerformanceMetricDto>();
            using (var dal = dalContext.GetDal<IPlanogramExportTemplatePerformanceMetricDal>())
            {
                foreach (var planogramPerformanceDto in parentPlanogramExportTemplateDtos)
                {
                    for (Int32 i = 0; i < 20; i++)
                    {
                        var dto = new PlanogramExportTemplatePerformanceMetricDto();
                        SetPropertyValues(dto, false);
                        dto.PlanogramExportTemplateId = planogramPerformanceDto.Id;
                        dto.MetricId = (Byte)i;
                        dal.Insert(dto);
                        dtoList.Add(dto);
                    }
                }
            }
            return dtoList;
        }
        #endregion


        #region Planogram Import Template

        public static List<PlanogramImportTemplateDto> InsertPlanogramImportTemplateDtos(IDalFactory dalFactory, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<PlanogramImportTemplateDto>();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                dtos = InsertPlanogramImportTemplateDtos(dalContext, numToInsert, entityId);
                dalContext.Commit();
            }

            return dtos;
        }

        public static List<PlanogramImportTemplateDto> InsertPlanogramImportTemplateDtos(IDalContext dalContext, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<PlanogramImportTemplateDto>();

            using (var dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
            {
                for (var i = 0; i < numToInsert; i++)
                {
                    dtos.Add(new PlanogramImportTemplateDto
                    {
                        DateCreated = DateTime.Now,
                        DateLastModified = DateTime.Now,
                        EntityId = entityId,
                        Id = i,
                        Name = String.Format("name{0}", i),
                        FileVersion = "V9",
                        FileType = 0
                    });
                }

                foreach (var dto in dtos)
                {
                    dal.Insert(dto);
                }
            }

            return dtos;
        }

        #endregion

        #region Planogram Import Template Performance Metric

        public static List<PlanogramImportTemplatePerformanceMetricDto> InsertPlanogramImportTemplatePerformanceMetricDtos(IDalContext dalContext, List<PlanogramImportTemplateDto> parentPlanogramImportTemplateDtos)
        {
            var dtoList = new List<PlanogramImportTemplatePerformanceMetricDto>();
            using (var dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
            {
                foreach (var planogramPerformanceDto in parentPlanogramImportTemplateDtos)
                {
                    for (Int32 i = 0; i < 20; i++)
                    {
                        var dto = new PlanogramImportTemplatePerformanceMetricDto();
                        SetPropertyValues(dto, false);
                        dto.PlanogramImportTemplateId = planogramPerformanceDto.Id;
                        dto.MetricId = (Byte)i;
                        dal.Insert(dto);
                        dtoList.Add(dto);
                    }
                }
            }
            return dtoList;
        }
        #endregion

        #region Planogram Name Template

        public static List<PlanogramNameTemplateDto> InsertPlanogramNameTemplateDtos(IDalFactory dalFactory, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<PlanogramNameTemplateDto>();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                dtos = InsertPlanogramNameTemplateDtos(dalContext, numToInsert, entityId);
                dalContext.Commit();
            }

            return dtos;
        }

        public static List<PlanogramNameTemplateDto> InsertPlanogramNameTemplateDtos(IDalContext dalContext, Int32 numToInsert,
            Int32 entityId)
        {
            var dtos = new List<PlanogramNameTemplateDto>();

            using (var dal = dalContext.GetDal<IPlanogramNameTemplateDal>())
            {
                for (var i = 0; i < numToInsert; i++)
                {
                    dtos.Add(new PlanogramNameTemplateDto
                    {
                        DateCreated = DateTime.Now,
                        DateLastModified = DateTime.Now,
                        EntityId = entityId,
                        Id = i,
                        Name = String.Format("name{0}", i),
                        BuilderFormula = "Test",
                        PlanogramAttribute = 0
                    });
                }

                foreach (var dto in dtos)
                {
                    dal.Insert(dto);
                }
            }

            return dtos;
        }

        #endregion

        #region Planogram Comparison Template

        public static List<PlanogramComparisonTemplateDto> InsertPlanogramComparisonTemplateDtos(
            IDalFactory dalFactory,
            Int32 numToInsert,
            Int32 entityId)
        {
            List<PlanogramComparisonTemplateDto> dtos;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                dtos = InsertPlanogramComparisonTemplateDtos(dalContext, numToInsert, entityId);
                dalContext.Commit();
            }
            return dtos;
        }

        public static List<PlanogramComparisonTemplateDto> InsertPlanogramComparisonTemplateDtos(IDalContext dalContext,
                                                                                                  Int32 numToInsert,
                                                                                                  Int32 entityId)
        {
            var dtos = new List<PlanogramComparisonTemplateDto>();
            for (var i = 0; i < numToInsert; i++)
            {
                dtos.Add(new PlanogramComparisonTemplateDto
                         {
                             DateCreated = DateTime.Now,
                             DateLastModified = DateTime.Now,
                             EntityId = entityId,
                             Id = i,
                             Name = String.Format("Planogram Comparison Template {0}", i),
                             Description = "Description for: Planogram Comparison Template {0}"
                         });
            }
            using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>()) { dtos.ForEach(dto => dal.Insert(dto)); }
            return dtos;
        }

        #endregion

        #region Planogram Comparison Template Field

        public static List<PlanogramComparisonTemplateFieldDto> InsertPlanogramComparisonTemplateFieldDtos(IDalContext dalContext,
                                                                                                            Int32 numToInsert,
                                                                                                            IEnumerable
                                                                                                                <PlanogramComparisonTemplateDto>
                                                                                                                parentDtos)
        {
            var dtos = new List<PlanogramComparisonTemplateFieldDto>();
            using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
            {
                foreach (PlanogramComparisonTemplateDto parentDto in parentDtos)
                {
                    for (var i = 0; i < numToInsert; i++)
                    {
                        var dto = new PlanogramComparisonTemplateFieldDto();
                        SetPropertyValues(dto, false);
                        dto.Id = i;
                        dto.PlanogramComparisonTemplateId = parentDto.Id;
                        dal.Insert(dto);
                        dtos.Add(dto);
                    }
                }
            }
            return dtos;
        }

        #endregion

        public static List<EntityComparisonAttributeDto> InsertEntityComparisonAttributeDtos(IDalFactory dalFactory,
            Int32 entityId, String propertyName, byte itemType, string propertyDisplayName)
        {
            List<EntityComparisonAttributeDto> dtos = new List<EntityComparisonAttributeDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                dtos.Add(new EntityComparisonAttributeDto()
                {
                    EntityId = entityId,
                    Id = IdentityHelper.GetNextInt32(),
                    PropertyName = propertyName,
                    ItemType = itemType,
                    PropertydisplayName = propertyDisplayName
                });

                using (var dal = dalContext.GetDal<IEntityComparisonAttributeDal>())
                {
                    dtos.ForEach(dto => dal.Insert(dto));
                }
                dalContext.Commit();
            }

            return dtos;
        }
    }
}