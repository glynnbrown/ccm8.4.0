﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.01)
// CCM-28755 : L.Ineson
//	Created
// V8-28676 : A.Silva
//      Added implementation for ShowIndeterminateBusy(String header, String desc, EventHandler cancelHandler).

#endregion
#region Version History : CCM820
// V8-29968 : A.Kuszyk
//  Added 4 button ShowMessage method.
#endregion

#region Version History : CCM830

// V8-32255 : A.Silva
//  Added implementation for ContinueWithItemChange allowing to pass a parameter to the save command.
// V8-32810 : L.Ineson
//  Added ShowSelectDirectoryDialog
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Framework.Controls.Wpf;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Helpers
{
    /// <summary>
    ///  Nunit implementation of IWindowService
    ///  - Service used to show windows and dialogs.
    /// </summary>
    public sealed class NUnitWindowService : IWindowService
    {
        public sealed class NUnitWindowServiceArgs
        {
            public String ServiceMethodName { get; private set; }
            public Type WindowType { get; private set; }
            public Object[] WindowParameters { get; private set; }
            public Object Result { get; set; }

            public NUnitWindowServiceArgs(String serviceMethod, Type windowType, params Object[] requestParams)
            {
                ServiceMethodName = serviceMethod;
                WindowType = windowType;
                WindowParameters = requestParams;
            }
        }

        private sealed class ReponseQueueItem
        {
            public String CallMethodName { get; set; }
            public Action<NUnitWindowServiceArgs> Action { get; set; }

            public ReponseQueueItem(String callMethod, Action<NUnitWindowServiceArgs> action)
            {
                CallMethodName = callMethod;
                Action = action;
            }
        }

        #region Constants

        public const String ShowWindowTMethod = "ShowWindowT";
        public const String ShowDialogTMethod = "ShowDialogT";
        public const String CloseWindowMethod = "CloseWindow";
        public const String ShowErrorMessageMethod = "ShowErrorMessage";
        public const String ShowMessageMethod = "ShowMessage";
        public const String ShowErrorOccurredMessageMethod = "ShowErrorOccurredMessage";
        public const String ConfirmDeleteWithUserMethod = "ConfirmDeleteWithUser";
        public const String ContinueWithItemChangeMethod = "ContinueWithItemChange";
        public const String PromptIfNameIsNotUniqueMethod = "PromptIfNameIsNotUnique";
        public const String ShowConcurrencyReloadPromptMethod = "ShowConcurrencyReloadPrompt";
        public const String PromptForSaveAsNameMethod = "PromptForSaveAsName";
        public const String PromptUserForValueMethod = "PromptUserForValue";
        public const String ShowDeterminateBusyMethod = "ShowDeterminateBusy";
        public const String ShowIndeterminateBusyMethod = "ShowIndeterminateBusy";
        public const String UpdateBusyMethod = "UpdateBusy";
        public const String CloseCurrentBusyMethod = "CloseCurrentBusy";
        public const String OpenRibbonBackstage = "OpenRibbonBackstage";
        public const String CloseRibbonBackstage = "CloseRibbonBackstage";
        public const String ShowOpenFileDialogMethod = "ShowOpenFileDialog";
        public const String ShowSaveFileDialogMethod = "ShowSaveFileDialog";
        public const String ShowSelectDirectoryDialogMethod = "ShowSelectDirectoryDialog";
        #endregion

        #region Fields
        private Queue<ReponseQueueItem> _responseQueue = new Queue<ReponseQueueItem>();

        #endregion

        #region Cache Methods

        private void AddResponseItem(ReponseQueueItem item)
        {
            _responseQueue.Enqueue(item);
        }

        public void AddResponse(Action<NUnitWindowServiceArgs> action)
        {
            AddResponse(null, action);
        }

        public void AddResponse(String callMethodName, Action<NUnitWindowServiceArgs> action)
        {
            AddResponseItem(new ReponseQueueItem(callMethodName, action));
        }

        public void AddResponseResult(String callMethodName, Object result)
        {
            AddResponse(callMethodName, (p) => { p.Result = result; });
        }

        public void AddNullResponse(String callMethodName)
        {
            AddResponse(callMethodName, (p) => { p.Result = null; });
        }

        public void ClearResponseQueue()
        {
            _responseQueue.Clear();
        }

        private Object GetNextResponse(String calledMethodName, Type windowType, params Object[] paramValues)
        {
            if (_responseQueue.Count == 0) Assert.Fail("No window response registered for {0}", calledMethodName);

            NUnitWindowServiceArgs args = new NUnitWindowServiceArgs(calledMethodName, windowType, paramValues);

            if (_responseQueue.Peek().CallMethodName != null)
            {
                Assert.AreEqual(calledMethodName, _responseQueue.Peek().CallMethodName, "No window response registered for {0}", calledMethodName);
            }

            ReponseQueueItem item = _responseQueue.Dequeue();
            try
            {
                item.Action.Invoke(args);
            }
            catch (Exception ex)
            {
                Assert.Fail("IWindowService response error {0} - {1}", calledMethodName, ex.Message);
            }

            //log the response
            Debug.WriteLine("IWindowService call: {0}", calledMethodName);

            return args.Result;
        }

        private Boolean HasResponse(String calledMethodName)
        {
            if (_responseQueue.Count == 0) return false;

            ReponseQueueItem item = _responseQueue.Peek();
            return (item.CallMethodName == calledMethodName);
        }

        public void AssertResponsesUsed()
        {
            Assert.AreEqual(0, _responseQueue.Count, "Not all expected window reponses were fired");
        }

        #endregion

        #region IWindowService implementations

        #region General

        void IWindowService.ShowWindow<T>(params object[] windowArgs)
        {
            GetNextResponse(ShowWindowTMethod, typeof(T), windowArgs);
        }
        public void ShowWindowSetReponse(Action<NUnitWindowServiceArgs> action)
        {
            AddResponse(NUnitWindowService.ShowWindowTMethod, action);
        }

        void IWindowService.ShowDialog<T>(params object[] windowArgs)
        {
            GetNextResponse(ShowDialogTMethod, typeof(T), windowArgs);
        }
        public void ShowDialogSetReponse(Action<NUnitWindowServiceArgs> action)
        {
            AddResponse(NUnitWindowService.ShowDialogTMethod, action);
        }

        void IWindowService.CloseWindow(Window window)
        {
             GetNextResponse(CloseWindowMethod, typeof(Window), window);
        }
        public void CloseWindowSetNullResponse()
        {
            AddNullResponse(NUnitWindowService.CloseWindowMethod);
        }
        public void CloseWindowSetResponse(Action<NUnitWindowServiceArgs> action)
        {
            AddResponse(NUnitWindowService.CloseWindowMethod, action);
        }

        void IWindowService.CloseRibbonBackstage(Window window)
        {
            if (HasResponse(CloseRibbonBackstage))
            {
                GetNextResponse(CloseRibbonBackstage, typeof(Window), window);
            }
        }

        void IWindowService.OpenRibbonBackstage(Window window)
        {
            if (HasResponse(OpenRibbonBackstage))
            {
                GetNextResponse(OpenRibbonBackstage, typeof(Window), window);
            }
        }

        T IWindowService.ActivateWindow<T>()
        {
            return null;
        }

        #endregion

        #region Messages/Prompts

        void IWindowService.ShowErrorMessage(string header, string description)
        {
            GetNextResponse(ShowErrorMessageMethod, typeof(ModalMessage), /*isModal*/true, header, description);
        }
        public void ShowErrorMessageSetResponse()
        {
            AddNullResponse(NUnitWindowService.ShowErrorMessageMethod);
        }

        ModalMessageResult IWindowService.ShowMessage(MessageWindowType messageType, String header, String description, String button1Content, String button2Content, String button3Content)
        {
            return (ModalMessageResult)GetNextResponse(ShowMessageMethod, typeof(ModalMessage), messageType, header, description, 3, button1Content, button2Content, button3Content);
        }

        ModalMessageResult IWindowService.ShowMessage(MessageWindowType messageType, String header, String description, Int32 buttonCount, String button1Content, String button2Content, String button3Content)
        {
            return (ModalMessageResult)GetNextResponse(ShowMessageMethod, typeof(ModalMessage), messageType, header, description, buttonCount, button1Content, button2Content, button3Content);
        }

        ModalMessageResult IWindowService.ShowMessage(MessageWindowType messageType, String header, String description, Int32 buttonCount, String button1Content, String button2Content, String button3Content, String button4Content)
        {
            return (ModalMessageResult)GetNextResponse(ShowMessageMethod, typeof(ModalMessage), messageType, header, description, buttonCount, button1Content, button2Content, button3Content, button4Content);
        }
        public void ShowMessageSetReponse(ModalMessageResult result)
        {
            AddResponseResult(NUnitWindowService.ShowMessageMethod, result);
        }

        ModalMessageResult IWindowService.ShowMessage(MessageWindowType messageType, String header, String description,String button1Content, String button2Content)
        {
            return ((IWindowService)this).ShowMessage(messageType, header, description, 2, button1Content, button2Content, null);
        }

        public ModalMessageResult ShowMessage(MessageWindowType messageType, string header, string description, int buttonCount, string button1Content, 
            string button2Content, string button3Content, ModalMessageButton defaultButton, ModalMessageButton cancelButton)
        {
            return (ModalMessageResult)GetNextResponse(ShowMessageMethod, typeof(ModalMessage), messageType, header, description, buttonCount, button1Content, button2Content, button3Content, defaultButton, cancelButton);
        }


        void IWindowService.ShowOkMessage(MessageWindowType messageType, string header, string description)
        {
            GetNextResponse(ShowMessageMethod, typeof(ModalMessage), messageType, header, description, 1, "OK", null, null);
        }

        ModalMessageResult IWindowService.ShowMessage(MessageWindowType messageType, string header, string description, int buttonCount, 
            string button1Content, string button2Content, string button3Content, string hideFutureWarningsText, out bool hideFutureWarningsResult)
        {
            throw new NotImplementedException();
        }

        void IWindowService.ShowErrorOccurredMessage(String itemName, OperationType actionType)
        {
            GetNextResponse(ShowErrorOccurredMessageMethod, typeof(ModalMessage), itemName, actionType);
        }

        Boolean IWindowService.ConfirmDeleteWithUser(String itemName)
        {
            if (HasResponse(ConfirmDeleteWithUserMethod))
            {
                return (Boolean)GetNextResponse(ConfirmDeleteWithUserMethod, typeof(ModalMessage), itemName);
            }
            return true;
        }
        public void ConfirmDeleteWithUserSetResponse(Boolean reponse)
        {
            AddResponseResult(NUnitWindowService.ConfirmDeleteWithUserMethod, reponse);
        }

        Boolean IWindowService.ContinueWithItemChange(Csla.Core.BusinessBase modelObject, Framework.ViewModel.RelayCommand saveCommand)
        {
            return ((IWindowService)this).ContinueWithItemChange(modelObject, saveCommand, null);
        }

        Boolean IWindowService.ContinueWithItemChange(Csla.Core.BusinessBase modelObject, Framework.ViewModel.RelayCommand saveCommand, Object commandParameter)
        {
            //check first if the dialog would be shown or if the method would just return true.
            //TODO - Need to really expose this code out as a framework method properly.

            //if the object is null just allow a continue.
            if (modelObject == null) { return true; }

            //if item is not dirty just continue
            if (!modelObject.IsDirty) { return true; }

            //if item is deleted just continue
            if (modelObject.IsDeleted) { return true; }

            //if the item has changed need to show a warning
            //check if the item is marked as initialized
            Boolean isInitialized = false;
            PropertyInfo isInitializedProperty = modelObject.GetType().GetProperty("IsInitialized");
            if (isInitializedProperty != null)
            {
                isInitialized = (Boolean)isInitializedProperty.GetValue(modelObject, null);
            }

            if (isInitialized)
            {
                //item is in a new state so just continue
                return true;
            }
            else
            {
                return (Boolean)GetNextResponse(ContinueWithItemChangeMethod, typeof(ModalMessage), modelObject, saveCommand);
            }
        }

        public void ContinueWithItemChangeSetReponse(Boolean reponse)
        {
            AddResponseResult(NUnitWindowService.ContinueWithItemChangeMethod, reponse);
        }

        Boolean IWindowService.PromptIfNameIsNotUnique(Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue)
        {
            //the dialog will only show if the name is not unique so first perform the unique check.
            if (isUniqueCheck(proposedValue))
            {
                selectedValue = proposedValue;
                return true;
            } 

            //proposed value was not unqiue - so request a reponse as the dialog would have shown.
            Object[] responseArray = (Object[])GetNextResponse(PromptIfNameIsNotUniqueMethod, typeof(ModalMessage), isUniqueCheck, proposedValue);
            Boolean returnResult = (Boolean)responseArray[0];
            selectedValue = (String)responseArray[1];

            return returnResult;
        }
        public void PromptIfNameIsNotUniqueSetReponse(Boolean dialogOkayed, String proposedValue)
        {
            AddResponseResult(NUnitWindowService.PromptIfNameIsNotUniqueMethod, new Object[] { true, proposedValue });
        }

        Boolean IWindowService.ShowConcurrencyReloadPrompt(String itemName)
        {
            return (Boolean)GetNextResponse(ShowConcurrencyReloadPromptMethod, typeof(ModalMessage), itemName);
        }
        public void ShowConcurrencyReloadPromptSetReponse(Boolean reponse)
        {
            AddResponseResult(NUnitWindowService.ShowConcurrencyReloadPromptMethod, reponse);
        }

        Boolean IWindowService.PromptForSaveAsName(Predicate<String> isUniqueCheck, String proposedValue, out String selectedValue)
        {
            Object[] responseArray = (Object[])GetNextResponse(PromptForSaveAsNameMethod, typeof(ModalMessage), isUniqueCheck, proposedValue);

            Boolean returnResult = (Boolean)responseArray[0];
            selectedValue = (String)responseArray[1];

            return returnResult;
        }
        public void PromptForSaveAsNameSetReponse(Boolean dialogOkayed, String newName)
        {
            AddResponseResult(NUnitWindowService.PromptForSaveAsNameMethod, new Object[] { true, newName });
        }

        Boolean IWindowService.PromptUserForValue(string dialogTitle, string forceFirstShowDescription, string notUniqueDescription, string inputLabel, bool forceFirstShow, Predicate<string> isUniqueCheck, string proposedValue, out string selectedValue)
        {
            Object[] responseArray = (Object[])GetNextResponse(PromptUserForValueMethod, typeof(ModalMessage), dialogTitle, 
                forceFirstShowDescription, notUniqueDescription, inputLabel, forceFirstShow,  isUniqueCheck, proposedValue);

            Boolean returnResult = (Boolean)responseArray[0];
            selectedValue = (String)responseArray[1];

            return returnResult;
        }
        public void PromptUserForValueSetReponse(Boolean dialogOkayed, String value)
        {
            AddResponseResult(NUnitWindowService.PromptUserForValueMethod, new Object[] { true, "Group A" });
        }


        #endregion

        #region Busy

        void IWindowService.ShowDeterminateBusy(String header, String desc)
        {
            GetNextResponse(ShowDeterminateBusyMethod, typeof(ModalBusy), header, desc);
        }

        void IWindowService.ShowDeterminateBusy(String header, String desc, EventHandler cancelHandler)
        {
            GetNextResponse(ShowDeterminateBusyMethod, typeof(ModalBusy), header, desc, cancelHandler);
        }

        void IWindowService.ShowIndeterminateBusy(string header, string desc)
        {
            GetNextResponse(ShowIndeterminateBusyMethod, typeof(ModalBusy), header, desc);
        }

        void IWindowService.ShowIndeterminateBusy(String header, String desc, EventHandler cancelHandler)
        {
            GetNextResponse(ShowIndeterminateBusyMethod, typeof(ModalBusy), header, desc, cancelHandler);
        }

        void IWindowService.UpdateBusy(string description, int progressPercentage)
        {
            if (HasResponse(UpdateBusyMethod))
            {
                GetNextResponse(UpdateBusyMethod, typeof(ModalBusy), description, progressPercentage);
            }
        }

        void IWindowService.UpdateBusy(int progressPercentage)
        {
            if (HasResponse(UpdateBusyMethod))
            {
                GetNextResponse(UpdateBusyMethod, typeof(ModalBusy), progressPercentage);
            }
        }

        void IWindowService.UpdateBusy(String header, String description, Int32 progressPercentage)
        {
            if (HasResponse(UpdateBusyMethod))
            {
                GetNextResponse(UpdateBusyMethod, typeof(ModalBusy), header, description, progressPercentage);
            }
        }

        void IWindowService.CloseCurrentBusy()
        {
            GetNextResponse(CloseCurrentBusyMethod, typeof(ModalBusy));
        }

        void IWindowService.CloseCurrentBusy(EventHandler cancelHandler)
        {
            GetNextResponse(CloseCurrentBusyMethod, typeof(ModalBusy), cancelHandler);
        }

        bool IWindowService.ShowWaitCursor()
        {
            //do nothing
            return true;
        }

        void IWindowService.HideWaitCursor()
        {
            //do nothing
        }

        #endregion

        #region File dialogs

        Boolean IWindowService.ShowOpenFileDialog(String intialDirectory, String filter, out string fileName)
        {
            Object[] responseArray = (Object[])GetNextResponse(ShowOpenFileDialogMethod, null, intialDirectory, filter, false);

            Boolean returnResult = (Boolean)responseArray[0];
            fileName = (String)responseArray[1];

            return returnResult;
        }
        public void ShowOpenFileDialogSetReponse(Boolean result, String filename)
        {
            AddResponseResult(ShowOpenFileDialogMethod, new Object[]{result, filename});
        }

        Boolean IWindowService.ShowOpenMultiFileDialog(string intialDirectory, string filter, out string[] fileNames)
        {
            Object[] responseArray = (Object[])GetNextResponse(ShowOpenFileDialogMethod, null, intialDirectory, filter, true);

            Boolean returnResult = (Boolean)responseArray[0];
            fileNames = (String[])responseArray[1];

            return returnResult;
        }

        Boolean IWindowService.ShowSaveFileDialog(String fileName, String intialDirectory, String filter, out String selectedFileName)
        {
            Object[] responseArray = (Object[])GetNextResponse(ShowSaveFileDialogMethod, null, fileName, intialDirectory, filter);

            Boolean returnResult = (Boolean)responseArray[0];
            selectedFileName = (String)responseArray[1];

            return returnResult;
        }
        public void ShowSaveFileDialogSetReponse(Boolean result, String filename)
        {
            AddResponseResult(ShowSaveFileDialogMethod, new Object[] { result, filename });
        }

        Boolean IWindowService.ShowSelectDirectoryDialog(String intialDirectory, out String selectedDirectory)
        {
            Object[] responseArray = (Object[])GetNextResponse(ShowSelectDirectoryDialogMethod, null, intialDirectory);

            Boolean returnResult = (Boolean)responseArray[0];
            selectedDirectory = (String)responseArray[1];

            return returnResult;
        }
        public void ShowSelectDirectoryDialogSetReponse(Boolean result, String directory)
        {
            AddResponseResult(ShowSelectDirectoryDialogMethod, new Object[] { result, directory });
        }

        #endregion


        #endregion    

    

        
    }
}