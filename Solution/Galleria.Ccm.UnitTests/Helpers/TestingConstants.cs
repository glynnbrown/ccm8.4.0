﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.UnitTests.Helpers
{
    public static class TestingConstants
    {
        /// <summary>
        /// The name of the server instance to create mssql test databases
        /// </summary>
        public const String MssqlServerInstance = @".\DEVSQL2012";

    }
}
