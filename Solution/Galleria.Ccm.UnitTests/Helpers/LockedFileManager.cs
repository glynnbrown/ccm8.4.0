﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Galleria.Ccm.UnitTests.Helpers
{
    public static class LockedFileManager
    {
        private static readonly object _fileLock = new object();

        public static bool WaitForFileToBeUnlocked(string fileToDelete, int numOfTries = 10)
        {
            while (numOfTries-- > 0)
            {
                lock (_fileLock)
                try
                {
                    using (var stream = File.Open(fileToDelete, FileMode.Open, FileAccess.ReadWrite))
                    {
                        stream.ReadByte();
                        stream.Close();
                        return true;
                    }
                }
                catch (FileNotFoundException)
                {
                    return false;
                }
                catch (IOException)
                {

                }

                Thread.Sleep(100);
            }

            return false;
        }
    }
}
