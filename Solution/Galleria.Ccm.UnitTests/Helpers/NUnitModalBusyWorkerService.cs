﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.01)
// CCM-28755 : L.Ineson
//	Created
#endregion

#endregion


using System;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Services;
using Galleria.Ccm.Common.Wpf.Services;

namespace Galleria.Ccm.UnitTests.Helpers
{
    /// <summary>
    /// Nunit implementation of the IModalBusyWorkerService
    /// - runs work which is normally performed by a background worker whilst showing a modal busy dialog.
    /// </summary>
    public sealed class NUnitModalBusyWorkerService : IModalBusyWorkerService
    {
        private IModalBusyWork _work;

        #region Properties

        public Boolean CancellationPending { get; set; }

        #endregion

        public void RunWorker(IModalBusyWork work)
        {
            //run the work synchronously.
            _work = work;

            //show busy
            work.ShowModalBusy(this, Worker_CancelRequested);

            //do work
            System.ComponentModel.DoWorkEventArgs doWorkArgs = new System.ComponentModel.DoWorkEventArgs(work.GetWorkerArgs());
            Exception error = null;

            try
            {
                work.OnDoWork(this, doWorkArgs);
            }
            catch (Exception ex)
            {
                error = ex;
            }

            //close busy
            CommonHelper.GetWindowService().CloseCurrentBusy(Worker_CancelRequested);

            //complete work
            work.OnWorkCompleted(this, new System.ComponentModel.RunWorkerCompletedEventArgs(doWorkArgs.Result, error, this.CancellationPending));

            //reset
            this.CancellationPending = false;
            _work = null;
        }


        public void ReportProgress(int progress, Object args)
        {
            _work.OnProgressChanged(this,
                new System.ComponentModel.ProgressChangedEventArgs(progress, args));
        }

        public void ReportProgress(int progress)
        {
            ReportProgress(progress, null);
        }


        #region Event Handlers

        private void Worker_CancelRequested(object sender, EventArgs e)
        {
            CancellationPending = true;
        }

        #endregion
    }
}
