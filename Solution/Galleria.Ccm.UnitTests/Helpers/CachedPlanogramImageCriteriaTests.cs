﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802

// V8-28964 : A.Silva
//      Created.

#endregion

#endregion

using System;
using System.Text.RegularExpressions;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Helpers
{
    [TestFixture]
    public class CachedPlanogramImageCriteriaTests
    {
        [Test]
        public void GetCacheItemIdPattern_ShouldReturnPattern()
        {
            const String expectation = "The regular expression for the pattern should have the expected value.";
            const String expected = @"^(?<attributeValue>.+)_(?<imageType>.+)\.(?<facingType>.+)$";

            String actual = CachedPlanogramImageCriteria.GetCacheItemIdPattern();

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void GetCacheItemId_ShouldReturnValidPattern()
        {
            const String expectation = "The generated CacheItemId should follow the correct pattern.";
            const String attributeValue = "AttributeValue";
            const PlanogramImageType imageType = PlanogramImageType.None;
            const PlanogramImageFacingType facingType = PlanogramImageFacingType.Front;
            CachedPlanogramImageCriteria instance = new CachedPlanogramImageCriteria(attributeValue, imageType, facingType);

            String cacheItemId = instance.GetCacheItemId();
            Boolean condition = Regex.IsMatch(cacheItemId, CachedPlanogramImageCriteria.GetCacheItemIdPattern());

            Assert.IsTrue(condition, expectation);
        }

        [Test]
        public void ExtractFacingImageTypes_ShouldReturnCorrectString()
        {
            const String expectation = "The extracted facing and image types should be the provided ones.";
            const String attributeValue = "AttributeValue";
            const PlanogramImageType imageType = PlanogramImageType.None;
            const PlanogramImageFacingType facingType = PlanogramImageFacingType.Front;
            CachedPlanogramImageCriteria instance = new CachedPlanogramImageCriteria(attributeValue, imageType, facingType);
            String expected = String.Format("{0}{1}", (Int32) imageType, (Int32) facingType);

            String actual = CachedPlanogramImageCriteria.ExtractFacingImageTypes(instance.GetCacheItemId());

            Assert.AreEqual(expected, actual, expectation);
        }
    }
}