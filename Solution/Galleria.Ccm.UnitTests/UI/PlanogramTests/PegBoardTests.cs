﻿using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.PlanogramTests
{
    [TestFixture]
    class PegBoardTests : TestBase
    {
        [Test]
        public void CanMoveProductAroundPegBoard()
        {
            Package package = "TestPlan".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            PlanogramFixtureItem bay = planogram.AddFixtureItem();

            PlanogramFixture fixture = planogram.Fixtures.FindById(bay.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a pegboard
            PlanogramFixtureComponent peg1Fc =
                fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 100, 1F);
            PlanogramComponent peg1C = planogram.Components.FindById(peg1Fc.PlanogramComponentId);

            peg1Fc.Y = 50;

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            pegSub.IsProductOverlapAllowed = true;
            pegSub.MerchConstraintRow1StartX = 4;
            pegSub.MerchConstraintRow1SpacingX = 4;
            pegSub.MerchConstraintRow1StartY = 4;
            pegSub.MerchConstraintRow1SpacingY = 4;
            pegSub.MerchConstraintRow1Height = 1;
            pegSub.MerchConstraintRow1Width = 1;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                pegSub,
                bay,
                peg1Fc);

            //Product 1 (w: 8.2, h: 32.2, d: 24.6)
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            PlanogramPosition position = subComponentPlacement.AddPosition(prod1);
            position.FacingsHigh = 1;
            position.FacingsWide = 1;
            position.FacingsDeep = 3;
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            position.OrientationType = PlanogramPositionOrientationType.Front0;
            position.X = 3.9F;
            position.Y = 65.3F;
            position.Z = 1F;
            position.Sequence = 1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView view = packageView.PlanogramViews.First();
            PlanogramPositionView positionView = view.EnumerateAllPositions().First();

            positionView.Product.Y += 10;

            // Check the position
            Assert.That(position.X, Is.EqualTo(3.9F).Within(0.1F));
            Assert.That(position.Y, Is.EqualTo(74.6F).Within(0.1F));

        }
    }
}

