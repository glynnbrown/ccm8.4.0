﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
// V8-25395 : A.Probyn
//  Added generic view model checkes for commands properties
#endregion

#region Version History: (CCM802)
// V8-29023 : M.Pettit
//  Added the registration fo the default ImageRenderer - this is required for the tests to run
#endregion

#region Version History: CCM8.2.0
// V8-30803/30804/30805 : A.Kuszyk
//  Load the assembly "Galleria.Ccm.Engine.Tasks.dll" into the TaskContainer
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using System.Diagnostics;

namespace Galleria.Ccm.UnitTests.UI
{
    [NUnit.Framework.Category("Editor UI")]
    public abstract class TestBase
    {
        #region Fields
        private IDalFactory _planogramDalFactory; // the dal factory to use for testing
        private IDalFactory _userDalFactory; // the dal factory to use for testing
        private IDalFactory _dalFactory;
        private String _dalName;
        private String _originalDalName;
        private readonly List<String> _propertyChangedNotifications = new List<String>();
        private readonly List<String> _propertyChangingNotifications = new List<String>();
        private String _tempDir;
        #endregion

        #region Properties

        protected List<String> PropertyChangedNotifications
        {
            get { return _propertyChangedNotifications; }
        }

        protected List<String> PropertyChangingNotifications
        {
            get { return _propertyChangingNotifications; }
        }

        protected IDalFactory PlanogramDalFactory
        {
            get { return _planogramDalFactory; }
        }

        protected IDalFactory UserDalFactory
        {
            get { return _userDalFactory; }
        }

        protected IDalFactory DalFactory
        {
            get { return _dalFactory; }
        }

        protected String TestDir
        {
            get { return _tempDir; }
        }

        public NUnitWindowService WindowService
        {
            get { return (NUnitWindowService)Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>(); }
        }

        /// <summary>
        /// Returns the connected entity id,
        /// nb - you will need to call SetMockRepositoryConnection first.
        /// </summary>
        public Int32 EntityId
        {
            get { return App.ViewState.EntityId; }
        }


        #endregion

        #region Methods

        [SetUp]
        public virtual void Setup()
        {
            _originalDalName = DalContainer.DalName;

            _tempDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName,
                    "Test Files");

            if (Directory.Exists(_tempDir))
            {
                try
                {
                    Directory.Delete(_tempDir, true);
                }
                catch { }
            }
            Directory.CreateDirectory(_tempDir);


            DalContainer.Reset();
            _propertyChangedNotifications.Clear();

            //Register the planogram mock dal.
            String dalName = Guid.NewGuid().ToString();
            _planogramDalFactory = new Galleria.Framework.Planograms.Dal.Mock.DalFactory();
            DalContainer.RegisterFactory(dalName, _planogramDalFactory);


            #region User Dal
            String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName, "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (string file in Directory.GetFiles(templateDir))
                {
                    //if (!Path.GetFileName(file).StartsWith(templateName))
                    //{
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch
                    {
                        // No point in crying over spilt milk
                    }
                    //}
                }
            }


            String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
            var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
            userFactory.UnitTestFolder = templateDir;
            _userDalFactory = userFactory;
            DalContainer.RegisterFactory(Constants.UserDal, _userDalFactory);
            #endregion


            //Register the main mock dal
            _dalName = Guid.NewGuid().ToString();
            _dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();
            DalContainer.RegisterFactory(_dalName, _dalFactory);
            _dalFactory.CreateDatabase();

            // set this factory to be the default of the container
            DalContainer.DalName = _dalName;

            //Authenticate the test user.
            DomainPrincipal.Authenticate();

            //register the tasks assembly
            TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");

            //register the window service
            Galleria.Ccm.Services.ServiceContainer.
                RegisterServiceObject<IWindowService>(new NUnitWindowService());
            Galleria.Ccm.Services.ServiceContainer.
                RegisterServiceObject<IModalBusyWorkerService>(new NUnitModalBusyWorkerService());

            //Register the Image Renderer
            Galleria.Framework.Planograms.Helpers.PlanogramImagesHelper.RegisterPlanogramImageRenderer
                <Galleria.Framework.Planograms.Controls.Wpf.Helpers.PlanogramImageRenderer>();

            //Add a default entity - doing as dto otherwise
            // we get a tonne of default data and the test takes ages.
            TestDataHelper.InsertDefaultTestEntity(_dalFactory);
            //Galleria.Ccm.Model.Entity entity1 = Galleria.Ccm.Model.Entity.NewEntity();
            //entity1.Name = "Default";
            //entity1.Save();

            //tell the app that we are unit testing.
            App.InitialiseForUnitTest();

            //make the settings point to the test directory
            App.ViewState.Settings.Model.FixtureLibraryLocation = GetFixtureDirectoryPath();

            //and turn off selection blink to speed tests up.
            App.ViewState.Settings.Model.IsShowSelectionAsBlinkingEnabled = false;
        }

        [TearDown]
        public virtual void TearDown()
        {
            App.MainPageViewModel.Dispose();

            //make the user dal factory unlock and drop everything.
            if (_userDalFactory != null)
            {
                //Remove the User DalFactory
                DalContainer.RemoveFactory(_userDalFactory.DalFactoryConfig.Name);
                _userDalFactory.Dispose();
            }

            //try to unlock all pog files in the temp directory
            // in case they were left open by the test.
            if (Directory.Exists(_tempDir))
            {
                foreach (String file in Directory.EnumerateFiles(_tempDir, "*.*", SearchOption.AllDirectories))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch (Exception)
                    {
                        if (Path.GetExtension(file).Equals(".pog"))
                        {
                            try
                            {
                                using (Package p = Package.FetchByFileName(file))
                                {
                                    p.Unlock();
                                }

                                System.IO.File.Delete(file);
                            }
                            catch (Exception) { }
                        }
                    }
                }
            }



            // remove the registered dal
            DalContainer.RemoveFactory(_dalName);

            // and restore the original dal name
            DalContainer.DalName = _originalDalName;


            //delete all temp directories
            if (Directory.Exists(_tempDir))
            {
                try
                {
                    Directory.Delete(_tempDir, true);
                }
                catch { }
            }
            Directory.CreateDirectory(_tempDir);

        }

        protected Object GetPropertyTestValue1(Type propertyType)
        {
            if (propertyType == typeof(Single) || propertyType == typeof(Single?))
            {
                return 0.3F;
            }
            else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?))
            {
                return 2;
            }
            else if (propertyType == typeof(Byte) || propertyType == typeof(Byte?))
            {
                return (Byte)2;
            }
            else if (propertyType == typeof(Int16) || propertyType == typeof(Int16?))
            {
                return (Int16)2;
            }
            else if (propertyType == typeof(Boolean) || propertyType == typeof(Boolean?))
            {
                return true;
            }
            else if (propertyType == typeof(String))
            {
                return "TEST";
            }
            else if (propertyType.IsEnum)
            {
                return Enum.Parse(propertyType, "1");
            }
            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                return new DateTime(2014, 8, 15);
            }

            return null;
        }

        protected Object GetPropertyTestValue2(Type propertyType)
        {
            if (propertyType == typeof(Single) || propertyType == typeof(Single?))
            {
                return 0.5F;
            }
            else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?))
            {
                return 3;
            }
            else if (propertyType == typeof(Byte) || propertyType == typeof(Byte?))
            {
                return (Byte)3;
            }
            else if (propertyType == typeof(Int16) || propertyType == typeof(Int16?))
            {
                return (Int16)3;
            }
            else if (propertyType == typeof(Boolean) || propertyType == typeof(Boolean?))
            {
                return false;
            }
            else if (propertyType == typeof(String))
            {
                return "TEST2";
            }
            else if (propertyType.IsEnum)
            {
                return Enum.Parse(propertyType, "2");
            }
            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                return new DateTime(2012, 5, 6);
            }

            return null;
        }

        protected String GetFixtureDirectoryPath()
        {
            String path = Path.Combine(this.TestDir, "Fixtures");

            if (Directory.Exists(path))
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch { }
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        protected String GetProductLibraryDirectoryPath()
        {
            String path = Path.Combine(this.TestDir, "ProductLibraries");

            if (Directory.Exists(path))
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch { }
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        protected void SetMockRepositoryConnection()
        {
            DalContainer.RegisterFactory(_dalName, _dalFactory);
            DalContainer.DalName = _dalName;
            DomainPrincipal.Authenticate();
            App.ViewState.ServerName = TestingConstants.MssqlServerInstance;
            App.ViewState.DatabaseName = _dalName;
            App.ViewState.EntityId = Galleria.Ccm.Model.EntityInfoList.FetchAllEntityInfos().First().Id;
        }

        #endregion

        #region Event Handlers

        protected void TestModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //add to the change notification collection
            _propertyChangedNotifications.Add(e.PropertyName);
        }

        protected void TestModel_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            //add to the change notification collection
            _propertyChangingNotifications.Add(e.PropertyName);
        }

        #endregion

        #region Quick UI Helpers

        /// <summary>
        /// Creates a new empty plan in the ui and
        /// return the plan controller handle.
        /// </summary>
        protected PlanControllerViewModel AddNewEmptyPlan()
        {
            var mainPageView = App.MainPageViewModel;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });

            return mainPageView.CreateNewPlanogram();
        }

        /// <summary>
        /// Creates a new CreweSparklingWater plan in the ui and
        /// return the plan controller handle.
        /// </summary>
        protected PlanControllerViewModel AddNewCreweSparklingWaterPlan()
        {
            var mainPageView = App.MainPageViewModel;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });

            var newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.BeginUndoableAction();
            newPlan.SourcePlanogram.BeginUpdate();

            TestDataHelper.CreatePlanCreweSparklingWater(newPlan.SourcePlanogram.Model);

            newPlan.SourcePlanogram.EndUpdate();
            newPlan.SourcePlanogram.EndUndoableAction();
            newPlan.SourcePlanogram.ClearAllUndoRedoActions();

            return newPlan;
        }

        protected enum SamplePlans
        {
            Cereals1Bay,
            Cereals2Bay,
            Cereals3Bay,
            FreshMeat1Bay,
        }

        protected PlanControllerViewModel OpenSamplePlan(SamplePlans plan)
        {
            var mainPageView = App.MainPageViewModel;

            String filePath;
            switch (plan)
            {
                case SamplePlans.Cereals1Bay:
                    filePath = @".\Sample Files\Common\Customer Centric Merchandising\Planograms\Cereals\Cereals 1 Bay.pog";
                    break;

                case SamplePlans.Cereals2Bay:
                    filePath = @".\Sample Files\Common\Customer Centric Merchandising\Planograms\Cereals\Cereals 2 Bay.pog";
                    break;

                case SamplePlans.Cereals3Bay:
                    filePath = @".\Sample Files\Common\Customer Centric Merchandising\Planograms\Cereals\Cereals 3 Bay.pog";
                    break;

                case SamplePlans.FreshMeat1Bay:
                    filePath = @".\Sample Files\Common\Customer Centric Merchandising\Planograms\Fresh Meat\Fresh Meat 1 Bay.pog";
                    break;

                default: throw new ArgumentOutOfRangeException();
            }


            mainPageView.OpenPlanogramsFromFileSystem(filePath);

            return mainPageView.PlanControllers.Last();
        }

        #endregion

    }

    public abstract class TestBase<T> : TestBase
    {
        #region Fields
        private T _testModel;
        #endregion

        #region Properties

        protected T TestModel
        {
            get { return _testModel; }
            set
            {
                if (_testModel != null)
                {
                    INotifyPropertyChanged oldModel = _testModel as INotifyPropertyChanged;
                    if (oldModel != null)
                    {
                        oldModel.PropertyChanged -= base.TestModel_PropertyChanged;
                    }
                }

                _testModel = value;

                if (value != null)
                {
                    INotifyPropertyChanged newModel = value as INotifyPropertyChanged;
                    if (newModel != null)
                    {
                        newModel.PropertyChanged += new PropertyChangedEventHandler(TestModel_PropertyChanged);
                    }
                }
            }
        }

        #endregion

        #region Methods

        public override void TearDown()
        {
            base.TearDown();

            this.TestModel = default(T);
        }

        #endregion

        #region Tests

        /// <summary>
        /// Checksum intended to flag up if a command has been added but not tested
        /// </summary>
        [Test]
        public void TestChecksum_Command()
        {
            List<String> commandErrors = new List<String>();

            Type testModelType = typeof(T);

            IEnumerable<MethodInfo> testedCommands = this.GetType().GetMethods().Where(m => m.Name.StartsWith("Command_"));
            IEnumerable<PropertyInfo> cmdPropertyInfos = testModelType.GetProperties()
                .Where(p => p.DeclaringType == testModelType)
               .Where(p => p.PropertyType.GetInterface("IRelayCommand") != null);



            int testedCommandsCount = testedCommands.Count();
            int cmdPropertyInfosCount = cmdPropertyInfos.Count();

            if (testedCommandsCount != cmdPropertyInfosCount)
            {
                if (testedCommandsCount > cmdPropertyInfosCount)
                {
                    commandErrors.Add("Too many command test methods?");
                }
                else
                {
                    if (testedCommandsCount > cmdPropertyInfosCount)
                    {
                        commandErrors.Add("Too many property test methods?");
                    }
                    else
                    {
                        foreach (PropertyInfo info in cmdPropertyInfos)
                        {
                            String commandName = info.Name.Replace("Command", "");

                            String expectedName = String.Format("Command_{0}", commandName);
                            MethodInfo foundInfo = testedCommands.FirstOrDefault(p => p.Name == expectedName);
                            if (foundInfo == null)
                            {
                                commandErrors.Add(info.Name + " is not being tested");
                            }
                        }
                    }
                }

                //Output reusults to text output window
                if (commandErrors.Count() > 0)
                {
                    foreach (String commandError in commandErrors)
                    {
                        Debug.WriteLine(commandError);
                    }
                    //and throw inconclusive exception
                    Assert.Inconclusive(commandErrors.First());
                }
            }

        }

        /// <summary>
        /// Intended to flag up if a property has been added but not tested
        /// </summary>
        [Test]
        public void TestChecksum_Property()
        {
            List<String> propertyErrors = new List<String>();

            Type testModelType = typeof(T);

            IEnumerable<MethodInfo> testedProperties = this.GetType().GetMethods().Where(m => m.Name.StartsWith("Property_"));

            IEnumerable<PropertyInfo> propertyInfos = testModelType.GetProperties()
                 .Where(p => p.DeclaringType == testModelType)
               .Where(p => p.PropertyType.GetInterface("IRelayCommand") == null);

            int testPropertiesCount = testedProperties.Count();
            int propertyInfosCount = propertyInfos.Count();

            if (propertyInfosCount != testPropertiesCount)
            {
                if (testPropertiesCount > propertyInfosCount)
                {
                    propertyErrors.Add("Too many property test methods?");
                }
                else
                {
                    foreach (PropertyInfo info in propertyInfos)
                    {
                        String expectedName = String.Format("Property_{0}", info.Name);
                        MethodInfo foundInfo = testedProperties.FirstOrDefault(p => p.Name == expectedName);
                        if (foundInfo == null)
                        {
                            propertyErrors.Add(info.Name + " is not being tested");
                        }
                    }
                }
            }

            //Output reusults to text output window
            if (propertyErrors.Count() > 0)
            {
                foreach (String propertyError in propertyErrors)
                {
                    Debug.WriteLine(propertyError);
                }
                //and throw inconclusive exception
                Assert.Inconclusive(propertyErrors.First());
            }
        }

        protected void NewCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Generic_New, cmd.FriendlyName);
            Assert.IsNotNull(cmd.FriendlyDescription);
            Assert.AreEqual(ImageResources.NewDocument_32, cmd.Icon);
            Assert.AreEqual(ImageResources.NewDocument_16, cmd.SmallIcon);
            Assert.AreEqual(ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(Key.N, cmd.InputGestureKey);
        }

        protected void OpenCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Generic_Open, cmd.FriendlyName);
        }

        protected void SaveCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Generic_Save, cmd.FriendlyName);
            Assert.IsNotNull(cmd.FriendlyDescription);
            Assert.AreEqual(ImageResources.Save_32, cmd.Icon);
            Assert.AreEqual(ImageResources.Save_16, cmd.SmallIcon);
            Assert.AreEqual(ModifierKeys.Control, cmd.InputGestureModifiers);
            Assert.AreEqual(Key.S, cmd.InputGestureKey);
        }

        protected void SaveAsCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Generic_SaveAs, cmd.FriendlyName);
            Assert.AreEqual(ImageResources.SaveAs_16, cmd.SmallIcon);
            Assert.AreEqual(ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(Key.F12, cmd.InputGestureKey);
        }

        protected void SaveAndNewCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Generic_SaveAndNew, cmd.FriendlyName);
            Assert.AreEqual(Message.Generic_SaveAndNew_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(ImageResources.SaveAndNew_16, cmd.SmallIcon);
            Assert.AreEqual(ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(null, cmd.InputGestureKey);
        }

        protected void SaveAndCloseCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Generic_SaveAndClose, cmd.FriendlyName);
            Assert.AreEqual(Message.Generic_SaveAndClose_Tooltip, cmd.FriendlyDescription);
            Assert.AreEqual(ImageResources.SaveAndClose_16, cmd.SmallIcon);
            Assert.AreEqual(ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(null, cmd.InputGestureKey);
        }

        protected void DeleteCommandCheckExpectedValues(IRelayCommand cmd)
        {
            Assert.AreEqual(Message.Generic_Delete, cmd.FriendlyName);
            Assert.AreEqual(ImageResources.Delete_16, cmd.SmallIcon);
            Assert.AreEqual(ModifierKeys.None, cmd.InputGestureModifiers);
            Assert.AreEqual(null, cmd.InputGestureKey);
        }

        #endregion
    }
}
