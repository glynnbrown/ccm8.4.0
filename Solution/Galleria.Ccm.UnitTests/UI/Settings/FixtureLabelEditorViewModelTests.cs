﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
// V8-26249 : L.Luong
//  Changed to fit new Design
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using System.IO;
using Galleria.Ccm.Editor.Client.Wpf.Settings;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.UI.Settings
{
    [TestFixture]
    class FixtureLabelEditorViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private FixtureLabelEditorViewModel _fixtureLabelEditorViewModel;

        PlanogramView _planogramView;

        public override void Setup()
        {
            base.Setup();
            InsertLabels();

            Package pk = Package.NewPackage(null, PackageLockType.Unknown);
            pk.Planograms.Add();
            _planogramView = PackageViewModel.NewPackageViewModel(pk).PlanogramViews[0];
                
            _fixtureLabelEditorViewModel = new FixtureLabelEditorViewModel(null, _planogramView);
        }

        private void InsertLabels()
        {
            using (Label l1 = Label.NewLabel(LabelType.Fixture))
            {
                l1.Name = "l1";
                String path = Path.Combine(this.TestDir, l1.Name);
                path = Path.ChangeExtension(path, Label.FileExtension);
                l1.SaveAsFile(path);
            }


            using (Label l2 = Label.NewLabel(LabelType.Fixture))
            {
                l2.Name = "l2";
                String path = Path.Combine(this.TestDir, l2.Name);
                path = Path.ChangeExtension(path, Label.FileExtension);
                l2.SaveAsFile(path);
            }
        }

        #endregion

        #region Properties

        [Test]
        public void SelectedLabelProperty()
        {
            String propertyName = WpfHelper.GetPropertyPath<FixtureLabelEditorViewModel>(p => p.SelectedLabel).Path;
            InsertLabels();

            FixtureLabelEditorViewModel model = new FixtureLabelEditorViewModel(null, null);

            Assert.IsNotNull(_fixtureLabelEditorViewModel.SelectedLabel);

            var label = model.SelectedLabel;

            _fixtureLabelEditorViewModel.PropertyChanged += base.TestModel_PropertyChanged;

            _fixtureLabelEditorViewModel.SelectedLabel = label;
            Assert.AreEqual(label, _fixtureLabelEditorViewModel.SelectedLabel);

            Assert.Contains(propertyName, base.PropertyChangedNotifications);

            model.PropertyChanged -= base.TestModel_PropertyChanged;
        }


        [Test]
        public void AvailableFontsProperty()
        {
            Assert.IsNotEmpty(_fixtureLabelEditorViewModel.AvailableFonts);
        }

        [Test]
        public void AvailableFieldsProperty()
        {
            Assert.IsNotEmpty(_fixtureLabelEditorViewModel.AvailableFonts);
        }

        [Test]
        public void DisplayTextProperty()
        {
            _fixtureLabelEditorViewModel.SelectedLabel.Text = "TestText";

            Assert.AreEqual("TestText", _fixtureLabelEditorViewModel.DisplayText);
        }

        #endregion

        #region Commands

        //[Test]
        //public void ApplyToCurrentView_Execution()
        //{
        //    //_fixtureLabelEditorViewModel.SelectedOpenableLabel = _fixtureLabelEditorViewModel.AvailableOpenableSettings.First();

        //    //_fixtureLabelEditorViewModel.OpenFromFileCommand.Execute();

        //    //Assert.IsTrue(_fixtureLabelEditorViewModel.SetAndCloseCommand.CanExecute());

        //    //_fixtureLabelEditorViewModel.SetAndCloseCommand.Execute();

        //    //can't test
        //    Assert.Ignore();
        //}

        //[Test]
        //public void ApplyToAllViews_Execution()
        //{
        //    //_fixtureLabelEditorViewModel.SelectedOpenableLabel = _fixtureLabelEditorViewModel.AvailableOpenableSettings.First();

        //    //_fixtureLabelEditorViewModel.OpenFromFileCommand.Execute();

        //    //Assert.IsTrue(_fixtureLabelEditorViewModel.SetAllAndCloseCommand.CanExecute());

        //    //_fixtureLabelEditorViewModel.SetAndCloseCommand.Execute();

        //    //can't test
        //    Assert.Ignore();
        //}

        [Test]
        public void Cancel_Execution()
        {
            RelayCommand cmd = _fixtureLabelEditorViewModel.CancelCommand;

            Assert.IsNotNull(cmd.FriendlyName);

            Assert.IsTrue(cmd.CanExecute());
            WindowService.CloseWindowSetNullResponse();
            cmd.Execute();
        }

        //[Test]
        //public void SetField_Execution()
        //{
        //    RelayCommand cmd = _fixtureLabelEditorViewModel.SetFieldCommand;

        //    Assert.IsNotNull(cmd.FriendlyName);

        //    Assert.Ignore();
        //}

        [Test]
        public void SaveAsToFile_Execution()
        {
            //start with new label
            var viewModel = _fixtureLabelEditorViewModel;

            //set property
            String checkText = "<Bay>.<Name>";
            viewModel.SelectedLabel.Text = checkText;

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //fetch file
            Label fetchedLabel = Label.FetchByFilename(expectedPath);
            Assert.AreEqual(checkText, fetchedLabel.Text);
        }

        [Test]
        public void SaveAsToFile_ReplaceExecution()
        {
            //saved label
            Label test = Label.NewLabel(LabelType.Product);

            String checkText = "<Bay>.<Name>";
            test.Text = checkText;
            test.Name = Guid.NewGuid().ToString();

            String expectedPath = Path.Combine(this.TestDir, test.Name);
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            test = test.SaveAsFile(expectedPath);
            test.Dispose();
            test = null;

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //start with new label
            var viewModel = _fixtureLabelEditorViewModel;

            //change property
            checkText = "<Component>.<Brand>";
            viewModel.SelectedLabel.Text = checkText;

            //save
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //fetch file
            Label fetchedLabel = Label.FetchByFilename(expectedPath);
            Assert.AreEqual(checkText, fetchedLabel.Text);
        }

        //[Test]
        //public void SaveAs_Execution()
        //{
        //    Assert.Ignore();
        //}

        //[Test]
        //public void SaveAsToRepository_Execution()
        //{
        //    Assert.Ignore();
        //}

        [Test]
        public void OpenFromFile_Execution()
        {
            //saved label
            Label test = Label.NewLabel(LabelType.Product);

            String checkText = "<Bay>.<Name>";
            test.Text = checkText;
            test.Name = Guid.NewGuid().ToString();

            String expectedPath = Path.Combine(this.TestDir, test.Name);
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            test = test.SaveAsFile(expectedPath);
            test.Dispose();
            test = null;

            //open
            _fixtureLabelEditorViewModel.OpenFromFileCommand.Execute(expectedPath);

            //check if file has opened correctly
            Label fetchedLabel = Label.FetchByFilename(expectedPath);
            Assert.AreEqual(_fixtureLabelEditorViewModel.SelectedLabel.Text, fetchedLabel.Text);

        }

        //[Test]
        //public void Open_Execution()
        //{
        //    Assert.Ignore();
        //}

        //[Test]
        //public void OpenFromRepository_Execution()
        //{
        //    Assert.Ignore();
        //}

        #endregion

        #region OtherTests

        [Test]
        public void Other_OpenThenOverwrite()
        {
            //start with new label
            var viewModel = _fixtureLabelEditorViewModel;

            //set property
            String checkText = "<Bay>.<Name>";
            viewModel.SelectedLabel.Text = checkText;

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            viewModel.SelectedLabel.Text = "TTT";


            //open the label again
            viewModel.OpenFromFileCommand.Execute(expectedPath);
            Assert.AreEqual(checkText, viewModel.SelectedLabel.Text);

            //make another change
            checkText = "<Component>.<Brand>";
            viewModel.SelectedLabel.Text = checkText;
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            viewModel.SelectedLabel.Text = "TTT";
            viewModel.OpenFromFileCommand.Execute(expectedPath);
            Assert.AreEqual(checkText, viewModel.SelectedLabel.Text);
        }

        #endregion

    }
}
