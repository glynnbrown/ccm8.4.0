﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
// V8-25436 : L.Luong
//  Changed to match new design
// V8-28444 : M.Shelley
//  Fixed HighlightMethod_Characteristic unit test
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Settings;
using Galleria.Framework.ViewModel;
using System.IO;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;
using System.Globalization;
using Galleria.Ccm.Editor.Client.Wpf.Settings.HighlightEditor;

namespace Galleria.Ccm.UnitTests.UI.Settings
{
    [TestFixture]
    public class HighlightEditorViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private HighlightEditorViewModel _highlightEditorViewModel;

        PlanogramView _planogramView;

        public override void Setup()
        {
            base.Setup();

            Package pk = Package.NewPackage(null, PackageLockType.Unknown);
            pk.AddPlanogram();

            _planogramView = PackageViewModel.NewPackageViewModel(pk).PlanogramViews[0];

            _highlightEditorViewModel = new HighlightEditorViewModel(null, _planogramView);
        }

        private Highlight CreateTestHighlight1()
        {
            Highlight h1 = Highlight.NewHighlight();
            h1.Name = "h1";
            h1.Type = HighlightType.Position;
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogramProduct.Depth]";

            HighlightGroup group1 = HighlightGroup.NewHighlightGroup();
            group1.Name = "1";
            group1.Order = 1;
            group1.FillColour = 1;
            h1.Groups.Add(group1);

            HighlightGroup group2 = HighlightGroup.NewHighlightGroup();
            group2.Name = "2";
            group2.Order = 2;
            group2.FillColour = 2;
            h1.Groups.Add(group2);

            return h1;
        }

        private Highlight CreateTestHighlight2()
        {
            Highlight h2 = Highlight.NewHighlight();
            h2.Name = "h2";
            h2.Type = HighlightType.Position;
            h2.MethodType = HighlightMethodType.Characteristic;

            HighlightCharacteristic c1 = HighlightCharacteristic.NewHighlightCharacteristic();
            h2.Characteristics.Add(c1);
            c1.Name = "Wide 0";
            c1.IsAndFilter = true;

            HighlightCharacteristicRule c1r1 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            c1.Rules.Add(c1r1);
            c1r1.Field = "[PlanogramPosition.FacingsWide]";
            c1r1.Type = HighlightCharacteristicRuleType.Equals;
            c1r1.Value = "0";

            HighlightCharacteristic c2 = HighlightCharacteristic.NewHighlightCharacteristic();
            h2.Characteristics.Add(c2);
            c2.Name = "Wide 1-3";
            c2.IsAndFilter = true;

            HighlightCharacteristicRule c2r1 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            c2.Rules.Add(c2r1);
            c2r1.Field = "[PlanogramPosition.FacingsWide]";
            c2r1.Type = HighlightCharacteristicRuleType.MoreThan;
            c2r1.Value = "0";

            HighlightCharacteristicRule c2r2 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            c2.Rules.Add(c2r2);
            c2r2.Field = "[PlanogramPosition.FacingsWide]";
            c2r2.Type = HighlightCharacteristicRuleType.LessThanOrEqualTo;
            c2r2.Value = "3";


            HighlightCharacteristic c3 = HighlightCharacteristic.NewHighlightCharacteristic();
            h2.Characteristics.Add(c3);
            c3.Name = "Wide 4+";
            c3.IsAndFilter = true;

            HighlightCharacteristicRule c3r1 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            c2.Rules.Add(c3r1);
            c3r1.Field = "[PlanogramPosition.FacingsWide]";
            c3r1.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            c3r1.Value = "4";


            //groups

            HighlightGroup group1 = HighlightGroup.NewHighlightGroup();
            group1.Name = "Wide 0";
            group1.Order = 1;
            group1.FillColour = 1;
            h2.Groups.Add(group1);

            HighlightGroup group2 = HighlightGroup.NewHighlightGroup();
            group2.Name = "Wide 1-3";
            group2.Order = 2;
            group2.FillColour = 2;
            h2.Groups.Add(group2);

            HighlightGroup group3 = HighlightGroup.NewHighlightGroup();
            group3.Name = "Wide 4+";
            group3.Order = 3;
            group3.FillColour = 3;
            h2.Groups.Add(group3);

            return h2;
        }

        #endregion

        #region Properties

        [Test]
        public void SelectedHighlightProperty()
        {
            String propertyName = WpfHelper.GetPropertyPath<HighlightEditorViewModel>(p => p.SelectedHighlight).Path;

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, null);

            Assert.IsNotNull(model.SelectedHighlight);

            var highlight = model.SelectedHighlight;

            model.PropertyChanged += base.TestModel_PropertyChanged;

            model.SelectedHighlight = highlight;
            Assert.AreEqual(highlight, model.SelectedHighlight);

            Assert.Contains(propertyName, base.PropertyChangedNotifications);

            model.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void CurrentHighlightFiltersProperty()
        {
            HighlightEditorViewModel model = new HighlightEditorViewModel(null, null);

            var h1 = model.SelectedHighlight;
            var h2 = model.SelectedHighlight;

            model.SelectedHighlight = h1;
            Assert.AreEqual(0, model.SelectedHighlight.Filters.Count);
            Assert.AreEqual(1, model.CurrentHighlightFilters.Count, "Should have a blank filter loaded.");

            HighlightFilter filter = HighlightFilter.NewHighlightFilter();

            filter.Field = "[PlanogramPosition.FacingsWide]";
            filter.Value = "2";
            h2.Filters.Add(filter);

            model.SelectedHighlight = h2;
            Assert.AreEqual(2, model.CurrentHighlightFilters.Count, "Should have a blank filter loaded.");
        }

        [Test]
        public void CharacteristicGroupsProperty()
        {
            HighlightEditorViewModel model = new HighlightEditorViewModel(null, null);

            model.SelectedHighlight = CreateTestHighlight2();
            Assert.AreEqual(model.SelectedHighlight.MethodType, HighlightMethodType.Characteristic);

            Assert.AreEqual(model.SelectedHighlight.Characteristics.Count, model.CharacteristicGroups.Count);

            model.SelectedHighlight = CreateTestHighlight1();
            Assert.AreEqual(0, model.CharacteristicGroups.Count);

            model.SelectedHighlight.MethodType = HighlightMethodType.Characteristic;
            Assert.AreEqual(1, model.CharacteristicGroups.Count);
        }

        #endregion

        #region Commands

        [Test]
        public void SaveAsToFile_Execution()
        {
            //Create ViewModel
            var model = _highlightEditorViewModel;

            //Create Highlight
            String checkText = "[PlanogramPosition.FacingsWide]";
            model.SelectedHighlight.MethodType = HighlightMethodType.Group;
            model.SelectedHighlight.Field1 = checkText;

            //Save Highlight
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);
            model.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //fetch file
            Highlight fetchedHighlight = Highlight.FetchByFilename(expectedPath);
            Assert.AreEqual(checkText, fetchedHighlight.Field1);
        }

        [Test]
        public void SaveAsToFile_ReplaceExecution()
        {
            //Create ViewModel
            var model = _highlightEditorViewModel;

            //Create Highlight
            String checkText = "[PlanogramPosition.FacingsWide]";
            model.SelectedHighlight.MethodType = HighlightMethodType.Group;
            model.SelectedHighlight.Field1 = checkText;

            //Save Highlight
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);
            model.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //change property
            checkText = "[PlanogramPosition.UnitsHigh]";
            model.SelectedHighlight.Field1 = checkText;

            //save
            model.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //fetch file
            Highlight fetchedHighlight = Highlight.FetchByFilename(expectedPath);
            Assert.AreEqual(checkText, fetchedHighlight.Field1);

        }

        [Test]
        public void OpenFromFile_Execution()
        {
            //saved highlight
            Highlight test = Highlight.NewHighlight();

            test = CreateTestHighlight1();

            String expectedPath = Path.Combine(this.TestDir, test.Name);
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);
            test = test.SaveAsFile(expectedPath);
            test.Dispose();
            test = null;

            //open
            _highlightEditorViewModel.OpenFromFileCommand.Execute(expectedPath);

            //check if highlight has opened correctly
            Highlight fetchedHighlight = Highlight.FetchByFilename(expectedPath);
            Assert.AreEqual(_highlightEditorViewModel.SelectedHighlight.Field1, fetchedHighlight.Field1);
        }

        [Test]
        public void Cancel_Execution()
        {
            HighlightEditorViewModel model = new HighlightEditorViewModel(null, null);
            RelayCommand cmd = model.CancelCommand;

            Assert.IsNotNull(cmd.FriendlyName);

            WindowService.CloseWindowSetNullResponse();
            Assert.IsTrue(cmd.CanExecute());
            cmd.Execute();

        }

        //[Test]
        //public void SetField_Execution()
        //{
        //    RelayCommand cmd = _highlightEditorViewModel.SetFieldCommand;

        //    Assert.IsNotNull(cmd.FriendlyName);

        //    Assert.Ignore();
        //}


        [Test]
        public void AddCharacteristic_Execution()
        {
            HighlightEditorViewModel model = new HighlightEditorViewModel(null, null);

            model.SelectedHighlight = CreateTestHighlight2();

            Int32 preCount = model.CharacteristicGroups.Count;
            model.AddCharacteristicCommand.Execute();
            Assert.AreEqual(preCount + 1, model.CharacteristicGroups.Count);
        }

        [Test]
        public void RemoveCharacteristic_CanExecute_CannotRemoveLast()
        {
            HighlightEditorViewModel model = new HighlightEditorViewModel(null, null);

            model.SelectedHighlight = CreateTestHighlight1();
            model.SelectedHighlight.MethodType = HighlightMethodType.Characteristic;

            Assert.AreEqual(1, model.CharacteristicGroups.Count);
            Assert.IsFalse(model.RemoveCharacteristicCommand.CanExecute(model.CharacteristicGroups.First()));

            model.AddCharacteristicCommand.Execute();

            Assert.AreEqual(2, model.CharacteristicGroups.Count);
            Assert.IsTrue(model.RemoveCharacteristicCommand.CanExecute(model.CharacteristicGroups[0]));
            Assert.IsTrue(model.RemoveCharacteristicCommand.CanExecute(model.CharacteristicGroups[1]));
        }

        [Test]
        public void RemoveCharacteristic_Execution()
        {
            CreateTestHighlight1();
            HighlightEditorViewModel model = new HighlightEditorViewModel(null, null);

            model.SelectedHighlight = CreateTestHighlight1();
            model.SelectedHighlight.MethodType = HighlightMethodType.Characteristic;
            model.AddCharacteristicCommand.Execute();

            var cGroup = model.CharacteristicGroups.Last();

            model.RemoveCharacteristicCommand.Execute(cGroup);

            Assert.IsFalse(model.CharacteristicGroups.Contains(cGroup));
            Assert.IsFalse(model.SelectedHighlight.Characteristics.Contains(cGroup.Model));
        }


        #endregion

        #region Other Tests

        [Test]
        public void Other_OpenThenOverwrite()
        {
            //start with new label
            var model = _highlightEditorViewModel;

            //set property
            String checkText = "[PlanogramPosition.FacingsWide]";
            model.SelectedHighlight.Field1 = checkText;

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);
            model.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            model.SelectedHighlight.Field1 = "TTT";


            //open the label again
            model.OpenFromFileCommand.Execute(expectedPath);
            Assert.AreEqual(checkText, model.SelectedHighlight.Field1);

            //make another change
            checkText = "[PlanogramPosition.UnitsHigh]";
            model.SelectedHighlight.Field1 = checkText;
            model.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            model.SelectedHighlight.Field1 = "TTT";
            model.OpenFromFileCommand.Execute(expectedPath);
            Assert.AreEqual(checkText, model.SelectedHighlight.Field1);
        }

        //[Test]
        //public void Other_InsertFilterString()
        //{
        //    //setup planogram
        //    PlanogramView planView = new PlanogramView(TestDataHelper.CreatePlanCreweSparklingWater());

        //    //create view model
        //    HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

        //    //create highlight group
        //    var selectedHighlight = model.SelectedHighlight;
        //    selectedHighlight.MethodType = HighlightMethodType.Group;

        //    //set field
        //    selectedHighlight.Field1 = "<Product.Brand>";
        //    Assert.AreEqual(7, selectedHighlight.Groups.Count());
        //    Assert.AreEqual("<null>", selectedHighlight.Groups[0].Name);
        //    Assert.AreEqual("Badoit", selectedHighlight.Groups[1].Name);
        //    Assert.AreEqual("Buxton", selectedHighlight.Groups[2].Name);
        //    Assert.AreEqual("Highland Spring", selectedHighlight.Groups[3].Name);
        //    Assert.AreEqual("Morrisons", selectedHighlight.Groups[4].Name);
        //    Assert.AreEqual("Savers", selectedHighlight.Groups[5].Name);
        //    Assert.AreEqual("Strathmore", selectedHighlight.Groups[6].Name);

        //}

        #endregion

        #region Highlight Method Tests

        [Test]
        public void HighlightMethod_ColourScale()
        {
            PlanogramView planView =
                PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

            var selectedHighlight = model.SelectedHighlight;
            selectedHighlight.MethodType = HighlightMethodType.ColourScale;
            selectedHighlight.Field1 = "[PlanogramProduct.Height]";

            Assert.AreEqual(11, selectedHighlight.Groups.Count());

            Assert.AreEqual(Convert.ToString(21.5F, CultureInfo.CurrentCulture), selectedHighlight.Groups[0].Name);
            Assert.AreEqual(Convert.ToString(21.8F, CultureInfo.CurrentCulture), selectedHighlight.Groups[1].Name);
            Assert.AreEqual(Convert.ToString(23F, CultureInfo.CurrentCulture), selectedHighlight.Groups[2].Name);
            Assert.AreEqual(Convert.ToString(26.5F, CultureInfo.CurrentCulture), selectedHighlight.Groups[3].Name);
            Assert.AreEqual(Convert.ToString(30.8F, CultureInfo.CurrentCulture), selectedHighlight.Groups[4].Name);
            Assert.AreEqual(Convert.ToString(30.9F, CultureInfo.CurrentCulture), selectedHighlight.Groups[5].Name);
            Assert.AreEqual(Convert.ToString(32F, CultureInfo.CurrentCulture), selectedHighlight.Groups[6].Name);
            Assert.AreEqual(Convert.ToString(32.2F, CultureInfo.CurrentCulture), selectedHighlight.Groups[7].Name);
            Assert.AreEqual(Convert.ToString(32.5F, CultureInfo.CurrentCulture), selectedHighlight.Groups[8].Name);
            Assert.AreEqual(Convert.ToString(32.6F, CultureInfo.CurrentCulture), selectedHighlight.Groups[9].Name);
            Assert.AreEqual(Convert.ToString(34.4F, CultureInfo.CurrentCulture), selectedHighlight.Groups[10].Name);
        }

        [Test]
        public void HighlightMethod_Group()
        {
            PlanogramView planView = PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

            var selectedHighlight = model.SelectedHighlight;
            selectedHighlight.MethodType = HighlightMethodType.Group;
            selectedHighlight.Field1 = "[PlanogramPosition.FacingsWide]";

            Assert.AreEqual(5, selectedHighlight.Groups.Count());

            Assert.AreEqual(Convert.ToString(1, CultureInfo.CurrentCulture), selectedHighlight.Groups[0].Name);
            Assert.AreEqual(Convert.ToString(2, CultureInfo.CurrentCulture), selectedHighlight.Groups[1].Name);
            Assert.AreEqual(Convert.ToString(3, CultureInfo.CurrentCulture), selectedHighlight.Groups[2].Name);
            Assert.AreEqual(Convert.ToString(8, CultureInfo.CurrentCulture), selectedHighlight.Groups[3].Name);
            Assert.AreEqual(Convert.ToString(11, CultureInfo.CurrentCulture), selectedHighlight.Groups[4].Name);
        }

        [Test]
        public void HighlightMethod_Quadrant()
        {
            PlanogramView planView = PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

            var selectedHighlight = model.SelectedHighlight;
            selectedHighlight.MethodType = HighlightMethodType.Quadrant;
            selectedHighlight.Field1 =
                ObjectFieldInfo.NewObjectFieldInfo(
                typeof(PlanogramProduct), PlanogramProduct.FriendlyName,
                PlanogramProduct.WidthProperty).FieldPlaceholder;

            selectedHighlight.QuadrantXType = HighlightQuadrantType.Mean;

            selectedHighlight.Field2 =
                ObjectFieldInfo.NewObjectFieldInfo(
                typeof(PlanogramProduct), PlanogramProduct.FriendlyName,
                PlanogramProduct.HeightProperty).FieldPlaceholder;

            selectedHighlight.QuadrantYType = HighlightQuadrantType.Mean;
            Assert.AreEqual(4, selectedHighlight.Groups.Count());

            Assert.AreEqual("X < 14.62 & Y < 29.77", selectedHighlight.Groups[0].Name);
            Assert.AreEqual("X < 14.62 & Y >= 29.77", selectedHighlight.Groups[1].Name);
            Assert.AreEqual("X >= 14.62 & Y < 29.77", selectedHighlight.Groups[2].Name);
            Assert.AreEqual("X >= 14.62 & Y >= 29.77", selectedHighlight.Groups[3].Name);
        }

        [Test]
        public void HighlightMethod_Characteristic()
        {
            PlanogramView planView = PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

            var selectedHighlight = model.SelectedHighlight;
            selectedHighlight.MethodType = HighlightMethodType.Characteristic;

            var cGroup1 = model.CharacteristicGroups[0];
            cGroup1.Model.Name = "Wide 0";
            cGroup1.Model.IsAndFilter = true;

            cGroup1.Model.Rules.Add(HighlightCharacteristicRule.NewHighlightCharacteristicRule());
            cGroup1.Model.Rules[0].Field = "[PlanogramPosition.FacingsWide]";
            cGroup1.Model.Rules[0].Value = "0";

            model.AddCharacteristicCommand.Execute();
            var cGroup2 = model.CharacteristicGroups[1];
            cGroup2.Model.Name = "Wide 1-3";
            cGroup2.Model.IsAndFilter = true;

            cGroup2.Model.Rules.Add(HighlightCharacteristicRule.NewHighlightCharacteristicRule());
            cGroup2.Model.Rules.Add(HighlightCharacteristicRule.NewHighlightCharacteristicRule());

            cGroup2.Model.Rules[0].Field = "[PlanogramPosition.FacingsWide]";
            cGroup2.Model.Rules[0].Type = HighlightCharacteristicRuleType.MoreThan;
            cGroup2.Model.Rules[0].Value = "0";

            cGroup2.Model.Rules[1].Field = "[PlanogramPosition.FacingsWide]";
            cGroup2.Model.Rules[1].Type = HighlightCharacteristicRuleType.LessThanOrEqualTo;
            cGroup2.Model.Rules[1].Value = "3";

            Assert.AreEqual(2, selectedHighlight.Groups.Count());

            Assert.AreEqual(cGroup1.Model.Name, selectedHighlight.Groups[0].Name);
            Assert.AreEqual(cGroup2.Model.Name, selectedHighlight.Groups[1].Name);
        }

        #endregion

        #region Formula Tests

        [Test]
        public void FormulaText_SingleField()
        {
            PlanogramView planView = 
                PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

            var selectedHighlight = model.SelectedHighlight;
            selectedHighlight.MethodType = HighlightMethodType.Group;

            //String field
            selectedHighlight.Field1 = "[PlanogramProduct.Brand]";
            Assert.AreEqual(7, selectedHighlight.Groups.Count());
            Assert.AreEqual("<null>", selectedHighlight.Groups[0].Name);
            Assert.AreEqual("Badoit", selectedHighlight.Groups[1].Name);
            Assert.AreEqual("Buxton", selectedHighlight.Groups[2].Name);
            Assert.AreEqual("Highland Spring", selectedHighlight.Groups[3].Name);
            Assert.AreEqual("Morrisons", selectedHighlight.Groups[4].Name);
            Assert.AreEqual("Savers", selectedHighlight.Groups[5].Name);
            Assert.AreEqual("Strathmore", selectedHighlight.Groups[6].Name);

            //Numerical Field
            selectedHighlight.Field1 = "[PlanogramPosition.FacingsWide]";
            Assert.AreEqual(5, selectedHighlight.Groups.Count());
            Assert.AreEqual(Convert.ToString(1, CultureInfo.CurrentCulture), selectedHighlight.Groups[0].Name);
            Assert.AreEqual(Convert.ToString(2, CultureInfo.CurrentCulture), selectedHighlight.Groups[1].Name);
            Assert.AreEqual(Convert.ToString(3, CultureInfo.CurrentCulture), selectedHighlight.Groups[2].Name);
            Assert.AreEqual(Convert.ToString(8, CultureInfo.CurrentCulture), selectedHighlight.Groups[3].Name);
            Assert.AreEqual(Convert.ToString(11, CultureInfo.CurrentCulture), selectedHighlight.Groups[4].Name);
        }

        [Test]
        public void FormulaText_SimpleNumericalFormula()
        {
            PlanogramView planView = PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

            var selectedHighlight = model.SelectedHighlight;
            selectedHighlight.MethodType = HighlightMethodType.Group;

            selectedHighlight.Field1 = "{[PlanogramProduct.Width]/2}";
            Assert.AreEqual(10, selectedHighlight.Groups.Count());
            Assert.AreEqual(Convert.ToString(4.1, CultureInfo.CurrentCulture), selectedHighlight.Groups[0].Name);
            Assert.AreEqual(Convert.ToString(4.2, CultureInfo.CurrentCulture), selectedHighlight.Groups[1].Name);
            Assert.AreEqual(Convert.ToString(4.25, CultureInfo.CurrentCulture), selectedHighlight.Groups[2].Name);
            Assert.AreEqual(Convert.ToString(4.75, CultureInfo.CurrentCulture), selectedHighlight.Groups[3].Name);
            Assert.AreEqual(Convert.ToString(4.85, CultureInfo.CurrentCulture), selectedHighlight.Groups[4].Name);
            Assert.AreEqual(Convert.ToString(9.5, CultureInfo.CurrentCulture), selectedHighlight.Groups[5].Name);
            Assert.AreEqual(Convert.ToString(9.75, CultureInfo.CurrentCulture), selectedHighlight.Groups[6].Name);
            Assert.AreEqual(Convert.ToString(10, CultureInfo.CurrentCulture), selectedHighlight.Groups[7].Name);
            Assert.AreEqual(Convert.ToString(12.25, CultureInfo.CurrentCulture), selectedHighlight.Groups[8].Name);
            Assert.AreEqual(Convert.ToString(12.5, CultureInfo.CurrentCulture), selectedHighlight.Groups[9].Name);
        }

        [Test]
        public void FormulaText_BooleanFormula()
        {
            PlanogramView planView = PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

            var selectedHighlight = model.SelectedHighlight;
            selectedHighlight.MethodType = HighlightMethodType.Group;

            selectedHighlight.Field1 = "{[PlanogramProduct.Width]>10}";
            Assert.AreEqual(2, selectedHighlight.Groups.Count());
            Assert.AreEqual("False", selectedHighlight.Groups[0].Name);
            Assert.AreEqual("True", selectedHighlight.Groups[1].Name);
        }

        //[Test]
        //public void FormulaText_SumFormula()
        //{
        //    Assert.Ignore();

        //    //PlanogramView planView = new PlanogramView(TestDataHelper.CreatePlanCreweSparklingWater());

        //    //HighlightEditorViewModel model = new HighlightEditorViewModel(null, planView);

        //    //var selectedHighlight = model.SelectedHighlight;
        //    //selectedHighlight.MethodType = HighlightMethodType.Group;

        //    //Single totalWidth = planView.EnumerateAllPositions().Sum(p => p.Product.Width);

        //    //selectedHighlight.Field1 = "SUM([PlanogramProduct.Width])";
        //    //Assert.AreEqual(1, selectedHighlight.Groups.Count());
        //    //Assert.AreEqual(Convert.ToString(totalWidth, CultureInfo.CurrentCulture), selectedHighlight.Groups[0].Name);


        //    //selectedHighlight.Field1 = "<Product.Width>/SUM([PlanogramProduct.Width])";
        //    //Assert.AreEqual(10, selectedHighlight.Groups.Count());

        //}

        #endregion

    }


}
