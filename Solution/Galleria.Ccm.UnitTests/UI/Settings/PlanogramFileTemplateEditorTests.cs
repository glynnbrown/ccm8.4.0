﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-27804 : L.Ineson
//  Created
// V8-28219 : N.Haywood
//  changed PlanogramImportFileType.Spaceman to SpacemanV9
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using System.IO;
using Galleria.Ccm.Editor.Client.Wpf.Settings;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor;
using System.Runtime.InteropServices;

namespace Galleria.Ccm.UnitTests.UI.Settings
{
    [TestFixture]
    public class PlanogramFileTemplateEditorTests : TestBase
    {
        #region Test Fixture Helpers

        public override void Setup()
        {
            base.Setup();
        }

        private String[] InsertData()
        {
            String[] paths = new String[2];

            using (PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1"))
            {
                template.Name = "template1";
                template.UpdateFromCcmFieldList();
                template.Mappings[0].ExternalField = "field1";
                String path = Path.ChangeExtension(Path.Combine(this.TestDir, template.Name), PlanogramImportTemplate.FileExtension);
                template.SaveAsFile(path);
                paths[0] = template.Id.ToString();
            }

            using (PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1"))
            {
                template.Name = "template2";
                template.UpdateFromCcmFieldList();
                template.Mappings[1].ExternalField = "field2";
                String path = Path.ChangeExtension(Path.Combine(this.TestDir, template.Name), PlanogramImportTemplate.FileExtension);
                template.SaveAsFile(path);
                paths[1] = template.Id.ToString();

            }

            return paths;
        }



        private Boolean IsFileLocked(String filePath)
        {
            try
            {
                using (System.IO.File.Open(filePath, FileMode.Open)) { }
            }
            catch (IOException e)
            {
                var errorCode = Marshal.GetHRForException(e) & ((1 << 16) - 1);
                return errorCode == 32 || errorCode == 33;
            }

            return false;
        }

        #endregion

        #region Properties

        #region SelectedItem

        [Test]
        public void Property_SelectedItem_UpdatedOnOpen()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.SelectedItemProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region MappingTemplateName

        [Test]
        public void Property_MappingTemplateName_UpdatedOnSelectedItemChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.MappingTemplateNameProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_MappingTemplateName_UpdatedOnSelectedItemDirty()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.OpenCommand.Execute(filePaths[0]);

            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.PlanogramMappings.First().ExternalField = testModel.PlanogramMappings.First().AvailableExternalFields.First();

            Assert.Contains(PlanogramFileTemplateEditorViewModel.MappingTemplateNameProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region SelectedFileType


        [Test]
        public void Property_SelectedFileType_UpdatedOnSelectedItemChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.SelectedFileTypeProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region AvailableVersions


        [Test]
        public void Property_AvailableVersions_UpdatedOnSelectedItemChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.AvailableVersionsProperty.Path, base.PropertyChangedNotifications);
            Assert.AreNotEqual(0, testModel.AvailableVersions.Count);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        //[Test]
        //public void Property_AvailableVersions_UpdatedOnFileTypeChange()
        //{
        //    throw new InconclusiveException("Only have one type atm");
        //}

        #endregion

        #region SelectedVersion

        [Test]
        public void Property_SelectedVersion_UpdatedOnFileTypeChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.SelectedVersionProperty.Path, base.PropertyChangedNotifications);
            Assert.IsNotNullOrEmpty(testModel.SelectedVersion);
            Assert.Contains(testModel.SelectedVersion, testModel.AvailableVersions);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region Mappings

        [Test]
        public void Property_PlanogramMappings_UpdatedOnSelectedItemChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.PlanogramMappingsProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        //[Test]
        //public void Property_PlanogramMappings_UpdatedOnFileTypeChange()
        //{
        //    throw new NotImplementedException();
        //}

        [Test]
        public void Property_BayMappings_UpdatedOnSelectedItemChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.BayMappingsProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        //[Test]
        //public void Property_BayMappings_UpdatedOnFileTypeChange()
        //{
        //    throw new NotImplementedException();
        //}

        [Test]
        public void Property_ComponentMappings_UpdatedOnSelectedItemChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.ComponentMappingsProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        //[Test]
        //public void Property_ComponentMappings_UpdatedOnFileTypeChange()
        //{
        //    throw new NotImplementedException();
        //}

        [Test]
        public void Property_ProductMappings_UpdatedOnSelectedItemChange()
        {
            String[] filePaths = InsertData();

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(filePaths[0]);

            Assert.Contains(PlanogramFileTemplateEditorViewModel.ProductMappingsProperty.Path, base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        //[Test]
        //public void Property_ProductMappings_UpdatedOnFileTypeChange()
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        #endregion

        #region Constructor

        [Test]
        public void Constructor_InitialFilePath_Set()
        {
            //insert a template to open
            String testPath;

            using (PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1"))
            {
                template.Name = "template1";
                template.UpdateFromCcmFieldList();

                testPath = Path.ChangeExtension(Path.Combine(this.TestDir, template.Name), PlanogramImportTemplate.FileExtension);
                template.SaveAsFile(testPath);
                testPath = template.Id.ToString();
            }

            var testModel = new PlanogramFileTemplateEditorViewModel(testPath, PlanogramImportFileType.SpacemanV9);
            Assert.AreEqual("template1", testModel.SelectedItem.Name, "The test path should have been loaded.");
        }

        [Test]
        public void Constructor_InitialFilePath_NotSet()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            Assert.IsNullOrEmpty(testModel.SelectedItem.Name, "A new item should have been loaded");
        }

        [Test]
        public void Constructor_InitialFilePath_SetButNotAvailable()
        {
            String testPath = Path.ChangeExtension(Path.Combine(this.TestDir, "template1"), PlanogramImportTemplate.FileExtension);

            var testModel = new PlanogramFileTemplateEditorViewModel(testPath, PlanogramImportFileType.SpacemanV9);
            Assert.IsNullOrEmpty(testModel.SelectedItem.Name, "A new item should have been loaded");
        }

        #endregion

        #region Commands

        #region OK

        [Test]
        public void Command_OK_Executed()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = testModel.OKCommand;
            Assert.IsNotNull(cmd.FriendlyName, "Friendly name should be set");

            cmd.Execute();

            Assert.AreEqual(true, testModel.DialogResult);
        }

        #endregion

        #region Cancel

        [Test]
        public void Command_Cancel_Executed()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);

            RelayCommand cmd = testModel.CancelCommand;
            Assert.IsNotNull(cmd.FriendlyName, "Friendly name should be set");

            cmd.Execute();

            Assert.AreEqual(false, testModel.DialogResult);
        }

        #endregion

        #region Open

        [Test]
        public void Command_Open_Executed()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            //insert a template to open
            String testPath;
            String testMapping;
            String externalField;

            using (PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1"))
            {
                template.Name = "template1";
                template.UpdateFromCcmFieldList();

                var editMapping = template.Mappings.FirstOrDefault(m=> m.FieldType == PlanogramFieldMappingType.Planogram);
                editMapping.ExternalField = testModel.PlanogramMappings.First().AvailableExternalFields.First().Field;
                testMapping = editMapping.Field;
                externalField = editMapping.ExternalField;

                testPath = Path.ChangeExtension(Path.Combine(this.TestDir, template.Name), PlanogramImportTemplate.FileExtension);
                template.SaveAsFile(testPath);
                testPath = template.Id.ToString();
            }

          
            //open it
            base.PropertyChangedNotifications.Clear();

            testModel.OpenCommand.Execute(testPath);

            //check
            Assert.Contains(PlanogramFileTemplateEditorViewModel.SelectedItemProperty.Path, base.PropertyChangedNotifications, "Should have fired selected item changed");
            Assert.AreEqual("template1", testModel.SelectedItem.Name, "Incorrect name");

            var mapping = testModel.PlanogramMappings.FirstOrDefault(m => m.Field == testMapping);
            Assert.AreEqual(externalField, mapping.ExternalField.Field, "Field should be set"); 

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Command_Open_FileNotKeptLocked()
        {
            //insert a template to open
            String testPath;
            String testMapping;

            using (PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1"))
            {
                template.Name = "template1";
                template.UpdateFromCcmFieldList();

                var editMapping = template.Mappings.FirstOrDefault(m => m.FieldType == PlanogramFieldMappingType.Planogram);
                editMapping.ExternalField = "field1";
                testMapping = editMapping.Field;

                testPath = Path.ChangeExtension(Path.Combine(this.TestDir, template.Name), PlanogramImportTemplate.FileExtension);
                template.SaveAsFile(testPath);
                testPath = template.Id.ToString();
            }

            //open it
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.OpenCommand.Execute(testPath);

            //check that the file itself is not locked open
            Assert.IsFalse(IsFileLocked(testPath), "File should not be kept locked");
        }

        #endregion

        #region SaveAs

        [Test]
        public void Command_SaveAs_Executed()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.SelectedItem.Name = Guid.NewGuid().ToString();
            testModel.PlanogramMappings.First().ExternalField = testModel.PlanogramMappings.First().AvailableExternalFields.First();

            String ccmField = testModel.PlanogramMappings.First().Field;
            String externalField = testModel.PlanogramMappings.First().ExternalField.Field;

            String expectedPath = Path.ChangeExtension(Path.Combine(this.TestDir, testModel.SelectedItem.Name), PlanogramImportTemplate.FileExtension);
            testModel.SaveAsCommand.Execute(expectedPath);

            Assert.IsTrue(System.IO.File.Exists(expectedPath), "File was not saved as expected");

            //fetch and check
            using (PlanogramImportTemplate template = PlanogramImportTemplate.FetchByFilename(expectedPath, /*asReadOnly*/true))
            {
                Assert.AreEqual(testModel.SelectedItem.Name, template.Name, "Name incorrect");
                Assert.AreEqual(externalField,
                    template.Mappings.FirstOrDefault(f => f.FieldType == PlanogramFieldMappingType.Planogram
                    && f.Field == ccmField).ExternalField, "External field incorrect");
            }
        }

        [Test]
        public void Command_SaveAs_OverwriteExisting()
        {
            //insert a template overwrite
            String testPath;
            String testMapping;

            using (PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1"))
            {
                template.Name = "template1";
                template.UpdateFromCcmFieldList();

                var editMapping = template.Mappings.FirstOrDefault(m => m.FieldType == PlanogramFieldMappingType.Planogram);
                editMapping.ExternalField = "field1";
                testMapping = editMapping.Field;

                testPath = Path.ChangeExtension(Path.Combine(this.TestDir, template.Name), PlanogramImportTemplate.FileExtension);
                template.SaveAsFile(testPath);
                testPath = template.Id.ToString();
            }

            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.SelectedItem.Name = "template1";

            String expectedPath = Path.ChangeExtension(Path.Combine(this.TestDir, testModel.SelectedItem.Name), PlanogramImportTemplate.FileExtension);
            testModel.SaveAsCommand.Execute(expectedPath);

            Assert.IsTrue(System.IO.File.Exists(expectedPath), "File was not saved as expected");
        }

        [Test]
        public void Command_SaveAs_FileNotKeptLocked()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            testModel.SelectedItem.Name = Guid.NewGuid().ToString();

            String expectedPath = Path.ChangeExtension(Path.Combine(this.TestDir, testModel.SelectedItem.Name), PlanogramImportTemplate.FileExtension);

            testModel.SaveAsCommand.Execute(expectedPath);

            Assert.IsTrue(System.IO.File.Exists(expectedPath), "File was not saved as expected");
            Assert.IsFalse(IsFileLocked(expectedPath), "File should not be kept locked");
        }

        [Test]
        public void Command_SaveAs_OpenThenSaveAs()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);

            //insert a template to open
            String testPath;
            String testMapping;
            String externalField;

            using (PlanogramImportTemplate template = PlanogramImportTemplate.NewPlanogramImportTemplate(PlanogramImportFileType.SpacemanV9, "1"))
            {
                template.Name = "template1";
                template.UpdateFromCcmFieldList();

                var editMapping = template.Mappings.FirstOrDefault(m => m.FieldType == PlanogramFieldMappingType.Planogram);
                editMapping.ExternalField = testModel.PlanogramMappings.First().AvailableExternalFields.First().Field;
                testMapping = editMapping.Field;
                externalField = editMapping.ExternalField;

                testPath = Path.ChangeExtension(Path.Combine(this.TestDir, template.Name), PlanogramImportTemplate.FileExtension);
                template.SaveAsFile(testPath);
                testPath = template.Id.ToString();
            }


            //open it
            testModel.OpenCommand.Execute(testPath);

            //change it slightly
            testModel.PlanogramMappings.First().ExternalField = testModel.PlanogramMappings.First().AvailableExternalFields.ElementAt(1);

            //save it as another name
            String path2 = Path.ChangeExtension(Path.Combine(this.TestDir, "nn"), PlanogramImportTemplate.FileExtension);
            testModel.SaveAsCommand.Execute(path2);
        }

        #endregion

        #region ClearMapping

        [Test]
        public void Command_ClearMapping_Executed()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            var mapping = testModel.PlanogramMappings.First();
            mapping.ExternalField = mapping.AvailableExternalFields.First();

            Assert.IsNotNull(mapping.ExternalField, "mapping should be set");

            testModel.ClearMappingCommand.Execute(mapping);
            
            Assert.IsNull(mapping.ExternalField, "mapping should have been cleared");
        }

        #endregion

        #region ClearAllMappings

        [Test]
        public void Command_ClearAllMapping_Executed()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            var mapping = testModel.PlanogramMappings.First();
            mapping.ExternalField = mapping.AvailableExternalFields.First();
            var mapping1 = testModel.ProductMappings.First();
            mapping1.ExternalField = mapping1.AvailableExternalFields.First();

            Assert.IsNotNull(mapping.ExternalField, "mapping should be set");
            Assert.IsNotNull(mapping1.ExternalField, "mapping should be set");

            testModel.ClearAllMappingsCommand.Execute();

            Assert.IsNull(mapping.ExternalField, "mapping should have been cleared");
            Assert.IsNull(mapping1.ExternalField, "mapping should have been cleared");
        }

        #endregion

        #endregion

        #region Other

        [Test]
        public void Mappings_CCMFieldsAreDistinct()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            Assert.AreNotEqual(0, testModel.SelectedItem.Mappings.Count);

            StringBuilder sb = new StringBuilder();
            foreach (var mapTypeGroup in testModel.SelectedItem.Mappings.GroupBy(m => m.FieldType))
            {
                foreach (var mapGroup in mapTypeGroup.GroupBy(m => m.Field))
                {
                    if (mapGroup.Count() > 1)
                    {
                        sb.AppendLine(mapTypeGroup.Key + "." + mapGroup.Key);
                    }
                }
            }

            String duplicates = sb.ToString();
            Assert.IsNullOrEmpty(duplicates, duplicates);
        }

        [Test]
        public void Mappings_SpacemanExternalAreDistinct()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);

            List<IEnumerable<MappingRow>> mappings = new List<IEnumerable<MappingRow>>
            {
                 testModel.PlanogramMappings,
                 testModel.BayMappings,
                 testModel.ComponentMappings,
                 testModel.ProductMappings
            };

            foreach (var mappingCollection in mappings)
            {
                var firstMapping = mappingCollection.First();
                Assert.AreNotEqual(0, firstMapping.AvailableExternalFields.Count());
                Assert.AreEqual(firstMapping.AvailableExternalFields.Count(), firstMapping.AvailableExternalFields.Select(f => f.Field).Distinct().Count(),
                    "All available fields should be distinct");
            }

        }

        [Test]
        public void MappingRow_SetExternalField()
        {
            var testModel = new PlanogramFileTemplateEditorViewModel(null, PlanogramImportFileType.SpacemanV9);
            var mappingRow = testModel.PlanogramMappings.First();

            mappingRow.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            var selectedField = mappingRow.AvailableExternalFields.First();
            mappingRow.ExternalField = selectedField;

            Assert.Contains("ExternalField", base.PropertyChangedNotifications, "Property change not fired.");
            Assert.AreEqual(selectedField, mappingRow.ExternalField, "Field not set on row");
            Assert.AreEqual(selectedField.Field, testModel.SelectedItem.Mappings.First().ExternalField, "Field not set on model");

            mappingRow.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion
    }
}
