﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
// V8-26248 : L.Luong
//  Changed to fit new Design
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using System.IO;
using Galleria.Ccm.Editor.Client.Wpf.Settings;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.UI.Settings
{
    [TestFixture]
    class ProductLabelEditorViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private ProductLabelEditorViewModel _productLabelEditorViewModel;

        PlanogramView _planogramView;

        public override void Setup()
        {
            base.Setup();
            InsertLabels();

            Package pk = Package.NewPackage(null, PackageLockType.Unknown);
            pk.Planograms.Add();
            _planogramView = PackageViewModel.NewPackageViewModel(pk).PlanogramViews[0];

            _productLabelEditorViewModel = new ProductLabelEditorViewModel(null, _planogramView);
        }

        private void InsertLabels()
        {
            using (Label l1 = Label.NewLabel(LabelType.Product))
            {
                l1.Name = "l1";
                String path = Path.Combine(this.TestDir, l1.Name);
                path = Path.ChangeExtension(path, Label.FileExtension);
                l1.SaveAsFile(path);
            }

            using (Label l2 = Label.NewLabel(LabelType.Product))
            {
                l2.Name = "l2";
                String path = Path.Combine(this.TestDir, l2.Name);
                path = Path.ChangeExtension(path, Label.FileExtension);
                l2.SaveAsFile(path);
            }
        }

        #endregion

        #region Properties

        [Test]
        public void SelectedLabelProperty()
        {
            String propertyName = WpfHelper.GetPropertyPath<ProductLabelEditorViewModel>(p => p.SelectedLabel).Path;
            InsertLabels();

            ProductLabelEditorViewModel model = new ProductLabelEditorViewModel(null, null);

            Assert.IsNotNull(_productLabelEditorViewModel.SelectedLabel);

            var label = model.SelectedLabel;

            _productLabelEditorViewModel.PropertyChanged += base.TestModel_PropertyChanged;

            _productLabelEditorViewModel.SelectedLabel = label;
            Assert.AreEqual(label, _productLabelEditorViewModel.SelectedLabel);

            Assert.Contains(propertyName, base.PropertyChangedNotifications);

            model.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void AvailableFontsProperty()
        {
            Assert.IsNotEmpty(_productLabelEditorViewModel.AvailableFonts);
        }

        [Test]
        public void AvailableFieldsProperty()
        {
            Assert.IsNotEmpty(_productLabelEditorViewModel.AvailableFonts);
        }

        [Test]
        public void DisplayTextProperty()
        {
            _productLabelEditorViewModel.SelectedLabel.Text = "TestText";

            Assert.AreEqual("TestText", _productLabelEditorViewModel.DisplayText);
        }

        #endregion

        #region Commands

        //[Test]
        //public void ApplyToCurrentViewExecution()
        //{
        //    //_productLabelEditorViewModel.SelectedOpenableLabel = _productLabelEditorViewModel.AvailableOpenableSettings.First();

        //    //_productLabelEditorViewModel.OpenCommand.Execute();

        //    //Assert.IsTrue(_productLabelEditorViewModel.SetAndCloseCommand.CanExecute());

        //    //_productLabelEditorViewModel.SetAndCloseCommand.Execute();

        //    //can't test
        //    Assert.Ignore();
        //}

        //[Test]
        //public void ApplyToAllViewsExecution()
        //{
        //    //_productLabelEditorViewModel.SelectedOpenableLabel = _productLabelEditorViewModel.AvailableOpenableSettings.First();

        //    //_productLabelEditorViewModel.OpenCommand.Execute();

        //    //Assert.IsTrue(_productLabelEditorViewModel.SetAllAndCloseCommand.CanExecute());

        //    //_productLabelEditorViewModel.SetAndCloseCommand.Execute();

        //    //can't test
        //    Assert.Ignore();
        //}

        [Test]
        public void CancelExecution()
        {
            RelayCommand cmd = _productLabelEditorViewModel.CancelCommand;

            Assert.IsNotNull(cmd.FriendlyName);

            Assert.IsTrue(cmd.CanExecute());
            WindowService.CloseWindowSetNullResponse();
            cmd.Execute();
        }

        [Test]
        public void SetField_Execution()
        {
            RelayCommand cmd = _productLabelEditorViewModel.SetFieldCommand;

            Assert.IsNotNull(cmd.FriendlyName);
        }

        [Test]
        public void SaveAs_Execution()
        {
            //start with new label
            var viewModel = _productLabelEditorViewModel;

            //set property
            String checkText = "[PlanogramProduct.Name]";
            viewModel.SelectedLabel.Text = checkText;

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //fetch file
            Label fetchedLabel = Label.FetchByFilename(expectedPath);
            Assert.AreEqual(checkText, fetchedLabel.Text);
        }

        [Test]
        public void SaveAs_ReplaceExecution()
        {
            //saved label
            Label test = Label.NewLabel(LabelType.Product);

            String checkText = "[PlanogramProduct.Name]";
            test.Text = checkText;
            test.Name = Guid.NewGuid().ToString();

            String expectedPath = Path.Combine(this.TestDir, test.Name);
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            test = test.SaveAsFile(expectedPath);
            test.Dispose();
            test = null;

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //start with new label
            var viewModel = _productLabelEditorViewModel;

            //change property
            checkText = "<Product>.<Brand>";
            viewModel.SelectedLabel.Text = checkText;

            //save
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //fetch file
            Label fetchedLabel = Label.FetchByFilename(expectedPath);
            Assert.AreEqual(checkText, fetchedLabel.Text);
        }

        
        [Test]
        public void OpenFromFile_Execution()
        {
            //saved label
            Label test = Label.NewLabel(LabelType.Product);

            String checkText = "[PlanogramProduct.Name]";
            test.Text = checkText;
            test.Name = Guid.NewGuid().ToString();

            String expectedPath = Path.Combine(this.TestDir, test.Name);
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            test = test.SaveAsFile(expectedPath);
            test.Dispose();
            test = null;

            //open
            _productLabelEditorViewModel.OpenFromFileCommand.Execute(expectedPath);

            //check if file has opened correctly
            Label fetchedLabel = Label.FetchByFilename(expectedPath);
            Assert.AreEqual(_productLabelEditorViewModel.SelectedLabel.Text, fetchedLabel.Text);

        }

        #endregion

        #region OtherTests

        [Test]
        public void Other_OpenThenOverwrite()
        {
            //start with new label
            var viewModel = _productLabelEditorViewModel;

            //set property
            String checkText = "[PlanogramProduct.Name]";
            viewModel.SelectedLabel.Text = checkText;

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            viewModel.SelectedLabel.Text = "TTT";


            //open the label again
            viewModel.OpenFromFileCommand.Execute(expectedPath);
            Assert.AreEqual(checkText, viewModel.SelectedLabel.Text);
            
            //make another change
            checkText = "[PlanogramProduct.Brand]";
            viewModel.SelectedLabel.Text = checkText;
            viewModel.SaveAsToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            viewModel.SelectedLabel.Text = "TTT";
            viewModel.OpenFromFileCommand.Execute(expectedPath);
            Assert.AreEqual(checkText, viewModel.SelectedLabel.Text);

        }

        #endregion

    }
}
