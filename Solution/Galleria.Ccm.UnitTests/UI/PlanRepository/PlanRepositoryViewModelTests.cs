﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26822 : L.Ineson
//  Created
// V8-26944 : A.Silva ~ Changed references to AvailablePlanogramInfos in the ViewModel with references to DisplayedPlanogramValidationRows, and the SelectedPlanogramValidationRows property too.
// V8-28003 : A.Silva
//      Added tests for new cut/copy/paste commands, New, Edit and Delete Planogram Groups and Delete Planogram commands.
// V8-28374 : A.Silva
//      Some Test Maintenance.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Editor.Client.Wpf.PlanRepository;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.UnitTests.UI.PlanRepository
{
    [TestFixture]
    public class PlanRepositoryViewModelTests : TestBase
    {
        #region Constants
        private const Int32 InitialPlanogramCount = 5;
        #endregion

        #region Fields

        private List<Package> _packages;
        private Planogram _firstPlanogram;
        private String _entityName;

        #endregion

        #region Test Helpers

        public override void Setup()
        {
            base.Setup();

            SetMockRepositoryConnection();

            EntityInfo entityInfo = EntityInfoList.FetchAllEntityInfos().First();
            _entityName = entityInfo.Name;

            ////Create a planogram hierarchy
            //PlanogramHierarchy hierarchy = TestDataHelper.PopulatePlanogramHierarchy(entityInfo.Id);

            ////create some plans
            //_packages = TestDataHelper.InsertPlanograms(InitialPlanogramCount);
            //PlanogramGroup.AssociatePlanograms(hierarchy.RootGroup.Id, _packages.Select(p => Convert.ToInt32(p.Id)));
            //_firstPlanogram = _packages.First().Planograms.First();

            //clear the repository connection to start
            App.ViewState.ClearRepositoryConnection();
        }

        private PlanogramHierarchy PopulateHierarchy()
        {

            return TestDataHelper.PopulatePlanogramHierarchy(EntityId);
        }

        private List<Int32> InsertTestPlans(PlanogramGroup planGroup, Int32 planCount)
        {
            List<Int32> packageIds = TestDataHelper.InsertPlanograms(planCount);
            PlanogramGroup.AssociatePlanograms(planGroup.Id, packageIds);
            return packageIds;
        }

        public Int32 EntityId
        {
            get{return EntityInfoList.FetchAllEntityInfos().First().Id;}
        }

        #endregion

        #region Property tests

        #region CurrentEntityName

        [Test]
        public void CurrentEntityName()
        {
            const String expectedBindingPropertyPath = "CurrentEntityName";

            Assert.AreEqual(expectedBindingPropertyPath, PlanRepositoryViewModel.CurrentEntityNameProperty.Path, "The property's binding path was not the expected one.");
        }

        [Test]
        public void CurrentEntityName_UpdatedOnConnectionChange()
        {
            //null when no connection
            var testModel = new PlanRepositoryViewModel();
            Assert.IsFalse(App.ViewState.IsConnectedToRepository);
            Assert.IsNull(testModel.CurrentEntityName, "There should not be any enity name without a connection.");

            testModel.PropertyChanged += base.TestModel_PropertyChanged;

            SetMockRepositoryConnection();
            Assert.AreEqual(_entityName, testModel.CurrentEntityName, "There should be an entity name when a connections is set.");
            Assert.Contains("CurrentEntityName", base.PropertyChangedNotifications);

            testModel.PropertyChanged -= base.TestModel_PropertyChanged;
            testModel.Dispose();
        } 


        #endregion

        #region PlanogramHierarchyView

        [Test]
        public void PlanogramHierarchyView_Initialized()
        {
            SetMockRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.PlanogramHierarchyView, "Should not be null");
            Assert.IsNotNull(testModel.PlanogramHierarchyView.Model, "Model Should not be null");
        }

        [Test]
        public void PlanogramHierarchyView_WhenConnectionUnsetShouldBeNull()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNull(testModel.PlanogramHierarchyView.Model, "There should not be any planogram hierarchy without a connection.");
        }

        [Test]
        public void PlanogramHierarchyView_WhenConnectionSet_ShouldHaveHierarchy()
        {
            var testModel = new PlanRepositoryViewModel();
            SetMockRepositoryConnection();

            Assert.IsNotNull(testModel.PlanogramHierarchyView.Model, "There should be a planogram hierarchy when a connection is set.");
        }

        [Test]
        public void PlanogramHierarchyView_WhenConnectionSetShouldUpdateModel()
        {
            var testModel = new PlanRepositoryViewModel();
            Boolean wasSet = false;
            testModel.PlanogramHierarchyView.ModelChanged += (s, e) => { wasSet = true; };

            SetMockRepositoryConnection();

            Assert.IsTrue(wasSet);
        }

        #endregion

        #region SelectedPlanogramGroup

        [Test]
        public void SelectedPlanogramGroup_Intialized()
        {
            SetMockRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNotNull(testModel.SelectedPlanogramGroup, "Should not start null");
            Assert.IsTrue(testModel.PlanogramHierarchyView.EnumerateAllGroupViews().Contains(testModel.SelectedPlanogramGroup));

            testModel.Dispose();
        }



        [Test]
        public void SelectedPlanogramGroup_UpdatedOnConnectionChange()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNull(testModel.SelectedPlanogramGroup);

            //set connection
            SetMockRepositoryConnection();
            Assert.IsNotNull(testModel.SelectedPlanogramGroup);

            testModel.Dispose();
        }
        

        #endregion

        #region UserFavouriteGroupsView 

        [Test]
        public void UserFavouriteGroupsView_Initialized()
        {
            SetMockRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.UserFavouriteGroupsView, "Should not be null");
            Assert.IsNotNull(testModel.UserFavouriteGroupsView.Model, "Model Should not be null");
        }

        [Test]
        public void UserFavouriteGroupsView_WhenConnectionUnsetShouldBeNull()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNull(testModel.UserFavouriteGroupsView.Model);
        }

        [Test]
        public void UserFavouriteGroupsView_WhenConnectionSetShouldHaveFavoriteGroups()
        {
            var testModel = new PlanRepositoryViewModel();
            SetMockRepositoryConnection();
            Assert.IsNotNull(testModel.UserFavouriteGroupsView.Model);
        }

        [Test]
        public void UserFavouriteGroupsView_WhenConnectionSetShouldUpdateModel()
        {
            var testModel = new PlanRepositoryViewModel();
            Boolean wasSet = false;
            testModel.UserFavouriteGroupsView.ModelChanged += (s, e) => { wasSet = true; };

            SetMockRepositoryConnection();

            Assert.IsTrue(wasSet);
        }

        #endregion

        #region PlanogramSearchCriteria

        [Test]
        public void PlanogramSearchCriteria_Initialized()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNullOrEmpty(testModel.PlanogramSearchCriteria, "Should start empty");
        }

        [Test]
        public void PlanogramSearchCriteria_Setter()
        {
            var testModel = new PlanRepositoryViewModel();

            testModel.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            testModel.PlanogramSearchCriteria = "TEST";
            Assert.AreEqual("TEST", testModel.PlanogramSearchCriteria);

            Assert.Contains("PlanogramSearchCriteria", base.PropertyChangedNotifications);
        }

        [Test]
        public void PlanogramSearchCriteria_WhenConnectionUnsetShouldBeNull()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNull(testModel.PlanogramSearchCriteria);
        }

        #endregion

        #region PlanogramRows

        [Test]
        public void PlanogramRows_Initialized()
        {
            //without repository
            App.ViewState.ClearRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.PlanogramRows, "Should not be null");
            Assert.AreEqual(0, testModel.PlanogramRows.Count);

            testModel.Dispose();

            //with repository
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.PlanogramRows, "Should not be null");
            Assert.AreNotEqual(0, testModel.PlanogramRows.Count);
        }

        //[Test]
        //public void PlanogramRows_UpdatesOnSelectedPlanogramGroupChanged()
        //{
        //    Assert.Ignore("TODO");
        //}

        //[Test]
        //public void PlanogramRows_UpdatesOnPlanogramSearchCriteriaChanged()
        //{
        //    Assert.Ignore("TODO");
        //}


        [Test]
        public void PlanogramRows_WhenNotConnectedShouldNotHavePlanograms()
        {
            var testModel = new PlanRepositoryViewModel();
            Assert.AreEqual(0, testModel.PlanogramRows.Count, "There should be no planogram rows without an active connection.");
        }

        [Test]
        public void PlanogramRows_WhenSelectedPlanogramGroupChangedShouldUpdateList()
        {
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);
            var testModel = new PlanRepositoryViewModel();

            // Select an empty group.
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.RootGroup.ChildViews[0];
            Assert.AreEqual(0, testModel.PlanogramRows.Count, "When selecting a new group should update the list of planograms.");
        }

        #endregion

        #region SelectedPlanogramRows

        [Test]
        public void SelectedPlanogramRows_Initialized()
        {
            var testModel = new PlanRepositoryViewModel();

            Assert.IsNotNull(testModel.SelectedPlanogramRows, "Should not be null");
            //Assert.AreEqual(0, testModel.SelectedPlanogramRows.Count, "Should start with 1 plan selected");
        }
        #endregion

        #region ActivePlanogramInfo Property Tests

        [Test]
        public void ActivePlanogramInfo_Initialized()
        {
            //without repository
            App.ViewState.ClearRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();
            Assert.IsNull(testModel.ActivePlanogramInfo, "Should be null");

            testModel.Dispose();

            //with repository
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);
            testModel = new PlanRepositoryViewModel();
            Assert.IsNull(testModel.ActivePlanogramInfo, "Should still be null - should load with nothing selected.");
        }

        //[Test]
        //public void ActivePlanogramInfo_UpdatesOnSelectedRowsChanged()
        //{
        //    Assert.Ignore("TODO");
        //}

        [Test]
        public void ActivePlanogramInfo_WhenConnectionUnsetShouldBeNull()
        {
            var testModel = new PlanRepositoryViewModel();
            const String noSelectionWithoutConnectionFail = "Without a connection, there should be no active planogram info.";

            Assert.IsNull(testModel.ActivePlanogramInfo, noSelectionWithoutConnectionFail);
        }

        #endregion
               
        #endregion

        #region Command tests

        #region ConnectToRepository

        //[Test]
        //public void ConnectToRepositoryCommand_Execution()
        //{
        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.ConnectToRepositoryCommand;
        //    const String expectedCommandPath = "ConnectToRepositoryCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.ConnectToRepositoryCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //    Assert.Ignore("TODO: Test Command Execution");
        //}

        #endregion

        #region OpenPlanogram

        [Test]
        public void OpenPlanogramCommand()
        {
            var testModel = new PlanRepositoryViewModel();
            RelayCommand expectedCommand = testModel.OpenPlanogramCommand;
            const String expectedCommandPath = "OpenPlanogramCommand";
            const String viewModelCommandsContainsFailed =
                "The command was not found in the ViewModelCommands collection.";
            const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

            Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
            Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.OpenPlanogramCommandProperty.Path,
                bindingPropertyPathFailed);
        }

        [Test]
        public void OpenPlanogramCommand_EmptySelection_CannotExecute()
        {
            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();

            Assert.IsFalse(testModel.OpenPlanogramCommand.CanExecute(), "Open Planogram command should not be enabled without a selection.");
            Assert.IsNotNull(testModel.OpenPlanogramCommand.DisabledReason, "Open Planogram should have a disabled reason when there is no selection.");
        }

        [Test]
        public void OpenPlanogramCommand_RowsSelected_CanExecute()
        {
            var testModel = new PlanRepositoryViewModel();
            PlanogramInfo planogramInfo = PlanogramInfo.GetPlanogramInfo(null, new PlanogramInfoDto{Name = "Test"});
            testModel.SelectedPlanogramRows.Add(new PlanogramRepositoryRow(planogramInfo, null));
            
            Assert.IsTrue(testModel.OpenPlanogramCommand.CanExecute(), "Open Planogram command should be enabled when there are selected planograms.");
        }



        #endregion

        #region CutPlanogram 

        //[Test]
        //public void CutPlanogramCommand_Execution()
        //{
        //    Assert.Ignore("TODO: Test Command Execution");

        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.CutPlanogramCommand;
        //    const String expectedCommandPath = "CutPlanogramCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.CutPlanogramCommandProperty.Path,
        //        bindingPropertyPathFailed);

            
        //}

        [Test]
        public void CutPlanogramCommand_DisabledWhenNoDeletePermission()
        {
            //V8-32525 - Checks that the user cannot move planograms when the delete permission is 
            // off.

            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows.First());

            Assert.IsTrue(testModel.CutPlanogramCommand.CanExecute());

            testModel.Dispose();


            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramDelete.ToString());

            //check
            testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows.First());
            Assert.IsFalse(testModel.CutPlanogramCommand.CanExecute());
            testModel.Dispose();
        }

        #endregion

        #region CopyPlanogram

        //[Test]
        //public void CopyPlanogramCommand_DisabledWhenNoSelectedRows()
        //{
        //    Assert.Ignore("TODO");
        //}

        //[Test]
        //public void CopyPlanogramCommand_Execution()
        //{
        //    Assert.Ignore("TODO: Test Command Execution");

        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.CopyPlanogramCommand;
        //    const String expectedCommandPath = "CopyPlanogramCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.CopyPlanogramCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //}

        #endregion

        #region PastePlanogram

        //[Test]
        //public void PastePlanogramCommand_Executed()
        //{
        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.PastePlanogramCommand;
        //    const String expectedCommandPath = "PastePlanogramCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.PastePlanogramCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //    Assert.Ignore("TODO: Test Command Execution");
        //}

        #endregion

        #region DeletePlanogram

        [Test]
        public void DeletePlanogramCommand_DisabledWhenNoRepository()
        {
            App.ViewState.ClearRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();
            Assert.IsFalse(testModel.DeletePlanogramCommand.CanExecute());
        }

        [Test]
        public void DeletePlanogramCommand_DisabledWhenNoDeletePerm()
        {
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramDelete.ToString());

            var testModel = new PlanRepositoryViewModel();

            testModel.SelectedPlanogramRows.Clear();
            var rowToDelete = testModel.PlanogramRows.First();
            testModel.SelectedPlanogramRows.Add(rowToDelete);

            Assert.IsFalse(testModel.DeletePlanogramCommand.CanExecute());
        }

        [Test]
        public void DeletePlanogramCommand_DisabledWhenNoSelectedRows()
        {
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();

            testModel.SelectedPlanogramRows.Clear();
            var rowToDelete = testModel.PlanogramRows.First();
            testModel.SelectedPlanogramRows.Add(rowToDelete);

            Assert.IsTrue(testModel.DeletePlanogramCommand.CanExecute());

            testModel.SelectedPlanogramRows.Clear();

            Assert.IsFalse(testModel.DeletePlanogramCommand.CanExecute(), "disabled when no row to delete");
            Assert.IsNotNull(testModel.DeletePlanogramCommand.DisabledReason, "Disabled reason should be populated");


            testModel.Dispose();
        }

        [Test]
        public void DeletePlanogramCommand_IfNoToConfirmNoDelete()
        {
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[0]);
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[1]);

            List<PlanogramInfo> plansToDelete = testModel.SelectedPlanogramRows.Select(p => p.Info).ToList();
            Int32 startRowCount = testModel.PlanogramRows.Count;

            //make sure the busy window is shown and that update progress is called twice - once per plan.
            WindowService.ShowMessageSetReponse( ModalMessageResult.Button2); //confirm delete with user.

            testModel.DeletePlanogramCommand.Execute();

            //Check that neither plan is deleted
            PlanogramInfoList planInfos = PlanogramInfoList.FetchByIds(new List<Int32> { plansToDelete[0].Id, plansToDelete[1].Id });
            Assert.AreEqual(2, planInfos.Count);
            Assert.IsFalse(planInfos.Any(p => p.DateDeleted.HasValue));
        }

        [Test]
        public void DeletePlanogramCommand_Executed()
        {
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[0]);
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[1]);

            List<PlanogramInfo> plansToDelete = testModel.SelectedPlanogramRows.Select(p => p.Info).ToList();
            Int32 startRowCount = testModel.PlanogramRows.Count;

            //make sure the busy window is shown and that update progress is called twice - once per plan.
            WindowService.ShowMessageSetReponse( ModalMessageResult.Button1); //confirm delete with user.
            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //display busy
            WindowService.AddNullResponse(NUnitWindowService.UpdateBusyMethod); //update busy after plan 1 delete
            WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,//update busy after plan 2 delete
                (p) =>
                {
                    //make sure the viewmodel has already removed the prev row
                    Assert.AreEqual(startRowCount - 1, testModel.PlanogramRows.Count, "First deleted plan row should have already been removed");
                });
            WindowService.AddResponse(NUnitWindowService.CloseCurrentBusyMethod, //close busy
                (p) =>
                {
                    //make sure the viewmodel has now removed the second plan row
                    Assert.AreEqual(startRowCount - 2, testModel.PlanogramRows.Count, "Second deleted plan row should have already been removed");
                });


            testModel.DeletePlanogramCommand.Execute();

            WindowService.AssertResponsesUsed();


            //Chekc both plans are deleted.
            PlanogramInfoList planInfos = PlanogramInfoList.FetchByIds(new List<Int32> { plansToDelete[0].Id, plansToDelete[1].Id });
            Assert.AreEqual(0, planInfos.Count(p => p.DateDeleted == null));
        }

        [Test]
        public void DeletePlanogramCommand_CancelDelete()
        {
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramRows.Clear();
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[0]);
            testModel.SelectedPlanogramRows.Add(testModel.PlanogramRows[1]);

            List<PlanogramInfo> plansToDelete = testModel.SelectedPlanogramRows.Select(p => p.Info).ToList();
            Int32 startRowCount = testModel.PlanogramRows.Count;

            EventHandler cancelHandler = null;

            //confirm delete with user.
            WindowService.ShowMessageSetReponse( ModalMessageResult.Button1);

            //get the cancel handler to call 
            WindowService.AddResponse(NUnitWindowService.ShowDeterminateBusyMethod,
                (p) =>
                {
                    cancelHandler = (EventHandler)p.WindowParameters[2];
                });

            //call cancel after the first plan is deleted.
            WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
                (p) =>
                {
                    cancelHandler.Invoke(null, EventArgs.Empty);
                });

            //window should close
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);


            testModel.DeletePlanogramCommand.Execute();

            WindowService.AssertResponsesUsed();

            //check that only one plan was deleted.
            PlanogramInfoList planInfos = PlanogramInfoList.FetchByIds(new List<Int32> { plansToDelete[0].Id, plansToDelete[1].Id });
            Assert.AreEqual(1, planInfos.Count(p => p.DateDeleted == null));
        }

        #endregion

        #region NewGroup

        //[Test]
        //public void NewGroupCommand_Execution()
        //{
        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.NewGroupCommand;
        //    const String expectedCommandPath = "NewGroupCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.NewGroupCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //    Assert.Ignore("TODO: Test Command Execution");
        //}

        #endregion

        #region EditGroup Command

        //[Test]
        //public void EditGroupCommand_Execution()
        //{
        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.EditGroupCommand;
        //    const String expectedCommandPath = "EditGroupCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.EditGroupCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //    Assert.Ignore("TODO: Test Command Execution");
        //}

        #endregion

        #region CutPlanogram Command


        //[Test]
        //public void CutPlanogramGroupCommand_Execution()
        //{
        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.CutPlanogramGroupCommand;
        //    const String expectedCommandPath = "CutPlanogramGroupCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.CutPlanogramGroupCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //    Assert.Ignore("TODO: Test Command Execution");
        //}

        #endregion

        #region CopyPlanogramGroup Command


        //[Test]
        //public void CopyPlanogramGroupCommand_Execution()
        //{
        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.CopyPlanogramGroupCommand;
        //    const String expectedCommandPath = "CopyPlanogramGroupCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.CopyPlanogramGroupCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //    Assert.Ignore("TODO: Test Command Execution");
        //}

        #endregion

        #region PastePlanogramGroup


        //[Test]
        //public void PastePlanogramGroupCommand_Execution()
        //{
        //    var testModel = new PlanRepositoryViewModel();
        //    RelayCommand expectedCommand = testModel.PastePlanogramGroupCommand;
        //    const String expectedCommandPath = "PastePlanogramGroupCommand";
        //    const String viewModelCommandsContainsFailed =
        //        "The command was not found in the ViewModelCommands collection.";
        //    const String bindingPropertyPathFailed = "The command's Binding Property Path was not the expected one.";

        //    Assert.Contains(expectedCommand, testModel.ViewModelCommands, viewModelCommandsContainsFailed);
        //    Assert.AreEqual(expectedCommandPath, PlanRepositoryViewModel.PastePlanogramGroupCommandProperty.Path,
        //        bindingPropertyPathFailed);

        //    Assert.Ignore("TODO: Test Command Execution");
        //}

        #endregion

        #region DeleteGroup Command

        [Test]
        public void DeleteGroupCommand_DisabledWhemNoDeletePerm()
        {
            SetMockRepositoryConnection();
            InsertTestPlans(PopulateHierarchy().RootGroup, 5);

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.PlanogramHierarchyEdit.ToString());

            var testModel = new PlanRepositoryViewModel();
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.RootGroup.ChildViews[0];

            Assert.IsFalse(testModel.DeleteGroupCommand.CanExecute());
        }

        [Test]
        public void DeleteGroupCommand_GroupsWithPlans()
        {
            SetMockRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();

            List<Int32> plansToDelete = new List<int>();

            //add group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.RootGroup;
            WindowService.AddResponseResult(NUnitWindowService.PromptUserForValueMethod, new Object[] { true, "Group A" });
            testModel.NewGroupCommand.Execute();

            //add some plans to group a
            plansToDelete.AddRange(InsertTestPlans(testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0], 3));

            //add group b as a child of a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.RootGroup.ChildViews[0];
            WindowService.AddResponseResult(NUnitWindowService.PromptUserForValueMethod, new Object[] { true, "Group B" });
            testModel.NewGroupCommand.Execute();

            //add some plans to group b
            plansToDelete.AddRange(InsertTestPlans(testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0].ChildList[0], 5));

            //select group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.RootGroup.ChildViews[0];

            //delete
            WindowService.AddNullResponse(NUnitWindowService.ShowIndeterminateBusyMethod); //busy while building list.
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed
            WindowService.ShowMessageSetReponse( ModalMessageResult.Button1); //confirm delete

            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //busy while deleting

            Int32 curProgress = 0;
            for (Int32 i = 0; i < (plansToDelete.Count + 2); i++)
            {
                WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
                    (p) =>
                    {
                        //check progress has increased.
                        Int32 newProgress = (p.WindowParameters.Count() == 1) ? (Int32)p.WindowParameters[0] : (Int32)p.WindowParameters[1];
                        Assert.Greater(newProgress, curProgress);
                        Assert.LessOrEqual(newProgress, 100);

                        curProgress = newProgress;

                    }); //one update per item deleted.
            }

            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed

            testModel.DeleteGroupCommand.Execute();

            WindowService.AssertResponsesUsed();

            //check all plans and groups were deleted
            PlanogramInfoList deletedPlans = PlanogramInfoList.FetchByIds(plansToDelete);
            Assert.AreEqual(0, deletedPlans.Count(p => p.DateDeleted == null));


            //check groups were deleted
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count, "Groups should have gone.");
        }

        [Test]
        public void DeleteGroupCommand_GroupWithoutPlans()
        {
            SetMockRepositoryConnection();
            var testModel = new PlanRepositoryViewModel();

            //add group a
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.RootGroup;
            WindowService.AddResponseResult(NUnitWindowService.PromptUserForValueMethod, new Object[] { true, "Group A" });
            testModel.NewGroupCommand.Execute();

            //delete it
            testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.RootGroup.ChildViews[0];

            WindowService.AddNullResponse(NUnitWindowService.ShowIndeterminateBusyMethod); //busy while building list.
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed
            WindowService.ShowMessageSetReponse( ModalMessageResult.Button1); //confirm delete

            WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //busy while deleting

            Int32 curProgress = 0;
            WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
                (p) =>
                {
                    //check progress has increased.
                    Int32 newProgress = (p.WindowParameters.Count() == 1) ? (Int32)p.WindowParameters[0] : (Int32)p.WindowParameters[1];
                    Assert.Greater(newProgress, curProgress);
                    Assert.LessOrEqual(newProgress, 100);

                    curProgress = newProgress;

                }); //one update per item deleted.


            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed

            testModel.DeleteGroupCommand.Execute();

            WindowService.AssertResponsesUsed();

            //check groups were deleted
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
            Assert.AreEqual(0, hierarchy.RootGroup.ChildList.Count, "Groups should have gone.");
        }

        //[Test]
        //public void DeleteGroupCommand_GroupWithLockedPlans()
        //{
        //    SetMockRepositoryConnection();
        //    var testModel = new PlanRepositoryViewModel();

        //    List<Int32> plansToDelete = new List<int>();

        //    //add group a
        //    testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup;
        //    WindowService.AddResponseResult(NUnitWindowService.PromptUserForValueMethod, new Object[] { true, "Group A" });
        //    testModel.NewGroupCommand.Execute();

        //    //add group b as a child of a
        //    testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];
        //    WindowService.AddResponseResult(NUnitWindowService.PromptUserForValueMethod, new Object[] { true, "Group B" });
        //    testModel.NewGroupCommand.Execute();

        //    //add some plans to group b
        //    plansToDelete.AddRange(InsertTestPlans(testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0].ChildList[0], 2));

        //    //lock plan 1
        //    User newUser = User.NewUser();
        //    newUser.UserName = "USR1";
        //    newUser.FirstName = "u1";
        //    newUser.LastName = "u1";
        //    newUser = newUser.Save();
        //    Package lockedPlan = Package.FetchById(plansToDelete.First(), newUser.Id, PackageLockType.User);

        //    //select group a
        //    testModel.SelectedPlanogramGroup = testModel.PlanogramHierarchyView.Model.RootGroup.ChildList[0];

        //    //delete
        //    WindowService.AddNullResponse(NUnitWindowService.ShowIndeterminateBusyMethod); //busy while building list.
        //    WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed
        //    WindowService.ShowMessageSetReponse( ModalMessageResult.Button1); //confirm delete

        //    WindowService.AddNullResponse(NUnitWindowService.ShowDeterminateBusyMethod); //busy while deleting

        //    Int32 curProgress = 0;
        //    for (Int32 i = 0; i < (plansToDelete.Count + 2); i++)
        //    {
        //        WindowService.AddResponse(NUnitWindowService.UpdateBusyMethod,
        //            (p) =>
        //            {
        //                //check progress has increased.
        //                Int32 newProgress = (p.WindowParameters.Count() == 1) ? (Int32)p.WindowParameters[0] : (Int32)p.WindowParameters[1];
        //                Assert.Greater(newProgress, curProgress);
        //                Assert.LessOrEqual(newProgress, 100);

        //                curProgress = newProgress;

        //            }); //one update per item deleted.
        //    }

        //    WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod); //busy closed

        //    testModel.DeleteGroupCommand.Execute();

        //    WindowService.AssertResponsesUsed();

        //    //check plan 2 was deleted but plan 1 was not.
        //    PlanogramInfoList deletedPlans = PlanogramInfoList.FetchByIds(plansToDelete);

        //    PlanogramInfo p1 = deletedPlans.FindById(plansToDelete[0]);
        //    Assert.IsNotNull(p1);
        //    Assert.IsFalse(p1.DateDeleted.HasValue);

        //    PlanogramInfo p2 = deletedPlans.FindById(plansToDelete[1]);
        //    if (p2 != null) Assert.IsTrue(p2.DateDeleted.HasValue);


        //    //check the groups were not deleted
        //    PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(this.EntityId);
        //    Assert.AreEqual(1, hierarchy.RootGroup.ChildList.Count);
        //    Assert.AreEqual(1, hierarchy.RootGroup.ChildList[0].ChildList.Count);
        //}

        #endregion

        #endregion
    }
}