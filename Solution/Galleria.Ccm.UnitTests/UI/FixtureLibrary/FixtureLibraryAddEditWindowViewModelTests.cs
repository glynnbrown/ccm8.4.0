﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8)
// V8-25873 : A.Probyn
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;

namespace Galleria.Ccm.UnitTests.UI.FixtureLibrary
{
    [TestFixture]
    public class FixtureLibraryAddEditWindowViewModelTests : TestBase
    {
        #region Commands

        [Test]
        public void SelectFolderCommand()
        {
            FixturePackage package = FixturePackage.NewFixturePackage();

            FixtureLibraryAddEditWindowViewModel testViewModel = new FixtureLibraryAddEditWindowViewModel(package, true, String.Empty);

            //Check initial folder path
            Assert.IsTrue(String.IsNullOrEmpty(testViewModel.FolderPath));

            Assert.IsTrue(testViewModel.SelectFolderCommand.CanExecute());

            //Execute command
            testViewModel.SelectFolderCommand.Execute();

            Assert.AreNotEqual(String.Empty, testViewModel.FolderPath);
            Assert.AreEqual(App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.FixtureLibrary), testViewModel.FolderPath);
        }

        /// <summary>
        /// Checks that we can save the fixture package correctly
        /// </summary>
        [Test]
        public void SaveCommand()
        {
            FixturePackage package = FixturePackage.NewFixturePackage();

            FixtureLibraryAddEditWindowViewModel testViewModel = new FixtureLibraryAddEditWindowViewModel(package, true, String.Empty);

            //Check initial properties folder path
            Assert.IsTrue(String.IsNullOrEmpty(testViewModel.FolderPath));
            Assert.IsNullOrEmpty(testViewModel.FixtureName);

            //Check validation
            Assert.IsFalse(testViewModel.SaveCommand.CanExecute());

            //Update properties folder path
            testViewModel.FolderPath = App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.FixtureLibrary);
            testViewModel.FixtureName = String.Empty;

            //Check validation
            Assert.IsFalse(testViewModel.SaveCommand.CanExecute());

            //Update properties folder path
            testViewModel.FolderPath = App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.FixtureLibrary);
            testViewModel.FixtureName = "Test";

            //Check validation
            Assert.IsTrue(testViewModel.SaveCommand.CanExecute());

            //Execute command
            testViewModel.SaveCommand.Execute();

            //Window closing is handled by xaml and owner
        }


        #endregion
    }


}
