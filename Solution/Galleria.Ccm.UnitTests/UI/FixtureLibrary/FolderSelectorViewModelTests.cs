﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25873 : A.Probyn
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;

namespace Galleria.Ccm.UnitTests.UI.FixtureLibrary
{
    [TestFixture]
    public class FolderSelectorViewModelTests : TestBase
    {
        #region Commands

        [Test]
        public void NewFolderCommand()
        {
            FolderSelectorViewModel testViewModel = new FolderSelectorViewModel();
            testViewModel.NewFolderCommand.Execute();

            Assert.AreEqual(1, testViewModel.RootFolder.ChildFolders.Count);
        }

        [Test]
        public void RenameFolderCommand()
        {
            FolderSelectorViewModel testViewModel = new FolderSelectorViewModel();
            testViewModel.NewFolderCommand.Execute();

            String curFolderName = testViewModel.RootFolder.ChildFolders[0].Name;

            testViewModel.RenameFolderCommand.Execute(testViewModel.RootFolder.ChildFolders[0]);

            Assert.AreNotEqual(curFolderName, testViewModel.RootFolder.ChildFolders[0]);
        }

        [Test]
        public void DeleteFolderCommand()
        {
            FolderSelectorViewModel testViewModel = new FolderSelectorViewModel();
            testViewModel.NewFolderCommand.Execute();

            Assert.AreEqual(1, testViewModel.RootFolder.ChildFolders.Count);

            testViewModel.DeleteFolderCommand.Execute(testViewModel.RootFolder.ChildFolders[0]);
            Assert.AreEqual(0, testViewModel.RootFolder.ChildFolders.Count);
        }

        [Test]
        public void SelectFolderCommand()
        {
            FolderSelectorViewModel testViewModel = new FolderSelectorViewModel();
            testViewModel.NewFolderCommand.Execute();

            Assert.AreEqual(1, testViewModel.RootFolder.ChildFolders.Count);

            //Ensure nothing is selected
            testViewModel.SelectedFolder = null;
            Assert.IsFalse(testViewModel.SelectFolderCommand.CanExecute());

            //Select a folder
            testViewModel.SelectedFolder = testViewModel.RootFolder.ChildFolders[0];
            Assert.IsTrue(testViewModel.SelectFolderCommand.CanExecute());

            //Window close is handled by xaml and owner
        }

        #endregion
    }

    
}