﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25873 : A.Probyn
//      Corrected after reimplemntation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;

namespace Galleria.Ccm.UnitTests.UI.FixtureLibrary
{
    [TestFixture]
    public class FixtureLibraryPanelViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private List<Object> InsertFixtures()
        {
            String directory = GetFixtureDirectoryPath();

            List<Object> insertedIds = new List<Object>();

            //add some component fixtures
            for (Int32 i = 0; i < 5; i++)
            {
                String path = Path.Combine(directory, "fix" + i);
                path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

                FixturePackage package = FixturePackage.NewFixturePackage(path);
                package.Name = "fix" + i;
                package.Description = "fix description" + i;

                //add a component
                FixtureComponent component = FixtureComponent.NewFixtureComponent();
                component.Name = "component" + i;
                component.Height = 4;
                component.Width = 100;
                component.Depth = 75;

                FixtureSubComponent subComponent = FixtureSubComponent.NewFixtureSubComponent();
                subComponent.Name = "s1";
                subComponent.Height = 4;
                subComponent.Width = 100;
                subComponent.Depth = 75;
                subComponent.MerchandisingType = FixtureSubComponentMerchandisingType.Stack;

                component.SubComponents.Add(subComponent);
                package.Components.Add(component);

                package.Save();
                insertedIds.Add(package.Id);

                package.Dispose();
                package = null;
            }

            return insertedIds;
        }

        private Object InsertComponentFixtureLibrary(String name)
        {
            Object id = null;

            String directory = GetFixtureDirectoryPath();

            String path = Path.Combine(directory, "fix" + name);
            path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

            FixturePackage package = FixturePackage.NewFixturePackage(path);
            package.Name = "fix" + name;
            package.Description = "fix description" + name;

            //add a component
            FixtureComponent component = FixtureComponent.NewFixtureComponent();
            component.Name = "component" + name;
            component.Height = 4;
            component.Width = 100;
            component.Depth = 75;

            FixtureSubComponent subComponent = FixtureSubComponent.NewFixtureSubComponent();
            subComponent.Name = "s1";
            subComponent.Height = 4;
            subComponent.Width = 100;
            subComponent.Depth = 75;
            subComponent.MerchandisingType = FixtureSubComponentMerchandisingType.Stack;

            component.SubComponents.Add(subComponent);
            package.Components.Add(component);

            package.Save();
            id = package.Id;

            package.Dispose();
            package = null;

            return id;
        }

        private Object InsertAssemblyFixtureLibrary(String name)
        {
            Object id = null;

            String directory = GetFixtureDirectoryPath();

            String path = Path.Combine(directory, "fix" + name);
            path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

            FixturePackage package = FixturePackage.NewFixturePackage(path);
            package.Name = "fix" + name;
            package.Description = "fix description" + name;

            //add an assembly
            FixtureAssembly assembly = FixtureAssembly.NewFixtureAssembly();
            assembly.Name = "assembly 1" + name;
            assembly.Height = 4;
            assembly.Width = 100;
            assembly.Depth = 75;
            package.Assemblies.Add(assembly);

            //add a component
            FixtureComponent component = FixtureComponent.NewFixtureComponent();
            component.Name = "component" + name;
            component.Height = 4;
            component.Width = 100;
            component.Depth = 75;

            FixtureSubComponent subComponent = FixtureSubComponent.NewFixtureSubComponent();
            subComponent.Name = "s1";
            subComponent.Height = 4;
            subComponent.Width = 100;
            subComponent.Depth = 75;
            subComponent.MerchandisingType = FixtureSubComponentMerchandisingType.Stack;

            component.SubComponents.Add(subComponent);
            package.Components.Add(component);


            FixtureAssemblyComponent assemblyComponent = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
            assemblyComponent.FixtureComponentId = component.Id;
            assembly.Components.Add(assemblyComponent);


            package.Save();
            id = package.Id;

            package.Dispose();
            package = null;

            return id;
        }

        private Object InsertFixtureFixtureLibrary(String name)
        {
            Object id = null;

            String directory = GetFixtureDirectoryPath();

            String path = Path.Combine(directory, "fix" + name);
            path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

            FixturePackage package = FixturePackage.NewFixturePackage(path);
            package.Name = "fix" + name;
            package.Description = "fix description" + name;


            Fixture f1 = Fixture.NewFixture();
            f1.Name = "f1";
            f1.Height = 191;
            f1.Width = 120;
            f1.Depth = 100;
            package.Fixtures.Add(f1);

            FixtureComponent f1Backboard = FixtureComponent.NewFixtureComponent();
            f1Backboard.Name = "f1Backboard";
            f1Backboard.Height = 191;
            f1Backboard.Width = 120;
            f1Backboard.Depth = 1;
            package.Components.Add(f1Backboard);

            FixtureSubComponent f1BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f1BackboardSub.Name = "f1BackboardSub";
            f1BackboardSub.Height = 191;
            f1BackboardSub.Width = 120;
            f1BackboardSub.Depth = 1;
            f1Backboard.SubComponents.Add(f1BackboardSub);

            FixtureComponentItem f1ComponentItem = FixtureComponentItem.NewFixtureComponentItem();
            f1ComponentItem.FixtureComponentId = f1Backboard.Id;
            f1ComponentItem.Y = 20;
            f1.Components.Add(f1ComponentItem);


            FixtureAssembly f2Assembly = FixtureAssembly.NewFixtureAssembly();
            f2Assembly.Name = "f2Assembly";
            package.Assemblies.Add(f2Assembly);

            FixtureAssemblyItem f2AssemblyItem = FixtureAssemblyItem.NewFixtureAssemblyItem();
            f2AssemblyItem.FixtureAssemblyId = f2Assembly.Id;
            f2AssemblyItem.Y = 1;
            f1.Assemblies.Add(f2AssemblyItem);

            FixtureComponent f2Backboard = FixtureComponent.NewFixtureComponent();
            f2Backboard.Name = "f2Backboard";
            f2Backboard.Height = 191;
            f2Backboard.Width = 120;
            f2Backboard.Depth = 1;
            package.Components.Add(f2Backboard);

            FixtureAssemblyComponent f2AssemblyComponent = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
            f2AssemblyComponent.FixtureComponentId = f2Backboard.Id;
            f2Assembly.Components.Add(f2AssemblyComponent);

            FixtureSubComponent f2BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f2BackboardSub.Name = "f2BackboardSub";
            f2BackboardSub.Height = 191;
            f2BackboardSub.Width = 120;
            f2BackboardSub.Depth = 1;
            f2Backboard.SubComponents.Add(f2BackboardSub);

            package.Save();
            id = package.Id;

            package.Dispose();
            package = null;

            return id;
        }

        public override void TearDown()
        {
            base.TearDown();

            //Delete any created fixture files
            foreach (String file in Directory.GetFiles(this.TestDir, "*.pogfx", SearchOption.AllDirectories))
            {
                System.IO.File.Delete(file);
            }
        }

        #endregion

        #region Commands

        #region AddToPlanogram Command

        [Test]
        public void AddToPlanogram_Fixture()
        {
            Object fixtureLibraryId = InsertFixtureFixtureLibrary("test");

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            planView.AddFixture();
            App.ViewState.ActivePlanogramView.Model = planView;

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            testViewModel.SelectedFixturePackageInfo = testViewModel.AvailableFixturePackageInfos.First();
            testViewModel.AddToPlanogramCommand.Execute();

            Assert.AreEqual(2, planView.Fixtures.Count);
            Assert.IsTrue(planView.Fixtures[1].Components[0].IsSelectable);

            Assert.AreNotEqual(0, planView.Fixtures[1].BaySequenceNumber);
        }

        [Test]
        public void AddToPlanogram_Assembly()
        {
            Object fixtureLibraryId = InsertAssemblyFixtureLibrary("test");

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            planView.AddFixture();
            App.ViewState.ActivePlanogramView.Model = planView;

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            testViewModel.SelectedFixturePackageInfo = testViewModel.AvailableFixturePackageInfos.First();
            testViewModel.AddToPlanogramCommand.Execute();

            Assert.AreEqual(1, planView.Fixtures.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies.Count);

            Assert.IsTrue(planView.Fixtures[0].Assemblies[0].IsSelectable);
        }

        /// <summary>
        /// V8-25280
        /// </summary>
        [Test]
        public void AddToPlanogram_AssemblyWithNoFixtures()
        {
            Object fixtureLibraryId = InsertAssemblyFixtureLibrary("test");

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            Assert.AreEqual(0, planView.Fixtures.Count);

            App.ViewState.ActivePlanogramView.Model = planView;

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            testViewModel.SelectedFixturePackageInfo = testViewModel.AvailableFixturePackageInfos.First();
            testViewModel.AddToPlanogramCommand.Execute();

            Assert.AreEqual(1, planView.Fixtures.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies.Count);

        }

        [Test]
        public void AddToPlanogram_Component()
        {
            Object fixtureLibraryId = InsertComponentFixtureLibrary("test");

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            planView.AddFixture();
            App.ViewState.ActivePlanogramView.Model = planView;

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            testViewModel.SelectedFixturePackageInfo = testViewModel.AvailableFixturePackageInfos.First();
            testViewModel.AddToPlanogramCommand.Execute();

            Assert.AreEqual(1, planView.Fixtures.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Components.Count);

            Assert.IsTrue(planView.Fixtures[0].Components[0].IsSelectable);
        }

        /// <summary>
        /// V8-25280
        /// </summary>
        [Test]
        public void AddToPlanogram_ComponentWithNoFixtures()
        {
            Object fixtureLibraryId = InsertComponentFixtureLibrary("test");

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            Assert.AreEqual(0, planView.Fixtures.Count);

            App.ViewState.ActivePlanogramView.Model = planView;

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            testViewModel.SelectedFixturePackageInfo = testViewModel.AvailableFixturePackageInfos.First();
            testViewModel.AddToPlanogramCommand.Execute();

            Assert.AreEqual(1, planView.Fixtures.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Components.Count);

        }

        /// <summary>
        /// V8-26182
        /// </summary>
        [Test]
        public void AddToPlanogram_FixtureWithAssemblies()
        {

            MainPageViewModel mainView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var controller = mainView.CreateNewPlanogram();

            //add a fixture
            var fixtureView = controller.SourcePlanogram.AddFixture();

            //add a couple of shelves
            var componentView = fixtureView.AddComponent(PlanogramComponentType.Shelf);
            var componentView2 = fixtureView.AddComponent(PlanogramComponentType.Shelf);

            //select both shelves
            controller.SelectedPlanItems.Clear();
            controller.SelectedPlanItems.Add(componentView);
            controller.SelectedPlanItems.Add(componentView2);

            //group them as an assembly
            MainPageCommands.CreateAssembly.Execute();
            Assert.AreEqual(1, fixtureView.Assemblies.Count, "Assembly was not created");


            //add to the fixture library
            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            var f = testViewModel.AvailableFixturePackageInfos; //force load
            Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.MainPageCommands.AddToFixtureLibrary.Execute(new List<IPlanItem> { fixtureView });

            //check the fixture library that was saved
            using (var fixtureLibrary = FixturePackage.FetchByFileName((String)testViewModel.AvailableFixturePackageInfos.First().Id))
            {
                Assert.AreEqual(1, fixtureLibrary.Fixtures.Count, "Fixture library not saved correctly");
                Assert.AreEqual(1, fixtureLibrary.Fixtures[0].Assemblies.Count, "Fixture library not saved correctly");
                Assert.AreEqual(1, fixtureLibrary.Assemblies.Count, "Fixture library not saved correctly");
                Assert.AreEqual(2, fixtureLibrary.Assemblies[0].Components.Count, "Fixture library not saved correctly");
            }

            //create a new plan
            mainView.ClosePlanController(controller);


            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var controller2 = mainView.CreateNewPlanogram();
            Assert.AreEqual(1, controller2.SourcePlanogram.Fixtures.Count);

            //add the assembly to the plan
            testViewModel.SelectedFixturePackageInfo = testViewModel.AvailableFixturePackageInfos.First();
            testViewModel.AddToPlanogramCommand.Execute();

            //check the plan
            Assert.AreEqual(2, controller2.SourcePlanogram.Fixtures.Count, "Add to Plan failed - incorrect no of fixtures");
            Assert.AreEqual(1, controller2.SourcePlanogram.Fixtures[1].Assemblies.Count, "Add to Plan failed - incorrect no of assemblies");
            Assert.AreEqual(2, controller2.SourcePlanogram.Fixtures[1].Assemblies[0].Components.Count, "Add to Plan failed - incorrect no of components");
        }

        #endregion

        [Test]
        public void EditFixturePackageCommand()
        {
            InsertComponentFixtureLibrary("test");

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel();

            var infoToRename = testViewModel.AvailableFixturePackageInfos.First();

            String curName = infoToRename.Name;

            testViewModel.EditFixturePackageCommand.Execute(infoToRename);

            Assert.AreNotEqual(curName, testViewModel.AvailableFixturePackageInfos.First().Name);
        }

        /// <summary>
        /// Checks that we can remove an item from the fixture library.
        /// </summary>
        [Test]
        public void DeleteFixturePackageCommand()
        {
            InsertFixtures();

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel();

            var infoToDelete = testViewModel.AvailableFixturePackageInfos.First();

            testViewModel.DeleteFixturePackageCommand.Execute(infoToDelete);

            Assert.IsFalse(testViewModel.AvailableFixturePackageInfos.Select(f => f.Model.FileName).Contains(infoToDelete.Model.FileName));

            Assert.IsFalse(System.IO.File.Exists((String)infoToDelete.Id));

        }

        #endregion

        #region AddToLibrary

        [Test]
        public void AddToLibrary_Fixture()
        {
            MainPageViewModel mainView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var controller = mainView.CreateNewPlanogram();

            var fixtureView = controller.SourcePlanogram.AddFixture();

            //add a couple of shelves
            var componentView = fixtureView.AddComponent(PlanogramComponentType.Shelf);
            var componentView2 = fixtureView.AddComponent(PlanogramComponentType.Shelf);

            //select both shelves
            controller.SelectedPlanItems.Clear();
            controller.SelectedPlanItems.Add(componentView);
            controller.SelectedPlanItems.Add(componentView2);

            //group them as an assembly
            MainPageCommands.CreateAssembly.Execute();
            Assert.AreEqual(1, fixtureView.Assemblies.Count, "Assembly was not created");

            //add another shelf on its own
            var componentView3 = fixtureView.AddComponent(PlanogramComponentType.Shelf);


            controller.SelectedPlanItems.Clear();
            controller.SelectedPlanItems.Add(fixtureView);

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            var f = testViewModel.AvailableFixturePackageInfos; //force load


            Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.MainPageCommands.AddToFixtureLibrary.Execute(new List<IPlanItem> { fixtureView });

            Assert.AreEqual(1, testViewModel.AvailableFixturePackageInfos.Count);

            //check the fixture library that was saved
            using (var fixtureLibrary = FixturePackage.FetchByFileName((String)testViewModel.AvailableFixturePackageInfos.First().Id))
            {
                Assert.AreEqual(1, fixtureLibrary.Fixtures.Count, "Fixture count wrong");
                Assert.AreEqual(1, fixtureLibrary.Fixtures[0].Assemblies.Count, "FixtureAssemblyItem count wrong");
                Assert.AreEqual(1, fixtureLibrary.Fixtures[0].Components.Count, "FixtureComponentItem count wrong");
                Assert.AreEqual(1, fixtureLibrary.Assemblies.Count, "FixtureAssembly count wrong");
                Assert.AreEqual(2, fixtureLibrary.Assemblies[0].Components.Count, "FixtureComponent count wrong.");
            }
        }

        [Test]
        public void AddToLibrary_Assembly()
        {
            MainPageViewModel mainView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var controller = mainView.CreateNewPlanogram();

            var fixtureView = controller.SourcePlanogram.AddFixture();

            var assemblyView = fixtureView.AddAssembly();
            var c1 = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            var c2 = assemblyView.AddComponent(PlanogramComponentType.Shelf);

            controller.SelectedPlanItems.Clear();
            controller.SelectedPlanItems.Add(assemblyView);

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            var f = testViewModel.AvailableFixturePackageInfos; //force load
            Int32 preCount = testViewModel.AvailableFixturePackageInfos.Count;

            Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.MainPageCommands.AddToFixtureLibrary.Execute(new List<IPlanItem> { assemblyView });

            Assert.AreEqual(preCount+ 1, testViewModel.AvailableFixturePackageInfos.Count);
        }

        [Test]
        public void AddToLibrary_Component()
        {
            MainPageViewModel mainView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var controller = mainView.CreateNewPlanogram();

            var fixtureView = controller.SourcePlanogram.AddFixture();
            var componentView = fixtureView.AddComponent(PlanogramComponentType.Shelf);

            controller.SelectedPlanItems.Clear();
            controller.SelectedPlanItems.Add(componentView);

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel(App.ViewState.Settings, App.ViewState.ActivePlanogramView);
            var f = testViewModel.AvailableFixturePackageInfos; //force load
            Int32 preCount = testViewModel.AvailableFixturePackageInfos.Count;

            Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.MainPageCommands.AddToFixtureLibrary.Execute(
                new List<IPlanItem> { componentView });

            Assert.AreEqual(preCount + 1, testViewModel.AvailableFixturePackageInfos.Count);

        }


        [Test]
        public void AddToLibrary_ComponentAndTextBox()
        {
            //V8-32600 - Checks that if a user selects a component and a textbox which are not linked
            // the add to fixture library command is disabled

            var newPlanController = base.AddNewEmptyPlan();
            PlanogramView planView = newPlanController.SourcePlanogram;

            //add a shelf and textbox - not linked
            var shelf = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);

            var anno = planView.Fixtures[0].AddAnnotation();
            anno.Text = "test";
            anno.Height = 20;
            anno.Width = 100;
            anno.Depth = 1;
            anno.X = -anno.Width;

            //now select both
            newPlanController.SelectedPlanItems.Clear();
            newPlanController.SelectedPlanItems.Add(anno);
            newPlanController.SelectedPlanItems.Add(shelf);

            //check that the command is disabled.
            Assert.IsTrue(MainPageCommands.AddToFixtureLibrary.CanExecute(), "Command should be enabled");

            String filePath = FixtureLibraryHelper.CreateFilePath(
                App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.FixtureLibrary),
                Guid.NewGuid().ToString());

            FixtureLibraryHelper.AddToFixtureLibrary(newPlanController.SelectedPlanItems.ToList(), filePath);

            //load back and check
            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.FetchFromFile(filePath);

                Assert.AreEqual(1, packageView.Model.Components.Count, "Should have 1 component");
                Assert.AreEqual(0, packageView.Model.Annotations.Count, "Should not have an annotation.");
            }

            //clean up
            FixtureLibraryHelper.DeleteFixturePackage(filePath);





            //now link them
            anno = shelf.MoveAnnotation(anno);

            //select both
            newPlanController.SelectedPlanItems.Clear();
            newPlanController.SelectedPlanItems.Add(anno);
            newPlanController.SelectedPlanItems.Add(shelf);

            Assert.IsTrue(MainPageCommands.AddToFixtureLibrary.CanExecute(), "Command should be enabled");

            //create the package - use helper method so that we can pass filename.
            filePath = FixtureLibraryHelper.CreateFilePath(
                App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.FixtureLibrary),
                Guid.NewGuid().ToString());


            FixtureLibraryHelper.AddToFixtureLibrary(newPlanController.SelectedPlanItems.ToList(), filePath);
                
            //load back and check
            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.FetchFromFile(filePath);

                Assert.AreEqual(1, packageView.Model.Components.Count, "Should have 1 component");
                Assert.AreEqual(1, packageView.Model.Annotations.Count, "Should have 1 annotation.");
            }

            //clean up
            FixtureLibraryHelper.DeleteFixturePackage(filePath);
        }


        [Test]
        public void AddToLibrary_AllFixtureChildrenSelected()
        {
            //V8-25514

            var newPlanController = base.AddNewEmptyPlan();
            PlanogramView planView = newPlanController.SourcePlanogram;

            //add a shelf
            var shelf = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);

            //select the fixture and all of its child components
            newPlanController.SelectedPlanItems.Clear();
            newPlanController.SelectedPlanItems.AddRange(planView.Fixtures[0].Components);
            newPlanController.SelectedPlanItems.Add(planView.Fixtures[0]);

            Assert.IsTrue(MainPageCommands.AddToFixtureLibrary.CanExecute(), "Command should be enabled");

            //create the package - use helper method so that we can pass filename.
            String filePath = FixtureLibraryHelper.CreateFilePath(
                App.ViewState.GetSessionDirectory(Ccm.Common.Wpf.Helpers.SessionDirectory.FixtureLibrary),
                Guid.NewGuid().ToString());


            FixtureLibraryHelper.AddToFixtureLibrary(newPlanController.SelectedPlanItems.ToList(), filePath);

            //load back and check
            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.FetchFromFile(filePath);

                Assert.AreEqual(1, packageView.Model.Fixtures.Count, "Should have 1 fixture");
                Assert.AreEqual(planView.Fixtures[0].Components.Count, packageView.Model.Components.Count, "Should have all components");
            }

            //clean up
            FixtureLibraryHelper.DeleteFixturePackage(filePath);
        }


        #endregion

        #region General

        /// <summary>
        /// Checks that the viewmodel correctly shows the list of available fixture packages.
        /// </summary>
        [Test]
        public void ShowAvailablePackages()
        {
            List<Object> fixturePackageIds = InsertFixtures();

            FixtureLibraryPanelViewModel testViewModel = new FixtureLibraryPanelViewModel();

            Assert.AreEqual(fixturePackageIds.Count, testViewModel.AvailableFixturePackageInfos.Count);
        }

        [Test]
        public void SaveFixtureWithAssemblies()
        {
            //GEM26182

            Object id = 0;

            Galleria.Ccm.Model.FixturePackage package = Galleria.Ccm.Model.FixturePackage.NewFixturePackage();
            package.Name = Guid.NewGuid().ToString();

            Fixture fixture1 = Fixture.NewFixture();
            fixture1.Name = "fx1";
            package.Fixtures.Add(fixture1);

            FixtureAssembly assembly1 = FixtureAssembly.NewFixtureAssembly();
            assembly1.Name = "assem1";
            package.Assemblies.Add(assembly1);

            FixtureAssemblyItem fAssembly1 = FixtureAssemblyItem.NewFixtureAssemblyItem();
            fAssembly1.FixtureAssemblyId = assembly1.Id;
            fixture1.Assemblies.Add(fAssembly1);

            FixtureComponent component1 = FixtureComponent.NewFixtureComponent();
            component1.Name = "c1";
            package.Components.Add(component1);

            FixtureAssemblyComponent cItem1 = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
            cItem1.FixtureComponentId = component1.Id;
            assembly1.Components.Add(cItem1);

            //save
            Galleria.Ccm.Model.FixturePackage savedPackage = null;
            savedPackage = package.Save();
            savedPackage = Galleria.Ccm.Model.FixturePackage.FetchById(savedPackage.Id);
            id = savedPackage.Id;


            //check the fixture library that was saved
            var fixtureLibrary = FixturePackage.FetchById(id);
            Assert.AreEqual(1, fixtureLibrary.Fixtures.Count, "Fixture count wrong");
            Assert.AreEqual(1, fixtureLibrary.Fixtures[0].Assemblies.Count, "FixtureAssemblyItem count wrong");
            Assert.AreEqual(1, fixtureLibrary.Assemblies.Count, "FixtureAssembly count wrong");
            Assert.AreEqual(1, fixtureLibrary.Assemblies[0].Components.Count, "FixtureComponent count wrong.");
        }


       
        #endregion

    }


}
