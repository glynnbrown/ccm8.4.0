﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24979 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Media;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.ComponentEditor;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.UI.ComponentEditor
{
    [TestFixture]
    public sealed class EditorComponentViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramComponentView CreateFixtureComponentView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);



            PlanogramView planView = new PlanogramView(plan);
            return planView.EnumerateAllComponents().First(c => c.FixtureComponentModel != null);
        }

        private static PlanogramComponentView CreateSimpleShelfComponent()
        {
            //create a simple plan
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramComponentView shelfView =  planView.Fixtures[0].Components[0];

            return shelfView;
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> ComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { 
                    "Parent", "Height", "Width", "Depth",
                    "ImageIdFront", "ImageIdBack", "ImageIdTop", "ImageIdBottom", "ImageIdLeft", "ImageIdRight",
                    "ComponentType"
                };

                foreach (PropertyInfo modelProperty in typeof(PlanogramComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("ComponentModelPropertyNames")]
        public void ComponentModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramComponent).GetProperty(propertyName);
            PropertyInfo editorViewProperty = typeof(EditorComponentView).GetProperty(propertyName);

            if (editorViewProperty != null)
            {
                PlanogramComponent planComponent = CreateFixtureComponentView().ComponentModel;
                if (modelProperty.CanWrite)
                {
                    Object newValue = GetPropertyTestValue1(modelProperty.PropertyType);
                    modelProperty.SetValue(planComponent, newValue, null);
                }

                EditorComponentView editorView = new EditorComponentView(planComponent);

                //Test get
                Assert.AreEqual(Convert.ToString(modelProperty.GetValue(planComponent, null)), Convert.ToString(editorViewProperty.GetValue(editorView, null)),
                    String.Format("{0} get failed", editorViewProperty.Name));

                //Test set
                if (editorViewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();
                    base.PropertyChangingNotifications.Clear();
                    editorView.PropertyChanging += base.TestModel_PropertyChanging;
                    editorView.PropertyChanged += base.TestModel_PropertyChanged;


                    //set another value
                    Object newValue2 = GetPropertyTestValue2(editorViewProperty.PropertyType);
                    editorViewProperty.SetValue(editorView, newValue2, null);

                    Assert.AreEqual(Convert.ToString(newValue2), Convert.ToString(editorViewProperty.GetValue(editorView, null)),
                    String.Format("{0} set failed", editorViewProperty.Name));

                    Assert.Contains(editorViewProperty.Name, base.PropertyChangingNotifications,
                        String.Format("{0} property changing not fired", editorViewProperty.Name));

                    Assert.Contains(editorViewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", editorViewProperty.Name));

                    editorView.PropertyChanging -= base.TestModel_PropertyChanging;
                    editorView.PropertyChanged -= base.TestModel_PropertyChanged;
                    

                    //now check that the property gets copied back to the model.
                    editorView.ToPlanogramComponent();


                    Assert.AreEqual(Convert.ToString(newValue2), Convert.ToString(modelProperty.GetValue(planComponent, null)));

                }


            }
            else
            {
                throw new InconclusiveException("Property missing from view");
            }

        }

        #endregion

        #region Methods

        [Test]
        public void Constructor()
        {
           //nothing to test atm
        }

        [Test]
        public void AddSubComponent()
        {
            EditorComponentView view = new EditorComponentView(CreateFixtureComponentView().ComponentModel);

            EditorSubComponentView subView = view.AddSubComponent(0,0,0,10,10,10);
            Assert.IsNotNull(subView);
            Assert.Contains(subView, view.SubComponents);
        }

        [Test]
        public void AddSubComponentCopy()
        {
            EditorComponentView view = new EditorComponentView(CreateFixtureComponentView().ComponentModel);

            EditorSubComponentView subView = view.AddSubComponent(0, 0, 0, 10, 10, 10);
            Assert.IsNotNull(subView);
            Assert.Contains(subView, view.SubComponents);
        }

        [Test]
        public void UndoRedo_ChangeComponentProperty()
        {
            EditorComponentView view = new EditorComponentView(CreateFixtureComponentView().ComponentModel);

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.Name = val1;
            view.ClearAllUndoRedoActions();

            //create the undo action
            view.BeginUndoableAction();

            view.Name = val2;

            view.EndUndoableAction();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);

            //undo it
            view.Undo();

            Assert.AreEqual(val1, view.Name);
            Assert.AreEqual(0, view.UndoActions.Count);
            Assert.AreEqual(1, view.RedoActions.Count);

            //redo it
            view.Redo();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);
        }

        [Test]
        public void UndoRedo_AddSubComponent()
        {
            EditorComponentView view = new EditorComponentView(CreateFixtureComponentView().ComponentModel);

            Int32 preCount = view.SubComponents.Count;

            //create the undo action
            view.BeginUndoableAction();

            view.AddSubComponent(0, 0, 0, 10, 10, 10);

            view.EndUndoableAction();

            Assert.AreEqual(preCount + 1, view.SubComponents.Count);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);

            //undo it
            view.Undo();

            Assert.AreEqual(preCount, view.SubComponents.Count);
            Assert.AreEqual(0, view.UndoActions.Count);
            Assert.AreEqual(1, view.RedoActions.Count);

            //redo it
            view.Redo();

            Assert.AreEqual(preCount + 1, view.SubComponents.Count);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);
        }

        [Test]
        public void ComponentType_SimplePropertiesDoNotChangeType()
        {
            PlanogramComponentView planShelfView = CreateSimpleShelfComponent();

            //create the component view
            EditorComponentView view = new EditorComponentView(planShelfView.ComponentModel);

            //set the sub colour
            view.SubComponents[0].FillColourFront = CommonHelper.ColorToInt(Colors.Red);

            //convert back
            view.ToPlanogramComponent();

            Assert.AreEqual(CommonHelper.ColorToInt(Colors.Red), planShelfView.SubComponents[0].FillColourFront);
            Assert.AreEqual(PlanogramComponentType.Shelf, planShelfView.ComponentType);
        }

        [Test]
        public void ComponentType_AdvancedPropertiesChangeType()
        {
            PlanogramComponentView planShelfView = CreateSimpleShelfComponent();

            //create the component view
            EditorComponentView view = new EditorComponentView(planShelfView.ComponentModel);

            //set the sub merch type
            view.SubComponents[0].MerchandisingType = Ccm.Model.FixtureSubComponentMerchandisingType.Hang;

            //convert back
            view.ToPlanogramComponent();

            Assert.AreEqual(PlanogramSubComponentMerchandisingType.Hang, planShelfView.SubComponents[0].MerchandisingType);
            Assert.AreNotEqual(PlanogramComponentType.Shelf, planShelfView.ComponentType);
        }


        [Test]
        public void ToPlanogramComponent_SubImages()
        {
            PlanogramComponentView planShelfView = CreateSimpleShelfComponent();

            //create the component view
            EditorComponentView view = new EditorComponentView(planShelfView.ComponentModel);

            //add some images to the subcomponent
            FixtureImage img1 =  view.AddImage(new Byte[] { 1, 2, 3 }, "Img1", "Img1Description");
            FixtureImage img2 = view.AddImage(new Byte[] { 2, 3, 4 }, "Img2", "Img2Description");

            view.SubComponents[0].FrontImage = img1;
            view.SubComponents[0].BackImage = img2;


            //convert back
            view.ToPlanogramComponent();

            //check that the planogram now has the correct images
            Assert.AreEqual(2, planShelfView.Planogram.Model.Images.Count);
            Assert.AreEqual(img1.FileName, planShelfView.Planogram.Model.Images[0].FileName);
            Assert.AreEqual(img2.FileName, planShelfView.Planogram.Model.Images[1].FileName);

            //check that the images were correctly linked
            Assert.AreEqual(planShelfView.Planogram.Model.Images[0].Id, planShelfView.SubComponents[0].Model.ImageIdFront);
            Assert.AreEqual(planShelfView.Planogram.Model.Images[1].Id, planShelfView.SubComponents[0].Model.ImageIdBack);
        }

        #endregion

    }
}
