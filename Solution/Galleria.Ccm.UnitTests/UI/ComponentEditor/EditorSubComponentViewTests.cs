﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24979 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.ComponentEditor;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.ComponentEditor
{
    [TestFixture]
    public sealed class EditorSubComponentViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramComponentView CreateFixtureComponentView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);



            PlanogramView planView = new PlanogramView(plan);
            return planView.EnumerateAllComponents().First(c => c.FixtureComponentModel != null);
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> SubComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { 
                    "Parent",
                    "ImageIdFront", "ImageIdBack", "ImageIdTop", "ImageIdBottom", "ImageIdLeft", "ImageIdRight"
                };

                foreach (PropertyInfo modelProperty in typeof(PlanogramSubComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramSubComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("SubComponentModelPropertyNames")]
        public void SubComponentModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramSubComponent).GetProperty(propertyName);
            PropertyInfo editorViewProperty = typeof(EditorSubComponentView).GetProperty(propertyName);

            if (editorViewProperty != null)
            {
                PlanogramSubComponent planSubComponent = CreateFixtureComponentView().ComponentModel.SubComponents[0];
                if (modelProperty.CanWrite)
                {
                    Object newValue = GetPropertyTestValue1(modelProperty.PropertyType);
                    modelProperty.SetValue(planSubComponent, newValue, null);
                }

                EditorComponentView componentView = new EditorComponentView(planSubComponent.Parent);
                EditorSubComponentView editorView = componentView.SubComponents[0];

                //Test get
                Assert.AreEqual(Convert.ToString(modelProperty.GetValue(planSubComponent, null)), Convert.ToString(editorViewProperty.GetValue(editorView, null)),
                    String.Format("{0} get failed", editorViewProperty.Name));

                //Test set
                if (editorViewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();
                    base.PropertyChangingNotifications.Clear();
                    editorView.PropertyChanging += base.TestModel_PropertyChanging;
                    editorView.PropertyChanged += base.TestModel_PropertyChanged;


                    //set another value
                    Object newValue2 = GetPropertyTestValue2(editorViewProperty.PropertyType);
                    editorViewProperty.SetValue(editorView, newValue2, null);

                    Assert.AreEqual(Convert.ToString(newValue2), Convert.ToString(editorViewProperty.GetValue(editorView, null)),
                    String.Format("{0} set failed", editorViewProperty.Name));

                    Assert.Contains(editorViewProperty.Name, base.PropertyChangingNotifications,
                        String.Format("{0} property changing not fired", editorViewProperty.Name));

                    Assert.Contains(editorViewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", editorViewProperty.Name));

                    editorView.PropertyChanging -= base.TestModel_PropertyChanging;
                    editorView.PropertyChanged -= base.TestModel_PropertyChanged;


                    //now check that the property gets copied back to the model.
                    componentView.ToPlanogramComponent();
                    //editorView.ToPlanogramSubComponent();

                    if (editorViewProperty.Name != "X"
                        && editorViewProperty.Name != "Y"
                        && editorViewProperty.Name != "Z")
                    {

                        Assert.AreEqual(Convert.ToString(newValue2), Convert.ToString(modelProperty.GetValue(planSubComponent, null)));
                    }

                }


            }
            else
            {
                throw new InconclusiveException("Property missing from view");
            }

        }

        #endregion

        #region Undo Redo

        [Test]
        public void UndoRedo_ChangeSubComponentProperty()
        {
            EditorComponentView view = new EditorComponentView(CreateFixtureComponentView().ComponentModel);

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.SubComponents[0].Name = val1;
            view.ClearAllUndoRedoActions();

            //create the undo action
            view.SubComponents[0].Name = val2;

            Assert.AreEqual(val2, view.SubComponents[0].Name);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);

            //undo it
            view.Undo();

            Assert.AreEqual(val1, view.SubComponents[0].Name);
            Assert.AreEqual(0, view.UndoActions.Count);
            Assert.AreEqual(1, view.RedoActions.Count);

            //redo it
            view.Redo();

            Assert.AreEqual(val2, view.SubComponents[0].Name);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);
        }

        #endregion
    }

    
}
