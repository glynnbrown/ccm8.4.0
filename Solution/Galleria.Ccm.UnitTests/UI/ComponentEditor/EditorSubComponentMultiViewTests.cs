﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26495 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Media;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.ComponentEditor;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Converters;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.UI.ComponentEditor
{
    [TestFixture]
    public sealed class EditorSubComponentMultiViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static EditorComponentView CreateEditorComponentView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a component with 2 subs
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);

            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponent sub2 = PlanogramSubComponent.NewPlanogramSubComponent();
            sub2.Name = "sub2";
            shelf1C.SubComponents.Add(sub2);

            EditorComponentView componentView = new EditorComponentView(shelf1C);

            return componentView;
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> EditorSubComponentViewPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { 
                    "ParentComponentView", "Model", "SourcePlanogramSubComponent","Error", "Item",
                    "Name"
                };

                foreach (PropertyInfo modelProperty in typeof(EditorSubComponentView).GetProperties()
             .Where(p => p.DeclaringType == typeof(EditorSubComponentView)))
                {
                    if (!exclusions.Contains(modelProperty.Name))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion


        [Test, TestCaseSource("EditorSubComponentViewPropertyNames")]
        public void SubComponentProperty(String propertyName)
        {
            EditorComponentView componentView = CreateEditorComponentView();
            EditorSubComponentView item1 = componentView.SubComponents[0];
            EditorSubComponentView item2 = componentView.SubComponents[1];

            EditorSubComponentMultiView selection;

            PropertyInfo selectionProperty = typeof(EditorSubComponentMultiView).GetProperty(propertyName);
            if (selectionProperty == null)
            {
                throw new InconclusiveException("Property not found");
            }

            PropertyInfo viewProperty = typeof(EditorSubComponentView).GetProperty(propertyName);



            if (viewProperty.CanWrite)
            {
                selection = new EditorSubComponentMultiView(new List<EditorSubComponentView> { item1, item2 });
                ((INotifyPropertyChanged)selection).PropertyChanged += base.TestModel_PropertyChanged;

                //set both values to the same thing
                Object newValue1 = GetPropertyTestValue1(viewProperty.PropertyType);
                viewProperty.SetValue(item1, newValue1, null);
                viewProperty.SetValue(item2, newValue1, null);

                if (selectionProperty.PropertyType == typeof(Color))
                {
                    Assert.AreEqual(CommonHelper.IntToColor((Int32)newValue1), selectionProperty.GetValue(selection, null),
                        String.Format("{0} get same failed", propertyName));
                }
                else if (viewProperty.Name == "Slope" || viewProperty.Name == "Roll" || viewProperty.Name == "Angle")
                {
                    Assert.AreEqual(CommonHelper.ToDegrees((Single)newValue1), selectionProperty.GetValue(selection, null),
                        String.Format("{0} get same failed", propertyName));
                }
                else
                {
                    Assert.AreEqual(newValue1, selectionProperty.GetValue(selection, null),
                        String.Format("{0} get same failed", propertyName));
                }

                //set item2 to different
                Object newValue2 = GetPropertyTestValue2(viewProperty.PropertyType);
                viewProperty.SetValue(item2, newValue2, null);

                if (selectionProperty.PropertyType == typeof(Color))
                {
                    Assert.AreEqual(Colors.White, selectionProperty.GetValue(selection, null),
                        String.Format("{0} get different failed", propertyName));
                }
                else
                {
                    Assert.IsNull(selectionProperty.GetValue(selection, null),
                        String.Format("{0} get different failed", propertyName));
                }
            }
            else
            {
                selection = new EditorSubComponentMultiView(new List<EditorSubComponentView> { item1 });
                ((INotifyPropertyChanged)selection).PropertyChanged += base.TestModel_PropertyChanged;

                Assert.AreEqual(viewProperty.GetValue(item1, null),
                    selectionProperty.GetValue(selection, null),
                    String.Format("{0} get failed", propertyName));
            }



            ((INotifyPropertyChanged)selection).PropertyChanged -= base.TestModel_PropertyChanged;
        }
    }
}
