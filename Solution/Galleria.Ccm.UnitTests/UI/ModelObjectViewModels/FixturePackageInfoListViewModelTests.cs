﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24979 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Model;
using System.IO;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class FixturePackageInfoListViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private List<String> InsertFixturePackages()
        {
            List<String> returnList = new List<String>();

            String directory = GetFixtureDirectoryPath();

            //add some component fixtures
            for (Int32 i = 0; i < 5; i++)
            {
                String path = Path.Combine(directory, "fix" + i);
                path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

                FixturePackage package = FixturePackage.NewFixturePackage(path);
                package.Name = "fix" + i;
                package.Description = "fix description" + i;

                //add a component
                FixtureComponent component = FixtureComponent.NewFixtureComponent();
                component.Name = "component" + i;
                component.Height = 4;
                component.Width = 100;
                component.Depth = 75;

                FixtureSubComponent subComponent = FixtureSubComponent.NewFixtureSubComponent();
                subComponent.Name = "s1";
                subComponent.Height = 4;
                subComponent.Width = 100;
                subComponent.Depth = 75;
                subComponent.MerchandisingType = FixtureSubComponentMerchandisingType.Stack;

                component.SubComponents.Add(subComponent);
                package.Components.Add(component);

                package = package.Save();
                
                returnList.Add(package.FileName);

                package.Dispose();
                package = null;
            }

            return returnList;
        }

        #endregion


        [Test]
        public void FetchAll()
        {
            List<String> packageNames = InsertFixturePackages();


            FixturePackageInfoListViewModel viewModel = new FixturePackageInfoListViewModel();
            viewModel.FetchAll();

            Assert.AreEqual(packageNames.Count, viewModel.Model.Count);

            foreach (FixturePackageInfo info in viewModel.Model)
            {
                Assert.Contains(info.FileName, packageNames);
            }
        }

    }
}
