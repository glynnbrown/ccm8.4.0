﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class PlanogramAnnotationViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramAnnotationView CreateFixtureAnnotationView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

            //add 1 plan
            Planogram plan = Planogram.NewPlanogram();
            plan.Name = "Plan 1";
            package.Planograms.Add(plan);

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = "Fixture 1";
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);

            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = fixture.Id;
            fixtureItem.BaySequenceNumber = (Int16)1;
            plan.FixtureItems.Add(fixtureItem);


            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);


            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllAnnotations().First();
        }

        private static PlanogramAnnotationView CreateFixtureSubComponentAnnotationView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

            //add 1 plan
            Planogram plan = Planogram.NewPlanogram();
            plan.Name = "Plan 1";
            package.Planograms.Add(plan);

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = "Fixture 1";
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);

            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = fixture.Id;
            fixtureItem.BaySequenceNumber = (Int16)1;
            plan.FixtureItems.Add(fixtureItem);

            //add a component
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 10, 10, 10);


            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.PlanogramFixtureComponentId = fc.Id;
            anno1.PlanogramSubComponentId = fc.GetPlanogramComponent().SubComponents[0].Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);



            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllAnnotations().First();
        }

        private static PlanogramAnnotationView CreateAssemblySubComponentAnnotationView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

            //add 1 plan
            Planogram plan = Planogram.NewPlanogram();
            plan.Name = "Plan 1";
            package.Planograms.Add(plan);

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = "Fixture 1";
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);

            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = fixture.Id;
            fixtureItem.BaySequenceNumber = (Int16)1;
            plan.FixtureItems.Add(fixtureItem);

            //add a component
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 10, 10, 10);
            PlanogramFixtureAssembly ac =  fixture.Assemblies.Add(
                new List<Tuple<PlanogramFixtureComponent, PlanogramFixtureItem>>{
                    new Tuple<PlanogramFixtureComponent, PlanogramFixtureItem>(fc, fixtureItem)
                }, fixtureItem);

            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.PlanogramFixtureAssemblyId = ac.Id;
            anno1.PlanogramAssemblyComponentId = ac.GetPlanogramAssembly().Components[0].Id;
            anno1.PlanogramSubComponentId = ac.GetPlanogramAssembly().Components[0].GetPlanogramComponent().SubComponents[0].Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);



            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllAnnotations().First();
        }

        private static PlanogramAnnotationView CreatePositionAnnotationView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

            //add 1 plan
            Planogram plan = Planogram.NewPlanogram();
            plan.Name = "Plan 1";
            package.Planograms.Add(plan);

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = "Fixture 1";
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);

            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = fixture.Id;
            fixtureItem.BaySequenceNumber = (Int16)1;
            plan.FixtureItems.Add(fixtureItem);

            //add a component
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 10, 10, 10);

            //add a product
            PlanogramProduct product1 = PlanogramProduct.NewPlanogramProduct();
            plan.Products.Add(product1);

            //add a position
            PlanogramPosition pos = plan.Positions.Add(
                product1,
                PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                    fc.GetPlanogramComponent().SubComponents[0],
                    fixtureItem,
                    fc));

            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.PlanogramFixtureComponentId = fc.Id;
            anno1.PlanogramSubComponentId = fc.GetPlanogramComponent().SubComponents[0].Id;
            anno1.PlanogramPositionId = pos.Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);



            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllAnnotations().First();
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> ModelPropertyNames
        {
            get 
            { 
                String[] exclusions = new String[] { "Parent" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramAnnotation).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramAnnotation)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("ModelPropertyNames")]
        public void ModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramAnnotation).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramAnnotationView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramAnnotationView annoView = CreateFixtureAnnotationView();
                annoView.PropertyChanged += base.TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(annoView.Model, null),
                    viewProperty.GetValue(annoView, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    if (Object.Equals(newValue, modelProperty.GetValue(annoView.Model, null)))
                    {
                        newValue = GetPropertyTestValue2(viewProperty.PropertyType);
                    }
                    viewProperty.SetValue(annoView, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(annoView.Model, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(annoView, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                annoView.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }


        [Test]
        public void WorldSpaceProperties_AreInitialized()
        {
            var view = CreateFixtureAnnotationView();

            PointValue expectedValue = view.Model.GetPlanogramRelativeCoordinates();

            Assert.AreEqual(expectedValue.X, view.WorldX);
            Assert.AreEqual(expectedValue.Y, view.WorldY);
            Assert.AreEqual(expectedValue.Z, view.WorldZ);
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByFixtureMove()
        {
            var view = CreateFixtureAnnotationView();
            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the fixture
            view.Model.GetPlanogramFixtureItem().X += 10;

            Assert.Contains(PlanogramAnnotationView.WorldXProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldYProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldZProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByAssemblyMove()
        {
            var view = CreateAssemblySubComponentAnnotationView();
            PlanogramView planView = view.Planogram;

            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the assembly
            view.Model.GetPlanogramFixtureAssembly().X += 10;

            Assert.Contains(PlanogramAnnotationView.WorldXProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldYProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldZProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByAssemblyComponentMove()
        {
            var view = CreateAssemblySubComponentAnnotationView();
            PlanogramView planView = view.Planogram;

            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the assembly
            view.Model.GetPlanogramAssemblyComponent().X += 10;

            Assert.Contains(PlanogramAnnotationView.WorldXProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldYProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldZProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByFixtureComponentMove()
        {
            var view = CreateFixtureSubComponentAnnotationView();
            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the fixture
            view.Model.GetPlanogramFixtureComponent().X += 10;

            Assert.Contains(PlanogramAnnotationView.WorldXProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldYProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldZProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedBySubComponentMove()
        {
            var view = CreateFixtureSubComponentAnnotationView();
            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the fixture
            view.Model.GetPlanogramSubComponent().X += 10;

            Assert.Contains(PlanogramAnnotationView.WorldXProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldYProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldZProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        //NB: Position annotations not yet supported.
        //[Test]
        //public void WorldSpaceProperties_UpdatedByPositionMove()
        //{
        //    var view = CreatePositionAnnotationView();
        //    view.PropertyChanged += base.TestModel_PropertyChanged;
        //    base.PropertyChangedNotifications.Clear();

        //    //move the fixture
        //    view.Model.GetPlanogramPosition().X += 10;

        //    Assert.Contains(PlanogramAnnotationView.WorldXProperty.Path, base.PropertyChangedNotifications);
        //    Assert.Contains(PlanogramAnnotationView.WorldYProperty.Path, base.PropertyChangedNotifications);
        //    Assert.Contains(PlanogramAnnotationView.WorldZProperty.Path, base.PropertyChangedNotifications);

        //    view.PropertyChanged -= base.TestModel_PropertyChanged;
        //}

        [Test]
        public void WorldSpaceProperties_UpdatedByAnnotationMove()
        {
            var view = CreateFixtureAnnotationView();
            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the fixture
            view.X = 10;

            Assert.Contains(PlanogramAnnotationView.WorldXProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldYProperty.Path, base.PropertyChangedNotifications);
            Assert.Contains(PlanogramAnnotationView.WorldZProperty.Path, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }


        #endregion

        [Test]
        public void Constructor()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();

            PlanogramAnnotation anno = fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            PlanogramView planView = new PlanogramView(plan);
            PlanogramAnnotationView view = planView.Fixtures[0].Annotations[0];

            Assert.AreEqual(anno, view.Model);
        }

    }
}
