﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015
#region Version History: (CCM 8.30)
// V8-32592 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.UnitTests.Helpers;


namespace Galleria.Ccm.UnitTests.UI
{
    /// <summary>
    /// Tests checking that copy paste behaviour is correct.
    /// </summary>
    [TestFixture]
    public sealed class CopyPasteBehaviourTests : TestBase
    {
        [Test]
        public void AddPlanItemPastesPlanItemEvenWhenOriginalParentFixtureIsMissing()
        {
            // V8-32498 : A.Silva
            //  Added AddPlanItemPastesPlanItemEvenWhenOriginalParentFixtureIsMissing so that 
            //  deleting the parent fixture of the source items will not crash when pasting back to the same planogram.

            const String expectation = "The plan item should have been pasted even though the original parent fixture is missing.";
            Package package = "TestPackage".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            List<PlanogramFixtureItem> fixtureItems = planogram.AddFixtureItems(2);
            PlanogramFixtureItem originalParentFixture = fixtureItems.Last();
            originalParentFixture.AddFixtureComponent(PlanogramComponentType.Shelf);
            PackageViewModel packageViewModel = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView view = packageViewModel.PlanogramViews.First();
            PlanogramComponentView planItem = view.EnumerateAllComponents().First();
            if (view.Fixtures.First().Components.Any()) Assert.Inconclusive("There should be no component on the target fixture.");

            PlanogramFixtureView fixtureView = view.Fixtures.Last();
            fixtureView.RemoveComponent(planItem);
            view.RemoveFixture(fixtureView, true);
            view.AddPlanItemCopy(planItem, false, false);

            Assert.IsTrue(view.Fixtures.First().Components.Any(), expectation);
        }

        [Test]
        public void AddingNewPositionFromAnotherPlanogramAddsNewProduct()
        {
            // V8-32080 : A.Silva
            //  Added tests for adding new positions from another plan.
            // V8-32327 : A.Silva
            //  Added tests to check that positions are added in the expected order.

            const String expectation =
                "When adding a new position (product did not exist previously) from an existing position in another planogram, the product list should contain the added position's product.";
            Planogram sourcePlan = "SourcePlan".CreatePackage().AddPlanogram();
            PlanogramFixtureItem sourceFixture = sourcePlan.AddFixtureItem();
            PlanogramProduct sourceProduct = sourcePlan.AddProduct(10, 10, 10);
            sourceFixture.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(sourceFixture, sourceProduct);
            Object expectedProductGtin = sourceProduct.Gtin;
            Planogram targetPlan = "TargetPlan".CreatePackage().AddPlanogram();
            targetPlan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            var sourceView = new PlanogramView(sourcePlan, PackageViewModel.NewPackageViewModel(sourcePlan.Parent));
            var targetView = new PlanogramView(targetPlan, PackageViewModel.NewPackageViewModel(targetPlan.Parent));
            if (targetView.Products.Any(view => Equals(view.Gtin, expectedProductGtin)))
                Assert.Inconclusive("The added new position should be for a product that did not exist previously in the target planogram.");
            PlanogramSubComponentView testView = targetView.Fixtures.First().Components.First().SubComponents.First();

            testView.AddPositions(sourceView.Products.ToList(), 0, 0, 0);

            Assert.That(targetView.Products.Any(view => Equals(view.Gtin, expectedProductGtin)), Is.True, expectation);
        }

        [Test]
        public void AddingNewPositionFromAnotherPlanogramUsingAnchorAddsNewProduct()
        {
            // V8-32080 : A.Silva
            //  Added tests for adding new positions from another plan.
            // V8-32327 : A.Silva
            //  Added tests to check that positions are added in the expected order.

            const String expectation =
                "When adding a new position (product did not exist previously) from an existing position in another planogram and using an anchor, the product list should contain the added position's product.";
            Planogram sourcePlan = "SourcePlan".CreatePackage().AddPlanogram();
            PlanogramFixtureItem sourceFixture = sourcePlan.AddFixtureItem();
            PlanogramProduct sourceProduct = sourcePlan.AddProduct();
            sourceFixture.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(sourceFixture, sourceProduct);
            Object expectedProductGtin = sourceProduct.Gtin;
            Planogram targetPlan = "TargetPlan".CreatePackage().AddPlanogram();
            PlanogramFixtureItem targetFixture = targetPlan.AddFixtureItem();
            PlanogramProduct anchorProduct = targetPlan.AddProduct();
            anchorProduct.Gtin = "AnchorGtin";
            targetFixture.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(targetFixture, anchorProduct);
            var sourceView = new PlanogramView(sourcePlan, PackageViewModel.NewPackageViewModel(sourcePlan.Parent));
            var targetView = new PlanogramView(targetPlan, PackageViewModel.NewPackageViewModel(targetPlan.Parent));
            if (targetView.Products.Any(view => Equals(view.Gtin, expectedProductGtin)))
                Assert.Inconclusive("The added new position should be for a product that did not exist previously in the target planogram.");
            PlanogramSubComponentView testView = targetView.Fixtures.First().Components.First().SubComponents.First();
            PlanogramPositionView anchorPosition = targetView.Fixtures.First().Components.First().SubComponents.First().Positions.First();

            testView.AddPositions(sourceView.Products.ToList(), anchorPosition, PlanogramPositionAnchorDirection.ToRight);

            Assert.That(targetView.Products.Any(view => Equals(view.Gtin, expectedProductGtin)), Is.True, expectation);
        }


        [Test]
        public void CopyPasteComponentWithLinkedTextBox()
        {
            //V8-32592 - Checks that copying a component with a linked textbox
            // pastes the textbox correctly.

            var newPlanController = base.AddNewEmptyPlan();
            PlanogramView planView = newPlanController.SourcePlanogram;

            //add a shelf and linked textbox
            var shelf = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            var linkedAnno = shelf.AddAnnotation();
            linkedAnno.Text = "test";
            linkedAnno.Height = 20;
            linkedAnno.Width = 100;
            linkedAnno.Depth = 1;
            linkedAnno.X = -linkedAnno.Width;

            //now select the shelf
            newPlanController.SelectedPlanItems.SetSelection(shelf);

            //copy
            MainPageCommands.Copy.Execute();
            

            //create a new plan
            var newPlanController2 = base.AddNewEmptyPlan();
            PlanogramView planView2 = newPlanController.SourcePlanogram;
            App.MainPageViewModel.ActivePlanController = newPlanController2;

            //paste
            MainPageCommands.Paste.Execute();


            //check that both were copied and copied correctly.
            var anno2 = planView2.Fixtures[0].Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Shelf).Annotations[0];
            Assert.AreEqual(linkedAnno.Text, anno2.Text);
            Assert.AreEqual(linkedAnno.X, anno2.X);
        }

        [Test]
        public void CopyPasteToAllPlansComponentWithLinkedTextBox()
        {
            //V8-32592 - Checks that copying a component with a linked textbox
            // pastes the textbox correctly.

            var newPlanController = base.AddNewEmptyPlan();
            PlanogramView planView = newPlanController.SourcePlanogram;

            //add a shelf and linked textbox
            var shelf = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            var linkedAnno = shelf.AddAnnotation();
            linkedAnno.Text = "test";
            linkedAnno.Height = 20;
            linkedAnno.Width = 100;
            linkedAnno.Depth = 1;
            linkedAnno.X = -linkedAnno.Width;

            //now select the shelf
            newPlanController.SelectedPlanItems.SetSelection(shelf);

            //copy
            MainPageCommands.Copy.Execute();


            //create a new plan
            var newPlanController2 = base.AddNewEmptyPlan();
            PlanogramView planView2 = newPlanController.SourcePlanogram;
            App.MainPageViewModel.ActivePlanController = newPlanController2;

            //paste
            MainPageCommands.PasteToAllPlanograms.Execute();


            //check that both were copied and copied correctly.
            var anno2 = planView2.Fixtures[0].Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Shelf).Annotations[0];
            Assert.AreEqual(linkedAnno.Text, anno2.Text);
            Assert.AreEqual(linkedAnno.X, anno2.X);
        }

        [Test]
        public void Copy_PositionFromAnotherPlan()
        {
            var mainPageView = App.MainPageViewModel;

            //create a plan to copy from
            var controller1 = base.AddNewEmptyPlan();
            controller1.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(controller1.SourcePlanogram.Model);
            controller1.SourcePlanogram.EndUpdate();
            var posToCopy = controller1.SourcePlanogram.EnumerateAllPositions().First();

            //create another to copy to
            var controller2 = base.AddNewEmptyPlan();
            if (controller2.SourcePlanogram.Fixtures.Count == 0) controller2.SourcePlanogram.AddFixture();
            var shelf = controller2.SourcePlanogram.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(0, controller2.SourcePlanogram.Products.Count, "Should start with no products");
            Assert.AreEqual(0, controller2.SourcePlanogram.EnumerateAllPositions().Count(), "Shoudl start with no positions");

            //copy the position
            mainPageView.ActivePlanController = controller1;
            mainPageView.ActivePlanController.SelectedPlanItems.SetSelection(posToCopy);
            MainPageCommands.Copy.Execute();

            //switch to the second plan and paste
            mainPageView.ActivePlanController = controller2;
            mainPageView.ActivePlanController.SelectedPlanItems.SetSelection(shelf);
            MainPageCommands.Paste.Execute();

            //check
            Assert.AreEqual(1, controller2.SourcePlanogram.Products.Count, "Product not added");
            Assert.AreEqual(posToCopy.Product.Gtin, controller2.SourcePlanogram.Products[0].Gtin, "Product does not match.");
            Assert.AreEqual(1, controller2.SourcePlanogram.EnumerateAllPositions().Count(), "Position not added");
        }

        [Test]
        public void Copy_ComponentFromAnotherPlan()
        {
            var mainPageView = App.MainPageViewModel;

            //create a plan to copy from
            var controller1 = base.AddNewEmptyPlan();
            if (controller1.SourcePlanogram.Fixtures.Count == 0) controller1.SourcePlanogram.AddFixture();
            var componentToCopy = controller1.SourcePlanogram.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);

            //create another to copy to
            var controller2 = base.AddNewEmptyPlan();
            if (controller2.SourcePlanogram.Fixtures.Count == 0) controller2.SourcePlanogram.AddFixture();
            controller2.SourcePlanogram.Fixtures[0].SetBackboard(false);
            controller2.SourcePlanogram.Fixtures[0].SetBase(false);
            Assert.AreEqual(0, controller2.SourcePlanogram.EnumerateAllComponents().Count());

            //copy the component
            mainPageView.ActivePlanController = controller1;
            mainPageView.ActivePlanController.SelectedPlanItems.SetSelection(componentToCopy);
            MainPageCommands.Copy.Execute();

            //switch to the second plan and paste
            mainPageView.ActivePlanController = controller2;
            MainPageCommands.Paste.Execute();

            //check
            Assert.AreEqual(1, controller2.SourcePlanogram.EnumerateAllComponents().Count());
            Assert.AreEqual(1, controller2.SourcePlanogram.EnumerateAllSubComponents().Count());
        }
    }
}
