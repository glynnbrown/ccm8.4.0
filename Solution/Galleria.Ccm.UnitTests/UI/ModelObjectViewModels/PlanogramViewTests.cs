﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24764 : L.Hodson
//  Created

#endregion

#region Version History: (CCM 8.1.1)

// V8-27469 : A.Probyn
//  Updated ModelPropertyGetSet to set 2 values, ensuring the property changed events are always
//  triggered for values where it may already be set to the test value.

#endregion

#region Version History: (CCM 8.30)

// V8-32359 : J.Pickup
//  Added UpdateProducts_AssertDeaultValueIsNotApplied, UpdateProducts_AssertDeaultValueIsApplied, UpdateProducts_AssertDefaultValueDoesNotOverwriteProductLibraryValue
// V8-32498 : A.Silva
//  Added AddPlanItemPastesPlanItemEvenWhenOriginalParentFixtureIsMissing so that 
//  deleting the parent fixture of the source items will not crash when pasting back to the same planogram.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class PlanogramViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramView CreateView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

            //add 1 plan
            Planogram plan = Planogram.NewPlanogram();
            plan.Name = "Plan 1";
            package.Planograms.Add(plan);

            return new PlanogramView(plan);
        }

        private static Planogram CreatePlanogram()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

            //add 1 plan
            Planogram plan = Planogram.NewPlanogram();
            plan.Name = "Plan 1";
            package.Planograms.Add(plan);

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = "Fixture 1";
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);

            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = fixture.Id;
            fixtureItem.BaySequenceNumber = (Int16)1;
            plan.FixtureItems.Add(fixtureItem);


            //add a backboard
            PlanogramComponent component1 = PlanogramComponent.NewPlanogramComponent();
            component1.Name = "Backboard";
            component1.Width = fixture.Width;
            component1.Height = fixture.Height;
            component1.Depth = 75;
            plan.Components.Add(component1);

            PlanogramFixtureComponent fixtureComponent1 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
            fixtureComponent1.PlanogramComponentId = component1.Id;
            fixture.Components.Add(fixtureComponent1);

            PlanogramSubComponent sub1 = PlanogramSubComponent.NewPlanogramSubComponent();
            sub1.Name = "Backboard part";
            sub1.LineThickness = 0.5F;
            sub1.LineColour = -1;
            sub1.MerchandisingType = PlanogramSubComponentMerchandisingType.None;
            sub1.FillColourFront = -1;
            sub1.FillColourBack = -1;
            sub1.FillColourLeft = -1;
            sub1.FillColourRight = -1;
            sub1.FillColourTop = -1;
            sub1.FillColourBottom = -1;
            sub1.Width = component1.Width;
            sub1.Height = component1.Height;
            sub1.Depth = component1.Depth;
            component1.SubComponents.Add(sub1);

            //add a shelf
            PlanogramComponent component2 = PlanogramComponent.NewPlanogramComponent();
            component2.Name = "Shelf 1";
            component2.Width = fixture.Width;
            component2.Height = 4;
            component2.Depth = 75;
            plan.Components.Add(component2);

            PlanogramFixtureComponent fixtureComponent2 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
            fixtureComponent2.PlanogramComponentId = component2.Id;
            fixtureComponent2.Y = 50;
            fixture.Components.Add(fixtureComponent2);

            PlanogramSubComponent sub2 = PlanogramSubComponent.NewPlanogramSubComponent();
            sub2.Name = "Shelf part";
            sub2.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
            sub2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
            sub2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            sub2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            sub2.FillColourFront = -1;
            sub2.FillColourBack = -1;
            sub2.FillColourLeft = -1;
            sub2.FillColourRight = -1;
            sub2.FillColourTop = -1;
            sub2.FillColourBottom = -1;
            sub2.Width = component2.Width;
            sub2.Height = component2.Height;
            sub2.Depth = component2.Depth;
            component2.SubComponents.Add(sub2);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            //add a position
            PlanogramPosition pos1 = PlanogramPosition.NewPlanogramPosition(plan.Positions.GetNextSequence());
            pos1.PlanogramFixtureItemId = fixtureItem.Id;
            pos1.PlanogramFixtureComponentId = fixtureComponent2.Id;
            pos1.PlanogramSubComponentId = sub2.Id;
            pos1.PlanogramProductId = prod1.Id;
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;
            plan.Positions.Add(pos1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);


            //add an assembly
            PlanogramAssembly assembly1 = PlanogramAssembly.NewPlanogramAssembly();
            assembly1.Name = "Assembly1";
            plan.Assemblies.Add(assembly1);

            PlanogramFixtureAssembly fixtureAssembly1 = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
            fixtureAssembly1.PlanogramAssemblyId = assembly1.Id;
            fixtureAssembly1.Y = 70;
            fixture.Assemblies.Add(fixtureAssembly1);

            //add an assembly component
            PlanogramComponent component3 = PlanogramComponent.NewPlanogramComponent();
            component3.Name = "Shelf 2";
            component3.Width = fixture.Width;
            component3.Height = 4;
            component3.Depth = 75;
            plan.Components.Add(component3);

            PlanogramAssemblyComponent assemblyComponent1 = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
            assemblyComponent1.PlanogramComponentId = component3.Id;
            assemblyComponent1.Y = 50;
            assembly1.Components.Add(assemblyComponent1);

            PlanogramSubComponent sub3 = PlanogramSubComponent.NewPlanogramSubComponent();
            sub3.Name = "Shelf part";
            sub3.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
            sub3.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
            sub3.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            sub3.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            sub3.FillColourFront = -1;
            sub3.FillColourBack = -1;
            sub3.FillColourLeft = -1;
            sub3.FillColourRight = -1;
            sub3.FillColourTop = -1;
            sub3.FillColourBottom = -1;
            sub3.Width = component3.Width;
            sub3.Height = component3.Height;
            sub3.Depth = component3.Depth;
            component3.SubComponents.Add(sub3);


            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = assemblyComponent1.Id;
            anno2.PlanogramSubComponentId = sub3.Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);

            return plan;
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> ModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent" };

                foreach (PropertyInfo modelProperty in typeof(Planogram).GetProperties()
             .Where(p => p.DeclaringType == typeof(Planogram)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.Name.EndsWith("Async")
                        && !modelProperty.Name.StartsWith("Meta")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(Galleria.Framework.Model.IModelObject))
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion

        #region Constructors

        [Test]
        public void Constructor()
        {
            Planogram plan = CreatePlanogram();

            PlanogramView planView = new PlanogramView(plan);

            Assert.AreEqual(plan, planView.Model);

            //check counts
            Assert.AreEqual(plan.Products.Count, planView.Products.Count);
            Assert.AreEqual(plan.Positions.Count, planView.EnumerateAllPositions().Count());
            Assert.AreEqual(plan.Annotations.Count, planView.EnumerateAllAnnotations().Count());
            Assert.AreEqual(plan.FixtureItems.Count, planView.Fixtures.Count);

            Int32 expectedAssemblyCount = 0;
            Int32 expectedComponentCount = 0;
            Int32 expectedSubComponentCount = 0;

            foreach (var fi in plan.FixtureItems)
            {
                PlanogramFixture fx = plan.Fixtures.FindById(fi.PlanogramFixtureId);
                expectedAssemblyCount += fx.Assemblies.Count;
                expectedComponentCount += fx.Components.Count;

                foreach (var fa in fx.Assemblies)
                {
                    PlanogramAssembly assembly = plan.Assemblies.FindById(fa.PlanogramAssemblyId);
                    expectedComponentCount += assembly.Components.Count;

                    foreach(var ac in assembly.Components)
                    {
                        PlanogramComponent component =  plan.Components.FindById(ac.PlanogramComponentId);
                        expectedSubComponentCount += component.SubComponents.Count;
                    }
                }
                foreach (var fc in fx.Components)
                {
                    PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
                    expectedSubComponentCount += component.SubComponents.Count;
                }
            }

            Assert.AreEqual(expectedAssemblyCount, planView.EnumerateAllAssemblies().Count());
            Assert.AreEqual(expectedComponentCount, planView.EnumerateAllComponents().Count());
            Assert.AreEqual(expectedSubComponentCount, planView.EnumerateAllSubComponents().Count());
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("ModelPropertyNames")]
        public void ModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(Planogram).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramView view = CreateView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(view.Model, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(view.Model, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Object newValue2 = GetPropertyTestValue2(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue2, null);

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Inconclusive("Property missing from view - include it or exclude it.");
            }

        }

        #endregion

        #region Child Methods

        [Test]
        public void RemovePlanItem()
        {
            Planogram plan;
            PlanogramView planView;

            //Remove fixture
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.Fixtures.First();
            
            planView.RemovePlanItem(fixture);

            Assert.IsFalse(planView.Fixtures.Contains(fixture));
            Assert.IsTrue(fixture.FixtureItemModel.IsDeleted);


            //Remove Assembly
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramAssemblyView assembly = planView.EnumerateAllAssemblies().First();

            planView.RemovePlanItem(assembly);

            Assert.IsFalse(planView.EnumerateAllAssemblies().Contains(assembly));
            Assert.IsTrue(assembly.FixtureAssemblyModel.IsDeleted);


            //Remove assembly component
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramComponentView assemblyComp = planView.EnumerateAllAssemblies().First().Components[0];
            planView.RemovePlanItem(assemblyComp);

            Assert.IsFalse(planView.EnumerateAllComponents().Contains(assemblyComp));
            Assert.IsTrue(assemblyComp.AssemblyComponentModel.IsDeleted);

            //Remove fixture component
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramComponentView fixtureComp = planView.Fixtures[0].Components[0];
            planView.RemovePlanItem(fixtureComp);

            Assert.IsFalse(planView.EnumerateAllComponents().Contains(fixtureComp));
            Assert.IsTrue(fixtureComp.FixtureComponentModel.IsDeleted);


            //remove subcomponent
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramSubComponentView subComponent = planView.EnumerateAllSubComponents().Where(s => s.Positions.Count > 0).First();

            planView.RemovePlanItem(subComponent);

            Assert.IsFalse(planView.EnumerateAllSubComponents().Contains(subComponent));
            Assert.IsTrue(subComponent.Model.IsDeleted);
            Assert.IsFalse(plan.Positions.Any(p=> Object.Equals(p.PlanogramSubComponentId, subComponent.Model.Id)));

            //remove position
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramPositionView pos = planView.EnumerateAllPositions().First();
            PlanogramProduct productModel = pos.Product.Model;

            planView.RemovePlanItem(pos);

            Assert.IsFalse(planView.EnumerateAllPositions().Contains(pos));
            Assert.IsTrue(pos.Model.IsDeleted);
            Assert.IsFalse(productModel.IsDeleted);

            //remove product
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramProductView product = planView.Products.First();

            planView.RemovePlanItem(product);

            Assert.IsFalse(planView.Products.Contains(product));
            Assert.IsTrue(product.Model.IsDeleted);
            Assert.IsFalse(plan.Positions.Any(p=> Object.Equals(p.PlanogramProductId, product.Model.Id)));

            //remove annotation
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);
            PlanogramAnnotationView annotation = planView.EnumerateAllAnnotations().First();

            planView.RemovePlanItem(annotation);

            Assert.IsFalse(planView.EnumerateAllAnnotations().Contains(annotation));
            Assert.IsTrue(annotation.Model.IsDeleted);
        }

        /// <summary>
        /// Checks to see that the default value is not applied when the isDefault value checkbox is false but valeu applied for default
        /// </summary>
        [Test]
        public void UpdateProducts_AssertDefaultValueIsNotApplied()
        {
            const String cInitialDescription = "Anything is possible at Galleria.";
            const String cEndDescription = "Especially in 3d.";
            const String cPropertyNameSD = "ShortDescription";
            const String cPropertyNameBrand = "Brand";
            const String cPropertyNameFlavour = "Flavour";

            Planogram plan;
            PlanogramView planView;
            
            plan = CreatePlanogram();
            planView = new PlanogramView(plan);

            PlanogramProduct myTestProduct = planView.Products.First().Model;
            myTestProduct.ShortDescription = cInitialDescription;
            myTestProduct.Brand = cInitialDescription;
            myTestProduct.Flavour = cInitialDescription;

            BulkObservableCollection<ProductAttributeSelectorRow> AvailableAttributes = new BulkObservableCollection<ProductAttributeSelectorRow>();
            AvailableAttributes.AddRange(PlanogramProduct.
                EnumerateDisplayableFieldInfos(/*incMetadata*/false, false, false, includeAssortmentData: false).
                Select(f => new ProductAttributeSelectorRow(f)));
            
            ProductAttributeSelectorRow selectorShortDescription = AvailableAttributes.First(a => a.PropertyName == cPropertyNameSD);
            ProductAttributeSelectorRow selectorBrand = AvailableAttributes.First(a => a.PropertyName == cPropertyNameBrand);
            ProductAttributeSelectorRow selectorFlavour = AvailableAttributes.First(a => a.PropertyName == cPropertyNameFlavour);

            selectorShortDescription.IsDefaultValueApplied = false;
            selectorShortDescription.DefaultValue = cEndDescription;
            selectorBrand.IsDefaultValueApplied = false;
            selectorBrand.DefaultValue = cEndDescription;
            selectorFlavour.IsDefaultValueApplied = false;
            selectorFlavour.DefaultValue = cEndDescription;

            List<ProductLibraryProduct> libProducts = new List<ProductLibraryProduct>();
            BulkObservableCollection<ProductAttributeSelectorRow> adjustedSelectors = new BulkObservableCollection<ProductAttributeSelectorRow>();
            adjustedSelectors.Add(selectorShortDescription);

            planView.UpdateProducts(libProducts, adjustedSelectors);

            Assert.IsTrue(myTestProduct.ShortDescription == cInitialDescription);
            Assert.IsTrue(myTestProduct.Brand == cInitialDescription);
            Assert.IsTrue(myTestProduct.Flavour == cInitialDescription);
        }

        /// <summary>
        /// Checks to see that the value does actually get applied when is checked to do so.
        /// </summary>
        [Test]
        public void UpdateProducts_AssertDefaultValueIsApplied()
        {
            const String cInitialDescription = "Anything is possible at Galleria.";
            const String cEndDescription = "Especially in 3d.";
            const String cPropertyNameSD = "ShortDescription";
            const String cPropertyNameBrand = "Brand";
            const String cPropertyNameFlavour = "Flavour";

            Planogram plan;
            PlanogramView planView;

            plan = CreatePlanogram();
            planView = new PlanogramView(plan);

            PlanogramProduct myTestProduct = planView.Products.First().Model;
            myTestProduct.ShortDescription = cInitialDescription;
            myTestProduct.Brand = cInitialDescription;
            myTestProduct.Flavour = cInitialDescription;

            BulkObservableCollection<ProductAttributeSelectorRow> AvailableAttributes = new BulkObservableCollection<ProductAttributeSelectorRow>();
            AvailableAttributes.AddRange(PlanogramProduct.
                EnumerateDisplayableFieldInfos(/*incMetadata*/false, false, false, includeAssortmentData: false).
                Select(f => new ProductAttributeSelectorRow(f)));

            ProductAttributeSelectorRow selectorShortDescription = AvailableAttributes.First(a => a.PropertyName == cPropertyNameSD);
            ProductAttributeSelectorRow selectorBrand = AvailableAttributes.First(a => a.PropertyName == cPropertyNameBrand);
            ProductAttributeSelectorRow selectorFlavour = AvailableAttributes.First(a => a.PropertyName == cPropertyNameFlavour);

            selectorShortDescription.IsDefaultValueApplied = true;
            selectorShortDescription.DefaultValue = cEndDescription;
            selectorBrand.IsDefaultValueApplied = true;
            selectorBrand.DefaultValue = cEndDescription;
            selectorFlavour.IsDefaultValueApplied = true;
            selectorFlavour.DefaultValue = cEndDescription;

            List<ProductLibraryProduct> libProducts = new List<ProductLibraryProduct>();
            BulkObservableCollection<ProductAttributeSelectorRow> adjustedSelectors = new BulkObservableCollection<ProductAttributeSelectorRow>();
            adjustedSelectors.Add(selectorShortDescription);
            adjustedSelectors.Add(selectorBrand);
            adjustedSelectors.Add(selectorFlavour);

            planView.UpdateProducts(libProducts, adjustedSelectors);

            Assert.IsTrue(myTestProduct.ShortDescription == cEndDescription);
            Assert.IsTrue(myTestProduct.Brand == cEndDescription);
            Assert.IsTrue(myTestProduct.Flavour == cEndDescription);
        }

        /// <summary>
        /// Checks to see that a default value does NOT override the prodduct library value when it exists.
        /// </summary>
        [Test]
        public void UpdateProducts_AssertDefaultValueDoesNotOverwriteProductLibraryValue()
        {
            const String cInitialDescription = "Anything is possible at Galleria.";
            const String cEndDescription = "Especially in 3d.";
            const String cPropertyName = "ShortDescription";
            const String cExampleShortDescription = "Who uses short description anyway?";

            Planogram plan;
            PlanogramView planView;

            plan = CreatePlanogram();
            planView = new PlanogramView(plan);

            PlanogramProduct myTestProduct = planView.Products.First().Model;
            myTestProduct.ShortDescription = cInitialDescription;

            BulkObservableCollection<ProductAttributeSelectorRow> AvailableAttributes = new BulkObservableCollection<ProductAttributeSelectorRow>();
            AvailableAttributes.AddRange(PlanogramProduct.
                EnumerateDisplayableFieldInfos(/*incMetadata*/false, false, false, includeAssortmentData: false).
                Select(f => new ProductAttributeSelectorRow(f)));

            ProductAttributeSelectorRow selectorShortDescription = AvailableAttributes.First(a => a.PropertyName == cPropertyName);
            selectorShortDescription.IsDefaultValueApplied = true;
            selectorShortDescription.DefaultValue = cEndDescription;

            Product tempProduct = Product.NewProduct();
            List<ProductLibraryProduct> libProducts = new List<ProductLibraryProduct>();
            ProductLibraryProduct productLibraryProduct = ProductLibraryProduct.NewProductLibraryProduct(tempProduct);

            //Update matches on gtin, so set, and then include relevant property.
            
            productLibraryProduct.Product.Gtin = myTestProduct.Gtin;
            productLibraryProduct.Product.Name = "Galleria's  Example Product Library Product ";
            productLibraryProduct.Product.ShortDescription = cExampleShortDescription;

            libProducts.Add(productLibraryProduct);

            BulkObservableCollection<ProductAttributeSelectorRow> adjustedSelectors = new BulkObservableCollection<ProductAttributeSelectorRow>();
            adjustedSelectors.Add(selectorShortDescription);

            planView.UpdateProducts(libProducts, adjustedSelectors);

            Assert.IsTrue(myTestProduct.ShortDescription == cExampleShortDescription);
        }
        #endregion

        
    }
}