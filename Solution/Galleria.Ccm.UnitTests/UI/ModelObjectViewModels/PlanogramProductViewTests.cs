﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class PlanogramProductViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramProductView CreateView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);



            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.Products.First();
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> ProductModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent","Model"};

                foreach (PropertyInfo modelProperty in typeof(PlanogramProduct).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramProduct)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.Name.Contains("Image")
                        && !modelProperty.Name.EndsWith("Async")
                        && !modelProperty.Name.StartsWith("Meta")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        public IEnumerable<String> ProductImagePropertyNames
        {
            get
            {
                yield return "FrontImage";
                yield return "BackImage";
                yield return "TopImage";
                yield return "BottomImage";
                yield return "LeftImage";
                yield return "RightImage";

                yield return "FrontDisplayImage";
                yield return "BackDisplayImage";
                yield return "TopDisplayImage";
                yield return "LeftDisplayImage";
                yield return "RightDisplayImage";

                yield return "FrontTrayImage";
                yield return "BackTrayImage";
                yield return "TopTrayImage";
                yield return "LeftTrayImage";
                yield return "RightTrayImage";

                yield return "FrontPointOfPurchaseImage";
                yield return "BackPointOfPurchaseImage";
                yield return "TopPointOfPurchaseImage";
                yield return "LeftPointOfPurchaseImage";
                yield return "RightPointOfPurchaseImage";

                yield return "FrontAlternateImage";
                yield return "BackAlternateImage";
                yield return "TopAlternateImage";
                yield return "LeftAlternateImage";
                yield return "RightAlternateImage";

                yield return "FrontCaseImage";
                yield return "BackCaseImage";
                yield return "TopCaseImage";
                yield return "LeftCaseImage";
                yield return "RightCaseImage";
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("ProductModelPropertyNames")]
        public void ProductModelPropertyGetSet(String propertyName)
        {

            PropertyInfo modelProperty = typeof(PlanogramProduct).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramProductView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramProductView view = CreateView();
                view.Model.Height = 10;
                view.Model.Width = 10;
                view.Model.Depth = 10;

                view.PropertyChanged += base.TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(view.Model, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(view.Model, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }


        [Test, TestCaseSource("ProductImagePropertyNames")]
        public void ProductImagePropertiesGetSet(String propertyName)
        {
            PropertyInfo viewProperty = typeof(PlanogramProductView).GetProperty(propertyName);
            Assert.IsNotNull(viewProperty);

            PlanogramProductView view = CreateView();
            view.PropertyChanged += base.TestModel_PropertyChanged;

            

            PlanogramImage testImage = view.Planogram.AddPlanogramImage(new Byte[] { }, "img", "imgdesc");

            base.PropertyChangedNotifications.Clear();
            viewProperty.SetValue(view, testImage, null);

            Assert.AreEqual(testImage, viewProperty.GetValue(view, null));

            Assert.Contains(propertyName, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", propertyName));


            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void MetaTotalUnits()
        {
            Package package = TestDataHelper.CreatePlanCreweSparklingWater();

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = packageView.PlanogramViews.First();
            
            PlanogramProductView prodView = planView.Products.First();
            prodView.PropertyChanged += base.TestModel_PropertyChanged;

            //check that the value was initialized.
            Assert.AreEqual(prodView.Model.GetPlanogramPositions().Sum(p => p.TotalUnits), prodView.MetaTotalUnits);

            var p1 = prodView.Model.GetPlanogramPositions().First();

            //add a unit to the first position
            Int32? preVal = prodView.MetaTotalUnits;
            base.PropertyChangedNotifications.Clear();
            p1.FacingsWide += 1;
            Assert.Contains(PlanogramProduct.MetaTotalUnitsProperty.Name, base.PropertyChangedNotifications, "Property changed not fired");
            Assert.AreEqual(preVal + (1 * p1.FacingsHigh * p1.FacingsDeep), prodView.MetaTotalUnits, "Not updated for added units");

            //remove a unit from the first position
            preVal = prodView.MetaTotalUnits;
            base.PropertyChangedNotifications.Clear();
            p1.FacingsWide -= 1;
            Assert.Contains(PlanogramProduct.MetaTotalUnitsProperty.Name, base.PropertyChangedNotifications, "Property changed not fired");
            Assert.AreEqual(preVal - (1 * p1.FacingsHigh * p1.FacingsDeep), prodView.MetaTotalUnits, "Not updated for removed units");

            //add on a whole new position for the product
            p1.FacingsWide = 1;
            preVal = prodView.MetaTotalUnits;
            base.PropertyChangedNotifications.Clear();

            var p2 = planView.EnumerateMerchandisableSubComponents().First().AddPosition(prodView);
            Assert.Contains(PlanogramProduct.MetaTotalUnitsProperty.Name, base.PropertyChangedNotifications, "Property changed not fired");
            Assert.AreEqual(preVal + p2.TotalUnits, prodView.MetaTotalUnits, "Not updated for added position");

            //remove the position
            preVal = prodView.MetaTotalUnits;
            base.PropertyChangedNotifications.Clear();

            planView.EnumerateMerchandisableSubComponents().First().RemovePosition(p2);
            Assert.Contains(PlanogramProduct.MetaTotalUnitsProperty.Name, base.PropertyChangedNotifications, "Property changed not fired");
            Assert.AreEqual(preVal - p2.TotalUnits, prodView.MetaTotalUnits, "Not updated for removed position");

            //add a new product with no positions
            var newProd = planView.AddProduct();
            Assert.AreEqual(0, newProd.MetaTotalUnits);
        }

        #endregion

        #region Methods

        [Test]
        public void Constructor()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = packageView.PlanogramViews.First();
            PlanogramProductView view = planView.Products[0];

            Assert.AreEqual(prod1, view.Model);
        }


        [Test]
        public void StoreExternalImages_WhenNoProvider_ShouldNotThrow()
        {
            PlanogramImagesHelper.RegisterRealImageProvider(null);

            TestDelegate code = () => ProductImageHelper.StoreExternalImages(null);

            Assert.DoesNotThrow(code, "StoreExternalImages should not throw any exceptions if there is no provider.");
        }

        #endregion


        [Test, Category(Categories.ClientRaised)]
        public void CopyPasteTakesPerformanceData()
        {
            //Client raised V8-32296
            //Ensures that products copied between plans take perforance data.

            //open two plans.
            var controller1 = base.OpenSamplePlan(SamplePlans.Cereals3Bay);
            var controller2 = base.OpenSamplePlan(SamplePlans.FreshMeat1Bay);

            //select product on cereals
            var pos1 = controller1.SourcePlanogram.EnumerateAllPositions().First();
            controller1.SelectedPlanItems.SetSelection(pos1);
            App.MainPageViewModel.ActivePlanController = controller1;
            MainPageCommands.Copy.Execute();

            Single p1Value = 5;
            pos1.Product.PerformanceData.P1 = p1Value;


            App.MainPageViewModel.ActivePlanController = controller2;
            var shelf1 = controller2.SourcePlanogram.EnumerateAllComponents().FirstOrDefault(c=> c.ComponentType == PlanogramComponentType.Shelf);
            controller2.SelectedPlanItems.SetSelection(shelf1);
            MainPageCommands.Paste.Execute();


            //check the performance value was copied
            var prod = controller2.SourcePlanogram.Products.FirstOrDefault(p => p.Gtin == pos1.Product.Gtin);
            Assert.AreEqual(p1Value, prod.PerformanceData.P1.Value, "P1 value should have been copied");
         }
    }
}
