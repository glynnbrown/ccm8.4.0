﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015
#region Version History: (CCM 8.30)
// V8- : L.Ineson
//  Created
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI
{
    /// <summary>
    /// Contains tests to confirm that the planogram view undo redo behaviour is working correctly.
    /// </summary>
    public sealed class UndoRedoBehaviourTests : TestBase
    {
        #region Test Fixture Helpers

        private PlanogramView CreateView()
        {
            var controller =base.OpenSamplePlan(SamplePlans.Cereals3Bay);
            return controller.SourcePlanogram;
        }

        #endregion

        #region Change Single Property

        [Test]
        public void SinglePropertyChange_Planogram()
        {
            PlanogramView view = CreateView();

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.Name = val1;

            Int32 undoCount = view.UndoActions.Count;

            //change the property
            view.Name = val2;

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);

            //undo it
            view.Undo();

            Assert.AreEqual(val1, view.Name);
            Assert.AreEqual(undoCount, view.UndoActions.Count);
            Assert.AreEqual(1, view.RedoActions.Count);

            //redo it
            view.Redo();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);
        }

        [Test]
        public void SinglePropertyChange_Fixture()
        {
            var planView = CreateView();
            PlanogramFixtureView view = planView.Fixtures[1];

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.Name = val1;

            //create the undo action
            Int32 undoCount = planView.UndoActions.Count;
            view.Name = val2;

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);

            //undo it
            planView.Undo();

            Assert.AreEqual(val1, view.Name);
            Assert.AreEqual(undoCount, planView.UndoActions.Count);
            Assert.AreEqual(1, planView.RedoActions.Count);

            //redo it
            planView.Redo();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);
        }

        [Test]
        public void SinglePropertyChange_Assembly()
        {
            PlanogramView planView =  CreateView();
            
            //group 2 components
            var controller = App.MainPageViewModel.ActivePlanController;
            controller.SelectedPlanItems.SetSelection(
                new List<IPlanItem>{
                    planView.EnumerateAllComponents().Where(c=> c.ComponentType == PlanogramComponentType.Shelf).ElementAt(0),
                    planView.EnumerateAllComponents().Where(c=> c.ComponentType == PlanogramComponentType.Shelf).ElementAt(1)
                });
            MainPageCommands.CreateAssembly.Execute();

            PlanogramAssemblyView view = planView.EnumerateAllAssemblies().First();
            

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.Name = val1;

            //create the undo action
            Int32 undoCount = planView.UndoActions.Count;
            view.Name = val2;

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);

            //undo it
            planView.Undo();

            Assert.AreEqual(val1, view.Name);
            Assert.AreEqual(undoCount, planView.UndoActions.Count);
            Assert.AreEqual(1, planView.RedoActions.Count);

            //redo it
            planView.Redo();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count); 
        }

        [Test]
        public void SinglePropertyChange_Component()
        {
            PlanogramView planView = CreateView();
            PlanogramComponentView view = planView.EnumerateAllFixtureComponents().First();

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.Name = val1;

            //create the undo action
            Int32 undoCount = planView.UndoActions.Count;
            view.Name = val2;


            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);

            //undo it
            planView.Undo();

            Assert.AreEqual(val1, view.Name);
            Assert.AreEqual(undoCount, planView.UndoActions.Count);
            Assert.AreEqual(1, planView.RedoActions.Count);

            //redo it
            planView.Redo();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);
        }

        [Test]
        public void SinglePropertyChange_Position()
        {
            PlanogramView planView =  CreateView();
            PlanogramPositionView view = planView.EnumerateAllPositions().First();
            

            const PlanogramPositionOrientationType val1 = PlanogramPositionOrientationType.Default;
            const PlanogramPositionOrientationType val2 = PlanogramPositionOrientationType.Front180;

            view.OrientationType = val1;

            //create the undo action
            Int32 undoCount = planView.UndoActions.Count;
            view.OrientationType = val2;

            Assert.AreEqual(val2, view.OrientationType);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);

            //undo it
            planView.Undo();

            Assert.AreEqual(val1, view.OrientationType);
            Assert.AreEqual(undoCount, planView.UndoActions.Count);
            Assert.AreEqual(1, planView.RedoActions.Count);

            //redo it
            planView.Redo();

            Assert.AreEqual(val2, view.OrientationType);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);
        }

        [Test]
        public void SinglePropertyChange_Product()
        {
            PlanogramView planView =CreateView();
            PlanogramProductView view = planView.Products.First();
            

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.Name = val1;

            //create the undo action
            Int32 undoCount = planView.UndoActions.Count;
            view.Name = val2;

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);

            //undo it
            planView.Undo();

            Assert.AreEqual(val1, view.Name);
            Assert.AreEqual(undoCount, planView.UndoActions.Count);
            Assert.AreEqual(1, planView.RedoActions.Count);

            //redo it
            planView.Redo();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);
        }

        [Test]
        public void SinglePropertyChange_ProductPerformanceDataProperty()
        {
            //V8-30519 - Makes sure that undo redo works for performance data properties.

            PlanogramView planView = CreateView();
            PlanogramProductView view = planView.Products.First();

            Single val1 = 2;
            Single val2 = 23.5F;

            view.PerformanceData.P1 = val1;

            //create the undo action
            Int32 undoCount = planView.UndoActions.Count;
            view.PerformanceData.P1 = val2;

            Assert.AreEqual(val2, view.PerformanceData.P1);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);

            //undo it
            planView.Undo();

            Assert.AreEqual(val1, view.PerformanceData.P1);
            Assert.AreEqual(undoCount, planView.UndoActions.Count);
            Assert.AreEqual(1, planView.RedoActions.Count);

            //redo it
            planView.Redo();

            Assert.AreEqual(val2, view.PerformanceData.P1);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);
        }

        //[Test]
        //public void SinglePropertyChange_Annotation()
        //{
        //    Assert.Ignore();

        //    PlanogramView planView = CreateView();
        //    PlanogramAnnotationView view = planView.Fixtures[0].AddAnnotation();
        //    planView.ClearAllUndoRedoActions();

        //    String val1 = "OriginalVal";
        //    String val2 = "NewVal";

        //    view.Text = val1;

        //    //create the undo action
        //    Int32 undoCount = planView.UndoActions.Count;
        //    view.Text = val2;

        //    Assert.AreEqual(val2, view.Text);
        //    Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
        //    Assert.AreEqual(0, planView.RedoActions.Count);

        //    //undo it
        //    planView.Undo();

        //    Assert.AreEqual(val1, view.Text);
        //    Assert.AreEqual(undoCount, planView.UndoActions.Count);
        //    Assert.AreEqual(1, planView.RedoActions.Count);

        //    //redo it
        //    planView.Redo();

        //    Assert.AreEqual(val2, view.Text);
        //    Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
        //    Assert.AreEqual(0, planView.RedoActions.Count);
        //}

        [Test]
        public void SinglePropertyChange_SubComponent()
        {
            PlanogramView planView = CreateView();
            PlanogramSubComponentView view = planView.EnumerateAllSubComponents().FirstOrDefault();

            String val1 = "OriginalVal";
            String val2 = "NewVal";

            view.Name = val1;

            //create the undo action
            Int32 undoCount = planView.UndoActions.Count;
            view.Name = val2;


            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);


            //undo it
            planView.Undo();

            Assert.AreEqual(val1, view.Name);
            Assert.AreEqual(undoCount, planView.UndoActions.Count);
            Assert.AreEqual(1, planView.RedoActions.Count);

            //redo it
            planView.Redo();

            Assert.AreEqual(val2, view.Name);
            Assert.AreEqual(undoCount + 1, planView.UndoActions.Count);
            Assert.AreEqual(0, planView.RedoActions.Count);
        }

        #endregion

        #region AddRemove children

        [Test]
        public void AddChild_Fixture()
        {
            PlanogramView view = CreateView();

            Int32 preCount = view.Fixtures.Count;

            //create the undo action
            Assert.AreEqual(0, view.UndoActions.Count, "Should start with no actions.");

            view.BeginUndoableAction();
            view.AddFixture();
            view.EndUndoableAction();

            Assert.AreEqual(preCount + 1, view.Fixtures.Count);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);

            //undo it
            view.Undo();

            Assert.AreEqual(preCount, view.Fixtures.Count);
            Assert.AreEqual(0, view.UndoActions.Count);
            Assert.AreEqual(1, view.RedoActions.Count);

            //redo it
            view.Redo();

            Assert.AreEqual(preCount + 1, view.Fixtures.Count);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);
        }

        [Test]
        public void RemoveChild_Fixture()
        {
            PlanogramView view = CreateView();

            Int32 preCount = view.Fixtures.Count;

            //create the undo action
            Assert.AreEqual(0, view.UndoActions.Count, "Should start with no actions.");

            var fixtureToRemove = view.Fixtures.ElementAt(1);
            Int16 seq = fixtureToRemove.BaySequenceNumber;
            Single[] fixtureXs = view.Fixtures.OrderBy(f => f.BaySequenceNumber).Select(f => f.X).ToArray();

            //view.BeginUndoableAction();
            view.RemovePlanItem(fixtureToRemove);
            Assert.AreEqual(1, view.UndoActions.Count, "Should  have created 1 action only.");
            //view.EndUndoableAction();

            Assert.AreEqual(preCount - 1, view.Fixtures.Count);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);

            //undo it
            view.Undo();
            Assert.AreEqual(preCount, view.Fixtures.Count);
            Assert.AreEqual(0, view.UndoActions.Count);
            Assert.AreEqual(1, view.RedoActions.Count);

            //Assert.AreEqual(seq, view.Fixtures.ElementAt(1).BaySequenceNumber);
            //Assert.IsTrue(fixtureXs.SequenceEqual(view.Fixtures.OrderBy(f => f.BaySequenceNumber).Select(f => f.X).ToArray()));


            //redo it
            view.Redo();
            Assert.AreEqual(preCount - 1, view.Fixtures.Count);
            Assert.AreEqual(1, view.UndoActions.Count);
            Assert.AreEqual(0, view.RedoActions.Count);
        }

        [Test]
        public void AddChild_FixtureComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            var pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Components.Count);

            //Add a new component using view
            Assert.AreEqual(0, fixtureView.Components.Count);
            PlanogramComponentView view1 = fixtureView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, fixtureView.Components.Count);


            //undo
            planView.Undo();
            Assert.AreEqual(0, fixtureView.Components.Count);

            //check
            planView.Redo();
            Assert.AreEqual(1, fixtureView.Components.Count);
        }

        [Test]
        public void RemoveChild_FixtureComponent()
        {
            var controller = base.OpenSamplePlan(SamplePlans.Cereals1Bay);
            var planview = controller.SourcePlanogram;

            var shelf1 = planview.EnumerateAllFixtureComponents().FirstOrDefault(f => f.ComponentType == PlanogramComponentType.Shelf);
            Int32 positionCount = shelf1.SubComponents[0].Positions.Count;
            Assert.AreNotEqual(0, positionCount, "Start shelf should have positions.");

            //start with no actions
            planview.ClearAllUndoRedoActions();
            Assert.AreEqual(0, planview.UndoActions.Count, "Start with no actions");
            Assert.AreEqual(0, planview.RedoActions.Count, "Start with no actions");

            //remove it
            var parentFixture = shelf1.Fixture;
            Int32 shelfCount = parentFixture.Components.Count;

            parentFixture.RemoveComponent(shelf1);
            Assert.AreEqual(shelfCount - 1, parentFixture.Components.Count, "Should have 1 less shelf.");

            //undo
            Assert.AreEqual(1, planview.UndoActions.Count, "Should have 1 undo action");
            planview.Undo();
            Assert.AreEqual(shelfCount, parentFixture.Components.Count, "Should have 1 less shelf.");
            Assert.AreEqual(shelf1.ComponentModel, parentFixture.Components.Last().ComponentModel, "Component model should match.");

            //check positions
            Int32 newPositionCount = parentFixture.Components.Last().SubComponents[0].Positions.Count;
            Assert.AreEqual(positionCount, newPositionCount);

            //redo
            Assert.AreEqual(0, planview.UndoActions.Count, "Should have no undo action");
            Assert.AreEqual(1, planview.RedoActions.Count, "Should have 1 redo action");
            planview.Redo();
            Assert.AreEqual(shelfCount - 1, parentFixture.Components.Count, "Should have 1 less shelf.");
            Assert.AreEqual(1, planview.UndoActions.Count, "Should have 1 undo action");
            Assert.AreEqual(0, planview.RedoActions.Count, "Should have no redo action");
        }

        [Test]
        public void AddChild_FixtureAssembly()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            var pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Assemblies.Count);

            //Add a new assembly using view
            PlanogramAssemblyView view1 = fixtureView.AddAssembly();
            Assert.AreEqual(1, fixtureView.Assemblies.Count);

            //undo
            planView.Undo();
            Assert.AreEqual(0, fixtureView.Assemblies.Count);

            //redo
            planView.Redo();
            Assert.AreEqual(1, fixtureView.Assemblies.Count);
        }

        //[Test]
        //public void AddChild_FixtureAssembly()
        //{
        //    Assert.Ignore("TODO");
        //}

        [Test]
        public void AddChild_AssemblyComponent()
        {
            //Create a plan with an assembly
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramAssemblyView assemblyView = planView.EnumerateAllAssemblies().First(a => a.FixtureAssemblyModel == fixtureAssembly);

            Assert.AreEqual(0, assemblyView.Components.Count);

            //add a new component using the view method
            PlanogramComponentView view = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, assemblyView.Components.Count);
            Assert.AreEqual(PlanogramComponentType.Shelf, assemblyView.Components[0].ComponentType);

            //undo
            planView.Undo();
            Assert.AreEqual(0, assemblyView.Components.Count);

            //redo
            planView.Redo();
            Assert.AreEqual(1, assemblyView.Components.Count);
        }

        //[Test]
        //public void RemoveChild_AssemblyComponent()
        //{
        //    Assert.Ignore("TODO");
        //}

        //[Test]
        //public void AddRemoveChild_Product()
        //{
        //    Assert.Ignore("TODO");
        //}

        [Test]
        public void AddChild_Position()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                sub, fixtureItem, fc);

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);


            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            Assert.AreEqual(0, subView.Positions.Count);

            //add new view
            PlanogramPositionView view1 = subView.AddPosition(planView.Products[0]);
            Assert.AreEqual(1, subView.Positions.Count);
            Assert.AreEqual(planView.Products[0], view1.Product);

            //undo
            planView.Undo();
            Assert.AreEqual(0, subView.Positions.Count);

            //redo
            planView.Redo();
            Assert.AreEqual(1, subView.Positions.Count);
        }

        //[Test]
        //public void RemoveChild_Position()
        //{
        //    Assert.Ignore("TODO");
        //}

        [Test]
        public void AddChild_SubComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 2, 2, 2);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramComponentView componentView = planView.Fixtures[0].Components[0];

            Assert.AreEqual(1, componentView.SubComponents.Count);

            //Add new view
            PlanogramSubComponentView view1 = componentView.AddSubComponent();
            Assert.AreEqual(2, componentView.SubComponents.Count);

            //undo 
            planView.Undo();
            Assert.AreEqual(1, componentView.SubComponents.Count);

            //redo
            planView.Redo();
            Assert.AreEqual(2, componentView.SubComponents.Count);
        }

        //[Test]
        //public void RemoveChild_SubComponent()
        //{
        //    Assert.Ignore("TODO");
        //}

        [Test]
        public void AddChild_FixtureAnnotation()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            var pkView = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Annotations.Count);

            //add an annotation
            PlanogramAnnotationView view1 = fixtureView.AddAnnotation();
            Assert.AreEqual(1, fixtureView.Annotations.Count);

            //undo the add
            planView.Undo();
            Assert.AreEqual(0, fixtureView.Annotations.Count);

            //redo the add
            planView.Redo();
            Assert.AreEqual(1, fixtureView.Annotations.Count);
        }

        //[Test]
        //public void RemoveChild_FixtureAnnotation()
        //{
        //    Assert.Ignore("TODO");
        //}

        #endregion

        #region Specific Tests

        [Test]
        public void CheckSinglePropertyRecording()
        {
            PlanogramView planView = CreateView();
            PlanogramSubComponentView sub1 = planView.EnumerateMerchandisableSubComponents().First();

            //get initial value
            PlanogramSubComponentXMerchStrategyType oldValue = sub1.MerchandisingStrategyX;
            Assert.AreNotEqual(PlanogramSubComponentXMerchStrategyType.Right, oldValue);

            //change it
            sub1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Right;
            Assert.AreEqual(PlanogramSubComponentXMerchStrategyType.Right, sub1.MerchandisingStrategyX);

            //undo
            Assert.AreEqual(1, planView.UndoActions.Count, "Only 1 action should exist - even though the change might have caused a reprocess");
            planView.Undo();

            //check
            Assert.AreEqual(oldValue, sub1.MerchandisingStrategyX, "Change should have been undone");
            Assert.AreEqual(0, planView.UndoActions.Count, "No actions should now exist.");
        }

        [Test]
        public void CheckMultiPropertyRecording()
        {
            PlanogramView planView = CreateView();
            PlanogramSubComponentView sub1 = planView.EnumerateMerchandisableSubComponents().First();

            //get 2 initial values
            PlanogramSubComponentXMerchStrategyType oldValue1 = sub1.MerchandisingStrategyX;
            Single oldValue2 = sub1.MerchandisableHeight;

            //start recording
            planView.BeginUndoableAction();
            sub1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Right;
            sub1.MerchandisableHeight = 22;
            planView.EndUndoableAction();

            //Undo
            Assert.AreEqual(1, planView.UndoActions.Count, "Only 1 action should exist");
            planView.Undo();

            //Check
            Assert.AreEqual(oldValue1, sub1.MerchandisingStrategyX, "Change should have been undone");
            Assert.AreEqual(oldValue2, sub1.MerchandisableHeight, "Change should have been undone");
            Assert.AreEqual(0, planView.UndoActions.Count, "No actions should now exist.");

        }

        //[Test]
        //public void CheckMovePosition()
        //{
        //    Assert.Ignore("TODO");
        //}

        [Test]
        public void CopyPasteBay()
        {
            //G32115
            var planController = base.AddNewEmptyPlan();
            var planView = planController.SourcePlanogram;

            Assert.AreEqual(1, planView.Fixtures.Count);
            
                
            //copy paste the default bay
            planController.SelectedPlanItems.SetSelection(planView.Fixtures[0]);
            Assert.AreEqual(2, planView.Fixtures[0].Components.Count);
            MainPageCommands.Copy.Execute();
            MainPageCommands.Paste.Execute();
            Assert.AreEqual(2, planView.Fixtures.Count, "copy paste failed");
            Assert.AreEqual(2, planView.Fixtures[1].Components.Count);

            //undo
            planView.Undo();
            Assert.AreEqual(1, planView.Fixtures.Count, "undo failed");

            //redo
            planView.Redo();
            Assert.AreEqual(2, planView.Fixtures.Count, "redo failed");
            Assert.AreEqual(2, planView.Fixtures[1].Components.Count, "redo failed");
        }

        #endregion


    }
}
