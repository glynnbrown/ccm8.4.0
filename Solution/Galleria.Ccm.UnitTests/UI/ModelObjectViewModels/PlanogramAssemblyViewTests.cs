﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class PlanogramAssemblyViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramAssemblyView CreateView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC =
                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);


            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllAssemblies().First();
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> AssemblyModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent","Height", "Width", "Depth", "NumberOfComponents" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramAssembly).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramAssembly)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.StartsWith("Meta")) continue; //ignore meta properties

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> FixtureAssemblyModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramFixtureAssembly).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramFixtureAssembly)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.StartsWith("Meta")) continue; //ignore meta properties

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("AssemblyModelPropertyNames")]
        public void AssemblyModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramAssembly).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramAssemblyView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramAssemblyView view = CreateView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(view.AssemblyModel, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.GetSetMethod(false) != null)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(view.AssemblyModel, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                String[] notInUse = new String[]
                {
                    PlanogramAssembly.TotalComponentCostProperty.Name
                };
                if (notInUse.Contains(propertyName)) Assert.Inconclusive("Not in use");

                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }

        [Test, TestCaseSource("FixtureAssemblyModelPropertyNames")]
        public void FixtureAssemblyModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramFixtureAssembly).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramAssemblyView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramAssemblyView view = CreateView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                PlanogramFixtureAssembly model = view.FixtureAssemblyModel;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(model, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.GetSetMethod(false) != null)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(model, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }

        [Test]
        public void WorldSpaceProperties_AreInitialized()
        {
            var view = CreateView();

            PointValue expectedValue =
                view.FixtureAssemblyModel.GetPlanogramRelativeCoordinates(view.Fixture.FixtureItemModel);

            RotationValue expectedRotation =
                view.FixtureAssemblyModel.GetPlanogramRelativeRotation(view.Fixture.FixtureItemModel);


            Assert.AreEqual(expectedValue.X, view.WorldX);
            Assert.AreEqual(expectedValue.Y, view.WorldY);
            Assert.AreEqual(expectedValue.Z, view.WorldZ);

            Assert.AreEqual(expectedRotation.Slope, view.WorldSlope);
            Assert.AreEqual(expectedRotation.Angle, view.WorldAngle);
            Assert.AreEqual(expectedRotation.Roll, view.WorldRoll);
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByFixtureMove()
        {
            var view = CreateView();
            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the fixture
            view.Fixture.X += 10;

            Assert.Contains("WorldX", base.PropertyChangedNotifications);
            Assert.Contains("WorldY", base.PropertyChangedNotifications);
            Assert.Contains("WorldZ", base.PropertyChangedNotifications);

            Assert.Contains("WorldSlope", base.PropertyChangedNotifications);
            Assert.Contains("WorldAngle", base.PropertyChangedNotifications);
            Assert.Contains("WorldRoll", base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByAssemblyMove()
        {
            var view = CreateView();

            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the assembly
            view.X += 10;

            Assert.Contains("WorldX", base.PropertyChangedNotifications);
            Assert.Contains("WorldY", base.PropertyChangedNotifications);
            Assert.Contains("WorldZ", base.PropertyChangedNotifications);

            Assert.Contains("WorldSlope", base.PropertyChangedNotifications);
            Assert.Contains("WorldAngle", base.PropertyChangedNotifications);
            Assert.Contains("WorldRoll", base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region Methods

        [Test]
        public void Constructor()
        {
            //Create a plan with an assembly
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);
            PlanogramAssemblyComponent ac1 = assembly.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramAssemblyComponent ac2 = assembly.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramAssemblyView assemblyView = planView.EnumerateAllAssemblies().First(a => a.FixtureAssemblyModel == fixtureAssembly);
            
            //check the assembly model.
            Assert.AreEqual(assembly, assemblyView.AssemblyModel);

            //check components have loaded.
            Assert.AreEqual(2, assemblyView.Components.Count);
            Assert.AreEqual(ac1, assemblyView.Components[0].AssemblyComponentModel);
            Assert.AreEqual(ac2, assemblyView.Components[1].AssemblyComponentModel);

            //check i plan item values
            Assert.AreEqual(planView, assemblyView.Planogram);
            Assert.AreEqual(fixtureItem, assemblyView.Fixture.FixtureItemModel);
        }

        [Test]
        public void AddNewComponent()
        {
            //Create a plan with an assembly
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramAssemblyView assemblyView = planView.EnumerateAllAssemblies().First(a => a.FixtureAssemblyModel == fixtureAssembly);

            Assert.AreEqual(0, assemblyView.Components.Count);

            //add a new component using the view method
            PlanogramComponentView view = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, assemblyView.Components.Count);
            Assert.AreEqual(PlanogramComponentType.Shelf, assemblyView.Components[0].ComponentType);

            //add a new component via the model
            PlanogramAssemblyComponent ac2 = assembly.Components.Add(PlanogramComponentType.Peg, 120, 4, 4);
            Assert.AreEqual(2, assemblyView.Components.Count);
            Assert.AreEqual(PlanogramComponentType.Peg, assemblyView.Components[1].ComponentType);
        }

        [Test]
        public void RemoveComponent()
        {
            //Create a plan with an assembly
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramAssemblyView assemblyView = planView.EnumerateAllAssemblies().First(a => a.FixtureAssemblyModel == fixtureAssembly);

            PlanogramComponentView view = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            PlanogramComponentView view2 = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(2, assemblyView.Components.Count);


            //remove view 1 via the method
            assemblyView.RemoveComponent(view);
            Assert.AreEqual(1, assemblyView.Components.Count);
            Assert.Contains(view2, assemblyView.Components);

            //remove view 2 via the model
            assembly.Components.Remove(view2.AssemblyComponentModel);
            Assert.AreEqual(0, assemblyView.Components.Count);
        }

        [Test]
        public void AddComponentCopyOfAssemblyComponent()
        {
            //Create a plan with an assembly
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramAssemblyView assemblyView = planView.EnumerateAllAssemblies().First(a => a.FixtureAssemblyModel == fixtureAssembly);

            Assert.AreEqual(0, assemblyView.Components.Count);

            //add a new component using the view method
            PlanogramComponentView view = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, assemblyView.Components.Count);
            Assert.AreEqual(PlanogramComponentType.Shelf, assemblyView.Components[0].ComponentType);

            //add a copy
            PlanogramComponentView viewCopy = assemblyView.AddComponentCopy(view);
            Assert.AreEqual(2, assemblyView.Components.Count);
            Assert.AreEqual(PlanogramComponentType.Shelf, assemblyView.Components[1].ComponentType);
            Assert.AreEqual(view.SubComponents.Count, viewCopy.SubComponents.Count);
        }

        [Test]
        public void AddComponentCopyOfFixtureComponent()
        {
            //Create a plan with an assembly
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramAssemblyView assemblyView = planView.EnumerateAllAssemblies().First(a => a.FixtureAssemblyModel == fixtureAssembly);

            Assert.AreEqual(0, assemblyView.Components.Count);

            //add a new fixture component using the view method
            PlanogramComponentView view = assemblyView.Fixture.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(0, assemblyView.Components.Count);
            Assert.AreEqual(1, assemblyView.Fixture.Components.Count);

            //add a copy
            PlanogramComponentView viewCopy = assemblyView.AddComponentCopy(view);
            Assert.AreEqual(1, assemblyView.Components.Count);
            Assert.AreEqual(PlanogramComponentType.Shelf, assemblyView.Components[0].ComponentType);
            Assert.AreEqual(view.SubComponents.Count, viewCopy.SubComponents.Count);
        }



        #endregion

        #region IPlanAssemblyRenderable

        [Test]
        public void IPlanAssemblyRenderableComponents()
        {
            //Create a plan with an assembly
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);
            PlanogramAssemblyComponent ac1 = assembly.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramAssemblyComponent ac2 = assembly.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramAssemblyView assemblyView = planView.EnumerateAllAssemblies().First(a => a.FixtureAssemblyModel == fixtureAssembly);

            IPlanAssemblyRenderable assemblyRenderable = assemblyView as IPlanAssemblyRenderable;
            Assert.IsNotNull(assemblyRenderable);

            Assert.AreEqual(assemblyView.Components, assemblyRenderable.Components);
        }

        #endregion

        #region Other tests

        [Test]
        public void GroupAsAssembly_ResetsComponentPositions()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC =
                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //Add another shelf
            PlanogramFixtureComponent shelf2FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            shelf1FC.Y = 50;
            shelf2FC.Y = 100;

            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVm.PlanogramViews.First();

            //group as assembly
            PlanogramAssemblyView newAssembly = 
                planView.Fixtures[0].AddAssembly(new List<PlanogramComponentView>()
                {
                    planView.Fixtures[0].Components[2],
                    planView.Fixtures[0].Components[3]
                });


            Assert.AreEqual(2, planView.Fixtures[0].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies.Count);

            Assert.AreEqual(50, newAssembly.Y);
            Assert.AreEqual(0, newAssembly.Components[0].Y);
            Assert.AreEqual(50, newAssembly.Components[1].Y);

            //check that shelf 1 still has its position
            Assert.AreEqual(1, newAssembly.Components[0].SubComponents[0].Positions.Count);
        }

        [Test]
        public void SplitAssembly_ResetsComponentPositions()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC =
                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //Add another shelf
            PlanogramFixtureComponent shelf2FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            shelf1FC.Y = 50;
            shelf2FC.Y = 100;

            PlanogramView planView = new PlanogramView(plan);

            //group as assembly
            PlanogramAssemblyView newAssembly =
            planView.Fixtures[0].AddAssembly(
                new List<PlanogramComponentView>()
                {
                planView.Fixtures[0].Components[2],
                planView.Fixtures[0].Components[3],});


            List<PlanogramComponentView> splitComponents = newAssembly.Ungroup();

            Assert.AreEqual(50, splitComponents[0].Y);
            Assert.AreEqual(100, splitComponents[1].Y);
        }

        [Test]
        public void GroupAsAssembly_ComponentWithLinkedTextBox()
        {
            //V8-32558 - Checks that adding a component with a linked 
            //textbox to an assembly does not lose the textbox.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVm.PlanogramViews.First();

            //add components
            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            var shelf2 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);

            //link a textbox to component 1
            var anno = shelf1.AddAnnotation();
            anno.Text = "TEST";

            //check
            Assert.AreEqual(1, shelf1.Annotations.Count, "Should only have 1 annotation");
            Assert.AreEqual(1, plan.Annotations.Count, "Should only have 1 annotation");

            //group as assembly
            PlanogramAssemblyView newAssembly =
                planView.Fixtures[0].AddAssembly(new List<PlanogramComponentView>() {shelf1, shelf2});

            //check
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies.Count, "Should have 1 assembly");
            Assert.AreEqual(2, newAssembly.Components.Count, "assembly should have 2 components");

            Assert.AreEqual(1, plan.Annotations.Count, "Should still only have 1 annotation");
            Assert.AreEqual(1, newAssembly.Components[0].Annotations.Count, "Shelf should have 1 annotation");
        }

        [Test]
        public void GroupAsAssembly_ComponentsOnDifferentBaysKeepPosition()
        {
            //V8-31845 - Checks that components on different bays do not loose their world position when grouping

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVm.PlanogramViews.First();
            var fixture1 = planView.AddFixture();
            var fixture2 = planView.AddFixture();
            fixture2.X = fixture1.Width;
            Assert.AreNotEqual(fixture1.X, fixture2.X);

            //add components
            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            var shelf2 = planView.Fixtures[1].AddComponent(PlanogramComponentType.Shelf);

            //get their current world positions
            PointValue shelf1World = new PointValue(shelf1.MetaWorldX, shelf1.MetaWorldY, shelf1.MetaWorldZ);
            PointValue shelf2World = new PointValue(shelf2.MetaWorldX, shelf2.MetaWorldY, shelf2.MetaWorldZ);

            //group as assembly
            PlanogramAssemblyView newAssembly =
                planView.Fixtures[0].AddAssembly(new List<PlanogramComponentView>() { shelf1, shelf2 });

            //check
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies.Count, "Should have 1 assembly");
            Assert.AreEqual(2, newAssembly.Components.Count, "assembly should have 2 components");

            Assert.AreEqual(0, planView.Fixtures[0].Components.Count, "Neither bay should have any components");
            Assert.AreEqual(0, planView.Fixtures[1].Components.Count, "Neither bay should have any components");

            Assert.AreEqual(shelf1World.X, newAssembly.Components[0].MetaWorldX, "Shelf 1 has moved");
            Assert.AreEqual(shelf1World.Y, newAssembly.Components[0].MetaWorldY, "Shelf 1 has moved");
            Assert.AreEqual(shelf1World.Z, newAssembly.Components[0].MetaWorldZ, "Shelf 1 has moved");

            Assert.AreEqual(shelf2World.X, newAssembly.Components[1].MetaWorldX, "Shelf 2 has moved");
            Assert.AreEqual(shelf2World.Y, newAssembly.Components[1].MetaWorldY, "Shelf 2 has moved");
            Assert.AreEqual(shelf2World.Z, newAssembly.Components[1].MetaWorldZ, "Shelf 2 has moved");
        }

        #endregion
    }
}
