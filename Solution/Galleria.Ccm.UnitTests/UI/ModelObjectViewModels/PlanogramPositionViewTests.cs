﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24764 : L.Hodson
//  Created

#endregion

#region Version History : CCM 801

// V8-28837 : A.Kuszyk
//  Added tests for GetFillableSpace method.

#endregion

#region Version History: CCM811

// V8-28856 : A.Silva
//  Removed obsolete Unit Tests having to do with GetFillablespace.

#endregion

#endregion

using Csla.Core;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class PlanogramPositionViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramPositionView CreateView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);

            shelf1C.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelf1C.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelf1C.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            prod1.Height = 15;
            prod1.Width = 10;
            prod1.Depth = 20;
            plan.Products.Add(prod1);

            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0],
                fixtureItem,
                shelf1Fc);

            subComponentPlacement1.AddPosition(prod1);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramPositionView posView = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];

            return posView;
        }

        /// <summary>
        ///     Returns all available dto types in the database
        /// </summary>
        private IEnumerable<String> PositionModelPropertyNames
        {
            get
            {
                String[] exclusions =
                {
                    "Parent",
                    "X",
                    "Y",
                    "Z", // will be tested separately.
                    PlanogramPosition.SlopeProperty.Name,
                    PlanogramPosition.AngleProperty.Name,
                    PlanogramPosition.RollProperty.Name, //rotation not used.
                    PlanogramPosition.SequenceProperty.Name,
                    PlanogramPosition.SequenceXProperty.Name,
                    PlanogramPosition.SequenceYProperty.Name,
                    PlanogramPosition.SequenceZProperty.Name,
                    PlanogramPosition.PositionSequenceNumberProperty.Name,
                };

                return typeof (PlanogramPosition).GetProperties()
                                                 .Where(p => p.DeclaringType == typeof (PlanogramPosition))
                                                 .Where(modelProperty => !exclusions.Contains(modelProperty.Name)
                                                                         && !modelProperty.Name.EndsWith("Id")
                                                                         &&
                                                                         !modelProperty.PropertyType.GetInterfaces()
                                                                                       .Contains(typeof (IObservableBindingList)))
                                                 .Where(modelProperty => !modelProperty.Name.StartsWith("Meta"))
                                                 .Select(modelProperty => modelProperty.Name);
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("PositionModelPropertyNames")]
        public void ModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof (PlanogramPosition).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof (PlanogramPositionView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramPositionView view = CreateView();
                view.PropertyChanged += TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(view.Model, null),
                                viewProperty.GetValue(view, null),
                                String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.GetSetMethod(false) != null)
                {
                    PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue,
                                    modelProperty.GetValue(view.Model, null),
                                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue,
                                    viewProperty.GetValue(view, null),
                                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name,
                                    PropertyChangedNotifications,
                                    String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= TestModel_PropertyChanged;
            }
            else
                Assert.Fail("Property missing from view - include it or exclude it.");
        }

        [Test]
        public void ImageProperties()
        {
            PlanogramPositionView posView = CreateView();
            PlanogramImage testImage = posView.Planogram.AddPlanogramImage(new Byte[] {1}, "test", "test");

            posView.PropertyChanged += TestModel_PropertyChanged;

            PropertyChangedNotifications.Clear();
            posView.Product.FrontImage = testImage;
            Assert.Contains("FrontImageData", PropertyChangedNotifications);
            Assert.Contains("FrontXImageData", PropertyChangedNotifications);
            Assert.Contains("FrontYImageData", PropertyChangedNotifications);
            Assert.Contains("FrontZImageData", PropertyChangedNotifications);

            PropertyChangedNotifications.Clear();
            posView.Product.BackImage = testImage;
            Assert.Contains("BackImageData", PropertyChangedNotifications);
            Assert.Contains("BackXImageData", PropertyChangedNotifications);
            Assert.Contains("BackYImageData", PropertyChangedNotifications);
            Assert.Contains("BackZImageData", PropertyChangedNotifications);

            PropertyChangedNotifications.Clear();
            posView.Product.TopImage = testImage;
            Assert.Contains("TopImageData", PropertyChangedNotifications);
            Assert.Contains("TopXImageData", PropertyChangedNotifications);
            Assert.Contains("TopYImageData", PropertyChangedNotifications);
            Assert.Contains("TopZImageData", PropertyChangedNotifications);

            PropertyChangedNotifications.Clear();
            posView.Product.BottomImage = testImage;
            Assert.Contains("BottomImageData", PropertyChangedNotifications);
            Assert.Contains("BottomXImageData", PropertyChangedNotifications);
            Assert.Contains("BottomYImageData", PropertyChangedNotifications);
            Assert.Contains("BottomZImageData", PropertyChangedNotifications);

            PropertyChangedNotifications.Clear();
            posView.Product.LeftImage = testImage;
            Assert.Contains("LeftImageData", PropertyChangedNotifications);
            Assert.Contains("LeftXImageData", PropertyChangedNotifications);
            Assert.Contains("LeftYImageData", PropertyChangedNotifications);
            Assert.Contains("LeftZImageData", PropertyChangedNotifications);

            PropertyChangedNotifications.Clear();
            posView.Product.RightImage = testImage;
            Assert.Contains("RightImageData", PropertyChangedNotifications);
            Assert.Contains("RightXImageData", PropertyChangedNotifications);
            Assert.Contains("RightYImageData", PropertyChangedNotifications);
            Assert.Contains("RightZImageData", PropertyChangedNotifications);


            posView.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void CalculatedProperty_HeightWidthDepth()
        {
            PlanogramPositionView posView = CreateView();
            posView.PropertyChanged += TestModel_PropertyChanged;

            Assert.AreNotEqual(0, posView.Height);
            Assert.AreNotEqual(0, posView.Width);
            Assert.AreNotEqual(0, posView.Depth);


            //set position value
            PropertyChangedNotifications.Clear();
            posView.FacingsHigh = 5;
            Assert.Contains("Height", PropertyChangedNotifications);
            posView.FacingsWide = 5;
            Assert.Contains("Width", PropertyChangedNotifications);
            posView.FacingsDeep = 5;
            Assert.Contains("Depth", PropertyChangedNotifications);

            //set product value
            PropertyChangedNotifications.Clear();
            posView.Product.Height = 11;
            Assert.Contains("Height", PropertyChangedNotifications);
            posView.Product.Width = 22;
            Assert.Contains("Width", PropertyChangedNotifications);
            posView.Product.Depth = 33;
            Assert.Contains("Depth", PropertyChangedNotifications);

            posView.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void CalculatedProperty_IsMerchandisedAsTrays()
        {
            const String propertyName = "IsMerchandisedAsTrays";

            PlanogramPositionView posView = CreateView();
            posView.PropertyChanged += TestModel_PropertyChanged;

            // unit, default - false;
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            posView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            Assert.IsFalse(posView.IsMerchandisedAsTrays);

            //set product default to tray
            PropertyChangedNotifications.Clear();
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to unit
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            Assert.IsFalse(posView.IsMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to tray
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            posView.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void CalculatedProperty_UnitHeightWidthDepth()
        {
            PlanogramPositionView posView = CreateView();
            PlanogramProductView productView = posView.Product;

            Assert.AreNotEqual(0, posView.Height);
            Assert.AreNotEqual(0, posView.Width);
            Assert.AreNotEqual(0, posView.Depth);

            //set product values
            productView.SqueezeHeight = 0;
            productView.SqueezeWidth = 0;
            productView.SqueezeDepth = 0;

            productView.Height = 1;
            productView.Width = 2;
            productView.Depth = 3;
            productView.DisplayHeight = 4;
            productView.DisplayWidth = 5;
            productView.DisplayDepth = 5;
            productView.TrayHeight = 6;
            productView.TrayWidth = 7;
            productView.TrayDepth = 8;
            productView.CaseHeight = 9;
            productView.CaseWidth = 10;
            productView.CaseDepth = 11;

            productView.MerchandisingStyle = PlanogramProductMerchandisingStyle.Case;

            //check
            posView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            Assert.AreEqual(productView.Height, posView.UnitHeight);
            Assert.AreEqual(productView.Width, posView.UnitWidth);
            Assert.AreEqual(productView.Depth, posView.UnitDepth);

            posView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Display;
            Assert.AreEqual(productView.DisplayHeight, posView.UnitHeight);
            Assert.AreEqual(productView.DisplayWidth, posView.UnitWidth);
            Assert.AreEqual(productView.DisplayDepth, posView.UnitDepth);

            posView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            Assert.AreEqual(productView.TrayHeight - 0.2F, posView.UnitHeight);
            Assert.AreEqual(productView.TrayWidth - 0.2F, posView.UnitWidth);
            Assert.AreEqual(productView.TrayDepth - 0.2F, posView.UnitDepth);

            posView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            Assert.AreEqual(productView.CaseHeight, posView.UnitHeight);
            Assert.AreEqual(productView.CaseWidth, posView.UnitWidth);
            Assert.AreEqual(productView.CaseDepth, posView.UnitDepth);
        }

        [Test]
        public void CalculatedProperty_BlockMainStartX()
        {
            PlanogramPositionView posView = CreateView();

            Assert.AreEqual(0, posView.BlockMainStartX);

            //add a right block
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
            posView.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            posView.IsXPlacedLeft = false;
            posView.FacingsXWide = 1;
            posView.FacingsXHigh = 1;
            posView.FacingsXDeep = 1;

            Assert.AreEqual(0, posView.BlockMainStartX);

            //shift x block to left
            posView.IsXPlacedLeft = true;
            Assert.AreEqual(20, posView.BlockMainStartX);
        }

        [Test]
        public void CalculatedProperty_BlockMainStartY()
        {
            PlanogramPositionView posView = CreateView();

            Assert.AreEqual(0, posView.BlockMainStartY);

            //add a Y block
            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
            posView.OrientationTypeY = PlanogramPositionOrientationType.Top0;
            posView.IsYPlacedBottom = false;
            posView.FacingsYWide = 1;
            posView.FacingsYHigh = 1;
            posView.FacingsYDeep = 1;

            Assert.AreEqual(0, posView.BlockMainStartY);

            //shift block to bottom
            posView.IsYPlacedBottom = true;
            Assert.AreEqual(20, posView.BlockMainStartY);
        }

        [Test]
        public void CalculatedProperty_BlockMainStartZ()
        {
            PlanogramPositionView posView = CreateView();

            Assert.AreEqual(0, posView.BlockMainStartZ);

            //add a Z block
            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
            posView.OrientationTypeZ = PlanogramPositionOrientationType.Front0;
            posView.IsZPlacedFront = false;
            posView.FacingsZWide = 1;
            posView.FacingsZHigh = 1;
            posView.FacingsZDeep = 1;

            Assert.AreEqual(20, posView.BlockMainStartZ);

            //shift block to bottom
            posView.IsZPlacedFront = true;
            Assert.AreEqual(0, posView.BlockMainStartZ);
        }

        [Test]
        public void CalculatedProperty_IsXMerchandisedAsTrays()
        {
            var propertyName = "IsXMerchandisedAsTrays";

            PlanogramPositionView posView = CreateView();
            posView.PropertyChanged += TestModel_PropertyChanged;

            // unit, default - false;
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Default;
            Assert.IsFalse(posView.IsXMerchandisedAsTrays);

            //set product default to tray
            PropertyChangedNotifications.Clear();
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsXMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to unit
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
            Assert.IsFalse(posView.IsXMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to tray
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsXMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            posView.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void CalculatedProperty_UnitXHeightWidthDepth()
        {
            PlanogramPositionView posView = CreateView();
            PlanogramProductView productView = posView.Product;

            Assert.AreNotEqual(0, posView.Height);
            Assert.AreNotEqual(0, posView.Width);
            Assert.AreNotEqual(0, posView.Depth);

            //set product values
            productView.SqueezeHeight = 0;
            productView.SqueezeWidth = 0;
            productView.SqueezeDepth = 0;

            productView.Height = 1;
            productView.Width = 2;
            productView.Depth = 3;
            productView.DisplayHeight = 4;
            productView.DisplayWidth = 5;
            productView.DisplayDepth = 5;
            productView.TrayHeight = 6;
            productView.TrayWidth = 7;
            productView.TrayDepth = 8;
            productView.CaseHeight = 9;
            productView.CaseWidth = 10;
            productView.CaseDepth = 11;

            productView.MerchandisingStyle = PlanogramProductMerchandisingStyle.Case;

            //check
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
            Assert.AreEqual(productView.Height, posView.UnitXHeight);
            Assert.AreEqual(productView.Width, posView.UnitXWidth);
            Assert.AreEqual(productView.Depth, posView.UnitXDepth);

            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Display;
            Assert.AreEqual(productView.DisplayHeight, posView.UnitXHeight);
            Assert.AreEqual(productView.DisplayWidth, posView.UnitXWidth);
            Assert.AreEqual(productView.DisplayDepth, posView.UnitXDepth);

            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Tray;
            Assert.AreEqual(productView.TrayHeight - 0.2F, posView.UnitXHeight);
            Assert.AreEqual(productView.TrayWidth - 0.2F, posView.UnitXWidth);
            Assert.AreEqual(productView.TrayDepth - 0.2F, posView.UnitXDepth);

            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Default;
            Assert.AreEqual(productView.CaseHeight, posView.UnitXHeight);
            Assert.AreEqual(productView.CaseWidth, posView.UnitXWidth);
            Assert.AreEqual(productView.CaseDepth, posView.UnitXDepth);
        }

        [Test]
        public void CalculatedProperty_BlockXStartX()
        {
            PlanogramPositionView posView = CreateView();
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
            posView.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            posView.IsXPlacedLeft = false;
            posView.FacingsXWide = 1;
            posView.FacingsXHigh = 1;
            posView.FacingsXDeep = 1;

            Assert.AreEqual(10, posView.BlockXStartX);

            posView.IsXPlacedLeft = true;

            Assert.AreEqual(0, posView.BlockXStartX);
        }

        [Test]
        public void CalculatedProperty_BlockXStartY()
        {
            PlanogramPositionView posView = CreateView();
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
            posView.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            posView.IsXPlacedLeft = false;
            posView.FacingsXWide = 1;
            posView.FacingsXHigh = 1;
            posView.FacingsXDeep = 1;

            Assert.AreEqual(0, posView.BlockXStartY);

            posView.IsXPlacedLeft = true;

            Assert.AreEqual(0, posView.BlockXStartY);
        }

        [Test]
        public void CalculatedProperty_BlockXStartZ()
        {
            PlanogramPositionView posView = CreateView();
            posView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
            posView.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            posView.IsXPlacedLeft = false;
            posView.FacingsXWide = 1;
            posView.FacingsXHigh = 1;
            posView.FacingsXDeep = 1;

            Assert.AreEqual(10, posView.BlockXStartZ);

            posView.IsXPlacedLeft = true;

            Assert.AreEqual(10, posView.BlockXStartZ);
        }

        [Test]
        public void CalculatedProperty_IsYMerchandisedAsTrays()
        {
            const String propertyName = "IsYMerchandisedAsTrays";

            PlanogramPositionView posView = CreateView();
            posView.PropertyChanged += TestModel_PropertyChanged;

            // unit, default - false;
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Default;
            Assert.IsFalse(posView.IsYMerchandisedAsTrays);

            //set product default to tray
            PropertyChangedNotifications.Clear();
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsYMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to unit
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
            Assert.IsFalse(posView.IsYMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to tray
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsYMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            posView.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void CalculatedProperty_UnitYHeightWidthDepth()
        {
            PlanogramPositionView posView = CreateView();
            PlanogramProductView productView = posView.Product;

            Assert.AreNotEqual(0, posView.Height);
            Assert.AreNotEqual(0, posView.Width);
            Assert.AreNotEqual(0, posView.Depth);

            //set product values
            productView.SqueezeHeight = 0;
            productView.SqueezeWidth = 0;
            productView.SqueezeDepth = 0;

            productView.Height = 1;
            productView.Width = 2;
            productView.Depth = 3;
            productView.DisplayHeight = 4;
            productView.DisplayWidth = 5;
            productView.DisplayDepth = 5;
            productView.TrayHeight = 6;
            productView.TrayWidth = 7;
            productView.TrayDepth = 8;
            productView.CaseHeight = 9;
            productView.CaseWidth = 10;
            productView.CaseDepth = 11;

            productView.MerchandisingStyle = PlanogramProductMerchandisingStyle.Case;

            //check
            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
            Assert.AreEqual(productView.Height, posView.UnitYHeight);
            Assert.AreEqual(productView.Width, posView.UnitYWidth);
            Assert.AreEqual(productView.Depth, posView.UnitYDepth);

            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Display;
            Assert.AreEqual(productView.DisplayHeight, posView.UnitYHeight);
            Assert.AreEqual(productView.DisplayWidth, posView.UnitYWidth);
            Assert.AreEqual(productView.DisplayDepth, posView.UnitYDepth);

            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Tray;
            Assert.AreEqual(productView.TrayHeight - 0.2F, posView.UnitYHeight);
            Assert.AreEqual(productView.TrayWidth - 0.2F, posView.UnitYWidth);
            Assert.AreEqual(productView.TrayDepth - 0.2F, posView.UnitYDepth);

            posView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Default;
            Assert.AreEqual(productView.CaseHeight, posView.UnitYHeight);
            Assert.AreEqual(productView.CaseWidth, posView.UnitYWidth);
            Assert.AreEqual(productView.CaseDepth, posView.UnitYDepth);
        }

        [Test]
        public void CalculatedProperty_BlockYStartX()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_BlockYStartY()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_BlockYStartZ()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_IsZMerchandisedAsTrays()
        {
            const String propertyName = "IsZMerchandisedAsTrays";

            PlanogramPositionView posView = CreateView();
            posView.PropertyChanged += TestModel_PropertyChanged;

            // unit, default - false;
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Default;
            Assert.IsFalse(posView.IsZMerchandisedAsTrays);

            //set product default to tray
            PropertyChangedNotifications.Clear();
            posView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsZMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to unit
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
            Assert.IsFalse(posView.IsZMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            //set pos to tray
            PropertyChangedNotifications.Clear();
            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Tray;
            Assert.IsTrue(posView.IsZMerchandisedAsTrays);
            Assert.Contains(propertyName, PropertyChangedNotifications);

            posView.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void CalculatedProperty_UnitZHeightWidthDepth()
        {
            PlanogramPositionView posView = CreateView();
            PlanogramProductView productView = posView.Product;

            Assert.AreNotEqual(0, posView.Height);
            Assert.AreNotEqual(0, posView.Width);
            Assert.AreNotEqual(0, posView.Depth);

            //set product values
            productView.SqueezeHeight = 0;
            productView.SqueezeWidth = 0;
            productView.SqueezeDepth = 0;

            productView.Height = 1;
            productView.Width = 2;
            productView.Depth = 3;
            productView.DisplayHeight = 4;
            productView.DisplayWidth = 5;
            productView.DisplayDepth = 5;
            productView.TrayHeight = 6;
            productView.TrayWidth = 7;
            productView.TrayDepth = 8;
            productView.CaseHeight = 9;
            productView.CaseWidth = 10;
            productView.CaseDepth = 11;

            productView.MerchandisingStyle = PlanogramProductMerchandisingStyle.Case;

            //check
            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;
            Assert.AreEqual(productView.Height, posView.UnitZHeight);
            Assert.AreEqual(productView.Width, posView.UnitZWidth);
            Assert.AreEqual(productView.Depth, posView.UnitZDepth);

            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Display;
            Assert.AreEqual(productView.DisplayHeight, posView.UnitZHeight);
            Assert.AreEqual(productView.DisplayWidth, posView.UnitZWidth);
            Assert.AreEqual(productView.DisplayDepth, posView.UnitZDepth);

            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Tray;
            Assert.AreEqual(productView.TrayHeight - 0.2F, posView.UnitZHeight);
            Assert.AreEqual(productView.TrayWidth - 0.2F, posView.UnitZWidth);
            Assert.AreEqual(productView.TrayDepth - 0.2F, posView.UnitZDepth);

            posView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Default;
            Assert.AreEqual(productView.CaseHeight, posView.UnitZHeight);
            Assert.AreEqual(productView.CaseWidth, posView.UnitZWidth);
            Assert.AreEqual(productView.CaseDepth, posView.UnitZDepth);
        }

        [Test]
        public void CalculatedProperty_BlockZStartX()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_BlockZStartY()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_BlockZStartZ()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_FacingSpaceX()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_FacingSpaceY()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void CalculatedProperty_FacingSpaceZ()
        {
            Assert.Ignore("TODO");
        }


        [Test]
        public void WorldSpaceProperties_AreInitialized()
        {
            PlanogramPositionView view = CreateView();

            PointValue expectedValue = view.Model.GetPlanogramRelativeCoordinates(view.SubComponent.ModelPlacement);

            Assert.AreEqual(expectedValue.X, view.MetaWorldX);
            Assert.AreEqual(expectedValue.Y, view.MetaWorldY);
            Assert.AreEqual(expectedValue.Z, view.MetaWorldZ);
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByFixtureMove()
        {
            PlanogramPositionView view = CreateView();
            view.PropertyChanged += TestModel_PropertyChanged;
            PropertyChangedNotifications.Clear();

            //move the fixture
            view.Fixture.X += 10;

            Assert.Contains(PlanogramPosition.MetaWorldXProperty.Name, PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByAssemblyMove()
        {
            PlanogramPositionView view = CreateView();
            PlanogramView planView = view.Planogram;

            view.Fixture.AddAssembly(new List<PlanogramComponentView>{view.Component});

            view = planView.EnumerateAllPositions().First();

            view.PropertyChanged += TestModel_PropertyChanged;
            PropertyChangedNotifications.Clear();

            //move the assembly
            planView.EnumerateAllAssemblies().First().X += 10;

            Assert.Contains(PlanogramPosition.MetaWorldXProperty.Name, PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByComponentMove()
        {
            PlanogramPositionView view = CreateView();
            view.PropertyChanged += TestModel_PropertyChanged;
            PropertyChangedNotifications.Clear();

            //move the fixture
            view.Component.X += 10;

            Assert.Contains(PlanogramPosition.MetaWorldXProperty.Name, PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedBySubComponentMove()
        {
            PlanogramPositionView view = CreateView();
            view.PropertyChanged += TestModel_PropertyChanged;
            PropertyChangedNotifications.Clear();

            //move the fixture
            view.SubComponent.X += 10;

            Assert.Contains(PlanogramPosition.MetaWorldXProperty.Name, PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= TestModel_PropertyChanged;
        }

        [Test]
        public void WorldSpaceProperties_UpdatedByPositionMove()
        {
            PlanogramPositionView view = CreateView();
            view.PropertyChanged += TestModel_PropertyChanged;
            PropertyChangedNotifications.Clear();

            //move the fixture
            view.X += 10;

            Assert.Contains(PlanogramPosition.MetaWorldXProperty.Name, PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramPosition.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= TestModel_PropertyChanged;
        }

        #endregion

        #region Methods

        [Test]
        public void Constructor()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin2";
            prod1.Name = "Product2";
            plan.Products.Add(prod2);

            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0],
                fixtureItem,
                shelf1Fc);

            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod2);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramPositionView posView = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];

            Assert.AreEqual(pos1, posView.Model);
            Assert.AreEqual(prod2, posView.Product.Model);
        }

        [Test]
        public void GetLabelText()
        {
            foreach (FieldSelectorGroup fieldGroup in PlanItemHelper.GetPlanogramFieldSelectorGroups())
            {
                foreach (ObjectFieldInfo field in fieldGroup.Fields)
                {
                    PlanogramPositionView planPositionView = CreateView();

                    //get the item to set the value of
                    Object setItem = planPositionView;

                    if (fieldGroup.Name == PlanItemHelper.FriendlyNames[PlanItemType.Product])
                        setItem = planPositionView.Product;
                    else if (fieldGroup.Name == PlanItemHelper.FriendlyNames[PlanItemType.Position])
                        setItem = planPositionView;
                    else if (fieldGroup.Name == PlanItemHelper.FriendlyNames[PlanItemType.SubComponent])
                        setItem = planPositionView.SubComponent;
                    else if (fieldGroup.Name == PlanItemHelper.FriendlyNames[PlanItemType.Component])
                        setItem = planPositionView.Component;
                    else if (fieldGroup.Name == PlanItemHelper.FriendlyNames[PlanItemType.Fixture])
                        setItem = planPositionView.Fixture;
                    else if (fieldGroup.Name == PlanItemHelper.FriendlyNames[PlanItemType.Planogram])
                        setItem = planPositionView.Planogram;
                    else
                        Assert.Fail("Test needs updating: field group not handled");

                    String formatString = field.FieldPlaceholder;


                    try
                    {
                        TestDataHelper.SetItemPropertyValue1(setItem, field.PropertyName);
                    }
                    catch (Exception)
                    {
                        Assert.Fail("{0}: PropertyInfo not found.", formatString);
                    }


                    //check we have a value
                    Object value = Convert.ToString(planPositionView.GetLabelText(formatString));
                    Assert.AreNotEqual(value, formatString, "{0}: Value was not resolved", formatString);
                }
            }
        }

        [Test]
        public void GetLabelText_MerchTypeReturnsApplied()
        {
            //V8-28007

            PlanogramPositionView planPositionView = CreateView();

            planPositionView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            planPositionView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Default;
            planPositionView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Default;
            planPositionView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Default;
            planPositionView.Product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Case;

            ObjectFieldInfo merchstyleFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.MerchandisingStyleProperty.Name);
            ObjectFieldInfo merchstyleXFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.MerchandisingStyleXProperty.Name);
            ObjectFieldInfo merchstyleYFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.MerchandisingStyleYProperty.Name);
            ObjectFieldInfo merchstyleZFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.MerchandisingStyleZProperty.Name);


            Assert.AreNotEqual(Convert.ToString(PlanogramPositionMerchandisingStyle.Default),
                               planPositionView.GetLabelText(merchstyleFieldInfo.FieldPlaceholder));
            Assert.AreNotEqual(Convert.ToString(PlanogramPositionMerchandisingStyle.Default),
                               planPositionView.GetLabelText(merchstyleXFieldInfo.FieldPlaceholder));
            Assert.AreNotEqual(Convert.ToString(PlanogramPositionMerchandisingStyle.Default),
                               planPositionView.GetLabelText(merchstyleYFieldInfo.FieldPlaceholder));
            Assert.AreNotEqual(Convert.ToString(PlanogramPositionMerchandisingStyle.Default),
                               planPositionView.GetLabelText(merchstyleZFieldInfo.FieldPlaceholder));

            planPositionView.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            planPositionView.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Tray;
            planPositionView.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Display;
            planPositionView.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.PointOfPurchase;

            //nb - the label should contain the enum friendly names
            Assert.AreEqual(PlanogramPositionMerchandisingStyleHelper.FriendlyNames[PlanogramPositionMerchandisingStyle.Unit],
                            planPositionView.GetLabelText(merchstyleFieldInfo.FieldPlaceholder));
            Assert.AreEqual(PlanogramPositionMerchandisingStyleHelper.FriendlyNames[PlanogramPositionMerchandisingStyle.Tray],
                            planPositionView.GetLabelText(merchstyleXFieldInfo.FieldPlaceholder));
            Assert.AreEqual(PlanogramPositionMerchandisingStyleHelper.FriendlyNames[PlanogramPositionMerchandisingStyle.Display],
                            planPositionView.GetLabelText(merchstyleYFieldInfo.FieldPlaceholder));
            Assert.AreEqual(PlanogramPositionMerchandisingStyleHelper.FriendlyNames[PlanogramPositionMerchandisingStyle.PointOfPurchase],
                            planPositionView.GetLabelText(merchstyleZFieldInfo.FieldPlaceholder));
        }

        [Test]
        public void GetLabelText_OrientationTypeReturnsApplied()
        {
            //V8-28007

            PlanogramPositionView planPositionView = CreateView();

            planPositionView.OrientationType = PlanogramPositionOrientationType.Default;
            planPositionView.OrientationTypeX = PlanogramPositionOrientationType.Default;
            planPositionView.OrientationTypeY = PlanogramPositionOrientationType.Default;
            planPositionView.OrientationTypeZ = PlanogramPositionOrientationType.Default;
            planPositionView.Product.OrientationType = PlanogramProductOrientationType.Back90;

            ObjectFieldInfo mainFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.OrientationTypeProperty.Name);
            ObjectFieldInfo xFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.OrientationTypeXProperty.Name);
            ObjectFieldInfo yFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.OrientationTypeYProperty.Name);
            ObjectFieldInfo zFieldInfo =
                PlanogramPositionViewModelBase.EnumerateDisplayableFields()
                                              .First(p => p.PropertyName == "Applied" + PlanogramPosition.OrientationTypeZProperty.Name);


            Assert.AreNotEqual(Convert.ToString(PlanogramPositionOrientationType.Default),
                               planPositionView.GetLabelText(mainFieldInfo.FieldPlaceholder));
            Assert.AreNotEqual(Convert.ToString(PlanogramPositionOrientationType.Default), planPositionView.GetLabelText(xFieldInfo.FieldPlaceholder));
            Assert.AreNotEqual(Convert.ToString(PlanogramPositionOrientationType.Default), planPositionView.GetLabelText(yFieldInfo.FieldPlaceholder));
            Assert.AreNotEqual(Convert.ToString(PlanogramPositionOrientationType.Default), planPositionView.GetLabelText(zFieldInfo.FieldPlaceholder));

            planPositionView.OrientationType = PlanogramPositionOrientationType.Back0;
            planPositionView.OrientationTypeX = PlanogramPositionOrientationType.Back180;
            planPositionView.OrientationTypeY = PlanogramPositionOrientationType.Bottom180;
            planPositionView.OrientationTypeZ = PlanogramPositionOrientationType.Front0;

            //nb - the label should contain the enum friendly names
            Assert.AreEqual(PlanogramPositionOrientationTypeHelper.FriendlyNames[PlanogramPositionOrientationType.Back0],
                            planPositionView.GetLabelText(mainFieldInfo.FieldPlaceholder));
            Assert.AreEqual(PlanogramPositionOrientationTypeHelper.FriendlyNames[PlanogramPositionOrientationType.Back180],
                            planPositionView.GetLabelText(xFieldInfo.FieldPlaceholder));
            Assert.AreEqual(PlanogramPositionOrientationTypeHelper.FriendlyNames[PlanogramPositionOrientationType.Bottom180],
                            planPositionView.GetLabelText(yFieldInfo.FieldPlaceholder));
            Assert.AreEqual(PlanogramPositionOrientationTypeHelper.FriendlyNames[PlanogramPositionOrientationType.Front0],
                            planPositionView.GetLabelText(zFieldInfo.FieldPlaceholder));
        }

        #endregion

        #region Other

        [Test]
        public void Delete_RemoveComponentRemovesChildPositionModels()
        {
            //v8-28300 - check that positions get cleaned up.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            var planView = new PlanogramView(plan);

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            PlanogramFixtureView fixture1 = planView.AddFixture();
            PlanogramComponentView component1 = fixture1.AddComponent(PlanogramComponentType.Shelf);
            PlanogramPosition p1Model = component1.SubComponents[0].AddPosition(prod1).Model;

            //delete the component from the fixture
            fixture1.RemoveComponent(component1);

            Assert.IsTrue(p1Model.IsDeleted, "position should also have been deleted");
            Assert.AreEqual(0, plan.Positions.Count, "plan should have no positions.");
        }

        [Test]
        public void Delete_RemoveAssemblyRemovesChildPositionModels()
        {
            //v8-28300 - check that positions get cleaned up.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            var planView = new PlanogramView(plan);

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            PlanogramFixtureView fixture1 = planView.AddFixture();
            PlanogramAssemblyView assembly1 = fixture1.AddAssembly();
            PlanogramComponentView component1 = assembly1.AddComponent(PlanogramComponentType.Shelf);
            PlanogramPosition p1Model = component1.SubComponents[0].AddPosition(prod1).Model;

            //delete the component from the fixture
            fixture1.RemoveAssembly(assembly1);

            Assert.IsTrue(p1Model.IsDeleted, "position should also have been deleted");
            Assert.AreEqual(0, plan.Positions.Count, "plan should have no positions.");
        }

        [Test]
        public void Delete_RemoveFixtureRemovesChildPositionModels()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            var planView = new PlanogramView(plan);

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            PlanogramFixtureView fixture1 = planView.AddFixture();
            PlanogramComponentView component1 = fixture1.AddComponent(PlanogramComponentType.Shelf);
            PlanogramPosition p1Model = component1.SubComponents[0].AddPosition(prod1).Model;

            //delete the fixture from the plan
            planView.RemoveFixture(fixture1);

            Assert.IsTrue(p1Model.IsDeleted, "position should also have been deleted");
            Assert.AreEqual(0, plan.Positions.Count, "plan should have no positions.");
        }

        #endregion
    }
}