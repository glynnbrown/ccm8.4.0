﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion

#region Version History: CCM830

// V8-32080 : A.Silva
//  Added tests for adding new positions from another plan.
// V8-32327 : A.Silva
//  Added tests to check that positions are added in the expected order.

#endregion

#endregion

using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Galleria.Framework.Planograms.Rendering;


namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class PlanogramSubComponentViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramSubComponentView CreateView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC =
                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);


            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);


            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllSubComponents().First();
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> SubComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { 
                    "Parent",
                    "ImageIdFront", "ImageIdBack", "ImageIdTop", "ImageIdBottom", "ImageIdLeft", "ImageIdRight"
                };

                foreach (PropertyInfo modelProperty in typeof(PlanogramSubComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramSubComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.StartsWith("Meta")) continue; //ignore meta properties

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        public IEnumerable<String> SubComponentImagePropertyNames
        {
            get
            {
                yield return "FrontImage";
                yield return "BackImage";
                yield return "TopImage";
                yield return "BottomImage";
                yield return "LeftImage";
                yield return "RightImage";
            }
        }

        private static PlanogramSubComponentView CreateKeepSequenceOrderScenario(out List<PlanogramPositionView> draggedPositions, out List<PositionSnapShot> expected, out PlanogramPositionView anchorPosition)
        {
            Planogram planogram = "TestPackage".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItem = planogram.AddFixtureItem();
            PlanogramFixtureComponent sourceShelfOne = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent sourceShelfTwo = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                                    new PointValue(sourceShelfOne.X, sourceShelfOne.Y + 40, sourceShelfOne.Z));
            PlanogramFixtureComponent targetShelf = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf,
                                                                                    new PointValue(sourceShelfTwo.X, sourceShelfTwo.Y + 40, sourceShelfTwo.Z));
            for (var i = 0; i < 5; i++)
            {
                sourceShelfOne.AddPosition(fixtureItem);
            }
            for (var i = 0; i < 5; i++)
            {
                sourceShelfTwo.AddPosition(fixtureItem);
            }
            targetShelf.AddPosition(fixtureItem);
            PlanogramView planogramView = PackageViewModel.NewPackageViewModel(planogram.Parent, false).PlanogramViews.First();
            PlanogramSubComponentView sourceViewOne = planogramView.Fixtures.First().Components.First().SubComponents.First();
            PlanogramSubComponentView sourceViewTwo = planogramView.Fixtures.First().Components.Skip(1).First().SubComponents.First();
            PlanogramSubComponentView targetView = planogramView.Fixtures.First().Components.Last().SubComponents.First();
            anchorPosition = targetView.Positions.First();
            draggedPositions = new List<PlanogramPositionView>();
            draggedPositions.AddRange(sourceViewOne.Positions);
            draggedPositions.AddRange(sourceViewTwo.Positions);
            expected = sourceViewOne.Positions.Select(PositionSnapShot.Take).OrderBy(shot => shot.SequenceX).ToList();
            expected.AddRange(sourceViewTwo.Positions.Select(PositionSnapShot.Take).OrderBy(shot => shot.SequenceX));
            Int16 nextSeq = 0;
            foreach (PositionSnapShot shot in expected)
            {
                shot.SequenceX = ++nextSeq;
            }
            return targetView;
        }

        public class PositionSnapShot
        {
            public static PositionSnapShot Take(PlanogramPositionView view)
            {
                return new PositionSnapShot(view);
            }

            private readonly Int16 _sequenceY;
            private readonly Int16 _sequenceZ;
            private readonly String _gtin;
            public Int16 SequenceX { get; set; }

            public Int16 SequenceY { get { return _sequenceY; } }
            public Int16 SequenceZ { get { return _sequenceZ; } }
            public String Gtin { get { return _gtin; } }

            private PositionSnapShot(Int16 sequenceX, Int16 sequenceY, Int16 sequenceZ, String gtin)
            {
                SequenceX = sequenceX;
                _sequenceY = sequenceY;
                _sequenceZ = sequenceZ;
                _gtin = gtin;
            }

            private PositionSnapShot(PlanogramPositionView view) : this(view.SequenceX, view.SequenceY, view.SequenceZ, view.Product.Gtin) { }

            public override String ToString()
            {
                var builder = new StringBuilder();
                builder.Append("{ Gtin = ");
                builder.Append(Gtin);
                //builder.Append(" - Seq(");
                //builder.Append(SequenceX);
                //builder.Append(", ");
                //builder.Append(SequenceY);
                //builder.Append(", ");
                //builder.Append(SequenceZ);
                //builder.Append(")");
                builder.Append(" }");
                return builder.ToString();
            }

            public override Boolean Equals(Object value)
            {
                var type = value as PositionSnapShot;
                return (type != null) && EqualityComparer<Int16>.Default.Equals(type.SequenceX, SequenceX) && EqualityComparer<Int16>.Default.Equals(type.SequenceY, SequenceY) && EqualityComparer<Int16>.Default.Equals(type.SequenceZ, SequenceZ) && EqualityComparer<String>.Default.Equals(type.Gtin, Gtin);
            }

            public override Int32 GetHashCode()
            {
                var num = 0x7a2f0b42;
                num = (-1521134295 * num) + EqualityComparer<Int16>.Default.GetHashCode(SequenceX);
                num = (-1521134295 * num) + EqualityComparer<Int16>.Default.GetHashCode(SequenceY);
                num = (-1521134295 * num) + EqualityComparer<Int16>.Default.GetHashCode(SequenceZ);
                return (-1521134295 * num) + EqualityComparer<String>.Default.GetHashCode(Gtin);
            }
        }

        private static PlanogramSubComponentView GetEmptySubComponentViewWithoutDividerInfo()
        {
            PackageViewModel packageViewModel = PackageViewModel.NewPackageViewModel("TestPackage".CreatePackage(), false);
            packageViewModel.Model.AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponentView subView = packageViewModel.PlanogramViews.First().Fixtures.First().Components.First().SubComponents.First();
            return subView;
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("SubComponentModelPropertyNames")]
        public void ModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramSubComponent).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramSubComponentView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramSubComponentView view = CreateView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                PlanogramSubComponent model = view.Model;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(model, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.GetSetMethod(false) != null)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    if (Object.Equals(modelProperty.GetValue(model, null), newValue))
                    {
                        newValue = GetPropertyTestValue2(viewProperty.PropertyType);
                    }
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(model, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                String[] notInUse = new String[]
                {
                    PlanogramSubComponent.IsVisibleProperty.Name,
                    PlanogramSubComponent.HasCollisionDetectionProperty.Name,
                    PlanogramSubComponent.RiserFillPatternTypeProperty.Name,
                };
                if (!notInUse.Contains(propertyName))
                {
                    Assert.Fail("Property missing from view - include it or exclude it.");
                }
            }

        }

        [Test, TestCaseSource("SubComponentImagePropertyNames")]
        public void SubComponentImagePropertiesGetSet(String propertyName)
        {
            PropertyInfo viewProperty = typeof(PlanogramSubComponentView).GetProperty(propertyName);
            Assert.IsNotNull(viewProperty);

            PlanogramSubComponentView view = CreateView();
            view.PropertyChanged += base.TestModel_PropertyChanged;

            PlanogramImage testImage = view.Planogram.AddPlanogramImage(new Byte[] { }, "img", "imgdesc");

            base.PropertyChangedNotifications.Clear();
            viewProperty.SetValue(view, testImage, null);

            Assert.AreEqual(testImage, viewProperty.GetValue(view, null));

            Assert.Contains(propertyName, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", propertyName));


            //check that the data property has also been fired
            Assert.Contains(propertyName + "Data", base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", propertyName + "Data"));


            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_SetPositionSubComponentId()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                sub, fixtureItem, fc);

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PlanogramPosition pos = subComponentPlacement.AddPosition(prod1);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];
            PlanogramPositionView posView = planView.EnumerateAllPositions().First();

            PlanogramSubComponentView subView2 = subView.Component.AddSubComponent();

            Assert.AreEqual(1, subView.Positions.Count);
            Assert.AreEqual(0, subView2.Positions.Count);

            //move
            posView.Model.PlanogramFixtureItemId = subView2.Fixture.FixtureItemModel.Id;
            posView.Model.PlanogramFixtureComponentId = subView2.Component.FixtureComponentModel.Id;
            posView.Model.PlanogramSubComponentId = subView2.Model.Id;

            Assert.AreEqual(0, subView.Positions.Count);
            Assert.AreEqual(1, subView2.Positions.Count);
            Assert.AreEqual(pos, subView2.Positions[0].Model);


            posView = subView2.Positions[0];

            //move back
            posView.Model.PlanogramFixtureItemId = subView.Fixture.FixtureItemModel.Id;
            posView.Model.PlanogramFixtureComponentId = subView.Component.FixtureComponentModel.Id;
            posView.Model.PlanogramSubComponentId = subView.Model.Id;

            Assert.AreEqual(1, subView.Positions.Count);
            Assert.AreEqual(0, subView2.Positions.Count);
            Assert.AreEqual(pos, subView.Positions[0].Model);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                sub, fixtureItem, fc);

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PlanogramPosition pos = subComponentPlacement.AddPosition(prod1);

            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureComponentId = fc.Id;
            anno2.PlanogramSubComponentId = sub.Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);


            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            Assert.AreEqual(sub, subView.Model);
            Assert.AreEqual(plan, subView.Planogram.Model);


            Assert.AreEqual(1, subView.Positions.Count);
            Assert.AreEqual(1, subView.Annotations.Count);
        }

        #endregion

        #region Methods

        #region Annotation Methods

        [Test]
        public void AddAnnotation()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];

            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            //add new view
            PlanogramAnnotationView view1 = subView.AddAnnotation();
            Assert.AreEqual(1, subView.Annotations.Count);

            //add model
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureComponentId = fc.Id;
            anno2.PlanogramSubComponentId = sub.Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);
            Assert.AreEqual(2, subView.Annotations.Count);
        }

        [Test]
        public void AddAnnotationCopy()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];

            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            //add new view
            PlanogramAnnotationView view1 = subView.AddAnnotation();
            Assert.AreEqual(1, subView.Annotations.Count);

            //add copy
            PlanogramAnnotationView view2 = subView.AddAnnotationCopy(view1);
            Assert.AreEqual(2, subView.Annotations.Count);
            Assert.AreNotEqual(view2.Model, view1.Model);
        }

        [Test]
        public void RemoveAnnotation()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];

            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            //add new view
            PlanogramAnnotationView view1 = subView.AddAnnotation();
            PlanogramAnnotationView view2 = subView.AddAnnotation();
            Assert.AreEqual(2, subView.Annotations.Count);

            //remove view
            subView.RemoveAnnotation(view1);
            Assert.AreEqual(1, subView.Annotations.Count);

            //remove model
            plan.Annotations.Remove(view2.Model);
            Assert.AreEqual(0, subView.Annotations.Count);
        }

        [Test]
        public void SetAnnotationSubComponentId()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];

            PlanogramAnnotation anno = PlanogramAnnotation.NewPlanogramAnnotation();
            sub.GetPlanogramSubComponentPlacement().AddAnnotation(anno);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];
            
            PlanogramAnnotationView annoView = planView.EnumerateAllAnnotations().First();

            PlanogramSubComponentView subView2 = subView.Component.AddSubComponent();

            Assert.AreEqual(1, subView.Annotations.Count);
            Assert.AreEqual(0, subView2.Annotations.Count);

            //move
            annoView.Model.PlanogramFixtureItemId = subView2.Fixture.FixtureItemModel.Id;
            annoView.Model.PlanogramFixtureComponentId = subView2.Component.FixtureComponentModel.Id;
            annoView.Model.PlanogramSubComponentId = subView2.Model.Id;

            Assert.AreEqual(0, subView.Annotations.Count);
            Assert.AreEqual(1, subView2.Annotations.Count);
            Assert.AreEqual(anno, subView2.Annotations[0].Model);


            annoView = subView2.Annotations[0];

            //move back
            annoView.Model.PlanogramFixtureItemId = subView.Fixture.FixtureItemModel.Id;
            annoView.Model.PlanogramFixtureComponentId = subView.Component.FixtureComponentModel.Id;
            annoView.Model.PlanogramSubComponentId = subView.Model.Id;

            Assert.AreEqual(1, subView.Annotations.Count);
            Assert.AreEqual(0, subView2.Annotations.Count);
            Assert.AreEqual(anno, subView.Annotations[0].Model);
        }

        [Test]
        public void MoveFixtureAnnotation()
        {
            Assert.Ignore("TODO");
        }

        #endregion

        #region Position Methods

        [Test]
        public void AddPosition()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                sub, fixtureItem, fc);

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "Gtin2";
            prod2.Name = "Product2";
            plan.Products.Add(prod2);


            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            Assert.AreEqual(0, subView.Positions.Count);

            //add new view
            PlanogramPositionView view1 = subView.AddPosition(planView.Products[0]);
            Assert.AreEqual(1, subView.Positions.Count);
            Assert.AreEqual(planView.Products[0], view1.Product);

            //add new model
            subComponentPlacement.AddPosition(prod2);
            Assert.AreEqual(2, subView.Positions.Count);
            Assert.AreEqual(prod2, subView.Positions[1].Product.Model);
        }

        [Test]
        public void AddPositionCopy()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            //add new view
            PlanogramPositionView view1 = subView.AddPosition(planView.Products[0]);
            Assert.AreEqual(1, subView.Positions.Count);

            //add copy
            PlanogramPositionView view2 = subView.AddPositionCopy(view1);
            Assert.AreEqual(2, subView.Positions.Count);
            Assert.AreNotEqual(view1.Model, view2.Model);
        }

        [Test]
        public void RemovePosition()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "Gtin2";
            prod2.Name = "Product2";
            plan.Products.Add(prod2);


            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            PlanogramPositionView view1 = subView.AddPosition(planView.Products[0]);
            PlanogramPositionView view2 = subView.AddPosition(planView.Products[1]);
            Assert.AreEqual(2, subView.Positions.Count);

            //remove view
            subView.RemovePosition(view1);
            Assert.AreEqual(1, subView.Positions.Count);
            Assert.Contains(view2, subView.Positions);

            //remove model
            plan.Positions.Remove(view2.Model);
            Assert.AreEqual(0, subView.Positions.Count);
        }

        [Test]
        public void MovePosition()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                sub, fixtureItem, fc);

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            PlanogramPosition pos = subComponentPlacement.AddPosition(prod1);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];
            PlanogramPositionView posView = planView.EnumerateAllPositions().First();

            PlanogramSubComponentView subView2 = subView.Component.AddSubComponent();

            Assert.AreEqual(1, subView.Positions.Count);
            Assert.AreEqual(0, subView2.Positions.Count);

            //move
            subView2.MovePosition(posView);
            Assert.AreEqual(0, subView.Positions.Count);
            Assert.AreEqual(1, subView2.Positions.Count);
            Assert.AreEqual(pos, subView2.Positions[0].Model);

            posView = subView2.Positions[0];

            //move back
            subView.MovePosition(posView);
            Assert.AreEqual(1, subView.Positions.Count);
            Assert.AreEqual(0, subView2.Positions.Count);
            Assert.AreEqual(pos, subView.Positions[0].Model);
        }

        [Test]
        public void MovesPositionsToComponentKeepingSequenceX()
        {
            List<PlanogramPositionView> draggedPositions;
            List<PositionSnapShot> expected;
            PlanogramPositionView anchorPosition;
            PlanogramSubComponentView targetView = CreateKeepSequenceOrderScenario(out draggedPositions, out expected, out anchorPosition);
            //  Correct Sequence as they should be added after the exiting one.
            foreach (PositionSnapShot shot in expected)
            {
                shot.SequenceX += 1;
            }

            foreach (PlanogramPositionView view in draggedPositions)
            {
                targetView.MovePosition(view);
            }

            CollectionAssert.AreEquivalent(expected, targetView.Positions.Select(PositionSnapShot.Take).Where(shot => draggedPositions.Any(view => Equals(view.Product.Gtin, shot.Gtin))).OrderBy(shot => shot.SequenceX).ToList(), "Each position should have the original sequence order.");
        }

        [Test]
        public void MovesPositionsToRightOfAnchorKeepingSequenceX()
        {
            List<PlanogramPositionView> draggedPositions;
            List<PositionSnapShot> expected;
            PlanogramPositionView anchorPosition;
            PlanogramSubComponentView targetView = CreateKeepSequenceOrderScenario(out draggedPositions, out expected, out anchorPosition);
            //  Correct Sequence as they should be added after the exiting one.
            foreach (PositionSnapShot shot in expected)
            {
                shot.SequenceX += 1;
            }

            targetView.MovePositions(draggedPositions, anchorPosition, PlanogramPositionAnchorDirection.ToRight);

            CollectionAssert.AreEquivalent(expected, targetView.Positions.Select(PositionSnapShot.Take).Where(shot => draggedPositions.Any(view => Equals(view.Product.Gtin, shot.Gtin))).OrderBy(shot => shot.SequenceX).ToList(), "Each position should have the original sequence order.");
        }

        [Test]
        public void MovesPositionsToLeftOfAnchorKeepingSequenceX()
        {
            List<PlanogramPositionView> draggedPositions;
            List<PositionSnapShot> expected;
            PlanogramPositionView anchorPosition;
            PlanogramSubComponentView targetView = CreateKeepSequenceOrderScenario(out draggedPositions, out expected, out anchorPosition);

            targetView.MovePositions(draggedPositions, anchorPosition, PlanogramPositionAnchorDirection.ToLeft);

            CollectionAssert.AreEquivalent(expected, targetView.Positions.Select(PositionSnapShot.Take).Where(shot => draggedPositions.Any(view => Equals(view.Product.Gtin, shot.Gtin))).OrderBy(shot => shot.SequenceX).ToList(), "Each position should have the original sequence order.");
        }

        [Test]
        public void AddsPositionCopiesToComponentKeepingSequenceX()
        {
            Assert.Ignore("Need to work out why the first position is messed up");
            List<PlanogramPositionView> draggedPositions;
            List<PositionSnapShot> expected;
            PlanogramPositionView anchorPosition;
            PlanogramSubComponentView targetView = CreateKeepSequenceOrderScenario(out draggedPositions, out expected, out anchorPosition);
            //  Correct Sequence as they should be added after the exiting one.
            foreach (PositionSnapShot shot in expected)
            {
                shot.SequenceX += 1;
            }

            targetView.AddPositionCopies(draggedPositions);

            CollectionAssert.AreEquivalent(expected, targetView.Positions.Select(PositionSnapShot.Take).Where(shot => draggedPositions.Any(view => Equals(view.Product.Gtin, shot.Gtin))).OrderBy(shot => shot.SequenceX).ToList(), "Each position should have the original sequence order.");
        }

        [Test]
        public void AddsPositionCopiesToRightOfAnchorKeepingSequenceX()
        {
            List<PlanogramPositionView> draggedPositions;
            List<PositionSnapShot> expected;
            PlanogramPositionView anchorPosition;
            PlanogramSubComponentView targetView = CreateKeepSequenceOrderScenario(out draggedPositions, out expected, out anchorPosition);
            //  Correct Sequence as they should be added after the exiting one.
            foreach (PositionSnapShot shot in expected)
            {
                shot.SequenceX += 1;
            }

            targetView.AddPositionCopies(draggedPositions, anchorPosition, PlanogramPositionAnchorDirection.ToRight);

            CollectionAssert.AreEquivalent(expected, targetView.Positions.Select(PositionSnapShot.Take).Where(shot => draggedPositions.Any(view => Equals(view.Product.Gtin, shot.Gtin))).OrderBy(shot => shot.SequenceX).ToList(), "Each position should have the original sequence order.");
        }

        [Test]
        public void AddsPositionCopiesToLeftOfAnchorKeepingSequenceX()
        {
            List<PlanogramPositionView> draggedPositions;
            List<PositionSnapShot> expected;
            PlanogramPositionView anchorPosition;
            PlanogramSubComponentView targetView = CreateKeepSequenceOrderScenario(out draggedPositions, out expected, out anchorPosition);

            targetView.AddPositionCopies(draggedPositions, anchorPosition, PlanogramPositionAnchorDirection.ToLeft);

            CollectionAssert.AreEquivalent(expected, targetView.Positions.Select(PositionSnapShot.Take).Where(shot => draggedPositions.Any(view => Equals(view.Product.Gtin, shot.Gtin))).OrderBy(shot => shot.SequenceX).ToList(), "Each position should have the original sequence order.");
        }

        #endregion

        [Test]
        public void PlanSubComponentRenderableGetDividerLinesReturnsEmptyWhenDividerSettingsEmpty()
        {
            PlanogramSubComponentView subView = GetEmptySubComponentViewWithoutDividerInfo();
            IPlanSubComponentRenderable sut = subView;

            List<RectValue> dividerLines = sut.GetDividerLines();

            Assert.That(dividerLines, Has.Count.EqualTo(0));
        }

        [Test]
        public void PlanSubComponentRenderableGetDividerLinesReturnsEmptyWhenObstructionSpacingXEqualsObstructionWidth()
        {
            PlanogramSubComponentView subView = GetEmptySubComponentViewWithoutDividerInfo();
            IPlanSubComponentRenderable sut = subView;
            subView.DividerObstructionHeight = 2;
            subView.DividerObstructionWidth = 2;
            subView.DividerObstructionDepth = 2;
            subView.DividerObstructionSpacingX = 2;
            subView.DividerObstructionWidth = 2;

            List<RectValue> dividerLines = sut.GetDividerLines();

            Assert.That(dividerLines, Has.Count.EqualTo(0));
        }

        [Test]
        public void PlanSubComponentRenderableGetDividerLinesReturnsLinesWhenObstructionSpacingXGreaterThanObstructionWidth()
        {
            PlanogramSubComponentView subView = GetEmptySubComponentViewWithoutDividerInfo();
            IPlanSubComponentRenderable sut = subView;
            subView.DividerObstructionHeight = 2;
            subView.DividerObstructionWidth = 2;
            subView.DividerObstructionDepth = 2;
            subView.DividerObstructionSpacingX = 4;
            subView.DividerObstructionWidth = 2;

            List<RectValue> dividerLines = sut.GetDividerLines();

            Assert.That(dividerLines, Has.Count.EqualTo(60));
        }

        #endregion
    }
}