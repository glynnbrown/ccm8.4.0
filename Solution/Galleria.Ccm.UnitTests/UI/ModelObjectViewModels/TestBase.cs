﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-24764 : L.Hodson
////  Created
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using NUnit.Framework;
//using Galleria.Framework.Planograms.Model;
//using Galleria.Ccm.Editor.Client.Wpf.Common;
//using Galleria.Ccm.Security;
//using Galleria.Framework.Dal;
//using System.ComponentModel;

//namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
//{
//    public abstract class TestBase
//    {
//        #region Fields
//        private string _dalName; // the dal name
//        private IDalFactory _dalFactory; // the dal factory to use for testing
//        private readonly List<String> _propertyChangedNotifications = new List<String>();
//        #endregion

//        #region Properties

//        protected List<String> PropertyChangedNotifications
//        {
//            get { return _propertyChangedNotifications; }
//        }

//        #endregion

//        #region Methods

//        [SetUp]
//        public virtual void Setup()
//        {
//            // generate a new dal name
//            _dalName = Guid.NewGuid().ToString();

//            // create a new, in memory dal factory
//            _dalFactory = new Galleria.Framework.Planograms.Dal.Mock.DalFactory();

//            // register this factory within the dal container
//            DalContainer.RegisterFactory(_dalName, _dalFactory);

//            // set this factory to be the default of the container
//            DalContainer.DalName = _dalName;

//            DomainPrincipal.Authenticate();
//        }

//        [TearDown]
//        public virtual void TearDown()
//        {

//        }

//        protected Object GetPropertyTestValue1(Type propertyType)
//        {
//            if (propertyType == typeof(Single) || propertyType == typeof(Single?))
//            {
//                return 2F;
//            }
//            else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?))
//            {
//                return 2;
//            }
//            else if (propertyType == typeof(Byte) || propertyType == typeof(Byte?))
//            {
//                return (Byte)2;
//            }
//            else if (propertyType == typeof(Int16) || propertyType == typeof(Int16?))
//            {
//                return (Int16)2;
//            }
//            else if (propertyType == typeof(Boolean) || propertyType == typeof(Boolean?))
//            {
//                return true;
//            }
//            else if (propertyType == typeof(String))
//            {
//                return "TEST";
//            }
//            else if (propertyType.IsEnum)
//            {
//                return Enum.Parse(propertyType, "1");
//            }

//            return null;
//        }

//        protected Object GetPropertyTestValue2(Type propertyType)
//        {
//            if (propertyType == typeof(Single) || propertyType == typeof(Single?))
//            {
//                return 3F;
//            }
//            else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?))
//            {
//                return 3;
//            }
//            else if (propertyType == typeof(Byte) || propertyType == typeof(Byte?))
//            {
//                return (Byte)3;
//            }
//            else if (propertyType == typeof(Int16) || propertyType == typeof(Int16?))
//            {
//                return (Int16)3;
//            }
//            else if (propertyType == typeof(Boolean) || propertyType == typeof(Boolean?))
//            {
//                return false;
//            }
//            else if (propertyType == typeof(String))
//            {
//                return "TEST2";
//            }
//            else if (propertyType.IsEnum)
//            {
//                return Enum.Parse(propertyType, "2");
//            }

//            return null;
//        }

//        #endregion

//        #region Event Handlers

//        protected void TestModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
//        {
//            //add to the change notification collection
//            _propertyChangedNotifications.Add(e.PropertyName);
//        }

//        #endregion

//    }
//}
