﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion

#region Version History: (CCM 8.2.0)
// V8-31384 : M.Shelley
//  Add a test (SetBase_MovesComponent) to check that if a base is added, that any existing components
//  that collide with the base are moved to eliminate the collision
#endregion

#endregion

using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    public sealed class PlanogramFixtureViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramFixtureView CreateView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture("f1");
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add(fixture);

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);


            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);



            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.Fixtures.First();
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> FixtureModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramFixture).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramFixture)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.StartsWith("Meta")) continue; //ignore meta properties

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> FixtureItemModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent", "PercentageX", "PercentageWidth", "BaySequenceNumber" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramFixtureItem).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramFixtureItem)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.StartsWith("Meta")) continue; //ignore meta properties

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("FixtureItemModelPropertyNames")]
        public void FixtureItemModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramFixtureItem).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramFixtureView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramFixtureView view = CreateView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(view.FixtureItemModel, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(view.FixtureItemModel, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }

        [Test, TestCaseSource("FixtureModelPropertyNames")]
        public void FixtureModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramFixture).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramFixtureView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramFixtureView view = CreateView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(view.FixtureModel, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(view.FixtureModel, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }

        #endregion

        #region Assembly Methods

        [Test]
        public void AddAssembly()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Assemblies.Count);

            //Add a new assembly using view
            PlanogramAssemblyView view1 = fixtureView.AddAssembly();
            Assert.AreEqual(1, fixtureView.Assemblies.Count);

            //add new assembly using model
            fixture.Assemblies.Add();
            Assert.AreEqual(2, fixtureView.Assemblies.Count);
        }

        [Test]
        public void AddAssemblyCopy()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Assemblies.Count);

            //Add a new assembly using view
            PlanogramAssemblyView view1 = fixtureView.AddAssembly();
            Assert.AreEqual(1, fixtureView.Assemblies.Count);
            view1.X = 999;
            view1.AddComponent(PlanogramComponentType.Peg);
            view1.AddComponent(PlanogramComponentType.Shelf);

            //add a copy
            PlanogramAssemblyView view2 = fixtureView.AddAssemblyCopy(view1);
            Assert.AreEqual(2, fixtureView.Assemblies.Count);
            Assert.AreEqual(view1.X, view2.X);
            Assert.AreEqual(view1.Components.Count, view2.Components.Count);
            Assert.AreNotEqual(view1.AssemblyModel, view2.AssemblyModel);
        }

        [Test]
        public void RemoveAssembly()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Assemblies.Count);

            //Add a new assemblies
            PlanogramAssemblyView view1 = fixtureView.AddAssembly();
            Assert.AreEqual(1, fixtureView.Assemblies.Count);

            PlanogramAssemblyView view2 = fixtureView.AddAssembly();
            Assert.AreEqual(2, fixtureView.Assemblies.Count);

            //remove using view
            fixtureView.RemoveAssembly(view1);
            Assert.AreEqual(1, fixtureView.Assemblies.Count);
            Assert.Contains(view2, fixtureView.Assemblies);

            //remove using model
            fixture.Assemblies.Remove(view2.FixtureAssemblyModel);
            Assert.AreEqual(0, fixtureView.Assemblies.Count);
        }

        [Test]
        public void RemoveAssembly_RemovesPlanogramAssembly()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Int32 count = plan.Assemblies.Count;

            //add a shelf with a child component
            var shelf = fixtureView.AddAssembly();
            Assert.AreEqual(count + 1, plan.Assemblies.Count);
            var childComponent = shelf.AddComponent(PlanogramComponentType.Chest);

            //remove it
            fixtureView.RemoveAssembly(shelf);
            Assert.AreEqual(count, plan.Assemblies.Count, "PlanogramAssembly was not removed.");
            Assert.AreEqual(0, plan.Components.Count, "Child component should have been removed too.");
        }

        [Test]
        public void MoveAssembly()
        {
            //Create a plan with 2 fixtures
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramFixtureItem fixtureItem1 = plan.FixtureItems.Add();
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);
            PlanogramFixtureAssembly assembly1 = fixture1.Assemblies.Add();

            PlanogramFixtureItem fixtureItem2 = plan.FixtureItems.Add();
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            PlanogramView planView = new PlanogramView(plan);

            PlanogramFixtureView fixtureView1 = planView.Fixtures[0];
            PlanogramFixtureView fixtureView2 = planView.Fixtures[1];
            PlanogramAssemblyView assemblyView = fixtureView1.Assemblies[0];

            Assert.AreEqual(1, fixtureView1.Assemblies.Count);
            Assert.AreEqual(0, fixtureView2.Assemblies.Count);

            //move
            fixtureView2.MoveAssembly(assemblyView);
            Assert.AreEqual(0, fixtureView1.Assemblies.Count);
            Assert.AreEqual(1, fixtureView2.Assemblies.Count);
            Assert.AreEqual(assemblyView.AssemblyModel, fixtureView2.Assemblies[0].AssemblyModel);

            assemblyView = fixtureView2.Assemblies[0];

            //move back
            fixtureView1.MoveAssembly(assemblyView);
            Assert.AreEqual(1, fixtureView1.Assemblies.Count);
            Assert.AreEqual(0, fixtureView2.Assemblies.Count);
            Assert.AreEqual(assemblyView.AssemblyModel, fixtureView1.Assemblies[0].AssemblyModel);
        }

        [Test]
        public void MoveAssembliesBetweenBays()
        {
            //GEM26569

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture with backboard and base
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture("f1");
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add(fixture);
            fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.First();
            assembly1.Components.Add(PlanogramComponentType.Shelf, 10, 10, 10);

            //add another fixture
            PlanogramFixture fixture2 = PlanogramFixture.NewPlanogramFixture("f1");
            fixture2.Height = 191;
            fixture2.Width = 120;
            fixture2.Depth = 100;
            plan.Fixtures.Add(fixture2);

            fixtureItem = plan.FixtureItems.Add(fixture2);
            fixtureItem.X = fixture.Width;


            PlanogramView planView = new PlanogramView(plan);


            //get the assembly
            PlanogramAssemblyView assembly = planView.Fixtures[0].Assemblies.FirstOrDefault();
            Assert.AreEqual(0, assembly.WorldX);
            assembly.AddComponent(PlanogramComponentType.Shelf);

            //Update the shelf world position to bay 2.
            assembly.WorldX = planView.Fixtures[1].X;

            //check the shefl has actually moved.
            Assert.IsNull(planView.Fixtures[0].Assemblies.FirstOrDefault());
            Assert.IsNotNull(planView.Fixtures[1].Assemblies.FirstOrDefault());

            Assert.AreEqual(planView.Fixtures[1].X, planView.Fixtures[1].Assemblies.FirstOrDefault().WorldX);
        }

        #endregion

        #region Component Methods

        [Test]
        public void AddComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Components.Count);

            //Add a new assembly using view
            PlanogramComponentView view1 = fixtureView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, fixtureView.Components.Count);

            //add new assembly using model
            fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            Assert.AreEqual(2, fixtureView.Components.Count);
        }

        [Test]
        public void AddComponentCopy()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Components.Count);

            //Add a new assembly using view
            PlanogramComponentView view1 = fixtureView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, fixtureView.Components.Count);
            view1.X = 999;
            view1.AddSubComponent();
            view1.AddSubComponent();

            //add a copy
            PlanogramComponentView view2 = fixtureView.AddComponentCopy(view1);
            Assert.AreEqual(2, fixtureView.Components.Count);
            Assert.AreEqual(view1.X, view2.X);
            Assert.AreEqual(view1.SubComponents.Count, view2.SubComponents.Count);
            Assert.AreNotEqual(view1.ComponentModel, view2.ComponentModel);
        }

        [Test]
        public void RemoveComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Assemblies.Count);

            //Add a new assemblies
            PlanogramComponentView view1 = fixtureView.AddComponent(PlanogramComponentType.Panel);
            Assert.AreEqual(1, fixtureView.Components.Count);

            PlanogramComponentView view2 = fixtureView.AddComponent(PlanogramComponentType.ClipStrip);
            Assert.AreEqual(2, fixtureView.Components.Count);

            //remove using view
            fixtureView.RemoveComponent(view1);
            Assert.AreEqual(1, fixtureView.Components.Count);
            Assert.Contains(view2, fixtureView.Components);

            //remove using model
            fixture.Components.Remove(view2.FixtureComponentModel);
            Assert.AreEqual(0, fixtureView.Components.Count);
        }

        [Test]
        public void RemoveComponent_RemovesPlanogramComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Int32 count = plan.Components.Count;

             //add a shelf
            var shelf = fixtureView.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(count + 1, plan.Components.Count);

             //remove it
            fixtureView.RemoveComponent(shelf);
            Assert.AreEqual(count, plan.Components.Count, "PlanogramComponent was not removed.");
         }

        [Test]
        public void MoveFixtureComponent()
        {
            //Create a plan with 2 fixtures
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramFixtureItem fixtureItem1 = plan.FixtureItems.Add();
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);
            PlanogramFixtureComponent component1 = fixture1.Components.Add(PlanogramComponentType.ClipStrip, 2, 2, 2);

            PlanogramFixtureItem fixtureItem2 = plan.FixtureItems.Add();
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);

            PlanogramView planView = new PlanogramView(plan);

            PlanogramFixtureView fixtureView1 = planView.Fixtures[0];
            PlanogramFixtureView fixtureView2 = planView.Fixtures[1];
            PlanogramComponentView componentView = fixtureView1.Components[0];

            Assert.AreEqual(1, fixtureView1.Components.Count);
            Assert.AreEqual(0, fixtureView2.Components.Count);

            //move
            fixtureView2.MoveComponent(componentView);
            Assert.AreEqual(0, fixtureView1.Components.Count);
            Assert.AreEqual(1, fixtureView2.Components.Count);
            Assert.AreEqual(componentView.ComponentModel, fixtureView2.Components[0].ComponentModel);

            componentView = fixtureView2.Components[0];

            //move back
            fixtureView1.MoveComponent(componentView);
            Assert.AreEqual(1, fixtureView1.Components.Count);
            Assert.AreEqual(0, fixtureView2.Components.Count);
            Assert.AreEqual(componentView.ComponentModel, fixtureView1.Components[0].ComponentModel);
        }

        [Test]
        public void MoveAssemblyComponent()
        {
            //Create a plan with 2 fixtures
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramFixtureItem fixtureItem1 = plan.FixtureItems.Add();
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            PlanogramFixtureAssembly fixtureAssembly = fixture1.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);
            PlanogramAssemblyComponent component1 = assembly.Components.Add(PlanogramComponentType.ClipStrip, 2, 2, 2);
            PlanogramAssemblyComponent component2 = assembly.Components.Add(PlanogramComponentType.ClipStrip, 2, 2, 2);

            PlanogramFixtureItem fixtureItem2 = plan.FixtureItems.Add();
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);

            PackageViewModel packageVM = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVM.PlanogramViews.First();

            PlanogramFixtureView fixtureView1 = planView.Fixtures[0];
            PlanogramFixtureView fixtureView2 = planView.Fixtures[1];
            PlanogramComponentView componentView = fixtureView1.Assemblies[0].Components[0];

            //move
            fixtureView2.MoveComponent(componentView);
            Assert.AreEqual(1, fixtureView1.Assemblies[0].Components.Count, "Should only have 1 component left.");
            Assert.AreEqual(1, fixtureView2.Components.Count);
            Assert.AreEqual(componentView.ComponentModel, fixtureView2.Components[0].ComponentModel);

        }

        [Test]
        public void MoveAssemblyComponent_LastComponent()
        {
            //Create a plan with 2 fixtures
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramFixtureItem fixtureItem1 = plan.FixtureItems.Add();
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            PlanogramFixtureAssembly fixtureAssembly = fixture1.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);
            PlanogramAssemblyComponent component1 = assembly.Components.Add(PlanogramComponentType.ClipStrip, 2, 2, 2);

            PlanogramFixtureItem fixtureItem2 = plan.FixtureItems.Add();
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);

            PackageViewModel packageVM = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVM.PlanogramViews.First();

            PlanogramFixtureView fixtureView1 = planView.Fixtures[0];
            PlanogramFixtureView fixtureView2 = planView.Fixtures[1];
            PlanogramComponentView componentView = fixtureView1.Assemblies[0].Components[0];

            //move
            fixtureView2.MoveComponent(componentView);
            Assert.AreEqual(0, fixtureView1.Assemblies.Count, "Assembly should have been completely removed.");
            Assert.AreEqual(1, fixtureView2.Components.Count);
            Assert.AreEqual(componentView.ComponentModel, fixtureView2.Components[0].ComponentModel);

        }

        [Test]
        public void SetBase()
        {
            //Create a new fixture without a base
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture("f1");
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add(fixture);

            PlanogramView planView = new PlanogramView(plan);
            var fixtureView = planView.Fixtures.First();


            //call method to add base
            PlanogramComponentView baseComponent = fixtureView.SetBase(true);
            Assert.Contains(baseComponent, fixtureView.Components, "Base should have been added");
            Assert.AreEqual(PlanogramComponentType.Base, baseComponent.ComponentType);
            Assert.AreEqual(1, fixtureView.Components.Count);

            //set a value on it
            baseComponent.Depth = 51;

            //remove it
            fixtureView.SetBase(false);
            Assert.AreEqual(0, fixtureView.Components.Count, "Base should have been removed");

            //readd it
            PlanogramComponentView baseComponent2 = fixtureView.SetBase(true);
            Assert.Contains(baseComponent2, fixtureView.Components, "Base should have been added");
            Assert.AreEqual(51, baseComponent2.Depth, "Value should have been remembered");
        }

        [Test]
        public void SetBase_MovesComponent()
        {
            /// Test to check when a base is added to a fixture that already has components at the base of the fixture
            /// that the components are moved above the base.

            // Create a plan with a fixture but without a base
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Width = 120;

            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Int32 assemblyCount = plan.Assemblies.Count;

            // add a shelf with a child component
            var shelf1 = fixtureView.AddAssembly();
            assemblyCount++;
            Assert.AreEqual(assemblyCount, plan.Assemblies.Count);
            var childComponent1 = shelf1.AddComponent(PlanogramComponentType.Chest);

            // set the component position at the bottom of the fixture
            childComponent1.Y = 1.0F;
            childComponent1.Height = 5.0F;

            // add a shelf with a child component
            var shelf2 = fixtureView.AddAssembly();
            assemblyCount++;
            Assert.AreEqual(assemblyCount, plan.Assemblies.Count);
            var childComponent2 = shelf2.AddComponent(PlanogramComponentType.Shelf);

            // set the component position at the bottom of the fixture
            childComponent2.Y = 7.0F;
            childComponent2.Height = 4.0F;

            // call method to add base
            PlanogramComponentView baseComponent = fixtureView.SetBase(true);
            Assert.Contains(baseComponent, fixtureView.Components, "Base should have been added");
            Assert.AreEqual(PlanogramComponentType.Base, baseComponent.ComponentType);
            Assert.AreEqual(1, fixtureView.Components.Count);

            // check that the components have been moved to prevent a collision
            Assert.AreEqual(11.0F, childComponent1.Y, "Child component should have been moved above the base.");
            Assert.AreEqual(17.0F, childComponent2.Y, "Child component should have been moved above the base.");
        }

        [Test]
        public void SetBackboard()
        {
            //Create a new fixture without a backboard
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture("f1");
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add(fixture);

            PlanogramView planView = new PlanogramView(plan);
            var fixtureView = planView.Fixtures.First();

            //call method to add backboard
            PlanogramComponentView backboardComponent = fixtureView.SetBackboard(true);
            Assert.Contains(backboardComponent, fixtureView.Components, "Backboard should have been added");
            Assert.AreEqual(PlanogramComponentType.Backboard, backboardComponent.ComponentType);
            Assert.AreEqual(1, fixtureView.Components.Count);

            //set a value on it
            backboardComponent.Depth = 51;

            //remove it
            fixtureView.SetBackboard(false);
            Assert.AreEqual(0, fixtureView.Components.Count, "Backboard should have been removed");

            //readd it
            PlanogramComponentView backboardComponent2 = fixtureView.SetBackboard(true);
            Assert.Contains(backboardComponent2, fixtureView.Components, "Backboard should have been added");
            Assert.AreEqual(51, backboardComponent2.Depth, "Value should have been remembered");
        }

        #endregion

        #region Annotation Methods

        [Test]
        public void AddAnnotation()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Annotations.Count);

            //Add a new using view
            PlanogramAnnotationView view1 = fixtureView.AddAnnotation();
            Assert.AreEqual(1, fixtureView.Annotations.Count);

            //add new using model
            fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());
            Assert.AreEqual(2, fixtureView.Annotations.Count);
        }

        [Test]
        public void AddAnnotationCopy()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            //Add a new using view
            PlanogramAnnotationView view1 = fixtureView.AddAnnotation();
            Assert.AreEqual(1, fixtureView.Annotations.Count);
            view1.Text = "TTT";

            //Add copy
            PlanogramAnnotationView view2 = fixtureView.AddAnnotationCopy(view1);
            Assert.AreEqual(2, fixtureView.Annotations.Count);
            Assert.AreEqual(view1.Text, view2.Text);
            Assert.AreNotEqual(view1.Model, view2.Model);
        }

        [Test]
        public void RemoveAnnotation()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            Assert.AreEqual(0, fixtureView.Annotations.Count);

            //Add a new using view
            PlanogramAnnotationView view1 = fixtureView.AddAnnotation();
            PlanogramAnnotationView view2 = fixtureView.AddAnnotation();
            Assert.AreEqual(2, fixtureView.Annotations.Count);
            
            //remove using view
            fixtureView.RemoveAnnotation(view1);
            Assert.AreEqual(1, fixtureView.Annotations.Count);
            Assert.Contains(view2, fixtureView.Annotations);

            //remove using model
            plan.Annotations.Remove(view2.Model);
            Assert.AreEqual(0, fixtureView.Annotations.Count);
        }

        [Test]
        public void MoveAnnotation()
        {
            //Create a plan with 2 fixtures
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramFixtureItem fixtureItem1 = plan.FixtureItems.Add();
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);
            PlanogramAnnotation anno1 = fixtureItem1.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            PlanogramFixtureItem fixtureItem2 = plan.FixtureItems.Add();
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);

            PlanogramView planView = new PlanogramView(plan);

            PlanogramFixtureView fixtureView1 = planView.Fixtures[0];
            PlanogramFixtureView fixtureView2 = planView.Fixtures[1];
            PlanogramAnnotationView annoView = fixtureView1.Annotations[0];

            Assert.AreEqual(1, fixtureView1.Annotations.Count);
            Assert.AreEqual(0, fixtureView2.Annotations.Count);

            //move
            fixtureView2.MoveAnnotation(annoView);
            Assert.AreEqual(0, fixtureView1.Annotations.Count);
            Assert.AreEqual(1, fixtureView2.Annotations.Count);
            Assert.AreEqual(annoView.Model, fixtureView2.Annotations[0].Model);

            annoView = fixtureView2.Annotations[0];

            //move back
            fixtureView1.MoveAnnotation(annoView);
            Assert.AreEqual(1, fixtureView1.Annotations.Count);
            Assert.AreEqual(0, fixtureView2.Annotations.Count);
            Assert.AreEqual(annoView.Model, fixtureView1.Annotations[0].Model);

        }

        #endregion

        #region IPlanFixtureRenderable

        [Test]
        public void IPlanFixtureRenderableCollections()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            IPlanFixtureRenderable planFixture = fixtureView as IPlanFixtureRenderable;
            Assert.IsNotNull(planFixture);

            Assert.AreEqual(fixtureView.Assemblies, planFixture.Assemblies);
            Assert.AreEqual(fixtureView.Components, planFixture.Components);
            Assert.AreEqual(fixtureView.Annotations, planFixture.Annotations);
        }

        #endregion

        #region Constructor

        [Test]
        public void Constructor()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);
            PlanogramAssemblyComponent ac1 = assembly.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramAssemblyComponent ac2 = assembly.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);

            //add a fixture annotation
            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno1.PlanogramFixtureItemId = fixtureItem.Id;
            anno1.Text = "Annotation 1";
            plan.Annotations.Add(anno1);


            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixtureView = planView.Fixtures[0];

            //check view
            Assert.AreEqual(fixtureItem, fixtureView.FixtureItemModel);
            Assert.AreEqual(fixture, fixtureView.FixtureModel);

            //check components
            Assert.AreEqual(1, fixtureView.Components.Count);
            Assert.AreEqual(backboardFC, fixtureView.Components[0].FixtureComponentModel);

            //check assemblies
            Assert.AreEqual(1, fixtureView.Assemblies.Count);
            Assert.AreEqual(fixtureAssembly, fixtureView.Assemblies[0].FixtureAssemblyModel);

            //check annotations
            Assert.AreEqual(1, fixtureView.Annotations.Count);
            Assert.AreEqual(anno1, fixtureView.Annotations[0].Model);

            //check parents
            Assert.AreEqual(plan, fixtureView.Planogram.Model);
        }

        #endregion
    }
}
