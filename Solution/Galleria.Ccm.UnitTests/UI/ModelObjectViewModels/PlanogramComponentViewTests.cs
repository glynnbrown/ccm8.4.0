﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class PlanogramComponentViewTests : TestBase
    {
        #region Test Fixture Helpers

        private static PlanogramComponentView CreateFixtureComponentView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC =
                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);



            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllComponents().First(c=> c.FixtureComponentModel != null);
        }

        private static PlanogramComponentView CreateAssemblyComponentView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a backboard
            PlanogramFixtureComponent backboardFC =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC =
                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC =
                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create sub component placement
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);

            //add a fixture annotation
            PlanogramAnnotation anno1 = fixtureItem.AddAnnotation(PlanogramSettings.NewPlanogramSettings());

            //add an assembly
            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

            //add an assembly component
            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

            //add a subcomponent annotation
            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
            anno2.PlanogramFixtureItemId = fixtureItem.Id;
            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
            anno2.Text = "Annotation 2";
            plan.Annotations.Add(anno2);



            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            return planView.EnumerateAllComponents().First(c => c.AssemblyComponentModel != null);
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> ComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { 
                    "Parent",
                    "ImageIdFront", "ImageIdBack", "ImageIdTop", "ImageIdBottom", "ImageIdLeft", "ImageIdRight",
                    "IsMerchandisable"
                };

                foreach (PropertyInfo modelProperty in typeof(PlanogramComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.StartsWith("Meta")) continue; //ignore meta properties
                        if (modelProperty.Name.EndsWith("Async")) continue;

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> FixtureComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent", "NotchNumber" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramFixtureComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramFixtureComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.Name.StartsWith("Meta")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.EndsWith("Async")) continue;

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<String> AssemblyComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent", "NotchNumber", "ComponentSequenceNumber" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramAssemblyComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramAssemblyComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        if (modelProperty.Name.StartsWith("Meta")) continue; //ignore meta properties

                        yield return modelProperty.Name;
                    }
                }
            }
        }

        #endregion

        #region Property Tests

        [Test, TestCaseSource("ComponentModelPropertyNames")]
        public void Property_ComponentModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramComponent).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramComponentView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramComponentView view = CreateFixtureComponentView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(view.ComponentModel, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(view.ComponentModel, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Ignore("Property missing from view");
            }

        }

        [Test, TestCaseSource("FixtureComponentModelPropertyNames")]
        public void Property_FixtureComponentModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramFixtureComponent).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramComponentView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramComponentView view = CreateFixtureComponentView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                PlanogramFixtureComponent model = view.FixtureComponentModel;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(model, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(model, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }

        [Test, TestCaseSource("AssemblyComponentModelPropertyNames")]
        public void Property_AssemblyComponentModelPropertyGetSet(String propertyName)
        {
            PropertyInfo modelProperty = typeof(PlanogramAssemblyComponent).GetProperty(propertyName);
            PropertyInfo viewProperty = typeof(PlanogramComponentView).GetProperty(propertyName);

            if (viewProperty != null)
            {
                PlanogramComponentView view = CreateAssemblyComponentView();
                view.PropertyChanged += base.TestModel_PropertyChanged;

                PlanogramAssemblyComponent model = view.AssemblyComponentModel;

                //Test get
                Assert.AreEqual(modelProperty.GetValue(model, null),
                    viewProperty.GetValue(view, null),
                    String.Format("{0} get failed", viewProperty.Name));

                //Test set
                if (viewProperty.CanWrite)
                {
                    base.PropertyChangedNotifications.Clear();

                    Object newValue = GetPropertyTestValue1(viewProperty.PropertyType);
                    viewProperty.SetValue(view, newValue, null);

                    Assert.AreEqual(newValue, modelProperty.GetValue(model, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.AreEqual(newValue, viewProperty.GetValue(view, null),
                    String.Format("{0} set failed", viewProperty.Name));

                    Assert.Contains(viewProperty.Name, base.PropertyChangedNotifications,
                        String.Format("{0} property change not fired", viewProperty.Name));
                }


                view.PropertyChanged -= base.TestModel_PropertyChanged;


            }
            else
            {
                Assert.Fail("Property missing from view - include it or exclude it.");
            }

        }


        [Test]
        public void Property_WorldSpaceProperties_AreInitialized()
        {
            var view = CreateFixtureComponentView();

            PointValue expectedValue = 
                view.FixtureComponentModel.GetPlanogramRelativeCoordinates(view.Fixture.FixtureItemModel);

            RotationValue expectedRotation =
                view.FixtureComponentModel.GetPlanogramRelativeRotation(view.Fixture.FixtureItemModel);


            Assert.AreEqual(expectedValue.X, view.MetaWorldX);
            Assert.AreEqual(expectedValue.Y, view.MetaWorldY);
            Assert.AreEqual(expectedValue.Z, view.MetaWorldZ);

            Assert.AreEqual(expectedRotation.Slope, view.MetaWorldSlope);
            Assert.AreEqual(expectedRotation.Angle, view.MetaWorldAngle);
            Assert.AreEqual(expectedRotation.Roll, view.MetaWorldRoll);
        }

        [Test]
        public void Property_WorldSpaceProperties_UpdatedByFixtureMove()
        {
            var view = CreateFixtureComponentView();
            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the fixture
            view.Fixture.X += 10;

            Assert.Contains(PlanogramFixtureComponent.MetaWorldXProperty.Name, base.PropertyChangedNotifications);

            //Only x has changed.
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            //Assert.Contains(PlanogramFixtureComponent.MetaWorldSlopeProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldAngleProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldRollProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_WorldSpaceProperties_UpdatedByAssemblyMove()
        {
            var view = CreateAssemblyComponentView();

            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the assembly
            view.Assembly.X += 10;

            Assert.Contains(PlanogramFixtureComponent.MetaWorldXProperty.Name, base.PropertyChangedNotifications);

            //Only x has changed.
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            //Assert.Contains(PlanogramFixtureComponent.MetaWorldSlopeProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldAngleProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldRollProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_WorldSpaceProperties_UpdatedByComponentMove()
        {
            var view = CreateFixtureComponentView();
            view.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            //move the fixture
            view.X += 10;

            Assert.Contains(PlanogramFixtureComponent.MetaWorldXProperty.Name, base.PropertyChangedNotifications);

            //Only x has changed.
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldYProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldZProperty.Name, base.PropertyChangedNotifications);

            //Assert.Contains(PlanogramFixtureComponent.MetaWorldSlopeProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldAngleProperty.Name, base.PropertyChangedNotifications);
            //Assert.Contains(PlanogramFixtureComponent.MetaWorldRollProperty.Name, base.PropertyChangedNotifications);

            view.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void Property_NotchNumber()
        {
            //V8-27634 - Ensure notch number property behaves.

            //create a simple notched fixture with a shelf.
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);

            var fixture = planView.Fixtures.FirstOrDefault();
            if (fixture == null) fixture = planView.AddFixture();

            fixture.Height = 200;
            fixture.Width = 120;
            fixture.Depth = 76;

            var backboard = fixture.SetBackboard(true);
            backboard.SubComponents[0].IsNotchPlacedOnFront = true;
            backboard.SubComponents[0].NotchHeight = 1;
            backboard.SubComponents[0].NotchWidth = 2;
            backboard.SubComponents[0].NotchStartX = 2;
            backboard.SubComponents[0].NotchStartY = 20;
            backboard.SubComponents[0].NotchSpacingY = 4;

            fixture.SetBase(true);


            var shelf = fixture.AddComponent(PlanogramComponentType.Shelf);
            shelf.Y = 16;
            Assert.AreEqual(1, shelf.NotchNumber, "Notch number wrong");

            //change the notch number
            shelf.PropertyChanged += base.TestModel_PropertyChanged;
            base.PropertyChangedNotifications.Clear();

            shelf.NotchNumber = 30;
            Assert.AreEqual(30, shelf.NotchNumber, "Should be at notch 30");
            Assert.AreEqual(132, shelf.Y, "Shelf y pos incorrect");
            Assert.Contains("NotchNumber", base.PropertyChangedNotifications, "Property change notification not fired");
 
            // set the y position and make sure notch also updates.
            base.PropertyChangedNotifications.Clear();
            shelf.Y = 108;
            Assert.AreEqual(108, shelf.Y, "Y value incorrect");
            Assert.AreEqual(24, shelf.NotchNumber, "Notch number incorrect");
            Assert.Contains("NotchNumber", base.PropertyChangedNotifications, "Property change notification not fired");

            shelf.PropertyChanged -= base.TestModel_PropertyChanged;

            //set notch number below 0
            shelf.NotchNumber = -1;
            Assert.AreEqual(-1, shelf.NotchNumber, "Should be at notch -1");
            Assert.AreEqual(8, shelf.Y, "Shelf y pos incorrect");
        }


        #endregion

        #region Methods

        [Test]
        public void ConstructorFixtureComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 2, 2, 2);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramComponentView view = planView.Fixtures[0].Components[0];

            Assert.AreEqual(component, view.ComponentModel);
            Assert.IsNull(view.AssemblyComponentModel);
            Assert.AreEqual(fc, view.FixtureComponentModel);

            Assert.AreEqual(component.SubComponents.Count, view.SubComponents.Count);

            Assert.AreEqual(plan, view.Planogram.Model);
        }

        [Test]
        public void ConstructorAssemblyComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureAssembly fa = fixture.Assemblies.Add();
            PlanogramAssembly assembly = plan.Assemblies.FindById(fa.PlanogramAssemblyId);

            PlanogramAssemblyComponent ac = assembly.Components.Add(PlanogramComponentType.Shelf, 2, 2, 2);
            PlanogramComponent component = plan.Components.FindById(ac.PlanogramComponentId);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramComponentView view = planView.Fixtures[0].Assemblies[0].Components[0];

            Assert.AreEqual(component, view.ComponentModel);
            Assert.IsNull(view.FixtureComponentModel);
            Assert.AreEqual(ac, view.AssemblyComponentModel);

            Assert.AreEqual(component.SubComponents.Count, view.SubComponents.Count);

            Assert.AreEqual(plan, view.Planogram.Model);
        }

        [Test]
        public void AddSubComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 2, 2, 2);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramComponentView componentView = planView.Fixtures[0].Components[0];

            Assert.AreEqual(1, componentView.SubComponents.Count);

            //Add new view
            PlanogramSubComponentView view1 = componentView.AddSubComponent();
            Assert.AreEqual(2, componentView.SubComponents.Count);

            //add new model
            component.SubComponents.Add(PlanogramSubComponent.NewPlanogramSubComponent());
            Assert.AreEqual(3, componentView.SubComponents.Count);
        }

        [Test]
        public void AddSubComponentCopy()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 2, 2, 2);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramComponentView componentView = planView.Fixtures[0].Components[0];

            Assert.AreEqual(1, componentView.SubComponents.Count);

            PlanogramSubComponentView view1 = componentView.SubComponents[0];
            view1.NotchWidth = 99;

            PlanogramSubComponentView view2 = componentView.AddSubComponentCopy(view1);
            Assert.AreEqual(2, componentView.SubComponents.Count);
            Assert.AreEqual(view1.NotchWidth, view2.NotchWidth);
            Assert.AreNotEqual(view1.Model, view2.Model);
        }

        [Test]
        public void AddSubComponentCopy_WithPositions()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 2, 2, 2);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramComponentView componentView = planView.Fixtures[0].Components[0];
            PlanogramSubComponentView subView = componentView.SubComponents[0];

            //add a product to the shelf
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            plan.Products.Add(prod1);

            subView.AddPosition(planView.Products[0]);


            PlanogramSubComponentView view2 = componentView.AddSubComponentCopy(subView, /*copy positions*/true);
            Assert.AreEqual(2, componentView.SubComponents.Count);
            Assert.AreEqual(1, view2.Positions.Count);
            Assert.AreNotEqual(subView.Model, view2.Model);
        }

        [Test]
        public void RemoveSubComponent()
        {
            //Create a plan with a fixture
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 2, 2, 2);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = pkView.PlanogramViews.First();
            PlanogramComponentView componentView = planView.Fixtures[0].Components[0];

            PlanogramSubComponentView view1 = componentView.SubComponents[0];
            PlanogramSubComponentView view2 = componentView.AddSubComponent();
            Assert.AreEqual(2, componentView.SubComponents.Count);

            //remove view
            componentView.RemoveSubComponent(view1);
            Assert.AreEqual(1, componentView.SubComponents.Count);

            //remove model
            component.SubComponents.Remove(view2.Model);
            Assert.AreEqual(0, componentView.SubComponents.Count);
        }


        #endregion

        [Test]
        public void MoveComponentBetweenBays()
        {
            //GEM26569

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture with backboard and base
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture("f1");
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add(fixture);

            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            plan.Products.Add(prod1);

            // create a sub component placement
            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);


            //add another fixture
            PlanogramFixture fixture2 = PlanogramFixture.NewPlanogramFixture("f1");
            fixture2.Height = 191;
            fixture2.Width = 120;
            fixture2.Depth = 100;
            plan.Fixtures.Add(fixture2);

            fixtureItem = plan.FixtureItems.Add(fixture2);
            fixtureItem.X = fixture.Width;

            backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;
            baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = packageVm.PlanogramViews.First();

            //get the shelf
            PlanogramComponentView shelf = planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            Assert.AreEqual(0, shelf.MetaWorldX);
            Assert.AreEqual(1, shelf.SubComponents[0].Positions.Count);

            //Update the shelf world position to bay 2.
            shelf.MetaWorldX = planView.Fixtures[1].X;

            //check the shefl has actually moved.
            Assert.IsNull(planView.Fixtures[0].Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Shelf));
            Assert.IsNotNull(planView.Fixtures[1].Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Shelf));

            shelf = planView.Fixtures[1].Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Shelf);
            Assert.AreEqual(planView.Fixtures[1].X, shelf.MetaWorldX);

            //check that the position is still in place
            Assert.AreEqual(1, shelf.SubComponents[0].Positions.Count);
        }

        [Test]
        public void MoveComponentBetweenBaysWithLinkedTextBox()
        {
            //GEM32563 - Checks that when a component is moved between bays,
            // it also takes any linked textboxes.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture with backboard and base
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture("f1");
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            plan.Fixtures.Add(fixture);
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add(fixture);

            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            //Add a linked textbox.
            PlanogramAnnotation linkedAnno = PlanogramAnnotation.NewPlanogramAnnotation(shelf1FC, fixtureItem, PlanogramSettings.NewPlanogramSettings());
            linkedAnno.Text = "test";
            linkedAnno.Height = 20;
            linkedAnno.Width = 100;
            linkedAnno.Depth = 1;
            linkedAnno.X = -linkedAnno.Width;
            plan.Annotations.Add(linkedAnno);

            //create the plan view
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = packageVm.PlanogramViews.First();

            //move the component
            var shelf = planView.Fixtures[0].Components.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Shelf);
            Assert.AreEqual(1, shelf.Annotations.Count, "Should have 1 annotation");

            //add another fixture
            var fixture2 = planView.AddFixture();
            var newShelf = fixture2.MoveComponent(shelf);

            //check
            Assert.AreEqual(1, newShelf.Annotations.Count, "Should have 1 annotation");

        }
    }
}
