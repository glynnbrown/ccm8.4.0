﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using System.IO;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public class ProductLibraryViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private String CreateProductLibrary()
        {
            String libraryPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            libraryPath = Path.ChangeExtension(libraryPath, Ccm.Model.ProductLibrary.FileExtension);

            Ccm.Model.ProductLibrary library = Ccm.Model.ProductLibrary.NewProductLibrary();
            library.ExcelWorkbookSheet = "Test";
            library.ColumnMappings.Add(Ccm.Model.ProductLibraryColumnMapping.NewProductLibraryColumnMapping());

            library.ColumnMappings[0].CanDefault = true;
            library.ColumnMappings[0].Default = null;
            library.ColumnMappings[0].Destination = "Test"; 
            library.ColumnMappings[0].Max = 25;
            library.ColumnMappings[0].Min = 0;
            library.ColumnMappings[0].Source = "Test"; 
            library.ColumnMappings[0].Type = "String";

            library = library.SaveAsFile(libraryPath);
            library.Dispose();

            return libraryPath;
        }

        #endregion

        [Test]
        public void Create()
        {
            ProductLibraryViewModel viewModel = new ProductLibraryViewModel();
            Ccm.Model.ProductLibrary model = Ccm.Model.ProductLibrary.NewProductLibrary();

            viewModel.Model = model;
            Assert.IsNotNull(model);
            Assert.IsTrue(model.IsNew);
            Assert.IsTrue(model.IsInitialized);
        }

        [Test]
        public void FetchById()
        {
            String filePath = CreateProductLibrary();
            ProductLibraryViewModel viewModel = new ProductLibraryViewModel();
            viewModel.FetchById(filePath, /*asReadonly*/false);

            Ccm.Model.ProductLibrary model = viewModel.Model;
            Assert.IsNotNull(model);
            Assert.IsFalse(model.IsNew);
            Assert.AreEqual(1, model.ColumnMappings.Count);

        }

        [Test]
        public void SaveAsFile()
        {
            String filePath = CreateProductLibrary();
            ProductLibraryViewModel viewModel = new ProductLibraryViewModel();
            viewModel.FetchById(filePath, /*asReadonly*/false);

            Ccm.Model.ProductLibrary model = viewModel.Model;
            Assert.IsNotNull(model);
            Assert.AreEqual(1, model.ColumnMappings.Count);

            //Amend
            viewModel.Model.ColumnMappings.Add(Ccm.Model.ProductLibraryColumnMapping.NewProductLibraryColumnMapping());
            //Set properties
            viewModel.Model.ColumnMappings[1].CanDefault = true;
            viewModel.Model.ColumnMappings[1].Default = null;
            viewModel.Model.ColumnMappings[1].Destination = "Test2";
            viewModel.Model.ColumnMappings[1].Max = 25;
            viewModel.Model.ColumnMappings[1].Min = 0;
            viewModel.Model.ColumnMappings[1].Source = "Test2";
            viewModel.Model.ColumnMappings[1].Type = "String";
            //Save
            viewModel.SaveAsFile(filePath);

            model = viewModel.Model;
            Assert.IsNotNull(model);
            Assert.IsFalse(model.IsNew);
            Assert.AreEqual(2, model.ColumnMappings.Count);

            //refetch the file model
            ProductLibraryViewModel viewModel2 = new ProductLibraryViewModel();
            viewModel2.FetchById(filePath, /*asReadonly*/false);
            Assert.IsNotNull(viewModel2.Model);
        }

        [Test]
        public void GetConnectedProductLibraryType()
        {
            Ccm.Model.ProductLibraryDalFactoryType type = ProductLibraryHelper.GetConnectedProductLibraryType();
            Assert.AreEqual(Ccm.Model.ProductLibraryDalFactoryType.FileSystem, type);
        }

        [Test]
        public void ResolveFilePathFromProductLibraryId()
        {
            String id = Path.Combine(this.TestDir, "TestProductLibraryId.POGPL");

            String path = ProductLibraryHelper.ResolveFilePathFromProductLibraryId(id);

            //No file data exists or can be created for test
            Assert.AreEqual(null, path);
        }

        [Test]
        public void ResolveProductLibraryIdFromFilePath()
        {
            String path = Path.Combine(this.TestDir, "TestProductLibraryId.XLS");

            Object productLibraryId = ProductLibraryHelper.ResolveProductLibraryIdFromFilePath(path);

            Assert.AreEqual(Path.Combine(this.TestDir, "TestProductLibraryId.pogpl"), productLibraryId);
        }
    }
}
