﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Model;


namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public sealed class RecentPlanogramListViewTests : TestBase
    {
        [Test]
        public void FetchAll()
        {
            //add a few items to the recent list
            RecentPlanogramList list = RecentPlanogramList.FetchAll();

            RecentPlanogram item1 = RecentPlanogram.NewRecentPlanogram();
            item1.Name = "item1";
            item1.Type = RecentPlanogramType.Pog;
            item1.Location = "loc1";
            item1.DateLastAccessed = DateTime.UtcNow;
            list.Add(item1);

            RecentPlanogram item2 = RecentPlanogram.NewRecentPlanogram();
            item2.Name = "item2";
            item2.Type = RecentPlanogramType.Pog;
            item2.Location = "loc2";
            item2.DateLastAccessed = DateTime.UtcNow;
            list.Add(item2);

            list.Save();

            //load
            RecentPlanogramListView view = new RecentPlanogramListView();
            view.FetchAll();
            Assert.AreEqual(2, view.Model.Count);

        }

        [Test]
        public void Save()
        {
            RecentPlanogramListView view = new RecentPlanogramListView();
            view.FetchAll();

            RecentPlanogram item1 = RecentPlanogram.NewRecentPlanogram();
            item1.Name = "item1";
            item1.Type = RecentPlanogramType.Pog;
            item1.Location = "loc1";
            item1.DateLastAccessed = DateTime.UtcNow;
            view.Model.Add(item1);

            view.Save();

            Assert.AreEqual(1, view.Model.Count);
            view.Dispose();

            //refetch
            view = new RecentPlanogramListView();
            view.FetchAll();
            Assert.AreEqual(1, view.Model.Count);
        }

        [Test]
        public void Add()
        {
            RecentPlanogramListView view = new RecentPlanogramListView();
            view.FetchAll();

            String packageName = "item1";
            RecentPlanogramType packageType = RecentPlanogramType.Pog;
            String location = "loc1";

            view.AddFromFileSystem(packageName, location);

            Assert.AreEqual(1, view.Model.Count);
            Assert.AreEqual(packageName, view.Model[0].Name);
            Assert.AreEqual(packageType, view.Model[0].Type);
            Assert.AreEqual(location, view.Model[0].Location);
            Assert.AreNotEqual(new DateTime(), view.Model[0].DateLastAccessed);
        }

        [Test]
        public void AddUpdatesExisting()
        {
            RecentPlanogramListView view = new RecentPlanogramListView();
            view.FetchAll();

            RecentPlanogram item1 = RecentPlanogram.NewRecentPlanogram();
            item1.Name = "item1";
            item1.Type = RecentPlanogramType.Pog;
            item1.Location = "loc1";
            item1.DateLastAccessed = DateTime.UtcNow.Subtract(new TimeSpan(1,0,0));
            view.Model.Add(item1);

            RecentPlanogram item2 = RecentPlanogram.NewRecentPlanogram();
            item2.Name = "item2";
            item2.Type = RecentPlanogramType.Pog;
            item2.Location = "loc2";
            item2.DateLastAccessed = DateTime.UtcNow.Subtract(new TimeSpan(0, 30, 0));
            view.Model.Add(item2);


            //re-add the first item
            DateTime oldDate = item1.DateLastAccessed;
            view.AddFromFileSystem(item1.Name, item1.Location);

            Assert.AreEqual(2, view.Model.Count);
            Assert.AreNotEqual(oldDate, item1.DateLastAccessed);
        }

        [Test]
        public void AddMaxItemCount()
        {
            RecentPlanogramListView view = new RecentPlanogramListView();
            view.FetchAll();

            for (Int32 i = 0; i < view.MaxItemCount; i++)
            {
                view.AddFromFileSystem("item" + i, "loc"+i);

                Assert.AreEqual(i + 1, view.Model.Count);
            }

            //now add above the count
            Int32 count = view.Model.Count;
            for (Int32 i = 0; i < 10; i++)
            {
                view.AddFromFileSystem("item" + count, "loc" + count);

                Assert.AreEqual(view.MaxItemCount, view.Model.Count);
                count++;
            }
        }
    }
}
