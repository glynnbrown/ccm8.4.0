﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24979 : L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Csla.Core;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public class FixturePackageViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private static FixturePackage Create2FixtureItemsPackage()
        {
            FixturePackage package = FixturePackage.NewFixturePackage();
            package.Name = Guid.NewGuid().ToString();

            Fixture f1 = Fixture.NewFixture();
            f1.Name = "f1";
            f1.Height = 191;
            f1.Width = 120;
            f1.Depth = 100;
            package.Fixtures.Add(f1);

            FixtureItem f1Item = FixtureItem.NewFixtureItem();
            f1Item.FixtureId = f1.Id;
            package.FixtureItems.Add(f1Item);

            FixtureComponent f1Backboard = FixtureComponent.NewFixtureComponent();
            f1Backboard.Name = "f1Backboard";
            f1Backboard.Height = 191;
            f1Backboard.Width = 120;
            f1Backboard.Depth = 1;
            package.Components.Add(f1Backboard);

            FixtureSubComponent f1BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f1BackboardSub.Name = "f1BackboardSub";
            f1BackboardSub.Height = 191;
            f1BackboardSub.Width = 120;
            f1BackboardSub.Depth = 1;
            f1Backboard.SubComponents.Add(f1BackboardSub);

            FixtureComponentItem f1ComponentItem = FixtureComponentItem.NewFixtureComponentItem();
            f1ComponentItem.FixtureComponentId = f1Backboard.Id;
            f1ComponentItem.Y = 20;
            f1.Components.Add(f1ComponentItem);

            Fixture f2 = Fixture.NewFixture();
            f2.Name = "f2";
            f2.Height = 191;
            f2.Width = 120;
            f2.Depth = 100;
            package.Fixtures.Add(f2);

            FixtureItem f2Item = FixtureItem.NewFixtureItem();
            f2Item.FixtureId = f2.Id;
            f2Item.X = 120;
            package.FixtureItems.Add(f2Item);

            FixtureAssembly f2Assembly = FixtureAssembly.NewFixtureAssembly();
            f2Assembly.Name = "f2Assembly";
            package.Assemblies.Add(f2Assembly);

            FixtureAssemblyItem f2AssemblyItem = FixtureAssemblyItem.NewFixtureAssemblyItem();
            f2AssemblyItem.FixtureAssemblyId = f2Assembly.Id;
            f2AssemblyItem.Y = 1;
            f2.Assemblies.Add(f2AssemblyItem);

            FixtureComponent f2Backboard = FixtureComponent.NewFixtureComponent();
            f2Backboard.Name = "f2Backboard";
            f2Backboard.Height = 191;
            f2Backboard.Width = 120;
            f2Backboard.Depth = 1;
            package.Components.Add(f2Backboard);

            FixtureAssemblyComponent f2AssemblyComponent = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
            f2AssemblyComponent.FixtureComponentId = f2Backboard.Id;
            f2Assembly.Components.Add(f2AssemblyComponent);

            FixtureSubComponent f2BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f2BackboardSub.Name = "f2BackboardSub";
            f2BackboardSub.Height = 191;
            f2BackboardSub.Width = 120;
            f2BackboardSub.Depth = 1;
            f2Backboard.SubComponents.Add(f2BackboardSub);

            return package;
        }

        private static FixturePackage Create1FixturePackage()
        {
            FixturePackage package = FixturePackage.NewFixturePackage();
            package.Name = Guid.NewGuid().ToString();

            Fixture f1 = Fixture.NewFixture();
            f1.Name = "f1";
            f1.Height = 191;
            f1.Width = 120;
            f1.Depth = 100;
            package.Fixtures.Add(f1);

            FixtureComponent f1Backboard = FixtureComponent.NewFixtureComponent();
            f1Backboard.Name = "f1Backboard";
            f1Backboard.Height = 191;
            f1Backboard.Width = 120;
            f1Backboard.Depth = 1;
            package.Components.Add(f1Backboard);

            FixtureSubComponent f1BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f1BackboardSub.Name = "f1BackboardSub";
            f1BackboardSub.Height = 191;
            f1BackboardSub.Width = 120;
            f1BackboardSub.Depth = 1;
            f1Backboard.SubComponents.Add(f1BackboardSub);

            FixtureComponentItem f1ComponentItem = FixtureComponentItem.NewFixtureComponentItem();
            f1ComponentItem.FixtureComponentId = f1Backboard.Id;
            f1ComponentItem.Y = 20;
            f1.Components.Add(f1ComponentItem);


            FixtureAssembly f2Assembly = FixtureAssembly.NewFixtureAssembly();
            f2Assembly.Name = "f2Assembly";
            package.Assemblies.Add(f2Assembly);

            FixtureAssemblyItem f2AssemblyItem = FixtureAssemblyItem.NewFixtureAssemblyItem();
            f2AssemblyItem.FixtureAssemblyId = f2Assembly.Id;
            f2AssemblyItem.Y = 1;
            f1.Assemblies.Add(f2AssemblyItem);

            FixtureComponent f2Backboard = FixtureComponent.NewFixtureComponent();
            f2Backboard.Name = "f2Backboard";
            f2Backboard.Height = 191;
            f2Backboard.Width = 120;
            f2Backboard.Depth = 1;
            package.Components.Add(f2Backboard);

            FixtureAssemblyComponent f2AssemblyComponent = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
            f2AssemblyComponent.FixtureComponentId = f2Backboard.Id;
            f2Assembly.Components.Add(f2AssemblyComponent);

            FixtureSubComponent f2BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f2BackboardSub.Name = "f2BackboardSub";
            f2BackboardSub.Height = 191;
            f2BackboardSub.Width = 120;
            f2BackboardSub.Depth = 1;
            f2Backboard.SubComponents.Add(f2BackboardSub);

            return package;
        }

        private static FixturePackage Create1AssemblyPackage()
        {
            FixturePackage package = FixturePackage.NewFixturePackage();
            package.Name = Guid.NewGuid().ToString();

            FixtureAssembly f2Assembly = FixtureAssembly.NewFixtureAssembly();
            f2Assembly.Name = "f2Assembly";
            package.Assemblies.Add(f2Assembly);

            FixtureAssemblyItem f2AssemblyItem = FixtureAssemblyItem.NewFixtureAssemblyItem();
            f2AssemblyItem.FixtureAssemblyId = f2Assembly.Id;
            f2AssemblyItem.Y = 1;

            FixtureComponent f2Backboard = FixtureComponent.NewFixtureComponent();
            f2Backboard.Name = "f2Backboard";
            f2Backboard.Height = 191;
            f2Backboard.Width = 120;
            f2Backboard.Depth = 1;
            package.Components.Add(f2Backboard);

            FixtureAssemblyComponent f2AssemblyComponent = FixtureAssemblyComponent.NewFixtureAssemblyComponent();
            f2AssemblyComponent.FixtureComponentId = f2Backboard.Id;
            f2Assembly.Components.Add(f2AssemblyComponent);

            FixtureSubComponent f2BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f2BackboardSub.Name = "f2BackboardSub";
            f2BackboardSub.Height = 191;
            f2BackboardSub.Width = 120;
            f2BackboardSub.Depth = 1;
            f2Backboard.SubComponents.Add(f2BackboardSub);

            return package;
        }

        private static FixturePackage Create1ComponentPackage()
        {
            FixturePackage package = FixturePackage.NewFixturePackage();
            package.Name = Guid.NewGuid().ToString();

            FixtureComponent f1Backboard = FixtureComponent.NewFixtureComponent();
            f1Backboard.Name = "f1Backboard";
            f1Backboard.Height = 191;
            f1Backboard.Width = 120;
            f1Backboard.Depth = 1;
            package.Components.Add(f1Backboard);

            FixtureSubComponent f1BackboardSub = FixtureSubComponent.NewFixtureSubComponent();
            f1BackboardSub.Name = "f1BackboardSub";
            f1BackboardSub.Height = 191;
            f1BackboardSub.Width = 120;
            f1BackboardSub.Depth = 1;
            f1Backboard.SubComponents.Add(f1BackboardSub);

            return package;
        }

        #endregion

        #region Create New Tests

        [Test]
        public void CreateNew_PlanogramComponentView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            var fixtureView = planView.AddFixture();

            var componentView = fixtureView.AddComponent(PlanogramComponentType.Shelf);

            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.CreateNew(componentView);
                FixturePackage fixPackage = packageView.Model;

                Assert.AreEqual(0, fixPackage.FixtureItems.Count);
                Assert.AreEqual(0, fixPackage.Fixtures.Count);
                Assert.AreEqual(0, fixPackage.Assemblies.Count);
                Assert.AreEqual(1, fixPackage.Components.Count);
                Assert.AreEqual(1, fixPackage.Components[0].SubComponents.Count);
            }
        }

        [Test]
        public void CreateNew_PlanogramAssemblyView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            var fixtureView = planView.AddFixture();

            var assemblyView = fixtureView.AddAssembly();
            var c1 = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            var c2 = assemblyView.AddComponent(PlanogramComponentType.Shelf);


            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.CreateNew(assemblyView);
                FixturePackage fixturePackage = packageView.Model;

                Assert.AreEqual(0, fixturePackage.FixtureItems.Count);
                Assert.AreEqual(0, fixturePackage.Fixtures.Count);
                Assert.AreEqual(1, fixturePackage.Assemblies.Count);
                Assert.AreEqual(2, fixturePackage.Assemblies[0].Components.Count);
                Assert.AreEqual(2, fixturePackage.Components.Count);
                Assert.AreEqual(1, fixturePackage.Components[0].SubComponents.Count);
                Assert.AreEqual(1, fixturePackage.Components[1].SubComponents.Count);
            }
        }

        [Test]
        public void CreateNew_PlanogramFixtureView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            var fixtureView = planView.AddFixture();

            var c1 = fixtureView.AddComponent(PlanogramComponentType.Shelf);

            var assemblyView = fixtureView.AddAssembly();
            var c2 = assemblyView.AddComponent(PlanogramComponentType.Shelf);

            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.CreateNew(fixtureView);
                FixturePackage fixturePackage = packageView.Model;

                Assert.AreEqual(0, fixturePackage.FixtureItems.Count);
                Assert.AreEqual(1, fixturePackage.Fixtures.Count);

                Assert.AreEqual(1, fixturePackage.Fixtures[0].Assemblies.Count);
                Assert.AreEqual(1, fixturePackage.Fixtures[0].Components.Count);

                Assert.AreEqual(1, fixturePackage.Assemblies.Count);
                Assert.AreEqual(1, fixturePackage.Assemblies[0].Components.Count);

                Assert.AreEqual(2, fixturePackage.Components.Count);
                Assert.AreEqual(1, fixturePackage.Components[0].SubComponents.Count);
                Assert.AreEqual(1, fixturePackage.Components[1].SubComponents.Count);
            }

        }

        [Test]
        public void CreateNew_PlanogramFixtureItems()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);

            for (Int32 i = 0; i < 3; i++)
            {
                var fixtureView = planView.AddFixture();

                var c1 = fixtureView.AddComponent(PlanogramComponentType.Shelf);

                var assemblyView = fixtureView.AddAssembly();
                var c2 = assemblyView.AddComponent(PlanogramComponentType.Shelf);
            }

            using (FixturePackageViewModel packageView = new FixturePackageViewModel())
            {
                packageView.CreateNew(planView.Fixtures);

                FixturePackage fixturePackage = packageView.Model;

                Assert.AreEqual(3, fixturePackage.FixtureItems.Count);
                Assert.AreEqual(3, fixturePackage.Fixtures.Count);
                Assert.AreEqual(3, fixturePackage.Assemblies.Count);
                Assert.AreEqual(6, fixturePackage.Components.Count);
            }

        }


        #endregion

        #region Fetch Tests

        [Test]
        public void FetchFromInfo_File()
        {
            FixturePackage package = Create1FixturePackage();

            String directory = GetFixtureDirectoryPath();
            String path = Path.Combine(directory, package.Name);
            path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

            package = package.SaveAs(path);
            package.Dispose();
            package = null;

            FixturePackageInfo info;
            using (FixturePackageInfoListViewModel viewModel = new FixturePackageInfoListViewModel())
            {
                viewModel.FetchAll();
                info = viewModel.Model.First();
            }


            FixturePackageViewModel packageViewModel = new FixturePackageViewModel();
            packageViewModel.Fetch(info);
            Assert.AreEqual(info.FileName, packageViewModel.Model.FileName);
        }

        #endregion

        #region Save Tests

        [Test]
        public void SaveAsFile()
        {
            FixturePackage package = Create1FixturePackage();

            String directory = GetFixtureDirectoryPath();
            String path = Path.Combine(directory, package.Name);
            path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

            package = package.SaveAs(path);
            package.Dispose();
            package = null;

            FixturePackageInfo info;
            using (FixturePackageInfoListViewModel viewModel = new FixturePackageInfoListViewModel())
            {
                viewModel.FetchAll();
                info = viewModel.Model.First();
            }


            FixturePackageViewModel packageViewModel = new FixturePackageViewModel();
            packageViewModel.Fetch(info);

            String path2 = Path.Combine(directory, packageViewModel.Model.Name + "_copy");
            path2= Path.ChangeExtension(path2, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

            FixturePackage oldModel = packageViewModel.Model;
            packageViewModel.SaveAsFile(path2);

            Assert.AreNotEqual(oldModel, packageViewModel.Model);
            Assert.AreEqual(path2, packageViewModel.Model.FileName);

            packageViewModel.Dispose();
        }

        #endregion

        #region Delete Tests

        [Test]
        public void DeleteFromInfo_File()
        {
            FixturePackage package = Create1FixturePackage();

            String directory = GetFixtureDirectoryPath();
            String path = Path.Combine(directory, package.Name);
            path = Path.ChangeExtension(path, Galleria.Ccm.Constants.FixtureLibraryFileExtension);

            package = package.SaveAs(path);
            package.Dispose();
            package = null;

            FixturePackageInfo info;
            using (FixturePackageInfoListViewModel viewModel = new FixturePackageInfoListViewModel())
            {
                viewModel.FetchAll();
                info = viewModel.Model.First();
            }

            String fileName = info.FileName;

            FixtureLibraryHelper.DeleteFixturePackage(info);

            using (FixturePackageInfoListViewModel viewModel = new FixturePackageInfoListViewModel())
            {
                viewModel.FetchAll();
                Assert.AreEqual(0, viewModel.Model.Count);
            }
        }

        #endregion

        #region Add To Planogram Tests

        [Test]
        public void AddToPlanogram_2FixtureItemsPackage()
        {
            FixturePackage fixturePackage = Create2FixtureItemsPackage();
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVm.PlanogramViews.First();

            //call method
            FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, null);

            //check
            Assert.AreEqual(2, planView.Fixtures.Count);

            //fixture 1
            Assert.AreEqual(0, planView.Fixtures[0].X);
            Assert.AreEqual(0, planView.Fixtures[0].Assemblies.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Components[0].SubComponents.Count);

            //fixture 2
            Assert.AreEqual(120, planView.Fixtures[1].X);
            Assert.AreEqual(1, planView.Fixtures[1].Assemblies.Count);
            Assert.AreEqual(0, planView.Fixtures[1].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[1].Assemblies[0].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[1].Assemblies[0].Components[0].SubComponents.Count);
        }

        [Test]
        public void AddToPlanogram_1FixturePackage()
        {
            FixturePackage fixturePackage = Create1FixturePackage();
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVm.PlanogramViews.First();

            var f1 = planView.AddFixture();


            //call method
            FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, f1);

            //check
            Assert.AreEqual(2, planView.Fixtures.Count);

            //fixture 1
            Assert.AreEqual(1, planView.Fixtures[1].Assemblies.Count);
            Assert.AreEqual(1, planView.Fixtures[1].Assemblies[0].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[1].Assemblies[0].Components[0].SubComponents.Count);

            Assert.AreEqual(1, planView.Fixtures[1].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[1].Components[0].SubComponents.Count);


        }

        [Test]
        public void AddToPlanogram_1AssemblyPackage()
        {
            FixturePackage fixturePackage = Create1AssemblyPackage();
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVm.PlanogramViews.First();

            var f1 = planView.AddFixture();


            //call method
            FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, f1);

            //check
            Assert.AreEqual(1, planView.Fixtures.Count);

            //fixture 1
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies[0].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Assemblies[0].Components[0].SubComponents.Count);

            Assert.AreEqual(0, planView.Fixtures[0].Components.Count);

        }

        [Test]
        public void AddToPlanogram_1ComponentPackage()
        {
            FixturePackage fixturePackage = Create1ComponentPackage();
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
            PlanogramView planView = packageVm.PlanogramViews.First();

            var f1 = planView.AddFixture();


            //call method
            FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, f1);

            //check
            Assert.AreEqual(1, planView.Fixtures.Count);

            //fixture 1
            Assert.AreEqual(0, planView.Fixtures[0].Assemblies.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Components.Count);
            Assert.AreEqual(1, planView.Fixtures[0].Components[0].SubComponents.Count);


        }

        #endregion

        #region Property Copying

        #region SubComponent

        public IEnumerable<String> SubComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { 
                    "Parent",
                    "ImageIdFront", "ImageIdBack", "ImageIdTop", "ImageIdBottom", "ImageIdLeft", "ImageIdRight"
                };

                foreach (PropertyInfo modelProperty in typeof(PlanogramSubComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramSubComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("SubComponentModelPropertyNames")]
        public void CopyValues_SubComponentProperty(String propertyName)
        {
            PropertyInfo fixtureSubProperty = typeof(FixtureSubComponent).GetProperty(propertyName);
            PropertyInfo planSubProperty = typeof(PlanogramSubComponent).GetProperty(propertyName);

            if (fixtureSubProperty != null && planSubProperty != null)
            {
                Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
                Planogram plan = package.Planograms.Add();
                PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
                PlanogramView planView = packageVm.PlanogramViews.First();
                FixturePackage fixturePackage = Create1ComponentPackage();
                
                FixtureSubComponent fixtureSub = fixturePackage.Components[0].SubComponents[0];

                //set the property
                if (fixtureSubProperty.GetSetMethod(false) != null)
                {
                    fixtureSubProperty.SetValue(fixtureSub, GetPropertyTestValue2(fixtureSubProperty.PropertyType), null);
                }

                //copy to the plan
                FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, null);

                //get the sub
                PlanogramSubComponentView planSub = planView.EnumerateAllSubComponents().First();

                //check the property value
                Assert.AreEqual(Convert.ToString(fixtureSubProperty.GetValue(fixtureSub, null)), Convert.ToString(planSubProperty.GetValue(planSub.Model, null)));

                //create fixture package
                FixturePackageViewModel view = new FixturePackageViewModel();
                view.CreateNew(planSub.Component);

                fixtureSub = view.Model.Components[0].SubComponents[0];
                Assert.AreEqual(Convert.ToString(planSubProperty.GetValue(planSub.Model, null)), Convert.ToString(fixtureSubProperty.GetValue(fixtureSub, null)));
            }
        }

        #endregion

        #region Component

        public IEnumerable<String> ComponentModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { 
                    "Parent",
                    "ImageIdFront", "ImageIdBack", "ImageIdTop", "ImageIdBottom", "ImageIdLeft", "ImageIdRight",
                    "Height", "Width", "Depth",
                    "NumberOfSubComponents", "NumberOfMerchandisedSubComponents"
                };

                foreach (PropertyInfo modelProperty in typeof(PlanogramComponent).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramComponent)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("ComponentModelPropertyNames")]
        public void CopyValues_ComponentProperty(String propertyName)
        {
            PropertyInfo fixtureCompProperty = typeof(FixtureComponent).GetProperty(propertyName);
            PropertyInfo planCompProperty = typeof(PlanogramComponent).GetProperty(propertyName);

            if (fixtureCompProperty != null && planCompProperty != null)
            {
                Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
                Planogram plan = package.Planograms.Add();
                PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
                PlanogramView planView = packageVm.PlanogramViews.First();
                FixturePackage fixturePackage = Create1ComponentPackage();

                FixtureComponent fixtureComp = fixturePackage.Components[0];

                //set the property
                if (fixtureCompProperty.GetSetMethod(false) != null)
                {
                    fixtureCompProperty.SetValue(fixtureComp, GetPropertyTestValue2(fixtureCompProperty.PropertyType), null);
                }

                //copy to the plan
                FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, null);

                //get the sub
                PlanogramComponentView planComp = planView.EnumerateAllComponents().First();

                //check the property value
                Assert.AreEqual(Convert.ToString(fixtureCompProperty.GetValue(fixtureComp, null)), Convert.ToString(planCompProperty.GetValue(planComp.ComponentModel, null)));

                //create fixture package
                FixturePackageViewModel view = new FixturePackageViewModel();
                view.CreateNew(planComp);

                fixtureComp = view.Model.Components[0];
                Assert.AreEqual(Convert.ToString(planCompProperty.GetValue(planComp.ComponentModel, null)), Convert.ToString(fixtureCompProperty.GetValue(fixtureComp, null)));
            }
        }

        #endregion

        #region Assembly

        public IEnumerable<String> AssemblyModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent", "Height", "Width", "Depth", "NumberOfComponents" };

                foreach (PropertyInfo modelProperty in typeof(PlanogramAssembly).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramAssembly)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("AssemblyModelPropertyNames")]
        public void CopyValues_AssemblyProperty(String propertyName)
        {
            PropertyInfo fixtureAssemblyProperty = typeof(FixtureAssembly).GetProperty(propertyName);
            PropertyInfo planAssemblyProperty = typeof(PlanogramAssembly).GetProperty(propertyName);

            if (fixtureAssemblyProperty != null && planAssemblyProperty != null)
            {
                Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
                Planogram plan = package.Planograms.Add();
                PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
                PlanogramView planView = packageVm.PlanogramViews.First();
                FixturePackage fixturePackage = Create1AssemblyPackage();

                FixtureAssembly fixtureAssembly = fixturePackage.Assemblies[0];

                //set the property
                if (fixtureAssemblyProperty.GetSetMethod(false) != null)
                {
                    fixtureAssemblyProperty.SetValue(fixtureAssembly, GetPropertyTestValue2(fixtureAssemblyProperty.PropertyType), null);
                }

                //copy to the plan
                FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, null);

                //get the sub
                PlanogramAssemblyView planAssembly = planView.EnumerateAllAssemblies().First();

                //check the property value
                Assert.AreEqual(Convert.ToString(fixtureAssemblyProperty.GetValue(fixtureAssembly, null)), Convert.ToString(planAssemblyProperty.GetValue(planAssembly.AssemblyModel, null)));

                //create fixture package
                FixturePackageViewModel view = new FixturePackageViewModel();
                view.CreateNew(planAssembly);

                fixtureAssembly = view.Model.Assemblies[0];
                Assert.AreEqual(Convert.ToString(planAssemblyProperty.GetValue(planAssembly.AssemblyModel, null)), Convert.ToString(fixtureAssemblyProperty.GetValue(fixtureAssembly, null)));
            }
        }

        #endregion

        #region Fixture

        public IEnumerable<String> FixtureModelPropertyNames
        {
            get
            {
                String[] exclusions = new String[] { "Parent", 
                "NumberOfAssemblies", "NumberOfMerchandisedSubComponents"};

                foreach (PropertyInfo modelProperty in typeof(PlanogramFixture).GetProperties()
             .Where(p => p.DeclaringType == typeof(PlanogramFixture)))
                {
                    if (!exclusions.Contains(modelProperty.Name)
                        && !modelProperty.Name.EndsWith("Id")
                        && !modelProperty.PropertyType.GetInterfaces().Contains(typeof(IObservableBindingList)))
                    {
                        yield return modelProperty.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("FixtureModelPropertyNames")]
        public void CopyValues_FixtureProperty(String propertyName)
        {
            PropertyInfo fixtureProperty = typeof(Fixture).GetProperty(propertyName);
            PropertyInfo planFixtureProperty = typeof(PlanogramFixture).GetProperty(propertyName);

            if (fixtureProperty != null && planFixtureProperty != null)
            {
                Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
                Planogram plan = package.Planograms.Add();
                PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(package, false);
                PlanogramView planView = packageVm.PlanogramViews.First();
                FixturePackage fixturePackage = Create1FixturePackage();

                Fixture fixture = fixturePackage.Fixtures[0];

                //set the property
                if (fixtureProperty.GetSetMethod(false) != null)
                {
                    fixtureProperty.SetValue(fixture, GetPropertyTestValue2(fixtureProperty.PropertyType), null);
                }

                //copy to the plan
                FixtureLibraryHelper.AddToPlanogram(fixturePackage, planView, null);

                //get the sub
                PlanogramFixtureView planFixture = planView.Fixtures.First();

                //check the property value
                Assert.AreEqual(Convert.ToString(fixtureProperty.GetValue(fixture, null)), Convert.ToString(planFixtureProperty.GetValue(planFixture.FixtureModel, null)));

                //create fixture package
                FixturePackageViewModel view = new FixturePackageViewModel();
                view.CreateNew(planFixture);

                fixture = view.Model.Fixtures[0];
                Assert.AreEqual(Convert.ToString(planFixtureProperty.GetValue(planFixture.FixtureModel, null)), Convert.ToString(fixtureProperty.GetValue(fixture, null)));
            }
        }

        #endregion

        #endregion

    }
}
