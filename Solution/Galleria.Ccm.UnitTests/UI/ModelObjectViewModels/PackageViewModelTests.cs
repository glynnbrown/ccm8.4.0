﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25653 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using System.Reflection;
using Csla.Core;
using System.IO;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Editor.Client.Wpf;

namespace Galleria.Ccm.UnitTests.UI.ModelObjectViewModels
{
    [TestFixture]
    public class PackageViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private String CreateFilePackage()
        {
            Package package = TestDataHelper.CreatePlanCreweSparklingWater();
            package.Name = Guid.NewGuid().ToString();

            String planPath = Path.Combine(this.TestDir, package.Name);
            planPath = Path.ChangeExtension(planPath, PackageViewModel.FileSystemExtension);

            Planogram plan = package.Planograms.First();

            package = package.SaveAs(DomainPrincipal.CurrentUserId, planPath);
            package.Dispose();

            return planPath;
        }

        private Object CreateRepositoryPackage()
        {
            Object planId = null;

            Package package = TestDataHelper.CreatePlanCreweSparklingWater();
            package.Name = Guid.NewGuid().ToString();
            Planogram plan = package.Planograms.First();

            package = package.Save();

            planId = package.Planograms[0].Id;
            package.Dispose();

            return planId;
        }

        #endregion

        [Test]
        public void CreateNewPackage()
        {
            PackageViewModel viewModel = PackageViewModel.CreateNewPackage(true);

            Package model = viewModel.Model;
            Assert.IsNotNull(model, "model should be set");
            Assert.IsTrue(model.IsNew, "model should be new");
            Assert.IsTrue(model.IsInitialized, "model should be initialized");
            Assert.AreEqual(1, model.Planograms.Count, "should have 1 plan");

            Assert.AreEqual(1, viewModel.PlanogramViews.Count, "should have 1 planview");
            Assert.AreEqual(model.Planograms[0], viewModel.PlanogramViews[0].Model, "model and planview should match");
            Assert.AreEqual(viewModel, viewModel.PlanogramViews[0].ParentPackageView, "parent packageview should match");
        }

        [Test]
        public void FetchFromFile()
        {
            String filePath = CreateFilePackage();
            PackageViewModel viewModel = PackageViewModel.FetchPackageByFileName(filePath);

            using (Package model = viewModel.Model)
            {
                Assert.IsNotNull(model);
                Assert.IsFalse(model.IsNew);
                Assert.AreEqual(1, model.Planograms.Count);

                Assert.AreEqual(1, viewModel.PlanogramViews.Count);
                Assert.AreEqual(model.Planograms[0], viewModel.PlanogramViews[0].Model);
                Assert.AreEqual(viewModel, viewModel.PlanogramViews[0].ParentPackageView);
            }
        }

        [Test]
        public void FetchFromRepository()
        {
            Object planId = CreateRepositoryPackage();
            PackageViewModel viewModel = PackageViewModel.FetchPackageById(planId);

            Package model = viewModel.Model;
            Assert.IsNotNull(model);
            Assert.IsFalse(model.IsNew);
            Assert.AreEqual(1, model.Planograms.Count);

            Assert.AreEqual(1, viewModel.PlanogramViews.Count);
            Assert.AreEqual(model.Planograms[0], viewModel.PlanogramViews[0].Model);
            Assert.AreEqual(viewModel, viewModel.PlanogramViews[0].ParentPackageView);
        }

        [Test]
        public void SaveAsToRepository()
        {
            String filePath = CreateFilePackage();
            PackageViewModel viewModel = PackageViewModel.FetchPackageByFileName(filePath);

            Package fileModel = viewModel.Model;

            viewModel.SaveAsToRepository(0, null);

            Package repositoryModel = viewModel.Model;

            Assert.AreNotEqual(fileModel, repositoryModel);
            Assert.AreEqual(fileModel.Planograms[0].Name, repositoryModel.Planograms[0].Name);
            Assert.AreEqual(1, repositoryModel.Planograms.Count);
            Assert.AreEqual(1, viewModel.PlanogramViews.Count);

            //refetch the repository model
            PackageViewModel viewModel2 = PackageViewModel.FetchPackageById(repositoryModel.Planograms[0].Id);
            Assert.IsNotNull(viewModel2.Model);
        }

        [Test]
        public void SaveAsToFile()
        {
            Object planId = CreateRepositoryPackage();
            String planPath;

            using (PackageViewModel viewModel = PackageViewModel.FetchPackageById(planId))
            {
                Package repositoryModel = viewModel.Model;

                planPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
                planPath = Path.ChangeExtension(planPath, ".pog");

                viewModel.SaveAsFile(planPath);

                Package fileModel = viewModel.Model;

                Assert.AreNotEqual(fileModel, repositoryModel);
                Assert.AreEqual(fileModel.Planograms[0].Name, repositoryModel.Planograms[0].Name);
                Assert.AreEqual(1, repositoryModel.Planograms.Count);
                Assert.AreEqual(1, viewModel.PlanogramViews.Count);
            }


            //refetch the file model
            using (PackageViewModel viewModel2 = PackageViewModel.FetchPackageByFileName(planPath))
            {
                Assert.IsNotNull(viewModel2.Model);
            }
        }

        [Test]
        public void AutoSave()
        {
            String filePath = CreateFilePackage();
            PackageViewModel viewModel = PackageViewModel.FetchPackageByFileName(filePath);

            String autoSaveDirectory = this.TestDir;
            viewModel.AutoSave(autoSaveDirectory);

            String expectedPath = Path.Combine(this.TestDir, viewModel.Model.Name);
            expectedPath = Path.ChangeExtension(expectedPath, Galleria.Ccm.Constants.PlanogramAutosaveFileExtension);

            Assert.IsTrue(File.Exists(expectedPath));
        }


        [Test]
        public void CreateNewPackage_NoRepositoryConnection()
        {
            //clear the repository connection.
            App.ViewState.ClearRepositoryConnection();

            PackageViewModel viewModel = PackageViewModel.CreateNewPackage(true);
            Package model = viewModel.Model;
            Assert.IsTrue(model.IsValid, "Package should be created as valid");
        }
    }
}
