﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentAddRegionalProductsViewModelTests : TestBase
    {
        PlanogramAssortment _assortment;
        private AssortmentAddRegionalProductsViewModel _viewModel;

        [SetUp]
        public void SetUp()
        {
            _assortment = PlanogramAssortment.NewPlanogramAssortment();
            _assortment.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct());
            _assortment.Regions.Add(PlanogramAssortmentRegion.NewPlanogramAssortmentRegion());
            _viewModel = new AssortmentAddRegionalProductsViewModel(_assortment, new List<IPlanItem>());
        }

        [Test]
        public void Command_Apply()
        {
            RelayCommand cmd = this._viewModel.ApplyCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as regional product rule not valid");

            this._viewModel.SelectedPrimaryProduct = _assortment.Products.First();
            this._viewModel.SelectedRegionProductRow = this._viewModel.RegionProductRows.First();
            this._viewModel.SelectedRegionProductRow.SelectedProduct = _assortment.Products.Last();

            Assert.AreEqual(0, _assortment.Regions[0].Products.Count(), "There shouldnt be any regional products initially set");
            Assert.AreEqual(PlanogramAssortmentProductLocalizationType.None, this._viewModel.SelectedPrimaryProduct.ProductLocalizationType, "There shouldnt be any localisation type initially set");
            Assert.IsFalse(this._viewModel.SelectedPrimaryProduct.IsPrimaryRegionalProduct, "The product should not initially be marked as the primary regional product");

            cmd.Execute();
            Assert.AreEqual(1, _assortment.Regions[0].Products.Count(), "The regional product should now be set");
            Assert.AreEqual(PlanogramAssortmentProductLocalizationType.Regional, this._viewModel.SelectedPrimaryProduct.ProductLocalizationType, "The regional localisation type should be set");
            Assert.IsTrue(this._viewModel.SelectedPrimaryProduct.IsPrimaryRegionalProduct, "The product should now be the primary regional product");
        }

        [Test]
        public void Command_ApplyAndClose()
        {
            RelayCommand cmd = this._viewModel.ApplyAndCloseCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            this._viewModel.SelectedPrimaryProduct = _assortment.Products.First();
            this._viewModel.SelectedRegionProductRow = this._viewModel.RegionProductRows.First();
            this._viewModel.SelectedRegionProductRow.SelectedProduct = _assortment.Products.Last();

            Assert.AreEqual(0, _assortment.Regions[0].Products.Count(), "There shouldnt be any regional products initially set");
            Assert.AreEqual(PlanogramAssortmentProductLocalizationType.None, this._viewModel.SelectedPrimaryProduct.ProductLocalizationType, "There shouldnt be any localisation type initially set");
            Assert.IsFalse(this._viewModel.SelectedPrimaryProduct.IsPrimaryRegionalProduct, "The product should not initially be marked as the primary regional product");

            cmd.Execute();
            Assert.AreEqual(1, _assortment.Regions[0].Products.Count(), "The regional product should now be set");
            Assert.AreEqual(PlanogramAssortmentProductLocalizationType.Regional, this._viewModel.SelectedPrimaryProduct.ProductLocalizationType, "The regional localisation type should be set");
            Assert.IsTrue(this._viewModel.SelectedPrimaryProduct.IsPrimaryRegionalProduct, "The product should now be the primary regional product");
        }
    }
}
