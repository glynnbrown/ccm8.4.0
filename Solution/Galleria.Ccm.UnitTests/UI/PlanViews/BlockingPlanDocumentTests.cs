﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26891 : L.Ineson
//  Created

#endregion

#region Version History : CCM 802

// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
// V8-29000 : A.Silva
//      Added tests to CurrentBlocking when updating the sequence after changes to the blocking.

#endregion

#region Version History: CCM 803

// V8-29518 : A.Silva
//      Added unit tests for GenerateProductSequenceCommand.

#endregion

#region Version History: CCM810

// V8-28878: A.Silva
//  Removed obsolete tests regarding updating the product sequence when the blocks changed in some way. 
//      The user now clicks a button to do that and changing blocks does no longer update the product sequence.
//  Amended test to check LoadFromRepositoryCommand_Executed as now the blocking is loaded from a planogram and not a master blocking.

#endregion

#region Version History: CCM811

// V8-30472 : A.Silva
//  Added Unit Test to ensure tool is reset after becoming inactive.
// V8-30631 : A.Silva
//  Added Unit Test to ensure the Generate Product Sequence button is disabled if there are groups with duplicate colours.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking;
using Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor;
using System.Collections.Generic;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class BlockingPlanDocumentTests : TestBase
    {
        #region Fields

        private MainPageViewModel _mainViewModel;
        private BlockingPlanDocument _viewModel;
        private PlanControllerViewModel _planViewModel;

        #endregion

        #region Properties

        [Test]
        public void BlockingHeight_Initialized()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            Assert.AreNotEqual(0, viewModel.BlockingHeight);
            Assert.AreEqual(viewModel.Planogram.Height, viewModel.BlockingHeight);
        }

        [Test]
        public void BlockingHeight_UpdatesOnPlanSizeChange()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            var fixture = viewModel.Planogram.AddFixture();
            fixture.Height = 270;
            Assert.AreEqual(viewModel.Planogram.Height, viewModel.BlockingHeight);

            Assert.Contains("BlockingHeight", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void BlockingWidth_Initialized()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            Assert.AreNotEqual(0, viewModel.BlockingWidth);
            Assert.AreEqual(viewModel.Planogram.Width, viewModel.BlockingWidth);
        }

        [Test]
        public void BlockingWidth_UpdatesOnPlanSizeChange()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            viewModel.Planogram.Fixtures[0].Width = 200;
            Assert.AreEqual(viewModel.Planogram.Width, viewModel.BlockingWidth);

            Assert.Contains("BlockingWidth", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void AvailableBlocking_CreatesFinalIfNotAvailable()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            Assert.AreEqual(1, newPlan.SourcePlanogram.Model.Blocking.Count);
            Assert.AreEqual(PlanogramBlockingType.Final, newPlan.SourcePlanogram.Model.Blocking[0].Type);
        }

        [Test]
        public void CurrentBlocking_InitializedToFinal()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            Assert.AreEqual(PlanogramBlockingType.Final, viewModel.CurrentBlocking.Type);

            //load with existing
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.Model.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));
            newPlan.SourcePlanogram.Model.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final));

            viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);
            Assert.AreEqual(PlanogramBlockingType.Final, viewModel.CurrentBlocking.Type);
            Assert.AreEqual(2, newPlan.SourcePlanogram.Model.Blocking.Count);
        }

        [Test]
        public void CurrentBlockingHeader_UpdatedOnCurrentBlockingChange()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.Model.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));
            newPlan.SourcePlanogram.Model.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final));

            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            viewModel.CurrentBlocking = new Framework.Planograms.Controls.Wpf.ViewModel.PlanogramBlockingViewModel(newPlan.SourcePlanogram.Model.Blocking[0], newPlan.SourcePlanogram.Model.Sequence);
            Assert.Contains("CurrentBlocking", base.PropertyChangedNotifications);
            Assert.Contains("CurrentBlockingHeader", base.PropertyChangedNotifications);


            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void SelectedBlockLocation_SetWhenFirstDividerPlaced()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            viewModel.CurrentBlocking.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);
            Assert.AreEqual(viewModel.CurrentBlocking.Locations[0], viewModel.SelectedBlockLocation);
        }

        [Test]
        public void SelectedBlockLocation_UpdatedOnSelectedGroupChange()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);
            viewModel.CurrentBlocking.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);

            //select group 2
            viewModel.SelectedBlockGroup = viewModel.CurrentBlocking.Groups[1];
            Assert.AreEqual(viewModel.CurrentBlocking.Locations[1], viewModel.SelectedBlockLocation);

            //select group 1
            viewModel.SelectedBlockGroup = viewModel.CurrentBlocking.Groups[0];
            Assert.AreEqual(viewModel.CurrentBlocking.Locations[0], viewModel.SelectedBlockLocation);
        }

        [Test]
        public void SelectedBlockGroup_SetWhenFirstDividerPlaced()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            viewModel.CurrentBlocking.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);
            Assert.AreEqual(viewModel.CurrentBlocking.Groups[0], viewModel.SelectedBlockGroup);
        }

        [Test]
        public void SelectedBlockGroup_UpdatedOnSelectedLocationChange()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            viewModel.CurrentBlocking.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);

            //select loc 2
            viewModel.SelectedBlockLocation = viewModel.CurrentBlocking.Locations[1];
            Assert.AreEqual(viewModel.CurrentBlocking.Groups[1], viewModel.SelectedBlockGroup);

            //select group 1
            viewModel.SelectedBlockLocation = viewModel.CurrentBlocking.Locations[0];
            Assert.AreEqual(viewModel.CurrentBlocking.Groups[0], viewModel.SelectedBlockGroup);
        }

        [Test]
        public void ToolType_Initialized()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            Assert.AreEqual(BlockingToolType.Pointer, viewModel.ToolType);

        }

        [Test]
        public void ToolType_Setter()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);

            viewModel.PropertyChanged += base.TestModel_PropertyChanged;

            viewModel.ToolType = BlockingToolType.BlockConnector;
            Assert.AreEqual(BlockingToolType.BlockConnector, viewModel.ToolType);
            Assert.Contains("ToolType", base.PropertyChangedNotifications);

            viewModel.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region Commands

        [Test]
        public void LoadFromRepository_Executed()
        {
            //  Setup the database.
            SetMockRepositoryConnection();
            //  Add a response to accept the dialog when creating a new planogram in the UI.
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      p => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            //  Create the planogram.
            MainPageViewModel pageViewModel = App.MainPageViewModel;
            PlanControllerViewModel planogram = pageViewModel.CreateNewPlanogram();
            //  Create the Blocking Plan Document.
            var blockingPlanDocument = (BlockingPlanDocument) planogram.AddNewDocument(DocumentType.Blocking, true);
            PlanogramBlocking testBlocking = blockingPlanDocument.CurrentBlocking.Model;
            testBlocking.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            testBlocking.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            testBlocking.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);
            pageViewModel.SaveAsRepository(planogram);
            pageViewModel.CloseAllPlans();
            //  Add a response to accept the dialog when creating a new planogram in the UI.
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      p => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            planogram = pageViewModel.CreateNewPlanogram();
            //  Create the Blocking Plan Document.
            blockingPlanDocument = (BlockingPlanDocument) planogram.AddNewDocument(DocumentType.Blocking, true);
            //  Check assumptions.
            PlanogramBlocking blocking = planogram.SourcePlanogram.Model.Blocking.FirstOrDefault();
            if (blocking == null) Assert.Inconclusive("A newly created planogram should have only one default blocking strategy.");
            if (blocking.Dividers.Any() ||
                blocking.Groups.Any() ||
                blocking.Locations.Any())
                Assert.Inconclusive("A newly created planogram default blocking strategy should be empty.");
            // Add a response to select the planogram to load from.
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      p =>
                                      {
                                          var viewModel = (PlanogramSelectorViewModel) p.WindowParameters[0];
                                          viewModel.SelectedPlanogramInfo = viewModel.AvailablePlanogramInfos[0].Model;
                                          viewModel.OKCommand.Execute();
                                      });

            blockingPlanDocument.LoadFromRepositoryCommand.Execute();

            //check the planogram blocking
            Planogram plan = blockingPlanDocument.Planogram.Model;
            Assert.AreEqual(2, plan.Blocking.Count);

            PlanogramBlocking b = plan.Blocking[1];

            Assert.AreEqual(3, b.Dividers.Count);

            PlanogramBlockingDivider divider1 = b.Dividers[0];
            PlanogramBlockingDivider divider2 = b.Dividers[1];
            PlanogramBlockingDivider divider3 = b.Dividers[2];

            //check dividers
            Assert.AreEqual(0.45F, divider1.X);
            Assert.AreEqual(0F, divider1.Y);
            Assert.AreEqual(1F, divider1.Length);
            Assert.AreEqual(1, divider1.Level);

            Assert.AreEqual(0.8F, divider2.X);
            Assert.AreEqual(0F, divider2.Y);
            Assert.AreEqual(1F, divider2.Length);
            Assert.AreEqual(1, divider2.Level);

            Assert.AreEqual(0.8F, divider3.X);
            Assert.AreEqual(0.5F, divider3.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(divider3.Length, 5)));
            Assert.AreEqual(2, divider3.Level);

            //check groups
            Assert.AreEqual(4, b.Groups.Count, "Should have 4 groups");
            Assert.AreEqual(4,
                            b.Groups.Select(g => g.Colour).Distinct().Count(),
                            "Each group should have a distinct colour");

            //check locations.
            Assert.AreEqual(4, b.Locations.Count, "Should have 4 locations");

            PlanogramBlockingLocation loc1 = b.Locations[0];
            PlanogramBlockingLocation loc2 = b.Locations[1];
            PlanogramBlockingLocation loc3 = b.Locations[2];
            PlanogramBlockingLocation loc4 = b.Locations[3];

            Assert.AreEqual(b.Groups[0].Id, loc1.PlanogramBlockingGroupId);
            Assert.AreEqual(divider1.Id, loc1.PlanogramBlockingDividerRightId);
            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.45F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);

            Assert.AreEqual(b.Groups[1].Id, loc2.PlanogramBlockingGroupId);
            Assert.AreEqual(divider1.Id, loc2.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(divider2.Id, loc2.PlanogramBlockingDividerRightId);
            Assert.AreEqual(0.45F, loc2.X);
            Assert.AreEqual(0, loc2.Y);
            Assert.AreEqual(0.35F, Convert.ToSingle(Math.Round(loc2.Width, 5)));
            Assert.AreEqual(1, Convert.ToSingle(Math.Round(loc2.Height, 5)));

            Assert.AreEqual(b.Groups[2].Id, loc3.PlanogramBlockingGroupId);
            Assert.AreEqual(divider2.Id, loc3.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(divider3.Id, loc3.PlanogramBlockingDividerTopId);
            Assert.AreEqual(0.8F, loc3.X);
            Assert.AreEqual(0F, loc3.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc3.Width, 5)));
            Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc3.Height, 5)));

            Assert.AreEqual(b.Groups[3].Id, loc4.PlanogramBlockingGroupId);
            Assert.AreEqual(divider2.Id, loc4.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(divider3.Id, loc4.PlanogramBlockingDividerBottomId);
            Assert.AreEqual(0.8F, loc4.X);
            Assert.AreEqual(0.5, loc4.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc4.Width, 5)));
            Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc4.Height, 5)));

            Assert.IsTrue(plan.IsValid, "The plan should still be valid");

        }

        #region Generate Product Sequence Command

        [Test]
        public void GenerateProductSequenceCommand_WhenNoPositions_ShouldBeDisabled()
        {
            InitializeViewModels();
            const String expectation =
                "The Generate Product Sequence Command should be disabled when there are no positions on the planogram.";
            Boolean hasPositions = _viewModel.Planogram.Model.Positions.Any();
            if (hasPositions) Assert.Inconclusive("The planogram should have no positions to test this.");
            Boolean hasDuplicateColoursForBlockinGroups =
                _viewModel.Planogram.Model.Blocking.Single(b => b.Type == PlanogramBlockingType.Final)
                          .Groups.GroupBy(g => g.Colour)
                          .Any(g => g.Count() > 1);
            if (hasDuplicateColoursForBlockinGroups) Assert.Inconclusive("The planogram should not have duplicate colours for blocking groups to test this.");

            Boolean isEnabled = _viewModel.GenerateProductSequenceCommand.CanExecute();

            Assert.IsFalse(isEnabled, expectation);
        }

        [Test]
        public void GenerateProductSequenceCommand_WhenDuplicateBlockingGroupColours_ShouldBeDisabled()
        {
            InitializeViewModels();
            const String expectation =
                "The Generate Product Sequence Command should be disabled when there are duplicate colours for Blocking Groups.";
            var document = (PlanVisualDocument) _planViewModel.AddNewDocument(DocumentType.Design, true);
            PlanogramProductView productView = document.Planogram.AddProduct();
            PlanogramFixtureView fixtureView = _planViewModel.SourcePlanogram.Fixtures.First();
            var size = new WidthHeightDepthValue(fixtureView.Width, 4, fixtureView.Depth);
            IPlanItem component = document.AddNewComponent(MouseToolType.CreateShelf,
                                                           new PointValue(10, 10, 0),
                                                           new RotationValue(0, 0, 0),
                                                           size,
                                                           fixtureView);
            document.AddNewPositions(component.Component.SubComponents.First(),
                                     new PointValue(0, 0, 0),
                                     new List<PlanogramProductView> {productView});
            Boolean hasPositions = _viewModel.Planogram.Model.Positions.Any();
            if (!hasPositions) Assert.Inconclusive("The planogram should have positions to test this.");
            _viewModel.CurrentBlocking.Dividers.Add(0F, 0.25F, PlanogramBlockingDividerType.Horizontal);
            _viewModel.CurrentBlocking.Dividers.Add(0F, 0.5F, PlanogramBlockingDividerType.Horizontal);
            _viewModel.CurrentBlocking.Groups.First().Colour = _viewModel.CurrentBlocking.Groups.Last().Colour;
            Boolean hasDuplicateColoursForBlockinGroups =
                _viewModel.Planogram.Model.Blocking.Single(b => b.Type == PlanogramBlockingType.Final)
                          .Groups.GroupBy(g => g.Colour)
                          .Any(g => g.Count() > 1);
            if (!hasDuplicateColoursForBlockinGroups) Assert.Inconclusive("The planogram should have duplicate colours for blocking groups to test this.");

            Boolean isEnabled = _viewModel.GenerateProductSequenceCommand.CanExecute();

            Assert.IsFalse(isEnabled, expectation);
        }

        [Test]
        public void GenerateProductSequenceCommand_WhenExecuted_ShouldGetSequenceFromBlocking()
        {
            const String expectation = "The Generate Product Sequence Command should sequence the products when executed";
            InitializeViewModels();
            PlanogramSequence sequence = _viewModel.Planogram.Model.Sequence;
            Boolean hasAtLeastOneSequencedProduct = sequence.Groups.SelectMany(g => g.Products).Any();
            if (hasAtLeastOneSequencedProduct) Assert.Inconclusive("There should be no sequenced products at all to test this.");
            var document = (PlanVisualDocument) _planViewModel.AddNewDocument(DocumentType.Design, true);
            PlanogramProductView productView = document.Planogram.AddProduct();
            PlanogramFixtureView fixtureView = _planViewModel.SourcePlanogram.Fixtures.First();
            var size = new WidthHeightDepthValue(fixtureView.Width, 4, fixtureView.Depth);
            IPlanItem component = document.AddNewComponent(MouseToolType.CreateShelf,
                                                           new PointValue(10, 10, 0),
                                                           new RotationValue(0, 0, 0),
                                                           size,
                                                           fixtureView);
            document.AddNewPositions(component.Component.SubComponents.First(),
                                     new PointValue(0, 0, 0),
                                     new List<PlanogramProductView> {productView});
            Boolean hasPositions = _viewModel.Planogram.Model.Positions.Any();
            if (!hasPositions) Assert.Inconclusive("The planogram should have at least one position to test this.");
            _viewModel.CurrentBlocking.Dividers.Add(0F, 0F, PlanogramBlockingDividerType.Horizontal);

            //set reponse to ok message:
            base.WindowService.ShowMessageSetReponse(Framework.Controls.Wpf.ModalMessageResult.Button1);

            _viewModel.GenerateProductSequenceCommand.Execute();

            hasAtLeastOneSequencedProduct = sequence.Groups.SelectMany(g => g.Products).Any();
            Assert.IsTrue(hasAtLeastOneSequencedProduct, expectation);
        }

        #endregion

        #region Clear Blocking

        [Test]
        public void ClearBlocking_Executed()
        {
            //  Setup the database.
            SetMockRepositoryConnection();

            //add a plan and blocking
            var controller = base.AddNewEmptyPlan();
            var vm = (BlockingPlanDocument)controller.AddNewDocument(DocumentType.Blocking, false);

            vm.CurrentBlocking.Dividers.Add(0, 0.5F, PlanogramBlockingDividerType.Horizontal);

            //check
            Assert.AreEqual(2, vm.CurrentBlocking.Groups.Count);
            Assert.AreEqual(2, vm.CurrentBlocking.Locations.Count);
            Assert.AreEqual(1, vm.CurrentBlocking.Dividers.Count);

            //execute
            vm.ClearBlockingCommand.Execute();

            Assert.AreEqual(0, vm.CurrentBlocking.Groups.Count);
            Assert.AreEqual(0, vm.CurrentBlocking.Locations.Count);
            Assert.AreEqual(0, vm.CurrentBlocking.Dividers.Count);
        }

        [Test]
        public void ClearBlocking_OnlyClearsCurrentBlocking()
        {
            //  Setup the database.
            SetMockRepositoryConnection();

            //add a plan and blocking
            var controller = base.AddNewEmptyPlan();
            

            //Add initial and performance blocking
            PlanogramBlocking initial = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            controller.SourcePlanogram.Model.Blocking.Add(initial);
            initial.Dividers.Add(0, 0.7F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlocking perfApplied = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.PerformanceApplied);
            controller.SourcePlanogram.Model.Blocking.Add(perfApplied);
            perfApplied.Dividers.Add(0, 0.7F, PlanogramBlockingDividerType.Horizontal);

            var vm = (BlockingPlanDocument)controller.AddNewDocument(DocumentType.Blocking, false);
            vm.CurrentBlocking.Dividers.Add(0, 0.5F, PlanogramBlockingDividerType.Horizontal);

            //execute
            vm.ClearBlockingCommand.Execute();

            //check
            Assert.AreEqual(0, vm.CurrentBlocking.Groups.Count);
            Assert.AreEqual(0, vm.CurrentBlocking.Locations.Count);
            Assert.AreEqual(0, vm.CurrentBlocking.Dividers.Count);

            Assert.AreNotEqual(0, initial.Groups.Count);
            Assert.AreNotEqual(0, initial.Locations.Count);
            Assert.AreNotEqual(0, initial.Dividers.Count);

            Assert.AreNotEqual(0, perfApplied.Groups.Count);
            Assert.AreNotEqual(0, perfApplied.Locations.Count);
            Assert.AreNotEqual(0, perfApplied.Dividers.Count);
        }

        [Test]
        public void ClearBlocking_DisabledWhenNoBlocks()
        {
            //  Setup the database.
            SetMockRepositoryConnection();

            //add a plan and blocking
            var controller = base.AddNewEmptyPlan();
            var vm = (BlockingPlanDocument)controller.AddNewDocument(DocumentType.Blocking, false);

            Assert.IsFalse(vm.ClearBlockingCommand.CanExecute(), "Command should be disabled as there are no blocks");

            vm.CurrentBlocking.Dividers.Add(0, 0.5F, PlanogramBlockingDividerType.Horizontal);

            Assert.IsTrue(vm.ClearBlockingCommand.CanExecute(), "Command should be enabled");
        }

        [Test]
        public void ClearBlocking_DisabledWhenNotFinalType()
        {
            //  Setup the database.
            SetMockRepositoryConnection();

            //add a plan and blocking
            var controller = base.AddNewEmptyPlan();


            //Add initial and performance blocking
            PlanogramBlocking initial = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            controller.SourcePlanogram.Model.Blocking.Add(initial);
            initial.Dividers.Add(0, 0.7F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlocking perfApplied = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.PerformanceApplied);
            controller.SourcePlanogram.Model.Blocking.Add(perfApplied);
            perfApplied.Dividers.Add(0, 0.7F, PlanogramBlockingDividerType.Horizontal);

            var vm = (BlockingPlanDocument)controller.AddNewDocument(DocumentType.Blocking, false);
            vm.CurrentBlocking.Dividers.Add(0, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlocking final = vm.CurrentBlocking.Model;

            Assert.AreEqual(PlanogramBlockingType.Final, vm.CurrentBlocking.Model.Type); 
            Assert.IsTrue(vm.ClearBlockingCommand.CanExecute());

            //select initial
            vm.CurrentBlocking = new PlanogramBlockingViewModel(initial, vm.Planogram.Model.Sequence);
            Assert.IsFalse(vm.ClearBlockingCommand.CanExecute());

            //select perf applied
            vm.CurrentBlocking = new PlanogramBlockingViewModel(perfApplied, vm.Planogram.Model.Sequence);
            Assert.IsFalse(vm.ClearBlockingCommand.CanExecute());

            //select final
            vm.CurrentBlocking = new PlanogramBlockingViewModel(final, vm.Planogram.Model.Sequence);
            Assert.IsTrue(vm.ClearBlockingCommand.CanExecute());
        }

        #endregion

        #endregion

        #region Other

        [Test]
        public void Constructor_NoRepositoryConnection()
        {
            App.ViewState.ClearRepositoryConnection();
            Assert.IsFalse(App.ViewState.IsConnectedToRepository);

            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                           (p) => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            BlockingPlanDocument viewModel = (BlockingPlanDocument) newPlan.AddNewDocument(DocumentType.Blocking, true);
        }

        [Test]
        public void WhenControllerIsNoLongerActive_ShouldResetTool()
        {
            InitializeViewModels();
            _viewModel.ToolType = BlockingToolType.HorizontalDivider;

            _planViewModel.IsActivePlanController = false;

            Assert.IsTrue(_viewModel.ToolType == BlockingToolType.Pointer);
        }

        #endregion

        #region Test Helper Methods

        private void InitializeViewModels()
        {
            _mainViewModel = App.MainPageViewModel;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      o =>
                                      {
                                          var windowViewModel = (PlanogramPropertiesViewModel) o.WindowParameters[0];
                                          windowViewModel.OKCommand.Execute();
                                      });
            _planViewModel = _mainViewModel.CreateNewPlanogram();
            _viewModel = (BlockingPlanDocument) _planViewModel.AddNewDocument(DocumentType.Blocking, true);
        }

        #endregion
    }
}