﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.01)
// V8-28770 : L.Ineson
// Created
#endregion

#region Version History: CCM830

// V8-32322 : A.Silva
//  Added some tests for drag and drop products.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public sealed class ProductListPlanDocumentTests : TestBase
    {
        [Test]
        public void UnplacedProductsUpdateOnPositionChange()
        {
            // V8-28770 - Check that the unplaced product list automatically
            // updates when a position is removed.

            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.BeginUpdate();
TestDataHelper.CreatePlanCreweSparklingWater(newPlan.SourcePlanogram.Model);
newPlan.SourcePlanogram.EndUpdate();

            PlanogramView planView = newPlan.SourcePlanogram;

            ProductListPlanDocument prodListDoc = (ProductListPlanDocument)newPlan.AddNewDocument(DocumentType.ProductList, true);
            
            //set to unplaced filter
            prodListDoc.PlacementFilterOption = PlacementFilterType.Unplaced;

            Assert.AreEqual(0, prodListDoc.Rows.Count, "Should have no rows");

            //remove a position from the plan
            var p1 = planView.EnumerateAllPositions().First();
            var prod1 = p1.Product;
            var sub1 = p1.SubComponent;
            sub1.RemovePosition(p1);

            //product list should now show one row
            Assert.AreEqual(1, prodListDoc.Rows.Count, "Should have 1 row");

            //add it back
            sub1.AddPosition(prod1);

            // product list should have no rows again
            Assert.AreEqual(0, prodListDoc.Rows.Count, "Should have no rows");
        }

        [Test]
        public void CopiesPlanogramProductsToPlanogramProductListWhenNew()
        {
            //  Create the source plan and get some positions to move to the target product list.
            MainPageViewModel mainPageViewModel = App.MainPageViewModel;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      args => { ((PlanogramPropertiesViewModel) args.WindowParameters[0]).OKCommand.Execute(); });
            PlanControllerViewModel sourcePlan = mainPageViewModel.CreateNewPlanogram();
            TestDataHelper.CreatePlanCreweSparklingWater(sourcePlan.SourcePlanogram.Model);
            PlanogramView sourcePlanView = sourcePlan.SourcePlanogram;
            List<PlanogramProduct> sourceProducts = sourcePlanView.EnumerateAllPositions().Take(2).Select(view => view.Product.Model).ToList();
            IEnumerable<String> expectedGtins = sourceProducts.Select(product => product.Gtin);
            //  Create the target plan.
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                          args => { ((PlanogramPropertiesViewModel)args.WindowParameters[0]).OKCommand.Execute(); });
            PlanControllerViewModel targetPlan = mainPageViewModel.CreateNewPlanogram();
            var productListDoc = targetPlan.AddNewDocument(DocumentType.ProductList, true) as ProductListPlanDocument;
            if (productListDoc == null) Assert.Inconclusive("Could not create the Product List Document.");

            productListDoc.CopyPlanogramProductsToPlanogram(sourceProducts);

            IEnumerable<String> actualGtins = productListDoc.Rows.Select(row => row.PlanItem.Product.Gtin);
            CollectionAssert.AreEquivalent(expectedGtins, actualGtins, "All the selected products should now exist in the target product list.");
        }

        [Test]
        public void CopiesPlanogramProductsToPlanogramProductListWithoutDuplicatingExistingOnes()
        {
            //  Create the source plan and get some positions to move to the target product list.
            MainPageViewModel mainPageViewModel = App.MainPageViewModel;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      args => { ((PlanogramPropertiesViewModel)args.WindowParameters[0]).OKCommand.Execute(); });
            PlanControllerViewModel sourcePlan = mainPageViewModel.CreateNewPlanogram();
            TestDataHelper.CreatePlanCreweSparklingWater(sourcePlan.SourcePlanogram.Model);
            PlanogramView sourcePlanView = sourcePlan.SourcePlanogram;
            List<PlanogramProduct> sourceProducts = sourcePlanView.EnumerateAllPositions().Take(2).Select(view => view.Product.Model).ToList();
            IEnumerable<String> expectedGtins = sourceProducts.Select(product => product.Gtin);
            //  Create the target plan.
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                          args => { ((PlanogramPropertiesViewModel)args.WindowParameters[0]).OKCommand.Execute(); });
            PlanControllerViewModel targetPlan = mainPageViewModel.CreateNewPlanogram();
            targetPlan.AddNewProduct();
            targetPlan.SourcePlanogram.Products.First().Gtin = sourceProducts.First().Gtin;
            var productListDoc = targetPlan.AddNewDocument(DocumentType.ProductList, true) as ProductListPlanDocument;
            if (productListDoc == null) Assert.Inconclusive("Could not create the Product List Document.");

            productListDoc.CopyPlanogramProductsToPlanogram(sourceProducts);

            IEnumerable<String> actualGtins = productListDoc.Rows.Select(row => row.PlanItem.Product.Gtin);
            CollectionAssert.AreEquivalent(expectedGtins, actualGtins, "All the selected products should now exist in the target product list.");
        }
    }
}
