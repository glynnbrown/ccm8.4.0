﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentRegionProductRowTests : TestBase
    {
        private AssortmentRegionProductRow _testRow;

        [SetUp]
        public void SetUp()
        {
            var region = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();
            var primaryProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            _testRow = new AssortmentRegionProductRow(region, primaryProduct);
        }

    }
}
