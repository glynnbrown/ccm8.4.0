﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31763 : A.Silva
//  Created.
// V8-31946 : A.Silva
//  Added PlanogramComparisonEditorViewModel_WhenNoOriginalModel_ShouldCreateNew
//  Added PlanogramComparisonEditorViewModel_WhenOriginalModel_ShouldClone

#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.PlanViews.PlanogramComparison
{
    [TestFixture]
    public class PlanogramComparisonEditorViewModelTests : TestBase
    {
        #region Constructor & Initialization

        [Test]
        public void PlanogramComparisonEditorViewModel_WhenNoOriginalModel_ShouldCreateNew()
        {
            var model = new PlanogramComparisonEditorViewModel();

            Assert.IsNotNull(model.PlanComparisonEdit, "When no original model is passed to edit, a new one should be created.");
        }

        #endregion
    }
}