﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31747 : A.Silva
//  Created.
// V8-31763 : A.Silva
//  Implemented test Command_ShowSettings.
// V8-31873 : A.Silva
//  Refactored Compare and ShowSettings Commands tests.
// V8-31961 : A.Silva
//  Added Date Last Compared property tests.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.PlanViews.PlanogramComparison
{
    [TestFixture]
    public class PlanogramComparisonPlanDocumentTests : TestBase
    {
        #region Test Helper Methods

        private PlanogramComparisonPlanDocument CreateTestModel(String planName = "Master Plan", DateTime? dateLastCompared = null)
        {
            CreateView(planName, dateLastCompared);

            return new PlanogramComparisonPlanDocument(App.MainPageViewModel.PlanControllers.First());
        }

        private void CreateView(String planName = "Master Plan", DateTime? dateLastCompared = null)
        {
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, ExecuteOkCommand);
            PlanogramView view = App.MainPageViewModel.CreateNewPlanogram().SourcePlanogram;
            view.Name = planName;
            view.Comparison.DateLastCompared = dateLastCompared;
        }

        private static void ExecuteOkCommand(NUnitWindowService.NUnitWindowServiceArgs p)
        {
            ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_AvailablePlanograms()
        {
            const String propertyPath = "AvailablePlanograms";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check there is a Binding Property Path defined.
            Assert.IsNotNull(PlanogramComparisonPlanDocument.AvailablePlanogramsProperty);

            //  Check the property path is correct.
            Assert.AreEqual(propertyPath, PlanogramComparisonPlanDocument.AvailablePlanogramsProperty.Path);
        }

        [Test]
        public void Property_DateLastCompared()
        {
            const String propertyPath = "DateLastCompared";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check there is a Binding Property Path defined.
            Assert.IsNotNull(PlanogramComparisonPlanDocument.DateLastComparedProperty);

            //  Check the property path is correct.
            Assert.AreEqual(propertyPath, PlanogramComparisonPlanDocument.DateLastComparedProperty.Path);

            //  Check the default value is null.
            Assert.IsNull(testModel.DateLastCompared);
        }

        [Test]
        public void Property_DocumentType()
        {
            const String propertyPath = "DocumentType";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check the document type is correctly set.
            Assert.AreEqual(DocumentType.PlanogramComparison, testModel.DocumentType);

            //  Check that the document type cannot be writen to.
            AssertHelper.AssertPropertyIsReadOnly(typeof (PlanogramComparisonPlanDocument), propertyPath);
        }

        [Test]
        public void Property_FilterType()
        {
            const String propertyPath = "FilterType";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check there is a Binding Property Path defined.
            Assert.IsNotNull(PlanogramComparisonPlanDocument.FilterTypeProperty);

            //  Check the property path is correct.
            Assert.AreEqual(propertyPath, PlanogramComparisonPlanDocument.FilterTypeProperty.Path);

            //  Check the default value is Products.
            Assert.AreEqual(PlanogramComparisonFilterType.Products, testModel.FilterType);
        }

        [Test]
        public void Property_HasComparisonResults()
        {
            const String propertyPath = "HasComparisonResults";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check there is a Binding Property Path defined.
            Assert.IsNotNull(PlanogramComparisonPlanDocument.HasComparisonResultsProperty);

            //  Check the property path is correct.
            Assert.AreEqual(propertyPath, PlanogramComparisonPlanDocument.HasComparisonResultsProperty.Path);

            //  Check that the default value is false.
            Assert.IsFalse(testModel.HasComparisonResults);
        }

        [Test]
        public void Property_IsShowUnchanged()
        {
            const String propertyPath = "IsShowUnchanged";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check there is a Binding Property Path defined.
            Assert.IsNotNull(PlanogramComparisonPlanDocument.IsShowUnchangedProperty);

            //  Check the property path is correct.
            Assert.AreEqual(propertyPath, PlanogramComparisonPlanDocument.IsShowUnchangedProperty.Path);

            //  Check that the default value is false.
            Assert.IsFalse(testModel.IsShowUnchanged);
        }

        [Test]
        public void Property_Rows()
        {
            const String propertyPath = "Rows";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check there is a Binding Property Path defined.
            Assert.IsNotNull(PlanogramComparisonPlanDocument.RowsProperty);

            //  Check the property path is correct.
            Assert.AreEqual(propertyPath, PlanogramComparisonPlanDocument.RowsProperty.Path);

            //  Check the default value is an empty list.
            CollectionAssert.IsEmpty(testModel.Rows);

            //  Check the property is readonly.
            AssertHelper.AssertPropertyIsReadOnly(typeof (PlanogramComparisonPlanDocument), propertyPath);

            //  Check the collection is readonly.
            AssertHelper.AssertIsReadOnlyObservable(testModel.Rows);
        }

        [Test]
        public void Property_SelectedRows()
        {
            const String propertyPath = "SelectedRows";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check there is a Binding Property Path defined.
            Assert.IsNotNull(PlanogramComparisonPlanDocument.SelectedRowsProperty);

            //  Check the property path is correct.
            Assert.AreEqual(propertyPath, PlanogramComparisonPlanDocument.SelectedRowsProperty.Path);

            //  Check the default value is an empty list.
            CollectionAssert.IsEmpty(testModel.SelectedRows);

            // Check that the collection is observable.
            AssertHelper.AssertIsEditableObservable(testModel.SelectedRows);
        }

        [Test]
        public void Property_Title()
        {
            const String propertyPath = "Title";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check the title is correctly set.
            Assert.AreEqual(DocumentTypeHelper.FriendlyNames[DocumentType.PlanogramComparison], testModel.Title);

            //  Check that the title cannot be writen to.
            AssertHelper.AssertPropertyIsReadOnly(typeof (PlanogramComparisonPlanDocument), propertyPath);
        }

        #endregion

        #region Commands

        [Test]
        public void Command_ShowSettings()
        {
            const String expectation = "The ShowSettingsCommand was not registered.";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            //  Check the command is registered.
            Assert.Contains(testModel.ShowSettingsCommand, testModel.ViewModelCommands, expectation);
        }

        [Test]
        public void ShowSettingsCommand_ShouldBeEnabled()
        {
            const String expectation = "ShowSettingsCommand should always be enabled.";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            Boolean canExecute = testModel.ShowSettingsCommand.CanExecute();

            Assert.IsTrue(canExecute,expectation);
        }

        [Test]
        public void ShowSettingsCommand_WhenExecuted_ShouldDisplayPlanogramCompareSettingsWindow()
        {
            const String expectation = "When executed, the ShowSettingsCommand should display the Planogram Compare Settings Window.";
            const String shouldHaveViewModelOfExpectedType = "The window's view model was not of the expected type.";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();
            Type expected = typeof(PlanogramComparisonEditorOrganiser);
            Type actual = null;
            WindowService.ClearResponseQueue();
            WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                args =>
                {
                    actual = args.WindowType;
                    if (!(args.WindowParameters.First() is PlanogramComparisonEditorViewModel))
                    {
                        Assert.Inconclusive(shouldHaveViewModelOfExpectedType);
                    }
                });

            testModel.ShowSettingsCommand.Execute();

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void Command_Compare()
        {
            const String expectation = "The CompareCommand was not registered.";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();

            Assert.Contains(testModel.CompareCommand, testModel.ViewModelCommands, expectation);
        }

        [Test]
        public void CompareCommand_WhenNoPlanograms_ShouldBeDisabled()
        {
            const String expectation = "When there are no open planograms, CompareCommand should be disabled.";
            const String shouldNotHaveInitialPlanograms = "There should be no initial planograms to run this test.";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();
            if (App.MainPageViewModel.PlanControllers.Count > 1) Assert.Inconclusive(shouldNotHaveInitialPlanograms);

            Boolean canExecute = testModel.CompareCommand.CanExecute();

            Assert.IsFalse(canExecute, expectation);
        }

        #endregion

        #region Initialization Behavior

        [Test]
        public void DateLastComparedIsInitializedFromMasterPlanogram()
        {
            const String expectation =
                "When Planogram Comparison Document is initialized, the Date Last Compared should have been got from the related Planogram Comparison.";
            DateTime? expected = new DateTime(2015, 09, 15, 14, 0, 0);
            PlanogramComparisonPlanDocument testModel = CreateTestModel(dateLastCompared: expected);

            DateTime? actual = testModel.DateLastCompared;

            Assert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region Filter Type Behavior

        [Test]
        public void ChangingFilterTypeShouldReloadResultRows()
        {
            Assert.Inconclusive("Not implemented.");
            //PlanogramComparisonPlanDocument testModel = CreateTestModel();
            //PlanControllerViewModel viewModel = App.MainPageViewModel.PlanControllers.First();
            //viewModel.AddNewProduct();
            //PlanogramProductView planogramProductView = viewModel.SourcePlanogram.Products.First();
           // viewModel.AddNewPosition();
        }

        #endregion

        #region Available Planograms Behavior

        [Test]
        public void OpeningPlanogramsUpdatesAvailablePlanograms()
        {
            //  No Planograms open other than the first.
            PlanogramComparisonPlanDocument testModel = CreateTestModel();
            List<String> actual = testModel.AvailablePlanograms.Select(row => row.Name).ToList();
            IEnumerable<String> expected = App.MainPageViewModel.PlanControllers.Select(model => model.SourcePlanogram.Name);
            CollectionAssert.AreEquivalent(expected, actual, "There should be available the same planograms as open.");

            //  Open plans...
            CreateView("Plan One");
            CreateView("Plan Two");
            List<String> oldActual = actual;
            actual = testModel.AvailablePlanograms.Select(row => row.Name).ToList();
            CollectionAssert.AreNotEqual(oldActual, actual, "New available collection should be different after opening planograms.");
            expected = App.MainPageViewModel.PlanControllers.Select(model => model.SourcePlanogram.Name);
            CollectionAssert.AreEquivalent(expected, actual, "There should be available the same planograms as open.");
        }

        [Test]
        public void ClosingPlanogramsUpdatesAvailablePlanograms()
        {
            //  No Planograms open other than the first.
            PlanogramComparisonPlanDocument testModel = CreateTestModel();
            CreateView("Plan One");
            CreateView("Plan Two");
            List<String> actual = testModel.AvailablePlanograms.Select(row => row.Name).ToList();
            IEnumerable<String> expected = App.MainPageViewModel.PlanControllers.Select(model => model.SourcePlanogram.Name);
            CollectionAssert.AreEquivalent(expected, actual, "There should be available the same planograms as open.");

            //  Remove open plans...
            App.MainPageViewModel.ClosePlanController(App.MainPageViewModel.PlanControllers.Last());
            App.MainPageViewModel.ClosePlanController(App.MainPageViewModel.PlanControllers.Last());
            List<String> oldActual = actual;
            actual = testModel.AvailablePlanograms.Select(row => row.Name).ToList();
            CollectionAssert.AreNotEqual(oldActual, actual, "New available collection should be different after closing planograms.");
            expected = App.MainPageViewModel.PlanControllers.Select(model => model.SourcePlanogram.Name);
            CollectionAssert.AreEquivalent(expected, actual, "There should be available the same planograms as open.");
        }

        #endregion

        #region Planograms Panel Behavior

        [Test]
        public void PlanogramsPanelIsExpandedIfComparisonHasNoResults()
        {
            const String expectation = "The Planograms Panel should be expanded as there are no results in the planogram comparison.";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();
            
            Boolean condition = testModel.PlanogramPanelIsExpanded;

            Assert.IsTrue(condition, expectation);
        }

        [Test]
        public void PlanogramsPanelIsCollapsedIfComparisonHasResults()
        {
            const String expectation = "The Planograms Panel should be collapsed as there are results in the planogram comparison.";
            PlanogramComparisonPlanDocument testModel = CreateTestModel();
            CreateView("Other Plan");
            testModel.CompareCommand.Execute();

            Boolean condition = testModel.PlanogramPanelIsExpanded;

            Assert.IsFalse(condition, expectation);
        }

        #endregion
    }
}