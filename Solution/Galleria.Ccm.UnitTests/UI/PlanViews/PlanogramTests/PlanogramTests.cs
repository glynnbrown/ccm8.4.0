﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Controls.Wpf.PlanVisualisation;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.PlanViews.PlanogramTests
{
    [TestFixture]
    class PlanogramTests
    {
        [Test]
        public void TestCheckThatProductCanBeMovedAroundAPegBoard()
        {
            var testPlanogram = Planogram.NewPlanogram();
            var bay = testPlanogram.AddFixtureItem();
            var pegBoard = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            var product = testPlanogram.Products.AddNew();

            var fixture = PlanogramFixture.NewPlanogramFixture();


            var position = pegBoard.AddPosition(bay, product);
            position.X = 10;
            position.Y = 10;

            var planogramViewModel = new PlanogramViewModel(testPlanogram);
            PlanogramFixtureViewModel fixtureViewModel = new PlanogramFixtureViewModel(planogramViewModel, bay, PlanogramFixture.NewPlanogramFixture("test"));
            
            
        }
    }
}
