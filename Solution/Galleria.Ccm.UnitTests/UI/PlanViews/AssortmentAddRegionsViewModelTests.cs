﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26491 : A.Kuszyk
//  Created.

#endregion

#region Version History : CCM830

// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll commands tests, amended others accordingly.

#endregion

#endregion

using System;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentAddRegionsViewModelTests : TestBase
    {
        #region Fields

        private AssortmentAddRegionsViewModel _viewModel;

        #endregion

        #region Test Helper Methods

        private void AddAllLocations()
        {
            foreach (Location info in _viewModel.AvailableLocations)
            {
                _viewModel.SelectedAvailableLocations.Add(info);
            }
            _viewModel.AddSelectedLocationsCommand.Execute();
        }

        #endregion

        [SetUp]
        public void SetUp()
        {
            Int32 entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id;
            Editor.Client.Wpf.App.ViewState.EntityId = entityId;
            TestDataHelper.InsertLocationDtos(DalFactory, 5, entityId);
            PlanogramAssortment assortment = PlanogramAssortment.NewPlanogramAssortment();
            _viewModel = new AssortmentAddRegionsViewModel(assortment);
        }

        [Test]
        public void Command_Apply()
        {
            RelayCommand cmd = _viewModel.ApplyCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as region not valid");

            _viewModel.AddRegionCommand.Execute();

            String testRegionName = "Test";
            _viewModel.SelectedRegionRow.Region.Name = testRegionName;
            AddAllLocations();

            Assert.IsTrue((_viewModel.CurrentAssortment.Regions.Count == 0), "No current Assortment regions");

            cmd.Execute();
            Assert.IsTrue((_viewModel.CurrentAssortment.Regions.Count == 1), "Assortment local region applied");
        }

        [Test]
        public void Command_ApplyAndClose()
        {
            RelayCommand cmd = _viewModel.ApplyAndCloseCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as regions valid");

            _viewModel.AddRegionCommand.Execute();

            String testRegionName = "Test";
            _viewModel.SelectedRegionRow.Region.Name = testRegionName;
            AddAllLocations();

            Assert.IsTrue((_viewModel.CurrentAssortment.Regions.Count == 0), "No current Assortment regions");

            cmd.Execute();
            Assert.IsTrue((_viewModel.CurrentAssortment.Regions.Count == 1), "Assortment local region applied");
        }

        [Test]
        public void Command_Cancel()
        {
            RelayCommand cmd = _viewModel.CancelCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsTrue(cmd.CanExecute(), "Should be Enabled");
        }

        [Test]
        public void Command_AddRegion()
        {
            RelayCommand cmd = _viewModel.AddRegionCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsTrue(cmd.CanExecute(), "Should be Enabled");

            Assert.IsNull(_viewModel.SelectedRegionRow, "There shouldnt be any selected region");

            cmd.Execute();

            Assert.IsNotNull(_viewModel.SelectedRegionRow, "There should be a selected region");
        }

        [Test]
        public void Command_RemoveSelectedRegion()
        {
            RelayCommand cmd = _viewModel.RemoveSelectedRegionCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabed as no region selected");
        }

        [Test]
        public void Command_AddSelectedLocations()
        {
            RelayCommand cmd = _viewModel.AddSelectedLocationsCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as region not valid");

            _viewModel.AddRegionCommand.Execute();

            _viewModel.SelectedAvailableLocations.Add(_viewModel.AvailableLocations.First());
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled region valid and locations assigned");

            Assert.AreEqual(0, _viewModel.SelectedRegionRow.RegionLocations.Count, "There shouldnt be any locations initially assigned");

            cmd.Execute();

            Assert.AreEqual(1, _viewModel.SelectedRegionRow.RegionLocations.Count, "The first product should be assigned");
        }

        [Test]
        public void Command_RemoveSelectedLocations()
        {
            RelayCommand cmd = _viewModel.RemoveSelectedLocationsCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as region not valid");

            _viewModel.AddRegionCommand.Execute();
            AddAllLocations();

            Location assortmentLocation = _viewModel.SelectedRegionRow.RegionLocations.First();
            _viewModel.SelectedRegionLocations.Add(assortmentLocation);

            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as location selected for removal");

            Assert.AreEqual(_viewModel.SelectedRegionRow.RegionLocations.Count(p => p.Id == assortmentLocation.Id), 1, "Location should be assigned");

            cmd.Execute();

            Assert.AreEqual(_viewModel.SelectedRegionRow.RegionLocations.Count(p => p.Id == assortmentLocation.Id), 0, "Location should no longer be assigned");
        }
    }
}