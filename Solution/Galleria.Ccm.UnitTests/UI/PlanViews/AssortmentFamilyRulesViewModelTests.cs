﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.Collections.Generic;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentFamilyRulesViewModelTests : TestBase
    {
        private AssortmentFamilyRulesViewModel _viewModel;

        [SetUp]
        public void SetUp()
        {
            var assortment = PlanogramAssortment.NewPlanogramAssortment();
            var assortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            assortmentProduct.FamilyRuleName = "FamilyRule";
            assortmentProduct.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None;
            assortment.Products.Add(assortmentProduct);
            _viewModel = new AssortmentFamilyRulesViewModel(assortment, assortmentProduct, new List<IPlanItem>());
        }

        [Test]
        public void Command_AddFamilyRule()
        {
            RelayCommand cmd = this._viewModel.AddFamilyRuleCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            //check can excute
            Assert.IsTrue(cmd.CanExecute(), "Should be Enabled");

            Assert.IsNull(this._viewModel.CurrentAssortmentFamilyRule, "There shouldnt be any selected family rules");

            cmd.Execute();

            Assert.IsNotNull(this._viewModel.CurrentAssortmentFamilyRule, "There should be a selected family rule");
        }

        [Test]
        public void Command_Remove()
        {
            RelayCommand cmd = this._viewModel.RemoveCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            //check can execute
            Assert.IsFalse(cmd.CanExecute(), "command should be disabled for initial item");
            this._viewModel.AddFamilyRuleCommand.Execute();

            // Add Family Rule
            String testFamilyRuleName = "Test";
            this._viewModel.SelectedFamilyRuleName = testFamilyRuleName;
            this._viewModel.SelectedFamilyRuleType = PlanogramAssortmentProductFamilyRuleType.DependencyList;
            foreach (PlanogramAssortmentProductView assortmentProduct in _viewModel.AvailableProducts)
            {
                _viewModel.SelectedAvailableProducts.Add(assortmentProduct);
            }
            _viewModel.AddSelectedProductsCommand.Execute();
            this._viewModel.ApplyCommand.Execute();

            Assert.IsTrue(cmd.CanExecute(this._viewModel.CurrentAssortmentFamilyRule), "command should be enabled for old item");

            this._viewModel.CurrentAssortmentFamilyRule = this._viewModel.CurrentAssortmentFamilyRules.First();

            //check execution
            cmd.Execute(this._viewModel.CurrentAssortmentFamilyRule);

            //attempt the fetch the item  back
            Assert.AreEqual(this._viewModel.CurrentAssortmentFamilyRules.Where(p => p.FamilyRuleName == testFamilyRuleName).Count(), 0, "family rule should no longer exist");
        }

        [Test]
        public void Command_Apply()
        {
            RelayCommand cmd = this._viewModel.ApplyCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            String testFamilyRuleName = "Test";
            this._viewModel.AddFamilyRuleCommand.Execute();

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            this._viewModel.SelectedFamilyRuleName = testFamilyRuleName;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            this._viewModel.SelectedFamilyRuleType = PlanogramAssortmentProductFamilyRuleType.DependencyList;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            foreach (PlanogramAssortmentProductView assortmentProduct in _viewModel.AvailableProducts)
            {
                _viewModel.SelectedAvailableProducts.Add(assortmentProduct);
            }
            _viewModel.AddSelectedProductsCommand.Execute();
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled family rule valid and products assigned");

            cmd.Execute();
            Assert.IsTrue((this._viewModel.CurrentAssortmentFamilyRules.Count == 1), "Assortment family rule should be applied");
        }

        [Test]
        public void Command_ApplyAndClose()
        {
            RelayCommand cmd = this._viewModel.ApplyAndCloseCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no family rule changes made");

            this._viewModel.AddFamilyRuleCommand.Execute();
            // Add Family Rule
            String testFamilyRuleName = "Test";
            this._viewModel.SelectedFamilyRuleName = testFamilyRuleName;
            this._viewModel.SelectedFamilyRuleType = PlanogramAssortmentProductFamilyRuleType.DependencyList;
            foreach (PlanogramAssortmentProductView assortmentProduct in _viewModel.AvailableProducts)
            {
                _viewModel.SelectedAvailableProducts.Add(assortmentProduct);
            }
            _viewModel.AddSelectedProductsCommand.Execute();
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as family rule applied");

            cmd.Execute();
            Assert.IsTrue((this._viewModel.CurrentAssortmentFamilyRules.Count == 1), "Assortment family rule should be applied");
        }

        [Test]
        public void Command_AddSelectedProducts()
        {
            RelayCommand cmd = this._viewModel.AddSelectedProductsCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            String testFamilyRuleName = "Test";
            this._viewModel.AddFamilyRuleCommand.Execute();

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            this._viewModel.SelectedFamilyRuleName = testFamilyRuleName;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            this._viewModel.SelectedFamilyRuleType = PlanogramAssortmentProductFamilyRuleType.DependencyList;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            this._viewModel.SelectedAvailableProducts.Add(this._viewModel.AvailableProducts.First());
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled family rule valid and products assigned");

            Assert.AreEqual(0, this._viewModel.AssignedProducts.Count, "There shouldnt be any products initially assigned");

            cmd.Execute();

            Assert.AreEqual(1, this._viewModel.AssignedProducts.Count, "The first product should be assigned");
        }

        [Test]
        public void Command_RemoveSelectedProducts()
        {
            RelayCommand cmd = this._viewModel.RemoveSelectedProductsCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            String testFamilyRuleName = "Test";
            this._viewModel.AddFamilyRuleCommand.Execute();

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            this._viewModel.SelectedFamilyRuleName = testFamilyRuleName;
            this._viewModel.SelectedFamilyRuleType = PlanogramAssortmentProductFamilyRuleType.DependencyList;
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as family rule not valid");

            foreach (PlanogramAssortmentProductView item in _viewModel.AvailableProducts)
            {
                _viewModel.SelectedAvailableProducts.Add(item);
            }
            _viewModel.AddSelectedProductsCommand.Execute();

            var assortmentProduct = this._viewModel.AssignedProducts.First();
            this._viewModel.SelectedAssignedProducts.Add(assortmentProduct);

            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as product selected for removal");

            Assert.AreEqual(this._viewModel.SelectedAssignedProducts.Where(p => p.Id == assortmentProduct.Id).Count(), 1, "Product should be assigned");

            cmd.Execute();

            Assert.AreEqual(this._viewModel.SelectedAssignedProducts.Where(p => p.Id == assortmentProduct.Id).Count(), 0, "Product should no longer be assigned");
        }

        [Test]
        public void Command_Cancel()
        {
            RelayCommand cmd = this._viewModel.CancelCommand;
            Assert.Contains(cmd, this._viewModel.ViewModelCommands);

            //check can excute
            Assert.IsTrue(cmd.CanExecute(), "Should be Enabled");
        }
    }
}
