﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-28340 : N.Haywood
//  Created
#endregion
#region Version History: (CCM 8.2)
// V8-30870 : L.Ineson
//  Refactored screen, added new tests and removed the old ones.
#endregion

#region Version History: CCM830

// V8-32157 : A.Silva
//  Amended CalculatedColumn_RemoveDoesNotAddAnotherToAvailable to use remove selected columns instead of remove all.

#endregion

#endregion

using System;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Editor.Wpf.ReportColumnLayouts;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using System.IO;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Collections.Generic;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class ReportColumnLayoutEditorViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private ReportColumnLayoutEditorViewModel CreateViewModel()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan2 = mainPageView.CreateNewPlanogram();
            PlanogramView planView2 = newPlan2.SourcePlanogram;
            planView2.Name = "PLAN2";

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan3 = mainPageView.CreateNewPlanogram();
            PlanogramView planView3 = newPlan3.SourcePlanogram;
            planView3.Name = "PLAN3";

            return new ReportColumnLayoutEditorViewModel(newPlan3);
        }

        #endregion

        #region Property Tests

        #region AvailablePlanograms

        [Test]
        public void AvailablePlanograms_IsInitialized()
        {
            var vm = CreateViewModel();
            Assert.AreEqual(App.MainPageViewModel.PlanControllers.Count, vm.AvailablePlanograms.Count);

            //only the current plan should be selected.
            Assert.AreEqual(1, vm.AvailablePlanograms.Count(p => p.IsSelected));
            Assert.IsTrue(vm.AvailablePlanograms.First(p => p.IsSelected).IsCurrentPlanogram);

            vm.Dispose();
            vm = CreateViewModel();

            //create a layout with select all on
            CustomColumnLayout layout1 = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout1.IncludeAllPlanograms = true;

            vm = new ReportColumnLayoutEditorViewModel(layout1, App.MainPageViewModel.PlanControllers[0]);
            Assert.IsTrue(vm.AvailablePlanograms.All(p => p.IsSelected));
            vm.Dispose();

            //cycle through each plan making sure the correct one is set as the current
            foreach (var controller in App.MainPageViewModel.PlanControllers)
            {
                vm = new ReportColumnLayoutEditorViewModel(controller);
                Assert.AreEqual(1, vm.AvailablePlanograms.Count(p => p.IsSelected));

                var selectedPlan = vm.AvailablePlanograms.First(p => p.IsSelected);
                Assert.AreEqual(controller, selectedPlan.PlanController);
                Assert.IsTrue(selectedPlan.IsCurrentPlanogram);

                vm.Dispose();
            }
        }

        [Test]
        public void AvailablePlanograms_SelectAllUpdatesPlanIsSelected()
        {
            //Checks that checking and unchecking select all
            // will update the plan items accordingly.
            var vm = CreateViewModel();

            //only current should be selected
            Assert.AreEqual(1, vm.AvailablePlanograms.Count(p => p.IsSelected));

            //select
            vm.CurrentLayout.IncludeAllPlanograms = true;
            Assert.IsTrue(vm.AvailablePlanograms.All(p => p.IsSelected));

            //unselect
            vm.CurrentLayout.IncludeAllPlanograms = false;
            Assert.AreEqual(1, vm.AvailablePlanograms.Count(p => p.IsSelected));
            Assert.IsTrue(vm.AvailablePlanograms.First(p => p.IsSelected).IsCurrentPlanogram);
        }

        [Test]
        public void AvailablePlanograms_SelectAllUpdatedOnAllPlansSelected()
        {
            //checks that ticking is selected on all plans 
            // sets the select all property
            var vm = CreateViewModel();

            foreach (var row in vm.AvailablePlanograms)
            {
                row.IsSelected = true;
            }
            Assert.IsTrue(vm.CurrentLayout.IncludeAllPlanograms);
        }

        [Test]
        public void AvailablePlanograms_SelectAllUpdatedWhenPlanUnselected()
        {
            //Checks that if select all is true and a single plan is 
            // unchecked then the value is updated.
            var vm = CreateViewModel();
            vm.CurrentLayout.IncludeAllPlanograms = true;

            vm.AvailablePlanograms.Last().IsSelected = false;
            Assert.IsFalse(vm.CurrentLayout.IncludeAllPlanograms);
        }

        #endregion


        #endregion

        #region Command Tests

        [Test]
        public void MoveColumnUpCommand_Execution()
        {
            //Checks that columns can be moved up.

            ReportColumnLayoutEditorViewModel vm = CreateViewModel();

            //add a couple of column from product
            vm.SelectedFieldGroup = vm.AvailableFieldGroups.First(f => f.GroupName == PlanogramProduct.FriendlyName);
            vm.SelectedFields.Clear();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();

            //check order
            Assert.AreEqual(1, vm.AssignedColumnViews[0].Number);
            Assert.AreEqual(2, vm.AssignedColumnViews[1].Number);
            Assert.AreEqual(3, vm.AssignedColumnViews[2].Number);

            var field1 = vm.AssignedColumnViews[0].Column;
            var field2 = vm.AssignedColumnViews[1].Column;
            var field3 = vm.AssignedColumnViews[2].Column;

            //select the last column
            vm.SelectedAssignedColumnViews.Clear();
            vm.SelectedAssignedColumnViews.Add(vm.AssignedColumnViews[2]);

            //move it up
            vm.MoveColumnUpCommand.Execute();

            //check it - 
            Assert.AreEqual(1, field1.Number);
            Assert.AreEqual(3, field2.Number);
            Assert.AreEqual(2, field3.Number);

            //check that the field is still selected
            Assert.AreEqual(1, vm.SelectedAssignedColumnViews.Count);
            Assert.AreEqual(field3, vm.SelectedAssignedColumnViews[0].Column);

            //move it up again
            vm.MoveColumnUpCommand.Execute();
            Assert.AreEqual(2, field1.Number);
            Assert.AreEqual(3, field2.Number);
            Assert.AreEqual(1, field3.Number);

            //check command is now disabled
            Assert.IsFalse(vm.MoveColumnUpCommand.CanExecute());
        }

        [Test]
        public void MoveColumnDownCommand_Execution()
        {
            // Checks that columns can be moved down.

            ReportColumnLayoutEditorViewModel vm = CreateViewModel();

            //add a couple of column from product
            vm.SelectedFieldGroup = vm.AvailableFieldGroups.First(f => f.GroupName == PlanogramProduct.FriendlyName);
            vm.SelectedFields.Clear();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();

            //check order
            Assert.AreEqual(1, vm.AssignedColumnViews[0].Number);
            Assert.AreEqual(2, vm.AssignedColumnViews[1].Number);
            Assert.AreEqual(3, vm.AssignedColumnViews[2].Number);

            var field1 = vm.AssignedColumnViews[0].Column;
            var field2 = vm.AssignedColumnViews[1].Column;
            var field3 = vm.AssignedColumnViews[2].Column;

            //select the first column
            vm.SelectedAssignedColumnViews.Clear();
            vm.SelectedAssignedColumnViews.Add(vm.AssignedColumnViews[0]);

            //move it up
            vm.MoveColumnDownCommand.Execute();

            //check it - 
            Assert.AreEqual(2, field1.Number);
            Assert.AreEqual(1, field2.Number);
            Assert.AreEqual(3, field3.Number);

            //check that the field is still selected
            Assert.AreEqual(1, vm.SelectedAssignedColumnViews.Count);
            Assert.AreEqual(field1, vm.SelectedAssignedColumnViews[0].Column);

            //move it up again
            vm.MoveColumnDownCommand.Execute();
            Assert.AreEqual(3, field1.Number);
            Assert.AreEqual(1, field2.Number);
            Assert.AreEqual(2, field3.Number);

            //check command is now disabled
            Assert.IsFalse(vm.MoveColumnDownCommand.CanExecute());
        }

        [Test]
        public void OpenFromFile_Execution()
        {
            ReportColumnLayoutEditorViewModel vm = CreateViewModel();

            //add a couple of column from product
            vm.SelectedFieldGroup = vm.AvailableFieldGroups.First(f => f.GroupName == PlanogramProduct.FriendlyName);
            vm.SelectedFields.Clear();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();
            vm.SelectedFields.Add(vm.SelectedGroupFields.First());
            vm.AddSelectedColumnsCommand.Execute();

            //save it
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, CustomColumnLayout.DataSheetFileExtension);
            vm.SaveAsCommand.Execute(expectedPath);
            
            
            //new viewmodel.
            vm.Dispose();
            vm = CreateViewModel();

            //check
            Assert.AreEqual(0, vm.AssignedColumnViews.Count);

            //open
            vm.OpenCommand.Execute(expectedPath);

            //check
            Assert.AreEqual(3, vm.AssignedColumnViews.Count);
        }

        #endregion

        #region CalculatedColumn tests
       
        [Test]
        public void CalculatedColumn_AddNew()
        {
            //checks the ability to add a new calculated column
            var vm = CreateViewModel(); 

            Int32 fieldsCount = vm.SelectedGroupFields.Count;

            //create the field selector response
            String columnText =
                "{" + String.Format("{0} * {1} * {2}",
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsHighProperty).FieldPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsDeepProperty).FieldPlaceholder) + "}";

            String usercolumnText =
                "{" + String.Format("{0} * {1} * {2}",
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldFriendlyPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsHighProperty).FieldFriendlyPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsDeepProperty).FieldFriendlyPlaceholder) + "}";

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) =>
               {
                   var fieldVm = ((FieldSelectorViewModel)p.WindowParameters[0]);
                   fieldVm.Text = usercolumnText;
                   fieldVm.OKCommand.Execute();
               });

            //add it
            vm.AddCalculatedColumnCommand.Execute();

            Assert.AreEqual(1, vm.AssignedColumnViews.Count);
            Assert.AreEqual(columnText, vm.AssignedColumnViews[0].Column.Path);

            //check the field is still in the all groups list
            Assert.AreEqual(fieldsCount, vm.SelectedGroupFields.Count, "No column should have been removed from the available list");

            //Add another and check that the display names are different
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) =>
               {
                   var fieldVm = ((FieldSelectorViewModel)p.WindowParameters[0]);
                   fieldVm.Text = usercolumnText;
                   fieldVm.OKCommand.Execute();
               });
            vm.AddCalculatedColumnCommand.Execute();

            Assert.AreNotEqual(vm.AssignedColumnViews[0].DisplayName, vm.AssignedColumnViews[1].DisplayName);
        }

        [Test]
        public void CalculatedColumn_AddNewPicksUpExisting()
        {
            //checks that creating a calculated column that is just a single field
            // existing one will just add the existing field.
            var vm = CreateViewModel();
            Int32 fieldsCount = vm.SelectedGroupFields.Count;

            //create the field selector response
            String columnText =ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldPlaceholder;
            String usercolumnText = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldFriendlyPlaceholder;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) =>
               {
                   var fieldVm = ((FieldSelectorViewModel)p.WindowParameters[0]);
                   fieldVm.Text = usercolumnText;
                   fieldVm.OKCommand.Execute();
               });

            //add it
            vm.AddCalculatedColumnCommand.Execute();

            Assert.AreEqual(1, vm.AssignedColumnViews.Count);
            Assert.AreEqual(columnText, vm.AssignedColumnViews[0].Column.Path);

            //check the field is still in the all groups list
            Assert.AreEqual(fieldsCount -1, vm.SelectedGroupFields.Count, "The existing column should have been removed.");
        }

        [Test]
        public void CalculatedColumn_RemoveDoesNotAddAnotherToAvailable()
        {
            //checks that removing a calculated column does not add
            // it to the available list

            var vm = CreateViewModel();
           

            //create the field selector response
            String columnText =
                "{" + String.Format("{0} * {1} * {2}",
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsHighProperty).FieldPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsDeepProperty).FieldPlaceholder) + "}";

            String usercolumnText =
                "{" + String.Format("{0} * {1} * {2}",
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldFriendlyPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsHighProperty).FieldFriendlyPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsDeepProperty).FieldFriendlyPlaceholder) + "}";

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) =>
               {
                   var fieldVm = ((FieldSelectorViewModel)p.WindowParameters[0]);
                   fieldVm.Text = usercolumnText;
                   fieldVm.OKCommand.Execute();
               });

            //add it
            vm.AddCalculatedColumnCommand.Execute();

            //select it and remove it
            Int32 fieldsCount = vm.SelectedGroupFields.Count;

            foreach (ReportColumnLayoutEditorViewModel.DataSheetColumnView view in vm.AssignedColumnViews)
            {
                vm.SelectedAssignedColumnViews.Add(view);
            }

            vm.RemoveSelectedColumnsCommand.Execute();

            Assert.AreEqual(0, vm.AssignedColumnViews.Count, "Calculated column should have been removed");
            Assert.AreEqual(fieldsCount, vm.SelectedGroupFields.Count, "Available count should not have changed.");
            
        }

        [Test]
        public void CalculatedColumn_OpenLayoutWithPreexisting()
        {
            //checks the load of a layout which already has a calculated column.
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);

            String columnText =
                "{" + String.Format("{0} * {1} * {2}",
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsHighProperty).FieldPlaceholder,
                        ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsDeepProperty).FieldPlaceholder) + "}";

            layout.Columns.Add(CustomColumn.NewCustomColumn(columnText, 1));

            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            var vm = new ReportColumnLayoutEditorViewModel(layout, newPlan);


            Assert.AreEqual(1, vm.AssignedColumnViews.Count);
            Assert.AreEqual(columnText, vm.AssignedColumnViews[0].Column.Path);

        }

        #endregion

        #region Factory Tests

        [Test]
        public void FactoryFields()
        {
            ReportColumnLayoutFactory factory = new ReportColumnLayoutFactory(typeof(Object));

            Dictionary<String, ObjectFieldInfo> masterFieldLookup = new Dictionary<string, ObjectFieldInfo>();

            foreach (ObjectFieldInfo f in factory.GetModelObjectFields())
            {
                if (masterFieldLookup.Keys.Contains(f.FieldPlaceholder))
                    Assert.Fail("Already added: " + f.FieldPlaceholder);

                masterFieldLookup.Add(f.FieldPlaceholder, f);
            }
        }

        #endregion
    }
}