﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentFamilyRuleRowTests
    {
        private AssortmentFamilyRuleRow _row;
        private List<PlanogramAssortmentProduct>_assignedProducts;
        private String _newName = "Hello World!";
        private PlanogramAssortmentProductFamilyRuleType _newType = PlanogramAssortmentProductFamilyRuleType.MaximumProductCount;
        private Byte? _newValue = 1;

        [SetUp]
        public void SetUp()
        {
            _assignedProducts = new List<PlanogramAssortmentProduct>()
                {
                    PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(),
                    PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(),
                    PlanogramAssortmentProduct.NewPlanogramAssortmentProduct()
                };

            _row = new AssortmentFamilyRuleRow(
                String.Empty,
                PlanogramAssortmentProductFamilyRuleType.None,
                null,
                _assignedProducts);
        }

        [Test]
        public void ToString_ReturnsFamilyRuleName()
        {
            Assert.AreEqual(_row.FamilyRuleName, _row.ToString());
        }

        [Test]
        public void CommitChanges_ChangesValuesForAllAssignedProducts()
        {
            _row.FamilyRuleName = _newName;
            _row.FamilyRuleType = _newType;
            _row.FamilyRuleValue = _newValue;

            _row.CommitChanges();

            foreach (var product in _assignedProducts)
            {
                Assert.AreEqual(_newName, product.FamilyRuleName);
                Assert.AreEqual(_newType, product.FamilyRuleType);
                Assert.AreEqual(_newValue, product.FamilyRuleValue);
            }
        }

        [Test]
        public void ClearRule_ClearsValues()
        {
            _row.FamilyRuleName = _newName;
            _row.FamilyRuleType = _newType;
            _row.FamilyRuleValue = _newValue;

            _row.ClearRule();

            Assert.AreEqual(String.Empty,_row.FamilyRuleName);
            Assert.AreEqual(PlanogramAssortmentProductFamilyRuleType.None, _row.FamilyRuleType);
            Assert.IsNull(_row.FamilyRuleValue);
        }

        [Test]
        public void DeleteFamilyRule_ClearsValuesForAllAssignedProducts()
        {
            _row.FamilyRuleName = _newName;
            _row.FamilyRuleType = _newType;
            _row.FamilyRuleValue = _newValue;
            _row.CommitChanges();

            _row.DeleteFamilyRule();

            foreach (var product in _assignedProducts)
            {
                Assert.AreEqual(String.Empty, product.FamilyRuleName);
                Assert.AreEqual(PlanogramAssortmentProductFamilyRuleType.None, product.FamilyRuleType);
                Assert.IsNull(product.FamilyRuleValue);
            }
        }
    }
}
