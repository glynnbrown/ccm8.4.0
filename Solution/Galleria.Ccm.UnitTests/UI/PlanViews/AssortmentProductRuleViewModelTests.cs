﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentProductRuleViewModelTests : TestBase
    {
        private AssortmentProductRuleViewModel _viewModel;

        [SetUp]
        public void SetUp()
        {
            var assortment = PlanogramAssortment.NewPlanogramAssortment();
            for (Int32 i = 0; i < 5; i++)
            {
                var product = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
                product.Name = String.Format("Name{0}",i);
                product.Gtin = String.Format("Gtin{0}", i);
                //product.LocationCode = String.Format("Code{0}",i);
                assortment.Products.Add(product); 
            }
            _viewModel = new AssortmentProductRuleViewModel(assortment, new List<IPlanItem>());
        }

        [Test]
        public void Command_Apply()
        {
            String propertyName = "ApplyCommand";
            Assert.AreEqual(propertyName, AssortmentProductRuleViewModel.ApplyCommandProperty.Path);

            RelayCommand cmd = _viewModel.ApplyCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);

            //check can execute

            //no editted product rows
            Assert.IsFalse(cmd.CanExecute(), cmd.DisabledReason);

            //Take copy of preselected
            List<AssortmentProductRuleRow> preSelectedRows = _viewModel.SelectedProductRows.ToList();

            //set rule up
            _viewModel.EditRuleType = PlanogramAssortmentProductRuleType.Preserve;
            _viewModel.EditValue = 3;

            Assert.IsTrue(cmd.CanExecute(), cmd.DisabledReason);

            cmd.Execute();

            //check the same product is still selected
            Assert.AreEqual(1, _viewModel.SelectedProductRows.Count);
            Assert.AreEqual(_viewModel.ProductRows[0], _viewModel.SelectedProductRows[0]);

            //check the rule got added
            Assert.AreEqual(PlanogramAssortmentProductRuleType.Preserve, _viewModel.ProductRows[0].Product.RuleType);
            Assert.AreEqual(PlanogramAssortmentProductRuleValueType.Units, _viewModel.ProductRows[0].Product.RuleValueType);
            Assert.AreEqual(3, _viewModel.ProductRows[0].Product.RuleValue);

            //check that the apply command is now disabled
            Assert.IsFalse(cmd.CanExecute());
        }

        [Test]
        public void Command_ApplyAndClose()
        {
            String propertyName = "ApplyAndCloseCommand";
            Assert.AreEqual(propertyName, AssortmentProductRuleViewModel.ApplyAndCloseCommandProperty.Path);

            RelayCommand cmd = _viewModel.ApplyAndCloseCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);


            //check can execute

            //no editted product rows
            Assert.IsFalse(cmd.CanExecute());

            //Take copy of preselected
            List<AssortmentProductRuleRow> preSelectedRows = _viewModel.SelectedProductRows.ToList();

            //set rule up
            _viewModel.EditRuleType = PlanogramAssortmentProductRuleType.Force;
            _viewModel.EditValueType = PlanogramAssortmentProductRuleValueType.Facings;
            _viewModel.EditValue = 5;

            Assert.IsTrue(cmd.CanExecute());

            cmd.Execute();

            //check the same product is still selected
            Assert.AreEqual(1, _viewModel.SelectedProductRows.Count);
            Assert.AreEqual(_viewModel.ProductRows[0], _viewModel.SelectedProductRows[0]);

            //check the rule got added
            Assert.AreEqual(PlanogramAssortmentProductRuleType.Force, _viewModel.ProductRows[0].Product.RuleType);
            Assert.AreEqual(PlanogramAssortmentProductRuleValueType.Facings, _viewModel.ProductRows[0].Product.RuleValueType);
            Assert.AreEqual(5, _viewModel.ProductRows[0].Product.RuleValue);

            //check that the apply command is now disabled
            Assert.IsFalse(cmd.CanExecute());
        }

        [Test]
        public void Command_Cancel()
        {
            String propertyName = "CancelCommand";
            Assert.AreEqual(propertyName, AssortmentProductRuleViewModel.CancelCommandProperty.Path);

            RelayCommand cmd = _viewModel.CancelCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);

            Assert.IsTrue(cmd.CanExecute());
        }

        [Test]
        public void Command_RemoveRule()
        {
            String propertyName = "RemoveRuleCommand";
            Assert.AreEqual(propertyName, AssortmentProductRuleViewModel.RemoveRuleCommandProperty.Path);

            RelayCommand cmd = _viewModel.RemoveRuleCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);
            Assert.IsNotNull(cmd.SmallIcon);

            //check canexecute

            //valid
            _viewModel.EditRuleType = PlanogramAssortmentProductRuleType.MinimumHurdle;
            _viewModel.EditValue = 2;
            Assert.IsTrue(cmd.CanExecute(_viewModel.SelectedProductRows[0]));

            //no row
            Assert.IsFalse(cmd.CanExecute());

            //is null rule
            Assert.IsFalse(cmd.CanExecute(_viewModel.ProductRows[1]));

            //check execute
            Assert.IsFalse(_viewModel.SelectedProductRows[0].IsNullRule);
            cmd.Execute(_viewModel.SelectedProductRows[0]);
            Assert.IsTrue(_viewModel.SelectedProductRows[0].IsNullRule);

        }
    }
}
