﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentProductRuleRowTests
    {
        [Test]
        public void CommitChanges_UpdatesModel()
        {
            var product = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            var assortment = PlanogramAssortment.NewPlanogramAssortment();
            var ruleRow = new AssortmentProductRuleRow(assortment, product, null);

            ruleRow.Product.ExactListFacings = 1;
            ruleRow.Product.ExactListUnits = 1;
            ruleRow.Product.PreserveListFacings = 1;
            ruleRow.Product.PreserveListUnits = 1;
            ruleRow.Product.MinListFacings = 1;
            ruleRow.Product.MinListUnits = 1;
            ruleRow.Product.MaxListFacings = 1;
            ruleRow.Product.MaxListUnits = 1;
            ruleRow.Product.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Optional;

            ruleRow.CommitChanges();

            Assert.AreEqual(1, product.ExactListFacings);
            Assert.AreEqual(1, product.ExactListUnits);
            Assert.AreEqual(1, product.PreserveListFacings);
            Assert.AreEqual(1, product.PreserveListUnits);
            Assert.AreEqual(1, product.MinListFacings);
            Assert.AreEqual(1, product.MinListUnits);
            Assert.AreEqual(1, product.MaxListFacings);
            Assert.AreEqual(1, product.MaxListUnits);
            Assert.AreEqual(PlanogramAssortmentProductTreatmentType.Optional, product.ProductTreatmentType);
        }
    }
}
