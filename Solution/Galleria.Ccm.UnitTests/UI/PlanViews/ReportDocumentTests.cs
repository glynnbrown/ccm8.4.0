﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31541 : L.Ineson
//  Created
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Editor.Wpf.ReportColumnLayouts;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using System.IO;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Controls.Wpf;
using System.Text.RegularExpressions;
using Microsoft.CSharp.RuntimeBinder;
using System.Runtime.CompilerServices;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    public sealed class ReportDocumentTests : TestBase
    {

        #region Properties

        #region Rows

        [Test]
        public void Rows_UpdatedOnNormalPlanDataChange()
        {
            //checks that aggregated data is updated,

            //var newPlan = AddNewCreweSparklingWaterPlan();
            //PlanogramView planView = newPlan.SourcePlanogram;
            //planView.Name = "PLAN1";

            ////create a second plan, same positions as the first.
            //var newPlan2 = AddNewCreweSparklingWaterPlan();
            //PlanogramView planView2 = newPlan2.SourcePlanogram;
            //planView2.Name = "PLAN2";

            ////create the vm
            //ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            ////create a layout at position level 
            //CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            //layout.Columns.Add(
            //    CustomColumn.NewCustomColumn(
            //    ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder,
            //    1));
            //layout.Columns.Add(
            //    CustomColumn.NewCustomColumn(
            //    ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty).FieldPlaceholder,
            //    2));

            //var brandField = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty);
            //layout.Columns.Add(CustomColumn.NewCustomColumn(brandField.FieldPlaceholder, 3));

            //layout.GroupList.Add(CustomColumnGroup.NewCustomColumnGroup(brandField.FieldPlaceholder));
            //layout.IsGroupDetailHidden = true;
            //layout.IncludeAllPlanograms = true;

            ////load it into the vm
            //vm.LoadLayout(layout);

            ////String brand = vm.Rows[0][0].Value;

            ////change cell data
            //var cell = vm.Rows[0][0];

            //cell.PropertyChanged += base.TestModel_PropertyChanged;
            //base.PropertyChangedNotifications.Clear();

            //cell.Source.ElementAt(0).Product.Brand = "TEST";

            //Assert.Contains("Value", base.PropertyChangedNotifications);
            //cell.PropertyChanged -= base.TestModel_PropertyChanged;
            //Assert.AreEqual("TEST", cell.Value);


            //Test changing data on the second plan.
            Assert.Ignore();
        }


        [Test]
        public void Rows_UpdatedOnKeyPlanDataChange()
        {
            //Checks that changing a key column value will
            // refresh rows.
            Assert.Ignore();
        }

        [Test]
        public void Rows_UpdatedOnIncludedPlanClosed()
        {
            //checks that when an included plan is closed, 
            // its rows are removed.
            Assert.Ignore();
        }

        [Test]
        public void Rows_UpdatedOnPlanOpenedIfIncludeAll()
        {
            //checks that when a new plan is opened, 
            // if the layout includes all plans then rows are added 
            // for the new plan.
            Assert.Ignore();
        }

        #endregion

        #region Columns

        [Test]
        public void Columns_UpdatedOnIncludedPlanClosed()
        {
            //Checks that when an included plan is closed,
            // any plan columns are removed.

            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create a second plan, same positions as the first.
            var newPlan2 = AddNewCreweSparklingWaterPlan();
            PlanogramView planView2 = newPlan2.SourcePlanogram;
            planView2.Name = "PLAN2";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan2.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty).FieldPlaceholder);
            layout.IncludeAllPlanograms = true;
            vm.LoadLayout(layout);

            Int32 colCount = vm.CreateColumnSet().Count;
            Assert.IsTrue(vm.GetOrderedColumnDetails().Any(c => c.Plan.Controller == newPlan));

            //close the plan
            App.MainPageViewModel.ClosePlanController(newPlan);

            Assert.Less(vm.CreateColumnSet().Count, colCount);
            Assert.IsFalse(vm.GetOrderedColumnDetails().Any(c => c.Plan.Controller == newPlan));
        }

        [Test]
        public void Columns_UpdatedOnPlanOpenedIfAllIncluded()
        {
            //checks that when a new plan is opened, 
            // if the layout includes all plans then columns are added 
            // for the new plan.

            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create a second plan, same positions as the first.
            var newPlan2 = AddNewCreweSparklingWaterPlan();
            PlanogramView planView2 = newPlan2.SourcePlanogram;
            planView2.Name = "PLAN2";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty).FieldPlaceholder);
            layout.IncludeAllPlanograms = true;
            vm.LoadLayout(layout);

            Int32 colCount = vm.CreateColumnSet().Count;


            //open another plan
            var newPlan3 = AddNewCreweSparklingWaterPlan();
            PlanogramView planView3 = newPlan3.SourcePlanogram;
            planView3.Name = "PLAN3";


            //check more columns were added
            Assert.Greater(vm.CreateColumnSet().Count, colCount);
        }

        [Test]
        public void Columns_IsReadOnlyFlag()
        {
            //checks that the isreadonly flag is set correctly.
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty).FieldPlaceholder);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.MetaIsCoreRuleBrokenProperty).FieldPlaceholder);
            layout.Columns.Add(
                "{" +ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.FingerSpaceToTheSideProperty).FieldPlaceholder
                + " * " + ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.DisplayHeightProperty).FieldPlaceholder + "}");
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty).FieldPlaceholder + " Extra Text");
            vm.LoadLayout(layout);


            Assert.AreEqual(4, vm.CreateColumnSet().Count);

            //Normal non-readonly.
            Assert.IsFalse(vm.CreateColumnSet().ElementAt(0).IsReadOnly);

            //Readonly
            Assert.IsTrue(vm.CreateColumnSet().ElementAt(1).IsReadOnly);

            //Calculated readonly.
            Assert.IsTrue(vm.CreateColumnSet().ElementAt(2).IsReadOnly);

            //Normal with extra - readonly.
            Assert.IsTrue(vm.CreateColumnSet().ElementAt(3).IsReadOnly);
        }

        #endregion

        #endregion


        /* Planogram Selection:
         * 
         * 
         * Multisited products viewed at position level..
         * 
         * Aggregation by multiple group descriptions.
*/

        #region Product Level Data

        [Test]
        public void ProductLevelData_SetValue()
        {
            //checks the ability to set a cell value.
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty).FieldPlaceholder);
            vm.LoadLayout(layout);

            var cell = vm.Rows[0][1];
            String newValue = "TEST";

            //set the brand
            cell.Value = newValue;
            Assert.AreEqual(newValue, (String)cell.Value);
            Assert.AreEqual(newValue, cell.Source[0].Product.Brand);
        }

        [Test]
        public void ProductLevelData_SinglePlan()
        {
            //Test to load position level data for a single plan.
            var newPlan = AddNewCreweSparklingWaterPlan();

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Gtin]");
            layout.Columns.Add("[PlanogramProduct.Name]");


            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(newPlan.SourcePlanogram.Products.Count(), vm.Rows.Count, "Should have a row per product");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();

            Assert.AreEqual(2, columnSet.Count);

            //check values
            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Product;
                Assert.AreEqual(source.Gtin, row[0].Value);
                Assert.AreEqual(source.Name, row[1].Value);
            }
        }

        [Test]
        public void ProductLevelData_DataItemsChanged()
        {
            //Test to load position level data for a single plan.
            var newPlan = AddNewCreweSparklingWaterPlan();

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Gtin]");

            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(newPlan.SourcePlanogram.Products.Count(), vm.Rows.Count, "Should have a row per product");

            //add another product to the plan
            newPlan.AddNewProduct();
            Assert.AreEqual(newPlan.SourcePlanogram.Model.Products.Count, vm.Rows.Count, "Should have a row per product");
        }
    
        #endregion

        #region Position Level Data

        [Test]
        public void PositionLevelData_SinglePlan()
        {
            //Test to load position level data for a single plan.
            var newPlan = AddNewCreweSparklingWaterPlan();
            
            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder);
            layout.Columns.Add(ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty).FieldPlaceholder);


            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(newPlan.SourcePlanogram.EnumerateAllPositions().Count(), vm.Rows.Count, "Should have a row per position");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();

            Assert.AreEqual(2, columnSet.Count);

            //check values
            var p1 = newPlan.SourcePlanogram.EnumerateAllPositions().First();
            var row1 = vm.Rows[0];
            Assert.AreEqual(p1.Product.Gtin, row1[0].Value);
            Assert.AreEqual(p1.MetaWorldX, row1[1].Value);
            //Assert.AreEqual(p1.Product.Gtin, ResolveDynamicPath(row1, ExtendedDataGrid.GetBindingProperty(columnSet[0])));
            //Assert.AreEqual(p1.MetaWorldX, ResolveDynamicPath(row1, ExtendedDataGrid.GetBindingProperty(columnSet[1])));
                
        }

        [Test]
        public void PositionLevelData_AllPlans_ByPlan()
        {
            //Test to load position level data for all plans ordered by plan
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create a second plan, same positions as the first.
            var newPlan2 = AddNewCreweSparklingWaterPlan();
            PlanogramView planView2 = newPlan2.SourcePlanogram;
            planView2.Name = "PLAN2";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create and load the layout
            ObjectFieldInfo col1Field = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty);
            ObjectFieldInfo col2Field =ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty);
            ObjectFieldInfo col3Field =ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramComponent), PlanogramComponent.FriendlyName, PlanogramComponent.NameProperty);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(CustomColumn.NewCustomColumn(col1Field.FieldPlaceholder, 1));
            layout.Columns.Add(CustomColumn.NewCustomColumn(col2Field.FieldPlaceholder, 2));
            layout.Columns.Add(CustomColumn.NewCustomColumn(col3Field.FieldPlaceholder,2));
            layout.IncludeAllPlanograms = true;
            layout.DataOrderType = DataSheetDataOrderType.ByPlanogram;
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(planView.EnumerateAllPositions().Count(), vm.Rows.Count, "Should have a row per position - positions match up in the plans");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();
            Assert.AreEqual(5, columnSet.Count, "Should have a column per attribute per plan, but the key should not be duplicated");

            //check the columns are as expected
            //Assert.AreEqual(planView.Name, ((IGalleriaDataGridColumn)columnSet[0]).HeaderGroupNames[0].HeaderName);
            Assert.IsTrue(columnSet[0].Header.ToString().Contains(col1Field.PropertyFriendlyName));

            Assert.AreEqual(planView.Name, ((IGalleriaDataGridColumn)columnSet[1]).HeaderGroupNames[0].HeaderName);
            Assert.IsTrue(columnSet[1].Header.ToString().Contains(col2Field.PropertyFriendlyName));

            Assert.AreEqual(planView.Name, ((IGalleriaDataGridColumn)columnSet[2]).HeaderGroupNames[0].HeaderName);
            Assert.IsTrue(columnSet[2].Header.ToString().Contains(col3Field.PropertyFriendlyName));


            Assert.AreEqual(planView2.Name, ((IGalleriaDataGridColumn)columnSet[3]).HeaderGroupNames[0].HeaderName);
            Assert.IsTrue(columnSet[3].Header.ToString().Contains(col2Field.PropertyFriendlyName));

            Assert.AreEqual(planView2.Name, ((IGalleriaDataGridColumn)columnSet[4]).HeaderGroupNames[0].HeaderName);
            Assert.IsTrue(columnSet[4].Header.ToString().Contains(col3Field.PropertyFriendlyName));

        }

        [Test]
        public void PositionLevelData_AllPlans_ByAttribute()
        {
            //Test to load position level data for all plans ordered by attribute
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create a second plan, same positions as the first.
            var newPlan2 = AddNewCreweSparklingWaterPlan();
            PlanogramView planView2 = newPlan2.SourcePlanogram;
            planView2.Name = "PLAN2";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create and load the layout
            ObjectFieldInfo col1Field = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty);
            ObjectFieldInfo col2Field = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty);
            ObjectFieldInfo col3Field = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramComponent), PlanogramComponent.FriendlyName, PlanogramComponent.NameProperty);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(CustomColumn.NewCustomColumn(col1Field.FieldPlaceholder, 1));
            layout.Columns.Add(CustomColumn.NewCustomColumn(col2Field.FieldPlaceholder, 2));
            layout.Columns.Add(CustomColumn.NewCustomColumn(col3Field.FieldPlaceholder, 2));
            layout.IncludeAllPlanograms = true;
            layout.DataOrderType = DataSheetDataOrderType.ByAttribute;
            vm.LoadLayout(layout);

            //check we have the correct number of rows.
            Assert.AreEqual(planView.EnumerateAllPositions().Count(), vm.Rows.Count, "Should have a row per position - positions match up in the plans");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();
            Assert.AreEqual(5, columnSet.Count, "Should have a column per attribute per plan, but the key column should not be duplicated");

            //check the columns are as expected

            //Key:
            Assert.IsFalse(((IGalleriaDataGridColumn)columnSet[0]).HeaderGroupNames.Any());
            Assert.IsTrue(columnSet[0].Header.ToString().Contains(col1Field.PropertyFriendlyName));

            //Normal:
            Assert.IsTrue(((IGalleriaDataGridColumn)columnSet[1]).HeaderGroupNames[0].HeaderName.Contains(col2Field.FieldFriendlyName));
            Assert.AreEqual(planView.Name, columnSet[1].Header.ToString());

            Assert.IsTrue(((IGalleriaDataGridColumn)columnSet[2]).HeaderGroupNames[0].HeaderName.Contains(col2Field.FieldFriendlyName));
            Assert.AreEqual(planView2.Name, columnSet[2].Header.ToString());

            Assert.IsTrue(((IGalleriaDataGridColumn)columnSet[3]).HeaderGroupNames[0].HeaderName.Contains(col3Field.FieldFriendlyName));
            Assert.AreEqual(planView.Name, columnSet[3].Header.ToString());

            Assert.IsTrue(((IGalleriaDataGridColumn)columnSet[4]).HeaderGroupNames[0].HeaderName.Contains(col3Field.FieldFriendlyName));
            Assert.AreEqual(planView2.Name, columnSet[4].Header.ToString());
        }

        [Test]
        public void PositionLevelData_AllPlans_WithDifferentGtins()
        {
            //Test to load position level data where some gtins match but others dont.
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create a second plan, same positions as the first.
            var newPlan2 = AddNewCreweSparklingWaterPlan();
            PlanogramView planView2 = newPlan2.SourcePlanogram;
            planView2.Name = "PLAN2";

            //Change one of the gtins on the second plan.
            planView2.EnumerateAllPositions().First().Product.Gtin = "OTHER";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create and load the layout
            ObjectFieldInfo col1Field = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty);
            ObjectFieldInfo col2Field = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty);
            ObjectFieldInfo col3Field = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramComponent), PlanogramComponent.FriendlyName, PlanogramComponent.NameProperty);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(CustomColumn.NewCustomColumn(col1Field.FieldPlaceholder, 1));
            layout.Columns.Add(CustomColumn.NewCustomColumn(col2Field.FieldPlaceholder, 2));
            layout.Columns.Add(CustomColumn.NewCustomColumn(col3Field.FieldPlaceholder, 3));
            layout.IncludeAllPlanograms = true;
            layout.DataOrderType = DataSheetDataOrderType.ByAttribute;
            vm.LoadLayout(layout);

            //check we have the correct number of rows.
            Assert.AreEqual(planView.EnumerateAllPositions().Count() +1, vm.Rows.Count);

        }

        [Test]
        public void PositionLevelData_CalculatedColumn()
        {
            //Test the load of a calculated column against position level data.
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;


            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder,
                1));
            
            //add the calculated column
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                "{" +String.Format("{0} * {1} * {2}", 
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsWideProperty).FieldPlaceholder,
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsHighProperty).FieldPlaceholder,
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.FacingsDeepProperty).FieldPlaceholder) + "}",
                2));


            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(planView.EnumerateAllPositions().Count(), vm.Rows.Count, "Should have a row per position");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();
            Assert.AreEqual(2, columnSet.Count);

            //check the value of the calculated column.
            var p1 = planView.EnumerateAllPositions().First();
            var row1 = vm.Rows[0];
            Assert.AreEqual(p1.FacingsHigh * p1.FacingsWide * p1.FacingsDeep, row1[1].Value);

        }

        [Test]
        public void PositionLevelData_SinglePlan_AggregatedByBrand()
        {
            //Test the load of product data aggregated to brand level.
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder,
                1));
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty).FieldPlaceholder,
                2));

            var brandField = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty);
            layout.Columns.Add(CustomColumn.NewCustomColumn(brandField.FieldPlaceholder, 1));

            layout.GroupList.Add(CustomColumnGroup.NewCustomColumnGroup(brandField.FieldPlaceholder));
            layout.IsGroupDetailHidden = true;

            //load it into the vm
            vm.LoadLayout(layout);

            //check we have the correct number of rows.
            Assert.AreEqual(planView.EnumerateAllPositions().Select(p=> p.Product.Brand).Distinct().Count(), vm.Rows.Count, "Should have a row per brand");

            //make sure that group descriptions are not created.
            Assert.AreEqual(0, vm.CreateGroupDescriptions().Count);

            //check column count
            Assert.AreEqual(3, vm.CreateColumnSet().Count);
        }

        [Test]
        public void PositionLevelData_AllPlans_AggregatedByBrand()
        {
            //Test the load of product data aggregated to brand level.
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;
            planView.Name = "PLAN1";

            //create a second plan, same positions as the first.
            var newPlan2 = AddNewCreweSparklingWaterPlan();
            PlanogramView planView2 = newPlan2.SourcePlanogram;
            planView2.Name = "PLAN2";

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder,
                1));
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty).FieldPlaceholder,
                2));

            var brandField = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty);
            layout.Columns.Add(CustomColumn.NewCustomColumn(brandField.FieldPlaceholder, 1));

            layout.GroupList.Add(CustomColumnGroup.NewCustomColumnGroup(brandField.FieldPlaceholder));
            layout.IsGroupDetailHidden = true;
            layout.IncludeAllPlanograms = true;

            //load it into the vm
            vm.LoadLayout(layout);

            //check we have the correct number of rows.
            Assert.AreEqual(planView.EnumerateAllPositions().Select(p => p.Product.Brand).Distinct().Count(), vm.Rows.Count, "Should have a row per brand");

            //make sure that group descriptions are not created.
            Assert.AreEqual(0, vm.CreateGroupDescriptions().Count);

            //check that the key column has not been duplicated
            Assert.AreEqual(5, vm.CreateColumnSet().Count);
        }

        [Test]
        public void PositionLevelData_GroupedByBrand()
        {
            //Test the load of product data grouped to brand level but with no aggreggation
            var newPlan = AddNewCreweSparklingWaterPlan();
            PlanogramView planView = newPlan.SourcePlanogram;

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.GtinProperty).FieldPlaceholder,
                1));
            layout.Columns.Add(
                CustomColumn.NewCustomColumn(
                ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramPosition), PlanogramPosition.FriendlyName, PlanogramPosition.MetaWorldXProperty).FieldPlaceholder,
                2));

            var brandField = ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramProduct), PlanogramProduct.FriendlyName, PlanogramProduct.BrandProperty);
            layout.Columns.Add(CustomColumn.NewCustomColumn(brandField.FieldPlaceholder, 1));

            layout.GroupList.Add(CustomColumnGroup.NewCustomColumnGroup(brandField.FieldPlaceholder));


            //load it into the vm
            vm.LoadLayout(layout);

            //check we have the correct number of rows.
            Assert.AreEqual(planView.EnumerateAllPositions().Count(), vm.Rows.Count, "Should have a row per position");

            //make sure that group descriptions are  created.
            Assert.AreEqual(layout.GroupList.Count, vm.CreateGroupDescriptions().Count);
        }

        #endregion

        #region SubComponent level Data

        [Test]
        public void SubComponentLevelData_SinglePlan()
        {
            //Test to load position level data for a single plan.
            var newPlan = AddNewCreweSparklingWaterPlan();

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramSubComponent.X]");
            layout.Columns.Add("[PlanogramSubComponent.Name]");


            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(newPlan.SourcePlanogram.EnumerateAllComponents().Count(), vm.Rows.Count, "Should have a row per component");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();

            Assert.AreEqual(2, columnSet.Count);

            //check values
            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].SubComponent;
                Assert.AreEqual(source.X, row[0].Value);
                Assert.AreEqual(source.Name, row[1].Value);
            }
        }

        #endregion

        #region Component level data

        [Test]
        public void ComponentLevelData_SinglePlan()
        {
            //Test to load position level data for a single plan.
            var newPlan = AddNewCreweSparklingWaterPlan();

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramComponent.Name]");
            layout.Columns.Add("[PlanogramComponent.MetaWorldX]");


            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(newPlan.SourcePlanogram.EnumerateAllComponents().Count(), vm.Rows.Count, "Should have a row per component");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();

            Assert.AreEqual(2, columnSet.Count);

            //check values
            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Component;
                Assert.AreEqual(source.Name, row[0].Value);
                Assert.AreEqual(source.MetaWorldX, row[1].Value);
            }
        }

        #endregion

        #region Assembly level data

        #endregion

        #region Fixture level data

        [Test]
        public void FixtureLevelData_SinglePlan()
        {
            //Test to load position level data for a single plan.
            var newPlan = AddNewCreweSparklingWaterPlan();

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramFixture.Name]");
            layout.Columns.Add("[PlanogramFixture.BaySequenceNumber]");


            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(newPlan.SourcePlanogram.Fixtures.Count(), vm.Rows.Count, "Should have a row per fixture");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();

            Assert.AreEqual(2, columnSet.Count);

            //check values
            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Fixture;
                Assert.AreEqual(source.Name, row[0].Value);
                Assert.AreEqual(source.BaySequenceNumber, row[1].Value);
            }
        }

        #endregion

        #region Planogram level data

        [Test]
        public void PlanogramLevelData_SinglePlan()
        {
            //Test to load position level data for a single plan.
            var newPlan = AddNewCreweSparklingWaterPlan();

            //create the vm
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            //create a layout at position level 
            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[Planogram.Name]");


            //load it into the vm
            vm.LoadLayout(layout);


            //check we have the correct number of rows.
            Assert.AreEqual(1, vm.Rows.Count, "Should have a row per plan");

            //check we have the correct number of columns
            DataGridColumnCollection columnSet = vm.CreateColumnSet();

            Assert.AreEqual(1, columnSet.Count);

            //check values
            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Planogram;
                Assert.AreEqual(source.Name, row[0].Value);
            }
        }

        #endregion

        #region ShowDataSheetEditorWindow

        [Test]
        public void ShowDataSheetEditorWindow_DoesNotLosePlanSelection()
        {
            //Checks that plan selection is passed through to the data sheet editor window.

            var newPlan1 = OpenSamplePlan(SamplePlans.Cereals3Bay);
            var newPlan2 = OpenSamplePlan(SamplePlans.Cereals2Bay);
            var newPlan3 = OpenSamplePlan(SamplePlans.Cereals1Bay);
            ReportDocument vm = (ReportDocument)newPlan1.AddNewDocument(DocumentType.DataSheet, false);

            //show the editor window and include all plans
            base.WindowService.AddResponse((e) =>
                {
                    ReportColumnLayoutEditorViewModel editorVm = (ReportColumnLayoutEditorViewModel)e.WindowParameters[0];
                    editorVm.CurrentLayout.IncludeAllPlanograms = true;
                    editorVm.SelectedFields.Add(editorVm.SelectedGroupFields.Last());
                    editorVm.AddSelectedColumnsCommand.Execute();
                    editorVm.ApplyCommand.Execute();
                });
            vm.ShowDataSheetEditorWindow();

            //check that all plans are selected
            Assert.AreEqual(3, vm.IncludedPlans.Count());

            //reopen the editor and check
            base.WindowService.AddResponse((e) =>
            {
                ReportColumnLayoutEditorViewModel editorVm = (ReportColumnLayoutEditorViewModel)e.WindowParameters[0];
                Assert.IsTrue(editorVm.CurrentLayout.IncludeAllPlanograms);
                Assert.IsTrue(editorVm.AvailablePlanograms.All(p => p.IsSelected));

                //unselect 1
                editorVm.AvailablePlanograms.First(p => !p.IsCurrentPlanogram).IsSelected = false;

                editorVm.ApplyCommand.Execute();
            });
            vm.ShowDataSheetEditorWindow();

            //check that only 2 are selected
            Assert.AreEqual(2, vm.IncludedPlans.Count());

            //reopen and check again
            base.WindowService.AddResponse((e) =>
            {
                ReportColumnLayoutEditorViewModel editorVm = (ReportColumnLayoutEditorViewModel)e.WindowParameters[0];
                Assert.IsFalse(editorVm.CurrentLayout.IncludeAllPlanograms);
                Assert.AreEqual(2, editorVm.AvailablePlanograms.Count(p => p.IsSelected));

                 editorVm.CloseCommand.Execute();
            });
            vm.ShowDataSheetEditorWindow();
        }

        [Test]
        public void ShowDataSheetEditorWindow_PlanDisplayNames()
        {
            //Checks that plan display names are passed through

            var newPlan1 = OpenSamplePlan(SamplePlans.Cereals3Bay);
            var newPlan2 = OpenSamplePlan(SamplePlans.Cereals2Bay);
            var newPlan3 = OpenSamplePlan(SamplePlans.Cereals1Bay);
            ReportDocument vm = (ReportDocument)newPlan1.AddNewDocument(DocumentType.DataSheet, false);

            //show the editor window and include all plans
            //set display names
            base.WindowService.AddResponse((e) =>
            {
                ReportColumnLayoutEditorViewModel editorVm = (ReportColumnLayoutEditorViewModel)e.WindowParameters[0];
                editorVm.CurrentLayout.IncludeAllPlanograms = true;
                editorVm.CurrentLayout.DataOrderType = DataSheetDataOrderType.ByPlanogram;

                editorVm.SelectedFields.Add(editorVm.SelectedGroupFields.Last());
                editorVm.AddSelectedColumnsCommand.Execute();

                editorVm.AvailablePlanograms[0].DisplayName ="TEST2";
                editorVm.ApplyCommand.Execute();
            });
            vm.ShowDataSheetEditorWindow();

            //check that the display name came through
            Assert.IsTrue(
                vm.CreateColumnSet().Any(c => ((IGalleriaDataGridColumn)c).HeaderGroupNames[0].HeaderName == "TEST2"));

        }

        [Test]
        public void ShowDataSheetEditorWindow_CreateNewReportDocument()
        {
            //Checks that a new report doc gets created correctly when
            // showing the report editor.

            var newPlan1 = OpenSamplePlan(SamplePlans.Cereals3Bay);

            //cancel the window to check no doc gets left behind.
            base.WindowService.AddResponse((e) =>
            {
                ReportColumnLayoutEditorViewModel editorVm = (ReportColumnLayoutEditorViewModel)e.WindowParameters[0];
                editorVm.CloseCommand.Execute();
            });
            MainPageCommands.ShowReportEditorAsNew.Execute();

            Assert.IsFalse(newPlan1.PlanDocuments.Any(d => d.DocumentType == DocumentType.DataSheet));

            //now create the layout
            base.WindowService.AddResponse((e) =>
            {
                ReportColumnLayoutEditorViewModel editorVm = (ReportColumnLayoutEditorViewModel)e.WindowParameters[0];
                editorVm.SelectedFields.Add(editorVm.SelectedGroupFields.Last());
                editorVm.AddSelectedColumnsCommand.Execute();
                editorVm.ApplyCommand.Execute();
            });
            MainPageCommands.ShowReportEditorAsNew.Execute();

            Assert.IsTrue(newPlan1.PlanDocuments.Any(d => d.DocumentType == DocumentType.DataSheet));
        }

        #endregion

        #region CalculatedColumns

        [Test]
        public void CalculatedColumn_Sum()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[Planogram.Name]");
            layout.Columns.Add("SUM([PlanogramPosition.TotalUnits])");

            vm.LoadLayout(layout);

            Assert.AreEqual(newPlan.SourcePlanogram.EnumerateAllPositions().Sum(p => p.TotalUnits), vm.Rows[0][1].Value);

        }

        [Test]
        public void CalculatedColumn_Count()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Brand]");
            layout.Columns.Add("COUNT()");
            layout.IsGroupDetailHidden = true;
            layout.GroupList.Add("[PlanogramProduct.Brand]");


            vm.LoadLayout(layout);

            //check
            foreach (var row in vm.Rows)
            {
                Assert.AreEqual(row[1].Source.Count, row[1].Value);
            }
           
        }

        [Test]
        public void CalculatedColumn_Average()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[Planogram.Name]");
            layout.Columns.Add("AVG([PlanogramPosition.TotalUnits])");

            vm.LoadLayout(layout);

            Assert.AreEqual(Math.Round(newPlan.SourcePlanogram.EnumerateAllPositions().Average(p => p.TotalUnits),2), 
                Math.Round((Double)vm.Rows[0][1].Value,2));
        }

        [Test]
        public void CalculatedColumn_Min()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[Planogram.Name]");
            layout.Columns.Add("MIN([PlanogramPosition.TotalUnits])");

            vm.LoadLayout(layout);

            Assert.AreEqual(newPlan.SourcePlanogram.EnumerateAllPositions().Min(p => p.TotalUnits), vm.Rows[0][1].Value);
        }

        [Test]
        public void CalculatedColumn_Max()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[Planogram.Name]");
            layout.Columns.Add("MAX([PlanogramPosition.TotalUnits])");

            vm.LoadLayout(layout);

            Assert.AreEqual(newPlan.SourcePlanogram.EnumerateAllPositions().Max(p => p.TotalUnits), vm.Rows[0][1].Value);
        }

        [Test]
        public void CalculatedColumn_RoundUp()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Gtin]");
            layout.Columns.Add("ROUNDUP(7.4, 0)");
            vm.LoadLayout(layout);

            Assert.AreEqual(8, vm.Rows[0][1].Value);
        }

        [Test]
        public void CalculatedColumn_RoundDown()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Gtin]");
            layout.Columns.Add("ROUNDDOWN(7.8, 0)");
            vm.LoadLayout(layout);

            Assert.AreEqual(7, vm.Rows[0][1].Value);
        }

        [Test]
        public void CalculatedColumn_And()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramPosition.Gtin]");
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] > 2 AND [PlanogramPosition.FacingsWide] > 2}");
            vm.LoadLayout(layout);


            foreach (var row in vm.Rows)
            {
                PlanogramPosition source = row[1].Source[0].Position.Model;
                Assert.AreEqual(source.FacingsHigh > 2 && source.FacingsWide > 2, row[1].Value);
            }
        }

        [Test]
        public void CalculatedColumn_Or()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramPosition.Gtin]");
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] > 2 OR [PlanogramPosition.FacingsWide] > 2}");
            vm.LoadLayout(layout);


            foreach (var row in vm.Rows)
            {
                PlanogramPosition source = row[1].Source[0].Position.Model;
                Assert.AreEqual(source.FacingsHigh > 2 || source.FacingsWide > 2, row[1].Value);
            }
        }

        [Test]
        public void CalculatedColumn_UpperCase()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Brand]");
            vm.LoadLayout(layout);

            String value = (String)vm.Rows[0][0].Value;

            CustomColumnLayout layout2 = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout2.Columns.Add("UPPERCASE([PlanogramProduct.Brand])");
            vm.LoadLayout(layout2);

            Assert.AreEqual(value.ToUpperInvariant(), (String)vm.Rows[0][0].Value);
        }

        [Test]
        public void CalculatedColumn_LowerCase()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Brand]");
            vm.LoadLayout(layout);

            String value = (String)vm.Rows[0][0].Value;

            CustomColumnLayout layout2 = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout2.Columns.Add("LOWERCASE([PlanogramProduct.Brand])");
            vm.LoadLayout(layout2);

            Assert.AreEqual(value.ToLowerInvariant(), (String)vm.Rows[0][0].Value);
        }

        [Test]
        public void CalculatedColumn_Contains()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Name]");
            layout.Columns.Add("CONTAINS([PlanogramProduct.Name], \"Crunchy\")");
            layout.SortList.Add("[PlanogramProduct.Name]", System.ComponentModel.ListSortDirection.Ascending);
            vm.LoadLayout(layout);

            //check
            foreach (var row in vm.Rows)
            {
                Boolean contains = row[1].Source[0].Product.Name.Contains("Crunchy");
                Assert.AreEqual(contains, row[1].Value);
            }

            //now make it more complicated by forcing all case to be the same before checking:
            layout.Columns[1].Path = "CONTAINS(UPPERCASE([PlanogramProduct.Name]), \"CRUNCHY\")";
            vm.LoadLayout(layout);

            //check
            foreach (var row in vm.Rows)
            {
                Boolean contains = row[1].Source[0].Product.Name.ToUpperInvariant().Contains("CRUNCHY");
                Assert.AreEqual(contains, row[1].Value);
            }
        }

        [Test]
        public void CalculatedColumn_NotContains()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Name]");
            layout.Columns.Add("NOTCONTAINS([PlanogramProduct.Name], \"Crunchy\")");
            layout.SortList.Add("[PlanogramProduct.Name]", System.ComponentModel.ListSortDirection.Ascending);
            vm.LoadLayout(layout);

            //check
            foreach (var row in vm.Rows)
            {
                Boolean contains = row[1].Source[0].Product.Name.Contains("Crunchy");
                Assert.AreEqual(!contains, row[1].Value);
            }

            //now make it more complicated by forcing all case to be the same before checking:
            layout.Columns[1].Path = "NOTCONTAINS(UPPERCASE([PlanogramProduct.Name]), \"CRUNCHY\")";
            vm.LoadLayout(layout);

            //check
            foreach (var row in vm.Rows)
            {
                Boolean contains = row[1].Source[0].Product.Name.ToUpperInvariant().Contains("CRUNCHY");
                Assert.AreEqual(!contains, row[1].Value);
            }
        }

        [Test]
        public void CalculatedColumn_Add()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] + [PlanogramPosition.FacingsYHigh]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                Int32 expected = row[0].Source[0].Position.FacingsHigh + row[0].Source[0].Position.FacingsYHigh;
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_Subtract()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] - [PlanogramPosition.FacingsWide]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Int32 expected = source.FacingsHigh - source.FacingsWide;
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_Multiply()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] * [PlanogramPosition.FacingsWide] * [PlanogramPosition.FacingsDeep]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Int32 expected = source.FacingsHigh * source.FacingsWide * source.FacingsDeep;
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_Divide()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.TotalUnits] / [PlanogramPosition.FacingsHigh]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Double expected = source.TotalUnits / (Double)source.FacingsHigh;
                Assert.AreEqual(Math.Round(expected, 2), Math.Round(Convert.ToDouble(row[0].Value), 2));
            }
        }

        [Test]
        public void CalculatedColumn_GreaterThan()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] > [PlanogramPosition.FacingsWide]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Boolean expected = (source.FacingsHigh > source.FacingsWide);
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_GreaterThanOrEqualTo()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] >= [PlanogramPosition.FacingsWide]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Boolean expected = (source.FacingsHigh >= source.FacingsWide);
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_LessThan()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] < [PlanogramPosition.FacingsWide]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Boolean expected = (source.FacingsHigh < source.FacingsWide);
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_If()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("IF({[PlanogramPosition.FacingsHigh] < [PlanogramPosition.FacingsWide]}, 0, 1)");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Int32 expected = (source.FacingsHigh < source.FacingsWide) ? 0 : 1;
                Assert.AreEqual(expected, row[0].Value);
            }

            layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("IF({[PlanogramPosition.FacingsHigh] < [PlanogramPosition.FacingsWide]}, \"Test\", \"Test2\")");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                String expected = (source.FacingsHigh < source.FacingsWide) ? "Test" : "Test2";
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_LessOrEqualTo()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] <= [PlanogramPosition.FacingsWide]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Boolean expected = (source.FacingsHigh <= source.FacingsWide);
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_Equals()
        {
            //Numeric:
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] = [PlanogramPosition.FacingsWide]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Boolean expected = (source.FacingsHigh == source.FacingsWide);
                Assert.AreEqual(expected, row[0].Value);
            }

            //String:
            layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramProduct.Brand] = Galleria}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Product;

                Boolean expected = (source.Brand == "Galleria");
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_NotEquals()
        {
            //Numeric:
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramPosition.FacingsHigh] <> [PlanogramPosition.FacingsWide]}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Position;

                Boolean expected = !(source.FacingsHigh == source.FacingsWide);
                Assert.AreEqual(expected, row[0].Value);
            }

            //String:
            layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("{[PlanogramProduct.Brand] <> Galleria}");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Product;

                Boolean expected = !(source.Brand == "Galleria");
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        [Test]
        public void CalculatedColumn_NestedFunctions()
        {
            //Checks that functions still work even when nested.

            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Brand]");
            vm.LoadLayout(layout);

            String value = (String)vm.Rows[0][0].Value;

            CustomColumnLayout layout2 = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout2.Columns.Add("UPPERCASE(LOWERCASE([PlanogramProduct.Brand]))");
            vm.LoadLayout(layout2);

            Assert.AreEqual(value.ToUpperInvariant(), (String)vm.Rows[0][0].Value);
        }

        [Test]
        public void CalculatedColumn_StringConcatonation()
        {
            var newPlan = OpenSamplePlan(SamplePlans.Cereals3Bay);
            ReportDocument vm = (ReportDocument)newPlan.AddNewDocument(DocumentType.DataSheet, false);

            CustomColumnLayout layout = CustomColumnLayout.NewCustomColumnLayout(CustomColumnLayoutType.DataSheet);
            layout.Columns.Add("[PlanogramProduct.Gtin] [PlanogramProduct.Brand]");
            vm.LoadLayout(layout);

            foreach (var row in vm.Rows)
            {
                var source = row[0].Source[0].Product;

                String expected = String.Format("{0} {1}", source.Gtin, source.Brand);
                Assert.AreEqual(expected, row[0].Value);
            }
        }

        #endregion
    }
}
