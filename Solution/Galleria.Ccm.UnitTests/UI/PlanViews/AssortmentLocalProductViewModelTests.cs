﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26491 : A.Kuszyk
//  Created.
// V8-28444 : Fix failed unit tests

#endregion

#region Version History : CCM830

// V8-32560 : A.Silva
//  Removed old AddAll, RemoveAll tests and amended others related.

#endregion

#endregion

using System;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentLocalProductViewModelTests : TestBase
    {
        #region Fields

        private AssortmentLocalProductViewModel _viewModel;
        private String _productName = "Hello World";

        #endregion

        #region Test Helper Methods

        private void AddAllAvailableLocations()
        {
            foreach (Location info in _viewModel.AvailableLocations)
            {
                _viewModel.SelectedAvailableLocations.Add(info);
            }
            _viewModel.AddSelectedLocationsCommand.Execute();
        }

        #endregion

        [SetUp]
        public void SetUp()
        {
            var entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;
            TestDataHelper.InsertLocationDtos(base.DalFactory, 10, entityId);
            Galleria.Ccm.Editor.Client.Wpf.App.ViewState.EntityId = entityId;

            var assortment = PlanogramAssortment.NewPlanogramAssortment();
            assortment.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct());
            var product = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            product.Name = _productName;
            product.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.Local;
            assortment.Products.Add(product);
            _viewModel = new AssortmentLocalProductViewModel(assortment, new List<IPlanItem>());
        }

        #region Command Tests

        [Test]
        public void Command_Apply()
        {
            RelayCommand cmd = _viewModel.ApplyCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as local product rule not valid");

            _viewModel.SelectedLocalAssortmentProduct = _viewModel.SelectedAssortment.Products.First();
            AddAllAvailableLocations();

            cmd.Execute();
            Assert.IsTrue((_viewModel.CurrentAssortmentLocalProducts.Count == 1), "Assortment local product rule should be applied");
        }

        [Test]
        public void Command_ApplyAndClose()
        {
            RelayCommand cmd = _viewModel.ApplyAndCloseCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as no local product rule changes made");

            // Add local product rule
            _viewModel.SelectedLocalAssortmentProduct = _viewModel.SelectedAssortment.Products.First();
            AddAllAvailableLocations();

            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as local product rule applied");

            cmd.Execute();
            Assert.IsTrue((_viewModel.CurrentAssortmentLocalProducts.Count == 1), "Assortment local product rule should be applied");
        }

        [Test]
        public void Command_GetLocalProduct()
        {
            RelayCommand cmd = _viewModel.GetLocalProductCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can execute
            Assert.IsTrue(cmd.CanExecute(), "command should be enabled initially");
        }

        [Test]
        public void Command_AddSelectedLocations()
        {
            RelayCommand cmd = _viewModel.AddSelectedLocationsCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as local product rule not valid");

            _viewModel.SelectedLocalAssortmentProduct = _viewModel.SelectedAssortment.Products.First();
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as local product rule not valid");

            _viewModel.SelectedAvailableLocations.Add(_viewModel.AvailableLocations.First());
            Assert.IsTrue(cmd.CanExecute(), "Should be enabled local product rule valid and locations assigned");

            Assert.AreEqual(0, _viewModel.TakenLocations.Count, "There shouldnt be any locations initially assigned");

            cmd.Execute();

            Assert.AreEqual(1, _viewModel.TakenLocations.Count, "The first product should be assigned");
        }

        [Test]
        public void Command_RemoveSelectedLocations()
        {
            RelayCommand cmd = _viewModel.RemoveSelectedLocationsCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            //check can excute
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as local product rule not valid");

            _viewModel.SelectedLocalAssortmentProduct = _viewModel.SelectedAssortment.Products.First();
            Assert.IsFalse(cmd.CanExecute(), "Should be disabled as local product rule not valid");

            AddAllAvailableLocations();

            Location assortmentLocation = _viewModel.TakenLocations.First();
            _viewModel.SelectedTakenLocations.Add(assortmentLocation);

            Assert.IsTrue(cmd.CanExecute(), "Should be enabled as location selected for removal");

            Assert.AreEqual(_viewModel.SelectedTakenLocations.Count(p => p.Id == assortmentLocation.Id), 1, "Location should be assigned");

            cmd.Execute();

            Assert.AreEqual(_viewModel.SelectedTakenLocations.Count(p => p.Id == assortmentLocation.Id), 0, "Location should no longer be assigned");
        }

        [Test]
        public void Command_Cancel()
        {
            String propertyName = "CancelCommand";
            Assert.AreEqual(propertyName, AssortmentLocalProductViewModel.CancelCommandProperty.Path);

            RelayCommand cmd = _viewModel.CancelCommand;
            Assert.Contains(cmd, _viewModel.ViewModelCommands);

            Assert.IsNotNull(cmd.FriendlyName);

            Assert.IsTrue(cmd.CanExecute());
        }

        #endregion

        [Test]
        public void GetMatchingLocalProducts_ReturnsMatchingList()
        {
            var results = _viewModel.GetMatchingLocalProducts(_productName);

            Assert.That(results.Any(p => p.Name == _productName));
        }
    }
}