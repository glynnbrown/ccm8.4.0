﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25278 : L.Ineson
//  Created
#endregion
#region Version History: (CCM 8.2.0)
// V8-31193 : J.Pickup
//  Cntrl + a now selects based on item selection type.
#endregion
#region Version History : CCM830
// V8-32820 : A.Kuszyk
//  Added test for sequence template highlighting.
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using NUnit.Framework;
using System;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class PlanVisualDocumentTests : TestBase
    {
        #region Properties

        /// <summary>
        /// GEM:25278
        /// </summary>
        [Test]
        public void ProductHighlights_UpdatesOnHighlightChange()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(newPlan.SourcePlanogram.Model);
            newPlan.SourcePlanogram.EndUpdate();

            PlanogramView planView = newPlan.SourcePlanogram;

            //create the viewmodel
            PlanVisualDocument viewModel = (PlanVisualDocument)newPlan.AddNewDocument(DocumentType.Design, true);

            //add a highlight
            Highlight h1 = Highlight.NewHighlight();
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogamProduct.Brand]";
            viewModel.Highlight = new Editor.Client.Wpf.Common.HighlightItem(h1);

            Assert.IsNotNull(((IPlanRenderSettings)viewModel).PositionHighlights);

            //remove the highlight
            viewModel.Highlight = null;
            Assert.IsNull(((IPlanRenderSettings)viewModel).PositionHighlights);
        }

        /// <summary>
        /// GEM:25278
        /// </summary>
        [Test]
        public void ProductHighlights_UpdatesForNewlyAddedPosition()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(newPlan.SourcePlanogram.Model);
            newPlan.SourcePlanogram.EndUpdate();

            PlanogramView planView = newPlan.SourcePlanogram;

            //remove a position
            var pos = planView.EnumerateAllPositions().First();
            var prod1 = pos.Product;
            pos.SubComponent.RemovePosition(pos);
            pos = null;

            //create the viewmodel
            PlanVisualDocument viewModel = (PlanVisualDocument)newPlan.AddNewDocument(DocumentType.Design, true);

            //add a highlight
            Highlight h1 = Highlight.NewHighlight();
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogamProduct.Brand]";
            viewModel.Highlight = new Editor.Client.Wpf.Common.HighlightItem(h1);

            //add the product back
            pos = newPlan.SourcePlanogram.EnumerateMerchandisableSubComponents().First().AddPosition(prod1);

            //check there is a highlight entry
            Int32 col;
            Assert.IsTrue(((IPlanRenderSettings)viewModel).PositionHighlights.TryGetValue(pos.Model.Id, out col), "colour not found");
        }

        /// <summary>
        /// GEM:25278
        /// </summary>
        [Test]
        public void ProductHighlights_MovedPositionHighlightNotLost()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(newPlan.SourcePlanogram.Model);
            newPlan.SourcePlanogram.EndUpdate();

            PlanogramView planView = newPlan.SourcePlanogram;

            //get a position
            var pos = planView.EnumerateAllPositions().First();

            //create the viewmodel
            PlanVisualDocument viewModel = (PlanVisualDocument)newPlan.AddNewDocument(DocumentType.Design, true);

            //add a highlight
            Highlight h1 = Highlight.NewHighlight();
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogamProduct.Brand]";
            viewModel.Highlight = new Editor.Client.Wpf.Common.HighlightItem(h1);

            //move the position
            pos = planView.EnumerateMerchandisableSubComponents().FirstOrDefault(s => s != pos.SubComponent).MovePosition(pos);

            //check there is a highlight entry
            Int32 col;
            Assert.IsTrue(((IPlanRenderSettings)viewModel).PositionHighlights.TryGetValue(pos.Model.Id, out col), "colour not found");
        }

        [Test]
        public void ClearAndSelectAllPlanItemsBasedOnSelectionType_SelectsCorrectPlanItemTypes()
        {
            // Set up environment...
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var newPlan = mainPageView.CreateNewPlanogram();
            newPlan.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(newPlan.SourcePlanogram.Model);
            newPlan.SourcePlanogram.EndUpdate();
            PlanogramView planView = newPlan.SourcePlanogram;
            PlanVisualDocument viewModel = (PlanVisualDocument)newPlan.AddNewDocument(DocumentType.Design, true);

            // Test only selects components:
            App.ViewState.SelectionMode = PlanItemSelectionType.OnlyComponents;
            viewModel.ClearAndSelectAllPlanItemsBasedOnSelectionType();
            Assert.IsTrue(viewModel.SelectedPlanItems.Where(spi => spi.PlanItemType != PlanItemType.Component).Count() == 0);

            // Test only selects positions:
            App.ViewState.SelectionMode = PlanItemSelectionType.OnlyPositions;
            viewModel.ClearAndSelectAllPlanItemsBasedOnSelectionType();
            Assert.IsTrue(viewModel.SelectedPlanItems.Where(spi => spi.PlanItemType != PlanItemType.Position).Count() == 0);

            // Test only selects positions:
            App.ViewState.SelectionMode = PlanItemSelectionType.AllProductPositions;
            viewModel.ClearAndSelectAllPlanItemsBasedOnSelectionType();
            Assert.IsTrue(viewModel.SelectedPlanItems.Where(spi => spi.PlanItemType != PlanItemType.Position).Count() == 0);

            // Test only selects positions and components
            App.ViewState.SelectionMode = PlanItemSelectionType.PositionsAndComponents;
            viewModel.ClearAndSelectAllPlanItemsBasedOnSelectionType();
            Assert.IsTrue(viewModel.SelectedPlanItems.Where(spi => spi.PlanItemType != PlanItemType.Position && spi.PlanItemType != PlanItemType.Component).Count() == 0);

            // Test only selects positions and components
            App.ViewState.SelectionMode = PlanItemSelectionType.ProductsAndComponents;
            Assert.IsTrue(viewModel.SelectedPlanItems.Where(spi => spi.PlanItemType != PlanItemType.Position && spi.PlanItemType != PlanItemType.Component).Count() == 0);
        }

        #endregion

        #region Commands

        [Test]
        public void ShowSequenceGroupsCommand_SetsIsSequenceGroupsVisibleToTrue()
        {
            var package = "Package".CreatePackage();
            package.AddPlanogram();
            var packageView = PackageViewModel.NewPackageViewModel(package, false);
            var parentController = new PlanControllerViewModel(packageView.PlanogramViews.First());
            var planVisualDoc = new PlanVisualDocument(parentController, CameraViewType.Design);

            planVisualDoc.ShowSequenceGroupsCommand.Execute();

            Assert.That(parentController.IsSequenceGroupsPanelVisible, Is.True);
        }

        [Test]
        public void AddSequenceSubGroupCommand_CreatesSubGroupInModel()
        {
            var package = "Package".CreatePackage();
            var plan = package.AddPlanogram();
            plan.PlanogramType = PlanogramType.SequenceTemplate;
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay);
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            var sequenceGroup = plan.AddSequenceGroup(blocking.Groups.First(), plan.Products);
            plan.UpdatePositionSequenceData();
            var packageView = PackageViewModel.NewPackageViewModel(package, false);
            var planView = packageView.PlanogramViews.First();
            var parentController = new PlanControllerViewModel(planView);
            var planVisualDoc = new PlanVisualDocument(parentController, CameraViewType.Design);
            parentController.SelectedPlanItems.Add(planView.EnumerateAllPositions().First());

            planVisualDoc.AddSequenceSubGroupCommand.Execute();

            Assert.That(plan.Sequence.Groups.First().SubGroups, Has.Count.EqualTo(1));
            Assert.That(plan.Sequence.Groups.First().Products.First().PlanogramSequenceGroupSubGroupId, Is.Not.Null);
        }

        [Test]
        public void RemoveSequenceSubGroupCommand_RemovesSubGroupInModel()
        {
            var package = "Package".CreatePackage();
            var plan = package.AddPlanogram();
            plan.PlanogramType = PlanogramType.SequenceTemplate;
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay);
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            var sequenceGroup = plan.AddSequenceGroup(blocking.Groups.First(), plan.Products);
            plan.UpdatePositionSequenceData();
            var packageView = PackageViewModel.NewPackageViewModel(package, false);
            var planView = packageView.PlanogramViews.First();
            var parentController = new PlanControllerViewModel(planView);
            var planVisualDoc = new PlanVisualDocument(parentController, CameraViewType.Design);
            parentController.SelectedPlanItems.Add(planView.EnumerateAllPositions().First());
            planVisualDoc.AddSequenceSubGroupCommand.Execute();

            planVisualDoc.RemoveSequenceSubGroupCommand.Execute();

            Assert.That(plan.Sequence.Groups.First().SubGroups, Is.Empty);
            Assert.That(plan.Sequence.Groups.First().Products.First().PlanogramSequenceGroupSubGroupId, Is.Null);
        }

        [Test]
        public void ReverseSequenceOrderCommand_ReverseOrderOfSelectedPositions()
        {
            var package = "Package".CreatePackage();
            var plan = package.AddPlanogram();
            plan.PlanogramType = PlanogramType.SequenceTemplate;
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for(Int32 i = 0; i<4; i++) shelf.AddPosition(bay);
            plan.ReprocessAllMerchandisingGroups();
            var blocking = plan.AddBlocking(new[] { new Tuple<Single,Single>(0,0) });
            plan.AddSequenceGroup(blocking.Groups[0], plan.Products);
            shelf.GetPlanogramComponent().SubComponents[0].FillColourFront = blocking.Groups[0].Colour;
            plan.UpdatePositionSequenceData();
            var packageView = PackageViewModel.NewPackageViewModel(package, false);
            var planView = packageView.PlanogramViews.First();
            var parentController = new PlanControllerViewModel(planView);
            var planVisualDoc = new PlanVisualDocument(parentController, CameraViewType.Design);
            parentController.SelectedPlanItems.AddRange(planView.EnumerateAllPositions());

            planVisualDoc.ReverseSequenceOrderCommand.Execute();

            Assert.That(
                plan.Positions.OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList(),
                Is.EqualTo(plan.Products.Reverse().Select(p => p.Gtin).ToList()));
        }

        [Test]
        public void ReverseSequenceOrderCommand_ReversingTwiceRestoresOriginalOrder()
        {
            var package = "Package".CreatePackage();
            var plan = package.AddPlanogram();
            plan.PlanogramType = PlanogramType.SequenceTemplate;
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (Int32 i = 0; i < 4; i++) shelf.AddPosition(bay);
            plan.ReprocessAllMerchandisingGroups();
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.AddSequenceGroup(blocking.Groups[0], plan.Products);
            shelf.GetPlanogramComponent().SubComponents[0].FillColourFront = blocking.Groups[0].Colour;
            plan.UpdatePositionSequenceData();
            var packageView = PackageViewModel.NewPackageViewModel(package, false);
            var planView = packageView.PlanogramViews.First();
            var parentController = new PlanControllerViewModel(planView);
            var planVisualDoc = new PlanVisualDocument(parentController, CameraViewType.Design);
            parentController.SelectedPlanItems.AddRange(planView.EnumerateAllPositions());

            planVisualDoc.ReverseSequenceOrderCommand.Execute();
            planVisualDoc.ReverseSequenceOrderCommand.Execute();

            Assert.That(
                plan.Positions.OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList(),
                Is.EqualTo(plan.Products.Select(p => p.Gtin).ToList()));
        }
        #endregion

        [Test]
        public void ChangingSequenceGroupColour_OnSequenceTemplate_UpdatesSequenceColourHighlight()
        {
            var package = "Package".CreatePackage();
            var plan = package.AddPlanogram();
            plan.PlanogramType = PlanogramType.SequenceTemplate;
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,0,0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            shelf1.AddPosition(bay);
            shelf2.AddPosition(bay);
            shelf1.GetPlanogramComponent().SubComponents[0].FillColourFront = 1;
            shelf2.GetPlanogramComponent().SubComponents[0].FillColourFront = 2;
            using (var mgs = plan.GetMerchandisingGroups())
            {
                foreach (var mg in mgs) { mg.Process(); mg.ApplyEdit(); }
                plan.Sequence.ApplyFromMerchandisingGroups(mgs);
            }
            var packageView = PackageViewModel.NewPackageViewModel(package);
            var planController = new PlanControllerViewModel(packageView.PlanogramViews[0]);
            App.MainPageViewModel.ActivePlanController = planController;
            var planVisualDoc = planController.AddNewDocument(DocumentType.Design, false);
            var sequenceColourHighlight = Highlight.NewHighlight();
            sequenceColourHighlight.Type = HighlightType.Position;
            sequenceColourHighlight.MethodType = HighlightMethodType.Group;
            sequenceColourHighlight.Field1 = PlanogramPosition.EnumerateDisplayableFieldInfos()
                .First(o => o.PropertyName.Equals(PlanogramPosition.SequenceColourProperty.Name))
                .FieldPlaceholder;
            HighlightUIHelper.CreateSettingGroups(sequenceColourHighlight, plan, true);
            planVisualDoc.Highlight = new HighlightItem(sequenceColourHighlight);

            shelf2.GetPlanogramComponent().SubComponents[0].FillColourFront = 3;

            Assert.That(
                plan.Positions.Select(g => g.SequenceColour).ToList(),
                Is.EquivalentTo(plan.Components.Where(c => c.IsMerchandisable).Select(c => c.SubComponents[0].FillColourFront).ToList()));
            Assert.That(
                planVisualDoc.HighlightLegendItems.Select(g => g.Colour).ToList(),
                Is.EquivalentTo(plan.Components.Where(c => c.IsMerchandisable).Select(c => c.SubComponents[0].FillColourFront).ToList()));
            Assert.That(
                planVisualDoc.HighlightLegendItems.Select(g => g.Colour).ToList(),
                Is.EquivalentTo(plan.Sequence.Groups.Select(g => g.Colour).ToList()));
        }
    }
}
