﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//    Created.
// V8-28444 : M.Shelley
//  Fix failed unit test
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.PlanViews.PlanogramConsumerDecisionTree
{
    [TestFixture]
    public class ConsumerDecisionTreePlanDocumentTests : TestBase<ConsumerDecisionTreePlanDocument>
    {
        #region Test Fixture Helpers

        ConsumerDecisionTreePlanDocument _planDoc;

        [SetUp]
        public new void Setup()
        {
            var planogram = Planogram.NewPlanogram();
            PlanogramProduct planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Name = "Beans";
            planogramProduct.Brand = "Brand1";
            planogramProduct.Gtin = "12341234123412";

            PlanogramProduct planogramProduct2 = PlanogramProduct.NewPlanogramProduct();
            planogramProduct2.Name = "Beans2";
            planogramProduct2.Brand = "Brand2";
            planogramProduct2.Gtin = "12341234123413";

            planogram.Products.Add(planogramProduct);
            planogram.Products.Add(planogramProduct2);

            var planView = new PlanogramView(planogram);
            var parentController = new PlanControllerViewModel(planView);
            _planDoc = new ConsumerDecisionTreePlanDocument(parentController);

            _planDoc.SelectedNodes.Add(_planDoc.RootNode);
            _planDoc.SelectedLevelId = _planDoc.ConsumerDecisionTreeModel.RootLevel.Id;
        }

        private Boolean CheckCollectionContainsPlanogramProduct(PlanogramProductView expected, IEnumerable<PlanogramProductView> collection)
        {
            foreach (PlanogramProductView productInfo in collection)
            {
                if (productInfo.Model.Id != expected.Model.Id) { continue; }
                if (productInfo.Model.Parent != expected.Model.Parent) { continue; }
                if (productInfo.Gtin != expected.Gtin) { continue; }
                if (productInfo.Name != expected.Name) { continue; }

                return true;
            }
            return false;
        }

        #endregion

        #region Properties

        [Test]
        public void Property_ConsumerDecisionTreeModel()
        {
            String propertyName = "ConsumerDecisionTreeModel";
            Assert.IsNotNull(ConsumerDecisionTreePlanDocument.ConsumerDecisionTreeModelProperty);
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.ConsumerDecisionTreeModelProperty.Path);
        }

        [Test]
        public void Property_RootNode()
        {
            String propertyName = "RootNode";
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.RootNodeProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(_planDoc.GetType(), propertyName);
            Assert.IsNotNull(_planDoc.RootNode, "A new root node view should be set when the page loads");
            Assert.IsTrue(_planDoc.RootNode.IsNew, "A new root node view should be set when the page loads");
        }

        [Test]
        public void Property_SelectedLevelId()
        {
            String propertyName = "SelectedLevelId";
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.SelectedLevelIdProperty.Path);

            Assert.IsNotNull(_planDoc.SelectedLevelId, "Selected level id should be set the root level's id");
        }

        [Test]
        public void Property_SelectedNodes()
        {
            String propertyName = "SelectedNodes";
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.SelectedNodesProperty.Path);

            Assert.AreEqual(1, _planDoc.SelectedNodes.Count, "Should start with CDT's root node selected");
        }

        [Test]
        public void Property_SelectedUnassignedProducts()
        {
            String propertyName = "SelectedUnassignedProducts";
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.SelectedUnassignedProductsProperty.Path);

            AssertHelper.AssertIsEditableObservable(_planDoc.SelectedUnassignedProducts);
            Assert.IsEmpty(_planDoc.SelectedUnassignedProducts);
        }

        [Test]
        public void Property_UnassignedProducts()
        {
            String propertyName = "UnassignedProducts";
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.UnassignedProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(_planDoc.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyCollection(_planDoc.UnassignedProducts);

            Assert.IsEmpty(_planDoc.UnassignedProducts);

            _planDoc.RemoveAllProductsCommand.Execute();

            Assert.IsNotEmpty(_planDoc.UnassignedProducts);

        }

        [Test]
        public void Property_SelectedAssignedProducts()
        {
            String propertyName = "SelectedAssignedProducts";
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.SelectedAssignedProductsProperty.Path);

            AssertHelper.AssertIsEditableObservable(_planDoc.SelectedAssignedProducts);
            Assert.IsEmpty(_planDoc.SelectedAssignedProducts);
        }

        [Test]
        public void Property_SelectedNodeProducts()
        {
            String propertyName = "SelectedNodeProducts";
            Assert.AreEqual(propertyName, ConsumerDecisionTreePlanDocument.SelectedNodeProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(_planDoc.GetType(), propertyName);
            AssertHelper.AssertIsReadOnlyCollection(_planDoc.SelectedNodeProducts);

            Assert.IsNotEmpty(_planDoc.SelectedNodeProducts);
        }

        #endregion

        #region Commands

        #region AddNodeCommand

        [Test]
        public void Command_AddNode_Execution()
        {
            RelayCommand cmd = _planDoc.AddNodeCommand;

            //check command is registered
            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            //can execute
            Assert.IsTrue(cmd.CanExecute(), "Should be able to execute with as new CDT selects root node");
            _planDoc.SelectedNodes.Clear();
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected nodes");
            _planDoc.SelectedNodes.Add(_planDoc.RootNode);
            Assert.IsTrue(cmd.CanExecute());

            //execute
            PlanogramConsumerDecisionTreeNode previousNode = _planDoc.SelectedNodes.First();//_planDoc.SelectedNode;
            cmd.Execute();

            Assert.IsNotNull(previousNode.ChildList.FirstOrDefault());
            Assert.IsTrue(previousNode.ChildList.First().IsNew);
        }

        #endregion

        #region RemoveNodeCommand

        [Test]
        [STAThread]
        public void Command_RemoveNode_Execution()
        {
            RelayCommand cmd = _planDoc.RemoveNodeCommand;

            //check command is registered
            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            //can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute as new CDT selects root node");
            _planDoc.SelectedNodes.Clear();
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected nodes");

            //Add a new node to the cdt to be removed
            _planDoc.SelectedNodes.Add(_planDoc.RootNode);
            _planDoc.AddNodeCommand.Execute();

            _planDoc.SelectedNodes.Clear();
            _planDoc.SelectedNodes.AddRange(_planDoc.RootNode.ChildList);

            Assert.IsTrue(cmd.CanExecute(), "Should now be able to execute the command with the new node selected");

            //execute
            PlanogramConsumerDecisionTreeNode previousNode = _planDoc.RootNode.ChildList.FirstOrDefault();
            cmd.Execute();

            Assert.IsTrue(previousNode.IsDeleted, "The node should have been removed");
            Assert.AreEqual(0, _planDoc.RootNode.ChildList.Count);
        }

        #endregion

        #region EditNodeCommand

        [Test]
        public void Command_EditNode_Execution()
        {
            RelayCommand cmd = _planDoc.EditNodeCommand;

            //check command is registered
            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            //Execute opens new window so cannot be tested
        }

        #endregion

        #region SplitNodeCommand

        [Test]
        public void Command_SplitNode_Execution()
        {
            RelayCommand<String> cmd = _planDoc.SplitNodeCommand;

            //Check command is registered
            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(null));

            //Remove all products as a new root node will have all he products assigned
            _planDoc.RemoveAllProductsCommand.Execute();

            //Create a property description to test
            String testProperty = "Brand";
            Assert.IsFalse(cmd.CanExecute(testProperty), "Requires a selected node with assigned products");

            _planDoc.AddAllProductsCommand.Execute();

            _planDoc.SelectedNodes.Clear();

            //Get the first available leaf node to select
            PlanogramConsumerDecisionTreeNode rootNode = _planDoc.RootNode;

            _planDoc.SelectedNodes.Add(rootNode);

            Assert.IsTrue(cmd.CanExecute(testProperty), "Requires a selected node with assigned products");
            IEnumerable<PlanogramProduct> products = _planDoc.ConsumerDecisionTreeModel.Parent.Products;
            IEnumerable<IGrouping<String, PlanogramProduct>> groupedProducts = products.GroupBy(p => p.Brand);

            //Check the execute
            cmd.Execute(testProperty);

            //Created nodes should now be selected
            Assert.AreEqual(groupedProducts.Count(), _planDoc.SelectedNodes.Count, "Node should have been split by size and have the same number as the groups");
            foreach (IGrouping<String, PlanogramProduct> expectedGrouping in groupedProducts)
            {
                PlanogramConsumerDecisionTreeNode foundNode = _planDoc.SelectedNodes.FirstOrDefault(p => p.Name.Equals(expectedGrouping.Key));
                Assert.IsNotNull(foundNode, "A node should have been created for this key");

                Assert.AreEqual(expectedGrouping.Count(), foundNode.Products.Count);

                List<Object> nodeProductIds = foundNode.Products.Select(p => p.PlanogramProductId).ToList();
                foreach (PlanogramProduct expectedProduct in expectedGrouping)
                {
                    Assert.Contains(expectedProduct.Id, nodeProductIds);
                }
            }
        }

        #endregion

        #region AddLevelCommand

        [Test]
        public void Command_AddLevel_Execution()
        {
            RelayCommand cmd = _planDoc.AddLevelCommand;

            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            PlanogramConsumerDecisionTreeLevel previousLevel = _planDoc.ConsumerDecisionTreeModel.RootLevel;
            cmd.Execute();

            PlanogramConsumerDecisionTreeLevel newLevel = _planDoc.ConsumerDecisionTreeModel.RootLevel.ChildLevel;
            Assert.AreNotEqual(previousLevel, newLevel);

            Assert.IsTrue(newLevel.IsNew, "Should be a new level");
            Assert.IsTrue(newLevel.IsInitialized, "Should be initialised");
        }

        #endregion

        #region RemoveLevelCommand

        [Test]
        public void Command_RemoveLevel_Execution()
        {
            RelayCommand cmd = _planDoc.RemoveLevelCommand;

            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            //Check can execute
            Assert.IsFalse(cmd.CanExecute(), "Selected level will be the root level whick cannot be removed");

            //Add a level to remove
            _planDoc.AddLevelCommand.Execute();
            _planDoc.SelectedLevelId = _planDoc.ConsumerDecisionTreeModel.RootLevel.ChildLevel.Id;

            Assert.IsTrue(cmd.CanExecute(), "The new level should be selected");

            //Check execute
            PlanogramConsumerDecisionTreeLevel previousLevel = _planDoc.ConsumerDecisionTreeModel.RootLevel.ChildLevel;
            cmd.Execute();

            PlanogramConsumerDecisionTreeLevel newLevel = _planDoc.ConsumerDecisionTreeModel.RootLevel.ChildLevel;
            Assert.AreNotEqual(previousLevel, newLevel);
            Assert.IsNull(newLevel);
        }

        #endregion

        #region AddSelectedProductsCommand

        [Test]
        public void Command_AddSelectedProducts_Execution()
        {
            RelayCommand cmd = _planDoc.AddSelectedProductsCommand;

            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Cannot execute without selected unassigned products");

            //Remove all products as a new root node will have all he products assigned
            _planDoc.RemoveAllProductsCommand.Execute();

            //Select products
            PlanogramProductView testProduct = _planDoc.UnassignedProducts.First();

            _planDoc.SelectedUnassignedProducts.Add(testProduct);

            //Check execute
            Assert.IsTrue(cmd.CanExecute());

            //Execute
            cmd.Execute();

            //Check unassigned products
            Assert.IsEmpty(_planDoc.SelectedUnassignedProducts, "Selected unassigned products should have been cleared");
            Assert.IsFalse(_planDoc.UnassignedProducts.Contains(testProduct), "The product should have been removed from the unassigned collection");

            //Check the assigned products
            Assert.AreEqual(1, _planDoc.SelectedNodeProducts.Count);
            Assert.AreEqual(testProduct.Model.Id, _planDoc.SelectedNodeProducts.First().Model.Id, "The test product should have been moved into the assigned collection");

            Assert.AreEqual(testProduct.Model.Id, _planDoc.SelectedNodes.First().Products.First().PlanogramProductId);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        [Test]
        public void Command_RemoveSelectedProducts_Execution()
        {
            RelayCommand cmd = _planDoc.RemoveSelectedProductsCommand;

            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected assigned products");

            //Remove all products as a new root node will have all he products assigned
            _planDoc.RemoveAllProductsCommand.Execute();

            //Add products
            PlanogramProductView testProduct = _planDoc.UnassignedProducts.First();
            _planDoc.SelectedUnassignedProducts.Add(testProduct);
            _planDoc.AddSelectedProductsCommand.Execute();

            _planDoc.SelectedAssignedProducts.Add(testProduct);
            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            cmd.Execute();
            Assert.IsEmpty(_planDoc.SelectedAssignedProducts);
            Assert.IsEmpty(_planDoc.SelectedNodeProducts);
            Assert.IsTrue(CheckCollectionContainsPlanogramProduct(testProduct, _planDoc.UnassignedProducts));
        }

        #endregion

        #region AddAllProductsCommand

        [Test]
        public void Command_AddAllProducts_Execution()
        {
            RelayCommand cmd = _planDoc.AddAllProductsCommand;

            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            //remove all products as node will have all products if new
            _planDoc.RemoveAllProductsCommand.Execute();

            //check if command can execute
            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            Int32 expectedCount = _planDoc.UnassignedProducts.Count;

            //execute
            cmd.Execute();
            Assert.IsEmpty(_planDoc.UnassignedProducts, "Unassigned products should have been cleared");
            Assert.AreEqual(expectedCount, _planDoc.SelectedNodeProducts.Count);
        }

        #endregion

        #region RemoveAllProductsCommand

        [Test]
        public void Command_RemoveAllProducts_Execution()
        {
            RelayCommand cmd = _planDoc.RemoveAllProductsCommand;

            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            //check if command can execute
            Assert.IsTrue(cmd.CanExecute());

            //Check execute
            Int32 expectedCount = _planDoc.SelectedNodeProducts.Count;

            //execute the command
            cmd.Execute();
            Assert.IsEmpty(_planDoc.SelectedNodeProducts, "Unassigned products should have been cleared");
            Assert.AreEqual(expectedCount, _planDoc.UnassignedProducts.Count);
        }

        #endregion

        #region MoreSplitPropertiesCommand

        [Test]
        public void Command_MoreSplitProperties_Execution()
        {
            RelayCommand cmd = _planDoc.MoreSplitPropertiesCommand;

            //Check command is registered
            Assert.Contains(cmd, _planDoc.ViewModelCommands);

            //Remove all products as a new root node will have all he products assigned
            _planDoc.RemoveAllProductsCommand.Execute();

            //can execute
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute as the selected node has no products");

            //Add products
            foreach (PlanogramProduct product in _planDoc.ConsumerDecisionTreeModel.Parent.Products)
            {
                _planDoc.ConsumerDecisionTreeModel.RootNode.Products.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(product.Id));
            }
            Assert.IsTrue(cmd.CanExecute());

            _planDoc.SelectedNodes.Clear();
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute with no selected nodes");

            //Add a new node to the cdt
            _planDoc.SelectedNodes.Add(_planDoc.RootNode);
            _planDoc.AddNodeCommand.Execute();

            _planDoc.SelectedNodes.Clear();
            _planDoc.SelectedNodes.Add(_planDoc.RootNode);
            Assert.IsFalse(cmd.CanExecute(), "Should not be able to execute as the selected node is no longer a leaf");

            _planDoc.SelectedNodes.Clear();
            _planDoc.SelectedNodes.AddRange(_planDoc.RootNode.ChildList);
            Assert.IsTrue(cmd.CanExecute(), "Should now be able to execute the command with the new node selected");

            //Execute opens new window
        }

        #endregion

        #endregion
    }
}
