﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
#endregion
#region Version History: (CCM 8.2)
// V8-31116 : L.Ineson
//  Added more tests.
// V8-31366 : A.Probyn
//  Fixed test data on UnrangeAllProducts_Executed_SetsIsRangedToFalse
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32560 : A.Silva
//  Amended AddAssortmentProductFromRepository_Executed as the Product Selector no longer allows adding all.

#endregion
#endregion

using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Model;
using System;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;

namespace Galleria.Ccm.UnitTests.UI.PlanViews
{
    [TestFixture]
    public class AssortmentPlanDocumentTests : TestBase
    {
        #region Test Fixture Helpers

        /// <summary>
        /// Creates a new sparkling water test plan and creates a new assortment doc for it.
        /// </summary>
        /// <returns></returns>
        private AssortmentPlanDocument CreateVm()
        {
            PlanControllerViewModel controller = CreatePlanController();
            AssortmentPlanDocument assortmentDoc = (AssortmentPlanDocument)controller.AddNewDocument(Ccm.Model.DocumentType.Assortment, true);
            return assortmentDoc;
        }

        private AssortmentPlanDocument CreateVm(PlanControllerViewModel controller)
        {
            AssortmentPlanDocument assortmentDoc = 
                (AssortmentPlanDocument)controller.AddNewDocument(Ccm.Model.DocumentType.Assortment, true);
            return assortmentDoc;
        }

        private PlanControllerViewModel CreatePlanController()
        {
            //add ok response to properties dialog.
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            
            PlanControllerViewModel controller = App.MainPageViewModel.CreateNewPlanogram();
            controller.CloseAllDocuments();
            controller.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(controller.SourcePlanogram.Model);
            controller.SourcePlanogram.EndUpdate();
            return controller;
        }

        private PlanogramAssortmentProduct AddAssortmentProduct(AssortmentPlanDocument vm)
        {
            PlanogramProduct prod = vm.Planogram.Model.Products.FirstOrDefault(
                p => !vm.AssortmentModel.Products.Select(a => a.Gtin).Contains(p.Gtin));

            if (prod == null)
            {
                prod = PlanogramProduct.NewPlanogramProduct();
                prod.Gtin = "g" + vm.Planogram.Model.Products.Count;
                prod.Name =  "n" + vm.Planogram.Model.Products.Count;
                vm.Planogram.Model.Products.Add(prod);
            }

            PlanogramAssortmentProduct ap = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(prod.Gtin, prod.Name);
            vm.AssortmentModel.Products.Add(ap);
            return ap;
        }

        #endregion
        
        #region Properties

        [Test]
        public void Rows_IsInitialized()
        {
            PlanControllerViewModel controller = CreatePlanController();
            AssortmentPlanDocument vm = CreateVm(controller);

            Assert.IsNotNull(vm.Rows);

            //check that the row count matches
            Assert.AreEqual(vm.Planogram.Model.Assortment.Products.Count, vm.Rows.Count);

            //close the doc
            controller.CloseDocument(vm);

            //add a new assortment product
            PlanogramProduct product = vm.Planogram.Model.Products.First(p =>
               !vm.Planogram.Model.Assortment.Products.Select(a => a.Gtin).Contains(p.Gtin));

            vm.Planogram.Model.Assortment.Products.Add(product);

            //open a new doc
            vm = CreateVm(controller);

            //check that the row count matches
            Assert.AreEqual(vm.Planogram.Model.Assortment.Products.Count, vm.Rows.Count);
        }

        [Test]
        public void SelectedProducts_IsInitialized()
        {
            AssortmentPlanDocument vm = CreateVm();
            Assert.IsNotNull(vm);
        }

        /// <summary>
        /// Checks that when a position is selected on a plan, 
        /// the assortment product is also selected
        /// </summary>
        [Test]
        public void SelectedProducts_UpdatedWhenPositionsSelected()
        {
            PlanControllerViewModel controller = CreatePlanController();
            PlanogramView planView=  controller.SourcePlanogram;

            //Make sure the first 2 positions have assortment products
            PlanogramProductView product = planView.Products.First(p =>
               !planView.Model.Assortment.Products.Select(a => a.Gtin).Contains(p.Gtin));
            planView.Model.Assortment.Products.Add(product.Model);

            PlanogramProductView product2 = planView.Products.First(p =>
               !planView.Model.Assortment.Products.Select(a => a.Gtin).Contains(p.Gtin));
            planView.Model.Assortment.Products.Add(product2.Model);


            PlanogramPositionView p1 = planView.EnumerateAllPositions().First(p => p.Product == product);
            PlanogramPositionView p2 = planView.EnumerateAllPositions().First(p => p.Product == product2);

            AssortmentPlanDocument vm = CreateVm(controller);

            //clear any existing selection
            controller.SelectedPlanItems.Clear();
            Assert.AreEqual(0, vm.SelectedProducts.Count);

            //select a position
            controller.SelectedPlanItems.Add(p1);

            //check that the assortment doc now has the product selected
            Assert.AreEqual(1, vm.SelectedProducts.Count);
            Assert.AreEqual(product, vm.SelectedProducts[0].Product);

            //add another
            controller.SelectedPlanItems.Add(p2);
            Assert.AreEqual(2, vm.SelectedProducts.Count);
            Assert.AreEqual(product2, vm.SelectedProducts[1].Product);

            //remove the first
            controller.SelectedPlanItems.Remove(p1);
            Assert.AreEqual(1, vm.SelectedProducts.Count);
            Assert.AreEqual(product2, vm.SelectedProducts[0].Product);

            //now check it also works the other way
            controller.SelectedPlanItems.Clear();
            Assert.AreEqual(0, vm.SelectedProducts.Count);

            vm.SelectedProducts.Add(product);
            Assert.AreEqual(1, controller.SelectedPlanItems.Count);
            Assert.AreEqual(p1, controller.SelectedPlanItems[0]);

            //now select a component, the assortment doc should not add anything but it should not crash out either.
            controller.SelectedPlanItems.Clear();
            controller.SelectedPlanItems.Add(
                controller.SourcePlanogram.EnumerateMerchandisableSubComponents().First().Component);

            Assert.AreEqual(0, vm.SelectedProducts.Count);

        }

        #endregion

        #region Commands

        #region ShowProductRules

        [Test]
        public void ShowProductRules_CanExecute_ReturnsFalseWhenNoAssortmentProducts()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var result = planDoc.ShowProductRulesCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void ShowProductRules_CanExecute_ReturnsTrueWhenSomeAssortmentProducts()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            planDoc.AssortmentModel.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct());

            var result = planDoc.ShowProductRulesCommand.CanExecute();

            Assert.That(result);
        }

        #endregion

        #region ShowFamilyRules

        [Test]
        public void ShowFamilyRules_Executed()
        {
            Assert.Ignore();
        }

        #endregion

        #region ShowLocalProducts

        [Test]
        public void ShowLocalProducts_CanExecute_ReturnsFalseWhenNoAssortmentProducts()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var result = planDoc.ShowLocalProductsCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void ShowLocalProducts_CanExecute_ReturnsTrueWhenSomeAssortmentProducts()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            planDoc.AssortmentModel.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct());

            var result = planDoc.ShowLocalProductsCommand.CanExecute();

            Assert.That(result);
        }

        #endregion

        #region RegionalProductSetup

        [Test]
        public void RegionalProductSetup_Executed()
        {
            Assert.Ignore();
        }

        #endregion

        #region ShowRulePriorities

        [Test]
        public void ShowRulePriorities_Executed()
        {
            Assert.Ignore();
        }


        #endregion

        #region RemoveSelectedProducts

        [Test]
        public void RemoveSelectedProducts_CanExecute_ReturnsFalseWhenSelectedProductsEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var result = planDoc.RemoveSelectedProductsCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void RemoveSelectedProducts_CanExecute_ReturnsTrueWhenSelectedProductsNotEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var product = AddAssortmentProduct(planDoc);

            planDoc.SelectedProducts.Add(planDoc.Rows.First());

            var result = planDoc.RemoveSelectedProductsCommand.CanExecute();

            Assert.That(result);
        }

        [Test]
        public void RemoveSelectedProducts_Executed_RemovesProduct()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var product = AddAssortmentProduct(planDoc);
            var p = product.GetPlanogramProduct();
            planDoc.SelectedProducts.Add(planDoc.Planogram.FindProduct(p.Id));

            planDoc.RemoveSelectedProductsCommand.Execute();

            CollectionAssert.DoesNotContain(planDoc.SelectedProducts, product);
            CollectionAssert.DoesNotContain(planDoc.AssortmentModel.Products, product);
        }

        #endregion

        #region UnrangeSelectedProducts

        [Test]
        public void UnrangeSelectedProducts_CanExecute_ReturnsFalseWhenSelectedProductsEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var result = planDoc.UnrangeSelectedProductsCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void UnrangeSelectedProducts_CanExecute_ReturnsFalseWhenSelectedProductsNotEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var product = AddAssortmentProduct(planDoc);

            planDoc.SelectedProducts.Add(planDoc.Rows.First());

            var result = planDoc.UnrangeSelectedProductsCommand.CanExecute();

            Assert.That(result);
        }

        [Test]
        public void UnrangeSelectedProducts_Executed_SetsIsRangedToFalse()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var product = AddAssortmentProduct(planDoc);

            planDoc.SelectedProducts.Add(planDoc.Rows.First());

            planDoc.UnrangeSelectedProductsCommand.Execute();

            Assert.IsFalse(product.IsRanged);
        }

        #endregion

        #region RangeSelectedProducts

        public void RangeSelectedProducts_CanExecute_ReturnsFalseWhenSelectedProductsEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var result = planDoc.RangeSelectedProductsCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void RangeSelectedProducts_CanExecute_ReturnsTrueWhenSelectedProductsNotEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var product = AddAssortmentProduct(planDoc);

            planDoc.SelectedProducts.Add(planDoc.Rows.First());

            var result = planDoc.RangeSelectedProductsCommand.CanExecute();

            Assert.That(result);
        }

        [Test]
        public void RangeSelectedProducts_Executed_SetsIsRangedToFalse()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var product = AddAssortmentProduct(planDoc);

            planDoc.SelectedProducts.Add(planDoc.Rows.First());
            planDoc.RangeSelectedProductsCommand.Execute();

            Assert.That(product.IsRanged);
        }

        #endregion

        #region UnrangeAllProducts

        [Test]
        public void UnrangeAllProducts_CanExecute_ReturnsFalseWhenAssortmentProductsEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var result = planDoc.UnrangeAllProductsCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void UnrangeAllProducts_CanExecute_ReturnsFalseWhenAssortmentProductsNotEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            planDoc.AssortmentModel.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct());

            var result = planDoc.UnrangeAllProductsCommand.CanExecute();

            Assert.That(result);
        }

        [Test]
        public void UnrangeAllProducts_Executed_SetsIsRangedToFalse()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var products = new List<PlanogramAssortmentProduct>()
            {
                PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(),
                PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(),
                PlanogramAssortmentProduct.NewPlanogramAssortmentProduct()
            };
            products[0].Gtin = "001";
            products[1].Gtin = "002";
            products[2].Gtin = "003";

            planDoc.AssortmentModel.Products.AddRange(products);

            planDoc.UnrangeAllProductsCommand.Execute();

            Assert.That(planDoc.AssortmentModel.Products.All(p => !p.IsRanged));
        }

        #endregion

        #region RangeAllProducts

        [Test]
        public void RangeAllProducts_CanExecute_ReturnsFalseWhenAssortmentProductsEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var result = planDoc.RangeAllProductsCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void RangeAllProducts_CanExecute_ReturnsFalseWhenAssortmentProductsNotEmpty()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            planDoc.AssortmentModel.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct());

            var result = planDoc.RangeAllProductsCommand.CanExecute();

            Assert.That(result);
        }

        [Test]
        public void RangeAllProducts_Executed_SetsIsRangedToFalse()
        {
            AssortmentPlanDocument planDoc = CreateVm();
            var products = new List<PlanogramAssortmentProduct>()
            {
                PlanogramAssortmentProduct.NewPlanogramAssortmentProduct("gtin1", "name1"),
                PlanogramAssortmentProduct.NewPlanogramAssortmentProduct("gtin2", "name2"),
                PlanogramAssortmentProduct.NewPlanogramAssortmentProduct("gtin3", "name3")
            };


            planDoc.AssortmentModel.Products.AddRange(products);

            planDoc.RangeAllProductsCommand.Execute();

            Assert.That(planDoc.AssortmentModel.Products.All(p => p.IsRanged));
        }

        #endregion

        #region AddAssortmentProduct

        [Test]
        public void AddAssortmentProduct_Executed()
        {
            Assert.Ignore();
        }

        [Test]
        public void AddAssortmentProductFromRepository_Executed()
        {
            //add a couple of products to the db
            SetMockRepositoryConnection();
            Product prod1 = Product.NewProduct(this.EntityId);
            prod1.Gtin = "GTIN1";
            prod1.Name = "TestProduct1";
            prod1 = prod1.Save();

            Product prod2 = Product.NewProduct(this.EntityId);
            prod2.Gtin = "GTIN2";
            prod2.Name = "TestProduct2";
            prod2 = prod2.Save();

            var vm = CreateVm();


            this.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (args) =>
                {
                    //In the search window - select both products.
                    var winView = (ProductSelectorViewModel)args.WindowParameters.First();
                    winView.SearchMode = ProductSelectorSearchMode.Criteria;
                    winView.SearchCriteria = "TestProduct";
                    Assert.AreEqual(2, winView.SearchResults.Count);
                    winView.AssignedProducts.AddRange(new List<Product>{prod1, prod2});
                    //winView.AssignSelectedCommand.Execute();

                    winView.OKCommand.Execute();
                });

            Int32 preAssortmentProductCount = vm.AssortmentModel.Products.Count;
            Int32 preProductCount = vm.Planogram.Products.Count;

            //execute the command
            vm.AddAssortmentProductFromRepositoryCommand.Execute();

            //check that we now have 2 new assortment products
            Assert.AreEqual(preAssortmentProductCount + 2, vm.AssortmentModel.Products.Count);
            Assert.Contains(prod1.Gtin, vm.AssortmentModel.Products.Select(P => P.Gtin).ToList());
            Assert.Contains(prod2.Gtin, vm.AssortmentModel.Products.Select(P => P.Gtin).ToList());

            //check that 2 new products have also been added
            Assert.AreEqual(preProductCount + 2, vm.Planogram.Products.Count);
            Assert.Contains(prod1.Gtin, vm.Planogram.Products.Select(P => P.Gtin).ToList());
            Assert.Contains(prod2.Gtin, vm.Planogram.Products.Select(P => P.Gtin).ToList());

        }

        [Test]
        public void AddAssortmentProductFromLibrary_Executed()
        {
            Assert.Ignore();
        }

        #endregion

        #region SelectAssortment

        [Test]
        public void SelectAssortment_Executed()
        {
            SetMockRepositoryConnection();

            AssortmentPlanDocument planDoc = CreateVm();
            planDoc.AssortmentModel.Products.Clear();

            //make sure that all plan products are in masterdata
            List<Product> productList =
                ProductList.FetchByEntityIdProductGtins(this.EntityId, planDoc.Planogram.Products.Select(p=> p.Gtin))
                .ToList();

            foreach(var pprod in planDoc.Planogram.Products)
            {
                Product prod = productList.FirstOrDefault(p=> p.Gtin == pprod.Gtin);
                if(prod == null)
                {
                   prod= Product.NewProduct(this.EntityId);
                    prod.Gtin = pprod.Gtin;
                    prod.Name = pprod.Name;
                    prod = prod.Save();
                    productList.Add(prod);
                }
            }

            //create an assortment from the planogram,
           // using existing products
            Assortment assortment = Assortment.NewAssortment(this.EntityId);
            assortment.Name = "NEW";
            foreach (var product in planDoc.Planogram.Products)
            {
                Product prod = productList.FirstOrDefault(p=> p.Gtin == product.Gtin);
                assortment.Products.Add(AssortmentProduct.NewAssortmentProduct(prod));
            }

            //add a new extra product
            Product newProd = Product.NewProduct(this.EntityId);
            newProd.Gtin = "g1";
            newProd.Name = "n1";
            newProd = newProd.Save();
            assortment.Products.Add(newProd);

            assortment = assortment.Save();


            //now load the assortment into the doc.
            Assert.AreEqual(0, planDoc.Rows.Count);

            AssortmentInfo ai = AssortmentInfoList.FetchByEntityId(this.EntityId).First(a=> a.Id == assortment.Id);
            planDoc.SelectAssortmentCommand.Execute(ai);

            //check that this has just loaded
            Assert.AreEqual(assortment.Products.Count, planDoc.Rows.Count);

            //and the new product has been added
            Assert.IsNotNull(planDoc.Planogram.Products.FirstOrDefault(p => p.Gtin == newProd.Gtin));

        }

        [Test]
        public void SelectAssortment_Executed_MergeWithExisting()
        {
            Assert.Ignore();
        }

        [Test]
        public void SelectAssortment_Executed_ReplaceExisting()
        {
            Assert.Ignore();
        }

        #endregion

        #endregion
    }
}
