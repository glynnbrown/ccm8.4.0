﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26685 : A.Silva ~ Created
// V8-26721 : A.Silva ~ Refactored to use ValidationTemplate Interfaces.
// V8-26815 : A.Silva ~ Refactored some tests.
// V8-26817 : A.Silva ~ Refactored usage of IValidationTemplateEditorViewModel.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.ValidationTemplateEditor;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Moq;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.ValidationTemplateEditor
{
    [TestFixture]
    public class ValidationTemplateEditorViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        private const PlanogramView InvalidPlanogramView = null;
        private PlanogramView _validPlanogramView;

        [SetUp]
        public void Setup()
        {
            _validPlanogramView = new PlanogramView(Planogram.NewPlanogram());
        }

        private static IValidationTemplate GetValidationTemplate(bool isValid = true, IList<IValidationTemplateGroup> groupList = null)
        {
            if (groupList == null) groupList = GetGroupList();

            var modelMock = new Mock<IValidationTemplate>();

            modelMock.Setup(o => o.IsValid).Returns(isValid);
            modelMock.Setup(o => o.Groups).Returns(groupList);

            return modelMock.Object;
        }

        private static List<IValidationTemplateGroup> GetGroupList(int validGroups = 1, int invalidGroups = 0)
        {
            var returnValue = new List<IValidationTemplateGroup>();
            for (var i = 0; i < validGroups; i++) returnValue.Add(GetValidationTemplateGroup(metricList: GetMetricList()));
            for (var i = 0; i < invalidGroups; i++) returnValue.Add(GetValidationTemplateGroup(isValid: false));
            return returnValue;
        }

        private static IValidationTemplateGroup GetValidationTemplateGroup(bool isValid = true, IList<IValidationTemplateGroupMetric> metricList = null)
        {
            if (metricList == null) metricList = GetMetricList();

            var modelMock = new Mock<IValidationTemplateGroup>();

            modelMock.Setup(o => o.IsValid).Returns(isValid);
            modelMock.Setup(o => o.Metrics).Returns(metricList);

            return modelMock.Object;
        }

        private static List<IValidationTemplateGroupMetric> GetMetricList(int validMetrics = 1, int invalidMetrics = 0)
        {
            var returnValue = new List<IValidationTemplateGroupMetric>();
            for (var i = 0; i < validMetrics; i++) returnValue.Add(GetValidationTemplateGroupMetric());
            for (var i = 0; i < invalidMetrics; i++) returnValue.Add(GetValidationTemplateGroupMetric(isValid: false));
            return returnValue;
        }

        private static IValidationTemplateGroupMetric GetValidationTemplateGroupMetric(bool isValid = true)
        {
            var modelMock = new Mock<IValidationTemplateGroupMetric>();

            modelMock.Setup(o => o.IsValid).Returns(isValid);

            return modelMock.Object;
        }

        private ValidationTemplateInfoList InsertTestRepositoryData()
        {
            ValidationTemplate t1 = ValidationTemplate.NewValidationTemplate(App.ViewState.EntityId);
            t1.Name = "Test1";
            t1.Save();

            ValidationTemplate t2 = ValidationTemplate.NewValidationTemplate(App.ViewState.EntityId);
            t2.Name = "Test2";
            t2.Save();

            return ValidationTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
        }

        #endregion

        #region Commands

        [Test]
        public void OpenCommand_EnabledWhenNoRepository()
        {
            App.ViewState.ClearRepositoryConnection();

            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            Assert.IsTrue(viewModel.OpenCommand.CanExecute());
        }

        [Test]
        public void OpenCommand_EnabledWhenNoFetchPerm()
        {
            SetMockRepositoryConnection();
            ValidationTemplateInfoList infos = InsertTestRepositoryData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateGet.ToString());
            
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            Assert.IsTrue(viewModel.OpenCommand.CanExecute());
        }

        [Test]
        public void OpenFromRepositoryCommand_DisabledWhenNoFetchPerm()
        {
            SetMockRepositoryConnection();
            ValidationTemplateInfoList infos = InsertTestRepositoryData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateGet.ToString());

            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            Assert.IsFalse(viewModel.OpenFromRepositoryCommand.CanExecute(infos.First().Id));
        }

        [Test]
        public void OpenFromRepositoryCommand_Execution()
        {
            SetMockRepositoryConnection();
            Int32 entityId = App.ViewState.EntityId;

            //setup the template
            ValidationTemplate template = ValidationTemplate.NewValidationTemplate(entityId);
            template.Name = Guid.NewGuid().ToString();
            ValidationTemplateGroup g1 = template.Groups.FirstOrDefault();
            if (g1 == null)
            {
                g1 = ValidationTemplateGroup.NewValidationTemplateGroup();
                template.Groups.Add(g1);
                g1.Name = "group1";
            }
            ValidationTemplateGroupMetric m1 = g1.Metrics.FirstOrDefault();
            if (m1 == null)
            {
                m1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
                g1.Metrics.Add(m1);
                m1.Field = "[Planogram.Height]";
            }

            //save it
            template = template.SaveAs();
            Object templateId = template.Id;
            template.Dispose();
            template = null;

            //open
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            viewModel.OpenFromRepositoryCommand.Execute(templateId);

            //check if file has opened correctly
            Assert.AreEqual("[Planogram.Height]", ((PlanogramValidationTemplate)viewModel.ValidationTemplateEdit).Groups[0].Metrics[0].Field);
        }

        [Test]
        public void OpenFromFileCommand_Execution()
        {
            //setup the template
            ValidationTemplate template = ValidationTemplate.NewValidationTemplate(1);
            template.Name = Guid.NewGuid().ToString();
            ValidationTemplateGroup g1 = template.Groups.FirstOrDefault();
            if (g1 == null)
            {
                g1 = ValidationTemplateGroup.NewValidationTemplateGroup();
                template.Groups.Add(g1);
                g1.Name = "group1";
            }
            ValidationTemplateGroupMetric m1 = g1.Metrics.FirstOrDefault();
            if (m1 == null)
            {
                m1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
                g1.Metrics.Add(m1);
                m1.Field = "[Planogram.Height]";
            }

            //save it
            String expectedPath = Path.Combine(this.TestDir, template.Name);
            expectedPath = Path.ChangeExtension(expectedPath, ValidationTemplate.FileExtension);
            template = template.SaveAs(expectedPath);
            template.Dispose();
            template = null;

            //open
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            viewModel.OpenFromFileCommand.Execute(expectedPath);

            //check if file has opened correctly
            Assert.AreEqual("[Planogram.Height]", ((PlanogramValidationTemplate)viewModel.ValidationTemplateEdit).Groups[0].Metrics[0].Field);

        }

        [Test]
        public void OpenFromFileCommand_FileIsNotLeftLocked()
        {
            //setup the template
            ValidationTemplate template = ValidationTemplate.NewValidationTemplate(1);
            template.Name = Guid.NewGuid().ToString();
            ValidationTemplateGroup g1 = template.Groups.FirstOrDefault();
            if (g1 == null)
            {
                g1 = ValidationTemplateGroup.NewValidationTemplateGroup();
                template.Groups.Add(g1);
                g1.Name = "group1";
            }
            ValidationTemplateGroupMetric m1 = g1.Metrics.FirstOrDefault();
            if (m1 == null)
            {
                m1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
                g1.Metrics.Add(m1);
                m1.Field = "[Planogram.Height]";
            }

            //save it
            String expectedPath = Path.Combine(this.TestDir, template.Name);
            expectedPath = Path.ChangeExtension(expectedPath, ValidationTemplate.FileExtension);
            template = template.SaveAs(expectedPath);
            template.Dispose();
            template = null;

            //open
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            viewModel.OpenFromFileCommand.Execute(expectedPath);

            //check if file has opened correctly
            Assert.AreEqual("[Planogram.Height]", ((PlanogramValidationTemplate)viewModel.ValidationTemplateEdit).Groups[0].Metrics[0].Field);


            //fetch file again
            using (ValidationTemplate fetchedData = ValidationTemplate.FetchByFilename(expectedPath))
            {
                Assert.AreEqual("[Planogram.Height]", fetchedData.Groups[0].Metrics[0].Field);
            }
        }

        [Test]
        public void SaveAsCommand_EnabledWhenNoCreatePerm()
        {
            SetMockRepositoryConnection();
            ValidationTemplateInfoList infos = InsertTestRepositoryData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateCreate.ToString());

            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            viewModel.ValidationTemplateEdit.Name = "Test";

            Assert.IsTrue(viewModel.SaveAsCommand.CanExecute());
        }

        [Test]
        public void SaveAsToRepositoryCommand_DisabledWhenNoRepositioryConnection()
        {
            App.ViewState.ClearRepositoryConnection();

            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            viewModel.ValidationTemplateEdit.Name = "Test";

            Assert.IsFalse(viewModel.SaveToRepositoryCommand.CanExecute());
        }

        [Test]
        public void SaveAsToRepositoryCommand_DisabledWhenNoCreatePerm()
        {
            SetMockRepositoryConnection();
            ValidationTemplateInfoList infos = InsertTestRepositoryData();

            //remove the permission from the user.
            DomainPrincipal domainPrincipal = Csla.ApplicationContext.User as DomainPrincipal;
            DomainIdentity domainIdentity = domainPrincipal.Identity as DomainIdentity;
            domainIdentity.Roles.Remove(DomainPermission.ValidationTemplateCreate.ToString());

            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);
            viewModel.ValidationTemplateEdit.Name = "Test";

            Assert.IsFalse(viewModel.SaveToRepositoryCommand.CanExecute());
        }

        [Test]
        public void SaveAsToRespositoryCommand_Execution()
        {
            SetMockRepositoryConnection();
            Int32 entityId = App.ViewState.EntityId;

            //start with new template
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);

            //setup the template
            PlanogramValidationTemplate template = (PlanogramValidationTemplate)viewModel.ValidationTemplateEdit;
            PlanogramValidationTemplateGroup g1 = template.Groups.FirstOrDefault();
            if (g1 == null)
            {
                g1 = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
                template.Groups.Add(g1);
                g1.Name = "group1";
            }
            PlanogramValidationTemplateGroupMetric m1 = g1.Metrics.FirstOrDefault();
            if (m1 == null)
            {
                m1 = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
                g1.Metrics.Add(m1);
                m1.Field = "[Planogram.Height]";
            }

            //save
            viewModel.SaveToRepositoryCommand.Execute();

            //fetch back
            ValidationTemplateInfoList templates = ValidationTemplateInfoList.FetchByEntityId(entityId);
            ValidationTemplate fetchedData = ValidationTemplate.FetchById(templates[0].Id);
            Assert.AreEqual("[Planogram.Height]", fetchedData.Groups[0].Metrics[0].Field);
        }

        [Test]
        public void SaveAsFileCommand_Execution()
        {
            //start with new template
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);

            //setup the template
            PlanogramValidationTemplate template = (PlanogramValidationTemplate)viewModel.ValidationTemplateEdit;
            PlanogramValidationTemplateGroup g1 = template.Groups.FirstOrDefault();
            if (g1 == null)
            {
                g1 = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
                template.Groups.Add(g1);
                g1.Name = "group1";
            }
            PlanogramValidationTemplateGroupMetric m1 = g1.Metrics.FirstOrDefault();
            if (m1 == null)
            {
                m1 = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
                g1.Metrics.Add(m1);
                m1.Field = "[Planogram.Height]";
            }

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, ValidationTemplate.FileExtension);
            viewModel.SaveToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //fetch file
            using (ValidationTemplate fetchedData = ValidationTemplate.FetchByFilename(expectedPath))
            {
                Assert.AreEqual("[Planogram.Height]", fetchedData.Groups[0].Metrics[0].Field);
            }
        }

        [Test]
        public void SaveAsFileCommand_OverwriteExisting()
        {
            //start with new template
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);

            //setup the template
            PlanogramValidationTemplate template = (PlanogramValidationTemplate)viewModel.ValidationTemplateEdit;
            PlanogramValidationTemplateGroup g1 = template.Groups.FirstOrDefault();
            if (g1 == null)
            {
                g1 = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
                template.Groups.Add(g1);
                g1.Name = "group1";
            }
            PlanogramValidationTemplateGroupMetric m1 = g1.Metrics.FirstOrDefault();
            if (m1 == null)
            {
                m1 = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
                g1.Metrics.Add(m1);
                m1.Field = "[Planogram.Height]";
            }

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, ValidationTemplate.FileExtension);
            viewModel.SaveToFileCommand.Execute(expectedPath);

            //check file exists
            Assert.IsTrue(System.IO.File.Exists(expectedPath));

            //make a change and save to it again
            template = (PlanogramValidationTemplate)viewModel.ValidationTemplateEdit;
            template.Groups[0].Metrics[0].Field = "[Planogram.Width]";

            viewModel.SaveToFileCommand.Execute(expectedPath);

            //fetch file
            using (ValidationTemplate fetchedData = ValidationTemplate.FetchByFilename(expectedPath))
            {
                Assert.AreEqual("[Planogram.Width]", fetchedData.Groups[0].Metrics[0].Field);
            }

        }

        [Test]
        public void SaveAsFileCommand_DeleteMetric()
        {
            //start with new template
            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);

            //setup the template
            PlanogramValidationTemplate template = (PlanogramValidationTemplate)viewModel.ValidationTemplateEdit;
            PlanogramValidationTemplateGroup g1 = template.Groups.FirstOrDefault();
            if (g1 == null)
            {
                g1 = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
                template.Groups.Add(g1);
                g1.Name = "group1";
            }
            PlanogramValidationTemplateGroupMetric m1 = g1.Metrics.FirstOrDefault();
            if (m1 == null)
            {
                m1 = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
                g1.Metrics.Add(m1);
                m1.Field = "[Planogram.Height]";
            }

            //save
            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, ValidationTemplate.FileExtension);
            viewModel.SaveToFileCommand.Execute(expectedPath);

            //delete the metric
            viewModel.DeleteGroupMetric
                (viewModel.ValidationTemplateEdit.Groups.First(), viewModel.ValidationTemplateEdit.Groups.First().Metrics[0]);

            //save again
            viewModel.SaveToFileCommand.Execute(expectedPath);

            //open again
            using (ValidationTemplate fetchedData = ValidationTemplate.FetchByFilename(expectedPath))
            {
                Assert.AreEqual(5, fetchedData.Groups.Count, "groups should not have been deleted");
            }


        }

        [Test]
        public void OkCommand_CurrentValidationTemplateNotDirtyCannotExecute()
        {
            throw new InconclusiveException("TODO");


            //const string expectation =
            //    "The Ok Command, when the current validation template is not dirty, should not be able to be excuted.";
            //var validValidationTemplate = GetValidationTemplate();

            //var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView)
            //{
            //    ValidationTemplateEdit = validValidationTemplate
            //};

            //Assert.IsFalse(viewModel.OkCommand.CanExecute(), expectation);
        }

        [Test]
        public void OkCommand_CurrentValidationTemplateWithoutGroupsCannotExecute()
        {
            throw new InconclusiveException("TODO");

            //const string expectation = "The Ok Command, when the current validation template has no groups, should NOT be able to be executed.";
            //var validationTemplateWithoutGroups = GetValidationTemplate(groupList: GetGroupList(validGroups: 0));

            //var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView)
            //{
            //    ValidationTemplateEdit = validationTemplateWithoutGroups
            //};

            //Assert.IsFalse(viewModel.OkCommand.CanExecute(), expectation);
        }

        [Test]
        public void OkCommand_CurrentValidationTemplateGroupWithoutMetricsCannotExecute()
        {
            throw new InconclusiveException("TODO");

            //const string expectation = "The Ok Command, when the current validation template has groups without metrics, should NOT be able to be executed.";
            //var validationTemplateWithoutGroups = GetValidationTemplate(groupList: GetGroupList(invalidGroups: 1));

            //var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView)
            //{
            //    ValidationTemplateEdit = validationTemplateWithoutGroups
            //};

            //Assert.IsFalse(viewModel.OkCommand.CanExecute(), expectation);
        }


        [Test]
        public void CancelCommand_AlwaysCanExecute()
        {
            const string expectation = "The Cancel Command, always, should be able to be excuted.";

            var viewModel = new ValidationTemplateEditorViewModel(_validPlanogramView);

            Assert.IsTrue(viewModel.CancelCommand.CanExecute(), expectation);
        }

        #endregion

        #region Other tests

        [Test]
        public void Other_SavingToFileNotLoadingTemplateCorrectly()
        {
            //GEM27346

            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;

            ValidationTemplate template = ValidationTemplate.NewValidationTemplate(entityId);
            template.Name = "Test Template";

            ValidationTemplateGroup group1 = ValidationTemplateGroup.NewValidationTemplateGroup();
            template.Groups.Add(group1);
            group1.Name = "Group1";
            group1.Threshold1 = 2;
            group1.Threshold2 = 1;

            ObjectFieldInfo field = ObjectFieldInfo.NewObjectFieldInfo(typeof(Product), "Product", Product.CostPriceProperty);

            ValidationTemplateGroupMetric metric1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            metric1.Field = field.FieldPlaceholder;
            metric1.Score1 = 3;
            metric1.Score2 = 2;
            metric1.Score3 = 1;
            metric1.Threshold1 = 2;
            metric1.Threshold2 = 1;
            metric1.AggregationType = PlanogramValidationAggregationType.Max;
            group1.Metrics.Add(metric1);

            template = template.Save();


            //create a package and set a validation template
            Package package = TestDataHelper.CreatePlanCreweSparklingWater();
            package.Name = Guid.NewGuid().ToString();

            String planPath = Path.Combine(this.TestDir, package.Name);
            planPath = Path.ChangeExtension(planPath, PackageViewModel.FileSystemExtension);

            Planogram plan = package.Planograms.First();
            plan.ValidationTemplate.LoadFrom(template);

            package = package.SaveAs(DomainPrincipal.CurrentUserId, planPath);
            package.Dispose();

            //refetch the plan
            PackageViewModel viewModel = PackageViewModel.FetchPackageByFileName(planPath);

            //load in the validation screen viewmodel
            ValidationTemplateEditorViewModel screenViewModel = new ValidationTemplateEditorViewModel(viewModel.PlanogramViews[0]);

            Assert.AreEqual(screenViewModel.ValidationTemplateEdit.Groups.Count(), 5, "Groups not loaded"); // should always have 5 groups.
            Assert.AreEqual(screenViewModel.ValidationTemplateEdit.Groups.First().Metrics.Count(), 1, "Metrics not loaded");

            var viewGroup = screenViewModel.ValidationTemplateEdit.Groups.First();
            Assert.AreEqual(group1.Name, viewGroup.Name);
            Assert.AreEqual(group1.Threshold1, viewGroup.Threshold1);
            Assert.AreEqual(group1.Threshold2, viewGroup.Threshold2);

            var viewMetric = viewGroup.Metrics.First();
            metric1.Field = field.FieldPlaceholder;
            Assert.AreEqual(3, metric1.Score1);
            Assert.AreEqual (2, metric1.Score2);
            Assert.AreEqual(1, metric1.Score3);
            Assert.AreEqual(2, metric1.Threshold1);
            Assert.AreEqual(1, metric1.Threshold2);
            Assert.AreEqual(PlanogramValidationAggregationType.Max, metric1.AggregationType);

        }

        #endregion

    }
}