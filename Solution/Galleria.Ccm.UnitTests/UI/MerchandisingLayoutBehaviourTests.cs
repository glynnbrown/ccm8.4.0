﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#region Version History: (CCM 8.0.2)
// V8-29023 : M.Pettit
//  Added Merchandisable Depth tests for Pegboard/Rod types
#endregion
#region Version History : CCM810
// V8-30085 : A.Kuszyk
//  Fixed some failing tests.
#endregion
#region Version History: CCM811
// V8-28856 : A.Silva
//  Removed obsolete Unit Tests having to do with GetFillablespace, FillHigh, FillWide and FillHigh.
#endregion
#region Version History: CCM820
// V8-31233 : A.Probyn
//  Slightly updated expected values on X position rounding due to change in value rounding.
#endregion
#region Version History: CCM830
// V8-31858 : J.Pickup
//  Added autofil test: AutoFillHigh_AppliesNoTopCapsWhenNoSpaceAfterMainPositionHeightIsSqueezed
// V8-31804 : A.Kuszyk
//  Added shouldCalculateMetadata to PLanogramView constructors to disable metadata calculations (improves test performance).
//  Also added SuppressBackgroundProcessing flag to prevent background processing during test setup.
//  Note: any future tests might have a performance benefit from making use of this flag.
// V8-31385 : A.Silva
//  Added AutoFillWide_BarUsesOverhang To ensure that overhang is used on bars despite positions being withou pegs.
// V8-31981 : L.Ineson
//  Updated tests after changing the y placment of hanging products when the pegy is 0.
// V8-32267 : L.Ineson
//  Added tests for new peg offset x and peg offset y functionality.
// V8-32893 : L.Ineson
//  Added test for Bar above rod fill high scenario.
// V8-18307 : J.Pickup
//  Adjustments to tests to take into account new rule around sequencing (50%). 
//  This results in positions belonging to different components and also their position on the component also to have changed for the test values.
#endregion

#endregion

using FluentAssertions;
using Galleria.Ccm.Common.Wpf.Converters;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace Galleria.Ccm.UnitTests.UI
{
    /// <summary>
    ///     Tests the different merchandising layout rules that the editor must adhere to.
    /// </summary>
    [TestFixture]
    [Category(Categories.Smoke)]
    public sealed class MerchandisingLayoutBehaviourTests : TestBase
    {
        #region Test Fixture Helpers

        private static Planogram CreateShelfTestPlan()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);

            shelf1Fc.Y = 105;

            PlanogramSubComponent shelfSub = shelf1C.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            shelfSub.IsProductOverlapAllowed = true;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelfSub,
                fixtureItem,
                shelf1Fc);

            //Product 1 (w: 8.2, h: 32.2, d: 73.8)
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 9;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 2.7F;
            pos1.TotalUnits = 9;
            pos1.Sequence = 1;

            //Product 2- (w: 17, h: 32, d: 76.5)
            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 32F;
            prod2.Width = 8.5F;
            prod2.Depth = 8.5F;
            plan.Products.Add(prod2);

            PlanogramPosition pos2 = subComponentPlacement.AddPosition(prod2);
            pos2.FacingsHigh = 1;
            pos2.FacingsWide = 2;
            pos2.FacingsDeep = 9;
            pos2.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos2.OrientationType = PlanogramPositionOrientationType.Front0;
            pos2.X = 17.37F;
            pos2.Y = 4;
            pos2.Z = 0;
            pos2.TotalUnits = 9;
            pos2.Sequence = 2;

            //Product 3 - (w: 25, h:23.2 , d:39 )
            PlanogramProduct prod3 = PlanogramProduct.NewPlanogramProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 23F;
            prod3.Width = 24.5F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 23.2F;
            prod3.TrayWidth = 25;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 1;
            prod3.TrayDeep = 3;
            plan.Products.Add(prod3);

            PlanogramPosition pos3 = subComponentPlacement.AddPosition(prod3);
            pos3.FacingsHigh = 1;
            pos3.FacingsWide = 1;
            pos3.FacingsDeep = 1;
            pos3.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos3.OrientationType = PlanogramPositionOrientationType.Front0;
            pos3.X = 44.05F;
            pos3.Y = 4;
            pos3.Z = 37.5F;
            pos3.TotalUnits = 1;
            pos3.Sequence = 3;

            //Product 4 - (w: 8.5, h: 30.8, d: 76.5)
            PlanogramProduct prod4 = PlanogramProduct.NewPlanogramProduct();
            prod4.Gtin = "p4";
            prod4.Name = "p4";
            prod4.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod4.Height = 30.8F;
            prod4.Width = 8.5F;
            prod4.Depth = 8.5F;
            plan.Products.Add(prod4);

            PlanogramPosition pos4 = subComponentPlacement.AddPosition(prod4);
            pos4.FacingsHigh = 1;
            pos4.FacingsWide = 1;
            pos4.FacingsDeep = 9;
            pos4.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos4.OrientationType = PlanogramPositionOrientationType.Front0;
            pos4.X = 77.92F;
            pos4.Y = 4;
            pos4.Z = 0F;
            pos4.TotalUnits = 9;
            pos4.Sequence = 4;

            //Product 5 - (w: 25, h: 30.9, d: 34)
            PlanogramProduct prod5 = PlanogramProduct.NewPlanogramProduct();
            prod5.Gtin = "p5";
            prod5.Name = "p5";
            prod5.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod5.Height = 30.9F;
            prod5.Width = 25F;
            prod5.Depth = 17F;
            plan.Products.Add(prod5);

            PlanogramPosition pos5 = subComponentPlacement.AddPosition(prod5);
            pos5.FacingsHigh = 1;
            pos5.FacingsWide = 1;
            pos5.FacingsDeep = 2;
            pos5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos5.OrientationType = PlanogramPositionOrientationType.Front0;
            pos5.X = 95F;
            pos5.Y = 4;
            pos5.Z = 42.5F;
            pos5.TotalUnits = 2;
            pos5.Sequence = 5;

            //Int16 seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.X).GroupBy(p => p.X))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceX = seq;
            //    }
            //    seq++;
            //}

            //seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.Y).GroupBy(p => p.Y))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceY = seq;
            //    }
            //    seq++;
            //}

            //seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.Z).GroupBy(p => p.Z))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceZ = seq;
            //    }
            //    seq++;
            //}

            //output plan if required
            //Package pk = Package.NewPackage(1, PackageLockType.User);
            //pk.Name = "ShelfTest";
            //pk.Planograms.Add(plan);
            //pk.SaveAs(1, @"C:\Users\usr095\shelfTest.pog");
            //pk.Dispose();

            return plan;
        }

        private static Package CreatePegTestPlan()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a pegboard
            PlanogramFixtureComponent peg1Fc =
                fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 100, 1F);
            PlanogramComponent peg1C = plan.Components.FindById(peg1Fc.PlanogramComponentId);

            peg1Fc.Y = 50;

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            pegSub.IsProductOverlapAllowed = true;
            pegSub.MerchConstraintRow1StartX = 4;
            pegSub.MerchConstraintRow1SpacingX = 4;
            pegSub.MerchConstraintRow1StartY = 4;
            pegSub.MerchConstraintRow1SpacingY = 4;
            pegSub.MerchConstraintRow1Height = 1;
            pegSub.MerchConstraintRow1Width = 1;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                pegSub,
                fixtureItem,
                peg1Fc);

            //Product 1 (w: 8.2, h: 32.2, d: 24.6)
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 3;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 3.9F;
            pos1.Y = 65.3F;
            pos1.Z = 1F;
            pos1.Sequence = 1;

            //Product 2- (w: 17, h: 32, d: 17)
            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 32F;
            prod2.Width = 8.5F;
            prod2.Depth = 8.5F;
            plan.Products.Add(prod2);

            PlanogramPosition pos2 = subComponentPlacement.AddPosition(prod2);
            pos2.FacingsHigh = 1;
            pos2.FacingsWide = 2;
            pos2.FacingsDeep = 2;
            pos2.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos2.OrientationType = PlanogramPositionOrientationType.Front0;
            pos2.X = 19.75F;
            pos2.Y = 65.5F;
            pos2.Z = 1;
            pos2.Sequence = 2;

            //Product 3 - (w: 25, h:23.2 , d:12 )
            PlanogramProduct prod3 = PlanogramProduct.NewPlanogramProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 23F;
            prod3.Width = 24.5F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 23.2F;
            prod3.TrayWidth = 25;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 1;
            prod3.TrayDeep = 3;
            plan.Products.Add(prod3);

            PlanogramPosition pos3 = subComponentPlacement.AddPosition(prod3);
            pos3.FacingsHigh = 1;
            pos3.FacingsWide = 1;
            pos3.FacingsDeep = 1;
            pos3.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            pos3.OrientationType = PlanogramPositionOrientationType.Front0;
            pos3.X = 47.75F;
            pos3.Y = 74.5F;
            pos3.Z = 1F;
            pos3.Sequence = 3;

            //Product 4 - (w: 8.5, h: 30.8, d: 17)
            PlanogramProduct prod4 = PlanogramProduct.NewPlanogramProduct();
            prod4.Gtin = "p4";
            prod4.Name = "p4";
            prod4.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod4.Height = 30.8F;
            prod4.Width = 8.5F;
            prod4.Depth = 8.5F;
            plan.Products.Add(prod4);

            PlanogramPosition pos4 = subComponentPlacement.AddPosition(prod4);
            pos4.FacingsHigh = 1;
            pos4.FacingsWide = 1;
            pos4.FacingsDeep = 2;
            pos4.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos4.OrientationType = PlanogramPositionOrientationType.Front0;
            pos4.X = 79.75F;
            pos4.Y = 66.7F;
            pos4.Z = 1F;
            pos4.Sequence = 4;

            //Product 5 - (w: 25, h: 30.9, d: 34)
            PlanogramProduct prod5 = PlanogramProduct.NewPlanogramProduct();
            prod5.Gtin = "p5";
            prod5.Name = "p5";
            prod5.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod5.Height = 30.9F;
            prod5.Width = 25F;
            prod5.Depth = 17F;
            plan.Products.Add(prod5);

            PlanogramPosition pos5 = subComponentPlacement.AddPosition(prod5);
            pos5.FacingsHigh = 1;
            pos5.FacingsWide = 1;
            pos5.FacingsDeep = 2;
            pos5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos5.OrientationType = PlanogramPositionOrientationType.Front0;
            pos5.X = 95.5F;
            pos5.Y = 66.6F;
            pos5.Z = 1F;
            pos5.Sequence = 5;


            //Int16 seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.X).GroupBy(p => p.X))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceX = seq;
            //    }
            //    seq++;
            //}

            //seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.Y).GroupBy(p => p.Y))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceY = seq;
            //    }
            //    seq++;
            //}

            //seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.Z).GroupBy(p => p.Z))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceZ = seq;
            //    }
            //    seq++;
            //}

            //output plan if required
            //Package pk = Package.NewPackage(1, PackageLockType.User);
            //pk.Name = "PegTest";
            //pk.Planograms.Add(plan);
            //pk.SaveAs(1, @"C:\Users\usr095\pegTest.pog");
            //pk.Dispose();

            return package;
        }

        private static PlanogramView CreateEmptyPlan()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            AddDefaultProducts(plan);

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(package);
            return pkView.PlanogramViews.First();
        }

        private static void AddDefaultProducts(Planogram plan)
        {
            //Product 1 (w: 8.2, h: 32.2, d: 73.8)
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            plan.Products.Add(prod1);

            //Product 2- (w: 17, h: 32, d: 76.5)
            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 32F;
            prod2.Width = 8.5F;
            prod2.Depth = 8.5F;
            plan.Products.Add(prod2);

            //Product 3 - (w: 25, h:23.2 , d:39 )
            PlanogramProduct prod3 = PlanogramProduct.NewPlanogramProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 23F;
            prod3.Width = 24.5F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 23.2F;
            prod3.TrayWidth = 25;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 1;
            prod3.TrayDeep = 3;
            plan.Products.Add(prod3);

            //Product 4 - (w: 8.5, h: 30.8, d: 76.5)
            PlanogramProduct prod4 = PlanogramProduct.NewPlanogramProduct();
            prod4.Gtin = "p4";
            prod4.Name = "p4";
            prod4.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod4.Height = 30.8F;
            prod4.Width = 8.5F;
            prod4.Depth = 8.5F;
            plan.Products.Add(prod4);

            //Product 5 - (w: 25, h: 30.9, d: 34)
            PlanogramProduct prod5 = PlanogramProduct.NewPlanogramProduct();
            prod5.Gtin = "p5";
            prod5.Name = "p5";
            prod5.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod5.Height = 30.9F;
            prod5.Width = 25F;
            prod5.Depth = 17F;
            plan.Products.Add(prod5);
        }

        [DebuggerStepThrough]
        private void AssertPlacement(Single x, Single y, Single z, PlanogramPositionView pos)
        {
            if (!x.EqualTo(pos.X) ||
                !y.EqualTo(pos.Y) ||
                !z.EqualTo(pos.Z))
                Assert.Fail("Expected ({0},{1}, {2}) but got ({3},{4},{5})", x, y, z, pos.X, pos.Y, pos.Z);
        }

        private void AssertWorldPlacement(Single x, Single y, Single z, PlanogramPositionView pos)
        {
            PointValue posWorld = pos.Model.GetPlanogramRelativeCoordinates(pos.GetParentSubComponentPlacement());

            if (!x.EqualTo(posWorld.X) ||
                !y.EqualTo(posWorld.Y) ||
                !z.EqualTo(posWorld.Z))
                Assert.Fail("Expected ({0},{1}, {2}) but got ({3},{4},{5})", x, y, z, posWorld.X, posWorld.Y, posWorld.Z);
        }


        [DebuggerStepThrough]
        private void AssertPlacement(Single x, Single y, Single z, PlanogramSubComponentDivider div)
        {
            if (!x.EqualTo(div.X) ||
                !y.EqualTo(div.Y) ||
                !z.EqualTo(div.Z))
                Assert.Fail("Expected ({0},{1}, {2}) but got ({3},{4},{5})", x, y, z, div.X, div.Y, div.Z);
        }

        [DebuggerStepThrough]
        private void AssertSequence(Int16 x, Int16 y, Int16 z, PlanogramPositionView pos)
        {
            Assert.AreEqual(x, pos.SequenceX, "Sequence X incorrect");
            Assert.AreEqual(y, pos.SequenceY, "Sequence Y incorrect");
            Assert.AreEqual(z, pos.SequenceZ, "Sequence Z incorrect");
        }

        private IEnumerable<PlanogramSubComponentXMerchStrategyType> XPlacementStrategies
        {
            get { return Enum.GetValues(typeof(PlanogramSubComponentXMerchStrategyType)).Cast<PlanogramSubComponentXMerchStrategyType>(); }
        }

        private IEnumerable<PlanogramSubComponentYMerchStrategyType> YPlacementStrategies
        {
            get { return Enum.GetValues(typeof(PlanogramSubComponentYMerchStrategyType)).Cast<PlanogramSubComponentYMerchStrategyType>(); }
        }

        private IEnumerable<PlanogramSubComponentZMerchStrategyType> ZPlacementStrategies
        {
            get { return Enum.GetValues(typeof(PlanogramSubComponentZMerchStrategyType)).Cast<PlanogramSubComponentZMerchStrategyType>(); }
        }

        #endregion

        #region Sequence Tests

        [Test]
        public void Sequence_ManualLoadSequence()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];


            AssertSequence(1, 1, 2, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 3, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 4, p5);

            Assert.AreEqual(1, p1.Sequence);
            Assert.AreEqual(2, p2.Sequence);
            Assert.AreEqual(3, p3.Sequence);
            Assert.AreEqual(4, p4.Sequence);
            Assert.AreEqual(5, p5.Sequence);
        }

        [Test]
        public void Sequence_SingleMerchShelf()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //Manual to stacked
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;

            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(4, p4.SequenceX);
            Assert.AreEqual(5, p5.SequenceX);


            //set y to bottom stacked
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(4, p4.SequenceX);
            Assert.AreEqual(5, p5.SequenceX);


            Assert.AreEqual(1, p1.SequenceY);
            Assert.AreEqual(1, p2.SequenceY);
            Assert.AreEqual(1, p3.SequenceY);
            Assert.AreEqual(1, p4.SequenceY);
            Assert.AreEqual(1, p5.SequenceY);

            //stack 2 on top of 1
            shelf.MovePosition(p2, p1, PlanogramPositionAnchorDirection.Above);

            //now set y to bottom
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;

            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(4, p4.SequenceX);
            Assert.AreEqual(5, p5.SequenceX);

            Assert.AreEqual(1, p1.SequenceY);
            Assert.AreEqual(1, p2.SequenceY);
            Assert.AreEqual(1, p3.SequenceY);
            Assert.AreEqual(1, p4.SequenceY);
            Assert.AreEqual(1, p5.SequenceY);
        }

        [Test]
        public void Sequence_ShelfAbove()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);

            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p1 above p2
            shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Above);

            AssertSequence(1, 2, 1, p1);
            AssertSequence(1, 1, 1, p2);
            AssertSequence(2, 1, 1, p3);
            AssertSequence(3, 1, 1, p4);
            AssertSequence(4, 1, 1, p5);


            //check position
            AssertPlacement(0, 36, 2.7F, p1);
            AssertPlacement(0F, 4, 0F, p2);
            AssertPlacement(17F, 4, 37.5F, p3);
            AssertPlacement(42F, 4, 0F, p4);
            AssertPlacement(50.5F, 4, 42.5F, p5);
        }

        [Test]
        public void Sequence_ShelfBelow()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);

            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p1 below p2
            shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Below);

            AssertSequence(1, 1, 1, p1);
            AssertSequence(1, 2, 1, p2);
            AssertSequence(2, 1, 1, p3);
            AssertSequence(3, 1, 1, p4);
            AssertSequence(4, 1, 1, p5);

            //check position
            AssertPlacement(0, 4, 2.7F, p1);
            AssertPlacement(0F, 36.2F, 0F, p2);
            AssertPlacement(17F, 4, 37.5F, p3);
            AssertPlacement(42F, 4, 0F, p4);
            AssertPlacement(50.5F, 4, 42.5F, p5);
        }

        [Test]
        public void Sequence_ShelfToRight()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);

            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p5 to right of p1.
            shelf.MovePosition(p5, p1, PlanogramPositionAnchorDirection.ToRight);

            AssertSequence(1, 1, 1, p1);
            AssertSequence(3, 1, 1, p2);
            AssertSequence(4, 1, 1, p3);
            AssertSequence(5, 1, 1, p4);
            AssertSequence(2, 1, 1, p5);


            AssertPlacement(0, 4, 2.7F, p1);
            AssertPlacement(33.2F, 4, 0F, p2);
            AssertPlacement(50.2F, 4, 37.5F, p3);
            AssertPlacement(75.2F, 4, 0F, p4);
            AssertPlacement(8.2F, 4, 42.5F, p5);


            //move p5 back to after 4
            shelf.MovePosition(p5, p4, PlanogramPositionAnchorDirection.ToRight);

            //put p1 on top of p3
            shelf.MovePosition(p1, p3, PlanogramPositionAnchorDirection.Above);

            //now put p5 to the right of p2
            shelf.MovePosition(p5, p2, PlanogramPositionAnchorDirection.ToRight);

            //check sequence
            AssertSequence(3, 2, 1, p1);
            AssertSequence(1, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(2, 1, 1, p5);
        }

        [Test]
        public void Sequence_ShelfToLeft()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);

            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p5 to left of p1.
            shelf.MovePosition(p5, p1, PlanogramPositionAnchorDirection.ToLeft);

            AssertSequence(2, 1, 1, p1);
            AssertSequence(3, 1, 1, p2);
            AssertSequence(4, 1, 1, p3);
            AssertSequence(5, 1, 1, p4);
            AssertSequence(1, 1, 1, p5);
        }

        [Test]
        public void Sequence_ShelfInFront()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);

            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p1 infront of p2
            shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.InFront);

            AssertSequence(1, 1, 2, p1);
            AssertSequence(1, 1, 1, p2);
            AssertSequence(2, 1, 1, p3);
            AssertSequence(3, 1, 1, p4);
            AssertSequence(4, 1, 1, p5);
        }

        [Test]
        public void Sequence_ShelfBehind()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);

            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p1 behind p2
            shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Behind);

            AssertSequence(1, 1, 1, p1);
            AssertSequence(1, 1, 2, p2);
            AssertSequence(2, 1, 1, p3);
            AssertSequence(3, 1, 1, p4);
            AssertSequence(4, 1, 1, p5);
        }

        [Test]
        public void Sequence_ShelfAboveThenToLeft()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);

            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p1 above p2
            shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Above);

            AssertSequence(1, 2, 1, p1);
            AssertSequence(1, 1, 1, p2);
            AssertSequence(2, 1, 1, p3);
            AssertSequence(3, 1, 1, p4);
            AssertSequence(4, 1, 1, p5);


            //now move p5 to the left of p2
            shelf.MovePosition(p5, p2, PlanogramPositionAnchorDirection.ToLeft);

            AssertSequence(2, 2, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(1, 1, 1, p5);
        }


        [Test]
        public void Sequence_PegboardAbove()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set the pegboard strategies
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

            //check sequence
            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p1 above p2
            pegboard.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Above);

            //check sequence
            AssertSequence(1, 2, 1, p1);
            AssertSequence(1, 1, 1, p2);
            AssertSequence(2, 1, 1, p3);
            AssertSequence(3, 1, 1, p4);
            AssertSequence(4, 1, 1, p5);
        }

        [Test]
        public void Sequence_PegboardBelow()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set the pegboard strategies
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

            //check sequence
            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p1 below p2
            pegboard.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Below);

            //check sequences
            AssertSequence(1, 1, 1, p1);
            AssertSequence(1, 2, 1, p2);
            AssertSequence(2, 2, 1, p3);
            AssertSequence(3, 2, 1, p4);
            AssertSequence(4, 2, 1, p5);
        }

        [Test]
        public void Sequence_PegboardToRight()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set the pegboard strategies
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

            //check sequence
            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p5 to right of p1.
            pegboard.MovePosition(p5, p1, PlanogramPositionAnchorDirection.ToRight);

            AssertSequence(1, 1, 1, p1);
            AssertSequence(3, 1, 1, p2);
            AssertSequence(4, 1, 1, p3);
            AssertSequence(5, 1, 1, p4);
            AssertSequence(2, 1, 1, p5);

            //move p5 back to after 4
            pegboard.MovePosition(p5, p4, PlanogramPositionAnchorDirection.ToRight);

            //put p1 on below of p3
            pegboard.MovePosition(p1, p3, PlanogramPositionAnchorDirection.Below);

            //check sequence
            AssertSequence(1, 1, 1, p1);
            AssertSequence(1, 2, 1, p2);
            AssertSequence(2, 2, 1, p3);
            AssertSequence(3, 2, 1, p4);
            AssertSequence(4, 2, 1, p5);

            //now put p5 to the right of p2
            pegboard.MovePosition(p5, p2, PlanogramPositionAnchorDirection.ToRight);

            //check sequence
            AssertSequence(1, 1, 1, p1);
            AssertSequence(1, 2, 1, p2);
            AssertSequence(3, 2, 1, p3);
            AssertSequence(4, 2, 1, p4);
            AssertSequence(2, 2, 1, p5);
        }

        [Test]
        public void Sequence_PegboardToLeft()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set the pegboard strategies
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

            //check sequence
            AssertSequence(1, 1, 1, p1);
            AssertSequence(2, 1, 1, p2);
            AssertSequence(3, 1, 1, p3);
            AssertSequence(4, 1, 1, p4);
            AssertSequence(5, 1, 1, p5);

            //move p5 to left of p1.
            pegboard.MovePosition(p5, p1, PlanogramPositionAnchorDirection.ToLeft);

            AssertSequence(2, 1, 1, p1);
            AssertSequence(3, 1, 1, p2);
            AssertSequence(4, 1, 1, p3);
            AssertSequence(5, 1, 1, p4);
            AssertSequence(1, 1, 1, p5);
        }

        #endregion

        #region Shelf Tests

        [Test, TestCaseSource("XPlacementStrategies")]
        public void Shelf_MerchX(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];
            planView.BeginUpdate();

            //set shelf value
            shelf.MerchandisingStrategyX = xStrategy;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            planView.EndUpdate();
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            switch (xStrategy)
            {
                default:
                    throw new NotImplementedException();

                #region Left Stacked

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    {
                        //check sequence
                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(2, p2.SequenceX);
                        Assert.AreEqual(3, p3.SequenceX);
                        Assert.AreEqual(4, p4.SequenceX);
                        Assert.AreEqual(5, p5.SequenceX);

                        //check positions
                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(8.2F, 4, 0F, p2);
                        AssertPlacement(25.2F, 4, 37.5F, p3);
                        AssertPlacement(50.2F, 4, 0F, p4);
                        AssertPlacement(58.7F, 4, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Left

                case PlanogramSubComponentXMerchStrategyType.Left:
                    {
                        //check sequence
                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(1, p2.SequenceX);
                        Assert.AreEqual(1, p3.SequenceX);
                        Assert.AreEqual(1, p4.SequenceX);
                        Assert.AreEqual(1, p5.SequenceX);

                        //check positions
                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(0, 4, 0F, p2);
                        AssertPlacement(0, 4, 37.5F, p3);
                        AssertPlacement(0, 4, 0F, p4);
                        AssertPlacement(0, 4, 42.5F, p5);

                        //set back to stacked to check it goes back.
                        shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;

                        //check sequence
                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(2, p2.SequenceX);
                        Assert.AreEqual(3, p3.SequenceX);
                        Assert.AreEqual(4, p4.SequenceX);
                        Assert.AreEqual(5, p5.SequenceX);

                        //check positions
                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(8.2F, 4, 0F, p2);
                        AssertPlacement(25.2F, 4, 37.5F, p3);
                        AssertPlacement(50.2F, 4, 0F, p4);
                        AssertPlacement(58.7F, 4, 42.5F, p5);

                        //and back again
                        shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;

                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(1, p2.SequenceX);
                        Assert.AreEqual(1, p3.SequenceX);
                        Assert.AreEqual(1, p4.SequenceX);
                        Assert.AreEqual(1, p5.SequenceX);

                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(0, 4, 0F, p2);
                        AssertPlacement(0, 4, 37.5F, p3);
                        AssertPlacement(0, 4, 0F, p4);
                        AssertPlacement(0, 4, 42.5F, p5);
                    }
                    break;

                #endregion

                #region RightStacked

                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        //check sequence
                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(2, p2.SequenceX);
                        Assert.AreEqual(3, p3.SequenceX);
                        Assert.AreEqual(4, p4.SequenceX);
                        Assert.AreEqual(5, p5.SequenceX);

                        //check positions
                        AssertPlacement(36.3F, 4F, 2.7F, p1);
                        AssertPlacement(44.5F, 4F, 0F, p2);
                        AssertPlacement(61.5F, 4F, 37.5F, p3);
                        AssertPlacement(86.5F, 4F, 0F, p4);
                        AssertPlacement(95F, 4F, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Right

                case PlanogramSubComponentXMerchStrategyType.Right:
                    {
                        //check sequence
                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(1, p2.SequenceX);
                        Assert.AreEqual(1, p3.SequenceX);
                        Assert.AreEqual(1, p4.SequenceX);
                        Assert.AreEqual(1, p5.SequenceX);

                        //check positions
                        AssertPlacement(111.8F, 4F, 2.7F, p1);
                        AssertPlacement(103F, 4F, 0F, p2);
                        AssertPlacement(95F, 4F, 37.5F, p3);
                        AssertPlacement(111.5F, 4F, 0F, p4);
                        AssertPlacement(95F, 4F, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramSubComponentXMerchStrategyType.Manual:
                    {
                        //ensure settings
                        shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                        shelf.IsProductOverlapAllowed = true;

                        //check sequence
                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(2, p2.SequenceX);
                        Assert.AreEqual(3, p3.SequenceX);
                        Assert.AreEqual(4, p4.SequenceX);
                        Assert.AreEqual(5, p5.SequenceX);

                        //check positions
                        AssertPlacement(0F, 4F, 2.7F, p1);
                        AssertPlacement(17.37F, 4F, 0F, p2);
                        AssertPlacement(44.05F, 4F, 37.5F, p3);
                        AssertPlacement(77.92F, 4F, 0F, p4);
                        AssertPlacement(95F, 4F, 42.5F, p5);


                        //move p1 below min - it should not move
                        p1.X = -5;

                        //check positions
                        AssertPlacement(-5F, 4F, 2.7F, p1);
                        AssertPlacement(17.37F, 4F, 0F, p2);
                        AssertPlacement(44.05F, 4F, 37.5F, p3);
                        AssertPlacement(77.92F, 4F, 0F, p4);
                        AssertPlacement(95F, 4F, 42.5F, p5);


                        //set p5 over - it should not move
                        p5.X = 120;

                        //check positions
                        AssertPlacement(-5F, 4F, 2.7F, p1);
                        AssertPlacement(17.37F, 4F, 0F, p2);
                        AssertPlacement(44.05F, 4F, 37.5F, p3);
                        AssertPlacement(77.92F, 4F, 0F, p4);
                        AssertPlacement(120F, 4F, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Even

                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        //check sequence
                        Assert.AreEqual(1, p1.SequenceX);
                        Assert.AreEqual(2, p2.SequenceX);
                        Assert.AreEqual(3, p3.SequenceX);
                        Assert.AreEqual(4, p4.SequenceX);
                        Assert.AreEqual(5, p5.SequenceX);

                        //check positions
                        AssertPlacement(0F, 4F, 2.7F, p1);
                        AssertPlacement(17.27F, 4F, 0F, p2);
                        AssertPlacement(43.34F, 4F, 37.5F, p3);
                        AssertPlacement(77.41F, 4F, 0F, p4);
                        AssertPlacement(94.98F, 4F, 42.5F, p5);
                    }
                    break;

                #endregion
            }
        }

        [Test]
        public void Shelf_MerchX_EvenSingle()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            //remove all positions but 1
            planView.BeginUpdate();

            PlanogramPositionView p1 = shelf.Positions[0];
            while (shelf.Positions.Count > 1)
                shelf.RemovePosition(shelf.Positions.Last());

            //set shelf value
            planView.EndUpdate();

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;

            //check
            AssertHelper.AreSinglesEqual((shelf.Width - p1.Width) / 2F, p1.X);
        }

        [Test]
        public void Shelf_MerchX_EvenOverfit()
        {
            //v8-25274/25224 - check that if the positions width is larger than the shelf width
            // even strategy places then evenly over each side.

            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView shelfComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelfComponent.Width = 50;
            PlanogramSubComponentView shelfSub = shelfComponent.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //add a product
            PlanogramPositionView p1 = shelfSub.AddPosition(plan.Products[0]);
            plan.EndUpdate();
            p1.FacingsWide = 10;
            Assert.Greater(p1.Width, shelfSub.Width);

            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;

            //check placement
            Assert.AreEqual((shelfSub.Width / 2F) - (p1.Width / 2f), p1.X);

            //add another pos
            PlanogramPositionView p2 = shelfSub.AddPosition(plan.Products[1]);
            shelfSub.MovePosition(p2, p1, PlanogramPositionAnchorDirection.ToRight);

            //check placements
            Assert.AreEqual((shelfSub.Width / 2F) - ((p1.Width + p2.Width) / 2F), p1.X);
            Assert.AreEqual(p1.X + p1.Width, p2.X);
        }

        [Test]
        public void Shelf_MerchX_Manual_AllowsBreakMax()
        {
            /*Checks the products placed beyond shelf limits 
             * when x is manual merch do not move when the plan is processed.*/

            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];

            //ensure settings
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelf.IsProductOverlapAllowed = true;

            Single testX = shelf.Width + 50;

            p1.X = testX;
            Assert.AreEqual(testX, p1.X);
        }

        [Test]
        public void Shelf_MerchX_IsOverlapAllowed()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            planView.Model.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            planView.Model.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            planView.Model.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //ensure settings
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelf.IsProductOverlapAllowed = true;

            //move p1 over p2
            planView.EndUpdate();
            p1.X = 15;

            //check positions
            AssertHelper.AreSinglesEqual(15, p1.X);
            AssertHelper.AreSinglesEqual(4, p1.Y);
            AssertHelper.AreSinglesEqual(2.7F, p1.Z);

            AssertHelper.AreSinglesEqual(17.37F, p2.X);
            AssertHelper.AreSinglesEqual(4, p2.Y);
            AssertHelper.AreSinglesEqual(0, p2.Z);

            AssertHelper.AreSinglesEqual(44.05F, p3.X);
            AssertHelper.AreSinglesEqual(4, p3.Y);
            AssertHelper.AreSinglesEqual(37.5F, p3.Z);

            AssertHelper.AreSinglesEqual(77.92F, p4.X);
            AssertHelper.AreSinglesEqual(4, p4.Y);
            AssertHelper.AreSinglesEqual(0, p4.Z);

            AssertHelper.AreSinglesEqual(95F, p5.X);
            AssertHelper.AreSinglesEqual(4, p5.Y);
            AssertHelper.AreSinglesEqual(42.5F, p5.Z);

            //turn overlap off 
            //planView.Model.Parent.SaveAs(1, @"C:\users\usr145\before.pog");
            shelf.IsProductOverlapAllowed = false;
            //planView.Model.Parent.SaveAs(1, @"C:\users\usr145\after.pog");

            //check positions
            AssertHelper.AreSinglesEqual(15, p1.X);
            AssertHelper.AreSinglesEqual(4, p1.Y);
            AssertHelper.AreSinglesEqual(2.7F, p1.Z);

            AssertHelper.AreSinglesEqual(23.2F, p2.X);
            AssertHelper.AreSinglesEqual(4, p2.Y);
            AssertHelper.AreSinglesEqual(0, p2.Z);

            AssertHelper.AreSinglesEqual(44.05F, p3.X);
            AssertHelper.AreSinglesEqual(4, p3.Y);
            AssertHelper.AreSinglesEqual(37.5F, p3.Z);

            AssertHelper.AreSinglesEqual(77.92F, p4.X);
            AssertHelper.AreSinglesEqual(4, p4.Y);
            AssertHelper.AreSinglesEqual(0, p4.Z);

            AssertHelper.AreSinglesEqual(95F, p5.X);
            AssertHelper.AreSinglesEqual(4, p5.Y);
            AssertHelper.AreSinglesEqual(42.5F, p5.Z);
        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void Shelf_MerchY(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set shelf value
            shelf.MerchandisingStrategyY = yStrategy;

            switch (yStrategy)
            {
                default:
                    throw new NotImplementedException();

                #region Bottom

                case PlanogramSubComponentYMerchStrategyType.Bottom:
                    {
                        //check sequence
                        Assert.AreEqual(1, p1.SequenceY);
                        Assert.AreEqual(1, p2.SequenceY);
                        Assert.AreEqual(1, p3.SequenceY);
                        Assert.AreEqual(1, p4.SequenceY);
                        Assert.AreEqual(1, p5.SequenceY);

                        //check positions
                        AssertHelper.AreSinglesEqual(4, p1.Y);
                        AssertHelper.AreSinglesEqual(4, p2.Y);
                        AssertHelper.AreSinglesEqual(4, p3.Y);
                        AssertHelper.AreSinglesEqual(4, p4.Y);
                        AssertHelper.AreSinglesEqual(4, p5.Y);

                        //increase the sub size
                        shelf.Height = 5;

                        //check positions
                        AssertHelper.AreSinglesEqual(shelf.Height, p1.Y);
                        AssertHelper.AreSinglesEqual(shelf.Height, p2.Y);
                        AssertHelper.AreSinglesEqual(shelf.Height, p3.Y);
                        AssertHelper.AreSinglesEqual(shelf.Height, p4.Y);
                        AssertHelper.AreSinglesEqual(shelf.Height, p5.Y);
                    }
                    break;

                #endregion

                #region Top

                case PlanogramSubComponentYMerchStrategyType.Top:
                    {
                        Assert.AreEqual(0, shelf.MerchandisableHeight);

                        //check positions - should be placed from top of fixture.
                        AssertHelper.AreSinglesEqual(158.8F, p1.MetaWorldY);
                        AssertHelper.AreSinglesEqual(159F, p2.MetaWorldY);
                        AssertHelper.AreSinglesEqual(167.8F, p3.MetaWorldY);
                        AssertHelper.AreSinglesEqual(160.2F, p4.MetaWorldY);
                        AssertHelper.AreSinglesEqual(160.1F, p5.MetaWorldY);

                        //set the merch height
                        shelf.MerchandisableHeight = 50;

                        //check positions
                        AssertHelper.AreSinglesEqual(21.8F, p1.Y);
                        AssertHelper.AreSinglesEqual(22F, p2.Y);
                        AssertHelper.AreSinglesEqual(30.8F, p3.Y);
                        AssertHelper.AreSinglesEqual(23.2F, p4.Y);
                        AssertHelper.AreSinglesEqual(23.1F, p5.Y);
                    }
                    break;

                #endregion

                #region Even

                case PlanogramSubComponentYMerchStrategyType.Even:
                    {
                        Assert.AreEqual(0, shelf.MerchandisableHeight);

                        //check sequence
                        AssertHelper.AreSinglesEqual(1, p1.SequenceY);
                        AssertHelper.AreSinglesEqual(1, p2.SequenceY);
                        AssertHelper.AreSinglesEqual(1, p3.SequenceY);
                        AssertHelper.AreSinglesEqual(1, p4.SequenceY);
                        AssertHelper.AreSinglesEqual(1, p5.SequenceY);

                        //check positions
                        AssertHelper.AreSinglesEqual(129.9F, p1.MetaWorldY);
                        AssertHelper.AreSinglesEqual(130F, p2.MetaWorldY);
                        AssertHelper.AreSinglesEqual(134.4F, p3.MetaWorldY);
                        AssertHelper.AreSinglesEqual(130.6F, p4.MetaWorldY);
                        AssertHelper.AreSinglesEqual(130.55F, p5.MetaWorldY);

                        //set the merch height
                        shelf.MerchandisableHeight = 50;

                        //check positions
                        AssertHelper.AreSinglesEqual(8.9F, p1.Y);
                        AssertHelper.AreSinglesEqual(9F, p2.Y);
                        AssertHelper.AreSinglesEqual(13.4F, p3.Y);
                        AssertHelper.AreSinglesEqual(9.6F, p4.Y);
                        AssertHelper.AreSinglesEqual(9.55F, p5.Y);
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramSubComponentYMerchStrategyType.Manual:
                    {
                        //set p1 below
                        p1.Y = 1F;

                        AssertHelper.AreSinglesEqual(4F, p1.Y);
                    }
                    break;

                #endregion

                #region BottomStacked

                case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                    {
                        //check sequence
                        Assert.AreEqual(1, p1.SequenceY);
                        Assert.AreEqual(1, p2.SequenceY);
                        Assert.AreEqual(1, p3.SequenceY);
                        Assert.AreEqual(1, p4.SequenceY);
                        Assert.AreEqual(1, p5.SequenceY);

                        //check positions
                        AssertHelper.AreSinglesEqual(4, p1.Y);
                        AssertHelper.AreSinglesEqual(4, p2.Y);
                        AssertHelper.AreSinglesEqual(4, p3.Y);
                        AssertHelper.AreSinglesEqual(4, p4.Y);
                        AssertHelper.AreSinglesEqual(4, p5.Y);


                        //move p1 above p2
                        shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
                        shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
                        shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Above);

                        //check positions
                        AssertHelper.AreSinglesEqual(36, p1.Y);
                        AssertHelper.AreSinglesEqual(4, p2.Y);
                        AssertHelper.AreSinglesEqual(4, p3.Y);
                        AssertHelper.AreSinglesEqual(4, p4.Y);
                        AssertHelper.AreSinglesEqual(4, p5.Y);
                    }
                    break;

                #endregion

                #region TopStacked

                case PlanogramSubComponentYMerchStrategyType.TopStacked:
                    {
                        shelf.Component.Y = 0;

                        //check sequence
                        Assert.AreEqual(1, p1.SequenceY);
                        Assert.AreEqual(1, p2.SequenceY);
                        Assert.AreEqual(1, p3.SequenceY);
                        Assert.AreEqual(1, p4.SequenceY);
                        Assert.AreEqual(1, p5.SequenceY);

                        //move p1 above p2
                        shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
                        shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
                        shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Above);

                        //check positions
                        AssertHelper.AreSinglesEqual(158.8F, p1.Y);
                        AssertHelper.AreSinglesEqual(126.8F, p2.Y);
                        AssertHelper.AreSinglesEqual(167.8F, p3.Y);
                        AssertHelper.AreSinglesEqual(160.2F, p4.Y);
                        AssertHelper.AreSinglesEqual(160.1F, p5.Y);
                    }
                    break;

                #endregion
            }
        }

        [Test]
        public void Shelf_MerchY_ConsidersSpaceAbove()
        {
            //V8-29028 
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;
            Assert.AreEqual(0, shelf.MerchandisableHeight);
            planView.BeginUpdate();

            PlanogramPositionView p5 = shelf.Positions[4];
            p5.FacingsHigh = 1;

            // add a shelf above, merch height = 0, check that space above is considered
            // when placing top strategy
            shelf.Component.Y = 25;
            PlanogramSubComponentView shelf2 = shelf.Fixture.AddComponent(PlanogramComponentType.Shelf).SubComponents[0];
            planView.EndUpdate();
            shelf2.Component.Y = 125;

            Assert.IsTrue(p5.Height < 50, "height of p5 should be under 50 for this.");
            Assert.AreEqual(94.1F, p5.MetaWorldY, "p5 should be placed according to space above");

            //move the shelf above to reduce the available merch space slightly - positions should adjust.
            shelf2.Component.Y = 115;
            AssertHelper.AreSinglesEqual(84.1F, p5.MetaWorldY, "p5 should be placed according to space above, which has been adjusted");

            // set merch height under space above - merch height should be applied
            shelf.MerchandisableHeight = 50;
            AssertHelper.AreSinglesEqual(48.1F, p5.MetaWorldY, "p5 should be placed according to merch height");

            // set merch height over space above - space above should be applied.
            shelf.MerchandisableHeight = 150;
            AssertHelper.AreSinglesEqual(84.1F, p5.MetaWorldY, "p5 should be placed according to space above");
        }

        [Test]
        public void Shelf_MerchY_TopOverfilled()
        {
            //Test to check that setting a merch height smaller than the position
            // height with a top strategy does not cause the position
            // to go through the shelf.

            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView shelfComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponentView shelfSub = shelfComponent.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            shelfSub.MerchandisableHeight = 5;

            //add a position
            plan.Products[0].PegDepth = 0;
            PlanogramPositionView p1 = shelfSub.AddPosition(plan.Products[0]);
            plan.EndUpdate();
            p1.FacingsHigh = 10;
            Assert.Greater(p1.Height, shelfSub.MerchandisableHeight, "pos height should be greater than merch height");
            Assert.AreEqual(shelfSub.Height, p1.Y);

            //check top stacked is the same
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            Assert.AreEqual(shelfSub.Height, p1.Y);
        }

        [Test]
        public void Shelf_MerchY_EvenSingle()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            planView.BeginUpdate();
            //remove all positions but 1
            PlanogramPositionView p1 = shelf.Positions[0];
            while (shelf.Positions.Count > 1)
                shelf.RemovePosition(shelf.Positions.Last());

            //set shelf value
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            shelf.MerchandisableHeight = 150;

            //move the shelf to the bottom of the fixture so that
            // we have enough space to play with.
            planView.EndUpdate();
            shelf.Component.Y = 0;

            //check
            AssertHelper.AreSinglesEqual((150 - p1.Height) / 2F, p1.Y);
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void Shelf_MerchZ(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyZ = zStrategy;

            switch (zStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                #region Front

                case PlanogramSubComponentZMerchStrategyType.Front:
                    {
                        //check positions
                        AssertHelper.AreSinglesEqual(2.7F, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(37.5F, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(42.5F, p5.Z);
                    }
                    break;

                #endregion

                #region FrontStacked

                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                    {
                        //check positions
                        AssertHelper.AreSinglesEqual(2.7F, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(37.5F, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(42.5F, p5.Z);


                        //move 5 behind 1


                        throw new InconclusiveException("TODO");
                    }

                #endregion

                #region Back

                case PlanogramSubComponentZMerchStrategyType.Back:
                    {
                        //check positions
                        AssertHelper.AreSinglesEqual(0, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(0, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(0, p5.Z);
                    }
                    break;

                #endregion

                #region Even

                case PlanogramSubComponentZMerchStrategyType.Even:
                    {
                        //check positions
                        AssertHelper.AreSinglesEqual(1.35F, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(18.75F, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(21.25F, p5.Z);
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramSubComponentZMerchStrategyType.Manual:
                    {
                        //check positions - should not move
                        AssertHelper.AreSinglesEqual(2.7F, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(37.5F, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(42.5F, p5.Z);


                        // push 1 over front - should not move
                        p1.Z = 5F;
                        AssertHelper.AreSinglesEqual(5F, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(37.5F, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(42.5F, p5.Z);

                        //push p1 back - valid
                        p1.Z = 2F;
                        AssertHelper.AreSinglesEqual(2F, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(37.5F, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(42.5F, p5.Z);

                        //push p1 back further - should not move
                        p1.Z = -2F;
                        AssertHelper.AreSinglesEqual(-2, p1.Z);
                        AssertHelper.AreSinglesEqual(0, p2.Z);
                        AssertHelper.AreSinglesEqual(37.5F, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(42.5F, p5.Z);
                    }
                    break;

                #endregion

                #region BackStacked

                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                    {
                        p1.FacingsDeep = 1;
                        p2.FacingsDeep = 1;

                        //move p1 behind p2
                        shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
                        shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
                        shelf.MovePosition(p1, p2, PlanogramPositionAnchorDirection.Behind);

                        //check positions
                        AssertHelper.AreSinglesEqual(0, p1.Z);
                        AssertHelper.AreSinglesEqual(8.2F, p2.Z);
                        AssertHelper.AreSinglesEqual(0, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(0, p5.Z);

                        //shift strategies
                        shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;
                        shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;

                        //check again
                        AssertHelper.AreSinglesEqual(0, p1.Z);
                        AssertHelper.AreSinglesEqual(8.2F, p2.Z);
                        AssertHelper.AreSinglesEqual(0, p3.Z);
                        AssertHelper.AreSinglesEqual(0, p4.Z);
                        AssertHelper.AreSinglesEqual(0, p5.Z);
                    }
                    break;

                #endregion
            }
        }

        [Test]
        public void Shelf_MerchZ_EvenSingle()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            //remove all positions but 1
            planView.BeginUpdate();
            PlanogramPositionView p1 = shelf.Positions[0];
            while (shelf.Positions.Count > 1)
                shelf.RemovePosition(shelf.Positions.Last());

            //set shelf value
            planView.EndUpdate();
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;

            //check
            AssertHelper.AreSinglesEqual((shelf.Depth - p1.Depth) / 2F, p1.Z);
        }

        [Test]
        public void Shelf_UndoEvenBackToManual()
        {
            //V8-27930 - Test to check that setting a shelf from manual to even
            //and then hitting undo will shift the products back to their manual position

            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            planView.BeginUpdate();
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;


            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set shelf value
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelf.IsProductOverlapAllowed = true;

            //set position placements
            p1.X = 0;
            p2.X = 1;
            p3.X = 2;
            p4.X = 3;
            p5.X = 4;


            //change to even
            planView.EndUpdate();
            planView.BeginUndoableAction();
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            planView.EndUndoableAction();

            AssertPlacement(0F, 4F, 2.7F, p1);
            AssertPlacement(17.27F, 4F, 0F, p2);
            AssertPlacement(43.34F, 4F, 37.5F, p3);
            AssertPlacement(77.41F, 4F, 0F, p4);
            AssertPlacement(94.98F, 4F, 42.5F, p5);

            //undo
            planView.Undo();
            Assert.AreEqual(PlanogramSubComponentXMerchStrategyType.Manual, shelf.MerchandisingStrategyX);
            Assert.AreEqual(0, p1.X);
            Assert.AreEqual(1, p2.X);
            Assert.AreEqual(2, p3.X);
            Assert.AreEqual(3, p4.X);
            Assert.AreEqual(4, p5.X);
        }

        #endregion

        #region Combined Shelf Tests

        private static PlanogramView CreateCombinedShelfTestPlanView()
        {
            Planogram plan = CreateShelfTestPlan();
            Package pk = plan.Parent;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            fixtureItem.X = 120;

            //add a backboard
            PlanogramFixtureComponent backboardFc =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);

            shelf1Fc.Y = 105;

            PlanogramSubComponent shelfSub = shelf1C.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            shelfSub.IsProductOverlapAllowed = true;

            shelfSub.CombineType = PlanogramSubComponentCombineType.Both;

            //output plan if required
            //Package pk = Package.NewPackage(1, PackageLockType.User, 1);
            //pk.Name = "CombinedShelfTest";
            //pk.Planograms.Add(plan);
            //pk.SaveAs(1, @"C:\Users\usr095\CombinedShelfTest.pog");
            //pk.Dispose();

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(pk, shouldCalculateMetadata: false);
            var planView = pkView.PlanogramViews.First();
            planView.ShouldCalculateMetadata = false;
            return planView;
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void CombinedShelves_MerchX(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            var planView = CreateCombinedShelfTestPlanView();
            planView.BeginUpdate();
            PlanogramSubComponentView shelf1 =
                planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            PlanogramSubComponentView shelf2 =
                planView.Fixtures[1].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];

            PlanogramPositionView p1 = shelf1.Positions[0];
            PlanogramPositionView p2 = shelf1.Positions[1];
            PlanogramPositionView p3 = shelf1.Positions[2];
            PlanogramPositionView p4 = shelf1.Positions[3];
            PlanogramPositionView p5 = shelf1.Positions[4];

            shelf1.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.MerchandisingStrategyX = xStrategy;
            shelf1.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            shelf2.CombineType = PlanogramSubComponentCombineType.None;
            shelf2.MerchandisingStrategyX = xStrategy;
            shelf2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //move product to shelf 2
            planView.EndUpdate();
            p5 = shelf2.MovePosition(p5, 0, 0, 0);
            Assert.Contains(p5, shelf2.Positions);
            Assert.IsFalse(shelf1.Positions.Contains(p5));

            //set combine
            shelf2.CombineType = PlanogramSubComponentCombineType.Both;

            switch (xStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                #region LeftStacked

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    {
                        //check placements
                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(8.2F, 4, 0F, p2);
                        AssertPlacement(25.2F, 4, 37.5F, p3);
                        AssertPlacement(50.2F, 4, 0F, p4);

                        Assert.IsFalse(shelf2.Positions.Contains(p5));
                        p5 = shelf1.Positions[4];
                        AssertPlacement(58.7F, 4, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Left

                case PlanogramSubComponentXMerchStrategyType.Left:
                    {
                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(0, 4, 0F, p2);
                        AssertPlacement(0, 4, 37.5F, p3);
                        AssertPlacement(0, 4, 0F, p4);
                        AssertPlacement(0, 4, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Right

                case PlanogramSubComponentXMerchStrategyType.Right:
                    {
                        AssertPlacement(231.8F - 120, 4, 2.7F, p1);
                        AssertPlacement(223 - 120, 4, 0F, p2);
                        AssertPlacement(215 - 120, 4, 37.5F, p3);
                        AssertPlacement(231.5F - 120, 4, 0F, p4);
                        AssertPlacement(215 - 120, 4, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Right Stacked

                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        AssertPlacement(120 - p5.Width - p4.Width - p3.Width - p2.Width - p1.Width, 4, 2.7F, p1);
                        AssertPlacement(120 - p5.Width - p4.Width - p3.Width - p2.Width, 4, 0F, p2);
                        AssertPlacement(120 - p5.Width - p4.Width - p3.Width, 4, 37.5F, p3);
                        AssertPlacement(120 - p5.Width - p4.Width, 4, 0F, p4);
                        AssertPlacement(120 - p5.Width, 4, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Even

                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(47.27F, 4, 0F, p2);
                        AssertPlacement(103.34F, 4, 37.5F, p3);
                        AssertPlacement(167.41F - 120, 4, 0F, p4);
                        AssertPlacement(94.98F, 4, 42.5F, p5);
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramSubComponentXMerchStrategyType.Manual:
                    {
                        AssertPlacement(0, 4, 2.7F, p1);
                        AssertPlacement(17.37F, 4, 0F, p2);
                        AssertPlacement(44.05F, 4, 37.5F, p3);
                        AssertPlacement(77.92F, 4, 0F, p4);
                        AssertPlacement(0, 4, 42.5F, p5);
                    }
                    break;

                #endregion
            }
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void CombinedShelves_WithXDividers(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void CombinedShelves_WithDividersPerFacing(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            var planView = CreateCombinedShelfTestPlanView();
            planView.BeginUpdate();
            PlanogramSubComponentView shelf1 =
                planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            PlanogramSubComponentView shelf2 =
                planView.Fixtures[1].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];

            PlanogramPositionView p1 = shelf1.Positions[0];
            PlanogramPositionView p2 = shelf1.Positions[1];
            PlanogramPositionView p3 = shelf1.Positions[2];
            PlanogramPositionView p4 = shelf1.Positions[3];
            PlanogramPositionView p5 = shelf1.Positions[4];

            shelf1.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.MerchandisingStrategyX = xStrategy;
            shelf1.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            shelf2.CombineType = PlanogramSubComponentCombineType.None;
            shelf2.MerchandisingStrategyX = xStrategy;
            shelf2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //move product to shelf 2
            planView.EndUpdate();
            p5 = shelf2.MovePosition(p5, 0, 0, 0);
            Assert.Contains(p5, shelf2.Positions);
            Assert.IsFalse(shelf1.Positions.Contains(p5));

            //set combine
            planView.BeginUpdate();
            shelf2.CombineType = PlanogramSubComponentCombineType.Both;

            //increase facings
            p1.FacingsWide = 3;
            p2.FacingsWide = 2;
            p3.FacingsWide = 2;
            p4.FacingsWide = 1;
            p5.FacingsWide = 2;

            //set dividers
            shelf1.DividerObstructionHeight = 30;
            shelf1.DividerObstructionWidth = 1;
            shelf1.DividerObstructionDepth = 1;
            shelf1.IsDividerObstructionAtStart = true;
            shelf1.IsDividerObstructionAtEnd = true;
            planView.EndUpdate();
            shelf1.IsDividerObstructionByFacing = true;

            //check
            switch (xStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                #region Left Stacked

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    {
                        AssertHelper.AreSinglesEqual(1, shelf1.Positions[0].MetaWorldX);
                        AssertHelper.AreSinglesEqual(28.6F, shelf1.Positions[1].MetaWorldX);
                        AssertHelper.AreSinglesEqual(47.6F, shelf1.Positions[2].MetaWorldX);
                        AssertHelper.AreSinglesEqual(99.6F, shelf1.Positions[3].MetaWorldX);

                        Assert.AreEqual(11, shelf1.Dividers.Count + shelf2.Dividers.Count, "should have dividers");
                        AssertHelper.AreSinglesEqual(0, shelf1.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(9.2F, shelf1.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(18.4F, shelf1.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(27.6F, shelf1.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(37.1F, shelf1.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(46.6F, shelf1.Dividers[5].X);
                        AssertHelper.AreSinglesEqual(72.6F, shelf1.Dividers[6].X);
                        AssertHelper.AreSinglesEqual(98.6F, shelf1.Dividers[7].X);
                        AssertHelper.AreSinglesEqual(108.1F, shelf1.Dividers[8].X);
                        AssertHelper.AreSinglesEqual(134.1F, shelf1.Dividers[9].X);
                        AssertHelper.AreSinglesEqual(160.1F, shelf1.Dividers[10].X);

                        //swap to opposite and back.
                        shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
                        shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;


                        AssertHelper.AreSinglesEqual(1, shelf1.Positions[0].MetaWorldX);
                        AssertHelper.AreSinglesEqual(28.6F, shelf1.Positions[1].MetaWorldX);
                        AssertHelper.AreSinglesEqual(47.6F, shelf1.Positions[2].MetaWorldX);
                        AssertHelper.AreSinglesEqual(99.6F, shelf1.Positions[3].MetaWorldX);
                    }
                    break;

                #endregion

                #region Right Stacked

                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        AssertHelper.AreSinglesEqual(79.9F, shelf1.Positions[0].MetaWorldX);
                        

                        //values are right but looks like these have been added to the shelf
                        // in the wrong order. Not failing for this tho as it makes no odds really.
                        AssertHelper.AreSinglesEqual(126.5F, shelf2.Positions[1].MetaWorldX);
                        AssertHelper.AreSinglesEqual(178.5F, shelf2.Positions[2].MetaWorldX);
                        AssertHelper.AreSinglesEqual(188F, shelf2.Positions[0].MetaWorldX);

                        Assert.AreEqual(11, shelf1.Dividers.Count + shelf2.Dividers.Count, "should have dividers");
                        //Assert.AreEqual(78, shelf1.Dividers[0].X);
                        //Assert.AreEqual(87.5, shelf1.Dividers[1].X);
                        //Assert.AreEqual(97, shelf1.Dividers[2].X);
                        //Assert.AreEqual(106.5F, shelf1.Dividers[3].X);
                        //Assert.AreEqual(116F, shelf1.Dividers[4].X);
                        //Assert.AreEqual(125.5F, shelf1.Dividers[5].X);
                        //Assert.AreEqual(151.5F, shelf1.Dividers[6].X);
                        //Assert.AreEqual(177.5F, shelf1.Dividers[7].X);
                        //Assert.AreEqual(187F, shelf1.Dividers[8].X);
                        //Assert.AreEqual(213F, shelf1.Dividers[9].X);
                        //Assert.AreEqual(239F, shelf1.Dividers[10].X);
                    }
                    break;

                #endregion

                #region Even

                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        AssertHelper.AreSinglesEqual(40.45F, shelf1.Positions[0].MetaWorldX);
                        AssertHelper.AreSinglesEqual(68.05F, shelf1.Positions[1].MetaWorldX);
                        AssertHelper.AreSinglesEqual(87.05F, shelf1.Positions[2].MetaWorldX);
                        AssertHelper.AreSinglesEqual(139.05F, shelf2.Positions[1].MetaWorldX);
                        AssertHelper.AreSinglesEqual(148.55F, shelf2.Positions[0].MetaWorldX);

                        Assert.AreEqual(11, shelf1.Dividers.Count + shelf2.Dividers.Count, "should have dividers");
                    }
                    break;

                #endregion
            }
        }

        [Test]
        public void CombinedShelves_ChangeShelfPosition()
        {
            var planView = CreateCombinedShelfTestPlanView();
            planView.BeginUpdate();
            PlanogramSubComponentView shelf1 =
                planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            PlanogramSubComponentView shelf2 =
                planView.Fixtures[1].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];

            shelf2.Component.Y = 0;

            shelf1.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            shelf1.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            shelf2.CombineType = PlanogramSubComponentCombineType.Both;
            shelf2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            shelf2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            planView.EndUpdate();
            shelf2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //positions should start on shelf 1
            Assert.AreEqual(5, shelf1.Positions.Count);
            Assert.AreEqual(0, shelf2.Positions.Count);

            //move shelf 2 adjacent - positions should shift
            shelf2.Component.Y = 105;
            Assert.AreEqual(0, shelf1.Positions.Count);
            Assert.AreEqual(5, shelf2.Positions.Count);

            //move shelf 2 away - positions should go with it.
            shelf2.Component.Y = 0;
            Assert.AreEqual(0, shelf1.Positions.Count);
            Assert.AreEqual(5, shelf2.Positions.Count);
        }

        [Test]
        public void CombinedShelves_MerchXEvenBayOrdering()
        {
            //Checks a scenario where 3 shelves are next to one another and the spread is even
            // also the shelves have been added in an odd order.

            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();

            //add the 3 bays (not in order)
            PlanogramFixtureView bay1 = plan.Fixtures[0];
            bay1.X = 0F;
            bay1.Y = 5.5F; //bays are all raised but that should not matter
            bay1.Z = 0;
            bay1.Height = 62;
            bay1.Width = 96;
            bay1.Depth = 27;

            PlanogramFixtureView bay3 = plan.AddFixture();
            bay3.X = 192;
            bay3.Y = 5.5F;
            bay3.Z = 0;
            bay3.Height = 62;
            bay3.Width = 72; //different width to others should not matter
            bay3.Depth = 27;

            PlanogramFixtureView bay2 = plan.AddFixture();
            bay2.X = 96F;
            bay2.Y = 5.5F;
            bay2.Z = 0;
            bay2.Height = 62;
            bay2.Width = 96;
            bay2.Depth = 27;

            //add the 3 shelves as combined (but not created in order)

            PlanogramComponentView shelf1 = bay1.AddComponent(PlanogramComponentType.Shelf);
            shelf1.Name = "SHELF 1";
            shelf1.Height = 2;
            shelf1.Width = 96;
            shelf1.Depth = 26;
            shelf1.X = 0;
            shelf1.Y = 11;
            shelf1.Z = 0;
            shelf1.SubComponents[0].MerchandisableHeight = 0;
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            plan.EndUpdate();
            shelf1.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            Assert.IsTrue(shelf1.SubComponents[0].Model.IsCombinable(), "Shelf 1 should be combinable");
            plan.BeginUpdate();

            //add a position to shelf 1 that should exactly span all 3 shelves
            plan.Products[0].Width = 26.4F;
            PlanogramPositionView p1 = shelf1.SubComponents[0].AddPosition(plan.Products[0]);
            p1.FacingsWide = 10;

            PlanogramComponentView shelf3 = bay3.AddComponent(PlanogramComponentType.Shelf);
            shelf3.Name = "SHELF 3";
            shelf3.Height = 2;
            shelf3.Width = 72;
            shelf3.Depth = 26;
            shelf3.X = 0;
            shelf3.Y = 11;
            shelf3.Z = 0;
            shelf3.SubComponents[0].MerchandisableHeight = 8; //diff merch hieght to the rest.
            shelf3.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf3.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf3.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            plan.EndUpdate();
            shelf3.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            Assert.IsTrue(shelf3.SubComponents[0].Model.IsCombinable(), "Shelf 3 should be combinable");
            plan.BeginUpdate();

            PlanogramComponentView shelf2 = bay2.AddComponent(PlanogramComponentType.Shelf);
            shelf2.Name = "SHELF 2";
            shelf2.Height = 2;
            shelf2.Width = 96;
            shelf2.Depth = 26;
            shelf2.X = 0;
            shelf2.Y = 11;
            shelf2.Z = 0;
            shelf2.SubComponents[0].MerchandisableHeight = 0;
            shelf2.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            plan.EndUpdate();
            shelf2.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            Assert.IsTrue(shelf2.SubComponents[0].Model.IsCombinable(), "Shelf 2 should be combinable");

            //check the combined with list
            List<PlanogramSubComponentPlacement> combinedWith = shelf1.SubComponents[0].ModelPlacement.GetCombinedWithList();
            Assert.AreEqual(3, combinedWith.Count(), "Combine list should contain all 3 shelves");

            p1 = plan.EnumerateAllPositions().First();
            Assert.AreEqual(shelf1.Width + shelf2.Width + shelf3.Width, p1.Width);
            Assert.AreEqual(0, p1.MetaWorldX, "Position should be placed at 0");
        }

        [Test]
        public void CombinedShelves_MerchXEvenRotatedBays()
        {
            Assert.Ignore("TODO - find out why this now fails");

            //Checks a scenario where 3 shelves are next to one another and the spread is even
            // also the shelves have been added in an odd order.

            PlanogramView plan = CreateEmptyPlan();

            //add the 3 bays (not in order)
            PlanogramFixtureView bay1 = plan.Fixtures[0];
            bay1.X = 0F;
            bay1.Y = 5.5F; //bays are all raised but that should not matter
            bay1.Z = 0;
            bay1.Height = 62;
            bay1.Width = 96;
            bay1.Depth = 27;
            bay1.Angle = RadiansToDegreesConverter.ToRadians(90); // all bays are rotated

            PlanogramFixtureView bay3 = plan.AddFixture();
            bay3.X = 0;
            bay3.Y = 5.5F;
            bay3.Z = 192;
            bay3.Height = 62;
            bay3.Width = 72; //different width to others should not matter
            bay3.Depth = 27;
            bay3.Angle = RadiansToDegreesConverter.ToRadians(90);

            PlanogramFixtureView bay2 = plan.AddFixture();
            bay2.X = 0;
            bay2.Y = 5.5F;
            bay2.Z = 96;
            bay2.Height = 62;
            bay2.Width = 96;
            bay2.Depth = 27;
            bay2.Angle = RadiansToDegreesConverter.ToRadians(90);

            //add the 3 shelves as combined (but not created in order)

            PlanogramComponentView shelf1 = bay1.AddComponent(PlanogramComponentType.Shelf);
            shelf1.Name = "SHELF 1";
            shelf1.Height = 2;
            shelf1.Width = 96;
            shelf1.Depth = 26;
            shelf1.X = 0;
            shelf1.Y = 11;
            shelf1.Z = 0;
            shelf1.SubComponents[0].MerchandisableHeight = 0;
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            Assert.IsTrue(shelf1.SubComponents[0].Model.IsCombinable(), "Shelf 1 should be combinable");

            //add a position to shelf 1 that should exactly span all 3 shelves
            plan.Products[0].Width = 26.4F;
            PlanogramPositionView p1 = shelf1.SubComponents[0].AddPosition(plan.Products[0]);
            p1.FacingsWide = 10;

            PlanogramComponentView shelf3 = bay3.AddComponent(PlanogramComponentType.Shelf);
            shelf3.Name = "SHELF 3";
            shelf3.Height = 2;
            shelf3.Width = 72;
            shelf3.Depth = 26;
            shelf3.X = 0;
            shelf3.Y = 11;
            shelf3.Z = 0;
            shelf3.SubComponents[0].MerchandisableHeight = 8; //diff merch hieght to the rest.
            shelf3.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf3.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf3.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf3.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            Assert.IsTrue(shelf3.SubComponents[0].Model.IsCombinable(), "Shelf 3 should be combinable");

            PlanogramComponentView shelf2 = bay2.AddComponent(PlanogramComponentType.Shelf);
            shelf2.Name = "SHELF 2";
            shelf2.Height = 2;
            shelf2.Width = 96;
            shelf2.Depth = 26;
            shelf2.X = 0;
            shelf2.Y = 11;
            shelf2.Z = 0;
            shelf2.SubComponents[0].MerchandisableHeight = 0;
            shelf2.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf2.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            Assert.IsTrue(shelf2.SubComponents[0].Model.IsCombinable(), "Shelf 2 should be combinable");

            //check the combined with list
            List<PlanogramSubComponentPlacement> combinedWith = bay1.Components[0].SubComponents[0].ModelPlacement.GetCombinedWithList();
            Assert.AreEqual(3, combinedWith.Count(), "Combine list should contain all 3 shelves");

            p1 = plan.EnumerateAllPositions().First();
            Assert.AreEqual(shelf1.Width + shelf2.Width + shelf3.Width, p1.Width);
            Assert.AreEqual(shelf1.SubComponents[0], p1.ParentSubComponent, "Position should be on shelf1");
            AssertHelper.AreSinglesEqual(0, p1.X, "Position should be placed at 0");
        }

        [Test]
        public void CombinedShelves_ProductInCenterOfShelves()
        {
            // Checks a scenario where a product is placed over the join of 2 shelves
            // changing between left and right stacked should not break the merch style.

            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            plan.Model.Products.Clear();

            #region Setup

            //add 2 bays and 2 shelves.
            PlanogramFixtureView bay1 = plan.Fixtures[0];
            bay1.Height = 196;
            bay1.Width = 120.6F;
            bay1.Depth = 62;

            PlanogramComponentView shelf1 = bay1.AddComponent(PlanogramComponentType.Shelf);
            shelf1.Height = 4;
            shelf1.Width = 120.6F;
            shelf1.Depth = 61;
            shelf1.SubComponents[0].MerchandisableHeight = 34;
            shelf1.SubComponents[0].CombineType = PlanogramSubComponentCombineType.None;
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramFixtureView bay2 = plan.AddFixture();
            bay2.Height = 196;
            bay2.Width = 120.6F;
            bay2.Depth = 62;
            bay2.X = 120.6F;

            PlanogramComponentView shelf2 = bay2.AddComponent(PlanogramComponentType.Shelf);
            shelf2.Height = 4;
            shelf2.Width = 120.6F;
            shelf2.Depth = 61;
            shelf2.SubComponents[0].MerchandisableHeight = 34;
            shelf2.SubComponents[0].CombineType = PlanogramSubComponentCombineType.None;
            shelf2.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelf2.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //add the products
            PlanogramProductView prod1 = plan.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.TrayHeight = 24.8F;
            prod1.TrayWidth = 14.4F;
            prod1.TrayDepth = 28;
            prod1.TrayHigh = 1;
            prod1.TrayWide = 2;
            prod1.TrayDeep = 4;

            PlanogramProductView prod2 = plan.AddProduct();
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod2.OrientationType = PlanogramProductOrientationType.Front0;
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.TrayHeight = 24.8F;
            prod2.TrayWidth = 14.4F;
            prod2.TrayDepth = 28;
            prod2.TrayHigh = 1;
            prod2.TrayWide = 2;
            prod2.TrayDeep = 4;

            PlanogramProductView prod3 = plan.AddProduct();
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.OrientationType = PlanogramProductOrientationType.Front0;
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.TrayHeight = 24.7F;
            prod3.TrayWidth = 15.2F;
            prod3.TrayDepth = 22.4F;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 2;
            prod3.TrayDeep = 4;

            PlanogramProductView prod4 = plan.AddProduct();
            prod4.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod4.OrientationType = PlanogramProductOrientationType.Front0;
            prod4.Gtin = "p4";
            prod4.Name = "p4";
            prod4.Height = 22.4F;
            prod4.Width = 7.5F;
            prod4.Depth = 7.5F;

            PlanogramProductView prod5 = plan.AddProduct();
            prod5.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod5.OrientationType = PlanogramProductOrientationType.Front0;
            prod5.Gtin = "p5";
            prod5.Name = "p5";
            prod5.TrayHeight = 22.4F;
            prod5.TrayWidth = 6.8F;
            prod5.TrayDepth = 6.8F;
            prod5.TrayHigh = 1;
            prod5.TrayWide = 2;
            prod5.TrayDeep = 4;

            PlanogramProductView prod6 = plan.AddProduct();
            prod6.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod6.OrientationType = PlanogramProductOrientationType.Front0;
            prod6.Gtin = "p6";
            prod6.Name = "p6";
            prod6.TrayHeight = 24.1F;
            prod6.TrayWidth = 15.6F;
            prod6.TrayDepth = 30.5F;
            prod6.TrayHigh = 1;
            prod6.TrayWide = 2;
            prod6.TrayDeep = 4;

            PlanogramProductView prod7 = plan.AddProduct();
            prod7.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod7.OrientationType = PlanogramProductOrientationType.Front0;
            prod7.Gtin = "p7";
            prod7.Name = "p7";
            prod7.TrayHeight = 25.9F;
            prod7.TrayWidth = 15F;
            prod7.TrayDepth = 28F;
            prod7.TrayHigh = 1;
            prod7.TrayWide = 2;
            prod7.TrayDeep = 4;


            //add shelf 1 positions
            PlanogramPositionView p1 = shelf1.SubComponents[0].AddPosition(prod1, 0, 0, 0);
            p1.FacingsHigh = 1;
            p1.FacingsWide = 2;
            p1.FacingsDeep = 2;

            PlanogramPositionView p2 = shelf1.SubComponents[0].AddPosition(prod2, 29.725F, 0, 0);
            p2.FacingsHigh = 1;
            p2.FacingsWide = 2;
            p2.FacingsDeep = 2;

            PlanogramPositionView p3 = shelf1.SubComponents[0].AddPosition(prod3, 59.45F, 0, 0);
            p3.FacingsHigh = 1;
            p3.FacingsWide = 3;
            p3.FacingsDeep = 2;

            //p1.X = 0;
            //p2.X = 29.725F;
            //p3.X = 59.45F;

            //p1.Sequence = 1;
            //p2.Sequence = 2;
            //p3.Sequence = 3;

            //add shelf 2 positions
            PlanogramPositionView p4 = shelf2.SubComponents[0].AddPosition(prod4);
            p4.FacingsHigh = 1;
            p4.FacingsWide = 5;
            p4.FacingsDeep = 8;

            PlanogramPositionView p5 = shelf2.SubComponents[0].AddPosition(prod5);
            p5.FacingsHigh = 1;
            p5.FacingsWide = 2;
            p5.FacingsDeep = 2;

            PlanogramPositionView p6 = shelf2.SubComponents[0].AddPosition(prod6);
            p6.FacingsHigh = 1;
            p6.FacingsWide = 2;
            p6.FacingsDeep = 2;

            PlanogramPositionView p7 = shelf2.SubComponents[0].AddPosition(prod7);
            p7.FacingsHigh = 1;
            p7.FacingsWide = 3;
            p7.FacingsDeep = 2;


            p4.X = 105.975F - 120.6F;
            p5.X = 144.4F - 120.6F;
            p6.X = 174.325F - 120.6F;
            p7.X = 206.45F - 120.6F;

            p4.Sequence = 1;
            p5.Sequence = 2;
            p6.Sequence = 3;
            p7.Sequence = 4;

            #endregion

            plan.EndUpdate();

            //Check initial sequences
            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(1, p4.SequenceX);
            Assert.AreEqual(2, p5.SequenceX);
            Assert.AreEqual(3, p6.SequenceX);
            Assert.AreEqual(4, p7.SequenceX);

            //set combine
            shelf1.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            shelf2.SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;


            //check positions have not moved
            AssertHelper.AreSinglesEqual(0, p1.MetaWorldX);
            AssertHelper.AreSinglesEqual(29.725F, p2.MetaWorldX);
            AssertHelper.AreSinglesEqual(59.45F, p3.MetaWorldX);
            AssertHelper.AreSinglesEqual(105.975F, p4.MetaWorldX);
            AssertHelper.AreSinglesEqual(144.4F, p5.MetaWorldX);
            AssertHelper.AreSinglesEqual(174.325F, p6.MetaWorldX);
            AssertHelper.AreSinglesEqual(206.45F, p7.MetaWorldX);

            //set both shelves to left stacked
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf2.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;

            //check sequence
            Assert.AreEqual(1, p1.Sequence);
            Assert.AreEqual(2, p2.Sequence);
            Assert.AreEqual(3, p3.Sequence);
            Assert.AreEqual(1, p4.Sequence); //this should have moved to the first shelf.
            Assert.AreEqual(2, p5.Sequence);
            Assert.AreEqual(3, p6.Sequence);
            Assert.AreEqual(4, p7.Sequence);

            //set both shelves to right stacked
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            shelf2.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;

            //check sequence
            Assert.AreEqual(1, p1.Sequence);
            Assert.AreEqual(2, p2.Sequence);
            Assert.AreEqual(3, p3.Sequence);
            Assert.AreEqual(1, p4.Sequence); //this should still be on the first shelf.
            Assert.AreEqual(2, p5.Sequence);
            Assert.AreEqual(3, p6.Sequence);
            Assert.AreEqual(4, p7.Sequence);
        }

        [Test]
        public void CombinedShelves_MerchYTop_WithDifferentSpaceAbove()
        {
            // create 2 shelves and combine.
            // create 2 more shelves above.

            // set one shelf above lower than the other so that shelf 2 has less space available above than shelf 1.

            // check that when merch top y is set, the products placed on shelf 2 are lower.

            Assert.Ignore("TODO");
        }

        [Test]
        public void CombinedShelves_SequenceX()
        {
            var planView = CreateCombinedShelfTestPlanView();
            planView.BeginUpdate();
            PlanogramSubComponentView shelf1 =
                planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            PlanogramSubComponentView shelf2 =
                planView.Fixtures[1].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];

            PlanogramPositionView p1 = shelf1.Positions[0];
            PlanogramPositionView p2 = shelf1.Positions[1];
            PlanogramPositionView p3 = shelf1.Positions[2];
            PlanogramPositionView p4 = shelf1.Positions[3];
            PlanogramPositionView p5 = shelf1.Positions[4];

            shelf1.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            shelf2.CombineType = PlanogramSubComponentCombineType.None;
            shelf2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //move product to shelf 2
            planView.EndUpdate();
            p5 = shelf2.MovePosition(p5, 0, 0, 0);
            Assert.Contains(p5, shelf2.Positions);
            Assert.IsFalse(shelf1.Positions.Contains(p5));

            //set combine
            shelf2.CombineType = PlanogramSubComponentCombineType.Both;


            //check sequence
            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(1, p4.SequenceX);
            Assert.AreEqual(2, p5.SequenceX);

            //check parent subcomponents
            Assert.AreEqual(shelf1, p1.SubComponent);
            Assert.AreEqual(shelf1, p2.SubComponent);
            Assert.AreEqual(shelf1, p3.SubComponent);
            Assert.AreEqual(shelf1, p4.SubComponent);
            Assert.AreEqual(shelf2, p5.SubComponent);


            //set to left to force values to 1.
            shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(1, p2.SequenceX);
            Assert.AreEqual(1, p3.SequenceX);
            Assert.AreEqual(1, p4.SequenceX);
            Assert.AreEqual(1, p5.SequenceX);

            //reset back to even
            shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(1, p4.SequenceX);
            Assert.AreEqual(2, p5.SequenceX);
        }

        [Test]
        public void CombinedShelves_MerchWidthDoesNotIncludeOverlappingOverhangs()
        {
            //V8-32001 - client raised.
            //Checks that where combined shelves have overhangs, the merch width does not include overlaps.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture and shelf
            PlanogramFixtureItem fixtureItem1 = plan.AddFixtureItem(67.5F, 48, 28, backboardDepth: 1, baseHeight: 5.5F);
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);
            var shelfFC1 = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 5.5F, 0), new WidthHeightDepthValue(48, 2, 26));
            var shelfSub1 = plan.Components.FindById(shelfFC1.PlanogramComponentId).SubComponents[0];
            shelfSub1.CombineType = PlanogramSubComponentCombineType.Both;

            //add combined shelf
            PlanogramFixtureItem fixtureItem2 = plan.AddFixtureItem(67.5F, 48, 28, backboardDepth: 1, baseHeight: 5.5F);
            fixtureItem2.X = fixture1.Width;
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);
            var shelfFC2 = fixtureItem2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 5.5F, 0), new WidthHeightDepthValue(48, 2, 26));
            var shelfSub2 = plan.Components.FindById(shelfFC2.PlanogramComponentId).SubComponents[0];
            shelfSub2.CombineType = PlanogramSubComponentCombineType.Both;

            //add a left overhang to shelf 2
            shelfSub2.LeftOverhang = 10;

            //check
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                Assert.AreEqual(1, merchGroups.Count);
                Assert.AreEqual(2, merchGroups[0].SubComponentPlacements.Count());
                Assert.AreEqual(shelfSub1.Width + shelfSub2.Width, merchGroups[0].GetMerchandisingSpace().UnhinderedSpace.Width);
            }
        }

        #endregion

        #region Pegboard Tests

        [Test]
        public void Pegboard_InitialLoad()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //check sequence
            Assert.AreEqual(1, p1.Sequence);
            Assert.AreEqual(2, p2.Sequence);
            Assert.AreEqual(3, p3.Sequence);
            Assert.AreEqual(4, p4.Sequence);
            Assert.AreEqual(5, p5.Sequence);

            //Check positions
            AssertPlacement(3.9F, 64.6F, 1F, p1);
            AssertPlacement(19.75F, 64.8F, 1F, p2);
            AssertPlacement(47.75F, 73.8F, 1F, p3);
            AssertPlacement(79.75F, 66F, 1F, p4);
            AssertPlacement(91.5F, 65.9F, 1F, p5);
        }

        [Test]
        public void Pegboard_FacingsSpaceToPegholes()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();

            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p4 = pegboard.Positions[3];

            //check p2 facings have spread
            p2.FacingsWide = 2;
            AssertHelper.AreSinglesEqual(3.5F, p2.FacingSpaceX);
            AssertPlacement(19.75F, 64.8F, 1F, p2);

            //increase p4 facings high
            p4.FacingsHigh = 2;
            AssertHelper.AreSinglesEqual(1.2F, p4.FacingSpaceY);
            AssertPlacement(79.75F, 34F, 1F, p4);
        }

        [Test]
        public void Pegboard_SnapToPegholes()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];

            //set p1 to a non-peg hole value
            p1.X = 15;
            AssertPlacement(15.9F, 64.6F, 1F, p1);

            p1.X = 16;
            AssertPlacement(15.9F, 64.6F, 1F, p1);

            p1.X = 18;
            AssertPlacement(19.9F, 64.6F, 1F, p1);
        }

        [Test]
        public void Pegboard_MerchXY()
        {
            PackageViewModel packageVm =
                PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();

            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set merch x left
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            AssertPlacement(3.9F, 64.6F, 1F, p1);
            AssertPlacement(15.75F, 64.8F, 1F, p2);
            AssertPlacement(39.75F, 73.8F, 1F, p3);
            AssertPlacement(67.75F, 66F, 1F, p4);
            AssertPlacement(79.5F, 65.9F, 1F, p5);

            //set merch y top
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            AssertPlacement(3.9F, 64.6F, 1F, p1);
            AssertPlacement(15.75F, 64.8F, 1F, p2);
            AssertPlacement(39.75F, 73.8F, 1F, p3);
            AssertPlacement(67.75F, 66F, 1F, p4);
            AssertPlacement(79.5F, 65.9F, 1F, p5);

            //set merch x right
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            AssertPlacement(15.9F, 64.6F, 1F, p1);
            AssertPlacement(27.75F, 64.8F, 1F, p2);
            AssertPlacement(51.75F, 73.8F, 1F, p3);
            AssertPlacement(79.75F, 66F, 1F, p4);
            AssertPlacement(91.5F, 65.9F, 1F, p5);

            //set merch y bottom
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            AssertPlacement(15.9F, 0.6F, 1F, p1);
            AssertPlacement(27.75F, 0.8F, 1F, p2);
            AssertPlacement(51.75F, 1.8F, 1F, p3);
            AssertPlacement(79.75F, 2F, 1F, p4);
            AssertPlacement(91.5F, 1.9F, 1F, p5);

            //set merch x left
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            AssertPlacement(3.9F, 0.6F, 1F, p1);
            AssertPlacement(15.75F, 0.8F, 1F, p2);
            AssertPlacement(39.75F, 1.8F, 1F, p3);
            AssertPlacement(67.75F, 2F, 1F, p4);
            AssertPlacement(79.5F, 1.9F, 1F, p5);

            //set merch x even
            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;

            //This is going all over the place atm!

            //set merch y even
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            throw new InconclusiveException("TODO");
        }

        [Test]
        public void Pegboard_MerchZ_Back()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            planView.BeginUpdate();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);
            pegboard.Depth = 1;

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set peg prongs
            p1.Product.PegDepth = 30;
            p1.Product.NumberOfPegHoles = 1;

            p2.Product.PegDepth = 30;
            p2.Product.NumberOfPegHoles = 1;

            p3.Product.PegDepth = 30;
            p3.Product.NumberOfPegHoles = 1;

            p4.Product.PegDepth = 30;
            p4.Product.NumberOfPegHoles = 1;

            p5.Product.PegDepth = 30;
            p5.Product.NumberOfPegHoles = 1;

            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            planView.EndUpdate();
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;

            //check
            AssertPlacement(3.9F, 64.6F, 1F, p1);
            AssertPlacement(15.75F, 64.8F, 1F, p2);
            AssertPlacement(39.75F, 73.8F, 1F, p3);
            AssertPlacement(67.75F, 66F, 1F, p4);
            AssertPlacement(79.5F, 65.9F, 1F, p5);

            //increase the depth of the pegboard - products should move
            pegboard.Depth = 5;

            //check
            AssertPlacement(3.9F, 64.6F, 5F, p1);
            AssertPlacement(15.75F, 64.8F, 5F, p2);
            AssertPlacement(39.75F, 73.8F, 5F, p3);
            AssertPlacement(67.75F, 66F, 5F, p4);
            AssertPlacement(79.5F, 65.9F, 5F, p5);
        }

        [Test]
        public void Pegboard_MerchZ_Front()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            planView.BeginUpdate();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            planView.EndUpdate();
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            // Initially with no peg depth set, check placements. 
            // Products should align with front of fixture
            //check (z for p1 should be peg backplate depth (1) + 0)
            AssertPlacement(3.9F, 64.6F, 1F, p1);
            AssertPlacement(15.75F, 64.8F, 1F, p2);
            AssertPlacement(39.75F, 73.8F, 1F, p3);
            AssertPlacement(67.75F, 66F, 1F, p4);
            AssertPlacement(79.5F, 65.9F, 1F, p5);

            //Apply a Merchandising Depth - products should re-align to the new max depth
            pegboard.MerchandisableDepth = 40;
            //check (z for p1 should be peg backplate depth (1) + Merch depth (40) - blockdepth (24.6))
            AssertPlacement(3.9F, 64.6F, 16.4F, p1);
            AssertPlacement(15.75F, 64.8F, 24F, p2);
            AssertPlacement(39.75F, 73.8F, 29F, p3);
            AssertPlacement(67.75F, 66F, 24F, p4);
            AssertPlacement(79.5F, 65.9F, 7F, p5);

            //set peg prongs - products should now use these depths and ignore merchandisable depth
            planView.BeginUpdate();
            p1.Product.PegDepth = 30;
            p1.Product.NumberOfPegHoles = 1;

            p2.Product.PegDepth = 30;
            p2.Product.NumberOfPegHoles = 1;

            p3.Product.PegDepth = 30;
            p3.Product.NumberOfPegHoles = 1;

            p4.Product.PegDepth = 30;
            p4.Product.NumberOfPegHoles = 1;

            p5.Product.PegDepth = 30;
            p5.Product.NumberOfPegHoles = 1;
            planView.EndUpdate();
            p5.FacingsDeep = 1;

            //check (z for p1 should be peg backplate depth (1) + peg depth (30) - blockdepth (24.6))
            AssertPlacement(3.9F, 64.6F, 6.4F, p1);
            AssertPlacement(15.75F, 64.8F, 14F, p2);
            AssertPlacement(39.75F, 73.8F, 19F, p3);
            AssertPlacement(67.75F, 66F, 14F, p4);
            AssertPlacement(79.5F, 65.9F, 14F, p5);
        }

        [Test]
        public void Pegboard_MerchZ_FrontOverfilled()
        {
            PlanogramView plan = CreateEmptyPlan();
            PlanogramComponentView pegComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Peg);
            PlanogramSubComponentView pegSub = pegComponent.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //add a position
            plan.Products[0].PegDepth = 0;
            PlanogramPositionView p1 = pegSub.AddPosition(plan.Products[0]);
            p1.FacingsDeep = 10;
            Assert.AreEqual(pegSub.Depth, p1.Z);

            //check front stacked is the same
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;
            Assert.AreEqual(pegSub.Depth, p1.Z);
        }

        [Test]
        public void Pegboard_SlopeApplied()
        {
            //V8-27722 - Check that positions are correctly placed
            // when the pegboard is sloped.

            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            //set a slope of 65
            pegboard.Slope = CommonHelper.ToRadians(65);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //check sequence
            Assert.AreEqual(1, p1.Sequence);
            Assert.AreEqual(2, p2.Sequence);
            Assert.AreEqual(3, p3.Sequence);
            Assert.AreEqual(4, p4.Sequence);
            Assert.AreEqual(5, p5.Sequence);

            //Check positions
            AssertPlacement(3.9F, 64.6F, 1F, p1);
            AssertPlacement(19.75F, 64.8F, 1F, p2);
            AssertPlacement(47.75F, 73.8F, 1F, p3);
            AssertPlacement(79.75F, 66F, 1F, p4);
            AssertPlacement(91.5F, 65.9F, 1F, p5);
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void Pegboard_MerchX(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 58;
            fixture.Width = 39;
            fixture.Depth = 14;

            //Add a pegboard
            PlanogramFixtureComponent peg1Fc = fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 24, 1F);
            PlanogramComponent peg1C = plan.Components.FindById(peg1Fc.PlanogramComponentId);
            peg1Fc.Y = 20;

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            pegSub.IsProductOverlapAllowed = true;
            pegSub.IsProductSqueezeAllowed = false;
            pegSub.MerchConstraintRow1StartX = 0;
            pegSub.MerchConstraintRow1SpacingX = 1.5F;
            pegSub.MerchConstraintRow1StartY = 1;
            pegSub.MerchConstraintRow1SpacingY = 1;
            pegSub.MerchConstraintRow1Height = 0.2F;
            pegSub.MerchConstraintRow1Width = 0.2F;


            //add the peg products
            var prod1 = plan.AddProduct(6, 3.5F, 0.38F);
            prod1.PegDepth = 3;
            prod1.NumberOfPegHoles = 1;
            prod1.PegProngOffsetX = 0;
            prod1.PegProngOffsetY = 0;
            prod1.PegX = 0;
            prod1.PegY = 0.25F;

            var prod2 = plan.AddProduct(6, 3.5F, 0.38F);
            prod2.PegDepth = 3;
            prod2.NumberOfPegHoles = 1;
            prod2.PegProngOffsetX = 0;
            prod2.PegProngOffsetY = 0;
            prod2.PegX = 0;
            prod2.PegY = 0.25F;

            var p1 = plan.Positions.Add(prod1, pegSub.GetPlanogramSubComponentPlacement());
            var p2 = plan.Positions.Add(prod2, pegSub.GetPlanogramSubComponentPlacement());
            p1.Sequence = 1;
            p1.SequenceX = 1;
            p1.SequenceY = 1;
            p1.SequenceZ = 1;
            p2.Sequence = 2;
            p2.SequenceX = 2;
            p2.SequenceY = 1;
            p2.SequenceZ = 1;

            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();

            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];
            var p2View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[1];

            //set the strategy
            planView.EnumerateAllComponents()
                .FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Peg)
                .SubComponents[0].MerchandisingStrategyX = xStrategy;

            switch (xStrategy)
            {
                case PlanogramSubComponentXMerchStrategyType.Manual:
                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    {
                        AssertWorldPlacement(1.25F, 37.25F, 3.62F, p1View);
                        AssertWorldPlacement(5.75F, 37.25F, 3.62F, p2View);
                    }
                    break;

                case PlanogramSubComponentXMerchStrategyType.Left:
                    {
                        //p1 should be underneath 
                        AssertWorldPlacement(1.25F, 37.25F, 3.62F, p2View);
                        AssertWorldPlacement(1.25F, 31.25F, 3.62F, p1View); 
                    }
                    break;

                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        AssertWorldPlacement(1.25F, 37.25F, 3.62F, p1View);
                        AssertWorldPlacement(34.25F, 37.25F, 3.62F, p2View);
                    }
                    break;

                case PlanogramSubComponentXMerchStrategyType.Right:
                    {
                        AssertWorldPlacement(34.25F, 31.25F, 3.62F, p1View);
                        AssertWorldPlacement(34.25F, 37.25F, 3.62F, p2View);
                    }
                    break;

                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        AssertWorldPlacement(29.75F, 37.25F, 3.62F, p1View);
                        AssertWorldPlacement(34.25F, 37.25F, 3.62F, p2View);
                    }
                    break;
            }

        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void Pegboard_MerchY(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void Pegboard_MerchZ(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void Pegboard_PegProngOffsetX()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Inches;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 39;
            fixture.Width = 58;
            fixture.Depth = 14;
           
            //Add a pegboard
            PlanogramFixtureComponent peg1Fc = fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 8, 1F);
            PlanogramComponent peg1C = plan.Components.FindById(peg1Fc.PlanogramComponentId);
            peg1Fc.Y = 20;

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            pegSub.IsProductOverlapAllowed = true;
            pegSub.IsProductSqueezeAllowed = false;
            pegSub.MerchConstraintRow1StartX = 0;
            pegSub.MerchConstraintRow1SpacingX =1.5F;
            pegSub.MerchConstraintRow1StartY = 1;
            pegSub.MerchConstraintRow1SpacingY = 1;
            pegSub.MerchConstraintRow1Height = 0.2F;
            pegSub.MerchConstraintRow1Width = 0.2F;


            //add the peg product
            var prod1 = plan.AddProduct(6, 3.5F, 0.38F);
            prod1.PegDepth = 3;
            prod1.NumberOfPegHoles = 1;
            prod1.PegProngOffsetX = 0;
            prod1.PegProngOffsetY = 0;
            prod1.PegX = 0;
            prod1.PegY = 0.25F;

            var p1 = plan.Positions.Add(prod1, pegSub.GetPlanogramSubComponentPlacement());


            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();

            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];


            //check
            AssertWorldPlacement(1.25F, 21.25F, 3.62F, p1View);

            //set peg offset x - should hook to hole 1 now.
            prod1.PegProngOffsetX = 0.75F;


            //check
            AssertWorldPlacement(0.5F, 21.25F, 3.62F, p1View);

            //set peg offset x - should hook to hole 2 now.
            // because the distance would push the product off the edge of the board
            prod1.PegProngOffsetX = 0.2F;

            AssertWorldPlacement(1.45F, 21.25F, 3.62F, p1View);
        }

        [Test]
        public void Pegboard_PegProngOffsetY()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Inches;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 39;
            fixture.Width = 58;
            fixture.Depth = 14;

            //Add a pegboard
            PlanogramFixtureComponent peg1Fc = fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 8, 1F);
            PlanogramComponent peg1C = plan.Components.FindById(peg1Fc.PlanogramComponentId);
            peg1Fc.Y = 20;

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            pegSub.IsProductOverlapAllowed = true;
            pegSub.IsProductSqueezeAllowed = false;
            pegSub.MerchConstraintRow1StartX = 0;
            pegSub.MerchConstraintRow1SpacingX = 1.5F;
            pegSub.MerchConstraintRow1StartY = 1;
            pegSub.MerchConstraintRow1SpacingY = 1;
            pegSub.MerchConstraintRow1Height = 0.2F;
            pegSub.MerchConstraintRow1Width = 0.2F;


            //add the peg product
            var prod1 = plan.AddProduct(6, 3.5F, 0.38F);
            prod1.PegDepth = 3;
            prod1.NumberOfPegHoles = 1;
            prod1.PegProngOffsetX = 0;
            prod1.PegProngOffsetY = 0;
            prod1.PegX = 0;
            prod1.PegY = 0.25F;

            var p1 = plan.Positions.Add(prod1, pegSub.GetPlanogramSubComponentPlacement());


            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();

            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];


            //check
            AssertWorldPlacement(1.25F, 21.25F, 3.62F, p1View);

            //set peg offset Y
            prod1.PegProngOffsetY = 0.5F;


            //check
            AssertWorldPlacement(1.25F, 20.75F, 3.62F, p1View);

        }

        #endregion

        #region Slotwall Tests

        [Test]
        public void Slotwall_PegProngOffsetX()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Centimeters;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height =200;
            fixture.Width = 120;
            fixture.Depth = 76;

            //Add a pegboard
            PlanogramFixtureComponent peg1Fc = fixture.Components.Add(PlanogramComponentType.SlotWall, fixture.Width, 100, 1F);
            PlanogramComponent peg1C = plan.Components.FindById(peg1Fc.PlanogramComponentId);
            peg1Fc.Y = 90;

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            pegSub.IsProductOverlapAllowed = false;
            pegSub.IsProductSqueezeAllowed = false;
            pegSub.MerchConstraintRow1StartX = 0;
            pegSub.MerchConstraintRow1SpacingX = 0;
            pegSub.MerchConstraintRow1StartY = 4;
            pegSub.MerchConstraintRow1SpacingY = 4;
            pegSub.MerchConstraintRow1Height = 1;
            pegSub.MerchConstraintRow1Width = 0;


            //add the peg product
            var prod1 = plan.AddProduct(3.63F, 3.5F, 0.85F);
            prod1.PegDepth = 10;
            prod1.NumberOfPegHoles = 1;
            prod1.PegProngOffsetX = 0;
            prod1.PegProngOffsetY = 0;
            prod1.PegX = 0;
            prod1.PegY = 0;

            var p1 = plan.Positions.Add(prod1, pegSub.GetPlanogramSubComponentPlacement());


            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();

            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];


            //check
            AssertWorldPlacement(0, 187.17F, 1, p1View);

            prod1.PegProngOffsetX = 5;

            //check
            AssertWorldPlacement(3.25F, 187.17F, 1, p1View);

        }


        #endregion

        #region Chest Tests

        private static Planogram CreateChestTestPlan()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a chest
            PlanogramFixtureComponent chest1Fc = fixture.Components.Add(PlanogramComponentType.Chest, fixture.Width, 60, 76.5F);
            PlanogramComponent chest1C = plan.Components.FindById(chest1Fc.PlanogramComponentId);

            PlanogramSubComponent chestSub = chest1C.SubComponents[0];
            chestSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            chestSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            chestSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            chestSub.IsProductOverlapAllowed = false;

            PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(chestSub, fixtureItem, chest1Fc);

            AddDefaultProducts(plan);

            return plan;
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void Chest_MerchX(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void Chest_MerchY(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void Chest_MerchZ(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void Chest_ChangeThickness_Bottom()
        {
            // Test to check that changing the bottom thickness of a chest
            // correctly adjusts the position placements

            var planView = new PlanogramView(CreateChestTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView chest =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Chest);
            chest.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;

            //add a position to the chest
            chest.AddPosition(planView.Products[0]);

            Assert.AreEqual(chest.FaceThicknessBottom,
                            chest.Positions[0].Y,
                            "Position should be at the bottom of the chest");

            //increase the bottom thickness
            chest.FaceThicknessBottom = 20;

            Assert.AreEqual(chest.FaceThicknessBottom, chest.Positions[0].Y, "Position should have moved.");
        }

        [Test]
        public void Chest_ChangingThickness_Front()
        {
            var planView = new PlanogramView(CreateChestTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView chest =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Chest);
            chest.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            //add a position to the chest
            chest.AddPosition(planView.Products[0]);

            PlanogramPositionView p1 = chest.Positions[0];

            Assert.AreEqual(chest.Depth - chest.FaceThicknessFront - p1.Depth,
                            p1.Z,
                            "Position should be at the front of the chest");

            chest.FaceThicknessFront = 10;

            Assert.AreEqual(chest.Depth - chest.FaceThicknessFront - p1.Depth, p1.Z, "Position should have moved.");
        }

        [Test]
        public void Chest_ChangingThickness_Walls()
        {
            var planView = new PlanogramView(CreateChestTestPlan(), shouldCalculateMetadata: false);
            PlanogramSubComponentView chest =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Chest);
            chest.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;

            //add a position to the chest
            chest.AddPosition(planView.Products[0]);

            PlanogramPositionView p1 = chest.Positions[0];

            Assert.AreEqual(chest.FaceThicknessLeft, p1.X, "Position should be at the left of the chest");

            chest.FaceThicknessLeft = 10;

            Assert.AreEqual(chest.FaceThicknessLeft, p1.X, "Position should have moved.");
        }

        [Test]
        public void Chest_OnlyBottomThickness()
        {
            //GEM:27603

            var planView = new PlanogramView(CreateChestTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView chest =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Chest);
            chest.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            chest.FaceThicknessBottom = 20;
            chest.FaceThicknessFront = 0;
            chest.FaceThicknessBack = 0;
            chest.FaceThicknessLeft = 0;
            chest.FaceThicknessRight = 0;

            //add a position to the chest
            planView.EndUpdate();
            chest.AddPosition(planView.Products[0]);

            Assert.AreEqual(chest.FaceThicknessBottom,
                            chest.Positions[0].Y,
                            "Position should be at the bottom of the chest");
        }

        [Test]
        public void Chest_WallsAreEnforced()
        {
            var planView = new PlanogramView(CreateChestTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView chest =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Chest);
            chest.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;

            //add a position to the chest
            planView.EndUpdate();
            chest.AddPosition(planView.Products[0]);

            PlanogramPositionView p1 = chest.Positions[0];

            Assert.AreEqual(chest.FaceThicknessLeft, p1.X, "Position should be at the left of the chest");

            chest.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            chest.FaceThicknessLeft = 10;

            Assert.AreEqual(chest.FaceThicknessLeft, p1.X, "Position should have moved.");
        }

        [Test]
        public void Chest_MerchY_TopOverfilled()
        {
            //Test to check that setting a merch height smaller than the position
            // height with a top strategy does not cause the position
            // to go through the bottom of the chest.

            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView chestComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Chest);
            PlanogramSubComponentView chestSub = chestComponent.SubComponents[0];


            //add a position
            plan.Products[0].PegDepth = 0;
            plan.BeginUpdate();
            PlanogramPositionView p1 = chestSub.AddPosition(plan.Products[0]);
            p1.FacingsHigh = 10;
            chestSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            chestSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            chestSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            Assert.Greater(p1.Height, chestSub.Height, "pos height should be greater than merch height");
            Assert.AreEqual(chestSub.FaceThicknessBottom, p1.Y);

            //check top stacked is the same
            chestSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            Assert.AreEqual(chestSub.FaceThicknessBottom, p1.Y);
        }

        #endregion

        #region Rod Tests

        [Test, TestCaseSource("XPlacementStrategies")]
        public void Rod_MerchX(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView rodComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
            PlanogramSubComponentView rodSub = rodComponent.SubComponents[0];
            rodSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
            rodSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            rodSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

            plan.EndUpdate();
            rodSub.MerchandisingStrategyX = xStrategy;

            switch (xStrategy)
            {
                default:
                    Assert.Ignore("TODO");
                    break;

                #region Even

                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        // V8-25304 - Test to check that products are placed
                        // centrally on a reod for an even merch strategy

                        //add a couple of products
                        PlanogramPositionView p1 = rodSub.AddPosition(plan.Products[0]);
                        PlanogramPositionView p2 = rodSub.AddPosition(plan.Products[1]);

                        //check placement
                        AssertPlacement((rodSub.Width / 2f) - ((p1.Width + p2.Width) / 2F), -p1.Height, 0, p1);
                        AssertPlacement((rodSub.Width / 2f) - ((p1.Width + p2.Width) / 2F) + p1.Width, -p2.Height, 0, p2);
                    }
                    break;

                #endregion

                #region Left/LeftStacked

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                case PlanogramSubComponentXMerchStrategyType.Left:
                    {
                        // V8-27724 - Test to check that products are placed
                        // left on the rod

                        //add a product and check
                        PlanogramPositionView p1 = rodSub.AddPosition(plan.Products[0]);
                        AssertPlacement(0, -p1.Height, 0, p1);
                    }
                    break;

                #endregion

                #region Right/RightStacked

                case PlanogramSubComponentXMerchStrategyType.Right:
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        // V8-27724 - Test to check that products are placed
                        // right on the rod

                        //add a product and check
                        PlanogramPositionView p1 = rodSub.AddPosition(plan.Products[0]);
                        AssertPlacement(rodSub.Width - p1.Width, -p1.Height, 0, p1);
                    }
                    break;

                #endregion
            }
        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void Rod_MerchY(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void Rod_MerchZ(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView rodComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
            PlanogramSubComponentView rodSub = rodComponent.SubComponents[0];
            rodSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
            rodSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            rodSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

            //add a couple of products
            PlanogramPositionView p1 = rodSub.AddPosition(plan.Products[0]);
            p1.FacingsDeep = 1;
            PlanogramPositionView p2 = rodSub.AddPosition(plan.Products[1]);
            p2.FacingsDeep = 1;

            rodSub.MovePosition(p2, p1, PlanogramPositionAnchorDirection.InFront);
            plan.EndUpdate();
            rodSub.MerchandisingStrategyZ = zStrategy;

            switch (zStrategy)
            {
                default:
                    Assert.Ignore("TODO");
                    break;

                case PlanogramSubComponentZMerchStrategyType.Even:
                    {
                        //27724 - check that rod products are central on the z axis


                        //check placement
                        // because of the size and merch strategy of the component products should be placed along the z axis?
                        AssertPlacement((rodSub.Width / 2F) - (p1.Width / 2F), -p1.Height, 0, p1);
                        AssertPlacement((rodSub.Width / 2F) - (p2.Width / 2F), -p2.Height, rodSub.Depth - p2.Depth, p2);

                        //add a third
                        PlanogramPositionView p3 = rodSub.AddPosition(plan.Products[1]);
                        p3.FacingsDeep = 1;
                        rodSub.MovePosition(p3, p2, PlanogramPositionAnchorDirection.InFront);

                        AssertSequence(1, 1, 1, p1);
                        AssertSequence(1, 1, 2, p2);
                        AssertSequence(1, 1, 3, p3);


                        Assert.Greater(rodSub.Depth, (p1.Depth + p2.Depth + p3.Depth));

                        Single spacing = (rodSub.Depth - (p1.Depth + p2.Depth + p3.Depth)) / 2F;


                        AssertPlacement((rodSub.Width / 2F) - (p1.Width / 2F), -p1.Height, 0, p1);
                        AssertPlacement((rodSub.Width / 2F) - (p2.Width / 2F), -p2.Height, p1.Depth + spacing, p2);
                        AssertPlacement((rodSub.Width / 2F) - (p2.Width / 2F), -p2.Height, rodSub.Depth - p3.Depth, p3);
                    }
                    break;
            }
        }

        [Test]
        public void Rod_MerchZ_MerchandisableDepth()
        {
            PlanogramView planView = CreateEmptyPlan();
            planView.BeginUpdate();
            planView.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
            PlanogramSubComponentView rod = planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Rod);
            rod.Depth = 30;
            rod.X = 50;
            rod.Y = 50;

            rod.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            rod.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top; //align the tops of the products
            rod.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            //add a single product
            planView.EndUpdate();
            PlanogramPositionView p1 = rod.AddPosition(planView.Products[0]);
            // check placements. 
            // Product should align with front of component
            // check x should be the -(width of the product/2 - the width of the rod /2)
            //       y should be the height of the product
            //       z should be the depth of the rod - the blockdepth of the product)
            AssertPlacement(-2.1F, -32.2F, 21.8F, p1);

            //Apply a Merchandisable Depth - products should re-align to the new max depth
            rod.MerchandisableDepth = 20;
            // check placements. 
            // Product should align with max merch depth
            // check x should be the -(width of the product/2 - the width of the rod /2)
            //       y should be the height of the product
            //       z should be the merch depth - the blockdepth of the product)
            AssertPlacement(-2.1F, -32.2F, 11.8F, p1);


            // Add a second product
            PlanogramPositionView p2 = rod.AddPositions(new[] { planView.Products[1] }, p1, PlanogramPositionAnchorDirection.InFront).First();

            //remove Merchandisable Depth
            rod.MerchandisableDepth = 0;

            // check placements - should take other products into account
            AssertPlacement(-2.1F, -32.2F, 13.3F, p1);
            AssertPlacement(-2.25F, -32F, 21.5F, p2);

            //Re-apply a Merchandisable Depth - products should re-align to the new max depth
            rod.MerchandisableDepth = 20;

            // check placements - should take Merchandisable Depth and other products into account
            AssertPlacement(-2.1F, -32.2F, 3.3F, p1);
            AssertPlacement(-2.25F, -32F, 11.5F, p2);
        }

        //[Test]
        //public void Rod_MerchX_Even()
        //{


        //    PlanogramView plan = CreateEmptyPlan();
        //    PlanogramComponentView rodComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
        //    var rodSub = rodComponent.SubComponents[0];
        //    rodSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
        //    rodSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
        //    rodSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

        //    //add a couple of products
        //    var p1 = rodSub.AddPosition(plan.Products[0]);
        //    var p2 = rodSub.AddPosition(plan.Products[1]);
        //    SetSequence(p1, 1, 1, 1);
        //    SetSequence(p2, 1, 1, 2);

        //    //check placement
        //    AssertPlacement((rodSub.Width / 2F) - (p1.Width / 2F), -p1.Height, 0, p1);
        //    AssertPlacement((rodSub.Width / 2F) - (p2.Width / 2F), -p2.Height, p1.Depth, p2);
        //}

        //[Test]
        //public void Rod_MerchX_Left()
        //{
        //    /// V8-27724 - Test to check that products are placed
        //    /// left on the rod

        //    PlanogramView plan = CreateEmptyPlan();
        //    PlanogramComponentView rodComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
        //    var rodSub = rodComponent.SubComponents[0];
        //    rodSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
        //    rodSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
        //    rodSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

        //    //add a product and check
        //    var p1 = rodSub.AddPosition(plan.Products[0]);
        //    AssertPlacement(0, -p1.Height, 0, p1);
        //}

        //[Test]
        //public void Rod_MerchX_Right()
        //{
        //    /// V8-27724 - Test to check that products are placed
        //    /// right on the rod

        //    PlanogramView plan = CreateEmptyPlan();
        //    PlanogramComponentView rodComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
        //    var rodSub = rodComponent.SubComponents[0];
        //    rodSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
        //    rodSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Right;
        //    rodSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

        //    //add a product and check
        //    var p1 = rodSub.AddPosition(plan.Products[0]);
        //    AssertPlacement(rodSub.Width - p1.Width, -p1.Height, 0, p1);
        //}

        //[Test]
        //public void Rod_MerchZ_Even()
        //{
        //    //27724 - check that rod products are central on the z axis

        //    PlanogramView plan = CreateEmptyPlan();
        //    PlanogramComponentView rodComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
        //    var rodSub = rodComponent.SubComponents[0];
        //    rodSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
        //    rodSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
        //    rodSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

        //    //add a couple of products
        //    var p1 = rodSub.AddPosition(plan.Products[0]);
        //    var p2 = rodSub.AddPosition(plan.Products[1]);
        //    SetSequence(p1, 1, 1, 1);
        //    SetSequence(p2, 1, 1, 2);

        //    //check placement
        //    // because of the size and merch strategy of the component products should be placed along the z axis?
        //    AssertPlacement((rodSub.Width / 2F) - (p1.Width / 2F), -p1.Height, 0, p1);
        //    AssertPlacement((rodSub.Width / 2F) - (p2.Width / 2F), -p2.Height, rodSub.Depth - p2.Depth, p2);

        //    //add a third
        //    var p3 = rodSub.AddPosition(plan.Products[1]);
        //    SetSequence(p3, 1, 1, 3);

        //    Assert.Greater(rodSub.Depth, (p1.Depth + p2.Depth + p3.Depth));

        //    Single spacing = (rodSub.Depth - (p1.Depth + p2.Depth + p3.Depth)) / 2F;

        //    AssertPlacement((rodSub.Width / 2F) - (p1.Width / 2F), -p1.Height, 0, p1);
        //    AssertPlacement((rodSub.Width / 2F) - (p2.Width / 2F), -p2.Height, p1.Depth + spacing, p2);
        //    AssertPlacement((rodSub.Width / 2F) - (p2.Width / 2F), -p2.Height, rodSub.Depth - p3.Depth, p3);
        //}

        [Test]
        public void Rod_MerchY_BottomOverfilled()
        {
            //Test to check that setting a merch height smaller than the position
            // height with a bottom strategy does not cause the position
            // to go through the rod itself.

            Assert.Ignore("TODO");
        }

        //[Test]
        //public void Rod_MerchHeight()
        //{
        //    //Checks that merch height is correctly applied to a hang below component
        //    throw new InconclusiveException("TODO");
        //}

        #endregion

        #region Clipstrip Tests

        [Test]
        public void Clipstrip_MerchX_Even()
        {
            // V8-28711 - Test to check that products are placed
            // centrally on a clipstrip for an even merch strategy

            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView clipComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.ClipStrip);
            PlanogramSubComponentView clipSub = clipComponent.SubComponents[0];
            clipSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            clipSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            clipSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            clipSub.X = 0;
            clipSub.Y = 0;
            clipSub.Height = 100;
            clipSub.MerchConstraintRow1StartX = 2;
            clipSub.MerchConstraintRow1SpacingX = 2;
            clipSub.MerchConstraintRow1StartY = 4;
            clipSub.MerchConstraintRow1SpacingY = 4;
            clipSub.MerchConstraintRow1Width = 1;
            clipSub.MerchConstraintRow1Height = 1;

            //add a product
            PlanogramProductView prod1 = plan.Products[0];
            prod1.Height = 17;
            prod1.Width = 7.5F;
            prod1.Depth = 3.2F;
            prod1.PegDepth = 15;
            prod1.NumberOfPegHoles = 1;
            plan.EndUpdate();
            PlanogramPositionView p1 = clipSub.AddPosition(plan.Products[0]);

            //check placement
            AssertPlacement((clipSub.Width / 2f) - (prod1.Width / 2f), 79.8F, clipSub.Depth, p1);
        }

        #endregion

        #region Bar Tests

        [Test, TestCaseSource("YPlacementStrategies")]
        public void Bar_MerchY(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            //V8-32002 - client raised.
            //Checks that the snap point on a bar does indeed change according to y strategy.
            //This is important because spaceman, jda etc have different snap points.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add bay 1  and bar
            PlanogramFixtureItem fixtureItem1 = plan.AddFixtureItem(75.2F, 48, 4.33F, backboardDepth: 4.33F, baseHeight: 4.33F);
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            var bay1Bar1fc = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 58.854F, 0), new WidthHeightDepthValue(48, 1, 12));
            var bay1Bar1S = plan.Components.FindById(bay1Bar1fc.PlanogramComponentId).SubComponents[0];
            bay1Bar1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            bay1Bar1S.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

            bay1Bar1S.MerchandisingStrategyY = yStrategy;

            //add a product
            var prod1 = plan.AddProduct(5.5F, 3.5F, 0.5F);
            prod1.NumberOfPegHoles = 1;
            prod1.PegDepth = 14;
            prod1.PegX = 0;
            prod1.PegY = 0.25F;
            prod1.PegProngOffsetX = 0;
            prod1.PegProngOffsetY = 0;

            var p1 = bay1Bar1fc.AddPosition(fixtureItem1, prod1);

            //load and check
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = packageView.PlanogramViews.First();

            switch (yStrategy)
            {
                case PlanogramSubComponentYMerchStrategyType.Bottom:
                case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                    Assert.AreEqual(53.604F, planView.EnumerateAllPositions().First().MetaWorldY);
                    break;

                case PlanogramSubComponentYMerchStrategyType.Even:
                    Assert.AreEqual(54.104F, planView.EnumerateAllPositions().First().MetaWorldY);
                    break;

                case PlanogramSubComponentYMerchStrategyType.Manual:
                case PlanogramSubComponentYMerchStrategyType.Top:
                case PlanogramSubComponentYMerchStrategyType.TopStacked:
                    Assert.AreEqual(54.604F, planView.EnumerateAllPositions().First().MetaWorldY);
                    break;
            }
        }

        [Test]
        public void Bar_PegProngOffsetY()
        {
            //V8-32267 - Checks that pegprong offset y works correctly when there are no pegholes.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add bay 1  and bar
            PlanogramFixtureItem fixtureItem1 = plan.AddFixtureItem(75.2F, 48, 4.33F, backboardDepth: 4.33F, baseHeight: 4.33F);
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            var bay1Bar1fc = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 60F, 0), new WidthHeightDepthValue(48, 1, 12));
            var bay1Bar1S = plan.Components.FindById(bay1Bar1fc.PlanogramComponentId).SubComponents[0];
            bay1Bar1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            bay1Bar1S.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            bay1Bar1S.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

            //add a product
            var prod1 = plan.AddProduct(5.5F, 3.5F, 0.5F);
            prod1.NumberOfPegHoles = 1;
            prod1.PegDepth = 14;
            prod1.PegX = 0;
            prod1.PegY = 0.25F;
            prod1.PegProngOffsetX = 0;
            prod1.PegProngOffsetY = 0.5F;

            var p1 = bay1Bar1fc.AddPosition(fixtureItem1, prod1);

            //load and check
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = packageView.PlanogramViews.First();

            Assert.AreEqual(55.25F, planView.EnumerateAllPositions().First().MetaWorldY);
        }

        [Test]
        public void Bar_PegProngOffsetX()
        {
            //V8-32267 - Checks that pegprong offset x works correctly when there are no pegholes.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add bay 1  and bar
            PlanogramFixtureItem fixtureItem1 = plan.AddFixtureItem(75.2F, 48, 4.33F, backboardDepth: 4.33F, baseHeight: 4.33F);
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            var bay1Bar1fc = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 60F, 0), new WidthHeightDepthValue(48, 1, 12));
            var bay1Bar1S = plan.Components.FindById(bay1Bar1fc.PlanogramComponentId).SubComponents[0];
            bay1Bar1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            bay1Bar1S.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            bay1Bar1S.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;

            //add a product
            var prod1 = plan.AddProduct(5.5F, 3.5F, 0.5F);
            prod1.NumberOfPegHoles = 1;
            prod1.PegDepth = 14;
            prod1.PegX = 0;
            prod1.PegY = 0.25F;
            prod1.PegProngOffsetX = 5F;
            prod1.PegProngOffsetY = 0F;

            var p1 = bay1Bar1fc.AddPosition(fixtureItem1, prod1);

            //load and check
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package);
            PlanogramView planView = packageView.PlanogramViews.First();

            Assert.AreEqual(3.25F, planView.EnumerateAllPositions().First().MetaWorldX);

        }

        #endregion

        #region Divider Tests

        [Test]
        public void Dividers_LoadOfPreExisting()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
            PlanogramSubComponent sub = component.SubComponents[0];
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                sub,
                fixtureItem,
                fc);

            sub.DividerObstructionHeight = 1;
            sub.DividerObstructionDepth = 1;
            sub.DividerObstructionWidth = 1;
            sub.IsDividerObstructionByFacing = true;
            sub.IsDividerObstructionAtStart = true;
            sub.IsDividerObstructionAtEnd = true;

            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "Gtin1";
            prod1.Name = "Product1";
            prod1.Height = 1;
            prod1.Width = 1;
            prod1.Depth = 1;
            plan.Products.Add(prod1);

            PlanogramPosition pos = subComponentPlacement.AddPosition(prod1);
            pos.FacingsWide = 2;

            var planView = new PlanogramView(plan, shouldCalculateMetadata: false);
            PlanogramSubComponentView subView = planView.Fixtures[0].Components[0].SubComponents[0];

            Assert.AreEqual(3, subView.Dividers.Count);
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void Dividers_XDividers(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set shelf values
            shelf.MerchandisingStrategyX = xStrategy;

            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionDepth = 1;
            shelf.IsDividerObstructionAtStart = true;
            shelf.IsDividerObstructionAtEnd = true;
            shelf.DividerObstructionStartX = 0;
            planView.EndUpdate();
            shelf.DividerObstructionSpacingX = 5;
            shelf.IsDividerObstructionByFacing = false;

            Assert.AreNotEqual(0, shelf.Dividers.Count, "should have dividers");


            switch (xStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                #region LeftStacked

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    {
                        //check positions
                        AssertHelper.AreSinglesEqual(1F, p1.X);
                        AssertHelper.AreSinglesEqual(11F, p2.X);
                        AssertHelper.AreSinglesEqual(31F, p3.X);
                        AssertHelper.AreSinglesEqual(61F, p4.X);
                        AssertHelper.AreSinglesEqual(71F, p5.X);

                        //check dividers
                        Assert.AreEqual(6, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(0F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(10F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(30F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(60F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(100F, shelf.Dividers[5].X);


                        //remove end divider
                        shelf.IsDividerObstructionAtEnd = false;

                        AssertHelper.AreSinglesEqual(1F, p1.X);
                        AssertHelper.AreSinglesEqual(11F, p2.X);
                        AssertHelper.AreSinglesEqual(31F, p3.X);
                        AssertHelper.AreSinglesEqual(61F, p4.X);
                        AssertHelper.AreSinglesEqual(71F, p5.X);

                        Assert.AreEqual(5, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(0F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(10F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(30F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(60F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[4].X);

                        shelf.IsDividerObstructionAtEnd = true;
                        Assert.AreEqual(6, shelf.Dividers.Count);

                        //remove start divider
                        shelf.IsDividerObstructionAtStart = false;

                        AssertHelper.AreSinglesEqual(0F, p1.X);
                        AssertHelper.AreSinglesEqual(11F, p2.X);
                        AssertHelper.AreSinglesEqual(31F, p3.X);
                        AssertHelper.AreSinglesEqual(61F, p4.X);
                        AssertHelper.AreSinglesEqual(71F, p5.X);

                        Assert.AreEqual(5F, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(10F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(30F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(60F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(100F, shelf.Dividers[4].X);

                        shelf.IsDividerObstructionAtStart = true;
                        Assert.AreEqual(6, shelf.Dividers.Count);
                    }
                    break;

                #endregion

                #region Even

                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        //v8-27795
                        //check then when the merch strategy is even dividers are placed correctly.

                        throw new InconclusiveException("TODO");
                    }

                #endregion

                #region RightStacked

                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        //nb - We are doing this slightly differently than jda/spaceman
                        // as we have an extra field called div startx - so we place dividers from the right

                        //check 
                        AssertHelper.AreSinglesEqual(20.8F, p1.X);
                        AssertHelper.AreSinglesEqual(32F, p2.X);
                        AssertHelper.AreSinglesEqual(54F, p3.X);
                        AssertHelper.AreSinglesEqual(80.5F, p4.X);
                        AssertHelper.AreSinglesEqual(94F, p5.X);

                        Assert.AreEqual(6, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(119F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(89F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(79F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(49F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(29F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(19F, shelf.Dividers[5].X);


                        //remove end divider
                        shelf.IsDividerObstructionAtEnd = false;

                        //Check
                        AssertHelper.AreSinglesEqual(20.8F, p1.X);
                        AssertHelper.AreSinglesEqual(32F, p2.X);
                        AssertHelper.AreSinglesEqual(54F, p3.X);
                        AssertHelper.AreSinglesEqual(80.5F, p4.X);
                        AssertHelper.AreSinglesEqual(94F, p5.X);

                        Assert.AreEqual(5, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(119F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(89F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(79F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(49F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(29F, shelf.Dividers[4].X);


                        shelf.IsDividerObstructionAtEnd = true;
                        Assert.AreEqual(6, shelf.Dividers.Count);


                        //remove start data
                        shelf.IsDividerObstructionAtStart = false;

                        //Check
                        AssertHelper.AreSinglesEqual(25.8F, p1.X);
                        AssertHelper.AreSinglesEqual(37F, p2.X);
                        AssertHelper.AreSinglesEqual(59F, p3.X);
                        AssertHelper.AreSinglesEqual(85.5F, p4.X);
                        AssertHelper.AreSinglesEqual(95F, p5.X);

                        Assert.AreEqual(5, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(94F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(84F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(54F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(34F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(24F, shelf.Dividers[4].X);
                    }
                    break;

                #endregion

                #region Manual

                case PlanogramSubComponentXMerchStrategyType.Manual:
                    {
                        //check positions have not moved.
                        AssertHelper.AreSinglesEqual(0F, p1.X);
                        AssertHelper.AreSinglesEqual(17.37F, p2.X);
                        AssertHelper.AreSinglesEqual(44.05F, p3.X);
                        AssertHelper.AreSinglesEqual(77.92F, p4.X);
                        AssertHelper.AreSinglesEqual(95F, p5.X);

                        //check dividers have been placed
                        Assert.AreEqual(6, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(0F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(10F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(35F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(90F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(125F, shelf.Dividers[5].X);

                        //remove end divider
                        shelf.IsDividerObstructionAtEnd = false;
                        Assert.AreEqual(5, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(0F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(10F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(35F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(90F, shelf.Dividers[4].X);
                        shelf.IsDividerObstructionAtEnd = true;
                        Assert.AreEqual(6, shelf.Dividers.Count);

                        //remove start divider
                        shelf.IsDividerObstructionAtStart = false;
                        Assert.AreEqual(5, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(10F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(35F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(90F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(125F, shelf.Dividers[4].X);
                        shelf.IsDividerObstructionAtStart = true;
                        Assert.AreEqual(6, shelf.Dividers.Count);
                    }
                    break;

                #endregion
            }
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void Dividers_XDividersPerFacing(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set shelf values
            shelf.MerchandisingStrategyX = xStrategy;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionDepth = 1;
            shelf.IsDividerObstructionAtStart = true;
            shelf.IsDividerObstructionAtEnd = true;
            shelf.DividerObstructionStartX = 0;
            shelf.DividerObstructionSpacingX = 5;
            planView.EndUpdate();
            shelf.IsDividerObstructionByFacing = true;


            switch (xStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                #region LeftStacked

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    {
                        //check positions & dividers
                        AssertHelper.AreSinglesEqual(1F, p1.X);
                        AssertHelper.AreSinglesEqual(10.2F, p2.X);
                        AssertHelper.AreSinglesEqual(29.2F, p3.X);
                        AssertHelper.AreSinglesEqual(55.2F, p4.X);
                        AssertHelper.AreSinglesEqual(64.7F, p5.X);

                        Assert.AreEqual(7, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(0F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(9.2F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(18.7F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(28.2F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(54.2F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(63.7F, shelf.Dividers[5].X);
                        AssertHelper.AreSinglesEqual(89.7F, shelf.Dividers[6].X);

                        //change start x and spacing - dividers should remain unaffected
                        shelf.DividerObstructionStartX = 20;
                        shelf.DividerObstructionSpacingX = 10;
                        Assert.AreEqual(7, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(1F, p1.X);
                    }
                    break;

                #endregion

                #region Even

                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        //v8-27795
                        //check then when the merch strategy is even and dividers between facings flag is set.

                        //NOTE: Ideally we would spread facings here but as this is not currently an option we will just assert they
                        // all get placed central.

                        shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
                        shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

                        //check placements
                        AssertHelper.AreSinglesEqual(15.65F, p1.X);
                        AssertHelper.AreSinglesEqual(24.85F, p2.X);
                        AssertHelper.AreSinglesEqual(43.85F, p3.X);
                        AssertHelper.AreSinglesEqual(69.85001F, p4.X);
                        AssertHelper.AreSinglesEqual(79.35F, p5.X);

                        //check dividers
                        Assert.AreEqual(7, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(14.65F, shelf.Dividers[0].X); //start div
                        AssertHelper.AreSinglesEqual(23.85F, shelf.Dividers[1].X);
                        //AssertHelper.AreSinglesEqual(35F, shelf.Dividers[2].X); //in between facing.
                        AssertHelper.AreSinglesEqual(42.85F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(68.85001F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(78.35F, shelf.Dividers[5].X); //end div
                    }
                    break;

                #endregion

                #region RightStacked

                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        //check positions & dividers
                        AssertHelper.AreSinglesEqual(30.3F, p1.X);
                        AssertHelper.AreSinglesEqual(39.5F, p2.X);
                        AssertHelper.AreSinglesEqual(58.5F, p3.X);
                        AssertHelper.AreSinglesEqual(84.5F, p4.X);
                        AssertHelper.AreSinglesEqual(94F, p5.X);

                        Assert.AreEqual(7, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(119F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(93F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(83.5F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(57.5F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(48F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(38.5F, shelf.Dividers[5].X);
                        AssertHelper.AreSinglesEqual(29.3F, shelf.Dividers[6].X);

                        //change start x and spacing - dividers should remain unaffected
                        shelf.DividerObstructionStartX = 20;
                        shelf.DividerObstructionSpacingX = 10;
                        Assert.AreEqual(7, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(30.3F, p1.X);
                    }
                    break;

                #endregion
            }
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void Dividers_XStart(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set shelf values
            shelf.MerchandisingStrategyX = xStrategy;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionDepth = 1;
            shelf.IsDividerObstructionAtStart = true;
            shelf.IsDividerObstructionAtEnd = true;
            shelf.DividerObstructionStartX = 0;
            shelf.DividerObstructionSpacingX = 5;
            planView.EndUpdate();
            shelf.IsDividerObstructionByFacing = false;

            switch (xStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                #region LeftStacked

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                    {
                        //set divider start x to under first pos
                        shelf.DividerObstructionStartX = 5;

                        //Check
                        AssertHelper.AreSinglesEqual(6F, p1.X);
                        AssertHelper.AreSinglesEqual(16F, p2.X);
                        AssertHelper.AreSinglesEqual(36F, p3.X);
                        AssertHelper.AreSinglesEqual(66F, p4.X);
                        AssertHelper.AreSinglesEqual(76F, p5.X);

                        Assert.AreEqual(6, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(5F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(15F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(35F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(65F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(75F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(105F, shelf.Dividers[5].X);

                        //increase to middle of the second to last right pos
                        //should place start still
                        shelf.DividerObstructionStartX = 10;

                        AssertHelper.AreSinglesEqual(0F, p1.X);
                        AssertHelper.AreSinglesEqual(11F, p2.X);
                        AssertHelper.AreSinglesEqual(31F, p3.X);
                        AssertHelper.AreSinglesEqual(61F, p4.X);
                        AssertHelper.AreSinglesEqual(71F, p5.X);


                        Assert.AreEqual(5, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(10F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(30F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(60F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(100F, shelf.Dividers[4].X);


                        //dont place start div
                        shelf.IsDividerObstructionAtStart = false;

                        AssertHelper.AreSinglesEqual(0F, p1.X);
                        AssertHelper.AreSinglesEqual(8.2F, p2.X);
                        AssertHelper.AreSinglesEqual(31F, p3.X);
                        AssertHelper.AreSinglesEqual(61F, p4.X);
                        AssertHelper.AreSinglesEqual(71F, p5.X);

                        Assert.AreEqual(4, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(30F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(60F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(70F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(100F, shelf.Dividers[3].X);
                    }
                    break;

                #endregion

                #region RightStacked

                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                    {
                        //set divider start x to under first pos
                        shelf.DividerObstructionStartX = 15;

                        //Check
                        AssertHelper.AreSinglesEqual(5.8F, p1.X);
                        AssertHelper.AreSinglesEqual(17F, p2.X);
                        AssertHelper.AreSinglesEqual(39F, p3.X);
                        AssertHelper.AreSinglesEqual(65.5F, p4.X);
                        AssertHelper.AreSinglesEqual(79F, p5.X);

                        Assert.AreEqual(6, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(104F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(74F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(64F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(34F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(14F, shelf.Dividers[4].X);
                        AssertHelper.AreSinglesEqual(4F, shelf.Dividers[5].X);

                        //increase to middle of the second to last right pos
                        //should place start still
                        shelf.DividerObstructionStartX = 30;

                        AssertHelper.AreSinglesEqual(20.8F, p1.X);
                        AssertHelper.AreSinglesEqual(32F, p2.X);
                        AssertHelper.AreSinglesEqual(54F, p3.X);
                        AssertHelper.AreSinglesEqual(80.5F, p4.X);
                        AssertHelper.AreSinglesEqual(95F, p5.X);


                        Assert.AreEqual(5, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(89F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(79F, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(49F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(29F, shelf.Dividers[3].X);
                        AssertHelper.AreSinglesEqual(19F, shelf.Dividers[4].X);


                        //dont place start div
                        shelf.IsDividerObstructionAtStart = false;

                        AssertHelper.AreSinglesEqual(25.8F, p1.X);
                        AssertHelper.AreSinglesEqual(37F, p2.X);
                        AssertHelper.AreSinglesEqual(59F, p3.X);
                        AssertHelper.AreSinglesEqual(86.5F, p4.X);
                        AssertHelper.AreSinglesEqual(95F, p5.X);

                        Assert.AreEqual(4, shelf.Dividers.Count);
                        AssertHelper.AreSinglesEqual(84F, shelf.Dividers[0].X);
                        AssertHelper.AreSinglesEqual(54, shelf.Dividers[1].X);
                        AssertHelper.AreSinglesEqual(34F, shelf.Dividers[2].X);
                        AssertHelper.AreSinglesEqual(24F, shelf.Dividers[3].X);
                    }
                    break;

                #endregion
            }
        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void Dividers_YDividers(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void Dividers_YStart(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void Dividers_ZDividers(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            //place products on behind one another
            p1.FacingsDeep = 2;
            p2.FacingsDeep = 1;
            p3.FacingsDeep = 1;
            p3.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            p4.FacingsDeep = 1;
            p5.FacingsDeep = 1;

            shelf.MovePosition(p2, p1, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p3, p2, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p4, p3, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p5, p4, PlanogramPositionAnchorDirection.Behind);


            shelf.MerchandisingStrategyZ = zStrategy;

            //set dividers
            shelf.DividerObstructionDepth = 1;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionSpacingZ = 5;
            shelf.IsDividerObstructionAtStart = true;
            planView.EndUpdate();
            shelf.IsDividerObstructionAtEnd = true;

            Assert.AreNotEqual(0, shelf.Dividers.Count, "should have dividers");

            switch (zStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                #region Front Stacked

                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                    {
                        Assert.AreEqual(6, shelf.Dividers.Count, "should have dividers");

                        AssertHelper.AreSinglesEqual(3.5F, p5.Z);
                        AssertHelper.AreSinglesEqual(22F, p4.Z);
                        AssertHelper.AreSinglesEqual(33.5F, p3.Z);
                        AssertHelper.AreSinglesEqual(47F, p2.Z);
                        AssertHelper.AreSinglesEqual(59.1F, p1.Z);
                    }
                    break;

                #endregion

                #region BackStacked

                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                    {
                        Assert.AreEqual(6, shelf.Dividers.Count, "should have dividers");

                        AssertHelper.AreSinglesEqual(1F, p5.Z);
                        AssertHelper.AreSinglesEqual(21F, p4.Z);
                        AssertHelper.AreSinglesEqual(31F, p3.Z);
                        AssertHelper.AreSinglesEqual(46F, p2.Z);
                        AssertHelper.AreSinglesEqual(56F, p1.Z);
                    }
                    break;

                #endregion

                #region Back

                case PlanogramSubComponentZMerchStrategyType.Back:
                    {
                        Assert.AreEqual(10, shelf.Dividers.Count, "should have dividers");

                        AssertHelper.AreSinglesEqual(1F, p5.Z);
                        AssertHelper.AreSinglesEqual(1F, p4.Z);
                        AssertHelper.AreSinglesEqual(1F, p3.Z);
                        AssertHelper.AreSinglesEqual(1F, p2.Z);
                        AssertHelper.AreSinglesEqual(1F, p1.Z);
                    }
                    break;

                #endregion

                #region Front

                case PlanogramSubComponentZMerchStrategyType.Front:
                    {
                        Assert.AreEqual(10, shelf.Dividers.Count, "should have dividers");
                    }
                    break;

                #endregion
            }
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void Dividers_ZStart(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void Dividers_PegboardXLeftYTop()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void Dividers_PegboardWithPegHolesSet()
        {
            //throw new InconclusiveException("TODO");

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a pegboard
            PlanogramFixtureComponent peg1Fc =
                fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 100, 1F);
            PlanogramComponent peg1C = plan.Components.FindById(peg1Fc.PlanogramComponentId);

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
            pegSub.IsProductOverlapAllowed = false;
            pegSub.MerchConstraintRow1StartX = 4;
            pegSub.MerchConstraintRow1SpacingX = 4;
            pegSub.MerchConstraintRow1StartY = 4;
            pegSub.MerchConstraintRow1SpacingY = 4;
            pegSub.MerchConstraintRow1Height = 1;
            pegSub.MerchConstraintRow1Width = 1;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                pegSub,
                fixtureItem,
                peg1Fc);

            //Product 1
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 3.5F;
            prod1.Width = 25.5F;
            prod1.Depth = 14F;


            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 3.25F;
            pos1.Y = 88.75F;
            pos1.Z = 1F;

            //set dividers
            var planView = new PlanogramView(plan, shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);
            PlanogramPositionView p1 = pegboard.Positions[0];


            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;

            pegboard.DividerObstructionHeight = 1;
            pegboard.DividerObstructionWidth = 1;
            pegboard.DividerObstructionDepth = 1;
            pegboard.IsDividerObstructionAtStart = true;
            pegboard.IsDividerObstructionAtEnd = true;
            pegboard.DividerObstructionSpacingY = 5;
            planView.EndUpdate();
            pegboard.DividerObstructionSpacingX = 5;


            //check
            AssertPlacement(3.25F, 93.3F, 1F, p1);

            //check dividers
            Assert.AreEqual(4, pegboard.Dividers.Count);

            //div 1
            PlanogramSubComponentDivider div = pegboard.Dividers[0];
            AssertPlacement(0, 99, 1, div);
            Assert.AreEqual(pegboard.Width, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionHeight, div.Height);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);

            //div2
            div = pegboard.Dividers[1];
            AssertPlacement(0, 89, 1, div);
            Assert.AreEqual(pegboard.Width, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionHeight, div.Height);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);

            //div3
            div = pegboard.Dividers[2];
            AssertPlacement(0, 93.3F, 1, div);
            Assert.AreEqual(pegboard.DividerObstructionWidth, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);

            //div4
            div = pegboard.Dividers[3];
            AssertPlacement(30, 93.3F, 1, div);
            Assert.AreEqual(pegboard.DividerObstructionWidth, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);


            //set peg hole values

            prod1.Height = 3.5F;
            prod1.Width = 25.5F;
            prod1.Depth = 14F;

            prod1.PegDepth = 25;
            prod1.NumberOfPegHoles = 1;
            prod1.PegY = 0.25F;


            //check
            AssertPlacement(3.25F, 92.75F, 1F, p1);

            //check dividers
            Assert.AreEqual(4, pegboard.Dividers.Count);

            //div 1
            div = pegboard.Dividers[0];
            AssertPlacement(0, 99, 1, div);
            Assert.AreEqual(pegboard.Width, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionHeight, div.Height);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);

            //div2
            div = pegboard.Dividers[1];
            AssertPlacement(0, 89, 1, div);
            Assert.AreEqual(pegboard.Width, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionHeight, div.Height);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);

            //div3
            div = pegboard.Dividers[2];
            AssertPlacement(0, 92.75F, 1, div);
            Assert.AreEqual(pegboard.DividerObstructionWidth, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);

            //div4
            div = pegboard.Dividers[3];
            AssertPlacement(30, 92.75F, 1, div);
            Assert.AreEqual(pegboard.DividerObstructionWidth, div.Width);
            Assert.AreEqual(pegboard.DividerObstructionDepth, div.Depth);
        }

        [Test]
        public void Dividers_NoPositionsNoDividers()
        {
            // v8-27795 -Check that when there are positions, no dividers are drawn.

            PlanogramView planView = CreateEmptyPlan();
            planView.BeginUpdate();
            PlanogramComponentView shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponentView shelf1Sub = shelf1.SubComponents[0];


            //set shelf divider values
            shelf1Sub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf1Sub.DividerObstructionHeight = 1;
            shelf1Sub.DividerObstructionWidth = 1;
            shelf1Sub.DividerObstructionDepth = 1;
            shelf1Sub.IsDividerObstructionAtStart = true;
            shelf1Sub.IsDividerObstructionAtEnd = true;
            shelf1Sub.DividerObstructionStartX = 0;
            shelf1Sub.DividerObstructionSpacingX = 5;
            planView.EndUpdate();
            shelf1Sub.IsDividerObstructionByFacing = false;

            //check there are no dividers
            Assert.AreEqual(0, shelf1Sub.Dividers.Count, "Shelf should have no dividers");

            //add a position
            shelf1Sub.AddPosition(planView.Products[0]);
            Assert.AreNotEqual(0, shelf1Sub.Dividers.Count, "Shelf should have dividers");

            //remove the position again
            shelf1Sub.RemovePosition(shelf1Sub.Positions[0]);
            Assert.AreEqual(0, shelf1Sub.Dividers.Count, "Shelf should have no dividers");
        }


        [Test]
        public void Dividers_PerFacingPlaceStartDividerFlag()
        {
            //Check that the place start divider flag is still acknowledged when
            // divider per facings flag is true.

            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];

            //set shelf values
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionDepth = 1;
            shelf.IsDividerObstructionAtEnd = true;
            shelf.DividerObstructionStartX = 0;
            shelf.DividerObstructionSpacingX = 5;

            shelf.IsDividerObstructionByFacing = true;


            //flag on
            planView.EndUpdate();
            shelf.IsDividerObstructionAtStart = true;
            AssertHelper.AreSinglesEqual(1F, p1.X);
            AssertHelper.AreSinglesEqual(10.2F, p2.X);
            Assert.AreEqual(7, shelf.Dividers.Count);
            AssertHelper.AreSinglesEqual(0F, shelf.Dividers[0].X);
            AssertHelper.AreSinglesEqual(9.2F, shelf.Dividers[1].X);


            //flag off
            shelf.IsDividerObstructionAtStart = false;
            AssertHelper.AreSinglesEqual(0F, p1.X);
            AssertHelper.AreSinglesEqual(9.2F, p2.X);
            Assert.AreEqual(6, shelf.Dividers.Count);
            AssertHelper.AreSinglesEqual(8.2F, shelf.Dividers[0].X);
        }

        [Test]
        public void Dividers_PerFacingPlaceEndDividerFlag()
        {
            //Check that the place end divider flag is still acknowledged when
            // divider per facings flag is true.

            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);

            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set shelf values
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionDepth = 1;
            shelf.IsDividerObstructionAtStart = true;
            shelf.DividerObstructionStartX = 0;
            shelf.DividerObstructionSpacingX = 5;

            shelf.IsDividerObstructionByFacing = true;

            //flag on
            planView.EndUpdate();
            shelf.IsDividerObstructionAtEnd = true;
            AssertHelper.AreSinglesEqual(55.2F, p4.X);
            AssertHelper.AreSinglesEqual(64.7F, p5.X);

            Assert.AreEqual(7, shelf.Dividers.Count);
            AssertHelper.AreSinglesEqual(63.7F, shelf.Dividers[5].X);
            AssertHelper.AreSinglesEqual(89.7F, shelf.Dividers[6].X);

            //flag off
            shelf.IsDividerObstructionAtEnd = false;
            AssertHelper.AreSinglesEqual(55.2F, p4.X);
            AssertHelper.AreSinglesEqual(64.7F, p5.X);
            Assert.AreEqual(6, shelf.Dividers.Count);
            AssertHelper.AreSinglesEqual(63.7F, shelf.Dividers[5].X);
        }

        #endregion

        #region Overhang Tests

        [Test]
        public void Overhang_Front()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;


            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set front overhang to positive
            planView.EndUpdate();
            shelf.FrontOverhang = 10;

            AssertPlacement(0, 4, 12.7F, p1);
            AssertPlacement(8.2F, 4, 10F, p2);
            AssertPlacement(25.2F, 4, 47.5F, p3);
            AssertPlacement(50.2F, 4, 10F, p4);
            AssertPlacement(58.7F, 4, 52.5F, p5);

            //set front overhang to negative
            shelf.FrontOverhang = -10;
            AssertPlacement(0, 4, -7.3F, p1);
            AssertPlacement(8.2F, 4, -10F, p2);
            AssertPlacement(25.2F, 4, 27.5F, p3);
            AssertPlacement(50.2F, 4, -10F, p4);
            AssertPlacement(58.7F, 4, 32.5F, p5);
        }

        [Test]
        public void Overhang_Back()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;


            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set back overhang to positive
            planView.EndUpdate();
            shelf.BackOverhang = 10;

            AssertPlacement(0, 4, -10F, p1);
            AssertPlacement(8.2F, 4, -10F, p2);
            AssertPlacement(25.2F, 4, -10F, p3);
            AssertPlacement(50.2F, 4, -10F, p4);
            AssertPlacement(58.7F, 4, -10F, p5);

            //set back overhang to negative
            shelf.BackOverhang = -10;

            AssertPlacement(0, 4, 10F, p1);
            AssertPlacement(8.2F, 4, 10F, p2);
            AssertPlacement(25.2F, 4, 10F, p3);
            AssertPlacement(50.2F, 4, 10F, p4);
            AssertPlacement(58.7F, 4, 10F, p5);
        }

        [Test]
        public void Overhang_Top()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            planView.BeginUpdate();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set top overhang to positive
            planView.EndUpdate();
            pegboard.TopOverhang = 10;

            AssertPlacement(3.9F, 74.6F, 1F, p1);
            AssertPlacement(15.75F, 74.8F, 1F, p2);
            AssertPlacement(39.75F, 83.8F, 1F, p3);
            AssertPlacement(67.75F, 76F, 1F, p4);
            AssertPlacement(79.5F, 75.9F, 1F, p5);


            //set top overhang to negative
            pegboard.TopOverhang = -10;

            AssertPlacement(3.9F, 54.6F, 1F, p1);
            AssertPlacement(15.75F, 54.8F, 1F, p2);
            AssertPlacement(39.75F, 63.8F, 1F, p3);
            AssertPlacement(67.75F, 56F, 1F, p4);
            AssertPlacement(79.5F, 55.9F, 1F, p5);
        }

        [Test]
        public void Overhang_Bottom()
        {
            PackageViewModel packageVm = PackageViewModel.NewPackageViewModel(CreatePegTestPlan(), shouldCalculateMetadata: false);
            var planView = packageVm.PlanogramViews.First();
            planView.BeginUpdate();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            pegboard.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];
            PlanogramPositionView p4 = pegboard.Positions[3];
            PlanogramPositionView p5 = pegboard.Positions[4];

            //set bottom overhang to positive
            planView.EndUpdate();
            pegboard.BottomOverhang = 10;

            AssertPlacement(3.9F, -9.4F, 1F, p1);
            AssertPlacement(15.75F, -9.2F, 1F, p2);
            AssertPlacement(39.75F, -8.2F, 1F, p3);
            AssertPlacement(67.75F, -8F, 1F, p4);
            AssertPlacement(79.5F, -8.1F, 1F, p5);

            //set bottom overhang to negative
            pegboard.BottomOverhang = -10;

            AssertPlacement(3.9F, 10.6F, 1F, p1);
            AssertPlacement(15.75F, 10.8F, 1F, p2);
            AssertPlacement(39.75F, 11.8F, 1F, p3);
            AssertPlacement(67.75F, 12F, 1F, p4);
            AssertPlacement(79.5F, 11.9F, 1F, p5);
        }

        [Test]
        public void Overhang_Left()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];


            //set left overhang as positive
            planView.EndUpdate();
            shelf.LeftOverhang = 10;

            AssertPlacement(-10, 4, 2.7F, p1);
            AssertPlacement(-1.8F, 4, 0F, p2);
            AssertPlacement(15.2F, 4, 37.5F, p3);
            AssertPlacement(40.2F, 4, 0F, p4);
            AssertPlacement(48.7F, 4, 42.5F, p5);


            //set left overhang as negative
            shelf.LeftOverhang = -10;

            AssertPlacement(10, 4, 2.7F, p1);
            AssertPlacement(18.2F, 4, 0F, p2);
            AssertPlacement(35.2F, 4, 37.5F, p3);
            AssertPlacement(60.2F, 4, 0F, p4);
            AssertPlacement(68.7F, 4, 42.5F, p5);
        }

        [Test]
        public void Overhang_Right()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //set right overhang as positive
            planView.EndUpdate();
            shelf.RightOverhang = 10;

            AssertPlacement(46.3F, 4F, 2.7F, p1);
            AssertPlacement(54.5F, 4F, 0F, p2);
            AssertPlacement(71.5F, 4F, 37.5F, p3);
            AssertPlacement(96.5F, 4F, 0F, p4);
            AssertPlacement(105F, 4F, 42.5F, p5);

            //set right overhang as negative
            shelf.RightOverhang = -10;

            AssertPlacement(26.3F, 4F, 2.7F, p1);
            AssertPlacement(34.5F, 4F, 0F, p2);
            AssertPlacement(51.5F, 4F, 37.5F, p3);
            AssertPlacement(76.5F, 4F, 0F, p4);
            AssertPlacement(85F, 4F, 42.5F, p5);
        }

        #endregion

        #region ProductSqueeze Tests

        [Test]
        public void ProductSqueeze_NotAppliedForManualStrategy()
        {
            //V8-29028 - Checks that when the sub merch strategy is set to manual,
            // the product squeeze does not get applied even when required.
            // same behaviour when can overlap is on or off.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 2;
            p3.FacingsWide = 1;
            planView.EndUpdate();
            p4.FacingsWide = 5;
            p5.FacingsWide = 1;
            Assert.IsTrue((p5.X + p5.Width) > shelf.Width, "p5 should be hanging off the end of the shelf");

            //apply squeeze to p5
            p5.Product.SqueezeWidth = 0.75F; //squeeze the width of p5 to 75%
            //Assert.IsFalse((p5.X + p5.Width) > shelf.Width, "p5 should have been squeezed");

            //set strategy to manual
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;

            //p5 should still be hanging off the end of the shelf.
            Assert.IsTrue((p5.X + p5.Width) > shelf.Width, "p5 should be hanging off the end of the shelf");
        }

        [Test]
        public void ProductSqueeze_NotAppliedWhenComponentIsUnderfitted()
        {
            //V8-29028 - Checks that when a component is underfitted, product squeeze dimensions are not applied.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p2 = shelf.Positions[1];

            //set shelf strategy to left stacked.
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            planView.EndUpdate();
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //confirm that the shelf is underfitted
            Assert.IsTrue(shelf.Positions.Sum(p => p.Width) < shelf.Width, "shelf should be underfitted.");

            //apply squeeze to p2
            Single p2Height = p2.Height;
            Single p2Width = p2.Width;
            Single p2Depth = p2.Depth;

            p2.Product.SqueezeHeight = 0.5F;
            p2.Product.SqueezeWidth = 0.5F;
            p2.Product.SqueezeDepth = 0.5F;

            //check it did not actually get squeezed
            Assert.AreEqual(p2Height, p2.Height, "Height should not have changed");
            Assert.AreEqual(p2Width, p2.Width, "Width should not have changed");
            Assert.AreEqual(p2Depth, p2.Depth, "Height should not have changed");
        }

        [Test]
        public void ProductSqueeze_AppliedWhenComponentIsOverfitted_OneSqueezeableProduct()
        {
            //V8-29028 - Checks that when a component is overfitted, product squeeze dimensions are applied.
            // - checks that squeeze is applied only to the amount required

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 2;
            p3.FacingsWide = 1;
            planView.EndUpdate();
            p4.FacingsWide = 5;
            p5.FacingsWide = 1;
            Assert.IsTrue((p5.X + p5.Width) > shelf.Width, "p5 should be hanging off the end of the shelf");

            //apply squeeze to p5
            Single p5Height = p5.Height;
            Single p5Width = p5.Width;
            Single p5Depth = p5.Depth;

            p5.Product.SqueezeHeight = 0.75F;
            p5.Product.SqueezeWidth = 0.75F; //squeeze the width of p5 to 75%
            p5.Product.SqueezeDepth = 0.75F;

            //check that squeeze was applied to the width
            Assert.AreEqual(p5Height, p5.Height, "height should not have changed");
            Assert.Less(p5.Width, p5Width, "width should have decreased");
            Assert.AreEqual(p5Depth, p5.Depth, "depth should not have changed");

            //switch strategy to max and even - should still be squeezed
            p5Width = p5.Width;
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            Assert.AreEqual(p5Width, p5.Width, "width should not have squeezed further");
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            Assert.AreEqual(p5Width, p5.Width, "width should not have squeezed further");


            //now set the product to allow more squeeze - product width should not change anymore.
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            p5.Product.SqueezeWidth = 0.5F;
            Assert.AreEqual(p5Width, p5.Width, "width should not have squeezed further");
            Assert.AreEqual(p5Height, p5.Height, "height should not have changed");
            Assert.AreEqual(p5Depth, p5.Depth, "depth should not have changed");
        }

        [Test]
        public void ProductSqueeze_AppliedWhenComponentIsOverfitted_MultipleSqueezeableProducts()
        {
            //V8-29028 - Checks that when a component is overfitted, product squeeze dimensions are appled.

            // - checks that squeeze is applied only to the amount required accross all squeezeable products.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 2;
            p3.FacingsWide = 1;
            p4.FacingsWide = 5;
            p5.FacingsWide = 1;


            //start by only allowing squeeze by product width
            p5.Product.SqueezeHeight = 1;
            p5.Product.SqueezeWidth = 0.5F;
            planView.EndUpdate();
            p5.Product.SqueezeDepth = 1;

            //check that the width was fully squeezed
            Assert.AreNotEqual(1, p5.Model.HorizontalSqueeze, "Main block should have been squeezed");

            //now set p1 to squeeze too -the squeeze amount should be spread evenly so p5 should now be a little wider.
            Single p5Width = p5.Width;
            p1.Product.SqueezeWidth = 0.5F;

            Assert.AreNotEqual(1, p1.Model.HorizontalSqueeze, "p1 should have been squeezed");
            Assert.AreNotEqual(1, p5.Model.HorizontalSqueeze, "p5 Main block should still be squeezed");
            Assert.Greater(p5.Width, p5Width, "p5 width should have increased");

            Assert.AreEqual(1, p2.Model.HorizontalSqueeze, "p2 should not have been squeezed");
            Assert.AreEqual(1, p3.Model.HorizontalSqueeze, "p3 should not have been squeezed");
            Assert.AreEqual(1, p4.Model.HorizontalSqueeze, "p4 should not have been squeezed");
        }

        [Test]
        public void ProductSqueeze_SqueezeToFitMoreByDepth()
        {
            //V8-29028 - Checks that products can be squeezed on the z axis.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //place 4 facings
            p5.Product.SqueezeDepth = 0.5F;
            planView.EndUpdate();
            p5.FacingsDeep = 4;
            Assert.AreEqual(p5.Product.Depth * 4, p5.Depth, "Should not be squeezed");
            Assert.Less(p5.Depth, shelf.Depth, "Should not fill shelf");

            //now add a 5th - the product should get squeezed evenly accross its facings.
            p5.FacingsDeep = 5;
            Single overhangAmount = (p5.Product.Depth * 5) - shelf.Depth;
            Assert.AreEqual(p5.Depth, shelf.Depth, "Product should have been squeezed to fill shelf");


            Assert.AreEqual(p5.Product.Depth - (overhangAmount / 5.0F),
                            p5.UnitDepth,
                            "Squeeze should have been distributed evenly accross units");

            //now overfit the shelf even with products at max squeeze
            p5.FacingsDeep = 10;
            Assert.Greater(p5.Depth, shelf.Depth, "shelf should now be overfit");
            Assert.AreEqual(p5.Product.Depth * 0.5F, p5.UnitDepth, "units should be at max squeeze");
        }

        [Test]
        public void ProductSqueeze_SqueezeToFitMoreByHeight()
        {
            //V8-29028 - Checks that products can be squeezed on the y axis.


            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //set a merch height on the shelf
            shelf.Component.Y = 0;
            shelf.MerchandisableHeight = 100;
            planView.EndUpdate();
            p5.FacingsHigh = 3;
            Assert.Less(p5.Height, shelf.MerchandisableHeight, "should not be overfitted");

            p5.FacingsHigh = 4;
            Assert.Greater(p5.Height, shelf.MerchandisableHeight, "should now be overfitted");

            //allow squeeze
            p5.Product.SqueezeHeight = 0.5F;
            Assert.AreEqual(p5.Height, shelf.MerchandisableHeight, "should now ne squeezed");

            Single overfitAmount = (p5.Product.Height * 4) - shelf.MerchandisableHeight;
            Assert.AreEqual(p5.Product.Height - (overfitAmount / 4.0F),
                            p5.UnitHeight,
                            "Squeeze should have been distributed evenly accross units");

            //now overfit the shelf even with products at max squeeze
            p5.FacingsHigh = 7;
            Assert.Greater(p5.Height, shelf.MerchandisableHeight, "shelf should now be overfit");
            Assert.AreEqual(p5.Product.Height * 0.5F, p5.UnitHeight, "units should be at max squeeze");
        }

        [Test]
        public void ProductSqueeze_AppliedToXCap()
        {
            //V8-29028 - Checks that x capping gets correctly squeezed

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 2;
            p3.FacingsWide = 1;
            p4.FacingsWide = 5;
            p5.FacingsWide = 1;

            //add a right cap
            p5.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            p5.FacingsXDeep = 1;
            p5.FacingsXHigh = 1;
            p5.FacingsXWide = 1;

            //planView.Model.Parent.SaveAs(1, @"C:\users\usr145\before.pog");

            //start by only allowing squeeze by product width
            p5.Product.SqueezeHeight = 1;
            p5.Product.SqueezeWidth = 0.5F;
            planView.EndUpdate();
            p5.Product.SqueezeDepth = 1;

            //planView.Model.Parent.SaveAs(1, @"C:\users\usr145\after.pog");

            //check that the width was fully squeezed
            Assert.AreEqual(0.5F, p5.Model.HorizontalSqueeze, "Main block should have been fully squeezed");
            Assert.AreEqual(1, p5.Model.HorizontalSqueezeX, "X block should not have been squeezed");

            //allow the cap to squeeze
            p5.Product.SqueezeDepth = 0.5F;
            Assert.AreEqual(0.5F, p5.Model.HorizontalSqueeze, "Main block should have been squeezed to min");
            Assert.AreEqual(0.5F, p5.Model.HorizontalSqueezeX, "X block should have been squeezed to min");
            AssertHelper.AreSinglesEqual(21, p5.Width, "pos should have been squeezed enough but it will overfit the shelf");

            //now set p1 to squeeze too -the squeeze amount should be spread evenly so p5 should now be a little wider.
            Single p5Width = p5.Width;
            p1.Product.SqueezeWidth = 0.5F;

            Assert.AreNotEqual(1, p1.Model.HorizontalSqueeze, "p1 should have been squeezed");
            Assert.AreNotEqual(1, p5.Model.HorizontalSqueeze, "p5 Main block should still be squeezed");
            Assert.AreNotEqual(1, p5.Model.HorizontalSqueezeX, "p5 X block should still be squeezed");
            Assert.Greater(p5.Width, p5Width, "p5 width should have increased");
        }

        [Test]
        public void ProductSqueeze_AppliedToYCap()
        {
            //V8-29028 - Checks that y capping gets correctly squeezed

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 2;
            p3.FacingsWide = 1;
            p4.FacingsWide = 5;
            p5.FacingsWide = 1;

            //apply squeeze to p5

            p5.Product.SqueezeHeight = 0.5F;
            p5.Product.SqueezeWidth = 0.5F;
            p5.Product.SqueezeDepth = 0.5F;

            // add a shelf above, merch height = 0, c
            shelf.Component.Y = 105;
            shelf.MerchandisableHeight = 0;
            PlanogramSubComponentView shelf2 = shelf.Fixture.AddComponent(PlanogramComponentType.Shelf).SubComponents[0];
            shelf2.Component.Y = 145;

            //add a top cap
            p5.FacingsDeep = 1;
            p5.FacingsYHigh = 1;
            p5.FacingsYDeep = 1;
            planView.EndUpdate();
            p5.FacingsYWide = 1;
            p5.OrientationTypeY = PlanogramPositionOrientationType.Top0;
            p5.IsYPlacedBottom = false;

            //check top cap got squeezed.
            PlanogramPositionDetails p5Details = p5.Model.GetPositionDetails();

            AssertHelper.AreSinglesEqual(p5Details.MainTotalSize.Width,
                                         p5Details.YTotalSize.Width,
                                         "Top cap should have been squeezed to the same width as the bottom");

            Assert.AreNotEqual(1, p5.Model.VerticalSqueezeY, "top cap should have been squeezed");
            Assert.AreNotEqual(p5Details.MainTotalSize.Depth, p5Details.YTotalSize.Height, "top cap should have been squeezed");
        }

        [Test]
        public void ProductSqueeze_AppliedToZCap()
        {
            //V8-29028 - Checks that z capping gets correctly squeezed

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 2;
            p3.FacingsWide = 1;
            p4.FacingsWide = 5;
            p5.FacingsWide = 1;

            //apply squeeze to p5
            p5.Product.SqueezeHeight = 1;
            p5.Product.SqueezeWidth = 0.5F;
            p5.Product.SqueezeDepth = 0.5F;

            //add a z cap
            p5.FacingsZDeep = 1;
            p5.FacingsZHigh = 1;
            p5.FacingsZWide = 1;
            planView.EndUpdate();
            p5.OrientationTypeZ = PlanogramPositionOrientationType.Front0;

            //check that z is now squeezed by width
            Assert.AreNotEqual(1, p5.Model.HorizontalSqueeze);
            Assert.AreEqual(p5.Model.HorizontalSqueeze, p5.Model.HorizontalSqueezeZ, "cap should be squeezed to match");

            //check that neither is squeezed by depth
            Assert.AreEqual(1, p5.Model.DepthSqueeze, "main should not be squeezed by depth");
            Assert.AreEqual(1, p5.Model.DepthSqueezeZ, "Z cap should not be squeezed by depth");

            //now add a couple more facings to overfit by depth
            p5.FacingsDeep = 4;

            //check both are now squeezed
            Assert.AreNotEqual(1, p5.Model.DepthSqueeze, "main should now be squeezed by depth");
            Assert.AreNotEqual(1, p5.Model.DepthSqueezeZ, "Z cap should now be squeezed by depth");

            AssertHelper.AreSinglesEqual(shelf.Depth, p5.Depth, "should be squeezed to fit shelf depth");
        }

        [Test]
        public void ProductSqueeze_OnlyAppliedToUnitMerchStyle()
        {
            //V8-29028 - Checks that squeeze is only applied if the merchandising style = unit.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //place enough facings to squeeze the product
            p5.FacingsDeep = 5;
            planView.EndUpdate();
            p5.Product.SqueezeDepth = 0.5F;
            Assert.AreEqual(p5.Depth, shelf.Depth, "Product should have been squeezed to fill shelf");

            //change the merch style
            p5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Alternate;
            Assert.Greater(p5.Depth, shelf.Depth, "Product should hnot have been squeezed");

            p5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Case;
            Assert.Greater(p5.Depth, shelf.Depth, "Product should hnot have been squeezed");

            p5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Display;
            Assert.Greater(p5.Depth, shelf.Depth, "Product should hnot have been squeezed");

            p5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.PointOfPurchase;
            Assert.Greater(p5.Depth, shelf.Depth, "Product should hnot have been squeezed");

            p5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            Assert.Greater(p5.Depth, shelf.Depth, "Product should hnot have been squeezed");

            p5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            Assert.AreEqual(p5.Depth, shelf.Depth, "Product should have been squeezed");
        }

        [Test]
        public void ProductSqueeze_DoesNotGoOverMax()
        {
            //V8-29028 - Checks that if I have 2 products to squeeze that neither can go beyond its max.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            // set up values
            p1.FacingsWide = 1;
            p1.Product.SqueezeWidth = 0.5F;

            p2.FacingsWide = 2;
            p3.FacingsWide = 2;
            p4.FacingsWide = 3;

            p5.FacingsWide = 1;
            planView.EndUpdate();
            p5.Product.SqueezeWidth = 0.5F;

            //check initial squeeze is equally spread between the 2 products
            AssertHelper.AreSinglesEqual(p1.Width, 5.35F, "p1 should have hit minimum width");
            AssertHelper.AreSinglesEqual(p5.Width, 22.15F, "p5 should have squeezed to make up the difference.");

            //change the max squeeze on p1.
            p1.Product.SqueezeWidth = 0.9F; //max squeeze set to 90%

            //check that p1 did not break max.
            AssertHelper.AreSinglesEqual(p1.HorizontalSqueeze, 0.9F, "p1 should have hit max squeeze");
            AssertHelper.AreSinglesEqual(p1.Width, 7.38F, "p1 should have hit minimum width");

            //check that p5 has squeezed correctly.
            AssertHelper.AreSinglesEqual(p5.Width, 20.12F, "p5 should have squeezed to make up the difference.");
        }

        [Test]
        public void ProductSqueeze_SqueezetoZCapButNotMain()
        {
            //V8-29028 -checks that squeezing the z cap only affects the main if required.

            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 1;
            p2.FacingsWide = 2;
            p3.FacingsWide = 2;
            p4.FacingsWide = 3;
            p5.FacingsWide = 1;

            //apply squeeze to p5 and p1.
            p1.Product.SqueezeWidth = 0.5F;
            p5.Product.SqueezeHeight = 1;
            p5.Product.SqueezeWidth = 0.5F;
            p5.Product.SqueezeDepth = 1;

            //add a z cap to p5
            //put more facings wide on z cap than on main.
            p5.FacingsZDeep = 1;
            p5.FacingsZHigh = 1;
            planView.EndUpdate();
            p5.FacingsZWide = 2;

            //check squeeze
            Assert.AreEqual(0.5F,
                            p5.Model.HorizontalSqueezeZ,
                            "The z cap units should be fully squeezed");

            Assert.Less(p5.Model.HorizontalSqueezeZ,
                        p5.Model.HorizontalSqueeze,
                        "the z cap should be squeezed more than the main block is.");
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void ProductSqueeze_X(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = xStrategy;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 3;
            p3.FacingsWide = 1;
            p4.FacingsWide = 4;
            p5.FacingsWide = 1;

            //allow p1 and p5 to squeeze
            p1.Product.SqueezeWidth = 0.5F;
            planView.EndUpdate();
            p5.Product.SqueezeWidth = 0.5F;

            //check
            switch (xStrategy)
            {
                default:
                    throw new InconclusiveException("TODO");

                case PlanogramSubComponentXMerchStrategyType.LeftStacked:
                case PlanogramSubComponentXMerchStrategyType.RightStacked:
                case PlanogramSubComponentXMerchStrategyType.Even:
                    {
                        Assert.AreNotEqual(1, p1.HorizontalSqueeze, "p1 should be squeezed");
                        Assert.AreEqual(1, p2.HorizontalSqueeze, "p2 should not be squeezed");
                        Assert.AreEqual(1, p3.HorizontalSqueeze, "p3 should not be squeezed");
                        Assert.AreEqual(1, p4.HorizontalSqueeze, "p4 should not be squeezed");
                        Assert.AreNotEqual(1, p5.HorizontalSqueeze, "p5 should be squeezed");

                        AssertHelper.AreSinglesEqual(0, p1.X);
                        AssertHelper.AreSinglesEqual(13.45F, p2.X);
                        AssertHelper.AreSinglesEqual(38.95F, p3.X);
                        AssertHelper.AreSinglesEqual(63.95F, p4.X);
                        AssertHelper.AreSinglesEqual(97.95F, p5.X);
                    }
                    break;

                case PlanogramSubComponentXMerchStrategyType.Left:
                case PlanogramSubComponentXMerchStrategyType.Right:
                case PlanogramSubComponentXMerchStrategyType.Manual:
                    {
                        Assert.AreEqual(1, p1.HorizontalSqueeze, "p1 should not be squeezed");
                        Assert.AreEqual(1, p2.HorizontalSqueeze, "p2 should not be squeezed");
                        Assert.AreEqual(1, p3.HorizontalSqueeze, "p3 should not be squeezed");
                        Assert.AreEqual(1, p4.HorizontalSqueeze, "p4 should not be squeezed");
                        Assert.AreEqual(1, p5.HorizontalSqueeze, "p5 should not be squeezed");
                    }
                    break;
            }
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void ProductSqueeze_XDividersPerFacing(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            //V8- 29028 - checks that squeeze is applied correctly
            // when dividers per facing is on.

            //create the initial plan with products
            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(CreateShelfTestPlan().Parent, shouldCalculateMetadata: false);
            var planView = pkView.PlanogramViews.First();
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            shelf.RemovePosition(shelf.Positions[4]);
            shelf.RemovePosition(shelf.Positions[3]);
            shelf.RemovePosition(shelf.Positions[2]);
            shelf.RemovePosition(shelf.Positions[1]);

            shelf.MerchandisingStrategyX = xStrategy;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;


            //allow p1 to squeeze
            p1.Product.SqueezeWidth = 0.5F;

            //set dividers
            shelf.DividerObstructionDepth = 1;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.IsDividerObstructionAtStart = true;
            shelf.IsDividerObstructionAtEnd = true;
            shelf.IsDividerObstructionByFacing = true;

            //check unsqueezed
            planView.EndUpdate();
            p1.FacingsWide = 12;
            Assert.AreEqual(1, p1.HorizontalSqueeze, "p1 should not have squeezed");
            Assert.Less(p1.Width, shelf.Width, "p1 should be smaller than the shelf space");


            if (xStrategy == PlanogramSubComponentXMerchStrategyType.Manual)
            {
                //manual should not squeeze at all
                p1.FacingsWide = 13;
                Assert.AreEqual(1, p1.HorizontalSqueeze, "p1 should not have squeezed");

                //squeeze again
                p1.FacingsWide = 14;
                Assert.AreEqual(1, p1.HorizontalSqueeze, "p1 should not have squeezed");
            }
            else
            {
                //check with a little squeeze
                p1.FacingsWide = 13;
                Assert.Less(p1.HorizontalSqueeze, 1, "p1 should have squeezed");
                Assert.Less(p1.Width, shelf.Width, "p1 should be smaller than the shelf space");

                //squeeze again
                p1.FacingsWide = 14;
                Assert.Less(p1.HorizontalSqueeze, 1, "p1 should have squeezed");
                Assert.Less(p1.Width, shelf.Width, "p1 should be smaller than the shelf space");

                //remove start and end dividers - p1 should be squeezed a bit less
                shelf.IsDividerObstructionAtStart = false;
                shelf.IsDividerObstructionAtEnd = false;
                Assert.Less(p1.HorizontalSqueeze, 1, "p1 should have squeezed");
                AssertHelper.AreSinglesEqual(p1.Width, shelf.Width, "p1 should be equal to the shelf space.");
            }
        }

        [Test, TestCaseSource("XPlacementStrategies")]
        public void ProductSqueeze_XDividers(PlanogramSubComponentXMerchStrategyType xStrategy)
        {
            //V8- 29028 - checks that squeeze is applied correctly 
            // when x dividers are on.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = xStrategy;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //overfit the shelf.
            p1.FacingsWide = 1;
            p2.FacingsWide = 2;
            p3.FacingsWide = 1;
            p4.FacingsWide = 4;
            p5.FacingsWide = 1;

            //allow squeeze on p1 and p5.
            p1.Product.SqueezeWidth = 0.5F;
            p5.Product.SqueezeWidth = 0.5F;

            //apply dividers
            shelf.DividerObstructionDepth = 1;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionSpacingX = 5;
            shelf.IsDividerObstructionAtStart = true;
            planView.EndUpdate();
            shelf.IsDividerObstructionAtEnd = true;

            //check
            if (xStrategy == PlanogramSubComponentXMerchStrategyType.Manual
                ||
                xStrategy == PlanogramSubComponentXMerchStrategyType.Left
                ||
                xStrategy == PlanogramSubComponentXMerchStrategyType.Right)
            {
                //products should not get squeezed
                Assert.AreEqual(1, p1.HorizontalSqueeze, "p1 can squeeze but it should not have needed to.");
                Assert.AreEqual(1, p2.HorizontalSqueeze, "p2 is not allowed to squeeze");
                Assert.AreEqual(1, p3.HorizontalSqueeze, "p3 is not allowed to squeeze");
                Assert.AreEqual(1, p4.HorizontalSqueeze, "p4 is not allowed to squeeze");
                Assert.AreEqual(1, p5.HorizontalSqueeze, "p5 should not have squeezed");
            }
            else
            {
                Assert.AreEqual(6, shelf.Dividers.Count, "Should have 6 dividers");
                Assert.AreEqual(1, p1.HorizontalSqueeze, "p1 can squeeze but it should not have needed to.");
                Assert.AreEqual(1, p2.HorizontalSqueeze, "p2 is not allowed to squeeze");
                Assert.AreEqual(1, p3.HorizontalSqueeze, "p3 is not allowed to squeeze");
                Assert.AreEqual(1, p4.HorizontalSqueeze, "p4 is not allowed to squeeze");
                Assert.Greater(1, p5.HorizontalSqueeze, "p5 should have squeezed");

                AssertHelper.AreEqual(0, shelf.Dividers[0].X, "div 1 position is incorrect");
                AssertHelper.AreEqual(10, shelf.Dividers[1].X, "div 2 position is incorrect");
                AssertHelper.AreEqual(30, shelf.Dividers[2].X, "div 3 position is incorrect");
                AssertHelper.AreEqual(60, shelf.Dividers[3].X, "div 4 position is incorrect");
                AssertHelper.AreEqual(95, shelf.Dividers[4].X, "div 5 position is incorrect");
                AssertHelper.AreEqual(115, shelf.Dividers[5].X, "div 6 position is incorrect");
            }
        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void ProductSqueeze_Y(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.Component.Y = 0;
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //place products on top of one another
            shelf.MovePosition(p2, p1, PlanogramPositionAnchorDirection.Above);
            shelf.MovePosition(p3, p2, PlanogramPositionAnchorDirection.Above);
            shelf.MovePosition(p4, p3, PlanogramPositionAnchorDirection.Above);
            shelf.MovePosition(p5, p4, PlanogramPositionAnchorDirection.Above);

            //overfit
            p1.FacingsHigh = 2;
            p2.FacingsHigh = 1;
            p3.FacingsHigh = 2;
            p4.FacingsHigh = 1;
            p5.FacingsHigh = 1;

            //allow squeeze
            p1.Product.SqueezeHeight = 0.5F;
            planView.EndUpdate();
            p5.Product.SqueezeHeight = 0.5F;

            //set the strategy to test
            shelf.MerchandisingStrategyY = yStrategy;

            switch (yStrategy)
            {
                default:
                    throw new NotImplementedException();

                case PlanogramSubComponentYMerchStrategyType.Bottom:
                case PlanogramSubComponentYMerchStrategyType.Top:
                case PlanogramSubComponentYMerchStrategyType.Manual:
                    Assert.AreEqual(1, p1.VerticalSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p2.VerticalSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p3.VerticalSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p4.VerticalSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p5.VerticalSqueeze, "no squeeze should be applied");
                    break;

                case PlanogramSubComponentYMerchStrategyType.BottomStacked:
                case PlanogramSubComponentYMerchStrategyType.TopStacked:
                case PlanogramSubComponentYMerchStrategyType.Even:
                    Assert.AreNotEqual(1, p1.VerticalSqueeze, "p1 should be squeezed");
                    Assert.AreEqual(1, p2.VerticalSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p3.VerticalSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p4.VerticalSqueeze, "no squeeze should be applied");
                    Assert.AreNotEqual(1, p5.VerticalSqueeze, "p5 should be squeezed");
                    break;
            }
        }

        [Test, TestCaseSource("YPlacementStrategies")]
        public void ProductSqueeze_YDividers(PlanogramSubComponentYMerchStrategyType yStrategy)
        {
            //V8- 29028 - checks that squeeze is applied correctly 
            // when y dividers are on.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.Component.Y = 0;
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //place products on top of one another
            shelf.MovePosition(p2, p1, PlanogramPositionAnchorDirection.Above);
            shelf.MovePosition(p3, p2, PlanogramPositionAnchorDirection.Above);
            shelf.MovePosition(p4, p3, PlanogramPositionAnchorDirection.Above);
            shelf.MovePosition(p5, p4, PlanogramPositionAnchorDirection.Above);

            //overfit
            p1.FacingsHigh = 2;
            p2.FacingsHigh = 1;
            p3.FacingsHigh = 2;
            p4.FacingsHigh = 1;
            p5.FacingsHigh = 1;

            //allow squeeze
            p1.Product.SqueezeHeight = 0.5F;
            p5.Product.SqueezeHeight = 0.5F;

            //apply dividers
            shelf.DividerObstructionDepth = 1;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionSpacingY = 5;
            shelf.IsDividerObstructionAtStart = true;
            shelf.IsDividerObstructionAtEnd = true;

            //set the strategy to test
            planView.EndUpdate();
            shelf.MerchandisingStrategyY = yStrategy;

            switch (yStrategy)
            {
                default:
                    Assert.Ignore("TODO");
                    break;
            }
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void ProductSqueeze_Z(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            //place products on behind one another
            p1.FacingsDeep = 1;
            p2.FacingsDeep = 1;
            p3.FacingsDeep = 1;
            p4.FacingsDeep = 1;
            p5.FacingsDeep = 1;

            shelf.MovePosition(p2, p1, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p3, p2, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p4, p3, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p5, p4, PlanogramPositionAnchorDirection.Behind);

            //overfit
            p1.FacingsDeep = 2;
            p2.FacingsDeep = 1;
            p3.FacingsDeep = 2;
            p4.FacingsDeep = 1;
            p5.FacingsDeep = 1;

            //allow squeeze
            p1.Product.SqueezeDepth = 0.5F;
            planView.EndUpdate();
            p5.Product.SqueezeDepth = 0.5F;

            //set the strategy to test
            shelf.MerchandisingStrategyZ = zStrategy;

            switch (zStrategy)
            {
                default:
                    throw new NotImplementedException();

                case PlanogramSubComponentZMerchStrategyType.Manual:
                case PlanogramSubComponentZMerchStrategyType.Front:
                case PlanogramSubComponentZMerchStrategyType.Back:
                    Assert.AreEqual(1, p1.DepthSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p2.DepthSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p3.DepthSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p4.DepthSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p5.DepthSqueeze, "no squeeze should be applied");
                    break;

                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                case PlanogramSubComponentZMerchStrategyType.Even:
                    Assert.AreNotEqual(1, p1.DepthSqueeze, "p1 should be squeezed");
                    Assert.AreEqual(1, p2.DepthSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p3.DepthSqueeze, "no squeeze should be applied");
                    Assert.AreEqual(1, p4.DepthSqueeze, "no squeeze should be applied");
                    Assert.AreNotEqual(1, p5.DepthSqueeze, "p5 should be squeezed");
                    break;
            }
        }

        [Test, TestCaseSource("ZPlacementStrategies")]
        public void ProductSqueeze_ZDividers(PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            //V8- 29028 - checks that squeeze is applied correctly 
            // when z dividers are on.

            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.FrontStacked;

            //place products on behind one another
            p1.FacingsDeep = 1;
            p2.FacingsDeep = 1;
            p3.FacingsDeep = 1;
            p4.FacingsDeep = 1;
            p5.FacingsDeep = 1;

            shelf.MovePosition(p2, p1, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p3, p2, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p4, p3, PlanogramPositionAnchorDirection.Behind);
            shelf.MovePosition(p5, p4, PlanogramPositionAnchorDirection.Behind);

            //overfit
            p1.FacingsDeep = 2;
            p2.FacingsDeep = 2;
            p3.FacingsDeep = 2;
            p3.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            p4.FacingsDeep = 1;
            p5.FacingsDeep = 1;

            //allow squeeze
            p1.Product.SqueezeDepth = 0.5F;
            p5.Product.SqueezeDepth = 0.5F;

            //apply dividers
            shelf.DividerObstructionDepth = 1;
            shelf.DividerObstructionHeight = 1;
            shelf.DividerObstructionWidth = 1;
            shelf.DividerObstructionSpacingZ = 5;
            shelf.IsDividerObstructionAtStart = true;
            planView.EndUpdate();
            shelf.IsDividerObstructionAtEnd = true;

            //set the strategy to test
            //planView.Model.Parent.SaveAs(1, @"c:\users\usr145\before.pog");
            shelf.MerchandisingStrategyZ = zStrategy;
            //planView.Model.Parent.SaveAs(1, @"c:\users\usr145\after.pog");


            switch (zStrategy)
            {
                default:
                    Assert.Ignore("TODO");
                    break;

                case PlanogramSubComponentZMerchStrategyType.Back:
                case PlanogramSubComponentZMerchStrategyType.Front:
                case PlanogramSubComponentZMerchStrategyType.Manual:
                    {
                        Assert.AreEqual(1, p1.DepthSqueeze, "no squeeze should be applied");
                        Assert.AreEqual(1, p2.DepthSqueeze, "no squeeze should be applied");
                        Assert.AreEqual(1, p3.DepthSqueeze, "no squeeze should be applied");
                        Assert.AreEqual(1, p4.DepthSqueeze, "no squeeze should be applied");
                        Assert.AreEqual(1, p5.DepthSqueeze, "no squeeze should be applied");
                    }
                    break;

                case PlanogramSubComponentZMerchStrategyType.FrontStacked:
                    {
                        throw new InconclusiveException("TODO");

                        // somthing is still not quite right here - positions are squeezed too much
                    }

                #region BackStacked

                case PlanogramSubComponentZMerchStrategyType.BackStacked:
                    {
                        throw new InconclusiveException("TODO");

                        // somthing is still not quite right here - positions are squeezed too much
                    }

                #endregion
            }
        }

        [Test]
        public void ProductSqueeze_IsProductSqueezeAllowed()
        {
            //create the initial plan with products
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            //make the component overfitted.
            p1.FacingsWide = 2;
            p2.FacingsWide = 3;
            p3.FacingsWide = 1;
            p4.FacingsWide = 4;
            p5.FacingsWide = 1;

            //allow p1 and p5 to squeeze
            planView.EndUpdate();
            p1.Product.SqueezeWidth = 0.5F;
            p5.Product.SqueezeWidth = 0.5F;
            shelf.IsProductSqueezeAllowed = true;
            Assert.AreNotEqual(1, p1.HorizontalSqueeze, "p1 should be squeezed");
            Assert.AreEqual(1, p2.HorizontalSqueeze, "p2 should not be squeezed");
            Assert.AreEqual(1, p3.HorizontalSqueeze, "p3 should not be squeezed");
            Assert.AreEqual(1, p4.HorizontalSqueeze, "p4 should not be squeezed");
            Assert.AreNotEqual(1, p5.HorizontalSqueeze, "p5 should be squeezed");

            //turn setting off
            shelf.IsProductSqueezeAllowed = false;
            Assert.AreEqual(1, p1.HorizontalSqueeze, "p1 should not be squeezed");
            Assert.AreEqual(1, p2.HorizontalSqueeze, "p2 should not be squeezed");
            Assert.AreEqual(1, p3.HorizontalSqueeze, "p3 should not be squeezed");
            Assert.AreEqual(1, p4.HorizontalSqueeze, "p4 should not be squeezed");
            Assert.AreEqual(1, p5.HorizontalSqueeze, "p5 should not be squeezed");

            //turn setting on
            shelf.IsProductSqueezeAllowed = true;
            Assert.AreNotEqual(1, p1.HorizontalSqueeze, "p1 should be squeezed");
            Assert.AreEqual(1, p2.HorizontalSqueeze, "p2 should not be squeezed");
            Assert.AreEqual(1, p3.HorizontalSqueeze, "p3 should not be squeezed");
            Assert.AreEqual(1, p4.HorizontalSqueeze, "p4 should not be squeezed");
            Assert.AreNotEqual(1, p5.HorizontalSqueeze, "p5 should be squeezed");
        }

        #endregion

        #region Position Tests

        [Test]
        public void Position_FingerSpaceToTheSideLeftFlow()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //add finger space to p2 - this has multiple facings.
            planView.EndUpdate();
            p2.Product.FingerSpaceToTheSide = 1;

            AssertPlacement(0, 4, 2.7F, p1);
            AssertPlacement(8.2F, 4, 0F, p2);
            AssertPlacement(27.2F, 4, 37.5F, p3);
            AssertPlacement(52.2F, 4, 0F, p4);
            AssertPlacement(60.7F, 4, 42.5F, p5);
        }

        [Test]
        public void Position_FingerSpaceToTheSideRightFlow()
        {
            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView p1 = shelf.Positions[0];
            PlanogramPositionView p2 = shelf.Positions[1];
            PlanogramPositionView p3 = shelf.Positions[2];
            PlanogramPositionView p4 = shelf.Positions[3];
            PlanogramPositionView p5 = shelf.Positions[4];

            //add finger space to p2 - this has multiple facings.
            planView.EndUpdate();
            p2.Product.FingerSpaceToTheSide = 1;

            AssertPlacement(34.3F, 4F, 2.7F, p1);
            AssertPlacement(43.5F, 4F, 0F, p2);
            AssertPlacement(61.5F, 4F, 37.5F, p3);
            AssertPlacement(86.5F, 4F, 0F, p4);
            AssertPlacement(95F, 4F, 42.5F, p5);
        }


        [Test]
        public void Position_FingerSpaceAbove()
        {
            Assert.Ignore("TODO");
        }

        [Test]
        public void Position_CreatePositionWithManualStrategyOverlapNotAllowed()
        {
            // This test has been created as placeholder for testing Gemini ticket V8-27203 also

            var planView = new PlanogramView(CreateShelfTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView shelf =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Shelf);
            shelf.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelf.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf.IsProductOverlapAllowed = false;

            // Add new product
            PlanogramProductView addProduct = planView.AddProduct();
            addProduct.Gtin = "p6";
            addProduct.Name = "p6";
            addProduct.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            addProduct.Height = 23F;
            addProduct.Width = 1.5F;
            addProduct.Depth = 1.2F;

            shelf.AddPosition(addProduct);

            List<PlanogramPositionView> positions = shelf.Positions.OrderBy(p => p.Product.Gtin).ToList();

            PlanogramPositionView p1 = positions[0];
            PlanogramPositionView p2 = positions[1];
            PlanogramPositionView p3 = positions[2];
            PlanogramPositionView p4 = positions[3];
            PlanogramPositionView p5 = positions[4];
            PlanogramPositionView p6 = positions[5];
            planView.EndUpdate();

            // Ensure that the plan is re-merchandised before asserting.
            using (PlanogramMerchandisingGroupList merchGroups = planView.Model.GetMerchandisingGroups())
            {
                merchGroups.ForEach(g =>
                                    {
                                        g.Process();
                                        g.ApplyEdit();
                                    });
            }

            AssertPlacement(0, 4, 2.7F, p1);
            AssertPlacement(8.2F, 4, 75.3F, p6);
            AssertPlacement(17.37F, 4, 0F, p2);
            AssertPlacement(44.05F, 4, 37.5F, p3);
            AssertPlacement(77.92F, 4, 0F, p4);
            AssertPlacement(95F, 4, 42.5F, p5);
        }

        #endregion

        #region Moving

        private static Planogram CreateMovePositionWithAnchorTestPlan()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120.6F;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a pegboard
            PlanogramFixtureComponent peg1Fc = fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 100, 1F);
            PlanogramComponent peg1C = plan.Components.FindById(peg1Fc.PlanogramComponentId);

            peg1Fc.Y = 50;

            PlanogramSubComponent pegSub = peg1C.SubComponents[0];
            pegSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            pegSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            pegSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            pegSub.IsProductOverlapAllowed = true;
            pegSub.MerchConstraintRow1StartX = 4;
            pegSub.MerchConstraintRow1SpacingX = 4;
            pegSub.MerchConstraintRow1StartY = 4;
            pegSub.MerchConstraintRow1SpacingY = 4;
            pegSub.MerchConstraintRow1Height = 1;
            pegSub.MerchConstraintRow1Width = 1;

            PlanogramSubComponentPlacement subComponentPlacement =
                PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(pegSub, fixtureItem, peg1Fc);

            //Product 1 (w: 8.2, h: 32.2, d: 24.6)
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.Height = 34.4F;
            prod1.Width = 9.7F;
            prod1.Depth = 9.7F;
            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 4;
            pos1.FacingsDeep = 2;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 127.75F;
            pos1.Y = 154.1F;
            pos1.Z = 1F;

            //Product 2- (w: 17, h: 32, d: 17)
            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 21.5F;
            prod2.Width = 20F;
            prod2.Depth = 13.2F;
            plan.Products.Add(prod2);

            PlanogramPosition pos2 = subComponentPlacement.AddPosition(prod2);
            pos2.FacingsHigh = 1;
            pos2.FacingsWide = 1;
            pos2.FacingsDeep = 5;
            pos2.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos2.OrientationType = PlanogramPositionOrientationType.Front0;
            pos2.X = 186.6F;
            pos2.Y = 167F;
            pos2.Z = 1;

            //Product 3 - (w: 25, h:23.2 , d:12 )
            PlanogramProduct prod3 = PlanogramProduct.NewPlanogramProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod3.Height = 32.6F;
            prod3.Width = 9.5F;
            prod3.Depth = 9.5F;
            plan.Products.Add(prod3);

            PlanogramPosition pos3 = subComponentPlacement.AddPosition(prod3);
            pos3.FacingsHigh = 1;
            pos3.FacingsWide = 2;
            pos3.FacingsDeep = 3;
            pos3.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            pos3.OrientationType = PlanogramPositionOrientationType.Front0;
            pos3.X = 215.85F;
            pos3.Y = 155.9F;
            pos3.Z = 1F;


            //Int16 seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.X).GroupBy(p => p.X))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceX = seq;
            //    }
            //    seq++;
            //}

            //seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.Y).GroupBy(p => p.Y))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceY = seq;
            //    }
            //    seq++;
            //}

            //seq = 1;
            //foreach (var group in plan.Positions.OrderBy(p => p.Z).GroupBy(p => p.Z))
            //{
            //    foreach (PlanogramPosition pos in group)
            //    {
            //        pos.SequenceZ = seq;
            //    }
            //    seq++;
            //}

            ////output plan if required
            //Package pk = Package.NewPackage();
            //pk.Name = "ShelfTest";
            //pk.Planograms.Add(plan);
            //pk.SaveAs(@"C:\Users\usr095\pegTest.pog");


            return plan;
        }

        [Test]
        public void MovePositionWithAnchor_PegboardMoveBelow1()
        {
            var planView = new PlanogramView(CreateMovePositionWithAnchorTestPlan(), shouldCalculateMetadata: false);
            planView.BeginUpdate();
            PlanogramSubComponentView pegboard =
                planView.EnumerateAllSubComponents().First(s => s.Component.ComponentType == PlanogramComponentType.Peg);

            PlanogramPositionView p1 = pegboard.Positions[0];
            PlanogramPositionView p2 = pegboard.Positions[1];
            PlanogramPositionView p3 = pegboard.Positions[2];

            pegboard.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboard.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;

            planView.EndUpdate();
            pegboard.MovePosition(p2, p1, PlanogramPositionAnchorDirection.Below);

            AssertSequence(1, 2, 1, p1);
            AssertSequence(1, 1, 1, p2);
            AssertSequence(2, 2, 1, p3);
        }

        [Test]
        public void MovePosition_BetweenShelfAndPegboard()
        {
            //Check that when moving between shelf and pegboard the 
            // position correctly resets its placement

            // if the position hovers above the shelf after being placed in the pegboard this can 
            // indicate that it is still attached to an old PlanogramPositionPlacement handler which
            // is causing it to snap to pegholes.


            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView shelfComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            PlanogramComponentView pegComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Peg);

            //Add a position to the shelf
            PlanogramPositionView p1 = shelfComponent.SubComponents[0].AddPosition(plan.Products[0]);
            Assert.AreEqual(shelfComponent.Height, p1.Y, "position should start on the shelf.");

            //move to the pegboard
            p1 = pegComponent.SubComponents[0].MovePosition(p1);

            //move back to the shelf
            plan.EndUpdate();
            p1 = shelfComponent.SubComponents[0].MovePosition(p1);
            Assert.AreEqual(shelfComponent.Height, p1.Y, "position should be back on the shelf.");
        }

        #endregion

        #region General Rules

        [Test]
        public void Rule_AllowProductDimensionsLessThan1()
        {
            //V8-27824 - Check that products can have a depth of less than 1.

            PlanogramView plan = CreateEmptyPlan();
            plan.BeginUpdate();
            PlanogramComponentView shelfComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponentView shelfSub = shelfComponent.SubComponents[0];

            shelfSub.Depth = 1;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            plan.Products[0].Depth = 0.1F;

            PlanogramPositionView p1 = shelfSub.AddPosition(plan.Products[0]);
            p1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            plan.EndUpdate();
            p1.FacingsDeep = 10;

            //check placement
            Assert.AreEqual(0, p1.Z);
        }

        [Test]
        public void Rule_AllowMoreThan255Facings()
        {
            //v8-27823 - Checks that a position can have more than 255 facings.
            // This issue should be resolved by V8-30164.

            PlanogramView plan = CreateEmptyPlan();
            PlanogramComponentView shelfComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponentView shelfSub = shelfComponent.SubComponents[0];

            PlanogramPositionView p1 = shelfSub.AddPosition(plan.Products[0]);

            p1.FacingsHigh = 600;
            p1.FacingsWide = 600;
            p1.FacingsDeep = 600;
            p1.FacingsXHigh = 600;
            p1.FacingsXWide = 600;
            p1.FacingsXDeep = 600;
            p1.FacingsYHigh = 600;
            p1.FacingsYWide = 600;
            p1.FacingsYDeep = 600;
            p1.FacingsZHigh = 600;
            p1.FacingsZWide = 600;
            p1.FacingsZDeep = 600;
        }

        [Test]
        public void Rule_AddTrayProductToHangingAddsUnits()
        {
            //V8-27395 - Check that adding a tray product to a hanging sub
            // adds units.

            PlanogramView plan = CreateEmptyPlan();
            PlanogramComponentView pegComponent = plan.Fixtures[0].AddComponent(PlanogramComponentType.Peg);
            PlanogramSubComponentView pegSub = pegComponent.SubComponents[0];
            Assert.AreEqual(PlanogramSubComponentMerchandisingType.Hang, pegSub.MerchandisingType);

            //check the product to be added is a tray one.
            Assert.AreEqual(PlanogramProductMerchandisingStyle.Tray, plan.Products[2].MerchandisingStyle);

            //add it
            PlanogramPositionView p1 = pegSub.AddPosition(plan.Products[2]);
            Assert.AreNotEqual(PlanogramPositionMerchandisingStyle.Tray, p1.MerchandisingStyle, "Should not have added as a tray");
            Assert.AreNotEqual(PlanogramPositionMerchandisingStyle.Default, p1.MerchandisingStyle, "Should not have added as a tray");

            //check for hang below type:
            plan.Fixtures[0].AddComponent(PlanogramComponentType.Rod);
            PlanogramSubComponentView rodSub = pegComponent.SubComponents[0];
            Assert.AreEqual(PlanogramSubComponentMerchandisingType.Hang, rodSub.MerchandisingType);

            PlanogramPositionView p2 = rodSub.AddPosition(plan.Products[2]);
            Assert.AreNotEqual(PlanogramPositionMerchandisingStyle.Tray, p2.MerchandisingStyle, "Should not have added as a tray");
            Assert.AreNotEqual(PlanogramPositionMerchandisingStyle.Default, p2.MerchandisingStyle, "Should not have added as a tray");
        }

        #endregion

        #region AutoFill

        private PlanogramView CreateAutoFillEmptyPlan()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;


            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 133;
            fixture.Depth = 67;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 67);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 57);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);
            shelf1Fc.Y = 58;
            PlanogramSubComponent shelf1S = shelf1C.SubComponents[0];
            shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1S.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1S.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1S.MerchandisableHeight = 0;
            shelf1S.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1S.IsProductSqueezeAllowed = true;
            shelf1S.IsProductOverlapAllowed = false;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planogramView = packageView.PlanogramViews.First();
            planogramView.ShouldCalculateMetadata = false;
            return planogramView;
        }

        #region High

        [Test]
        public void AutoFillHigh_AppliesCorrectTopCapsWide()
        {
            //V8-30198

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            //add a shelf above the existing one.
            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 58;
            PlanogramComponentView shelf2 = planView.Fixtures.First().AddComponent(PlanogramComponentType.Shelf);
            shelf2.Y = 108;

            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 29.5F;
            prod1.Width = 23.2F;
            prod1.Depth = 7F;
            prod1.MaxTopCap = 99;

            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 3;
            pos1.FacingsDeep = 8;
            pos1.FacingsYHigh = 1; //place less then could be fitted.
            pos1.FacingsYWide = 3;
            pos1.FacingsYDeep = 1;

            //turn on auto fill high for the plan
            planView.EndUpdate();
            planView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;


            //check the position is now correct
            Assert.AreEqual(1, pos1.FacingsHigh);
            Assert.AreEqual(3, pos1.FacingsWide);
            Assert.AreEqual(8, pos1.FacingsDeep);

            Assert.AreEqual(2, pos1.FacingsYHigh, "top cap high should have increased.");
            Assert.AreEqual(3, pos1.FacingsYWide);
            Assert.AreEqual(1, pos1.FacingsYDeep);
        }

        [Test]
        public void AutoFillHigh_AppliesCorrectRightCapsHigh()
        {
            //V8-30198
            // Checks that right caps are not placed on hanging products.

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();
            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 153; // move shelf to near the top.

            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 29F;
            prod1.Width = 19.2F;
            prod1.Depth = 6.8F;
            prod1.MaxTopCap = 99;
            prod1.MaxRightCap = 99;

            //add the position
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 2;
            pos1.FacingsDeep = 8;

            pos1.FacingsXHigh = 1;
            pos1.FacingsXWide = 2;
            pos1.FacingsXDeep = 2;

            pos1.FacingsYHigh = 0;
            pos1.FacingsYWide = 0;
            pos1.FacingsYDeep = 0;

            //turn on auto fill high for the plan
            planView.EndUpdate();
            planView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;

            Assert.AreEqual(1, pos1.FacingsHigh);
            Assert.AreEqual(2, pos1.FacingsWide);
            Assert.AreEqual(8, pos1.FacingsDeep);

            Assert.AreEqual(1, pos1.FacingsXHigh);
            Assert.AreEqual(2, pos1.FacingsXWide);
            Assert.AreEqual(2, pos1.FacingsXDeep);

            Assert.AreEqual(2, pos1.FacingsYHigh);
            Assert.AreEqual(2, pos1.FacingsYWide);
            Assert.AreEqual(1, pos1.FacingsYDeep);

            //move the shelf down
            shelf1.Y = 124;
            Assert.AreEqual(2, pos1.FacingsHigh);
            Assert.AreEqual(2, pos1.FacingsWide);
            Assert.AreEqual(8, pos1.FacingsDeep);

            Assert.AreEqual(2, pos1.FacingsXHigh);
            Assert.AreEqual(2, pos1.FacingsXWide);
            Assert.AreEqual(2, pos1.FacingsXDeep);

            Assert.AreEqual(2, pos1.FacingsYHigh);
            Assert.AreEqual(2, pos1.FacingsYWide);
            Assert.AreEqual(1, pos1.FacingsYDeep);
        }

        [Test]
        public void AutoFillHigh_DoesNotPutTopCapsOnHangingProducts()
        {
            //V8-30181
            // Checks that where products are hanging, the increase and
            // decrease units methods do not start placing top caps.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            plan.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;


            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 120;
            fixture.Depth = 76;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);


            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            //Add a clip strip
            PlanogramComponentView clipstrip = planView.Fixtures.First().AddComponent(PlanogramComponentType.ClipStrip);
            clipstrip.X = 22;
            clipstrip.Y = 78;
            clipstrip.Z = 0;
            clipstrip.Height = 100;
            clipstrip.Width = 4;
            clipstrip.Depth = 1;
            clipstrip.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            clipstrip.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            clipstrip.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            clipstrip.SubComponents[0].MerchConstraintRow1StartX = 2;
            clipstrip.SubComponents[0].MerchConstraintRow1SpacingX = 2;
            clipstrip.SubComponents[0].MerchConstraintRow1StartY = 4;
            clipstrip.SubComponents[0].MerchConstraintRow1SpacingY = 4;
            clipstrip.SubComponents[0].MerchConstraintRow1Height = 1;
            clipstrip.SubComponents[0].MerchConstraintRow1Width = 1;

            //Add product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 28.5F;
            prod1.Width = 19.8F;
            prod1.Depth = 5F;
            prod1.MaxTopCap = 99;
            prod1.MaxRightCap = 99;

            //add it to the clipstrip
            planView.EndUpdate();
            PlanogramPositionView pos1 = clipstrip.SubComponents.First().AddPosition(prod1);

            //check there are no caps
            Assert.AreEqual(0, pos1.FacingsYHigh);
            Assert.AreEqual(0, pos1.FacingsYWide);
            Assert.AreEqual(0, pos1.FacingsYDeep);
            Assert.AreEqual(0, pos1.FacingsXHigh);
            Assert.AreEqual(0, pos1.FacingsXWide);
            Assert.AreEqual(0, pos1.FacingsXDeep);
            Assert.AreEqual(0, pos1.FacingsZHigh);
            Assert.AreEqual(0, pos1.FacingsZWide);
            Assert.AreEqual(0, pos1.FacingsZDeep);

            Assert.AreEqual(3, pos1.FacingsHigh);
            Assert.AreEqual(1, pos1.FacingsWide);
            Assert.AreEqual(15, pos1.FacingsDeep);
        }

        [Test]
        public void AutoFillHigh_StillFillsWhenProductBreaksMerchHeight()
        {
            //V8-30173
            // Checks that where one single facing product breaks merch height, 
            // this does not cause others on the same sub to be placed at one
            // even though there is space above.

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            //add a shelf above the existing one.
            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 168;

            //add the products
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 15F;
            prod1.Width = 6.2F;
            prod1.Depth = 3.8F;
            prod1.MaxTopCap = 99;
            prod1.MaxRightCap = 99;

            PlanogramProductView prod2 = planView.AddProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.OrientationType = PlanogramProductOrientationType.Front0;
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 5F;
            prod2.Width = 6.3F;
            prod2.Depth = 6.5F;
            prod2.MaxTopCap = 99;
            prod2.MaxRightCap = 99;

            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod1);
            PlanogramPositionView pos2 = shelf1.SubComponents.First().AddPosition(prod2);

            //turn on auto fill high for the plan
            planView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;

            //now set the merch height to below the first product
            planView.EndUpdate();
            shelf1.SubComponents[0].MerchandisableHeight = 13;


            //now check that p2 was filled high correctly.
            Assert.AreEqual(1, pos1.FacingsHigh);
            Assert.AreEqual(0, pos1.FacingsYHigh);

            Assert.AreEqual(2, pos2.FacingsHigh);
            Assert.AreEqual(0, pos2.FacingsYHigh);
        }

        [Test]
        public void AutoFillHigh_CombinedShelvesWithDifferentSpaceAbove()
        {
            //V8-31638 - client raised.
            // Checks that where 2 combined shelves have different heights above, 
            // both still fill to the maximum available.
            var planView = CreateCombinedShelfTestPlanView();
            planView.BeginUpdate();

            PlanogramSubComponentView shelf1 =
                planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.Component.Y = 40;

            PlanogramSubComponentView shelf2 =
                planView.Fixtures[1].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            shelf2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf2.CombineType = PlanogramSubComponentCombineType.Both;
            shelf2.Component.Y = 40;


            //add another shelf above on the second bay
            PlanogramComponentView shelf3 = planView.Fixtures[1].AddComponent(PlanogramComponentType.Shelf);
            shelf3.Y = 120;
            shelf3.Depth = shelf2.Depth;


            PlanogramPositionView p1 = planView.EnumerateAllPositions().ElementAt(0);
            PlanogramPositionView p2 = planView.EnumerateAllPositions().ElementAt(1);
            PlanogramPositionView p3 = planView.EnumerateAllPositions().ElementAt(2);
            PlanogramPositionView p4 = planView.EnumerateAllPositions().ElementAt(3);
            PlanogramPositionView p5 = planView.EnumerateAllPositions().ElementAt(4);

            p1.FacingsWide = 1;
            p2.FacingsWide = 1;
            p3.FacingsWide = 1;
            p4.FacingsWide = 1;
            p5.FacingsWide = 1;

            //set the plan to fill high.
            planView.EndUpdate();
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;



            Assert.AreEqual(2, p1.FacingsHigh);//should not reduce -- now reduced because of obstructed space changes
            Assert.AreEqual(2, p2.FacingsHigh);//should not reduce -- now reduced because of obstructed space changes

            Assert.AreEqual(3, p3.FacingsHigh); //should reduce
            Assert.AreEqual(2, p4.FacingsHigh);//should reduce
            Assert.AreEqual(2, p5.FacingsHigh);//should reduce
        }

        [Test]
        public void AutoFillHigh_CombinedShelvesWithDifferentMerchHeights()
        {
            //V8-31638 - client raised.
            // Checks that where 2 combined shelves have different heights above, 
            // both still fill to the maximum available.
            var planView = CreateCombinedShelfTestPlanView();
            planView.BeginUpdate();

            PlanogramSubComponentView shelf1 =
                planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.Component.Y = 40;

            PlanogramSubComponentView shelf2 =
                planView.Fixtures[1].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            shelf2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf2.CombineType = PlanogramSubComponentCombineType.Both;
            shelf2.Component.Y = 40;


            //add another shelf above on the second bay
            //PlanogramComponentView shelf3 = planView.Fixtures[1].AddComponent(PlanogramComponentType.Shelf);
            //shelf3.Y = 120;
            //shelf3.Depth = shelf2.Depth;


            PlanogramPositionView p1 = planView.EnumerateAllPositions().ElementAt(0);
            PlanogramPositionView p2 = planView.EnumerateAllPositions().ElementAt(1);
            PlanogramPositionView p3 = planView.EnumerateAllPositions().ElementAt(2);
            PlanogramPositionView p4 = planView.EnumerateAllPositions().ElementAt(3);
            PlanogramPositionView p5 = planView.EnumerateAllPositions().ElementAt(4);

            p1.FacingsWide = 1;
            p2.FacingsWide = 1;
            p3.FacingsWide = 1;
            p4.FacingsWide = 1;
            p5.FacingsWide = 1;

            //set the plan to fill high.
            planView.EndUpdate();
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;


            //set merch height on shelf 1 to be lower than on shelf 2
            shelf1.MerchandisableHeight = 80;
            shelf2.MerchandisableHeight = 120;

            Assert.AreEqual(2, p1.FacingsHigh);
            Assert.AreEqual(2, p2.FacingsHigh);
            Assert.AreEqual(3, p3.FacingsHigh);
            Assert.AreEqual(2, p4.FacingsHigh);
            Assert.AreEqual(2, p5.FacingsHigh);

            //swap them
            // it should still use the merch height from the first sub regardless of 
            // whether its lower or higher!
            shelf1.MerchandisableHeight = 120;
            shelf2.MerchandisableHeight = 80;
            Assert.AreEqual(3, p1.FacingsHigh);
            Assert.AreEqual(3, p2.FacingsHigh);
            Assert.AreEqual(5, p3.FacingsHigh);
            Assert.AreEqual(3, p4.FacingsHigh);
            Assert.AreEqual(3, p5.FacingsHigh);

            //set the first merch height to 0 and the second to 5.
            shelf1.MerchandisableHeight = 0;
            shelf2.MerchandisableHeight = 5;

            Assert.AreEqual(4, p1.FacingsHigh);
            Assert.AreEqual(4, p2.FacingsHigh);
            Assert.AreEqual(6, p3.FacingsHigh);
            Assert.AreEqual(4, p4.FacingsHigh);
            Assert.AreEqual(4, p5.FacingsHigh);

        }

        [Test]
        public void AutoFillHigh_BarAboveShelf()
        {
            //Checks that a bar placed above a shelf with products hanging down is correctly
            // handled.
            var planView = CreateCombinedShelfTestPlanView();
            planView.BeginUpdate();

            PlanogramSubComponentView shelf1 =
                planView.Fixtures[0].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            shelf1.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.Component.Y = 40;

            PlanogramSubComponentView shelf2 =
                planView.Fixtures[1].Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents[0];
            shelf2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf2.CombineType = PlanogramSubComponentCombineType.Both;
            shelf2.Component.Y = 40;

            //add a hanging bar above
            PlanogramComponentView bar = planView.Fixtures[1].AddComponent(PlanogramComponentType.Bar);
            bar.Y = 140;

            //move one of the products onto the bar
            PlanogramPositionView p1 = planView.EnumerateAllPositions().ElementAt(0);
            PlanogramPositionView p2 = planView.EnumerateAllPositions().ElementAt(1);
            PlanogramPositionView p3 = planView.EnumerateAllPositions().ElementAt(2);
            PlanogramPositionView p4 = planView.EnumerateAllPositions().ElementAt(3);
            PlanogramPositionView p5 = planView.EnumerateAllPositions().ElementAt(4);

            p1.FacingsWide = 1;
            p2.FacingsWide = 1;
            p3.FacingsWide = 1;
            p4.FacingsWide = 1;
            p5.FacingsWide = 1;

            p1 = bar.SubComponents[0].MovePosition(p1);


            //turn autofills on
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            planView.EndUpdate();
            planView.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //check
            Assert.AreEqual(1, p1.FacingsHigh);
            Assert.AreEqual(14, p1.FacingsWide);
            Assert.AreEqual(11, p1.FacingsDeep);

            Assert.AreEqual(3, p2.FacingsHigh);
            Assert.AreEqual(2, p3.FacingsHigh);
            Assert.AreEqual(2, p4.FacingsHigh);
            Assert.AreEqual(2, p5.FacingsHigh);
        }

        [Test]
        public void AutoFillHigh_AllowTrayToTopCap()
        {
            //checks that autofill will place rotated top caps on trays
            // if it has space and is allowed to.
            Assert.Ignore(); //check this expected behaviour against spaceman first!

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            PlanogramProductView prod3 = planView.AddProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 23F;
            prod3.Width = 24.5F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 23.2F;
            prod3.TrayWidth = 25;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 1;
            prod3.TrayDeep = 3;

            //allow the cap.
            prod3.MaxTopCap = 99;
            prod3.CanBreakTrayTop = true;


            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            PlanogramPositionView pos3 = shelf1.SubComponents[0].AddPosition(prod3);
            pos3.FacingsHigh = 3;
            pos3.FacingsWide = 1;
            pos3.FacingsDeep = 1;
            pos3.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos3.OrientationType = PlanogramPositionOrientationType.Front0;
            pos3.X = 44.05F;
            pos3.Y = 4;
            pos3.Z = 37.5F;

            //add another shelf above
            PlanogramComponentView shelf2 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf2.Y = 126;
            shelf2.Depth = shelf1.Depth;

            //turn on fill high
            //an orientated top cap should be placed.
            planView.BeginUpdate();
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;

            Assert.AreEqual(3, pos3.FacingsHigh);
            Assert.AreEqual(1, pos3.FacingsYHigh);

        }

        [Test]
        public void AutoFillHigh_AdheresToMaxStack()
        {
            //Checks that reducing max stack is picked up and adhered to by autofill
            //Client raised V8-31525
            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;

            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;

            prod1.MaxTopCap = 0; //dont allow top caps.
            prod1.MaxStack = 99; //allow full max stack.


            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;

            //turn on auto fill high for the plan
            planView.EndUpdate();
            planView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            Assert.AreEqual(5, pos1.FacingsHigh);

            //reduce max stack
            prod1.MaxStack = 2;
            Assert.AreEqual(2, pos1.FacingsHigh);
        }

        [Test]
        public void AutoFillHigh_PegboardCanHangDown()
        {
            //Checks that products can autofill high on a pegboard 
            // in such a way that they are still on a peg but hang down.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;


            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);
            shelf1Fc.Y = 58;
            PlanogramSubComponent shelf1S = shelf1C.SubComponents[0];
            shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1S.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1S.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1S.MerchandisableHeight = 0;
            shelf1S.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1S.IsProductSqueezeAllowed = true;
            shelf1S.IsProductOverlapAllowed = false;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;

            PlanogramComponentView pegboard = planView.Fixtures.First().AddComponent(PlanogramComponentType.Peg);
            pegboard.Y = 91;
            pegboard.Height = 100;
            pegboard.Width = 120;
            pegboard.Depth = 1;

            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 24F;
            prod1.Width = 8.5F;
            prod1.Depth = 8.5F;

            //place it on the pegboard
            planView.EndUpdate();
            PlanogramPositionView pos1 = pegboard.SubComponents.First().AddPosition(prod1);

            //check 
            Assert.AreEqual(4, pos1.FacingsHigh);
            Assert.AreEqual(1, pos1.FacingsWide);
            Assert.AreEqual(1, pos1.FacingsDeep);

            ////Add the position to the shelf
            ////and change strategy from right to left - high should adjust due to the collision of
            //// the product hanging down from the pegboard.
            //shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;

            //PlanogramPositionView pos2 = pegboard.SubComponents.First().AddPosition(prod1);
            //Assert.AreEqual(3, pos2.FacingsHigh);

            //shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            //Assert.AreEqual(2, pos2.FacingsHigh);
        }

        [Test]
        public void AutoFillHigh_CanBreakTrayTop()
        {
            //Checks that trays can break top
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;


            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);
            shelf1Fc.Y = 58;
            PlanogramSubComponent shelf1S = shelf1C.SubComponents[0];
            shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1S.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1S.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1S.MerchandisableHeight = 0;
            shelf1S.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1S.IsProductSqueezeAllowed = true;
            shelf1S.IsProductOverlapAllowed = false;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;


            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 83;

            PlanogramProductView prod3 = planView.AddProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 7;
            prod3.Width = 7F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 21;
            prod3.TrayWidth = 21;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 3;
            prod3.TrayWide = 3;
            prod3.TrayDeep = 3;
            prod3.CanBreakTrayDown = false;
            prod3.CanBreakTrayTop = false;
            prod3.CanBreakTrayUp = false;
            prod3.CanBreakTrayBack = false;


            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod3);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;

            //turn on fill wide
            planView.EndUpdate();
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;

            Assert.AreEqual(4, pos1.FacingsHigh);
            Assert.AreEqual(0, pos1.FacingsYHigh);

            //allow break up
            prod3.CanBreakTrayTop = true;
            Assert.AreEqual(4, pos1.FacingsHigh);
            Assert.AreEqual(2, pos1.FacingsYHigh);
        }

        [Test]
        public void AutoFillHigh_DoesNotGoOverBackboard()
        {
            //V8-31674 - client raised.
            //Checks that products do not autofill over the backboard.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;


            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 76;
            fixture.Width = 39;
            fixture.Depth = 23.15F;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 3.15F);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 5.5F, 23.15F);


            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.EndUpdate();
            planView.ShouldCalculateMetadata = false;

            PlanogramComponentView shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.Y = 64.97F;
            shelf1.Height = 1.57F;
            shelf1.Depth = 16F;
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;


            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 4.75F;
            prod1.Width = 2.25F;
            prod1.Depth = 2F;
            var p1 = shelf1.SubComponents[0].AddPosition(prod1);

            PlanogramProductView prod2 = planView.AddProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 5.13F;
            prod2.Width = 4.18F;
            prod2.Depth = 1.97F;
            var p2 = shelf1.SubComponents[0].AddPosition(prod2);

            PlanogramComponentView shelf2 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf2.Y = 1.57F;
            shelf2.Height = 5.5F;
            shelf2.Depth = 16F;
            shelf2.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf2.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;


            PlanogramProductView prod3 = planView.AddProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod3.Height = 12.25F;
            prod3.Width = 4.13F;
            prod3.Depth = 3.63F;
            var p3 = shelf2.SubComponents[0].AddPosition(prod3);


            //turn on fill high and deep
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.EndUpdate();
            planView.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //check
            // p3 is a tall product which would previously cause the top shelf to be filled up
            // above the backboard based to the height of p3. This behaviour has been flagged up
            // by pns as incorrect - we should only fill the top shelf to the backboard now.

            Assert.AreEqual(1, p1.FacingsHigh);
            Assert.AreEqual(1, p2.FacingsHigh);
            Assert.AreEqual(4, p3.FacingsHigh);
        }

        [Test]
        public void AutoFillHigh_NoTopCapsWhenNoSpaceAfterMainHeightIsSqueezed()
        {
            //V8-31858
            //Create plan
            var plan = "Test".CreatePackage().AddPlanogram();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Inches;

            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard, new PointValue(0, 0, -3.57f), new WidthHeightDepthValue(48f, 76f, 3.57f));

            bay.AddFixtureComponent(PlanogramComponentType.Base, new PointValue(0, 0, -0), new WidthHeightDepthValue(48f, 5.5f, 26));

            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 5.46f, 0), new WidthHeightDepthValue(48f, 1.57f, 18f));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 19.24f, 0), new WidthHeightDepthValue(48f, 1.57f, 18f));
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;

            var prod1 = plan.AddProduct(new WidthHeightDepthValue(11.04f, 15.75f, 4.72f), allowSqueeze: true);
            prod1.SqueezeWidthActual = 9.94f;
            prod1.SqueezeHeightActual = 9.77f;
            prod1.SqueezeDepthActual = 4.72f;
            prod1.MaxTopCap = 99;
            shelf1.AddPosition(bay, prod1);

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(plan.Parent, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            //trigger autofill with correct combination of fill settings.
            planView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;
            planView.EndUpdate();
            planView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;

            PlanogramPosition positionToEvaluate = plan.Positions.First();
            Assert.That(positionToEvaluate.FacingsHigh, Is.EqualTo(1));
            Assert.That(positionToEvaluate.FacingsDeep, Is.EqualTo(3));
            Assert.That(positionToEvaluate.FacingsYHigh, Is.EqualTo(0));
        }

        [Test]
        public void AutoFillHigh_TrayPositionsWithSmallerMerchHeight()
        {
            //V8-31888 - Checks that trays that break merch height behave themselves
            // and dont get converted to units with an incorrect position height.


            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 76;
            fixture.Width = 48;
            fixture.Depth = 23.15F;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();

            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.Height = 1.57F;
            shelf1.Width = 48;
            shelf1.Depth = 16;
            shelf1.Y = 21.21F;
            shelf1.SubComponents[0].MerchandisableHeight = 0;

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod1.Height = 8.2F;
            prod1.Width = 2.45F;
            prod1.Depth = 2.45F;

            //nb: tray info is odd here but we should still deal with
            // what we are given as best we can.
            prod1.TrayHeight = 0;
            prod1.TrayWidth = 0;
            prod1.TrayDepth = 0;
            prod1.TrayHigh = 1;
            prod1.TrayWide = 1;
            prod1.TrayDeep = 1;

            prod1.MaxStack = 0;
            prod1.MaxTopCap = 0;
            prod1.MaxDeep = 0;
            prod1.MinDeep = 0;

            planView.EndUpdate();
            planView.ShouldCalculateMetadata = true;
            var p1 = shelf1.SubComponents[0].AddPosition(prod1);
            p1.FacingsHigh = 1;
            p1.FacingsWide = 1;
            p1.FacingsDeep = 1;


            //set a merch height on the shelf which is smaller than the unit height
            shelf1.SubComponents[0].MerchandisableHeight = 7;

            //check
            Assert.AreEqual(p1.MetaHeight, 8.2F, "Height should be equal to trays high * unit height if no tray height is specified.");
            Assert.AreEqual(p1.MerchandisingStyle, PlanogramPositionMerchandisingStyle.Default, "Merch style should not have changed");
        }

        [Test]
        public void AutoFillHigh_SqueezeAppliedToTopCap()
        {
            //V8-31828 - client raised issue.
            // Checks that when a product width is squeezed and auto fill high is on,
            // top caps also have their width squeezed accordingly.


            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 76;
            fixture.Width = 48;
            fixture.Depth = 26;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.Height = 1.57F;
            shelf1.Width = 48;
            shelf1.Depth = 18;
            shelf1.Y = 29;
            shelf1.SubComponents[0].MerchandisableHeight = 12;

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 7.6F;
            prod1.Width = 7.5F;
            prod1.Depth = 4.13F;
            prod1.SqueezeWidth = 0.9F;
            prod1.MaxStack = 99;
            prod1.MaxTopCap = 1;

            var p1 = shelf1.SubComponents[0].AddPosition(prod1);
            p1.FacingsHigh = 1;
            p1.FacingsWide = 7;
            planView.EndUpdate();
            p1.FacingsDeep = 1;

            Assert.AreEqual(7, p1.FacingsWide);
            Assert.AreEqual(7, p1.FacingsYWide);
            Assert.AreEqual(1, p1.FacingsYHigh);
            Assert.AreEqual(2, p1.FacingsYDeep);
        }

        [Test]
        public void AutoFillHigh_SpaceIsFilledByTopCapsAfterMaxStackReached()
        {
            //V8-31828 - client raised issue.
            // Checks that when max stack is reached, autofill high will try to fill
            // the remaining space with top caps.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 76;
            fixture.Width = 48;
            fixture.Depth = 26;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.Height = 1.57F;
            shelf1.Width = 48;
            shelf1.Depth = 18;
            shelf1.Y = 29;
            shelf1.SubComponents[0].MerchandisableHeight = 14;

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 4.375F;
            prod1.Width = 3F;
            prod1.Depth = 3F;
            prod1.SqueezeHeight = 0.9F;
            prod1.MaxStack = 1;
            prod1.MaxTopCap = 99;
            planView.EndUpdate();
            var p1 = shelf1.SubComponents[0].AddPosition(prod1);

            Assert.AreEqual(1, p1.FacingsHigh);
            Assert.AreEqual(1, p1.FacingsWide);
            Assert.AreEqual(6, p1.FacingsDeep);

            Assert.AreEqual(3, p1.FacingsYHigh);
            Assert.AreEqual(1, p1.FacingsYWide);
            Assert.AreEqual(4, p1.FacingsYDeep);
        }

        [Test]
        public void AutoFillHigh_CollisionOnPositionByXDoesntStopFillHigh()
        {
            //V8-32001 - checks the scenario where a position is on a shelf with a left overhang
            // which causes it to collide by x with a position on another shelf.
            // Makes sure this does not stop autofill high.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add bay 1
            PlanogramFixtureItem fixtureItem1 = plan.AddFixtureItem(75.2F, 48, 4.33F, backboardDepth: 4.33F, baseHeight: 4.33F);
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            var bay1Shelf1fc = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 23.03F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay1Shelf1S = plan.Components.FindById(bay1Shelf1fc.PlanogramComponentId).SubComponents[0];
            bay1Shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;

            var bay1Shelf2fc = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 42.73F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay1Shelf2S = plan.Components.FindById(bay1Shelf2fc.PlanogramComponentId).SubComponents[0];

            var prod1 = plan.AddProduct(15.3F, 8.8F, 4.5F);
            var p1 = bay1Shelf1fc.AddPosition(fixtureItem1, prod1);

            //add bay2
            PlanogramFixtureItem fixtureItem2 = plan.AddFixtureItem(75.2F, 48, 4.33F, backboardDepth: 4.33F, baseHeight: 4.33F);
            fixtureItem2.X = fixture1.Width;
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);

            var bay2Shelf1fc = fixtureItem2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 28.94F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay2Shelf1S = plan.Components.FindById(bay2Shelf1fc.PlanogramComponentId).SubComponents[0];
            bay2Shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            bay2Shelf1S.LeftOverhang = 0.5F;

            var bay2Shelf2fc = fixtureItem2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 42.73F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay2Shelf2S = plan.Components.FindById(bay2Shelf2fc.PlanogramComponentId).SubComponents[0];

            var prod2 = plan.AddProduct(4.375F, 10.625F, 1.625F);
            prod2.MaxTopCap = 1;
            var p2 = bay2Shelf1fc.AddPosition(fixtureItem2, prod2);

            //load and check
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.ShouldCalculateMetadata = false;

            Assert.AreEqual(2, p2.FacingsHigh);
            Assert.AreEqual(1, p2.FacingsYHigh);
        }

        [Test]
        public void AutoFillHigh_Pegboard()
        {
            //V8-32529 - Check that fill high on a pegboard does not push it off pegs!

            //Setup pegboard plan:
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Centimeters;
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 120;
            fixture.Depth = 76;

            PlanogramFixtureComponent pegboardFc = fixture.Components.Add(PlanogramComponentType.Peg, 120, 100, 1);
            pegboardFc.X = 0;
            pegboardFc.Y = 58;
            pegboardFc.Z = 0;
            PlanogramComponent pegboardC = pegboardFc.GetPlanogramComponent();
            PlanogramSubComponent pegboardSub = pegboardC.SubComponents[0];
            pegboardSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboardSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboardSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            pegboardSub.MerchandisableDepth = 0;
            pegboardSub.IsProductSqueezeAllowed = true;
            pegboardSub.MerchConstraintRow1StartX = 4;
            pegboardSub.MerchConstraintRow1SpacingX = 4;
            pegboardSub.MerchConstraintRow1SpacingY = 4;
            pegboardSub.MerchConstraintRow1StartY = 4;
            pegboardSub.MerchConstraintRow1Height = 1;
            pegboardSub.MerchConstraintRow1Width = 1;

            var pegboardPlacement = pegboardSub.GetPlanogramSubComponentPlacement();

            //Add Positions
            var prod1 = plan.AddProduct(28.5F, 20.5F, 5.2F);
            var p1 = plan.Positions.Add(prod1, pegboardPlacement);

            var prod2 = plan.AddProduct(29.5F, 19F, 5.1F);
            var p2 = plan.Positions.Add(prod2, pegboardPlacement);

            var prod3 = plan.AddProduct(25.1F, 19.3F, 4.6F);
            var p3 = plan.Positions.Add(prod3, pegboardPlacement);

            p1.SequenceX = 1;
            p1.SequenceY = 1;
            p2.SequenceX = 1;
            p2.SequenceY = 2;
            p3.SequenceX = 1;
            p3.SequenceY = 3;


            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();


            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];
            var p2View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[1];
            var p3View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[2];

            //turn on autofill high
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;

            //check they are all still one facing high
            Assert.AreEqual(1, p1View.FacingsHigh);
            Assert.AreEqual(1, p2View.FacingsHigh);
            Assert.AreEqual(1, p3View.FacingsHigh);


            //now remove p3 and check that p1 increases.
            p3View.SubComponent.RemovePosition(p3View);
            Assert.AreEqual(2, p1View.FacingsHigh);
            Assert.AreEqual(1, p2View.FacingsHigh);


        }

       
        [Test]
        public void AutoFillHigh_RodBelowBar()
        {
            //V8-32893 - Checks that products do not fill too high when a rod which is lower then a bar was
            // causing the merch space obstructions to ignore the bar.

            //setup the plan
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Centimeters;
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 120;
            fixture.Depth = 76;

            PlanogramFixtureComponent shelfFc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 10, 50);
            shelfFc.X = 0;
            shelfFc.Y = 10;
            shelfFc.Z = 0;
            PlanogramComponent shelfC = shelfFc.GetPlanogramComponent();
            PlanogramSubComponent shelfSub = shelfC.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelfSub.MerchandisableHeight = 0;
            shelfSub.IsProductSqueezeAllowed = true;


            PlanogramFixtureComponent barFc = fixture.Components.Add(PlanogramComponentType.Bar, 120, 2, 2);
            barFc.X = 0;
            barFc.Y = 77;
            barFc.Z = 0;

            PlanogramFixtureComponent rodFc = fixture.Components.Add(PlanogramComponentType.Rod, 4, 4, 60);
            rodFc.X = 78;
            rodFc.Y = 53;
            rodFc.Z = 0;

            var shelfPlacement = shelfSub.GetPlanogramSubComponentPlacement();

            //Add Positions
            var prod1 = plan.AddProduct(19F, 25F, 10F);
            var p1 = plan.Positions.Add(prod1, shelfPlacement);

            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];

            //turn on autofill high & deep
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //Check
            Assert.AreEqual(1, p1View.FacingsWide);
            Assert.AreEqual(1, p1View.FacingsHigh); //changed from 3 to 1 due to obstructed space changes
            Assert.AreEqual(5, p1View.FacingsDeep);

        }

        [Test]
        public void AutoFillHigh_PegboardWithAffectedYRows()
        {
            // V8-32875 - Checks that pegboards do not overfill when one row of positions affects the placement of another
            // but are in different placement groups.

            //Setup pegboard plan:
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Centimeters;
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 133;
            fixture.Depth = 67;

            PlanogramFixtureComponent pegboardFc = fixture.Components.Add(PlanogramComponentType.Peg, 133, 100, 1);
            pegboardFc.X = 0;
            pegboardFc.Y = 87;
            pegboardFc.Z = 0;
            PlanogramComponent pegboardC = pegboardFc.GetPlanogramComponent();
            PlanogramSubComponent pegboardSub = pegboardC.SubComponents[0];
            pegboardSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboardSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboardSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            pegboardSub.MerchandisableDepth = 0;
            pegboardSub.IsProductSqueezeAllowed = true;
            pegboardSub.MerchConstraintRow1StartX = 4;
            pegboardSub.MerchConstraintRow1SpacingX = 4;
            pegboardSub.MerchConstraintRow1SpacingY = 4;
            pegboardSub.MerchConstraintRow1StartY = 4;
            pegboardSub.MerchConstraintRow1Height = 1;
            pegboardSub.MerchConstraintRow1Width = 1;

            var pegboardPlacement = pegboardSub.GetPlanogramSubComponentPlacement();

            //Add Positions
            var prod1 = plan.AddProduct(19F, 25F, 5F);
            var p1 = plan.Positions.Add(prod1, pegboardPlacement);

            var prod2 = plan.AddProduct(29.9F, 19.5F, 6F);
            var p2 = plan.Positions.Add(prod2, pegboardPlacement);

            var prod3 = plan.AddProduct(25F, 19F, 5F);
            var p3 = plan.Positions.Add(prod3, pegboardPlacement);

            p1.SequenceX = 1;
            p1.SequenceY = 2;
            p1.Sequence = 2;

            p2.SequenceX = 2;
            p2.SequenceY = 2;
            p2.Sequence = 3;

            p3.SequenceX = 1;
            p3.SequenceY = 1;
            p3.Sequence = 1;

            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();


            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];
            var p2View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[1];
            var p3View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[2];

            //check intial placement
            AssertWorldPlacement(3.5F, 164.8F, 1, p1View);
            AssertWorldPlacement(30.25F, 153.9F, 1, p2View);
            AssertWorldPlacement(2.5F, 126.8F, 1, p3View);

            //turn on autofill high wide and deep
            planView.BeginUpdate();
            planView.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;
            planView.EndUpdate();

            //check
            Assert.AreEqual(3, p1.FacingsWide);
            Assert.AreEqual(1, p1.FacingsHigh); // changed from 3 due to obstructed space changes

            Assert.AreEqual(2, p2.FacingsWide);
            Assert.AreEqual(2, p2.FacingsHigh);

            Assert.AreEqual(6, p3.FacingsWide);
            Assert.AreEqual(1, p3.FacingsHigh);
            AssertWorldPlacement(2.5F, 94.8F, 1, p3View);
        }

        [Test]
        public void AutoFillHigh_ProductOverhangingOntoNextShelf()
        {
            //CCM-18762 - Client raised.
            // Checks that a product on the secondary shelf of a 
            // combined manual strategy shelf still pays attention to
            // shelves above when autofill is turned on.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Centimeters;
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;

            //add bays
            var bay1Fc =plan.AddFixtureItem(171, 120, 55, 1, 10);
            var bay2Fc = plan.AddFixtureItem(171, 120, 55, 1, 10);
            bay2Fc.X = 120;

            //add shelves
            var bay1shelf1 = bay1Fc.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0), new WidthHeightDepthValue(120, 4, 61.5F));
            bay1shelf1.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            bay1shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            bay1shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            bay1shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            var bay2shelf1 = bay2Fc.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0), new WidthHeightDepthValue(120, 4, 61.5F));
            bay2shelf1.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            bay2shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            bay2shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            bay2shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            var bay2shelf2 = bay2Fc.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 63.2F, 0), new WidthHeightDepthValue(120, 4, 61.5F));
            bay2shelf2.GetPlanogramComponent().SubComponents[0].Name = "OBSTRUCTION";


            //add the position to bay 1
            //visually it looks as if it belongs to bay 2.
            var product = plan.AddProduct(10.8F, 15, 15);
            var pos1 = bay1shelf1.AddPosition(bay1Fc, product);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 2;
            pos1.FacingsDeep = 1;
            pos1.X = 150.5F;
            pos1.Y = 14;
            pos1.Z = 0.5F;

            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            var p1View = planView.EnumerateAllPositions().First();



            //turn on autofill high and deep
            planView.BeginUpdate();
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;
            planView.EndUpdate();

            //check the position values
            Assert.AreEqual(2, p1View.FacingsWide);
            Assert.AreEqual(4, p1View.FacingsHigh);
            Assert.AreEqual(4, p1View.FacingsDeep);
        }

        #endregion

        #region Wide

        [Test]
        public void AutoFillWide_RightCapsAllowedWhenOff()
        {
            //V8-31437 : L.Ineson
            // Checks that placing a right cap is accepted when fill high and deep are on but wide is off.
            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;

            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 29.5F;
            prod1.Width = 23.2F;
            prod1.Depth = 7F;

            prod1.MaxTopCap = 0; //dont allow top caps.
            prod1.MaxStack = 99; //allow full max stack.


            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;

            //turn on auto fill high and deep on for the plan
            planView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.EndUpdate();
            planView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            Assert.AreEqual(0, pos1.FacingsXWide);

            //Manually add a right cap
            pos1.FacingsXWide = 1;

            //check that this did not get cancelled by high and deep autofill.
            Assert.AreEqual(1, pos1.FacingsXWide);
            Assert.AreNotEqual(0, pos1.FacingsXHigh);
            Assert.AreNotEqual(0, pos1.FacingsXDeep);
        }

        [Test]
        public void AutoFillWide_CanBreakTrayUp()
        {
            //Checks that autofill deep pays attention to the can break tray back setting.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;


            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);
            shelf1Fc.Y = 58;
            PlanogramSubComponent shelf1S = shelf1C.SubComponents[0];
            shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1S.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1S.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1S.MerchandisableHeight = 0;
            shelf1S.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1S.IsProductSqueezeAllowed = true;
            shelf1S.IsProductOverlapAllowed = false;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;


            PlanogramProductView prod3 = planView.AddProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 7;
            prod3.Width = 7F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 21;
            prod3.TrayWidth = 21;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 3;
            prod3.TrayWide = 3;
            prod3.TrayDeep = 3;
            prod3.CanBreakTrayDown = false;
            prod3.CanBreakTrayTop = false;
            prod3.CanBreakTrayUp = false;
            prod3.CanBreakTrayBack = false;


            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod3);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;

            //turn on fill wide
            planView.EndUpdate();
            planView.ProductPlacementX = PlanogramProductPlacementXType.FillWide;

            Assert.AreEqual(5, pos1.FacingsWide);
            Assert.AreEqual(0, pos1.FacingsXWide);

            //allow break up
            prod3.CanBreakTrayUp = true;
            Assert.AreEqual(5, pos1.FacingsWide);
            Assert.AreEqual(2, pos1.FacingsXWide);

        }

        [Test]
        public void AutoFillWide_CanBreakTrayDown()
        {
            //Checks that autofill deep pays attention to the can break tray back setting.

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;
            shelf1.Width = 20; //reduce the shelf with to under 1 trays.
            shelf1.Depth = 76.5F;

            PlanogramProductView prod3 = planView.AddProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 7;
            prod3.Width = 7F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 21;
            prod3.TrayWidth = 21;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 3;
            prod3.TrayWide = 3;
            prod3.TrayDeep = 3;
            prod3.CanBreakTrayDown = false;
            prod3.CanBreakTrayTop = false;
            prod3.CanBreakTrayUp = false;
            prod3.CanBreakTrayBack = false;


            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod3);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;

            //turn on fill wide
            planView.EndUpdate();
            planView.ProductPlacementX = PlanogramProductPlacementXType.FillWide;

            Assert.AreEqual(1, pos1.FacingsWide);
            Assert.IsTrue(pos1.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default);

            //allow break down
            prod3.CanBreakTrayDown = true;
            Assert.AreEqual(2, pos1.FacingsWide);
            Assert.IsTrue(pos1.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit);

        }

        [Test]
        public void AutoFillWide_BreakingHighDoesNotReduceWide()
        {
            //check that when fill wide is applied but high is manual,
            // if high causes overfill this does not make units wide drop down to 1.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 133;
            fixture.Depth = 67;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.Height = 4;
            shelf1.Width = 133;
            shelf1.Depth = 57;
            shelf1.Y = 10;

            var shelf2 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf2.Height = 4;
            shelf2.Width = 133;
            shelf2.Depth = 57;
            shelf2.Y = 108;


            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 25;
            prod1.Width = 19;
            prod1.Depth = 5F;

            var p1 = shelf1.SubComponents[0].AddPosition(prod1);
            p1.FacingsHigh = 3;
            planView.EndUpdate();
            p1.OrientationTypeY = PlanogramPositionOrientationType.Top0;

            Assert.AreEqual(7, p1.FacingsWide);

            //add 4 top caps and check that wide does not get reduced to 1
            p1.FacingsYHigh = 4;

            Assert.AreEqual(7, p1.FacingsWide);

            Assert.AreEqual(4, p1.FacingsYHigh);
            Assert.AreEqual(7, p1.FacingsYWide);
        }

        [Test]
        public void AutoFillWide_EvenXCollisionDoesNotStopFill()
        {
            //V8-31700 Checks that autofill wide does not reduce to
            // 1 facing when even strategies are colliding.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add fixture 1
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 133;
            fixture.Depth = 67;
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //add fixture 2
            PlanogramFixtureItem fixtureItem2 = plan.FixtureItems.Add();
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 133;
            fixture.Depth = 67;
            fixtureItem2.X = 133;
            PlanogramFixtureComponent backboardFc2 = fixture2.Components.Add(PlanogramComponentType.Backboard, fixture2.Width, fixture2.Height, 1);
            backboardFc2.Z = -1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.Height = 4;
            shelf1.Width = 133;
            shelf1.Depth = 67;
            shelf1.Y = 10;
            shelf1.SubComponents[0].MerchandisableHeight = 0;

            var shelf2 = planView.Fixtures[1].AddComponent(PlanogramComponentType.Shelf);
            shelf2.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf2.Height = 4;
            shelf2.Width = 133;
            shelf2.Depth = 67;
            shelf2.Y = 10;
            shelf2.SubComponents[0].MerchandisableHeight = 0;


            //p1
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 25;
            prod1.Width = 19;
            prod1.Depth = 5F;
            prod1.MaxStack = 99;
            prod1.MaxTopCap = 99;
            prod1.MaxRightCap = 99;

            var p1 = shelf1.SubComponents[0].AddPosition(prod1);
            p1.FacingsWide = 1;
            p1.FacingsXWide = 2;


            PlanogramProductView prod2 = planView.AddProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 25;
            prod2.Width = 19;
            prod2.Depth = 10F;
            prod2.MaxStack = 99;
            prod2.MaxTopCap = 99;
            prod2.MaxRightCap = 99;

            var p2 = shelf1.SubComponents[0].AddPosition(prod2);
            p2.FacingsWide = 4;


            PlanogramProductView prod3 = planView.AddProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod3.Height = 19;
            prod3.Width = 25;
            prod3.Depth = 5F;
            prod3.MaxStack = 99;
            prod3.MaxTopCap = 99;
            prod3.MaxRightCap = 99;

            var p3 = shelf1.SubComponents[0].AddPosition(prod3);
            p3.FacingsWide = 1;

            PlanogramProductView prod4 = planView.AddProduct();
            prod4.Gtin = "p4";
            prod4.Name = "p4";
            prod4.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod4.Height = 30.3F;
            prod4.Width = 19.1F;
            prod4.Depth = 5.8F;
            prod4.MaxStack = 99;
            prod4.MaxTopCap = 99;
            prod4.MaxRightCap = 99;
            var p4 = shelf2.SubComponents[0].AddPosition(prod4);
            p4.FacingsWide = 8;


            //now turn on fill wide and check
            planView.EndUpdate();
            plan.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            Assert.AreEqual(2, p1.FacingsWide);
            Assert.AreEqual(2, p2.FacingsWide);
            Assert.AreEqual(2, p3.FacingsWide);
            Assert.AreEqual(6, p4.FacingsWide);
        }

        [Test]
        public void AutoFillWide_BarUsesOverhang()
        {
            const Boolean withoutMetaDataCalculation = false;
            // V8-31385 Checks that when a bar has overhang values, these are used even if the positions would not be on pegs.
            //  This behavior mimicks JDA Space Planning's.

            Package package = "V8-31385".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            planogram.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            planogram.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            planogram.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            PlanogramFixtureItem bay = planogram.AddFixtureItem();
            PlanogramFixtureComponent testBar = bay.AddFixtureComponent(PlanogramComponentType.Bar);
            PlanogramSubComponent planogramSubComponent = testBar.GetPlanogramSubComponentPlacements().First().SubComponent;
            planogramSubComponent.LeftOverhang = planogramSubComponent.RightOverhang = 20;
            testBar.AddPosition(bay);
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, withoutMetaDataCalculation);
            PlanogramView planView = packageView.PlanogramViews.First();
            Single expected = planogramSubComponent.LeftOverhang + planogramSubComponent.Width + planogramSubComponent.RightOverhang;

            planView.ProductPlacementX = PlanogramProductPlacementXType.FillWide;

            PlanogramPositionView positionView = planView.EnumerateAllPositions().First();
            Assert.AreEqual(expected, positionView.Width);
        }

        [Test]
        public void AutoFillWide_BarWithoutOverhangStaysWithinSpace()
        {
            Assert.Inconclusive("Work in progress");
            const Boolean withoutMetaDataCalculation = false;
            // V8-31385 Checks that when a bar has no overhang values the positions do not overhang at all.
            //  This behavior mimicks JDA Space Planning's.

            Package package = "V8-31385".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            planogram.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            planogram.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            planogram.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            PlanogramFixtureItem bay = planogram.AddFixtureItem();
            PlanogramFixtureComponent testBar = bay.AddFixtureComponent(PlanogramComponentType.Bar, size: new WidthHeightDepthValue(114, 2, 2));
            PlanogramSubComponent planogramSubComponent = testBar.GetPlanogramSubComponentPlacements().First().SubComponent;
            planogramSubComponent.LeftOverhang = planogramSubComponent.RightOverhang = 0;
            testBar.AddPosition(bay);
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, withoutMetaDataCalculation);
            PlanogramView planView = packageView.PlanogramViews.First();
            Single expected = planogramSubComponent.LeftOverhang + planogramSubComponent.Width + planogramSubComponent.RightOverhang;

            planView.ProductPlacementX = PlanogramProductPlacementXType.FillWide;

            PlanogramPositionView positionView = planView.EnumerateAllPositions().First();
            Assert.AreEqual(expected - 4, positionView.Width);
        }

        [Test]
        public void AutoFillWideForMultiStackedChest()
        {
            var package = "testChest".CreatePackage();
            var plan = package.AddPlanogram();
            var bay = plan.AddFixtureItem();
            var testChest = bay.AddFixtureComponent(PlanogramComponentType.Chest, size: new WidthHeightDepthValue(30, 30, 30));
            var testPosition1 = testChest.AddPosition(bay, plan.AddProduct(dimensions: new WidthHeightDepthValue(10, 10, 10))).SetSequence(1, 1, 1);
            var testPosition2 = testChest.AddPosition(bay, plan.AddProduct(dimensions: new WidthHeightDepthValue(10, 10, 10))).SetSequence(1, 1, 2);
            var testPosition3 = testChest.AddPosition(bay, plan.AddProduct(dimensions: new WidthHeightDepthValue(10,10,5))).SetSequence(2, 1, 1);

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata:false);
            PlanogramView planView = packageView.PlanogramViews.First();

            plan.SetAutoFill(PlanogramProductPlacementXType.FillWide);
            
            testPosition1.FacingsWide.Should().Be(1, "there is no room to fill wide");
            testPosition2.FacingsWide.Should().Be(1, "there is no room to fill wide");
            testPosition3.FacingsWide.Should().Be(1, "there is no room to fill wide");           
        }

        [Test]
        public void AutoFillWide_Pegboard()
        {
            //V8-32529 - Check that fill wide on a pegboard does not push it off pegs!

            //Setup pegboard plan:
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;

            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 120;
            fixture.Depth = 76;

            PlanogramFixtureComponent pegboardFc = fixture.Components.Add(PlanogramComponentType.Peg, 120, 100, 1);
            pegboardFc.X = 0;
            pegboardFc.Y = 58;
            pegboardFc.Z = 0;
            PlanogramComponent pegboardC = pegboardFc.GetPlanogramComponent();
            PlanogramSubComponent pegboardSub = pegboardC.SubComponents[0];
            pegboardSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            pegboardSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.TopStacked;
            pegboardSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
            pegboardSub.MerchandisableDepth = 0;
            pegboardSub.IsProductSqueezeAllowed = true;
            pegboardSub.MerchConstraintRow1StartX = 4;
            pegboardSub.MerchConstraintRow1SpacingX = 4;
            pegboardSub.MerchConstraintRow1SpacingY = 4;
            pegboardSub.MerchConstraintRow1StartY = 4;
            pegboardSub.MerchConstraintRow1Height = 1;
            pegboardSub.MerchConstraintRow1Width = 1;

            var pegboardPlacement = pegboardSub.GetPlanogramSubComponentPlacement();

            //Add Positions
            var prod1 = plan.AddProduct(28.5F, 20.5F, 5.2F);
            var p1 = plan.Positions.Add(prod1, pegboardPlacement);

            var prod2 = plan.AddProduct(29.5F, 19F, 5.1F);
            var p2 = plan.Positions.Add(prod2, pegboardPlacement);

            var prod3 = plan.AddProduct(25.1F, 19.3F, 4.6F);
            var p3 = plan.Positions.Add(prod3, pegboardPlacement);

            p1.SequenceX = 1;
            p1.SequenceY = 1;
            p2.SequenceX = 1;
            p2.SequenceY = 2;
            p3.SequenceX = 1;
            p3.SequenceY = 3;


            //create the planview
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();

            var p1View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[0];
            var p2View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[1];
            var p3View = planView.Fixtures[0].Components[0].SubComponents[0].Positions[2];

            //turn on autofill high and wide
            planView.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.ProductPlacementX = PlanogramProductPlacementXType.FillWide;

            //check they are all still one facing wide
            Assert.AreEqual(5, p1View.FacingsWide);
            Assert.AreEqual(5, p2View.FacingsWide);
            Assert.AreEqual(5, p3View.FacingsWide);

        }

        #endregion

        #region Deep

        [Test]
        public void AutoFillDeep_SlopingShelf()
        {
            //V8-30205

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            //slope the shelf
            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 58;
            shelf1.Slope = RadiansToDegreesConverter.ToRadians(340);

            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 29.5F;
            prod1.Width = 23.2F;
            prod1.Depth = 7F;
            prod1.MaxTopCap = 0;

            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 3;
            pos1.FacingsDeep = 2;

            //turn on auto fill deep for the plan
            planView.EndUpdate();
            planView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;


            //check the position is now correct
            Assert.AreEqual(1, pos1.FacingsHigh);
            Assert.AreEqual(3, pos1.FacingsWide);
            Assert.AreEqual(8, pos1.FacingsDeep);
        }

        [Test]
        public void AutoFillDeep_AdheresToMaxDeep()
        {
            //Checks that reducing max deep is picked up and adhered to by autofill
            //Client raised V8-31525
            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;

            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 29.5F;
            prod1.Width = 23.2F;
            prod1.Depth = 7F;

            prod1.MaxTopCap = 0; //dont allow top caps.
            prod1.MaxDeep = 0; //allow full max deep.


            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;

            //turn on auto fill high for the plan
            planView.EndUpdate();
            planView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            Assert.Greater(pos1.FacingsDeep, 2);
            Int32 facingsDeep = pos1.FacingsDeep;

            //reduce max stack
            prod1.MaxDeep = (Byte)(facingsDeep - 1);

            Assert.AreEqual(prod1.MaxDeep, pos1.FacingsDeep);
        }

        [Test]
        public void AutoFillDeep_AdheresToPegAndMerchDepth()
        {
            //Checks that when filling peg products deep it adheres to the merch depth and peg depth.

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();
            planView.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;

            PlanogramComponentView pegboard = planView.Fixtures.First().AddComponent(PlanogramComponentType.Peg);
            pegboard.Y = 91;
            pegboard.Height = 100;
            pegboard.Width = 120;
            pegboard.Depth = 1;


            //add the product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Name = "p1";
            prod1.OrientationType = PlanogramProductOrientationType.Front0;
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 24F;
            prod1.Width = 8.5F;
            prod1.Depth = 8.5F;
            prod1.NumberOfPegHoles = 1;


            //place it on the pegboard
            PlanogramPositionView pos1 = pegboard.SubComponents.First().AddPosition(prod1);

            //both 0 - should fill to front of fixture
            pegboard.SubComponents[0].MerchandisableDepth = 0;
            prod1.PegDepth = 0;
            planView.EndUpdate();
            Assert.AreEqual(7, pos1.FacingsDeep);

            //merch depth set 
            pegboard.SubComponents[0].MerchandisableDepth = 50;
            Assert.AreEqual(5, pos1.FacingsDeep);

            //peg depth set
            prod1.PegDepth = 30;
            Assert.AreEqual(3, pos1.FacingsDeep);

            //make merch depth less than peg depth - peg depth should still win
            pegboard.SubComponents[0].MerchandisableDepth = 10;
            Assert.AreEqual(3, pos1.FacingsDeep);

        }

        [Test]
        public void AutoFillDeep_FrontOverhang()
        {
            Assert.Ignore("TODO - not currently supported but it should be.");
        }

        [Test]
        public void AutoFillDeep_CanBreakTrayBack()
        {
            //Checks that autofill deep pays attention to the can break tray back setting.

            PlanogramView planView = CreateAutoFillEmptyPlan();
            planView.BeginUpdate();

            PlanogramComponentView shelf1 = planView.Fixtures.First().Components.First(c => c.ComponentType == PlanogramComponentType.Shelf);
            shelf1.Y = 10;
            shelf1.Depth = 76.5F;

            PlanogramProductView prod3 = planView.AddProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 23F;
            prod3.Width = 24.5F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 23.2F;
            prod3.TrayWidth = 25;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 1;
            prod3.TrayDeep = 3;
            prod3.CanBreakTrayDown = false;
            prod3.CanBreakTrayTop = false;
            prod3.CanBreakTrayUp = false;
            prod3.CanBreakTrayBack = false;

            //place it on the first shelf.
            PlanogramPositionView pos1 = shelf1.SubComponents.First().AddPosition(prod3);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;

            //turn on fill deep
            planView.EndUpdate();
            planView.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //No break tray back allowed
            Assert.AreEqual(1, pos1.FacingsDeep);
            Assert.AreEqual(0, pos1.FacingsZDeep);

            //turn break tray back on
            prod3.CanBreakTrayBack = true;
            Assert.AreEqual(1, pos1.FacingsDeep);
            Assert.AreEqual(2, pos1.FacingsZDeep);
        }

        [Test]
        public void AutoFillDeep_NestedTopCap()
        {
            //V8-31886 - Checks that a top cap with nested products fills deep correctly.
            // Also makes sure that top caps arent placed unless they need to be!

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 200;
            fixture.Width = 120;
            fixture.Depth = 76;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.Height = 4;
            shelf1.Width = 120;
            shelf1.Depth = 75;
            shelf1.Y = 108;
            shelf1.SubComponents[0].MerchandisableHeight = 0;

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 20;
            prod1.Width = 5;
            prod1.Depth = 5;
            prod1.NestingHeight = 2;
            prod1.NestingWidth = 0;
            prod1.NestingDepth = 0;
            prod1.MaxStack = 99;
            prod1.MaxTopCap = 99;
            prod1.MaxDeep = 0;
            prod1.MinDeep = 0;

            var p1 = shelf1.SubComponents[0].AddPosition(prod1);
            p1.FacingsHigh = 1;
            p1.FacingsWide = 1;
            p1.FacingsDeep = 1;
            p1.OrientationTypeY = PlanogramPositionOrientationType.Top0;
            p1.FacingsYHigh = 1;
            p1.FacingsYWide = 1;
            p1.FacingsYDeep = 1;

            //turn on fill
            plan.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            planView.EndUpdate();
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            Assert.AreEqual(35, p1.FacingsHigh);
            Assert.AreEqual(24, p1.FacingsWide);
            Assert.AreEqual(15, p1.FacingsDeep);

            //now change values to force a top cap.
            p1.Product.NestingHeight = 6;
            shelf1.Y = 111;

            //check main
            Assert.AreEqual(11, p1.FacingsHigh);
            Assert.AreEqual(24, p1.FacingsWide);
            Assert.AreEqual(15, p1.FacingsDeep);

            //check top caps
            Assert.AreEqual(1, p1.FacingsYHigh);
            Assert.AreEqual(24, p1.FacingsYWide);
            Assert.AreEqual(10, p1.FacingsYDeep); //3


        }

        [Test]
        public void AutoFillDeep_AdheresToMinDeep()
        {
            //V8-31915 - Checks that autofill always adheres to min deep even if it causes collisions.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 78.5F;
            fixture.Width = 48;
            fixture.Depth = 48;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.BeginUpdate();
            planView.ShouldCalculateMetadata = false;

            var shelf1 = planView.Fixtures[0].AddComponent(PlanogramComponentType.Shelf);
            shelf1.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponents[0].MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponents[0].MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.Height = 1;
            shelf1.Width = 48;
            shelf1.Depth = 16;
            shelf1.Y = 50;
            shelf1.SubComponents[0].MerchandisableHeight = 0;

            //add a product
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 3.25F;
            prod1.Width = 4F;
            prod1.Depth = 4F;
            prod1.MaxStack = 0;
            prod1.MaxTopCap = 0;
            prod1.MinDeep = 0;

            var p1 = shelf1.SubComponents[0].AddPosition(prod1);
            p1.FacingsWide = 2;
            planView.EndUpdate();
            p1.FacingsHigh = 2;

            Assert.AreEqual(4, p1.FacingsDeep);

            //set min deep to force it through the backboard.
            prod1.MinDeep = 5;
            Assert.AreEqual(5, p1.FacingsDeep);

            //increase further
            prod1.MinDeep = 10;
            Assert.AreEqual(10, p1.FacingsDeep);

        }

        [Test]
        public void AutoFillDeep_ProductsStopAtShallowerShelves()
        {
            //V8-32004 - CLIENT RAISED.
            // Checks that when a product is placed such that fill deep collides with a 
            // shallower shelf above then it only fills to that shelf

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.AddFixtureItem(67.5F, 48, 28, backboardDepth: 1, baseHeight: 5.5F);
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add 2 shelves, high one slightly shallower
            var shelfFC1 = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 5.5F, 0), new WidthHeightDepthValue(48, 2, 26));
            var shelfS1 = plan.Components.FindById(shelfFC1.PlanogramComponentId).SubComponents[0];
            var shelfFC2 = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 15.114F, 0), new WidthHeightDepthValue(48, 2.2F, 18));

            //add a product to shelf 1.
            var prod1 = plan.AddProduct(8.5F, 6, 1);
            var p1 = shelfFC1.AddPosition(fixtureItem, prod1);
            p1.FacingsWide = 1;
            p1.FacingsHigh = 1;
            p1.FacingsDeep = 26; //max out.

            shelfS1.MerchandisableHeight = 9;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.ShouldCalculateMetadata = false;

            //check
            Assert.AreEqual(1, p1.FacingsWide);
            Assert.AreEqual(1, p1.FacingsHigh);
            Assert.AreEqual(8, p1.FacingsDeep, "Facings deep should have been corrected to only go up to the shallower shelf.");

        }

        [Test]
        public void AutoFillDeep_CollisionOnPositionByXDoesntStopFillDeep()
        {
            //V8-32001 - checks the scenario where a position is on a shelf with a left overhang
            // which causes it to collide by x with a position on another shelf.
            // Makes sure this does not stop autofill deep.

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add bay 1
            PlanogramFixtureItem fixtureItem1 = plan.AddFixtureItem(75.2F, 48, 4.33F, backboardDepth: 4.33F, baseHeight: 4.33F);
            PlanogramFixture fixture1 = plan.Fixtures.FindById(fixtureItem1.PlanogramFixtureId);

            var bay1Shelf1fc = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 23.03F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay1Shelf1S = plan.Components.FindById(bay1Shelf1fc.PlanogramComponentId).SubComponents[0];
            bay1Shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;

            var bay1Shelf2fc = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 42.73F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay1Shelf2S = plan.Components.FindById(bay1Shelf2fc.PlanogramComponentId).SubComponents[0];

            var prod1 = plan.AddProduct(15.3F, 8.8F, 4.5F);
            var p1 = bay1Shelf1fc.AddPosition(fixtureItem1, prod1);

            //add bay2
            PlanogramFixtureItem fixtureItem2 = plan.AddFixtureItem(75.2F, 48, 4.33F, backboardDepth: 4.33F, baseHeight: 4.33F);
            fixtureItem2.X = fixture1.Width;
            PlanogramFixture fixture2 = plan.Fixtures.FindById(fixtureItem2.PlanogramFixtureId);

            var bay2Shelf1fc = fixtureItem2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 28.94F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay2Shelf1S = plan.Components.FindById(bay2Shelf1fc.PlanogramComponentId).SubComponents[0];
            bay2Shelf1S.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            bay2Shelf1S.LeftOverhang = 0.5F;

            var bay2Shelf2fc = fixtureItem2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 42.73F, 0), new WidthHeightDepthValue(48, 1, 16));
            var bay2Shelf2S = plan.Components.FindById(bay2Shelf2fc.PlanogramComponentId).SubComponents[0];

            var prod2 = plan.AddProduct(4.375F, 10.625F, 1.625F);
            prod2.MaxTopCap = 99;
            var p2 = bay2Shelf1fc.AddPosition(fixtureItem2, prod2);

            //load and check
            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.ShouldCalculateMetadata = false;

            Assert.AreEqual(9, p2.FacingsDeep);
            Assert.AreEqual(3, p2.FacingsYDeep);
        }

        [Test]
        public void AutoFillDeep_ProductFillsShelfWhenAutoFillDeepIsEnabled()
        {
            //V8-32004 - CLIENT RAISED.
            //Checks that a product fills a shelf by depth when Fill Deep is Enabled

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.AddFixtureItem(67.5F, 48, 28, backboardDepth: 1, baseHeight: 5.5F);
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add 2 shelves, high one slightly shallower
            var shelfFC1 = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 5.5F, 0), new WidthHeightDepthValue(48, 1.57F, 14.5F));
            var shelfS1 = plan.Components.FindById(shelfFC1.PlanogramComponentId).SubComponents[0];
            var shelfFC2 = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 15.114F, 0), new WidthHeightDepthValue(48, 1.57F, 14.5F));

            //add a product to shelf 1.
            var prod1 = plan.AddProduct(5.25F, 4.25F, .125F);
            var p1 = shelfFC1.AddPosition(fixtureItem, prod1);
            p1.FacingsWide = 1;
            p1.FacingsHigh = 1;
            p1.FacingsDeep = 12; //max out.

            shelfS1.MerchandisableHeight = 9;

            PackageViewModel packageView = PackageViewModel.NewPackageViewModel(package, shouldCalculateMetadata: false);
            PlanogramView planView = packageView.PlanogramViews.First();
            planView.ShouldCalculateMetadata = false;

            //check
            Assert.AreEqual(1, p1.FacingsWide);
            Assert.AreEqual(1, p1.FacingsHigh);
            Assert.AreEqual(shelfS1.Depth / prod1.Depth, p1.FacingsDeep, "Facings deep should have been corrected to only go up to the shallower shelf.");

        }

        #endregion

        #endregion



        /*Tickets to write tests for:
         * 25486
         * 25224
         * 25529
        */
    }
}