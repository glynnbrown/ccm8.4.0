﻿using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Settings.HighlightEditor;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using Galleria.Framework.Planograms.Rendering;

namespace Galleria.Ccm.UnitTests.UI
{
    /// <summary>
    /// Contains scenario tests which check the basic highlight functionality of Space Planning.
    /// If any of these tests are broken then highlight functionality is considered to not be working.
    /// </summary>
    [TestFixture]
    [Category(Categories.Smoke)]
    public class HighlightFunctionalityTests : TestBase
    {
        #region TestFixtureHelpers

        private void OpenAndApplyHighlightFile(String path)
        {
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    HighlightEditorViewModel highlightEditorVm = (HighlightEditorViewModel)p.WindowParameters[0];
                    //reopen the original file
                    highlightEditorVm.OpenFromFileCommand.Execute(path);

                    //apply
                    highlightEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            WindowService.CloseWindowSetNullResponse();
            MainPageCommands.ShowHighlightEditor.Execute();
        }

        #endregion

        [Test]
        public void CanCreateAndUseHighlightFromFile()
        {
            //Checks that it is possible to save a highlight to file then reopen it

            const String testField = "[PlanogramProduct.Brand]";

            var planController = base.AddNewCreweSparklingWaterPlan();

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) => 
                {
                    HighlightEditorViewModel highlightEditorVm = (HighlightEditorViewModel)p.WindowParameters[0];

                    //Create a new highlight
                    highlightEditorVm.SelectedHighlight.MethodType = HighlightMethodType.Group;
                    highlightEditorVm.SelectedHighlight.Field1 = testField;

                    //save to file
                    highlightEditorVm.SaveAsToFileCommand.Execute(expectedPath);

                    //check file exists
                    Assert.IsTrue(System.IO.File.Exists(expectedPath));

                    //change a property on the highlight
                    highlightEditorVm.SelectedHighlight.Field1 = "[PlanogramProduct.FinancialCode]";

                    //reopen the original file
                    highlightEditorVm.OpenFromFileCommand.Execute(expectedPath);

                    //check
                    Assert.AreEqual(testField, highlightEditorVm.SelectedHighlight.Field1);

                    //apply
                    highlightEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            WindowService.CloseWindowSetNullResponse();
            MainPageCommands.ShowHighlightEditor.Execute();

            //now check that the highlight was applied.
            var curDoc = App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Assert.AreEqual(testField, curDoc.Highlight.Field1);
        }

        [Test]
        public void CanCreateAndUseHighlightFromRepository()
        {
            //Checks that it is possible to save a highlight to the repository then reopen it

            const String testField = "[PlanogramProduct.Brand]";
            base.SetMockRepositoryConnection();

            var planController = base.AddNewCreweSparklingWaterPlan();

            String highlightName = Guid.NewGuid().ToString();

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    HighlightEditorViewModel highlightEditorVm = (HighlightEditorViewModel)p.WindowParameters[0];

                    //Create a new highlight
                    highlightEditorVm.SelectedHighlight.MethodType = HighlightMethodType.Group;
                    highlightEditorVm.SelectedHighlight.Field1 = testField;

                    //save to file
                    WindowService.PromptForSaveAsNameSetReponse(true, highlightName);
                    highlightEditorVm.SaveAsToRepositoryCommand.Execute();

                   //check repository item exists
                    HighlightInfo saved = HighlightInfoList.FetchByEntityId(App.ViewState.EntityId).First(h => h.Name == highlightName);

                    //change a property on the highlight
                    highlightEditorVm.SelectedHighlight.Field1 = "[PlanogramProduct.FinancialCode]";

                    //reopen the original file
                    highlightEditorVm.OpenFromRepositoryCommand.Execute(saved.Id);

                    //check
                    Assert.AreEqual(testField, highlightEditorVm.SelectedHighlight.Field1);

                    //apply
                    WindowService.CloseWindowSetNullResponse();
                    highlightEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            MainPageCommands.ShowHighlightEditor.Execute();

            //now check that the highlight was applied.
            var curDoc = App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Assert.AreEqual(testField, curDoc.Highlight.Field1);

        }

        [Test]
        public void CanOpenReadOnlyHighlightFile()
        {
            //Checks that it is possible to open a readonly highlight file

            //create a new highlight file.
            Highlight h1 = Highlight.NewHighlight();
            h1.Name = "h1";
            h1.Type = HighlightType.Position;
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogramProduct.Brand]";

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);
            h1 = h1.SaveAsFile(expectedPath);
            h1.Dispose();

            //set the file to be readonly.
            System.IO.File.SetAttributes(expectedPath, FileAttributes.ReadOnly);

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();
            OpenAndApplyHighlightFile(expectedPath);

            //now check that the highlight was applied.
            var curDoc = App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Assert.AreEqual("[PlanogramProduct.Brand]", curDoc.Highlight.Field1);
        }

        [Test]
        public void HighlightIsAppliedToPlanVisual()
        {
            //Checks that highlight colours get applied to a plan visual doc

            var planController = base.AddNewCreweSparklingWaterPlan();

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    HighlightEditorViewModel highlightEditorVm = (HighlightEditorViewModel)p.WindowParameters[0];

                    //Create a new highlight
                    highlightEditorVm.SelectedHighlight.MethodType = HighlightMethodType.Group;
                    highlightEditorVm.SelectedHighlight.Field1 = "[PlanogramProduct.Brand]";

                    //apply
                    highlightEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            WindowService.CloseWindowSetNullResponse();
            MainPageCommands.ShowHighlightEditor.Execute();

            //now check that the highlight was applied.
            PlanVisualDocument curDoc = (PlanVisualDocument)App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Plan3DData planData = curDoc.PlanogramModelData;

            Assert.IsNotNull(curDoc.Highlight, "should have highlight");
            Assert.IsNotNull(curDoc.HighlightLegendItems, "should have legend items");
            Assert.IsNotNull(planData.Settings.PositionHighlights, "Should have highlight colours");
        }

        [Test]
        public void OpeningHighlightAddsItToMostRecentList()
        {
            //create a new highlight file.
            Highlight h1 = Highlight.NewHighlight();
            h1.Name = "h1";
            h1.Type = HighlightType.Position;
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogramProduct.Brand]";

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);
            h1 = h1.SaveAsFile(expectedPath);
            h1.Dispose();

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();
            OpenAndApplyHighlightFile(expectedPath);

            //check the mru
            var mrus = App.ViewState.Settings.Model.RecentHighlight.Where(h => Object.Equals(h.HighlightId, expectedPath));
            Assert.AreEqual(1, mrus.Count(), "Should have  item in the mru list.");
        }

        [Test]
        public void CanOpenFileMruItem()
        {
            //Checks that a file item can be opened from the mru

            //create a file and a repository item
            Highlight h1 = Highlight.NewHighlight();
            h1.Name = "h1";
            h1.Type = HighlightType.Position;
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogramProduct.Brand]";

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Highlight.FileExtension);
            h1 = h1.SaveAsFile(expectedPath);
            h1.Dispose();

            //add to the mru
            App.ViewState.Settings.Model.RecentHighlight.AddHighlightId(h1.Id);

            App.MainPageViewModel.RefreshAvailableHighlights();

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();

            var info = App.MainPageViewModel.AvailableHighlights.FirstOrDefault(h => Object.Equals(h.Id, expectedPath));
            App.MainPageViewModel.SetHighlight(info);

            //now check that the Label was applied.
            PlanVisualDocument curDoc = (PlanVisualDocument)App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Plan3DData planData = curDoc.PlanogramModelData;

            Assert.IsNotNull(curDoc.Highlight, "should have highlight");
        }

        [Test]
        public void CanOpenRepositoryMruItem()
        {
            //Checks that a repository item can be opened from the mru

            base.SetMockRepositoryConnection();


            //create a file and a repository item
            Highlight h1 = Highlight.NewHighlight();
            h1.Name = "h1";
            h1.Type = HighlightType.Position;
            h1.MethodType = HighlightMethodType.Group;
            h1.Field1 = "[PlanogramProduct.Brand]";
            h1 = h1.SaveAs();
            h1.Dispose();

            //add to the mru
            App.ViewState.Settings.Model.RecentHighlight.AddHighlightId(h1.Id);

            App.MainPageViewModel.RefreshAvailableHighlights();

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();

            var info = App.MainPageViewModel.AvailableHighlights.FirstOrDefault(h => Object.Equals(h.Id, h1.Id));
            App.MainPageViewModel.SetHighlight(info);

            //now check that the Label was applied.
            PlanVisualDocument curDoc = (PlanVisualDocument)App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Plan3DData planData = curDoc.PlanogramModelData;

            Assert.IsNotNull(curDoc.Highlight, "should have highlight");
        }
    }
}
