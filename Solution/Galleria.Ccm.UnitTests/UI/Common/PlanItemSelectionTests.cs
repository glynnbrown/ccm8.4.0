﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Media;

namespace Galleria.Ccm.UnitTests.UI.Common
{
    [TestFixture]
    public sealed class PlanItemSelectionTests : TestBase
    {

        #region Property Tests

        private void TestProperty<TMultiView>(IPlanItem item1, IPlanItem item2, String propertyName, PlanogramView planView)
            where TMultiView : class
        {
            PropertyInfo selectionProperty = typeof(TMultiView).GetProperty(propertyName);
            if (selectionProperty == null)
            {
                throw new InconclusiveException("Property not found");
            }

            PropertyInfo viewProperty = item1.GetType().GetProperty(propertyName);

            PlanItemSelection selection = new PlanItemSelection(planView);

            TMultiView multiView;

            if (viewProperty.GetSetMethod(false) != null)
            {
                selection.Add(item1);
                selection.Add(item2);

                multiView = GetMultiView<TMultiView>(selection);
                ((INotifyPropertyChanged)multiView).PropertyChanged += base.TestModel_PropertyChanged;

                //set both values to the same thing
                Object newValue1 = GetPropertyTestValue1(viewProperty.PropertyType);
                viewProperty.SetValue(item1, newValue1, null);
                viewProperty.SetValue(item2, newValue1, null);

                if (selectionProperty.PropertyType == typeof(Color))
                {
                    Assert.AreEqual(ColorHelper.IntToColor((Int32)newValue1), selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get same failed", propertyName));
                }
                else if (viewProperty.Name == "Slope" || viewProperty.Name == "Roll" || viewProperty.Name == "Angle"
                    || viewProperty.Name == "WorldSlope" || viewProperty.Name == "WorldRoll" || viewProperty.Name == "WorldAngle")
                {
                    Assert.AreEqual(CommonHelper.ToDegrees((Single)newValue1), selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get same failed", propertyName));
                }
                else
                {
                    Assert.AreEqual(newValue1, selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get same failed", propertyName));
                }

                //set item2 to different
                Object newValue2 = GetPropertyTestValue2(viewProperty.PropertyType);
                viewProperty.SetValue(item2, newValue2, null);

                if (selectionProperty.PropertyType == typeof(Color))
                {
                    Assert.AreEqual(Colors.White, selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get different failed", propertyName));
                }
                else
                {
                    Assert.IsNull(selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get different failed", propertyName));
                }
            }
            else
            {
                selection.Add(item1);

                multiView = GetMultiView<TMultiView>(selection);
                ((INotifyPropertyChanged)multiView).PropertyChanged += base.TestModel_PropertyChanged;

                Assert.AreEqual(viewProperty.GetValue(item1, null),
                    selectionProperty.GetValue(multiView, null),
                    String.Format("{0} get failed", propertyName));
            }



            ((INotifyPropertyChanged)multiView).PropertyChanged -= base.TestModel_PropertyChanged;
        }

        private TMultiView GetMultiView<TMultiView>(PlanItemSelection selection)
            where TMultiView : class
        {
            Type multiViewType = typeof(TMultiView);

            if (multiViewType == typeof(PlanogramAnnotationMultiView))
            {
                return selection.AnnotationView as TMultiView;
            }
            else if (multiViewType == typeof(PlanogramProductMultiView))
            {
                return selection.ProductView as TMultiView; 
            }
            else if (multiViewType == typeof(PlanogramPositionMultiView))
            {
                return selection.PositionView as TMultiView;
            }
            else if (multiViewType == typeof(PlanogramComponentMultiView))
            {
                return selection.ComponentView as TMultiView;
            }
            else if (multiViewType == typeof(PlanogramSubComponentMultiView))
            {
                return selection.ChildSubComponentView as TMultiView;
            }
            else if (multiViewType == typeof(PlanogramAssemblyMultiView))
            {
                return selection.AssemblyView as TMultiView;
            }

            return null;
        }

        #region Annotation

        public IEnumerable<String> PlanogramAnnotationViewPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { "Model", "Planogram", "Fixture", "Assembly", "Component", "SubComponent", "Position", "Annotation",
                "Product", "IsSelectable", "IsUpdating", "X", "Y", "Z", "Slope", "Angle", "Roll",};

                foreach (PropertyInfo property in typeof(PlanogramAnnotationView).GetProperties())
             //.Where(p => p.DeclaringType == typeof(PlanogramAnnotationView)))
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        yield return property.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("PlanogramAnnotationViewPropertyNames")]
        public void AnnotationProperty(String propertyName)
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            
            PlanogramView planView = new PlanogramView(plan);
            
            PlanogramAnnotationView item1 = planView.Fixtures[0].AddAnnotation();
            PlanogramAnnotationView item2 = planView.Fixtures[0].AddAnnotation();

            TestProperty<PlanogramAnnotationMultiView>(item1, item2, propertyName, planView);
        }

        #endregion

        #region Product

        public IEnumerable<String> PlanogramProductViewPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { "Model", "Planogram", "Fixture", "Assembly", "Component", "SubComponent", "Position", "Annotation",
                "Product", "IsSelectable","CustomAttributes", "PerformanceData", "IsUpdating",
                "X", "Y", "Z", "Angle", "Slope", "Roll"};

                foreach (PropertyInfo property in typeof(PlanogramProductView).GetProperties())
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        if (property.Name.Contains("Image")) continue;

                        yield return property.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("PlanogramProductViewPropertyNames")]
        public void ProductProperty(String propertyName)
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);

            PlanogramProductView item1 = planView.AddProduct();
            PlanogramProductView item2 = planView.AddProduct();

            TestProperty<PlanogramProductMultiView>(item1, item2, propertyName, planView);
        }

        #endregion

        #region Position

        public IEnumerable<String> PlanogramPositionViewPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { "Model", "Planogram", "Fixture", "Assembly", "Component", "SubComponent", "Position", "Annotation",
                "Product", "IsSelectable",  "IsOnPegs", "IsInChest", "IsUpdating",
                "Slope", "Angle", "Roll","X", "Y", "Z", "MetaWorldY"};

                foreach (PropertyInfo property in typeof(PlanogramPositionView).GetProperties())
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        yield return property.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("PlanogramPositionViewPropertyNames")]
        public void PositionProperty(String propertyName)
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.RenumberingStrategy.IsEnabled = false;
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 2, 75);


            PlanogramView planView = new PlanogramView(plan);
            PlanogramSubComponentView subView = planView.EnumerateAllSubComponents().First();

            //make sure positions don't get moved.
            subView.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            subView.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            subView.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            subView.IsProductOverlapAllowed = true;

            PlanogramProductView p1 = planView.AddProduct();
            
            PlanogramPositionView item1 = subView.AddPosition(p1);
            PlanogramPositionView item2 = subView.AddPosition(p1);

            TestProperty<PlanogramPositionMultiView>(item1, item2, propertyName, planView);
        }

        #endregion

        #region Component

        public IEnumerable<String> PlanogramComponentViewPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { "Model", "Planogram", "Fixture", "Assembly", "Component", "SubComponent", "Position", "Annotation",
                "Product", "IsSelectable", "FixtureComponentModel", "AssemblyComponentModel", "ComponentModel", "NotchNumber",
                "CustomAttributes", "IsUpdating", "X", "Y", "Z", "Slope", "Angle", "Roll", 
                "MetaWorldSlope", "MetaWorldAngle", "MetaWorldRoll"};

                foreach (PropertyInfo property in typeof(PlanogramComponentView).GetProperties())
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        yield return property.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("PlanogramComponentViewPropertyNames")]
        public void ComponentProperty(String propertyName)
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.RenumberingStrategy.IsEnabled = false;
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            PlanogramFixtureComponent fc = fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramComponentView item1 = planView.EnumerateAllComponents().ElementAt(0);
            PlanogramComponentView item2 = planView.EnumerateAllComponents().ElementAt(1);

            TestProperty<PlanogramComponentMultiView>(item1, item2, propertyName, planView);
        }

        #endregion

        #region Assembly

        public IEnumerable<String> PlanogramAssemblyViewPropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { "Model", "Planogram", "Fixture", "Assembly", "Component", "SubComponent", "Position", "Annotation",
                "Product", "IsSelectable", "FixtureAssemblyModel", "AssemblyModel"
                , "Height", "Width", "Depth", "IsUpdating", "X", "Y", "Z", "Slope", "Angle", "Roll"};

                foreach (PropertyInfo property in typeof(PlanogramAssemblyView).GetProperties())
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        yield return property.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("PlanogramAssemblyViewPropertyNames")]
        public void AssemblyProperty(String propertyName)
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
        
            PlanogramView planView = new PlanogramView(plan);
            PlanogramAssemblyView item1 = planView.Fixtures[0].AddAssembly();
            PlanogramAssemblyView item2 = planView.Fixtures[0].AddAssembly();

            TestProperty<PlanogramAssemblyMultiView>(item1, item2, propertyName, planView);
        }

        #endregion

        #region ChildSubComponent

        public IEnumerable<String> PlanogramSubComponentViewPropertyNames
        {
            get
            {
                //NB- majority will be excluded as this is brief access only.
                String[] exclusions = new String[] 
                { "Model", "Planogram", "Fixture", "Assembly", "Component", "SubComponent", "Position", "Annotation",
                "Product", "IsSelectable",
                "CanMultiMerchX", "CanMultiMerchY", "CanMultiMerchZ", "MerchandisingType",
                "FillPatternTypeFront", "FillPatternTypeBack", "FillPatternTypeTop", 
                "FillPatternTypeBottom", "FillPatternTypeLeft", "FillPatternTypeRight",
                "FillColourFront", "FillColourBack", "FillColourTop", "FillColourBottom", "FillColourLeft", "FillColourRight",
                "TransparencyPercentFront", "TransparencyPercentBack", "TransparencyPercentTop", 
                "TransparencyPercentBottom", "TransparencyPercentLeft", "TransparencyPercentRight",
                "X", "Y", "Z", "Height", "Width", "Depth", "Slope", "Angle", "Roll", "IsUpdating"};

                foreach (PropertyInfo property in typeof(PlanogramSubComponentView).GetProperties())
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        yield return property.Name;
                    }
                }
            }
        }

        [Test, TestCaseSource("PlanogramSubComponentViewPropertyNames")]
        public void ChildSubProperty(String propertyName)
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);
            fixture.Components.Add(PlanogramComponentType.Shelf, 120, 4, 75);

            PlanogramView planView = new PlanogramView(plan);
            PlanogramComponentView item1 = planView.EnumerateAllComponents().ElementAt(0);
            PlanogramComponentView item2 = planView.EnumerateAllComponents().ElementAt(1);



            PropertyInfo selectionProperty = typeof(PlanogramSubComponentMultiView).GetProperty(propertyName);
            if (selectionProperty == null)
            {
                throw new InconclusiveException("Property not found");
            }

            PropertyInfo viewProperty = typeof(PlanogramSubComponentView).GetProperty(propertyName);

            PlanItemSelection selection = new PlanItemSelection(planView);

            PlanogramSubComponentMultiView multiView;
            

            if (viewProperty.CanWrite)
            {
                selection.Add(item1);
                selection.Add(item2);

                multiView = selection.ChildSubComponentView;
                ((INotifyPropertyChanged)multiView).PropertyChanged += base.TestModel_PropertyChanged;

                //set both values to the same thing
                Object newValue1 = GetPropertyTestValue1(viewProperty.PropertyType);
                viewProperty.SetValue(item1.SubComponents[0], newValue1, null);
                viewProperty.SetValue(item2.SubComponents[0], newValue1, null);

                if (selectionProperty.PropertyType == typeof(Color))
                {
                    Assert.AreEqual(ColorHelper.IntToColor((Int32)newValue1), selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get same failed", propertyName));
                }
                else
                {
                    Assert.AreEqual(newValue1, selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get same failed", propertyName));
                }

                //set item2 to different
                Object newValue2 = GetPropertyTestValue2(viewProperty.PropertyType);
                viewProperty.SetValue(item2.SubComponents[0], newValue2, null);

                if (selectionProperty.PropertyType == typeof(Color))
                {
                    Assert.AreEqual(Colors.White, selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get different failed", propertyName));
                }
                else
                {
                    Assert.IsNull(selectionProperty.GetValue(multiView, null),
                        String.Format("{0} get different failed", propertyName));
                }
            }
            else
            {
                selection.Add(item1);

                multiView = selection.ChildSubComponentView;
                ((INotifyPropertyChanged)multiView).PropertyChanged += base.TestModel_PropertyChanged;

                Assert.AreEqual(viewProperty.GetValue(item1.SubComponents[0], null),
                    selectionProperty.GetValue(multiView, null),
                    String.Format("{0} get failed", propertyName));
            }



            ((INotifyPropertyChanged)multiView).PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #endregion

    }
}
