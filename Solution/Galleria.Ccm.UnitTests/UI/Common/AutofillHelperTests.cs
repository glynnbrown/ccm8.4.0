﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 803)
//// V8-28766 : J.Pickup
////  Created
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using NUnit.Framework;
//using Galleria.Framework.Planograms.Model;
//using Galleria.Ccm.Editor.Client.Wpf.Common;
//using Galleria.Ccm.Security;
//using System.Reflection;
//using Csla.Core;
//using System.Collections;
//using System.ComponentModel;
//using System.Windows.Media;
//using Galleria.Ccm.Editor.Client.Wpf.Resources.Converters;
//using Galleria.Ccm.Common.Wpf.Helpers;
//using Galleria.Framework.Planograms.Merchandising;
//using Galleria.Ccm.UnitTests.Helpers;

//namespace Galleria.Ccm.UnitTests.UI.Common
//{
//    [TestFixture]
//    public sealed class AutofillHelperTests : TestBase
//    {
//        #region Test Help Methods

//        private static PlanogramSubComponentView CreateViewWithShelf()
//        {
//            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
//            Planogram plan = package.Planograms.Add();

//            //add a fixture
//            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
//            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

//            //add a backboard
//            PlanogramFixtureComponent backboardFC =
//                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
//            backboardFC.Z = -1;

//            //Add a base
//            PlanogramFixtureComponent baseFC =
//                fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 75);

//            //Add a shelf
//            PlanogramFixtureComponent shelf1FC =
//                fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
//            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

//            //add a product
//            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
//            prod1.Gtin = "Gtin1";
//            prod1.Name = "Product1";
//            plan.Products.Add(prod1);

//            // create a sub component placement
//            PlanogramSubComponentPlacement subComponentPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
//                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

//            //add a position
//            PlanogramPosition pos1 = subComponentPlacement1.AddPosition(prod1);

//            //add a fixture annotation
//            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno1.PlanogramFixtureItemId = fixtureItem.Id;
//            anno1.Text = "Annotation 1";
//            plan.Annotations.Add(anno1);


//            //add an assembly
//            PlanogramFixtureAssembly fixtureAssembly1 = fixture.Assemblies.Add();
//            PlanogramAssembly assembly1 = plan.Assemblies.FindById(fixtureAssembly1.PlanogramAssemblyId);

//            //add an assembly component
//            PlanogramAssemblyComponent shelf2AC = assembly1.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 75);
//            PlanogramComponent shelf2C = plan.Components.FindById(shelf2AC.PlanogramComponentId);

//            //add a subcomponent annotation
//            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno2.PlanogramFixtureItemId = fixtureItem.Id;
//            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
//            anno2.PlanogramAssemblyComponentId = shelf2AC.Id;
//            anno2.PlanogramSubComponentId = shelf2C.SubComponents[0].Id;
//            anno2.Text = "Annotation 2";
//            plan.Annotations.Add(anno2);


//            PlanogramView planView = new PlanogramView(plan);
//            return planView.EnumerateAllSubComponents().First();
//        }

//        private static Planogram CreatePlanogramWithBar()
//        {
//            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

//            //add 1 plan
//            Planogram plan = Planogram.NewPlanogram();
//            plan.Name = "Plan 1";
//            package.Planograms.Add(plan);

//            //add a fixture
//            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
//            fixture.Name = "Fixture 1";
//            fixture.Height = 191;
//            fixture.Width = 120;
//            fixture.Depth = 100;
//            plan.Fixtures.Add(fixture);

//            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
//            fixtureItem.PlanogramFixtureId = fixture.Id;
//            fixtureItem.BaySequenceNumber = (Int16)1;
//            plan.FixtureItems.Add(fixtureItem);


//            //add a backboard
//            PlanogramComponent component1 = PlanogramComponent.NewPlanogramComponent();
//            component1.Name = "Backboard";
//            component1.Width = fixture.Width;
//            component1.Height = fixture.Height;
//            component1.Depth = 75;
//            plan.Components.Add(component1);

//            PlanogramFixtureComponent fixtureComponent1 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent1.PlanogramComponentId = component1.Id;
//            fixture.Components.Add(fixtureComponent1);

//            PlanogramSubComponent sub1 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub1.Name = "Backboard part";
//            sub1.LineThickness = 0.5F;
//            sub1.LineColour = -1;
//            sub1.MerchandisingType = PlanogramSubComponentMerchandisingType.Hang;
//            sub1.FillColourFront = -1;
//            sub1.FillColourBack = -1;
//            sub1.FillColourLeft = -1;
//            sub1.FillColourRight = -1;
//            sub1.FillColourTop = -1;
//            sub1.FillColourBottom = -1;
//            sub1.Width = component1.Width;
//            sub1.Height = component1.Height;
//            sub1.Depth = component1.Depth;
//            component1.SubComponents.Add(sub1);

//            //add a bar
//            PlanogramComponent component2 = PlanogramComponent.NewPlanogramComponent();
//            component2.Name = "Bar 1";
//            component2.Width = fixture.Width;
//            component2.Height = 4;
//            component2.Depth = 75;
//            component2.ComponentType = PlanogramComponentType.Bar;
//            plan.Components.Add(component2);

//            PlanogramFixtureComponent fixtureComponent2 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent2.PlanogramComponentId = component2.Id;
//            fixtureComponent2.Y = 50;
//            fixture.Components.Add(fixtureComponent2);

//            PlanogramSubComponent sub2 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub2.Name = "Shelf part";
//            sub2.MerchandisingType = PlanogramSubComponentMerchandisingType.Hang;
//            sub2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub2.FillColourFront = -1;
//            sub2.FillColourBack = -1;
//            sub2.FillColourLeft = -1;
//            sub2.FillColourRight = -1;
//            sub2.FillColourTop = -1;
//            sub2.FillColourBottom = -1;
//            sub2.Width = component2.Width;
//            sub2.Height = component2.Height;
//            sub2.Depth = component2.Depth;
//            component2.SubComponents.Add(sub2);

//            //add a product
//            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
//            prod1.Gtin = "Gtin1";
//            prod1.Name = "Product1";
//            plan.Products.Add(prod1);

//            //add a position
//            PlanogramPosition pos1 = PlanogramPosition.NewPlanogramPosition(plan.Positions.GetNextSequence());
//            pos1.PlanogramFixtureItemId = fixtureItem.Id;
//            pos1.PlanogramFixtureComponentId = fixtureComponent2.Id;
//            pos1.PlanogramSubComponentId = sub2.Id;
//            pos1.PlanogramProductId = prod1.Id;
//            pos1.FacingsHigh = 1;
//            pos1.FacingsWide = 1;
//            pos1.FacingsDeep = 1;
//            plan.Positions.Add(pos1);

//            //add a fixture annotation
//            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno1.PlanogramFixtureItemId = fixtureItem.Id;
//            anno1.Text = "Annotation 1";
//            plan.Annotations.Add(anno1);


//            //add an assembly
//            PlanogramAssembly assembly1 = PlanogramAssembly.NewPlanogramAssembly();
//            assembly1.Name = "Assembly1";
//            plan.Assemblies.Add(assembly1);

//            PlanogramFixtureAssembly fixtureAssembly1 = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
//            fixtureAssembly1.PlanogramAssemblyId = assembly1.Id;
//            fixtureAssembly1.Y = 70;
//            fixture.Assemblies.Add(fixtureAssembly1);

//            //add an assembly component
//            PlanogramComponent component3 = PlanogramComponent.NewPlanogramComponent();
//            component3.Name = "Shelf 2";
//            component3.Width = fixture.Width;
//            component3.Height = 4;
//            component3.Depth = 75;
//            plan.Components.Add(component3);

//            PlanogramAssemblyComponent assemblyComponent1 = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
//            assemblyComponent1.PlanogramComponentId = component3.Id;
//            assemblyComponent1.Y = 50;
//            assembly1.Components.Add(assemblyComponent1);

//            PlanogramSubComponent sub3 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub3.Name = "Shelf part";
//            sub3.MerchandisingType = PlanogramSubComponentMerchandisingType.Hang;
//            sub3.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub3.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub3.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub3.FillColourFront = -1;
//            sub3.FillColourBack = -1;
//            sub3.FillColourLeft = -1;
//            sub3.FillColourRight = -1;
//            sub3.FillColourTop = -1;
//            sub3.FillColourBottom = -1;
//            sub3.Width = component3.Width;
//            sub3.Height = component3.Height;
//            sub3.Depth = component3.Depth;
//            component3.SubComponents.Add(sub3);


//            //add a subcomponent annotation
//            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno2.PlanogramFixtureItemId = fixtureItem.Id;
//            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
//            anno2.PlanogramAssemblyComponentId = assemblyComponent1.Id;
//            anno2.PlanogramSubComponentId = sub3.Id;
//            anno2.Text = "Annotation 2";
//            plan.Annotations.Add(anno2);

//            return plan;
//        }

//        private static Planogram CreatePlanogramWithRod()
//        {
//            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

//            //add 1 plan
//            Planogram plan = Planogram.NewPlanogram();
//            plan.Name = "Plan 1";
//            package.Planograms.Add(plan);

//            //add a fixture
//            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
//            fixture.Name = "Fixture 1";
//            fixture.Height = 191;
//            fixture.Width = 120;
//            fixture.Depth = 100;
//            plan.Fixtures.Add(fixture);

//            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
//            fixtureItem.PlanogramFixtureId = fixture.Id;
//            fixtureItem.BaySequenceNumber = (Int16)1;
//            plan.FixtureItems.Add(fixtureItem);


//            //add a backboard
//            PlanogramComponent component1 = PlanogramComponent.NewPlanogramComponent();
//            component1.Name = "Backboard";
//            component1.Width = fixture.Width;
//            component1.Height = fixture.Height;
//            component1.Depth = 75;
//            plan.Components.Add(component1);

//            PlanogramFixtureComponent fixtureComponent1 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent1.PlanogramComponentId = component1.Id;
//            fixture.Components.Add(fixtureComponent1);

//            PlanogramSubComponent sub1 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub1.Name = "Backboard part";
//            sub1.LineThickness = 0.5F;
//            sub1.LineColour = -1;
//            sub1.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;
//            sub1.FillColourFront = -1;
//            sub1.FillColourBack = -1;
//            sub1.FillColourLeft = -1;
//            sub1.FillColourRight = -1;
//            sub1.FillColourTop = -1;
//            sub1.FillColourBottom = -1;
//            sub1.Width = component1.Width;
//            sub1.Height = component1.Height;
//            sub1.Depth = component1.Depth;
//            component1.SubComponents.Add(sub1);

//            //add a bar
//            PlanogramComponent component2 = PlanogramComponent.NewPlanogramComponent();
//            component2.Name = "Bar 1";
//            component2.Width = fixture.Width;
//            component2.Height = 4;
//            component2.Depth = 75;
//            component2.ComponentType = PlanogramComponentType.Rod;
//            plan.Components.Add(component2);

//            PlanogramFixtureComponent fixtureComponent2 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent2.PlanogramComponentId = component2.Id;
//            fixtureComponent2.Y = 50;
//            fixture.Components.Add(fixtureComponent2);

//            PlanogramSubComponent sub2 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub2.Name = "Shelf part";
//            sub2.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;
//            sub2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub2.FillColourFront = -1;
//            sub2.FillColourBack = -1;
//            sub2.FillColourLeft = -1;
//            sub2.FillColourRight = -1;
//            sub2.FillColourTop = -1;
//            sub2.FillColourBottom = -1;
//            sub2.Width = component2.Width;
//            sub2.Height = component2.Height;
//            sub2.Depth = component2.Depth;
//            component2.SubComponents.Add(sub2);

//            //add a product
//            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
//            prod1.Gtin = "Gtin1";
//            prod1.Name = "Product1";
//            plan.Products.Add(prod1);

//            //add a position
//            PlanogramPosition pos1 = PlanogramPosition.NewPlanogramPosition(plan.Positions.GetNextSequence());
//            pos1.PlanogramFixtureItemId = fixtureItem.Id;
//            pos1.PlanogramFixtureComponentId = fixtureComponent2.Id;
//            pos1.PlanogramSubComponentId = sub2.Id;
//            pos1.PlanogramProductId = prod1.Id;
//            pos1.FacingsHigh = 1;
//            pos1.FacingsWide = 1;
//            pos1.FacingsDeep = 1;
//            plan.Positions.Add(pos1);

//            //add a fixture annotation
//            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno1.PlanogramFixtureItemId = fixtureItem.Id;
//            anno1.Text = "Annotation 1";
//            plan.Annotations.Add(anno1);


//            //add an assembly
//            PlanogramAssembly assembly1 = PlanogramAssembly.NewPlanogramAssembly();
//            assembly1.Name = "Assembly1";
//            plan.Assemblies.Add(assembly1);

//            PlanogramFixtureAssembly fixtureAssembly1 = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
//            fixtureAssembly1.PlanogramAssemblyId = assembly1.Id;
//            fixtureAssembly1.Y = 70;
//            fixture.Assemblies.Add(fixtureAssembly1);

//            //add an assembly component
//            PlanogramComponent component3 = PlanogramComponent.NewPlanogramComponent();
//            component3.Name = "Shelf 2";
//            component3.Width = fixture.Width;
//            component3.Height = 4;
//            component3.Depth = 75;
//            plan.Components.Add(component3);

//            PlanogramAssemblyComponent assemblyComponent1 = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
//            assemblyComponent1.PlanogramComponentId = component3.Id;
//            assemblyComponent1.Y = 50;
//            assembly1.Components.Add(assemblyComponent1);

//            PlanogramSubComponent sub3 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub3.Name = "Shelf part";
//            sub3.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;
//            sub3.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub3.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub3.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub3.FillColourFront = -1;
//            sub3.FillColourBack = -1;
//            sub3.FillColourLeft = -1;
//            sub3.FillColourRight = -1;
//            sub3.FillColourTop = -1;
//            sub3.FillColourBottom = -1;
//            sub3.Width = component3.Width;
//            sub3.Height = component3.Height;
//            sub3.Depth = component3.Depth;
//            component3.SubComponents.Add(sub3);


//            //add a subcomponent annotation
//            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno2.PlanogramFixtureItemId = fixtureItem.Id;
//            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
//            anno2.PlanogramAssemblyComponentId = assemblyComponent1.Id;
//            anno2.PlanogramSubComponentId = sub3.Id;
//            anno2.Text = "Annotation 2";
//            plan.Annotations.Add(anno2);

//            return plan;
//        }
        
//        private static Planogram CreatePlanogramWithShelf()
//        {
//            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

//            //add 1 plan
//            Planogram plan = Planogram.NewPlanogram();
//            plan.Name = "Plan 1";
//            package.Planograms.Add(plan);

//            //add a fixture
//            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
//            fixture.Name = "Fixture 1";
//            fixture.Height = 191;
//            fixture.Width = 120;
//            fixture.Depth = 100;
//            plan.Fixtures.Add(fixture);

//            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
//            fixtureItem.PlanogramFixtureId = fixture.Id;
//            fixtureItem.BaySequenceNumber = (Int16)1;
//            plan.FixtureItems.Add(fixtureItem);


//            //add a backboard
//            PlanogramComponent component1 = PlanogramComponent.NewPlanogramComponent();
//            component1.Name = "Backboard";
//            component1.Width = fixture.Width;
//            component1.Height = fixture.Height;
//            component1.Depth = 75;
//            plan.Components.Add(component1);

//            PlanogramFixtureComponent fixtureComponent1 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent1.PlanogramComponentId = component1.Id;
//            fixture.Components.Add(fixtureComponent1);

//            PlanogramSubComponent sub1 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub1.Name = "Backboard part";
//            sub1.LineThickness = 0.5F;
//            sub1.LineColour = -1;
//            sub1.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
//            sub1.FillColourFront = -1;
//            sub1.FillColourBack = -1;
//            sub1.FillColourLeft = -1;
//            sub1.FillColourRight = -1;
//            sub1.FillColourTop = -1;
//            sub1.FillColourBottom = -1;
//            sub1.Width = component1.Width;
//            sub1.Height = component1.Height;
//            sub1.Depth = component1.Depth;
//            component1.SubComponents.Add(sub1);

//            //add a bar
//            PlanogramComponent component2 = PlanogramComponent.NewPlanogramComponent();
//            component2.Name = "Bar 1";
//            component2.Width = fixture.Width;
//            component2.Height = 4;
//            component2.Depth = 75;
//            component2.ComponentType = PlanogramComponentType.Shelf;
//            plan.Components.Add(component2);

//            PlanogramFixtureComponent fixtureComponent2 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent2.PlanogramComponentId = component2.Id;
//            fixtureComponent2.Y = 50;
//            fixture.Components.Add(fixtureComponent2);

//            PlanogramSubComponent sub2 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub2.Name = "Shelf part";
//            sub2.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
//            sub2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub2.FillColourFront = -1;
//            sub2.FillColourBack = -1;
//            sub2.FillColourLeft = -1;
//            sub2.FillColourRight = -1;
//            sub2.FillColourTop = -1;
//            sub2.FillColourBottom = -1;
//            sub2.Width = component2.Width;
//            sub2.Height = component2.Height;
//            sub2.Depth = component2.Depth;
//            component2.SubComponents.Add(sub2);

//            //add a product
//            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
//            prod1.Gtin = "Gtin1";
//            prod1.Name = "Product1";
//            prod1.Height = 20;
//            prod1.Width = 2;
//            prod1.Depth = 2;
//            plan.Products.Add(prod1);

//            //add a position
//            PlanogramPosition pos1 = PlanogramPosition.NewPlanogramPosition(plan.Positions.GetNextSequence());
//            pos1.PlanogramFixtureItemId = fixtureItem.Id;
//            pos1.PlanogramFixtureComponentId = fixtureComponent2.Id;
//            pos1.PlanogramSubComponentId = sub2.Id;
//            pos1.PlanogramProductId = prod1.Id;
//            pos1.FacingsHigh = 1;
//            pos1.FacingsWide = 1;
//            pos1.FacingsDeep = 1;
//            plan.Positions.Add(pos1);

//            //add a fixture annotation
//            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno1.PlanogramFixtureItemId = fixtureItem.Id;
//            anno1.Text = "Annotation 1";
//            plan.Annotations.Add(anno1);


//            //add an assembly
//            PlanogramAssembly assembly1 = PlanogramAssembly.NewPlanogramAssembly();
//            assembly1.Name = "Assembly1";
//            plan.Assemblies.Add(assembly1);

//            PlanogramFixtureAssembly fixtureAssembly1 = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
//            fixtureAssembly1.PlanogramAssemblyId = assembly1.Id;
//            fixtureAssembly1.Y = 70;
//            fixture.Assemblies.Add(fixtureAssembly1);

//            //add an assembly component
//            PlanogramComponent component3 = PlanogramComponent.NewPlanogramComponent();
//            component3.Name = "Shelf 2";
//            component3.Width = fixture.Width;
//            component3.Height = 4;
//            component3.Depth = 75;
//            plan.Components.Add(component3);

//            PlanogramAssemblyComponent assemblyComponent1 = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
//            assemblyComponent1.PlanogramComponentId = component3.Id;
//            assemblyComponent1.Y = 50;
//            assembly1.Components.Add(assemblyComponent1);

//            PlanogramSubComponent sub3 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub3.Name = "Shelf part";
//            sub3.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
//            sub3.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub3.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub3.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub3.FillColourFront = -1;
//            sub3.FillColourBack = -1;
//            sub3.FillColourLeft = -1;
//            sub3.FillColourRight = -1;
//            sub3.FillColourTop = -1;
//            sub3.FillColourBottom = -1;
//            sub3.Width = component3.Width;
//            sub3.Height = component3.Height;
//            sub3.Depth = component3.Depth;
//            component3.SubComponents.Add(sub3);


//            //add a subcomponent annotation
//            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno2.PlanogramFixtureItemId = fixtureItem.Id;
//            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
//            anno2.PlanogramAssemblyComponentId = assemblyComponent1.Id;
//            anno2.PlanogramSubComponentId = sub3.Id;
//            anno2.Text = "Annotation 2";
//            plan.Annotations.Add(anno2);

//            return plan;
//        }

//        private static Planogram CreatePlanogramWithShelfAndxNumberOfProductsWithEqualWidth(Int32 numberOfProductsToGenerate)
//        {
//            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);

//            //add 1 plan
//            Planogram plan = Planogram.NewPlanogram();
//            plan.Name = "Plan 1";
//            package.Planograms.Add(plan);

//            //add a fixture
//            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
//            fixture.Name = "Fixture 1";
//            fixture.Height = 191;
//            fixture.Width = 120;
//            fixture.Depth = 100;
//            plan.Fixtures.Add(fixture);

//            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
//            fixtureItem.PlanogramFixtureId = fixture.Id;
//            fixtureItem.BaySequenceNumber = (Int16)1;
//            plan.FixtureItems.Add(fixtureItem);


//            //add a backboard
//            PlanogramComponent component1 = PlanogramComponent.NewPlanogramComponent();
//            component1.Name = "Backboard";
//            component1.Width = fixture.Width;
//            component1.Height = fixture.Height;
//            component1.Depth = 75;
//            plan.Components.Add(component1);

//            PlanogramFixtureComponent fixtureComponent1 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent1.PlanogramComponentId = component1.Id;
//            fixture.Components.Add(fixtureComponent1);

//            PlanogramSubComponent sub1 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub1.Name = "Backboard part";
//            sub1.LineThickness = 0.5F;
//            sub1.LineColour = -1;
//            sub1.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
//            sub1.FillColourFront = -1;
//            sub1.FillColourBack = -1;
//            sub1.FillColourLeft = -1;
//            sub1.FillColourRight = -1;
//            sub1.FillColourTop = -1;
//            sub1.FillColourBottom = -1;
//            sub1.Width = component1.Width;
//            sub1.Height = component1.Height;
//            sub1.Depth = component1.Depth;
//            component1.SubComponents.Add(sub1);

//            //add a bar
//            PlanogramComponent component2 = PlanogramComponent.NewPlanogramComponent();
//            component2.Name = "Bar 1";
//            component2.Width = fixture.Width;
//            component2.Height = 4;
//            component2.Depth = 76;
//            component2.ComponentType = PlanogramComponentType.Shelf;
//            plan.Components.Add(component2);

//            PlanogramFixtureComponent fixtureComponent2 = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
//            fixtureComponent2.PlanogramComponentId = component2.Id;
//            fixtureComponent2.Y = 50;
//            fixture.Components.Add(fixtureComponent2);

//            PlanogramSubComponent sub2 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub2.Name = "Shelf part";
//            sub2.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
//            sub2.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub2.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub2.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub2.FillColourFront = -1;
//            sub2.FillColourBack = -1;
//            sub2.FillColourLeft = -1;
//            sub2.FillColourRight = -1;
//            sub2.FillColourTop = -1;
//            sub2.FillColourBottom = -1;
//            sub2.Width = component2.Width;
//            sub2.Height = component2.Height;
//            sub2.Depth = component2.Depth;
//            component2.SubComponents.Add(sub2);

//            //add x number of products with same dimensions
//            Int32 xNo = 1;
//            while (xNo <= numberOfProductsToGenerate)
//            {
//                PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
//                prod1.Gtin = String.Format("{0}{1}", "Gtin", xNo.ToString());
//                prod1.Name = String.Format("{0}{1}", "Product", xNo.ToString());
//                prod1.Height = 20;
//                prod1.Width = 2;
//                prod1.Depth = 2;
//                plan.Products.Add(prod1);

//                xNo++;

//                //add a position
//                PlanogramPosition pos1 = PlanogramPosition.NewPlanogramPosition(plan.Positions.GetNextSequence());
//                pos1.PlanogramFixtureItemId = fixtureItem.Id;
//                pos1.PlanogramFixtureComponentId = fixtureComponent2.Id;
//                pos1.PlanogramSubComponentId = sub2.Id;
//                pos1.PlanogramProductId = prod1.Id;
//                pos1.FacingsHigh = 1;
//                pos1.FacingsWide = 1;
//                pos1.FacingsDeep = 1;
//                plan.Positions.Add(pos1);
//            }
            

    
            

//            //add a fixture annotation
//            PlanogramAnnotation anno1 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno1.PlanogramFixtureItemId = fixtureItem.Id;
//            anno1.Text = "Annotation 1";
//            plan.Annotations.Add(anno1);


//            //add an assembly
//            PlanogramAssembly assembly1 = PlanogramAssembly.NewPlanogramAssembly();
//            assembly1.Name = "Assembly1";
//            plan.Assemblies.Add(assembly1);

//            PlanogramFixtureAssembly fixtureAssembly1 = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
//            fixtureAssembly1.PlanogramAssemblyId = assembly1.Id;
//            fixtureAssembly1.Y = 70;
//            fixture.Assemblies.Add(fixtureAssembly1);

//            //add an assembly component
//            PlanogramComponent component3 = PlanogramComponent.NewPlanogramComponent();
//            component3.Name = "Shelf 2";
//            component3.Width = fixture.Width;
//            component3.Height = 4;
//            component3.Depth = 75;
//            plan.Components.Add(component3);

//            PlanogramAssemblyComponent assemblyComponent1 = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
//            assemblyComponent1.PlanogramComponentId = component3.Id;
//            assemblyComponent1.Y = 50;
//            assembly1.Components.Add(assemblyComponent1);

//            PlanogramSubComponent sub3 = PlanogramSubComponent.NewPlanogramSubComponent();
//            sub3.Name = "Shelf part";
//            sub3.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
//            sub3.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
//            sub3.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            sub3.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            sub3.FillColourFront = -1;
//            sub3.FillColourBack = -1;
//            sub3.FillColourLeft = -1;
//            sub3.FillColourRight = -1;
//            sub3.FillColourTop = -1;
//            sub3.FillColourBottom = -1;
//            sub3.Width = component3.Width;
//            sub3.Height = component3.Height;
//            sub3.Depth = component3.Depth;
//            component3.SubComponents.Add(sub3);


//            //add a subcomponent annotation
//            PlanogramAnnotation anno2 = PlanogramAnnotation.NewPlanogramAnnotation();
//            anno2.PlanogramFixtureItemId = fixtureItem.Id;
//            anno2.PlanogramFixtureAssemblyId = fixtureAssembly1.Id;
//            anno2.PlanogramAssemblyComponentId = assemblyComponent1.Id;
//            anno2.PlanogramSubComponentId = sub3.Id;
//            anno2.Text = "Annotation 2";
//            plan.Annotations.Add(anno2);

//            return plan;
//        }

//        #endregion

//        [Test]
//        public void Method_GetNearestMultipleReturnsCorrectMulitple()
//        {
//            //Integers

//            Assert.AreEqual(3, AutofillHelper.getNearestMultiple(1, 3));
//            Assert.AreEqual(4, AutofillHelper.getNearestMultiple(1, 4));
//            Assert.AreEqual(5, AutofillHelper.getNearestMultiple(1, 5));
//            Assert.AreEqual(6, AutofillHelper.getNearestMultiple(2, 6));

//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(3, 12));
//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(3, 13));
//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(3, 14));
//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(4, 15));
//            Assert.AreEqual(16, AutofillHelper.getNearestMultiple(4, 16));

//            //Floating Numbers

//            Assert.AreEqual(3, AutofillHelper.getNearestMultiple(1, (Single)3.2));
//            Assert.AreEqual(4, AutofillHelper.getNearestMultiple(1, (Single)4.5));
//            Assert.AreEqual(5, AutofillHelper.getNearestMultiple(1, (Single)5.6));
//            Assert.AreEqual(6, AutofillHelper.getNearestMultiple(2, (Single)6.7));

//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(3, (Single)12.8));
//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(3, (Single)13.7));
//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(3, (Single)14.6));
//            Assert.AreEqual(12, AutofillHelper.getNearestMultiple(4, (Single)15.7));
//            Assert.AreEqual(16, AutofillHelper.getNearestMultiple(4, (Single)16.1));


//        }

//        [Test]
//        public void Method_IsAutofillAllowed()
//        {
//            PlanogramSubComponentView newView = CreateViewWithShelf();
//            PlanogramSubComponentPlacement sub = newView.Model.GetPlanogramSubComponentPlacement();

//            sub.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
//            sub.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
//            sub.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

//            PlanogramMerchandisingGroup newGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sub);
//            Assert.IsFalse(AutofillHelper.IsAutofillAllowed(newGroup));

//            //Check when we have manual merch types we don't autofill as thats not handled.
//            sub.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Right;
//            newGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sub);
//            Assert.IsFalse(AutofillHelper.IsAutofillAllowed(newGroup));

//            sub.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
//            newGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sub);
//            Assert.IsFalse(AutofillHelper.IsAutofillAllowed(newGroup));

//            sub.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
//            newGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sub);
//            Assert.IsTrue(AutofillHelper.IsAutofillAllowed(newGroup));

//            //Check that We can handle the other merchandising types.
//            sub.SubComponent.MerchandisingType =  PlanogramSubComponentMerchandisingType.Hang;
//            newGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sub);
//            Assert.IsTrue(AutofillHelper.IsAutofillAllowed(newGroup));

//            sub.SubComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;
//            newGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sub);
//            Assert.IsTrue(AutofillHelper.IsAutofillAllowed(newGroup));
//        }

//        [Test]
//        public void Scenario_HangMerchTypeCanOnlyStackOneFacingHigh()
//        {   
//            Planogram newPlanogramWithBar = CreatePlanogramWithBar();
//            PlanogramView newPlanogramView = new PlanogramView(newPlanogramWithBar);

//            using (PlanogramMerchandisingGroupList merchGroups = newPlanogramView.Model.GetMerchandisingGroups())
//            {
//                AutofillHelper.ProcessAutoFillHigh(merchGroups, newPlanogramView);

//                PlanogramMerchandisingGroup theMerchGroup = merchGroups.First(mg => mg.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang && mg.PositionPlacements.Count() > 0);
//                Assert.IsFalse(theMerchGroup.PositionPlacements.Any(pp => pp.Position.FacingsHigh > 1));
//            }
//        }

//        [Test]
//        public void Scenario_HangFromBottomMerchTypeCanOnlyStackOneFacingHigh()
//        {
//            Planogram newPlanogramWithRod = CreatePlanogramWithBar();
//            PlanogramView newPlanogramView = new PlanogramView(newPlanogramWithRod);

//            using (PlanogramMerchandisingGroupList merchGroups = newPlanogramView.Model.GetMerchandisingGroups())
//            {
//                AutofillHelper.ProcessAutoFillHigh(merchGroups, newPlanogramView);

//                PlanogramMerchandisingGroup theMerchGroup = merchGroups.First(mg => mg.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang && mg.PositionPlacements.Count() > 0);
//                Assert.IsFalse(theMerchGroup.PositionPlacements.Any(pp => pp.Position.FacingsHigh > 1));
//            }
//        }

//        [Test]
//        public void Scenario_OrientationChangeCausesAResize()
//        {
//            // As the dispatcher is used to call the process plan changes we cannot test whether the orientation makes a change as the dispatcher is 
//            // null in the test environment. 
//            throw new InconclusiveException("Cannot test plan process method call at the moment as dispatcher is Null in Test Environment."); 
            
//            //Planogram newPlanogramWithBar = CreatePlanogramWithShelf();
//            //PlanogramView newPlanogramView = new PlanogramView(newPlanogramWithBar);
//            //newPlanogramView.Model.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
//            //newPlanogramView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
//            //newPlanogramView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

//            //PlanogramMerchandisingGroupList merchGroups = newPlanogramView.Model.GetMerchandisingGroups();
//            //AutofillHelper.ProcessAutoFillWide(merchGroups, newPlanogramView);

//            //PlanogramPositionPlacement currentlyAssessingPP = merchGroups.First(mg => mg.PositionPlacements.Count() != 0).PositionPlacements.FirstOrDefault(pp => pp.Product.Gtin == "Gtin1");
//            //PlanogramPositionView actualChangedPositionView = newPlanogramView.EnumerateAllPositions().FirstOrDefault(p => (Int32)p.Model.Id == (Int32)currentlyAssessingPP.Position.Id);
//            //Int32 facings = actualChangedPositionView.FacingsWide;

//            ////actualChangedPositionView.OrientationType = PlanogramPositionOrientationType.Right90;
         
//            ////Shouldn't be necessary to call autofill
//            //Int32 newFacings = actualChangedPositionView.FacingsWide;
//            //Assert.AreNotEqual(facings, newFacings);
//        }

//        [Test]
//        public void Scenario_BasicWidthAutoFillTestWithTwoProducts()
//        {
//            Planogram newPlanogramWithBar = CreatePlanogramWithShelfAndxNumberOfProductsWithEqualWidth(2);
//            PlanogramView newPlanogramView = new PlanogramView(newPlanogramWithBar);
//            newPlanogramView.Model.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
//            newPlanogramView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
//            newPlanogramView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

//            using (PlanogramMerchandisingGroupList merchGroups = newPlanogramView.Model.GetMerchandisingGroups())
//            {
//                AutofillHelper.ProcessAutoFillWide(merchGroups, newPlanogramView);

//                PlanogramMerchandisingGroup currentlyAssessingMerchGroup = merchGroups.First(mg => mg.PositionPlacements.Count() != 0);

//                Assert.IsTrue(currentlyAssessingMerchGroup.SubComponentPlacements.First().GetWorldMerchandisingSpaceBounds(false).Width == 120);
//                IEnumerable<PlanogramPositionView> Positions = newPlanogramView.EnumerateAllPositions();

//                //We expect both positions to be placed 30 facings wide as they are an exact multiple of the shelf and there are just the two positions.
//                Assert.AreEqual(Positions.Count(prop => prop.FacingsWide == 30), 2, "I expected the products to both be filled evenly and exactly (products fit exactly) within the width of the merch area but they werent.");
//            }
//        }

//        [Test]
//        public void Scenario_BasicWidthAutoFillTestWithThreeProducts()
//        {
//            Planogram newPlanogramWithBar = CreatePlanogramWithShelfAndxNumberOfProductsWithEqualWidth(3);
//            PlanogramView newPlanogramView = new PlanogramView(newPlanogramWithBar);
//            newPlanogramView.Model.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
//            newPlanogramView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
//            newPlanogramView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

//            using (PlanogramMerchandisingGroupList merchGroups = newPlanogramView.Model.GetMerchandisingGroups())
//            {
//                AutofillHelper.ProcessAutoFillWide(merchGroups, newPlanogramView);

//                PlanogramMerchandisingGroup currentlyAssessingMerchGroup = merchGroups.First(mg => mg.PositionPlacements.Count() != 0);

//                Assert.IsTrue(currentlyAssessingMerchGroup.SubComponentPlacements.First().GetWorldMerchandisingSpaceBounds(false).Width == 120);
//                IEnumerable<PlanogramPositionView> Positions = newPlanogramView.EnumerateAllPositions();

//                //We expect both positions to be placed 30 facings wide as they are an exact multiple of the shelf and there are just the two positions.
//                Assert.AreEqual(Positions.Count(prop => prop.FacingsWide == 20), 3);
//            }
//        }

//        [Test]
//        public void Scenario_BasicDepthAutoFillTestWithOneProducts()
//        {
//            const Single shelfDepth = 76;
//            const Single expectedFacings = 38;

//            Planogram newPlanogramWithBar = CreatePlanogramWithShelfAndxNumberOfProductsWithEqualWidth(1);
//            PlanogramView newPlanogramView = new PlanogramView(newPlanogramWithBar);
//            newPlanogramView.Model.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
//            newPlanogramView.Model.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
//            newPlanogramView.Model.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;

//            using (PlanogramMerchandisingGroupList merchGroups = newPlanogramView.Model.GetMerchandisingGroups())
//            {
//                AutofillHelper.ProcessAutoFillDeep(merchGroups, newPlanogramView);

//                PlanogramMerchandisingGroup currentlyAssessingMerchGroup = merchGroups.First(mg => mg.PositionPlacements.Count() != 0);

//                Assert.IsTrue(currentlyAssessingMerchGroup.SubComponentPlacements.First().GetWorldMerchandisingSpaceBounds(false).Depth == shelfDepth);
//                IEnumerable<PlanogramPositionView> Positions = newPlanogramView.EnumerateAllPositions();

//                //We expect the position to be placed 38 facings deep.
//                Assert.AreEqual(Positions.Count(prop => prop.FacingsDeep == expectedFacings), 1, "I expected the products to both be filled evenly and exactly (products fit exactly) within the width of the merch area but they werent.");

//                //Facings deep * the product depth cannot exceed the space
//                PlanogramPositionDetails pDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(Positions.First().Model, Positions.First().Product.Model, currentlyAssessingMerchGroup.SubComponentPlacements.First());
//                Assert.IsTrue((pDetails.TotalSize.Depth) <= shelfDepth);
//            }
//        }

//    }
//}
