﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2)
// V8-28987 : I.George
//  Created 
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf;

namespace Galleria.Ccm.UnitTests.UI.Common.Selectors
{   
    [TestFixture]
    public class ClusterSchemeSelectionViewModelTests : TestBase
    {
        public  override void Setup()
        {
            base.Setup();
            var entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<ClusterSchemeDto> clusterSchemeDto = TestDataHelper.InsertClusterSchemeDtos(this.DalFactory, entityId, 1);
            App.ViewState.EntityId = entityId;
            var testModel = new ClusterSchemeSelectionViewModel();
        }

        [Test]
        public void Property_ClusterSchemeProductGroupRows()
        {
            String PropertyName = "ClusterSchemeProductGroupRows";
            Assert.AreEqual(PropertyName, ClusterSchemeSelectionViewModel.ClusterSchemeProductGroupRowsProperty.Path);
        }

        [Test]
        public void Property_SelectedClusterSchemeProductGroupRow()
        {
            String PropertyName = "SelectedClusterSchemeProductGroupRow";
            Assert.AreEqual(PropertyName, ClusterSchemeSelectionViewModel.SelectedClusterSchemeProductGroupRowProperty.Path);
        }

        #region Constructor
        [Test]
         public void Constructor()
         {
            // check that a lis of clusterSchemes gets loaded
             var viewModel = new ClusterSchemeSelectionViewModel();
             IEnumerable<ClusterSchemeInfo> clusterSchemeInfo = viewModel.MasterClusterSchemeInfoList;
             Assert.IsNotNull(clusterSchemeInfo);
         }
        #endregion

        #region Command

        [Test]
        public void ClusterScheme_OKCommand()
        {
            var viewModel = new ClusterSchemeSelectionViewModel();
            Assert.IsFalse(viewModel.OKCommand.CanExecute(), "no cluster scheme info has been set");

            viewModel.SelectedClusterSchemeInfo = viewModel.MasterClusterSchemeInfoList.FirstOrDefault();
            Assert.IsTrue(viewModel.OKCommand.CanExecute());
        }

        #endregion
        
    }
}
  
   
