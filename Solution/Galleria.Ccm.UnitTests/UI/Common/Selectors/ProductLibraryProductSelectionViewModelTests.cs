﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26705 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.UnitTests.UI.Common.Selectors
{
    [TestFixture]
    public class ProductLibraryProductSelectionViewModelTests : TestBase
    {
        private ProductLibraryProductSelectionViewModel _testModel;
        private ProductList _products;
        private Boolean _handlerCalled;

        [SetUp]
        public void SetUp()
        {
            var entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1).First().Id;
            var productIds = TestDataHelper.InsertProductDtos(base.DalFactory, 10, entityId).Select(dto => dto.Id);
            _products = ProductList.FetchByProductIds(productIds);
            _testModel = new ProductLibraryProductSelectionViewModel(_products);
            _handlerCalled = false;
        }

        [Test]
        public void ProductLibraryProductSelectionViewModel_PopulatesSearchResultsWithProducts()
        {
            CollectionAssert.AreEqual(_products, _testModel.SearchResults);
        }

        [Test]
        public void SelectAndCloseCommand_CannotExecute_WhenNoProductsSelected()
        {
            var result = _testModel.SelectAndCloseCommand.CanExecute();

            Assert.IsFalse(result);
        }

        [Test]
        public void SelectAndCloseCommand_CanExecute_WhenProductsAreSelected()
        {
            _testModel.SelectedProducts.Add(_products.First());
            
            var result = _testModel.SelectAndCloseCommand.CanExecute();

            Assert.IsTrue(result);
        }

        [Test]
        public void SelectAndCloseCommand_FiresEvent_WhenExecuted()
        {
            _testModel.SelectedProducts.Add(_products.First());
            _testModel.AddToSelectionRequested += AddSelectionHandler;
            _testModel.SelectAndCloseCommand.Execute();
            Assert.That(_handlerCalled);
        }

        [Test]
        public void SearchCriteriaChanges_UpdateSearchResults()
        {
            var product= _products.First();

            _testModel.SearchCriteria = product.Name;

            CollectionAssert.Contains(_testModel.SearchResults,product);
        }

        private void AddSelectionHandler(Object sender, EventArgs<IEnumerable<Product>> e)
        {
            _handlerCalled = true;
        }

    }
}
