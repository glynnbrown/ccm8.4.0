﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2)
// V8-28987 : I.George
//  Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.UI.Common.Selectors
{   
    [TestFixture]
    class LocationClusterSelectionViewModelTests : TestBase
    {
        private ClusterScheme _clusterScheme;
        
        public override void Setup()
        {
            base.Setup();
            var entityId = TestDataHelper.InsertEntityDtos(base.DalFactory, 1)[0].Id;
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 1, entityId);
            List<ClusterSchemeDto> clusterSchemeDto = TestDataHelper.InsertClusterSchemeDtos(this.DalFactory, entityId, 1);
            List<ClusterDto> clusterDtos = TestDataHelper.InsertClusterDtos(this.DalFactory, clusterSchemeDto, 1);
            List<ClusterLocationDto> clusterLocationDtos = TestDataHelper.InsertClusterLocationDtos(this.DalFactory, clusterDtos, locationDtos);
            _clusterScheme = ClusterScheme.FetchById(clusterSchemeDto.FirstOrDefault().Id);
            App.ViewState.EntityId = entityId;
            var TestModel = new LocationClusterSelectorViewModel(_clusterScheme, true);
        }

        #region Property
        [Test]
        public void Property_SelectedClusterScheme()
        {
            String PropertyName = "SelectedClusterScheme";
            Assert.AreEqual(PropertyName, LocationClusterSelectorViewModel.SelectedClusterSchemeProperty.Path);
        }

        [Test]
        public void Property_AssignedLocations()
        {
            String PropertyName = "AssignedLocations";
            Assert.AreEqual(PropertyName, LocationClusterSelectorViewModel.AssignedLocationsProperty.Path);
        }

        [Test]
        public void Property_ClusterList()
        {
            String PropertyName = "ClusterList";
            Assert.AreEqual(PropertyName, LocationClusterSelectorViewModel.ClusterListProperty.Path);
        }

        [Test]
        public void Property_CanShowClusters()
        {
            String PropertyName = "CanShowClusters";
            Assert.AreEqual(PropertyName, LocationClusterSelectorViewModel.CanShowClustersProperty.Path);
        }

        [Test]
        public void Property_SelectedCluster_OnSelectedClusterSchemeChanged()
        {
            var viewModel = new LocationClusterSelectorViewModel(_clusterScheme, true);
            String PropertyName = "SelectedCluster";
            Assert.AreEqual(PropertyName, LocationClusterSelectorViewModel.SelectedClusterProperty.Path);

            //check that the locations gets set
            viewModel.SelectedCluster = this._clusterScheme.Clusters.FirstOrDefault();
            Assert.AreEqual(viewModel.SelectedCluster.Locations.Count, viewModel.AssignedLocations.Count);
        }
        
        #endregion

        [Test]
        public void Constructor()
        {
            var viewModel = new LocationClusterSelectorViewModel(_clusterScheme, true);
            int clusterCount = viewModel.ClusterList.Count;
            Assert.IsNotNull(this._clusterScheme.Clusters);
            Assert.AreEqual(this._clusterScheme.Clusters.Count, clusterCount, "should have returned the same number of clusters");
            
            //Test it when no clusterSchemeName is set
            var TestViewModel = new LocationClusterSelectorViewModel(_clusterScheme, false);
            Assert.IsFalse(TestViewModel.CanShowClusters, "No cluster should be shown");
        }

        [Test]
        public void OK_Command()
        {
            var viewModel = new LocationClusterSelectorViewModel(_clusterScheme, true);
            Assert.IsFalse(viewModel.OKCommand.CanExecute(), "No cluser has been set so button should be disabled");

            viewModel.SelectedCluster = this._clusterScheme.Clusters.FirstOrDefault();
            Assert.IsTrue(viewModel.OKCommand.CanExecute());
        }
    }
}
