﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibrary;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using Galleria.Framework.Imports;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement;

namespace Galleria.Ccm.UnitTests.UI.ProductLibrary
{
    [TestFixture]
    public class ProductLibraryValidationViewModelTests : TestBase<ProductLibraryValidationViewModel>
    {
        #region Test Fixture Helpers

        public override void Setup()
        {
            base.Setup();

            ImportFileData importFileData = ImportFileData.NewImportFileData();
            ImportDataValidationViewModel dataValidation = new ImportDataValidationViewModel(importFileData);
            this.TestModel = new ProductLibraryValidationViewModel(dataValidation);
        }

        #endregion

        #region Properties

        #region Property_ImportDataValidationViewModel

        [Test]
        public void Property_ImportDataValidationViewModel()
        {
            String propertyName = "ImportDataValidationViewModel";
            Assert.AreEqual(propertyName, ProductLibraryValidationViewModel.ImportDataValidationViewModelProperty.Path);

            Assert.IsNotNull(this.TestModel.ImportDataValidationViewModel);
        }

        #endregion

        #endregion

        #region Commands

        #region Command_ExportErrorsCommand

        [Test]
        public void Command_ExportErrorsCommand()
        {
            RelayCommand cmd = this.TestModel.ExportErrorsCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(this.TestModel.ImportDataValidationViewModel.WorksheetErrors.Count == 0);
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without any errors");

            //Cant test errors can execute as errors need to exist and that is loaded async
        }

        #endregion

        #region Command_LoadCommand

        [Test]
        public void Command_LoadCommand()
        {
            RelayCommand cmd = this.TestModel.LoadCommand;
            Assert.Contains(cmd, this.TestModel.ViewModelCommands);

            Assert.IsTrue(this.TestModel.ImportDataValidationViewModel.ValidatedFileData.GetValidRows().Count() == 0);
            Assert.IsFalse(cmd.CanExecute(), "Command should be disabled without no data");

            //Cant test errors can execute as errors need to exist and that is loaded async
        }

        #endregion

        #endregion
    }
}
