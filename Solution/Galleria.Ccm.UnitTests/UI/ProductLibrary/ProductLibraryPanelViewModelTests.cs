﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25395 A.Probyn
//  Created
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibrary;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI.ProductLibrary
{
    [TestFixture]
    public class ProductLibraryPanelViewModelTests : TestBase<ProductLibraryPanelViewModel>
    {
        #region Test Fixture Helpers

        public override void Setup()
        {
            base.Setup();

            this.TestModel = new ProductLibraryPanelViewModel();
        }

        #endregion

        #region Properties

        [Test]
        public void Property_SelectedProductLibraryStatus()
        {
            String propertyName = "SelectedProductLibraryStatus";
            Assert.AreEqual(propertyName, ProductLibraryPanelViewModel.SelectedProductLibraryStatusProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_Products()
        {
            String propertyName = "Products";
            Assert.AreEqual(propertyName, ProductLibraryPanelViewModel.ProductsProperty.Path);

            AssertHelper.AssertPropertyIsReadOnly(this.TestModel.GetType(), propertyName);
        }

        [Test]
        public void Property_ProductLoadInProgress()
        {
            String propertyName = "ProductLoadInProgress";
            Assert.AreEqual(propertyName, ProductLibraryPanelViewModel.ProductLoadInProgressProperty.Path);

            //Check initial value
            Assert.IsFalse(this.TestModel.ProductLoadInProgress);

            //Check setter
            Boolean newValue = true;
            this.TestModel.ProductLoadInProgress = newValue;
            Assert.AreEqual(newValue, this.TestModel.ProductLoadInProgress);
        }

        [Test]
        public void Property_ProductLoadProgress()
        {
            String propertyName = "ProductLoadProgress";
            Assert.AreEqual(propertyName, ProductLibraryPanelViewModel.ProductLoadProgressProperty.Path);

            //Check initial value
            Assert.AreEqual(0, this.TestModel.ProductLoadProgress);

            //Check setter
            Int32 newValue = 48;
            this.TestModel.ProductLoadProgress = newValue;
            Assert.AreEqual(newValue, this.TestModel.ProductLoadProgress);
        }

        [Test]
        public void Property_CurrentProductLibrary()
        {
            String propertyName = "CurrentProductLibrary";
            Assert.AreEqual(propertyName, ProductLibraryPanelViewModel.CurrentProductLibraryProperty.Path);

            //Check initial value
            Assert.IsNotNull(this.TestModel.CurrentProductLibrary);
            Assert.IsNull(this.TestModel.CurrentProductLibrary.Model);

            //Check setter
            //Ccm.Model.ProductLibrary productLibrary = Ccm.Model.ProductLibrary.NewProductLibrary(ProductLibraryDalFactoryType.FileSystem);
            //ProductLibraryViewModel newValue = new ProductLibraryViewModel();
            //newValue.Model = productLibrary;
            //this.TestModel.CurrentProductLibrary = newValue;
            //Assert.AreEqual(newValue, this.TestModel.CurrentProductLibrary);

            //NB - Cant test product load as its asynchronous
        }

        [Test]
        public void Property_CurrentProductUniverse()
        {
            String propertyName = "CurrentProductUniverse";
            Assert.AreEqual(propertyName, ProductLibraryPanelViewModel.CurrentProductUniverseProperty.Path);

            //Check initial value
            Assert.AreEqual(null, this.TestModel.CurrentProductUniverse);

            //Check setter
            Product product = Product.NewProduct();
            product.Name = "Test";
            ProductUniverse newValue = ProductUniverse.NewProductUniverse(1);
            newValue.Products.Add(ProductUniverseProduct.NewProductUniverseProduct(product));
            this.TestModel.CurrentProductUniverse = newValue;
            Assert.AreEqual(newValue, this.TestModel.CurrentProductUniverse);

            //NB - Cant test product load as its asynchronous
        }

        #endregion

        #region Others

        [Test]
        public void GEM26523_CanAddProductWhenNotConnectedToRepository()
        {
            //clear any repository connection
            App.ViewState.ClearRepositoryConnection();

            //create a plan
            PlanogramView planView =
                PackageViewModel.NewPackageViewModel(TestDataHelper.CreatePlanCreweSparklingWater(), false).PlanogramViews.First();

            //create a new product
            Product product = Product.NewProduct();
            product.Name = "Product A";
            product.Height = 10;
            product.Width = 10;
            product.Depth = 10;

            PlanogramSubComponentView shelfSub = 
                planView.EnumerateMerchandisableSubComponents().First(s => s.Component.ComponentType == Framework.Planograms.Model.PlanogramComponentType.Shelf);

            shelfSub.AddPositions(new List<Product> { product }, new PointValue());
        }

        #endregion
    }
}
