﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25395 A.Probyn
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Imports;
using Csla;
using System.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.IO;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;

namespace Galleria.Ccm.UnitTests.UI.ProductLibrary
{
    [TestFixture]
    public class ProductLibraryMaintenanceViewModelTests : TestBase
    {
        #region Test Fixture Helpers

        public override void Setup()
        {
            base.Setup();

            //copy the sample file to the test directory
            String newSample = 
                System.IO.Path.Combine(
                base.TestDir, 
                System.IO.Path.GetFileName("Cereals Universe.xlsx"));

            if (!System.IO.File.Exists(newSample))
            {
                System.IO.File.Copy(
                    @".\Sample Files\Common\Customer Centric Merchandising\Product Universes\Cereals Universe.xlsx", 
                    newSample);
            }

            _sampleLibraryData = newSample;
        }


        String _sampleLibraryData = @".\Sample Files\Common\Customer Centric Merchandising\Product Universes\Cereals Universe.xlsx";


        private Ccm.Model.ProductLibrary CreateMappingTemplateFile()
        {
            Ccm.Model.ProductLibrary model = Ccm.Model.ProductLibrary.NewProductLibrary();
            model.Name = "Test";
            model.ExcelWorkbookSheet = "Cereals Universe";
            model.IsFirstRowHeaders = true;

            ProductLibraryColumnMapping mapping1 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping1.Destination = "Gtin";
            mapping1.Source = "GTIN";
            mapping1.Type = "System.String";
            model.ColumnMappings.Add(mapping1);

            ProductLibraryColumnMapping mapping2 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping2.Destination = "Name";
            mapping2.Source = "Name";
            mapping2.Type = "System.String";
            model.ColumnMappings.Add(mapping2);

            ProductLibraryColumnMapping mapping3 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping3.Destination = "Height";
            mapping3.Source = "Height";
            mapping3.Type = "System.Single";
            model.ColumnMappings.Add(mapping3);

            String newTemplateFile = 
                System.IO.Path.ChangeExtension(System.IO.Path.Combine(
                base.TestDir,
                Guid.NewGuid().ToString()), Ccm.Model.ProductLibrary.FileExtension);


            model.SaveAsFile(newTemplateFile);
            model.Dispose();

            return Ccm.Model.ProductLibrary.FetchByFilename(newTemplateFile, true);
        }

        private Ccm.Model.ProductLibrary CreateMappingTemplateRepositoryItem()
        {
            base.SetMockRepositoryConnection();

            Ccm.Model.ProductLibrary model = Ccm.Model.ProductLibrary.NewProductLibrary();
            model.Name = Guid.NewGuid().ToString();
            model.ExcelWorkbookSheet = "Cereals Universe";
            model.IsFirstRowHeaders = true;

            ProductLibraryColumnMapping mapping1 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping1.Destination = "Gtin";
            mapping1.Source = "GTIN";
            mapping1.Type = "System.String";
            model.ColumnMappings.Add(mapping1);

            ProductLibraryColumnMapping mapping2 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping2.Destination = "Name";
            mapping2.Source = "Name";
            mapping2.Type = "System.String";
            model.ColumnMappings.Add(mapping2);

            ProductLibraryColumnMapping mapping3 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping3.Destination = "Height";
            mapping3.Source = "Height";
            mapping3.Type = "System.Single";
            model.ColumnMappings.Add(mapping3);

            model = model.SaveAsToRepository(CCMClient.ViewState.EntityId);
            model.Dispose();

            return model;
        }

        #endregion


        #region Properties

        [Test]
        public void PropertiesAreInitialized()
        {
            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();

            //CurrentProductLibrary
            Assert.IsNotNull(vm.CurrentProductLibrary);
            Assert.IsTrue(vm.CurrentProductLibrary.IsNew);

            //FileData
            Assert.IsNotNull(vm.FileData);

            //AvailableWorksheets
            Assert.AreEqual(0, vm.AvailableWorksheets.Count);

            //SelectedWorksheet
            Assert.IsNullOrEmpty(vm.SelectedWorksheet);

            //PreviewData
            Assert.AreEqual(0, vm.PreviewData.Count);

            //FilePath
            Assert.IsNullOrEmpty(vm.FilePath);
        }

        [Test]
        public void FilePath_BlankedOffOnNewCommand()
        {
            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();
            base.WindowService.ShowOpenFileDialogSetReponse(true, _sampleLibraryData);
            vm.BrowseForFileCommand.Execute();

            Assert.IsNotNull(vm.FilePath);

            vm.NewCommand.Execute();
            Assert.IsNullOrEmpty(vm.FilePath);
        }

        #endregion

        #region Commands

        [Test]
        public void NewCommand_Execution()
        {
            var viewModel = new ProductLibraryMaintenanceViewModel();
            var oldModel = viewModel.CurrentProductLibrary;

            viewModel.NewCommand.Execute();

            Assert.AreNotEqual(oldModel, viewModel.CurrentProductLibrary);
            Assert.IsTrue(viewModel.CurrentProductLibrary.IsNew);
        }

        [Test]
        public void BrowseForFileCommand_Execution()
        {
            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();
            base.WindowService.ShowOpenFileDialogSetReponse(true, _sampleLibraryData);
            vm.BrowseForFileCommand.Execute();

            Assert.AreEqual(_sampleLibraryData, vm.FilePath);
        }

        [Test]
        public void OpenFromRepositoryCommand_Execution()
        {
            Ccm.Model.ProductLibrary libraryToOpen = CreateMappingTemplateRepositoryItem();

            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();
            vm.OpenFromRepositoryCommand.Execute(libraryToOpen.Id);

            Assert.AreEqual(libraryToOpen.Id, vm.CurrentProductLibrary.Id);
        }

        [Test]
        public void OpenFromFileCommand_Execution()
        {
            Ccm.Model.ProductLibrary libraryToOpen = CreateMappingTemplateFile();

            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();
            base.WindowService.ShowOpenFileDialogSetReponse(true, (String)libraryToOpen.Id);
            vm.OpenFromFileCommand.Execute();

            Assert.AreEqual(vm.CurrentProductLibrary.Id, libraryToOpen.Id);

            vm.Dispose();
        }

        [Test]
        public void SaveCommand_Execution_FileItem()
        {
            //checks that a mapping template file can be updated.
            Ccm.Model.ProductLibrary libraryToOpen = CreateMappingTemplateFile();

            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();
            base.WindowService.ShowOpenFileDialogSetReponse(true, (String)libraryToOpen.Id);
            vm.OpenFromFileCommand.Execute();

            vm.FileData.MappingList[3].ColumnReference = "Width";

            Assert.IsTrue(vm.SaveCommand.CanExecute());

            vm.SaveCommand.Execute();
            vm.Dispose();


            //open and check the file
            Ccm.Model.ProductLibrary savedLibrary =  Ccm.Model.ProductLibrary.FetchByFilename((String)libraryToOpen.Id, true);
            Assert.AreEqual("Width", savedLibrary.ColumnMappings[3].Source);
        }

        [Test]
        public void DeleteCommand_Execution_FileItem()
        {
            //checks that a file item can be deleted
            Ccm.Model.ProductLibrary libraryToOpen = CreateMappingTemplateFile();

            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();
            base.WindowService.ShowOpenFileDialogSetReponse(true, (String)libraryToOpen.Id);
            vm.OpenFromFileCommand.Execute();

            vm.DeleteCommand.Execute();

            Assert.IsFalse(System.IO.File.Exists((String)libraryToOpen.Id));

            vm.Dispose();
        }

        [Test]
        public void DeleteCommand_Execution_RepositoryItem()
        {
            Ccm.Model.ProductLibrary libraryToOpen = CreateMappingTemplateRepositoryItem();

            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();
            vm.OpenFromRepositoryCommand.Execute(libraryToOpen.Id);

            vm.DeleteCommand.Execute();

            Assert.AreEqual(0,
            Ccm.Model.ProductLibraryInfoList.FetchByIds(new List<Object> { libraryToOpen.Id }).Count);

            vm.Dispose();
        }

        [Test]
        public void CloseCommand_Execution()
        {
            var viewModel = new ProductLibraryMaintenanceViewModel();

            Boolean winClosed = false;
            WindowService.AddResponse(NUnitWindowService.CloseWindowMethod, (p) => winClosed = true);
            viewModel.CloseCommand.Execute();

            Assert.IsTrue(winClosed, "window should have been closed");
        }


        #endregion

        #region Scenario Tests

        [Test]
        public void CreateFileItem()
        {
            //V8-32361 - checks that a file mapping template can be created.
            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel();

            //open the sample file
            base.WindowService.ShowOpenFileDialogSetReponse(true, _sampleLibraryData);
            vm.BrowseForFileCommand.Execute();

            //ensure that some fields are mapped.
            vm.FileData.MappingList[0].ColumnReference = "Gtin";
            vm.FileData.MappingList[1].ColumnReference = "Name";
            vm.FileData.MappingList[2].ColumnReference = "Height";

            //save the tempate
            String newTemplateFile =
                System.IO.Path.ChangeExtension(System.IO.Path.Combine(
                base.TestDir,
                Guid.NewGuid().ToString()), Ccm.Model.ProductLibrary.FileExtension);

            base.WindowService.ShowSaveFileDialogSetReponse(true, newTemplateFile);
            vm.SaveAsToFileCommand.Execute();

            Assert.IsTrue(System.IO.File.Exists(newTemplateFile));
            //Assert.IsFalse(vm.CurrentProductLibrary.IsDirty);

            //check that the sample file is still in place.
            Assert.AreEqual(_sampleLibraryData, vm.FilePath);

            //blank off
            vm.NewCommand.Execute();
            Assert.IsTrue(vm.CurrentProductLibrary.IsNew);

            //reopen the file
            base.WindowService.ShowOpenFileDialogSetReponse(true, newTemplateFile);
            vm.OpenFromFileCommand.Execute();


            //check
            Assert.AreEqual(vm.CurrentProductLibrary.Id, newTemplateFile);

            Assert.AreEqual(vm.FileData.MappingList[0].ColumnReference, "Gtin");
            Assert.AreEqual(vm.FileData.MappingList[1].ColumnReference, "Name");
            Assert.AreEqual(vm.FileData.MappingList[2].ColumnReference, "Height");


            vm.Dispose();
        }

        [Test]
        public void LoadProductUniverseFile_UsingDefaultFileMappingTemplate()
        {
            var planController = base.AddNewEmptyPlan();

            //create a default mapping template
            Ccm.Model.ProductLibrary libraryToOpen = CreateMappingTemplateFile();

            //set it as the settings default.
            App.ViewState.Settings.Model.DefaultProductUniverseTemplateId = (String)libraryToOpen.Id;

            //check that no template exists in folder for the spreadsheet
            Assert.IsFalse(System.IO.File.Exists(
                @".\Sample Files\Common\Customer Centric Merchandising\Product Universes\Cereals Universe.pogpl"));

            //load the universe - it should load silently using the default template
            base.WindowService.ShowOpenFileDialogSetReponse(true, _sampleLibraryData);
            MainPageCommands.SelectProductLibraryFromFile.Execute();

            //check that the default mapping template was loaded.
            Assert.AreEqual(App.ViewState.ProductLibraryView.Model.Id, libraryToOpen.Id);
        }

        [Test]
        public void LoadProductUniverseFile_UsingActualFileMappingTemplate()
        {
            var planController = base.AddNewEmptyPlan();

            //create a default mapping template
            Ccm.Model.ProductLibrary defaultLibrary = CreateMappingTemplateFile();
            App.ViewState.Settings.Model.DefaultProductUniverseTemplateId = (String)defaultLibrary.Id;

            //create a copy but save as the specific template name.
            Ccm.Model.ProductLibrary specificLibrary = 
                defaultLibrary.SaveAsFile(System.IO.Path.ChangeExtension(_sampleLibraryData, Ccm.Model.ProductLibrary.FileExtension));
            specificLibrary.Dispose();
            Assert.IsTrue(System.IO.File.Exists((String)specificLibrary.Id));
                

            //load the universe - it should load silently using the specific template
            base.WindowService.ShowOpenFileDialogSetReponse(true, _sampleLibraryData);
            MainPageCommands.SelectProductLibraryFromFile.Execute();

            //check that the default mapping template was loaded.
            Assert.AreEqual(App.ViewState.ProductLibraryView.Model.Id, specificLibrary.Id);
        }

        [Test]
        public void LoadProductUniverseFile_UsingNonMatchingDefaultFileMappingTemplate()
        {
            //create a default file template with no matching columns
            
            Ccm.Model.ProductLibrary defaultFileTemplate = Ccm.Model.ProductLibrary.NewProductLibrary();
            defaultFileTemplate.Name = "Test";
            defaultFileTemplate.ExcelWorkbookSheet = "Cereals Universe";
            defaultFileTemplate.IsFirstRowHeaders = true;

            ProductLibraryColumnMapping mapping1 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping1.Destination = "GTIN";
            mapping1.Source = "TEST1";
            mapping1.Type = "System.String";
            defaultFileTemplate.ColumnMappings.Add(mapping1);

            ProductLibraryColumnMapping mapping2 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping2.Destination = "Name";
            mapping2.Source = "Test2";
            mapping2.Type = "System.String";
            defaultFileTemplate.ColumnMappings.Add(mapping2);

            ProductLibraryColumnMapping mapping3 = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
            mapping3.Destination = "Height";
            mapping3.Source = "Test3";
            mapping3.Type = "System.Single";
            defaultFileTemplate.ColumnMappings.Add(mapping3);

            String newTemplateFile =
                System.IO.Path.ChangeExtension(System.IO.Path.Combine(
                base.TestDir,
                Guid.NewGuid().ToString()), Ccm.Model.ProductLibrary.FileExtension);

            defaultFileTemplate.SaveAsFile(newTemplateFile);
            defaultFileTemplate.Dispose();

            //set it as the settings default.
            App.ViewState.Settings.Model.DefaultProductUniverseTemplateId = (String)defaultFileTemplate.Id;

            //check that no template exists in folder for the spreadsheet
            Assert.IsFalse(System.IO.File.Exists(
                @".\Sample Files\Common\Customer Centric Merchandising\Product Universes\Cereals Universe.pogpl"));

            //load the universe - it should load silently using the default template
            base.WindowService.ShowOpenFileDialogSetReponse(true, _sampleLibraryData);

            base.WindowService.ShowWindowSetReponse(
                (a)=>
                {
                    Assert.IsTrue(a.WindowType == typeof(ProductLibraryMaintenanceOrganiser));
                });
            MainPageCommands.SelectProductLibraryFromFile.Execute();

            base.WindowService.AssertResponsesUsed();

        }

        [Test]
        public void LoadProductUniverseFile_UsingDefaultRepositoryMappingTemplate()
        {
            var planController = base.AddNewEmptyPlan();

            //create a default mapping template
            Ccm.Model.ProductLibrary libraryToOpen = CreateMappingTemplateRepositoryItem();

            //set it as the settings default.
            App.ViewState.Settings.Model.DefaultProductUniverseTemplateId = libraryToOpen.Name;

            //check that no template exists in folder for the spreadsheet
            Assert.IsFalse(System.IO.File.Exists(
                @".\Sample Files\Common\Customer Centric Merchandising\Product Universes\Cereals Universe.pogpl"));

            //load the universe - it should load silently using the default template
            base.WindowService.ShowOpenFileDialogSetReponse(true, _sampleLibraryData);
            MainPageCommands.SelectProductLibraryFromFile.Execute();

            //check that the default mapping template was loaded.
            Assert.AreEqual(App.ViewState.ProductLibraryView.Model.Id, libraryToOpen.Id);
        }


        #endregion
    }
}
