﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// V8-25645 : A.Silva
//  Created

#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Startup;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.DbConnectionWizard
{
    [TestFixture]
    class DbConnectionWizardViewModelTests //: TestBase<DbConnectionWizardViewModel>
    {
        //public override void Setup()
        //{
        //    base.Setup();

        //    TestModel = new DbConnectionWizardViewModel(true);
        //}

        [Test]
        public void ConnectionTypeProperty_HasCorrectPath()
        {
            const string expected = "ConnectionType";

            Assert.AreEqual(expected, DbConnectionWizardViewModel.ConnectionTypeProperty.Path);
        }

    }
}