﻿using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Settings;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Rendering;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI
{
    /// <summary>
    /// Contains scenario tests which check the basic fixture label functionality of Space Planning.
    /// If any of these tests are broken then functionality is considered to not be working.
    /// </summary>
    [TestFixture]
    [Category(Categories.Smoke)]
    public class FixtureLabelFunctionalityTests : TestBase
    {
        #region TestFixtureHelpers

        private void OpenAndApplyLabelFile(String path)
        {
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    FixtureLabelEditorViewModel LabelEditorVm = (FixtureLabelEditorViewModel)p.WindowParameters[0];
                    //reopen the original file
                    LabelEditorVm.OpenFromFileCommand.Execute(path);

                    //apply
                    LabelEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            WindowService.CloseWindowSetNullResponse();
            MainPageCommands.ShowFixtureLabelEditor.Execute();
        }

        #endregion

        [Test]
        public void CanCreateAndUseLabelFromFile()
        {
            //Checks that it is possible to save a Label to file then reopen it

            const String testField = "[PlanogramComponent.Name]";

            var planController = base.AddNewCreweSparklingWaterPlan();

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    FixtureLabelEditorViewModel labelEditorVm = (FixtureLabelEditorViewModel)p.WindowParameters[0];

                    //Create a new Label
                    labelEditorVm.SelectedLabel.Text = testField;

                    //save to file
                    labelEditorVm.SaveAsToFileCommand.Execute(expectedPath);

                    //check file exists
                    Assert.IsTrue(System.IO.File.Exists(expectedPath));

                    //change a property on the Label
                    labelEditorVm.SelectedLabel.Text = "[PlanogramComponent.FinancialCode]";

                    //reopen the original file
                    labelEditorVm.OpenFromFileCommand.Execute(expectedPath);

                    //check
                    Assert.AreEqual(testField, labelEditorVm.SelectedLabel.Text);

                    //apply
                    labelEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            WindowService.CloseWindowSetNullResponse();
            MainPageCommands.ShowFixtureLabelEditor.Execute();

            //now check that the Label was applied.
            var curDoc = App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Assert.AreEqual(testField, curDoc.FixtureLabel.Text);
        }

        [Test]
        public void CanCreateAndUseLabelFromRepository()
        {
            //Checks that it is possible to save a Label to the repository then reopen it

            const String testField = "[PlanogramComponent.Name]";
            base.SetMockRepositoryConnection();

            var planController = base.AddNewCreweSparklingWaterPlan();

            String LabelName = Guid.NewGuid().ToString();

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    FixtureLabelEditorViewModel labelEditorVm = (FixtureLabelEditorViewModel)p.WindowParameters[0];

                    //Create a new Label
                    labelEditorVm.SelectedLabel.Text = testField;

                    //save to file
                    WindowService.PromptForSaveAsNameSetReponse(true, LabelName);
                    labelEditorVm.SaveAsToRepositoryCommand.Execute();

                    //check repository item exists
                    LabelInfo saved = LabelInfoList.FetchByEntityId(App.ViewState.EntityId).First(h => h.Name == LabelName);

                    //change a property on the Label
                    labelEditorVm.SelectedLabel.Text = "[PlanogramComponent.FinancialCode]";

                    //reopen the original file
                    labelEditorVm.OpenFromRepositoryCommand.Execute(saved.Id);

                    //check
                    Assert.AreEqual(testField, labelEditorVm.SelectedLabel.Text);

                    //apply
                    WindowService.CloseWindowSetNullResponse();
                    labelEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            MainPageCommands.ShowFixtureLabelEditor.Execute();

            //now check that the Label was applied.
            var curDoc = App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Assert.AreEqual(testField, curDoc.FixtureLabel.Text);

        }

        [Test]
        public void CanOpenReadOnlyLabelFile()
        {
            //Checks that it is possible to open a readonly Label file

            //create a new Label file.
            Label h1 = Label.NewLabel(LabelType.Fixture);
            h1.Name = "h1";
            h1.Type = LabelType.Fixture;
            h1.Text = "[PlanogramComponent.Name]";

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            h1 = h1.SaveAsFile(expectedPath);
            h1.Dispose();

            //set the file to be readonly.
            System.IO.File.SetAttributes(expectedPath, FileAttributes.ReadOnly);

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();
            OpenAndApplyLabelFile(expectedPath);

            //now check that the Label was applied.
            var curDoc = App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Assert.AreEqual("[PlanogramComponent.Name]", curDoc.FixtureLabel.Text);
        }

        [Test]
        public void LabelIsAppliedToPlanVisual()
        {
            //Checks that Label colours get applied to a plan visual doc

            var planController = base.AddNewCreweSparklingWaterPlan();

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    FixtureLabelEditorViewModel labelEditorVm = (FixtureLabelEditorViewModel)p.WindowParameters[0];

                    //Create a new Label
                    labelEditorVm.SelectedLabel.Text = "[PlanogramComponent.Brand]";

                    //apply
                    labelEditorVm.ApplyToCurrentViewCommand.Execute();
                });
            WindowService.CloseWindowSetNullResponse();
            MainPageCommands.ShowFixtureLabelEditor.Execute();

            //now check that the Label was applied.
            PlanVisualDocument curDoc = (PlanVisualDocument)App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Plan3DData planData = curDoc.PlanogramModelData;

            Assert.IsNotNull(curDoc.FixtureLabel, "should have Label");
            Assert.IsNotNull(planData.Settings.FixtureLabel, "Should have Label ");
            Assert.IsNotNull(planData.Settings.FixtureLabelText, "Should have Label text");
        }

        [Test]
        public void OpeningLabelAddsItToMostRecentList()
        {
            //create a new Label file.
            Label h1 = Label.NewLabel(LabelType.Fixture);
            h1.Name = "h1";
            h1.Type = LabelType.Fixture;
            h1.Text = "[PlanogramComponent.Name]";

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            h1 = h1.SaveAsFile(expectedPath);
            h1.Dispose();

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();
            OpenAndApplyLabelFile(expectedPath);

            //check the mru
            var mrus = App.ViewState.Settings.Model.RecentLabels.Where(h => Object.Equals(h.LabelId, expectedPath));
            Assert.AreEqual(1, mrus.Count(), "Should have  item in the mru list.");

        }

        [Test]
        public void CanOpenFileMruItem()
        {
            //Checks that a file item can be opened from the mru

            //create a file and a repository item
            Label h1 = Label.NewLabel(LabelType.Fixture);
            h1.Name = "h1";
            h1.Type = LabelType.Fixture;
            h1.Text = "[PlanogramComponent.Name]";

            String expectedPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            expectedPath = Path.ChangeExtension(expectedPath, Label.FileExtension);
            h1 = h1.SaveAsFile(expectedPath);
            h1.Dispose();

            //add to the mru
            App.ViewState.Settings.Model.RecentLabels.AddLabelId(h1.Id, h1.Name, h1.Type);

            App.MainPageViewModel.RefreshAvailableLabels();

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();

            var info = App.MainPageViewModel.AvailableFixtureLabels.FirstOrDefault(h => Object.Equals(h.Id, expectedPath));
            App.MainPageViewModel.SetFixtureLabel(info);

            //now check that the Label was applied.
            PlanVisualDocument curDoc = (PlanVisualDocument)App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Plan3DData planData = curDoc.PlanogramModelData;

            Assert.IsNotNull(curDoc.FixtureLabel, "should have Label");
            Assert.IsNotNull(planData.Settings.FixtureLabel, "Should have Label ");
            Assert.IsNotNull(planData.Settings.FixtureLabelText, "Should have Label text");
        }

        [Test]
        public void CanOpenRepositoryMruItem()
        {
            //Checks that a repository item can be opened from the mru

            base.SetMockRepositoryConnection();


            //create a file and a repository item
            Label h1 = Label.NewLabel(LabelType.Fixture);
            h1.Name = "h1";
            h1.Type = LabelType.Fixture;
            h1.Text = "[PlanogramComponent.Name]";
            h1 = h1.SaveAs();
            h1.Dispose();

            //add to the mru
            App.ViewState.Settings.Model.RecentLabels.AddLabelId(h1.Id, h1.Name, h1.Type);

            App.MainPageViewModel.RefreshAvailableLabels();

            //now open and apply to a plan
            var planController = base.AddNewCreweSparklingWaterPlan();

            var info = App.MainPageViewModel.AvailableFixtureLabels.FirstOrDefault(h => Object.Equals(h.Id, h1.Id));
            App.MainPageViewModel.SetFixtureLabel(info);

            //now check that the Label was applied.
            PlanVisualDocument curDoc = (PlanVisualDocument)App.MainPageViewModel.ActivePlanController.SelectedPlanDocument;
            Plan3DData planData = curDoc.PlanogramModelData;

            Assert.IsNotNull(curDoc.FixtureLabel, "should have Label");
            Assert.IsNotNull(planData.Settings.FixtureLabel, "Should have Label ");
            Assert.IsNotNull(planData.Settings.FixtureLabelText, "Should have Label text");
        }

    }
}
