﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26860 : L.Luong
//  Created
// V8-27404 : L.Luong
//  Renamed LevelType to EntrytType and added Content
// V8-28061 : A.Kuszyk
//  Updated to reflect changes to view model.
#endregion

#region Version History: (CCM 8.0.3)
//V8-29590 : A.Probyn
//  Layout updates and expanded for new Score property
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.UI.PrimaryWindow
{
    [TestFixture]
    public sealed class EventLogViewModelTests
    {
        #region Fields
        private const PlanogramView InvalidPlanogramView = null;
        private PlanogramView _validPlanogramView;
        private PlanogramEventLog _error;
        private PlanogramEventLog _warning;
        private PlanogramEventLog _information;
        private PlanogramEventLog _debug;
        #endregion

        [SetUp]
        public void Setup()
        {
            var planogram = Planogram.NewPlanogram();
            _debug = PlanogramEventLog.NewPlanogramEventLog(PlanogramEventLogEntryType.Debug, PlanogramEventLogEventType.Addition, null, null, String.Empty, String.Empty,String.Empty, String.Empty,0,0);
            _information = PlanogramEventLog.NewPlanogramEventLog(PlanogramEventLogEntryType.Information, PlanogramEventLogEventType.Addition, null, null, String.Empty, String.Empty, String.Empty, String.Empty, 0,0);
            _warning = PlanogramEventLog.NewPlanogramEventLog(PlanogramEventLogEntryType.Warning, PlanogramEventLogEventType.Addition, null, null, String.Empty, String.Empty, String.Empty, String.Empty, 0,2);
            _error = PlanogramEventLog.NewPlanogramEventLog(PlanogramEventLogEntryType.Error, PlanogramEventLogEventType.Addition, null, null, String.Empty, String.Empty, String.Empty, String.Empty, 0,5);
            planogram.EventLogs.AddRange(new[] { _debug, _warning, _information, _error });
            _validPlanogramView = new PlanogramView(planogram);
        }

        [Test]
        public void IsDebugVisible_WhenTrueAndRestFalse_OnlyDebugInFilteredList()
        {
            var viewModel = new EventLogViewModel(_validPlanogramView);

            viewModel.IsErrorVisible = false;
            viewModel.IsWarningVisible = false;
            viewModel.IsInformationVisible = false;
            viewModel.IsDetailedVisible = true;

            var expected = new[] { _debug };
            var actual = viewModel.FilteredEventLogs.Select(vm => vm.Model);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void IsInformationVisible_WhenTrueAndRestFalse_OnlyInformationInFilteredList()
        {
            var viewModel = new EventLogViewModel(_validPlanogramView);

            viewModel.IsErrorVisible = false;
            viewModel.IsWarningVisible = false;
            viewModel.IsInformationVisible = true;
            viewModel.IsDetailedVisible = false;

            var expected = new[] { _information };
            var actual = viewModel.FilteredEventLogs.Select(vm => vm.Model);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void IsWarningVisible_WhenTrueAndRestFalse_OnlyWarningInFilteredList()
        {
            var viewModel = new EventLogViewModel(_validPlanogramView);

            viewModel.IsErrorVisible = false;
            viewModel.IsWarningVisible = true;
            viewModel.IsInformationVisible = false;
            viewModel.IsDetailedVisible = false;

            var expected = new[] { _warning };
            var actual = viewModel.FilteredEventLogs.Select(vm => vm.Model);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void IsErrorVisible_WhenTrueAndRestFalse_OnlyErrorInFilteredList()
        {
            var viewModel = new EventLogViewModel(_validPlanogramView);

            viewModel.IsErrorVisible = true;
            viewModel.IsWarningVisible = false;
            viewModel.IsInformationVisible = false;
            viewModel.IsDetailedVisible = false;

            var expected = new[] { _error };
            var actual = viewModel.FilteredEventLogs.Select(vm => vm.Model);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void EventLogs_CloseCommand_Execution()
        {
            var eventLogViewModel = new EventLogViewModel(_validPlanogramView);
            RelayCommand cmd = eventLogViewModel.CloseCommand;
            Assert.IsNotNull(cmd.FriendlyName);

            Assert.IsTrue(cmd.CanExecute());
            cmd.Execute();
        }

        [Test]
        public void Constructor_WhenNullPlanogram_ShouldThrow()
        {
            const string expectation =
                "When initializating EventLogViewModel with a null Planogram parameter an ArgumentNullException should be thrown.";

            TestDelegate operation = () => new EventLogViewModel(InvalidPlanogramView);

            Assert.Throws<ArgumentNullException>(operation, expectation);
        }
    }
}
