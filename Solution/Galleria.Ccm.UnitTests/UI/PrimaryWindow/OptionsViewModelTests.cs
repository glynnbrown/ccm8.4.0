﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24863 : L.Hodson
//  Created
// V8-28676 : A.Silva
//      Added tests for Product Image related commands.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.UnitTests.UI.PrimaryWindow
{
    [TestFixture]
    public sealed class OptionsViewModelTests : TestBase
    {
        #region Properties

        [Test]
        public void Property_AvailablePositionProperties()
        {
            OptionsViewModel optionsView = new OptionsViewModel();

            Assert.AreNotEqual(0, optionsView.AvailablePositionProperties.Count, "Should have something in there");

            Assert.AreEqual(optionsView.AvailablePositionProperties.Count,
                optionsView.AvailablePositionProperties.Select(a => a.FieldFriendlyName).Distinct().Count(),
                "Should not have duplicates");

            Assert.IsNotNull(optionsView.AvailablePositionProperties.FirstOrDefault(o => o.OwnerFriendlyName == PlanogramComponent.FriendlyName),
                "Should have component properties available");
        }

        [Test]
        public void Property_AvailableComponentProperties()
        {
            OptionsViewModel optionsView = new OptionsViewModel();

            Assert.AreNotEqual(0, optionsView.AvailableComponentProperties.Count, "Should have something in there");

            Assert.AreEqual(optionsView.AvailableComponentProperties.Count,
                optionsView.AvailableComponentProperties.Select(a => a.FieldFriendlyName).Distinct().Count(),
                "Should not have duplicates");

            //v8-27634  - check for notch number
            Assert.IsNotNull(optionsView.AvailableComponentProperties.FirstOrDefault(o=> o.PropertyName == "NotchNumber"), 
                "Notch number is missing.");
        }

        #endregion

        #region Commands

        [Test]
        public void Command_SetComponentHoverStatusBarText()
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            OptionsViewModel optionsView = new OptionsViewModel();

            EventHandler<WindowHelper.ShowWindowRequestArgs> handler =
                (s, e) =>
                {
                    var fieldSelector = (FieldSelectorViewModel)e.Args[0];
                    fieldSelector.Text = "Test";
                    fieldSelector.OKCommand.Execute();
                };
            WindowHelper.ShowWindowRequest += handler;

            optionsView.SetComponentHoverStatusBarTextCommand.Execute();

            WindowHelper.ShowWindowRequest -= handler;


            Assert.AreEqual("Test", optionsView.Settings.ComponentHoverStatusBarText);
        }

        [Test]
        public void Command_SetPositionHoverStatusBarText()
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            OptionsViewModel optionsView = new OptionsViewModel();

            EventHandler<WindowHelper.ShowWindowRequestArgs> handler =
                (s, e) =>
                {
                    var fieldSelector = (FieldSelectorViewModel)e.Args[0];
                    fieldSelector.Text = "Test";
                    fieldSelector.OKCommand.Execute();
                };
            WindowHelper.ShowWindowRequest += handler;

            optionsView.SetPositionHoverStatusBarTextCommand.Execute();

            WindowHelper.ShowWindowRequest -= handler;


            Assert.AreEqual("Test", optionsView.Settings.PositionHoverStatusBarText);
        }

        [Test]
        public void SetImageProductAttributeFilterCommand_WhenSourceIsFolder_ShouldBeEnabled()
        {
            OptionsViewModel model = new OptionsViewModel();
            
            model.Settings.ProductImageSource = RealImageProviderType.Folder;
            
            Assert.IsTrue(model.SetImageProductAttributeFilterCommand.CanExecute());
        }

        [Test]
        public void SetImageProductAttributeFilterCommand_WhenSourceIsNotFolder_ShouldNotBeEnabled()
        {
            OptionsViewModel model = new OptionsViewModel();

            model.Settings.ProductImageSource = RealImageProviderType.Repository;

            Assert.IsFalse(model.SetImageProductAttributeFilterCommand.CanExecute());
        }

        [Test]
        public void SetImageProductAttributeFilterCommand_WhenValidField_ShouldAssignProductAttribute()
        {
            OptionsViewModel model = new OptionsViewModel();
            model.Settings.ProductImageSource = RealImageProviderType.Folder;
            ObjectFieldInfo objectFieldInfo = Product.EnumerateDisplayableFieldInfos().First(o => !o.FieldPlaceholder.ToUpper().Contains("GTIN"));
            String expected = objectFieldInfo.FieldPlaceholder;
            MockFieldSelectorWindow_SetSelectedField(objectFieldInfo, reset: true);

            model.SetImageProductAttributeFilterCommand.Execute();

            String actual = model.Settings.ProductImageAttribute;
            Assert.AreEqual(expected, actual, "The actual product attribute was not the expected.");
        }

        #endregion

        #region Settings

        [Test]
        public void LengthUnitOfMeasure_NewPlanogram()
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            settings.Model.LengthUnitOfMeasure = PlanogramLengthUnitOfMeasureType.Centimeters;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            Assert.AreEqual(settings.Model.LengthUnitOfMeasure, (PlanogramLengthUnitOfMeasureType)planController.SourcePlanogram.LengthUnitsOfMeasure);


            //try another
            settings.Model.LengthUnitOfMeasure = PlanogramLengthUnitOfMeasureType.Inches;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            planController = mainPageView.CreateNewPlanogram();
            Assert.AreEqual(settings.Model.LengthUnitOfMeasure, (PlanogramLengthUnitOfMeasureType)planController.SourcePlanogram.LengthUnitsOfMeasure);
        }

        [Test]
        public void ProductSize_NewProduct()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();

            var settings = App.ViewState.Settings;
            settings.Model.ProductHeight = 10;
            settings.Model.ProductWidth = 11;
            settings.Model.ProductDepth = 12;
            
            MainPageCommands.AddNewProduct.Execute();

            PlanogramProductView prod = planController.SourcePlanogram.Products[0];

            Assert.AreEqual(10, prod.Height);
            Assert.AreEqual(11, prod.Width);
            Assert.AreEqual(12, prod.Depth);
        }

        [Test]
        public void FixtureSize_NewFixture()
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            settings.Model.FixtureHeight = 201;
            settings.Model.FixtureWidth = 101;
            settings.Model.FixtureDepth = 5;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();

            PlanogramPropertiesViewModel planProperties = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            var fixture = planController.SourcePlanogram.Fixtures.First();

            Assert.AreEqual(201, fixture.Height);
            Assert.AreEqual(101, fixture.Width);
            Assert.AreEqual(5, fixture.Depth);
        }

        [Test]
        public void HasBackboard_NewFixture()
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            settings.Model.FixtureHasBackboard = true;
            settings.Model.BackboardDepth = 10;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();

            PlanogramPropertiesViewModel planProperties = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            var fixture = planController.SourcePlanogram.Fixtures.First();
            var backboardComponent = fixture.Components.FirstOrDefault(c => c.ComponentType == Galleria.Framework.Planograms.Model.PlanogramComponentType.Backboard);

            Assert.IsNotNull(backboardComponent);
            Assert.AreEqual(10, backboardComponent.Depth);

            settings.Model.FixtureHasBackboard = false;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            planController = mainPageView.CreateNewPlanogram();

            planProperties = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            fixture = planController.SourcePlanogram.Fixtures.First();
            Assert.IsNull(fixture.Components.FirstOrDefault(c => c.ComponentType == Galleria.Framework.Planograms.Model.PlanogramComponentType.Backboard));
        }

        [Test]
        public void HasBase_NewFixture()
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            settings.Model.FixtureHasBase = true;
            settings.Model.BaseHeight = 11;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();

            PlanogramPropertiesViewModel planProperties = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            var fixture = planController.SourcePlanogram.Fixtures.First();
            var baseComponent = fixture.Components.FirstOrDefault(c => c.ComponentType == Galleria.Framework.Planograms.Model.PlanogramComponentType.Base);

            Assert.IsNotNull(baseComponent);
            Assert.AreEqual(11, baseComponent.Height);

            settings.Model.FixtureHasBase = false;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            planController = mainPageView.CreateNewPlanogram();

            planProperties = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            fixture = planController.SourcePlanogram.Fixtures.First();
            Assert.IsNull(fixture.Components.FirstOrDefault(c => c.ComponentType == Galleria.Framework.Planograms.Model.PlanogramComponentType.Base));
        }


        public IEnumerable<String> DeafultSizesSettings
        {
            get
            {
                yield return "Shelf";
                yield return "Pegboard";
                yield return "Rod";
                yield return "Bar";
                yield return "ClipStrip";
                yield return "Pallet";
                yield return "Chest";
            }
        }
        [Test, TestCaseSource("DeafultSizesSettings")]
        public void Defaults_NewComponent(String typeName)
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            var settingHeightProperty = typeof(UserEditorSettings).GetProperty(typeName + "Height");
            var settingWidthProperty = typeof(UserEditorSettings).GetProperty(typeName + "Width");
            var settingDepthProperty = typeof(UserEditorSettings).GetProperty(typeName + "Depth");
            var settingFillProperty = typeof(UserEditorSettings).GetProperty(typeName + "FillColour");

            //set the defaults
            settingHeightProperty.SetValue(settings.Model, 10, null);
            settingWidthProperty.SetValue(settings.Model, 11, null);
            settingDepthProperty.SetValue(settings.Model, 12, null);
            settingFillProperty.SetValue(settings.Model, 5, null);

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            var fixture = planController.SourcePlanogram.AddFixture();

            String componentTypeName = typeName;
            switch (typeName)
            {
                case "Pegboard": componentTypeName = "Peg"; break;
            }

            var type = (Framework.Planograms.Model.PlanogramComponentType)Enum.Parse(typeof(Framework.Planograms.Model.PlanogramComponentType), componentTypeName);

            var component = fixture.AddComponent(type);
            Assert.AreEqual(10, component.Height);
            Assert.AreEqual(11, component.Width);
            Assert.AreEqual(12, component.Depth);
            Assert.AreEqual(5, component.FillColour);
            
        }
    

        [Test]
        public void FixtureLabel_Plan()
        {
            throw new InconclusiveException("TODO");
        }

        [Test]
        public void ProductLabel_Plan()
        {
            throw new InconclusiveException("TODO");
        }


        public IEnumerable<String> BooleanDisplaySettings
        {
            get
            {
                yield return "PositionUnits";
                yield return "ProductImages";
                yield return "ProductShapes";
                yield return "ProductFillPatterns";
                yield return "FixtureImages";
                yield return "FixtureFillPatterns";
                yield return "ChestWalls";
                yield return "RotateTopDownComponents";
                yield return "ShelfRisers";
                yield return "Notches";
                yield return "PegHoles";
                yield return "Pegs";
                yield return "DividerLines";
                yield return "Dividers";
            }
        }
        [Test, TestCaseSource("BooleanDisplaySettings")]
        public void BooleanDisplaySetting(String propertyName)
        {
            var settings = App.ViewState.Settings;

            var test = typeof(UserEditorSettings).GetProperties();

            var settingProperty = typeof(UserEditorSettings).GetProperty(propertyName);
            var planDocProperty = typeof(IPlanDocument).GetProperty(String.Format("Show{0}", propertyName));
            if (propertyName == "RotateTopDownComponents")
            {
                planDocProperty = typeof(IPlanDocument).GetProperty(propertyName);
            }
            
            //check true
            settingProperty.SetValue(settings.Model, true, null);

            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();

            var planDoc = planController.PlanDocuments.First();
            Assert.IsTrue((Boolean)planDocProperty.GetValue(planDoc, null));

            planController.CloseDocument(planDoc);

            //check false
            settingProperty.SetValue(settings.Model, false, null);

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            planController = mainPageView.CreateNewPlanogram();
            planDoc = planController.PlanDocuments.First();

            Assert.IsFalse((Boolean)planDocProperty.GetValue(planDoc, null));
        }

        [Test]
        public void SettingsSaveOnOk()
        {
            var mainPageView = App.MainPageViewModel;
            var settings = App.ViewState.Settings;

            OptionsViewModel optionsView = new OptionsViewModel();

            optionsView.Settings.ProductHeight = 999;

            Assert.IsTrue(settings.Model.IsDirty);

            optionsView.ApplyCommand.Execute();

            Assert.IsFalse(settings.Model.IsDirty);
            Assert.AreEqual(999, settings.Model.ProductHeight);
        }

        #endregion

        #region Private Helper Methods

        private void MockFieldSelectorWindow_SetSelectedField(ObjectFieldInfo objectFieldInfo, Boolean reset = false)
        {
            if (reset)
            {
                WindowService.ClearResponseQueue();
            }

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, args =>
            {
                FieldSelectorViewModel viewModel = args.WindowParameters.First() as FieldSelectorViewModel;
                if (viewModel == null) return;

                // User selects the field.
                viewModel.SelectedField = objectFieldInfo;

                // User accepts.
                viewModel.OKCommand.Execute();
            });
        }

        #endregion
    }
}