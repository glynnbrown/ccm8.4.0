﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261 : L.Hodson
//  Created
// V8-27496 : L.Luong
//  Added SaveSettings test
// V8-28444 : M.Shelley
//  Fixed the PlanogramFile_SaveAsFile_SaveSettings unit test
#endregion
#region Version History: (CCM 801)
// V8-25148 : I.George
//  Added test to check that no plan gets created if the creaion process is cancelled.
#endregion
#region Version History: (CCM 830)
// V8-32389  : J.Pickup
//  Added some basic tests for the toggling of labels and highlights.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Linq;
using System.IO;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Security;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using System.Collections.Generic;
using Galleria.Framework.Controls.Wpf;
using File = System.IO.File;
using Galleria.Framework.ViewModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Collections;

namespace Galleria.Ccm.UnitTests.UI.PrimaryWindow
{
    [TestFixture]
    public sealed class MainPageViewModelTests : TestBase
    {

        #region Toggle highlight and label tests

        [Test]
        public void ToggleFixtureLabel_SetsAndUnsets()
        {
            // Create a planogram.
            var plan = "TestPlanogram".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var planProduct1 = plan.AddProduct();
            var position1 = shelf.AddPosition(bay, planProduct1);
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());

            //Add plan controller
            PlanogramView planView = new PlanogramView(plan, false);
            PlanControllerViewModel planController = new PlanControllerViewModel(planView);
            App.MainPageViewModel.ActivePlanController = planController;

            IPlanDocument document = PlanDocumentHelper.CreateDocument(DocumentType.Design, planController);
            planController.SelectedPlanDocument = document;

            const String cName = "I love labelling items.";

            Label newLabel = Label.NewLabel(LabelType.Fixture);
            newLabel.Name = cName;
            LabelItem labelItem = new LabelItem(newLabel);

            planController.SelectedPlanDocument.FixtureLabel = labelItem;

            App.MainPageViewModel.ToggleFixtureLabelSet();

            Assert.IsTrue(planController.SelectedPlanDocument.FixtureLabel == null, "Should have toggled off");
        }

        [Test]
        public void ToggleProductLabel_SetsAndUnsets()
        {
            // Create a planogram.
            var plan = "TestPlanogram".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var planProduct1 = plan.AddProduct();
            var position1 = shelf.AddPosition(bay, planProduct1);
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());

            //Add plan controller
            PlanogramView planView = new PlanogramView(plan, false);
            PlanControllerViewModel planController = new PlanControllerViewModel(planView);
            App.MainPageViewModel.ActivePlanController = planController;

            IPlanDocument document = PlanDocumentHelper.CreateDocument(DocumentType.Design, planController);
            planController.SelectedPlanDocument = document;

            const String cName = "I love labelling items.";

            Label newLabel = Label.NewLabel(LabelType.Product);
            newLabel.Name = cName;
            LabelItem labelItem = new LabelItem(newLabel);

            planController.SelectedPlanDocument.ProductLabel = labelItem;

            App.MainPageViewModel.ToggleProductLabelSet();

            Assert.IsTrue(planController.SelectedPlanDocument.ProductLabel == null, "Should have toggled off");
        }

        [Test]
        public void ToggleHighlightLabel_SetsAndUnsets()
        {
            // Create a planogram.
            var plan = "TestPlanogram".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var planProduct1 = plan.AddProduct();
            var position1 = shelf.AddPosition(bay, planProduct1);
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());

            //Add plan controller
            PlanogramView planView = new PlanogramView(plan, false);
            PlanControllerViewModel planController = new PlanControllerViewModel(planView);
            App.MainPageViewModel.ActivePlanController = planController;

            IPlanDocument document = PlanDocumentHelper.CreateDocument(DocumentType.Design, planController);
            planController.SelectedPlanDocument = document;

            const String cName = "I love highlighting items.";

            Highlight newHighlight = Highlight.NewHighlight();
            newHighlight.Name = cName;
            HighlightItem HighlightItem = new HighlightItem(newHighlight);

            planController.SelectedPlanDocument.Highlight = HighlightItem;

            App.MainPageViewModel.ToggleHighlightSet();

            Assert.IsTrue(planController.SelectedPlanDocument.Highlight == null, "Should have toggled off");
        }

        #endregion

        #region Test Fixture Helpers
        private PlanControllerViewModel CreateNewPlanogram()
        {
            base.WindowService.ShowDialogSetReponse((p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var controller = App.MainPageViewModel.CreateNewPlanogram();
            controller.SourcePlanogram.ParentPackageView.Model.Name = Guid.NewGuid().ToString();
            return controller;
        }
        #endregion

        #region Planogram File Tests

        [Test]
        public void PlanogramFile_New()
        {
            var mainPageView = App.MainPageViewModel;

            Assert.AreEqual(null, mainPageView.ActivePlanController, "Should start with no plans loaded.");

            //create the new planogram 
            var newPlan = CreateNewPlanogram();

            Assert.AreEqual(newPlan, mainPageView.ActivePlanController);
            Assert.AreNotEqual(0, newPlan.PlanDocuments.Count, "Should have at least one view document loaded");

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_OpenFromFile()
        {
            var mainPageView = App.MainPageViewModel;

            //create and save a plan file
            Package package = TestDataHelper.CreatePlanCreweSparklingWater();
            package.Name = Guid.NewGuid().ToString();

            String planPath = Path.Combine(this.TestDir, package.Name);
            planPath = Path.ChangeExtension(planPath, ".pog");

            Planogram plan = package.Planograms.First();

            package.SaveAs(DomainPrincipal.CurrentUserId, planPath);
            package.Dispose();

            //reopen it
            mainPageView.OpenPlanogramsFromFileSystem(planPath);

            Assert.AreEqual(1, mainPageView.PlanControllers.Count);

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_SaveAsFile()
        {
            var mainPageView = App.MainPageViewModel;

            //create a new plan
            var planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();

            String planPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            planPath = Path.ChangeExtension(planPath, ".pog");

            //save
            mainPageView.SaveAsFile(planController, planPath);

            Assert.IsTrue(System.IO.File.Exists(planPath));
            Assert.IsFalse(planController.SourcePlanogram.Model.IsNew, "Plan should no longer be new");

            //close the plan
            mainPageView.ClosePlanController(planController);
            planController = null;


            //reopen
            mainPageView.OpenPlanogramsFromFileSystem(planPath);

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_Save()
        {
            var mainPageView = App.MainPageViewModel;

            //create and save a plan file


            Package package = TestDataHelper.CreatePlanCreweSparklingWater();
            package.Name = Guid.NewGuid().ToString();

            String planPath = Path.Combine(this.TestDir, package.Name);
            planPath = Path.ChangeExtension(planPath, ".pog");

            Planogram plan = package.Planograms.First();

            package.SaveAs(DomainPrincipal.CurrentUserId, planPath);
            package.Dispose();

            //reopen it
            mainPageView.OpenPlanogramsFromFileSystem(planPath);

            //make a change
            var planController = mainPageView.ActivePlanController;
            planController.SourcePlanogram.Fixtures[0].Components[0].Weight = 20;

            //resave
            mainPageView.Save(planController);

            Assert.IsTrue(planController.SourcePlanogram.Model.IsInitialized, "Should be initialized to stop continue with item change warning showing.");

            Assert.AreEqual(20, planController.SourcePlanogram.Fixtures[0].Components[0].Weight);

            //close and reopen
            mainPageView.ClosePlanController(planController);
            planController = null;
            mainPageView.OpenPlanogramsFromFileSystem(planPath);

            planController = mainPageView.ActivePlanController;
            Assert.AreEqual(20, planController.SourcePlanogram.Fixtures[0].Components[0].Weight);

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_SaveAsFile_SaveSettings()
        {
            var mainPageView = App.MainPageViewModel;

            //create a new plan
            var planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();

            Object imageId = 8;

            // Assortment
            planController.SourcePlanogram.Model.Assortment.AddAssortment(PlanogramAssortment.NewPlanogramAssortment());
            planController.SourcePlanogram.Model.Assortment.Name = "AssortmentName";
            planController.SourcePlanogram.Model.Assortment.Regions.Add(PlanogramAssortmentRegion.NewPlanogramAssortmentRegion());
            planController.SourcePlanogram.Model.Assortment.Regions[0].Name = "RegionName";

            // Blocking
            planController.SourcePlanogram.Model.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final));
            planController.SourcePlanogram.Model.Blocking[0].Groups.Add(PlanogramBlockingGroup.NewPlanogramBlockingGroup());
            planController.SourcePlanogram.Model.Blocking[0].Groups[0].Name = "BlockGroupName";

            // Consumer Decision Tree
            planController.SourcePlanogram.Model.ConsumerDecisionTree.RootNode.AddNewChildNode();

            // Event Log
            planController.SourcePlanogram.Model.EventLogs.Add(PlanogramEventLog.NewPlanogramEventLog());
            planController.SourcePlanogram.Model.EventLogs[0].Description = "EventDescription";
            planController.SourcePlanogram.Model.EventLogs[0].EntryType = PlanogramEventLogEntryType.Error;
            planController.SourcePlanogram.Model.EventLogs[0].EventType = PlanogramEventLogEventType.General;

            // Images
            planController.SourcePlanogram.Model.Images.Add(PlanogramImage.NewPlanogramImage());
            planController.SourcePlanogram.Model.Images[0].Id = imageId;

            planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateBack = imageId;
            planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateBottom = imageId;
            planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateFront = imageId;
            planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateLeft = imageId;

            // Validation Template
            planController.SourcePlanogram.Model.ValidationTemplate.Groups.Add(PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup());
            planController.SourcePlanogram.Model.ValidationTemplate.Groups[0].Name = "ValidationGroupName";

            String planPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            planPath = Path.ChangeExtension(planPath, ".pog");

            // check they exsist 
            Assert.IsTrue(planController.SourcePlanogram.Model.Assortment.Regions.Count == 1);
            Assert.IsTrue(planController.SourcePlanogram.Model.Blocking[0].Groups.Count == 1);
            Assert.IsTrue(planController.SourcePlanogram.Model.ConsumerDecisionTree.RootNode.ChildList.Count == 1);
            Assert.IsTrue(planController.SourcePlanogram.Model.EventLogs.Any());
            Assert.IsTrue(planController.SourcePlanogram.Model.Images.Count == 1);
            Assert.IsTrue(planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateBack != null);
            Assert.IsTrue(planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateBottom != null);
            Assert.IsTrue(planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateFront != null);
            Assert.IsTrue(planController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateLeft != null);
            Assert.IsTrue(planController.SourcePlanogram.Model.Performance.Metrics.Count == 3);
            Assert.IsTrue(planController.SourcePlanogram.Model.Performance.PerformanceData.Count == 13);
            Assert.IsTrue(planController.SourcePlanogram.Model.ValidationTemplate.Groups.Count == 1);

            // save
            mainPageView.SaveAsFile(planController, planPath);

            Assert.IsTrue(System.IO.File.Exists(planPath));

            // close the plan
            mainPageView.ClosePlanController(planController);
            planController = null;

            // reopen it
            mainPageView.OpenPlanogramsFromFileSystem(planPath);

            // set all settings to false
            mainPageView.ActivePlanController.SourcePlanogram.SaveSettings.SaveAssortment = false;
            mainPageView.ActivePlanController.SourcePlanogram.SaveSettings.SaveBlocking = false;
            mainPageView.ActivePlanController.SourcePlanogram.SaveSettings.SaveCtd = false;
            mainPageView.ActivePlanController.SourcePlanogram.SaveSettings.SaveEventLogs = false;
            mainPageView.ActivePlanController.SourcePlanogram.SaveSettings.SaveImages = false;
            mainPageView.ActivePlanController.SourcePlanogram.SaveSettings.SavePerformances = false;
            mainPageView.ActivePlanController.SourcePlanogram.SaveSettings.SaveValidationTemplate = false;

            String newPlanPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            newPlanPath = Path.ChangeExtension(newPlanPath, ".pog");

            // save
            mainPageView.SaveAsFile(mainPageView.ActivePlanController, newPlanPath);

            Assert.IsTrue(System.IO.File.Exists(newPlanPath));

            // close the plan
            mainPageView.ClosePlanController(mainPageView.ActivePlanController);
            planController = null;

            //reopen
            mainPageView.OpenPlanogramsFromFileSystem(newPlanPath);

            // check that the planogram has been cleared
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Assortment.Regions.Count == 0);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Blocking.Count == 0);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.ConsumerDecisionTree.RootNode.ChildList.Count == 0);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.EventLogs.Count == 0);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Images.Count == 0);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateBack == null);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateBottom == null);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateFront == null);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Products[0].PlanogramImageIdAlternateLeft == null);
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Performance.Metrics.Count == 0);
           // Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.Performance.PerformanceData.Count == 0); // values no longer get removed just nulled off.
            Assert.IsTrue(mainPageView.ActivePlanController.SourcePlanogram.Model.ValidationTemplate.Groups.Count == 0);

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_OpenDeletedPogFile()
        {
            //V8-26637.

            var mainPageView = App.MainPageViewModel;

            //create a new plan
            var planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();

            String planPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            planPath = Path.ChangeExtension(planPath, ".pog");

            //save
            mainPageView.SaveAsFile(planController, planPath);

            Assert.IsTrue(System.IO.File.Exists(planPath));

            //close the plan
            mainPageView.ClosePlanController(planController);
            planController = null;

            //delete the file
            File.Delete(planPath);
            Assert.IsFalse(System.IO.File.Exists(planPath));

            //try to reopen
            WindowService.ShowErrorMessageSetResponse();

            mainPageView.OpenPlanogramsFromFileSystem(planPath);

            WindowService.AssertResponsesUsed();

            //check that the file was not created
            Assert.IsFalse(System.IO.File.Exists(planPath));

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_SaveAsToRepository_OverwriteSamePlan()
        {
            //V8-28838 - Check that save as to the same plan just does it. 

            //Connect to repository
            SetMockRepositoryConnection();
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(entityId);

            //create a new plan and save it to the repository.
            var mainPageView = App.MainPageViewModel;
            var planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);

            Object planId = planController.SourcePlanogram.ParentPackageView.Model.Id;
            PlanogramInfo planInfo = PlanogramInfoList.FetchByIds(new List<Int32> { (Int32)planId }).First();

            //close and reopen it
            mainPageView.ClosePlanController(planController);
            mainPageView.OpenPlanogramsFromRepository(new List<PlanogramInfo> { planInfo });

            //make a change
            Assert.AreEqual(1, mainPageView.PlanControllers.Count);
            planController = mainPageView.PlanControllers.First();

            planController.SourcePlanogram.EnumerateAllPositions().First().FacingsHigh += 1;
            Int32 newVal = planController.SourcePlanogram.EnumerateAllPositions().First().FacingsHigh;

            //do save as - it should just save, no messages shown.
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);

            mainPageView.ClosePlanController(planController);
            mainPageView.OpenPlanogramsFromRepository(new List<PlanogramInfo> { planInfo });
            planController = mainPageView.PlanControllers.First();
            Assert.AreEqual(newVal, planController.SourcePlanogram.EnumerateAllPositions().First().FacingsHigh);

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_SaveAsToRepository_OverwriteDifferentNotLockedPlan()
        {
            //V8-28838 - Check that save as to the diffrent plan allows overwrite 

            //Connect to repository
            SetMockRepositoryConnection();
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(entityId);

            //Add two plans to the repository.
            Int32 plan1Id;
            String plan1Name = "Plan1";
            Int32 plan2Id;
            String plan2Name = "Plan2";

            var mainPageView = App.MainPageViewModel;
            var planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();
            planController.SourcePlanogram.Name = plan1Name;
            planController.SourcePlanogram.ParentPackageView.Model.Name = plan1Name;
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);
            plan1Id = (Int32)planController.SourcePlanogram.ParentPackageView.Model.Id;
            mainPageView.ClosePlanController(planController);

            planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();
            planController.SourcePlanogram.Name = plan2Name;
            planController.SourcePlanogram.ParentPackageView.Model.Name = plan2Name;
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);
            plan2Id = (Int32)planController.SourcePlanogram.ParentPackageView.Model.Id;
            mainPageView.ClosePlanController(planController);

            //reopen plan 1 and change its name.
            mainPageView.OpenPlanogramsFromRepository(PlanogramInfoList.FetchByIds(new List<Int32> { (Int32)plan1Id }));
            planController = mainPageView.PlanControllers.First();
            planController.SourcePlanogram.Name = plan2Name;
            planController.SourcePlanogram.ParentPackageView.Model.Name = plan2Name;

            //try to save over plan 2 - overwrite warning should display, cancel it.
            base.WindowService.AddResponseResult(NUnitWindowService.ShowMessageMethod, ModalMessageResult.Button2); // cancel
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);
            base.WindowService.AssertResponsesUsed();
            Assert.AreEqual(plan1Id, planController.SourcePlanogram.ParentPackageView.Model.Id, "controller id should not have changed");

            //try to save again, this time ok it.
            base.WindowService.AddResponseResult(NUnitWindowService.ShowMessageMethod, ModalMessageResult.Button1); // ok
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);
            base.WindowService.AssertResponsesUsed();
            Assert.AreNotEqual(plan1Id, planController.SourcePlanogram.ParentPackageView.Model.Id, "controller id have changed");

            Object plan3Id = planController.SourcePlanogram.ParentPackageView.Model.Id;
            mainPageView.ClosePlanController(planController);

            //check that plan 2 was overwritten by plan 3.
            var groupInfos = PlanogramInfoList.FetchByPlanogramGroupId(hierarchy.RootGroup.Id).Where(p => p.DateDeleted == null);
            Assert.AreEqual(2, groupInfos.Count(), "Should still only have 2 plans");
            Assert.IsNotNull(groupInfos.FirstOrDefault(p => Object.Equals(p.Id, plan1Id)), "plan1 should still exist");
            Assert.IsNull(groupInfos.FirstOrDefault(p => Object.Equals(p.Id, plan2Id)), "plan2 should not exist");
            Assert.IsNotNull(groupInfos.FirstOrDefault(p => Object.Equals(p.Id, plan3Id)), "plan3 should exist");

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void PlanogramFile_SaveAsToRepository_OverwriteDifferentLockedPlan()
        {
            //V8-28838 - Check that save as to the diffrent locked plan just 
            // tells user to go away.

            //Connect to repository
            SetMockRepositoryConnection();
            Int32 entityId = EntityInfoList.FetchAllEntityInfos().First().Id;
            PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(entityId);

            //Add two plans to the repository.
            Int32 plan1Id;
            String plan1Name = "Plan1";
            Int32 plan2Id;
            String plan2Name = "Plan2";

            var mainPageView = App.MainPageViewModel;
            var planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();
            planController.SourcePlanogram.Name = plan1Name;
            planController.SourcePlanogram.ParentPackageView.Model.Name = plan1Name;
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);
            plan1Id = (Int32)planController.SourcePlanogram.ParentPackageView.Model.Id;
            mainPageView.ClosePlanController(planController);

            planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();
            planController.SourcePlanogram.Name = plan2Name;
            planController.SourcePlanogram.ParentPackageView.Model.Name = plan2Name;
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);
            plan2Id = (Int32)planController.SourcePlanogram.ParentPackageView.Model.Id;
            mainPageView.ClosePlanController(planController);


            //open both plans
            mainPageView.OpenPlanogramsFromRepository(PlanogramInfoList.FetchByIds(new List<Int32> { plan1Id, plan2Id }));

            //change plan 1 name to match plan 2
            planController = mainPageView.PlanControllers.First();
            planController.SourcePlanogram.Name = plan2Name;
            planController.SourcePlanogram.ParentPackageView.Model.Name = plan2Name;

            //try to overwrite plan 2 - dialog with only an ok button should show.
            base.WindowService.AddResponse(NUnitWindowService.ShowMessageMethod,
                (a) =>
                {
                    Assert.AreEqual(1, (Int32)a.WindowParameters[3]);
                });
            mainPageView.SaveAsRepository(planController, hierarchy.RootGroup);
            base.WindowService.AssertResponsesUsed();

            mainPageView.CloseAllPlans();
        }

        #endregion

        #region Document Tests

        [Test]
        public void Documents_LoadAllWithNoRepositoryConnection()
        {
            var mainPageView = App.MainPageViewModel;
            App.ViewState.ClearRepositoryConnection();

            var planController = CreateNewPlanogram();


            //add a new document of each available type
            foreach (Ccm.Model.DocumentType docType in Enum.GetValues(typeof(Ccm.Model.DocumentType)))
            {
                if (docType == Ccm.Model.DocumentType.None) continue;

                planController.AddNewDocument(docType, false);
            }

            //now close the plan
            mainPageView.ClosePlanController(planController);

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void Documents_LoadAllWithRepositoryConnection()
        {
            SetMockRepositoryConnection();
            var mainPageView = App.MainPageViewModel;

            var planController = CreateNewPlanogram();

            //add a new document of each available type
            foreach (Ccm.Model.DocumentType docType in Enum.GetValues(typeof(Ccm.Model.DocumentType)))
            {
                if (docType == Ccm.Model.DocumentType.None) continue;

                planController.AddNewDocument(docType, false);
            }

            //now close the plan
            mainPageView.ClosePlanController(planController);

            mainPageView.CloseAllPlans();
        }

        [Test]
        public void Documents_SaveWithAllDocumentsOpen()
        {
            SetMockRepositoryConnection();
            var mainPageView = App.MainPageViewModel;

            var planController = CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.EndUpdate();
            planController.SourcePlanogram.Name = Guid.NewGuid().ToString();


            //add a new document of each available type
            foreach (Ccm.Model.DocumentType docType in Enum.GetValues(typeof(Ccm.Model.DocumentType)))
            {
                if (docType == Ccm.Model.DocumentType.None) continue;

                IPlanDocument doc = planController.AddNewDocument(docType, false);

                //populate the docs as required.
                switch (docType)
                {
                    case Ccm.Model.DocumentType.Assortment:
                        {
                            //add an assortment product
                            AssortmentPlanDocument assortDoc = doc as AssortmentPlanDocument;
                            assortDoc.AssortmentModel.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct());
                            assortDoc.AssortmentModel.Products[0].Gtin = "11";
                        }
                        break;

                    case Ccm.Model.DocumentType.Blocking:
                        {
                            //add some blocks
                            BlockingPlanDocument blockingDoc = doc as BlockingPlanDocument;
                            blockingDoc.CurrentBlocking.Dividers.Add(0.5F, 0, PlanogramBlockingDividerType.Vertical);
                        }
                        break;
                }
            }

            //set a selected item
            planController.SelectedPlanItems.Add(planController.SourcePlanogram.EnumerateAllComponents().First());


            //save
            Assert.IsTrue(planController.SourcePlanogram.Model.IsValid, "Planogram should be valid");
            mainPageView.SaveAsRepository(planController);

            mainPageView.CloseAllPlans();
        }

        #endregion

        #region NewPlanogramTest

        [Test]
        public void Ok_NewPlanogram()
        {
            // start up the viewModel
            var viewModel = App.MainPageViewModel;

            // check that no plan has been loaded
            Assert.AreEqual(null, viewModel.ActivePlanController, "No plan should have loaded");

            // Create an event to click the ok button when window pops up
            base.WindowService.ShowDialogSetReponse(
                 (p) =>
                 {
                     var planProperties = ((PlanogramPropertiesViewModel)p.WindowParameters[0]);
                     planProperties.OKCommand.Execute();
                 });

            var planController = viewModel.CreateNewPlanogram();

            // check that a new planogram was created
            Assert.AreNotEqual(null, viewModel.ActivePlanController, "A new Plan should have opened");
            Assert.AreEqual(1, planController.PlanDocuments.Count, "A plan document should have been loaded");

            viewModel.CloseAllPlans();

        }

        [Test]
        public void Cancel_NewPlanogram()
        {
            var viewModel = App.MainPageViewModel;

            // check that no plan has been loaded in
            Assert.AreEqual(null, viewModel.ActivePlanController, "No Plan should have been loaded");

            // Execute the command and execute cancel on the window.
            base.WindowService.ShowDialogSetReponse((p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).CancelCommand.Execute(); });
            MainPageCommands.NewPlanogram.Execute();

            // checks that no plan got created since the window was cancelled
            Assert.AreEqual(null, viewModel.ActivePlanController, "No Plan should have been loaded");

            viewModel.CloseAllPlans();
        }

        [Test]
        public void Cancel_NewPlanogram_AfterMakingChanges()
        {
            var viewModel = App.MainPageViewModel;

            // check that no plan gets loaded
            Assert.AreEqual(null, viewModel.ActivePlanController, "No Plan should have been loaded");

            // opens up a new planogram and the event gets passed through to the window
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    var planProperties = ((PlanogramPropertiesViewModel)p.WindowParameters[0]);
                    planProperties.AddFixtureAfterCommand.Execute();
                    planProperties.CancelCommand.Execute();
                });

            MainPageCommands.NewPlanogram.Execute();
            base.WindowService.AssertResponsesUsed();

            Assert.AreEqual(null, viewModel.ActivePlanController, "No Plan should have been loaded");

            viewModel.CloseAllPlans();
        }

        [Test]
        public void Cancel_AlreadyCreatedPlanogramAfterMakingChanges()
        {
            // start up the viewModel
            var viewModel = App.MainPageViewModel;

            // check that no plan has been loaded
            Assert.AreEqual(null, viewModel.ActivePlanController, "No plan should have loaded");

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    var planProperties = ((PlanogramPropertiesViewModel)p.WindowParameters[0]);
                    planProperties.AddFixtureAfterCommand.Execute();
                    planProperties.OKCommand.Execute();
                });

            var planController = viewModel.CreateNewPlanogram();


            Assert.AreNotEqual(null, viewModel.ActivePlanController, "A new planogram should have been open");
            Assert.AreEqual(1, planController.PlanDocuments.Count, "plan document should have been loaded");


            String planogramBayName = "Plan 1";

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) =>
                {
                    var planProperties = ((PlanogramPropertiesViewModel)p.WindowParameters[0]);
                    planController.SourcePlanogram.Name = planogramBayName;
                    planProperties.CancelCommand.Execute();
                });
            MainPageCommands.ShowPlanogramProperties.Execute();

            // check that the name didn't change since the action was cancelled
            Assert.AreNotEqual(planController.SourcePlanogram.Name, planogramBayName);


            viewModel.CloseAllPlans();
        }

        [Test]
        public void Cancel_AfterOpeningAnExistingPlanFromFile()
        {
            var viewModel = App.MainPageViewModel;

            // create a new package
            Package package = TestDataHelper.CreatePlanCreweSparklingWater();
            package.Name = "Test";

            // set the name and file extesion 
            String planPath = Path.Combine(this.TestDir, package.Name);
            planPath = Path.ChangeExtension(planPath, ".pog");

            // create a new planogram
            Planogram testPlan = package.Planograms.First();
            testPlan.Name = "SparklingWater";

            // save the newly created planogram
            package.SaveAs(DomainPrincipal.CurrentUserId, planPath);
            package.Dispose();

            // Open the plan from file 
            viewModel.OpenPlanogramsFromFileSystem(planPath);

            // check that the plan gets opened
            Assert.AreNotEqual(null, viewModel.ActivePlanController, "a plan should have opened");
            Assert.AreNotEqual(0, viewModel.PlanControllers.Count);

            String planName1 = "NonSparklingWater";

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) =>
               {
                   var planProperties = ((PlanogramPropertiesViewModel)p.WindowParameters[0]);
                   viewModel.ActivePlanController.SourcePlanogram.Name = planName1;
                   planProperties.CancelCommand.Execute();
               });
            MainPageCommands.ShowPlanogramProperties.Execute();

            // check that nothing changed since the cancel button got clicked
            Assert.AreNotEqual(planName1, viewModel.ActivePlanController.SourcePlanogram.Name, "The name should not change since the window was cancelled");


            String planName2 = "BottledWater";
            // create another response for the OK buttton
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
               (p) =>
               {
                   var planProperties = ((PlanogramPropertiesViewModel)p.WindowParameters[0]);
                   viewModel.ActivePlanController.SourcePlanogram.Name = planName2;
                   planProperties.OKCommand.Execute();
               });

            // Open up the plan info to make changes
            MainPageCommands.ShowPlanogramProperties.Execute();

            // Check that the changes were made since the window was okayed
            Assert.AreEqual(planName2, viewModel.ActivePlanController.SourcePlanogram.Name, "Name should have changed since the window was okayed");

            viewModel.CloseAllPlans();
        }
        #endregion


    }

    [TestFixture]
    public class ProductFixtureSearchViewModelTests : TestBase
    {
        private MainPageViewModel _mainPageViewModel;
        private IEnumerable<ObjectFieldInfo> _productFields = PlanogramProduct.EnumerateDisplayableFieldInfos(true, true, true);
        IEnumerable<PropertyInfo> _productProperties = typeof(PlanogramProduct).GetProperties();
        IEnumerable<PropertyInfo> _assortmentProductProperties = typeof(PlanogramAssortmentProduct).GetProperties();
        IEnumerable<PropertyInfo> _performanceDataProperties = typeof(PlanogramPerformanceData).GetProperties();
        IEnumerable<PropertyInfo> _customAttributesProperties = typeof(Galleria.Framework.Planograms.Model.CustomAttributeData).GetProperties();

        [SetUp]
        public void SetUp()
        {
            _mainPageViewModel = new MainPageViewModel();
        }

        #region Product Search
        [Test]
        public void NextCommand_SimpleProductSearch_ReturnsResults()
        {
            base.WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                ((NUnitWindowService.NUnitWindowServiceArgs e) =>
                {
                    ((PlanogramPropertiesViewModel)e.WindowParameters.First()).OKCommand.Execute();
                }));
            var plan = _mainPageViewModel.CreateNewPlanogram().SourcePlanogram.Model;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            plan.Products.First().Name = "Hello World";
            var viewModel = new ProductFixtureSearchViewModel(_mainPageViewModel);
            viewModel.SearchText = "Hello World";
            viewModel.SearchByProduct = true;

            viewModel.SearchCommand.Execute();

            Assert.That(viewModel.SearchResults, Has.Count.EqualTo(1));
        }

        [Test]
        public void NextCommand_SimpleProductSearch_WhenCaseDoesntMatchAndMatchCaseTrue_ShouldReturnNoResults()
        {
            base.WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                ((NUnitWindowService.NUnitWindowServiceArgs e) =>
                {
                    ((PlanogramPropertiesViewModel)e.WindowParameters.First()).OKCommand.Execute();
                }));
            var plan = _mainPageViewModel.CreateNewPlanogram().SourcePlanogram.Model;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            plan.Products.First().Name = "Hello World";
            var viewModel = new ProductFixtureSearchViewModel(_mainPageViewModel);
            viewModel.SearchText = "hello world";
            viewModel.SearchByProduct = true;
            viewModel.MatchCase = true;
            base.WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);

            viewModel.SearchCommand.Execute();

            Assert.That(viewModel.SearchResults, Is.Empty);
        }

        [Test]
        public void NextCommand_SimpleProductSearch_WhenCaseDoesntMatchAndMatchCaseFalse_ShouldReturnOneResult()
        {
            base.WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                ((NUnitWindowService.NUnitWindowServiceArgs e) =>
                {
                    ((PlanogramPropertiesViewModel)e.WindowParameters.First()).OKCommand.Execute();
                }));
            var plan = _mainPageViewModel.CreateNewPlanogram().SourcePlanogram.Model;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            plan.Products.First().Name = "Hello World";
            var viewModel = new ProductFixtureSearchViewModel(_mainPageViewModel);
            viewModel.SearchText = "hello world";
            viewModel.SearchByProduct = true;
            viewModel.MatchCase = false;
            base.WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);

            viewModel.SearchCommand.Execute();

            Assert.That(viewModel.SearchResults, Has.Count.EqualTo(1));
        }

        [Test]
        public void NextCommand_ProductSearch_ReturnsResultForProductField(
            [ValueSource("_productFields")] ObjectFieldInfo field)
        {
            base.WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                ((NUnitWindowService.NUnitWindowServiceArgs e) =>
                {
                    ((PlanogramPropertiesViewModel)e.WindowParameters.First()).OKCommand.Execute();
                }));
            var planView = _mainPageViewModel.CreateNewPlanogram().SourcePlanogram;
            planView.BeginUpdate();
            var plan = planView.Model;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            plan.CreateAssortmentFromProducts();
            Object setData = SetFieldData(plan.Products.First(), field);
            Galleria.Ccm.Editor.Client.Wpf.App.ViewState.Settings.Model.SelectedColumns.Add(
                UserEditorSettingsSelectedColumn.NewUserEditorSettingsSelectedColumn(field.FieldPlaceholder, SearchColumnType.Product));
            var viewModel = new ProductFixtureSearchViewModel(_mainPageViewModel);
            viewModel.SearchText = setData.ToString();
            viewModel.SearchByProduct = true;
            viewModel.MatchCase = false;
            base.WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);

            viewModel.SearchCommand.Execute();

            Assert.That(viewModel.SearchResults, Has.Count.EqualTo(1));
        }

        private object SetFieldData(PlanogramProduct prod, ObjectFieldInfo field)
        {
            Object fieldValue = GetDefaultFieldValue(field);

            if (field.PropertyName.Contains(PlanogramProduct.CustomAttributesProperty.Name))
            {
                var customAttributesProperty = _productProperties.First(p => p.Name.Equals(PlanogramProduct.CustomAttributesProperty.Name));
                var customAttributes = customAttributesProperty.GetValue(prod, null) as Galleria.Framework.Planograms.Model.CustomAttributeData;
                var customPropertyName = Regex.Match(field.PropertyName, String.Format(@"{0}\.(\w+)", PlanogramProduct.CustomAttributesProperty.Name)).Groups[1].Value;
                var property = _customAttributesProperties.First(p => p.Name.Equals(customPropertyName));
                property.SetValue(customAttributes, fieldValue, null);
            }
            else if (field.PropertyName.Contains(PlanogramProduct.AssortmentFieldPrefix))
            {
                var property = _assortmentProductProperties.First(p => p.Name.Equals(
                    Regex.Match(field.PropertyName, String.Format(@"{0}(\w+)", PlanogramProduct.AssortmentFieldPrefix)).Groups[1].Value));
                var assortmentProduct = prod.GetPlanogramAssortmentProduct();
                if (assortmentProduct != null) property.SetValue(assortmentProduct, fieldValue, null);
            }
            else if (field.PropertyName.Contains(PlanogramPerformance.PerformanceDataProperty.Name))
            {
                var property = _performanceDataProperties.First(p => p.Name.Equals(
                        Regex.Match(field.PropertyName, String.Format(@"{0}\.(\w+)", PlanogramPerformance.PerformanceDataProperty.Name)).Groups[1].Value));
                var performanceData = prod.GetPlanogramPerformanceData();
                if (performanceData != null) property.SetValue(performanceData, fieldValue, null);
            }
            else
            {
                var property = _productProperties.First(p => p.Name.Equals(field.PropertyName));
                property.SetValue(prod, fieldValue, null);
            }

            if (!field.PropertyType.IsEnum) return fieldValue;

            String friendlyName = GetFriendlyName(field.PropertyType, fieldValue);

            return friendlyName.Substring(0, friendlyName.Length / 2);
        }

        private String GetFriendlyName(Type enumType, Object enumValue)
        {
            String friendlyName;
            String enumText = enumValue.ToString();
            Type enumHelperType = Assembly.GetAssembly(enumType).GetType(String.Format("{0}Helper", enumType.FullName), throwOnError: false); //tries to get the helper enums friendly name using the enumType, which throws "False" if it doesnt work.
            if (enumHelperType == null)
            {
                friendlyName = enumText;
            }
            else
            {
                FieldInfo friendlyNamesField = enumHelperType.GetField("FriendlyNames", BindingFlags.Static | BindingFlags.Public);  //Finds the friendly names field for this enum helper type
                if (friendlyNamesField == null)
                {
                    friendlyName = enumText;
                }
                else
                {
                    IDictionary friendlyNamesDictionary = friendlyNamesField.GetValue(null) as IDictionary;  //The friendlyNames that where gathered from the enumHelper are now added to a dictionary 
                    if (friendlyNamesDictionary == null)
                    {
                        friendlyName = enumText;
                    }
                    else
                    {
                        if (friendlyNamesDictionary.Contains(enumValue)) //Checks to see if the dictionary that was created contains the enumValue (parsed above)
                        {
                            String friendlyNameFromDictionary = friendlyNamesDictionary[enumValue] as String; // new string friendly name becomes the value found in the dictionary (using enumValue) but as a string for comparison :)
                            if (friendlyNameFromDictionary == null)
                            {
                                friendlyName = enumText;
                            }
                            else
                            {
                                friendlyName = friendlyNameFromDictionary; //previously created friendlynameForComparison is given the friendlyName value
                            }
                        }
                        else
                        {
                            friendlyName = enumText;
                        }
                    }
                }
            }

            return friendlyName;
        }

        private Object GetDefaultFieldValue(ObjectFieldInfo field)
        {
            if (field.PropertyType.IsEnum)
            {
                return Enum.Parse(field.PropertyType, "1");
            }
            else if (field.PropertyType.Equals(typeof(String)))
            {
                return "HelloWorld";
            }
            else if (field.PropertyType.Equals(typeof(Boolean)) || field.PropertyType.Equals(typeof(Boolean?)))
            {
                return true;
            }
            else if (field.PropertyType.Equals(typeof(DateTime)) || field.PropertyType.Equals(typeof(DateTime?)))
            {
                return DateTime.Now;
            }
            else if (field.PropertyType.Equals(typeof(Single)) || field.PropertyType.Equals(typeof(Single?)))
            {
                return Convert.ToSingle(Math.PI);
            }
            else if (field.PropertyType.Equals(typeof(Double)) || field.PropertyType.Equals(typeof(Double?)))
            {
                return Math.PI;
            }
            else if (field.PropertyType.Equals(typeof(Int32)) || field.PropertyType.Equals(typeof(Int32?)))
            {
                return 10;
            }
            else if (field.PropertyType.Equals(typeof(Int16)) || field.PropertyType.Equals(typeof(Int16?)))
            {
                return (Int16)10;
            }
            else if (field.PropertyType.Equals(typeof(Byte)) || field.PropertyType.Equals(typeof(Byte?)))
            {
                return (Byte)10;
            }
            return null;
        }
        #endregion

        #region Component Search
        [Test]
        public void NextCommand_SimpleComponentSearch_ReturnsResults()
        {
            base.WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                ((NUnitWindowService.NUnitWindowServiceArgs e) =>
                {
                    ((PlanogramPropertiesViewModel)e.WindowParameters.First()).OKCommand.Execute();
                }));
            var plan = _mainPageViewModel.CreateNewPlanogram().SourcePlanogram.Model;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.Components.First().Name = "Hello World";
            var viewModel = new ProductFixtureSearchViewModel(_mainPageViewModel);
            viewModel.SearchText = "Hello World";
            viewModel.SearchByProduct = false;
            viewModel.SearchByComponent = true;

            viewModel.SearchCommand.Execute();

            Assert.That(viewModel.SearchResults, Has.Count.EqualTo(1));
        }

        [Test]
        public void NextCommand_SimpleComponentSearch_WhenCaseDoesntMatchAndMatchCaseTrue_ShouldReturnNoResults()
        {
            base.WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                ((NUnitWindowService.NUnitWindowServiceArgs e) =>
                {
                    ((PlanogramPropertiesViewModel)e.WindowParameters.First()).OKCommand.Execute();
                }));
            var plan = _mainPageViewModel.CreateNewPlanogram().SourcePlanogram.Model;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.Components.First().Name = "Hello World";
            var viewModel = new ProductFixtureSearchViewModel(_mainPageViewModel);
            viewModel.SearchText = "hello world";
            viewModel.SearchByProduct = false;
            viewModel.SearchByComponent = true;
            viewModel.MatchCase = true;
            base.WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);

            viewModel.SearchCommand.Execute();

            Assert.That(viewModel.SearchResults, Is.Empty);
        }

        [Test]
        public void NextCommand_SimpleComponentSearch_WhenCaseDoesntMatchAndMatchCaseFalse_ShouldReturnOneResult()
        {
            base.WindowService.AddResponse(
                NUnitWindowService.ShowDialogTMethod,
                ((NUnitWindowService.NUnitWindowServiceArgs e) =>
                {
                    ((PlanogramPropertiesViewModel)e.WindowParameters.First()).OKCommand.Execute();
                }));
            var plan = _mainPageViewModel.CreateNewPlanogram().SourcePlanogram.Model;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.Components.First().Name = "Hello World";
            var viewModel = new ProductFixtureSearchViewModel(_mainPageViewModel);
            viewModel.SearchText = "hello world";
            viewModel.SearchByProduct = false;
            viewModel.SearchByComponent = true;
            viewModel.MatchCase = false;
            base.WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);

            viewModel.SearchCommand.Execute();

            Assert.That(viewModel.SearchResults, Has.Count.EqualTo(1));
        }
        #endregion
    }

}

