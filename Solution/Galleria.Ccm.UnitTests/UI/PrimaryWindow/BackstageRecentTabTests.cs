﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 820
// V8-31140 : L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.UI.PrimaryWindow
{
    [TestFixture]
    public sealed class BackstageRecentTabTests : TestBase
    {
        #region Test Helpers

        private String AddFilePlanogram()
        {
            Int32 userId = User.GetCurrentUser().Id;

            Package pk = TestDataHelper.CreatePlanCreweSparklingWater();

            pk.Name = Guid.NewGuid().ToString();
            pk.Planograms[0].Name = pk.Name;

            String planPath = Path.Combine(this.TestDir, pk.Name);
            planPath = Path.ChangeExtension(planPath, ".pog");

            pk = pk.SaveAs(userId, planPath);
            pk.Dispose();

            return pk.Id.ToString();
        }

        private Object AddRepositoryPlanogram()
        {
            if(this.EntityId == 0)
            {
                SetMockRepositoryConnection();
            }

            Int32 userId = User.GetCurrentUser().Id;

            Package pk = TestDataHelper.CreatePlanCreweSparklingWater();

            pk.Name = Guid.NewGuid().ToString();
            pk.Planograms[0].Name = pk.Name;
           

            pk = pk.Save();
            pk.Dispose();

            //associate to the root planogram group.
            PlanogramGroup.AssociatePlanograms(
                PlanogramHierarchy.FetchByEntityId(this.EntityId).RootGroup.Id,
                new List<Int32> { (Int32)pk.Id });

            return pk.Id;
        }


        #endregion

        [Test]
        public void OpenAddsFileItem()
        {
            var viewModel = new BackstageRecentTabViewModel();

            //create a new planogram
            String planId = AddFilePlanogram();

            Int32 preItemCount = viewModel.RecentPlanograms.Count;

            //open it
            App.MainPageViewModel.OpenPlanogramsFromFileSystem(planId);

            //check the item was added
            Assert.AreEqual(preItemCount + 1, viewModel.RecentPlanograms.Count);

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r=> Object.Equals(r.PlanogramId, planId));
            Assert.AreEqual(App.MainPageViewModel.PlanControllers[0].SourcePlanogram.Name, entry.Name);
            Assert.AreEqual(RecentPlanogramType.Pog, entry.Type);
            Assert.IsNotNull(entry);

        }

        [Test]
        public void SaveAddsFileItem()
        {
            var viewModel = new BackstageRecentTabViewModel();

            //create a new planogram
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, 
                (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            
            var controller = App.MainPageViewModel.CreateNewPlanogram();
            TestDataHelper.CreatePlanCreweSparklingWater(controller.SourcePlanogram.Model);

            String planPath = Path.Combine(this.TestDir, Guid.NewGuid().ToString());
            planPath = Path.ChangeExtension(planPath, ".pog");

            Int32 preItemCount = viewModel.RecentPlanograms.Count;

            //save to file
            App.MainPageViewModel.SaveAsFile(controller, planPath);

            //check the item was added
            Assert.AreEqual(preItemCount + 1, viewModel.RecentPlanograms.Count);

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planPath));
            Assert.AreEqual(controller.SourcePlanogram.Name, entry.Name);
            Assert.AreEqual(RecentPlanogramType.Pog, entry.Type);
            Assert.IsNotNull(entry);
        }


        [Test]
        public void OpenUpdatesFileItem()
        {
            var viewModel = new BackstageRecentTabViewModel();

            //create a new planogram
            String planId = AddFilePlanogram();

            //open it
            App.MainPageViewModel.OpenPlanogramsFromFileSystem(planId);

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            DateTime openedTime = entry.DateLastAccessed;

            //close it
            App.MainPageViewModel.ClosePlanController(App.MainPageViewModel.PlanControllers.First());

            //pause a sec
            Thread.Sleep(new TimeSpan(0, 0, 1));

            Int32 preItemCount = viewModel.RecentPlanograms.Count;

            //open it again
            App.MainPageViewModel.OpenPlanogramsFromFileSystem(planId);

            entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            Assert.Greater(entry.DateLastAccessed, openedTime);

            App.MainPageViewModel.ClosePlanController(App.MainPageViewModel.PlanControllers.First());
        }

        [Test]
        public void OpenAddsRepositoryItem()
        {
            var viewModel = new BackstageRecentTabViewModel();

            //create a new planogram
            Object planId = AddRepositoryPlanogram();

            Int32 preItemCount = viewModel.RecentPlanograms.Count;

            //open it
            PlanogramInfo info = PlanogramInfoList.FetchByIds(new List<Int32> { (Int32)planId }).First();
            App.MainPageViewModel.OpenPlanogramsFromRepository(new List<PlanogramInfo>{info});

            //check the item was added
            Assert.AreEqual(preItemCount + 1, viewModel.RecentPlanograms.Count);

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            Assert.AreEqual(info.Name, entry.Name);
            Assert.AreEqual(RecentPlanogramType.Repository, entry.Type);
            Assert.IsNotNull(entry);
        }

        [Test]
        public void OpenUpdatesRepositoryItem()
        {
            var viewModel = new BackstageRecentTabViewModel();

            //create a new planogram
            Object planId = AddRepositoryPlanogram();

            //open it
            PlanogramInfo info = PlanogramInfoList.FetchByIds(new List<Int32> { (Int32)planId }).First();
            App.MainPageViewModel.OpenPlanogramsFromRepository(new List<PlanogramInfo> { info });

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            DateTime openedTime = entry.DateLastAccessed;

            //close it
            App.MainPageViewModel.ClosePlanController(App.MainPageViewModel.PlanControllers.First());

            //pause a sec
            Thread.Sleep(new TimeSpan(0, 0, 1));

            Int32 preItemCount = viewModel.RecentPlanograms.Count;

            //open it again
            App.MainPageViewModel.OpenPlanogramsFromRepository(new List<PlanogramInfo> { info });

            entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            Assert.Greater(entry.DateLastAccessed, openedTime);

            App.MainPageViewModel.ClosePlanController(App.MainPageViewModel.PlanControllers.First());
        }

        [Test]
        public void SaveAddsRepositoryItem()
        {
            SetMockRepositoryConnection();

            var viewModel = new BackstageRecentTabViewModel();

            //create a new planogram
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });

            var controller = App.MainPageViewModel.CreateNewPlanogram();
            TestDataHelper.CreatePlanCreweSparklingWater(controller.SourcePlanogram.Model);

            Int32 preItemCount = viewModel.RecentPlanograms.Count;

            //save to file
            App.MainPageViewModel.SaveAsRepository(controller);
            Object planId = controller.SourcePlanogram.Model.Id;

            //check the item was added
            Assert.AreEqual(preItemCount + 1, viewModel.RecentPlanograms.Count);

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            Assert.AreEqual(controller.SourcePlanogram.Name, entry.Name);
            Assert.AreEqual( RecentPlanogramType.Repository, entry.Type);
            Assert.IsNotNull(entry);
        }

        [Test]
        public void OpenExternalFileDoesNotAddItem()
        {
            Assert.Ignore();
        }

        [Test]
        public void MaxCountIsEnforced()
        {
            var viewModel = new BackstageRecentTabViewModel();
            Int32 maxCount = App.ViewState.RecentPlanogramsView.MaxItemCount;

            //add plans beyond the max count
            for (Int32 i = 0; i < maxCount + 5; i++)
            {
                //create a new planogram
                Object planId = AddRepositoryPlanogram();

                //open it
                PlanogramInfo info = PlanogramInfoList.FetchByIds(new List<Int32> { (Int32)planId }).First();
                App.MainPageViewModel.OpenPlanogramsFromRepository(new List<PlanogramInfo> { info });

                //close it
                App.MainPageViewModel.CloseAllPlans();
            }

            Assert.AreEqual(maxCount, viewModel.RecentPlanograms.Count);

            //pin the first item
            viewModel.PinCommand.Execute(viewModel.RecentPlanograms.First());

            //check that pinned items are ignored.
            App.MainPageViewModel.OpenPlanogramsFromRepository(
                new List<PlanogramInfo> { PlanogramInfoList.FetchByIds(
                    new List<Int32> { (Int32)AddRepositoryPlanogram() }).First() });
            App.MainPageViewModel.CloseAllPlans();

            Assert.AreEqual(maxCount+1, viewModel.RecentPlanograms.Count);
        }

        [Test]
        public void ChangingRepositoryUpdatesItemStatus()
        {
            var viewModel = new BackstageRecentTabViewModel();

            //add a repository plan
            Object planId = AddRepositoryPlanogram();

            //open it so that it goes in the recent list.
            App.MainPageViewModel.OpenPlanogramsFromRepository(
               new List<PlanogramInfo> { PlanogramInfoList.FetchByIds(
                    new List<Int32> { (Int32)planId }).First() });
            App.MainPageViewModel.CloseAllPlans();

            //now create another entity and connect to that.
            Entity et = Entity.NewEntity();
            et.Name = "Entity2";
            et = et.Save();
            App.ViewState.EntityId = et.Id;

            //check the plan is now disabled.
            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            Assert.IsFalse(entry.IsForCurrentRepository);

            //create another plan and open
            Object planId2 = AddRepositoryPlanogram();
            App.MainPageViewModel.OpenPlanogramsFromRepository(
               new List<PlanogramInfo> { PlanogramInfoList.FetchByIds(
                    new List<Int32> { (Int32)planId2 }).First() });
            App.MainPageViewModel.CloseAllPlans();

            //check that is active
            entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId2));
            Assert.IsTrue(entry.IsForCurrentRepository);

            //change connection
            SetMockRepositoryConnection();

            //check that first is now active but second is not.
            entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            Assert.IsTrue(entry.IsForCurrentRepository);

            entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId2));
            Assert.IsFalse(entry.IsForCurrentRepository);

        }

        [Test]
        public void OpenFilePlanFromRecent()
        {
            var viewModel = new BackstageRecentTabViewModel();
            
            String planId = AddFilePlanogram();
            App.MainPageViewModel.OpenPlanogramsFromFileSystem(planId);
            App.MainPageViewModel.CloseAllPlans();

            //open it from the recent list entry
            Assert.AreEqual(0, App.MainPageViewModel.PlanControllers.Count);

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            App.MainPageViewModel.OpenRecentPlanogram(entry);

            Assert.AreEqual(1, App.MainPageViewModel.PlanControllers.Count);
            Assert.AreEqual(planId, App.MainPageViewModel.PlanControllers[0].SourcePlanogram.ParentPackageView.FileName);
        }

        [Test]
        public void OpenRepositoryPlanFromRecent()
        {
            var viewModel = new BackstageRecentTabViewModel();

            Object planId = AddRepositoryPlanogram();
            App.MainPageViewModel.OpenPlanogramsFromRepository(
               new List<PlanogramInfo> { PlanogramInfoList.FetchByIds(new List<Int32> { (Int32)planId }).First() });
            App.MainPageViewModel.CloseAllPlans();

            //open it from the recent list entry
            Assert.AreEqual(0, App.MainPageViewModel.PlanControllers.Count);

            var entry = viewModel.RecentPlanograms.FirstOrDefault(r => Object.Equals(r.PlanogramId, planId));
            App.MainPageViewModel.OpenRecentPlanogram(entry);

            Assert.AreEqual(1, App.MainPageViewModel.PlanControllers.Count);
            Assert.AreEqual(planId, App.MainPageViewModel.PlanControllers[0].SourcePlanogram.Model.Id);
        }
    }
}
