﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 801)

// V8-27897 : A.Kuszyk
//      Created.
// V8-28676 : A.Silva
//      Added Real Images related tests.
// V8-28878 : A.Silva
//      Amended UpdatePlanogramProducts_Executed_UpdatesProductAttributes_WhenDialogResultTrue to respond to the messagebox.

#endregion

#region Version History : CCM820
// V8-30794 : A.Kuszyk
//  Added tests for ApplyHighlightAsProductColour command.
#endregion

#region Version History: CCM830

// V8-31742 : A.Silva
//  Added NewPlanogramCompareView_WhenMissingActivePlanogram_ShouldBeDisabled.
// V8-32415  : J.Pickup
//  Added new tests - UpdateFacingWideFromNumericKey_ConsidersAutofillSettings, UpdateFacingWideFromNumericKey_SetsFacingsWideCorrectly
// V8-32255 : A.Silva
//  Amended UpdatePlanogramProducts_Executed_UpdatesProductAttributes_WhenDialogResultTrue as it access UI components that require STA.
// V8-32359 : A.Silva
//  Added UpdatePlanogramFromProtuctAttribute and UpdateAllPlanogramsFromAttribute commands tests.

#endregion

#endregion

using System;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Helpers;
using Moq;

namespace Galleria.Ccm.UnitTests.UI.PrimaryWindow
{
    [TestFixture]
    public class MainPageCommandsTests : TestBase
    {
        #region Constants

        private const ModalMessageResult CancelResultForRemoveImagesPrompt = ModalMessageResult.Button2;
        private const ModalMessageResult OkResultForRemoveImagesPrompt = ModalMessageResult.Button1;
        private const Int32 ImageCount = 10;

        #endregion

        #region UpdatePlanogramProducts

        [Test, RequiresSTA]
        public void UpdatePlanogramProducts_Executed_UpdatesProductAttributes_WhenDialogResultTrue()
        {
            ProductLibraryViewModel productLibrary = new ProductLibraryViewModel();
            productLibrary.Model = Galleria.Ccm.Model.ProductLibrary.NewProductLibrary();
            var product = Galleria.Ccm.Model.Product.NewProduct();
            TestDataHelper.SetPropertyValues(product);
            productLibrary.Products.Add(Galleria.Ccm.Model.ProductLibraryProduct.NewProductLibraryProduct(product));

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.System);
            Planogram planogramModel = pk.Planograms.Add();
            PlanogramProduct planProduct = PlanogramProduct.NewPlanogramProduct();
            planProduct.Gtin = product.Gtin;
            planogramModel.Products.Add(planProduct);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(pk, false);
            PlanogramView planogram = pkView.PlanogramViews[0];

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,a=>
                {
                    Object firstParameter = a.WindowParameters.First();
                    var viewModel = firstParameter as ProductAttributeSelectorViewModel;
                    viewModel.DialogResult = true;
                });
            WindowService.ShowMessageSetReponse(ModalMessageResult.Button1);

            MainPageCommands.UpdatePlanogramProducts_Executed(productLibrary, planogram);

            AssertHelper.AreEqual<IPlanogramProduct>(product, planProduct, "Id");
        }

        [Test]
        public void UpdatePlanogramProducts_Executed_DoesNothing_WhenDialogResultFalse()
        {
            ProductLibraryViewModel productLibrary = new ProductLibraryViewModel();
            productLibrary.Model = Galleria.Ccm.Model.ProductLibrary.NewProductLibrary();
            var product = Galleria.Ccm.Model.Product.NewProduct();
            TestDataHelper.SetPropertyValues(product);
            productLibrary.Products.Add(Galleria.Ccm.Model.ProductLibraryProduct.NewProductLibraryProduct(product));

            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.Unknown);
            Planogram planogramModel = pk.Planograms.Add();
            PlanogramProduct planProduct = PlanogramProduct.NewPlanogramProduct();
            planProduct.Gtin = product.Gtin;
            var expected = planProduct.Copy();
            planogramModel.Products.Add(planProduct);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(pk, false);
            PlanogramView planogram = pkView.PlanogramViews.First();

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var viewModel = firstParameter as ProductAttributeSelectorViewModel;
                viewModel.DialogResult = false;
            });

            MainPageCommands.UpdatePlanogramProducts_Executed(productLibrary, planogram);

            AssertHelper.AreEqual<IPlanogramProduct>(expected, planProduct, "Id");
        }

        [Test]
        public void UpdatePlanogramProducts_Executed_OnlyPassesMappedColumnsToViewModel()
        {
            ProductLibraryViewModel productLibrary = new ProductLibraryViewModel();
            productLibrary.Model = Galleria.Ccm.Model.ProductLibrary.NewProductLibrary();
            var product = Galleria.Ccm.Model.Product.NewProduct();
            TestDataHelper.SetPropertyValues(product);
            productLibrary.Products.Add(Galleria.Ccm.Model.ProductLibraryProduct.NewProductLibraryProduct(product));
            Package pk = Package.NewPackage(User.GetCurrentUser().Id, PackageLockType.Unknown);
            Planogram planogramModel = pk.Planograms.Add();
            PlanogramProduct planProduct = PlanogramProduct.NewPlanogramProduct();
            planProduct.Gtin = product.Gtin;
            var expected = planProduct.Copy();
            planogramModel.Products.Add(planProduct);

            PackageViewModel pkView = PackageViewModel.NewPackageViewModel(pk, false);
            PlanogramView planogram = pkView.PlanogramViews.First();

            var fileData = ImportFileData.NewImportFileData();
            fileData.MappingList = ProductLibraryImportMappingList.NewProductLibraryImportMappingList();
            productLibrary.Model.Populate(true, String.Empty, fileData, false);
            productLibrary.Model.ColumnMappings.First().Source = "Source";
            Int32 noOfAvailableAttributes = 0;

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var viewModel = firstParameter as ProductAttributeSelectorViewModel;
                noOfAvailableAttributes = viewModel.AvailableAttributes.Count;
            });

            MainPageCommands.UpdatePlanogramProducts_Executed(productLibrary, planogram);

            Assert.AreEqual(1,noOfAvailableAttributes);
        }

        #endregion
        
        [Test]
        public void UpdateFacingWideFromNumericKey_ConsidersAutofillSettings()
        {
            // Create a planogram.
            var plan = "TestPlanogram".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var planProduct1 = plan.AddProduct();
            var position1 = shelf.AddPosition(bay, planProduct1);
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());

            //Add plan controller
            PlanogramView planView = new PlanogramView(plan, false);
            PlanControllerViewModel planController = new PlanControllerViewModel(planView);
            App.MainPageViewModel.ActivePlanController = planController;
            //Set selection
            planController.SelectedPlanItems.SetSelection(planController.SourcePlanogram.EnumerateAllPositions());

            Boolean result;
            result = MainPageCommands.UpdateFacingWideFromNumericKey(System.Windows.Input.Key.NumPad3);
            Assert.IsTrue(result, "Autofill was off. Should've filled");

            plan.ProductPlacementX = PlanogramProductPlacementXType.FillWide;

            result = MainPageCommands.UpdateFacingWideFromNumericKey(System.Windows.Input.Key.NumPad3);
            Assert.IsFalse(result, "UpdateFacingWideFromNumericKey should not apply change when autofill wide on.");            
        }

        [Test]
        public void UpdateFacingWideFromNumericKey_SetsFacingsWideCorrectly()
        {
            // Create a planogram.
            var plan = "TestPlanogram".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var planProduct1 = plan.AddProduct();
            var position1 = shelf.AddPosition(bay, planProduct1);
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());

            //Add plan controller
            PlanogramView planView = new PlanogramView(plan, false);
            PlanControllerViewModel planController = new PlanControllerViewModel(planView);
            App.MainPageViewModel.ActivePlanController = planController;
            //Set selection
            planController.SelectedPlanItems.SetSelection(planController.SourcePlanogram.EnumerateAllPositions().Where(p => p.Model.Id != position2.Id));

            position1.FacingsWide = 1;
            position2.FacingsWide = 5;

            Boolean result;
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;

            result = MainPageCommands.UpdateFacingWideFromNumericKey(System.Windows.Input.Key.NumPad3);
            Assert.IsTrue(result, "UpdateFacingWideFromNumericKey should not apply change when autofill wide on.");

            //only selected should change
            Assert.IsTrue(position1.FacingsWide == 3);
            Assert.IsTrue(position2.FacingsWide == 5);
        }

        #region ApplyHighlightAsProductColor Command

        private const String _applyHighlightHighlightName = "04 Brand";

        private Galleria.Ccm.Model.Highlight CreateBrandHighlight(Int32 entityId)
        {
            Galleria.Ccm.Model.Highlight brand = Galleria.Ccm.Model.Highlight.NewHighlight(entityId);

            brand.Name = _applyHighlightHighlightName;
            brand.Type = Galleria.Ccm.Model.HighlightType.Position;
            brand.MethodType = Galleria.Ccm.Model.HighlightMethodType.Group;
            brand.IsFilterEnabled = false;
            brand.IsPreFilter = true;
            brand.IsAndFilter = true;
            brand.Field1 = "[PlanogramProduct.Brand]";
            brand.Field2 = "";
            brand.QuadrantXType = Galleria.Ccm.Model.HighlightQuadrantType.Mean;
            brand.QuadrantXConstant = 0F;
            brand.QuadrantYType = Galleria.Ccm.Model.HighlightQuadrantType.Mean;
            brand.QuadrantYConstant = 0F;

            #region Highlight Groups

            var brandGroup01 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup01.Order = 1;
            brandGroup01.Name = "GALLERIA";
            brandGroup01.DisplayName = "";
            brandGroup01.FillColour = -65536;
            brandGroup01.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup01);

            var brandGroup02 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup02.Order = 2;
            brandGroup02.Name = "Kelloggs";
            brandGroup02.DisplayName = "";
            brandGroup02.FillColour = -16711936;
            brandGroup02.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup02);

            var brandGroup03 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup03.Order = 3;
            brandGroup03.Name = "Nestle";
            brandGroup03.DisplayName = "";
            brandGroup03.FillColour = -16776961;
            brandGroup03.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup03);

            var brandGroup04 = Galleria.Ccm.Model.HighlightGroup.NewHighlightGroup();
            brandGroup04.Order = 4;
            brandGroup04.Name = "Weetabix";
            brandGroup04.DisplayName = "";
            brandGroup04.FillColour = -256;
            brandGroup04.FillPatternType = Galleria.Ccm.Model.HighlightFillPatternType.Solid;
            brand.Groups.Add(brandGroup04);

            #endregion

            return brand;
        }

        private Planogram SetupApplyHighlightAsProductColourViews()
        {
            var entityId = TestDataHelper.InsertEntityDtos(DalFactory, 1).First().Id;
            var package = "".CreatePackage();
            var plan = package.AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            shelf.AddPosition(bay);
            shelf.AddPosition(bay);
            shelf.AddPosition(bay);
            foreach (var prod in plan.Products)
            {
                prod.FillColour = -1;
                prod.ColourGroupValue = null;
            }
            var packageView = PackageViewModel.NewPackageViewModel(package);
            var planView = packageView.PlanogramViews.First();
            var planController = new PlanControllerViewModel(planView);
            App.MainPageViewModel.ActivePlanController = planController;
            var designView = planController.AddNewDocument(Ccm.Model.DocumentType.Design, false);
            designView.Highlight = new HighlightItem(CreateBrandHighlight(entityId));
            designView.IsActiveDocument = true;

            return plan;
        }

        [Test]
        public void ApplyHighlightAsProductColor_Executed_UpdatesAllProductColours()
        {
            var plan = SetupApplyHighlightAsProductColourViews();
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, (NUnitWindowService.NUnitWindowServiceArgs e) => { });

            MainPageCommands.ApplyHighlightAsProductColour.Execute();

            foreach (var prod in plan.Products)
            {
                Assert.AreNotEqual(0, prod.FillColour);
            }
        }

        [Test]
        public void ApplyHighlightAsProductColor_Executed_UpdatesAllProductColourGroupValues()
        {
            var plan = SetupApplyHighlightAsProductColourViews();
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, (NUnitWindowService.NUnitWindowServiceArgs e) => { });

            MainPageCommands.ApplyHighlightAsProductColour.Execute();

            foreach (var prod in plan.Products)
            {
                Assert.IsNotNullOrEmpty(prod.ColourGroupValue);
            }
        }

        [Test]
        public void ApplyHighlightAsProductColor_Executed_UpdatesHighlightSequenceStrategy()
        {
            var plan = SetupApplyHighlightAsProductColourViews();
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, (NUnitWindowService.NUnitWindowServiceArgs e) => { });

            MainPageCommands.ApplyHighlightAsProductColour.Execute();

            Assert.AreEqual(_applyHighlightHighlightName, plan.HighlightSequenceStrategy);
        }

        [Test]
        public void ApplyHighlightAsProductColor_Executed_NotifiesUser()
        {
            var plan = SetupApplyHighlightAsProductColourViews();
            Boolean userNotified = false;
            WindowService.AddResponse(
                NUnitWindowService.ShowMessageMethod, 
                (NUnitWindowService.NUnitWindowServiceArgs e) => { userNotified = true; });

            MainPageCommands.ApplyHighlightAsProductColour.Execute();

            Assert.IsTrue(userNotified);
        }

        [Test]
        public void ApplyHighlightAsProductColor_CanExecute_ReturnsFalseWhenActiveDocumentDoesntSupportHighlights()
        {
            var plan = SetupApplyHighlightAsProductColourViews();
            App.MainPageViewModel.ActivePlanController.PlanDocuments.First().IsActiveDocument = false;
            var newDocument = App.MainPageViewModel.ActivePlanController.AddNewDocument(Ccm.Model.DocumentType.Assortment,false);
            newDocument.IsActiveDocument = true;

            Boolean canExecute = MainPageCommands.ApplyHighlightAsProductColour.CanExecute();

            Assert.IsFalse(canExecute);
        }

        [Test]
        public void ApplyHighlightAsProductColor_CanExecute_ReturnsTrueWhenActiveDocumentDoesSupportHighlights()
        {
            var plan = SetupApplyHighlightAsProductColourViews();

            Boolean canExecute = MainPageCommands.ApplyHighlightAsProductColour.CanExecute();

            Assert.IsTrue(canExecute);
        }

        [Test]
        public void ApplyHighlightAsProductColor_CanExecute_ReturnsTrueWhenActiveDocumentHasHighlight()
        {
            var plan = SetupApplyHighlightAsProductColourViews();

            Boolean canExecute = MainPageCommands.ApplyHighlightAsProductColour.CanExecute();

            Assert.IsTrue(canExecute);
        }

        [Test]
        public void ApplyHighlightAsProductColor_CanExecute_ReturnsFalseWhenActiveDocumentHasNoHighlight()
        {
            var plan = SetupApplyHighlightAsProductColourViews();
            App.MainPageViewModel.ActivePlanController.PlanDocuments.First().Highlight = null;

            Boolean canExecute = MainPageCommands.ApplyHighlightAsProductColour.CanExecute();

            Assert.IsFalse(canExecute);
        }

        #endregion

        #region RemoveImagesFromPlanogram

        [Test]
        public void RemoveImagesFromPlanogram_WhenNoSourcePlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "RemoveImagesFromPlanogram should not be enabled when there is no active planogram.";
            const String disabledMessageExpectation = "RemoveImagesFromPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoActivePlanogram;

            Boolean actual = MainPageCommands.RemoveImagesFromPlanogram.CanExecute();
            String actualMessage = MainPageCommands.RemoveImagesFromPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        //Commented out as doing this forces images to load.
        //[Test]
        //public void RemoveImagesFromPlanogram_WhenNoImagesInPlanogram_ShouldBeDisabled()
        //{
        //    const String disabledExpectation = "RemoveImagesFromPlanogram should not be enabled when there are no images in the active planogram.";
        //    const String disabledMessageExpectation = "RemoveImagesFromPlanogram should display the correct disabled reason when disabled.";
        //    String expectedMessage = Message.DisabledReason_NoImagesInPlanogram;
        //    SetupActivePlanogramForImagesTests();

        //    Boolean actual = MainPageCommands.RemoveImagesFromPlanogram.CanExecute();
        //    String actualMessage = MainPageCommands.RemoveImagesFromPlanogram.DisabledReason;

        //    Assert.IsFalse(actual, disabledExpectation);
        //    Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        //}

        [Test]
        public void RemoveImagesFromPlanogram_WhenImagesInPlanogram_ShouldBeEnabled()
        {
            const String expectation = "RemoveImagesFromPlanogram should be enabled when there are images in the active planogram.";
            SetupActivePlanogramForImagesTests(ImageCount);

            Boolean actual = MainPageCommands.RemoveImagesFromPlanogram.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void RemoveImagesFromPlanogram_WhenExecuted_ShouldPromptUserConfirmation()
        {
            const String expectation = "Should prompt user for confirmation before removing images from planogram.";
            Boolean actual = false;
            SetupActivePlanogramForImagesTests(ImageCount);
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                actual = CheckMessageParameters(args, 
                    Message.PromptConfirmation_RemoveImagesFromPlanogram_Header, 
                    Message.PromptConfirmation_RemoveImagesFromPlanogram_Description);
                args.Result = CancelResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveImagesFromPlanogram.Execute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void RemoveImagesFromPlanogram_WhenExecuted_ShouldNotRemoveWithoutUserConfirmation()
        {
            const String expectation = "Should cancel removal if user cancels at prompt for confirmation.";
            SetupActivePlanogramForImagesTests(ImageCount);
            //  Store how many images are there at the start... there should be the same after cancelling.
            PlanogramImageList images = App.MainPageViewModel.ActivePlanController.SourcePlanogram.Images;
            Int32 expected = images.Count;
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                args.Result = CancelResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveImagesFromPlanogram.Execute();

            Int32 actual = images.Count;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void RemoveImagesFromPlanogram_WhenExecuted_ShouldRemoveAfterUserConfirmation()
        {
            const String expectation = "Should remove images if user accepts at prompt for confirmation.";
            const String precondition = "There need to be images to start with so that this test can be run.";
            const Int32 expected = 0;
            SetupActivePlanogramForImagesTests(ImageCount);
            //  Assert there are images to remove initially... there should 0 after executing.
            PlanogramImageList images = App.MainPageViewModel.ActivePlanController.SourcePlanogram.Images;
            if (images.Count == expected) Assert.Inconclusive(precondition);
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                args.Result = OkResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveImagesFromPlanogram.Execute();

            Int32 actual = images.Count;
            Assert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region RemoveProductImagesFromPlanogram

        [Test]
        public void RemoveProductImagesFromPlanogram_WhenNoSourcePlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "RemoveProductImagesFromPlanogram should not be enabled when there is no active planogram.";
            const String disabledMessageExpectation = "RemoveProductImagesFromPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoActivePlanogram;

            Boolean actual = MainPageCommands.RemoveProductImagesFromPlanogram.CanExecute();
            String actualMessage = MainPageCommands.RemoveProductImagesFromPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        //Commented out as doing this forces images to load.
        //[Test]
        //public void RemoveProductImagesFromPlanogram_WhenNoImagesInPlanogram_ShouldBeDisabled()
        //{
        //    const String disabledExpectation = "RemoveImagesFromPlanogram should not be enabled when there are no images in the active planogram.";
        //    const String disabledMessageExpectation = "RemoveImagesFromPlanogram should display the correct disabled reason when disabled.";
        //    String expectedMessage = Message.DisabledReason_NoImagesInPlanogram;
        //    SetupActivePlanogramForImagesTests();

        //    Boolean actual = MainPageCommands.RemoveProductImagesFromPlanogram.CanExecute();
        //    String actualMessage = MainPageCommands.RemoveProductImagesFromPlanogram.DisabledReason;

        //    Assert.IsFalse(actual, disabledExpectation);
        //    Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        //}

        [Test]
        public void RemoveProductImagesFromPlanogram_WhenNoProductImagesInPlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "RemoveImagesFromPlanogram should not be enabled when there are no product images in the active planogram.";
            const String disabledMessageExpectation = "RemoveImagesFromPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoProductImagesInPlanogram;
            SetupActivePlanogramForImagesTests(componentImageCount: ImageCount);

            Boolean actual = MainPageCommands.RemoveProductImagesFromPlanogram.CanExecute();
            String actualMessage = MainPageCommands.RemoveProductImagesFromPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        [Test]
        public void RemoveProductImagesFromPlanogram_WhenImagesInPlanogram_ShouldBeEnabled()
        {
            const String expectation = "RemoveProductImagesFromPlanogram should be enabled when there are product images in the active planogram.";
            SetupActivePlanogramForImagesTests(ImageCount);

            Boolean actual = MainPageCommands.RemoveProductImagesFromPlanogram.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void RemoveProductImagesFromPlanogram_WhenExecuted_ShouldPromptUserConfirmation()
        {
            const String expectation = "Should prompt user for confirmation before removing product images from planogram.";
            Boolean actual = false;
            SetupActivePlanogramForImagesTests(ImageCount);
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                actual = CheckMessageParameters(args,
                    Message.PromptConfirmation_RemoveImagesFromPlanogram_Header,
                    Message.PromptConfirmation_RemoveImagesFromPlanogram_Description);
                args.Result = CancelResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveProductImagesFromPlanogram.Execute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void RemoveProductImagesFromPlanogram_WhenExecuted_ShouldNotRemoveWithoutUserConfirmation()
        {
            const String expectation = "Should cancel removal if user cancels at prompt for confirmation.";
            SetupActivePlanogramForImagesTests(ImageCount, ImageCount);
            //  Store how many images are there at the start... there should be the same after cancelling.
            PlanogramImageList images = App.MainPageViewModel.ActivePlanController.SourcePlanogram.Images;
            Int32 expected = images.Count;
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                args.Result = CancelResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveProductImagesFromPlanogram.Execute();

            Int32 actual = images.Count;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void RemoveProductImagesFromPlanogram_WhenExecuted_ShouldRemoveAfterUserConfirmation()
        {
            const String expectation = "Should remove only product images if user accepts at prompt for confirmation.";
            const String precondition = "There need to be product images to start with so that this test can be run.";
            const Int32 expected = ImageCount;
            SetupActivePlanogramForImagesTests(ImageCount, ImageCount);
            //  Assert there are product images to remove initially... there should be only ImageCount (from components) after executing.
            PlanogramImageList images = App.MainPageViewModel.ActivePlanController.SourcePlanogram.Images;
            if (images.Count == ImageCount) Assert.Inconclusive(precondition);
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                args.Result = OkResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveProductImagesFromPlanogram.Execute();

            Int32 actual = images.Count;
            Assert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region RemoveComponentImagesFromPlanogram

        [Test]
        public void RemoveComponentImagesFromPlanogram_WhenNoSourcePlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "RemoveComponentImagesFromPlanogram should not be enabled when there is no active planogram.";
            const String disabledMessageExpectation = "RemoveComponentImagesFromPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoActivePlanogram;

            Boolean actual = MainPageCommands.RemoveComponentImagesFromPlanogram.CanExecute();
            String actualMessage = MainPageCommands.RemoveComponentImagesFromPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        //Commented out as doing this forces images to load.
        //[Test]
        //public void RemoveComponentImagesFromPlanogram_WhenNoImagesInPlanogram_ShouldBeDisabled()
        //{
        //    const String disabledExpectation = "RemoveImagesFromPlanogram should not be enabled when there are no images in the active planogram.";
        //    const String disabledMessageExpectation = "RemoveImagesFromPlanogram should display the correct disabled reason when disabled.";
        //    String expectedMessage = Message.DisabledReason_NoImagesInPlanogram;
        //    SetupActivePlanogramForImagesTests();

        //    Boolean actual = MainPageCommands.RemoveComponentImagesFromPlanogram.CanExecute();
        //    String actualMessage = MainPageCommands.RemoveComponentImagesFromPlanogram.DisabledReason;

        //    Assert.IsFalse(actual, disabledExpectation);
        //    Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        //}

        [Test]
        public void RemoveComponentImagesFromPlanogram_WhenNoComponentImagesInPlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "RemoveImagesFromPlanogram should not be enabled when there are no product images in the active planogram.";
            const String disabledMessageExpectation = "RemoveImagesFromPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoComponentImagesInPlanogram;
            SetupActivePlanogramForImagesTests(ImageCount);

            Boolean actual = MainPageCommands.RemoveComponentImagesFromPlanogram.CanExecute();
            String actualMessage = MainPageCommands.RemoveComponentImagesFromPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        [Test]
        public void RemoveComponentImagesFromPlanogram_WhenImagesInPlanogram_ShouldBeEnabled()
        {
            const String expectation = "RemoveComponentImagesFromPlanogram should be enabled when there are product images in the active planogram.";
            SetupActivePlanogramForImagesTests(componentImageCount: ImageCount);

            Boolean actual = MainPageCommands.RemoveComponentImagesFromPlanogram.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void RemoveComponentImagesFromPlanogram_WhenExecuted_ShouldPromptUserConfirmation()
        {
            const String expectation = "Should prompt user for confirmation before removing product images from planogram.";
            Boolean actual = false;
            SetupActivePlanogramForImagesTests(componentImageCount: ImageCount);
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                actual = CheckMessageParameters(args,
                    Message.PromptConfirmation_RemoveImagesFromPlanogram_Header,
                    Message.PromptConfirmation_RemoveImagesFromPlanogram_Description);
                args.Result = CancelResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveComponentImagesFromPlanogram.Execute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void RemoveComponentImagesFromPlanogram_WhenExecuted_ShouldNotRemoveWithoutUserConfirmation()
        {
            const String expectation = "Should cancel removal if user cancels at prompt for confirmation.";
            SetupActivePlanogramForImagesTests(ImageCount, ImageCount);
            //  Store how many images are there at the start... there should be the same after cancelling.
            PlanogramImageList images = App.MainPageViewModel.ActivePlanController.SourcePlanogram.Images;
            Int32 expected = images.Count;
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                args.Result = CancelResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveComponentImagesFromPlanogram.Execute();

            Int32 actual = images.Count;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void RemoveComponentImagesFromPlanogram_WhenExecuted_ShouldRemoveAfterUserConfirmation()
        {
            const String expectation = "Should remove only product images if user accepts at prompt for confirmation.";
            const String precondition = "There need to be product images to start with so that this test can be run.";
            const Int32 expected = ImageCount;
            SetupActivePlanogramForImagesTests(ImageCount, ImageCount);
            //  Assert there are product images to remove initially... there should be only ImageCount (from components) after executing.
            PlanogramImageList images = App.MainPageViewModel.ActivePlanController.SourcePlanogram.Images;
            if (images.Count == ImageCount) Assert.Inconclusive(precondition);
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args =>
            {
                args.Result = OkResultForRemoveImagesPrompt;
            });

            MainPageCommands.RemoveComponentImagesFromPlanogram.Execute();

            Int32 actual = images.Count;
            Assert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region AddImagesToPlanogram

        [Test]
        public void AddImagesToPlanogram_WhenNoSourcePlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "AddImagesToPlanogram should not be enabled when there is no active planogram.";
            const String disabledMessageExpectation = "AddImagesToPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoActivePlanogram;

            Boolean actual = MainPageCommands.AddImagesToPlanogram.CanExecute();
            String actualMessage = MainPageCommands.AddImagesToPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        [Test]
        public void AddImagesToPlanogram_WhenSourcePlanogram_ShouldBeEnabled()
        {
            const String expectation = "AddImagesToPlanogram should be enabled when there is an active planogram.";
            SetupActivePlanogramForImagesTests();

            Boolean actual = MainPageCommands.AddImagesToPlanogram.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region AddProductImagesToPlanogram

        [Test]
        public void AddProductImagesToPlanogram_WhenNoSourcePlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "AddProductImagesToPlanogram should not be enabled when there is no active planogram.";
            const String disabledMessageExpectation = "AddProductImagesToPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoActivePlanogram;

            Boolean actual = MainPageCommands.AddProductImagesToPlanogram.CanExecute();
            String actualMessage = MainPageCommands.AddProductImagesToPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        [Test]
        public void AddProductImagesToPlanogram_WhenSourcePlanogram_ShouldBeEnabled()
        {
            const String expectation = "AddProductImagesToPlanogram should be enabled when there is an active planogram.";
            SetupActivePlanogramForImagesTests();

            Boolean actual = MainPageCommands.AddProductImagesToPlanogram.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void AddProductImagesToPlanogram_WhenExecuted_ShouldShowBusyDialog()
        {
            const String expectation = "AddProductImagesToPlanogram should show a determinate busy dialog while storing images in the planogram.";
            SetupActivePlanogramForImagesTests();
            Boolean actual = false;
            WindowService.AddResponse(NUnitWindowService.ShowDeterminateBusyMethod, args =>
            {
                actual = CheckBusyModalParameters(args,
                    String.Format(Message.StoreExternalImagesWork_ModalBusy_Header, Environment.NewLine),
                    Message.StoreExternalImagesWork_ModalBusy_Description);
            });
            WindowService.AddNullResponse(NUnitWindowService.CloseCurrentBusyMethod);
            WindowService.AddResponse(NUnitWindowService.ShowMessageMethod, args => args.Result = ModalMessageResult.Button1);
            PlanogramImagesHelper.RegisterRealImageProvider(Mock.Of<IRealImageProvider>());

            MainPageCommands.AddImagesToPlanogram.Execute();

            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region AddComponentImagesToPlanogram

        [Test]
        public void AddComponentImagesToPlanogram_WhenNoSourcePlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "AddComponentImagesToPlanogram should not be enabled when there is no active planogram.";
            const String disabledMessageExpectation = "AddComponentImagesToPlanogram should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoActivePlanogram;

            Boolean actual = MainPageCommands.AddComponentImagesToPlanogram.CanExecute();
            String actualMessage = MainPageCommands.AddComponentImagesToPlanogram.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        [Test]
        public void AddComponentImagesToPlanogram_WhenSourcePlanogram_ShouldBeEnabled()
        {
            const String expectation = "AddComponentImagesToPlanogram should be enabled when there is an active planogram.";
            SetupActivePlanogramForImagesTests();

            Boolean actual = MainPageCommands.AddComponentImagesToPlanogram.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region NewPlanogramCompareView

        [Test]
        public void NewPlanogramCompareView_WhenMissingActivePlanogram_ShouldBeDisabled()
        {
            const String disabledExpectation = "NewPlanogramCompareView should not be enabled when there is no active planogram.";
            const String disabledMessageExpectation = "NewPlanogramCompareView should display the correct disabled reason when disabled.";
            String expectedMessage = Message.DisabledReason_NoActivePlanogram;

            Boolean actual = MainPageCommands.NewPlanogramCompareView.CanExecute();
            String actualMessage = MainPageCommands.NewPlanogramCompareView.DisabledReason;

            Assert.IsFalse(actual, disabledExpectation);
            Assert.AreEqual(expectedMessage, actualMessage, disabledMessageExpectation);
        }

        [Test]
        public void NewPlanogramCompareView_WhenActivePlanogram_ShouldBeEnabled()
        {
            const String expectation = "NewPlanogramCompareView should be enabled when there is an active planogram.";
            Package package = "MainCommandsTestsPackage".CreatePackage();
            App.MainPageViewModel.ActivePlanController =
                new PlanControllerViewModel(new PlanogramView(package.AddPlanogram(), PackageViewModel.NewPackageViewModel(package)));

            Boolean actual = MainPageCommands.NewPlanogramCompareView.CanExecute();

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void NewPlanogramCompareView_WhenExecuted_ShouldCreatePlanDocument()
        {
            const String expectation = "NewPlanogramCompareView should be create the Plan Document when executed.";
            SetUpPlaceholderActivePlanogram();

            MainPageCommands.NewPlanogramCompareView.Execute();
            Boolean currentPlanDocumentIsPlanogramCompare = App.MainPageViewModel.ActivePlanController.SelectedPlanDocument.DocumentType == DocumentType.PlanogramComparison;

            Assert.IsTrue(currentPlanDocumentIsPlanogramCompare, expectation);
        }

        #endregion

        #region UpdatePlanogramFromProductAttibute

        [Test]
        public void UpdatePlanogramFromProductAttributeIsDisabledWhenNoOpenPlans()
        {
            const String disabledExpectation = "The command should have been disabled.";
            const String disabledMessageExpectation = "Unexpected disabled reason.";
            String expected = Message.DisabledReason_NoActivePlanogram;

            Boolean canExecute = MainPageCommands.UpdatePlanogramFromProductAttribute.CanExecute();
            String disabledReason = MainPageCommands.UpdatePlanogramFromProductAttribute.DisabledReason;

            Assert.IsFalse(canExecute, disabledExpectation);
            Assert.AreEqual(expected, disabledReason, disabledMessageExpectation);
        }

        [Test]
        public void UpdatePlanogramFromProductAttributeIsEnabledWhenOpenPlans()
        {
            const String expectation = "The command should have been enabled.";
            Package package = "MainCommandsTestsPackage".CreatePackage();
            App.MainPageViewModel.ActivePlanController =
                new PlanControllerViewModel(new PlanogramView(package.AddPlanogram(), PackageViewModel.NewPackageViewModel(package)));

            Boolean canExecute = MainPageCommands.UpdatePlanogramFromProductAttribute.CanExecute();

            Assert.IsTrue(canExecute, expectation);
        }

        #endregion

        #region UpdateAllPlanogramsFromProductAttibute

        [Test]
        public void UpdateAllPlanogramsFromProductAttributeIsDisabledWhenNoOpenPlans()
        {
            const String disabledExpectation = "The command should have been disabled.";
            const String disabledMessageExpectation = "Unexpected disabled reason.";
            String expected = Message.DisabledReason_NoActivePlanogram;

            Boolean canExecute = MainPageCommands.UpdateAllPlanogramsFromProductAttribute.CanExecute();
            String disabledReason = MainPageCommands.UpdateAllPlanogramsFromProductAttribute.DisabledReason;

            Assert.IsFalse(canExecute, disabledExpectation);
            Assert.AreEqual(expected, disabledReason, disabledMessageExpectation);
        }

        [Test]
        public void UpdateAllPlanogramsFromProductAttributeIsEnabledWhenOpenPlans()
        {
            const String expectation = "The command should have been enabled.";
            SetUpPlaceholderActivePlanogram();

            Boolean canExecute = MainPageCommands.UpdateAllPlanogramsFromProductAttribute.CanExecute();

            Assert.IsTrue(canExecute, expectation);
        }

        #endregion

        #region Static Private Helpers

        /// <summary>
        ///     Checks that a message dialog has the expected values.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="expectedHeader"></param>
        /// <param name="expectedDescription"></param>
        /// <returns></returns>
        private static Boolean CheckMessageParameters(NUnitWindowService.NUnitWindowServiceArgs args,
            String expectedHeader = null, String expectedDescription = null)
        {
            Object[] parameters = args.WindowParameters;
            String header = (parameters.Length > 1)
                                ? parameters[1] as String
                                : null;
            String description = (parameters.Length > 2)
                                     ? parameters[2] as String
                                     : null;
            return (expectedHeader == null || String.Equals(header, expectedHeader)) &&
                   (expectedDescription == null || String.Equals(description, expectedDescription));
        }

        /// <summary>
        ///     Checks that a busy modal dialog has the expected values.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="expectedHeader"></param>
        /// <param name="expectedDescription"></param>
        /// <returns></returns>
        private static Boolean CheckBusyModalParameters(NUnitWindowService.NUnitWindowServiceArgs args,
            String expectedHeader = null, String expectedDescription = null)
        {
            Object[] parameters = args.WindowParameters;
            String header = (parameters.Length > 0)
                                ? parameters[0] as String
                                : null;
            String description = (parameters.Length > 1)
                                     ? parameters[1] as String
                                     : null;
            return (expectedHeader == null || String.Equals(header, expectedHeader)) &&
                   (expectedDescription == null || String.Equals(description, expectedDescription));
        }

        /// <summary>
        ///     Makes sure there is a valid active planogram for the planogram images tests.
        /// </summary>
        /// <param name="productImageCount">The desired number of product images to start with. Zero by default.</param>
        /// <param name="componentImageCount">The desired number of component images to start with. Zero by default.</param>
        private static void SetupActivePlanogramForImagesTests(Int32 productImageCount = 0, Int32 componentImageCount = 0)
        {
            Package package = "MainCommandsTestsPackage".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            planogram.Fixtures.Add(fixture);
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture));

            //  Add the expected product images.
            for (Int32 i = 0; i < productImageCount; i++)
            {
                PlanogramImage planogramImage = PlanogramImage.NewPlanogramImage();
                planogram.Images.Add(planogramImage);
                PlanogramProduct planogramProduct = PlanogramProduct.NewPlanogramProduct();
                planogram.Products.Add(planogramProduct);
                planogramProduct.PlanogramImageIdFront = planogramImage.Id;
            }

            // Add the expected component images.
            for (Int32 i = 0; i < componentImageCount; i++)
            {
                PlanogramImage planogramImage = PlanogramImage.NewPlanogramImage();
                planogram.Images.Add(planogramImage);
                PlanogramComponent planogramComponent = PlanogramComponent.NewPlanogramComponent();
                planogram.Components.Add(planogramComponent);
                PlanogramSubComponent planogramSubComponent = PlanogramSubComponent.NewPlanogramSubComponent();
                fixture.Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogramComponent));
                planogramComponent.SubComponents.Add(planogramSubComponent);
                planogramSubComponent.ImageIdFront = planogramImage.Id;
            }
            App.MainPageViewModel.ActivePlanController =
                new PlanControllerViewModel(new PlanogramView(planogram, PackageViewModel.NewPackageViewModel(package)));
        }

        /// <summary>
        ///     Just setup some planograms so that they are available.
        /// </summary>
        /// <param name="planCount"></param>
        /// <remarks>Some window responses are set to create each plan. Set any other necessary requests AFTER calling this method.</remarks>
        private void SetUpPlaceholderActivePlanogram(Int32 planCount = 1)
        {
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      p => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            for (var currentPlan = 0; currentPlan < planCount; currentPlan++)
            {
                PlanControllerViewModel planController = App.MainPageViewModel.CreateNewPlanogram();
                if (currentPlan == 0)
                    App.MainPageViewModel.ActivePlanController = planController;
            }

            WindowService.ClearResponseQueue();
        }

        #endregion
    }
}