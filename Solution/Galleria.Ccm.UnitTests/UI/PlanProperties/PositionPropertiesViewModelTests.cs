﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27643 : L.Ineson
//  Created.
#endregion
#region Version History: (CCM 8.1.1)
// V8-28688 : A.Probyn
//  Updated reference to PositionPropertiesViewModel to take a new constructor depending on where it is called from.
#endregion
#endregion

using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.UI.PlanProperties
{
    [TestFixture]
    public sealed class PositionPropertiesViewModelTests : TestBase
    {
        [Test]
        public void UndoRedo_CappingChange()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            planController.SourcePlanogram.BeginUpdate();
            TestDataHelper.CreatePlanCreweSparklingWater(planController.SourcePlanogram.Model);
            planController.SourcePlanogram.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            planController.SourcePlanogram.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            planController.SourcePlanogram.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            planController.SourcePlanogram.EndUpdate();

            //select a position
            var p1 = planController.SourcePlanogram.EnumerateAllPositions().First();
            planController.SelectedPlanItems.SetSelection(p1);

            //"open" the properties window 
            using (PositionPropertiesViewModel viewModel = new PositionPropertiesViewModel(planController.SourcePlanogram, planController.SelectedPlanItems, false))
            {
                //set top capping through the viewmodel.
                viewModel.PositionContext.FacingsYHigh = 1;
                viewModel.PositionContext.FacingsYWide = 1;
                viewModel.PositionContext.FacingsYDeep = 1;

                //commit the changes
                viewModel.OKCommand.Execute();
            }

            //check
            Assert.AreEqual(1, p1.FacingsYHigh);
            Assert.AreEqual(1, p1.FacingsYWide);
            Assert.AreEqual(1, p1.FacingsYDeep);

            //"reopen"  the properties window 
            using (PositionPropertiesViewModel viewModel = new PositionPropertiesViewModel(planController.SourcePlanogram, planController.SelectedPlanItems, false))
            {
                //remove the top capping
                viewModel.PositionContext.FacingsYHigh = 0;
                viewModel.PositionContext.FacingsYWide = 0;
                viewModel.PositionContext.FacingsYDeep = 0;

                //commit the changes
                viewModel.OKCommand.Execute();
            }

            //check
            Assert.AreEqual(0, p1.FacingsYHigh);
            Assert.AreEqual(0, p1.FacingsYWide);
            Assert.AreEqual(0, p1.FacingsYDeep);

            //undo the changes
            planController.SourcePlanogram.Undo();

            //check
            Assert.AreEqual(1, p1.FacingsYHigh);
            Assert.AreEqual(1, p1.FacingsYWide);
            Assert.AreEqual(1, p1.FacingsYDeep);

        }

    }
}
