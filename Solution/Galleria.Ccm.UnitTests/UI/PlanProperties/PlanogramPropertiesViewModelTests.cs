﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24953 : L.Hodson
//  Created
// CCM-25880 : N.Haywood
//  Added SelectMerchGroupCommand
// V8-26338 : A.Kuszyk
//  Added tests for Metric commands.
// V8-28444 : M.Shelley
//  Fix broken unit tests "Command_NewMetricCanExectute_ReturnsTrue_WhenMetricCountLessThan20" 
//  and "Command_MetricSaveExecuted_AddsSelectedMetricToList"
#endregion

#region Version History: (CCM 8.02)
// V8-29004 : L.Luong
//  Added test for MoveFixtureRight and MoveFixtureLeft commands
#endregion

#region Version History: (CCM 8.1.1)
// V8-29290 : I.George
//  Added ClusterChangeWarning tests
#endregion

#region Version History: (CCM 8.3.0)
// V8-29527 : M.Shelley
//  Modified tests to allow for the multiple bay editing changes.
// V8-32143 : A.Silva
//  Added test for duplicate X coordinates when moving bays left or right.

#endregion

#endregion

using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using System.Collections.Generic;
using System.Dynamic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.UnitTests.UI.PlanProperties
{
    [TestFixture]
    public sealed class PlanogramPropertiesViewModelTests : TestBase
    {
        private int _entityId;
        public override void Setup()
        {
            base.Setup();
            _entityId = TestDataHelper.InsertEntityDtos(this.DalFactory, 1)[0].Id;
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 1, _entityId);
            List<ClusterSchemeDto> clusterSchemeDto = TestDataHelper.InsertClusterSchemeDtos(this.DalFactory, _entityId, 1);
            List<ClusterDto> clusterDtos = TestDataHelper.InsertClusterDtos(this.DalFactory, clusterSchemeDto, 1);
            List<ClusterLocationDto> clusterLocationDtos = TestDataHelper.InsertClusterLocationDtos(this.DalFactory, clusterDtos, locationDtos);
        }

        #region Properties

        [Test]
        public void Property_IsBackboardRequired()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            //true
            Assert.IsNotNull(viewModel.MultiFixtureView);
            
            var selectedItem = viewModel.MultiFixtureView.Items.FirstOrDefault();
            Assert.IsTrue(selectedItem != null);
            selectedItem.IsSelected = true;

            viewModel.MultiFixtureView.IsBackboardRequired = true;

            //Assert.AreEqual(planController.SourcePlanogram, viewModel.MultiFixtureView.Backboard.Planogram);
            Assert.IsNotNull(planController.SourcePlanogram.EnumerateAllComponents().FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard));

            //false
            viewModel.MultiFixtureView.IsBackboardRequired = false;

            var allComponents = planController.SourcePlanogram.EnumerateAllComponents().ToList();
            var backboardComponent = allComponents.FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);

            Assert.IsNull(planController.SourcePlanogram.EnumerateAllComponents().FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard));
        }

        [Test]
        public void Property_IsBaseRequired()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();

            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            //true
            viewModel.IsBaseRequired = true;

            Assert.IsNotNull(viewModel.Base);
            Assert.AreEqual(planController.SourcePlanogram, viewModel.Base.Planogram);
            Assert.IsNotNull(planController.SourcePlanogram.EnumerateAllComponents().FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Base));

            //false
            viewModel.IsBaseRequired = false;
            Assert.IsNull(viewModel.Base);

            Assert.IsNull(planController.SourcePlanogram.EnumerateAllComponents().FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Base));
        }

        #endregion

        #region Commands

        [Test]
        public void AddFixtureAfterCommand_Executed()
        {
            const String nameFixtureOne = "FX1";
            const String nameFixtureTwo = "FX2";
            const Int32 indexFixtureOne = 1;
            const Int32 indexFixtureTwo = 2;
            const Int32 indexFixtureThree = 3;
            var fixtureCount = 0;

            PlanogramPropertiesViewModel viewModel = CreateTestModel();
            fixtureCount++;

            ReadOnlyBulkObservableCollection<PlanogramFixtureView> fixtureViews = viewModel.Fixtures;
            Assert.AreEqual(fixtureCount, fixtureViews.Count);
            PlanogramFixtureView fixtureOne = fixtureViews.First();
            Assert.AreEqual(fixtureOne, viewModel.SelectedFixture);

            fixtureOne.Name = nameFixtureOne;

            // add a fixture
            fixtureOne.IsSelected = true;
            viewModel.AddFixtureAfterCommand.Execute();
            fixtureCount++;
            if (fixtureViews.Count < fixtureCount) Assert.Inconclusive("There should be at least {0} fixtures to run this test", fixtureCount);

            Assert.AreEqual(fixtureCount, fixtureViews.Count);
            PlanogramFixtureView fixtureTwo = fixtureViews.Skip(1).First();
            Assert.AreEqual(fixtureTwo, viewModel.SelectedFixture);

            Assert.AreEqual(nameFixtureOne, fixtureOne.Name);
            Assert.AreEqual(indexFixtureOne, fixtureOne.BaySequenceNumber);
            Assert.AreEqual(indexFixtureTwo, fixtureTwo.BaySequenceNumber);

            fixtureTwo.Name = nameFixtureTwo;

            //select fixture 1 again and add - should go between
            viewModel.SelectedFixture = fixtureOne;
            fixtureOne.IsSelected = true;
            fixtureTwo.IsSelected = false;
            viewModel.AddFixtureAfterCommand.Execute();
            fixtureCount++;
            if (fixtureViews.Count < fixtureCount) Assert.Inconclusive("There should be at least {0} fixtures to run this test", fixtureCount);

            Assert.AreEqual(fixtureCount, fixtureViews.Count);
            PlanogramFixtureView fixtureThree = fixtureViews.Skip(2).First();
            Assert.AreEqual(fixtureThree, viewModel.SelectedFixture);

            Assert.AreEqual(indexFixtureOne, fixtureOne.BaySequenceNumber);
            Assert.AreEqual(indexFixtureThree, fixtureTwo.BaySequenceNumber);
            Assert.AreEqual(indexFixtureTwo, fixtureThree.BaySequenceNumber);
        }

        [Test]
        public void AddFixtureAfterCommand_PlanWidthGetsUpdated()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.AreEqual(1, plan.Fixtures.Count);
            Assert.AreEqual(plan.Fixtures[0], viewModel.SelectedFixture);
            plan.Fixtures.First().IsSelected = true;

            Single planWidth = plan.Width;

            //add a fixture
            viewModel.AddFixtureAfterCommand.Execute();

            Assert.Greater(plan.Width, planWidth, "Plan width should have increased.");
        }

        [Test]
        public void AddFixtureBeforeCommand_Executed()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.AreEqual(1, plan.Fixtures.Count);
            Assert.AreEqual(plan.Fixtures[0], viewModel.SelectedFixture);
            plan.Fixtures.First().IsSelected = true;

            viewModel.AddFixtureBeforeCommand.Execute();

            Assert.AreEqual(2, plan.Fixtures[0].BaySequenceNumber);
            Assert.AreEqual(1, plan.Fixtures[1].BaySequenceNumber);
        }

        [Test]
        public void AddFixtureBeforeCommand_PlanWidthGetsUpdated()
        {
            var mainPageView = App.MainPageViewModel;

            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.AreEqual(1, plan.Fixtures.Count);
            Assert.AreEqual(plan.Fixtures[0], viewModel.SelectedFixture);
            plan.Fixtures.First().IsSelected = true;

            Single planWidth = plan.Width;

            //add a fixture
            viewModel.AddFixtureBeforeCommand.Execute();

            Assert.Greater(plan.Width, planWidth, "Plan width should have increased.");
        }

        [Test]
        public void DeleteFixtureCommand_DisabledWhenOnlyOnFixture()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.AreEqual(1, plan.Fixtures.Count);
            Assert.AreEqual(plan.Fixtures[0], viewModel.SelectedFixture);
            plan.Fixtures.First().IsSelected = true;

            Assert.IsFalse(viewModel.DeleteFixtureCommand.CanExecute());


            //add a fixture
            viewModel.AddFixtureAfterCommand.Execute();

            Assert.IsTrue(viewModel.DeleteFixtureCommand.CanExecute());
        }

        [Test]
        public void DeleteFixtureCommand_Executed()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.AreEqual(1, plan.Fixtures.Count);
            Assert.AreEqual(plan.Fixtures[0], viewModel.SelectedFixture);
            plan.Fixtures.First().IsSelected = true;

            //add a fixture
            viewModel.AddFixtureAfterCommand.Execute();

            Assert.AreEqual(2, plan.Fixtures.Count);
            PlanogramFixtureView fx1 = plan.Fixtures[0];

            //delete
            viewModel.DeleteFixtureCommand.Execute();

            Assert.AreEqual(1, plan.Fixtures.Count);
            Assert.IsTrue(plan.Fixtures.Contains(fx1));
        }

        [Test]
        public void OKCommand_Executed()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            //select the first bay
            plan.Fixtures.First().IsSelected = true;

            viewModel.AddFixtureAfterCommand.Execute();

            viewModel.OKCommand.Execute();

            Assert.AreEqual(true, viewModel.DialogResult);
            Assert.AreEqual(2, plan.Fixtures.Count);
        }

        [Test]
        public void CancelCommand_Executed()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            viewModel.AddFixtureAfterCommand.Execute();

            viewModel.CancelCommand.Execute();

            Assert.AreEqual(false, viewModel.DialogResult);
            Assert.AreEqual(1, plan.Fixtures.Count);
        }

        [Test]
        public void SelectMerchGroupCommand_Executed()
        {
            throw new InconclusiveException("TODO");
        }

        [Test]
        public void NewMetricCommand_CanExecuteFalseWhenMetricCountMoreThan19()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            for (Byte i = 1; i <= 20; i++)
            {
                viewModel.PerformanceMetrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(i));
            }

            Assert.IsFalse(viewModel.NewMetricCommand.CanExecute());
        }

        [Test]
        public void NewMetricCommand_CanExecuteTrueWhenMetricCountLessThan20()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            for (byte i = (byte) (plan.PerformanceMetrics.Count() + 1); i < 20; i++)
            {
                var metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(i);
                metric.Name = "metric" + i;
                viewModel.PerformanceMetrics.Add(metric);
            }

            Assert.IsTrue(viewModel.NewMetricCommand.CanExecute());
        }

        [Test]
        public void ViewMetricCommand_CanExecuteFalseWhenNoMetricSelected()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.IsFalse(viewModel.ViewMetricCommand.CanExecute());
        }

        [Test]
        public void ViewMetricCommand_CanExecuteTrueWhenAMetricSelected()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            viewModel.SelectedPerformanceMetric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric();

            Assert.IsTrue(viewModel.ViewMetricCommand.CanExecute());
        }

        [Test]
        public void RemoveMetricCommand_CanExecuteFalseWhenNoMetricSelected()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.IsFalse(viewModel.RemoveMetricCommand.CanExecute());
        }

        [Test]
        public void RemoveMetricCommand_CanExecuteTrueWhenAMetricSelected()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            viewModel.SelectedPerformanceMetric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric();

            Assert.IsTrue(viewModel.RemoveMetricCommand.CanExecute());
        }

        [Test]
        public void RemoveMetricCommand_RemovesMetric()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);
            var metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric();
            viewModel.PerformanceMetrics.Add(metric);
            viewModel.SelectedPerformanceMetric = metric;

            viewModel.RemoveMetricCommand.Execute();

            CollectionAssert.DoesNotContain(viewModel.PerformanceMetrics, metric);
        }

        [Test]
        public void MetricSaveCommand_CanExecuteFalseWhenMetricIsInvalid()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);
            viewModel.SelectedPerformanceMetric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric();

            Assert.IsFalse(viewModel.MetricSaveCommand.CanExecute());
        }

        [Test]
        public void MetricSaveCommand_CanExecuteTrueWhenMetricIsValid()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);
            var metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric.Name = "Name";
            viewModel.SelectedPerformanceMetric = metric;

            Assert.IsTrue(viewModel.MetricSaveCommand.CanExecute());
        }

        [Test]
        public void MetricSaveCommand_Executed_AddsSelectedMetricToList()
        {
            String metricName = "New Metric Name";

            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);
         
            viewModel.NewMetricCommand.Execute();

            viewModel.SelectedPerformanceMetric.Name = metricName;

            viewModel.MetricSaveCommand.Execute();

            // Check the new metric was added 
            var newMetic = viewModel.PerformanceMetrics.Where(x => x.Name.Equals(metricName));

            Assert.IsTrue(newMetic.Count() == 1, "The new metric has not been saved.");
        }

        #region Fixture Movement

        [Test]
        public void MoveFixtureLeftCommand_Executed()
        {
            PlanogramPropertiesViewModel testModel = CreateTestModel();
            ReadOnlyBulkObservableCollection<PlanogramFixtureView> fixtureViews = testModel.Fixtures;

            // add a fixture
            PlanogramFixtureView fixtureOne = fixtureViews.First();
            fixtureOne.IsSelected = true;
            testModel.AddFixtureAfterCommand.Execute();
            if (fixtureViews.Count < 2) Assert.Inconclusive("There should be at least two fixtures to run this test");

            // select new fixture
            PlanogramFixtureView fixtureTwo = fixtureViews.Skip(1).First();
            testModel.SelectedFixture = fixtureTwo;

            // expected values
            Single expectedMovedXValue = fixtureOne.X;
            Int16 expectedMovedSequenceValue = fixtureOne.BaySequenceNumber;
            Single expectedReplacedXValue = testModel.SelectedFixture.X;
            Int16 expectedReplacedSequenceValue = testModel.SelectedFixture.BaySequenceNumber;

            // execute command
            testModel.MoveFixtureLeftCommand.Execute();

            // check values are correct
            Assert.AreEqual(expectedMovedXValue, testModel.SelectedFixture.X);
            Assert.AreEqual(expectedMovedSequenceValue, testModel.SelectedFixture.BaySequenceNumber);
            Assert.AreEqual(expectedReplacedXValue, fixtureOne.X);
            Assert.AreEqual(expectedReplacedSequenceValue, fixtureOne.BaySequenceNumber);
        }

        [Test]
        public void MoveFixtureRightCommand_Executed()
        {
            PlanogramPropertiesViewModel testModel = CreateTestModel();
            ReadOnlyBulkObservableCollection<PlanogramFixtureView> fixtureViews = testModel.Fixtures;

            // add a fixture
            PlanogramFixtureView fixtureOne = fixtureViews.First();
            fixtureOne.IsSelected = true;
            testModel.AddFixtureAfterCommand.Execute();
            if (fixtureViews.Count < 2) Assert.Inconclusive("There should be at least two fixtures to run this test");

            // select new fixture
            testModel.SelectedFixture = fixtureOne;

            // expected values
            PlanogramFixtureView fixtureTwo = fixtureViews.Skip(1).First();
            Single expectedMovedXValue = fixtureTwo.X;
            Int16 expectedMovedSequenceValue = fixtureTwo.BaySequenceNumber;
            Single expectedReplacedXValue = testModel.SelectedFixture.X;
            Int16 expectedReplacedSequenceValue = testModel.SelectedFixture.BaySequenceNumber;

            // execute command
            testModel.MoveFixtureRightCommand.Execute();

            // check values are correct
            Assert.AreEqual(expectedMovedXValue, testModel.SelectedFixture.X);
            Assert.AreEqual(expectedMovedSequenceValue, testModel.SelectedFixture.BaySequenceNumber);
            Assert.AreEqual(expectedReplacedXValue, fixtureTwo.X);
            Assert.AreEqual(expectedReplacedSequenceValue, fixtureTwo.BaySequenceNumber);

        }

        [Test]
        public void FixtureMovesLeftWhenDuplicateXCoordinate()
        {
            PlanogramPropertiesViewModel testModel = CreateTestModel();
            ReadOnlyBulkObservableCollection<PlanogramFixtureView> fixtureViews = testModel.Fixtures;
            PlanogramFixtureView fixtureOne = fixtureViews.First();
            fixtureOne.IsSelected = true;
            testModel.AddFixtureAfterCommand.Execute();
            if (fixtureViews.Count < 2) Assert.Inconclusive("There should be at least two fixtures to run this test");
            PlanogramFixtureView fixtureTwo = fixtureViews.Skip(1).First();
            fixtureTwo.X = fixtureOne.X;
            fixtureOne.IsSelected = false;
            fixtureTwo.IsSelected = true;

            TestDelegate testCode = () => testModel.MoveFixtureLeftCommand.Execute();

            Assert.DoesNotThrow(testCode);
            Assert.AreEqual(2, fixtureOne.BaySequenceNumber);
            Assert.AreEqual(1, fixtureTwo.BaySequenceNumber);
        }

        [Test]
        public void FixtureMoveRightWhenDuplicateXCoordinate()
        {
            PlanogramPropertiesViewModel testModel = CreateTestModel();
            ReadOnlyBulkObservableCollection<PlanogramFixtureView> fixtureViews = testModel.Fixtures;
            PlanogramFixtureView fixtureOne = fixtureViews.First();
            fixtureOne.IsSelected = true;
            testModel.AddFixtureAfterCommand.Execute();
            if (fixtureViews.Count < 2) Assert.Inconclusive("There should be at least two fixtures to run this test");
            PlanogramFixtureView fixtureTwo = fixtureViews.Skip(1).First();
            fixtureTwo.X = fixtureOne.X;
            fixtureOne.IsSelected = true;
            fixtureTwo.IsSelected = false;

            TestDelegate testCode = () => testModel.MoveFixtureRightCommand.Execute();

            Assert.DoesNotThrow(testCode);
            Assert.AreEqual(2, fixtureOne.BaySequenceNumber);
            Assert.AreEqual(1, fixtureTwo.BaySequenceNumber);
        }

        #endregion

        [Test]
        public void SelectClusterSchemeCommand_NoLocationSet()
        {
            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.IsFalse(viewModel.SelectClusterSchemeCommand.CanExecute());
            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            String expectedSchemeName = null;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as ClusterSchemeSelectionViewModel;
                testViewModel.SelectedClusterSchemeInfo = testViewModel.MasterClusterSchemeInfoList.FirstOrDefault();
                expectedSchemeName = testViewModel.SelectedClusterSchemeInfo.Name;
                testViewModel.OKCommand.Execute();
            });

            viewModel.SelectClusterSchemeCommand.Execute();
            Assert.IsNotNull(expectedSchemeName, "expected name did not get set");
            Assert.AreEqual(expectedSchemeName, viewModel.Planogram.ClusterSchemeName, "incorrect name set"); 

        }

        [Test]
        public void selectClusterSchemeCommand_WhenLocationSet()
        {
            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.IsFalse(viewModel.SelectLocationCommand.CanExecute(),"No repository connection should have been made");
            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            viewModel.Planogram.LocationCode = "loccode0";
            String locationCode = viewModel.Planogram.LocationCode;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as ClusterSchemeSelectionViewModel;
                testViewModel.SelectedClusterSchemeInfo = testViewModel.MasterClusterSchemeInfoList.FirstOrDefault();
                testViewModel.OKCommand.Execute();
            });

            viewModel.SelectClusterSchemeCommand.Execute();
            Assert.IsNotNullOrEmpty(locationCode, "location code did not get set");
            Assert.IsNotNullOrEmpty(viewModel.Planogram.ClusterName, "incorrect name set"); 
        }

        [Test]
        public void SelectClusterCommand_WhenClusterSchemeExists()
        {
            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);


            Assert.IsFalse(viewModel.SelectClusterCommand.CanExecute(), "No repository connection should have been made");
             String ClusterName = null;
             this.SetMockRepositoryConnection();
             App.ViewState.EntityId = _entityId;
             Assert.IsFalse(viewModel.SelectClusterCommand.CanExecute(), "Button should still be disabled because to clusterSchemeName is set");
             String clusterSchemeName = "scheme0";
             viewModel.Planogram.ClusterSchemeName = clusterSchemeName;
            Assert.IsTrue(viewModel.SelectClusterCommand.CanExecute());
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as LocationClusterSelectorViewModel;
                testViewModel.SelectedCluster = testViewModel.SelectedClusterScheme.Clusters.FirstOrDefault();
                ClusterName = testViewModel.SelectedCluster.Name;
                testViewModel.OKCommand.Execute();
            });
            viewModel.SelectClusterCommand.Execute();

            Assert.IsNotNull(ClusterName, "The name should not be null");
            Assert.AreEqual(ClusterName, viewModel.Planogram.ClusterName, "The name should have been set");
        }

        [Test]
        public void SelectClusterCommand_WhenNoClusterSchemeExist()
        {

            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);


            Assert.IsFalse(viewModel.SelectClusterCommand.CanExecute(),"No repository connection should have been made");
            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            Assert.IsFalse(viewModel.SelectClusterCommand.CanExecute(), "Button should be disabled because no scheme name is set");
            String clusterSchemeName = "clusterScheme";// set a name that doesn't exist in the list
            viewModel.Planogram.ClusterSchemeName = clusterSchemeName;
            Assert.IsTrue(viewModel.SelectClusterCommand.CanExecute());
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as LocationClusterSelectorViewModel;
                Assert.IsFalse(testViewModel.OKCommand.CanExecute(), "this command should be disabled");
                testViewModel.CancelCommand.Execute();
            });
            viewModel.SelectClusterCommand.Execute();
            Assert.IsNullOrEmpty(viewModel.Planogram.ClusterName, "The name should not have been set");
        }

        [Test]
        public void SelectedLocationCommand()
        {

            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.IsFalse(viewModel.SelectLocationCommand.CanExecute(), "No repository connection should have been made");
            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            Assert.IsTrue(viewModel.SelectLocationCommand.CanExecute(), "Should be connected to a repository");

            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
                {
                     Object firstParameter = a.WindowParameters.First();
                     var testViewModel = firstParameter as LocationSelectionViewModel;
                     var location = testViewModel.SelectedAvailableLocations.FirstOrDefault();
                });
        }

        [Test]
        public void DonotShowClusterChangeWarning_WhenNoRepositoryConnection()
        {
            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.IsFalse(viewModel.SelectLocationCommand.CanExecute(), "No repository connection should have been made");
            String LocationCode = "2";
            String ClusterSchemeName = "ClusterScheme1";
            String ClusterName = "Cluster1";

            viewModel.Planogram.LocationCode = LocationCode;
            viewModel.Planogram.ClusterSchemeName = ClusterSchemeName;
            viewModel.Planogram.ClusterName = ClusterName;

            viewModel.ShowClusterChangeWarning();
            Assert.IsFalse(viewModel.IsShowWarning);
        }

        [Test]
        public void ShowClusterChangeWarning_WhenRepositoryIsConnected_ClusterSchemeClusterMismatch()
        {
            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            Assert.IsTrue(viewModel.SelectLocationCommand.CanExecute(), "Should be connected to a repository");
            String ClusterName = "Cluster2";

            //set the cluster scheme
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as ClusterSchemeSelectionViewModel;
                testViewModel.SelectedClusterSchemeInfo = testViewModel.MasterClusterSchemeInfoList.FirstOrDefault();
                testViewModel.OKCommand.Execute();
            });
            viewModel.SelectClusterSchemeCommand.Execute();

            viewModel.Planogram.ClusterName = ClusterName;
            viewModel.ShowClusterChangeWarning();
            Assert.IsTrue(viewModel.IsShowWarning);
            Assert.AreEqual("Cluster does not exist in the selected Cluster Scheme", viewModel.TooltipWarningMessage);
        }

        [Test]
        public void ShowClusterChangeWarning_WhenRepositoryIsConnected_ClusterSchemeClusterMatch()
        {
            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            Assert.IsTrue(viewModel.SelectLocationCommand.CanExecute(), "Should be connected to a repository");
            String ClusterName;

            //set clusterScheme
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as ClusterSchemeSelectionViewModel;
                testViewModel.SelectedClusterSchemeInfo = testViewModel.MasterClusterSchemeInfoList.FirstOrDefault();
                testViewModel.OKCommand.Execute();
            });

            viewModel.SelectClusterSchemeCommand.Execute();

            //set cluster
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as LocationClusterSelectorViewModel;
                testViewModel.SelectedCluster = testViewModel.SelectedClusterScheme.Clusters.FirstOrDefault();
                ClusterName = testViewModel.SelectedCluster.Name;
                testViewModel.OKCommand.Execute();
            });
            viewModel.SelectClusterCommand.Execute();
            Assert.IsFalse(viewModel.IsShowWarning);
            
        }

        [Test]
        public void ShowClusterChangeWarning_WhenRepositoryIsConnected_LocationCodeMismatch()
        {
            // create window
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            Assert.IsTrue(viewModel.SelectLocationCommand.CanExecute(), "Should be connected to a repository");
            String ClusterName;
            String LocationCode ="1";

            //set cluster scheme
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as ClusterSchemeSelectionViewModel;
                testViewModel.SelectedClusterSchemeInfo = testViewModel.MasterClusterSchemeInfoList.FirstOrDefault();
                testViewModel.OKCommand.Execute();
            });

            viewModel.SelectClusterSchemeCommand.Execute();

            //set cluster
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as LocationClusterSelectorViewModel;
                testViewModel.SelectedCluster = testViewModel.SelectedClusterScheme.Clusters.FirstOrDefault();
                ClusterName = testViewModel.SelectedCluster.Name;
                testViewModel.OKCommand.Execute();
            });
            viewModel.SelectClusterCommand.Execute();

            viewModel.Planogram.LocationCode = LocationCode;
            viewModel.ShowClusterChangeWarning();
           
            Assert.IsTrue(viewModel.IsShowWarning);
            Assert.AreEqual("Cluster does not have a matching Location Code", viewModel.TooltipWarningMessage);
        }

        [Test]
        public void ShowClusterChangeWarning_WhenRepositoryIsConnected_LocationCodeMatch()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            this.SetMockRepositoryConnection();
            App.ViewState.EntityId = _entityId;
            Assert.IsTrue(viewModel.SelectLocationCommand.CanExecute(), "Should be connected to a repository");
            String ClusterName;

            //set location code
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as LocationSelectionViewModel;
                var location = testViewModel.SelectedAvailableLocations.FirstOrDefault();
            });
            viewModel.SelectLocationCommand.Execute();

            //set clusterScheme name
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as ClusterSchemeSelectionViewModel;
                testViewModel.SelectedClusterSchemeInfo = testViewModel.MasterClusterSchemeInfoList.FirstOrDefault();
                testViewModel.OKCommand.Execute();
            });

            viewModel.SelectClusterSchemeCommand.Execute();

            //Set cluster name
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, a =>
            {
                Object firstParameter = a.WindowParameters.First();
                var testViewModel = firstParameter as LocationClusterSelectorViewModel;
                testViewModel.SelectedCluster = testViewModel.SelectedClusterScheme.Clusters.FirstOrDefault();
                ClusterName = testViewModel.SelectedCluster.Name;
                testViewModel.OKCommand.Execute();
            });
            viewModel.SelectClusterCommand.Execute();
            Assert.IsFalse(viewModel.IsShowWarning);
        }

        #endregion

        #region Other Tests

        /// <summary>
        /// Tests that the backboard is added with a z position of minus its depth.
        /// </summary>
        [Test]
        public void BackboardIsMinusZPosition()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            Assert.IsTrue(viewModel.MultiFixtureView != null);

            var selectedItem = viewModel.MultiFixtureView.Items.FirstOrDefault();
            Assert.IsTrue(selectedItem != null);
            selectedItem.IsSelected = true;

            viewModel.MultiFixtureView.IsBackboardRequired = true;
            Assert.IsTrue(viewModel.MultiFixtureView.BackboardDepth > 0);

            // Get the Z position of the backboard
            var selectedBackboard = selectedItem.BackboardComponent;

            Assert.IsTrue(selectedBackboard != null);
            Assert.IsTrue(selectedBackboard.Z < 0);
        }

        /// <summary>
        /// Tests changing the backboard depth adjusts its position
        /// </summary>
        [Test]
        public void BackboardDepthChangeAdjustsPosition()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            Assert.IsTrue(viewModel.MultiFixtureView != null);

            viewModel.MultiFixtureView.IsBackboardRequired = true;

            // ToDo: Get the Z position of the backboard
            //Assert.AreNotEqual(-5F, viewModel.MultiFixtureView.Backboard.Z);

            viewModel.MultiFixtureView.BackboardDepth = 5;

            // ToDo: Get the Z position of the backboard
            //Assert.AreEqual(-5F, viewModel.MultiFixtureView.Backboard.Z);
        }

        [Test]
        public void IncreaseFixtureWidthAdjustsBackboardAndBase()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            Assert.IsTrue(viewModel.MultiFixtureView != null);

            viewModel.MultiFixtureView.IsBackboardRequired = true;
            viewModel.MultiFixtureView.IsBaseRequired = true;
            viewModel.IsBaseRequired = true;

            Assert.AreNotEqual(150F, viewModel.MultiFixtureView.BackboardWidth);
            Assert.AreNotEqual(150F, viewModel.MultiFixtureView.BaseWidth);

            var selectedItem = viewModel.MultiFixtureView.Items.FirstOrDefault();
            Assert.IsTrue(selectedItem != null);
            selectedItem.IsSelected = true;

            selectedItem.Width = 150F;

            Assert.AreEqual(150F, viewModel.MultiFixtureView.BackboardWidth);
            Assert.AreEqual(150F, viewModel.MultiFixtureView.BaseWidth);
        }

        [Test]
        public void IncreaseFixtureHeightAdjustsBackboard()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            var planController = mainPageView.CreateNewPlanogram();
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(planController.SourcePlanogram);

            Assert.IsTrue(viewModel.MultiFixtureView != null);

            viewModel.MultiFixtureView.IsBackboardRequired = true;

            Assert.AreNotEqual(150F, viewModel.MultiFixtureView.BackboardHeight);

            var selectedItem = viewModel.MultiFixtureView.Items.FirstOrDefault();
            Assert.IsTrue(selectedItem != null);
            selectedItem.IsSelected = true;

            selectedItem.Height = 150F;

            Assert.AreEqual(150F, viewModel.MultiFixtureView.BackboardHeight);
        }

        [Test]
        public void AddFixtureCopiesSelectedFixture()
        {
            var mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(plan);

            Assert.IsTrue(viewModel.MultiFixtureView != null);

            var selectedItem = viewModel.MultiFixtureView.Items.FirstOrDefault();
            Assert.IsTrue(selectedItem != null);
            selectedItem.IsSelected = true;

            selectedItem.Width = 99;
            selectedItem.Height = 101;
            viewModel.MultiFixtureView.IsBackboardRequired = true;
            viewModel.IsBaseRequired = false;

            viewModel.AddFixtureAfterCommand.Execute();

            Assert.AreEqual(99, selectedItem.Width);
            Assert.AreEqual(101, selectedItem.Height);
            Assert.AreEqual(true, viewModel.MultiFixtureView.IsBackboardRequired);
            Assert.AreEqual(false, viewModel.MultiFixtureView.IsBaseRequired);
        }

        #endregion

        #region Test Helper Methods

        /// <summary>
        ///     Creates the test model.
        /// </summary>
        /// <returns></returns>
        /// <remarks>The viewmodel starts with an empty planogram.</remarks>
        private PlanogramPropertiesViewModel CreateTestModel()
        {
            MainPageViewModel mainPageView = App.MainPageViewModel;
            WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod,
                                      p => { ((PlanogramPropertiesViewModel) p.WindowParameters[0]).OKCommand.Execute(); });
            PlanogramView plan = mainPageView.CreateNewPlanogram().SourcePlanogram;
            var viewModel = new PlanogramPropertiesViewModel(plan);
            return viewModel;
        }

        #endregion
    }
}
