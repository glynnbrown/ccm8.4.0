﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32360 : M.Brumby
//  [PCR01564] Created
#endregion

#endregion

using System.Linq;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using System.Collections.Generic;
using System.Dynamic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.Collections;
using Galleria.Ccm.Editor.Client.Wpf.Settings;

namespace Galleria.Ccm.UnitTests.UI.PlanProperties
{
    [TestFixture]
    public sealed class SplitBayEditorViewModelTests : TestBase
    {
        private PlanogramView _planView;
        private int _entityId;
        public override void Setup()
        {
            base.Setup();
            _entityId = TestDataHelper.InsertEntityDtos(this.DalFactory, 1)[0].Id;
            List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(this.DalFactory, 1, _entityId);
            List<ClusterSchemeDto> clusterSchemeDto = TestDataHelper.InsertClusterSchemeDtos(this.DalFactory, _entityId, 1);
            List<ClusterDto> clusterDtos = TestDataHelper.InsertClusterDtos(this.DalFactory, clusterSchemeDto, 1);
            List<ClusterLocationDto> clusterLocationDtos = TestDataHelper.InsertClusterLocationDtos(this.DalFactory, clusterDtos, locationDtos);
        }
        #region Commands

        [Test]
        public void SplitByBayCount()
        {
            var fixtureCount = 1;

            SplitBayEditorViewModel viewModel = CreateTestModel();
            
            IEnumerable<PlanogramFixtureView> fixtureViews = _planView.Fixtures;
            fixtureCount = fixtureViews.Count();
            
            viewModel.SplitByBayCount = true;
            viewModel.SplitByFixedSize = false;
            viewModel.SplitByRecurringPattern = false;
            viewModel.SplitBayCount = 2;
            viewModel.SplitBayCommand.Execute();

            fixtureViews = _planView.Fixtures;
            Assert.AreEqual(fixtureCount * 2, fixtureViews.Count());
            
        }

        [Test]
        public void SplitByFixed()
        {
            var fixtureCount = 1;

            SplitBayEditorViewModel viewModel = CreateTestModel();

            IEnumerable<PlanogramFixtureView> fixtureViews = _planView.Fixtures;
            fixtureCount = fixtureViews.Count();

            viewModel.SplitByBayCount = false;
            viewModel.SplitByFixedSize = true;
            viewModel.SplitByRecurringPattern = false;
            viewModel.SplitFixedSize = 60d;
            viewModel.SplitBayCommand.Execute();

            fixtureViews = _planView.Fixtures;

            //Assuming 1 original fixture of width 120
            Assert.AreEqual(2, fixtureViews.Count());

        }

        [Test]
        public void SplitByPattern()
        {
            var fixtureCount = 1;

            SplitBayEditorViewModel viewModel = CreateTestModel();

            IEnumerable<PlanogramFixtureView> fixtureViews = _planView.Fixtures;
            fixtureCount = fixtureViews.Count();

            viewModel.SplitByBayCount = false;
            viewModel.SplitByFixedSize = false;
            viewModel.SplitByRecurringPattern = true;
            viewModel.SplitRecurringPattern = "60,40,20";
            viewModel.SplitBayCommand.Execute();

            fixtureViews = _planView.Fixtures;

            //Assuming 1 original fixture of width 120
            Assert.AreEqual(3, fixtureViews.Count());

        }
        #endregion

        #region Test Helper Methods

        /// <summary>
        ///     Creates the test model.
        /// </summary>
        /// <returns></returns>
        /// <remarks>The viewmodel starts with an empty planogram.</remarks>
        private SplitBayEditorViewModel CreateTestModel()
        {
            MainPageViewModel mainPageView = App.MainPageViewModel;
            base.WindowService.AddResponse(NUnitWindowService.ShowDialogTMethod, (p) => { ((PlanogramPropertiesViewModel)p.WindowParameters[0]).OKCommand.Execute(); });
            _planView = mainPageView.CreateNewPlanogram().SourcePlanogram;
            var viewModel = new SplitBayEditorViewModel(_planView, _planView.Fixtures);
            return viewModel;
        }

        #endregion
    }
}
