﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using NUnit.Framework;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI.PlanRender.ModelData
{
    [TestFixture]
    public sealed class PlanAssembly3DDataTests: TestBase
    {
        [Test]
        public void OnComponentAdded()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramAssemblyView assembly = fixture.AddAssembly();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanAssembly3DData assemblyData = planData.EnumerateAssemblyModels().First();

            Assert.AreEqual(0, assemblyData.EnumerateComponentModels().Count());

            var newItem = assembly.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, assemblyData.EnumerateComponentModels().Count());
            Assert.AreEqual(newItem, assemblyData.EnumerateComponentModels().First().Component);
        }

        [Test]
        public void OnComponentRemoved()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramAssemblyView assembly = fixture.AddAssembly();
            var item = assembly.AddComponent(PlanogramComponentType.Shelf);

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanAssembly3DData assemblyData = planData.EnumerateAssemblyModels().First();
            Assert.AreEqual(1, assemblyData.EnumerateComponentModels().Count());

            assembly.RemoveComponent(item);
            Assert.AreEqual(0, assemblyData.EnumerateComponentModels().Count());
        }

        [Test]
        public void UpdatePosition()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fx = planView.AddFixture();
            var item = fx.AddAssembly();
            item.X = 1;
            item.Y = 2;
            item.Z = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateAssemblyModels().First();

            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(item.Z, modelData.Position.Z);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set x
            item.X = 4;
            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set y
            item.Y = 5;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set z
            item.Z = 5;
            Assert.AreEqual(item.Z, modelData.Position.Z);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateRotation()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fx = planView.AddFixture();
            var item = fx.AddAssembly();
            item.Slope = 1;
            item.Angle = 2;
            item.Roll = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateAssemblyModels().First();

            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.Slope = 4;
            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.Angle = 5;
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.Roll = 5;
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateSize()
        {
            //can't test.
        }
    }
}
