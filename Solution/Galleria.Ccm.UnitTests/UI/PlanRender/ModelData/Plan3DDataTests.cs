﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using NUnit.Framework;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI.PlanRender.ModelData
{
    [TestFixture]
    public sealed class Plan3DDataTests: TestBase
    {
        [Test]
        public void OnFixtureAdded()
        {
            //Create a planogram
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());

            Assert.AreEqual(0, planData.EnumerateFixtureModels().Count());

            //add a fixture
            PlanogramFixtureView fx1 = planView.AddFixture();
            Assert.AreEqual(1, planData.EnumerateFixtureModels().Count());
            Assert.AreEqual(fx1, planData.EnumerateFixtureModels().First().Fixture);
        }

        [Test]
        public void OnFixtureRemoved()
        {
            //Create a planogram
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fx1 = planView.AddFixture();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());

            Assert.AreEqual(1, planData.EnumerateFixtureModels().Count());

            //remove the fixture
            planView.RemoveFixture(fx1);
            Assert.AreEqual(0, planData.EnumerateFixtureModels().Count());
        }

    }
}
