﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using NUnit.Framework;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI.PlanRender.ModelData
{
    [TestFixture]
    public sealed class PlanFixture3DDataTests : TestBase
    {
        [Test]
        public void OnAnnotationAdded()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanFixture3DData fixtureData = planData.EnumerateFixtureModels().First();

            Assert.AreEqual(0, fixtureData.EnumerateAnnotationModels().Count());
            
            //add an annotation
            var newItem = fixture.AddAnnotation();

            Assert.AreEqual(1, fixtureData.EnumerateAnnotationModels().Count());
            Assert.AreEqual(newItem, fixtureData.EnumerateAnnotationModels().First().Annotation);
        }

        [Test]
        public void OnAnnotationRemoved()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramAnnotationView anno1 = fixture.AddAnnotation();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanFixture3DData fixtureData = planData.EnumerateFixtureModels().First();
            Assert.AreEqual(1, fixtureData.EnumerateAnnotationModels().Count());

            ModelConstruct3DData itemData = fixtureData.EnumerateAnnotationModels().First();

            fixture.RemoveAnnotation(anno1);
            Assert.AreEqual(0, fixtureData.EnumerateAnnotationModels().Count());
            Assert.IsTrue(itemData.IsDisposed);
        }

        [Test]
        public void OnAssemblyAdded()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanFixture3DData fixtureData = planData.EnumerateFixtureModels().First();

            Assert.AreEqual(0, fixtureData.EnumerateAssemblyModels().Count());

            var newItem = fixture.AddAssembly();
            Assert.AreEqual(1, fixtureData.EnumerateAssemblyModels().Count());
            Assert.AreEqual(newItem, fixtureData.EnumerateAssemblyModels().First().Assembly);
        }

        [Test]
        public void OnAssemblyRemoved()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            var item = fixture.AddAssembly();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanFixture3DData fixtureData = planData.EnumerateFixtureModels().First();
            Assert.AreEqual(1, fixtureData.EnumerateAssemblyModels().Count());

            ModelConstruct3DData itemData = fixtureData.EnumerateAssemblyModels().First();

            fixture.RemoveAssembly(item);
            Assert.AreEqual(0, fixtureData.EnumerateAssemblyModels().Count());
            Assert.IsTrue(itemData.IsDisposed);
        }

        [Test]
        public void OnComponentAdded()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanFixture3DData fixtureData = planData.EnumerateFixtureModels().First();

            Assert.AreEqual(0, fixtureData.EnumerateComponentModels().Count());

            var newItem = fixture.AddComponent(PlanogramComponentType.Shelf);
            Assert.AreEqual(1, fixtureData.EnumerateComponentModels().Count());
            Assert.AreEqual(newItem, fixtureData.EnumerateComponentModels().First().Component);
        }

        [Test]
        public void OnComponentRemoved()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            var item = fixture.AddComponent(PlanogramComponentType.Shelf);

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanFixture3DData fixtureData = planData.EnumerateFixtureModels().First();
            Assert.AreEqual(1, fixtureData.EnumerateComponentModels().Count());

            ModelConstruct3DData itemData = fixtureData.EnumerateComponentModels().First();

            fixture.RemoveComponent(item);
            Assert.AreEqual(0, fixtureData.EnumerateComponentModels().Count());
            Assert.IsTrue(itemData.IsDisposed);
        }

        [Test]
        public void UpdatePosition()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            var item = planView.AddFixture();
            item.X = 1;
            item.Y = 2;
            item.Z = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateFixtureModels().First();

            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(item.Z, modelData.Position.Z);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set x
            item.X = 4;
            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set y
            item.Y = 5;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set z
            item.Z = 5;
            Assert.AreEqual(item.Z, modelData.Position.Z);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateRotation()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            var item = planView.AddFixture();
            item.Slope = 1;
            item.Angle = 2;
            item.Roll = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateFixtureModels().First();

            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.Slope = 4;
            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.Angle = 5;
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.Roll = 5;
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateSize()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            var item = planView.AddFixture();
            item.Width = 1;
            item.Height = 2;
            item.Depth = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateFixtureModels().First();

            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.AreEqual(item.Depth, modelData.Size.Depth);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.Width = 4;
            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.Height = 5;
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.Depth = 5;
            Assert.AreEqual(item.Depth, modelData.Size.Depth);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }
    }
}
