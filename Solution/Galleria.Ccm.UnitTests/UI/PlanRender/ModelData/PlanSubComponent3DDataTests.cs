﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using NUnit.Framework;
using System.Linq;
using System.Windows.Media;

namespace Galleria.Ccm.UnitTests.UI.PlanRender.ModelData
{
    [TestFixture]
    public class PlanSubComponent3DDataTests : TestBase
    {
        #region Setting Tests

        [Test]
        public void Settings_ShowDividers()
        {
            //Create a planogram
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            PlanogramComponentView backboard = fixture.AddComponent(PlanogramComponentType.Backboard);
            backboard.Height = fixture.Height;
            backboard.Width = fixture.Width;
            backboard.Depth = 1;

            PlanogramComponentView shelf = fixture.AddComponent(PlanogramComponentType.Shelf);
            shelf.Height = 4;
            shelf.Width = fixture.Width;
            shelf.Depth = 76.5F;
            PlanogramSubComponentView shelfSub = shelf.SubComponents[0];

            //Product 1 (w: 8.2, h: 32.2, d: 73.8)
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;

            PlanogramPositionView pos1 = shelfSub.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 9;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;

            //set dividers
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelfSub.DividerObstructionHeight = 1;
            shelfSub.DividerObstructionWidth = 1;
            shelfSub.DividerObstructionDepth = 1;
            shelfSub.IsDividerObstructionAtStart = true;
            shelfSub.IsDividerObstructionAtEnd = true;
            shelfSub.DividerObstructionStartX = 0;
            shelfSub.DividerObstructionSpacingX = 5;
            shelfSub.IsDividerObstructionByFacing = false;



            //Start with dividers showing
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowDividers = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == shelfSub);

            Assert.AreNotEqual(0, subData.DividerParts.Count());

            //hide dividers
            settings.ShowDividers = false;
            Assert.AreEqual(0, subData.DividerParts.Count());


            //start with dividers hidden
            settings = new PlanRenderSettings();
            settings.ShowDividers = false;

            planData = new Plan3DData(planView, settings);
            subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == shelfSub);

            Assert.AreEqual(0, subData.DividerParts.Count());

            //show dividers
            settings.ShowDividers = true;
            Assert.AreNotEqual(0, subData.DividerParts.Count());
        }

        [Test]
        public void Settings_ShowChestWalls()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Chest);
            var sub = component.SubComponents[0];

            //Start with part showing
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowChestWalls = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == sub);
            BoxModelPart3DData boxSubPart = (BoxModelPart3DData)subData.SubComponentPart;

            Assert.AreNotEqual(0, boxSubPart.FrontFaceThickness);
            Assert.AreNotEqual(0,boxSubPart.BackFaceThickness);
            Assert.AreNotEqual(0,boxSubPart.LeftFaceThickness);
            Assert.AreNotEqual(0, boxSubPart.RightFaceThickness);

            //hide part
            settings.ShowChestWalls = false;
            Assert.AreEqual(0, boxSubPart.FrontFaceThickness);
            Assert.AreEqual(0, boxSubPart.BackFaceThickness);
            Assert.AreEqual(0, boxSubPart.LeftFaceThickness);
            Assert.AreEqual(0, boxSubPart.RightFaceThickness);


            //start with part hidden
            settings = new PlanRenderSettings();
            settings.ShowChestWalls = false;

            planData = new Plan3DData(planView, settings);
            subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == sub);
            boxSubPart = (BoxModelPart3DData)subData.SubComponentPart;

            Assert.AreEqual(0, boxSubPart.FrontFaceThickness);
            Assert.AreEqual(0, boxSubPart.BackFaceThickness);
            Assert.AreEqual(0, boxSubPart.LeftFaceThickness);
            Assert.AreEqual(0, boxSubPart.RightFaceThickness);

            //show part
            settings.ShowChestWalls = true;
            Assert.AreNotEqual(0, boxSubPart.FrontFaceThickness);
            Assert.AreNotEqual(0, boxSubPart.BackFaceThickness);
            Assert.AreNotEqual(0, boxSubPart.LeftFaceThickness);
            Assert.AreNotEqual(0, boxSubPart.RightFaceThickness);
        }

        [Test]
        public void Settings_ShowShelfRisers()
        {
            //Create a planogram
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            PlanogramComponentView backboard = fixture.AddComponent(PlanogramComponentType.Backboard);
            backboard.Height = fixture.Height;
            backboard.Width = fixture.Width;
            backboard.Depth = 1;

            PlanogramComponentView shelf = fixture.AddComponent(PlanogramComponentType.Shelf);
            shelf.Height = 4;
            shelf.Width = fixture.Width;
            shelf.Depth = 76.5F;
            PlanogramSubComponentView shelfSub = shelf.SubComponents[0];

            //set shelf risers
            shelfSub.IsRiserPlacedOnFront = true;
            shelfSub.RiserHeight = 5;
            shelfSub.RiserThickness = 1;


            //Start with risers showing
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowShelfRisers = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == shelfSub);

            Assert.IsNotNull(subData.RiserParts);

            //hide risers
            settings.ShowShelfRisers = false;
            Assert.AreEqual(0, subData.RiserParts.Count());

            //start with risers hidden
            settings = new PlanRenderSettings();
            settings.ShowShelfRisers = false;

            planData = new Plan3DData(planView, settings);
            subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == shelfSub);

            Assert.AreEqual(0, subData.RiserParts.Count());

            //show riser
            settings.ShowShelfRisers = true;
            Assert.AreNotEqual(0, subData.RiserParts.Count());
        }

        [Test]
        public void Settings_ShowNotches()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Backboard);
            var item = component.SubComponents[0];
            item.IsNotchPlacedOnFront = true;

            //Start with part showing
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowNotches = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == item);

            Assert.IsNotNull(subData.NotchesPart);

            //hide part
            settings.ShowNotches = false;
            Assert.IsNull(subData.NotchesPart);

            //start with part hidden
            settings = new PlanRenderSettings();
            settings.ShowNotches = false;

            planData = new Plan3DData(planView, settings);
            subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == item);

            Assert.IsNull(subData.NotchesPart);

            //show part
            settings.ShowNotches = true;
            Assert.IsNotNull(subData.NotchesPart);
        }

        [Test]
        public void Settings_ShowPegHoles()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Peg);
            var sub = component.SubComponents[0];

            //Start with part showing
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowPegHoles = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == sub);

            Assert.IsNotNull(subData.PegholesPart);

            //hide part
            settings.ShowPegHoles = false;
            Assert.IsNull(subData.PegholesPart);

            //start with part hidden
            settings = new PlanRenderSettings();
            settings.ShowPegHoles = false;

            planData = new Plan3DData(planView, settings);
            subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == sub);

            Assert.IsNull(subData.PegholesPart);

            //show part
            settings.ShowPegHoles = true;
            Assert.IsNotNull(subData.PegholesPart);
        }

        [Test]
        public void Settings_ShowDividerLines()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add(); PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            PlanogramComponentView backboard = fixture.AddComponent(PlanogramComponentType.Backboard);
            backboard.Height = fixture.Height;
            backboard.Width = fixture.Width;
            backboard.Depth = 1;

            PlanogramComponentView shelf = fixture.AddComponent(PlanogramComponentType.Shelf);
            shelf.Height = 4;
            shelf.Width = fixture.Width;
            shelf.Depth = 76.5F;
            PlanogramSubComponentView shelfSub = shelf.SubComponents[0];

            //set dividers
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Left;
            shelfSub.DividerObstructionHeight = 1;
            shelfSub.DividerObstructionWidth = 1;
            shelfSub.DividerObstructionDepth = 1;
            shelfSub.IsDividerObstructionAtStart = true;
            shelfSub.IsDividerObstructionAtEnd = true;
            shelfSub.DividerObstructionStartX = 0;
            shelfSub.DividerObstructionSpacingX = 5;
            shelfSub.IsDividerObstructionByFacing = false;



            //Start with slotlines showing
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowDividerLines = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == shelfSub);

            Assert.IsNotNull(subData.DividerLinePart);

            //hide dividers
            settings.ShowDividerLines = false;
            Assert.IsNull(subData.DividerLinePart);


            //start with dividers hidden
            settings = new PlanRenderSettings();
            settings.ShowDividerLines = false;

            planData = new Plan3DData(planView, settings);
            subData = planData.EnumerateSubComponentModels().First(s => s.SubComponent == shelfSub);

            Assert.IsNull(subData.DividerLinePart);

            //show dividers
            settings.ShowDividerLines = true;
            Assert.IsNotNull(subData.DividerLinePart);
        }

        #endregion

        #region SubComponent model tests

        [Test]
        public void UpdatePosition()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
            var item = component.SubComponents[0];
            item.X = 1;
            item.Y = 2;
            item.Z = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateSubComponentModels().First();

            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(item.Z, modelData.Position.Z);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set x
            item.X = 4;
            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set y
            item.Y = 5;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set z
            item.Z = 5;
            Assert.AreEqual(item.Z, modelData.Position.Z);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateRotation()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
            var item = component.SubComponents[0];
            item.Slope = 1;
            item.Angle = 2;
            item.Roll = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateSubComponentModels().First();

            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.Slope = 4;
            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.Angle = 5;
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.Roll = 5;
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateSize()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
            var item = component.SubComponents[0];
            item.Width = 1;
            item.Height = 2;
            item.Depth = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateSubComponentModels().First();

            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.AreEqual(item.Depth, modelData.Size.Depth);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.Width = 4;
            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.Height = 5;
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.Depth = 5;
            Assert.AreEqual(item.Depth, modelData.Size.Depth);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        #endregion

        #region Child Annotation Tests

        [Test]
        public void OnAnnotationAdded()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First();

            Assert.AreEqual(0, subData.EnumerateAnnotationModels().Count());

            //add an annotation
            var newItem = component.SubComponents[0].AddAnnotation();

            Assert.AreEqual(1, subData.EnumerateAnnotationModels().Count());
            Assert.AreEqual(newItem, subData.EnumerateAnnotationModels().First().Annotation);
        }

        [Test]
        public void OnAnnotationRemoved()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
            PlanogramAnnotationView anno1 = component.SubComponents[0].AddAnnotation();

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanSubComponent3DData subData = planData.EnumerateSubComponentModels().First();
            Assert.AreEqual(1, subData.EnumerateAnnotationModels().Count());

            ModelConstruct3DData itemData = subData.EnumerateAnnotationModels().First();

            component.SubComponents[0].RemoveAnnotation(anno1);
            Assert.AreEqual(0, subData.EnumerateAnnotationModels().Count());
            Assert.IsTrue(itemData.IsDisposed);
        }

        #endregion

        #region Riser Part Tests

        [Test]
        public void Riser_ColourUpdates()
        {
            //V8-28615

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
            var item = component.SubComponents[0];
            item.IsRiserPlacedOnFront = true;
            item.RiserColour = CommonHelper.ColorToInt(Colors.Red);

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateSubComponentModels().First();

            //check the color of the riser part
            Assert.AreEqual(CommonHelper.ColorToInt(Colors.Red), ModelColour.ColorToInt(modelData.RiserParts.First().Value.Material.FillColour), "riser should have been loaded red"); 

            //change the color
            item.RiserColour = CommonHelper.ColorToInt(Colors.Blue);
            Assert.AreEqual(CommonHelper.ColorToInt(Colors.Blue), ModelColour.ColorToInt(modelData.RiserParts.First().Value.Material.FillColour), "riser should have been changed to blue"); 
        }

        //Cant test this as riser is being created as a mesh so the size is not being set.
        //[Test]
        //public void Riser_SizeUpdates()
        //{
        //    Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
        //    Planogram plan = package.Planograms.Add();
        //    PlanogramView planView = new PlanogramView(plan);
        //    PlanogramFixtureView fixture = planView.AddFixture();
        //    PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
        //    var item = component.SubComponents[0];
            
        //    item.IsRiserPlacedOnFront = true;
        //    item.RiserHeight = 10;
        //    item.RiserThickness = 1;
            
        //    Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
        //    var modelData = planData.EnumerateSubComponentModels().First();

        //    Assert.AreEqual(10, modelData.RiserPart.Size.Height, "Height should match");
        //    Assert.AreEqual(1, modelData.RiserPart.Size.Depth, "Thickness should match");
            
        //    //change height
        //    item.RiserHeight = 11;
        //    Assert.AreEqual(11, modelData.RiserPart.Size.Height, "Height should match");

        //    //change thick
        //    item.RiserThickness = 2;
        //    Assert.AreEqual(2, modelData.RiserPart.Size.Depth, "Thickness should match");
        //}

        [Test]
        public void Riser_ToggleRiserPart()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
            var item = component.SubComponents[0];

            item.IsRiserPlacedOnFront = false;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateSubComponentModels().First();

            Assert.AreEqual(0, modelData.RiserParts.Count(), "Should not draw riser");

            item.IsRiserPlacedOnFront = true;
            Assert.AreNotEqual(0, modelData.RiserParts.Count(), "Should draw riser");

            item.IsRiserPlacedOnFront = false;
            Assert.AreEqual(0, modelData.RiserParts.Count(), "Should not draw riser");
        }

        #endregion



    }
}
