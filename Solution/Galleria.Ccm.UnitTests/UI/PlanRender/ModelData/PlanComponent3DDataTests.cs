﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using NUnit.Framework;
using System.Linq;

namespace Galleria.Ccm.UnitTests.UI.PlanRender.ModelData
{
    [TestFixture]
    public sealed class PlanComponent3DDataTests: TestBase
    {
        #region Settings

        [Test]
        public void Settings_FixtureLabel()
        {
            throw new InconclusiveException("TODO");
        }

        [Test]
        public void Settings_RotateTopDownComponents()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            var item = fixture.AddComponent(PlanogramComponentType.Shelf);
            item.X = 0;
            item.Y = 2;
            item.Z = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateComponentModels().First();

            //flag = false, setting = false -> should not move
            item.IsMerchandisedTopDown = false;
            planData.Settings.RotateTopDownComponents = false;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(0, modelData.Rotation.Slope);

            //flag = false, setting = true -> should not move
            item.IsMerchandisedTopDown = false;
            planData.Settings.RotateTopDownComponents = true;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(0, modelData.Rotation.Slope);

            //flag = true, setting = false -> should not move
            item.IsMerchandisedTopDown = true;
            planData.Settings.RotateTopDownComponents = false;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(0, modelData.Rotation.Slope);

            //flag = true, setting = true -> should move
            item.IsMerchandisedTopDown = true;
            planData.Settings.RotateTopDownComponents = true;
            Assert.AreNotEqual(item.Y, modelData.Position.Y);
            Assert.AreNotEqual(0, modelData.Rotation.Slope);
        }

        #endregion

        [Test]
        public void UpdatePosition()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            var item = fixture.AddComponent(PlanogramComponentType.Shelf);
            item.X = 1;
            item.Y = 2;
            item.Z = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateComponentModels().First();

            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(item.Z, modelData.Position.Z);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set x
            item.X = 4;
            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set y
            item.Y = 5;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set z
            item.Z = 5;
            Assert.AreEqual(item.Z, modelData.Position.Z);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateRotation()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            var item = fixture.AddComponent(PlanogramComponentType.Shelf);
            item.Slope = 1;
            item.Angle = 2;
            item.Roll = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateComponentModels().First();

            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.Slope = 4;
            Assert.AreEqual(item.Slope, modelData.Rotation.Slope);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.Angle = 5;
            Assert.AreEqual(item.Angle, modelData.Rotation.Angle);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.Roll = 5;
            Assert.AreEqual(item.Roll, modelData.Rotation.Roll);
            Assert.Contains("Rotation", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateSize()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            var item = fixture.AddComponent(PlanogramComponentType.Shelf);
            item.Width = 1;
            item.Height = 2;
            item.Depth = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumerateComponentModels().First();

            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.AreEqual(item.Depth, modelData.Size.Depth);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.Width = 4;
            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.Height = 5;
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.Depth = 5;
            Assert.AreEqual(item.Depth, modelData.Size.Depth);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void OnSubComponentAdded()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanComponent3DData componentData = planData.EnumerateComponentModels().First();

            Assert.AreEqual(1, componentData.EnumerateSubComponentModels().Count());

            var newItem = component.AddSubComponent();
            Assert.AreEqual(2, componentData.EnumerateSubComponentModels().Count());
            Assert.AreEqual(newItem, componentData.EnumerateSubComponentModels().ElementAt(1).SubComponent);
        }

        [Test]
        public void OnSubComponentRemoved()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            PlanComponent3DData componentData = planData.EnumerateComponentModels().First();

            Assert.AreEqual(1, componentData.EnumerateSubComponentModels().Count());
            ModelConstruct3DData itemData = componentData.EnumerateSubComponentModels().First();

            component.RemoveSubComponent(component.SubComponents[0]);

            Assert.AreEqual(0, componentData.EnumerateSubComponentModels().Count());
            Assert.IsTrue(itemData.IsDisposed);
        }
    }
}
