﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24764 : L.Hodson
//  Created
// v8-24265 : J.Pickup
//  LabelSettingView is now LabelItem
#endregion
#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Rendering;
using Galleria.Framework.Rendering.Interfaces;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galleria.Framework.Planograms.Rendering;

namespace Galleria.Ccm.UnitTests.UI.PlanRender.ModelData
{
    [TestFixture]
    public sealed class PlanPosition3DDataTests: TestBase
    {
        #region Test Helpers

        private PlanogramPositionView CreateShelfPositionView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponentView subView = component.SubComponents[0];
            subView.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            subView.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            subView.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            subView.MerchandisableHeight = 100;
            subView.IsProductOverlapAllowed = true;

            //Product 1 (w: 8.2, h: 32.2, d: 73.8)
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;

            PlanogramPositionView pos1 = subView.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 9;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;

            return pos1;
        }

        private PlanogramPositionView CreateChestPositionView()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            PlanogramView planView = new PlanogramView(plan);
            PlanogramFixtureView fixture = planView.AddFixture();
            PlanogramComponentView component = fixture.AddComponent(PlanogramComponentType.Chest);
            PlanogramSubComponentView subView = component.SubComponents[0];

            //Product 1 (w: 8.2, h: 32.2, d: 73.8)
            PlanogramProductView prod1 = planView.AddProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;

            PlanogramPositionView pos1 = subView.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 9;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;

            return pos1;
        }

        public IEnumerable<String> PositionRenderablePropertyNames
        {
            get
            {
                String[] exclusions = new String[] 
                { };


                foreach (PropertyInfo property in typeof(IPlanPositionRenderable).GetProperties()
             .Where(p => p.DeclaringType == typeof(IPlanPositionRenderable)))
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        yield return property.Name;
                    }
                }
            }
        }

        public IEnumerable<String> ProductRenderablePropertyNames
        {
            get
            {
                String[] exclusions = new String[] { };


                foreach (PropertyInfo property in typeof(IPlanProductRenderable).GetProperties()
             .Where(p => p.DeclaringType == typeof(IPlanProductRenderable)))
                {
                    if (!exclusions.Contains(property.Name)
                        && !property.Name.EndsWith("Id")
                        && !property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        yield return property.Name;
                    }
                }
            }
        }

        #endregion

        #region Settings

        [Test]
        public void Settings_ShowPositions()
        {
            PlanogramPositionView pos = CreateShelfPositionView();
            PlanogramView planView = pos.Planogram;

            //start as true
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowPositions = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            var posModel = planData.EnumeratePositionModels().First();
            posModel.PropertyChanged += base.TestModel_PropertyChanged;

            Assert.IsTrue(posModel.IsVisible);
            foreach (var part in posModel.GetAllParts())
            {
                Assert.IsTrue(part.IsVisible);
            }

            //set to false.
            settings.ShowPositions = false;
            Assert.IsFalse(posModel.IsVisible);
            foreach (var part in posModel.GetAllParts())
            {
                Assert.IsFalse(part.IsVisible);
            }
            base.PropertyChangedNotifications.Contains("IsVisible");

            posModel.PropertyChanged -= base.TestModel_PropertyChanged;


            //start as false;
            settings = new PlanRenderSettings();
            settings.ShowPositions = false;

            planData = new Plan3DData(planView, settings);
            posModel = planData.EnumeratePositionModels().First();

            Assert.IsFalse(posModel.IsVisible);
            foreach (var part in posModel.GetAllParts())
            {
                Assert.IsFalse(part.IsVisible);
            }
        }

        [Test]
        public void Settings_ShowPositionUnits()
        {
            PlanogramPositionView pos = CreateShelfPositionView();
            PlanogramView planView = pos.Planogram;

            pos.FacingsWide = 1;
            pos.FacingsHigh = 1;
            pos.FacingsDeep = 1;

            pos.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            pos.FacingsXHigh = 1;
            pos.FacingsXWide = 1;
            pos.FacingsXDeep = 1;

            //start as true
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.ShowPositions = true;
            settings.ShowPositionUnits = true;

            Plan3DData planData = new Plan3DData(planView, settings);
            var posModel = planData.EnumeratePositionModels().First();

            Assert.AreNotEqual(1, posModel.ModelParts.Count);

            //set to false.
            settings.ShowPositionUnits = false;
            Assert.AreEqual(1, posModel.ModelParts.Count);

            //start as false;
            settings = new PlanRenderSettings();
            settings.ShowPositions = true;
            settings.ShowPositionUnits = false;

            planData = new Plan3DData(planView, settings);
            posModel = planData.EnumeratePositionModels().First();

            Assert.AreEqual(1, posModel.ModelParts.Count);
        }

        [Test]
        public void Settings_ShowProductImages()
        {
            Assert.Ignore();
        }

        [Test]
        public void Settings_ShowProductFillPatterns()
        {
            Assert.Ignore();
        }

        [Test]
        public void Settings_ShowProductShapes()
        {
            Assert.Ignore();
        }

        [Test]
        public void Settings_ProductLabel()
        {
            Assert.Ignore();
        }

        [Test]
        public void Settings_ShowChestsTopDown()
        {
            PlanogramPositionView pos = CreateChestPositionView();
            PlanogramView planView = pos.Planogram;

            //start as true
            PlanRenderSettings settings = new PlanRenderSettings();
            settings.RotateTopDownComponents = true;

            Label label = Label.NewLabel(LabelType.Product);
            label.Text = "Test";
            settings.ProductLabel = new LabelItem(label);
            settings.PositionLabelText = new PlanogramPositionLabelTexts(label.Text, planView.Model);

            Plan3DData planData = new Plan3DData(planView, settings);
            var posModel = planData.EnumeratePositionModels().First();

            IModelLabel3DData labelModel = posModel.ModelLabels[0];
            Assert.AreEqual(ModelLabelAttachFace.Top, labelModel.AttachFace);

            //set to false.
            settings.RotateTopDownComponents = false;
            Assert.AreEqual(ModelLabelAttachFace.Front, labelModel.AttachFace);

            //start as false;
            settings = new PlanRenderSettings();
            settings.RotateTopDownComponents = false;
            settings.ProductLabel = new LabelItem(label);
            settings.PositionLabelText = new PlanogramPositionLabelTexts(label.Text, planView.Model);

            planData = new Plan3DData(planView, settings);
            posModel = planData.EnumeratePositionModels().First();
           
            labelModel = posModel.ModelLabels[0];
            Assert.AreEqual(ModelLabelAttachFace.Front, labelModel.AttachFace);
            


            //Check that this does not affect non-chest products
            pos = CreateShelfPositionView();
            planView = pos.Planogram;

            settings = new PlanRenderSettings();
            settings.RotateTopDownComponents = true;
            settings.ProductLabel = new LabelItem(label);
            settings.PositionLabelText = new PlanogramPositionLabelTexts(label.Text, planView.Model);

            planData = new Plan3DData(planView, settings);
            posModel = planData.EnumeratePositionModels().First();

            labelModel = posModel.ModelLabels[0];
            Assert.AreEqual(ModelLabelAttachFace.Front, labelModel.AttachFace);
        }

        [Test]
        public void Settings_ShowPegs()
        {
            Assert.Ignore();
        }

        #endregion

        [Test]
        public void UpdatePosition()
        {
            PlanogramPositionView pos = CreateShelfPositionView();
            PlanogramView planView = pos.Planogram;

            var item = pos;
            item.X = 1;
            item.Y = 2;
            item.Z = 3;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumeratePositionModels().First();

            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.AreEqual(item.Z, modelData.Position.Z);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set x
            item.X = 4;
            Assert.AreEqual(item.X, modelData.Position.X);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set y
            item.Y = 5;
            Assert.AreEqual(item.Y, modelData.Position.Y);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set z
            item.Z = 1;
            Assert.AreEqual(item.Z, modelData.Position.Z);
            Assert.Contains("Position", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }

        [Test]
        public void UpdateSize()
        {
            PlanogramPositionView pos = CreateShelfPositionView();
            PlanogramView planView = pos.Planogram;

            var item = pos;
            item.FacingsWide = 2;
            item.FacingsHigh = 3;
            item.FacingsDeep = 4;

            Plan3DData planData = new Plan3DData(planView, new PlanRenderSettings());
            var modelData = planData.EnumeratePositionModels().First();

            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.AreEqual(item.Depth, modelData.Size.Depth);

            modelData.PropertyChanged += base.TestModel_PropertyChanged;

            //set slope
            item.FacingsWide = 4;
            Assert.AreEqual(item.Width, modelData.Size.Width);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set angle
            item.FacingsHigh = 5;
            Assert.AreEqual(item.Height, modelData.Size.Height);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();

            //set roll
            item.FacingsDeep = 5;
            Assert.AreEqual(item.Depth, modelData.Size.Depth);
            Assert.Contains("Size", base.PropertyChangedNotifications);

            base.PropertyChangedNotifications.Clear();
            modelData.PropertyChanged -= base.TestModel_PropertyChanged;
        }


    }
}
