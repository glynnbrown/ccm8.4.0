﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using System.Data;
using NUnit.Framework;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportMerchandisingHierarchyProcess : TestBase
    {
        #region Fields

        Int32 _numDtosToInsert = 200;

        #endregion

        #region Helper Methods

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(ImportFileData fileData, IDalFactory dalFactory)
        {
            //Fetch hierarchy 
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(1);

            //Fetch all of the levels
            IEnumerable<ProductLevel> levels = hierarchy.EnumerateAllLevels();

            //Pull back all of the product group records within the database
            List<ProductGroupDto> productGroups;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroups = dal.FetchByProductHierarchyId(hierarchy.Id).ToList();
                }
                dalContext.Commit();
            }

            //Check the same number of category (level 3) groups have been inserted into the database as 
            //are in the import file data.
            IEnumerable<ProductGroupDto> groups = productGroups.Where(p => p.ProductLevelId == (Int32)levels.Last().Id);
            Assert.AreEqual(groups.Count(), fileData.Rows.Count);

            foreach (ImportFileDataRow dataRow in fileData.Rows)
            {
                //Get values from row
                String categoryName = dataRow.Cells[dataRow.Cells.Count - 1].Value.ToString(); //last column
                String categoryCode = dataRow.Cells[dataRow.Cells.Count - 2].Value.ToString(); //2nd to last column

                //Find matching group
                ProductGroupDto matchingGroup = groups.FirstOrDefault(p => p.Code == categoryCode & p.Name == categoryName);

                Assert.IsNotNull(matchingGroup, "Product Group record should exist");
            }
        }


        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData(IDalFactory dalFactory)
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            IEnumerable<String> levelNames = ProductHierarchy.FetchByEntityId(1).EnumerateAllLevels()
                .Where(p => p.IsRoot == false).Select(p => p.Name).ToList();

            fileData.MappingList = MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(levelNames);

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < _numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    importRow.Cells.Add(
                        ImportFileDataCell.NewImportFileDataCell(cellColNumber, mappingItem.PropertyName + i)
                        );

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private EntityDto InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            Int32 entityId = entityDtos[0].Id;

            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);
            Int32 hierarchyId = hierarchyDtos[0].Id;

            //merch hierarchy
            List<ProductLevelDto> productLevel2Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchyId, 3);

            //only add the merch root group
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dal.Insert(new ProductGroupDto()
                    {
                        Code = "x",
                        ProductHierarchyId = hierarchyId,
                        ProductLevelId = productLevel2Dtos[0].Id,
                        Name = "root",
                        DateCreated = DateTime.UtcNow,
                        DateLastModified = DateTime.UtcNow
                    });
                }
            }


            return entityDtos[0];
        }

        #endregion

        #region Import


        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //Create initial required data
                EntityDto entityDto = InsertDtos(dalFactory);

                Random random = new Random();

                //Create data table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create data
                fileData = CreateData(dalFactory);

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess(1, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess>(importProcess);

                //Check imported data matches inserted.
                ValidateImportedData(fileData, dalFactory);

                #region Case 2 - [GEM21865] Existing code match is case insensitive

                //get the number of groups added.
                Int32 productGroupCount;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        productGroupCount = dal.FetchByProductHierarchyId(entityDto.Id).Count();
                    }
                }



                //+upper invariant
                fileData = CreateData(dalFactory);
                foreach (Int32 colNumber in new List<Int32> { 1, 3 })
                {
                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }
                }

                //import
                importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess(1, fileData));

                Int32 newGroupCount;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        newGroupCount = dal.FetchByProductHierarchyId(entityDto.Id).Count();
                    }
                }
                Assert.AreEqual(productGroupCount, newGroupCount);

                #endregion

            }
        }

        #endregion

        #region Ticket Tests

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22339_CanImportRaggedHierarchy(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                List<ProductHierarchyDto> productHierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId);

                //create a hierarchy with 2 levels and 1 group below the root
                ProductHierarchyDto productHierarchyDto = productHierarchyDtos[0];
                List<ProductLevelDto> level1Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDto.Id, 1);
                List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, level1Dtos, 1);


                ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(entityId);

                //add a second and third level
                ProductLevel level2 = ProductLevel.NewProductLevel();
                level2.Name = "level2";
                hierarchy.RootLevel.ChildLevel = level2;

                ProductLevel level3 = ProductLevel.NewProductLevel();
                level3.Name = "level3";
                level2.ChildLevel = level3;

                hierarchy = hierarchy.Save();


                //create file data to import 1 group against level 2.
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { "GEM22339", "GEM22339_NewGroup", String.Empty, String.Empty, String.Empty, String.Empty });
                AddDataRows(fileData, data);

                //check validation
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                //check there are only warnings
                Assert.AreEqual(0, validationProcess.Errors.Count(e => e.Type == ValidationErrorType.Error));

                //import
                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess(entityId, fileData));


                //refetch the hierarchy and check the new group was added.
                hierarchy = ProductHierarchy.FetchByEntityId(entityId);
                Assert.AreEqual(1, hierarchy.RootGroup.ChildList.Count);
                Assert.AreEqual("GEM22339", hierarchy.RootGroup.ChildList[0].Code);
                Assert.AreEqual(0, hierarchy.RootGroup.ChildList[0].ChildList.Count);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22916_MoveExistingToNewParent(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);

                //create a hierarchy with 2 levels and 1 group below the root
                ProductHierarchyDto productHierarchyDto = hierarchyDtos[0];
                List<ProductLevelDto> level1Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDto.Id, 1);
                List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, level1Dtos, 1);

                ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(entityId);

                //add some more levels
                ProductLevel level2 = ProductLevel.NewProductLevel();
                level2.Name = "Department";
                hierarchy.RootLevel.ChildLevel = level2;

                ProductLevel level3 = ProductLevel.NewProductLevel();
                level3.Name = "Section";
                level2.ChildLevel = level3;

                ProductLevel level4 = ProductLevel.NewProductLevel();
                level4.Name = "Planogram Class";
                level3.ChildLevel = level4;

                ProductLevel level5 = ProductLevel.NewProductLevel();
                level5.Name = "Planogram Class L1";
                level4.ChildLevel = level5;


                //add groups
                ProductGroup chilled = ProductGroup.NewProductGroup(level2.Id);
                chilled.Name = "chilled";
                chilled.Code = "MD2";
                hierarchy.RootGroup.ChildList.Add(chilled);

                ProductGroup farmersboy = ProductGroup.NewProductGroup(level3.Id);
                farmersboy.Name = "farmersboy";
                farmersboy.Code = "MS9";
                chilled.ChildList.Add(farmersboy);

                ProductGroup freshFoods = ProductGroup.NewProductGroup(level4.Id);
                freshFoods.Name = "freshFoods";
                freshFoods.Code = "90001";
                farmersboy.ChildList.Add(freshFoods);

                ProductGroup bacon = ProductGroup.NewProductGroup(level5.Id);
                bacon.Name = "bacon";
                bacon.Code = "90002";
                freshFoods.ChildList.Add(bacon);


                hierarchy = hierarchy.Save();


                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { "MD2", "chilled", "MS2", "farmersboy", "90001", "freshFoods", "90002", "bacon" });
                AddDataRows(fileData, data);


                //check validation - should fail.
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count(e => e.Type == ValidationErrorType.Error));

                //import
                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess(entityId, fileData));

                //refetch the hierarchy and check
                hierarchy = ProductHierarchy.FetchByEntityId(entityId);

                List<ProductGroup> finalGroups = hierarchy.EnumerateAllGroups().ToList();

                Assert.AreEqual("MD2", finalGroups[1].Code);
                Assert.AreEqual("chilled", finalGroups[1].Name);
                Assert.AreEqual(2, finalGroups[1].ChildList.Count);

                //orig farmersboy group-
                Assert.AreEqual("MS9", finalGroups[1].ChildList[0].Code);
                Assert.AreEqual("farmersboy", finalGroups[1].ChildList[0].Name);

                Assert.AreEqual(0, finalGroups[1].ChildList[0].ChildList.Count);


                //new farmersboy group
                Assert.AreEqual("MS2", finalGroups[1].ChildList[1].Code);
                Assert.AreEqual("farmersboy", finalGroups[1].ChildList[1].Name);

                //child group has moved from old farmersboy group to the new one.
                Assert.AreEqual(1, finalGroups[1].ChildList[1].ChildList.Count);

            }
        }

        #endregion
    }
}
