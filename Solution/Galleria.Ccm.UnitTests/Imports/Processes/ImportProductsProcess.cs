﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25452 : N.Haywood
//	Added from SA
// V8-26041 : A.Kuszyk
//      Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
//      Changed use of CCM ProductOrientationType to Framework PlanogramProductOrientationType.
// V8-27424 : L.Luong
//  Added FinancialGroupCode and FinancialGroupName
#endregion
#region Version History: CCM802
// V8-29230 : N.Foster
//  Removed unused parameter from import products process
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote  
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportProductsProcess : TestBase
    {
        #region Fields

        List<ProductGroupDto> _productGroups = new List<ProductGroupDto>();
        Int32 _numDtosToInsert = 200;
        #endregion

        #region Helper Methods

        private void InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);

            //merch hierarchy
            List<ProductHierarchyDto> productHierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);
            List<ProductLevelDto> productLevel2Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDtos[0].Id, 3);
            List<ProductGroupDto> productGroup2Dtos = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel2Dtos, 2);

            _productGroups = productGroup2Dtos;
        }

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(ImportFileData fileData, IDalFactory dalFactory)
        {
            List<ProductDto> productDtoList;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    productDtoList = dal.FetchByEntityId(1).ToList();
                }
            }

            foreach (ImportFileDataRow row in fileData.Rows)
            {
                //Get GTIN
                String code = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == ProductImportMappingList.GtinMapId)))
                    .Value.ToString().ToLowerInvariant();

                //Find corresponding Product dto
                ProductDto productDto = productDtoList.FirstOrDefault(p => p.Gtin.ToLowerInvariant() == code);
                Assert.IsNotNull(productDto, "Product record should exist");

                //Check each cell value
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateDepthMapId))).Value, productDto.AlternateDepth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateHeightMapId))).Value, productDto.AlternateHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateWidthMapId))).Value, productDto.AlternateWidth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayBackMapId))).Value, productDto.CanBreakTrayBack);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayDownMapId))).Value, productDto.CanBreakTrayDown);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayTopMapId))).Value, productDto.CanBreakTrayTop);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayUpMapId))).Value, productDto.CanBreakTrayUp);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CasePackUnitsMapId))).Value, productDto.CasePackUnits);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DepthMapId))).Value, productDto.Depth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayDepthMapId))).Value, productDto.DisplayDepth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayHeightMapId))).Value, productDto.DisplayHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayWidthMapId))).Value, productDto.DisplayWidth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceAboveMapId))).Value, productDto.FingerSpaceAbove);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceToTheSideMapId))).Value, productDto.FingerSpaceToTheSide);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceBottomCapMapId))).Value, productDto.ForceBottomCap);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceMiddleCapMapId))).Value, productDto.ForceMiddleCap);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FrontOverhangMapId))).Value, productDto.FrontOverhang);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId))).Value, productDto.Gtin);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HeightMapId))).Value, productDto.Height);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsActiveMapId))).Value, productDto.IsActive);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsFrontOnlyMapId))).Value, productDto.IsFrontOnly);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPlaceHolderProductMapId))).Value, productDto.IsPlaceHolderProduct);
               Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxDeepMapId))).Value, productDto.MaxDeep);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxRightCapMapId))).Value, productDto.MaxRightCap);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxStackMapId))).Value, productDto.MaxStack);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxTopCapMapId))).Value, productDto.MaxTopCap);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId))).Value, ProductMerchandisingStyleHelper.FriendlyNames[(ProductMerchandisingStyle)productDto.MerchandisingStyle]);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MinDeepMapId))).Value, productDto.MinDeep);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId))).Value, productDto.Name);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingDepthMapId))).Value, productDto.NestingDepth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingHeightMapId))).Value, productDto.NestingHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingWidthMapId))).Value, productDto.NestingWidth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NumberOfPegHolesMapId))).Value, productDto.NumberOfPegHoles);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId))).Value, productDto.OrientationType);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegDepthMapId))).Value, productDto.PegDepth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId))).Value, productDto.PegProngOffsetX);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegXMapId))).Value, productDto.PegX);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX2MapId))).Value, productDto.PegX2);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX3MapId))).Value, productDto.PegX3);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegYMapId))).Value, productDto.PegY);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY2MapId))).Value, productDto.PegY2);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY3MapId))).Value, productDto.PegY3);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDepthMapId))).Value, productDto.PointOfPurchaseDepth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseHeightMapId))).Value, productDto.PointOfPurchaseHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseWidthMapId))).Value, productDto.PointOfPurchaseWidth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeDepthMapId))).Value, productDto.SqueezeDepth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeHeightMapId))).Value, productDto.SqueezeHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeWidthMapId))).Value, productDto.SqueezeWidth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId))).Value, productDto.StatusType);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayDeepMapId))).Value, productDto.TrayDeep);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayHighMapId))).Value, productDto.TrayHigh);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickDepthMapId))).Value, productDto.TrayThickDepth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickHeightMapId))).Value, productDto.TrayThickHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickWidthMapId))).Value, productDto.TrayThickWidth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayWideMapId))).Value, productDto.TrayWide);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.WidthMapId))).Value, productDto.Width);

                ProductGroupDto productGroupDto = _productGroups.FirstOrDefault(c => c.Id == productDto.ProductGroupId);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId))).Value.ToString().ToLowerInvariant(), productGroupDto.Code.ToLowerInvariant());

                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDescriptionMapId))).Value, productDto.PointOfPurchaseDescription);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShortDescriptionMapId))).Value, productDto.ShortDescription);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SubCategoryMapId))).Value, productDto.Subcategory);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ColourMapId))).Value, productDto.Colour);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FlavourMapId))).Value, productDto.Flavour);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingShapeMapId))).Value, productDto.PackagingShape);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingTypeMapId))).Value, productDto.PackagingType);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfOriginMapId))).Value, productDto.CountryOfOrigin);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfProcessingMapId))).Value, productDto.CountryOfProcessing);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShelfLifeMapId))).Value, productDto.ShelfLife);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryFrequencyDaysMapId))).Value, productDto.DeliveryFrequencyDays);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryMethodMapId))).Value, productDto.DeliveryMethod);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BrandMapId))).Value, productDto.Brand);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorCodeMapId))).Value, productDto.VendorCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorMapId))).Value, productDto.Vendor);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerCodeMapId))).Value, productDto.ManufacturerCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerMapId))).Value, productDto.Manufacturer);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SizeMapId))).Value, productDto.Size);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.UnitOfMeasureMapId))).Value, productDto.UnitOfMeasure);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateIntroducedMapId))).Value, productDto.DateIntroduced);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateDiscontinuedMapId))).Value, productDto.DateDiscontinued);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateEffectiveMapId))).Value, productDto.DateEffective);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HealthMapId))).Value, productDto.Health);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CorporateCodeMapId))).Value, productDto.CorporateCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BarCodeMapId))).Value, productDto.Barcode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPriceMapId))).Value, productDto.SellPrice);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackCountMapId))).Value, productDto.SellPackCount);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackDescriptionMapId))).Value, productDto.SellPackDescription);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.RecommendedRetailPriceMapId))).Value, productDto.RecommendedRetailPrice);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId))).Value, productDto.ManufacturerRecommendedRetailPrice);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CostPriceMapId))).Value, productDto.CostPrice);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CaseCostMapId))).Value, productDto.CaseCost);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TaxRateMapId))).Value, productDto.TaxRate);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ConsumerInformationMapId))).Value, productDto.ConsumerInformation);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TextureMapId))).Value, productDto.Texture);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StyleNumberMapId))).Value, productDto.StyleNumber);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PatternMapId))).Value, productDto.Pattern);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ModelMapId))).Value, productDto.Model);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GarmentTypeMapId))).Value, productDto.GarmentType);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPrivateLabelMapId))).Value, productDto.IsPrivateLabel);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsNewMapId))).Value, productDto.IsNewProduct);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupCodeMapId))).Value, productDto.FinancialGroupCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupNameMapId))).Value, productDto.FinancialGroupName);
            }
        }

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(IEnumerable<ProductDto> productDtoList, ImportFileData fileData, IDalFactory dalFactory, ImportFileData newFileData, ImportMapping mappingItem)
        {
            Assert.AreEqual(productDtoList.Count(), fileData.Rows.Count);

            #region check for when a property hasn't changed
            foreach (ImportFileDataRow row in fileData.Rows)
            {
                //Get GTIN
                String GTIN = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == ProductImportMappingList.GtinMapId))).Value.ToString();

                //Find corresponding Product dto
                ProductDto productDto = productDtoList.FirstOrDefault(p => p.Gtin == GTIN);
                Assert.IsNotNull(productDto, "Product record should exist");

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateDepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateDepthMapId))).Value, productDto.AlternateDepth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateHeightMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateHeightMapId))).Value, productDto.AlternateHeight);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateWidthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateWidthMapId))).Value, productDto.AlternateWidth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayBackMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayBackMapId))).Value, productDto.CanBreakTrayBack);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayDownMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayDownMapId))).Value, productDto.CanBreakTrayDown);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayTopMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayTopMapId))).Value, productDto.CanBreakTrayTop);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayUpMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayUpMapId))).Value, productDto.CanBreakTrayUp);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CasePackUnitsMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CasePackUnitsMapId))).Value, productDto.CasePackUnits);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DepthMapId))).Value, productDto.Depth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayDepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayDepthMapId))).Value, productDto.DisplayDepth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayHeightMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayHeightMapId))).Value, productDto.DisplayHeight);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayWidthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayWidthMapId))).Value, productDto.DisplayWidth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceAboveMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceAboveMapId))).Value, productDto.FingerSpaceAbove);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceToTheSideMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceToTheSideMapId))).Value, productDto.FingerSpaceToTheSide);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceBottomCapMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceBottomCapMapId))).Value, productDto.ForceBottomCap);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceMiddleCapMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceMiddleCapMapId))).Value, productDto.ForceMiddleCap);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FrontOverhangMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FrontOverhangMapId))).Value, productDto.FrontOverhang);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId))).Value, productDto.Gtin);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HeightMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HeightMapId))).Value, productDto.Height);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsActiveMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsActiveMapId))).Value, productDto.IsActive);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsFrontOnlyMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsFrontOnlyMapId))).Value, productDto.IsFrontOnly);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPlaceHolderProductMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPlaceHolderProductMapId))).Value, productDto.IsPlaceHolderProduct);

              if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxDeepMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxDeepMapId))).Value, productDto.MaxDeep);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxRightCapMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxRightCapMapId))).Value, productDto.MaxRightCap);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxStackMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxStackMapId))).Value, productDto.MaxStack);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxTopCapMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxTopCapMapId))).Value, productDto.MaxTopCap);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId))).Value, ProductMerchandisingStyleHelper.FriendlyNames[(ProductMerchandisingStyle)productDto.MerchandisingStyle]);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MinDeepMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MinDeepMapId))).Value, productDto.MinDeep);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId))).Value, productDto.Name);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingDepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingDepthMapId))).Value, productDto.NestingDepth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingHeightMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingHeightMapId))).Value, productDto.NestingHeight);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingWidthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingWidthMapId))).Value, productDto.NestingWidth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NumberOfPegHolesMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NumberOfPegHolesMapId))).Value, productDto.NumberOfPegHoles);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId))).Value, productDto.OrientationType);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegDepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegDepthMapId))).Value, productDto.PegDepth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId))).Value, productDto.PegProngOffsetX);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegXMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegXMapId))).Value, productDto.PegX);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX2MapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX2MapId))).Value, productDto.PegX2);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX3MapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX3MapId))).Value, productDto.PegX3);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegYMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegYMapId))).Value, productDto.PegY);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY2MapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY2MapId))).Value, productDto.PegY2);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY3MapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY3MapId))).Value, productDto.PegY3);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDepthMapId))).Value, productDto.PointOfPurchaseDepth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseHeightMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseHeightMapId))).Value, productDto.PointOfPurchaseHeight);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseWidthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseWidthMapId))).Value, productDto.PointOfPurchaseWidth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeDepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeDepthMapId))).Value, productDto.SqueezeDepth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeHeightMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeHeightMapId))).Value, productDto.SqueezeHeight);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeWidthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeWidthMapId))).Value, productDto.SqueezeWidth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId))).Value, productDto.StatusType);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayDeepMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayDeepMapId))).Value, productDto.TrayDeep);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayHighMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayHighMapId))).Value, productDto.TrayHigh);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickDepthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickDepthMapId))).Value, productDto.TrayThickDepth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickHeightMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickHeightMapId))).Value, productDto.TrayThickHeight);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickWidthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickWidthMapId))).Value, productDto.TrayThickWidth);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayWideMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayWideMapId))).Value, productDto.TrayWide);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.WidthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.WidthMapId))).Value, productDto.Width);

                ProductGroupDto productGroupDto = _productGroups.FirstOrDefault(c => c.Id == productDto.ProductGroupId);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId))).Value, productGroupDto.Code);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDescriptionMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDescriptionMapId))).Value, productDto.PointOfPurchaseDescription);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShortDescriptionMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShortDescriptionMapId))).Value, productDto.ShortDescription);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SubCategoryMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SubCategoryMapId))).Value, productDto.Subcategory);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ColourMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ColourMapId))).Value, productDto.Colour);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FlavourMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FlavourMapId))).Value, productDto.Flavour);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingShapeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingShapeMapId))).Value, productDto.PackagingShape);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingTypeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingTypeMapId))).Value, productDto.PackagingType);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfOriginMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfOriginMapId))).Value, productDto.CountryOfOrigin);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfProcessingMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfProcessingMapId))).Value, productDto.CountryOfProcessing);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShelfLifeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShelfLifeMapId))).Value, productDto.ShelfLife);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryFrequencyDaysMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryFrequencyDaysMapId))).Value, productDto.DeliveryFrequencyDays);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryMethodMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryMethodMapId))).Value, productDto.DeliveryMethod);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BrandMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BrandMapId))).Value, productDto.Brand);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorCodeMapId))).Value, productDto.VendorCode);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorMapId))).Value, productDto.Vendor);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerCodeMapId))).Value, productDto.ManufacturerCode);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerMapId))).Value, productDto.Manufacturer);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SizeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SizeMapId))).Value, productDto.Size);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.UnitOfMeasureMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.UnitOfMeasureMapId))).Value, productDto.UnitOfMeasure);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateIntroducedMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateIntroducedMapId))).Value, productDto.DateIntroduced);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateDiscontinuedMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateDiscontinuedMapId))).Value, productDto.DateDiscontinued);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateEffectiveMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateEffectiveMapId))).Value, productDto.DateEffective);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HealthMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HealthMapId))).Value, productDto.Health);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CorporateCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CorporateCodeMapId))).Value, productDto.CorporateCode);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BarCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BarCodeMapId))).Value, productDto.Barcode);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPriceMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPriceMapId))).Value, productDto.SellPrice);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackCountMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackCountMapId))).Value, productDto.SellPackCount);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackDescriptionMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackDescriptionMapId))).Value, productDto.SellPackDescription);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.RecommendedRetailPriceMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.RecommendedRetailPriceMapId))).Value, productDto.RecommendedRetailPrice);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId))).Value, productDto.ManufacturerRecommendedRetailPrice);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CostPriceMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CostPriceMapId))).Value, productDto.CostPrice);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CaseCostMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CaseCostMapId))).Value, productDto.CaseCost);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TaxRateMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TaxRateMapId))).Value, productDto.TaxRate);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ConsumerInformationMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ConsumerInformationMapId))).Value, productDto.ConsumerInformation);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TextureMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TextureMapId))).Value, productDto.Texture);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StyleNumberMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StyleNumberMapId))).Value, productDto.StyleNumber);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PatternMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PatternMapId))).Value, productDto.Pattern);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ModelMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ModelMapId))).Value, productDto.Model);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GarmentTypeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GarmentTypeMapId))).Value, productDto.GarmentType);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPrivateLabelMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPrivateLabelMapId))).Value, productDto.IsPrivateLabel);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsNewMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsNewMapId))).Value, productDto.IsNewProduct);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupCodeMapId))).Value, productDto.FinancialGroupCode);

                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupNameMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupNameMapId))).Value, productDto.FinancialGroupName);
            }
            #endregion

            #region check for when a property has changed
            foreach (ImportFileDataRow row in newFileData.Rows)
            {
                //Get GTIN
                String GTIN = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == ProductImportMappingList.GtinMapId))).Value.ToString();

                //Find corresponding Product dto
                ProductDto productDto = productDtoList.FirstOrDefault(p => p.Gtin == GTIN);
                Assert.IsNotNull(productDto, "Product record should exist");

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateDepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateDepthMapId))).Value, productDto.AlternateDepth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateHeightMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateHeightMapId))).Value, productDto.AlternateHeight); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateWidthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.AlternateWidthMapId))).Value, productDto.AlternateWidth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayBackMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayBackMapId))).Value, productDto.CanBreakTrayBack); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayDownMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayDownMapId))).Value, productDto.CanBreakTrayDown); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayTopMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayTopMapId))).Value, productDto.CanBreakTrayTop); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayUpMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CanBreakTrayUpMapId))).Value, productDto.CanBreakTrayUp); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CasePackUnitsMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CasePackUnitsMapId))).Value, productDto.CasePackUnits); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DepthMapId))).Value, productDto.Depth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayDepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayDepthMapId))).Value, productDto.DisplayDepth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayHeightMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayHeightMapId))).Value, productDto.DisplayHeight); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayWidthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DisplayWidthMapId))).Value, productDto.DisplayWidth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceAboveMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceAboveMapId))).Value, productDto.FingerSpaceAbove); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceToTheSideMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FingerSpaceToTheSideMapId))).Value, productDto.FingerSpaceToTheSide); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceBottomCapMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceBottomCapMapId))).Value, productDto.ForceBottomCap); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceMiddleCapMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ForceMiddleCapMapId))).Value, productDto.ForceMiddleCap); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FrontOverhangMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FrontOverhangMapId))).Value, productDto.FrontOverhang); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId))).Value, productDto.Gtin); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HeightMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HeightMapId))).Value, productDto.Height); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsActiveMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsActiveMapId))).Value, productDto.IsActive); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsFrontOnlyMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsFrontOnlyMapId))).Value, productDto.IsFrontOnly); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPlaceHolderProductMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPlaceHolderProductMapId))).Value, productDto.IsPlaceHolderProduct); break;
                }

            if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxDeepMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxDeepMapId))).Value, productDto.MaxDeep); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxRightCapMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxRightCapMapId))).Value, productDto.MaxRightCap); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxStackMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxStackMapId))).Value, productDto.MaxStack); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxTopCapMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MaxTopCapMapId))).Value, productDto.MaxTopCap); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId))).Value, ProductMerchandisingStyleHelper.FriendlyNames[(ProductMerchandisingStyle)productDto.MerchandisingStyle]); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MinDeepMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MinDeepMapId))).Value, productDto.MinDeep); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId))).Value, productDto.Name); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingDepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingDepthMapId))).Value, productDto.NestingDepth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingHeightMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingHeightMapId))).Value, productDto.NestingHeight); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingWidthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NestingWidthMapId))).Value, productDto.NestingWidth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NumberOfPegHolesMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NumberOfPegHolesMapId))).Value, productDto.NumberOfPegHoles); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId))).Value, productDto.OrientationType); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegDepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegDepthMapId))).Value, productDto.PegDepth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId))).Value, productDto.PegProngOffsetX); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegXMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegXMapId))).Value, productDto.PegX); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX2MapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX2MapId))).Value, productDto.PegX2); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX3MapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegX3MapId))).Value, productDto.PegX3); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegYMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegYMapId))).Value, productDto.PegY); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY2MapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY2MapId))).Value, productDto.PegY2); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY3MapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegY3MapId))).Value, productDto.PegY3); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDepthMapId))).Value, productDto.PointOfPurchaseDepth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseHeightMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseHeightMapId))).Value, productDto.PointOfPurchaseHeight); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseWidthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseWidthMapId))).Value, productDto.PointOfPurchaseWidth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeDepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeDepthMapId))).Value, productDto.SqueezeDepth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeHeightMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeHeightMapId))).Value, productDto.SqueezeHeight); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeWidthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SqueezeWidthMapId))).Value, productDto.SqueezeWidth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId))).Value, productDto.StatusType); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayDeepMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayDeepMapId))).Value, productDto.TrayDeep); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayHighMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayHighMapId))).Value, productDto.TrayHigh); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickDepthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickDepthMapId))).Value, productDto.TrayThickDepth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickHeightMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickHeightMapId))).Value, productDto.TrayThickHeight); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickWidthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayThickWidthMapId))).Value, productDto.TrayThickWidth); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayWideMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayWideMapId))).Value, productDto.TrayWide); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.WidthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.WidthMapId))).Value, productDto.Width); break;
                }

                ProductGroupDto productGroupDto = _productGroups.FirstOrDefault(c => c.Id == productDto.ProductGroupId);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId))).Value, productGroupDto.Code);

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDescriptionMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PointOfPurchaseDescriptionMapId))).Value, productDto.PointOfPurchaseDescription); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShortDescriptionMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShortDescriptionMapId))).Value, productDto.ShortDescription); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SubCategoryMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SubCategoryMapId))).Value, productDto.Subcategory); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ColourMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ColourMapId))).Value, productDto.Colour); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FlavourMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FlavourMapId))).Value, productDto.Flavour); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingShapeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingShapeMapId))).Value, productDto.PackagingShape); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingTypeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PackagingTypeMapId))).Value, productDto.PackagingType); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfOriginMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfOriginMapId))).Value, productDto.CountryOfOrigin); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfProcessingMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CountryOfProcessingMapId))).Value, productDto.CountryOfProcessing); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShelfLifeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ShelfLifeMapId))).Value, productDto.ShelfLife); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryFrequencyDaysMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryFrequencyDaysMapId))).Value, productDto.DeliveryFrequencyDays); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryMethodMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DeliveryMethodMapId))).Value, productDto.DeliveryMethod); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BrandMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BrandMapId))).Value, productDto.Brand); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorCodeMapId))).Value, productDto.VendorCode); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.VendorMapId))).Value, productDto.Vendor); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerCodeMapId))).Value, productDto.ManufacturerCode); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerMapId))).Value, productDto.Manufacturer); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SizeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SizeMapId))).Value, productDto.Size); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.UnitOfMeasureMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.UnitOfMeasureMapId))).Value, productDto.UnitOfMeasure); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateIntroducedMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateIntroducedMapId))).Value, productDto.DateIntroduced); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateDiscontinuedMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateDiscontinuedMapId))).Value, productDto.DateDiscontinued); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateEffectiveMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.DateEffectiveMapId))).Value, productDto.DateEffective); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HealthMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HealthMapId))).Value, productDto.Health); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CorporateCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CorporateCodeMapId))).Value, productDto.CorporateCode); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BarCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.BarCodeMapId))).Value, productDto.Barcode); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPriceMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPriceMapId))).Value, productDto.SellPrice); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackCountMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackCountMapId))).Value, productDto.SellPackCount); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackDescriptionMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.SellPackDescriptionMapId))).Value, productDto.SellPackDescription); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.RecommendedRetailPriceMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.RecommendedRetailPriceMapId))).Value, productDto.RecommendedRetailPrice); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId))).Value, productDto.ManufacturerRecommendedRetailPrice); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CostPriceMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CostPriceMapId))).Value, productDto.CostPrice); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CaseCostMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.CaseCostMapId))).Value, productDto.CaseCost); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TaxRateMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TaxRateMapId))).Value, productDto.TaxRate); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ConsumerInformationMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ConsumerInformationMapId))).Value, productDto.ConsumerInformation); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TextureMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TextureMapId))).Value, productDto.Texture); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StyleNumberMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StyleNumberMapId))).Value, productDto.StyleNumber); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PatternMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PatternMapId))).Value, productDto.Pattern); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ModelMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.ModelMapId))).Value, productDto.Model); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GarmentTypeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GarmentTypeMapId))).Value, productDto.GarmentType); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPrivateLabelMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsPrivateLabelMapId))).Value, productDto.IsPrivateLabel); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsNewMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.IsNewMapId))).Value, productDto.IsNewProduct); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupCodeMapId))).Value, productDto.FinancialGroupCode); break;
                }

                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupNameMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.FinancialGroupNameMapId))).Value, productDto.FinancialGroupName); break;
                }
            }
            #endregion

        }

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = ProductImportMappingList.NewProductImportMappingList(true);

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < _numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;

                    if (mappingItem.PropertyIdentifier == ProductImportMappingList.GtinMapId)
                    {
                        dataItem = "code" + i.ToString();
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.NameMapId)
                    {
                        dataItem = "name" + i.ToString();
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId)
                    {
                        dataItem = _productGroups[rand.Next(0, _productGroups.Count - 1)].Code;
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId)
                    {
                        dataItem = Convert.ToByte(0);
                            //PlanogramProductStatusTypeHelper.FriendlyNames[PlanogramProductStatusType.Active].ToLowerInvariant();
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId)
                    {
                        dataItem = Convert.ToByte(0);
                            //PlanogramProductOrientationTypeHelper.FriendlyNames[PlanogramProductOrientationType.Front0].ToLowerInvariant();
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId)
                    {
                        dataItem = ProductMerchandisingStyleHelper.FriendlyNames[ProductMerchandisingStyle.Case];
                    }
                    else
                    {
                        dataItem = base.GenerateRandomData(mappingItem);
                    }


                    importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem));
                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateNonRandomData(Int32 numDtosToInsert, Boolean isNew)
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = ProductImportMappingList.NewProductImportMappingList(true);

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Int32 rowNumber = 1;
            for (int i = 0; i < numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;
                    if (mappingItem.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId)
                    {
                        dataItem = _productGroups[i].Code;
                    }
                    else if (mappingItem.PropertyIsMandatory)
                    {
                        //mandatory fields don't need to be changed for mapping test
                        dataItem = GenerateData(mappingItem, false);
                    }
                    else
                    {
                        dataItem = GenerateData(mappingItem, isNew);
                    }

                    ImportFileDataCell importCell =
                                ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                    importRow.Cells.Add(importCell);

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private Object GenerateData(ImportMapping mappingItem, Boolean isNew)
        {
            Object returnValue = null;

            //Get type
            Type t = null;
            if (mappingItem.PropertyType.IsGenericType && mappingItem.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                t = Nullable.GetUnderlyingType(mappingItem.PropertyType);
            }
            else
            {
                t = mappingItem.PropertyType;
            }

            if (!isNew)
            {
                //Generate data
                if (t == typeof(Boolean))
                {
                    returnValue = false;
                }
                else if (t == typeof(DateTime))
                {
                    returnValue = DateTime.Today;
                }
                else if (t == typeof(String))
                {
                    returnValue = "White";
                }
                else if (mappingItem.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId)
                {
                    returnValue = Convert.ToByte(0);
                        //PlanogramProductStatusTypeHelper.FriendlyNames[PlanogramProductStatusType.Promo];
                }
                else if (mappingItem.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId)
                {
                    returnValue = Convert.ToByte(0);
                        //PlanogramProductOrientationTypeHelper.FriendlyNames[PlanogramProductOrientationType.Bottom0];
                }
                else if (mappingItem.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId)
                {
                    returnValue = ProductMerchandisingStyleHelper.FriendlyNames[ProductMerchandisingStyle.Case];
                }
                else
                {
                    returnValue = 0;
                }
            }
            else
            {//Generate data
                if (t == typeof(Boolean))
                {
                    returnValue = true;
                }
                else if (t == typeof(DateTime))
                {
                    returnValue = DateTime.Today;
                }
                else if (t == typeof(String))
                {
                    returnValue = "Black";
                }
                else if (mappingItem.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId)
                {
                    returnValue = Convert.ToByte(0);
                        //PlanogramProductStatusTypeHelper.FriendlyNames[PlanogramProductStatusType.Active].ToLowerInvariant();
                }
                else if (mappingItem.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId)
                {
                    returnValue = Convert.ToByte(0);
                        //PlanogramProductOrientationTypeHelper.FriendlyNames[PlanogramProductOrientationType.Top0].ToLowerInvariant();
                }
                else if (mappingItem.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId)
                {
                    returnValue = ProductMerchandisingStyleHelper.FriendlyNames[ProductMerchandisingStyle.Display];
                }
                else
                {
                    returnValue = 1;
                }
            }

            return returnValue;
        }

        #endregion

        #region Import

        /// <summary>
        /// Import product data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);

                //Create data table
                ImportFileData fileData;

                #region Case 1 - Valid Import

                //Create data
                fileData = CreateData();

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportProductsProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(importProcess);

                //Check imported data matches inserted.
                ValidateImportedData(fileData, dalFactory);

                #endregion

                #region Case 2 - [GEM21865] ParentRecord match is case insensitive
                {
                    #region ProductGroup
                    {
                        fileData = CreateData();

                        Int32 productGroupColNum =
                                fileData.GetMappedColNumber(
                                fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId));

                        foreach (ImportFileDataRow row in fileData.Rows)
                        {
                            ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == productGroupColNum);
                            cell.Value = cell.Value.ToString().ToUpperInvariant();
                        }

                        //construct and execute process
                        importProcess =
                            ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(
                             new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData));

                        ValidateImportedData(fileData, dalFactory);
                    }
                    #endregion

                    #region Product
                    {
                        //get only the first row.
                        fileData = CreateData();
                        var row = fileData.Rows.First();
                        fileData.Rows.Clear();
                        fileData.Rows.Add(row);

                        Int32 preProductCount;
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (IProductDal dal = dalContext.GetDal<IProductDal>())
                            {
                                preProductCount = dal.FetchByEntityId(1).Count();
                            }
                        }

                        importProcess =
                            ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(
                             new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData));

                        ValidateImportedData(fileData, dalFactory);

                        Int32 postProductCount;
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (IProductDal dal = dalContext.GetDal<IProductDal>())
                            {
                                postProductCount = dal.FetchByEntityId(1).Count();
                            }
                        }

                        Assert.AreEqual(preProductCount, postProductCount);

                    }
                    #endregion


                }
                #endregion
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Upsert product data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Upsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                Random random = new Random();
                Int32 numDtosToInsert = 1;

                //Generate random count of dtos
                _productGroups.Clear();

                //Create levels
                List<ProductLevelDto> levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, 1, 5);
                //Create product groups based on levels
                _productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, levelDtos, 200);

                //Create data table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create data
                fileData = CreateNonRandomData(numDtosToInsert, false);

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportProductsProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(importProcess);

                //Fetch all data that exists in the db
                IEnumerable<ProductDto> productDtoList;

                //Check imported data matches inserted.
                ValidateImportedData(fileData, dalFactory);

                //upsert
                ImportFileDataRow previousRow = fileData.Rows[0];

                //create a second row
                ImportFileData fileData2 = ImportFileData.NewImportFileData();

                fileData2 = CreateNonRandomData(numDtosToInsert, true);

                ImportFileDataRow newRow = fileData2.Rows[0];

                //set the rows to be the same
                for (int i = 0; i < previousRow.Cells.Count; i++)
                {
                    newRow.Cells[i].Value = previousRow.Cells[i].Value;
                }


                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;
                    if (mappingItem.PropertyIdentifier == ProductImportMappingList.GtinMapId ||
                        mappingItem.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId)
                    {
                    }
                    else
                    {
                        dataItem = GenerateData(mappingItem, true);

                        newRow.Cells[cellColNumber - 1] =
                                    ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);

                        //Construct process
                        importProcess = new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData2);

                        // execute the import process
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(importProcess);

                        //Check imported data matches inserted.
                        ValidateImportedData(fileData2, dalFactory);

                        newRow.Cells[cellColNumber - 1].Value = previousRow.Cells[cellColNumber - 1].Value;
                    }
                    cellColNumber++;
                }

                //check mappings
                //reset what's in the database to the origional file
                //Construct process
                importProcess = new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(importProcess);

                fileData2 = CreateNonRandomData(numDtosToInsert, true);

                newRow = fileData2.Rows[0];

                foreach (ImportMapping mapping in fileData2.MappingList)
                {
                    if (!mapping.PropertyIsMandatory)
                    {
                        mapping.ColumnReference = null;
                    }
                }

                cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    if (!mappingItem.PropertyIsMandatory)
                    {
                        ImportMapping setMapping = fileData2.MappingList.First(m => m.PropertyIdentifier == mappingItem.PropertyIdentifier);
                        setMapping.ColumnReference = mappingItem.ColumnReference;

                        //Construct process
                        importProcess = new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData2);

                        // execute the import process
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(importProcess);

                        //fetch products
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (IProductDal dal = dalContext.GetDal<IProductDal>())
                            {
                                productDtoList = dal.FetchByEntityId(1);
                            }
                        }

                        //Check imported data matches inserted.
                        ValidateImportedData(productDtoList, fileData, dalFactory, fileData2, mappingItem);
                        setMapping.ColumnReference = null;

                        //Construct process
                        importProcess = new Galleria.Ccm.Imports.Processes.ImportProductsProcess(1, fileData);

                        // execute the import process
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(importProcess);
                    }
                    cellColNumber++;
                }
            }
        }

        #endregion
    }
}
