﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25554 : L.Hodson
//		Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using System.Data;
using NUnit.Framework;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public sealed class ValidateLocationHierarchyProcess : TestBase
    {
        #region Fields
        Int32 _numDtosToInsert = 200;
        private Int32 _locHierarchyId;
        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            IEnumerable<String> levelNames = LocationHierarchy.FetchByEntityId(1)
                .EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();

            fileData.MappingList = LocationHierarchyImportMappingList.NewList(levelNames);

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < _numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;

                    dataItem = mappingItem.PropertyName + i;

                    ImportFileDataCell importCell =
                                ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                    importRow.Cells.Add(importCell);

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private EntityDto InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            Int32 entityId = entityDtos[0].Id;

            //location hierarchy 
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityId);
            List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);

            _locHierarchyId = locHierarchyDtos[0].Id;

            //only add the root group
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dal.Insert(new LocationGroupDto()
                    {
                        Code = "x",
                        LocationHierarchyId = locHierarchyDtos[0].Id,
                        LocationLevelId = level1Dtos[0].Id,
                        Name = "root",
                        DateCreated = DateTime.UtcNow,
                        DateLastModified = DateTime.UtcNow
                    });
                }
            }


            return entityDtos[0];
        }

        #endregion

        #region Validate

        [Test, TestCaseSource("DalFactoryTypes")]
        public void Validate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);

                #region Case 1 - Valid Data
                //Create Data Table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create Data
                fileData = CreateData();

                //Construct Process
                Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess case1ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(case1ValidateProcess);

                //Validate process once completed
                Assert.AreEqual(0, case1ValidateProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(case1ValidateProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, case1ValidateProcess.ValidatedData.Rows.Count, "Output Row Count Must Match Input Row Count");

                #endregion

                #region Case 2 - Invalid data type

                //no properties can have an invalid data type

                #endregion

                #region Case 3 - Missing Data

                #region test non mandatory property

                //all mandatory fields

                #endregion

                #region Test Mandatory property

                //Create data
                fileData = CreateData();

                //Set missing data
                //Set merchandising hiearchy category name data to be null
                Int32 invalidRowNumber = 2;
                Int32 invalidColumnNumber = fileData.Columns.Count; //want last column
                Object invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess case31ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(case31ValidateProcess);

                //Get error item
                ValidationErrorItem errorItem = case31ValidateProcess.Errors.First().Errors.First();

                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.Columns.First(p => p.ColumnNumber == invalidColumnNumber).Header, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(String.Empty, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error as column is mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                #endregion

                #endregion

                #region Case 4 - Invalid Data Length

                #region Max Length

                //Create data
                fileData = CreateData();

                //Set invalid length data
                //Set category name data to be a string value that has too many characters
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.Columns.Count; //want last column
                string invalidData = new string('*', 101);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess case4ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(case4ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case4ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                ValidationErrorGroup errorGroup = case4ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_InvalidLengthHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.Columns.First(p => p.ColumnNumber == invalidColumnNumber).Header, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_AboveMaxLengthErrorMessage, 100), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case4ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case4ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Min Length

                //No string property with min length greater than 1 for merchandising hierarchy. If you 
                //set a string to have a length of 0, it is caught by the missing data validation.

                #endregion

                #endregion

                #region Case 5 - Out of Bounds Data

                // N/A to this type, the only data validated is strings, which are caught in the 
                // invalid data length checks.

                #endregion

                #region Case 6 - Parent Record

                // N/A to this type, there are no parent record checks in this process.

                #endregion

                #region Case 7 - Duplicate Record

                //Create data
                fileData = CreateData();

                //Choose a row to get original data from
                Int32 originalRowNumber = 4;
                invalidRowNumber = 5;
                invalidColumnNumber = fileData.Columns.Count - 1; //want 2nd to last column

                //Set invalid duplicate data
                Object duplicateData = fileData.Rows.First(p => p.RowNumber == originalRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value;
                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = duplicateData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess case7ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(case7ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case7ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case7ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");

                //Check details of these 2 errors
                foreach (ValidationErrorItem error in errorGroup.Errors)
                {
                    //Check row numbers (compensate for zero based index and row row being headers -> +2)
                    Assert.IsTrue((error.RowNumber == (originalRowNumber) || error.RowNumber == (invalidRowNumber)) ? true : false, "Row number should be one of the 2 duplicate rows");

                    //Check column number
                    Assert.AreEqual((invalidColumnNumber), error.ColumnNumber, "Column number should match the invalid column's");

                    //Check data affected
                    Assert.AreEqual(duplicateData.ToString(), error.DataAffected, "Data affected should match duplicate data");

                    //Check duplicate error flag
                    Assert.IsTrue(error.IsDuplicateError, "Should be marked as duplicate error");
                }

                //Validate process properties
                Assert.IsNotNull(case7ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case7ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion
            }
        }

        #endregion

        #region Ticket

        /// <summary>
        /// Scenario: Import a new group with a code that is case different to an existing one.
        /// Expected: Validation Error.
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM21865_ExistingGroupCheckIsCaseInsensitive(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                ///intert some dtos into the data source
                EntityDto entityDto = InsertDtos(dalFactory);
                Int32 entityId = entityDto.Id;

                //import valid data to start
                ImportFileData fileData = CreateData();

                var validationProcess =
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                Assert.IsTrue(validationProcess.ValidatedData.Rows.All(r => r.Cells[0].Value.ToString() == "Add"));

                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess(entityId, fileData));


                //get the number of groups added.
                Int32 locationGroupCount = 0;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        locationGroupCount = dal.FetchByLocationHierarchyId(_locHierarchyId).Count();
                    }
                }

                List<Int32> checkColNumbers = new List<Int32> { 1, 3 };

                //+ lower invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToLowerInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");

                    Assert.IsTrue(validationProcess.ValidatedData.Rows.All(r => r.Cells[0].Value.ToString() == "Update"));
                }

                //+upper invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");

                    Assert.IsTrue(validationProcess.ValidatedData.Rows.All(r => r.Cells[0].Value.ToString() == "Update"));
                }

            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22005_DuplicateCodes(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //start with a 4 level hierarchy
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityId);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 1);
                List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 1);


                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

                //add levels till we have 4
                LocationLevel level2 = LocationLevel.NewLocationLevel();
                level2.Name = "Level2";
                hierarchy.RootLevel.ChildLevel = level2;

                LocationLevel level3 = LocationLevel.NewLocationLevel();
                level3.Name = "Level3";
                level2.ChildLevel = level3;

                LocationLevel level4 = LocationLevel.NewLocationLevel();
                level4.Name = "Level4";
                level3.ChildLevel = level4;

                hierarchy = hierarchy.Save();

                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();

                ImportFileData fileData;

                #region Case 1 duplicates on same row different levels

                fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));

                //create row:
                var data = new List<String[]>();
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "group1", "Adult Dry Dog", "group1", "Test1" });
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "Group2", "Adult Wet Dog", "Group2", "Test2" });
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "GROUP3", "Aircare", "GROUP3", "Test3" });
                //data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Baked Beans", "1", "Name1" });
                //data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Bleach", "C00000446", "Test4" });
                AddDataRows(fileData, data);


                //construct and execute process
                var validatationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(1, validatationProcess.Errors.Where(err => err.Type == ValidationErrorType.Error).Count(), "Should have errors");
                Assert.AreEqual(3, validatationProcess.Errors[0].Errors.Count(), "Should have errors");

                #endregion

                #region Case 2 duplicates on different rows different levels

                fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));

                //create rows:
                data = new List<String[]>();
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Baked Beans", "1", "Name1" });
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Bleach", "C00000446", "Test4" });
                AddDataRows(fileData, data);

                //construct and execute process
                validatationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreNotEqual(0, validatationProcess.Errors.Where(err => err.Type == ValidationErrorType.Error).Count(), "Should have errors");

                #endregion
            }
        }

        /// <summary>
        /// Scenario: A new group is to be added which has the same code as one marked deleted.
        /// Expected: Duplicate code validation error.
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22339_DeletedGroupCodesCannotBeReintroduced(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                //create a hierarchy with 2 levels and 1 group below the root
                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityId);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 1);
                List<LocationGroupDto> groupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 1);

                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

                LocationLevel level = LocationLevel.NewLocationLevel();
                level.Name = "level2";
                hierarchy.RootLevel.ChildLevel = level;

                LocationGroup newGroup = LocationGroup.NewLocationGroup(hierarchy.RootLevel.ChildLevel.Id);
                newGroup.Code = "GEM22339";
                newGroup.Name = "GEM22339_Group";
                hierarchy.RootGroup.ChildList.Add(newGroup);
                hierarchy = hierarchy.Save();

                ////assign the current user to the location group so that it will only get marked deleted
                //// when we delete it later.
                //User usr = User.GetCurrentUser();
                //usr.LocationGroupId = hierarchy.RootGroup.ChildList[0].Id;
                //usr = usr.Save();

                //delete that group.
                hierarchy.RootGroup.ChildList.Clear();
                hierarchy = hierarchy.Save();

                //create some data that tries to reimport the deleted code
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { newGroup.Code, "GEM22339_GroupNew" });
                AddDataRows(fileData, data);

                //construct and execute process
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                //check
                Assert.AreEqual(1, validationProcess.Errors.Count, "1 error should have been found");
                Assert.AreEqual(1, validationProcess.Errors[0].Errors.Count, "1 error should have been found");
                Assert.AreEqual(1, validationProcess.Errors[0].Errors[0].RowNumber, "Error should be for row 1");

            }
        }

        /// <summary>
        /// Scenario: A new group is to be added as a child of an existing one with locations.
        /// Expected: No validation errors.
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22339_AddingNewChildBeneathGroupWithLocations(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                //create a hierarchy with 2 levels and 1 group below the root
                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 2);
                List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 1);

                //add locations against the lower group
                List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, entityId,  new List<LocationGroupDto> { locGroupDtos.Last() }, 5);

                //load the hierarchy and add a new level
                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);
                LocationLevel parentLevel = hierarchy.RootLevel.ChildLevel;
                LocationLevel newLevel = LocationLevel.NewLocationLevel();
                newLevel.Name = hierarchy.GetNextAvailableLevelName(parentLevel);
                hierarchy.AddLevel(newLevel, parentLevel);
                hierarchy = hierarchy.Save();

                LocationGroup parentGroup = hierarchy.RootGroup.ChildList[0];

                //create the file data to add a new group beneath the group with locations
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { parentGroup.Code, parentGroup.Name, "GEM22339", "GEM22339_NewGroup" });
                AddDataRows(fileData, data);

                //construct and execute process
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No errors should have been found");
            }
        }

        /// <summary>
        /// Scenario: An existing group is to be moved beneath one with locations assigned.
        /// Expected: Validation error.
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22339_MovingChildBeneathGroupWithLocations(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                //create a hierarchy with 2 levels and 1 group below the root
                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 2);
                List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 2);

                //add locations against the lower group
                List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, entityId, 
                   new List<LocationGroupDto> { locGroupDtos.Last() }, 5);

                //load the hierarchy and add a new level
                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);
                LocationLevel parentLevel = hierarchy.RootLevel.ChildLevel;
                LocationLevel newLevel = LocationLevel.NewLocationLevel();
                newLevel.Name = hierarchy.GetNextAvailableLevelName(parentLevel);
                hierarchy.AddLevel(newLevel, parentLevel);
                hierarchy = hierarchy.Save();

                LocationGroup parentGroup = hierarchy.RootGroup.ChildList.First(g => g.AssignedLocationsCount > 0);
                LocationGroup moveGroup = hierarchy.RootGroup.ChildList.First(g => g.AssignedLocationsCount == 0);

                //create the file data to move the group
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { parentGroup.Code, parentGroup.Name, moveGroup.Code, moveGroup.Name });
                AddDataRows(fileData, data);

                //construct and execute process
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(1, validationProcess.Errors.Count, "An error should have been found");
            }


        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22916_DuplicateExistingOnLowerLevel(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                //create a hierarchy with 2 levels and 1 group below the root
                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 2);
                List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 2);

                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

                //add some more levels
                LocationLevel level2 = LocationLevel.NewLocationLevel();
                level2.Name = "Department";
                hierarchy.RootLevel.ChildLevel = level2;

                LocationLevel level3 = LocationLevel.NewLocationLevel();
                level3.Name = "Section";
                level2.ChildLevel = level3;

                LocationLevel level4 = LocationLevel.NewLocationLevel();
                level4.Name = "Planogram Class";
                level3.ChildLevel = level4;

                LocationLevel level5 = LocationLevel.NewLocationLevel();
                level5.Name = "Planogram Class L1";
                level4.ChildLevel = level5;


                //add groups
                LocationGroup ambient = LocationGroup.NewLocationGroup(level2.Id);
                ambient.Name = "ambient";
                ambient.Code = "MD1";
                hierarchy.RootGroup.ChildList.Add(ambient);

                LocationGroup grocery = LocationGroup.NewLocationGroup(level3.Id);
                grocery.Name = "grocery";
                grocery.Code = "MS1";
                ambient.ChildList.Add(grocery);

                LocationGroup cereals = LocationGroup.NewLocationGroup(level4.Id);
                cereals.Name = "cereals";
                cereals.Code = "98764";
                grocery.ChildList.Add(cereals);

                LocationGroup crunchyNut = LocationGroup.NewLocationGroup(level5.Id);
                crunchyNut.Name = "crunchyNut";
                crunchyNut.Code = "98765";
                cereals.ChildList.Add(crunchyNut);

                hierarchy = hierarchy.Save();


                //Create data file but give cereals the code for crunchy nut and crunchy nut a new code entirely.

                //create file data to import 1 group against level 2.
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { "MD1", "ambient", "MS1", "grocery", "98765", "cereals", "88866", "crunchyNut" });
                AddDataRows(fileData, data);


                //check validation - should fail.
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreNotEqual(0, validationProcess.Errors.Count(e => e.Type == ValidationErrorType.Error));
            }


        }

        #endregion
    }
}
