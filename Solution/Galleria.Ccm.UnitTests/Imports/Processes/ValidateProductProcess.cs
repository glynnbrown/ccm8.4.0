﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25452 : N.Haywood
//	Added from SA
// V8-26041 : A.Kuszyk
//      Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
//      Changed use of CCM ProductOrientationType to Framework PlanogramProductOrientationType.
#endregion
#region Version History: CCM802
// V8-29230 : N.Foster
//  Removed unused parameter from import products process
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Processes;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ValidateProductProcess : TestBase
    {
        #region Fields
        List<ProductGroupDto> _productGroups;
        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData(Int32 numOfDtos = 200)
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = ProductImportMappingList.NewProductImportMappingList(true);

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < numOfDtos; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;

                    if (mappingItem.PropertyIdentifier == ProductImportMappingList.GtinMapId)
                    {
                        dataItem = "code" + i.ToString();
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.NameMapId)
                    {
                        dataItem = "name" + i.ToString();
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId)
                    {
                        dataItem = _productGroups[rand.Next(0, _productGroups.Count - 1)].Code;
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId)
                    {
                        dataItem = PlanogramProductStatusType.Active;// ProductStatusTypeHelper.FriendlyNames[ProductStatusType.Active];
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId)
                    {
                        dataItem = PlanogramProductOrientationType.Front0;//ProductOrientationTypeHelper.FriendlyNames[ProductOrientationType.Front0];
                    }
                    else if (mappingItem.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId)
                    {
                        dataItem = ProductMerchandisingStyle.Case;//ProductMerchandisingStyleHelper.FriendlyNames[ProductMerchandisingStyle.Case];
                    }
                    else
                    {
                        dataItem = base.GenerateRandomData(mappingItem);
                    }


                    importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem));
                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private EntityDto InsertDtos(IDalFactory dalFactory)
        {

            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);


            //merch hierarchy
            List<ProductHierarchyDto> productHierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);
            List<ProductLevelDto> productLevel2Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDtos[0].Id, 3);
            List<ProductGroupDto> productGroup2Dtos = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel2Dtos, 2);


            _productGroups = productGroup2Dtos;

            return entityDtos[0];
        }

        #endregion

        #region Validate

        /// <summary>
        /// Import and validate product data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Validate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);

                #region Case 1 - Valid Data
                //Create Data Table
                ImportFileData fileData = CreateData();

                //Construct Process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case1ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case1ValidateProcess);

                //Validate the Process Once Completed
                Assert.AreEqual(0, case1ValidateProcess.Errors.Count, "No Errors Should have Been Found");
                Assert.IsNotNull(case1ValidateProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, case1ValidateProcess.ValidatedData.Rows.Count, "Output Row Count Must Match Input Row Count");

                #endregion

                #region Case 2 - Invalid Data Type

                #region Single Error

                //Create Data
                fileData = CreateData();

                //Set invalid data type for a row
                //Set Height to String
                Int32 invalidRowNumber = 1;
                Int32 invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayHighMapId));
                String invalidData = "xx";

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct Process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case2ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case2ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case2ValidateProcess.Errors.Count, "1 Error Group Should Have Been Found");

                //Get error group
                ValidationErrorGroup errorGroup = case2ValidateProcess.Errors.First();
                //Check Properties
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectDataTypeHeader, errorGroup.Description, "Error Added to Wrong Group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 Error Should Exist");

                //Get error item
                ValidationErrorItem errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index and first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column Number Should Match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.TrayHighMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Warning, errorItem.ErrorType, "Item should be a warning");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case2ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case2ValidateProcess.ValidatedData.Rows.Count, "Output row count should match input");

                #endregion

                #region > 10% Errors

                //Create Data
                fileData = CreateData(2000);

                //Set invalid data type for all rows
                //Set Height to String
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.HeightMapId));
                invalidData = Guid.NewGuid().ToString();

                foreach (ImportFileDataRow row in fileData.Rows)
                {
                    row.Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;
                }

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case21ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case21ValidateProcess);


                //validate process once complete
                Assert.AreEqual(1, case21ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case21ValidateProcess.Errors.First();
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectDataTypeHeader, errorGroup.Description, "Error added to the wrong group");
                //Check column is marked as invalid
                Assert.IsTrue(errorGroup.InvalidColumnNumbers.Contains(invalidColumnNumber));
                Assert.IsTrue(errorGroup.ContainsInvalidColumnErrorsWarnings, "Should be marked as having invalid columns");

                //Check that 10% of rows limit has been met to invalidate column
                Double maxColumnErrorCount = ((Double)fileData.Rows.Count * 0.10);
                Assert.GreaterOrEqual(errorGroup.Errors.Count, maxColumnErrorCount);

                #endregion

                #endregion

                #region Case 3 - Missing Data

                #region Test Non Mandatory Property

                //create data
                fileData = CreateData();

                //Set missing data
                //Set Height to be missing
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegXMapId));
                object invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case3ValidateProcess
                                = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case3ValidateProcess);


                //Validate the process once completed
                Assert.AreEqual(1, case3ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case3ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingDataHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegXMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual("<empty>", errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Warning, errorItem.ErrorType, "Item should be a warning as column is not mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case3ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case3ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Test Mandatory Property
                //Create data
                fileData = CreateData();

                //Set missing data
                //Set store size data to be a string value
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId));
                invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case31ValidateProcess
                                = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case31ValidateProcess);

                //Get error item
                errorItem = case31ValidateProcess.Errors.First().Errors.First();

                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual("<empty>", errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error as column is mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                #endregion

                #endregion

                #region Case 4 - Invalid Data Length

                #region Max Length

                //Create data
                fileData = CreateData();

                //Set invalid length data
                //Set store county data to be a string value
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId));
                invalidData = new string('*', 101);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case4ValidateProcess
                                = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case4ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case4ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case4ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_InvalidLengthHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_AboveMaxLengthErrorMessage, fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.NameMapId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case4ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case4ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Min Length

                //No string property with min length greater than 1 for Product. If you set a string to have a length
                //of 0, it is caught by the missing data validation.

                #endregion

                #endregion

                #region Case 5 - Out of Bounds Data

                #region Upper Bound
                //All product mappings with a upper bound use the max value for their type, this cannot be exceded
                #endregion

                #region Lower Bound
                //Create data
                fileData = CreateData();

                //Set invalid length data
                //Set product peg prong data to be a number exceeding its upper bound of 180
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId));
                float invalidIntData = -1;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidIntData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case51ValidateProcess
                                = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case51ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case51ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case51ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_OutOfBoundsHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_LowerBoundErrorMessage, fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId).PropertyMin), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.PegProngOffsetXMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidIntData.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case51ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case51ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #endregion

                #region Case 6 - Parent Record

                //Create Data
                fileData = CreateData();

                //Set incorrect parent record
                //Set category code to nonexistent category
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId));
                String invalidCategoryCode = "j";

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidCategoryCode;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case6ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case6ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case6ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case6ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingParentRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");
                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidCategoryCode.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case6ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case6ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion

                #region Case 7 - Duplicate Record

                //Create data
                fileData = CreateData();

                //Choose a row to get original data from
                Int32 originalRowNumber = 4;
                invalidRowNumber = 5;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId));

                //Set invalid duplicate data
                Object duplicateData = fileData.Rows.First(p => p.RowNumber == originalRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value;
                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = duplicateData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case7ValidateProcess
                                = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case7ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case7ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case7ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual("Invalid identifying data", errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");

                //Check details of these 2 errors
                foreach (ValidationErrorItem error in errorGroup.Errors)
                {
                    //Check row numbers (compensate for zero based index and row row being headers -> +2)
                    Assert.IsTrue((error.RowNumber == (originalRowNumber) || error.RowNumber == (invalidRowNumber)) ? true : false, "Row number should be one of the 2 duplicate rows");

                    //Check column number
                    Assert.AreEqual((invalidColumnNumber), error.ColumnNumber, "Column number should match the invalid column's");

                    //Check data affected
                    Assert.AreEqual(duplicateData.ToString(), error.DataAffected, "Data affected should match duplicate data");

                    //Check duplicate error flag
                    Assert.IsTrue(error.IsDuplicateError, "Should be marked as duplicate error");
                }

                //Validate process properties
                Assert.IsNotNull(case7ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case7ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion

                #region Case 8 - Invalid Enum Type

                #region Status Type
                //Create Data
                fileData = CreateData();

                //Set incorrect parent record
                //Set status type to be nonexistent enum
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId));
                Byte invalidStatusType = 255;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidStatusType;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case8ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case8ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case8ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case8ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectEnumRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");
                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.StatusTypeMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidStatusType.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case8ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case8ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Orientation Type
                //Create Data
                fileData = CreateData();

                //Set incorrect parent record
                //Set status type to be nonexistent enum
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId));
                Byte invalidOrientationType = 52;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidOrientationType;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case81ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case81ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case81ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case81ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectEnumRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");
                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.OrientationTypeMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidOrientationType.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case81ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case81ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Merchandising Style Type
                //Create Data
                fileData = CreateData();

                //Set incorrect parent record
                //Set status type to be nonexistent enum
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId));
                Byte invalidMerchandisingStyleType = 61;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidMerchandisingStyleType;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateProductProcess case82ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateProductProcess(1, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(case82ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case82ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case82ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectEnumRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");
                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.MerchandisingStyleMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidMerchandisingStyleType.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case82ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case82ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #endregion
            }
        }

        #endregion

        #region Ticket

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM21865_ParentRecordCheckIsCaseInsensitive(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                ///intert some dtos into the data source
                EntityDto entityDto = InsertDtos(dalFactory);
                Int32 entityId = entityDto.Id;

                #region check valid data
                ImportFileData fileData = CreateData();

                var validationProcess =
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateProductProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                #endregion

                List<Int32> checkColNumbers =
                    new List<Int32>()
                    {
                        //ProductGroup
                        fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId)),

                        //ProductUniverse - done seperately below.
                    };

                //+ lower invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToLowerInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateProductProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }

                //+upper invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateProductProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }


                #region Product

                //import the data
                fileData = CreateData();

                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportProductsProcess>(
                        new Galleria.Ccm.Imports.Processes.ImportProductsProcess(entityId, fileData));



                Int32 productGtinColNum =
                fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId));

                fileData = CreateData();

                //remove all rows except the first
                var firstRow = fileData.Rows[0];
                fileData.Rows.Clear();
                fileData.Rows.Add(firstRow);

                ImportFileDataCell universeNameCell = firstRow.Cells.First(p => p.ColumnNumber == productGtinColNum);
                universeNameCell.Value = universeNameCell.Value.ToString().ToLowerInvariant();

                validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateProductProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                Assert.AreEqual("Update", validationProcess.ValidatedData.Rows[0].Cells[0].Value, "Row should be an update");

                //same again but as uppercase universe name
                fileData = CreateData();

                firstRow = fileData.Rows[0];
                fileData.Rows.Clear();
                fileData.Rows.Add(firstRow);

                universeNameCell = firstRow.Cells.First(p => p.ColumnNumber == productGtinColNum);
                universeNameCell.Value = universeNameCell.Value.ToString().ToUpperInvariant();

                validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateProductProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateProductProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                Assert.AreEqual("Update", validationProcess.ValidatedData.Rows[0].Cells[0].Value, "Row should be an update");

                #endregion


            }


        }

        #endregion
    }
}
