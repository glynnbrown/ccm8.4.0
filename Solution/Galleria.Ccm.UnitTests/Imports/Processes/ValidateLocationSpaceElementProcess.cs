﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Imports.Mappings;
using NUnit.Framework;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ValidateLocationSpaceElementProcess : TestBase
    {

        #region Fields

        List<ProductGroupDto> _productGroupDtoList = new List<ProductGroupDto>();
        List<LocationDto> _locationDtoList = new List<LocationDto>();
        List<LocationSpaceDto> _locationSpaceDtos = new List<LocationSpaceDto>();
        List<LocationSpaceProductGroupDto> _locationSpaceProductGroupDtos = new List<LocationSpaceProductGroupDto>();
        List<LocationSpaceBayDto> _locationSpaceBayDtos = new List<LocationSpaceBayDto>();

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationSpaceElementImportMappingList.NewLocationSpaceElementImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            foreach (LocationSpaceBayDto locationSpaceBay in _locationSpaceBayDtos)
            {
                //Find bays product group \ location
                LocationSpaceProductGroupDto locationSpaceProductGroupDto = _locationSpaceProductGroupDtos.Where(p => p.Id == locationSpaceBay.LocationSpaceProductGroupId).FirstOrDefault();
                LocationSpaceDto locationSpaceDto = _locationSpaceDtos.Where(p => p.Id == locationSpaceProductGroupDto.LocationSpaceId).FirstOrDefault();
                ProductGroupDto productGroup = _productGroupDtoList.Where(p => p.Id == locationSpaceProductGroupDto.ProductGroupId).FirstOrDefault();
                LocationDto location = _locationDtoList.Where(p => p.Id == locationSpaceDto.LocationId).FirstOrDefault();

                for (byte i = 1; i <= 2; i++)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        Object dataItem = null;
                        if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId)
                        {
                            dataItem = productGroup.Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId)
                        {
                            dataItem = location.Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.BayOrderMappingId)
                        {
                            dataItem = locationSpaceBay.Order;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.OrderMappingId)
                        {
                            dataItem = i;
                        }
                        else
                        {
                            dataItem = base.GenerateRandomData(mappingItem);
                        }

                        importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem));
                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }
            }

            return fileData;
        }


        private void InsertDtos(IDalFactory dalFactory)
        {
            //Create test data
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            Int32 entityId = entityDtos[0].Id;

            //merch hierarchy
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, 1, 3);
            List<ProductGroupDto> prodGroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);

            //location hierarchy
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
            List<LocationLevelDto> locLevelsDtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, locLevelsDtos, 2);
            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, entityId, locGroupDtos, 5);


            //location space
            List<LocationSpaceDto> locSpaceDtos = TestDataHelper.InsertLocationSpaceDtos(dalFactory, locDtos, entityId);

            List<LocationSpaceProductGroupDto> locSpaceProductGroupDtos = new List<LocationSpaceProductGroupDto>();
            foreach (LocationSpaceDto locationSpaceDto in locSpaceDtos)
            {
                locSpaceProductGroupDtos.AddRange(
                    TestDataHelper.InsertLocationSpaceProductGroupDtos(dalFactory, locationSpaceDto.Id, prodGroupDtos));
            }

            List<LocationSpaceBayDto> locSpaceBayDtos = new List<LocationSpaceBayDto>();
            foreach (LocationSpaceProductGroupDto locationSpaceProductGroupDto in locSpaceProductGroupDtos)
            {
                locSpaceBayDtos.AddRange(TestDataHelper.InsertLocationSpaceBayDtos(dalFactory, locationSpaceProductGroupDto.Id, 1));
            }

            //take required refs
            _productGroupDtoList = prodGroupDtos;
            _locationDtoList = locDtos;
            _locationSpaceDtos = locSpaceDtos;
            _locationSpaceProductGroupDtos = locSpaceProductGroupDtos;
            _locationSpaceBayDtos = locSpaceBayDtos;
        }

        #endregion

        #region Validate

        [Test, TestCaseSource("DalFactoryTypes")]
        public void Validate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);
                Int32 entityId = 1;

                Int32 invalidRowNumber;
                Int32 invalidColumnNumber;
                Object invalidData;
                Object invalidObjectData;
                ValidationErrorGroup errorGroup;
                ValidationErrorItem errorItem;


                #region Case 1 - Valid Data

                ImportFileData fileData = CreateData();

                //Construct and execute process
                var case1ValidateProcess =
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));

                //Validate process once completed
                Assert.AreEqual(0, case1ValidateProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(case1ValidateProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, case1ValidateProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");

                #endregion

                #region Case 2 - Invalid Data Type

                #region Single Error

                //Create Data
                fileData = CreateData();

                //Set invalid data type for a row
                //Set Height to String
                invalidRowNumber = 1;
                invalidColumnNumber = fileData.GetMappedColNumber(
                    fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.FaceThicknessMappingId));
                invalidData = "xx";

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct Process
                var case2ValidateProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));

                //Validate the process once completed
                Assert.AreEqual(1, case2ValidateProcess.Errors.Count, "1 Error Group Should Have Been Found");

                //Get error group
                errorGroup = case2ValidateProcess.Errors.First();
                //Check Properties
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectDataTypeHeader, errorGroup.Description, "Error Added to Wrong Group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 Error Should Exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index and first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column Number Should Match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.FaceThicknessMappingId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Warning, errorItem.ErrorType, "Item should be a warning");//no mandatory fields
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case2ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case2ValidateProcess.ValidatedData.Rows.Count, "Output row count should match input");

                #endregion

                #region > 10% errors

                //TODO

                #endregion

                #endregion

                #region Case 3 - Missing Data

                #region Test Non Mandatory property

                //create data
                fileData = CreateData();

                //Set missing data
                //Set Height to be missing
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(
                    fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.FaceThicknessMappingId));
                invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                var case3ValidateProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));


                //Validate the process once completed
                Assert.AreEqual(1, case3ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case3ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingDataHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.FaceThicknessMappingId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual("<empty>", errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Warning, errorItem.ErrorType, "Item should be a warning as column is not mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case3ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case3ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion

                #region Test Mandatory property

                //Create data
                fileData = CreateData();

                //Set missing data
                //Set product code data to be a null value
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(
                    fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId));
                invalidObjectData = String.Empty;

                ImportFileDataCell cellToInvalidate = fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber);
                cellToInvalidate.Value = invalidObjectData;
                Assert.AreEqual(invalidObjectData, cellToInvalidate.Value);

                //Construct Process
                var case31ValidateProcess =
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));


                //Validate the process once completed
                Assert.AreEqual(2, case31ValidateProcess.Errors.Count, "2 error groups should have been added");

                //Get error group
                errorGroup = case31ValidateProcess.Errors.Where(e => e.Description == "Invalid identifying data").FirstOrDefault();
                //Check properties
                Assert.IsNotNull(errorGroup, "Missing data error gruop should have been found");
                Assert.AreEqual("Invalid identifying data", errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties 
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual("<empty>", errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be a error as column is not mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Check other group is a parent record validation error group
                errorGroup = case31ValidateProcess.Errors.Where(e => e.Description == Message.DataManagement_Validation_MissingParentRecordHeader).FirstOrDefault();
                //Check properties
                Assert.IsNotNull(errorGroup, "Parent record validation group should have been found");
                Assert.AreEqual(Message.DataManagement_Validation_MissingParentRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 3 errors should exist");

                //Validate process properties
                Assert.IsNotNull(case31ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case31ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");


                #endregion

                #endregion

                #region Case 4 - Parent Record

                #region Location

                //Create data
                fileData = CreateData();

                //Set invalid data
                //Set product code to be a code that doesn't match any existing products in the db
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId));
                invalidData = new string('t', 25);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                var case6ValidateProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));


                //Validate the process once completed
                Assert.AreEqual(1, case6ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case6ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingParentRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties 
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId).PropertyName,
                    errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_NoMatchingLocation,
                    fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case6ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case6ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region ProductGroup

                //Create data
                fileData = CreateData();

                //Set invalid data
                //Set product code to be a code that doesn't match any existing products in the db
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(
                    fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId));
                invalidData = new string('t', 25);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                var case7ValidateProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));


                //Validate the process once completed
                Assert.AreEqual(1, case7ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case7ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingParentRecordHeader, errorGroup.Description, "Error added to the wrong group");

                //TODO:
                //Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                ////Get error item
                //errorItem = errorGroup.Errors.First();
                ////Check properties 
                //Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                //Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                //Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId).PropertyName, errorItem.ColumnName, "Column name should match");
                //Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                //Assert.AreEqual(String.Format(Message.DataManagement_Validation_NoMatchingProductGroup,
                //    fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                //Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case7ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case7ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #endregion

                #region Case 5 - Duplicate Record

                //Create data
                fileData = CreateData();

                ImportFileDataRow sourceRow = fileData.Rows[0];

                //create a copy of the row and add
                ImportFileDataRow copyRow = ImportFileDataRow.NewImportFileDataRow(fileData.Rows.Count + 1);
                foreach (var cell in sourceRow.Cells)
                {
                    copyRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cell.ColumnNumber, cell.Value));
                }
                fileData.Rows.Add(copyRow);
                invalidRowNumber = copyRow.RowNumber;

                //Construct and execute process
                var case8ValidateProcess =
                                 ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));

                //Validate the process once completed
                Assert.AreEqual(1, case8ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case8ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");

                //Check details of these 2 errors
                foreach (ValidationErrorItem error in errorGroup.Errors)
                {
                    //Check row numbers
                    Assert.IsTrue((error.RowNumber == (sourceRow.RowNumber) || error.RowNumber == (invalidRowNumber)), "Row number should be one of the 2 duplicate rows");

                    //Check duplicate error flag
                    Assert.IsTrue(error.IsDuplicateError, "Should be marked as duplicate error");
                }

                //Validate process properties
                Assert.IsNotNull(case8ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case8ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion

                #region Case 6 - GEM21865_ParentRecordCheckIsCaseInsensitive

                fileData = CreateData();

                List<Int32> checkColNumbers =
                    new List<Int32>()
                    {
                        //LocationCode
                        fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId)),

                        //ProductGroup
                        fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId)),
                    };

                //+ lower invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToLowerInvariant();
                    }

                    var validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }

                //+upper invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }

                    var validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationSpaceElementProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }

                #endregion
            }
        }


        #endregion

    }
}
