﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;
using NUnit.Framework;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportLocationClustersProcess : TestBase
    {
        #region Fields

        IImportMappingList _mappingList;
        Int32 _numDtosToInsert = 50;
        List<LocationDto> _locationDtoList;

        #endregion

        #region Helper Methods

        private void InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);

            //location hierarchy 
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
            List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 5);

            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, locGroupDtos, 10);
            _locationDtoList = locDtos;
        }


        ///// <summary>
        ///// Validates imported data and original datatable
        ///// </summary>
        ///// <param name="storeDtoList"></param>
        ///// <param name="dataTable"></param>
        private void ValidateImportedData(IEnumerable<ClusterSchemeDto> schemeDtoList, IEnumerable<ClusterDto> clusterDtoList, IEnumerable<ClusterLocationDto> clusterLocationDtoList, ImportFileData fileData, IDalFactory dalFactory)
        {
            Assert.AreEqual(clusterLocationDtoList.Count(), fileData.Rows.Count);

            Int32 rowNumber = 1;

            foreach (ImportFileDataRow row in fileData.Rows)
            {
                //Get scheme name from row
                String schemeName = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId))).Value.ToString();
                String clusterName = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationClusterImportMappingList.ClusterNameMappingId))).Value.ToString();
                String locationCode = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId))).Value.ToString();

                //Find matching inserted cluster scheme
                ClusterSchemeDto scheme = schemeDtoList.Where(p => p.Name == schemeName).FirstOrDefault();
                Assert.IsNotNull(scheme, "Scheme should have been found");

                //Get all inserted clusters inserted for this scheme
                List<ClusterDto> clusters = clusterDtoList.Where(p => p.ClusterSchemeId == scheme.Id).ToList();

                //Ensure cluster with matching name exists
                ClusterDto cluster = clusters.Where(p => p.Name == clusterName).FirstOrDefault();
                Assert.IsNotNull(cluster, "Cluster should have been found");

                //Find inserted location assigned to this
                List<ClusterLocationDto> clusterLocations = clusterLocationDtoList.Where(p => p.ClusterId == cluster.Id).ToList();

                //Ensure cluster location with matching code exists
                ClusterLocationDto location = clusterLocations.Where(p => p.Code == locationCode).FirstOrDefault();
                Assert.IsNotNull(location, "Cluster location should have been found");

                rowNumber++;
            }
        }

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            //Create 2 schemes with all locations assigned to their own groups
            for (Int32 i = 0; i < 2; i++)
            {
                String schemeName = Guid.NewGuid().ToString();
                Int32 rowNumber = 1;
                for (Int32 j = 0; j < _numDtosToInsert; j++)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        Object dataItem = null;
                        if (mappingItem.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId)
                        {
                            dataItem = _locationDtoList[j].Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId)
                        {
                            dataItem = schemeName;
                        }
                        else
                        {
                            dataItem = base.GenerateRandomData(mappingItem);
                        }

                        ImportFileDataCell importCell =
                                    ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                        importRow.Cells.Add(importCell);

                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }
            }
            return fileData;
        }
        #endregion

        #region Import

        /// <summary>
        /// Import store space data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);

                //Create data table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create data
                fileData = CreateData();

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportLocationClustersProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportLocationClustersProcess(1, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationClustersProcess>(importProcess);

                //Fetch all scheme data that exists in the db
                IEnumerable<ClusterSchemeDto> schemeDtoList;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                    {
                        schemeDtoList = dal.FetchByEntityId(1);
                    }
                }


                //Fetch all cluster leveldata that exists in the db
                List<ClusterDto> clusterDtoList = new List<ClusterDto>(); ;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                    {
                        foreach (ClusterSchemeDto schemeDto in schemeDtoList)
                        {
                            clusterDtoList.AddRange(dal.FetchByClusterSchemeId(schemeDto.Id));
                        }
                    }
                }

                //Fetch all cluster location data that exists in the db
                List<ClusterLocationDto> clusterLocationDtoList = new List<ClusterLocationDto>();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                    {
                        foreach (ClusterDto clusterDto in clusterDtoList)
                        {
                            clusterLocationDtoList.AddRange(dal.FetchByClusterId(clusterDto.Id));
                        }
                    }
                }

                //Check imported data matches inserted.
                ValidateImportedData(schemeDtoList, clusterDtoList, clusterLocationDtoList, fileData, dalFactory);
            }
        }

        #endregion
    }
}
