﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportLocationsProcess : TestBase
    {
        #region Fields

        Int32 _numDtosToInsert = 200;

        private Int32 _locHierarchyId;
        List<LocationGroupDto> _locationGroups;

        #endregion

        #region Helper Methods

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(ImportFileData fileData, IDalFactory dalFactory)
        {
            List<LocationDto> locationDtoList;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    locationDtoList = dal.FetchByEntityId(1).ToList();
                }
            }

            foreach (ImportFileDataRow row in fileData.Rows)
            {
                //Get Code
                String code = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationImportMappingList.CodeMapId))).Value.ToString();

                //Find corresponding location dto
                LocationDto locationDto = locationDtoList.FirstOrDefault(p => p.Code == code);
                Assert.IsNotNull(locationDto, "Location record should exist");

                //Check all of the properties match up
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address1MapId))).Value, locationDto.Address1);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address2MapId))).Value, locationDto.Address2);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AdvertisingZoneMapId))).Value, locationDto.AdvertisingZone);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AverageOpeningHoursMapId))).Value, locationDto.AverageOpeningHours);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkManagementMapId))).Value, locationDto.CarParkManagement);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkSpacesMapId))).Value, locationDto.CarParkSpaces);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CityMapId))).Value, locationDto.City);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId))).Value, locationDto.Code);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountryMapId))).Value, locationDto.Country);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId))).Value, locationDto.County);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.StateMapId))).Value, locationDto.State);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateClosedMapId))).Value, locationDto.DateClosed);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateLastRefittedMapId))).Value, locationDto.DateLastRefitted);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId))).Value, locationDto.DateOpen);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DefaultClusterAttributeMapId))).Value, locationDto.DefaultClusterAttribute);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DistributionCentreMapId))).Value, locationDto.DistributionCentre);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerEmailMapId))).Value, locationDto.DivisionalManagerEmail);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerNameMapId))).Value, locationDto.DivisionalManagerName);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxAreaCodeMapId))).Value, locationDto.FaxAreaCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxCountryCodeMapId))).Value, locationDto.FaxCountryCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxNumberMapId))).Value, locationDto.FaxNumber);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAlcoholMapId))).Value, locationDto.HasAlcohol);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAtmMachinesMapId))).Value, locationDto.HasAtmMachines);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasBabyChangingMapId))).Value, locationDto.HasBabyChanging);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasButcherMapId))).Value, locationDto.HasButcher);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCafeMapId))).Value, locationDto.HasCafe);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCarServiceAreaMapId))).Value, locationDto.HasCarServiceArea);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasClinicMapId))).Value, locationDto.HasClinic);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCustomerWCMapId))).Value, locationDto.HasCustomerWC);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDeliMapId))).Value, locationDto.HasDeli);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDryCleaningMapId))).Value, locationDto.HasDryCleaning);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFashionMapId))).Value, locationDto.HasFashion);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFishmongerMapId))).Value, locationDto.HasFishmonger);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGardenCentreMapId))).Value, locationDto.HasGardenCentre);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGroceryMapId))).Value, locationDto.HasGrocery);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHomeShoppingAvailableMapId))).Value, locationDto.HasHomeShoppingAvailable);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHotFoodToGoMapId))).Value, locationDto.HasHotFoodToGo);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasInStoreBakeryMapId))).Value, locationDto.HasInStoreBakery);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasJewelleryMapId))).Value, locationDto.HasJewellery);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasLotteryMapId))).Value, locationDto.HasLottery);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMobilePhonesMapId))).Value, locationDto.HasMobilePhones);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMovieRentalMapId))).Value, locationDto.HasMovieRental);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasNewsCubeMapId))).Value, locationDto.HasNewsCube);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOpticianMapId))).Value, locationDto.HasOptician);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOrganicMapId))).Value, locationDto.HasOrganic);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPetrolForecourtMapId))).Value, locationDto.HasPetrolForecourt);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPharmacyMapId))).Value, locationDto.HasPharmacy);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotocopierMapId))).Value, locationDto.HasPhotocopier);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotoDepartmentMapId))).Value, locationDto.HasPhotoDepartment);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPizzaMapId))).Value, locationDto.HasPizza);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPostOfficeMapId))).Value, locationDto.HasPostOffice);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRecyclingMapId))).Value, locationDto.HasRecycling);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRotisserieMapId))).Value, locationDto.HasRotisserie);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasSaladBarMapId))).Value, locationDto.HasSaladBar);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasTravelMapId))).Value, locationDto.HasTravel);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Is24HoursMapId))).Value, locationDto.Is24Hours);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsFreeholdMapId))).Value, locationDto.IsFreehold);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsMezzFittedMapId))).Value, locationDto.IsMezzFitted);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenFridayMapId))).Value, locationDto.IsOpenFriday);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenMondayMapId))).Value, locationDto.IsOpenMonday);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSaturdayMapId))).Value, locationDto.IsOpenSaturday);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSundayMapId))).Value, locationDto.IsOpenSunday);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenThursdayMapId))).Value, locationDto.IsOpenThursday);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenTuesdayMapId))).Value, locationDto.IsOpenTuesday);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenWednesdayMapId))).Value, locationDto.IsOpenWednesday);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LatitudeMapId))).Value, locationDto.Latitude);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LocationAttributeMapId))).Value, locationDto.Location);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId))).Value, locationDto.Longitude);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerEmailMapId))).Value, locationDto.ManagerEmail);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerNameMapId))).Value, locationDto.ManagerName);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NameMapId))).Value, locationDto.Name);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NoOfCheckoutsMapId))).Value, locationDto.NoOfCheckouts);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.OpeningHoursMapId))).Value, locationDto.OpeningHours);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PetrolForecourtTypeMapId))).Value, locationDto.PetrolForecourtType);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PostalCodeMapId))).Value, locationDto.PostalCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionMapId))).Value, locationDto.Region);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerEmailMapId))).Value, locationDto.RegionalManagerEmail);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerNameMapId))).Value, locationDto.RegionalManagerName);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RestaurantMapId))).Value, locationDto.Restaurant);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeGrossFloorAreaMapId))).Value, locationDto.SizeGrossFloorArea);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeMezzSalesAreaMapId))).Value, locationDto.SizeMezzSalesArea);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeNetSalesAreaMapId))).Value, locationDto.SizeNetSalesArea);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneAreaCodeMapId))).Value, locationDto.TelephoneAreaCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneCountryCodeMapId))).Value, locationDto.TelephoneCountryCode);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneNumberMapId))).Value, locationDto.TelephoneNumber);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TVRegionMapId))).Value, locationDto.TVRegion);
            }
        }

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(IEnumerable<LocationDto> locationDtoList, ImportFileData fileData, IDalFactory dalFactory, ImportFileData newFileData, ImportMapping mappingItem)
        {
            Assert.AreEqual(locationDtoList.Count(), fileData.Rows.Count);

            #region check for when a property hasn't changed
            foreach (ImportFileDataRow row in fileData.Rows)
            {
                //Get Code
                String code = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationImportMappingList.CodeMapId))).Value.ToString();

                //Find corresponding location dto
                LocationDto locationDto = locationDtoList.FirstOrDefault(p => p.Code == code);
                Assert.IsNotNull(locationDto, "Location record should exist");

                //Check all of the properties match up
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address1MapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address1MapId))).Value, locationDto.Address1);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address2MapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address2MapId))).Value, locationDto.Address2);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AdvertisingZoneMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AdvertisingZoneMapId))).Value, locationDto.AdvertisingZone);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AverageOpeningHoursMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AverageOpeningHoursMapId))).Value, locationDto.AverageOpeningHours);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkManagementMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkManagementMapId))).Value, locationDto.CarParkManagement);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkSpacesMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkSpacesMapId))).Value, locationDto.CarParkSpaces);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CityMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CityMapId))).Value, locationDto.City);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId))).Value, locationDto.Code);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountryMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountryMapId))).Value, locationDto.Country);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId))).Value, locationDto.County);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.StateMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.StateMapId))).Value, locationDto.State);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateClosedMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateClosedMapId))).Value, locationDto.DateClosed);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateLastRefittedMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateLastRefittedMapId))).Value, locationDto.DateLastRefitted);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId))).Value, locationDto.DateOpen);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DefaultClusterAttributeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DefaultClusterAttributeMapId))).Value, locationDto.DefaultClusterAttribute);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DistributionCentreMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DistributionCentreMapId))).Value, locationDto.DistributionCentre);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerEmailMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerEmailMapId))).Value, locationDto.DivisionalManagerEmail);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerNameMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerNameMapId))).Value, locationDto.DivisionalManagerName);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxAreaCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxAreaCodeMapId))).Value, locationDto.FaxAreaCode);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxCountryCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxCountryCodeMapId))).Value, locationDto.FaxCountryCode);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxNumberMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxNumberMapId))).Value, locationDto.FaxNumber);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAlcoholMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAlcoholMapId))).Value, locationDto.HasAlcohol);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAtmMachinesMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAtmMachinesMapId))).Value, locationDto.HasAtmMachines);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasBabyChangingMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasBabyChangingMapId))).Value, locationDto.HasBabyChanging);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasButcherMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasButcherMapId))).Value, locationDto.HasButcher);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCafeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCafeMapId))).Value, locationDto.HasCafe);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCarServiceAreaMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCarServiceAreaMapId))).Value, locationDto.HasCarServiceArea);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasClinicMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasClinicMapId))).Value, locationDto.HasClinic);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCustomerWCMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCustomerWCMapId))).Value, locationDto.HasCustomerWC);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDeliMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDeliMapId))).Value, locationDto.HasDeli);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDryCleaningMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDryCleaningMapId))).Value, locationDto.HasDryCleaning);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFashionMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFashionMapId))).Value, locationDto.HasFashion);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFishmongerMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFishmongerMapId))).Value, locationDto.HasFishmonger);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGardenCentreMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGardenCentreMapId))).Value, locationDto.HasGardenCentre);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGroceryMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGroceryMapId))).Value, locationDto.HasGrocery);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHomeShoppingAvailableMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHomeShoppingAvailableMapId))).Value, locationDto.HasHomeShoppingAvailable);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHotFoodToGoMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHotFoodToGoMapId))).Value, locationDto.HasHotFoodToGo);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasInStoreBakeryMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasInStoreBakeryMapId))).Value, locationDto.HasInStoreBakery);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasJewelleryMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasJewelleryMapId))).Value, locationDto.HasJewellery);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasLotteryMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasLotteryMapId))).Value, locationDto.HasLottery);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMobilePhonesMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMobilePhonesMapId))).Value, locationDto.HasMobilePhones);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMovieRentalMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMovieRentalMapId))).Value, locationDto.HasMovieRental);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasNewsCubeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasNewsCubeMapId))).Value, locationDto.HasNewsCube);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOpticianMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOpticianMapId))).Value, locationDto.HasOptician);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOrganicMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOrganicMapId))).Value, locationDto.HasOrganic);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPetrolForecourtMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPetrolForecourtMapId))).Value, locationDto.HasPetrolForecourt);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPharmacyMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPharmacyMapId))).Value, locationDto.HasPharmacy);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotocopierMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotocopierMapId))).Value, locationDto.HasPhotocopier);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotoDepartmentMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotoDepartmentMapId))).Value, locationDto.HasPhotoDepartment);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPizzaMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPizzaMapId))).Value, locationDto.HasPizza);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPostOfficeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPostOfficeMapId))).Value, locationDto.HasPostOffice);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRecyclingMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRecyclingMapId))).Value, locationDto.HasRecycling);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRotisserieMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRotisserieMapId))).Value, locationDto.HasRotisserie);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasSaladBarMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasSaladBarMapId))).Value, locationDto.HasSaladBar);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasTravelMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasTravelMapId))).Value, locationDto.HasTravel);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Is24HoursMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Is24HoursMapId))).Value, locationDto.Is24Hours);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsFreeholdMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsFreeholdMapId))).Value, locationDto.IsFreehold);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsMezzFittedMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsMezzFittedMapId))).Value, locationDto.IsMezzFitted);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenFridayMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenFridayMapId))).Value, locationDto.IsOpenFriday);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenMondayMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenMondayMapId))).Value, locationDto.IsOpenMonday);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSaturdayMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSaturdayMapId))).Value, locationDto.IsOpenSaturday);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSundayMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSundayMapId))).Value, locationDto.IsOpenSunday);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenThursdayMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenThursdayMapId))).Value, locationDto.IsOpenThursday);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenTuesdayMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenTuesdayMapId))).Value, locationDto.IsOpenTuesday);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenWednesdayMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenWednesdayMapId))).Value, locationDto.IsOpenWednesday);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LatitudeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LatitudeMapId))).Value, locationDto.Latitude);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LocationAttributeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LocationAttributeMapId))).Value, locationDto.Location);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId))).Value, locationDto.Longitude);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerEmailMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerEmailMapId))).Value, locationDto.ManagerEmail);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerNameMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerNameMapId))).Value, locationDto.ManagerName);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NameMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NameMapId))).Value, locationDto.Name);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NoOfCheckoutsMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NoOfCheckoutsMapId))).Value, locationDto.NoOfCheckouts);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.OpeningHoursMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.OpeningHoursMapId))).Value, locationDto.OpeningHours);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PetrolForecourtTypeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PetrolForecourtTypeMapId))).Value, locationDto.PetrolForecourtType);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PostalCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PostalCodeMapId))).Value, locationDto.PostalCode);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionMapId))).Value, locationDto.Region);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerEmailMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerEmailMapId))).Value, locationDto.RegionalManagerEmail);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerNameMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerNameMapId))).Value, locationDto.RegionalManagerName);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RestaurantMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RestaurantMapId))).Value, locationDto.Restaurant);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeGrossFloorAreaMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeGrossFloorAreaMapId))).Value, locationDto.SizeGrossFloorArea);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeMezzSalesAreaMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeMezzSalesAreaMapId))).Value, locationDto.SizeMezzSalesArea);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeNetSalesAreaMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeNetSalesAreaMapId))).Value, locationDto.SizeNetSalesArea);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneAreaCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneAreaCodeMapId))).Value, locationDto.TelephoneAreaCode);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneCountryCodeMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneCountryCodeMapId))).Value, locationDto.TelephoneCountryCode);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneNumberMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneNumberMapId))).Value, locationDto.TelephoneNumber);
                if (mappingItem != fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TVRegionMapId))
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TVRegionMapId))).Value, locationDto.TVRegion);

            }
            #endregion

            #region check for when a property has changed
            foreach (ImportFileDataRow row in newFileData.Rows)
            {
                //Get Code
                String code = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationImportMappingList.CodeMapId))).Value.ToString();

                //Find corresponding location dto
                LocationDto locationDto = locationDtoList.FirstOrDefault(p => p.Code == code);
                Assert.IsNotNull(locationDto, "Location record should exist");

                //Check all of the properties match up
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address1MapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address1MapId))).Value, locationDto.Address1); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address2MapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Address2MapId))).Value, locationDto.Address2); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AdvertisingZoneMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AdvertisingZoneMapId))).Value, locationDto.AdvertisingZone); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AverageOpeningHoursMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.AverageOpeningHoursMapId))).Value, locationDto.AverageOpeningHours); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkManagementMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkManagementMapId))).Value, locationDto.CarParkManagement); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkSpacesMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CarParkSpacesMapId))).Value, locationDto.CarParkSpaces); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CityMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CityMapId))).Value, locationDto.City); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId))).Value, locationDto.Code); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountryMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountryMapId))).Value, locationDto.Country); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId))).Value, locationDto.County); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.StateMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.StateMapId))).Value, locationDto.State); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateClosedMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateClosedMapId))).Value, locationDto.DateClosed); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateLastRefittedMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateLastRefittedMapId))).Value, locationDto.DateLastRefitted); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId))).Value, locationDto.DateOpen); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DefaultClusterAttributeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DefaultClusterAttributeMapId))).Value, locationDto.DefaultClusterAttribute); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DistributionCentreMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DistributionCentreMapId))).Value, locationDto.DistributionCentre); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerEmailMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerEmailMapId))).Value, locationDto.DivisionalManagerEmail); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerNameMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DivisionalManagerNameMapId))).Value, locationDto.DivisionalManagerName); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxAreaCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxAreaCodeMapId))).Value, locationDto.FaxAreaCode); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxCountryCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxCountryCodeMapId))).Value, locationDto.FaxCountryCode); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxNumberMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.FaxNumberMapId))).Value, locationDto.FaxNumber); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAlcoholMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAlcoholMapId))).Value, locationDto.HasAlcohol); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAtmMachinesMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasAtmMachinesMapId))).Value, locationDto.HasAtmMachines); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasBabyChangingMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasBabyChangingMapId))).Value, locationDto.HasBabyChanging); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasButcherMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasButcherMapId))).Value, locationDto.HasButcher); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCafeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCafeMapId))).Value, locationDto.HasCafe); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCarServiceAreaMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCarServiceAreaMapId))).Value, locationDto.HasCarServiceArea); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasClinicMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasClinicMapId))).Value, locationDto.HasClinic); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCustomerWCMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasCustomerWCMapId))).Value, locationDto.HasCustomerWC); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDeliMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDeliMapId))).Value, locationDto.HasDeli); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDryCleaningMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasDryCleaningMapId))).Value, locationDto.HasDryCleaning); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFashionMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFashionMapId))).Value, locationDto.HasFashion); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFishmongerMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasFishmongerMapId))).Value, locationDto.HasFishmonger); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGardenCentreMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGardenCentreMapId))).Value, locationDto.HasGardenCentre); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGroceryMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasGroceryMapId))).Value, locationDto.HasGrocery); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHomeShoppingAvailableMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHomeShoppingAvailableMapId))).Value, locationDto.HasHomeShoppingAvailable); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHotFoodToGoMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasHotFoodToGoMapId))).Value, locationDto.HasHotFoodToGo); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasInStoreBakeryMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasInStoreBakeryMapId))).Value, locationDto.HasInStoreBakery); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasJewelleryMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasJewelleryMapId))).Value, locationDto.HasJewellery); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasLotteryMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasLotteryMapId))).Value, locationDto.HasLottery); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMobilePhonesMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMobilePhonesMapId))).Value, locationDto.HasMobilePhones); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMovieRentalMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasMovieRentalMapId))).Value, locationDto.HasMovieRental); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasNewsCubeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasNewsCubeMapId))).Value, locationDto.HasNewsCube); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOpticianMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOpticianMapId))).Value, locationDto.HasOptician); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOrganicMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasOrganicMapId))).Value, locationDto.HasOrganic); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPetrolForecourtMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPetrolForecourtMapId))).Value, locationDto.HasPetrolForecourt); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPharmacyMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPharmacyMapId))).Value, locationDto.HasPharmacy); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotocopierMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotocopierMapId))).Value, locationDto.HasPhotocopier); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotoDepartmentMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPhotoDepartmentMapId))).Value, locationDto.HasPhotoDepartment); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPizzaMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPizzaMapId))).Value, locationDto.HasPizza); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPostOfficeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasPostOfficeMapId))).Value, locationDto.HasPostOffice); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRecyclingMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRecyclingMapId))).Value, locationDto.HasRecycling); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRotisserieMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasRotisserieMapId))).Value, locationDto.HasRotisserie); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasSaladBarMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasSaladBarMapId))).Value, locationDto.HasSaladBar); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasTravelMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.HasTravelMapId))).Value, locationDto.HasTravel); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Is24HoursMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.Is24HoursMapId))).Value, locationDto.Is24Hours); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsFreeholdMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsFreeholdMapId))).Value, locationDto.IsFreehold); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsMezzFittedMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsMezzFittedMapId))).Value, locationDto.IsMezzFitted); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenFridayMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenFridayMapId))).Value, locationDto.IsOpenFriday); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenMondayMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenMondayMapId))).Value, locationDto.IsOpenMonday); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSaturdayMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSaturdayMapId))).Value, locationDto.IsOpenSaturday); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSundayMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenSundayMapId))).Value, locationDto.IsOpenSunday); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenThursdayMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenThursdayMapId))).Value, locationDto.IsOpenThursday); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenTuesdayMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenTuesdayMapId))).Value, locationDto.IsOpenTuesday); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenWednesdayMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.IsOpenWednesdayMapId))).Value, locationDto.IsOpenWednesday); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LatitudeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LatitudeMapId))).Value, locationDto.Latitude); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LocationAttributeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LocationAttributeMapId))).Value, locationDto.Location); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId))).Value, locationDto.Longitude); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerEmailMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerEmailMapId))).Value, locationDto.ManagerEmail); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerNameMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.ManagerNameMapId))).Value, locationDto.ManagerName); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NameMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NameMapId))).Value, locationDto.Name); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NoOfCheckoutsMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.NoOfCheckoutsMapId))).Value, locationDto.NoOfCheckouts); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.OpeningHoursMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.OpeningHoursMapId))).Value, locationDto.OpeningHours); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PetrolForecourtTypeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PetrolForecourtTypeMapId))).Value, locationDto.PetrolForecourtType); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PostalCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.PostalCodeMapId))).Value, locationDto.PostalCode); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionMapId))).Value, locationDto.Region); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerEmailMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerEmailMapId))).Value, locationDto.RegionalManagerEmail); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerNameMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RegionalManagerNameMapId))).Value, locationDto.RegionalManagerName); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RestaurantMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.RestaurantMapId))).Value, locationDto.Restaurant); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeGrossFloorAreaMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeGrossFloorAreaMapId))).Value, locationDto.SizeGrossFloorArea); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeMezzSalesAreaMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeMezzSalesAreaMapId))).Value, locationDto.SizeMezzSalesArea); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeNetSalesAreaMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeNetSalesAreaMapId))).Value, locationDto.SizeNetSalesArea); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneAreaCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneAreaCodeMapId))).Value, locationDto.TelephoneAreaCode); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneCountryCodeMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneCountryCodeMapId))).Value, locationDto.TelephoneCountryCode); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneNumberMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TelephoneNumberMapId))).Value, locationDto.TelephoneNumber); break;
                }
                if (mappingItem == fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TVRegionMapId))
                {
                    Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.TVRegionMapId))).Value, locationDto.TVRegion); break;
                }
            }
            #endregion

        }

        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData(Int32 numDtosToInsert)
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationImportMappingList.NewLocationImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;

                    if (mappingItem.PropertyIdentifier == LocationImportMappingList.CodeMapId)
                    {
                        dataItem = "code" + i.ToString();
                    }
                    else if (mappingItem.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId)
                    {
                        dataItem = _locationGroups[rand.Next(1, _locationGroups.Count - 1)].Code;
                    }
                    else
                    {
                        dataItem = base.GenerateRandomData(mappingItem);
                    }

                    importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem));

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateNonRandomData(Int32 numDtosToInsert, Boolean isNew)
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationImportMappingList.NewLocationImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Int32 rowNumber = 1;
            for (int i = 0; i < numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;
                    if (mappingItem.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId)
                    {
                        dataItem = _locationGroups[i].Code;
                    }
                    else if (mappingItem.PropertyIsMandatory)
                    {
                        //mandatory fields don't need to be changed for mapping test
                        dataItem = GenerateData(mappingItem, false);
                    }
                    else
                    {
                        dataItem = GenerateData(mappingItem, isNew);
                    }
                    importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem));

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private Object GenerateData(ImportMapping mappingItem, Boolean isNew)
        {
            Object returnValue = null;

            //Get type
            Type t = null;
            if (mappingItem.PropertyType.IsGenericType && mappingItem.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                t = Nullable.GetUnderlyingType(mappingItem.PropertyType);
            }
            else
            {
                t = mappingItem.PropertyType;
            }

            if (!isNew)
            {
                //Generate data
                if (t == typeof(Boolean))
                {
                    returnValue = false;
                }
                else if (t == typeof(DateTime))
                {
                    returnValue = DateTime.Today;
                }
                else if (t == typeof(Byte))
                {
                    returnValue = 0;
                }
                else if (t == typeof(Single))
                {
                    returnValue = 0;
                }
                else if (t == typeof(String))
                {
                    returnValue = "White";
                }
                else if (t.BaseType == typeof(Enum))
                {
                    throw new Exception("Enum value must be specifically set by the processes calling this method");
                }
                else
                {
                    returnValue = 0;
                }
            }
            else
            {//Generate data
                if (t == typeof(Boolean))
                {
                    returnValue = true;
                }
                else if (t == typeof(DateTime))
                {
                    returnValue = DateTime.Today;
                }
                else if (t == typeof(Byte))
                {
                    returnValue = 1;
                }
                else if (t == typeof(Single))
                {
                    returnValue = 1;
                }
                else if (t == typeof(String))
                {
                    returnValue = "Black";
                }
                else if (t.BaseType == typeof(Enum))
                {
                    throw new Exception("Enum value must be specifically set by the processes calling this method");
                }
                else
                {
                    returnValue = 1;
                }
            }

            return returnValue;
        }


        private void InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);

            //location hierarchy 
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
            List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 5);

            _locHierarchyId = locHierarchyDtos[0].Id;

            _locationGroups = locGroupDtos.Where(l => l.LocationLevelId == level1Dtos.Last().Id).ToList(); //leaf levels only.
        }


        #endregion

        #region Import

        /// <summary>
        /// Import store data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);

                //Create data table
                ImportFileData fileData;

                #region Case 1 - Valid import

                //Create data
                fileData = CreateData(_numDtosToInsert);

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportLocationsProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1, fileData, "Test");

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(importProcess);


                //Check imported data matches inserted.
                ValidateImportedData(fileData, dalFactory);

                #endregion

                #region Case 2 - [GEM21865] ParentRecord match is case insensitive
                {
                    #region LocationGroup
                    {
                        fileData = CreateData(_numDtosToInsert);

                        Int32 locationGroupColNum =
                                fileData.GetMappedColNumber(
                                fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId));

                        foreach (ImportFileDataRow row in fileData.Rows)
                        {
                            ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == locationGroupColNum);
                            cell.Value = cell.Value.ToString().ToUpperInvariant();
                        }

                        Int32 preLocGroupCount;
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                            {
                                preLocGroupCount = dal.FetchByLocationHierarchyId(_locHierarchyId).Count();
                            }
                        }


                        //construct and execute process
                        importProcess =
                            ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(
                             new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1,
                                        fileData, String.Format("New Group {0}", DateTime.Now)));

                        ValidateImportedData(fileData, dalFactory);

                        Int32 postLocGroupCount;
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                            {
                                postLocGroupCount = dal.FetchByLocationHierarchyId(_locHierarchyId).Count();
                            }
                        }
                        Assert.AreEqual(preLocGroupCount, postLocGroupCount);
                    }
                    #endregion

                    #region Location
                    {
                        //get only the first row.
                        fileData = CreateData(_numDtosToInsert);
                        var row = fileData.Rows.First();
                        fileData.Rows.Clear();
                        fileData.Rows.Add(row);

                        Int32 preLocationCount;
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                            {
                                preLocationCount = dal.FetchByEntityId(1).Count();
                            }
                        }

                        importProcess =
                            ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(
                             new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1,
                                        fileData, String.Format("New Group {0}", DateTime.Now)));

                        ValidateImportedData(fileData, dalFactory);

                        Int32 postLocationCount;
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                            {
                                postLocationCount = dal.FetchByEntityId(1).Count();
                            }
                        }

                        Assert.AreEqual(preLocationCount, postLocationCount);

                    }
                    #endregion
                }
                #endregion
            }
        }

        #endregion

        #region Upsert

        /// <summary>
        /// Upsert store data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Upsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);
                Int32 numDtosToInsert = 1;

                //Create data table
                ImportFileData fileData;

                //Create data
                fileData = CreateNonRandomData(numDtosToInsert, false);

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportLocationsProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1, fileData, "Test");

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(importProcess);

                //Check imported data matches inserted.
                ValidateImportedData(fileData, dalFactory);

                //upsert
                //get the location codes
                ImportFileDataRow previousRow = fileData.Rows[0];

                //create a second row
                ImportFileData fileData2 = ImportFileData.NewImportFileData();

                fileData2 = CreateNonRandomData(numDtosToInsert, true);

                ImportFileDataRow newRow = fileData2.Rows[0];

                //set the rows to be the same
                for (int i = 0; i < previousRow.Cells.Count; i++)
                {
                    newRow.Cells[i].Value = previousRow.Cells[i].Value;
                }


                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;
                    if (mappingItem.PropertyIdentifier == LocationImportMappingList.CodeMapId ||
                        mappingItem.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId)
                    {
                    }
                    else
                    {
                        dataItem = GenerateData(mappingItem, true);

                        newRow.Cells[cellColNumber - 1] =
                                    ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);

                        //Construct process
                        importProcess = new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1, fileData2, "Test");

                        // execute the import process
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(importProcess);

                        //Check imported data matches inserted.
                        ValidateImportedData(fileData2, dalFactory);

                        newRow.Cells[cellColNumber - 1].Value = previousRow.Cells[cellColNumber - 1].Value;
                    }
                    cellColNumber++;
                }

                //check mappings
                //reset what's in the database to the origional file
                //Construct process
                importProcess = new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1, fileData, "Test");

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(importProcess);

                fileData2 = CreateNonRandomData(numDtosToInsert, true);

                newRow = fileData2.Rows[0];

                foreach (ImportMapping mapping in fileData2.MappingList)
                {
                    if (!mapping.PropertyIsMandatory)
                    {
                        mapping.ColumnReference = null;
                    }
                }

                cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    if (!mappingItem.PropertyIsMandatory)
                    {
                        ImportMapping setMapping = fileData2.MappingList.First(m => m.PropertyIdentifier == mappingItem.PropertyIdentifier);
                        setMapping.ColumnReference = mappingItem.ColumnReference;

                        //Construct process
                        importProcess = new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1, fileData2, "Test");

                        // execute the import process
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(importProcess);

                        //fetch products
                        IEnumerable<LocationDto> locationDtoList;
                        using (IDalContext dalContext = dalFactory.CreateContext())
                        {
                            using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                            {
                                locationDtoList = dal.FetchByEntityId(1);
                            }
                        }

                        //Check imported data matches inserted.
                        ValidateImportedData(locationDtoList, fileData, dalFactory, fileData2, mappingItem);
                        setMapping.ColumnReference = null;

                        //Construct process
                        importProcess = new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(1, fileData, "Test");

                        // execute the import process
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(importProcess);
                    }
                    cellColNumber++;
                }


            }
        }

        #endregion
    }
}
