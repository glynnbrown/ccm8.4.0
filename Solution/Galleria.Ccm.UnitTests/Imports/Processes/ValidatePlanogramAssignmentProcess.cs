﻿using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Model;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public sealed class ValidatePlanogramAssignmentProcess : TestBase
    {
        #region Fields

        List<PlanogramDto> _planogramDtos = new List<PlanogramDto>();
        List<ProductHierarchyDto> _hierrarchies = new List<ProductHierarchyDto>();
        List<ProductLevelDto> _levels = new List<ProductLevelDto>();
        List<ProductGroupDto> _productGroups = new List<ProductGroupDto>();
        List<LocationDto> _locationDtos = new List<LocationDto>();
        PlanogramGroupDto _planogramGroupDto = new PlanogramGroupDto();

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            IEnumerable<String> levelNames = ProductHierarchy.FetchByEntityId(1)
                .EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();

            fileData.MappingList = PlanogramAssignmentImportMappingList.NewPlanogramAssignmentImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }
            
            Int32 rowNumber = 1;
            for (int i = 0; i < _planogramDtos.Count-1; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);
                
                Object dataItem = null;
                dataItem = _locationDtos[i].Code;

                ImportFileDataCell importCell =
                            ImportFileDataCell.NewImportFileDataCell(1, dataItem);
                importRow.Cells.Add(importCell);

                dataItem = _productGroups[0].Code;

                importCell = ImportFileDataCell.NewImportFileDataCell(2, dataItem);
                importRow.Cells.Add(importCell);

                dataItem = _planogramDtos[i].Name;

                importCell = ImportFileDataCell.NewImportFileDataCell(3, dataItem);
                importRow.Cells.Add(importCell);

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private void InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);

            using (var dalContext = dalFactory.CreateContext())
            {
                var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
                var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, userName, entityDtos.First().Id, 20);
                _planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos.ToList());
                _hierrarchies = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                _levels = TestDataHelper.InsertProductLevelDtos(dalFactory, _hierrarchies.First().Id, 1);
                _productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, _levels, 1);
                
                List<PlanogramHierarchyDto> planogramHierarchyDtos = PlanogramHierarchyTests.InsertPlanogramHierarchyDtos(dalFactory, 1, entityDtos[0].Id);

                //Add planogram group planograms
                List<PlanogramGroupPlanogramDto> groupPlanogramDtoList = new List<PlanogramGroupPlanogramDto>();
                using (var planogramGroupDal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    using (var planogramGroupPlanogramDal = dalContext.GetDal<IPlanogramGroupPlanogramDal>())
                    {
                        Int32 groupNumber = 1;

                        //Create group
                        _planogramGroupDto = new PlanogramGroupDto();
                        _planogramGroupDto.PlanogramHierarchyId = planogramHierarchyDtos.First().Id;
                        _planogramGroupDto.Name = String.Format("Group {0}", groupNumber);
                        planogramGroupDal.Insert(_planogramGroupDto);
                        groupNumber++;

                        //For each generated planogram dto
                        foreach (PlanogramDto planDto in _planogramDtos)
                        {
                            //Create group planogram
                            PlanogramGroupPlanogramDto groupPlanogramDto = new PlanogramGroupPlanogramDto();
                            groupPlanogramDto.PlanogramId = Convert.ToInt32(planDto.Id);
                            groupPlanogramDto.PlanogramGroupId = _planogramGroupDto.Id;

                            //Add to list
                            groupPlanogramDtoList.Add(groupPlanogramDto);
                        }

                        //Call upsert
                        planogramGroupPlanogramDal.Upsert(groupPlanogramDtoList);
                    }
                }
                
                _locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 10, 1);                
            }
        }

        #endregion

        #region Validate

        [Test, TestCaseSource("DalFactoryTypes")]
        public void Validate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                Object invalidData;
                Int32 invalidColumnNumber;
                Int32 invalidRowNumber;
                ValidationErrorGroup errorGroup;
                ValidationErrorItem errorItem;

                InsertDtos(dalFactory);

                #region Valid Data
                //Create Data Table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                PlanogramHierarchy hierarchy = PlanogramHierarchy.FetchByEntityId(1);
                ProductHierarchy productHierarchy = ProductHierarchy.FetchByEntityId(1);
                ProductGroupInfoList productList = ProductGroupInfoList.FetchByProductGroupCodes(new List<String>() { _productGroups.First().Code });

                //Create Data
                fileData = CreateData();

                //Construct Process
                Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess
                    validate = new Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess(1, hierarchy.RootGroup, productList.First(), null, PlanogramAssignmentStatusType.Any, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess>(validate);
                
                //Validate process once completed
                Assert.AreEqual(0, validate.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validate.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validate.ValidatedData.Rows.Count, "Output Row Count Must Match Input Row Count");

                #endregion
                
                #region Parent Record

                #region Location

                //Create data
                fileData = CreateData();

                //Set invalid data
                //Set location code to be a code that doesn't match any existing locations in the db
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId));
                invalidData = new string('t', 25);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                validate =
                    new Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess(1, hierarchy.RootGroup, productList.First(), null, PlanogramAssignmentStatusType.Any, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess>(validate);

                //Validate the process once completed
                Assert.AreEqual(1, validate.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = validate.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingParentRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties 
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId).PropertyName,
                    errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_NoMatchingLocation,
                    fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(validate.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, validate.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Planogram Name

                //Create data
                fileData = CreateData();

                //Set invalid data
                //Set planogram name to be something that doesn't match any existing planograms in the db
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId));
                invalidData = new string('t', 25);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                validate =
                    new Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess(1, hierarchy.RootGroup, productList.First(), null, PlanogramAssignmentStatusType.Any, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess>(validate);

                //Validate the process once completed
                Assert.AreEqual(1, validate.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = validate.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingParentRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties 
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId).PropertyName,
                    errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_NoMatchingPlanogram,
                    fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(validate.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, validate.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Planogram Status

                //Create data
                fileData = CreateData();

                //Set invalid data
                //Set planogram status that doesn't match any existing planograms in the db
                invalidRowNumber = 1;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId));
                invalidData = fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value;

                //Construct process
                validate =
                    new Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess(1, hierarchy.RootGroup, productList.First(), null, PlanogramAssignmentStatusType.Published, fileData, 1, true);

                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess>(validate);

                //Validate the process once completed
                Assert.AreEqual(1, validate.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = validate.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingParentRecordHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(19, errorGroup.Errors.Count, "19 errors should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties 
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId).PropertyName,
                    errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_NoMatchingPlanogramStatus,
                    fileData.MappingList.First(p => p.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(validate.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, validate.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #endregion

                #region Duplicate Record

                //Create data
                fileData = CreateData();

                ImportFileDataRow sourceRow = fileData.Rows[0];

                //create a copy of the row and add
                ImportFileDataRow copyRow = ImportFileDataRow.NewImportFileDataRow(fileData.Rows.Count + 1);
                foreach (var cell in sourceRow.Cells)
                {
                    copyRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cell.ColumnNumber, cell.Value));
                }
                fileData.Rows.Add(copyRow);
                invalidRowNumber = copyRow.RowNumber;

                //Construct and execute process
                validate = new Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess(1, hierarchy.RootGroup, productList.First(), null, PlanogramAssignmentStatusType.Any, fileData, 1, true);
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidatePlanogramAssignmentProcess>(validate);

                //Validate the process once completed
                Assert.AreEqual(1, validate.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = validate.Errors.First();
                //Check properties
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");

                //Check details of these 2 errors
                foreach (ValidationErrorItem error in errorGroup.Errors)
                {
                    //Check row numbers
                    Assert.IsTrue((error.RowNumber == (sourceRow.RowNumber) || error.RowNumber == (invalidRowNumber)), "Row number should be one of the 2 duplicate rows");

                    //Check duplicate error flag
                    Assert.IsTrue(error.IsDuplicateError, "Should be marked as duplicate error");
                }

                //Validate process properties
                Assert.IsNotNull(validate.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, validate.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion
            }
        }

        #endregion
        
    }
}
