﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Processes;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportLocationSpaceBayProcess : TestBase
    {
        #region Fields

        private Dictionary<String, ProductGroupDto> _productGroupCodeToDtoLookup;
        private Dictionary<String, LocationDto> _locationCodeToDtoLookup;
        List<LocationSpaceDto> _locationSpaceDtos = new List<LocationSpaceDto>();
        List<LocationSpaceProductGroupDto> _locationSpaceProductGroupDtos = new List<LocationSpaceProductGroupDto>();

        #endregion

        #region Helper Methods

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(IEnumerable<LocationSpaceBayDto> locationSpaceBayDtoList, ImportFileData fileData)
        {
            Assert.AreEqual(locationSpaceBayDtoList.Count(), fileData.Rows.Count);

            foreach (ImportFileDataRow row in fileData.Rows)
            {
                //Get Location Code
                String locationCode = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationSpaceBayImportMappingList.LocationCodeMappingId))).Value.ToString();

                //Find corresponding location dto
                LocationDto locationDto = _locationCodeToDtoLookup[locationCode];
                Assert.IsNotNull(locationDto, "Location record should exist");

                //Get product Code
                String productGroupCode = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationSpaceBayImportMappingList.ProductGroupCodeMappingId))).Value.ToString();

                //Find corresponding product group dto
                ProductGroupDto productGroupDto = _productGroupCodeToDtoLookup[productGroupCode];
                Assert.IsNotNull(productGroupDto, "Product Group record should exist");

                //Get header location space record for location
                LocationSpaceDto locationSpaceDto = _locationSpaceDtos.FirstOrDefault(p => p.LocationId == locationDto.Id);
                Assert.IsNotNull(productGroupDto, "Location Space record should exist");

                //Get location space product group record
                LocationSpaceProductGroupDto locationSpaceProductGroupDto = _locationSpaceProductGroupDtos.Where(p => p.ProductGroupId == productGroupDto.Id && p.LocationSpaceId == locationSpaceDto.Id).FirstOrDefault();
                Assert.IsNotNull(productGroupDto, "Location Space Product Group record should exist");

                //Get the location space bay record
                LocationSpaceBayDto locationSpaceBayDto = locationSpaceBayDtoList.Where(p => p.LocationSpaceProductGroupId == locationSpaceProductGroupDto.Id).FirstOrDefault();
                Assert.IsNotNull(productGroupDto, "Location Space Bay record should exist");

                //Check all of the properties match up
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.OrderMappingId))).Value, locationSpaceBayDto.Order);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.HeightMappingId))).Value, locationSpaceBayDto.Height);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.WidthMappingId))).Value, locationSpaceBayDto.Width);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.DepthMappingId))).Value, locationSpaceBayDto.Depth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.BaseHeightMappingId))).Value, locationSpaceBayDto.BaseHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.BaseWidthMappingId))).Value, locationSpaceBayDto.BaseWidth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.BaseDepthMappingId))).Value, locationSpaceBayDto.BaseDepth);

            }
        }

        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationSpaceBayImportMappingList.NewLocationSpaceBayImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            foreach (ProductGroupDto productGroup in _productGroupCodeToDtoLookup.Values)
            {
                foreach (LocationDto location in _locationCodeToDtoLookup.Values)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        Object dataItem = null;
                        if (mappingItem.PropertyIdentifier == LocationSpaceBayImportMappingList.ProductGroupCodeMappingId)
                        {
                            dataItem = productGroup.Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceBayImportMappingList.LocationCodeMappingId)
                        {
                            dataItem = location.Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceBayImportMappingList.BayTypeMappingId)
                        {
                            dataItem = "End Cap";
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceBayImportMappingList.NotchPitchMappingId)
                        {
                            dataItem = 50;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceBayImportMappingList.FixtureTypeMappingId)
                        {
                            dataItem = Galleria.Ccm.Model.LocationSpaceFixtureType.Standard;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceBayImportMappingList.FixtureShapeMappingId)
                        {
                            dataItem = Galleria.Ccm.Model.LocationSpaceFixtureShapeType.Hexagon;
                        }
                        else
                        {
                            dataItem = base.GenerateRandomData(mappingItem);
                        }

                        ImportFileDataCell importCell =
                                    ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                        importRow.Cells.Add(importCell);

                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }
            }
            return fileData;
        }

        #endregion

        #region Import

        /// <summary>
        /// Import location space bay data
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {

                Random rand = new Random(1);

                //Create test data
                Int32 entityId = TestDataHelper.InsertEntityDtos(dalFactory, 1)[0].Id;
                List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, 1, 3);
                List<ProductGroupDto> prodGroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);
                List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, 5, entityId);
                _locationSpaceDtos = TestDataHelper.InsertLocationSpaceDtos(dalFactory, locDtos, entityId);

                foreach (LocationSpaceDto locationSpaceDto in _locationSpaceDtos)
                {
                    _locationSpaceProductGroupDtos.AddRange(TestDataHelper.InsertLocationSpaceProductGroupDtos(dalFactory, locationSpaceDto.Id, prodGroupDtos));
                }

                _locationCodeToDtoLookup = locDtos.ToDictionary(l => l.Code, StringComparer.OrdinalIgnoreCase);
                _productGroupCodeToDtoLookup = prodGroupDtos.ToDictionary(p => p.Code, StringComparer.OrdinalIgnoreCase);

                //Create data table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create data
                fileData = CreateData();

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportLocationSpaceBayProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportLocationSpaceBayProcess(1, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationSpaceBayProcess>(importProcess);

                //Fetch all location space bay data that exist in the db
                IEnumerable<LocationSpaceBaySearchCriteriaDto> locationSpaceBaySearchCriteriaList;
                List<LocationSpaceBayDto> locationSpaceBayDtoList = new List<LocationSpaceBayDto>();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationSpaceBaySearchCriteriaDal dal = dalContext.GetDal<ILocationSpaceBaySearchCriteriaDal>())
                    {
                        locationSpaceBaySearchCriteriaList = dal.FetchByEntityId(entityId);
                    }

                    using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                    {
                        foreach (LocationSpaceBaySearchCriteriaDto locationSpaceBaySearchCriteria in locationSpaceBaySearchCriteriaList)
                        {
                            locationSpaceBayDtoList.Add(dal.FetchById(locationSpaceBaySearchCriteria.LocationSpaceBayId));
                        }
                    }
                }

                //Check imported data matches inserted.
                ValidateImportedData(locationSpaceBayDtoList, fileData);

                #region Case 2 - [GEM21865] ParentRecord match is case insensitive
                {
                    fileData = CreateData();

                    Int32 locColNum =
                            fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.LocationCodeMappingId));

                    Int32 productGroupColNum =
                        fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.ProductGroupCodeMappingId));

                    Int32 baseDepthColNum =
                        fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceBayImportMappingList.BaseDepthMappingId));

                    Int32 i = 10;
                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        row[locColNum].Value = row[locColNum].Value.ToString().ToUpperInvariant();
                        row[productGroupColNum].Value = row[productGroupColNum].Value.ToString().ToUpperInvariant();

                        row[baseDepthColNum].Value = i;

                        i++;
                    }

                    //construct and execute process
                    importProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationSpaceBayProcess>(
                         new Galleria.Ccm.Imports.Processes.ImportLocationSpaceBayProcess(1, fileData));

                    //Fetch all location space bay data that exist in the db
                    locationSpaceBayDtoList = new List<LocationSpaceBayDto>();
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        using (ILocationSpaceBaySearchCriteriaDal dal = dalContext.GetDal<ILocationSpaceBaySearchCriteriaDal>())
                        {
                            locationSpaceBaySearchCriteriaList = dal.FetchByEntityId(entityId);
                        }

                        using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                        {
                            foreach (LocationSpaceBaySearchCriteriaDto locationSpaceBaySearchCriteria in locationSpaceBaySearchCriteriaList)
                            {
                                locationSpaceBayDtoList.Add(dal.FetchById(locationSpaceBaySearchCriteria.LocationSpaceBayId));
                            }
                        }
                    }


                    ValidateImportedData(locationSpaceBayDtoList, fileData);


                }
                #endregion

            }
        }

        #endregion
    }
}
