﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportLocationSpaceProcess : TestBase
    {
        #region Fields
        private Dictionary<String, Int16> _locCodeToIdLookup;
        private Dictionary<String, Int32> _productGroupCodeToIdLookup;
        #endregion

        #region Test Helpers

        private void InsertDtos(IDalFactory dalFactory)
        {

            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);

            //merch hierarchy
            List<ProductHierarchyDto> productHierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);
            List<ProductLevelDto> productLevel2Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDtos[0].Id, 3);
            List<ProductGroupDto> productGroup2Dtos = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel2Dtos, 2);

            _productGroupCodeToIdLookup = productGroup2Dtos.ToDictionary(p => p.Code, p => p.Id, StringComparer.OrdinalIgnoreCase);

            //loc hierarchy
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
            List<LocationLevelDto> locLevelDtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, locLevelDtos, 3);

            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, entityDtos[0].Id, locGroupDtos, 20);


            _locCodeToIdLookup = locDtos.ToDictionary(l => l.Code, l => l.Id, StringComparer.OrdinalIgnoreCase);

        }

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationSpaceImportMappingList.NewLocationSpaceImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Int32 rowNumber = 1;
            foreach (String locCode in _locCodeToIdLookup.Keys)
            {
                foreach (String productGroupCode in _productGroupCodeToIdLookup.Keys)
                {

                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        Object dataItem = null;

                        if (mappingItem.PropertyIdentifier == LocationSpaceImportMappingList.LocationCodeMappingId)
                        {
                            dataItem = locCode;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceImportMappingList.ProductGroupCodeMappingId)
                        {
                            dataItem = productGroupCode;
                        }
                        else
                        {
                            dataItem = base.GenerateRandomData(mappingItem);
                        }

                        ImportFileDataCell importCell = ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                        importRow.Cells.Add(importCell);

                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;

                }
            }



            return fileData;
        }

        /// <summary>
        /// Validate the imported  data
        /// </summary>
        /// <param name="productPerformanceDataDtoList">The data that has been inserted into the db</param>
        /// <param name="dataTable">The original source</param>
        public void ValidateImportedData(ImportFileData fileData, IDalFactory dalFactory)
        {
            Dictionary<Int32, Int32> mapIdToColNums = fileData.MappingList.ToDictionary(m => m.PropertyIdentifier, m => fileData.GetMappedColNumber(m));

            Int32 entityId = 1;

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                List<LocationSpaceInfoDto> locSpaceInfoDtos;
                using (ILocationSpaceInfoDal dal = dalContext.GetDal<ILocationSpaceInfoDal>())
                {
                    locSpaceInfoDtos = dal.FetchByEntityId(entityId)
                        .OrderBy(l => l.LocationId)
                        .ToList();
                }

                List<LocationSpaceDto> locSpaceDtos = new List<LocationSpaceDto>();
                Dictionary<Int32, List<LocationSpaceProductGroupDto>> spaceGroupsDict = new Dictionary<Int32, List<LocationSpaceProductGroupDto>>();

                //Check correct data in correct column
                foreach (ImportFileDataRow row in fileData.Rows)
                {
                    String locCode =
                        row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(
                            fileData.MappingList.First(c => c.PropertyIdentifier == LocationSpaceImportMappingList.LocationCodeMappingId))).Value
                            .ToString().ToLowerInvariant();

                    String productGroupCode =
                        row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(
                            fileData.MappingList.First(c => c.PropertyIdentifier == LocationSpaceImportMappingList.ProductGroupCodeMappingId))).Value
                            .ToString().ToLowerInvariant();


                    Int16 locId = _locCodeToIdLookup[locCode];
                    Int32 productGroupId = _productGroupCodeToIdLookup[productGroupCode];

                    //find the matching locationspace record
                    LocationSpaceDto locSpaceDto = locSpaceDtos.FirstOrDefault(u => u.LocationId == locId);
                    if (locSpaceDto == null)
                    {
                        LocationSpaceInfoDto infoDto = locSpaceInfoDtos.FirstOrDefault(u => u.LocationId == locId);

                        using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                        {
                            locSpaceDto = dal.FetchById(infoDto.Id);
                        }
                    }
                    Assert.IsNotNull(locSpaceDto);

                    List<LocationSpaceProductGroupDto> locSpaceGroups = null;
                    if (!spaceGroupsDict.TryGetValue(locSpaceDto.Id, out locSpaceGroups))
                    {
                        using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                        {
                            locSpaceGroups = dal.FetchByLocationSpaceId(locSpaceDto.Id).ToList();
                            spaceGroupsDict.Add(locSpaceDto.Id, locSpaceGroups);
                        }
                    }
                    Assert.AreNotEqual(0, locSpaceGroups.Count);


                    //find the matching inserted dto
                    LocationSpaceProductGroupDto insertedDto = locSpaceGroups.FirstOrDefault(p => p.ProductGroupId == productGroupId);
                    Assert.IsNotNull(insertedDto);

                    Assert.AreEqual(row[mapIdToColNums[LocationSpaceImportMappingList.AverageBayWidthMappingId]].Value, insertedDto.AverageBayWidth, "AverageBayWidthMappingId");
                    Assert.AreEqual(row[mapIdToColNums[LocationSpaceImportMappingList.BayCountMappingId]].Value, insertedDto.BayCount, "BayCountMappingId");
                    Assert.AreEqual(row[mapIdToColNums[LocationSpaceImportMappingList.ProductCountMappingId]].Value, insertedDto.ProductCount, "ProductCountMappingId");

                }
            }


        }

        #endregion

        #region Import


        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);
                Int32 entityId = 1;

                //Create data table
                ImportFileData fileData;

                #region Case 1

                fileData = CreateData();

                //construct and execute process
                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationSpaceProcess>(
                     new Galleria.Ccm.Imports.Processes.ImportLocationSpaceProcess(entityId, fileData));

                ValidateImportedData(fileData, dalFactory);

                #endregion

                #region Case 2 - [GEM21865] ParentRecord match is case insensitive
                {
                    fileData = CreateData();

                    Int32 locColNum =
                            fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceImportMappingList.LocationCodeMappingId));

                    Int32 productGroupColNum =
                        fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceImportMappingList.ProductGroupCodeMappingId));

                    Int32 avgWidthColNum =
                        fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceImportMappingList.AverageBayWidthMappingId));

                    Int32 i = 0;
                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        row[locColNum].Value = row[locColNum].Value.ToString().ToUpperInvariant();
                        row[productGroupColNum].Value = row[productGroupColNum].Value.ToString().ToUpperInvariant();

                        row[avgWidthColNum].Value = i;

                        i++;
                    }

                    //construct and execute process
                    importProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationSpaceProcess>(
                         new Galleria.Ccm.Imports.Processes.ImportLocationSpaceProcess(entityId, fileData));

                    ValidateImportedData(fileData, dalFactory);


                }
                #endregion
            }
        }


        #endregion
    }
}
