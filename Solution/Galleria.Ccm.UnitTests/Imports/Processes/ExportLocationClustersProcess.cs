﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Aspose.Cells;
using Galleria.Ccm.Imports.Mappings;
using NUnit.Framework;
using System.IO;
using Galleria.Framework.Processes;
using Galleria.Framework.Imports;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.Interfaces;
using System.Globalization;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ExportLocationClustersProcess : TestBase
    {
        #region Fields

        private Int32 _entityId;
        string _fileName = Directory.GetCurrentDirectory() + @"\LocationClusters.xlsx";
        List<LocationDto> _dtoLocationList = new List<LocationDto>();
        List<ClusterLocationDto> _dtoClusterLocationList = new List<ClusterLocationDto>();
        List<ClusterDto> _dtoClusterList = new List<ClusterDto>();
        List<ClusterSchemeDto> _dtoClusterSchemeList = new List<ClusterSchemeDto>();

        #endregion

        #region Helper Methods
        /// <summary>
        /// Inserts some test data into the data source
        /// </summary>
        /// <param name="dalFactory">The dal factory</param>
        /// <returns>The list of inserted dtos</returns>
        private void InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            _entityId = entityDtos[0].Id;

            //Clear global collections down for new test
            _dtoLocationList.Clear();
            _dtoClusterLocationList.Clear();
            _dtoClusterList.Clear();
            _dtoClusterSchemeList.Clear();

            List<ClusterSchemeDto> dtoList = new List<ClusterSchemeDto>();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                #region Insert Cluster Scheme

                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    _dtoClusterSchemeList.Add(new ClusterSchemeDto()
                    {
                        Name = Guid.NewGuid().ToString(),
                        IsDefault = true,
                        EntityId = _entityId
                    });

                    dal.Upsert(_dtoClusterSchemeList, new ClusterSchemeIsSetDto(true));
                }

                #endregion

                #region Insert Clusters

                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    foreach (ClusterSchemeDto clusterScheme in _dtoClusterSchemeList)
                    {
                        _dtoClusterList.Add(new ClusterDto()
                        {
                            Name = "Cluster 1",
                            ClusterSchemeId = clusterScheme.Id
                        });

                        _dtoClusterList.Add(new ClusterDto()
                        {
                            Name = "Cluster 2",
                            ClusterSchemeId = clusterScheme.Id
                        });
                    }

                    // upsert into the dal
                    dal.Upsert(_dtoClusterList, new ClusterIsSetDto(true));
                }


                #endregion

                dalContext.Commit();
            }

            #region Insert Locations

            //Insert levels
            List<LocationLevelDto> locationLevels = TestDataHelper.InsertLocationLevelDtos(dalFactory, 1, 5);

            //Insert groups
            List<LocationGroupDto> locationGroups = TestDataHelper.InsertLocationGroupDtos(dalFactory, locationLevels, 10);

            _dtoLocationList = TestDataHelper.InsertLocationDtos(dalFactory, _entityId, locationGroups.Select(p => p.Id), 1);

            #endregion

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                #region Insert Peer Group Stores
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    Int32 locationIndex = 0;
                    foreach (ClusterDto cluster in _dtoClusterList)
                    {
                        _dtoClusterLocationList.Add(new ClusterLocationDto()
                        {
                            ClusterId = cluster.Id,
                            Code = _dtoLocationList[locationIndex].Code,
                            LocationId = _dtoLocationList[locationIndex].Id,
                        });

                        locationIndex++;
                    }

                    // upsert into the dal
                    dal.Upsert(_dtoClusterLocationList, new ClusterLocationIsSetDto(true));
                }
                #endregion

                dalContext.Commit();
            }
        }
        #endregion

        #region Export

        /// <summary>
        /// Export product data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Export(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                // insert some dtos into the data source
                InsertDtos(dalFactory);

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportLocationClustersProcess exportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportLocationClustersProcess(_entityId, _fileName, false);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportLocationClustersProcess>(exportProcess);

                // Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];
                DataTable dataTable = worksheet.Cells.ExportDataTable(1, 0, _dtoClusterLocationList.Count, 3);

                int dataRow = 0;


                // Validate exported data
                foreach (DataRow row in dataTable.Rows)
                {
                    //Check scheme name is correct
                    Assert.AreEqual(_dtoClusterSchemeList[0].Name, Convert.ToString(row[LocationClusterImportMappingList.SchemeNameMappingId], CultureInfo.InvariantCulture));
                    Assert.AreEqual(_dtoClusterList[dataRow].Name, Convert.ToString(row[LocationClusterImportMappingList.ClusterNameMappingId], CultureInfo.InvariantCulture));
                    Assert.AreEqual(_dtoClusterLocationList[dataRow].Code, Convert.ToString(row[LocationClusterImportMappingList.LocationCodeMappingId], CultureInfo.InvariantCulture));
                    dataRow++;
                }
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void ExportHeader(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                //get the mapping list of this export type
                IImportMappingList mappingList = LocationClusterImportMappingList.NewLocationClusterImportMappingList();

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportLocationClustersProcess exportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportLocationClustersProcess(_entityId, _fileName, true);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportLocationClustersProcess>(exportProcess);

                // Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];
                DataTable dataTable = worksheet.Cells.ExportDataTable(0, 0, 1, 4);

                // Validate exported data
                foreach (DataRow row in dataTable.Rows)
                {
                    Assert.AreEqual(mappingList[LocationClusterImportMappingList.SchemeNameMappingId].PropertyName, Convert.ToString(row[LocationClusterImportMappingList.SchemeNameMappingId], CultureInfo.InvariantCulture));
                    Assert.AreEqual(mappingList[LocationClusterImportMappingList.ClusterNameMappingId].PropertyName, Convert.ToString(row[LocationClusterImportMappingList.ClusterNameMappingId], CultureInfo.InvariantCulture));
                    Assert.AreEqual(mappingList[LocationClusterImportMappingList.LocationCodeMappingId].PropertyName, Convert.ToString(row[LocationClusterImportMappingList.LocationCodeMappingId], CultureInfo.InvariantCulture));
                }
            }
        }

        /// <summary>
        /// ensure we delete the local export file
        /// </summary>
        [TearDown]
        public void RemoveExportFile()
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }
        }

        #endregion
    }
}
