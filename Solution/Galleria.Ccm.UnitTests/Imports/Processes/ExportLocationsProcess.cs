﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Imports.Mappings;
using Aspose.Cells;
using System.Data;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Framework.Dal;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using System.IO;
using System.Globalization;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ExportLocationsProcess : TestBase
    {
        #region Fields

        string _fileName = Directory.GetCurrentDirectory() + @"\Stores.xlsx";
        List<LocationDto> _sourceDtoList = new List<LocationDto>();

        #endregion

        #region Export

        /// <summary>
        /// Export store data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Export(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                //Create location types
                //Insert levels
                List<LocationLevelDto> locationLevels = TestDataHelper.InsertLocationLevelDtos(dalFactory, 1, 5);

                //Insert groups
                List<LocationGroupDto> locationGroups = TestDataHelper.InsertLocationGroupDtos(dalFactory, locationLevels, 10);

                // insert some dtos into the data source
                _sourceDtoList = TestDataHelper.InsertLocationDtos(dalFactory, 1, locationGroups.Select(p => p.Id), 1);

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportLocationsProcess exportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportLocationsProcess(1, _fileName, false);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportLocationsProcess>(exportProcess);

                // create the same mapping list as export
                IImportMappingList mappingList = LocationImportMappingList.NewLocationImportMappingList();

                // Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];
                DataTable dataTable = worksheet.Cells.ExportDataTable(1, 0, _sourceDtoList.Count, mappingList.Count);

                Int32 dataRow = 0;

                // Validate exported data
                foreach (DataRow row in dataTable.Rows)
                {
                    //Find index of code in mapping list as this is the order data is exported in
                    Int32 codeColIndex = mappingList.IndexOf(mappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId));

                    //Get Location Code value
                    String locationCode = Convert.ToString(row.ItemArray[codeColIndex]);

                    //Find matching location dto to check data against
                    LocationDto locationDto = _sourceDtoList.Where(p => p.Code == locationCode).FirstOrDefault();

                    if (locationDto != null)
                    {
                        Int32 colIndex = 0;
                        foreach (ImportMapping mappingItem in mappingList)
                        {
                            if (mappingItem.PropertyIdentifier == LocationImportMappingList.DateOpenMapId ||
                                    mappingItem.PropertyIdentifier == LocationImportMappingList.DateLastRefittedMapId ||
                                        mappingItem.PropertyIdentifier == LocationImportMappingList.DateClosedMapId)
                            {
                                Assert.AreEqual(Convert.ToDateTime(LocationImportMappingList.GetValueByMappingId(mappingItem.PropertyIdentifier, locationDto, locationGroups)).Date.ToString(), Convert.ToDateTime(row[colIndex]).Date.ToString());
                            }
                            else
                            {
                                Assert.AreEqual(Convert.ToString(LocationImportMappingList.GetValueByMappingId(mappingItem.PropertyIdentifier, locationDto, locationGroups)), Convert.ToString(row[colIndex]));
                            }
                            colIndex++;
                        }
                        dataRow++;
                    }
                }
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void ExportHeader(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportLocationsProcess exportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportLocationsProcess(1, _fileName, true);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportLocationsProcess>(exportProcess);

                // create the same mapping list as export
                IImportMappingList mappingList = LocationImportMappingList.NewLocationImportMappingList();

                // Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];
                DataTable dataTable = worksheet.Cells.ExportDataTable(0, 0, 1, mappingList.Count);

                // Validate exported data
                foreach (DataRow row in dataTable.Rows)
                {
                    Int32 colIndex = 0;
                    foreach (ImportMapping mappingItem in mappingList)
                    {
                        Assert.AreEqual(Convert.ToString(mappingItem.PropertyName), (Convert.ToString(row[colIndex], CultureInfo.InvariantCulture)));
                        colIndex++;
                    }
                }
            }
        }

        /// <summary>
        /// ensure we delete the local export file
        /// </summary>
        [TearDown]
        public void RemoveExportFile()
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }
        }

        #endregion
    }
}
