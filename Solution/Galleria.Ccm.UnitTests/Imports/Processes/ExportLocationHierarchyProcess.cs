﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25554 : L.Hodson
//		Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using Aspose.Cells;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    [Serializable]
    public partial class ExportLocationHierarchyProcess : TestBase
    {
        #region Fields
        string _fileName = Directory.GetCurrentDirectory() + @"LocationHierarchy.xlsx";
        #endregion

        #region Export
        ///<summary>
        ///Export store space data
        ///</summary>
        ///<param name="dalFactoryType">The dal factory Type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Export(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                LocationHierarchy structure = LocationHierarchy.FetchByEntityId(1);

                //structure = TestDataHelper.PopulateDefaultLocationHierarchyLevels(structure.Id);

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportLocationHierarchyProcess exportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportLocationHierarchyProcess(1, _fileName, false);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportLocationHierarchyProcess>(exportProcess);

                //Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];
                DataTable dataTable = worksheet.Cells.ExportDataTable(0, 0, (worksheet.Cells.MaxRow + 1), (worksheet.Cells.MaxColumn + 1));

                IEnumerable<LocationGroup> groups = structure.EnumerateAllGroups().Where(p => p.IsRoot != true);

                //Remove header row
                dataTable.Rows.RemoveAt(0);

                //Assert all of the groups have been exported
                Assert.AreEqual(groups.Count(), (dataTable.Rows.Count), "Rows are missing");

                Int32 rowIndex = 0;
                //Check all groups have been exported
                foreach (LocationGroup group in groups)
                {
                    if (group.ChildList.Count > 0) //Level group
                    {
                        Assert.AreEqual(group.Code, Convert.ToString(dataTable.Rows[rowIndex].ItemArray[dataTable.Columns.Count - dataTable.Columns.Count]));
                        Assert.AreEqual(group.Name, Convert.ToString(dataTable.Rows[rowIndex].ItemArray[dataTable.Columns.Count - (dataTable.Columns.Count - 1)]));
                    }
                    else // Lowest level group
                    {
                        Assert.AreEqual(group.Code, Convert.ToString(dataTable.Rows[rowIndex].ItemArray[dataTable.Columns.Count - (dataTable.Columns.Count - 2)]));
                        Assert.AreEqual(group.Name, Convert.ToString(dataTable.Rows[rowIndex].ItemArray[dataTable.Columns.Count - (dataTable.Columns.Count - 3)]));

                        //Find parent group
                        LocationGroup parentGroup = groups.Where(p => p.Id == group.ParentGroup.Id).First();

                        Assert.AreEqual(parentGroup.Code, Convert.ToString(dataTable.Rows[rowIndex].ItemArray[dataTable.Columns.Count - dataTable.Columns.Count]));
                        Assert.AreEqual(parentGroup.Name, Convert.ToString(dataTable.Rows[rowIndex].ItemArray[dataTable.Columns.Count - (dataTable.Columns.Count - 1)]));
                    }

                    rowIndex++;
                }
            }
        }

        #endregion

        #region Export Header

        /// <summary>
        /// Export store space data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void ExportHeader(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                LocationHierarchy structure = LocationHierarchy.FetchByEntityId(1);

                //structure = TestDataHelper.PopulateDefaultLocationHierarchyLevels(structure.Id);

                IEnumerable<LocationLevel> levels = structure.EnumerateAllLevels();

                IImportMappingList mappingList = LocationHierarchyImportMappingList.NewList(levels.Where(p => p.IsRoot == false).Select(p => p.Name));

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportLocationHierarchyProcess exportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportLocationHierarchyProcess(1, _fileName, true);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportLocationHierarchyProcess>(exportProcess);

                ///Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];

                DataTable dataTable = worksheet.Cells.ExportDataTable(0, 0, (worksheet.Cells.MaxRow + 1), (worksheet.Cells.MaxColumn + 1));

                // Validate exported data
                foreach (DataRow dt in dataTable.Rows)
                {
                    for (int i = 0; i < dt.ItemArray.Length; i++)
                    {
                        Assert.AreEqual(mappingList[i].PropertyName, Convert.ToString(dt[i], CultureInfo.InvariantCulture));
                    }

                }
            }
        }
        /// <summary>
        /// ensure we delete the local export file
        /// </summary>
        [TearDown]
        public void RemoveExportFile()
        {
            if (System.IO.File.Exists(_fileName))
            {
                System.IO.File.Delete(_fileName);

            }
        }

        #endregion
    }
}
