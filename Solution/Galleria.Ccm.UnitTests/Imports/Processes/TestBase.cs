﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Imports;
using Microsoft.SqlServer.Management.Smo;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public abstract class TestBase
    {
        #region Fields
        private List<Type> _dalFactoryTypes;

        private String _sqlDatabaseName;
        private String _sqlDatabaseDataFilePath;
        private String _sqlDatabaseLogFilePath;
        public String _sqlTemplateDataFilePath;
        public String _sqlTemplateLogFilePath;
        #endregion

        #region Serializable

        /// <summary>
        /// Tests that a dto is serializable
        /// </summary>
        protected static void Serialize(object obj)
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
            }
        }

        #endregion

        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get { return _dalFactoryTypes; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TestBase()
        {
            // create our list of dal factory types to create
            _dalFactoryTypes = new List<Type>();
            _dalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.Mock.DalFactory));
            _dalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.Mssql.DalFactory));
        }
        #endregion

        #region Methods

        [SetUp]
        public void SetUp()
        {
            #region User Dal
            String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName, "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (string file in Directory.GetFiles(templateDir))
                {
                    //if (!Path.GetFileName(file).StartsWith(templateName))
                    //{
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch
                    {
                        // No point in crying over spilt milk
                    }
                    //}
                }
            }


            String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
            var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
            userFactory.UnitTestFolder = templateDir;
            DalContainer.RegisterFactory(Constants.UserDal, userFactory);
            #endregion
        }

        /// <summary>
        /// Creates our dal factory from the provided type
        /// </summary>
        /// <param name="dalFactoryType">The type of dal factory to create</param>
        protected IDalFactory CreateDalFactory(Type dalFactoryType)
        {
            try
            {
                // The name to use for database templates.
                String templateName = TestDataHelper.TestDatabaseName;

                // The directory in which database templates will be created.
                String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName,
                    "Test Databases");
                if (!Directory.Exists(templateDir))
                {
                    // Directory doesn't exist yet: create it.
                    Directory.CreateDirectory(templateDir);
                }
                else
                {
                    // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                    // this just to be tidy.
                    foreach (String file in Directory.GetFiles(templateDir))
                    {
                        if (!Path.GetFileName(file).StartsWith(templateName))
                        {
                            try
                            {
                                System.IO.File.Delete(file);
                            }
                            catch
                            {
                                // No point in crying over spilt milk
                            }
                        }
                    }
                }

                IDalFactory dalFactory = null;
                if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mssql.DalFactory))
                {
                    //[SA-13322] in order to reduce the amount of time taken to run the
                    // unit tests, we create 'template' database for this series
                    // of unit tests. Then, for each test, simply copy the file
                    // rather than re-creating the database each time

                    // create a dal configuration item for this factory
                    DalFactoryConfigElement dalFactoryConfig;

                    // connect to the local server
                    Server server = new Server(TestingConstants.MssqlServerInstance);

                    // get the template file names
                    _sqlTemplateDataFilePath = Path.Combine(templateDir, templateName + ".mdf");
                    _sqlTemplateLogFilePath = Path.Combine(templateDir, templateName + "_Log.ldf");

                    //If template hasn't been created
                    if (!System.IO.File.Exists(_sqlTemplateDataFilePath))
                    {

                        //Get original files to build from
                        String templateDataPath = Path.Combine(Environment.CurrentDirectory, "Environment\\CCMUnitTestsTemplate.mdf");
                        String templateLogPath = Path.Combine(Environment.CurrentDirectory, "Environment\\CCMUnitTestsTemplate_log.ldf");

                        //Copy original and create templates
                        System.IO.File.Copy(templateDataPath, _sqlTemplateDataFilePath);
                        System.IO.File.Copy(templateLogPath, _sqlTemplateLogFilePath);

                        //Attach template
                        server.AttachDatabase(templateName, new StringCollection() { _sqlTemplateDataFilePath, _sqlTemplateLogFilePath }, AttachOptions.None);

                        //Create config
                        dalFactoryConfig = new DalFactoryConfigElement();
                        // configure dal factory
                        dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                        {
                            Name = "Server",
                            Value = TestingConstants.MssqlServerInstance
                        });
                        dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                        {
                            Name = "Database",
                            Value = templateName
                        });

                        // create the dal factory
                        dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig);

                        // perform an upgrade on the database
                        dalFactory.Upgrade();

                        //clear connections
                        SqlConnection.ClearAllPools();

                        // detach db store as copy
                        server.DetachDatabase(templateName, false);
                    }

                    // copy the template files
                    _sqlDatabaseName = Guid.NewGuid().ToString();
                    _sqlDatabaseDataFilePath = Path.Combine(Path.GetDirectoryName(_sqlTemplateDataFilePath), _sqlDatabaseName + ".mdf");
                    _sqlDatabaseLogFilePath = Path.Combine(Path.GetDirectoryName(_sqlTemplateLogFilePath), _sqlDatabaseName + "_Log.ldf");
                    System.IO.File.Copy(_sqlTemplateDataFilePath, _sqlDatabaseDataFilePath);
                    System.IO.File.Copy(_sqlTemplateLogFilePath, _sqlDatabaseLogFilePath);

                    //Attach the database
                    server.AttachDatabase(_sqlDatabaseName, new StringCollection() { _sqlDatabaseDataFilePath, _sqlDatabaseLogFilePath });

                    //create new config
                    dalFactoryConfig = new DalFactoryConfigElement();

                    // configure dal factory
                    dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                    {
                        Name = "Server",
                        Value = TestingConstants.MssqlServerInstance
                    });
                    dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                    {
                        Name = "Database",
                        Value = _sqlDatabaseName
                    });

                    // create the dal factory
                    dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig);

                    DalContainer.RegisterFactory("Mssql", dalFactory);
                    DalContainer.DalName = "Mssql";
                }
                else if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
                {
                    // create the dal factory
                    dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();

                    DalContainer.RegisterFactory("Mock", dalFactory);
                    DalContainer.DalName = "Mock";

                    dalFactory.CreateDatabase();
                }
                //else if (dalFactoryType == typeof(Galleria.Ccm.Dal.VistaDb.DalFactory))
                //{
                //    // in order to reduce the amount of time taken to run the
                //    // unit tests, we create 'template' database for this series
                //    // of unit tests. Then, for each test, simply copy the file
                //    // rather than re-creating the database each time

                //    _vistaDbtemplateFilename = Path.Combine(templateDir, templateName + ".vdb4");

                //    // create the template database if required
                //    if (!File.Exists(_vistaDbtemplateFilename))
                //    {
                //        // create a dal factory
                //        //[SA-13499] Pass exact path through.
                //        dalFactory = new Galleria.Ccm.Dal.VistaDb.DalFactory(_vistaDbtemplateFilename);

                //        dalFactory.CreateDatabase();

                //        // now perform an upgrade to create the database
                //        dalFactory.Upgrade();
                //    }

                //    // copy the template file
                //    _databaseFilename = Path.Combine(Path.GetDirectoryName(_vistaDbtemplateFilename), Guid.NewGuid().ToString() + ".vdb4");
                //    File.Copy(_vistaDbtemplateFilename, _databaseFilename);

                //    // create a new data factory for this file
                //    dalFactory = new Galleria.Ccm.Dal.VistaDb.DalFactory(_databaseFilename);

                //    DalContainer.RegisterFactory("VistaDb", dalFactory);
                //    DalContainer.DalName = "VistaDb";
                //}


                //authenticate
                bool authenticated = Galleria.Ccm.Security.DomainPrincipal.Authenticate();


                // and return the factory
                return dalFactory;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// Called when the test fixture is teared down
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
            //try
            //{
            //    //Clear vista db template if created
            //    if (_vistaDbtemplateFilename != null)
            //    {
            //        File.Delete(_vistaDbtemplateFilename);

            //    }

            //    //Clear sql templates if created
            //    if (_sqlTemplateDataFilePath != null && _sqlTemplateLogFilePath != null)
            //    {
            //        File.Delete(_sqlTemplateDataFilePath);
            //        File.Delete(_sqlTemplateLogFilePath);
            //    }
            //}
            //catch (Exception e)
            //{
            //    Debug.WriteLine(e.ToString());
            //    throw;
            //}
        }

        /// <summary>
        /// Called after a test has been executed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            try
            {
                // ensure the Mssql data is detached
                if (_sqlDatabaseDataFilePath != null)
                {
                    KillDatabase(_sqlDatabaseName);
                    System.IO.File.Delete(_sqlDatabaseDataFilePath);
                    System.IO.File.Delete(_sqlDatabaseLogFilePath);
                }

                // ensure we delete the local databases
                //if (_databaseFilename != null)
                //{
                //    File.Delete(_databaseFilename);

                //}
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// Creates the specified database on the local sql server
        /// </summary>
        /// <param name="databaseName">The database name</param>
        private static void CreateDatabase(String databaseName)
        {
            // kill the database just in case
            KillDatabase(databaseName);

            // connect to the local server
            Server server = new Server(TestingConstants.MssqlServerInstance);

            // create the database on the local server
            Database database = new Database(server, databaseName);
            database.Create();
        }

        /// <summary>
        /// Deletes the specified database from the local sql server
        /// </summary>
        /// <param name="databaseName">The database to delete</param>
        private static void KillDatabase(String databaseName)
        {
            SqlConnection.ClearAllPools();
            Server server = new Server(TestingConstants.MssqlServerInstance);
            if (server.Databases[databaseName] != null)
            {
                server.KillDatabase(databaseName);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Method to construct default data structures
        /// </summary>
        /// <param name="dalFactory"></param>
        public void SetupDefaultData(IDalFactory dalFactory)
        {
            //Create new entity
            Entity entity = Entity.NewEntity();
            entity.Name = Guid.NewGuid().ToString().Substring(0, 5);
            entity = entity.Save();

            //Set global context entity id to be 1 for the tests;
            Csla.ApplicationContext.GlobalContext["EntityId"] = entity.Id;

            //Create default hierarchy
            PopulateDefaultProductHierarchyLevels(entity.Id);
        }

        /// <summary>
        /// Method to create random data based on mapping items type
        /// </summary>
        /// <param name="mappingItem"></param>
        /// <returns></returns>
        public Object GenerateRandomData(ImportMapping mappingItem)
        {
            Object returnValue = null;

            Random rand = new Random(1); //seeded so this always produces the same values.

            //Get type
            Type t = null;
            if (mappingItem.PropertyType.IsGenericType && mappingItem.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                t = Nullable.GetUnderlyingType(mappingItem.PropertyType);
            }
            else
            {
                t = mappingItem.PropertyType;
            }


            //Generate data
            if (t == typeof(Boolean))
            {
                returnValue = true;
            }
            else if (t == typeof(DateTime))
            {
                returnValue = DateTime.Today;
            }
            else if (t == typeof(Byte))
            {
                returnValue = rand.Next(1, (Int32)mappingItem.PropertyMax);
            }
            else if (t == typeof(Single))
            {
                Int32 propMax = (Int32)mappingItem.PropertyMax;
                if (propMax > 1)
                {
                    returnValue = Convert.ToSingle(rand.Next(1, (Int32)mappingItem.PropertyMax));
                }
                else { returnValue = 1F; }
            }
            else if (t == typeof(Int16) || t == typeof(Int16?))
            {
                returnValue = Convert.ToInt16(rand.Next(1, (Int32)mappingItem.PropertyMax));
            }
            else if (t == typeof(String))
            {
                returnValue = Guid.NewGuid().ToString().Substring(0, 13);
            }
            else if (t.BaseType == typeof(Enum))
            {
                throw new Exception("Enum value must be specifically set by the processes calling this method");
            }
            else
            {
                Int32 propMax = (Int32)mappingItem.PropertyMax;
                if (propMax > 1)
                {
                    returnValue = rand.Next(1, (Int32)mappingItem.PropertyMax);
                }
                else
                {
                    returnValue = 1;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Populate a default product hierarchy (setup with levels) with product groups
        /// </summary>
        /// <param name="hierarchyId"></param>
        /// <returns>ProductHierarchy</returns>
        private static ProductHierarchy PopulateDefaultProductHierarchyLevels(Int32 hierarchyId)
        {
            ProductHierarchy hierarchy = ProductHierarchy.FetchById(hierarchyId);

            List<ProductLevel> levels = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).ToList();

            ProductLevel level1 = levels[0];
            ProductLevel level2 = levels[1];

            //add 3 level1 groups
            ProductGroup level1Group1 = ProductGroup.NewProductGroup(level1.Id);
            level1Group1.Name = "level1group1";
            level1Group1.Code = Guid.NewGuid().ToString();
            ProductGroup level1Group2 = ProductGroup.NewProductGroup(level1.Id);
            level1Group2.Name = "level1group2";
            level1Group2.Code = Guid.NewGuid().ToString();
            ProductGroup level1Group3 = ProductGroup.NewProductGroup(level1.Id);
            level1Group3.Name = "level1group3";
            level1Group3.Code = Guid.NewGuid().ToString();
            hierarchy.RootGroup.ChildList.Add(level1Group1);
            hierarchy.RootGroup.ChildList.Add(level1Group2);
            hierarchy.RootGroup.ChildList.Add(level1Group3);

            //add 2 level2 groups per level1 group
            foreach (ProductGroup level1Group in hierarchy.RootGroup.ChildList)
            {
                for (int i = 1; i <= 2; i++)
                {
                    ProductGroup level2Group = ProductGroup.NewProductGroup(level2.Id);
                    level2Group.Name = "level2group" + i.ToString();
                    level2Group.Code = Guid.NewGuid().ToString();
                    level1Group.ChildList.Add(level2Group);
                }
            }

            return hierarchy.Save();
        }

        /// <summary>
        /// Helper method to create empty filedata using the given mapping list
        /// </summary>
        /// <param name="mappingList"></param>
        /// <returns></returns>
        protected ImportFileData CreateEmptyFileData(IImportMappingList mappingList)
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = mappingList;

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            return fileData;
        }


        /// <summary>
        /// Helper to add rows to the given filedata
        /// </summary>
        /// <param name="fileData"></param>
        /// <param name="data"></param>
        protected void AddDataRows(ImportFileData fileData, List<String[]> data)
        {
            Int32 rowNumber = 1;
            for (int i = 0; i < data.Count; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);
                String[] rowData = data[i];

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, rowData[cellColNumber - 1]));
                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
        }

        #endregion

    }
}
