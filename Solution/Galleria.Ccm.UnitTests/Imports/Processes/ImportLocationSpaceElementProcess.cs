﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Imports;
using Galleria.Framework.Dal;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportLocationSpaceElementProcess : TestBase
    {
        #region Fields

        List<ProductGroupDto> _productGroupDtoList;
        List<LocationDto> _locationDtoList;
        List<LocationSpaceDto> _locationSpaceDtos;
        List<LocationSpaceProductGroupDto> _locationSpaceProductGroupDtos;
        List<LocationSpaceBayDto> _locationSpaceBayDtos;

        #endregion

        #region Helper Methods

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(ImportFileData fileData, IDalFactory dalFactory)
        {
            foreach (ImportFileDataRow row in fileData.Rows)
            {
                //Get Location Code
                String locationCode = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(
                    fileData.MappingList.First(c => c.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId)))
                    .Value.ToString().ToLowerInvariant();
                byte order = Convert.ToByte(row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(c => c.PropertyIdentifier == LocationSpaceElementImportMappingList.OrderMappingId))).Value.ToString());

                //Find corresponding location dto
                LocationDto locationDto = _locationDtoList.FirstOrDefault(p => p.Code.ToLowerInvariant() == locationCode);
                Assert.IsNotNull(locationDto, "Location record should exist");

                //Get product Code
                String productGroupCode = row.Cells.First(p => p.ColumnNumber == fileData.GetMappedColNumber(
                    fileData.MappingList.First(c => c.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId)))
                    .Value.ToString().ToLowerInvariant();

                //Find corresponding product group dto
                ProductGroupDto productGroupDto = _productGroupDtoList.FirstOrDefault(p => p.Code.ToLowerInvariant() == productGroupCode);
                Assert.IsNotNull(productGroupDto, "Product Group record should exist");

                //Get header location space record for location
                LocationSpaceDto locationSpaceDto = _locationSpaceDtos.FirstOrDefault(p => p.LocationId == locationDto.Id);
                Assert.IsNotNull(locationSpaceDto, "Location Space record should exist");

                //Get location space product group record
                LocationSpaceProductGroupDto locationSpaceProductGroupDto = _locationSpaceProductGroupDtos.Where(p => p.ProductGroupId == productGroupDto.Id && p.LocationSpaceId == locationSpaceDto.Id).FirstOrDefault();
                Assert.IsNotNull(locationSpaceProductGroupDto, "Location Space Product Group record should exist");

                //Get the location space bay record
                LocationSpaceBayDto locationSpaceBayDto = _locationSpaceBayDtos.Where(p => p.LocationSpaceProductGroupId == locationSpaceProductGroupDto.Id).FirstOrDefault();
                Assert.IsNotNull(locationSpaceBayDto, "Location Space Bay record should exist");

                //Get the location space element record
                List<LocationSpaceElementDto> locationSpaceElementDtoList;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                    {
                        locationSpaceElementDtoList = dal.FetchByLocationSpaceBayId(locationSpaceBayDto.Id).ToList();
                    }
                }

                LocationSpaceElementDto locationSpaceElementDto = locationSpaceElementDtoList.Where(p => p.LocationSpaceBayId == locationSpaceBayDto.Id && p.Order == order).FirstOrDefault();
                Assert.IsNotNull(locationSpaceElementDto, "Location Space Element record should exist");

                //Check all of the properties match up                
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.OrderMappingId))).Value, locationSpaceElementDto.Order);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.XPositionMappingId))).Value, locationSpaceElementDto.X);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.YPositionMappingId))).Value, locationSpaceElementDto.Y);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ZPositionMappingId))).Value, locationSpaceElementDto.Z);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.MerchandisableHeightMappingId))).Value, locationSpaceElementDto.MerchandisableHeight);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.HeightMappingId))).Value, locationSpaceElementDto.Height);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.WidthMappingId))).Value, locationSpaceElementDto.Width);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.DepthMappingId))).Value, locationSpaceElementDto.Depth);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.FaceThicknessMappingId))).Value, locationSpaceElementDto.FaceThickness);
                Assert.AreEqual(row.Cells.First(c => c.ColumnNumber == fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.NotchNumberMappingId))).Value, locationSpaceElementDto.NotchNumber);
            }
        }

        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationSpaceElementImportMappingList.NewLocationSpaceElementImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            //foreach (ProductGroupDto productGroup in _productGroupDtoList)
            //{
            //    foreach (LocationDto location in _locationDtoList)
            //    {
            foreach (LocationSpaceBayDto locationSpaceBay in _locationSpaceBayDtos)
            {
                //Find bays product group \ location
                LocationSpaceProductGroupDto locationSpaceProductGroupDto = _locationSpaceProductGroupDtos.Where(p => p.Id == locationSpaceBay.LocationSpaceProductGroupId).FirstOrDefault();
                LocationSpaceDto locationSpaceDto = _locationSpaceDtos.Where(p => p.Id == locationSpaceProductGroupDto.LocationSpaceId).FirstOrDefault();
                ProductGroupDto productGroup = _productGroupDtoList.Where(p => p.Id == locationSpaceProductGroupDto.ProductGroupId).FirstOrDefault();
                LocationDto location = _locationDtoList.Where(p => p.Id == locationSpaceDto.LocationId).FirstOrDefault();

                for (byte i = 1; i <= 2; i++)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        Object dataItem = null;
                        if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId)
                        {
                            dataItem = productGroup.Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId)
                        {
                            dataItem = location.Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.BayOrderMappingId)
                        {
                            dataItem = locationSpaceBay.Order;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationSpaceElementImportMappingList.OrderMappingId)
                        {
                            dataItem = i;
                        }
                        else
                        {
                            dataItem = base.GenerateRandomData(mappingItem);
                        }

                        ImportFileDataCell importCell =
                                    ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                        importRow.Cells.Add(importCell);

                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }
            }
            //    }
            //}
            return fileData;
        }

        private void InsertDtos(IDalFactory dalFactory)
        {
            //Create test data
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            Int32 entityId = entityDtos[0].Id;

            //merch hierarchy
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, 1, 3);
            List<ProductGroupDto> prodGroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);

            //location hierarchy
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
            List<LocationLevelDto> locLevelsDtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, locLevelsDtos, 2);
            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, entityId, locGroupDtos, 5);


            //location space
            List<LocationSpaceDto> locSpaceDtos = TestDataHelper.InsertLocationSpaceDtos(dalFactory, locDtos, entityId);

            List<LocationSpaceProductGroupDto> locSpaceProductGroupDtos = new List<LocationSpaceProductGroupDto>();
            foreach (LocationSpaceDto locationSpaceDto in locSpaceDtos)
            {
                locSpaceProductGroupDtos.AddRange(
                    TestDataHelper.InsertLocationSpaceProductGroupDtos(dalFactory, locationSpaceDto.Id, prodGroupDtos));
            }

            List<LocationSpaceBayDto> locSpaceBayDtos = new List<LocationSpaceBayDto>();
            foreach (LocationSpaceProductGroupDto locationSpaceProductGroupDto in locSpaceProductGroupDtos)
            {
                locSpaceBayDtos.AddRange(TestDataHelper.InsertLocationSpaceBayDtos(dalFactory, locationSpaceProductGroupDto.Id, 1));
            }

            //take required refs
            _productGroupDtoList = prodGroupDtos;
            _locationDtoList = locDtos;
            _locationSpaceDtos = locSpaceDtos;
            _locationSpaceProductGroupDtos = locSpaceProductGroupDtos;
            _locationSpaceBayDtos = locSpaceBayDtos;
        }

        #endregion

        #region Import

        /// <summary>
        /// Import location space bay data
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);
                Int32 entityId = 1;

                //Create data table
                ImportFileData fileData;

                #region Case 1 - Valid import

                //Create data
                fileData = CreateData();

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportLocationSpaceElementProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportLocationSpaceElementProcess(entityId, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationSpaceElementProcess>(importProcess);


                //Check imported data matches inserted.
                ValidateImportedData(fileData, dalFactory);

                #endregion

                #region Case 2 - [GEM21865] ParentRecord match is case insensitive
                {
                    fileData = CreateData();

                    Int32 locColNum =
                            fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId));

                    Int32 productGroupColNum =
                        fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId));

                    Int32 merchHeightColNum =
                        fileData.GetMappedColNumber(
                            fileData.MappingList.First(p => p.PropertyIdentifier == LocationSpaceElementImportMappingList.MerchandisableHeightMappingId));

                    Int32 i = 10;
                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        row[locColNum].Value = row[locColNum].Value.ToString().ToUpperInvariant();
                        row[productGroupColNum].Value = row[productGroupColNum].Value.ToString().ToUpperInvariant();

                        row[merchHeightColNum].Value = i;

                        i++;
                    }

                    //construct and execute process
                    importProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationSpaceElementProcess>(
                         new Galleria.Ccm.Imports.Processes.ImportLocationSpaceElementProcess(1, fileData));


                    ValidateImportedData(fileData, dalFactory);


                }
                #endregion

            }
        }

        #endregion
    }
}
