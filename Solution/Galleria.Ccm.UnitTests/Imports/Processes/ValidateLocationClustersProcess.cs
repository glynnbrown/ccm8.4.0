﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Processes;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ValidateLocationClustersProcess : TestBase
    {
        #region Fields
        List<LocationDto> _locationDtoList;
        Int32 _numDtosToInsert = 50;
        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            //Create 2 schemes with all locations assigned to their own groups
            for (Int32 i = 0; i < 2; i++)
            {
                String schemeName = Guid.NewGuid().ToString();
                Int32 rowNumber = 1;
                for (Int32 j = 0; j < _numDtosToInsert; j++)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        Object dataItem = null;
                        if (mappingItem.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId)
                        {
                            dataItem = _locationDtoList[j].Code;
                        }
                        else if (mappingItem.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId)
                        {
                            dataItem = schemeName;
                        }
                        else
                        {
                            dataItem = base.GenerateRandomData(mappingItem);
                        }

                        ImportFileDataCell importCell =
                                    ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                        importRow.Cells.Add(importCell);

                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }
            }
            return fileData;
        }

        private void InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);

            //location hierarchy 
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
            List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 5);

            List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, locGroupDtos, 10);
            _locationDtoList = locDtos;
        }

        #endregion

        #region Validate

        /// <summary>
        /// Validate location data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Validate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);

                #region Case 1 - Valid Data
                //Create Data Table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create Data
                fileData = CreateData();

                //Construct Process
                Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess case1ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(case1ValidateProcess);

                //Validate the Process Once Completed
                Assert.AreEqual(0, case1ValidateProcess.Errors.Count, "No Errors Should have Been Found");
                Assert.IsNotNull(case1ValidateProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, case1ValidateProcess.ValidatedData.Rows.Count, "Output Row Count Must Match Input Row Count");

                #endregion

                #region Case 2 - Invalid Data Type

                #region Single Error

                //No column that can accmodate invalid data type data.

                #endregion

                #region > 10% Errors

                //No column that can accmodate invalid data type data.

                #endregion

                #endregion

                #region Case 3 - Missing Data

                #region Test Non Mandatory Property

                //No  nonmandatory fields

                #endregion

                #region Test Mandatory Property
                //Create data
                fileData = CreateData();

                //Set missing data
                //Set location size data to be a string value
                Int32 invalidRowNumber = 2;
                Int32 invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId));
                Object invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess case31ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(case31ValidateProcess);

                //Get error item
                ValidationErrorItem errorItem = case31ValidateProcess.Errors.First().Errors.First();

                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual("<empty>", errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error as column is mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                #endregion

                #endregion

                #region Case 4 - Invalid Data Length

                #region Max Length

                //Create data
                fileData = CreateData();

                //Set invalid length data
                //Set group name data to be a string value
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.ClusterNameMappingId));
                Object invalidData = new string('*', 101);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess case4ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(case4ValidateProcess);

                //Validate the process once completed
                NUnit.Framework.Assert.AreEqual(1, case4ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                ValidationErrorGroup errorGroup = case4ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual("Invalid identifying data", errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.ClusterNameMappingId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_AboveMaxLengthErrorMessage, fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.ClusterNameMappingId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case4ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case4ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Min Length

                //No string property with min length greater than 1 for Product. If you set a string to have a length
                //of 0, it is caught by the missing data validation.

                #endregion

                #endregion

                #region Case 5 - Out of Bounds Data

                #region Upper Bound
                //No numeric properties
                #endregion

                #region Lower Bound
                //No numeric properties
                #endregion

                #endregion

                #region Case 6 - Parent Record

                //Create Data
                fileData = CreateData();

                //Set incorrect parent record
                //Set location code to nonexistent location
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId));
                String invalidLocationCode = "j";

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidLocationCode;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess case6ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(case6ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case6ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case6ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");
                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidLocationCode.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case6ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case6ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion

                #region Case 7 - Duplicate Record

                //Create data
                fileData = CreateData();

                //Choose a row to get original data from
                Int32 originalRowNumber = 4;
                invalidRowNumber = 5;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId));

                //Set invalid duplicate data
                ImportFileDataRow duplicateData = fileData.Rows[originalRowNumber];
                fileData.Rows[invalidRowNumber] = duplicateData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess case7ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(case7ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case7ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case7ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual("Invalid identifying data", errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");

                //Check details of these 2 errors
                foreach (ValidationErrorItem error in errorGroup.Errors)
                {
                    //Check row numbers (compensate for zero based index and row row being headers -> +2)
                    Assert.IsTrue((error.RowNumber == (originalRowNumber) || error.RowNumber == (invalidRowNumber)) ? true : false, "Row number should be one of the 2 duplicate rows");

                    //Check column number
                    Assert.AreEqual((invalidColumnNumber), error.ColumnNumber, "Column number should match the invalid column's");

                    //Check data affected
                    Assert.AreEqual(duplicateData[invalidColumnNumber].ToString(), error.DataAffected, "Data affected should match duplicate data");

                    //Check duplicate error flag
                    Assert.IsTrue(error.IsDuplicateError, "Should be marked as duplicate error");
                }

                //Validate process properties
                Assert.IsNotNull(case7ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case7ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion
            }
        }

        #endregion

        #region Ticket

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM21865_ParentRecordCheckIsCaseInsensitive(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);
                Int32 entityId = 1;

                #region check valid data
                ImportFileData fileData = CreateData();

                var validationProcess =
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                #endregion

                List<Int32> checkColNumbers =
                    new List<Int32>()
                    {
                        //Scheme
                        fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId)),

                        //cluster
                        fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.ClusterNameMappingId)),

                         //code
                        fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId)),
                    };

                //+ lower invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToLowerInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }

                //+upper invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationClustersProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }


            }


        }

        #endregion
    }
}
