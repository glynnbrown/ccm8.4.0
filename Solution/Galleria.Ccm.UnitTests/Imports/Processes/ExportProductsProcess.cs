﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;
using Aspose.Cells;
using System.Data;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Ccm.Imports.Mappings;
using System.Globalization;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ExportProductsProcess : TestBase
    {
        #region Fields

        string _fileName = Directory.GetCurrentDirectory() + @"\Products.xlsx";
        List<ProductDto> _sourceDtoList = new List<ProductDto>();

        #endregion

        #region Helper Methods

        #endregion

        #region Export

        /// <summary>
        /// Export product data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Export(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                // insert some dtos into the data source
                _sourceDtoList = TestDataHelper.InsertProductDtos(dalFactory, 50, 1);

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportProductsProcess SExportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportProductsProcess(1, _fileName, false);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportProductsProcess>(SExportProcess);

                // create the same mapping list as export
                IImportMappingList mappingList = ProductImportMappingList.NewProductImportMappingList(true);

                // Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];
                DataTable dataTable = worksheet.Cells.ExportDataTable(1, 0, _sourceDtoList.Count, mappingList.Count);

                Int32 dataRow = 0;

                // Validate exported data
                foreach (DataRow row in dataTable.Rows)
                {
                    //Find index of gtin in mapping list as this is the order data is exported in
                    Int32 gtinColIndex = mappingList.IndexOf(mappingList.First(p => p.PropertyIdentifier == ProductImportMappingList.GtinMapId));

                    //Get Product GTIN value
                    String productGTIN = Convert.ToString(row.ItemArray[gtinColIndex]);

                    //Find matching product dto to check data against
                    ProductDto productDto = _sourceDtoList.Where(p => p.Gtin == productGTIN).FirstOrDefault();

                    ProductHierarchyDto hierarchyDto;
                    IEnumerable<ProductGroupDto> productGroupDtoList;
                    //Fetch attribute data dto
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                        {
                            hierarchyDto = dal.FetchByEntityId(1);
                        }
                        using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                        {
                            productGroupDtoList = dal.FetchByProductHierarchyId(hierarchyDto.Id);
                        }
                    }

                    if (productDto != null)
                    {
                        Int32 colIndex = 0;
                        foreach (ImportMapping mappingItem in mappingList)
                        {
                            Assert.AreEqual(
                                Convert.ToString(
                                ProductImportMappingList.GetValueByMappingId(mappingItem.PropertyIdentifier, productDto, productGroupDtoList, new Framework.Planograms.Dal.DataTransferObjects.CustomAttributeDataDto())),
                                Convert.ToString(row[colIndex], CultureInfo.InvariantCulture));
                            colIndex++;
                        }
                        dataRow++;
                    }
                }
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void ExportHeader(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                base.SetupDefaultData(dalFactory);

                // create our export process object
                Galleria.Ccm.Imports.Processes.ExportProductsProcess SExportProcess =
                    new Galleria.Ccm.Imports.Processes.ExportProductsProcess(1, _fileName, true);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ExportProductsProcess>(SExportProcess);

                // create the same mapping list as export
                IImportMappingList mappingList = ProductImportMappingList.NewProductImportMappingList(true);

                // Load exported file
                Workbook workbook = new Workbook(_fileName);
                Worksheet worksheet = workbook.Worksheets[0];
                DataTable dataTable = worksheet.Cells.ExportDataTable(0, 0, 1, mappingList.Count);

                // Validate exported data
                foreach (DataRow row in dataTable.Rows)
                {
                    Int32 colIndex = 0;
                    foreach (ImportMapping mappingItem in mappingList)
                    {
                        Assert.AreEqual(Convert.ToString(mappingItem.PropertyName), (Convert.ToString(row[colIndex], CultureInfo.InvariantCulture)));
                        colIndex++;
                    }
                }


            }
        }

        /// <summary>
        /// ensure we delete the local export file
        /// </summary>
        [TearDown]
        public void RemoveExportFile()
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }
        }

        #endregion
    }
}
