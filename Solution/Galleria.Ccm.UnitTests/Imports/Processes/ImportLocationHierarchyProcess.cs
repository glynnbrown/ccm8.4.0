﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using System.Data;
using NUnit.Framework;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ImportLocationHierarchyProcess : TestBase
    {
        #region Fields

        Int32 _numDtosToInsert = 200;
        private Int32 _locHierarchyId;

        #endregion

        #region Helper Methods

        /// <summary>
        /// Validates imported data and original datatable
        /// </summary>
        /// <param name="storeDtoList"></param>
        /// <param name="dataTable"></param>
        private void ValidateImportedData(ImportFileData fileData, IDalFactory dalFactory)
        {
            //Fetch hierarchy 
            LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(1);

            //Fetch all of the levels
            IEnumerable<LocationLevel> levels = hierarchy.EnumerateAllLevels();

            //Pull back all of the location group records within the database
            List<LocationGroupDto> locationGroups;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    locationGroups = dal.FetchByLocationHierarchyId(hierarchy.Id).ToList();
                }
                dalContext.Commit();
            }

            //Check the same number of category (level 3) groups have been inserted into the database as 
            //are in the import file data.
            IEnumerable<LocationGroupDto> groups = locationGroups.Where(p => p.LocationLevelId == (Int32)levels.Last().Id);
            Assert.AreEqual(groups.Count(), fileData.Rows.Count);

            foreach (ImportFileDataRow dataRow in fileData.Rows)
            {
                //Get values from row
                String categoryName = dataRow.Cells[dataRow.Cells.Count - 1].Value.ToString(); //last column
                String categoryCode = dataRow.Cells[dataRow.Cells.Count - 2].Value.ToString(); //2nd to last column

                //Find matching group
                LocationGroupDto matchingGroup = groups.FirstOrDefault(p => p.Code == categoryCode & p.Name == categoryName);

                Assert.IsNotNull(matchingGroup, "Location Group record should exist");
            }
        }


        /// <summary>
        /// Creates data for import 
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData(IDalFactory dalFactory)
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            IEnumerable<String> levelNames = LocationHierarchy.FetchByEntityId(1).EnumerateAllLevels()
                .Where(p => p.IsRoot == false).Select(p => p.Name).ToList();

            fileData.MappingList = LocationHierarchyImportMappingList.NewList(levelNames);

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < _numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    importRow.Cells.Add(
                        ImportFileDataCell.NewImportFileDataCell(cellColNumber, mappingItem.PropertyName + i)
                        );

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private EntityDto InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            Int32 entityId = entityDtos[0].Id;

            //location hierarchy 
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityId);
            List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);

            _locHierarchyId = locHierarchyDtos[0].Id;

            //only add the root group
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dal.Insert(new LocationGroupDto()
                    {
                        Code = "x",
                        LocationHierarchyId = locHierarchyDtos[0].Id,
                        LocationLevelId = level1Dtos[0].Id,
                        Name = "root",
                        DateCreated = DateTime.UtcNow,
                        DateLastModified = DateTime.UtcNow
                    });
                }
            }


            return entityDtos[0];
        }

        #endregion

        #region Import


        [Test, TestCaseSource("DalFactoryTypes")]
        public void Import(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //Create initial required data
                EntityDto entityDto = InsertDtos(dalFactory);

                Random random = new Random();

                //Create data table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create data
                fileData = CreateData(dalFactory);

                //Construct process
                Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess importProcess =
                                new Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess(1, fileData);

                // execute the import process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess>(importProcess);

                //Check imported data matches inserted.
                ValidateImportedData(fileData, dalFactory);

                #region Case 2 - [GEM21865] Existing code match is case insensitive

                //get the number of groups added.
                Int32 locationGroupCount;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        locationGroupCount = dal.FetchByLocationHierarchyId(_locHierarchyId).Count();
                    }
                }



                //+upper invariant
                fileData = CreateData(dalFactory);
                foreach (Int32 colNumber in new List<Int32> { 1, 3 })
                {
                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }
                }

                //import
                importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess(1, fileData));

                Int32 newGroupCount;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        newGroupCount = dal.FetchByLocationHierarchyId(_locHierarchyId).Count();
                    }
                }
                Assert.AreEqual(locationGroupCount, newGroupCount);

                #endregion

            }
        }

        #endregion

        #region Ticket Tests

        /// <summary>
        /// Scenario: A new group is to be added as a child of an existing one with locations.
        /// Expected: The assigned locations are moved to the new child.
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22339_AddingNewChildBeneathGroupWithLocations(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                //create a hierarchy with 2 levels and 1 group below the root
                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 2);
                List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 1);
                

                //add locations against the lower group
                List<LocationDto> locDtos = TestDataHelper.InsertLocationDtos(dalFactory, entityId, new List<LocationGroupDto> { locGroupDtos.Last() }, 5);

                //load the hierarchy and add a new level
                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);
                LocationLevel parentLevel = hierarchy.RootLevel.ChildLevel;
                LocationLevel newLevel = LocationLevel.NewLocationLevel();
                newLevel.Name = hierarchy.GetNextAvailableLevelName(parentLevel);
                parentLevel.ChildLevel = newLevel;
                hierarchy = hierarchy.Save();

                LocationGroup parentGroup = hierarchy.RootGroup.ChildList[0];

                String newGroupCode = "GEM22339";
                String newGroupName = "GEM22339_NewGroup";

                //create the file data to add a new group beneath the group with locations
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { parentGroup.Code, parentGroup.Name, newGroupCode, newGroupName });
                AddDataRows(fileData, data);

                //construct and execute process
                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess(entityId, fileData));

                //reload and check the hierarchy
                hierarchy = LocationHierarchy.FetchByEntityId(entityId);

                Assert.AreEqual(1, hierarchy.RootGroup.ChildList.Count);
                Assert.AreEqual(1, hierarchy.RootGroup.ChildList[0].ChildList.Count);

                LocationGroup addedGroup = hierarchy.RootGroup.ChildList[0].ChildList[0];
                Assert.AreEqual(newGroupCode, addedGroup.Code);
                Assert.AreEqual(newGroupName, addedGroup.Name);

                Assert.AreEqual(0, addedGroup.ParentGroup.AssignedLocationsCount);
                Assert.AreNotEqual(0, addedGroup.AssignedLocationsCount);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22339_CanImportRaggedHierarchy(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                //create the hierarchy
                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityId);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 1);
                List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 1);


                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

                //add a second and third level
                LocationLevel level2 = LocationLevel.NewLocationLevel();
                level2.Name = "level2";
                hierarchy.RootLevel.ChildLevel = level2;

                LocationLevel level3 = LocationLevel.NewLocationLevel();
                level3.Name = "level3";
                level2.ChildLevel = level3;

                hierarchy = hierarchy.Save();


                //create file data to import 1 group against level 2.
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));

                var data = new List<String[]>();
                data.Add(new String[] { "GEM22339", "GEM22339_NewGroup", String.Empty, String.Empty, String.Empty, String.Empty });
                AddDataRows(fileData, data);

                //check validation
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                //check there are only warnings
                Assert.AreEqual(0, validationProcess.Errors.Count(e => e.Type == ValidationErrorType.Error));

                //import
                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess(entityId, fileData));


                //refetch the hierarchy and check the new group was added.
                hierarchy = LocationHierarchy.FetchByEntityId(entityId);
                Assert.AreEqual(1, hierarchy.RootGroup.ChildList.Count);
                Assert.AreEqual("GEM22339", hierarchy.RootGroup.ChildList[0].Code);
                Assert.AreEqual(0, hierarchy.RootGroup.ChildList[0].ChildList.Count);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22916_MoveExistingToNewParent(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                //create a hierarchy with 2 levels and 1 group below the root
                List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityId);
                List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 1);
                List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 1);


                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

                //add some more levels
                LocationLevel level2 = LocationLevel.NewLocationLevel();
                level2.Name = "Department";
                hierarchy.RootLevel.ChildLevel = level2;

                LocationLevel level3 = LocationLevel.NewLocationLevel();
                level3.Name = "Section";
                level2.ChildLevel = level3;

                LocationLevel level4 = LocationLevel.NewLocationLevel();
                level4.Name = "Planogram Class";
                level3.ChildLevel = level4;

                LocationLevel level5 = LocationLevel.NewLocationLevel();
                level5.Name = "Planogram Class L1";
                level4.ChildLevel = level5;


                //add groups
                LocationGroup chilled = LocationGroup.NewLocationGroup(level2.Id);
                chilled.Name = "chilled";
                chilled.Code = "MD2";
                hierarchy.RootGroup.ChildList.Add(chilled);

                LocationGroup farmersboy = LocationGroup.NewLocationGroup(level3.Id);
                farmersboy.Name = "farmersboy";
                farmersboy.Code = "MS9";
                chilled.ChildList.Add(farmersboy);

                LocationGroup freshFoods = LocationGroup.NewLocationGroup(level4.Id);
                freshFoods.Name = "freshFoods";
                freshFoods.Code = "90001";
                farmersboy.ChildList.Add(freshFoods);

                LocationGroup bacon = LocationGroup.NewLocationGroup(level5.Id);
                bacon.Name = "bacon";
                bacon.Code = "90002";
                freshFoods.ChildList.Add(bacon);


                hierarchy = hierarchy.Save();


                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(LocationHierarchyImportMappingList.NewList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { "MD2", "chilled", "MS2", "farmersboy", "90001", "freshFoods", "90002", "bacon" });
                AddDataRows(fileData, data);


                //check validation - should fail.
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count(e => e.Type == ValidationErrorType.Error));

                //import
                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportLocationHierarchyProcess(entityId, fileData));

                //refetch the hierarchy and check
                hierarchy = LocationHierarchy.FetchByEntityId(entityId);

                List<LocationGroup> finalGroups = hierarchy.EnumerateAllGroups().ToList();

                Assert.AreEqual("MD2", finalGroups[1].Code);
                Assert.AreEqual("chilled", finalGroups[1].Name);
                Assert.AreEqual(2, finalGroups[1].ChildList.Count);

                //orig farmersboy group-
                Assert.AreEqual("MS9", finalGroups[1].ChildList[0].Code);
                Assert.AreEqual("farmersboy", finalGroups[1].ChildList[0].Name);

                Assert.AreEqual(0, finalGroups[1].ChildList[0].ChildList.Count);


                //new farmersboy group
                Assert.AreEqual("MS2", finalGroups[1].ChildList[1].Code);
                Assert.AreEqual("farmersboy", finalGroups[1].ChildList[1].Name);

                //child group has moved from old farmersboy group to the new one.
                Assert.AreEqual(1, finalGroups[1].ChildList[1].ChildList.Count);

            }
        }

        #endregion
    }
}
