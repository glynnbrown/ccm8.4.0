﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Processes;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public class ValidateLocationsProcess : TestBase
    {
        #region Fields
        private Int32 _locHierarchyId;
        List<LocationGroupDto> _locationGroups;
        Int32 _numDtosToInsert = 1500;
        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.MappingList = LocationImportMappingList.NewLocationImportMappingList();

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < _numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;

                    if (mappingItem.PropertyIdentifier == LocationImportMappingList.CodeMapId)
                    {
                        dataItem = "code" + i.ToString();
                    }
                    else if (mappingItem.PropertyIdentifier == LocationImportMappingList.NameMapId)
                    {
                        dataItem = "name" + i.ToString();
                    }
                    else if (mappingItem.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId)
                    {
                        dataItem = _locationGroups[rand.Next(1, _locationGroups.Count - 1)].Code;

                    }
                    else if (mappingItem.PropertyIdentifier == LocationImportMappingList.LatitudeMapId ||
                                mappingItem.PropertyIdentifier == LocationImportMappingList.LongitudeMapId)
                    {
                        dataItem = rand.Next(0, 90);
                    }
                    else
                    {
                        dataItem = base.GenerateRandomData(mappingItem);
                    }

                    importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem));

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private void InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);

            //location hierarchy 
            List<LocationHierarchyDto> locHierarchyDtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtos);
            List<LocationLevelDto> level1Dtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchyDtos[0].Id, 3);
            List<LocationGroupDto> locGroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, level1Dtos, 5);

            _locHierarchyId = locHierarchyDtos[0].Id;

            _locationGroups = locGroupDtos.Where(l => l.LocationLevelId == level1Dtos.Last().Id).ToList(); //leaf levels only.
        }

        #endregion

        #region Validate

        /// <summary>
        /// Validate location data
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Validate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);


                #region Case 1 - Valid data
                //Create data table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create data
                fileData = CreateData();

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case1ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case1ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(0, case1ValidateProcess.Errors.Count(), "No errors or warnings should have been found");
                Assert.IsNotNull(case1ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case1ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion

                #region Case 2 - Invalid Data Type

                #region Single Error
                //Create data
                fileData = CreateData();

                //Set invalid data type
                //Set location open date data to be a string value
                Int32 invalidRowNumber = 1;
                Int32 invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId));
                String invalidData = Guid.NewGuid().ToString();

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case21ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case21ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case21ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                ValidationErrorGroup errorGroup = case21ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectDataTypeHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                ValidationErrorItem errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case21ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case21ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region > 10% errors

                //Create data
                fileData = CreateData();

                //Set invalid data type for all rows
                //Set location open date data to be a string value
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.DateOpenMapId));
                invalidData = Guid.NewGuid().ToString();

                foreach (ImportFileDataRow row in fileData.Rows)
                {
                    row.Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;
                }

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case22ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case22ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case22ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case22ValidateProcess.Errors.First();
                Assert.AreEqual(Message.DataManagement_Validation_IncorrectDataTypeHeader, errorGroup.Description, "Error added to the wrong group");
                //Check column is marked as invalid
                Assert.IsTrue(errorGroup.InvalidColumnNumbers.Contains((invalidColumnNumber)));
                Assert.IsTrue(errorGroup.ContainsInvalidColumnErrorsWarnings, "Should be marked as having invalid columns");

                //Check that 10% of rows limit has been met to invalidate column
                Double maxColumnErrorCount = ((Double)fileData.Rows.Count * 0.10);
                Assert.GreaterOrEqual(errorGroup.Errors.Count, maxColumnErrorCount);

                #endregion

                #endregion

                #region Case 3 - Missing Data

                #region Test Non Mandatory property

                //Create data
                fileData = CreateData();

                //Set missing data
                //Set location size data to be null
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeNetSalesAreaMapId));
                object invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case3ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case3ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case3ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case3ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_MissingDataHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.SizeNetSalesAreaMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual("<empty>", errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Warning, errorItem.ErrorType, "Item should be a warning as column is not mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case3ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case3ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Test Mandatory property

                //Create data
                fileData = CreateData();

                //Set missing data
                //Set location size data to be null
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId));
                invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case31ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case31ValidateProcess);

                //Get error item
                errorItem = case31ValidateProcess.Errors.First().Errors.First();

                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual("<empty>", errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error as column is not mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                #endregion

                #endregion

                #region Case 4 - Invalid Data length

                #region Max Length
                //Create data
                fileData = CreateData();

                //Set invalid length data
                //Set location county data to be a string value
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId));
                invalidData = new string('*', 51);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case4ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case4ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case4ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case4ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_InvalidLengthHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_AboveMaxLengthErrorMessage, fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CountyMapId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case4ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case4ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Min Length

                //No string property with min length greater than 1 for Locations. If you set a string to have a length
                //of 0, it is caught by the missing data validation.

                #endregion

                #endregion

                #region Case 5 - Out of bounds data

                #region Upper Bound

                //Create data
                fileData = CreateData();

                //Set invalid bound data
                //Set location longitude data to be a number exceeding its upper bound of 180
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId));
                float invalidFloatData = 185.43F;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidFloatData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case5ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case5ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case5ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case5ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_OutOfBoundsHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_UpperBoundErrorMessage, fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId).PropertyMax), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidFloatData.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case5ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case5ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Lower Bound

                //Create data
                fileData = CreateData();

                //Set invalid bound data
                //Set location longitude data to be a number exceeding its upper bound of 180
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId));
                invalidFloatData = -185.43F;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidFloatData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case51ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case51ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case51ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case51ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_OutOfBoundsHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_LowerBoundErrorMessage, fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId).PropertyMin), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.LongitudeMapId).PropertyName, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidFloatData.ToString(), errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case51ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case51ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #endregion

                #region Case 6 - Parent Record

                // N/A for location validation

                #endregion

                #region Case 7 - Duplicate Record

                //Create data
                fileData = CreateData();

                //Choose a row to get original data from
                Int32 originalRowNumber = 4;
                invalidRowNumber = 5;
                invalidColumnNumber = fileData.GetMappedColNumber(fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId));

                //Set invalid duplicate data
                Object duplicateData = fileData.Rows.First(p => p.RowNumber == originalRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value;
                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = duplicateData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateLocationsProcess case7ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(case7ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case7ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case7ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual("Invalid identifying data", errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");
                Assert.AreEqual("Duplicate location code found.", errorGroup.Errors[0].ProblemDescription);
                Assert.AreEqual("Duplicate location code found.", errorGroup.Errors[1].ProblemDescription);

                //Check details of these 2 errors
                foreach (ValidationErrorItem error in errorGroup.Errors)
                {
                    //Check row numbers (compensate for zero based index and row row being headers -> +2)
                    Assert.IsTrue((error.RowNumber == (originalRowNumber) || error.RowNumber == (invalidRowNumber)) ? true : false, "Row number should be one of the 2 duplicate rows");

                    //Check column number
                    Assert.AreEqual((invalidColumnNumber), error.ColumnNumber, "Column number should match the invalid column's");

                    //Check data affected
                    Assert.AreEqual(duplicateData.ToString(), error.DataAffected, "Data affected should match duplicate data");

                    //Check duplicate error flag
                    Assert.IsTrue(error.IsDuplicateError, "Should be marked as duplicate error");
                }

                //Validate process properties
                Assert.IsNotNull(case7ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case7ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion
            }
        }

        #endregion

        #region Ticket

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM21865_ParentRecordCheckIsCaseInsensitive(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);
                Int32 entityId = 1;

                #region check valid data
                ImportFileData fileData = CreateData();

                var validationProcess =
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                #endregion

                List<Int32> checkColNumbers =
                    new List<Int32>()
                    {
                        //LocationGroup
                        fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId)),

                        //Location - done seperately below.
                    };

                //+ lower invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToLowerInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }

                //+upper invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                }


                #region Location

                //import the data
                fileData = CreateData();

                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportLocationsProcess>(
                        new Galleria.Ccm.Imports.Processes.ImportLocationsProcess(entityId, fileData, "newLocationGroupName"));



                Int32 locCodeColNum =
                fileData.GetMappedColNumber(
                        fileData.MappingList.First(p => p.PropertyIdentifier == LocationImportMappingList.CodeMapId));

                fileData = CreateData();

                //remove all rows except the first
                var firstRow = fileData.Rows[0];
                fileData.Rows.Clear();
                fileData.Rows.Add(firstRow);

                ImportFileDataCell locCodeCell = firstRow.Cells.First(p => p.ColumnNumber == locCodeColNum);
                locCodeCell.Value = locCodeCell.Value.ToString().ToLowerInvariant();

                validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                Assert.AreEqual("Update", validationProcess.ValidatedData.Rows[0].Cells[0].Value, "Row should be an update");

                //same again but as uppercase universe name
                fileData = CreateData();

                firstRow = fileData.Rows[0];
                fileData.Rows.Clear();
                fileData.Rows.Add(firstRow);

                locCodeCell = firstRow.Cells.First(p => p.ColumnNumber == locCodeColNum);
                locCodeCell.Value = locCodeCell.Value.ToString().ToUpperInvariant();

                validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateLocationsProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateLocationsProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                Assert.AreEqual("Update", validationProcess.ValidatedData.Rows[0].Cells[0].Value, "Row should be an update");

                #endregion


            }


        }


        #endregion
    }
}
