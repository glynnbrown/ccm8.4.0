﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Imports.Mappings;
using System.Data;
using NUnit.Framework;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources;
using Aspose.Cells;
using Galleria.Framework.Dal;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Imports;
using Galleria.Ccm.Model;
using Galleria.Framework.Processes;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Imports.Processes
{
    public sealed class ValidateMerchandisingHierarchyProcess : TestBase
    {
        #region Fields
        Int32 _numDtosToInsert = 200;
        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates data for import
        /// </summary>
        /// <returns></returns>
        private ImportFileData CreateData()
        {
            ImportFileData fileData = ImportFileData.NewImportFileData();

            IEnumerable<String> levelNames = ProductHierarchy.FetchByEntityId(1)
                .EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();

            fileData.MappingList = MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(levelNames);

            //Add data table columns
            Int32 colNumber = 1;
            foreach (ImportMapping mappingItem in fileData.MappingList)
            {
                //Create column
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                importColumn.Header = mappingItem.PropertyName;
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                fileData.Columns.Add(importColumn);

                //Assign mapping item's column reference
                mappingItem.ColumnReference = mappingItem.PropertyName;

                colNumber++;
            }

            Random rand = new Random(234);

            Int32 rowNumber = 1;
            for (int i = 0; i < _numDtosToInsert; i++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                Int32 cellColNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    Object dataItem = null;

                    dataItem = mappingItem.PropertyName + i;

                    ImportFileDataCell importCell =
                                ImportFileDataCell.NewImportFileDataCell(cellColNumber, dataItem);
                    importRow.Cells.Add(importCell);

                    cellColNumber++;
                }

                //cycle through adding rows
                fileData.Rows.Add(importRow);

                rowNumber++;
            }
            return fileData;
        }

        private EntityDto InsertDtos(IDalFactory dalFactory)
        {
            List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);
            Int32 hierarchyId = hierarchyDtos[0].Id;

            List<ProductLevelDto> productLevel2Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchyDtos[0].Id, 3);


            //only add the merch root group
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dal.Insert(new ProductGroupDto()
                    {
                        Code = "x",
                        ProductHierarchyId = hierarchyId,
                        ProductLevelId = productLevel2Dtos[0].Id,
                        Name = "root",
                        DateCreated = DateTime.UtcNow,
                        DateLastModified = DateTime.UtcNow
                    });
                }
            }


            return entityDtos[0];
        }

        #endregion

        #region Validate

        [Test, TestCaseSource("DalFactoryTypes")]
        public void Validate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                InsertDtos(dalFactory);

                #region Case 1 - Valid Data
                //Create Data Table
                ImportFileData fileData = ImportFileData.NewImportFileData();

                //Create Data
                fileData = CreateData();

                //Construct Process
                Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess case1ValidateProcess
                    = new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(case1ValidateProcess);

                //Validate process once completed
                Assert.AreEqual(0, case1ValidateProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(case1ValidateProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, case1ValidateProcess.ValidatedData.Rows.Count, "Output Row Count Must Match Input Row Count");

                #endregion

                #region Case 2 - Invalid data type

                //no properties can have an invalid data type

                #endregion

                #region Case 3 - Missing Data

                #region test non mandatory property

                //all mandatory fields

                #endregion

                #region Test Mandatory property

                //Create data
                fileData = CreateData();

                //Set missing data
                //Set merchandising hiearchy category name data to be null
                Int32 invalidRowNumber = 2;
                Int32 invalidColumnNumber = fileData.Columns.Count; //want last column
                Object invalidObjectData = null;

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidObjectData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess case31ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(case31ValidateProcess);

                //Get error item
                ValidationErrorItem errorItem = case31ValidateProcess.Errors.First().Errors.First();

                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.Columns.First(p => p.ColumnNumber == invalidColumnNumber).Header, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(String.Empty, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be a warning as column is not mandatory");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                #endregion

                #endregion

                #region Case 4 - Invalid Data Length

                #region Max Length

                //Create data
                fileData = CreateData();

                //Set invalid length data
                //Set category name data to be a string value that has too many characters
                invalidRowNumber = 2;
                invalidColumnNumber = fileData.Columns.Count; //want last column
                string invalidData = new string('*', 101);

                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = invalidData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess case4ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(case4ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case4ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                ValidationErrorGroup errorGroup = case4ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(Message.DataManagement_Validation_InvalidLengthHeader, errorGroup.Description, "Error added to the wrong group");
                Assert.AreEqual(1, errorGroup.Errors.Count, "Only 1 error should exist");

                //Get error item
                errorItem = errorGroup.Errors.First();
                //Check properties (factor in zero based index & first row being headers for row/col number)
                Assert.AreEqual((invalidRowNumber), errorItem.RowNumber, "Row number should match");
                Assert.AreEqual((invalidColumnNumber), errorItem.ColumnNumber, "Column number should match");
                Assert.AreEqual(fileData.Columns.First(p => p.ColumnNumber == invalidColumnNumber).Header, errorItem.ColumnName, "Column name should match");
                Assert.AreEqual(invalidData, errorItem.DataAffected, "Data affected should match");
                Assert.AreEqual(String.Format(Message.DataManagement_Validation_AboveMaxLengthErrorMessage, 100), errorItem.ProblemDescription, "Problem description should match");
                Assert.AreEqual(ValidationErrorType.Error, errorItem.ErrorType, "Item should be an error");
                //Check duplicate error flag
                Assert.IsFalse(errorItem.IsDuplicateError, "Should not be marked as duplicate error");

                //Validate process properties
                Assert.IsNotNull(case4ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case4ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");
                #endregion

                #region Min Length

                //No string property with min length greater than 1 for merchandising hierarchy. If you 
                //set a string to have a length of 0, it is caught by the missing data validation.

                #endregion

                #endregion

                #region Case 5 - Out of Bounds Data

                // N/A to this type, the only data validated is strings, which are caught in the 
                // invalid data length checks.

                #endregion

                #region Case 6 - Parent Record

                // N/A to this type, there are no parent record checks in this process.

                #endregion

                #region Case 7 - Duplicate Record

                //Create data
                fileData = CreateData();

                //Choose a row to get original data from
                Int32 originalRowNumber = 4;
                invalidRowNumber = 5;
                invalidColumnNumber = fileData.Columns.Count - 1; //want 2nd to last column

                //Set invalid duplicate data
                Object duplicateData = fileData.Rows.First(p => p.RowNumber == originalRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value;
                fileData.Rows.First(p => p.RowNumber == invalidRowNumber).Cells.First(p => p.ColumnNumber == invalidColumnNumber).Value = duplicateData;

                //Construct process
                Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess case7ValidateProcess =
                                new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(1, fileData, 1, true);

                // execute the validation process
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(case7ValidateProcess);

                //Validate the process once completed
                Assert.AreEqual(1, case7ValidateProcess.Errors.Count, "1 error group should have been added");

                //Get error group
                errorGroup = case7ValidateProcess.Errors.First();
                //Check properties
                Assert.AreEqual(2, errorGroup.Errors.Count, "Only 2 errors should exist");

                //Check details of these 2 errors
                foreach (ValidationErrorItem error in errorGroup.Errors)
                {
                    //Check row numbers (compensate for zero based index and row row being headers -> +2)
                    Assert.IsTrue((error.RowNumber == (originalRowNumber) || error.RowNumber == (invalidRowNumber)) ? true : false, "Row number should be one of the 2 duplicate rows");

                    //Check column number
                    Assert.AreEqual((invalidColumnNumber), error.ColumnNumber, "Column number should match the invalid column's");

                    //Check data affected
                    Assert.AreEqual(duplicateData.ToString(), error.DataAffected, "Data affected should match duplicate data");

                    //Check duplicate error flag
                    Assert.IsTrue(error.IsDuplicateError, "Should be marked as duplicate error");
                }

                //Validate process properties
                Assert.IsNotNull(case7ValidateProcess.ValidatedData, "Validated data table must be output");
                Assert.AreEqual(fileData.Rows.Count, case7ValidateProcess.ValidatedData.Rows.Count, "Output row count must match input row count");

                #endregion
            }
        }

        #endregion

        #region Ticket

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM21865_ExistingGroupCheckIsCaseInsensitive(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                ///intert some dtos into the data source
                EntityDto entityDto = InsertDtos(dalFactory);
                Int32 entityId = entityDto.Id;

                //import valid data to start
                ImportFileData fileData = CreateData();

                var validationProcess =
                ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");
                Assert.IsTrue(validationProcess.ValidatedData.Rows.All(r => r.Cells[0].Value.ToString() == "Add"));

                var importProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ImportMerchandisingHierarchyProcess(entityId, fileData));


                //get the number of groups added.
                Int32 productGroupCount = 0;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        productGroupCount = dal.FetchByProductHierarchyId(entityId).Count();
                    }
                }

                List<Int32> checkColNumbers = new List<Int32> { 1, 3 };

                //+ lower invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToLowerInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");

                    Assert.IsTrue(validationProcess.ValidatedData.Rows.All(r => r.Cells[0].Value.ToString() == "Update"));
                }

                //+upper invariant
                foreach (Int32 colNumber in checkColNumbers)
                {
                    fileData = CreateData();

                    foreach (ImportFileDataRow row in fileData.Rows)
                    {
                        ImportFileDataCell cell = row.Cells.First(p => p.ColumnNumber == colNumber);
                        cell.Value = cell.Value.ToString().ToUpperInvariant();
                    }

                    validationProcess =
                        ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                        new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                    Assert.AreEqual(0, validationProcess.Errors.Count, "No Errors Should Have Been Found");
                    Assert.IsNotNull(validationProcess.ValidatedData, "Validated Data Table Should Be Output");
                    Assert.AreEqual(fileData.Rows.Count, validationProcess.ValidatedData.Rows.Count, "Output Rows Should Equal Input");

                    Assert.IsTrue(validationProcess.ValidatedData.Rows.All(r => r.Cells[0].Value.ToString() == "Update"));
                }

            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22005_DuplicateCodes(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {

                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);
                Int32 hierarchyId = hierarchyDtos[0].Id;
                

                List<ProductLevelDto> productLevel2Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchyDtos[0].Id, 4);


                ProductHierarchyDto productHierarchyDto = hierarchyDtos[0];
                Int32 entityId = productHierarchyDto.EntityId;

                //merch hierarchy

                ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(productHierarchyDto.EntityId);

                ImportFileData fileData;

                #region Case 1 duplicates on same row different levels

                #region Create Test Data

                fileData = ImportFileData.NewImportFileData();
                fileData.MappingList = MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(l => l.Name));

                //Add data table columns
                Int32 colNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    //Create column
                    ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                    importColumn.Header = mappingItem.PropertyName;
                    importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                    fileData.Columns.Add(importColumn);

                    //Assign mapping item's column reference
                    mappingItem.ColumnReference = mappingItem.PropertyName;

                    colNumber++;
                }

                //create row:
                var data = new List<String[]>();
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "group1", "Adult Dry Dog", "group1", "Test1" });
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "Group2", "Adult Wet Dog", "Group2", "Test2" });
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "GROUP3", "Aircare", "GROUP3", "Test3" });
                //data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Baked Beans", "1", "Name1" });
                //data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Bleach", "C00000446", "Test4" });


                Int32 rowNumber = 1;
                for (int i = 0; i < data.Count; i++)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    String[] rowData = data[i];


                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, rowData[cellColNumber - 1]));
                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }

                #endregion

                //construct and execute process
                var validatationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreEqual(1, validatationProcess.Errors.Where(err => err.Type == ValidationErrorType.Error).Count(), "Should have errors");
                Assert.AreEqual(3, validatationProcess.Errors[0].Errors.Count(), "Should have errors");

                #endregion

                #region Case 2 duplicates on different rows different levels

                #region Create Test Data

                fileData = ImportFileData.NewImportFileData();
                fileData.MappingList = MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(l => l.Name));

                //Add data table columns
                colNumber = 1;
                foreach (ImportMapping mappingItem in fileData.MappingList)
                {
                    //Create column
                    ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                    importColumn.Header = mappingItem.PropertyName;
                    importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                    fileData.Columns.Add(importColumn);

                    //Assign mapping item's column reference
                    mappingItem.ColumnReference = mappingItem.PropertyName;

                    colNumber++;

                }


                //create rows:
                data = new List<String[]>();
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Baked Beans", "1", "Name1" });
                data.Add(new String[] { "D00000001", "Ambient (excl H&L)", "C00000446", "Bleach", "C00000446", "Test4" });


                rowNumber = 1;
                for (int i = 0; i < data.Count; i++)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);

                    String[] rowData = data[i];


                    Int32 cellColNumber = 1;
                    foreach (ImportMapping mappingItem in fileData.MappingList)
                    {
                        importRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(cellColNumber, rowData[cellColNumber - 1]));
                        cellColNumber++;
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }

                #endregion

                //construct and execute process
                validatationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreNotEqual(0, validatationProcess.Errors.Where(err => err.Type == ValidationErrorType.Error).Count(), "Should have errors");

                #endregion
            }
        }

        /// <summary>
        /// Scenario: A new group is to be added which has the same code as one marked deleted.
        /// Expected: Duplicate code validation error.
        /// </summary>
        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22339_DeletedGroupCodesCannotBeReintroduced(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);


                //create a hierarchy with 2 levels and 1 group below the root
                ProductHierarchyDto productHierarchyDto = hierarchyDtos[0];
                List<ProductLevelDto> level1Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDto.Id, 1);
                List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, level1Dtos, 1);

                ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(entityId);

                ProductLevel level2 = ProductLevel.NewProductLevel();
                level2.Name = "level2";
                hierarchy.RootLevel.ChildLevel = level2;

                ProductGroup newGroup = ProductGroup.NewProductGroup(level2.Id);
                newGroup.Code = "GEM22339";
                newGroup.Name = "GEM22339_Group";
                hierarchy.RootGroup.ChildList.Add(newGroup);
                hierarchy = hierarchy.Save();

                //assign something to the group so that it will only get marked deleted
                //Assortment assortment = Assortment.NewAssortment(entityId);
                //assortment.Name = "a1";
                //assortment.ProductGroupId = hierarchy.RootGroup.ChildList[0].Id;
                //assortment.Save();

                //delete that group.
                hierarchy.RootGroup.ChildList.Clear();
                hierarchy = hierarchy.Save();

                //create some data that tries to reimport the deleted code
                IEnumerable<String> levelNames = ProductHierarchy.FetchByEntityId(1)
                .EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { newGroup.Code, "GEM22339_GroupNew" });
                AddDataRows(fileData, data);

                //construct and execute process
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                //check
                Assert.AreEqual(1, validationProcess.Errors.Count, "1 error should have been found");
                Assert.AreEqual(1, validationProcess.Errors[0].Errors.Count, "1 error should have been found");
                Assert.AreEqual(1, validationProcess.Errors[0].Errors[0].RowNumber, "Error should be for row 1");

            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void GEM22916_DuplicateExistingOnLowerLevel(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                List<EntityDto> entityDtos = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId = entityDtos[0].Id;

                List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtos[0].Id);

                //create a hierarchy with 2 levels and 1 group below the root
                ProductHierarchyDto productHierarchyDto = hierarchyDtos[0];
                List<ProductLevelDto> level1Dtos = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDto.Id, 1);
                List<ProductGroupDto> groupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, level1Dtos, 1);

                ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(entityId);

                //add some more levels
                ProductLevel level2 = ProductLevel.NewProductLevel();
                level2.Name = "Department";
                hierarchy.RootLevel.ChildLevel = level2;

                ProductLevel level3 = ProductLevel.NewProductLevel();
                level3.Name = "Section";
                level2.ChildLevel = level3;

                ProductLevel level4 = ProductLevel.NewProductLevel();
                level4.Name = "Planogram Class";
                level3.ChildLevel = level4;

                ProductLevel level5 = ProductLevel.NewProductLevel();
                level5.Name = "Planogram Class L1";
                level4.ChildLevel = level5;


                //add groups
                ProductGroup ambient = ProductGroup.NewProductGroup(level2.Id);
                ambient.Name = "ambient";
                ambient.Code = "MD1";
                hierarchy.RootGroup.ChildList.Add(ambient);

                ProductGroup grocery = ProductGroup.NewProductGroup(level3.Id);
                grocery.Name = "grocery";
                grocery.Code = "MS1";
                ambient.ChildList.Add(grocery);

                ProductGroup cereals = ProductGroup.NewProductGroup(level4.Id);
                cereals.Name = "cereals";
                cereals.Code = "98764";
                grocery.ChildList.Add(cereals);

                ProductGroup crunchyNut = ProductGroup.NewProductGroup(level5.Id);
                crunchyNut.Name = "crunchyNut";
                crunchyNut.Code = "98765";
                cereals.ChildList.Add(crunchyNut);

                hierarchy = hierarchy.Save();


                //Create data file but give cereals the code for crunchy nut and crunchy nut a new code entirely.

                //create file data to import 1 group against level 2.
                IEnumerable<String> levelNames = hierarchy.EnumerateAllLevels().Where(p => p.IsRoot == false).Select(p => p.Name).ToList();
                ImportFileData fileData = CreateEmptyFileData(MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(levelNames));
                var data = new List<String[]>();
                data.Add(new String[] { "MD1", "ambient", "MS1", "grocery", "98765", "cereals", "88866", "crunchyNut" });
                AddDataRows(fileData, data);


                //check validation - should fail.
                var validationProcess =
                    ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess>(
                    new Galleria.Ccm.Imports.Processes.ValidateMerchandisingHierarchyProcess(entityId, fileData, 1, true));

                Assert.AreNotEqual(0, validationProcess.Errors.Count(e => e.Type == ValidationErrorType.Error));
            }


        }

        #endregion
    }
}
