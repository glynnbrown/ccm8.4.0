﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Imports.Mappings
{
    public class LocationImportMappingList : TestBase
    {
        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationImportMappingList.NewLocationImportMappingList();

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationImportMappingList()
        {
            Random random = new Random();

            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationImportMappingList.NewLocationImportMappingList();

            //Update values
            model[0].ColumnReference = Guid.NewGuid().ToString();

            //Take copy
            Galleria.Ccm.Imports.Mappings.LocationImportMappingList modelCopy = model.Copy();

            //Recall factory method
            model = Galleria.Ccm.Imports.Mappings.LocationImportMappingList.NewLocationImportMappingList();

            //Assert not equal
            Assert.AreNotEqual(model, modelCopy);
        }

        #endregion

        #region Methods

        [Test]
        public void GetValueByMappingId()
        {
            //Insert a location Dto
            IEnumerable<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(this.DalFactory, 10, 1);

            //Get inserted location dto
            LocationDto locationDto = locationDtoList.First();

            //Get location group dto list
            IEnumerable<LocationGroupDto> locationGroupDtoList;
            LocationHierarchyDto hierarchyDto;
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                dalContext.Begin();

                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    hierarchyDto = dal.FetchByEntityId(1);
                }

                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    locationGroupDtoList = dal.FetchByLocationHierarchyId(hierarchyDto.Id);
                }
            }


            //Check all values match correctly
            #region Location properties
            Assert.AreEqual(locationDto.Address1, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.Address1MapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Address2, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.Address2MapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.AdvertisingZone, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.AdvertisingZoneMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.AverageOpeningHours, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.AverageOpeningHoursMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.CarParkManagement, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CarParkManagementMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.CarParkSpaces, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CarParkSpacesMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.City, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CityMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Code, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CodeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Country, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CountryMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.County, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CountyMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.DateClosed, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DateClosedMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.DateLastRefitted, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DateLastRefittedMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.DateOpen, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DateOpenMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.DefaultClusterAttribute, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DefaultClusterAttributeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.DistributionCentre, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DistributionCentreMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.DivisionalManagerEmail, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DivisionalManagerEmailMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.DivisionalManagerName, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DivisionalManagerNameMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.FaxAreaCode, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.FaxAreaCodeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.FaxCountryCode, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.FaxCountryCodeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.FaxNumber, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.FaxNumberMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasAlcohol, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasAlcoholMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasAtmMachines, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasAtmMachinesMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasBabyChanging, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasBabyChangingMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasButcher, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasButcherMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasCafe, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasCafeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasCarServiceArea, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasCarServiceAreaMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasClinic, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasClinicMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasCustomerWC, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasCustomerWCMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasDeli, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasDeliMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasDryCleaning, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasDryCleaningMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasFashion, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasFashionMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasFishmonger, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasFishmongerMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasGardenCentre, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasGardenCentreMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasGrocery, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasGroceryMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasHomeShoppingAvailable, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasHomeShoppingAvailableMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasHotFoodToGo, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasHotFoodToGoMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasInStoreBakery, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasInStoreBakeryMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasJewellery, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasJewelleryMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasLottery, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasLotteryMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasMobilePhones, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasMobilePhonesMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasMovieRental, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasMovieRentalMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasNewsCube, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasNewsCubeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasOptician, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasOpticianMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasOrganic, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasOrganicMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasPetrolForecourt, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPetrolForecourtMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasPharmacy, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPharmacyMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasPhotocopier, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPhotocopierMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasPhotoDepartment, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPhotoDepartmentMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasPizza, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPizzaMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasPostOffice, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPostOfficeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasRecycling, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasRecyclingMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasRotisserie, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasRotisserieMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasSaladBar, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasSaladBarMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.HasTravel, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasTravelMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Is24Hours, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.Is24HoursMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsFreehold, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsFreeholdMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsMezzFitted, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsMezzFittedMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsOpenFriday, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenFridayMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsOpenMonday, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenMondayMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsOpenSaturday, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenSaturdayMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsOpenSunday, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenSundayMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsOpenThursday, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenThursdayMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsOpenTuesday, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenTuesdayMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.IsOpenWednesday, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenWednesdayMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Latitude, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.LatitudeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Location, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.LocationAttributeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Longitude, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.LongitudeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.ManagerEmail, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.ManagerEmailMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.ManagerName, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.ManagerNameMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Name, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.NameMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.NoOfCheckouts, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.NoOfCheckoutsMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.OpeningHours, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.OpeningHoursMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.PetrolForecourtType, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.PetrolForecourtTypeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.PostalCode, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.PostalCodeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Region, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RegionMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.RegionalManagerEmail, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RegionalManagerEmailMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.RegionalManagerName, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RegionalManagerNameMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.Restaurant, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RestaurantMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.SizeGrossFloorArea, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.SizeGrossFloorAreaMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.SizeMezzSalesArea, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.SizeMezzSalesAreaMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.SizeNetSalesArea, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.SizeNetSalesAreaMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.TelephoneAreaCode, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneAreaCodeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.TelephoneCountryCode, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneCountryCodeMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.TelephoneNumber, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneNumberMapId, locationDto, locationGroupDtoList));
            Assert.AreEqual(locationDto.TVRegion, Ccm.Imports.Mappings.LocationImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TVRegionMapId, locationDto, locationGroupDtoList));


            #endregion
        }

        [Test]
        public void SetValueByMappingId()
        {
            //Create a location Dto
            LocationDto locationDto = new LocationDto();

            Random random = new Random();

            //Check all values match correctly
            Object data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.Address1MapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Address1);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.Address2MapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Address2);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.AdvertisingZoneMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.AdvertisingZone);

            data = Convert.ToSingle(random.Next(1, 1000));
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.AverageOpeningHoursMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.AverageOpeningHours);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CarParkManagementMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.CarParkManagement);

            data = Convert.ToSingle(random.Next(1, 500));
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CarParkSpacesMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.CarParkSpaces);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CityMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.City);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CodeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Code);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CountryMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Country);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.CountyMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.County);

            data = DateTime.Today;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DateClosedMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.DateClosed);

            data = DateTime.Today;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DateLastRefittedMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.DateLastRefitted);

            data = DateTime.Today;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DateOpenMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.DateOpen);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DefaultClusterAttributeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.DefaultClusterAttribute);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DistributionCentreMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.DistributionCentre);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DivisionalManagerEmailMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.DivisionalManagerEmail);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.DivisionalManagerNameMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.DivisionalManagerName);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.FaxAreaCodeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.FaxAreaCode);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.FaxCountryCodeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.FaxCountryCode);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.FaxNumberMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.FaxNumber);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasAlcoholMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasAlcohol);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasAtmMachinesMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasAtmMachines);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasBabyChangingMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasBabyChanging);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasButcherMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasButcher);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasCafeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasCafe);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasCarServiceAreaMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasCarServiceArea);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasClinicMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasClinic);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasCustomerWCMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasCustomerWC);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasDeliMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasDeli);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasDryCleaningMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasDryCleaning);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasFashionMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasFashion);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasFishmongerMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasFishmonger);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasGardenCentreMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasGardenCentre);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasGroceryMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasGrocery);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasHomeShoppingAvailableMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasHomeShoppingAvailable);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasHotFoodToGoMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasHotFoodToGo);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasInStoreBakeryMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasInStoreBakery);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasJewelleryMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasJewellery);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasLotteryMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasLottery);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasMobilePhonesMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasMobilePhones);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasMovieRentalMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasMovieRental);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasNewsCubeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasNewsCube);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasOpticianMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasOptician);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasOrganicMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasOrganic);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPetrolForecourtMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasPetrolForecourt);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPharmacyMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasPharmacy);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPhotocopierMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasPhotocopier);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPhotoDepartmentMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasPhotoDepartment);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPizzaMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasPizza);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasPostOfficeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasPostOffice);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasRecyclingMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasRecycling);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasRotisserieMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasRotisserie);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasSaladBarMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasSaladBar);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.HasTravelMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.HasTravel);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.Is24HoursMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Is24Hours);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsFreeholdMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsFreehold);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsMezzFittedMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsMezzFitted);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenFridayMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsOpenFriday);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenMondayMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsOpenMonday);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenSaturdayMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsOpenSaturday);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenSundayMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsOpenSunday);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenThursdayMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsOpenThursday);

            data = false;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenTuesdayMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsOpenTuesday);

            data = true;
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenWednesdayMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.IsOpenWednesday);

            data = Convert.ToSingle(random.Next(1, 1000));
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.LatitudeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Latitude);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.LocationAttributeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Location);

            data = Convert.ToSingle(random.Next(1, 1000));
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.LongitudeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Longitude);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.ManagerEmailMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.ManagerEmail);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.ManagerNameMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.ManagerName);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.NameMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Name);

            data = random.Next(1, 255);
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.NoOfCheckoutsMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.NoOfCheckouts);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.OpeningHoursMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.OpeningHours);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.PetrolForecourtTypeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.PetrolForecourtType);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.PostalCodeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.PostalCode);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RegionMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Region);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RegionalManagerEmailMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.RegionalManagerEmail);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RegionalManagerNameMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.RegionalManagerName);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.RestaurantMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.Restaurant);

            data = random.Next(1, 1000);
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.SizeGrossFloorAreaMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.SizeGrossFloorArea);

            data = random.Next(1, 1000);
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.SizeMezzSalesAreaMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.SizeMezzSalesArea);

            data = random.Next(1, 1000);
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.SizeNetSalesAreaMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.SizeNetSalesArea);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneAreaCodeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.TelephoneAreaCode);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneCountryCodeMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.TelephoneCountryCode);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneNumberMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.TelephoneNumber);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.LocationImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.LocationImportMappingList.TVRegionMapId, data, locationDto);
            Assert.AreEqual(data, locationDto.TVRegion);
        }

        [Test]
        public void GetBindingPath()
        {
            Galleria.Ccm.Imports.Mappings.LocationImportMappingList model
                 = Galleria.Ccm.Imports.Mappings.LocationImportMappingList.NewLocationImportMappingList();

            Assert.AreEqual(Location.Address1Property.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.Address1MapId));
            Assert.AreEqual(Location.Address2Property.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.Address2MapId));
            Assert.AreEqual(Location.AdvertisingZoneProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.AdvertisingZoneMapId));
            Assert.AreEqual(Location.AverageOpeningHoursProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.AverageOpeningHoursMapId));
            Assert.AreEqual(Location.CarParkManagementProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.CarParkManagementMapId));
            Assert.AreEqual(Location.CarParkSpacesProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.CarParkSpacesMapId));
            Assert.AreEqual(Location.CityProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.CityMapId));
            Assert.AreEqual(Location.CodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.CodeMapId));
            Assert.AreEqual(Location.CountryProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.CountryMapId));
            Assert.AreEqual(Location.CountyProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.CountyMapId));
            Assert.AreEqual(Location.DateClosedProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.DateClosedMapId));
            Assert.AreEqual(Location.DateLastRefittedProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.DateLastRefittedMapId));
            Assert.AreEqual(Location.DateOpenProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.DateOpenMapId));
            Assert.AreEqual(Location.DefaultClusterAttributeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.DefaultClusterAttributeMapId));
            Assert.AreEqual(Location.DistributionCentreProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.DistributionCentreMapId));
            Assert.AreEqual(Location.DivisionalManagerEmailProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.DivisionalManagerEmailMapId));
            Assert.AreEqual(Location.DivisionalManagerNameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.DivisionalManagerNameMapId));
            Assert.AreEqual(Location.FaxAreaCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.FaxAreaCodeMapId));
            Assert.AreEqual(Location.FaxCountryCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.FaxCountryCodeMapId));
            Assert.AreEqual(Location.FaxNumberProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.FaxNumberMapId));
            Assert.AreEqual(Location.HasAlcoholProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasAlcoholMapId));
            Assert.AreEqual(Location.HasAtmMachinesProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasAtmMachinesMapId));
            Assert.AreEqual(Location.HasBabyChangingProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasBabyChangingMapId));
            Assert.AreEqual(Location.HasButcherProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasButcherMapId));
            Assert.AreEqual(Location.HasCafeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasCafeMapId));
            Assert.AreEqual(Location.HasCarServiceAreaProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasCarServiceAreaMapId));
            Assert.AreEqual(Location.HasClinicProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasClinicMapId));
            Assert.AreEqual(Location.HasCustomerWCProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasCustomerWCMapId));
            Assert.AreEqual(Location.HasDeliProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasDeliMapId));
            Assert.AreEqual(Location.HasDryCleaningProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasDryCleaningMapId));
            Assert.AreEqual(Location.HasFashionProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasFashionMapId));
            Assert.AreEqual(Location.HasFishmongerProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasFishmongerMapId));
            Assert.AreEqual(Location.HasGardenCentreProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasGardenCentreMapId));
            Assert.AreEqual(Location.HasGroceryProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasGroceryMapId));
            Assert.AreEqual(Location.HasHomeShoppingAvailableProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasHomeShoppingAvailableMapId));
            Assert.AreEqual(Location.HasHotFoodToGoProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasHotFoodToGoMapId));
            Assert.AreEqual(Location.HasInStoreBakeryProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasInStoreBakeryMapId));
            Assert.AreEqual(Location.HasJewelleryProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasJewelleryMapId));
            Assert.AreEqual(Location.HasLotteryProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasLotteryMapId));
            Assert.AreEqual(Location.HasMobilePhonesProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasMobilePhonesMapId));
            Assert.AreEqual(Location.HasMovieRentalProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasMovieRentalMapId));
            Assert.AreEqual(Location.HasNewsCubeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasNewsCubeMapId));
            Assert.AreEqual(Location.HasOpticianProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasOpticianMapId));
            Assert.AreEqual(Location.HasOrganicProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasOrganicMapId));
            Assert.AreEqual(Location.HasPetrolForecourtProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasPetrolForecourtMapId));
            Assert.AreEqual(Location.HasPharmacyProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasPharmacyMapId));
            Assert.AreEqual(Location.HasPhotocopierProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasPhotocopierMapId));
            Assert.AreEqual(Location.HasPhotoDepartmentProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasPhotoDepartmentMapId));
            Assert.AreEqual(Location.HasPizzaProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasPizzaMapId));
            Assert.AreEqual(Location.HasPostOfficeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasPostOfficeMapId));
            Assert.AreEqual(Location.HasRecyclingProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasRecyclingMapId));
            Assert.AreEqual(Location.HasRotisserieProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasRotisserieMapId));
            Assert.AreEqual(Location.HasSaladBarProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasSaladBarMapId));
            Assert.AreEqual(Location.HasTravelProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.HasTravelMapId));
            Assert.AreEqual(Location.Is24HoursProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.Is24HoursMapId));
            Assert.AreEqual(Location.IsFreeholdProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsFreeholdMapId));
            Assert.AreEqual(Location.IsMezzFittedProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsMezzFittedMapId));
            Assert.AreEqual(Location.IsOpenFridayProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenFridayMapId));
            Assert.AreEqual(Location.IsOpenMondayProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenMondayMapId));
            Assert.AreEqual(Location.IsOpenSaturdayProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenSaturdayMapId));
            Assert.AreEqual(Location.IsOpenSundayProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenSundayMapId));
            Assert.AreEqual(Location.IsOpenThursdayProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenThursdayMapId));
            Assert.AreEqual(Location.IsOpenTuesdayProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenTuesdayMapId));
            Assert.AreEqual(Location.IsOpenWednesdayProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.IsOpenWednesdayMapId));
            Assert.AreEqual(Location.LatitudeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.LatitudeMapId));
            Assert.AreEqual(Location.LocationAttributeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.LocationAttributeMapId));
            Assert.AreEqual(Location.LongitudeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.LongitudeMapId));
            Assert.AreEqual(Location.ManagerEmailProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.ManagerEmailMapId));
            Assert.AreEqual(Location.ManagerNameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.ManagerNameMapId));
            Assert.AreEqual(Location.NameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.NameMapId));
            Assert.AreEqual(Location.NoOfCheckoutsProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.NoOfCheckoutsMapId));
            Assert.AreEqual(Location.OpeningHoursProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.OpeningHoursMapId));
            Assert.AreEqual(Location.PetrolForecourtTypeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.PetrolForecourtTypeMapId));
            Assert.AreEqual(Location.PostalCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.PostalCodeMapId));
            Assert.AreEqual(Location.RegionProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.RegionMapId));
            Assert.AreEqual(Location.RegionalManagerEmailProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.RegionalManagerEmailMapId));
            Assert.AreEqual(Location.RegionalManagerNameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.RegionalManagerNameMapId));
            Assert.AreEqual(Location.RestaurantProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.RestaurantMapId));
            Assert.AreEqual(Location.SizeGrossFloorAreaProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.SizeGrossFloorAreaMapId));
            Assert.AreEqual(Location.SizeMezzSalesAreaProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.SizeMezzSalesAreaMapId));
            Assert.AreEqual(Location.SizeNetSalesAreaProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.SizeNetSalesAreaMapId));
            Assert.AreEqual(Location.TelephoneAreaCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneAreaCodeMapId));
            Assert.AreEqual(Location.TelephoneCountryCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneCountryCodeMapId));
            Assert.AreEqual(Location.TelephoneNumberProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.TelephoneNumberMapId));
            Assert.AreEqual(Location.TVRegionProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationImportMappingList.TVRegionMapId));
        }

        #endregion
    }
}
