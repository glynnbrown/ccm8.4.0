﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Copied from SA
#endregion
#endregion

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using NUnit.Framework;

using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal.Configuration;


namespace Galleria.Ccm.UnitTests.Imports.Mappings
{
    public abstract class TestBase
    {
        #region Fields
        private String _dalName; // the dal name
        private IDalFactory _dalFactory; // the dal factory to use for testing
        private IDalFactory _userDalFactory; // the dal factory to use for testing
        #endregion

        #region Serializable

        /// <summary>
        /// Tests that a dto is serializable
        /// </summary>
        protected static void Serialize(object obj)
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
            }
        }

        #endregion

        #region Properties
        /// <summary>
        /// The dal factory to use for testing
        /// </summary>
        protected IDalFactory DalFactory
        {
            get { return _dalFactory; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public void Setup()
        {
            //  Register the tasks.
            Galleria.Ccm.Engine.TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");

            // generate a new dal name
            _dalName = Guid.NewGuid().ToString();

            // create a new, in memory dal factory
            _dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();

            // register this factory within the dal container
            DalContainer.RegisterFactory(_dalName, _dalFactory);

            // set this factory to be the default of the container
            DalContainer.DalName = _dalName;

            _dalFactory.CreateDatabase();

            //Set global context entity id to be 1 for the tests;
            Csla.ApplicationContext.GlobalContext["EntityId"] = 1;

            #region User Dal
            String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName, "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (string file in Directory.GetFiles(templateDir))
                {
                    //if (!Path.GetFileName(file).StartsWith(templateName))
                    //{
                    try
                    {
                        System.IO.File.Delete(file);
                    }
                    catch
                    {
                        // No point in crying over spilt milk
                    }
                    //}
                }
            }


            String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
            var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
            userFactory.UnitTestFolder = templateDir;
            _userDalFactory = userFactory;
            DalContainer.RegisterFactory(Constants.UserDal, _userDalFactory);
            #endregion

            //authenticate
            bool authenticated = Galleria.Ccm.Security.DomainPrincipal.Authenticate();

            //Create new entity
            Entity entity = Entity.NewEntity();
            entity.Name = Guid.NewGuid().ToString().Substring(0, 5);
            entity = entity.Save();


            //Set global context entity id to be 1 for the tests;
            Csla.ApplicationContext.GlobalContext["EntityId"] = entity.Id;

            //Create default hierarchy
            PopulateProductHierarchy(entity.Id);
        }

        /// <summary>
        /// Called when a test is completed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            //make the user dal factory unlock and drop everything.
            if (_userDalFactory != null)
            {
                //Remove the User DalFactory
                DalContainer.RemoveFactory(_userDalFactory.DalFactoryConfig.Name);
                _userDalFactory.Dispose();
            }

            // remove the registered dal
            DalContainer.RemoveFactory(_dalName);
        }

        /// <summary>
        /// Populate a product hierarchy
        /// </summary>
        /// <param name="hierarchyId"></param>
        /// <returns></returns>
        private static ProductHierarchy PopulateProductHierarchy(Int32 hierarchyId)
        {
            ProductHierarchy hierarchy = ProductHierarchy.FetchById(hierarchyId);

            //add 2 levels
            ProductLevel level1 = hierarchy.RootLevel.ChildLevel;
            if (level1 == null)
            {
                level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
                level1.Name = "level1";
            }
            ProductLevel level2 = level1.ChildLevel;
            if (level2 == null)
            {
                level2 = hierarchy.InsertNewChildLevel(level1);
                level2.Name = "level2";
            }


            //add 3 level1 groups
            ProductGroup level1Group1 = ProductGroup.NewProductGroup(level1.Id);
            level1Group1.Name = "level1Group1";
            level1Group1.Code = "level1Group1Code";
            ProductGroup level1Group2 = ProductGroup.NewProductGroup(level1.Id);
            level1Group2.Name = "level1Group2";
            level1Group2.Code = "level1Group2Code";
            ProductGroup level1Group3 = ProductGroup.NewProductGroup(level1.Id);
            level1Group3.Name = "level1Group3";
            level1Group3.Code = "level1Group3Code";
            hierarchy.RootGroup.ChildList.Add(level1Group1);
            hierarchy.RootGroup.ChildList.Add(level1Group2);
            hierarchy.RootGroup.ChildList.Add(level1Group3);

            //add 3 level2 groups per level1 group
            foreach (ProductGroup level1Group in hierarchy.RootGroup.ChildList)
            {
                for (int i = 1; i <= 3; i++)
                {
                    ProductGroup level2Group = ProductGroup.NewProductGroup(level2.Id);
                    level2Group.Name = level1Group.Name + i.ToString();
                    level2Group.Code = level1Group.Name + i.ToString();
                    level1Group.ChildList.Add(level2Group);
                }
            }

            return hierarchy.Save();
        }

        #endregion
    }
}
