﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Imports.Mappings
{
    public class MerchHierarchyImportMappingList : TestBase
    {
        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Fetch product hieararchy
            ProductHierarchy hiearchy = ProductHierarchy.FetchByEntityId(1);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewMerchHierarchyImportMappingList()
        {
            Random random = new Random();

            //Fetch product hieararchy
            ProductHierarchy hiearchy = ProductHierarchy.FetchByEntityId(1);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            //Update values
            model[0].ColumnReference = Guid.NewGuid().ToString();

            //Take copy
            Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList modelCopy = model.Copy();

            //Recall factory method
            model = Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            //Assert not equal
            Assert.AreNotEqual(model, modelCopy);
        }

        #endregion

        #region Methods

        [Test]
        public void GetValueByMappingId()
        {
            //Test presumes that there are 3 levels, root(level 1)/level 2/level 3

            //Fetch product hieararchy
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(1);

            //get the hierarchy groups to be exported
            List<ProductGroupDto> groupList;
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    groupList = dal.FetchByProductHierarchyId(hierarchy.Id).ToList();
                }
            }
            ProductGroupDto rootDto = groupList.FirstOrDefault(g => g.ParentGroupId == null);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(hierarchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            //Select a dto at the lowest level
            ProductGroupDto dto = groupList.First(p => Object.Equals(p.ProductLevelId, 3));

            //Find its parent
            ProductGroupDto parentDto = groupList.First(p => p.Id == dto.ParentGroupId);

            //Check all values match correctly
            Assert.AreEqual(parentDto.Code, model.GetValueByMappingId(1, dto, groupList, rootDto.Id)); //1st level item group = 1st column should be code
            Assert.AreEqual(parentDto.Name, model.GetValueByMappingId(2, dto, groupList, rootDto.Id)); //1st level item group = 2nd column should be name

            Assert.AreEqual(dto.Code, model.GetValueByMappingId(3, dto, groupList, rootDto.Id)); //2nd level item group = 3rd column should be code
            Assert.AreEqual(dto.Name, model.GetValueByMappingId(4, dto, groupList, rootDto.Id)); //2nd level item group = 4th column should be name
        }

        [Test]
        public void SetValueByMappingId()
        {
            //Fetch product hieararchy
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(1);

            //TestDataHelper.PopulateProductHierarchy(hierarchy.Id);

            //get the hierarchy groups to be exported
            List<ProductGroupDto> groupList;
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    groupList = dal.FetchByProductHierarchyId(hierarchy.Id).ToList();
                }
            }
            ProductGroupDto rootDto = groupList.FirstOrDefault(g => g.ParentGroupId == null);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(hierarchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));


            //Call all values set correctly
            Object data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(1, 1, data, groupList[1]);//1st level dto + code mapping (id = 1)
            Assert.AreEqual(data, groupList[1].Code);

            data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(2, 1, data, groupList[1]);//1st level dto + name mapping (id = 2)
            Assert.AreEqual(data, groupList[1].Name);

            data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(3, 1, data, groupList[3]);//2nd level dto + code mapping (id = 3)
            Assert.AreEqual(data, groupList[3].Code);

            data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(4, 1, data, groupList[3]);//2nd level dto + code mapping (id = 4)
            Assert.AreEqual(data, groupList[3].Name);

        }

        #endregion
    }
}
