﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25554 : L.Ineson
//		Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Imports.Mappings
{
    public class LocationHierarchyImportMappingList : TestBase
    {
        #region Test Fixture Helpers

        /// <summary>
        /// Creates a fully populated location hierarchy with levels,
        /// groups and locations
        /// </summary>
        /// <param name="dalFactory"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        private static LocationHierarchy PopulateLocationHierarchy(IDalFactory dalFactory, Int32 entityId)
        {
            LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);

            //add 2 levels
            LocationLevel level1 = hierarchy.RootLevel.ChildLevel;
            //LocationLevel level1 = hierarchy.InsertNewChildLevel(hierarchy.RootLevel);
            //level1.Name = "level1";
            LocationLevel level2 = level1.ChildLevel;
            //LocationLevel level2 = hierarchy.InsertNewChildLevel(level1);
            //level2.Name = "level2";

            //add 3 level1 groups
            LocationGroup level1Group1 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group1.Name = "level1Group1";
            level1Group1.Code = "level1Group1Code";
            LocationGroup level1Group2 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group2.Name = "level1Group2";
            level1Group2.Code = "level1Group2Code";
            LocationGroup level1Group3 = LocationGroup.NewLocationGroup(level1.Id);
            level1Group3.Name = "level1Group3";
            level1Group3.Code = "level1Group3Code";
            hierarchy.RootGroup.ChildList.Add(level1Group1);
            hierarchy.RootGroup.ChildList.Add(level1Group2);
            hierarchy.RootGroup.ChildList.Add(level1Group3);

            //add 3 level2 groups per level1 group
            foreach (LocationGroup level1Group in hierarchy.RootGroup.ChildList)
            {
                for (int i = 1; i <= 3; i++)
                {
                    LocationGroup level2Group = LocationGroup.NewLocationGroup(level2.Id);
                    level2Group.Name = "level2group" + i.ToString();
                    level2Group.Code = Guid.NewGuid().ToString();
                    level1Group.ChildList.Add(level2Group);
                }
            }

            hierarchy = hierarchy.Save();

            //insert locations against the hierarchy
            //IEnumerable<LocationGroup> leafGroups = hierarchy.FetchAllGroups().Where(g => g.ChildList.Count == 0);
            //IEnumerable<LocationTypeDto> locTypes = TestDataHelper.InsertLocationTypeDtos(dalFactory, 5, entityId);

            //TestDataHelper.InsertLocationDtos(dalFactory, entityId,
            //    leafGroups.Select(g => g.Id),
            //    locTypes.Select(t => t.Id), 20);


            return hierarchy;
        }

        #endregion

        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Fetch location hieararchy
            LocationHierarchy hiearchy = LocationHierarchy.FetchByEntityId(1);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList.NewList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationHierarchyImportMappingList()
        {
            Random random = new Random();

            //Fetch location hieararchy
            LocationHierarchy hiearchy = LocationHierarchy.FetchByEntityId(1);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList.NewList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            //Update values
            model[0].ColumnReference = Guid.NewGuid().ToString();

            //Take copy
            Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList modelCopy = model.Copy();

            //Recall factory method
            model = Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList.NewList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            //Assert not equal
            Assert.AreNotEqual(model, modelCopy);
        }

        #endregion

        #region Methods

        [Test]
        public void GetValueByMappingId()
        {
            //Fetch location hieararchy
            LocationHierarchy hiearchy = LocationHierarchy.FetchByEntityId(1);

            PopulateLocationHierarchy(this.DalFactory, 1);

            //get the hierarchy groups to be exported
            List<LocationGroupDto> groupList;
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    groupList = dal.FetchByLocationHierarchyId(hiearchy.Id).ToList();
                }
            }
            LocationGroupDto rootDto = groupList.FirstOrDefault(g => g.ParentGroupId == null);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList.NewList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));


            //Check all values match correctly
            Assert.AreEqual(groupList[1].Code, model.GetValueByMappingId(1, groupList[1], groupList, rootDto.Id)); //1st level item group = 1st column should be code
            Assert.AreEqual(groupList[1].Name, model.GetValueByMappingId(2, groupList[1], groupList, rootDto.Id)); //1st level item group = 2nd column should be name

            Assert.AreEqual(groupList[3].Code, model.GetValueByMappingId(3, groupList[3], groupList, rootDto.Id)); //2nd level item group = 3rd column should be code
            Assert.AreEqual(groupList[3].Name, model.GetValueByMappingId(4, groupList[3], groupList, rootDto.Id)); //2nd level item group = 4th column should be name
        }

        [Test]
        public void SetValueByMappingId()
        {
            //Fetch location hieararchy
            LocationHierarchy hiearchy = LocationHierarchy.FetchByEntityId(1);

            PopulateLocationHierarchy(this.DalFactory, 1);

            //get the hierarchy groups to be exported
            List<LocationGroupDto> groupList;
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    groupList = dal.FetchByLocationHierarchyId(hiearchy.Id).ToList();
                }
            }
            LocationGroupDto rootDto = groupList.FirstOrDefault(g => g.ParentGroupId == null);

            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationHierarchyImportMappingList.NewList(hiearchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name));

            //Call all values set correctly
            Object data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(1, 1, data, groupList[1]);//1st level dto + code mapping (id = 1)
            Assert.AreEqual(data, groupList[1].Code);

            data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(2, 1, data, groupList[1]);//1st level dto + name mapping (id = 2)
            Assert.AreEqual(data, groupList[1].Name);

            data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(3, 1, data, groupList[3]);//2nd level dto + code mapping (id = 3)
            Assert.AreEqual(data, groupList[3].Code);

            data = Guid.NewGuid().ToString();
            model.SetValueByMappingId(4, 1, data, groupList[3]);//2nd level dto + code mapping (id = 4)
            Assert.AreEqual(data, groupList[3].Name);

        }

        #endregion
    }
}
