﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.UnitTests.Imports.Mappings
{
    public class LocationClusterImportMappingList : TestBase
    {
        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewLocationClusterImportMappingList()
        {
            Random random = new Random();

            //Call factory method
            Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            //Update values
            model[0].ColumnReference = Guid.NewGuid().ToString();

            //Take copy
            Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList modelCopy = model.Copy();

            //Recall factory method
            model = Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            //Assert not equal
            Assert.AreNotEqual(model, modelCopy);
        }

        #endregion

        #region Methods

        [Test]
        public void GetValueByMappingId()
        {
            //Create a cluster search criteria Dto
            ClusterLocationSearchCriteriaDto clusterCriteriaDto = new ClusterLocationSearchCriteriaDto()
            {
                ClusterSchemeName = Guid.NewGuid().ToString(),
                ClusterName = Guid.NewGuid().ToString(),
                LocationCode = Guid.NewGuid().ToString()
            };

            //Check all values match correctly
            Assert.AreEqual(clusterCriteriaDto.ClusterSchemeName, Ccm.Imports.Mappings.LocationClusterImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationClusterImportMappingList.SchemeNameMappingId, clusterCriteriaDto));
            Assert.AreEqual(clusterCriteriaDto.ClusterName, Ccm.Imports.Mappings.LocationClusterImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationClusterImportMappingList.ClusterNameMappingId, clusterCriteriaDto));
            Assert.AreEqual(clusterCriteriaDto.LocationCode, Ccm.Imports.Mappings.LocationClusterImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.LocationClusterImportMappingList.LocationCodeMappingId, clusterCriteriaDto));
        }

        [Test]
        public void GetBindingPath()
        {
            Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList model
                = Galleria.Ccm.Imports.Mappings.LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            Assert.AreEqual(ClusterScheme.NameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationClusterImportMappingList.SchemeNameMappingId));
            Assert.AreEqual(Cluster.NameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationClusterImportMappingList.ClusterNameMappingId));
            Assert.AreEqual(Location.CodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.LocationClusterImportMappingList.LocationCodeMappingId));
        }

        #endregion
    }
}
