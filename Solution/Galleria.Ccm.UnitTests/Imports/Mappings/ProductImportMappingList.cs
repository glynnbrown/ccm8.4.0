﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
// V8-26041 : A.Kuszyk
//  Changed CCM ProductOrientationType to Framework PlanogramProductOrientationType.
//  Changed CCM ProductStatusType to Framework PlanogramProductStatusType.
//  Updated ProductDto.IsNew references to IsNewProduct.
// V8-27424 : L.Luong
//  Added FinancialGroupCode and FinancialGroupName
// V8-26333 : A.Probyn
//  Extended to be include with mapping list changes
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote 
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Imports.Mappings
{
    public class ProductImportMappingList : TestBase
    {
        #region Serializable

        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            //Call factory method
            Galleria.Ccm.Imports.Mappings.ProductImportMappingList model
                = Galleria.Ccm.Imports.Mappings.ProductImportMappingList.NewProductImportMappingList(true);

            Serialize(model);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewProductImportMappingList()
        {

            //Call factory method
            Galleria.Ccm.Imports.Mappings.ProductImportMappingList model
                 = Galleria.Ccm.Imports.Mappings.ProductImportMappingList.NewProductImportMappingList(true);

            //Update values
            model[0].ColumnReference = Guid.NewGuid().ToString();

            //Take copy
            Galleria.Ccm.Imports.Mappings.ProductImportMappingList modelCopy = model.Copy();

            //Recall factory method
            model = Galleria.Ccm.Imports.Mappings.ProductImportMappingList.NewProductImportMappingList(true);

            //Assert not equal
            Assert.AreNotEqual(model, modelCopy);
        }

        #endregion

        #region Methods

        [Test]
        public void GetValueByMappingId()
        {
            //Insert a product Dto
            List<ProductDto> productDtoList = TestDataHelper.InsertProductDtos(this.DalFactory, 1, 1);

            //Get inserted product dto
            ProductDto productDto = productDtoList.First();

            //Get linked attribute data dto & product group dto list
            IEnumerable<ProductGroupDto> productGroupDtoList;
            ProductHierarchyDto hierarchyDto;
            using (IDalContext dalContext = this.DalFactory.CreateContext())
            {
                dalContext.Begin();

                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyDto = dal.FetchByEntityId(1);
                }

                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroupDtoList = dal.FetchByProductHierarchyId(hierarchyDto.Id);
                }
            }

            //Check all values match correctly
            #region Product properties
            Assert.AreEqual(productDto.AlternateDepth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.AlternateDepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.AlternateHeight, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.AlternateHeightMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.AlternateWidth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.AlternateWidthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CanBreakTrayBack, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayBackMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CanBreakTrayDown, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayDownMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CanBreakTrayTop, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayTopMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CanBreakTrayUp, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayUpMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CasePackUnits, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CasePackUnitsMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Depth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DisplayDepth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DisplayDepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DisplayHeight, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DisplayHeightMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DisplayWidth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DisplayWidthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.FingerSpaceAbove, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FingerSpaceAboveMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.FingerSpaceToTheSide, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FingerSpaceToTheSideMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.ForceBottomCap, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ForceBottomCapMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.ForceMiddleCap, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ForceMiddleCapMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.FrontOverhang, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FrontOverhangMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Height, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.HeightMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.IsFrontOnly, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.IsFrontOnlyMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.IsPlaceHolderProduct, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.IsPlaceHolderProductMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
          
            Assert.AreEqual(productDto.MaxDeep, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxDeepMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.MaxRightCap, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxRightCapMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.MaxStack, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxStackMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.MaxTopCap, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxTopCapMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            //Assert.AreEqual(ProductMerchandisingStyleHelper.FriendlyNames[(ProductMerchandisingStyle)productDto.MerchandisingStyle], Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MerchandisingStyleMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.MinDeep, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MinDeepMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Name, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NameMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.NestingDepth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NestingDepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.NestingHeight, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NestingHeightMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.NestingWidth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NestingWidthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.NumberOfPegHoles, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NumberOfPegHolesMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(PlanogramProductOrientationTypeHelper.FriendlyNames[(PlanogramProductOrientationType)productDto.OrientationType], Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.OrientationTypeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegDepth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegDepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegProngOffsetX, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegProngOffsetXMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegX, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegXMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegX2, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegX2MapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegX3, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegX3MapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegY, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegYMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegY2, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegY2MapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PegY3, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegY3MapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PointOfPurchaseDepth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseDepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PointOfPurchaseHeight, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseHeightMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PointOfPurchaseWidth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseWidthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.SqueezeDepth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeDepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.SqueezeHeight, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeHeightMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.SqueezeWidth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeWidthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(PlanogramProductStatusTypeHelper.FriendlyNames[(PlanogramProductStatusType)productDto.StatusType], Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.StatusTypeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.TrayDeep, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayDeepMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.TrayHigh, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayHighMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.TrayWide, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayWideMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Width, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.WidthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.TrayThickDepth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickDepthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.TrayThickHeight, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickHeightMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.TrayThickWidth, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickWidthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));

            #endregion

            #region Product Attribute Properties
            Assert.AreEqual(productDto.Barcode, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.BarCodeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Brand, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.BrandMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CaseCost, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CaseCostMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Colour, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ColourMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.ConsumerInformation, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ConsumerInformationMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CorporateCode, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CorporateCodeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CostPrice, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CostPriceMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CountryOfOrigin, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CountryOfOriginMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.CountryOfProcessing, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CountryOfProcessingMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DateDiscontinued, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DateDiscontinuedMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DateEffective, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DateEffectiveMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DateIntroduced, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DateIntroducedMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DeliveryFrequencyDays, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DeliveryFrequencyDaysMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.DeliveryMethod, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DeliveryMethodMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.FinancialGroupCode, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FinancialGroupCodeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.FinancialGroupName, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FinancialGroupNameMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Flavour, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FlavourMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.GarmentType, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.GarmentTypeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Health, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.HealthMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Manufacturer, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.ManufacturerCode, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerCodeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.ManufacturerRecommendedRetailPrice, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Model, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ModelMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PackagingShape, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PackagingShapeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PackagingType, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PackagingTypeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Pattern, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PatternMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.PointOfPurchaseDescription, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseDescriptionMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.RecommendedRetailPrice, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.RecommendedRetailPriceMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.SellPackCount, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SellPackCountMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.SellPackDescription, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SellPackDescriptionMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.SellPrice, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SellPriceMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.ShelfLife, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ShelfLifeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.ShortDescription, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ShortDescriptionMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Size, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SizeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.StyleNumber, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.StyleNumberMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Subcategory, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SubCategoryMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.TaxRate, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TaxRateMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Texture, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TextureMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.UnitOfMeasure, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.UnitOfMeasureMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.Vendor, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.VendorMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            Assert.AreEqual(productDto.VendorCode, Ccm.Imports.Mappings.ProductImportMappingList.GetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.VendorCodeMapId, productDto, productGroupDtoList, new CustomAttributeDataDto()));
            #endregion
        }

        [Test]
        public void SetValueByMappingId()
        {
            //Create a product Dto
            ProductDto productDto = new ProductDto();
            CustomAttributeDataDto customAttributeDataDto = new CustomAttributeDataDto();

            //Check all values match correctly
            #region Product Properties
            Object data = 1.1f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.AlternateDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.AlternateDepth);

            data = 2.2f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.AlternateHeightMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.AlternateHeight);

            data = 3.3f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.AlternateWidthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.AlternateWidth);

            data = true;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayBackMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CanBreakTrayBack);

            data = false;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayDownMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CanBreakTrayDown);

            data = true;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayTopMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CanBreakTrayTop);

            data = false;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayUpMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CanBreakTrayUp);

            data = (Int16)1;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CasePackUnitsMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CasePackUnits);

            data = 5.5f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Depth);

            data = 6.6f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DisplayDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DisplayDepth);

            data = 7.7f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DisplayHeightMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DisplayHeight);

            data = 8.8f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DisplayWidthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DisplayWidth);

            data = 9.9f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FingerSpaceAboveMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.FingerSpaceAbove);

            data = 10.1f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FingerSpaceToTheSideMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.FingerSpaceToTheSide);

            data = false;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ForceBottomCapMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.ForceBottomCap);

            data = true;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ForceMiddleCapMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.ForceMiddleCap);

            data = 11.11f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FrontOverhangMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.FrontOverhang);

            data = 12.12f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.HeightMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Height);

            data = true;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.IsActiveMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.IsActive);

            data = false;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.IsFrontOnlyMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.IsFrontOnly);

            data = false;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.IsPlaceHolderProductMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.IsPlaceHolderProduct);

         
            data = Convert.ToByte(7);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxDeepMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.MaxDeep);

            data = Convert.ToByte(10);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxRightCapMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.MaxRightCap);

            data = Convert.ToByte(11);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxStackMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.MaxStack);

            data = Convert.ToByte(12);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MaxTopCapMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.MaxTopCap);

            ProductMerchandisingStyle merchStyle = (ProductMerchandisingStyle)(Byte)0;
            String merchStyleName = ProductMerchandisingStyleHelper.FriendlyNames[merchStyle];
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MerchandisingStyleMapId, merchStyleName, productDto, customAttributeDataDto);
            Assert.AreEqual((Byte)merchStyle, productDto.MerchandisingStyle);

            data = Convert.ToByte(13);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.MinDeepMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.MinDeep);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NameMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Name);

            data = 13.13f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NestingDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.NestingDepth);

            data = 14.14f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NestingHeightMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.NestingHeight);

            data = 15.15f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NestingWidthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.NestingWidth);

            data = Convert.ToByte(14);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.NumberOfPegHolesMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.NumberOfPegHoles);

            //PlanogramProductOrientationType orientationType = (PlanogramProductOrientationType)(Byte)1;
            //String orientationTypeName = PlanogramProductOrientationTypeHelper.FriendlyNames[orientationType];
            data = Convert.ToByte(0);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.OrientationTypeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.OrientationType);

            data = 15.15f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegDepth);

            data = 16.16f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegProngOffsetXMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegProngOffsetX);

            data = 17.17f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegXMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegX);

            data = 18.18f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegX2MapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegX2);

            data = 19.19f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegX3MapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegX3);

            data = 20.2f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegYMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegY);

            data = 21.21f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegY2MapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegY2);

            data = 22.22f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PegY3MapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PegY3);

            data = 23.23f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PointOfPurchaseDepth);

            data = 24.24f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseHeightMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PointOfPurchaseHeight);

            data = 25.25f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseWidthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PointOfPurchaseWidth);

            data = 26.26f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PointOfPurchaseDepth);

            data = 27.27f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.SqueezeDepth);

            data = 28.28f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeHeightMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.SqueezeHeight);

            data = 29.29f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeWidthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.SqueezeWidth);

            //PlanogramProductStatusType statusType = (PlanogramProductStatusType)(Byte)2;
            //String statusTypeName = PlanogramProductStatusTypeHelper.FriendlyNames[statusType];
            data = Convert.ToByte(0);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.StatusTypeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.StatusType);

            data = Convert.ToByte(15);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayDeepMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.TrayDeep);

            data = Convert.ToByte(16);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayHighMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.TrayHigh);

            data = Convert.ToByte(17);
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayWideMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.TrayWide);

            data = 30.3f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickDepthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.TrayThickDepth);

            data = 31.31f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickHeightMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.TrayThickHeight);

            data = 32.32f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickWidthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.TrayThickWidth);

            #endregion

            #region Product Attribute Data

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.BarCodeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Barcode);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.BrandMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Brand);

            data = 33.33f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CaseCostMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CaseCost);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ColourMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Colour);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ConsumerInformationMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.ConsumerInformation);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CorporateCodeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CorporateCode);

            data = 34.34f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CostPriceMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CostPrice);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CountryOfOriginMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CountryOfOrigin);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.CountryOfProcessingMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.CountryOfProcessing);

            data = DateTime.Today;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DateDiscontinuedMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DateDiscontinued);

            data = DateTime.Today;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DateEffectiveMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DateEffective);

            data = DateTime.Today;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DateIntroducedMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DateIntroduced);

            data = (Int16)2;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DeliveryFrequencyDaysMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DeliveryFrequencyDays);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.DeliveryMethodMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.DeliveryMethod);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FinancialGroupCodeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.FinancialGroupCode);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FinancialGroupNameMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.FinancialGroupName);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.FlavourMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Flavour);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.GarmentTypeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.GarmentType);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.HealthMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Health);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Manufacturer);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerCodeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.ManufacturerCode);

            data = 36.36f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.ManufacturerRecommendedRetailPrice);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ModelMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Model);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PackagingShapeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PackagingShape);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PackagingTypeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PackagingType);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PatternMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Pattern);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseDescriptionMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.PointOfPurchaseDescription);

            data = 37.37f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.RecommendedRetailPriceMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.RecommendedRetailPrice);

            data = (Int16)3;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SellPackCountMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.SellPackCount);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SellPackDescriptionMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.SellPackDescription);

            data = 39.39f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SellPriceMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.SellPrice);

            data = (Int16)4;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ShelfLifeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.ShelfLife);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.ShortDescriptionMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.ShortDescription);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SizeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Size);

            data = (Int16)5;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.StyleNumberMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.StyleNumber);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.SubCategoryMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Subcategory);

            data = 42.42f;
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TaxRateMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.TaxRate);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.TextureMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Texture);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.UnitOfMeasureMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.UnitOfMeasure);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.VendorMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.Vendor);

            data = Guid.NewGuid().ToString();
            Ccm.Imports.Mappings.ProductImportMappingList.SetValueByMappingId(Ccm.Imports.Mappings.ProductImportMappingList.VendorCodeMapId, data, productDto, customAttributeDataDto);
            Assert.AreEqual(data, productDto.VendorCode);

            #endregion
        }

        [Test]
        public void GetBindingPath()
        {
            Galleria.Ccm.Imports.Mappings.ProductImportMappingList model
                 = Galleria.Ccm.Imports.Mappings.ProductImportMappingList.NewProductImportMappingList(true);

            #region Product Properties
            Assert.AreEqual(Product.AlternateDepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.AlternateDepthMapId));
            Assert.AreEqual(Product.AlternateHeightProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.AlternateHeightMapId));
            Assert.AreEqual(Product.AlternateWidthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.AlternateWidthMapId));
            Assert.AreEqual(Product.CanBreakTrayBackProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayBackMapId));
            Assert.AreEqual(Product.CanBreakTrayDownProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayDownMapId));
            Assert.AreEqual(Product.CanBreakTrayTopProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayTopMapId));
            Assert.AreEqual(Product.CanBreakTrayUpProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CanBreakTrayUpMapId));
            Assert.AreEqual(Product.CasePackUnitsProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CasePackUnitsMapId));
            Assert.AreEqual(Product.DepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DepthMapId));
            Assert.AreEqual(Product.DisplayDepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DisplayDepthMapId));
            Assert.AreEqual(Product.DisplayHeightProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DisplayHeightMapId));
            Assert.AreEqual(Product.DisplayWidthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DisplayWidthMapId));
            Assert.AreEqual(Product.FingerSpaceAboveProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.FingerSpaceAboveMapId));
            Assert.AreEqual(Product.FingerSpaceToTheSideProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.FingerSpaceToTheSideMapId));
            Assert.AreEqual(Product.ForceBottomCapProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ForceBottomCapMapId));
            Assert.AreEqual(Product.ForceMiddleCapProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ForceMiddleCapMapId));
            Assert.AreEqual(Product.FrontOverhangProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.FrontOverhangMapId));
            Assert.AreEqual(Product.IsFrontOnlyProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.IsFrontOnlyMapId));
            Assert.AreEqual(Product.IsPlaceHolderProductProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.IsPlaceHolderProductMapId));
            Assert.AreEqual(Product.MaxDeepProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.MaxDeepMapId));
            Assert.AreEqual(Product.MaxRightCapProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.MaxRightCapMapId));
            Assert.AreEqual(Product.MaxStackProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.MaxStackMapId));
            Assert.AreEqual(Product.MaxTopCapProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.MaxTopCapMapId));
            Assert.AreEqual(Product.MerchandisingStyleProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.MerchandisingStyleMapId));
            Assert.AreEqual(Product.MinDeepProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.MinDeepMapId));
            Assert.AreEqual(Product.NameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.NameMapId));
            Assert.AreEqual(Product.NestingDepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.NestingDepthMapId));
            Assert.AreEqual(Product.NestingHeightProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.NestingHeightMapId));
            Assert.AreEqual(Product.NestingWidthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.NestingWidthMapId));
            Assert.AreEqual(Product.NumberOfPegHolesProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.NumberOfPegHolesMapId));
            Assert.AreEqual(Product.OrientationTypeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.OrientationTypeMapId));
            Assert.AreEqual(Product.PegDepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegDepthMapId));
            Assert.AreEqual(Product.PegProngOffsetXProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegProngOffsetXMapId));
            Assert.AreEqual(Product.PegX2Property.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegX2MapId));
            Assert.AreEqual(Product.PegX3Property.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegX3MapId));
            Assert.AreEqual(Product.PegXProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegXMapId));
            Assert.AreEqual(Product.PegY2Property.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegY2MapId));
            Assert.AreEqual(Product.PegY3Property.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegY3MapId));
            Assert.AreEqual(Product.PegYProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PegYMapId));
            Assert.AreEqual(Product.PointOfPurchaseDepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseDepthMapId));
            Assert.AreEqual(Product.PointOfPurchaseHeightProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseHeightMapId));
            Assert.AreEqual(Product.PointOfPurchaseWidthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseWidthMapId));
            Assert.AreEqual(Product.SqueezeDepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeDepthMapId));
            Assert.AreEqual(Product.SqueezeHeightProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeHeightMapId));
            Assert.AreEqual(Product.SqueezeWidthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SqueezeWidthMapId));
            Assert.AreEqual(Product.StatusTypeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.StatusTypeMapId));
            Assert.AreEqual(Product.TrayDeepProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TrayDeepMapId));
            Assert.AreEqual(Product.TrayHighProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TrayHighMapId));
            Assert.AreEqual(Product.TrayThickDepthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickDepthMapId));
            Assert.AreEqual(Product.TrayThickHeightProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickHeightMapId));
            Assert.AreEqual(Product.TrayThickWidthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TrayThickWidthMapId));
            Assert.AreEqual(Product.TrayWideProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TrayWideMapId));
            Assert.AreEqual(Product.WidthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.WidthMapId));
            #endregion

            #region Product Attribute Data Properties

            Assert.AreEqual(Product.BarcodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.BarCodeMapId));
            Assert.AreEqual(Product.BrandProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.BrandMapId));
            Assert.AreEqual(Product.CaseCostProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CaseCostMapId));
            Assert.AreEqual(Product.ColourProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ColourMapId));
            Assert.AreEqual(Product.ConsumerInformationProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ConsumerInformationMapId));
            Assert.AreEqual(Product.CorporateCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CorporateCodeMapId));
            Assert.AreEqual(Product.CostPriceProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CostPriceMapId));
            Assert.AreEqual(Product.CountryOfOriginProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CountryOfOriginMapId));
            Assert.AreEqual(Product.CountryOfProcessingProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.CountryOfProcessingMapId));
            Assert.AreEqual(Product.DateDiscontinuedProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DateDiscontinuedMapId));
            Assert.AreEqual(Product.DateEffectiveProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DateEffectiveMapId));
            Assert.AreEqual(Product.DateIntroducedProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DateIntroducedMapId));
            Assert.AreEqual(Product.DeliveryFrequencyDaysProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DeliveryFrequencyDaysMapId));
            Assert.AreEqual(Product.DeliveryMethodProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.DeliveryMethodMapId));
            Assert.AreEqual(Product.FinancialGroupCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.FinancialGroupCodeMapId));
            Assert.AreEqual(Product.FinancialGroupNameProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.FinancialGroupNameMapId));
            Assert.AreEqual(Product.FlavourProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.FlavourMapId));
            Assert.AreEqual(Product.GarmentTypeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.GarmentTypeMapId));
            Assert.AreEqual(Product.HealthProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.HealthMapId));
            Assert.AreEqual(Product.IsNewProductProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.IsNewMapId));
            Assert.AreEqual(Product.IsPrivateLabelProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.IsPrivateLabelMapId));
            Assert.AreEqual(Product.ManufacturerCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerCodeMapId));
            Assert.AreEqual(Product.ManufacturerCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerCodeMapId));
            Assert.AreEqual(Product.ManufacturerProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerMapId));
            Assert.AreEqual(Product.ManufacturerRecommendedRetailPriceProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId));
            Assert.AreEqual(Product.ModelProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ModelMapId));
            Assert.AreEqual(Product.PackagingShapeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PackagingShapeMapId));
            Assert.AreEqual(Product.PackagingTypeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PackagingTypeMapId));
            Assert.AreEqual(Product.PatternProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PatternMapId));
            Assert.AreEqual(Product.PointOfPurchaseDescriptionProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.PointOfPurchaseDescriptionMapId));
            Assert.AreEqual(Product.RecommendedRetailPriceProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.RecommendedRetailPriceMapId));
            Assert.AreEqual(Product.SellPackCountProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SellPackCountMapId));
            Assert.AreEqual(Product.SellPackDescriptionProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SellPackDescriptionMapId));
            Assert.AreEqual(Product.SellPriceProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SellPriceMapId));
            Assert.AreEqual(Product.ShelfLifeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ShelfLifeMapId));
            Assert.AreEqual(Product.ShortDescriptionProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.ShortDescriptionMapId));
            Assert.AreEqual(Product.SizeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SizeMapId));
            Assert.AreEqual(Product.StyleNumberProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.StyleNumberMapId));
            Assert.AreEqual(Product.SubcategoryProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.SubCategoryMapId));
            Assert.AreEqual(Product.TaxRateProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TaxRateMapId));
            Assert.AreEqual(Product.TextureProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.TextureMapId));
            Assert.AreEqual(Product.UnitOfMeasureProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.UnitOfMeasureMapId));
            Assert.AreEqual(Product.VendorCodeProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.VendorCodeMapId));
            Assert.AreEqual(Product.VendorProperty.Name, model.GetBindingPath(Ccm.Imports.Mappings.ProductImportMappingList.VendorMapId));

            #endregion
        }

        #endregion
    }
}
