﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26706 : L.Luong 
//   Created (Auto-generated)
// V8-26322 : A.Silva
//      Amended how User records are inserted to avoid random duplicates.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Dal.Mock;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramConsumerDecisionTreeDal : TestBase<IPlanogramConsumerDecisionTreeDal, PlanogramConsumerDecisionTreeDto>
    {
        #region Fields
        private readonly List<String> _excludeProperties = new List<String> { "ExtendedData" };

        private List<PlanogramConsumerDecisionTreeDto> _sourceDtoList;
        private List<PlanogramDto> _parentPlanogramDtoList;
        private List<PropertyInfo> _properties;
        #endregion

        #region Constructor

        public PlanogramConsumerDecisionTreeDal()
            : base(typeof (DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramConsumerDecisionTreeDto));
            _properties = typeof(PlanogramConsumerDecisionTreeDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 25);
            _parentPlanogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
            _sourceDtoList = TestDataHelper.InsertPlanogramConsumerDecisionTreeDtos(dalContext, _parentPlanogramDtoList);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dbDalFactoryType)
        {
            const String fetchMethodName = "FetchByPlanogramId";
            const String propertyName = "PlanogramId";
            base.TestFetchByProperty(dbDalFactoryType, fetchMethodName, propertyName, _excludeProperties);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludeProperties);
        }


        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeDal>())
            {
                InitializeDtosAndProperties(dalContext);
                foreach (PlanogramConsumerDecisionTreeDto planCdtDto in _sourceDtoList) {
                    dal.DeleteById(planCdtDto.Id);
                    Assert.Throws<DtoDoesNotExistException>(
                        () => dal.FetchByPlanogramId(planCdtDto.PlanogramId),
                        "Dto should not exist after delete but one was fetched.");
                }
            }
        }
    }
}