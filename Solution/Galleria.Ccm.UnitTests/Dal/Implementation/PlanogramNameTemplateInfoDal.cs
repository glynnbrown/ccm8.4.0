﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramNameTemplateInfoDal : TestBase<IPlanogramNameTemplateInfoDal, PlanogramNameTemplateInfoDto>
    {
        private readonly List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Ccm.Dal.Mssql.DalFactory),
            typeof(Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            TestInfoFetchByEntityId<PlanogramNameTemplateDto>(dbDalFactoryType);
        }
    }
}