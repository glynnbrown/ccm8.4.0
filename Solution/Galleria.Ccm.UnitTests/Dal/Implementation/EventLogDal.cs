﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//  Updated so unit tests pass when milliseconds of datetimes are slightly out.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class EventLogDal : TestBase<IEventLogDal, EventLogDto>
    {
        #region Constructor

        public EventLogDal() : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        #region Helper

        /// <summary>
        /// Asserts that the properties of a model
        /// match the dto it has been loaded from
        /// </summary>
        /// <param name="dto">The dto to compare</param>
        /// <param name="model">The model to compare</param>
        private void AssertDtoAndModelAreEqual(List<EventLogDto> Listdto, EventLogDto model)
        {

            foreach (EventLogDto dto in Listdto)
            {

                if (dto.Id == model.Id)
                {
                    Assert.AreEqual(dto.Id, model.Id);
                    Assert.AreEqual(dto.EntityId, model.EntityId);
                    Assert.AreEqual(dto.EntityName, model.EntityName);
                    Assert.AreEqual(dto.EventLogNameId, model.EventLogNameId);
                    Assert.AreEqual(dto.EventLogName, model.EventLogName);
                    Assert.AreEqual(dto.Source, model.Source);
                    Assert.AreEqual(dto.EventId, model.EventId);
                    Assert.AreEqual(dto.EntryType, model.EntryType);
                    Assert.AreEqual(dto.UserId, model.UserId);
                    Assert.AreEqual(dto.UserName, model.UserName);
                    Assert.AreEqual(dto.Process, model.Process);
                    Assert.AreEqual(dto.ComputerName, model.ComputerName);
                    Assert.AreEqual(dto.URLHelperLink, model.URLHelperLink);
                    Assert.AreEqual(dto.Description, model.Description);
                    Assert.AreEqual(dto.Content, model.Content);
                    Assert.AreEqual(dto.WorkpackageName, model.WorkpackageName);
                    return;
                }
            }

            Assert.Fail("No Matching Records");
        }

        private void AssertDtoAndModelAreEqual(EventLogDto actual, EventLogDto expected)
        {
            Assert.AreEqual(actual.Id, expected.Id);
            Assert.AreEqual(actual.EntityId, expected.EntityId);
            Assert.AreEqual(actual.EntityName, expected.EntityName);
            Assert.AreEqual(actual.EventLogNameId, expected.EventLogNameId);
            Assert.AreEqual(actual.EventLogName, expected.EventLogName);
            Assert.AreEqual(actual.Source, expected.Source);
            Assert.AreEqual(actual.EventId, expected.EventId);
            Assert.AreEqual(actual.EntryType, expected.EntryType);
            Assert.AreEqual(actual.UserId, expected.UserId);
            Assert.AreEqual(actual.UserName, expected.UserName);
            Assert.AreEqual(actual.Process, expected.Process);
            Assert.AreEqual(actual.ComputerName, expected.ComputerName);
            Assert.AreEqual(actual.URLHelperLink, expected.URLHelperLink);
            Assert.AreEqual(actual.Description, expected.Description);
            Assert.AreEqual(actual.Content, expected.Content);
            Assert.AreEqual(actual.WorkpackageName, expected.WorkpackageName);
        }

        #endregion

        #region Insert and Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchAll(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    List<EntityDto> entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogNameDto> dtoEventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogDto> dtoList = TestDataHelper.InsertEventLogDtos(dalContext, 1, entityDto, dtoEventLogNameList);
                    List<EventLogDto> dalList = null;
                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        dalList = dal.FetchAll().ToList();

                        foreach (EventLogDto dalRecord in dalList)
                        {
                            AssertDtoAndModelAreEqual(dtoList, dalRecord);
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            // The Insert method is tested by TestFetchAll.
            TestFetchAll(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    List<EntityDto> entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogNameDto> dtoEventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogDto> dtoList = TestDataHelper.InsertEventLogDtos(dalContext, 1, entityDto, dtoEventLogNameList);
                    EventLogDto returnedDto = null;
                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        foreach (EventLogDto sourceDto in dtoList)
                        {
                            returnedDto = dal.FetchById(sourceDto.Id);
                            Assert.GreaterOrEqual(60000, Math.Abs((returnedDto.DateTime - sourceDto.DateTime).TotalMilliseconds));
                            sourceDto.DateTime = returnedDto.DateTime;
                            Assert.AreEqual(sourceDto, returnedDto);
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEventLogNameId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    List<EntityDto> entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogNameDto> dtoEventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogDto> dtoList = TestDataHelper.InsertEventLogDtos(dalContext, 1, entityDto, dtoEventLogNameList);
                    EventLogDto returnedDto = null;
                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        foreach (EventLogDto dto in dtoList)
                        {
                            List<EventLogDto> dalList = dal.FetchByEventLogNameId(dto.EventLogNameId).ToList();
                            List<EventLogDto> sourceDtos = new List<EventLogDto>(
                                dtoList.Where(d => d.EventLogNameId == dto.EventLogNameId));

                            Assert.AreEqual(sourceDtos.Count, dalList.Count);

                            foreach (EventLogDto sourceDto in sourceDtos)
                            {
                                returnedDto = dtoList.First(d => d.Id == sourceDto.Id);
                                Assert.GreaterOrEqual(60000, Math.Abs((returnedDto.DateTime - sourceDto.DateTime).TotalMilliseconds));
                                sourceDto.DateTime = returnedDto.DateTime;
                                Assert.AreEqual(sourceDto, returnedDto);
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEventLogNameIdEntryTypeEntityId(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            {
                var entityDtos = TestDataHelper.InsertEntityDtos(dalContext, 1);
                var eventLogNameDtos = TestDataHelper.InsertEventLogNameDtos(dalContext, 1);
                var expectedDtos = TestDataHelper.InsertEventLogDtos(dalContext, 1, entityDtos, eventLogNameDtos);

                List<EventLogDto> fetchedEventLogs;
                using (var dal = dalContext.GetDal<IEventLogDal>())
                {
                    const Int16 topRowCount = 0;
                    var eventLogNameId = eventLogNameDtos.First().Id;
                    var entryType = expectedDtos.First().EntryType;
                    var entityId = entityDtos.First().Id;
                    fetchedEventLogs = dal.FetchByEventLogNameIdEntryTypeEntityId(topRowCount, eventLogNameId, entryType, entityId).ToList();
                }

                Assert.AreEqual(expectedDtos.Count, fetchedEventLogs.Count);
                foreach (var expected in expectedDtos)
                {
                    // Model object assertion used, because it reflects type and iterates over
                    // property values, which is suitable for DTO.
                    var actual = fetchedEventLogs.FirstOrDefault(dto => dto.Id == expected.Id);
                    AssertDtoAndModelAreEqual(actual, expected);
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByMixedCriteria(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var dtoEventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 1);
                    var dtoEntityList = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    var dtoList = TestDataHelper.InsertEventLogDtos(dalContext, 1, dtoEntityList, dtoEventLogNameList);
                    List<EventLogDto> dalList = null;
                    // return the dtos and compare

                    using (var dal = dalFactory.CreateContext().GetDal<IEventLogDal>())
                    {

                        dalList = dal.FetchByMixedCriteria(100, dtoList[0].EventLogNameId, null, null,
                            null, null, dtoList[0].EntityId, null, dtoList[0].Process, null, null,
                            dtoList[0].Description, null, dtoList[0].GibraltarSessionId, DateTime.UtcNow.AddYears(-1), DateTime.UtcNow, null, null).ToList();

                        if (dalList.Any())
                        {

                            foreach (EventLogDto dalRecord in dalList)
                            {
                                Assert.AreEqual(dalRecord.EventLogNameId, dtoList[0].EventLogNameId);
                                Assert.AreEqual(dalRecord.EntityId, dtoList[0].EntityId);
                                Assert.AreEqual(dalRecord.Process, dtoList[0].Process);
                                Assert.AreEqual(dalRecord.Description, dtoList[0].Description);
                                Assert.AreEqual(dalRecord.GibraltarSessionId, dtoList[0].GibraltarSessionId);
                            }
                        }
                        else
                        {
                            Assert.Fail("No Record found");
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchTopSpecifiedAmountIncludingDeleted(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var dtoEventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 10);
                    var dtoEntityList = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    var dtoList = TestDataHelper.InsertEventLogDtos(dalContext, 2, dtoEntityList, dtoEventLogNameList);
                    List<EventLogDto> dalList = null;
                    // return the dtos and compare

                    using (var dal = dalFactory.CreateContext().GetDal<IEventLogDal>())
                    {

                        dalList = dal.FetchTopSpecifiedAmountIncludingDeleted(10).ToList();

                        if (dalList.Any())
                        {
                            Assert.AreEqual(10, dalList.Count());
                        }
                        else
                        {
                            Assert.Fail("No Record found");
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchAllOccuredAfterDateTime(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var dtoEventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 10);
                    var dtoEntityList = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    var dtoList = TestDataHelper.InsertEventLogDtos(dalContext, 2, dtoEntityList, dtoEventLogNameList);
                    List<EventLogDto> dalList = null;
                    // return the dtos and compare

                    using (var dal = dalFactory.CreateContext().GetDal<IEventLogDal>())
                    {

                        dalList = dal.FetchAllOccuredAfterDateTime(DateTime.UtcNow.AddHours(-1)).ToList();

                        if (dalList.Any())
                        {
                            Assert.AreEqual(20, dalList.Count());
                        }
                        else
                        {
                            Assert.Fail("No Record found");
                        }

                        dalList = dal.FetchAllOccuredAfterDateTime(DateTime.UtcNow.AddHours(1)).ToList();

                        Assert.AreEqual(0, dalList.Count());
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByWorkpackageId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    List<EntityDto> entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogNameDto> dtoEventLogNameList = TestDataHelper.InsertEventLogNameDtos(dalContext, 1);

                    // insert a set of dtos into the data source
                    List<EventLogDto> dtoList = TestDataHelper.InsertEventLogDtos(dalContext, 1, entityDto, dtoEventLogNameList);
                    EventLogDto returnedDto = null;
                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        //Create and insert event logs for specific workpackages
                        EventLogDto workpackage1EventLog1Dto = new EventLogDto();
                        TestDataHelper.SetPropertyValues(workpackage1EventLog1Dto, false);
                        workpackage1EventLog1Dto.EventLogNameId = dtoEventLogNameList[0].Id;
                        workpackage1EventLog1Dto.EventLogName = dtoEventLogNameList[0].Name;
                        workpackage1EventLog1Dto.EntityId = entityDto[0].Id;
                        workpackage1EventLog1Dto.EntityName = entityDto[0].Name;
                        workpackage1EventLog1Dto.DateTime = DateTime.UtcNow;
                        workpackage1EventLog1Dto.UserName = null;
                        workpackage1EventLog1Dto.WorkpackageId = 1;
                        workpackage1EventLog1Dto.WorkpackageName = "Workpackage 1";
                        dal.Insert(workpackage1EventLog1Dto);

                        EventLogDto workpackage1EventLog2Dto = new EventLogDto();
                        TestDataHelper.SetPropertyValues(workpackage1EventLog2Dto, false);
                        workpackage1EventLog2Dto.EventLogNameId = dtoEventLogNameList[0].Id;
                        workpackage1EventLog2Dto.EventLogName = dtoEventLogNameList[0].Name;
                        workpackage1EventLog2Dto.EntityId = entityDto[0].Id;
                        workpackage1EventLog2Dto.EntityName = entityDto[0].Name;
                        workpackage1EventLog2Dto.DateTime = DateTime.UtcNow;
                        workpackage1EventLog2Dto.UserName = null;
                        workpackage1EventLog2Dto.WorkpackageId = 1;
                        workpackage1EventLog2Dto.WorkpackageName = "Workpackage 1";
                        dal.Insert(workpackage1EventLog2Dto);


                        EventLogDto workpackage2EventLog1Dto = new EventLogDto();
                        TestDataHelper.SetPropertyValues(workpackage2EventLog1Dto, false);
                        workpackage2EventLog1Dto.EventLogNameId = dtoEventLogNameList[0].Id;
                        workpackage2EventLog1Dto.EventLogName = dtoEventLogNameList[0].Name;
                        workpackage2EventLog1Dto.EntityId = entityDto[0].Id;
                        workpackage2EventLog1Dto.EntityName = entityDto[0].Name;
                        workpackage2EventLog1Dto.DateTime = DateTime.UtcNow;
                        workpackage2EventLog1Dto.UserName = null;
                        workpackage2EventLog1Dto.WorkpackageId = 2;
                        workpackage2EventLog1Dto.WorkpackageName = "Workpackage 2";
                        dal.Insert(workpackage2EventLog1Dto);

                        //Test workpackage 1
                        IEnumerable<EventLogDto> returnedDtoList = dal.FetchByWorkpackageId(1);
                        Assert.AreEqual(2, returnedDtoList.Count());

                        //Test workpackage 2
                        returnedDtoList = dal.FetchByWorkpackageId(2);
                        Assert.AreEqual(1, returnedDtoList.Count());
                    }
                }
            }
        }


        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsertWithName(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                UserDto userDto = TestDataHelper.InsertUserDtos(dalFactory, 1)[0];
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    EntityDto entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1)[0];
                    EventLogNameDto eventLogNameDto = new EventLogNameDto()
                    {
                        Name = "Test1"
                    };
                    EventLogDto dto1 = new EventLogDto()
                    {
                        EntityId = entityDto.Id,
                        EntityName = entityDto.Name,
                        EventLogNameId = 32769,
                        EventLogName = "Test1",
                        Source = "1",
                        EventId = 32770,
                        EntryType = 256,
                        UserId = null,
                        UserName = null,
                        DateTime = DateTime.UtcNow,
                        Process = "2",
                        ComputerName = "3",
                        URLHelperLink = "4",
                        Description = "5",
                        Content = "6",
                        GibraltarSessionId = "7"
                    };
                    EventLogDto dto2 = new EventLogDto()
                    {
                        EntityId = null,
                        EntityName = null,
                        EventLogNameId = 32769,
                        EventLogName = "Test2",
                        Source = "1",
                        EventId = 32770,
                        EntryType = 256,
                        UserId = userDto.Id,
                        UserName = userDto.FirstName + " " + userDto.LastName,
                        DateTime = DateTime.UtcNow,
                        Process = "2",
                        ComputerName = "3",
                        URLHelperLink = "4",
                        Description = "5",
                        Content = "6",
                        GibraltarSessionId = "7"
                    };

                    using (IEventLogNameDal eventLogNameDal = dalContext.GetDal<IEventLogNameDal>())
                    {
                        // Insert event log name
                        eventLogNameDal.Insert(eventLogNameDto);
                        Assert.AreNotEqual(0, eventLogNameDto.Id);

                        using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                        {
                            // Insert event log for existing name
                            dal.InsertWithName(dto1);
                            Assert.AreNotEqual(0, dto1.Id);
                            dto1.EventLogNameId = eventLogNameDto.Id;
                            EventLogDto returnedDto = dal.FetchById(dto1.Id);
                            // One minute's grace:
                            Assert.GreaterOrEqual(60000, Math.Abs((returnedDto.DateTime - dto1.DateTime).TotalMilliseconds));
                            dto1.DateTime = returnedDto.DateTime;
                            Assert.AreEqual(dto1, returnedDto);

                            // Insert event log without existing name
                            dal.InsertWithName(dto2);
                            Assert.AreNotEqual(0, dto2.Id);
                            eventLogNameDto = eventLogNameDal.FetchByName("Test2");
                            dto2.EventLogNameId = eventLogNameDto.Id;
                            returnedDto = dal.FetchById(dto2.Id);
                            Assert.GreaterOrEqual(60000, Math.Abs((returnedDto.DateTime - dto2.DateTime).TotalMilliseconds));
                            dto2.DateTime = returnedDto.DateTime;
                            Assert.AreEqual(dto2, returnedDto);
                        }
                    }
                }
            }
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestDeleteAll(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                UserDto userDto = TestDataHelper.InsertUserDtos(dalFactory, 1)[0];
                List<EventLogDto> dtoEventLogList = new List<EventLogDto>();
                EntityDto entityDto = TestDataHelper.InsertEntityDtos(dalFactory, 1)[0];
                EventLogNameDto eventLogNameDto = new EventLogNameDto()
                {
                    Name = "Test1"
                };

                EventLogDto dto1 = new EventLogDto()
                {
                    EntityId = entityDto.Id,
                    EntityName = entityDto.Name,
                    EventLogNameId = eventLogNameDto.Id,
                    EventLogName = eventLogNameDto.Name,
                    Source = "1",
                    EventId = 32770,
                    EntryType = 256,
                    UserId = userDto.Id,
                    UserName = userDto.FirstName + " " + userDto.LastName,
                    DateTime = DateTime.UtcNow,
                    Process = "2",
                    ComputerName = "3",
                    URLHelperLink = "4",
                    Description = "5",
                    Content = "6",
                    GibraltarSessionId = "7"
                };
                dtoEventLogList.Add(dto1);

                EventLogDto dto2 = new EventLogDto()
                {
                    EntityId = null,
                    EntityName = null,
                    EventLogNameId = 32769,
                    EventLogName = "Test2",
                    Source = "1",
                    EventId = 32770,
                    EntryType = 256,
                    UserId = null,
                    DateTime = DateTime.UtcNow,
                    Process = "2",
                    ComputerName = "3",
                    URLHelperLink = "4",
                    Description = "5",
                    Content = "6",
                    GibraltarSessionId = "7"
                };
                dtoEventLogList.Add(dto2);
                Int32 dtoCount = 0;

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IEventLogNameDal eventLogNameDal = dalContext.GetDal<IEventLogNameDal>())
                    {
                        using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                        {
                            // Insert event log name
                            eventLogNameDal.Insert(eventLogNameDto);
                            Assert.AreNotEqual(0, eventLogNameDto.Id);

                            // insert into the dal
                            foreach (EventLogDto dto in dtoEventLogList)
                            {
                                dal.InsertWithName(dto);
                            }

                            dtoCount = dal.FetchAll().Count();
                            Assert.AreEqual(dtoCount, 2);

                            //now delete logs from the data source
                            IEnumerable<EventLogDto> eventLogList = dal.FetchAll();
                            Assert.AreNotEqual(0, eventLogList.Count());
                            dal.DeleteAll();
                            eventLogList = dal.FetchAll();
                            Assert.AreEqual(0, eventLogList.Count());

                            dalContext.Commit();
                        }
                    }
                }
            }
        }

        #endregion
    }
}
