﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using System.Collections.Generic;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class WorkflowTaskDal : TestBase<IWorkflowTaskDal, WorkflowTaskDto>
    {
        #region Fields
        private static List<String> _excludeProperties = new List<String> { "WorkflowId" };
        #endregion

        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fetch
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByWorkflowId(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByWorkflowId", "WorkflowId", _excludeProperties);
        }
        #endregion

        #region Insert
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, _excludeProperties, true);
        }
        #endregion

        #region Update
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, _excludeProperties);
        }
        #endregion

        #region Delete
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
        #endregion
    }
}
