﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25478 : A. Kuszyk
//      Initial version.
// V8-25632 : K. Pickup
//      Refactored to work with the framework version of the DalImplementationTestBase, which I introduced as part of 
//      the fix for a test in ConsumerDecisionTreeLevelDal.
// V8-25881 : A.Probyn
//      Updated so it works, had to remove framework test base
// V8-26322 : A.Silva
//      Amended excluded properties so that UniqueContentReference is excluded, it was messing up TestUpdate as there cannot be duplicates of it.
//      Amended how User records are inserted to avoid random duplicates.

#endregion

#region Version History: (CCM V8.3.0)
// V8-32396 : A.Probyn
//  Fixed Update by setting the created PackageDto's DateLastModified property.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Reflection;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramDal : TestBase<IPlanogramDal,PlanogramDto>
    {
        #region Fields 

        private readonly List<String> _excludeProperties = new List<String>
        {
            "ExtendedData",
            "PackageId",
            "UniqueContentReference"
        };
        private List<PlanogramDto> _sourceDtoList;
        private List<PropertyInfo> _properties;
        private DtoCollection<PlanogramDto> _dtos;

        #endregion

        #region Constructor

        public PlanogramDal() : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPackageId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestFetchByProperty(
                    dalFactoryType,
                    "FetchByPackageId",
                    "PackageId",
                    _excludeProperties);
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IPlanogramDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                var returnedDto = dal.FetchByPackageId(_sourceDtoList[i].Id).ElementAt(0);
                                CheckCreatedModifiedDeletedDates(_sourceDtoList[i], returnedDto);
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name))
                                    {
                                        Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestInsert(dalFactoryType, _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IPlanogramDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            var returnedDto = dal.FetchByPackageId(_sourceDtoList[i].Id).ElementAt(0);
                            CheckCreatedModifiedDeletedDates(_sourceDtoList[i], returnedDto);
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);
                    using (var dal = dalContext.GetDal<IPlanogramDal>())
                    {
                        dal.Insert(new PackageDto() { Name = "Package", DateLastModified = DateTime.Now, Id = _sourceDtoList[0].Id }, _sourceDtoList[0]);
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            var originalSourceDto = _sourceDtoList[0];
                            var alternativeSourceDto = _sourceDtoList[i];

                            // Update the DTO in memory.
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
                                {
                                    property.SetValue(
                                        originalSourceDto,
                                        property.GetValue(alternativeSourceDto,null),
                                        null);
                                }
                            }

                            // Update the DTO in database and verify success.
                            dal.Update(new PackageDto() { Name = "Package", DateLastModified = DateTime.Now, Id = originalSourceDto.Id }, originalSourceDto);
                            var returnedDto = dal.FetchByPackageId(originalSourceDto.Id).ElementAt(0);
                            CheckCreatedModifiedDeletedDates(originalSourceDto, returnedDto);
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestDeleteById(dalFactoryType);
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IPlanogramDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                //Ensure it exists
                                var returnedDto = dal.FetchByPackageId(_sourceDtoList[i].Id).ElementAt(0);
                                
                                //Delete planogram
                                dal.DeleteById(_sourceDtoList[i].Id);

                                //Ensure record is still returned as only the child elements should be deleted
                                Assert.IsTrue(dal.FetchByPackageId(_sourceDtoList[i].Id).Count() == 1, "Parent Planogram record should still exist, only delete children");
                            }
                        }
                    }
                }
            }
        }

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtos = CreateDtos<PlanogramDto>(dalContext);
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramDto));
            _properties = typeof(PlanogramDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 25);
            _sourceDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
        }
    }
}
