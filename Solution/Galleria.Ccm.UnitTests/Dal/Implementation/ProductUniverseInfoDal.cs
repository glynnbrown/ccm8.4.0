﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created.
// V8-26560 : A.Probyn
//  Added TestFetchByProductUniverseIds
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductUniverseInfoDal : TestBase<IProductUniverseInfoDal,ProductUniverseInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<ProductUniverseDto>(dbDalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductUniverseIds(Type dalFactoryType)
        {
            base.TestInfoFetchByProperties<ProductUniverseDto, Int32>(
               dalFactoryType,
               "FetchByProductUniverseIds",
               new List<String> { },
               "Id",
               true);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductGroupId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType, "FetchByProductGroupId", "ProductGroupId");
        }
    }
}
