﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-27804 L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class PlanogramImportTemplateMappingDal : TestBase<IPlanogramImportTemplateMappingDal, PlanogramImportTemplateMappingDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.User.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory)
        };


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramImportTemplateId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestFetchByProperty(dalFactoryType,
                    "FetchByPlanogramImportTemplateId",
                    "PlanogramImportTemplateId",
                    _excludeProperties);
            }
        }


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties, true);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }
    }
}
