﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24863 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class SystemSettingDal : TestBase<ISystemSettingsDal, SystemSettingsDto>
    {
        private List<String> _excludeProperties = new List<String>();

        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            base.TestFetchByEntityId(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchDefault(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
           // base.TestFetchByEntityId(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            base.TestFetchById(dalFactoryType, _excludeProperties);
        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, _excludeProperties,true);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
