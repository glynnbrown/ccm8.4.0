﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductUniverseDal : TestBase<IProductUniverseDal,ProductUniverseDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdateProductUniverses(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    //create product groups
                    DtoCollection<ProductGroupDto> dtos = CreateDtos<ProductGroupDto>(dalContext);
                    List<ProductGroupDto> sourceDtoList = dtos.Dtos;
                    dtos.InsertDtos(dalContext, _dtoSpec);

                    foreach (var entGroup in sourceDtoList.GroupBy(g => g.ProductHierarchyId))
                    {
                        ProductHierarchyDto hierarchyDto;

                        using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                        {
                            hierarchyDto = dal.FetchById(entGroup.Key);
                        }

                        using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                        {
                            dal.UpdateProductUniverses(hierarchyDto.EntityId);

                            foreach (ProductGroupDto groupDto in entGroup)
                            {
                                ProductUniverseDto universeDto = 
                                    dal.FetchByEntityId(hierarchyDto.EntityId)
                                    .Where(pu=>pu.ProductGroupId==groupDto.Id)
                                    .FirstOrDefault();
                                Assert.IsNotNull(universeDto);

                                Assert.IsTrue(universeDto.Name.Contains(groupDto.Code));
                            }

                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dbDalFactoryType)
        {
            base.TestFetchByProperties(
                dbDalFactoryType,
                "FetchByEntityIdName",
                new List<String>() { "EntityId", "Name" });
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestFetchByEntityId(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, null);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestUpsert(Type dbDalFactoryType)
        {
            base.TestUpsert<ProductUniverseIsSetDto>(dbDalFactoryType, new List<String>(){"ParentUniqueContentReference"});
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteByEntityId(Type dbDalFactoryType)
        {
            base.TestDeleteByEntityId(dbDalFactoryType);
        }
    }
}
