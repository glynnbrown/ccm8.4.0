﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created
// V8-26815 : A.Silva ~ Added placeholders for the UserDal tests.
// V8-26322 : A.Silva
//      Tidying up.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class ValidationTemplateGroupDal : TestBase<IValidationTemplateGroupDal, ValidationTemplateGroupDto>
    {
        #region Constructor

        public ValidationTemplateGroupDal()
            : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory), typeof (Ccm.Dal.User.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByValidationTemplateId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
                Assert.Inconclusive("Not tested now as the base test does not work with this model.");
            
            TestFetchByProperty(dalFactoryType, "FetchByValidationTemplateId", "ValidationTemplateId");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
                Assert.Inconclusive("Not tested now as the base test does not work with this model.");
               
            base.TestInsert(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
                Assert.Inconclusive("Not tested now as the base test does not work with this model.");
       
            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
                Assert.Inconclusive("Not tested now as the base test does not work with this model.");
            
            base.TestDeleteById(dalFactoryType);
        }
    }
}