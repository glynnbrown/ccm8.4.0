﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramExportTemplateInfoDal : TestBase<IPlanogramExportTemplateInfoDal, PlanogramExportTemplateInfoDto>
    {
        private readonly List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Ccm.Dal.Mssql.DalFactory),
            typeof(Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            TestInfoFetchByEntityId<PlanogramExportTemplateDto>(dbDalFactoryType);
        }

    }
}
