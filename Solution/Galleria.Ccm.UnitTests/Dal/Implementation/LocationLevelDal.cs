﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25445 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationLevelDal : TestBase<ILocationLevelDal, LocationLevelDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByLocationHierarchyId(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByLocationHierarchyId", "LocationHierarchyId");
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, null, false);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }


        #region Ticket Related

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM23377_DeletingLevelDoesNotAffectChildGroups(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 2 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<LocationHierarchyDto> hierarchyDtoList = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityList[0].Id);


                //inert a root level and 2 child levels
                LocationLevelDto rootLevel, childLevel1, childLevel2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        rootLevel = new LocationLevelDto()
                        {
                            Name = "root",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new LocationLevelDto()
                        {
                            Name = "level1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new LocationLevelDto()
                        {
                            Name = "level2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);
                    }
                    dalContext.Commit();
                }

                //add a group per level
                LocationGroupDto rootGroupDto, group2Dto, group3Dto;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        rootGroupDto = new LocationGroupDto()
                        {
                            Code = "0",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            Name = "root",
                            LocationLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootGroupDto);

                        group2Dto = new LocationGroupDto()
                        {
                            Code = "1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            Name = "group2",
                            LocationLevelId = childLevel1.Id,
                            ParentGroupId = rootGroupDto.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group2Dto);

                        group3Dto = new LocationGroupDto()
                        {
                            Code = "2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            Name = "group3",
                            LocationLevelId = childLevel2.Id,
                            ParentGroupId = group2Dto.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group3Dto);
                    }
                    dalContext.Commit();
                }


                //delete level 2
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        dal.DeleteById(childLevel2.Id);
                    }
                    dalContext.Commit();
                }

                //check the group3 still points to level2.
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        LocationGroupDto dalDto = dal.FetchById(group3Dto.Id);
                        Assert.AreEqual(dalDto.LocationLevelId, group3Dto.LocationLevelId);
                        Assert.AreEqual(dalDto.LocationLevelId, childLevel2.Id);
                    }
                    dalContext.Commit();
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM14222_DeleteAllChildLevels(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 2 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<LocationHierarchyDto> hierarchyDtoList = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityList[0].Id);

                //inert a root level and 2 child levels
                LocationLevelDto rootLevel, childLevel1, childLevel2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        rootLevel = new LocationLevelDto()
                        {
                            Name = "root",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new LocationLevelDto()
                        {
                            Name = "level1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new LocationLevelDto()
                        {
                            Name = "level2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);
                    }
                    dalContext.Commit();
                }

                //remove both the child levels
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        dal.DeleteById(childLevel1.Id); //delete level 1 first as this is what happens through the model
                        dal.DeleteById(childLevel2.Id);
                    }
                    dalContext.Commit();
                }

                //try to retrieve again and check the dtodoesnotexist is thrown
                LocationLevelDto dto;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        dto = dal.FetchById(childLevel1.Id);
                        Assert.IsNotNull(dto.DateDeleted, "childLevel1 should have been deleted");

                        dto = dal.FetchById(childLevel2.Id);
                        Assert.IsNotNull(dto.DateDeleted, "childLevel1 should have been deleted");
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM14222_DeleteMiddleChildLevel(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 2 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<LocationHierarchyDto> hierarchyDtoList = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityList[0].Id);

                //inert a root level and 2 child levels
                LocationLevelDto rootLevel, childLevel1, childLevel2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        rootLevel = new LocationLevelDto()
                        {
                            Name = "root",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new LocationLevelDto()
                        {
                            Name = "level1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new LocationLevelDto()
                        {
                            Name = "level2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);
                    }
                    dalContext.Commit();
                }


                //remove the middle level and update the root and third levels
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(childLevel1.Id);

                        //set level 2 to now be the direct child of the root
                        childLevel2.ParentLevelId = rootLevel.Id;

                        dal.Update(rootLevel);
                        dal.Update(childLevel2);

                    }
                    dalContext.Commit();
                }


                //try to retrieve again and check the dtodoesnotexist is thrown
                LocationLevelDto dto;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        dto = dal.FetchById(childLevel1.Id);
                        Assert.IsNotNull(dto.DateDeleted, "childLevel1 should have been deleted");

                        childLevel2 = dal.FetchById(childLevel2.Id);
                        rootLevel = dal.FetchById(rootLevel.Id);
                    }
                }

                //check the child level 2 is now the direct child of the rootlevel
                Assert.AreEqual(childLevel2.ParentLevelId, rootLevel.Id);
            }
        }


        #endregion
    }
}
