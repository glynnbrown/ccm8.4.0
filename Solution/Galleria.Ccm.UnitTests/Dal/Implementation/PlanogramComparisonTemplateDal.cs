﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public class PlanogramComparisonTemplateDal : TestBase<IPlanogramComparisonTemplateDal, PlanogramComparisonTemplateDto>
    {
        #region Fields

        private static readonly Type MssqlDalFactoryType = typeof (DalFactory);
        private static readonly Type MockDalFactoryType = typeof (Ccm.Dal.Mock.DalFactory);
        private static readonly Type UserDalFactoryType = typeof(Ccm.Dal.User.DalFactory);

        private readonly List<Type> _dalFactoryTypes = new List<Type>
                                                       {
                                                           MssqlDalFactoryType,
                                                           MockDalFactoryType,
                                                       };

        #endregion

        #region Fetch

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            TestFetchByProperties(dalFactoryType, "FetchByEntityIdName", new List<String> { "EntityId", "Name" });
        }

        #endregion

        #region Insert

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            base.TestInsert(dalFactoryType);
        }

        #endregion

        #region Update

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            base.TestUpdate(dalFactoryType, new List<String> {"RowVersion"});
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            if (dalFactoryType == MockDalFactoryType)
            {
                base.TestDeleteById(dalFactoryType);
                return;
            }

            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    EntityDto entity = TestDataHelper.InsertEntityDtos(dalContext, 1).First();
                    List<PlanogramComparisonTemplateDto> dtos = TestDataHelper.InsertPlanogramComparisonTemplateDtos(dalFactory, 1, entity.Id);

                    using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
                    {
                        foreach (PlanogramComparisonTemplateDto dto in dtos)
                        {
                            //Check are correctly added.
                            PlanogramComparisonTemplateDto returnedDto = dal.FetchById(dto.Id);
                            Assert.IsNotNull(returnedDto);
                            Assert.AreEqual(dtos.First().Id, returnedDto.Id);

                            //Check are now removed
                            dal.DeleteById(dto.Id);
                            Assert.Throws<DtoDoesNotExistException>(() => dal.FetchById(dto.Id));
                        }
                    }
                }
            }
        }

        #endregion

        #region Locking

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestLockById(Type dalFactoryType)
        {
            Assert.Ignore("Cannot test file locking");
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUnlockById(Type dalFactoryType)
        {
            Assert.Ignore("Cannot test file locking");
        }

        #endregion
    }
}