﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public class PlanogramComparisonDal : TestBase<IPlanogramComparisonDal, PlanogramComparisonDto>
    {
        #region Constants

        private const String FetchByIdPropertyName = "PlanogramId";
        private const String FetchMethodName = "FetchByPlanogramId";

        #endregion

        #region Fields

        private readonly List<Type> _dalFactoryTypes =
            new List<Type>
            {
                typeof (Ccm.Dal.Mssql.DalFactory)
                // Mock dal is tested in the framework.
            };

        private static readonly List<String> PropertiesToExclude = new List<String> {"ExtendedData"};

        #endregion

        #region Fetch

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dalFactoryType)
        {
            TestFetchByProperty(dalFactoryType, FetchMethodName, FetchByIdPropertyName, PropertiesToExclude);
        }

        #endregion

        #region Insert

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, PropertiesToExclude);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, PropertiesToExclude);
        }

        #endregion

        #region Update

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            TestUpdate(dalFactoryType, PropertiesToExclude);
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }

        #endregion
    }
}