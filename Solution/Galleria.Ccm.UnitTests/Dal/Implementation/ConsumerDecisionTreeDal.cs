﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)

// V8-25629 : A. Kuszyk
//      Initial version.
// V8-26520 : J.Pickup
//  FetchByProductGroupId
// V8-26322 : A.Silva
//      Amended TestFetchByEntityIdConsumerDecisionTreeIds so that it compares dtos correctly.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ConsumerDecisionTreeDal : TestBase<IConsumerDecisionTreeDal,ConsumerDecisionTreeDto>
    {
        #region Constructor

        public ConsumerDecisionTreeDal() : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            const String fetchMethodName = "FetchById";
            const String propertyName = "Id";
            TestFetchByProperty(dbDalFactoryType, fetchMethodName, propertyName);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dbDalFactoryType)
        {
            const String fetchMethodName = "FetchByEntityIdName";
            var propertyNames = new List<String> { "EntityId", "Name" };
            TestFetchByProperties(dbDalFactoryType,fetchMethodName,propertyNames);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityIdConsumerDecisionTreeIds(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                var entityId = TestDataHelper.InsertEntityDtos(dalFactory, 1).First().Id;
                var expectedDtos = TestDataHelper.InsertConsumerDecisionTreeDtos(dalFactory, entityId, 10);

                List<ConsumerDecisionTreeDto> actualDtos;
                using (var dalContext = dalFactory.CreateContext())
                using (var dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    var consumerDecisionTreeIds = expectedDtos.Select(dto => dto.Id);
                    actualDtos = dal.FetchByEntityIdConsumerDecisionTreeIds(entityId, consumerDecisionTreeIds).ToList();
                }

                Assert.AreEqual(expectedDtos.Count, actualDtos.Count);
                foreach (var expected in expectedDtos)
                {
                    var actual = actualDtos.FirstOrDefault(item => item.Id == expected.Id);
                    AssertHelper.AssertModelObjectsAreEqual(expected, actual);
                }
            }
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, null, false);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

    }
}
