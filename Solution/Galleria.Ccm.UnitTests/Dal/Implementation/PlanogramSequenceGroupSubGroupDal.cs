﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramSequenceGroupSubGroupDal : TestBase<IPlanogramSequenceGroupSubGroupDal, PlanogramSequenceGroupSubGroupDto>
    {
        private static readonly List<String> ExcludedProperties = new List<String> { "ExtendedData" };

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramSequenceGroupSubGroupDal() : base(typeof(Ccm.Dal.Mock.DalFactory), typeof(Ccm.Dal.Mssql.DalFactory)) { }

        #endregion

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramSequenceGroupId(Type dalFactoryType)
        {
            const String fetchMethodName = "FetchByPlanogramSequenceGroupId";
            const String propertyName = "PlanogramSequenceGroupId";
            TestFetchByProperty(dalFactoryType, fetchMethodName, propertyName, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            TestUpdate(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
