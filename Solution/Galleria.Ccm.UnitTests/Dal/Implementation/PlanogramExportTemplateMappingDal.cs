﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class PlanogramExportTemplateMappingDal : TestBase<IPlanogramExportTemplateMappingDal, PlanogramExportTemplateMappingDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.User.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory)
        };


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramExportTemplateId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestFetchByProperty(dalFactoryType,
                    "FetchByPlanogramExportTemplateId",
                    "PlanogramExportTemplateId",
                    _excludeProperties);
            }
        }


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties, true);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }
    }
}
