﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25478 : A. Kuszyk
//      Initial version.
// V8-25881 : A.Probyn
//  Updated after MetaData properties
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public class PlanogramAssemblyComponentDal : TestBase<IPlanogramAssemblyComponentDal,PlanogramAssemblyComponentDto>
    {
        private List<String> _excludeProperties = new List<String> { "ExtendedData" };

        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
        };

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByPlanogramAssemblyId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType,
                "FetchByPlanogramAssemblyId", "PlanogramAssemblyId", _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }



        #region OLD

        //#region Fields
        //private List<String> _excludeProperties = new List<String>() { "ExtendedData" };
        //private List<Type> _dbDalFactoryTypes;
        //private List<PlanogramAssemblyComponentDto> _sourceDtoList;
        //private List<PlanogramAssemblyDto> _parentAssemblySourceDtoList;
        //private List<PlanogramComponentDto> _parentComponentSourceDtoList;
        //private new DtoSpec _dtoSpec;
        //private List<PropertyInfo> _properties;
        //#endregion

        //private void InitializeDtosAndProperties(IDalContext dalContext)
        //{
        //    _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramAssemblyComponentDto));
        //    _properties = typeof(PlanogramAssemblyComponentDto).GetProperties().Where(p => p.CanWrite).ToList();

        //    //Insert parents
        //    List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, 25);
        //    List<PlanogramDto> planogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, 1, packageDtoList);
        //    _parentAssemblySourceDtoList = TestDataHelper.InsertPlanogramAssemblyDtos(dalContext, 1, planogramDtoList);
        //    _parentComponentSourceDtoList = TestDataHelper.InsertPlanogramComponentDtos(dalContext, 1, planogramDtoList);
        //    _sourceDtoList = TestDataHelper.InsertPlanogramAssemblyComponentDtos(dalContext, _parentAssemblySourceDtoList, _parentComponentSourceDtoList);
        //}

        //public PlanogramAssemblyComponentDal()
        //{
        //    _dbDalFactoryTypes = new List<Type>();
        //    _dbDalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.Mock.DalFactory));
        //    _dbDalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.Mssql.DalFactory));
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public void TestFetchByPlanogramAssemblyId(Type dalFactoryType)
        //{
        //    if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //    {
        //        base.TestFetchByProperty(
        //            dalFactoryType,
        //            "FetchByPlanogramAssemblyId",
        //            "PlanogramAssemblyId",
        //            _excludeProperties);
        //        return;
        //    }

        //    using (var dalFactory = CreateDalFactory(dalFactoryType))
        //    {
        //        using (var dalContext = dalFactory.CreateContext())
        //        {
        //            InitializeDtosAndProperties(dalContext);

        //            using (var dal = dalContext.GetDal<IPlanogramAssemblyComponentDal>())
        //            {
        //                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //                {
        //                    IEnumerable<PlanogramAssemblyComponentDto> returnedDtoList = dal.FetchByPlanogramAssemblyId(_sourceDtoList[i].PlanogramAssemblyId);
        //                    PlanogramAssemblyComponentDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
        //                    foreach (var property in _properties)
        //                    {
        //                        if (!_excludeProperties.Contains(property.Name))
        //                        {
        //                            Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public void TestInsert(Type dalFactoryType)
        //{
        //   if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //   {
        //        base.TestInsert(dalFactoryType, _excludeProperties);
        //        return;
        //   }

        //   using (var dalFactory = CreateDalFactory(dalFactoryType))
        //   {
        //       using (var dalContext = dalFactory.CreateContext())
        //       {
        //           InitializeDtosAndProperties(dalContext);

        //           using (var dal = dalContext.GetDal<IPlanogramAssemblyComponentDal>())
        //           {
        //               for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //               {
        //                   IEnumerable<PlanogramAssemblyComponentDto> returnedDtoList = dal.FetchByPlanogramAssemblyId(_sourceDtoList[i].PlanogramAssemblyId);
        //                   PlanogramAssemblyComponentDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
        //                   foreach (var property in _properties)
        //                   {
        //                       if (!_excludeProperties.Contains(property.Name))
        //                       {
        //                           Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
        //                       }
        //                   }
        //               }
        //           }
        //       }
        //   }
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public void TestUpdate(Type dalFactoryType)
        //{
        //    if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //    {
        //        base.TestUpdate(
        //        dalFactoryType,
        //        _excludeProperties); 
        //        return;
        //    }

        //    using (var dalFactory = CreateDalFactory(dalFactoryType))
        //    {
        //        using (var dalContext = dalFactory.CreateContext())
        //        {
        //            InitializeDtosAndProperties(dalContext);

        //            using (var dal = dalContext.GetDal<IPlanogramAssemblyComponentDal>())
        //            {
        //                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //                {
        //                    var originalSourceDto = _sourceDtoList[0];
        //                    var alternativeSourceDto = _sourceDtoList[i];

        //                    // Update the DTO in memory.
        //                    foreach (var property in _properties)
        //                    {
        //                        if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
        //                        {
        //                            property.SetValue(
        //                                originalSourceDto,
        //                                property.GetValue(alternativeSourceDto, null),
        //                                null);
        //                        }
        //                    }

        //                    // Update the DTO in database and verify success.
        //                    dal.Update(originalSourceDto);
        //                    IEnumerable<PlanogramAssemblyComponentDto> returnedDtoList = dal.FetchByPlanogramAssemblyId(originalSourceDto.PlanogramAssemblyId);
        //                    PlanogramAssemblyComponentDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(originalSourceDto.Id));
                           
        //                    foreach (var property in _properties)
        //                    {
        //                        if (!_excludeProperties.Contains(property.Name))
        //                        {
        //                            Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public new void TestDeleteById(Type dalFactoryType)
        //{
        //    if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //    {
        //        base.TestDeleteById(dalFactoryType);            
        //        return;
        //    }

        //    using (var dalFactory = CreateDalFactory(dalFactoryType))
        //    {
        //        using (var dalContext = dalFactory.CreateContext())
        //        {
        //            InitializeDtosAndProperties(dalContext);

        //            using (var dal = dalContext.GetDal<IPlanogramAssemblyComponentDal>())
        //            {
        //                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //                {
        //                    //Call delete
        //                    dal.DeleteById(_sourceDtoList[i].Id);

        //                    //Ensure its not returned
        //                    IEnumerable<PlanogramAssemblyComponentDto> returnedDtoList = dal.FetchByPlanogramAssemblyId(_sourceDtoList[i].PlanogramAssemblyId);
        //                    PlanogramAssemblyComponentDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
        //                    Assert.IsNull(returnedDto);
        //                }
        //            }
        //        }
        //    }
        //}

        #endregion
    }
}
