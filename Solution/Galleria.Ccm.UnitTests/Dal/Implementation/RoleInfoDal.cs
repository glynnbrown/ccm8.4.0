﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class RoleInfoDal : TestBase<IRoleInfoDal, RoleInfoDto>
    {
        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };


        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            throw new InconclusiveException("TODO - Need to work out how to test - returns single dto");
            //base.TestInfoFetchByProperty<RoleDto>(dbDalFactoryType, "FetchById", "Id");
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            throw new InconclusiveException("TODO - Need to work out how to test - EntityId is on RoleEntity");
            //base.TestInfoFetchByProperty<RoleDto>(dbDalFactoryType, "FetchByEntityId", "EntityId");
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchAll(Type dbDalFactoryType)
        {
            base.TestInfoFetchAll<RoleDto>(dbDalFactoryType);
        }
    }
}
