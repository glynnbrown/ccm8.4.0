﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Reporting.Model;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ReportComponentDal : TestBase<IReportComponentDal, ReportComponentDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fields
        List<ReportSectionDto> _reportSectionDtos = new List<ReportSectionDto>();
        #endregion

        #region Helpers

        private List<ReportComponentDto> InsertDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<DataModelDto> dataModelDtos = TestDataHelper.InsertDataModelDtos(dalFactory);
            List<ReportDto> reportDtos = TestDataHelper.InsertReportDtos(dalFactory, numToInsert, dataModelDtos);
            _reportSectionDtos = TestDataHelper.InsertReportSectionDtos(dalFactory, 1, reportDtos);
            List<ReportComponentDto> reportComponentDtos = TestDataHelper.InsertReportComponentDtos(dalFactory, 5, _reportSectionDtos);
            return reportComponentDtos;
        }
        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDal dal = dalContext.GetDal<IReportComponentDal>())
                    {
                        foreach (ReportComponentDto sourceDto in sourceDtoList)
                        {
                            ReportComponentDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);

                            //Test return following deletion
                            dal.DeleteById(dalDto.Id);

                            ReportComponentDto returnedDto;
                            Assert.Throws<DtoDoesNotExistException>
                                (() => { returnedDto = dal.FetchById(dalDto.Id); },
                                "Deleted items should be returned when fetched by Id");
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByReportSectionId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDal dal = dalContext.GetDal<IReportComponentDal>())
                    {
                        foreach (ReportSectionDto sectionDto in _reportSectionDtos)
                        {
                            List<ReportComponentDto> dalDtos = dal.FetchByReportSectionId(sectionDto.Id).ToList();
                            List<ReportComponentDto> sourceDtosInSection = sourceDtoList.Where(r => r.ReportSectionId == sectionDto.Id).ToList();

                            Assert.AreEqual(sourceDtosInSection.Count, dalDtos.Count, "Correct number of items should be returned");

                            foreach (ReportComponentDto sourceDto in sourceDtosInSection)
                            {
                                ReportComponentDto dalDto = dalDtos.FirstOrDefault(s => s.Id == sourceDto.Id);
                                Assert.IsNotNull(dalDto);
                                Assert.AreEqual(dalDto, sourceDto);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDal dal = dalContext.GetDal<IReportComponentDal>())
                    {
                        foreach (ReportComponentDto sourceDto in sourceDtoList)
                        {
                            ReportComponentDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);
                        }
                    }
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be updated
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDal dal = dalContext.GetDal<IReportComponentDal>())
                    {
                        foreach (ReportComponentDto sourceDto in sourceDtoList)
                        {
                            ReportComponentDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);

                            // update the compare dto
                            dalDto.Height = 2.3F;
                            dalDto.Description = "New description";
                            dalDto.Width = 3.1F;
                            dal.Update(dalDto);

                            dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreNotEqual(sourceDto, dalDto);
                            Assert.AreEqual(2.3F, dalDto.Height);
                            Assert.AreEqual(3.1F, dalDto.Width);
                            Assert.AreEqual("New description", dalDto.Description);
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Tests that a product universe can be deleted
        /// </summary>
        /// <param name="dalFactoryType">The dal factory to test</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestDeleteById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDal dal = dalContext.GetDal<IReportComponentDal>())
                    {
                        foreach (ReportComponentDto sourceDto in sourceDtoList)
                        {
                            // delete the dto
                            dal.DeleteById(sourceDto.Id);

                            ReportComponentDto returnedDto;
                            Assert.Throws<DtoDoesNotExistException>
                                (() => { returnedDto = dal.FetchById(sourceDto.Id); },
                                "Deleted child items should not be returned when fetched by Id");
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

    }
}
