﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27153 : A.Silva
//      Created
// V8-27562 : A.Silva
//      Added TestFetchByEntityIdName()

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mock;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class RenumberingStrategyDal : TestBase<IRenumberingStrategyDal, RenumberingStrategyDto>
    {
        #region Test Helpers

        private const String DalFactoryTypes = "_dalFactoryTypes";

        private static readonly List<String> PropertiesToExclude = new List<String>
        {
            "EntityId",
            "DateCreated",
            "DateDeleted",
            "DateLastModified",
            "RowVersion",
            "DtoKey"
        };

        private readonly List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof (DalFactory),
            typeof (Ccm.Dal.Mssql.DalFactory)
        };

        #endregion

        [Test, TestCaseSource(DalFactoryTypes)]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }

        [Test, TestCaseSource(DalFactoryTypes)]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            base.TestFetchByEntityId(dalFactoryType);
        }

        [Test, TestCaseSource(DalFactoryTypes)]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            TestFetchByProperties(dalFactoryType, 
                "FetchByEntityIdName", 
                new List<String> { "EntityId", "Name" }, 
                PropertiesToExclude);
        }

        [Test, TestCaseSource(DalFactoryTypes)]
        public void TestFetchById(Type dalFactoryType)
        {
            base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource(DalFactoryTypes)]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, isReinsertAllowed: false);
        }

        [Test, TestCaseSource(DalFactoryTypes)]
        public void TestUpdate(Type dalFactoryType)
        {
            TestUpdate(dalFactoryType, PropertiesToExclude);
        }
    }
}