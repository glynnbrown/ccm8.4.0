﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25395 A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class ProductLibraryColumnMappingDal : TestBase<IProductLibraryColumnMappingDal, ProductLibraryColumnMappingDto>
    {
        private List<String> _excludeProperties = new List<String> { /*"ProductLibraryId"*/ };
        private List<String> _userDalExcludeProperties = new List<String> { "Id", "ProductLibraryId" };



        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.User.DalFactory);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("Ids change so cannot test");

            }
            else
            {
                base.TestFetchById(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByProductLibraryId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
            }
            else
            {
                base.TestFetchByProperty(dalFactoryType, "FetchByProductLibraryId", "ProductLibraryId", _excludeProperties);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TODO");
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties, true);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TODO");
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TODO");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }

    }
}
