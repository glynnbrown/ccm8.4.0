﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25587 L.Hodson
//  Created
// V8-26233 : A.Silva
//      Added TestFetchByWorkpackageIdAutomationProcessingStatus
#endregion
#region Version History: (CCM 8.0.1)
//V8-28507 : D.Pleasance
//  Added TestFetchByWorkpackageIdPagingCriteria \ TestFetchByWorkpackageIdAutomationProcessingStatusPagingCriteria
#endregion
#region Version History: CCM810
// V8-29986 : A.Probyn
//  Added FetchNonDebugByCategoryCode
// V8-30213 : L.Luong
//  Added FetchNonDebugByLocationCode
#endregion
#region Version History: CCM830
// V8-31831 : A.Probyn
//  Extended to populate CustomAttributeData for planograms too
// V8-33003 : J.Pickup
//	Added PlanogramInfo_AssignedLocationCount tests to all fetches
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Reflection;
using Galleria.Ccm.Dal.Mock;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public sealed class PlanogramInfoDal : TestBase<IPlanogramInfoDal, PlanogramInfoDto>
    {
        #region Fields

        private List<PlanogramDto> _testDtos;
        private UserDto _userDto;
        private IDalFactory _dalFactory;

        #endregion

        #region Constructor

        public PlanogramInfoDal()
            : base(typeof(DalFactory), typeof(Ccm.Dal.Mssql.DalFactory))
        { }

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPlanogramGroupId(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, 1);
                    List<ProductHierarchyDto> hierrarchies = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                    List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchies.First().Id, 1);
                    List<ProductGroupDto> productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, levels, 1);



                    //Add parent data
                    EntityDto entityDto = Helpers.TestDataHelper.InsertEntityDtos(dalFactory, 1).First();

                    //Insert hiearchy
                    PlanogramHierarchyDto hierarchyDto = new PlanogramHierarchyDto();
                    using (var dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                    {
                        hierarchyDto.EntityId = entityDto.Id;
                        hierarchyDto.Name = "Hieararchy";
                        dal.Insert(hierarchyDto);
                    }

                    //Add planogram group planograms
                    List<PlanogramGroupPlanogramDto> groupPlanogramDtoList = new List<PlanogramGroupPlanogramDto>();
                    using (var planogramGroupDal = dalContext.GetDal<IPlanogramGroupDal>())
                    {
                        using (var planogramGroupPlanogramDal = dalContext.GetDal<IPlanogramGroupPlanogramDal>())
                        {
                            Int32 groupNumber = 1;
                            //For each generated planogram dto
                            foreach (PlanogramDto planDto in _testDtos)
                            {
                                //Create group
                                PlanogramGroupDto groupDto = new PlanogramGroupDto();
                                groupDto.PlanogramHierarchyId = hierarchyDto.Id;
                                groupDto.Name = String.Format("Group {0}", groupNumber);
                                planogramGroupDal.Insert(groupDto);
                                groupNumber++;

                                //Create group planogram
                                PlanogramGroupPlanogramDto groupPlanogramDto = new PlanogramGroupPlanogramDto();
                                groupPlanogramDto.PlanogramId = Convert.ToInt32(planDto.Id);
                                groupPlanogramDto.PlanogramGroupId = groupDto.Id;

                                //Add to list
                                groupPlanogramDtoList.Add(groupPlanogramDto);
                            }

                            //Call upsert
                            planogramGroupPlanogramDal.Upsert(groupPlanogramDtoList);
                        }
                    }


                    using (var dal = dalContext.GetDal<IPlanogramInfoDal>())
                    {
                        for (Int32 i = 0; i < groupPlanogramDtoList.Count; i++)
                        {
                            IEnumerable<PlanogramInfoDto> returnedDtoList = dal.FetchByPlanogramGroupId(Convert.ToInt32(groupPlanogramDtoList[i].PlanogramGroupId));

                            //Only 1 should exist
                            Assert.AreEqual(1, returnedDtoList.Count());
                            //Find corresponding
                            PlanogramInfoDto returnedDto = returnedDtoList.FirstOrDefault();
                            Assert.IsNotNull(returnedDto, "Dto should have been found");
                            //Check properties
                            base.IsInfoDtoEqualToDto<PlanogramDto>(returnedDto, _testDtos[i]);

                            //Now test assigned location count is correctly calculated
                            for (Int16 assignedLocationCount = 1; assignedLocationCount == 5; assignedLocationCount++)
                            {
                                foreach (PlanogramDto sourceDto in _testDtos)
                                {
                                    InsertPlanAssignmentDto(Convert.ToInt32(sourceDto.Id), sourceDto.Name, locationDtos.First().Id, productGroups.First().Id, dalContext);

                                    IEnumerable<PlanogramInfoDto> updatedDtoList = dal.FetchByPlanogramGroupId(Convert.ToInt32(groupPlanogramDtoList[i].PlanogramGroupId));

                                    Boolean isAssignedLocationsCountCorrect = updatedDtoList.First(p => p.Id.Equals(sourceDto.Id)).AssignedLocationCount == assignedLocationCount;

                                    Assert.IsTrue(isAssignedLocationsCountCorrect, "The value for AssignedLocationCount does not appear to be the correct value.");
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageIdPagingCriteria(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext, 207, 1);

                    List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, 1);
                    List<ProductHierarchyDto> hierrarchies = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                    List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchies.First().Id, 1);
                    List<ProductGroupDto> productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, levels, 1);


                    //Add parent data
                    EntityDto entityDto = Helpers.TestDataHelper.InsertEntityDtos(dalFactory, 1).First();

                    //Insert workflow
                    WorkflowDto workflowDto = new WorkflowDto();
                    using (var workflowDal = dalContext.GetDal<IWorkflowDal>())
                    {
                        workflowDto.EntityId = entityDto.Id;
                        workflowDto.Name = "Workflow";
                        workflowDal.Insert(workflowDto);
                    }

                    //Add workpage planograms
                    List<WorkpackageDto> workpackageDtoList = new List<WorkpackageDto>();
                    using (var workpackageDal = dalContext.GetDal<IWorkpackageDal>())
                    {
                        using (var workpackagePlanogramDal = dalContext.GetDal<IWorkpackagePlanogramDal>())
                        {
                            //Create workpage
                            WorkpackageDto workpackageDto = new WorkpackageDto();
                            workpackageDto.WorkflowId = workflowDto.Id;
                            workpackageDto.Name = "Workpackage";
                            workpackageDto.EntityId = entityDto.Id;
                            workpackageDto.UserId = _userDto.Id;
                            workpackageDal.Insert(workpackageDto);

                            //For each generated planogram dto
                            foreach (PlanogramDto planDto in _testDtos)
                            {
                                //Create workpage planogram
                                WorkpackagePlanogramDto workpackagePlanogramDto = new WorkpackagePlanogramDto();
                                workpackagePlanogramDto.Name = "Package";
                                workpackagePlanogramDto.DestinationPlanogramId = Convert.ToInt32(planDto.Id);
                                workpackagePlanogramDto.WorkpackageId = workpackageDto.Id;

                                //Insert
                                workpackagePlanogramDal.Insert(workpackagePlanogramDto);
                            }

                            //Add to list
                            workpackageDtoList.Add(workpackageDto);
                        }
                    }


                    using (var dal = dalContext.GetDal<IPlanogramInfoDal>())
                    {
                        for (Int32 i = 0; i < workpackageDtoList.Count; i++)
                        {
                            Byte pageNumber = 1;

                            for (Int32 j = 0; j < _testDtos.Count; j += 100)
                            {
                                IEnumerable<PlanogramInfoDto> returnedDtoList = dal.FetchByWorkpackageIdPagingCriteria(Convert.ToInt32(workpackageDtoList[i].Id), pageNumber, 100);

                                Int32 expectedPlanCount = _testDtos.Count > (pageNumber * 100) ? 100 : _testDtos.Count - ((pageNumber - 1) * 100);
                                Assert.AreEqual(expectedPlanCount, returnedDtoList.Count());

                                pageNumber++;

                                //Now test assigned location count is correctly calculated
                                for (Int16 assignedLocationCount = 1; assignedLocationCount == 5; assignedLocationCount++)
                                {
                                    foreach (PlanogramInfoDto sourceDto in returnedDtoList)
                                    {
                                        InsertPlanAssignmentDto(Convert.ToInt32(sourceDto.Id), sourceDto.Name, locationDtos.First().Id, productGroups.First().Id, dalContext);
                                    }


                                    IEnumerable<PlanogramInfoDto> returnedDtoListAfterLocationPlanAssignmentUpdate = dal.FetchByWorkpackageIdPagingCriteria(Convert.ToInt32(workpackageDtoList[i].Id), pageNumber, 100);

                                    foreach (var item in returnedDtoListAfterLocationPlanAssignmentUpdate)
                                    {
                                        Boolean isAssignedLocationsCountCorrect = item.AssignedLocationCount == assignedLocationCount;
                                        Assert.IsTrue(isAssignedLocationsCountCorrect, "The value for AssignedLocationCount does not appear to be the correct value.");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageId(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);
                    List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, 1);
                    List<ProductHierarchyDto> hierrarchies = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                    List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchies.First().Id, 1);
                    List<ProductGroupDto> productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, levels, 1);


                    //Add parent data
                    EntityDto entityDto = Helpers.TestDataHelper.InsertEntityDtos(dalFactory, 1).First();

                    //Insert workflow
                    WorkflowDto workflowDto = new WorkflowDto();
                    using (var workflowDal = dalContext.GetDal<IWorkflowDal>())
                    {
                        workflowDto.EntityId = entityDto.Id;
                        workflowDto.Name = "Workflow";
                        workflowDal.Insert(workflowDto);
                    }

                    //Add workpage planograms
                    List<WorkpackagePlanogramDto> workpackagePlanogramDtoList = new List<WorkpackagePlanogramDto>();
                    using (var workpackageDal = dalContext.GetDal<IWorkpackageDal>())
                    {
                        using (var workpackagePlanogramDal = dalContext.GetDal<IWorkpackagePlanogramDal>())
                        {
                            //For each generated planogram dto
                            foreach (PlanogramDto planDto in _testDtos)
                            {
                                //Create workpage
                                WorkpackageDto workpackageDto = new WorkpackageDto();
                                workpackageDto.WorkflowId = workflowDto.Id;
                                workpackageDto.Name = "Workpackage";
                                workpackageDto.EntityId = entityDto.Id;
                                workpackageDto.UserId = _userDto.Id;
                                workpackageDal.Insert(workpackageDto);

                                //Create workpage planogram
                                WorkpackagePlanogramDto workpackagePlanogramDto = new WorkpackagePlanogramDto();
                                workpackagePlanogramDto.Name = "Package";
                                workpackagePlanogramDto.DestinationPlanogramId = Convert.ToInt32(planDto.Id);
                                workpackagePlanogramDto.WorkpackageId = workpackageDto.Id;

                                //Insert
                                workpackagePlanogramDal.Insert(workpackagePlanogramDto);

                                //Add to list
                                workpackagePlanogramDtoList.Add(workpackagePlanogramDto);
                            }
                        }
                    }


                    using (var dal = dalContext.GetDal<IPlanogramInfoDal>())
                    {
                        for (Int32 i = 0; i < workpackagePlanogramDtoList.Count; i++)
                        {
                            IEnumerable<PlanogramInfoDto> returnedDtoList = dal.FetchByWorkpackageId(Convert.ToInt32(workpackagePlanogramDtoList[i].WorkpackageId));

                            //Only 1 should exist
                            Assert.AreEqual(1, returnedDtoList.Count());
                            //Find corresponding
                            PlanogramInfoDto returnedDto = returnedDtoList.FirstOrDefault();
                            Assert.IsNotNull(returnedDto, "Dto should have been found");
                            //Check properties
                            base.IsInfoDtoEqualToDto<PlanogramDto>(returnedDto, _testDtos[i]);

                            //Now test assigned location count is correctly calculated
                            for (Int16 assignedLocationCount = 1; assignedLocationCount == 5; assignedLocationCount++)
                            {
                                foreach (PlanogramDto sourceDto in _testDtos)
                                {
                                    InsertPlanAssignmentDto(Convert.ToInt32(sourceDto.Id), sourceDto.Name, locationDtos.First().Id, productGroups.First().Id, dalContext);

                                    IEnumerable<PlanogramInfoDto> updatedDtoList = dal.FetchByWorkpackageId(Convert.ToInt32(workpackagePlanogramDtoList[i].WorkpackageId));


                                    Boolean isAssignedLocationsCountCorrect = updatedDtoList.First(p => p.Id.Equals(sourceDto.Id)).AssignedLocationCount == assignedLocationCount;

                                    Assert.IsTrue(isAssignedLocationsCountCorrect, "The value for AssignedLocationCount does not appear to be the correct value.");
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchBySearchCriteria(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByIds(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, 1);
                    List<ProductHierarchyDto> hierrarchies = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                    List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchies.First().Id, 1);
                    List<ProductGroupDto> productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, levels, 1);


                    using (var dal = dalContext.GetDal<IPlanogramInfoDal>())
                    {
                        IEnumerable<PlanogramInfoDto> returnedDtoList = dal.FetchByIds(_testDtos.Select(p => Convert.ToInt32(p.Id)));
                        Assert.AreEqual(_testDtos.Count, returnedDtoList.Count());
                        foreach (PlanogramDto sourceDto in _testDtos)
                        {
                            //Find corresponding
                            PlanogramInfoDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(Convert.ToInt32(sourceDto.Id)));
                            Assert.IsNotNull(returnedDto, "Dto should have been found");
                            //Check properties
                            base.IsInfoDtoEqualToDto<PlanogramDto>(returnedDto, sourceDto);
                        }

                        //Now test assigned location count is correctly calculated
                        for (Int16 assignedLocationCount = 1; assignedLocationCount == 5; assignedLocationCount++)
                        {
                            foreach (PlanogramDto sourceDto in _testDtos)
                            {
                                InsertPlanAssignmentDto(Convert.ToInt32(sourceDto.Id), sourceDto.Name, locationDtos.First().Id, productGroups.First().Id, dalContext);

                                IEnumerable<PlanogramInfoDto> updatedDtoList = dal.FetchByIds(_testDtos.Select(p => Convert.ToInt32(p.Id)));

                                Boolean isAssignedLocationsCountCorrect = updatedDtoList.First(p => p.Id.Equals(sourceDto.Id)).AssignedLocationCount == assignedLocationCount;

                                Assert.IsTrue(isAssignedLocationsCountCorrect, "The value for AssignedLocationCount does not appear to be the correct value.");
                            }
                        }

                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByCategoryCode(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchNonDebugByCategoryCode(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchNonDebugByLocationCode(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByNullCategoryCode(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByMetadataCalculationRequired(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByValidationCalculationRequired(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageIdAutomationProcessingStatus(Type dalFactoryType)
        {
            const byte automationProcessingStatus = (Byte)ProcessingStatus.Complete;
            List<PlanogramInfoDto> actual;
            List<PlanogramDto> expected;
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            {
                var userDtos = TestDataHelper.InsertUserDtos(dalContext, 1);
                var userName = userDtos.First().UserName;
                var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
                var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 25);
                var others = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos.Skip(10).ToList());
                var testWorkPackagePlans = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos.Take(10).ToList()).ToList();
                TestDataHelper.InsertPlanogramCustomAttributeDataDtos(dalContext, testWorkPackagePlans);
                expected = new List<PlanogramDto> { testWorkPackagePlans.First(), testWorkPackagePlans.Last() };

                // Create the workpackages.
                var entityDtos = TestDataHelper.InsertEntityDtos(dalContext, 1);
                var workflowDtos = TestDataHelper.InsertWorkflowDtos(dalContext, entityDtos, 2);
                var workPackageDtos = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtos, userDtos);
                TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext,
                    workPackageDtos.Where(dto => dto.Id == workPackageDtos.First().Id).ToList(), others);
                var workpackageId = workPackageDtos.Last().Id;
                TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext,
                    workPackageDtos.Where(dto => dto.Id == workpackageId).ToList(), testWorkPackagePlans);

                using (var dal = dalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                {
                    dalContext.Begin();
                    var processingStatusDtos = expected.Select(planogramDto => dal.FetchByPlanogramIdWorkpackageId(Convert.ToInt32(planogramDto.Id), workpackageId)).ToList();
                    foreach (var processingStatusDto in processingStatusDtos)
                    {
                        processingStatusDto.AutomationStatus = automationProcessingStatus;
                        dal.Update(processingStatusDto);
                    }
                    dalContext.Commit();
                }

                using (var dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    actual = dal.FetchByWorkpackageIdAutomationProcessingStatus(workpackageId, automationProcessingStatus).ToList();
                }

                CollectionAssert.AreEqual(expected.Select(dto => dto.Name).ToList(), actual.Select(dto => dto.Name).ToList());

                //Now test assigned location count is correctly calculated
                List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, 1);
                List<ProductHierarchyDto> hierrarchies = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchies.First().Id, 1);
                List<ProductGroupDto> productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, levels, 1);


                using (var dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    for (Int16 assignedLocationCount = 1; assignedLocationCount == 5; assignedLocationCount++)
                    {
                        foreach (PlanogramInfoDto sourceDto in actual)
                        {
                            InsertPlanAssignmentDto(Convert.ToInt32(sourceDto.Id), sourceDto.Name, locationDtos.First().Id, productGroups.First().Id, dalContext);

                            List<PlanogramInfoDto> actualUpdated = dal.FetchByWorkpackageIdAutomationProcessingStatus(workpackageId, automationProcessingStatus).ToList();

                            Boolean isAssignedLocationsCountCorrect = actualUpdated.First(p => p.Id.Equals(sourceDto.Id)).AssignedLocationCount == assignedLocationCount;

                            Assert.IsTrue(isAssignedLocationsCountCorrect, "The value for AssignedLocationCount does not appear to be the correct value.");
                        }
                    }
                }
            }


        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(Type dalFactoryType)
        {
            const byte automationProcessingStatus = (Byte)ProcessingStatus.Complete;
            List<PlanogramInfoDto> actual;
            List<PlanogramDto> expected;
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            {
                var userDtos = TestDataHelper.InsertUserDtos(dalContext, 1);
                var userName = userDtos.First().UserName;
                var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
                var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 25);
                var others = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos.Skip(10).ToList());
                var testWorkPackagePlans = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos.Take(10).ToList()).ToList();
                TestDataHelper.InsertPlanogramCustomAttributeDataDtos(dalContext, testWorkPackagePlans);
                expected = new List<PlanogramDto> { testWorkPackagePlans.First(), testWorkPackagePlans.Last() };

                List<LocationDto> locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, 1);
                List<ProductHierarchyDto> hierrarchies = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, 1);
                List<ProductLevelDto> levels = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchies.First().Id, 1);
                List<ProductGroupDto> productGroups = TestDataHelper.InsertProductGroupDtos(dalFactory, levels, 1);


                // Create the workpackages.
                var entityDtos = TestDataHelper.InsertEntityDtos(dalContext, 1);
                var workflowDtos = TestDataHelper.InsertWorkflowDtos(dalContext, entityDtos, 2);
                var workPackageDtos = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtos, userDtos);
                TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext,
                    workPackageDtos.Where(dto => dto.Id == workPackageDtos.First().Id).ToList(), others);
                var workpackageId = workPackageDtos.Last().Id;
                TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext,
                    workPackageDtos.Where(dto => dto.Id == workpackageId).ToList(), testWorkPackagePlans);

                using (var dal = dalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                {
                    dalContext.Begin();
                    var processingStatusDtos = expected.Select(planogramDto => dal.FetchByPlanogramIdWorkpackageId(Convert.ToInt32(planogramDto.Id), workpackageId)).ToList();
                    foreach (var processingStatusDto in processingStatusDtos)
                    {
                        processingStatusDto.AutomationStatus = automationProcessingStatus;
                        dal.Update(processingStatusDto);
                    }
                    dalContext.Commit();
                }

                using (var dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    for (Byte i = 1; i <= expected.Count; i++)
                    {
                        actual = dal.FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(workpackageId, new List<Byte>{automationProcessingStatus}, i, 1).ToList();
                        Assert.AreEqual(expected[i-1].Id, actual[0].Id);


                        //Now test assigned location count is correctly calculated
                        for (Int16 assignedLocationCount = 1; assignedLocationCount == 5; assignedLocationCount++)
                        {
                            foreach (PlanogramDto sourceDto in _testDtos)
                            {
                                InsertPlanAssignmentDto(Convert.ToInt32(sourceDto.Id), sourceDto.Name, locationDtos.First().Id, productGroups.First().Id, dalContext);

                                IEnumerable<PlanogramInfoDto> updatedDtoList = dal.FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(workpackageId, new List<Byte> { automationProcessingStatus }, i, 1).ToList();

                                Boolean isAssignedLocationsCountCorrect = updatedDtoList.First(p => p.Id.Equals(sourceDto.Id)).AssignedLocationCount == assignedLocationCount;

                                Assert.IsTrue(isAssignedLocationsCountCorrect, "The value for AssignedLocationCount does not appear to be the correct value.");
                            }
                        }
                    }
                }
            }
        }
        #region HelperMethods

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            InitializeDtosAndProperties(dalContext, 25, 1);
        }

        private void InitializeDtosAndProperties(IDalContext dalContext, Int32 packageCount, Int32 planogramCount)
        {
            _userDto = TestDataHelper.InsertUserDtos(dalContext, 1).First();
            var userName = _userDto.UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            var packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, packageCount);
            _testDtos = TestDataHelper.InsertPlanogramDtos(dalContext, userName, planogramCount, packageDtoList);
            TestDataHelper.InsertPlanogramCustomAttributeDataDtos(dalContext, _testDtos);
        }

        private void InsertPlanAssignmentDto(Int32 planId, String planName, Int16 locationId, Int32 productGroupId, IDalContext dalContext)
        {
            using (var dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
            {
                var locationPlanAssignmentDto = new LocationPlanAssignmentDto()
                {
                    LocationId = locationId,
                    PlanogramId = (Int32)planId,
                    PlanogramName = planName,
                    ProductGroupId = productGroupId
                };

                dal.Insert(locationPlanAssignmentDto);
            }
        }

        #endregion
    }
}
