﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26706 : L.Luong 
//   Created (Auto-generated)
// V8-26322 : A.Silva
//      Amended how User records are inserted to avoid random duplicates.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramConsumerDecisionTreeNodeDal : TestBase<IPlanogramConsumerDecisionTreeNodeDal, PlanogramConsumerDecisionTreeNodeDto>
    {
        #region Fields

        private readonly List<String> _excludeProperties = new List<String> 
        {
            "ExtendedData",
        };
        private List<PlanogramConsumerDecisionTreeNodeDto> _sourceDtoList;
        private List<PlanogramDto> _parentPlanogramDtoList;
        private List<PropertyInfo> _properties;
        #endregion

        #region Constructor

        public PlanogramConsumerDecisionTreeNodeDal()
            : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramConsumerDecisionTreeNodeDto));
            _properties = typeof(PlanogramConsumerDecisionTreeNodeDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 25);
            _parentPlanogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
            var parentPlanogramConsumerDecisionTree = TestDataHelper.InsertPlanogramConsumerDecisionTreeDtos(dalContext, _parentPlanogramDtoList)[0];
            var parentPlanogramConsumerDecisionTreeLevelList = TestDataHelper.InsertPlanogramConsumerDecisionTreeLevelDtos(dalContext, parentPlanogramConsumerDecisionTree, 3, true);
            _sourceDtoList = TestDataHelper.InsertPlanogramConsumerDecisionTreeNodeDtos(dalContext, parentPlanogramConsumerDecisionTree, parentPlanogramConsumerDecisionTreeLevelList, 5, /*addRoot*/true);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPlanogramConsumerDecisionTreeId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType,
                "FetchByPlanogramConsumerDecisionTreeId", "PlanogramConsumerDecisionTreeId", _excludeProperties);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludeProperties);
        }


        [Test]
        public void TestBulkInsert()
        {
            Assert.Inconclusive("Testing of bulk inserts of DTOs with self referencing properties is not supported by the dal test base");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
            {
                InitializeDtosAndProperties(dalContext);
                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                {
                    dal.DeleteById(_sourceDtoList[i].Id);
                    var returnedDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(_sourceDtoList[i].PlanogramConsumerDecisionTreeId);
                    var returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                    Assert.IsNull(returnedDto, "Dto should be deleted");
                }
            }
        }
        
    }

    //[TestFixture]
    //public sealed class PlanogramConsumerDecisionTreeNodeDal : TestBase<IPlanogramConsumerDecisionTreeNodeDal, PlanogramConsumerDecisionTreeNodeDto>
    //{
    //    #region Fields

    //    private List<Type> _dbDalFactoryTypes = new List<Type>()
    //    {
    //        typeof(Galleria.Ccm.Dal.Mssql.DalFactory)
    //    };
    //    private List<String> _excludeProperties = new List<String>() { "ExtendedData" };
    //    private List<PlanogramConsumerDecisionTreeNodeDto> _sourceDtoList;
    //    private List<PlanogramDto> _parentPlanogramDtoList;
    //    private new DtoSpec _dtoSpec;
    //    private List<PropertyInfo> _properties;

    //    #endregion

    //    private void InitializeDtosAndProperties(IDalContext dalContext)
    //    {
    //        _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramConsumerDecisionTreeNodeDto));
    //        _properties = typeof(PlanogramConsumerDecisionTreeNodeDto).GetProperties().Where(p => p.CanWrite).ToList();

    //        //Insert parents
    //        List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, 25);
    //        _parentPlanogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, 1, packageDtoList);
    //        var parentPlanogramConsumerDecisionTree = TestDataHelper.InsertPlanogramConsumerDecisionTreeDtos(dalContext, _parentPlanogramDtoList)[0];
    //        var parentPlanogramConsumerDecisionTreeLevelList = TestDataHelper.InsertPlanogramConsumerDecisionTreeLevelDtos(dalContext, parentPlanogramConsumerDecisionTree, 3, true);
    //        _sourceDtoList = TestDataHelper.InsertPlanogramConsumerDecisionTreeNodeDtos(dalContext, parentPlanogramConsumerDecisionTree, parentPlanogramConsumerDecisionTreeLevelList, 5, /*addRoot*/true);
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public void TestFetchByPlanogramConsumerDecisionTreeId(Type dbDalFactoryType)
    //    {
    //        using (var dalFactory = CreateDalFactory(dbDalFactoryType))
    //        using (var dalContext = dalFactory.CreateContext())
    //        using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
    //        {
    //            InitializeDtosAndProperties(dalContext);
    //            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
    //            {
    //                var returnedDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(_sourceDtoList[i].PlanogramConsumerDecisionTreeId);
    //                var returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
    //                foreach (var property in _properties)
    //                {
    //                    if (!_excludeProperties.Contains(property.Name))
    //                    {
    //                        Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public void TestInsert(Type dbDalFactoryType)
    //    {
    //        // The Insert method is tested by TestFetchByPlanogramConsumerDecisionTreeId.
    //        TestFetchByPlanogramConsumerDecisionTreeId(dbDalFactoryType);
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public void TestUpdate(Type dbDalFactoryType)
    //    {
    //        using (var dalFactory = CreateDalFactory(dbDalFactoryType))
    //        using (var dalContext = dalFactory.CreateContext())
    //        using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
    //        {
    //            InitializeDtosAndProperties(dalContext);
    //            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
    //            {
    //                var originalSourceDto = _sourceDtoList[i];
    //                var alternativeSourceDto = _sourceDtoList[i];

    //                // Update the DTO in memory.
    //                foreach (var property in _properties.Where(
    //                    p => p.Name != "Id" && p.Name != "PlanogramConsumerDecisionTreeId" && !_excludeProperties.Contains(p.Name)))
    //                {
    //                    property.SetValue(
    //                        originalSourceDto,
    //                        property.GetValue(alternativeSourceDto, null),
    //                        null);
    //                }

    //                // Update the DTO in database and verify success.
    //                dal.Update(originalSourceDto);
    //                var returnedDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(originalSourceDto.PlanogramConsumerDecisionTreeId);
    //                var returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(originalSourceDto.Id));

    //                foreach (var property in _properties.Where(p => !_excludeProperties.Contains(p.Name)))
    //                {
    //                    Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
    //                }
    //            }
    //        }
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public new void TestDeleteById(Type dbDalFactoryType)
    //    {
    //        using (var dalFactory = CreateDalFactory(dbDalFactoryType))
    //        using (var dalContext = dalFactory.CreateContext())
    //        using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
    //        {
    //            InitializeDtosAndProperties(dalContext);
    //            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
    //            {
    //                dal.DeleteById(_sourceDtoList[i].Id);
    //                var returnedDtoList = dal.FetchByPlanogramConsumerDecisionTreeId(_sourceDtoList[i].PlanogramConsumerDecisionTreeId);
    //                var returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
    //                Assert.IsNull(returnedDto, "Dto should be deleted");
    //            }
    //        }
    //    }
    //}
}
