﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25450 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductGroupDal : TestBase<IProductGroupDal, ProductGroupDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByProductHierarchyId(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByProductHierarchyId", "ProductHierarchyId");
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByProductHierarchyIdIncludingDeleted(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByProductHierarchyIdIncludingDeleted", "ProductHierarchyId", null, null, true);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, null, false);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestUpsert(Type dalFactoryType)
        {
            base.TestUpsert<ProductGroupIsSetDto>(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }

        #region Ticket Related Tests

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM14222_DeletingMiddleLevelMovesChildGroups(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 2 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<ProductHierarchyDto> hierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityList[0].Id);

                //insert a root level and 2 child levels
                ProductLevelDto rootLevel, childLevel1, childLevel2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                    {
                        rootLevel = new ProductLevelDto()
                        {
                            Name = "root",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new ProductLevelDto()
                        {
                            Name = "level1",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new ProductLevelDto()
                        {
                            Name = "level2",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);
                    }
                    dalContext.Commit();
                }

                //insert group per level
                ProductGroupDto rootGroup, group1, group2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        rootGroup = new ProductGroupDto()
                        {
                            Name = "root",
                            Code = "rootCode",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ProductLevelId = rootLevel.Id,
                            ParentGroupId = null,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootGroup);

                        group1 = new ProductGroupDto()
                        {
                            Name = "group1",
                            Code = "group1Code",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ProductLevelId = childLevel1.Id,
                            ParentGroupId = rootGroup.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group1);

                        group2 = new ProductGroupDto()
                        {
                            Name = "group2",
                            Code = "group2Code",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ProductLevelId = childLevel2.Id,
                            ParentGroupId = group1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group2);
                    }
                    dalContext.Commit();
                }

                //Perform actions as if we have removed the middle level - 
                //need to do this in the same order as the framework would
                //so delete the level, update other levels then
                //delete the group, update other groups

                //remove the middle level and update the root and third levels
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(childLevel1.Id);

                        //set level 2 to now be the direct child of the root
                        childLevel2.ParentLevelId = rootLevel.Id;

                        dal.Update(rootLevel);
                        dal.Update(childLevel2);

                    }
                    dalContext.Commit();
                }

                //remove the middle group and update the other groups
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(group1.Id);

                        //set group 2 to now be the direct child of the root
                        group2.ParentGroupId = rootGroup.Id;

                        dal.Update(rootGroup);
                        dal.Update(group2);

                    }
                    dalContext.Commit();
                }


                //try to retrieve group 1 again and check the dtodoesnotexist is thrown
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        group1 = dal.FetchById(group1.Id);
                        Assert.IsNotNull(group1.DateDeleted, "Item should have been deleted");

                        group2 = dal.FetchById(group2.Id);
                        rootGroup = dal.FetchById(rootGroup.Id);
                    }
                }

                //check the group 2 is now the direct child of the rootGroup
                Assert.AreEqual(rootGroup.Id, group2.ParentGroupId);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM14294_DeleteBottom2LevelsAtOnce(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 3 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<ProductHierarchyDto> hierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityList[0].Id);

                //insert a root level and 3 child levels
                ProductLevelDto rootLevel, childLevel1, childLevel2, childLevel3;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                    {
                        rootLevel = new ProductLevelDto()
                        {
                            Name = "root",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new ProductLevelDto()
                        {
                            Name = "level1",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new ProductLevelDto()
                        {
                            Name = "level2",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);

                        childLevel3 = new ProductLevelDto()
                        {
                            Name = "level3",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel2.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel3);
                    }
                    dalContext.Commit();
                }

                //insert group per level
                ProductGroupDto rootGroup, group1, group2, group3;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        rootGroup = new ProductGroupDto()
                        {
                            Name = "root",
                            Code = "rootCode",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ProductLevelId = rootLevel.Id,
                            ParentGroupId = null,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootGroup);

                        group1 = new ProductGroupDto()
                        {
                            Name = "group1",
                            Code = "group1Code",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ProductLevelId = childLevel1.Id,
                            ParentGroupId = rootGroup.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group1);

                        group2 = new ProductGroupDto()
                        {
                            Name = "group2",
                            Code = "group2Code",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ProductLevelId = childLevel2.Id,
                            ParentGroupId = group1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group2);

                        group3 = new ProductGroupDto()
                        {
                            Name = "group3",
                            Code = "group3Code",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ProductLevelId = childLevel3.Id,
                            ParentGroupId = group2.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group3);
                    }
                    dalContext.Commit();
                }

                //Perform actions as if we have removed the middle level - 
                //need to do this in the same order as the framework would
                //so delete the level, update other levels then
                //delete the group, update other groups

                //remove the bottom 2 levels
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(childLevel3.Id);
                        dal.DeleteById(childLevel2.Id);

                        dal.Update(rootLevel);
                        dal.Update(childLevel1);

                    }
                    dalContext.Commit();
                }

                //remove the bottom 2 groups
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(group3.Id);
                        dal.DeleteById(group2.Id);

                        dal.Update(rootGroup);
                        dal.Update(group1);

                    }
                    dalContext.Commit();
                }


                //try to retrieve group 2 & group 3 again and check the dtodoesnotexist is thrown
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        group2 = dal.FetchById(group2.Id);
                        Assert.IsNotNull(group2.DateDeleted, "Item should have been deleted");

                        group3 = dal.FetchById(group3.Id);
                        Assert.IsNotNull(group3.DateDeleted, "Item should have been deleted");

                        group1 = dal.FetchById(group1.Id);
                        rootGroup = dal.FetchById(rootGroup.Id);
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM23377_DeletingGroupDoesNotNullOffChildGroups(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 2 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<ProductHierarchyDto> hierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityList[0].Id);


                //inert a root level and 2 child levels
                ProductLevelDto rootLevel, childLevel1, childLevel2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                    {
                        rootLevel = new ProductLevelDto()
                        {
                            Name = "root",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new ProductLevelDto()
                        {
                            Name = "level1",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new ProductLevelDto()
                        {
                            Name = "level2",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);
                    }
                    dalContext.Commit();
                }

                //add a group per level
                ProductGroupDto rootGroupDto, group2Dto, group3Dto;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        rootGroupDto = new ProductGroupDto()
                        {
                            Code = "0",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            Name = "root",
                            ProductLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootGroupDto);

                        group2Dto = new ProductGroupDto()
                        {
                            Code = "1",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            Name = "group2",
                            ProductLevelId = childLevel1.Id,
                            ParentGroupId = rootGroupDto.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group2Dto);

                        group3Dto = new ProductGroupDto()
                        {
                            Code = "2",
                            ProductHierarchyId = hierarchyDtoList[0].Id,
                            Name = "group3",
                            ProductLevelId = childLevel2.Id,
                            ParentGroupId = group2Dto.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group3Dto);
                    }
                    dalContext.Commit();
                }


                //delete group3 the group 2
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        dal.DeleteById(group3Dto.Id);
                        dal.DeleteById(group2Dto.Id);
                    }
                    dalContext.Commit();
                }

                //confirm that group3 is still linked to group 2
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        ProductGroupDto dalDto = dal.FetchById(group3Dto.Id);
                        Assert.AreEqual(dalDto.ParentGroupId, group2Dto.Id);
                    }
                    dalContext.Commit();
                }
            }
        }

        #endregion

    }
}
