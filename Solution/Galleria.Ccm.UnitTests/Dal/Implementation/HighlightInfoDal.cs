﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
   public sealed class HighlightInfoDal: TestBase<IHighlightInfoDal, HighlightInfoDto>

    {

        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.User.DalFactory)
        };

       [Test, TestCaseSource("_dbDalFactoryTypes")]
       public void TestFetchByIds(Type dbDalFactoryType)
       {
           if (dbDalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
           {
               using (var dalFactory = CreateDalFactory(dbDalFactoryType))
               using (var dalContext = dalFactory.CreateContext())
               using (var dal = dalContext.GetDal<IHighlightInfoDal>())
               {
                   var dto = CreateDtos<HighlightDto>(dalContext);
                   var list = dto.Dtos;

                   List<Object> ids = new List<Object>();

                   foreach (var id in list)
                   {
                       ids.Add(id.Id);
                   }

                   IEnumerable<HighlightInfoDto> highlightInfos = dal.FetchByIds(ids);

                   foreach (HighlightInfoDto highlightInfo in highlightInfos)
                   {
                       var highlight = list.Where(l => Object.Equals(l.Id, highlightInfo.Id));
                       Assert.IsNotNull(highlight);
                   }
               }
           }
           else
           {
               base.TestInfoFetchByProperties<HighlightDto, Object>(
                   dbDalFactoryType,
                   "FetchByIds",
                   new List<String> { },
                   "Id",
                   /*includesDeleted*/true);
           }
       }
    }
}
