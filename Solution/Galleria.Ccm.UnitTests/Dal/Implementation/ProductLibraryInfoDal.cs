﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25395 : A.Probyn
//		Created
#endregion
#endregion


using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductLibraryInfoDal : TestBase<IProductLibraryInfoDal, ProductLibraryInfoDto>
    {
        private List<String> _excludeProperties = new List<String> { /*"Id"*/ };

        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.User.DalFactory);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByIds(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("Todo"); 
            }

            base.TestInfoFetchByProperties<ProductLibraryDto, Object>(
                dalFactoryType,
                "FetchByIds",
                new List<String> { },
                "Id");
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
            {
                Assert.Pass(); //nothing to test
            }

            base.TestInfoFetchByEntityId<ProductLibraryDto>(dalFactoryType);
        }
    }
}
