﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25669 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductInfoDal :TestBase<IProductInfoDal,ProductInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdSearchCriteria(Type dbDalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                //insert test data (3 products, 2 locations);
                List<ProductDto> searchResultDtoList = InsertSearchTestData(dalFactory);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    List<ProductInfoDto> resultList;
                    using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                    {
                        Int32 entityId = searchResultDtoList.First().EntityId;
                        ProductDto prod1Result = searchResultDtoList[0];//"Glade glass candle red"
                        ProductDto prod2Result = searchResultDtoList[1];//"Glade candle blue"
                        ProductDto prod3Result = searchResultDtoList[2];//"Glade candle GLASS yellow"
                        ProductDto prod4Result = searchResultDtoList[3];//"Domestos Original green"

                        //check single term search
                        //search: glade, result 3 products
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glade").ToList();
                        Assert.AreEqual(3, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod2Result.Id), prod2Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //check multiple or search
                        //search: glade or glass, result 3 products 1 location
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glade or glass").ToList();
                        Assert.AreEqual(3, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod2Result.Id), prod2Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //check multiple and
                        //search: glade and glass, result  prod0 prod1
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glade and glass").ToList();
                        Assert.AreEqual(2, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //check multiple spaced
                        //search: glade glass, result  prod0 prod1
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glade glass").ToList();
                        Assert.AreEqual(2, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //check multiple spaced with search terms reversed
                        //search: glass glade, result  prod0 prod1
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glass glade").ToList();
                        Assert.AreEqual(2, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //(GFS-17047) check search OR at the very end of the search criteria
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "Domestos OR ").ToList();
                        Assert.AreEqual(1, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod4Result.Id), prod4Result);

                        //(GFS-17047) check search with AND at the very end of the search criteria
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "yellow AND ").ToList();
                        Assert.AreEqual(1, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //(GFS-17553) Check search using a special character
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glass or @").ToList();
                        Assert.AreEqual(2, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //(GFS-17553) Check with duplicated ands
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glade and and").ToList();
                        Assert.AreEqual(3, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod2Result.Id), prod2Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);

                        //(GFS-17553) Check with duplicated ors
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "glade or or").ToList();
                        Assert.AreEqual(4, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod1Result.Id), prod1Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod2Result.Id), prod2Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod4Result.Id), prod4Result);

                        //(GFS-17553) Check with multiple criteria
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "DOM estoS Or ignal and green").ToList();
                        Assert.AreEqual(1, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod4Result.Id), prod4Result);

                        //(GFS-17553) Check with or as the start criteria
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "or domestos").ToList();
                        Assert.AreEqual(1, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod4Result.Id), prod4Result);

                        //(GFS-17553) Check with and as the start criteria
                        resultList = dal.FetchByEntityIdSearchCriteria(entityId, "and yellow").ToList();
                        Assert.AreEqual(1, resultList.Count);
                        AssertInfoAndDtoAreEqual(resultList.First(r => r.Id == prod3Result.Id), prod3Result);
                    }
                }
            }    
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<ProductDto>(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdProductGtins(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperties<ProductDto,String>(
                dbDalFactoryType,
                "FetchByEntityIdProductGtins",
                new List<String> { "EntityId"},
                "Gtin");
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductUniverseId(Type dbDalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                // insert some sample dtos into the data source
                List<ProductHierarchyDto> hierarchy1Dtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityList[0].Id);
                List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchy1Dtos[0].Id, 3);
                List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);

                // insert some sample dtos into the data source
                List<ProductDto> sourceDtoList = TestDataHelper.InsertProductDtos(dalFactory, 50, entityList[0].Id);

                //create product unvierses and hook up
                List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(dalFactory, entityList, hierarchy1GroupDtos);
                List<ProductUniverseProductDto> productUniverseProductDtoList = TestDataHelper.InsertProductUniverseProductDtos(dalFactory, productUniverseDtoList, sourceDtoList);

                // fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                    {
                        IEnumerable<IGrouping<Int32, ProductUniverseProductDto>> universeGroupList =
                            productUniverseProductDtoList.GroupBy(prop => prop.ProductUniverseId);

                        foreach (IGrouping<Int32, ProductUniverseProductDto> universeGroup in universeGroupList)
                        {
                            IEnumerable<ProductInfoDto> returnedDtos = dal.FetchByProductUniverseId(universeGroup.Key);
                            Assert.AreEqual(universeGroup.Count(), returnedDtos.Count(), "Count should be the same");

                            foreach (ProductUniverseProductDto sourceDto in universeGroup)
                            {
                                ProductInfoDto foundReturnedDto = returnedDtos.FirstOrDefault(p => p.Id == sourceDto.ProductId);
                                Assert.IsNotNull(foundReturnedDto, "Matching dto should have been fetched");

                                ProductDto productDto = sourceDtoList.First(a => a.Id == foundReturnedDto.Id);
                                AssertInfoAndDtoAreEqual(foundReturnedDto, productDto);
                            }
                        }

                    }
                    dalContext.Commit();
                }
            }
        }
        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByMerchandisingGroupId(Type dbDalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                // insert some sample dtos into the data source
                List<ProductHierarchyDto> hierarchy1Dtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityList[0].Id);
                List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchy1Dtos[0].Id, 3);
                List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);

                // insert some sample dtos into the data source
                List<ProductDto> sourceDtoList = TestDataHelper.InsertProductDtos(dalFactory, 50, entityList[0].Id);

                //create product unvierses and hook up
                List<ProductUniverseDto> productUniverseDtoList = TestDataHelper.InsertProductUniverseDtos(dalFactory, entityList, hierarchy1GroupDtos);
                List<ProductUniverseProductDto> productUniverseProductDtoList = TestDataHelper.InsertProductUniverseProductDtos(dalFactory, productUniverseDtoList, sourceDtoList);

                //fetch each group of dto's from the dal
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                    {
                            IEnumerable<ProductInfoDto> returnedDtos = dal.FetchByMerchandisingGroupId(hierarchy1GroupDtos[0].Id);

                            foreach (ProductUniverseDto sourceDto in productUniverseDtoList)
                            {
                                ProductInfoDto foundReturnedDto = returnedDtos.FirstOrDefault(p => p.Id == sourceDto.ProductGroupId);
                                Assert.IsNotNull(foundReturnedDto, "Matching dto should have been fetched");

                                ProductDto productDto = sourceDtoList.First(a => a.Id == foundReturnedDto.Id);
                                AssertInfoAndDtoAreEqual(foundReturnedDto, productDto);
                            }
                      
                    }
                }
            }
            //base.TestInfoFetchByProperty<ProductDto>(dbDalFactoryType, "FetchByMerchandisingGroupId", "ProductGroupId",/*includesDeleted*/ false);

        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductIds(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperties<ProductDto, Int32>(
                dbDalFactoryType,
                "FetchByProductIds",
                new List<String> { },
                "Id",
                true);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdProductIds(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperties<ProductDto, Int32>(
                dbDalFactoryType,
                "FetchByEntityIdProductIds",
                new List<String> { "EntityId" },
                "Id",
                false);
        }

        #region Private Methods
        
        private void SetPropertyValuesToProduct(ProductDto productDto, List<String> propertiesToExclude)
        {
            var properties = typeof(ProductDto).GetProperties();
            foreach (var property in properties)
            {
                if (propertiesToExclude.Contains(property.Name)) continue;
                if (property.PropertyType == typeof(String))
                {
                    property.SetValue(productDto, property.Name,null);
                }
            }
            productDto.DateCreated = DateTime.Now;
            productDto.DateLastModified = DateTime.Now;
            productDto.DateDeleted = null;
        }

        private List<ProductDto> InsertSearchTestData(IDalFactory dalFactory)
        {
            //insert an entity 
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
            Int32 entityId = entityDtoList[0].Id;

            List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId);
            List<ProductLevelDto> productLevelDtoListFin = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDtoList[0].Id, 2);
            List<ProductGroupDto> productGroupDtoListFin = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevelDtoListFin, 1);


            List<ProductDto> productDtoList = new List<ProductDto>();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                #region Products
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    ProductDto productDto1 = new ProductDto()
                    {
                        EntityId = entityDtoList[0].Id,
                        Gtin = Guid.NewGuid().ToString().Substring(0, 10),
                        Name = "Glade glass candle red"
                    };
                    SetPropertyValuesToProduct(productDto1, new List<String> { "Gtin", "Name" });
                    productDtoList.Add(productDto1);
                    dal.Insert(productDto1);

                    ProductDto productDto2 = new ProductDto()
                    {
                        EntityId = 1,
                        Gtin = Guid.NewGuid().ToString().Substring(0, 10),
                        Name = "Glade candle blue"
                    };
                    SetPropertyValuesToProduct(productDto2, new List<String> { "Gtin", "Name" });
                    productDtoList.Add(productDto2);
                    dal.Insert(productDto2);

                    ProductDto productDto3 = new ProductDto()
                    {
                        EntityId = 1,
                        Gtin = Guid.NewGuid().ToString().Substring(0, 10),
                        Name = "Glade candle GLASS yellow"
                    };
                    SetPropertyValuesToProduct(productDto3, new List<String> { "Gtin", "Name" });
                    productDtoList.Add(productDto3);
                    dal.Insert(productDto3);

                    ProductDto productDto4 = new ProductDto()
                    {
                        EntityId = 1,
                        Gtin = Guid.NewGuid().ToString().Substring(0, 10),
                        Name = "Domestos Original green"
                    };
                    SetPropertyValuesToProduct(productDto4, new List<String> { "Gtin", "Name" });
                    productDtoList.Add(productDto4);
                    dal.Insert(productDto4);
                }

                #endregion

                dalContext.Commit();
            }

            return productDtoList;
        }

        public void AssertInfoAndDtoAreEqual(ProductInfoDto infoDto, ProductDto dto)
        {
            Assert.AreEqual(4, infoDto.GetType().GetProperties().Count(), "Dto property count has changed.");

            Assert.AreEqual(dto.Id, infoDto.Id);
            Assert.AreEqual(dto.EntityId, infoDto.EntityId);
            Assert.AreEqual(dto.Name, infoDto.Name);
            Assert.AreEqual(dto.Gtin, infoDto.Gtin);
        }

        #endregion
    }
}
