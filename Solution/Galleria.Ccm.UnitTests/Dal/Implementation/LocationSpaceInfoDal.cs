﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationSpaceInfoDal : TestBase<ILocationSpaceInfoDal, LocationSpaceInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {

                // insert some sample dtos into the data source
                List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId1 = entityDtoList[0].Id;

                List<ProductHierarchyDto> hierarchy1Dtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId1);
                List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchy1Dtos[0].Id, 3);
                List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);

                List<LocationHierarchyDto> locHierarchy1Dtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtoList);
                List<LocationLevelDto> locHierarchy1levelDtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchy1Dtos[0].Id, 3);
                List<LocationGroupDto> locHierarchy1GroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, locHierarchy1levelDtos, 5);

                List<LocationDto> sourceLocationDtoList1 = TestDataHelper.InsertLocationDtos(dalFactory, entityId1, locHierarchy1GroupDtos.Select(p => p.Id), 5);

                List<LocationSpaceDto> sourceLocationSpaceFirstDtoList = TestDataHelper.InsertLocationSpaceDtos(dalFactory, sourceLocationDtoList1, entityId1);

                List<LocationSpaceDto> sourceLocationSpaceDtoList = sourceLocationSpaceFirstDtoList;

                // fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationSpaceInfoDal dal = dalContext.GetDal<ILocationSpaceInfoDal>())
                    {
                        List<LocationSpaceInfoDto> returnedDtoList = dal.FetchByEntityId(entityId1).ToList();

                        //Check to see if correct number of records has been returned
                        Assert.AreEqual(returnedDtoList.Count,
                            sourceLocationSpaceFirstDtoList.Where(l => l.EntityId == entityId1).Count());
                    }
                    dalContext.Commit();
                }
            }
        }
    }
}
