﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// CCM-25879 : N.Haywood
//		Created (Auto-generated)
// V8-26322 : A.Silva
//      Added PlanogramName to excluded properties. The Planogram record it comes from is not mandatory, and the test base will expect it but not generate it.

#endregion 

#region Version History: CCM 8.0.2
// V8-28986 : M.Shelley
//  Update test coverage to include FetchByLocationId
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    class LocationPlanAssignmentDal : TestBase<ILocationPlanAssignmentDal, LocationPlanAssignmentDto>
    {
        private readonly List<String> _excludedProperties = new List<String> { "PlanogramName" };

        #region Constructor

        public LocationPlanAssignmentDal() : base(typeof(Ccm.Dal.Mock.DalFactory), typeof(Ccm.Dal.Mssql.DalFactory)) { }

        #endregion

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType, _excludedProperties);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludedProperties);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludedProperties);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductGroupId(Type dbDalFactoryType)
        {
            const string fetchMethodName = "FetchByProductGroupId";
            const string propertyName = "ProductGroupId";
            TestFetchByProperty(dbDalFactoryType, fetchMethodName, propertyName, _excludedProperties);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dbDalFactoryType)
        {
            const string fetchMethodName = "FetchByPlanogramId";
            const string propertyName = "PlanogramId";
            TestFetchByProperty(dbDalFactoryType, fetchMethodName, propertyName, _excludedProperties);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByLocationId(Type dbDalFactoryType)
        {
            const string fetchMethodName = "FetchByLocationId";
            const string propertyName = "LocationId";
            TestFetchByProperty(dbDalFactoryType, fetchMethodName, propertyName, _excludedProperties);
        }
    }
}
