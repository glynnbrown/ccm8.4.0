﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion


using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class FixturePackageInfoDal : TestBase<IFixturePackageInfoDal, FixturePackageInfoDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByIds(Type dalFactoryType)
        {
            throw new InconclusiveException("Not sure how to test as requires both dals");
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestSetFolderId(Type dalFactoryType)
        {
            throw new InconclusiveException("Not sure how to test as requires both dals");
        }
    }
}
