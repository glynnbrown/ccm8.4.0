﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25629 : A. Kuszyk
//      Initial version (adapted from SA).
// V8-25632 : K. Pickup
//      Refactored to work with the framework version of the DalImplementationTestBase, which I introduced as part of 
//      the fix for a test in ConsumerDecisionTreeLevelDal.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ConsumerDecisionTreeNodeProductDal : TestBase<IConsumerDecisionTreeNodeProductDal, ConsumerDecisionTreeNodeProductDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {

                    DtoCollection<ConsumerDecisionTreeNodeProductDto> dtos = CreateDtos<ConsumerDecisionTreeNodeProductDto>(dalContext);
                    List<ConsumerDecisionTreeNodeProductDto> sourceDtoList = dtos.Dtos;
                    dtos.InsertDtos(dalContext, _dtoSpec);

                    //remove duplicate products before testing as it was incorrect to add any multiples in the same tree.
                    foreach (var prodGrouping in sourceDtoList.GroupBy(p => p.ProductId))
                    {
                        foreach (ConsumerDecisionTreeNodeProductDto sourceDto in prodGrouping)
                        {
                            if (sourceDto != prodGrouping.Last())
                            {
                                sourceDtoList.Remove(sourceDto);
                            }
                        }
                    }

                    using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                    {
                        foreach (ConsumerDecisionTreeNodeProductDto sourceDto in sourceDtoList)
                        {
                            ConsumerDecisionTreeNodeProductDto dalDto = dal.FetchById(sourceDto.Id);
                            AssertHelper.AssertModelObjectsAreEqual(sourceDto, dalDto,
                                new List<String> { "DateCreated", "DateLastModified" });
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByConsumerDecisionTreeNodeId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {

                    DtoCollection<ConsumerDecisionTreeNodeProductDto> dtos = CreateDtos<ConsumerDecisionTreeNodeProductDto>(dalContext);
                    List<ConsumerDecisionTreeNodeProductDto> sourceDtoList = dtos.Dtos;
                    dtos.InsertDtos(dalContext, _dtoSpec);

                    //remove duplicate products before testing as it was incorrect to add any multiples in the same tree.
                    foreach (var prodGrouping in sourceDtoList.GroupBy(p => p.ProductId))
                    {
                        foreach (ConsumerDecisionTreeNodeProductDto sourceDto in prodGrouping)
                        {
                            if (sourceDto != prodGrouping.Last())
                            {
                                sourceDtoList.Remove(sourceDto);
                            }
                        }
                    }

                    using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                    {
                        foreach (var nodeGroup in sourceDtoList.GroupBy(g => g.ConsumerDecisionTreeNodeId))
                        {
                            IEnumerable<ConsumerDecisionTreeNodeProductDto> dalDtos =
                                dal.FetchByConsumerDecisionTreeNodeId(nodeGroup.Key);

                            Assert.AreEqual(nodeGroup.Count(), dalDtos.Count());

                            foreach (ConsumerDecisionTreeNodeProductDto sourceDto in nodeGroup)
                            {
                                ConsumerDecisionTreeNodeProductDto dalDto =
                                    dalDtos.FirstOrDefault(d => d.Id == sourceDto.Id);
                                AssertHelper.AssertModelObjectsAreEqual(sourceDto, dalDto,
                                new List<String> { "DateCreated", "DateLastModified" });
                            }
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {

                    DtoCollection<ConsumerDecisionTreeNodeProductDto> dtos = CreateDtos<ConsumerDecisionTreeNodeProductDto>(dalContext);
                    List<ConsumerDecisionTreeNodeProductDto> sourceDtoList = dtos.Dtos;
                    dtos.InsertDtos(dalContext, _dtoSpec);

                    //remove duplicate products before testing as it was incorrect to add any multiples in the same tree.
                    foreach (var prodGrouping in sourceDtoList.GroupBy(p => p.ProductId))
                    {
                        foreach (ConsumerDecisionTreeNodeProductDto sourceDto in prodGrouping)
                        {
                            if (sourceDto != prodGrouping.Last())
                            {
                                sourceDtoList.Remove(sourceDto);
                            }
                        }
                    }

                    using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                    {
                        foreach (ConsumerDecisionTreeNodeProductDto sourceDto in sourceDtoList)
                        {
                            ConsumerDecisionTreeNodeProductDto dalDto = dal.FetchById(sourceDto.Id);
                            AssertHelper.AssertModelObjectsAreEqual(sourceDto, dalDto,
                                new List<String> { "DateCreated", "DateLastModified" });
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }
    }
}
