﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mssql;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public class PlanogramComparisonTemplateFieldDal : TestBase<IPlanogramComparisonTemplateFieldDal, PlanogramComparisonTemplateFieldDto>
    {
        #region Fields

        private static readonly Type MssqlDalFactoryType = typeof (DalFactory);
        private static readonly Type MockDalFactoryType = typeof (Ccm.Dal.Mock.DalFactory);
        private static readonly Type UserDalFactoryType = typeof(Ccm.Dal.User.DalFactory);

        private readonly List<Type> _dalFactoryTypes = new List<Type>
                                                       {
                                                           MssqlDalFactoryType,
                                                           MockDalFactoryType,
                                                           UserDalFactoryType
                                                       };

        private readonly List<String> _excludeProperties = new List<String> { "Id" };
        private List<PlanogramComparisonTemplateFieldDto> _dtos;
        private List<PlanogramComparisonTemplateDto> _parentDtos;
        private List<PropertyInfo> _properties;

        #endregion

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramComparisonTemplateFieldDto));
            _properties = typeof(PlanogramComparisonTemplateFieldDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalContext, 1);
            TestDataHelper.InsertUserDtos(dalContext, 1);
            _parentDtos = TestDataHelper.InsertPlanogramComparisonTemplateDtos(dalContext, 1, entityList[0].Id);
            _dtos = TestDataHelper.InsertPlanogramComparisonTemplateFieldDtos(dalContext, 1, _parentDtos);
        }

        #region Fetch

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramComparisonTemplateId(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                InitializeDtosAndProperties(dalContext);

                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
                {
                    foreach (PlanogramComparisonTemplateFieldDto dto in _dtos) {
                        IEnumerable<PlanogramComparisonTemplateFieldDto> returnedDtoList =
                            dal.FetchByPlanogramComparisonTemplateId(dto.PlanogramComparisonTemplateId);
                        PlanogramComparisonTemplateFieldDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(dto.Id));
                        foreach (PropertyInfo property in _properties.Where(property => !_excludeProperties.Contains(property.Name))) 
                        {
                            Assert.AreEqual(property.GetValue(dto, null), property.GetValue(returnedDto, null));
                        }
                    }
                }
            }
        }

        #endregion

        #region Insert

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            TestFetchByPlanogramComparisonTemplateId(dalFactoryType);
        }

        #endregion

        #region Update

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                InitializeDtosAndProperties(dalContext);
                _excludeProperties.Add("PlanogramComparisonTemplateId");

                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
                {
                    foreach (PlanogramComparisonTemplateFieldDto dto in _dtos)
                    {
                        PlanogramComparisonTemplateFieldDto originalSourceDto = _dtos[0];
                        PlanogramComparisonTemplateFieldDto alternativeSourceDto = dto;

                        // Update the DTO in memory.
                        IEnumerable<PropertyInfo> propertyInfos =
                            _properties.Where(property => !_excludeProperties.Contains(property.Name) && property.Name != "Id");
                        foreach (PropertyInfo property in propertyInfos)
                        {
                            property.SetValue(originalSourceDto, property.GetValue(alternativeSourceDto, null), null);
                        }

                        // Update the DTO in database and verify success.
                        dal.Update(originalSourceDto);
                        PlanogramComparisonTemplateFieldDto returnedDto = dal.FetchByPlanogramComparisonTemplateId(originalSourceDto.PlanogramComparisonTemplateId).ElementAt(0);
                        //CheckCreatedModifiedDeletedDates(originalSourceDto, returnedDto);
                        foreach (PropertyInfo property in _properties.Where(property => !_excludeProperties.Contains(property.Name)))
                        {
                            Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
                        }
                    }
                }
            }
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                InitializeDtosAndProperties(dalContext);

                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
                {
                    foreach (PlanogramComparisonTemplateFieldDto dto in _dtos)
                    {
                        //Ensure it exists
                        if (dal.FetchByPlanogramComparisonTemplateId(dto.PlanogramComparisonTemplateId).All(fieldDto => !Equals(fieldDto.Id, dto.Id)))
                            Assert.Inconclusive("The dto to be deleted was not found.");

                        //Delete
                        dal.DeleteById(dto.Id);
                    }

                    foreach (PlanogramComparisonTemplateFieldDto dto in _dtos)
                    {
                        PlanogramComparisonTemplateFieldDto fieldDto = dal.FetchByPlanogramComparisonTemplateId(dto.PlanogramComparisonTemplateId).FirstOrDefault(p => p.Id.Equals(dto.Id));
                        Assert.IsNull(fieldDto);
                    }
                }
            }
        }

        #endregion
    }
}