﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// CCM-29010 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.3.0)
//V8-31831 : A.Probyn
//  Corrected Insert test, added TestFetchByEntityIdName
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramNameTemplateDal : TestBase<IPlanogramNameTemplateDal, PlanogramNameTemplateDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestFetchByProperties(
                       dalFactoryType,
                       "FetchByEntityIdName",
                       new List<String>() { "EntityId", "Name" },
                       _excludeProperties,
                       fetchIncludesDeleted: true);
            }
        }
    }
}