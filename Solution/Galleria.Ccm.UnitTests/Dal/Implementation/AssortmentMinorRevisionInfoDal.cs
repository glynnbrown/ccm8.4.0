﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : I.George
//  Initial Version
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class AssortmentMinorRevisionInfoDal : TestBase<IAssortmentMinorRevisionInfoDal,AssortmentMinorRevisionInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<AssortmentMinorRevisionDto>(dbDalFactoryType);
        }
        
        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                var entity = TestDataHelper.InsertEntityDtos(dalFactory, 1)[0];
                var hrcy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.Id);
                var pdtlvl = TestDataHelper.InsertProductLevelDtos(dalFactory, hrcy.First().Id, 2);
                var pdtgrp = TestDataHelper.InsertProductGroupDtos(dalFactory, pdtlvl, 10);
                var assortments = TestDataHelper.InsertAssortmentDtos(dalFactory, 10, entity.Id, 1);
                var assortmentMinorRevisions = TestDataHelper.InsertAssortmentMinorRevisionDtos(dalFactory, 10, entity.Id);
                AssortmentMinorRevisionInfoDto fetchedAssortmentMinorRevisions = null;

                using (var dal = dalFactory.CreateContext().GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    fetchedAssortmentMinorRevisions = dal.FetchByEntityIdName(entity.Id, assortmentMinorRevisions.First().Name);
                }

                Assert.IsNotNull(fetchedAssortmentMinorRevisions);
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductGroupId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperty<AssortmentMinorRevisionDto>(dbDalFactoryType, "FetchByProductGroupId", "ProductGroupId", false);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdChangeDate(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperties<AssortmentMinorRevisionDto>(dbDalFactoryType, "FetchByEntityIdChangeDate", new List<String> { "EntityId", "DateLastModified" }, false);
        }
        
        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdAssortmentMinorRevisionSearchCriteria(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperties<AssortmentMinorRevisionDto>(dbDalFactoryType, "FetchByEntityIdAssortmentMinorRevisionSearchCriteria", new List<String> { "EntityId", "Name" }, false);
        }  
    }
}
