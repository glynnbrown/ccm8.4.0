﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25587 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramGroupDal : TestBase<IPlanogramGroupDal, PlanogramGroupDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramHierarchyId(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByPlanogramHierarchyId", "PlanogramHierarchyId");
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, null, false);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestUpsert(Type dalFactoryType)
        {
            base.TestUpsert<PlanogramGroupIsSetDto>(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
