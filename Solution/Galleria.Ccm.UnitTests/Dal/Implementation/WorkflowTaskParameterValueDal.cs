﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using System.Collections.Generic;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class WorkflowTaskParameterValueDal : TestBase<IWorkflowTaskParameterValueDal, WorkflowTaskParameterValueDto>
    {
        #region Fields
        private static List<String> _excludeProperties = new List<String> { };
        #endregion

        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fetch
        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByWorkflowTaskParameterId(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByWorkflowTaskParameterId", "WorkflowTaskParameterId", _excludeProperties);
        }
        #endregion

        #region Insert
        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, _excludeProperties, true);
        }
        #endregion

        #region Update
        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, _excludeProperties);
        }
        #endregion

        #region Delete
        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
        #endregion
    }
}
