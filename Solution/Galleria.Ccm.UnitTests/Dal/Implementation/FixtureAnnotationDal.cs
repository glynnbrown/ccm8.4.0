﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM830)
// CCM-32524 : L.Ineson
//		Created
#endregion
#endregion


using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class FixtureAnnotationDal : FixtureTestBase<IFixtureAnnotationDal, FixtureAnnotationDto>
    {
        private List<String> _excludeProperties = new List<String> { "ExtendedData" };

        internal override Ccm.Dal.Pogfx.SectionType SectionType
        {
            get { return Ccm.Dal.Pogfx.SectionType.FixtureAnnotations; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "FixturePackageId";
                yield return "FixtureItemId";
            }
        }


        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.User.DalFactory),
        };

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByFixturePackageId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType,
                "FetchByFixturePackageId", "FixturePackageId", _excludeProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

    }
}
