﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29471 : M.Pettit
//  DataModelId is now an Object data type
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class DataModelInfoDal : TestBase<IDataModelInfoDal, DataModelInfoDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Helpers

        private void AssertDataModelDtoAndInfoDtoAreEqual(DataModelDto sourceDto, DataModelInfoDto infoDto)
        {
            Assert.AreEqual(sourceDto.Name, infoDto.Name);
            Assert.AreEqual(sourceDto.Description, infoDto.Description);
            Assert.AreEqual(sourceDto.Id, infoDto.Id);
        }

        private DateTime RoundToNearest(DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            bool roundUp = delta > d.Ticks / 2;

            if (roundUp)
            {
                var deltaU = (d.Ticks - (dt.Ticks % d.Ticks)) % d.Ticks;
                return new DateTime(dt.Ticks + deltaU);
            }
            else
            {
                var deltaD = dt.Ticks % d.Ticks;
                return new DateTime(dt.Ticks - deltaD);
            }
        }

        #endregion

        #region FetchAll

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchAll(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                IEnumerable<DataModelDto> sourceDtoList = TestDataHelper.InsertDataModelDtos(dalFactory);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IDataModelInfoDal dal = dalContext.GetDal<IDataModelInfoDal>())
                    {
                        List<DataModelInfoDto> returnedDtos = dal.FetchAll().ToList();
                        foreach (DataModelDto sourceDto in sourceDtoList)
                        {
                            DataModelInfoDto infoDto = returnedDtos.FirstOrDefault(r => r.Id.Equals(sourceDto.Id));
                            Assert.IsNotNull(infoDto);
                            AssertDataModelDtoAndInfoDtoAreEqual(sourceDto, infoDto);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
