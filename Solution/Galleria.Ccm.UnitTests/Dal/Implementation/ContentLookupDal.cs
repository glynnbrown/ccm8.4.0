﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26520 : J.Pickup
//		Created
// V8-26322 : A.Silva
//      Set TestInsert to throw inconclusive until V8-28519 is fixed.

#endregion

#region Version History: (CCM v8.1.0)
// V8-29746 : A.Probyn
//  Updated inline with changes to replace Names with Ids
//  Needs resolving once V8-29847 is fixed.
#endregion
#endregion

using System;
using System.Collections.Generic;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ContentLookupDal : TestBase<IContentLookupDal, ContentLookupDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            Assert.Ignore();
            base.TestFetchByProperty(dbDalFactoryType, "FetchByEntityId", "EntityId");
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            Assert.Ignore();
            base.TestInsert(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            Assert.Ignore();
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            Assert.Ignore();
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dbDalFactoryType)
        {
            Assert.Ignore();
            base.TestFetchByProperty(dbDalFactoryType, "FetchByPlanogramId", "PlanogramId");
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            Assert.Ignore();
            base.TestFetchByProperty(dbDalFactoryType, "FetchById", "Id", fetchIncludesDeleted: true);
        }


        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPlanogramIds(Type dbDalFactoryType)
        {
            Assert.Ignore();
            base.TestFetchByProperties(dbDalFactoryType, "FetchByPlanogramIds", new List<String>{"PlanogramId"});
        }
    }
}
