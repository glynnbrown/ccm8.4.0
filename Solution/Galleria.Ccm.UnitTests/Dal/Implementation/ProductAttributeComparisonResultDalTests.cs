﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{

    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public class ProductAttributeComparisonResultDalTests : TestBase<IProductAttributeComparisonResultDal, ProductAttributeComparisonResultDto>
    {
        #region Constants

        private const String FetchByIdPropertyName = "EntityId";
        private const String FetchMethodName = "FetchByEntityId";

        #endregion

        #region Fields
        private readonly List<Type> _dalFactoryTypes =
            new List<Type>
            {
                typeof (Ccm.Dal.Mssql.DalFactory)
                // Mock dal is tested in the framework.
            };
        #endregion

        private static readonly List<String> PropertiesToExclude = new List<String> { "ExtendedData" };

        [Test]
        public void FetchCorrectMethod()
        {
            var dtoType = typeof(ProductAttributeComparisonResultDto);
            String dtoNamespace = dtoType.Namespace;
            String dalNamespace = dtoNamespace.Replace("DataTransferObjects", "Interfaces");
            String dalInterfaceName = dalNamespace + ".I" + dtoType.Name.Substring(0, dtoType.Name.Length - 3) + "Dal";
            Type dalType = dtoType.Assembly.GetType(dalInterfaceName);

            dalType.Should().NotBeNull();
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var context = dalFactory.CreateContext())
                {
                    var userName = TestDataHelper.InsertUserDtos(context, 1).First().UserName;
                    var entityId = TestDataHelper.InsertEntityDtos(context, 1).First().Id;
                    var insertedePackages = TestDataHelper.InsertPackageDtos(context, userName, entityId, 1);
                    var planogramToInsert = TestDataHelper.InsertPlanogramDtos(context, userName, 1, insertedePackages);

                    using (var dal = context.GetDal<IProductAttributeComparisonResultDal>())
                    {
                        var productAttributeComparisonResult = new ProductAttributeComparisonResultDto
                        {
                            PlanogramId = (Int32) planogramToInsert.First().Id,
                            ComparedProductAttribute = "[Product].[Brand]",
                            ProductGtin = "123456",
                            MasterDataValue = "123456",
                            ProductValue = "123457"
                        };

                        dal.Insert(productAttributeComparisonResult);

                        var entityAttributeFromDB = dal.FetchByPlanogramId(planogramToInsert.First().Id);
                        entityAttributeFromDB.FirstOrDefault().ShouldBeEquivalentTo(productAttributeComparisonResult);
                    }
                }
            }
            //TestFetchByProperty(dalFactoryType, FetchMethodName, FetchByIdPropertyName, PropertiesToExclude);
        }

        #region Insert

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var context = dalFactory.CreateContext())
                {
                    var userName = TestDataHelper.InsertUserDtos(context, 1).First().UserName;
                    var entityId = TestDataHelper.InsertEntityDtos(context, 1).First().Id;
                    var insertedePackages = TestDataHelper.InsertPackageDtos(context, userName, entityId, 1);
                    var planogramToInsert = TestDataHelper.InsertPlanogramDtos(context, userName, 1, insertedePackages);

                    using (var dal = context.GetDal<IProductAttributeComparisonResultDal>())
                    {
                        var ProductAttributeComparisonResult = new ProductAttributeComparisonResultDto
                        {
                            PlanogramId = (Int32)planogramToInsert.First().Id,
                            ComparedProductAttribute = "[Product].[Brand]",
                            ProductGtin = "123456",
                            MasterDataValue = "123456",
                            ProductValue = "123457"
                        };

                        dal.Insert(ProductAttributeComparisonResult);

                        var entityAttributeFromDB = dal.FetchByPlanogramId(planogramToInsert.First().Id);
                        entityAttributeFromDB.FirstOrDefault().ShouldBeEquivalentTo(ProductAttributeComparisonResult);
                    }
                }
            }

            //TestInsert(dalFactoryType, PropertiesToExclude);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, PropertiesToExclude);
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var context = dalFactory.CreateContext())
                {
                    var userName = TestDataHelper.InsertUserDtos(context, 1).First().UserName;
                    var entityId = TestDataHelper.InsertEntityDtos(context, 1).First().Id;
                    var insertedePackages = TestDataHelper.InsertPackageDtos(context, userName, entityId, 1);
                    var planogramToInsert = TestDataHelper.InsertPlanogramDtos(context, userName, 1, insertedePackages);

                    using (var dal = context.GetDal<IProductAttributeComparisonResultDal>())
                    {
                        var productAttributeComparisonResult = new ProductAttributeComparisonResultDto
                        {
                            PlanogramId = (Int32)planogramToInsert.First().Id,
                            ComparedProductAttribute = "[Product].[Brand]",
                            ProductGtin = "123456",
                            MasterDataValue = "123456",
                            ProductValue = "123457"
                        };

                        dal.Insert(productAttributeComparisonResult);

                        dal.DeleteById(productAttributeComparisonResult.Id);

                        var entityAttributeFromDB = dal.FetchByPlanogramId(productAttributeComparisonResult.PlanogramId);
                        CollectionAssert.IsEmpty(entityAttributeFromDB);
                    }
                }
            }

        }

        #endregion

        #region Update

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, null);
        }

        #endregion

    }
}
