﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25445 L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationGroupInfoDal : TestBase<ILocationGroupInfoDal, LocationGroupInfoDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchDeletedByLocationHierarchyId(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");

            //base.TestInfoFetchByProperties<LocationGroupDto, Int32>(
            //    dalFactoryType,
            //    "FetchByLocationGroupIds",
            //    new List<String> { },
            //    "Id",
            //    /*includesDeleted*/true);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByLocationGroupIds(Type dalFactoryType)
        {
            base.TestInfoFetchByProperties<LocationGroupDto, Int32>(
                dalFactoryType,
                "FetchByLocationGroupIds",
                new List<String> { },
                "Id",
                /*includesDeleted*/true);
        }
    }
}
