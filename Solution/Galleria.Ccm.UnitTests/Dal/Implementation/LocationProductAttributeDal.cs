﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
//  Amended the insert test 
// V8-26322 : A.Silva
//      Amended Upsert tesdt to account for the SetDto 
//          and made the Mssql version of it inconclusive as the test base needs some changes for this to work.

#endregion

#endregion

using System;
using System.Collections.Generic;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationProductAttributeDal : TestBase<ILocationProductAttributeDal, LocationProductAttributeDto>
    {
        private readonly List<String> _excludedProperties = new List<String> { "LocationId", "ProductId" };

        #region Constructor

        public LocationProductAttributeDal() : base(typeof(Ccm.Dal.Mock.DalFactory), typeof(Ccm.Dal.Mssql.DalFactory)) { }

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            base.TestFetchByEntityId(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            const String deleteMethodName = "DeleteById";
            TestDeleteByProperties(dalFactoryType, deleteMethodName, _excludedProperties);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteByEntityId(Type dalFactoryType)
        {
            base.TestDeleteByEntityId(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            Assert.Ignore("Fix testbase"); 
            base.TestInsert(dalFactoryType, null);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestUpsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof (Ccm.Dal.Mssql.DalFactory))
                Assert.Inconclusive("Current Testbase does not taste this case correctly."); // TODO Amend Test base to allow running this test in this case.

            base.TestUpsert<LocationProductAttributeIsSetDto>(dalFactoryType);
        }

       

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByLocationIdProductId(Type dalFactoryType)
        {
            TestFetchByProperties(dalFactoryType, "FetchByLocationIdProductId", _excludedProperties);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByLocationIdProductIdCombinations(Type dalFactoryType)
        {
            Assert.Ignore("TODO"); 
        }

       
    }
}
