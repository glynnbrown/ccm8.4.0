﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public sealed class PlanogramValidationTemplateDal : TestBase<IPlanogramValidationTemplateDal, PlanogramValidationTemplateDto>
    {
        private List<String> _excludeProperties = new List<String> { "ExtendedData" };

        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
        };

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType,
                "FetchByPlanogramId", "PlanogramId", _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }


    }
}
