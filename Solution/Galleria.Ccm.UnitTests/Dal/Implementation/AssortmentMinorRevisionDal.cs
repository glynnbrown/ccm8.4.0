﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 800)
//V8-25455 : J.Pickup
//	Created
// V8-28016 : I.George
//      Added TestFetchAllIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.UnitTests.UI;


namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class AssortmentMinorRevisionDal : TestBase<IAssortmentMinorRevisionDal, AssortmentMinorRevisionDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, null, true);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchAll(Type dbDalFactoryType)
        {           
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var product = TestDataHelper.InsertProductDtos(dalFactory, 1, entity.First().Id);

                    var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.First().Id);
                    var productLevel = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchy.First().Id, 1);
                    var productGroup = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel, 1);

                    var assortment = TestDataHelper.InsertAssortmentDtos(dalFactory, 1, entity.First().Id, productGroup.First().Id);

                    var _sourceDtoList = TestDataHelper.InsertAssortmentMinorRevisionDtos(dalFactory, 1, entity.First().Id);

                    using (var dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            //Check are correctly added.
                           IEnumerable<AssortmentMinorRevisionDto> returnedDto = dal.FetchAll();
                            Assert.IsNotNull(returnedDto);
                            Assert.AreEqual(_sourceDtoList.Count, returnedDto.Count());

                            //Check are now removed
                            dal.FetchAll();

                            IEnumerable<AssortmentMinorRevisionDto> returnedAfterDeletionDto = dal.FetchAll();
                            Assert.AreEqual(_sourceDtoList.Count, returnedAfterDeletionDto.Count(), "Expected item to be deleted, but was not.");
                        }
                    }
                }
            }
        }


        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dbDalFactoryType)
        {
            base.TestFetchByProperties(dbDalFactoryType, "FetchByEntityIdName", new List<string>() { "EntityId", "Name" }, fetchIncludesDeleted: true);
        }
    }
 
}