﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

//V8-25587 L.Hodson
//  Created
// V8-25881 : A.Probyn
//  Updated to manually insert data due to issues with planogram insertion with framework
// V8-26322 : A.Silva
//      Amended how User records are inserted to avoid random duplicates.

#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class WorkpackagePlanogramDal : TestBase<IWorkpackagePlanogramDal, WorkpackagePlanogramDto>
    {
         #region Fields 
        private readonly List<String> _excludeProperties = new List<String>() { "ExtendedData", "ProcessingStatus" };
        private List<WorkpackagePlanogramDto> _sourceDtoList;
        private List<PropertyInfo> _properties; 
        #endregion

        #region Constructor

        public WorkpackagePlanogramDal() : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestFetchByProperty(
                    dalFactoryType,
                    "FetchByWorkpackageId",
                    "WorkpackageId",
                    _excludeProperties);
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IWorkpackagePlanogramDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                IEnumerable<WorkpackagePlanogramDto> returnedDtoList = dal.FetchByWorkpackageId(_sourceDtoList[i].WorkpackageId);
                                WorkpackagePlanogramDto returnedDto = returnedDtoList.FirstOrDefault(p => p.DestinationPlanogramId.Equals(_sourceDtoList[i].DestinationPlanogramId));
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name))
                                    {
                                        Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestInsert(dalFactoryType, null);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IWorkpackagePlanogramDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            IEnumerable<WorkpackagePlanogramDto> returnedDtoList = dal.FetchByWorkpackageId(_sourceDtoList[i].WorkpackageId);
                            WorkpackagePlanogramDto returnedDto = returnedDtoList.FirstOrDefault(p => p.DestinationPlanogramId.Equals(_sourceDtoList[i].DestinationPlanogramId));
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);
                    using (var dal = dalContext.GetDal<IWorkpackagePlanogramDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            var originalSourceDto = _sourceDtoList[0];
                            var alternativeSourceDto = _sourceDtoList[i];

                            // Update the DTO in memory.
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
                                {
                                    property.SetValue(
                                        originalSourceDto,
                                        property.GetValue(alternativeSourceDto,null),
                                        null);
                                }
                            }

                            // Update the DTO in database and verify success.
                            dal.Update(originalSourceDto);
                            IEnumerable<WorkpackagePlanogramDto> returnedDtoList = dal.FetchByWorkpackageId(_sourceDtoList[i].WorkpackageId);
                            WorkpackagePlanogramDto returnedDto = returnedDtoList.FirstOrDefault(p => p.DestinationPlanogramId.Equals(_sourceDtoList[i].DestinationPlanogramId));
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestDeleteByProperty(dalFactoryType, "DeleteById", "PlanogramId");
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IWorkpackagePlanogramDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                //Ensure it exists
                                IEnumerable<WorkpackagePlanogramDto> returnedDtoList = dal.FetchByWorkpackageId(_sourceDtoList[i].WorkpackageId);
                                WorkpackagePlanogramDto returnedDto = returnedDtoList.FirstOrDefault(p => p.DestinationPlanogramId.Equals(_sourceDtoList[i].DestinationPlanogramId));
                            
                                //Delete planogram
                                dal.DeleteById(_sourceDtoList[i].DestinationPlanogramId);

                                //Ensure record is deleted
                                returnedDtoList = dal.FetchByWorkpackageId(_sourceDtoList[i].WorkpackageId);
                                returnedDto = returnedDtoList.FirstOrDefault(p => p.DestinationPlanogramId.Equals(_sourceDtoList[i].DestinationPlanogramId));
                                Assert.IsNull(returnedDto);
                            }
                        }
                    }
                }
            }
        }

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(WorkpackagePlanogramDto));
            _properties = typeof(WorkpackagePlanogramDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(dalContext, 1);
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First();
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName.UserName, entity, 5);
            List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entityDtoList, 1);
            List<WorkpackageDto> workpackageDtoList = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList, userName);
            List<PlanogramDto> planogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName.UserName, 1, packageDtoList);
            _sourceDtoList = TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext, workpackageDtoList, planogramDtoList);
        }
    }
}
