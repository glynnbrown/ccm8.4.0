﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
//V8-27941 J.Pickup
// MSSQL added
#endregion
#endregion

using System;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Reflection;
using System.IO;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class HighlightDal : TestBase<IHighlightDal, HighlightDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.User.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else 
            {
                base.TestFetchById(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestFetchByProperties(
                       dalFactoryType,
                       "FetchByEntityIdName",
                       new List<String>() { "EntityId", "Name" },
                       _excludeProperties,
                       fetchIncludesDeleted: true);
            }
        }



        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties, false);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestLockById(Type dalFactoryType)
        {
            throw new InconclusiveException("Cannot test file locking");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUnlockById(Type dalFactoryType)
        {
            throw new InconclusiveException("Cannot test file locking");
        }

        #region Ticket Tests

        /// <summary>
        /// Checks that changing the filename to a new name and back again does not fail.
        /// </summary>
        /// <param name="dalFactoryType"></param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFileNameChange(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                    UriBuilder uri = new UriBuilder(codeBase);
                    string path = Uri.UnescapeDataString(uri.Path);
                    String userFilePath = Path.GetDirectoryName(path);


                    HighlightDto dto = new HighlightDto();
                    dto.Id = String.Format("{0}\\1.pogh", userFilePath);
                    dto.Name = "1";

                    InsertDto(dalFactory, dto);

                    //refetch
                    dto = FetchDtoById(dalFactory, dto.Id);

                    //change the name & update
                    dto.Name = "2";
                    UpdateDto(dalFactory, dto);
                    Assert.AreEqual("2", dto.Name);

                    //change the name & update again
                    dto.Name = "3";
                    UpdateDto(dalFactory, dto);
                    Assert.AreEqual("3", dto.Name);

                    //refetch
                    dto = FetchDtoById(dalFactory, dto.Id);
                    Assert.AreEqual("3", dto.Name);
                }
            }
        }


        #endregion


    }
}
