﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Pogfx;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{

    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public class EntityComparisonAttributeDalTests : TestBase<IEntityComparisonAttributeDal, EntityComparisonAttributeDto>
    {
        #region Constants

        private const String FetchByIdPropertyName = "EntityId";
        private const String FetchMethodName = "FetchByEntityId";

        #endregion

        #region Fields
        private readonly List<Type> _dalFactoryTypes =
            new List<Type>
            {
                typeof (Ccm.Dal.Mssql.DalFactory)
                // Mock dal is tested in the framework.
            };
        #endregion

        private static readonly List<String> PropertiesToExclude = new List<String> { "ExtendedData" };

        [Test]
        public void FetchCorrectMethod()
        {
            var dtoType = typeof(EntityComparisonAttributeDto);
            String dtoNamespace = dtoType.Namespace;
            String dalNamespace = dtoNamespace.Replace("DataTransferObjects", "Interfaces");
            String dalInterfaceName = dalNamespace + ".I" + dtoType.Name.Substring(0, dtoType.Name.Length - 3) + "Dal";
            Type dalType = dtoType.Assembly.GetType(dalInterfaceName);

            dalType.Should().NotBeNull();
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            var entityToInsert = new EntityDto
            {
                Name = "Test",
                Id = 219383,
                DateCreated = DateTime.Now,
                DateLastMerchSynced = DateTime.Now,
                DateLastModified = DateTime.Now,
                Description = "Test"
            };

            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var context = dalFactory.CreateContext())
                {
                    using (var dal = context.GetDal<IEntityDal>())
                    {
                        dal.Insert(entityToInsert);
                    }

                    using (var dal = context.GetDal<IEntityComparisonAttributeDal>())
                    {
                        var entityComparisonAttribute = new EntityComparisonAttributeDto(entityToInsert)
                        {
                            ItemType = (byte)PlanogramItemType.Product,
                            PropertyName = "test",
                            PropertydisplayName = "test"
                        };

                        dal.Insert(entityComparisonAttribute);

                        var entityAttributeFromDB = dal.FetchByEntityId(entityToInsert.Id);

                        entityAttributeFromDB.FirstOrDefault().ShouldBeEquivalentTo(entityComparisonAttribute);
                    }
                }
            }

            //TestFetchByProperty(dalFactoryType, FetchMethodName, FetchByIdPropertyName, PropertiesToExclude);
        }

        #region Insert
        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            var entityToInsert = new EntityDto
            {
                Name = "Test",
                Id = 219383,
                DateCreated = DateTime.Now,
                DateLastMerchSynced = DateTime.Now,
                DateLastModified = DateTime.Now,
                Description = "Test"
            };

            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var context = dalFactory.CreateContext())
                {
                    using (var dal = context.GetDal<IEntityDal>())
                    {
                        dal.Insert(entityToInsert);
                    }

                    using (var dal = context.GetDal<IEntityComparisonAttributeDal>())
                    {
                        var entityComparisonAttribute = new EntityComparisonAttributeDto(entityToInsert);
                        dal.Insert(entityComparisonAttribute);
                    }
                }
            }

            //TestInsert(dalFactoryType, PropertiesToExclude);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, PropertiesToExclude);
        }

        #endregion


        #region Update
        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            //TestUpdate(dalFactoryType, PropertiesToExclude);
            var entityToInsert = new EntityDto
            {
                Name = "Test",
                Id = 1,
                DateCreated = DateTime.Now,
                DateLastMerchSynced = DateTime.Now,
                DateLastModified = DateTime.Now,
                Description = "Test"
            };

            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var context = dalFactory.CreateContext())
                {
                    using (var dal = context.GetDal<IEntityDal>())
                    {
                        dal.Insert(entityToInsert);
                    }

                    using (var dal = context.GetDal<IEntityComparisonAttributeDal>())
                    {
                        var entityComparisonAttribute = new EntityComparisonAttributeDto(entityToInsert)
                        {
                            ItemType = (byte)PlanogramItemType.Product,
                            PropertyName = "test",
                            PropertydisplayName = "test"
                        };

                        dal.Insert(entityComparisonAttribute);

                        entityComparisonAttribute.ItemType = (byte)PlanogramItemType.Planogram;
                        entityComparisonAttribute.PropertyName += "2";
                        entityComparisonAttribute.PropertydisplayName += "2";

                        dal.Update(entityComparisonAttribute);

                        var entityAttributeFromDB = dal.FetchByEntityId(entityToInsert.Id);

                        entityAttributeFromDB.FirstOrDefault().ShouldBeEquivalentTo(entityComparisonAttribute);
                    }
                }
            }


        }

        #endregion

        #region Delete

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            //base.TestDeleteById(dalFactoryType);

            var entityToInsert = new EntityDto
            {
                Name = "Test",
                Id = 1,
                DateCreated = DateTime.Now,
                DateLastMerchSynced = DateTime.Now,
                DateLastModified = DateTime.Now,
                Description = "Test"
            };

            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (var context = dalFactory.CreateContext())
                {
                    using (var dal = context.GetDal<IEntityDal>())
                    {
                        dal.Insert(entityToInsert);
                    }

                    using (var dal = context.GetDal<IEntityComparisonAttributeDal>())
                    {
                        var entityComparisonAttribute = new EntityComparisonAttributeDto(entityToInsert)
                        {
                            ItemType = (byte)PlanogramItemType.Product,
                            PropertyName = "test",
                            PropertydisplayName = "test"
                        };

                        dal.Insert(entityComparisonAttribute);

                        dal.DeleteById(entityComparisonAttribute.Id);

                        IEnumerable<EntityComparisonAttributeDto> entityAttributeFromDB = dal.FetchByEntityId(entityToInsert.Id);
                        CollectionAssert.IsEmpty(entityAttributeFromDB);
                    }
                }
            }
        }

        #endregion


    }
}
