﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class AssortmentLocationBuddyDal : TestBase<IAssortmentLocationBuddyDal, AssortmentLocationBuddyDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByAssortmentId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType, "FetchByAssortmentId", "AssortmentId", new List<String>() { "LocationCode", "S1LocationCode", "S2LocationCode", "S3LocationCode", "S4LocationCode", "S5LocationCode" });
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType, new List<String>() { "LocationCode", "S1LocationCode", "S2LocationCode", "S3LocationCode", "S4LocationCode", "S5LocationCode" });
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, new List<String>() { "LocationCode", "S1LocationCode", "S2LocationCode", "S3LocationCode", "S4LocationCode", "S5LocationCode" });
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, new List<String>() { "LocationCode", "S1LocationCode", "S2LocationCode", "S3LocationCode", "S4LocationCode", "S5LocationCode" });
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }
    }
}
