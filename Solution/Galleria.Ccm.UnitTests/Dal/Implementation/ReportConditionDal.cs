﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Reporting.Model;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ReportConditionDal : TestBase<IReportConditionDal, ReportConditionDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fields
        List<ReportDto> _reportDtos = new List<ReportDto>();
        #endregion

        #region Helpers

        private List<ReportConditionDto> InsertDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<DataModelDto> dataModelDtos = TestDataHelper.InsertDataModelDtos(dalFactory);
            _reportDtos = TestDataHelper.InsertReportDtos(dalFactory, numToInsert, dataModelDtos);
            List<ReportPredicateDto> reportPredicateDtos = TestDataHelper.InsertReportPredicateDtos(dalFactory, numToInsert, _reportDtos);
            List<ReportConditionDto> reportConditionDtos = TestDataHelper.InsertReportConditionDtos(dalFactory, numToInsert, _reportDtos, reportPredicateDtos);
            return reportConditionDtos;
        }
        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByReportId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportConditionDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportConditionDal dal = dalContext.GetDal<IReportConditionDal>())
                    {
                        foreach (ReportDto reportDto in _reportDtos)
                        {
                            List<ReportConditionDto> dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            List<ReportConditionDto> sourceDtosInReport = sourceDtoList.Where(r => r.ReportId.Equals(reportDto.Id)).ToList();

                            Assert.AreEqual(sourceDtosInReport.Count, dalDtos.Count, "Correct number of items should be returned");

                            foreach (ReportConditionDto sourceDto in sourceDtosInReport)
                            {
                                ReportConditionDto dalDto = dalDtos.FirstOrDefault(s => s.Id == sourceDto.Id);
                                Assert.IsNotNull(dalDto);
                                Assert.AreEqual(dalDto, sourceDto);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportConditionDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportConditionDal dal = dalContext.GetDal<IReportConditionDal>())
                    {
                        foreach (ReportDto reportDto in _reportDtos)
                        {
                            List<ReportConditionDto> dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            List<ReportConditionDto> sourceDtosInReport = sourceDtoList.
                                Where(r => r.ReportId == reportDto.Id).ToList();

                            Assert.AreEqual(sourceDtosInReport.Count, dalDtos.Count,
                                "Correct number of items should be returned");

                            foreach (ReportConditionDto sourceDto in sourceDtosInReport)
                            {
                                ReportConditionDto dalDto = dalDtos.FirstOrDefault(s => s.Id == sourceDto.Id);
                                Assert.IsNotNull(dalDto);
                                Assert.AreEqual(dalDto, sourceDto, "Each item should match");
                            }
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Tests that a dto Updated into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportConditionDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportConditionDal dal = dalContext.GetDal<IReportConditionDal>())
                    {
                        foreach (ReportDto reportDto in _reportDtos)
                        {
                            List<ReportConditionDto> dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            List<ReportConditionDto> sourceDtosInReport = sourceDtoList.
                                Where(r => r.ReportId == reportDto.Id).ToList();

                            ReportConditionDto dalDto = dalDtos.First();
                            Int32 dtoId = dalDto.Id;
                            ReportConditionDto sourceDto = sourceDtoList.First(p => p.Id == dtoId);

                            //update the dalDto
                            dalDto.Value = (Object)1.2;
                            dalDto.Value2 = (Object)3.4;
                            dalDto.DataOperatorId = "NotEquals";

                            dal.Update(dalDto);

                            dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            dalDto = dalDtos.First(p => p.Id == dtoId);
                            Assert.AreNotEqual(sourceDto, dalDto);
                            Assert.AreEqual((Object)1.2, dalDto.Value);
                            Assert.AreEqual((Object)3.4, dalDto.Value2);
                            Assert.AreEqual("NotEquals", dalDto.DataOperatorId);
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Tests that a product universe can be deleted
        /// </summary>
        /// <param name="dalFactoryType">The dal factory to test</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestDeleteById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportConditionDto> dtoList = InsertDtos(dalFactory, 2);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportConditionDal dal = dalContext.GetDal<IReportConditionDal>())
                    {
                        foreach (ReportConditionDto sourceDto in dtoList)
                        {
                            // delete the dto
                            dal.DeleteById(sourceDto.Id);

                            List<ReportConditionDto> returnedDtos = dal.FetchByReportId(sourceDto.ReportId).ToList();
                            ReportConditionDto returnedDto = returnedDtos.FirstOrDefault(p => p.Id == sourceDto.Id);
                            Assert.IsNull(returnedDto, "No item should be returned");
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion
    }
}
