﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015
#region Version History: (CCM 8.2.0)
// V8-30738 : L.Ineson
//  Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    public sealed class PrintTemplateInfoDal : TestBase<IPrintTemplateInfoDal, PrintTemplateInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.User.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            //method not supported by user dal.
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory)) return;

            base.TestInfoFetchByEntityId<PrintTemplateDto>(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByIds(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore("TestBase doesn't currently support user dal implementation tests.");
            }
            else
            {
                base.TestInfoFetchByProperties<PrintTemplateDto, Object>(dalFactoryType, "FetchByIds", new List<String> { }, "Id");
            }
        }
    }
}
