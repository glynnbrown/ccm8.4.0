﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25395 A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Reflection;
using System.IO;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class ProductLibraryDal : TestBase<IProductLibraryDal, ProductLibraryDto>
    {
        #region Helpers

        private List<String> _excludeProperties = new List<String> { /*"Id"*/ };
        private List<String> _userDalExcludeProperties = new List<String> { "RowVersion", "EntityId" };
        #endregion

        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.User.DalFactory);
            }
        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TODO");
            }
            else
            {
                base.TestFetchById(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TODO");
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties, true);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TODO");
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TODO");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestLockById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore();
            }
            else
            {
                Assert.Pass(); //Nothing to test
            }
        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUnlockById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore();
            }
            else
            {
                Assert.Pass(); //Nothing to test
            }
        }

    }
}
