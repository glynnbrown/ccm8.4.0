﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-28016 : I.George
//      Added TestFetchByUniqueContentReference and TestFetchAllIncludingDeleted
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    class AssortmentDal : TestBase<IAssortmentDal, AssortmentDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType, null);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, null, true);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            if (dbDalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestDeleteById(dbDalFactoryType);
                return;
            }

            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var product = TestDataHelper.InsertProductDtos(dalFactory, 1, entity.First().Id);

                    var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.First().Id);
                    var productLevel = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchy.First().Id, 1);
                    var productGroup = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel, 1);

                    var _sourceDtoList = TestDataHelper.InsertAssortmentDtos(dalFactory, 1, entity.First().Id, productGroup.First().Id);

                    using (var dal = dalContext.GetDal<IAssortmentDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            //Check are correctly added.
                            AssortmentDto returnedDto = dal.FetchById(_sourceDtoList[i].Id);
                            Assert.IsNotNull(returnedDto);
                            Assert.AreEqual(_sourceDtoList.First().Id, returnedDto.Id);

                            //Check are now removed
                            dal.DeleteById(_sourceDtoList[i].Id);
                            Assert.Throws<DtoDoesNotExistException>(() => dal.FetchById(_sourceDtoList[i].Id));
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dbDalFactoryType)
        {
            base.TestFetchByProperties(dbDalFactoryType, "FetchByEntityIdName", new List<String>() { "EntityId", "Name" }, null, null, false);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteByEntityId(Type dbDalFactoryType)
        {
            if (dbDalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestDeleteByProperty(dbDalFactoryType, "DeleteByEntityId", "EntityId", false);
                return;
            }

            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var product = TestDataHelper.InsertProductDtos(dalFactory, 1, entity.First().Id);

                    var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.First().Id);
                    var productLevel = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchy.First().Id, 1);
                    var productGroup = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel, 1);

                    var _sourceDtoList = TestDataHelper.InsertAssortmentDtos(dalFactory, 1, entity.First().Id, productGroup.First().Id);

                    using (var dal = dalContext.GetDal<IAssortmentDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            //Check are correctly added.
                            AssortmentDto returnedDto = dal.FetchById(_sourceDtoList[i].Id);
                            Assert.IsNotNull(returnedDto);
                            Assert.AreEqual(_sourceDtoList.First().Id, returnedDto.Id);

                            //Check are now removed
                            dal.DeleteByEntityId(_sourceDtoList[i].EntityId);

                            using (var dalInfo = dalContext.GetDal<IAssortmentInfoDal>())
                            {
                                IEnumerable<AssortmentInfoDto> entityAssortments = dalInfo.FetchByEntityId(_sourceDtoList[i].EntityId);
                                Assert.IsTrue(!entityAssortments.Any(), "Expected items to be deleted, but was not.");
                            }
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchAll(Type DalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(DalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var product = TestDataHelper.InsertProductDtos(dalFactory, 1, entity.First().Id);

                    var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.First().Id);
                    var productLevel = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchy.First().Id, 1);
                    var productGroup = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel, 1);

                    var _sourceDtoList = TestDataHelper.InsertAssortmentDtos(dalFactory, 1, entity.First().Id, productGroup.First().Id);

                    using (var dal = dalContext.GetDal<IAssortmentDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            IEnumerable<AssortmentDto> returnedDtos = dal.FetchAll();
                            Assert.IsNotNull(returnedDtos);
                            Assert.AreEqual(_sourceDtoList.Count(), returnedDtos.Count(), "Count should be the same");

                            //Check are now removed
                            dal.FetchAll();

                            IEnumerable<AssortmentDto> returnedAfterDeletingDtos = dal.FetchAll();
                            Assert.AreEqual(_sourceDtoList.Count(), returnedAfterDeletingDtos.Count(), "All deleted items should have been refetched");
                        }

                    }

                }

            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByUniqueContentReference(Type DalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(DalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var product = TestDataHelper.InsertProductDtos(dalFactory, 1, entity.First().Id);

                    var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.First().Id);
                    var productLevel = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchy.First().Id, 1);
                    var productGroup = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel, 1);

                    var _sourceDtoList = TestDataHelper.InsertAssortmentDtos(dalFactory, 1, entity.First().Id, productGroup.First().Id);
                    using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                    {
                        foreach (AssortmentDto sourceDto in _sourceDtoList)
                        {
                            AssortmentDto returnedDto = dal.FetchByUniqueContentReference(sourceDto.UniqueContentReference);
                            Assert.AreEqual(sourceDto, returnedDto);

                        }
                    }
                }
            }
        }
    }

}

