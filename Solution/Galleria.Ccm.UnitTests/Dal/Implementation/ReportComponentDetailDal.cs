﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Reporting.Model;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ReportComponentDetailDal : TestBase<IReportComponentDetailDal, ReportComponentDetailDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fields
        List<ReportComponentDto> _reportComponentDtos = new List<ReportComponentDto>();
        #endregion

        #region Helpers

        private List<ReportComponentDetailDto> InsertDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<DataModelDto> dataModelDtos = TestDataHelper.InsertDataModelDtos(dalFactory);
            List<ReportDto> reportDtos = TestDataHelper.InsertReportDtos(dalFactory, numToInsert, dataModelDtos);
            List<ReportSectionDto> reportSectionDtos = TestDataHelper.InsertReportSectionDtos(dalFactory, 1, reportDtos);
            _reportComponentDtos = TestDataHelper.InsertReportComponentDtos(dalFactory, 5, reportSectionDtos);
            List<ReportComponentDetailDto> reportComponentDetailDtos = TestDataHelper.InsertReportComponentDetailDtos(dalFactory, 5, _reportComponentDtos);
            return reportComponentDetailDtos;
        }
        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDetailDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDetailDal dal = dalContext.GetDal<IReportComponentDetailDal>())
                    {
                        //Test Fetch
                        foreach (ReportComponentDetailDto sourceDto in sourceDtoList)
                        {
                            ReportComponentDetailDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);
                        }

                        //Delete them all
                        foreach (ReportComponentDto componentDto in _reportComponentDtos)
                        {
                            //Test return following deletion
                            dal.DeleteByReportComponentId(componentDto.Id);
                        }

                        //Test return following deletion
                        foreach (ReportComponentDetailDto sourceDto in sourceDtoList)
                        {
                            ReportComponentDetailDto returnedDto;
                            Assert.Throws<DtoDoesNotExistException>
                                (() => { returnedDto = dal.FetchById(sourceDto.Id); },
                                "Deleted items should be returned when fetched by Id");
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByReportComponentId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDetailDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDetailDal dal = dalContext.GetDal<IReportComponentDetailDal>())
                    {
                        foreach (ReportComponentDto componentDto in _reportComponentDtos)
                        {
                            List<ReportComponentDetailDto> dalDtos = dal.FetchByReportComponentId(componentDto.Id).ToList();
                            List<ReportComponentDetailDto> sourceDtosInSection = sourceDtoList.Where(r => r.ReportComponentId == componentDto.Id).ToList();

                            Assert.AreEqual(sourceDtosInSection.Count, dalDtos.Count, "Correct number of items should be returned");

                            foreach (ReportComponentDetailDto sourceDto in sourceDtosInSection)
                            {
                                ReportComponentDetailDto dalDto = dalDtos.FirstOrDefault(s => s.Id == sourceDto.Id);
                                Assert.IsNotNull(dalDto);
                                Assert.AreEqual(dalDto, sourceDto);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDetailDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDetailDal dal = dalContext.GetDal<IReportComponentDetailDal>())
                    {
                        foreach (ReportComponentDetailDto sourceDto in sourceDtoList)
                        {
                            ReportComponentDetailDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);
                        }
                    }
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be updated
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            Int32 keySeed = 1;
            
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDetailDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDetailDal dal = dalContext.GetDal<IReportComponentDetailDal>())
                    {
                        foreach (ReportComponentDetailDto sourceDto in sourceDtoList)
                        {
                            ReportComponentDetailDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);

                            // update the compare dto
                            dalDto.Key = "Key" + keySeed.ToString();
                            dalDto.Value = keySeed.ToString();
                            dal.Update(dalDto);

                            dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreNotEqual(sourceDto, dalDto);
                            Assert.AreEqual("Key" + keySeed.ToString(), dalDto.Key);
                            Assert.AreEqual(keySeed.ToString(), dalDto.Value);
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Tests that a product universe can be deleted
        /// </summary>
        /// <param name="dalFactoryType">The dal factory to test</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestDeleteByReportComponentId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportComponentDetailDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportComponentDetailDal dal = dalContext.GetDal<IReportComponentDetailDal>())
                    {
                        foreach (ReportComponentDetailDto sourceDto in sourceDtoList)
                        {
                            // delete the dto
                            dal.DeleteByReportComponentId(sourceDto.ReportComponentId);

                            ReportComponentDetailDto returnedDto;
                            Assert.Throws<DtoDoesNotExistException>
                                (() => { returnedDto = dal.FetchById(sourceDto.Id); },
                                "Deleted child items should not be returned when fetched by Id");
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

    }
}
