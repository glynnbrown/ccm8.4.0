﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24265 : N.Haywood
//		Created
// V8-27940 : L.Luong
//      Test now use the Mssql dalfactory
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LabelDal : TestBase<ILabelDal, LabelDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.User.DalFactory)
            
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestFetchById(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestFetchByProperties(
                       dalFactoryType,
                       "FetchByEntityIdName",
                       new List<String>() { "EntityId", "Name" },
                       _excludeProperties,
                       fetchIncludesDeleted: true);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties, false);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestLockById(Type dalFactoryType)
        {
            throw new InconclusiveException("Cannot test file locking");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUnlockById(Type dalFactoryType)
        {
            throw new InconclusiveException("Cannot test file locking");
        }

    }
}
