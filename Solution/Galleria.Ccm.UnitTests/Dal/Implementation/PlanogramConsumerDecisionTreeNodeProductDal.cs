﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{

    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramConsumerDecisionTreeNodeProductDal : TestBase<IPlanogramConsumerDecisionTreeNodeProductDal, PlanogramConsumerDecisionTreeNodeProductDto>
    {
        private List<String> _excludeProperties = new List<String> { "ExtendedData" };

        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
        };

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByPlanogramConsumerDecisionTreeNodeId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType,
                "FetchByPlanogramConsumerDecisionTreeNodeId", "PlanogramConsumerDecisionTreeNodeId", _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludeProperties);
        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }


    }

    //[TestFixture]
    //public sealed class PlanogramConsumerDecisionTreeNodeProductDal : TestBase<IPlanogramConsumerDecisionTreeNodeProductDal, PlanogramConsumerDecisionTreeNodeProductDto>
    //{
    //    #region Fields

    //    private List<Type> _dbDalFactoryTypes = new List<Type>()
    //    {
    //        typeof(Galleria.Ccm.Dal.Mssql.DalFactory)
    //    };
    //    private List<String> _excludeProperties = new List<String>() { "ExtendedData" };
    //    private List<PlanogramConsumerDecisionTreeNodeProductDto> _sourceDtoList;
    //    private List<PlanogramDto> _parentPlanogramDtoList;
    //    private new DtoSpec _dtoSpec;
    //    private List<PropertyInfo> _properties;

    //    #endregion

    //    private void InitializeDtosAndProperties(IDalContext dalContext)
    //    {
    //        _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramConsumerDecisionTreeNodeProductDto));
    //        _properties = typeof(PlanogramConsumerDecisionTreeNodeProductDto).GetProperties().Where(p => p.CanWrite).ToList();

    //        //Insert parents
    //        List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, 25);
    //        _parentPlanogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, 1, packageDtoList);
    //        var parentPlanogramProduct = TestDataHelper.InsertPlanogramProductDtos(dalContext, _parentPlanogramDtoList);
    //        var parentPlanogramConsumerDecisionTree = TestDataHelper.InsertPlanogramConsumerDecisionTreeDtos(dalContext, _parentPlanogramDtoList)[0];
    //        var parentPlanogramConsumerDecisionTreeLevelList = TestDataHelper.InsertPlanogramConsumerDecisionTreeLevelDtos(dalContext, parentPlanogramConsumerDecisionTree, 3, true);
    //        var parentPlanogramConsumerDecisionTreeNodeList = TestDataHelper.InsertPlanogramConsumerDecisionTreeNodeDtos(dalContext, parentPlanogramConsumerDecisionTree, parentPlanogramConsumerDecisionTreeLevelList, 5, /*addRoot*/true);
    //        _sourceDtoList = TestDataHelper.InsertPlanogramConsumerDecisionTreeNodeProductDtos(dalContext, parentPlanogramConsumerDecisionTreeNodeList, parentPlanogramProduct, 3);
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public void TestFetchByPlanogramConsumerDecisionTreeNodeId(Type dbDalFactoryType)
    //    {
    //        using (var dalFactory = CreateDalFactory(dbDalFactoryType))
    //        using (var dalContext = dalFactory.CreateContext())
    //        using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeProductDal>())
    //        {
    //            InitializeDtosAndProperties(dalContext);
    //            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
    //            {
    //                var returnedDtoList = dal.FetchByPlanogramConsumerDecisionTreeNodeId(_sourceDtoList[i].PlanogramConsumerDecisionTreeNodeId);
    //                var returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
    //                foreach (var property in _properties)
    //                {
    //                    if (!_excludeProperties.Contains(property.Name))
    //                    {
    //                        Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public void TestInsert(Type dbDalFactoryType)
    //    {
    //        // The Insert method is tested by TestFetchByPlanogramConsumerDecisionTreeNodeId.
    //        TestFetchByPlanogramConsumerDecisionTreeNodeId(dbDalFactoryType);
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public void TestUpdate(Type dbDalFactoryType)
    //    {
    //        using (var dalFactory = CreateDalFactory(dbDalFactoryType))
    //        using (var dalContext = dalFactory.CreateContext())
    //        using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeProductDal>())
    //        {
    //            InitializeDtosAndProperties(dalContext);
    //            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
    //            {
    //                var originalSourceDto = _sourceDtoList[i];
    //                var alternativeSourceDto = _sourceDtoList[i];

    //                // Update the DTO in memory.
    //                foreach (var property in _properties.Where(
    //                    p => p.Name != "Id" && p.Name != "PlanogramConsumerDecisionTreeNodeId" && !_excludeProperties.Contains(p.Name)))
    //                {
    //                    property.SetValue(
    //                        originalSourceDto,
    //                        property.GetValue(alternativeSourceDto, null),
    //                        null);
    //                }

    //                // Update the DTO in database and verify success.
    //                dal.Update(originalSourceDto);
    //                var returnedDtoList = dal.FetchByPlanogramConsumerDecisionTreeNodeId(originalSourceDto.PlanogramConsumerDecisionTreeNodeId);
    //                var returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(originalSourceDto.Id));

    //                foreach (var property in _properties.Where(p => !_excludeProperties.Contains(p.Name)))
    //                {
    //                    Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
    //                }
    //            }
    //        }
    //    }

    //    [Test]
    //    [TestCaseSource("_dbDalFactoryTypes")]
    //    public new void TestDeleteById(Type dbDalFactoryType)
    //    {
    //        using (var dalFactory = CreateDalFactory(dbDalFactoryType))
    //        using (var dalContext = dalFactory.CreateContext())
    //        using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeProductDal>())
    //        {
    //            InitializeDtosAndProperties(dalContext);
    //            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
    //            {
    //                dal.DeleteById(_sourceDtoList[i].Id);
    //                var returnedDtoList = dal.FetchByPlanogramConsumerDecisionTreeNodeId(_sourceDtoList[i].PlanogramConsumerDecisionTreeNodeId);
    //                var returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
    //                Assert.IsNull(returnedDto, "Dto should be deleted");
    //            }
    //        }
    //    }
    //}
}
