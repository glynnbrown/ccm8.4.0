﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public sealed class PlanogramSequenceDal : TestBase<IPlanogramSequenceDal, PlanogramSequenceDto>
    {
        private static readonly List<String> ExcludedProperties = new List<String> {"ExtendedData"};

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PlanogramSequenceDal() : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dalFactoryType)
        {
            const String fetchMethodName = "FetchByPlanogramId";
            const String propertyName = "PlanogramId";
            TestFetchByProperty(dalFactoryType, fetchMethodName, propertyName, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            TestUpdate(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
