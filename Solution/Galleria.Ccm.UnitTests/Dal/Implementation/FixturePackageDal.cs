﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion


using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class FixturePackageDal : FixtureTestBase<IFixturePackageDal, FixturePackageDto>
    {
        internal override Ccm.Dal.Pogfx.SectionType SectionType
        {
            get { return Ccm.Dal.Pogfx.SectionType.FixturePackage; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "Id";
            }
        }


        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            //Tested by base.
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            //Tested by base.
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            //Tested by base.
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            //Tested by base.
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUnlockById(Type dalFactoryType)
        {
            //Tested by base.
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestLockById(Type dalFactoryType)
        {
            //Tested by base.
        }
    }
}
