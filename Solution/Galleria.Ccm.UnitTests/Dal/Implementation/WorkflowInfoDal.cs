﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using System.Collections.Generic;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class WorkflowInfoDal : TestBase<IWorkflowInfoDal, WorkflowInfoDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            base.TestInfoFetchByEntityId<WorkflowDto>(dalFactoryType);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityIdIncludingDeleted(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mssql.DalFactory))
            {
                Assert.Inconclusive("TODO: Behaviour changed, items now deleted outright");
            }

            base.TestInfoFetchByProperty<WorkflowDto>(dalFactoryType,
                "FetchByEntityIdIncludingDeleted", "EntityId", true);
        }

    }
}
