﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ReportDal : TestBase<IReportDal, ReportDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Helpers
        private void AssertDtoAndDtoAreEqual(ReportDto sourceDto, ReportDto dto)
        {
            Assert.AreEqual(sourceDto.Name, dto.Name);
            Assert.AreEqual(sourceDto.Description, dto.Description);
            Assert.AreEqual(sourceDto.DataModelId, dto.DataModelId);
            Assert.AreEqual(sourceDto.Id, dto.Id);
            Assert.AreEqual(sourceDto.RowVersion, dto.RowVersion);
            Assert.AreEqual(sourceDto.PaperSize, dto.PaperSize);
            Assert.AreEqual(sourceDto.PaperOrientation, dto.PaperOrientation);
            Assert.AreEqual(sourceDto.PaperMarginTop, dto.PaperMarginTop);
            Assert.AreEqual(sourceDto.PaperMarginLeft, dto.PaperMarginLeft);
            Assert.AreEqual(sourceDto.PaperMarginBottom, dto.PaperMarginBottom);
            Assert.AreEqual(sourceDto.PaperMarginRight, dto.PaperMarginRight);
            Assert.AreEqual(
                RoundToNearest((DateTime)sourceDto.DateLastExecuted, TimeSpan.FromMinutes(1)),
                RoundToNearest((DateTime)dto.DateLastExecuted, TimeSpan.FromMinutes(1)));
        }

        public DateTime RoundToNearest(DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            bool roundUp = delta > d.Ticks / 2;

            if (roundUp)
            {
                var deltaU = (d.Ticks - (dt.Ticks % d.Ticks)) % d.Ticks;
                return new DateTime(dt.Ticks + deltaU);
            }
            else
            {
                var deltaD = dt.Ticks % d.Ticks;
                return new DateTime(dt.Ticks - deltaD);
            }
        }
        #endregion
        
        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                IEnumerable<ReportDto> sourceDtoList = TestDataHelper.InsertPopulatedReportDtos(dalFactory, 5);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportDal dal = dalContext.GetDal<IReportDal>())
                    {
                        foreach (ReportDto sourceDto in sourceDtoList)
                        {
                            ReportDto dalDto = dal.FetchById(sourceDto.Id);
                            AssertDtoAndDtoAreEqual(dalDto, sourceDto);

                            //Test return following deletion
                            dal.DeleteById(dalDto.Id);

                            ReportDto returnedDto;
                            Assert.DoesNotThrow(() =>
                                {
                                    returnedDto = dal.FetchById(dalDto.Id);
                                },
                                "Deleted items should be returned when fetched by Id");

                            returnedDto = dal.FetchById(dalDto.Id);

                            Assert.IsNotNull(returnedDto.DateDeleted, "The DataDeleted field should not be null for deleted items");

                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchAll(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                IEnumerable<ReportDto> sourceDtoList = TestDataHelper.InsertPopulatedReportDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportDal dal = dalContext.GetDal<IReportDal>())
                    {
                        List<ReportDto> returnedDtos = dal.FetchAll().ToList();
                        foreach (ReportDto sourceDto in sourceDtoList)
                        {
                            ReportDto returnedDto = returnedDtos.FirstOrDefault(r => r.Id.Equals(sourceDto.Id));
                            Assert.IsNotNull(returnedDto);
                            AssertDtoAndDtoAreEqual(sourceDto, returnedDto);
                        }
                    }
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                IEnumerable<ReportDto> dtoList = TestDataHelper.InsertPopulatedReportDtos(dalFactory, 2);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IReportDal dal = dalContext.GetDal<IReportDal>())
                    {
                        foreach (ReportDto sourceDto in dtoList)
                        {
                            // fetch the dto
                            ReportDto compareDto = dal.FetchById(sourceDto.Id);

                            // and assert that they are equal
                            AssertDtoAndDtoAreEqual(sourceDto, compareDto);
                        }
                    }
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            Int32 newItemNumber = 1; 
            
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                IEnumerable<ReportDto> dtoList = TestDataHelper.InsertPopulatedReportDtos(dalFactory, 2);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IReportDal dal = dalContext.GetDal<IReportDal>())
                    {
                        foreach (ReportDto sourceDto in dtoList)
                        {
                            // fetch the dto
                            ReportDto compareDto = dal.FetchById(sourceDto.Id);

                            // and assert that they are equal
                            AssertDtoAndDtoAreEqual(sourceDto, compareDto);

                            // update the compare dto
                            String newName = "TeST " + newItemNumber.ToString();
                            compareDto.Name = newName;
                            compareDto.PaperMarginBottom = newItemNumber;
                            compareDto.PaperMarginTop = newItemNumber;
                            compareDto.PaperMarginLeft = newItemNumber;
                            compareDto.PaperMarginRight = newItemNumber;
                            
                            dal.Update(compareDto);

                            compareDto = dal.FetchById(sourceDto.Id);
                            Assert.AreNotEqual(sourceDto, compareDto);
                            Assert.AreEqual(newName, compareDto.Name);
                            Assert.AreEqual(Convert.ToDouble(newItemNumber), compareDto.PaperMarginBottom);
                            Assert.AreEqual(Convert.ToDouble(newItemNumber), compareDto.PaperMarginTop);
                            Assert.AreEqual(Convert.ToDouble(newItemNumber), compareDto.PaperMarginLeft);
                            Assert.AreEqual(Convert.ToDouble(newItemNumber), compareDto.PaperMarginRight);

                            newItemNumber++;
                        }
                    }
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestDeleteById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                IEnumerable<ReportDto> dtoList = TestDataHelper.InsertPopulatedReportDtos(dalFactory, 2);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IReportDal dal = dalContext.GetDal<IReportDal>())
                    {
                        foreach (ReportDto sourceDto in dtoList)
                        {
                            //Try deleting the item
                            dal.DeleteById(sourceDto.Id);

                            ReportDto returnedDto;

                            //Test return following deletion
                            Assert.DoesNotThrow(() => { returnedDto = dal.FetchById(sourceDto.Id); },
                                "Deleted items should be returned when fetched by Id");
                            returnedDto = dal.FetchById(sourceDto.Id);
                            Assert.IsNotNull(returnedDto.DateDeleted, "The DataDeleted field should not be null for deleted items");
                        }
                    }
                }
            }
        }
        #endregion
    }
}
