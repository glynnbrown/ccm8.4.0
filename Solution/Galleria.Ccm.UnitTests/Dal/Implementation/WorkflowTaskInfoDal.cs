﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using System.Collections.Generic;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class WorkflowTaskInfoDal : TestBase<IWorkflowTaskInfoDal, WorkflowTaskInfoDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByWorkflowId(Type dalFactoryType)
        {
            base.TestInfoFetchByProperty<WorkflowTaskDto>(dalFactoryType,
                "FetchByWorkflowId", "WorkflowId", false);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByWorkpackageId(Type dalFactoryType)
        {
            using(var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using(var dalContext = dalFactory.CreateContext())
                {
                    List<EntityDto> entDtoList = TestDataHelper.InsertEntityDtos(dalContext,1);
                    List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entDtoList,1);
                    List<UserDto> userDtoList = TestDataHelper.InsertUserDtos(dalContext, 1);
                    List<WorkpackageDto> workpackageDtoList = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList,userDtoList);
                    List<WorkflowTaskDto> workflowTaskDtoList = TestDataHelper.InsertWorkflowTaskDtos(dalContext, workpackageDtoList);

                    using (var dal = dalContext.GetDal<IWorkflowTaskInfoDal>())
                    {
                        //Ensure it returns expected id's
                        IEnumerable<WorkflowTaskInfoDto> returnedDtoList = dal.FetchByWorkpackageId((Int32)workflowTaskDtoList.First().WorkflowId);
                        Assert.AreEqual(returnedDtoList.Count(), workflowTaskDtoList.Count());
                    }
                }
            }
        }
    }
}
