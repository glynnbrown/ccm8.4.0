﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25628 : A. Kuszyk
//      Initial version.
// V8-27918 : J.Pickup
//      Introduced TestFetchByWorkpackagePlanogramIdIncludingDeleted & TestFetchByWorkpackageIdIncludingDeleted
// V8-26322 : A.Silva
//      Amended TestFetchByWorkpackageIdIncludingDeleted so that it does not duplicate the user record when setting up the test
//          and that it compares the result against the number of set locations.
//      Amended TestFetchByWorkpackagePlanogramIdIncludingDeleted so that it does not duplicate the user record when setting up the test.

#endregion
#region Version History: (CCM 8.1.0)
// V8-29667 : N.Haywood
//  Removed FetchAllIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationInfoDal : TestBase<ILocationInfoDal, LocationInfoDto>
    {
        public LocationInfoDal()
            : base(typeof (Galleria.Ccm.Dal.Mock.DalFactory), typeof (Galleria.Ccm.Dal.Mssql.DalFactory)) {}

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<LocationDto>(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdIncludingDeleted(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperty<LocationDto>(dbDalFactoryType,
                "FetchByEntityIdIncludingDeleted", "EntityId", true);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageIdIncludingDeleted(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            {
                //Insert all dto's required for test.
                var entityDtos = TestDataHelper.InsertEntityDtos(dalContext, 1);
                var entityId = entityDtos.First().Id;
                var userDtoList = TestDataHelper.InsertUserDtos(dalFactory, 1);
                var userName = userDtoList.First().UserName;
                var locationDtos = TestDataHelper.InsertLocationDtos(dalFactory, 1, entityId);
                var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
                List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 10);
                List<PlanogramDto> planogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
                List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId);
                List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDtoList.First().Id, 1);
                List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevelDtoList, 1);
                List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entityDtos, 1);
                var workPackageDtos = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList, userDtoList);
                var workpackagePlanogramDtos = TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext, workPackageDtos, planogramDtoList);
                List<LocationPlanAssignmentDto> locationPlanAssignmentDtos = TestDataHelper.InsertLocationPlanAssignmentDtos(dalFactory, locationDtos, workpackagePlanogramDtos, productGroupDtoList, 1, entityId);
                var dtos = new List<LocationDto>();
                foreach (var locationDto in locationDtos)
                {
                    var locationId = locationDto.Id;
                    foreach (var locationWorkpackageId in locationPlanAssignmentDtos
                        .Where(o => o.LocationId == locationId)
                        .Select(locationPlanAssignment => locationPlanAssignment.PlanogramId)
                        .SelectMany(destinationPlanogramId => workpackagePlanogramDtos
                            .Where(o => o.DestinationPlanogramId == destinationPlanogramId)
                            .Select(locationWorkpackagePlanogram => locationWorkpackagePlanogram.WorkpackageId))) 
                    {
                        dtos.AddRange(workPackageDtos.Where(o => o.Id == locationWorkpackageId).Select(locationWorkpackage => locationDto));
                    }
                }
                var expected = dtos.Count();

                using (var dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    //Ensure returns expected id's
                    IEnumerable<LocationInfoDto> returnedDtoList = dal.FetchByWorkpackageIdIncludingDeleted((Int32) workPackageDtos.First().Id);

                    //Should return more than one location (Based on our test conditions that each location is different).
                    var actual = returnedDtoList.Count();
                    Assert.AreEqual(expected, actual);
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackagePlanogramIdIncludingDeleted(Type dbDalFactoryType)
        {
            //base.TestInfoFetchByProperty<LocationDto>(dbDalFactoryType,
            //    "FetchByWorkpackagePlanogramIdIncludingDeleted", "PlanogramId", true);

            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            {
                //Insert all dto's required for test.
                List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(dalContext, 1);
                List<UserDto> userDtoList = TestDataHelper.InsertUserDtos(dalFactory, 20);
                var userName = userDtoList.First().UserName;
                List<LocationDto> locationDtoList = TestDataHelper.InsertLocationDtos(dalFactory, 1, entityDtoList.First().Id);
                Int32 entityId = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
                List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entityId, 10);
                List<PlanogramDto> planogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
                List<ProductHierarchyDto> productHierarchyDtoList = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDtoList.First().Id);
                List<ProductLevelDto> productLevelDtoList = TestDataHelper.InsertProductLevelDtos(dalFactory, productHierarchyDtoList.First().Id, 1);
                List<ProductGroupDto> productGroupDtoList = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevelDtoList, 1);
                List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entityDtoList, 1);
                List<WorkpackageDto> workpackageDtoList = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList, userDtoList);
                List<WorkpackagePlanogramDto> workpackagePlanogramDtoList = TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext, workpackageDtoList, planogramDtoList);
                TestDataHelper.InsertLocationPlanAssignmentDtos(dalFactory, locationDtoList, workpackagePlanogramDtoList, productGroupDtoList, 5, entityDtoList.First().Id);

                //WorkpackagePlanogram generates a new id for planogram during insert. This is the planogram id we need to check against in our checks:


                using (var dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    for (Int32 i = 0; i < 5; i++)
                    {
                        //Ensure returns expected id's
                        IEnumerable<LocationInfoDto> returnedDtoList =
                            dal.FetchByWorkpackagePlanogramIdIncludingDeleted(
                                (Int32) workpackagePlanogramDtoList[i].DestinationPlanogramId);
                        var expected = locationDtoList[i].Id;
                        LocationInfoDto returnedDto =
                            returnedDtoList.FirstOrDefault(p => p.Id.Equals(expected));

                        //Will be null if not found, but should always be found with the current test conditions.
                        Assert.IsNotNull(returnedDto, "As per the test conditions there should always be a found dto, but none was found.");
                        var actual = returnedDto.Id;
                        Assert.AreEqual(expected, actual);
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdLocationCodes(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperties<LocationDto, String>(
                dbDalFactoryType,
                "FetchByEntityIdLocationCodes",
                new List<String> { "EntityId" },
                "Code");
        }

    }
}
