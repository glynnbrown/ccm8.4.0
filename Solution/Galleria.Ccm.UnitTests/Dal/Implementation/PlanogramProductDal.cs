﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)

// V8-25478 : A. Kuszyk
//      Initial version.
// V8-25881 : A.Probyn
//      Updated so that planogram dal unit tests work using manual data insertion
// V8-26322 : A.Silva
//      Amended how User records are inserted to avoid random duplicates.

#endregion
#region Version History: (CCM V8.2)

// V8-30763 : I.George
// Added BulkInsert test
#endregion

#region Version History: (CCM V8.3.0)
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramProductDal : TestBase<IPlanogramProductDal,PlanogramProductDto>
    {
        #region Fields
        private readonly List<String> _excludeProperties = new List<String>() { "ExtendedData", "PegProngOffset" };
        private List<PlanogramProductDto> _sourceDtoList;
        private List<PlanogramDto> _parentPlanogramDtoList;
        private List<PropertyInfo> _properties;
        #endregion

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramProductDto));
            _properties = typeof(PlanogramProductDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 25);
            _parentPlanogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
            _sourceDtoList = TestDataHelper.InsertPlanogramProductDtos(dalContext, _parentPlanogramDtoList);
        }

        #region Constructor

        public PlanogramProductDal() : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestFetchByProperty(
                    dalFactoryType,
                    "FetchByPlanogramId",
                    "PlanogramId",
                    _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IPlanogramProductDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            IEnumerable<PlanogramProductDto> returnedDtoList = dal.FetchByPlanogramId(_sourceDtoList[i].PlanogramId);
                            PlanogramProductDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestInsert(
                dalFactoryType,
                _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IPlanogramProductDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            IEnumerable<PlanogramProductDto> returnedDtoList = dal.FetchByPlanogramId(_sourceDtoList[i].PlanogramId);
                            PlanogramProductDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestUpdate(
                dalFactoryType,
                _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IPlanogramProductDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            var originalSourceDto = _sourceDtoList[0];
                            var alternativeSourceDto = _sourceDtoList[i];

                            // Update the DTO in memory.
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
                                {
                                    property.SetValue(
                                        originalSourceDto,
                                        property.GetValue(alternativeSourceDto, null),
                                        null);
                                }
                            }

                            // Update the DTO in database and verify success.
                            dal.Update(originalSourceDto);
                            IEnumerable<PlanogramProductDto> returnedDtoList = dal.FetchByPlanogramId(originalSourceDto.PlanogramId);
                            PlanogramProductDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(originalSourceDto.Id));

                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestDeleteById(dalFactoryType);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IPlanogramProductDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            IEnumerable<PlanogramProductDto> returnedDtoList = dal.FetchByPlanogramId(_sourceDtoList[i].PlanogramId);
                            PlanogramProductDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                            Assert.IsNotNull(returnedDto, "Dto should not be removed, only its child elements are");
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }
    }
}
