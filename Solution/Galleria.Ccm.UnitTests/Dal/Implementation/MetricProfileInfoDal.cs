﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class MetricProfileInfoDal : TestBase<IMetricProfileInfoDal, MetricProfileInfoDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            base.TestInfoFetchByEntityId<MetricProfileDto>(dalFactoryType);
        }

    }
}
