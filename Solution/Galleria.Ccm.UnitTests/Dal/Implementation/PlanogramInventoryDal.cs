﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27647 : L.Luong 
//   Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramInventoryDal : TestBase<IPlanogramInventoryDal, PlanogramInventoryDto>
    {
        private List<Type> _dalFactoryTypes = new List<Type>()
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory) // Mock is tested in framework. 
        };
        private List<String> _excludeProperties = new List<String>() { "ExtendedData" };

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dalFactoryType)
        {
            base.TestFetchByProperties(dalFactoryType, "FetchByPlanogramId", new List<String>() { "PlanogramId" }, _excludeProperties);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, _excludeProperties);
        }


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, _excludeProperties);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
