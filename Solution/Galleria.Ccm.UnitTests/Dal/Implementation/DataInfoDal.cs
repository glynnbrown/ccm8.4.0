﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM803)
// CCM-29135 : D.Pleasance
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Reflection;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class DataInfoDal : TestBase<IDataInfoDal, DataInfoDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    // Insert Entity
                    using (IEntityDal entityDal = dalContext.GetDal<IEntityDal>())
                    {
                        EntityDto entityDto = new EntityDto()
                        {
                            Name = "Test"
                        };
                        entityDal.Insert(entityDto);
                    }

                    using (IDataInfoDal dal = dalContext.GetDal<IDataInfoDal>())
                    {
                        DataInfoDto dto = dal.FetchByEntityId(1);
                        Assert.IsFalse(dto.HasProductHierarchy);
                        Assert.IsFalse(dto.HasLocationHierarchy);
                        Assert.IsFalse(dto.HasLocation);
                        Assert.IsFalse(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsFalse(dto.HasLocationSpace);
                        Assert.IsFalse(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsFalse(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);
                        
                        HashSet<String> testedProperties = new HashSet<String>();
                        testedProperties.Add("EntityId");
                        testedProperties.Add("HasProductHierarchy");
                        testedProperties.Add("HasLocationHierarchy");
                        testedProperties.Add("HasLocation");
                        testedProperties.Add("HasProduct");
                        testedProperties.Add("HasAssortment");
                        testedProperties.Add("HasLocationSpace");
                        testedProperties.Add("HasLocationSpaceBay");
                        testedProperties.Add("HasLocationSpaceElement");
                        testedProperties.Add("HasClusterScheme");
                        testedProperties.Add("HasLocationProductAttribute");
                        testedProperties.Add("HasLocationProductIllegal");
                        // This test just verifies that if a developer has added a property to DataInfoDto, that he or
                        // she has thought to update the unit test accordingly, since in contrast to other tests, this
                        // test does not automatically figure out what test data is required.  Of course you could add
                        // the new property to testedProperties without adding a test below, but please don't!
                        foreach (PropertyInfo property in dto.GetType().GetProperties())
                        {
                            Assert.IsTrue(
                                testedProperties.Contains(property.Name),
                                String.Format("No check for the {0} property found.", property.Name));
                        }

                        // Insert ClusterScheme
                        using (IClusterSchemeDal clusterSchemeDal = dalContext.GetDal<IClusterSchemeDal>())
                        {
                            ClusterSchemeDto clusterSchemeDto = new ClusterSchemeDto()
                            {
                                EntityId = 1,
                                Name = "Test"
                            };
                            clusterSchemeDal.Insert(clusterSchemeDto);
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsFalse(dto.HasProductHierarchy);
                        Assert.IsFalse(dto.HasLocationHierarchy);
                        Assert.IsFalse(dto.HasLocation);
                        Assert.IsFalse(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsFalse(dto.HasLocationSpace);
                        Assert.IsFalse(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert LocationHierarchy
                        using (ILocationHierarchyDal locationHierarchyDal = dalContext.GetDal<ILocationHierarchyDal>())
                        {
                            LocationHierarchyDto locationHierarchyDto = new LocationHierarchyDto()
                            {
                                EntityId = 1,
                                Name = "Test",
                            };
                            locationHierarchyDal.Insert(locationHierarchyDto);
                        }                        
                        using (ILocationLevelDal locationLevelDal = dalContext.GetDal<ILocationLevelDal>())
                        {
                            using (ILocationGroupDal locationGroupDal = dalContext.GetDal<ILocationGroupDal>())
                            {
                                LocationLevelDto locationLevelDto1 = new LocationLevelDto()
                                {
                                    LocationHierarchyId = 1,
                                    Name = "Test1"
                                };
                                locationLevelDal.Insert(locationLevelDto1);
                                // Just a root level is not enough.
                                dto = dal.FetchByEntityId(1);
                                Assert.IsFalse(dto.HasLocationHierarchy);

                                LocationGroupDto locationGroupDto1 = new LocationGroupDto()
                                {
                                    LocationHierarchyId = 1,
                                    LocationLevelId = 1,
                                    Code = "Test1",
                                    Name = "Test1"
                                };
                                locationGroupDal.Insert(locationGroupDto1);
                                // Even with a group, a root level is still not enough.
                                dto = dal.FetchByEntityId(1);
                                Assert.IsFalse(dto.HasLocationHierarchy);

                                LocationLevelDto locationLevelDto2 = new LocationLevelDto()
                                {
                                    LocationHierarchyId = 1,
                                    Name = "Test2",
                                    ParentLevelId = 1
                                };
                                locationLevelDal.Insert(locationLevelDto2);
                                // A leaf level without a group is not enough.
                                dto = dal.FetchByEntityId(1);
                                Assert.IsFalse(dto.HasLocationHierarchy);

                                LocationGroupDto locationGroupDto2 = new LocationGroupDto()
                                {
                                    LocationHierarchyId = 1,
                                    LocationLevelId = 2,
                                    Code = "Test2",
                                    Name = "Test2"
                                };
                                locationGroupDal.Insert(locationGroupDto2);
                            }
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsFalse(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsFalse(dto.HasLocation);
                        Assert.IsFalse(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsFalse(dto.HasLocationSpace);
                        Assert.IsFalse(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        //location group (location has FK)
                        using (ILocationGroupDal locationGroupDal = dalContext.GetDal<ILocationGroupDal>())
                        {
                            LocationGroupDto locationGroupDto = new LocationGroupDto()
                            {
                                Code = "testLocationGroupCode",
                                Id = 1,
                                LocationHierarchyId = 1,
                                LocationLevelId = 2,
                                Name = "testLocationGroupName"
                            };
                            locationGroupDal.Insert(locationGroupDto);
                        }

                        // Insert Location
                        using (ILocationDal locationDal = dalContext.GetDal<ILocationDal>())
                        {
                            LocationDto locationDto = new LocationDto()
                            {
                                EntityId = 1,
                                Name = "testLocationName",
                                Code = "testLocationCode",
                                LocationGroupId = 1,
                                DateCreated = DateTime.Now,
                                DateLastModified = DateTime.Now
                            };
                            locationDal.Insert(locationDto);
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsFalse(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsFalse(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsFalse(dto.HasLocationSpace);
                        Assert.IsFalse(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert Product
                        using (IProductDal productDal = dalContext.GetDal<IProductDal>())
                        {
                            ProductDto productDto = new ProductDto()
                            {
                                EntityId = 1,
                                Gtin = "Test",
                                Name = "Test"
                            };
                            productDal.Insert(productDto);
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsFalse(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsFalse(dto.HasLocationSpace);
                        Assert.IsFalse(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert ProductHierarchy
                        using (IProductHierarchyDal productHierarchyDal = dalContext.GetDal<IProductHierarchyDal>())
                        {
                            ProductHierarchyDto productHierarchyDto = new ProductHierarchyDto()
                            {
                                EntityId = 1,
                                Name = "Test"
                            };
                            productHierarchyDal.Insert(productHierarchyDto);
                        }
                        // Just a hierarchy is not enough.
                        Assert.IsFalse(dto.HasProductHierarchy);
                        using (IProductLevelDal productLevelDal = dalContext.GetDal<IProductLevelDal>())
                        {
                            using (IProductGroupDal productGroupDal = dalContext.GetDal<IProductGroupDal>())
                            {
                                ProductLevelDto productLevelDto1 = new ProductLevelDto()
                                {
                                    ProductHierarchyId = 1,
                                    Name = "Test1"
                                };
                                productLevelDal.Insert(productLevelDto1);
                                // Just a root level is not enough.
                                dto = dal.FetchByEntityId(1);
                                Assert.IsFalse(dto.HasProductHierarchy);

                                ProductGroupDto productGroupDto1 = new ProductGroupDto()
                                {
                                    ProductHierarchyId = 1,
                                    ProductLevelId = 1,
                                    Code = "Test1",
                                    Name = "Test1"
                                };
                                productGroupDal.Insert(productGroupDto1);
                                // Even with a group, a root level is still not enough.
                                dto = dal.FetchByEntityId(1);
                                Assert.IsFalse(dto.HasProductHierarchy);

                                ProductLevelDto productLevelDto2 = new ProductLevelDto()
                                {
                                    ProductHierarchyId = 1,
                                    Name = "Test2",
                                    ParentLevelId = 1
                                };
                                productLevelDal.Insert(productLevelDto2);
                                // A leaf level without a group is not enough.
                                dto = dal.FetchByEntityId(1);
                                Assert.IsFalse(dto.HasProductHierarchy);

                                ProductGroupDto productGroupDto2 = new ProductGroupDto()
                                {
                                    ProductHierarchyId = 1,
                                    ProductLevelId = 2,
                                    Code = "Test2",
                                    Name = "Test2",
                                    ParentGroupId = 1
                                };
                                productGroupDal.Insert(productGroupDto2);
                            }
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsTrue(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsFalse(dto.HasLocationSpace);
                        Assert.IsFalse(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert location space 
                        LocationSpaceDto locationSpaceDto = new LocationSpaceDto();
                        using (ILocationSpaceDal locationSpaceDal = dalContext.GetDal<ILocationSpaceDal>())
                        {
                            locationSpaceDto.LocationId = 1;
                            locationSpaceDto.EntityId = 1;
                            locationSpaceDal.Insert(locationSpaceDto);
                        }
                        LocationSpaceProductGroupDto locationSpaceProductGroupDto = new LocationSpaceProductGroupDto();
                        using (ILocationSpaceProductGroupDal locationSpaceProductGroupDal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                        {
                            locationSpaceProductGroupDto.ProductGroupId = 1;
                            locationSpaceProductGroupDto.LocationSpaceId = locationSpaceDto.Id;
                            locationSpaceProductGroupDal.Insert(locationSpaceProductGroupDto);
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsTrue(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsTrue(dto.HasLocationSpace);
                        Assert.IsFalse(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert location space bay
                        LocationSpaceBayDto locationSpaceBayDto = new LocationSpaceBayDto();
                        using (ILocationSpaceBayDal locationSpaceBayDal = dalContext.GetDal<ILocationSpaceBayDal>())
                        {
                            locationSpaceBayDto.LocationSpaceProductGroupId = locationSpaceProductGroupDto.Id;
                            locationSpaceBayDto.Order = 1;

                            locationSpaceBayDal.Insert(locationSpaceBayDto);
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsTrue(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsTrue(dto.HasLocationSpace);
                        Assert.IsTrue(dto.HasLocationSpaceBay);
                        Assert.IsFalse(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert location space element
                        LocationSpaceElementDto locationSpaceElementDto = new LocationSpaceElementDto();
                        using (ILocationSpaceElementDal locationSpaceElementDal = dalContext.GetDal<ILocationSpaceElementDal>())
                        {
                            locationSpaceElementDto.LocationSpaceBayId = locationSpaceBayDto.Id;
                            locationSpaceElementDto.Order = 1;
                            locationSpaceElementDal.Insert(locationSpaceElementDto);
                        }
                        dto = dal.FetchByEntityId(1);
                        Assert.IsTrue(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsFalse(dto.HasAssortment);
                        Assert.IsTrue(dto.HasLocationSpace);
                        Assert.IsTrue(dto.HasLocationSpaceBay);
                        Assert.IsTrue(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);


                        // Insert Assortment
                        using (IAssortmentDal assortmentDal = dalContext.GetDal<IAssortmentDal>())
                        {
                            AssortmentDto assortmentDto = new AssortmentDto()
                            {
                                EntityId = 1,
                                ProductGroupId = 1,
                                Name = "Test"
                            };
                            assortmentDal.Insert(assortmentDto);
                        }

                        dto = dal.FetchByEntityId(1);
                        Assert.IsTrue(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsTrue(dto.HasAssortment);
                        Assert.IsTrue(dto.HasLocationSpace);
                        Assert.IsTrue(dto.HasLocationSpaceBay);
                        Assert.IsTrue(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsFalse(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert Location Product Attribute
                        using (ILocationProductAttributeDal locationProductAttributeDal = dalContext.GetDal<ILocationProductAttributeDal>())
                        {
                            LocationProductAttributeDto locationProductAttributeDto = new LocationProductAttributeDto()
                            {
                                EntityId = 1,
                                ProductId = 1,
                                LocationId = 1
                            };
                            locationProductAttributeDal.Insert(locationProductAttributeDto);
                        }

                        dto = dal.FetchByEntityId(1);
                        Assert.IsTrue(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsTrue(dto.HasAssortment);
                        Assert.IsTrue(dto.HasLocationSpace);
                        Assert.IsTrue(dto.HasLocationSpaceBay);
                        Assert.IsTrue(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsTrue(dto.HasLocationProductAttribute);
                        Assert.IsFalse(dto.HasLocationProductIllegal);

                        // Insert Location Product Illegal
                        using (ILocationProductIllegalDal locationProductIllegalDal = dalContext.GetDal<ILocationProductIllegalDal>())
                        {
                            LocationProductIllegalDto locationProductIllegalDto = new LocationProductIllegalDto()
                            {
                                EntityId = 1,
                                ProductId = 1,
                                LocationId = 1
                            };
                            locationProductIllegalDal.Insert(locationProductIllegalDto);
                        }

                        dto = dal.FetchByEntityId(1);
                        Assert.IsTrue(dto.HasProductHierarchy);
                        Assert.IsTrue(dto.HasLocationHierarchy);
                        Assert.IsTrue(dto.HasLocation);
                        Assert.IsTrue(dto.HasProduct);
                        Assert.IsTrue(dto.HasAssortment);
                        Assert.IsTrue(dto.HasLocationSpace);
                        Assert.IsTrue(dto.HasLocationSpaceBay);
                        Assert.IsTrue(dto.HasLocationSpaceElement);
                        Assert.IsTrue(dto.HasClusterScheme);
                        Assert.IsTrue(dto.HasLocationProductAttribute);
                        Assert.IsTrue(dto.HasLocationProductIllegal);
                    }
                }
            }
        }
    }
}
