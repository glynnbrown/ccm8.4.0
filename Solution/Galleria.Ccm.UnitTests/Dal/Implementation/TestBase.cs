﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
// V8-25476: A. Kuszyk
//  Added setup for MSSQL DAL Factory in CreateDalFactory().
// V8-25629 : A.Kuszyk
//  Amended DalImplementationTestBase.TestInsert to look for Int16 Ids as well as Int32 Ids.
// V8-25632 : K.Pickup
//  Changed to use framework version of DalImplementationTestBase.
// V8-26815 : A.Silva ~ Added DalFactoryTypeTestCaseSources to provide test case sources for dal unit tests.
// V8-27004 : A.Silva ~ Added TestMockDal as CaseSource.
// V8-26671 : A.Silva ~ Added TestMockAndUserDal as CaseSource.
// V8-26322 : A.Silva
//      Tidying up.
//      Added call to CreateDatabase when invoking CreateDalFactory for MockDal.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Galleria.Ccm.Engine;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.UnitTesting.TestBases;
using Microsoft.SqlServer.Management.Smo;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    public abstract class TestBase<DAL, DTO> : DalImplementationTestBase<DAL, DTO>
        where DAL : IDal
        where DTO : new()
    {

        #region Fields

        protected List<Type> _dbDalFactoryTypes;
        private String _sqlTemplateDataFilePath;
        private String _sqlTemplateLogFilePath;
        private string _sqlDatabaseName;
        private string _sqlDatabaseDataFilePath;
        private string _sqlDatabaseLogFilePath;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TestBase()
        {
            // create our list of dal factory types to create

            //db
            _dbDalFactoryTypes = new List<Type>();
            _dbDalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.Mock.DalFactory));
            _dbDalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.User.DalFactory));

            //register the tasks assembly
            TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");
        }

        /// <summary>
        ///     Initializes a new instance of <see cref="TestBase{DAL,DTO}"/> with the given DAL factory <paramref name="types"/> to be tested.
        /// </summary>
        /// <param name="types">List of DAL factory types to be included in the tests.</param>
        /// <remarks>If no type is indicated it will add the default ones (Mock, User and Mssql).</remarks>
        protected TestBase(params Type[] types)
        {
            if (types != null) _dbDalFactoryTypes = new List<Type>(types);
            else
            {
                _dbDalFactoryTypes = new List<Type>
                {
                    typeof (Galleria.Ccm.Dal.Mock.DalFactory),
                    typeof (Galleria.Ccm.Dal.User.DalFactory),
                    typeof (Galleria.Ccm.Dal.Mssql.DalFactory)
                };

            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DbDalFactoryTypes
        {
            get { return _dbDalFactoryTypes; }
        }

        #endregion

        #region Misc Helpers

        /// <summary>
        /// Creates our dal factory from the provided type
        /// </summary>
        /// <param name="dalFactoryType">The type of dal factory to create</param>
        protected override IDalFactory CreateDalFactory(Type dalFactoryType)
        {
            try
            {
                // The name to use for database templates.
                String templateName = TestDataHelper.TestDatabaseName;

                // The directory in which database templates will be created.
                string templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName,
                    "Test Files");
                if (!Directory.Exists(templateDir))
                {
                    // Directory doesn't exist yet: create it.
                    Directory.CreateDirectory(templateDir);
                }
                else
                {
                    //Directory.Delete(templateDir, true);

                    // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                    // this just to be tidy.
                    foreach (string file in Directory.GetFiles(templateDir))
                    {
                        if (!Path.GetFileName(file).StartsWith(templateName))
                        {
                            try
                            {
                                if (LockedFileManager.WaitForFileToBeUnlocked(file))
                                    File.Delete(file);
                            }
                            catch
                            {
                                // No point in crying over spilt milk
                            }
                        }
                    }
                }

                IDalFactory dalFactory = null;
                if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
                {
                    // User
                    String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
                    var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
                    userFactory.UnitTestFolder = templateDir;
                    dalFactory = userFactory;

                }
                else if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
                {
                    // create the dal factory
                    dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();
                    dalFactory.CreateDatabase();
                }
                else if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mssql.DalFactory))
                {
                    #region V8-25478: Mssql Dal Factory config.

                    // create a dal configuration item for this factory
                    DalFactoryConfigElement dalFactoryConfig;

                    // connect to the local server
                    Server server = new Server(TestingConstants.MssqlServerInstance);

                    // get the template file names
                    _sqlTemplateDataFilePath = Path.Combine(templateDir, templateName + ".mdf");
                    _sqlTemplateLogFilePath = Path.Combine(templateDir, templateName + "_Log.ldf");

                    //If template hasn't been created
                    if (!File.Exists(_sqlTemplateDataFilePath))
                    {

                        //Get original files to build from
                        string templateDataPath = Path.Combine(Environment.CurrentDirectory, "Environment\\CcmUnitTestsTemplate.mdf");
                        string templateLogPath = Path.Combine(Environment.CurrentDirectory, "Environment\\CcmUnitTestsTemplate_log.ldf");

                        //Copy original and create templates
                        File.Copy(templateDataPath, _sqlTemplateDataFilePath);
                        File.Copy(templateLogPath, _sqlTemplateLogFilePath);

                        //Attach template
                        server.AttachDatabase(templateName, new StringCollection() { _sqlTemplateDataFilePath, _sqlTemplateLogFilePath }, AttachOptions.None);

                        //Create config
                        dalFactoryConfig = new DalFactoryConfigElement();
                        // configure dal factory
                        dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                        {
                            Name = "Server",
                            Value = TestingConstants.MssqlServerInstance
                        });
                        dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                        {
                            Name = "Database",
                            Value = templateName
                        });

                        // create the dal factory
                        dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig);

                        // perform an upgrade on the database
                        dalFactory.Upgrade();

                        //clear connections
                        SqlConnection.ClearAllPools();

                        // detach db store as copy
                        server.DetachDatabase(templateName, false);
                    }

                    // copy the template files
                    _sqlDatabaseName = Guid.NewGuid().ToString();
                    _sqlDatabaseDataFilePath = Path.Combine(Path.GetDirectoryName(_sqlTemplateDataFilePath), _sqlDatabaseName + ".mdf");
                    _sqlDatabaseLogFilePath = Path.Combine(Path.GetDirectoryName(_sqlTemplateLogFilePath), _sqlDatabaseName + "_Log.ldf");
                    File.Copy(_sqlTemplateDataFilePath, _sqlDatabaseDataFilePath);
                    File.Copy(_sqlTemplateLogFilePath, _sqlDatabaseLogFilePath);

                    //Attach the database
                    server.AttachDatabase(_sqlDatabaseName, new StringCollection() { _sqlDatabaseDataFilePath, _sqlDatabaseLogFilePath });

                    //create new config
                    dalFactoryConfig = new DalFactoryConfigElement();

                    // configure dal factory
                    dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                    {
                        Name = "Server",
                        Value = TestingConstants.MssqlServerInstance
                    });
                    dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                    {
                        Name = "Database",
                        Value = _sqlDatabaseName
                    });

                    // create the dal factory
                    dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig);

                    #endregion
                }

                // and return the factory
                return dalFactory;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                throw;
            }
        }

        protected void InsertDto(IDalFactory dalFactory, DTO dto)
        {
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (DAL dal = dalContext.GetDal<DAL>())
                {
                    MethodInfo method = typeof(DAL).GetMethod("Insert", new Type[] { typeof(DTO) });
                    method.Invoke(dal, new Object[] { dto });
                }

                dalContext.Commit();
            }
        }

        protected void UpdateDto(IDalFactory dalFactory, DTO dto)
        {
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (DAL dal = dalContext.GetDal<DAL>())
                {
                    MethodInfo method = typeof(DAL).GetMethod("Update", new Type[] { typeof(DTO) });
                    method.Invoke(dal, new Object[] { dto });

                }

                dalContext.Commit();
            }
        }

        protected DTO FetchDtoById(IDalFactory dalFactory, Object id)
        {
            DTO dto = default(DTO);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (DAL dal = dalContext.GetDal<DAL>())
                {
                    MethodInfo method = typeof(DAL).GetMethod("FetchById", new Type[] { typeof(Object) });
                    dto = (DTO)method.Invoke(dal, new Object[] { id });
                }

                dalContext.Commit();
            }

            return dto;
        }

        protected void DeleteDtoById(IDalFactory dalFactory, Object id)
        {
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (DAL dal = dalContext.GetDal<DAL>())
                {
                    MethodInfo method = typeof(DAL).GetMethod("DeleteById", new Type[] { typeof(Object) });
                    method.Invoke(dal, new Object[] { id });
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Deletes the specified database from the local sql server
        /// </summary>
        /// <param name="databaseName">The database to delete</param>
        private static void KillDatabase(string databaseName)
        {
            SqlConnection.ClearAllPools();
            Server server = new Server(TestingConstants.MssqlServerInstance);
            if (server.Databases[databaseName] != null)
            {
                server.KillDatabase(databaseName);
            }
        }

        protected bool IsFileLocked(string filePath)
        {
            try
            {
                using (File.Open(filePath, FileMode.Open)) { }
            }
            catch (IOException e)
            {
                var errorCode = Marshal.GetHRForException(e) & ((1 << 16) - 1);
                return errorCode == 32 || errorCode == 33;
            }

            return false;
        }

        #endregion

        #region SetUp

        [SetUp]
        public void SetUp()
        {
           
        }

        #endregion

        #region Teardown

        /// <summary>
        /// Called after a test has been executed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            // ensure the mssql data is detached
            if (_sqlDatabaseDataFilePath != null)
            {
                KillDatabase(_sqlDatabaseName);
                File.Delete(_sqlDatabaseDataFilePath);
                File.Delete(_sqlDatabaseLogFilePath);
            }
        }

        #endregion

    }
}