﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Framework.Dal;
using System.IO;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramExportTemplateDal : TestBase<IPlanogramExportTemplateDal, PlanogramExportTemplateDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.User.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory)
        };


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                PlanogramExportTemplateDto templateDto = new PlanogramExportTemplateDto();
                templateDto.Id = 0;
                templateDto.Name = "test";
                templateDto.FileVersion = "1";
                templateDto.FileType = 2;

                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    //insert
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.Insert(templateDto);
                        }

                        dalContext.Commit();
                    }
                    Assert.IsTrue(File.Exists((String)templateDto.Id), "File does not exist");

                    //update
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            PlanogramExportTemplateDto dalDto = dal.FetchById(templateDto.Id);
                            Assert.AreEqual(templateDto.Name, dalDto.Name);
                            Assert.AreEqual(templateDto.FileVersion, dalDto.FileVersion);
                            Assert.AreEqual(templateDto.FileType, dalDto.FileType);

                            dalDto.FileType = 1;
                            templateDto.FileVersion = "2";
                            dal.Update(dalDto);

                            templateDto = dalDto;
                        }
                        dalContext.Commit();
                    }

                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            PlanogramExportTemplateDto dalDto = dal.FetchById(templateDto.Id);
                            Assert.AreEqual(templateDto.Name, dalDto.Name);
                            Assert.AreEqual(templateDto.FileVersion, dalDto.FileVersion);
                            Assert.AreEqual(templateDto.FileType, dalDto.FileType);
                        }
                        dalContext.Commit();
                    }
                }
            }
            else
            {
                base.TestFetchById(dalFactoryType, _excludeProperties);
            }
        }


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                PlanogramExportTemplateDto templateDto = new PlanogramExportTemplateDto();
                templateDto.Id = 0;
                templateDto.Name = "test";
                templateDto.FileVersion = "1";
                templateDto.FileType = 2;

                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    //insert
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.Insert(templateDto);
                        }

                        dalContext.Commit();
                    }
                    Assert.IsTrue(File.Exists((String)templateDto.Id), "File does not exist");

                    //update
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            PlanogramExportTemplateDto dalDto = dal.FetchById(templateDto.Id);
                            Assert.AreEqual(templateDto.Name, dalDto.Name);
                            Assert.AreEqual(templateDto.FileVersion, dalDto.FileVersion);
                            Assert.AreEqual(templateDto.FileType, dalDto.FileType);

                            dalDto.FileType = 1;
                            templateDto.FileVersion = "2";
                            dal.Update(dalDto);

                            templateDto = dalDto;
                        }
                        dalContext.Commit();
                    }

                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            PlanogramExportTemplateDto dalDto = dal.FetchByEntityIdName(templateDto.EntityId, templateDto.Name);
                            Assert.AreEqual(templateDto.Name, dalDto.Name);
                            Assert.AreEqual(templateDto.FileVersion, dalDto.FileVersion);
                            Assert.AreEqual(templateDto.FileType, dalDto.FileType);
                        }
                        dalContext.Commit();
                    }
                }
            }
            else
            {
                base.TestFetchByProperties(
                    dalFactoryType,
                    "FetchByEntityIdName",
                    new List<String>() { "EntityId", "Name" },
                    _excludeProperties,
                    fetchIncludesDeleted: true);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                PlanogramExportTemplateDto templateDto = new PlanogramExportTemplateDto();
                templateDto.Id = 0;
                templateDto.Name = "test";
                templateDto.FileVersion = "1";
                templateDto.FileType = 2;

                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    //insert
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.Insert(templateDto);
                        }

                        dalContext.Commit();
                    }
                    Assert.IsTrue(File.Exists((String)templateDto.Id), "File does not exist");

                    //fetch
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            PlanogramExportTemplateDto dalDto = dal.FetchById(templateDto.Id);
                            Assert.AreEqual(templateDto.Name, dalDto.Name);
                            Assert.AreEqual(templateDto.FileVersion, dalDto.FileVersion);
                            Assert.AreEqual(templateDto.FileType, dalDto.FileType);
                        }
                        dalContext.Commit();
                    }
                }
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                PlanogramExportTemplateDto templateDto = new PlanogramExportTemplateDto();
                templateDto.Id = 0;
                templateDto.Name = "test";
                templateDto.FileVersion = "1";
                templateDto.FileType = 2;

                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    //insert
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.Insert(templateDto);
                        }

                        dalContext.Commit();
                    }
                    Assert.IsTrue(File.Exists((String)templateDto.Id), "File does not exist");

                    //update
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            PlanogramExportTemplateDto dalDto = dal.FetchById(templateDto.Id);
                            Assert.AreEqual(templateDto.Name, dalDto.Name);
                            Assert.AreEqual(templateDto.FileVersion, dalDto.FileVersion);
                            Assert.AreEqual(templateDto.FileType, dalDto.FileType);

                            dalDto.FileType = 1;
                            templateDto.FileVersion = "2";
                            dal.Update(dalDto);

                            templateDto = dalDto;
                        }
                        dalContext.Commit();
                    }

                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            PlanogramExportTemplateDto dalDto = dal.FetchById(templateDto.Id);
                            Assert.AreEqual(templateDto.Name, dalDto.Name);
                            Assert.AreEqual(templateDto.FileVersion, dalDto.FileVersion);
                            Assert.AreEqual(templateDto.FileType, dalDto.FileType);
                        }
                        dalContext.Commit();
                    }
                }

            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                PlanogramExportTemplateDto templateDto = new PlanogramExportTemplateDto();
                templateDto.Id = 0;
                templateDto.Name = "test";
                templateDto.FileVersion = "1";
                templateDto.FileType = 2;

                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    //insert
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.Insert(templateDto);
                        }

                        dalContext.Commit();
                    }
                    Assert.IsTrue(File.Exists((String)templateDto.Id), "File does not exist");

                    //delete
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.DeleteById(templateDto.Id);
                        }
                        dalContext.Commit();
                    }
                    Assert.IsFalse(File.Exists((String)templateDto.Id), "File should have been deleted");
                }
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestLockById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                PlanogramExportTemplateDto templateDto = new PlanogramExportTemplateDto();
                templateDto.Id = 0;
                templateDto.Name = "test";
                templateDto.FileVersion = "1";
                templateDto.FileType = 2;

                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    //insert
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.Insert(templateDto);
                        }

                        dalContext.Commit();
                    }

                    Assert.IsTrue(File.Exists((String)templateDto.Id), "File does not exist");

                    //lock
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                        }

                        Assert.IsTrue(IsFileLocked((String)templateDto.Id), "File should be locked");

                        dalContext.Commit();
                    }
                }
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUnlockById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                PlanogramExportTemplateDto templateDto = new PlanogramExportTemplateDto();
                templateDto.Id = 0;
                templateDto.Name = "test";
                templateDto.FileVersion = "1";
                templateDto.FileType = 2;

                using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
                {
                    //insert
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.Insert(templateDto);
                        }

                        dalContext.Commit();
                    }

                    Assert.IsTrue(File.Exists((String)templateDto.Id), "File does not exist");

                    //lock unlock
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        dalContext.Begin();

                        using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                        {
                            dal.LockById(templateDto.Id);
                            dal.UnlockById(templateDto.Id);
                        }

                        Assert.IsFalse(IsFileLocked((String)templateDto.Id), "File should not be locked");

                        dalContext.Commit();
                    }

                }

            }
        }
    }
}
