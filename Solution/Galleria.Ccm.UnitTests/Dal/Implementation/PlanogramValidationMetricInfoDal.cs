﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27004 : A.Silva ~ Created.
// V8-26233 : A.Silva
//      Amended SetupDbScenario to avoid creating the same user twice.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    /// <summary>
    ///     Test Fixture for <see cref="PlanogramValidationMetricInfoDal"/>.
    /// </summary>
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public sealed class PlanogramValidationMetricInfoDal : TestBase<IPlanogramValidationMetricInfoDal, PlanogramValidationMetricInfoDto>
    {
        private static List<Int32> _planogramIds;

        #region Constructor

        public PlanogramValidationMetricInfoDal()
            : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchPlanogramValidationMetricsByPlanogramIds(Type dalFactoryType)
        {
            IEnumerable<Int32> actual;

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            {
                SetupDbScenario(dalContext);

                using (var dal = dalContext.GetDal<IPlanogramValidationMetricInfoDal>())
                {
                    actual = dal.FetchPlanogramValidationMetricsByPlanogramIds(_planogramIds).Select(o => o.Id);
                }
            }

            CollectionAssert.AreEquivalent(_planogramIds, actual);
        }

        #region Helper Methods

        private static void SetupDbScenario(IDalContext dalContext)
        {
            var userDtos = TestDataHelper.InsertUserDtos(dalContext, 1);
            var userName = userDtos.First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            var packageDtos = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 10);
            var planogramDtos = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtos);
            var planogramValidationTemplateDtos = TestDataHelper.InsertPlanogramValidationTemplateDtos(dalContext, planogramDtos);
            var planogramValidationTemplateGroupDtos = TestDataHelper.InsertPlanogramValidationTemplateGroupDtos(dalContext, planogramValidationTemplateDtos);
            TestDataHelper.InsertPlanogramValidationTemplateGroupMetricDtos(dalContext, planogramValidationTemplateGroupDtos);

            _planogramIds = planogramDtos.Where(o => o.Id != null).Select(o => Convert.ToInt32(o.Id)).ToList();
        }

        #endregion
    }
}