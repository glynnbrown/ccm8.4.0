﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductImageDal : TestBase<IProductImageDal, ProductImageDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestFetchByEntityId(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductId(Type dbDalFactoryType)
        {
            base.TestFetchByProperties(
                dbDalFactoryType,
                "FetchByProductId",
                new List<String>() {"ProductId"});
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductIdImageType(Type dbDalFactoryType)
        {
            base.TestFetchByProperties(
                dbDalFactoryType,
                "FetchByProductIdImageType",
                new List<String>() { "ProductId", "ImageType" });
        }

        /// <summary>
        /// Manual test copied from GFS, because of requirement for appropriate Image Dtos
        /// to be insterted in addition to parent/children that are inserted in framework
        /// test.
        /// </summary>
        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductIdImageTypeFacingTypeCompressionId(Type dbDalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                // insert test data
                List<ProductImageDto> sourceDtoList = this.InsertDtos(dalFactory);
                List<ImageDto> imageDtoList;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    //pull back the inserted imagedtos
                    imageDtoList = new List<ImageDto>();
                    using (IImageDal dal = dalContext.GetDal<IImageDal>())
                    {
                        foreach (ProductImageDto productImageDto in sourceDtoList)
                        {
                            imageDtoList.Add(dal.FetchById(productImageDto.ImageId));
                        }
                    }

                    //cycle through the images pulling back each one by criteria.
                    using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
                    {
                        foreach (ProductImageDto sourceDto in sourceDtoList)
                        {
                            ImageDto imageDto = imageDtoList.First(i => i.Id == sourceDto.ImageId);

                            ProductImageDto dalDto = dal.FetchByProductIdImageTypeFacingTypeCompressionId(
                                        sourceDto.ProductId, sourceDto.ImageType, sourceDto.FacingType,
                                        imageDto.CompressionId);

                            Assert.AreEqual(sourceDto, dalDto);
                        }
                    }
                }
            }
        }
        
        private List<ProductImageDto> InsertDtos(IDalFactory dalFactory)
        {
            List<ProductImageDto> dtoList = new List<ProductImageDto>();

            // insert non-entity specific 
            List<CompressionDto> compressionDtoList = TestDataHelper.InsertCompressionDtos(dalFactory, 2);

            //entity 1
            Int32 entity1Id = TestDataHelper.InsertEntityDtos(dalFactory,1)[0].Id;
            List<ProductHierarchyDto> hierarchyDtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1,entity1Id);
            List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchyDtos[0].Id, 3);
            List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);
            List<ProductDto> product1List = TestDataHelper.InsertProductDtos(dalFactory, 3, entity1Id);

            //entity 2
            Int32 entity2Id = TestDataHelper.InsertEntityDtos(dalFactory, 1)[0].Id;
            List<ProductHierarchyDto> hierarchy2Dtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity2Id);
            List<ProductLevelDto> hierarchy2levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchy2Dtos[0].Id, 3);
            List<ProductGroupDto> hierarchy2GroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy2levelDtos, 5);
            List<ProductDto> product2List = TestDataHelper.InsertProductDtos(dalFactory, 3, entity2Id);


            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dtoList.AddRange(TestDataHelper.InsertProductImageDtos(dalContext, entity1Id, product1List, compressionDtoList));
                dtoList.AddRange(TestDataHelper.InsertProductImageDtos(dalContext, entity2Id, product2List, compressionDtoList));
            }

            return dtoList;
        }
    }
}
