﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 800)
//V8-27059 : J.Pickup
//	Created

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.UnitTests.UI;


namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class AssortmentMinorRevisionReplaceActionLocationDal : TestBase<IAssortmentMinorRevisionReplaceActionLocationDal, AssortmentMinorRevisionReplaceActionLocationDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByAssortmentMinorRevisionReplaceActionId(Type dbDalFactoryType)
        {
            base.TestFetchByProperties(dbDalFactoryType, "FetchByAssortmentMinorRevisionReplaceActionId", new List<string>() { "AssortmentMinorRevisionReplaceActionId" }, fetchIncludesDeleted: true);
        }
    }
 
}