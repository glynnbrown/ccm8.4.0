﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
//V8-27941 J.Pickup
// MSSQL added
#endregion
#endregion

using System;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class HighlightCharacteristicDal : TestBase<IHighlightCharacteristicDal, HighlightCharacteristicDto>
    {
        private List<String> _excludeProperties = new List<String> { "HighlightId" };

        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.User.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByHighlightId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestFetchByProperty(dalFactoryType, "FetchByHighlightId", "HighlightId", _excludeProperties); 
            } 
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties, true);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }
    }
}
