﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using System.Collections.Generic;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class WorkflowDal : TestBase<IWorkflowDal, WorkflowDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fetch
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mssql.DalFactory))
            {
                Assert.Inconclusive("TODO: Stop unit test inserting workpackages as part of this as they are not deleted");
            }

            base.TestFetchById(dalFactoryType);
        }
        #endregion

        #region Insert
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, null, true);
        }
        #endregion

        #region Update
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType);
        }
        #endregion

        #region Delete
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mssql.DalFactory))
            {
                Assert.Inconclusive("TODO: Stop unit test inserting workpackages as part of this as they are not deleted");
            }

            base.TestDeleteById(dalFactoryType);
        }
        #endregion
    }
}
