﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationAttributeDal : TestBase<ILocationAttributeDal, LocationAttributeDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //Insert entity
                List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(dalFactory, 1);

                List<LocationHierarchyDto> hierarchy = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtoList);

                List<LocationLevelDto> levels = TestDataHelper.InsertLocationLevelDtos(dalFactory, hierarchy[0].Id, 3);

                List<LocationGroupDto> groups = TestDataHelper.InsertLocationGroupDtos(dalFactory, levels, 2);

                // insert some dtos into the data source
                List<LocationDto> sourceDtoList = new List<LocationDto>(
                    TestDataHelper.InsertLocationDtos(dalFactory, entityDtoList[0].Id, groups.Select(p => p.Id), 1));

                // now return these dtos from the data source
                LocationAttributeDto dalDto;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationAttributeDal dal = dalContext.GetDal<ILocationAttributeDal>())
                    {
                        dalDto = dal.FetchByEntityId(1);
                    }
                    dalContext.Commit();
                }

                Assert.IsNotNull(dalDto);

                //check to make sure that you can't fetch when deleted
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                    {
                        foreach (LocationDto dto in sourceDtoList)
                        {
                            dal.DeleteById(dto.Id);
                        }
                    }
                    using (ILocationAttributeDal dal = dalContext.GetDal<ILocationAttributeDal>())
                    {
                        dalDto = dal.FetchByEntityId(1);
                    }
                }

                #region assert all are false
                Assert.IsFalse(dalDto.HasPetrolForecourt);
                Assert.IsFalse(dalDto.HasRegion);
                Assert.IsFalse(dalDto.HasCounty);
                Assert.IsFalse(dalDto.HasTVRegion);
                Assert.IsFalse(dalDto.HasAddress1);
                Assert.IsFalse(dalDto.HasAddress2);
                Assert.IsFalse(dalDto.Has24Hours);
                Assert.IsFalse(dalDto.HasPetrolForecourt);
                Assert.IsFalse(dalDto.HasPetrolForecourtType);
                Assert.IsFalse(dalDto.HasRestaurant);
                Assert.IsFalse(dalDto.HasMezzFitted);
                Assert.IsFalse(dalDto.HasSizeGrossArea);
                Assert.IsFalse(dalDto.HasSizeNetSalesArea);
                Assert.IsFalse(dalDto.HasSizeMezzSalesArea);
                Assert.IsFalse(dalDto.HasCity);
                Assert.IsFalse(dalDto.HasCountry);
                Assert.IsFalse(dalDto.HasManagerName);
                Assert.IsFalse(dalDto.HasRegionalManagerName);
                Assert.IsFalse(dalDto.HasDivisionalManagerName);
                Assert.IsFalse(dalDto.HasAdvertisingZone);
                Assert.IsFalse(dalDto.HasDistributionCentre);
                Assert.IsFalse(dalDto.HasNewsCube);
                Assert.IsFalse(dalDto.HasAtmMachines);
                Assert.IsFalse(dalDto.HasNoOfCheckouts);
                Assert.IsFalse(dalDto.HasCustomerWC);
                Assert.IsFalse(dalDto.HasBabyChanging);
                Assert.IsFalse(dalDto.HasInLocationBakery);
                Assert.IsFalse(dalDto.HasHotFoodToGo);
                Assert.IsFalse(dalDto.HasRotisserie);
                Assert.IsFalse(dalDto.HasFishmonger);
                Assert.IsFalse(dalDto.HasButcher);
                Assert.IsFalse(dalDto.HasPizza);
                Assert.IsFalse(dalDto.HasDeli);
                Assert.IsFalse(dalDto.HasSaladBar);
                Assert.IsFalse(dalDto.HasOrganic);
                Assert.IsFalse(dalDto.HasGrocery);
                Assert.IsFalse(dalDto.HasMobilePhones);
                Assert.IsFalse(dalDto.HasDryCleaning);
                Assert.IsFalse(dalDto.HasHomeShoppingAvailable);
                Assert.IsFalse(dalDto.HasOptician);
                Assert.IsFalse(dalDto.HasPharmacy);
                Assert.IsFalse(dalDto.HasTravel);
                Assert.IsFalse(dalDto.HasPhotoDepartment);
                Assert.IsFalse(dalDto.HasCarServiceArea);
                Assert.IsFalse(dalDto.HasGardenCentre);
                Assert.IsFalse(dalDto.HasClinic);
                Assert.IsFalse(dalDto.HasAlcohol);
                Assert.IsFalse(dalDto.HasFashion);
                Assert.IsFalse(dalDto.HasCafe);
                Assert.IsFalse(dalDto.HasRecycling);
                Assert.IsFalse(dalDto.HasPhotocopier);
                Assert.IsFalse(dalDto.HasLottery);
                Assert.IsFalse(dalDto.HasPostOffice);
                Assert.IsFalse(dalDto.HasMovieRental);
                Assert.IsFalse(dalDto.HasOpenMonday);
                Assert.IsFalse(dalDto.HasOpenTuesday);
                Assert.IsFalse(dalDto.HasOpenWednesday);
                Assert.IsFalse(dalDto.HasOpenThursday);
                Assert.IsFalse(dalDto.HasOpenFriday);
                Assert.IsFalse(dalDto.HasOpenSaturday);
                Assert.IsFalse(dalDto.HasOpenSunday);
                Assert.IsFalse(dalDto.HasJewellery);
                #endregion

                throw new InconclusiveException("TODO: Make MUCH more thorough!!!");
            }
        }
    }
}
