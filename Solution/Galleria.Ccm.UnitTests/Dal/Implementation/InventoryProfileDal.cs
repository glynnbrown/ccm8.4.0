﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class InventoryProfileDal : TestBase<IInventoryProfileDal, InventoryProfileDto>
    {
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);

            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType,null, false);
        }
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {

            base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            base.TestFetchByProperties(
                dalFactoryType,
                "FetchByEntityIdName",
                new List<String>() { "EntityId", "Name" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {

            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }

    }
}
