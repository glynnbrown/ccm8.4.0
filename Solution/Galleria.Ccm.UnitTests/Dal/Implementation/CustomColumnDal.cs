﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26671 : A.Silva ~ Created
// V8-26322 : A.Silva
//      Amended some tests to work with the User Dal.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class CustomColumnDal : TestBase<ICustomColumnDal, CustomColumnDto>
    {
        private readonly List<String> _excludedProperties = new List<String> { "CustomColumnLayoutId" };

        private readonly List<Type> _dbDalFactories = new List<Type>
        {
            typeof(Ccm.Dal.Mock.DalFactory),
            typeof(Ccm.Dal.User.DalFactory)
        };

        #region Insert

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, _excludedProperties);
        }

        #endregion

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestFetchByColumnLayoutId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof (Ccm.Dal.User.DalFactory))
            {
                Assert.Inconclusive("TestBase does not currently support User Implementation tests.");
            }
            TestFetchByProperty(dalFactoryType, "FetchByColumnLayoutId", "CustomColumnLayoutId", _excludedProperties);
        }

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestDeleteByPath(Type dalFactoryType)
        {
            Assert.Inconclusive("TestBase does not currently support User Implementation tests.");
        }

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestUpdate(Type dalFactoryType)
        {
            Assert.Inconclusive("TestBase does not currently support User Implementation tests.");
        }
    }
}