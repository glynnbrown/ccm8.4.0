﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion


using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class FixtureItemDal : FixtureTestBase<IFixtureItemDal, FixtureItemDto>
    {
        internal override Ccm.Dal.Pogfx.SectionType SectionType
        {
            get { return Ccm.Dal.Pogfx.SectionType.FixtureItems; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "FixturePackageId";
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByFixturePackageId(Type dalFactoryType)
        {
            base.TestFetchByProperty(
                dalFactoryType,
                "FetchByFixturePackageId",
                "FixturePackageId",
                new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
