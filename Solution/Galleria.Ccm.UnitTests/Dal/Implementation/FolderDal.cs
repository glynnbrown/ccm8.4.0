﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion


using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.IO;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class FolderDal : TestBase<IFolderDal, FolderDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };


        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Ccm.Dal.User.DalFactory dalFactory = (Ccm.Dal.User.DalFactory)CreateDalFactory(dalFactoryType);
                String testDir = dalFactory.UnitTestFolder;

                List<FolderDto> insertedDtos = new List<FolderDto>();

                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        FolderDto dto1 = new FolderDto()
                        {
                            FileSystemPath = Path.Combine(testDir, "Folder1"),
                            Name = "Folder1",
                            ParentFolderId = testDir + "\\"
                        };

                        dal.Insert(dto1);
                        insertedDtos.Add(dto1);
                    }
                    context.Commit();
                }


                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        FolderDto dalDto = dal.FetchById(insertedDtos[0].Id);
                        Assert.AreEqual(dalDto, insertedDtos[0]);
                    }
                    context.Commit();
                }
            }
            else
            {
                base.TestFetchById(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByParentFolderId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO: Implement test.");
            }
            else
            {
                base.TestFetchByProperty(dalFactoryType, "FetchByParentFolderId", "ParentFolderId");
            }
        }


        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Ccm.Dal.User.DalFactory dalFactory = (Ccm.Dal.User.DalFactory)CreateDalFactory(dalFactoryType);
                String testDir = dalFactory.UnitTestFolder;

                List<FolderDto> insertedDtos = new List<FolderDto>();

                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        FolderDto dto1 = new FolderDto()
                        {
                            FileSystemPath = Path.Combine(testDir, "Folder1"),
                            Name = "Folder1",
                            ParentFolderId = testDir + "\\"
                        };

                        dal.Insert(dto1);
                        insertedDtos.Add(dto1);
                    }
                    context.Commit();
                }


                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        FolderDto dalDto = dal.FetchById(insertedDtos[0].Id);
                        Assert.AreEqual(dalDto, insertedDtos[0]);
                    }
                    context.Commit();
                }
            }
            else
            {
                base.TestInsert(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Ccm.Dal.User.DalFactory dalFactory = (Ccm.Dal.User.DalFactory)CreateDalFactory(dalFactoryType);
                String testDir = dalFactory.UnitTestFolder;

                List<FolderDto> insertedDtos = new List<FolderDto>();

                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        FolderDto dto1 = new FolderDto()
                        {
                            FileSystemPath = Path.Combine(testDir, "Folder1"),
                            Name = "Folder1",
                            ParentFolderId = testDir + "\\"
                        };

                        dal.Insert(dto1);
                        insertedDtos.Add(dto1);
                    }
                    context.Commit();
                }

                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        String newName = Guid.NewGuid().ToString();
                        insertedDtos[0].FileSystemPath = Path.Combine(testDir, newName);
                        insertedDtos[0].Name = newName;
                        dal.Update(insertedDtos[0]);
                    }
                    context.Commit();
                }


                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        FolderDto dalDto = dal.FetchById(insertedDtos[0].Id);
                        Assert.AreEqual(dalDto, insertedDtos[0]);
                    }
                    context.Commit();
                }
            }
            else
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Ccm.Dal.User.DalFactory dalFactory = (Ccm.Dal.User.DalFactory)CreateDalFactory(dalFactoryType);
                String testDir = dalFactory.UnitTestFolder;

                List<FolderDto> insertedDtos = new List<FolderDto>();

                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        FolderDto dto1 = new FolderDto()
                        {
                            FileSystemPath = Path.Combine(testDir, "Folder1"),
                            Name = "Folder1",
                            ParentFolderId = testDir + "\\"
                        };

                        dal.Insert(dto1);
                        insertedDtos.Add(dto1);
                    }
                    context.Commit();
                }

                Assert.IsTrue(Directory.Exists(insertedDtos[0].Id as String));

                using (IDalContext context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (IFolderDal dal = context.GetDal<IFolderDal>())
                    {
                        dal.DeleteById(insertedDtos[0].Id);
                    }
                    context.Commit();
                }

                Assert.IsFalse(Directory.Exists(insertedDtos[0].Id as String));
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }


    }
}
