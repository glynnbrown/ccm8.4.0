﻿#region Header Information
#region Version History CCM830
// V8-31699 : A.Heathcote 
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;


namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class UserEditorSettingsRecentLabelDalTests : TestBase<IUserEditorSettingsRecentLabelDal, UserEditorSettingsRecentLabelDto>
    {
        private List<String> _excludeProperties = new List<String>();
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.User.DalFactory);
            }
        }
        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchAll(Type dalFactoryType)
        {
            base.TestFetchAll(dalFactoryType, _excludeProperties);
        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, _excludeProperties, true);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }


}
