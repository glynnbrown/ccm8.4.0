﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public class PlanogramAssortmentLocationBuddyDal : TestBase<IPlanogramAssortmentLocationBuddyDal, PlanogramAssortmentLocationBuddyDto>
    {
        private List<Type> _dalFactoryTypes = new List<Type>()
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory) // Mock is tested in framework. 
        };
        private List<String> _excludeProperties = new List<String>() { "ExtendedData" };

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramAssortmentId(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByPlanogramAssortmentId", "PlanogramAssortmentId", _excludeProperties);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, _excludeProperties);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
