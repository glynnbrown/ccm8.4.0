﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25629 : A. Kuszyk
//      Initial version.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ConsumerDecisionTreeNodeDal : TestBase<IConsumerDecisionTreeNodeDal, ConsumerDecisionTreeNodeDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType, "FetchById", "Id");
            //base.TestFetchById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByConsumerDecisionTreeId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType, "FetchByConsumerDecisionTreeId", "ConsumerDecisionTreeId");
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchAll(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    //create a cdt hierarchy
                    Int32 entityId = TestDataHelper.InsertEntityDtos(dalFactory, 1)[0].Id;
                     var product = TestDataHelper.InsertProductDtos(dalFactory, 1, entityId);
                    var hierarchy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId);
                    var levelList = TestDataHelper.InsertProductLevelDtos(dalFactory,hierarchy.First().Id,1);
                    List<ProductGroupDto> productDto = TestDataHelper.InsertProductGroupDtos(dalFactory,levelList,1);
                    ConsumerDecisionTreeDto cdtDto = TestDataHelper.InsertConsumerDecisionTreeDtos(dalFactory,entityId,productDto,1)[0];
                    List<ConsumerDecisionTreeLevelDto> cdtLevelDtoList = TestDataHelper.InsertConsumerDecisionTreeLevelDtos(dalFactory, cdtDto, 3, true);
                    List<ConsumerDecisionTreeNodeDto> nodeDtosList = TestDataHelper.InsertConsumerDecisionTreeNodeDtos(dalFactory, cdtDto, cdtLevelDtoList, 5, /*addRoot*/true);

                    using (var dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                    {
                        for (Int32 i = 0; i < nodeDtosList.Count; i++)
                        {
                            IEnumerable<ConsumerDecisionTreeNodeDto> returnedDtos = dal.FetchAll();
                            Assert.IsNotNull(returnedDtos);
                            Assert.AreEqual(returnedDtos.Count(), nodeDtosList.Count, "count should be thesame");

                            //check that they are now removed
                            dal.FetchAll();

                            IEnumerable<ConsumerDecisionTreeNodeDto> returnedAfterDeletingDtos = dal.FetchAll();
                            Assert.AreEqual(nodeDtosList.Count, returnedAfterDeletingDtos.Count(), "All deleted items should have been fetched");
                        }
                    }

                }
            }

        }
    }
}
