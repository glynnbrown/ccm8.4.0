﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27153 : A.Silva   ~ Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class RenumberingStrategyInfoDal : TestBase<IRenumberingStrategyInfoDal, RenumberingStrategyInfoDto>
    {
        #region Test Helpers

        private const String DalFactoryTypes = "_dalFactoryTypes";

        private readonly List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof (Ccm.Dal.Mock.DalFactory),
            typeof (Ccm.Dal.Mssql.DalFactory)
        };

        #endregion

        [Test, TestCaseSource(DalFactoryTypes)]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            TestInfoFetchByEntityId<RenumberingStrategyDto>(dalFactoryType);
        }

        [Test, TestCaseSource(DalFactoryTypes)]
        public void TestFetchByEntityIdIncludingDeleted(Type dalFactoryType)
        {
            TestInfoFetchByProperty<RenumberingStrategyDto>(dalFactoryType,
                "FetchByEntityIdIncludingDeleted", "EntityId", true);
        }
    }
}