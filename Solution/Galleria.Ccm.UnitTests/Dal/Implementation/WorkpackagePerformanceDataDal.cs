﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class WorkpackagePerformanceDataDal : TestBase<IWorkpackagePerformanceDataDal, WorkpackagePerformanceDataDto>
    {
        #region Constructor

        public WorkpackagePerformanceDataDal() : base(typeof(Ccm.Dal.Mssql.DalFactory)) { }

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First();
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var products = TestDataHelper.InsertProductDtos(dalFactory, 50, entity.First().Id);
                    List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entity, 1);
                    List<WorkpackageDto> workpackageDtoList = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList, userName);
                    var performanceSelectionList = TestDataHelper.InsertPerformanceSelection(dalContext, entity.First().Id);
                    List<WorkpackagePerformanceDto> workpackagePerformanceDtoList = TestDataHelper.InsertWorkpackagePerformanceDtos(dalContext, workpackageDtoList);
                    List<WorkpackagePerformanceDataDto> workpackagePerformanceDataDtoList = new List<WorkpackagePerformanceDataDto>();

                    for (Byte dataType = 0; dataType < 3; dataType++)
                    {
                        foreach (ProductDto product in products)
                        {
                            workpackagePerformanceDataDtoList.Add(new WorkpackagePerformanceDataDto()
                            {
                                WorkpackageId = workpackageDtoList.First().Id,
                                PerformanceSelectionId = performanceSelectionList.First().Id,
                                ProductGTIN = product.Gtin,
                                DataType = dataType,
                                DataId = 0,
                                P1 = 1,
                                P2 = 2,
                                P3 = 3,
                                P4 = 4,
                                P5 = 5,
                                P6 = 6,
                                P7 = 7,
                                P8 = 8,
                                P9 = 9,
                                P10 = 10,
                                P11 = 11,
                                P12 = 12,
                                P13 = 13,
                                P14 = 14,
                                P15 = 15,
                                P16 = 16,
                                P17 = 17,
                                P18 = 18,
                                P19 = 19,
                                P20 = 20
                            });
                        }
                    }

                    using (var dal = dalContext.GetDal<IWorkpackagePerformanceDataDal>())
                    {
                        dal.BulkInsert(workpackageDtoList.First().Id, workpackagePerformanceDataDtoList);

                        for (Byte dataType = 0; dataType < 3; dataType++)
                        {
                            IEnumerable<WorkpackagePerformanceDataDto> workpackagePerformanceDataDtos = dal.FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(workpackageDtoList.First().Id, performanceSelectionList.First().Id, dataType, 0);
                            List<WorkpackagePerformanceDataDto> sourceWorkpackagePerformanceDataDtos = workpackagePerformanceDataDtoList.Where(p => p.DataType == dataType).ToList();

                            Assert.AreEqual(workpackagePerformanceDataDtos.Count(), sourceWorkpackagePerformanceDataDtos.Count);

                            foreach (WorkpackagePerformanceDataDto sourceWorkpackagePerformanceDataDto in sourceWorkpackagePerformanceDataDtos)
                            {
                                WorkpackagePerformanceDataDto workpackagePerformanceDataDto = workpackagePerformanceDataDtos.Where(p => p.ProductGTIN == sourceWorkpackagePerformanceDataDto.ProductGTIN).FirstOrDefault();

                                Assert.IsNotNull(workpackagePerformanceDataDto);
                                AssertDataModelDtoAndInfoDtoAreEqual(sourceWorkpackagePerformanceDataDto, workpackagePerformanceDataDto);
                            }
                        }
                    }
                }
            }
        }

        private void AssertDataModelDtoAndInfoDtoAreEqual(WorkpackagePerformanceDataDto sourceWorkpackagePerformanceDataDto, WorkpackagePerformanceDataDto workpackagePerformanceDataDto)
        {
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.DataType, workpackagePerformanceDataDto.DataType);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.DataId, workpackagePerformanceDataDto.DataId);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.PerformanceSelectionId, workpackagePerformanceDataDto.PerformanceSelectionId);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.WorkpackageId, workpackagePerformanceDataDto.WorkpackageId);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P1, workpackagePerformanceDataDto.P1);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P2, workpackagePerformanceDataDto.P2);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P3, workpackagePerformanceDataDto.P3);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P4, workpackagePerformanceDataDto.P4);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P5, workpackagePerformanceDataDto.P5);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P6, workpackagePerformanceDataDto.P6);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P7, workpackagePerformanceDataDto.P7);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P8, workpackagePerformanceDataDto.P8);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P9, workpackagePerformanceDataDto.P9);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P10, workpackagePerformanceDataDto.P10);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P11, workpackagePerformanceDataDto.P11);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P12, workpackagePerformanceDataDto.P12);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P13, workpackagePerformanceDataDto.P13);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P14, workpackagePerformanceDataDto.P14);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P15, workpackagePerformanceDataDto.P15);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P16, workpackagePerformanceDataDto.P16);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P17, workpackagePerformanceDataDto.P17);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P18, workpackagePerformanceDataDto.P18);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P19, workpackagePerformanceDataDto.P19);
            Assert.AreEqual(sourceWorkpackagePerformanceDataDto.P20, workpackagePerformanceDataDto.P20);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First();
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var products = TestDataHelper.InsertProductDtos(dalFactory, 50, entity.First().Id);
                    List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entity, 1);
                    List<WorkpackageDto> workpackageDtoList = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList, userName);
                    var performanceSelectionList = TestDataHelper.InsertPerformanceSelection(dalContext, entity.First().Id);
                    List<WorkpackagePerformanceDto> workpackagePerformanceDtoList = TestDataHelper.InsertWorkpackagePerformanceDtos(dalContext, workpackageDtoList);
                    List<WorkpackagePerformanceDataDto> workpackagePerformanceDataDtoList = new List<WorkpackagePerformanceDataDto>();

                    foreach (ProductDto product in products)
                    {
                        workpackagePerformanceDataDtoList.Add(new WorkpackagePerformanceDataDto()
                        {
                            WorkpackageId = workpackageDtoList.First().Id,
                            PerformanceSelectionId = performanceSelectionList.First().Id,
                            ProductGTIN = product.Gtin,
                            DataType = 0,
                            DataId = 0,
                            P1 = 1,
                            P2 = 2,
                            P3 = 3,
                            P4 = 4,
                            P5 = 5,
                            P6 = 6,
                            P7 = 7,
                            P8 = 8,
                            P9 = 9,
                            P10 = 10,
                            P11 = 11,
                            P12 = 12,
                            P13 = 13,
                            P14 = 14,
                            P15 = 15,
                            P16 = 16,
                            P17 = 17,
                            P18 = 18,
                            P19 = 19,
                            P20 = 20
                        });
                    }

                    using (var dal = dalContext.GetDal<IWorkpackagePerformanceDataDal>())
                    {
                        dal.BulkInsert(workpackageDtoList.First().Id, workpackagePerformanceDataDtoList);

                        IEnumerable<WorkpackagePerformanceDataDto> WorkpackagePerformanceDataDtos = dal.FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(workpackageDtoList.First().Id, performanceSelectionList.First().Id, 0, 0);
                        Assert.AreEqual(WorkpackagePerformanceDataDtos.Count(), 50);
                    }
                }
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestDeleteByWorkpackageId(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First();
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var products = TestDataHelper.InsertProductDtos(dalFactory, 50, entity.First().Id);
                    List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entity, 1);
                    List<WorkpackageDto> workpackageDtoList = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList, userName);
                    var performanceSelectionList = TestDataHelper.InsertPerformanceSelection(dalContext, entity.First().Id);
                    List<WorkpackagePerformanceDto> workpackagePerformanceDtoList = TestDataHelper.InsertWorkpackagePerformanceDtos(dalContext, workpackageDtoList);
                    List<WorkpackagePerformanceDataDto> workpackagePerformanceDataDtoList = new List<WorkpackagePerformanceDataDto>();

                    foreach (ProductDto product in products)
                    {
                        workpackagePerformanceDataDtoList.Add(new WorkpackagePerformanceDataDto()
                        {
                            WorkpackageId = workpackageDtoList.First().Id,
                            PerformanceSelectionId = performanceSelectionList.First().Id,
                            ProductGTIN = product.Gtin,
                            DataType = 0,
                            DataId = 0,
                            P1 = 1,
                            P2 = 2,
                            P3 = 3,
                            P4 = 4,
                            P5 = 5,
                            P6 = 6,
                            P7 = 7,
                            P8 = 8,
                            P9 = 9,
                            P10 = 10,
                            P11 = 11,
                            P12 = 12,
                            P13 = 13,
                            P14 = 14,
                            P15 = 15,
                            P16 = 16,
                            P17 = 17,
                            P18 = 18,
                            P19 = 19,
                            P20 = 20
                        });
                    }

                    using (var dal = dalContext.GetDal<IWorkpackagePerformanceDataDal>())
                    {
                        dal.BulkInsert(workpackageDtoList.First().Id, workpackagePerformanceDataDtoList);

                        IEnumerable<WorkpackagePerformanceDataDto> WorkpackagePerformanceDataDtos = dal.FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(workpackageDtoList.First().Id, performanceSelectionList.First().Id, 0, 0);
                        Assert.AreEqual(WorkpackagePerformanceDataDtos.Count(), 50);

                        dal.DeleteByWorkpackageId(workpackageDtoList.First().Id);

                        WorkpackagePerformanceDataDtos = dal.FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(workpackageDtoList.First().Id, performanceSelectionList.First().Id, 0, 0);
                        Assert.AreEqual(WorkpackagePerformanceDataDtos.Count(), 0);
                    }
                }
            }
        }
    }
}
