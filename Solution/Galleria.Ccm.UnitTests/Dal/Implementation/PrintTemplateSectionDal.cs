﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015
#region Version History: (CCM 8.2.0)
// V8-30738 : L.Ineson
//  Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class PrintTemplateSectionDal : TestBase<IPrintTemplateSectionDal, PrintTemplateSectionDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.User.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore("TestBase doesn't currently support user dal implementation tests.");
            }
            else
            {
                base.TestFetchById(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByPrintTemplateSectionGroupId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore("TestBase doesn't currently support user dal implementation tests.");
            }
            else
            {
                base.TestFetchByProperty(dalFactoryType, "FetchByPrintTemplateSectionGroupId", "PrintTemplateSectionGroupId");
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore("TestBase doesn't currently support user dal implementation tests.");
            }
            else
            {
                base.TestInsert(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore("TestBase doesn't currently support user dal implementation tests.");
            }
            else
            {
                base.TestUpdate(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                Assert.Ignore("TestBase doesn't currently support user dal implementation tests.");
            }
            else
            {
                base.TestDeleteById(dalFactoryType);
            }
        }
    }
}
