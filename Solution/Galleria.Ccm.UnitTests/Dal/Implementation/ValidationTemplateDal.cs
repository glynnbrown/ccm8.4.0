﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created
// V8-26815 : A.Silva ~ Added Lock and Unlock tests.
// V8-26322 : A.Silva
//      Tidying up.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.Mock;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class ValidationTemplateDal : TestBase<IValidationTemplateDal, ValidationTemplateDto>
    {
        private static readonly List<string> PropertiesToExclude = new List<String> { "EntityId", "DateCreated", "DateDeleted", "DateLastModified", "RowVersion", "DtoKey" };

        #region Constructor

        public ValidationTemplateDal()
            : base(typeof (DalFactory), typeof (Ccm.Dal.Mssql.DalFactory), typeof (Ccm.Dal.User.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
                RunAssertion(dalFactoryType,
                    (dto, dal) =>
                        AssertHelper.AssertModelObjectsAreEqual(dto, dal.FetchById(dto.Id), PropertiesToExclude));
            else
                base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdName(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
                RunAssertion(dalFactoryType,
                    (dto, dal) =>
                        AssertHelper.AssertModelObjectsAreEqual(dto, dal.FetchByEntityIdName(dto.EntityId, dto.Name),PropertiesToExclude));
            else
                base.TestFetchByProperties(
                    dalFactoryType,
                    "FetchByEntityIdName",
                    new List<String>() { "EntityId", "Name" },
                    PropertiesToExclude,
                    fetchIncludesDeleted:true);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
            {
                RunAssertion(dalFactoryType, (dto, dal) => AssertHelper.AssertModelObjectsAreEqual(dto, dal.FetchById(dto.Id), PropertiesToExclude));
            }
            else
            {
                base.TestInsert(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            var index = 0;
            if (dalFactoryType == typeof(Ccm.Dal.User.DalFactory))
                RunAssertion(dalFactoryType, (dto, dal) =>
                {
                    var compareDto = dal.FetchById(dto.Id);
                    AssertHelper.AssertModelObjectsAreEqual(dto,compareDto,PropertiesToExclude);
                    compareDto.Name = String.Format("{0}_{1}", compareDto.Name, index++);
                    dal.Update(compareDto);

                    var updatedDto = dal.FetchById(dto.Id);

                    Assert.AreNotEqual(dto.Name, compareDto.Name);
                    Assert.AreEqual(compareDto.Name, updatedDto.Name);
                    AssertHelper.AssertModelObjectsAreEqual(compareDto, updatedDto, PropertiesToExclude);
                });
            else
                TestUpdate(dalFactoryType, PropertiesToExclude);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(DalFactory))
                RunAssertion(dalFactoryType, (dto, dal) =>
                {
                    var templateDto = dal.FetchById(dto.Id);
                    AssertHelper.AssertModelObjectsAreEqual(dto,templateDto,PropertiesToExclude);
                    dal.DeleteById(templateDto.Id);

                    Assert.Throws<DtoDoesNotExistException>(() => dal.FetchById(dto.Id));
                });
            else
                base.TestDeleteById(dalFactoryType);
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestLockById(Type dalFactoryType)
        {
            throw new InconclusiveException("Cannot test file locking.");
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestUnlockById(Type dalFactoryType)
        {
            throw new InconclusiveException("Cannot test file unlocking.");
        }

        #region Private Helper Methods

        private void RunAssertion(Type dalFactoryType, Action<ValidationTemplateDto, IValidationTemplateDal> action)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IValidationTemplateDal>())
            {
                foreach (var dto in InsertDtos(dalContext))
                {
                    action.Invoke(dto, dal);
                }
            }
        }

        private static DtoSpec CreateDtoSpec()
        {
            var dtoSpec = DtoSpec.CreateDtoSpec(typeof (ValidationTemplateDto));
            dtoSpec.Parents.Clear();
            return dtoSpec;
        }

        private IEnumerable<ValidationTemplateDto> InsertDtos(IDalContext dalContext)
        {
            var dtos = CreateDtos<ValidationTemplateDto>(dalContext);
            var list = dtos.Dtos;
            dtos.InsertDtos(dalContext, CreateDtoSpec());
            return list;
        }

        #endregion
    }
}