﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-26147 : L.Ineson
//      Initial version.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class CustomAttributeDataDal : TestBase<ICustomAttributeDataDal,CustomAttributeDataDto>
    {
        public List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByParentTypeParentId(Type dalFactoryType)
        {
            base.TestFetchByProperties(
                dalFactoryType,
                "FetchByParentTypeParentId",
                new List<String> { "ParentType", "ParentId" },
                new List<String> { "ExtendedData" });

        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestUpsert(Type dbDalFactoryType)
        {
            base.TestUpsert<CustomAttributeDataIsSetDto>(dbDalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }


    }
}
