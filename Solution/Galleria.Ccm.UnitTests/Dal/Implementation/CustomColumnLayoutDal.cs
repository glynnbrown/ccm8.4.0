﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26671 : A.Silva ~ Created
// V8-26322 : A.Silva
//      Amended tests to work with the User Dal.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class CustomColumnLayoutDal : TestBase<ICustomColumnLayoutDal, CustomColumnLayoutDto>
    {
        private readonly List<String> _excludedProperties = new List<String> { "Id" };

        private readonly List<Type> _dbDalFactories = new List<Type>
        {
            typeof(Ccm.Dal.Mock.DalFactory),
            typeof(Ccm.Dal.User.DalFactory)
        };

        #region Insert

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.Mock.DalFactory))
            {
                base.TestInsert(dalFactoryType, _excludedProperties);
                return;
            }

            TestInsertAsFile(dalFactoryType, _excludedProperties);
        }

        #endregion

        #region Fetch

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof (Ccm.Dal.Mock.DalFactory))
            {
                base.TestFetchById(dalFactoryType, _excludedProperties);
                return;
            }

            TestFetchByIdAsFile(dalFactoryType);
        }

        #endregion

        #region Update

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.Mock.DalFactory))
            {
                base.TestUpdate(dalFactoryType, _excludedProperties);
                return;
            }

            TestUpdateAsFile(dalFactoryType, _excludedProperties);
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("_dbDalFactories")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Ccm.Dal.Mock.DalFactory))
            {
                base.TestDeleteById(dalFactoryType);
                return;
            }

            TestDeleteByIdAsFile(dalFactoryType);
        }

        #endregion

        #region Other Methods

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestLockById(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO test file locking");
        }

        [Test, TestCaseSource("_dbDalFactories")]
        public void TestUnlockById(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO test file unlocking");
        }

        #endregion

        #region Test Helper Methods

        private const string IdPropertyName = "Id";
        private const string RowVersionPropertyName = "RowVersion";

        private static List<CustomColumnLayoutDto> InsertCustomColumnLayoutDtos(Ccm.Dal.User.DalFactory dalFactory)
        {
            var dtos = new List<CustomColumnLayoutDto>();
            using (var context = dalFactory.CreateContext())
            {
                context.Begin();
                using (var dal = context.GetDal<ICustomColumnLayoutDal>())
                {
                    var dto = new CustomColumnLayoutDto { Name = "CustomColumnLayout1" };
                    dal.Insert(dto);
                    dtos.Add(dto);
                }
                context.Commit();
            }
            return dtos;
        }

        private void AssertInsertedIdsAreValid(PropertyInfo idProperty, IEnumerable<CustomColumnLayoutDto> dtosList,
            IDalContext dalContext, List<PropertyInfo> propertiesToCheck, DtoSpec dtoSpec)
        {
            var hasTimeStampProperties =
                propertiesToCheck.Any(
                    p => p.Name == "DateCreated" || p.Name == "DateLastModified" || p.Name == "DateDeleted");
            var expectedId = dtoSpec.PreloadedRecordCount;
            foreach (var dto in dtosList)
            {
                var idPropertyValue = idProperty.GetValue(dto, null);
                Int32 idPropertyInt32Value;
                if (Int32.TryParse(idPropertyValue.ToString(), out idPropertyInt32Value))
                    Assert.AreEqual(++expectedId, idPropertyInt32Value);
                var fetchedDto = (CustomColumnLayoutDto)dtoSpec.FetchDto(dalContext, dto);

                if (hasTimeStampProperties) CheckCreatedModifiedDeletedDates(dto, fetchedDto);

                foreach (var property in propertiesToCheck)
                {
                    var expected = property.GetValue(dto, null);
                    var actual = property.GetValue(fetchedDto, null);
                    if (!AreEqual(expected, actual))
                    {
                        Debug.WriteLine(expected);
                        Debug.WriteLine(actual);
                        Debug.WriteLine(property.Name);
                    }
                    Assert.AreEqual(expected, actual);
                }
            }
        }

        private static void AssertInitialIdsAreValid(PropertyInfo idProperty,
            IEnumerable<CustomColumnLayoutDto> dtosList)
        {
            foreach (var dto in dtosList)
            {
                Assert.IsTrue(IsNullOrEmpty(idProperty.GetValue(dto, null)));
            }
        }

        private static bool IsNullOrEmpty(Object value)
        {
            return Equals((Int16)0, value) ||
                   Equals(0, value) ||
                   Equals(String.Empty, value) ||
                   Equals(null, value);
        }

        /// <summary>
        ///     Modified version of Test Insert to make sure the Id being an Object is not an issue.
        /// </summary>
        /// <param name="dalFactoryType"></param>
        /// <param name="excludedProperties"></param>
        /// <param name="isReinsertAllowed"></param>
        private void TestInsertAsFile(Type dalFactoryType, IList<String> excludedProperties, Boolean isReinsertAllowed = true)
        {

            if (_dtoSpec == null) _dtoSpec = DtoSpec.CreateDtoSpec(typeof(CustomColumnLayoutDto));

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                var dtos = CreateDtos<CustomColumnLayoutDto>(dalContext);
                var dtosList = dtos.Dtos;
                PropertyInfo idProperty;
                var propertiesToCheck = GetCanWriteProperties<CustomColumnLayoutDto>(out idProperty, excludedProperties);
                var hasIdProperty = idProperty != null;
                if (hasIdProperty) AssertInitialIdsAreValid(idProperty, dtosList);

                InsertDtos(dalContext, dtos);

                if (hasIdProperty)
                    AssertInsertedIdsAreValid(idProperty, dtosList, dalContext, propertiesToCheck, _dtoSpec);

                var deleteMethod = typeof(ICustomColumnLayoutDal).GetMethod(_dtoSpec.DefaultDeleteMethodName);
                if (deleteMethod == null) return;

                foreach (var dto in dtosList) _dtoSpec.DeleteDto(dalContext, dto);

                var context = dalContext;
                TestDelegate code = () => InsertDtos(context, dtos, false);

                if (isReinsertAllowed)
                {
                    Assert.DoesNotThrow(code, "Reinsert should be allowed for this type.");

                    AssertInsertedIdsAreValid(idProperty, dtosList, dalContext, propertiesToCheck, _dtoSpec);
                }
                else
                {
                    Assert.Throws<Exception>(code, "Reinsert should not be allowed for this type.");
                }
            }
        }

        private void TestFetchByIdAsFile(Type dalFactoryType)
        {
            using (var dalFactory = (Ccm.Dal.User.DalFactory)CreateDalFactory(dalFactoryType))
            {
                var dtos = InsertCustomColumnLayoutDtos(dalFactory);

                using (var context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (var dal = context.GetDal<ICustomColumnLayoutDal>())
                    {
                        var dto = dal.FetchById(dtos[0].Id);
                        Assert.AreEqual(dtos[0], dto);
                    }
                    context.Commit();
                }
            }
        }

        private void TestUpdateAsFile(Type dalFactoryType, List<String> excludedProperties)
        {
            using (var dalFactory = (Ccm.Dal.User.DalFactory)CreateDalFactory(dalFactoryType))
            {
                var dtos = InsertCustomColumnLayoutDtos(dalFactory);
                var updateDto = new CustomColumnLayoutDto
                {
                    Description = "Updated Description",
                    HasTotals = true,
                    Name = "Updated Name",
                    Type = (byte) CustomColumnLayoutType.PlanogramComponent
                };

                PropertyInfo idProperty;
                var properties = GetCanWriteProperties<CustomColumnLayoutDto>(out idProperty, excludedProperties);

                //  Test that each writable property is updated.
                var dto = dtos[0];
                foreach (var property in properties)
                {
                    var updatedValue = property.GetValue(updateDto, null);
                    property.SetValue(dto, updatedValue, null);
                }

                using (var context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (var dal = context.GetDal<ICustomColumnLayoutDal>())
                    {
                        dal.Update(dto);
                    }
                    context.Commit();
                }

                AssertDtosAreFetched(dalFactory, dtos);
            }
        }

        private void TestDeleteByIdAsFile(Type dalFactoryType)
        {
            using (var dalFactory = (Ccm.Dal.User.DalFactory)CreateDalFactory(dalFactoryType))
            {
                var dtos = InsertCustomColumnLayoutDtos(dalFactory);

                using (var context = dalFactory.CreateContext())
                {
                    context.Begin();
                    using (var dal = context.GetDal<ICustomColumnLayoutDal>())
                    {
                        foreach (var dto in dtos)
                        {
                            dal.DeleteById(dto.Id);
                        }
                    }
                    context.Commit();
                }

                AssertDtosAreNotFetched(dalFactory, dtos);
            }
        }

        private static void AssertDtosAreFetched(Ccm.Dal.User.DalFactory dalFactory, List<CustomColumnLayoutDto> dtos)
        {
            using (var context = dalFactory.CreateContext())
            {
                context.Begin();
                using (var dal = context.GetDal<ICustomColumnLayoutDal>())
                {
                    var dto = dal.FetchById(dtos[0].Id);
                    Assert.AreEqual(dtos[0], dto);
                }
            }
        }

        private static void AssertDtosAreNotFetched(Ccm.Dal.User.DalFactory dalFactory, List<CustomColumnLayoutDto> dtos)
        {
            using (var context = dalFactory.CreateContext())
            {
                context.Begin();
                using (var dal = context.GetDal<ICustomColumnLayoutDal>())
                {
                    Assert.Throws<DtoDoesNotExistException>(() => dal.FetchById(dtos[0].Id));
                }
            }
        }

        private static List<PropertyInfo> GetCanWriteProperties<T>(out PropertyInfo idProperty, IList<String> excludedProperties)
        {
            var properties =
                new List<PropertyInfo>(typeof(T).GetProperties().Where(p => p.CanWrite));
            idProperty = properties.FirstOrDefault(p => p.Name == IdPropertyName);
            var propertiesToCheck =
                properties.Where(
                    p =>
                    excludedProperties == null || !excludedProperties.Contains(p.Name) ||
                    p.Name != IdPropertyName || p.Name != RowVersionPropertyName).ToList();
            return propertiesToCheck;
        }

        #endregion
    }
}