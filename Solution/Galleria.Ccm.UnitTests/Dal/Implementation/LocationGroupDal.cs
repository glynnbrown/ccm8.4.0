﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25445 L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationGroupDal : TestBase<ILocationGroupDal, LocationGroupDto>
    {
        private List<String> _excludeProperties = new List<String>{"AssignedLocationsCount"};

        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            base.TestFetchById(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByLocationHierarchyId(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByLocationHierarchyId", "LocationHierarchyId", _excludeProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByLocationHierarchyIdIncludingDeleted(Type dalFactoryType)
        {
            base.TestFetchByProperty(dalFactoryType, "FetchByLocationHierarchyIdIncludingDeleted", "LocationHierarchyId", _excludeProperties, null, /*includesDeleted*/true);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, _excludeProperties, false);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestUpsert(Type dalFactoryType)
        {
            base.TestUpsert<LocationGroupIsSetDto>(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }

        #region Ticket Related Tests

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM14222_DeletingMiddleLevelMovesChildGroups(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 2 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<LocationHierarchyDto> hierarchyDtoList = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityList[0].Id);

                //insert a root level and 2 child levels
                LocationLevelDto rootLevel, childLevel1, childLevel2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        rootLevel = new LocationLevelDto()
                        {
                            Name = "root",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new LocationLevelDto()
                        {
                            Name = "level1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new LocationLevelDto()
                        {
                            Name = "level2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);
                    }
                    dalContext.Commit();
                }

                //insert group per level
                LocationGroupDto rootGroup, group1, group2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        rootGroup = new LocationGroupDto()
                        {
                            Name = "root",
                            Code = "rootCode",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            LocationLevelId = rootLevel.Id,
                            ParentGroupId = null,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootGroup);

                        group1 = new LocationGroupDto()
                        {
                            Name = "group1",
                            Code = "group1Code",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            LocationLevelId = childLevel1.Id,
                            ParentGroupId = rootGroup.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group1);

                        group2 = new LocationGroupDto()
                        {
                            Name = "group2",
                            Code = "group2Code",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            LocationLevelId = childLevel2.Id,
                            ParentGroupId = group1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group2);
                    }
                    dalContext.Commit();
                }

                //Perform actions as if we have removed the middle level - 
                //need to do this in the same order as the framework would
                //so delete the level, update other levels then
                //delete the group, update other groups

                //remove the middle level and update the root and third levels
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(childLevel1.Id);

                        //set level 2 to now be the direct child of the root
                        childLevel2.ParentLevelId = rootLevel.Id;

                        dal.Update(rootLevel);
                        dal.Update(childLevel2);

                    }
                    dalContext.Commit();
                }

                //remove the middle group and update the other groups
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(group1.Id);

                        //set group 2 to now be the direct child of the root
                        group2.ParentGroupId = rootGroup.Id;

                        dal.Update(rootGroup);
                        dal.Update(group2);

                    }
                    dalContext.Commit();
                }


                //try to retrieve group 1 again and check the dtodoesnotexist is thrown
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        group1 = dal.FetchById(group1.Id);
                        Assert.IsNotNull(group1.DateDeleted, "Item should have been deleted");

                        group2 = dal.FetchById(group2.Id);
                        rootGroup = dal.FetchById(rootGroup.Id);
                    }
                }

                //check the group 2 is now the direct child of the rootGroup
                Assert.AreEqual(rootGroup.Id, group2.ParentGroupId);
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM14294_DeleteBottom2LevelsAtOnce(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 3 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<LocationHierarchyDto> hierarchyDtoList = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityList[0].Id);

                //insert a root level and 3 child levels
                LocationLevelDto rootLevel, childLevel1, childLevel2, childLevel3;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        rootLevel = new LocationLevelDto()
                        {
                            Name = "root",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new LocationLevelDto()
                        {
                            Name = "level1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new LocationLevelDto()
                        {
                            Name = "level2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);

                        childLevel3 = new LocationLevelDto()
                        {
                            Name = "level3",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel2.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel3);
                    }
                    dalContext.Commit();
                }

                //insert group per level
                LocationGroupDto rootGroup, group1, group2, group3;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        rootGroup = new LocationGroupDto()
                        {
                            Name = "root",
                            Code = "rootCode",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            LocationLevelId = rootLevel.Id,
                            ParentGroupId = null,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootGroup);

                        group1 = new LocationGroupDto()
                        {
                            Name = "group1",
                            Code = "group1Code",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            LocationLevelId = childLevel1.Id,
                            ParentGroupId = rootGroup.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group1);

                        group2 = new LocationGroupDto()
                        {
                            Name = "group2",
                            Code = "group2Code",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            LocationLevelId = childLevel2.Id,
                            ParentGroupId = group1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group2);

                        group3 = new LocationGroupDto()
                        {
                            Name = "group3",
                            Code = "group3Code",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            LocationLevelId = childLevel3.Id,
                            ParentGroupId = group2.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group3);
                    }
                    dalContext.Commit();
                }

                //Perform actions as if we have removed the middle level - 
                //need to do this in the same order as the framework would
                //so delete the level, update other levels then
                //delete the group, update other groups

                //remove the bottom 2 levels
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(childLevel3.Id);
                        dal.DeleteById(childLevel2.Id);

                        dal.Update(rootLevel);
                        dal.Update(childLevel1);

                    }
                    dalContext.Commit();
                }

                //remove the bottom 2 groups
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        //note - model deletes then updates so we must do the same
                        dal.DeleteById(group3.Id);
                        dal.DeleteById(group2.Id);

                        dal.Update(rootGroup);
                        dal.Update(group1);

                    }
                    dalContext.Commit();
                }


                //try to retrieve group 2 & group 3 again and check the dtodoesnotexist is thrown
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        group2 = dal.FetchById(group2.Id);
                        Assert.IsNotNull(group2.DateDeleted, "Item should have been deleted");

                        group3 = dal.FetchById(group3.Id);
                        Assert.IsNotNull(group3.DateDeleted, "Item should have been deleted");

                        group1 = dal.FetchById(group1.Id);
                        rootGroup = dal.FetchById(rootGroup.Id);
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void GEM23377_DeletingGroupDoesNotNullOffChildGroups(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //create a new hierarchy with 2 child levels
                List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                List<LocationHierarchyDto> hierarchyDtoList = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, 1, entityList[0].Id);


                //inert a root level and 2 child levels
                LocationLevelDto rootLevel, childLevel1, childLevel2;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                    {
                        rootLevel = new LocationLevelDto()
                        {
                            Name = "root",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootLevel);

                        childLevel1 = new LocationLevelDto()
                        {
                            Name = "level1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel1);

                        childLevel2 = new LocationLevelDto()
                        {
                            Name = "level2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            ShapeNo = 1,
                            Colour = 23,
                            ParentLevelId = childLevel1.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(childLevel2);
                    }
                    dalContext.Commit();
                }

                //add a group per level
                LocationGroupDto rootGroupDto, group2Dto, group3Dto;
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        rootGroupDto = new LocationGroupDto()
                        {
                            Code = "0",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            Name = "root",
                            LocationLevelId = rootLevel.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(rootGroupDto);

                        group2Dto = new LocationGroupDto()
                        {
                            Code = "1",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            Name = "group2",
                            LocationLevelId = childLevel1.Id,
                            ParentGroupId = rootGroupDto.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group2Dto);

                        group3Dto = new LocationGroupDto()
                        {
                            Code = "2",
                            LocationHierarchyId = hierarchyDtoList[0].Id,
                            Name = "group3",
                            LocationLevelId = childLevel2.Id,
                            ParentGroupId = group2Dto.Id,
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow
                        };
                        dal.Insert(group3Dto);
                    }
                    dalContext.Commit();
                }


                //delete group3 the group 2
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        dal.DeleteById(group3Dto.Id);
                        dal.DeleteById(group2Dto.Id);
                    }
                    dalContext.Commit();
                }

                //confirm that group3 is still linked to group 2
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        LocationGroupDto dalDto = dal.FetchById(group3Dto.Id);
                        Assert.AreEqual(dalDto.ParentGroupId, group2Dto.Id);
                    }
                    dalContext.Commit();
                }
            }
        }

        #endregion

    }
}
