﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25629 : A. Kuszyk
//      Initial version.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ConsumerDecisionTreeInfoDal : TestBase<IConsumerDecisionTreeInfoDal, ConsumerDecisionTreeInfoDto>
    {
        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<ConsumerDecisionTreeDto>(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dbDalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    // Insert entity
                    List<EntityDto> entityDto = TestDataHelper.InsertEntityDtos(dalContext, 1);

                    // Insert Product Hierarchy
                    List<ProductHierarchyDto> dtoProductHierarchyList = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityDto[0].Id);

                    // Insert Product Level
                    List<ProductLevelDto> dtoProductLevelList = TestDataHelper.InsertProductLevelDtos(dalFactory, dtoProductHierarchyList[0].Id,1);

                    // Insert Product Group
                    IEnumerable<ProductGroupDto> dtoProductGroupList = TestDataHelper.InsertProductGroupDtos(dalFactory, dtoProductLevelList, 1);

                    // insert a set of dtos into the data source
                    List<ConsumerDecisionTreeDto> dtoList = TestDataHelper.InsertConsumerDecisionTreeDtos(dalFactory, entityDto[0].Id, dtoProductGroupList, 1);

                    ConsumerDecisionTreeInfoDto returnedDto = null;
                    using (IConsumerDecisionTreeInfoDal dal = dalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                    {
                        foreach (ConsumerDecisionTreeDto sourceDto in dtoList)
                        {
                            returnedDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(sourceDto.Id, returnedDto.Id);
                            Assert.AreEqual(sourceDto.UniqueContentReference, returnedDto.UniqueContentReference);
                            Assert.AreEqual(sourceDto.ProductGroupId, returnedDto.ProductGroupId);
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchByProductGroupId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperty<ConsumerDecisionTreeDto>(dbDalFactoryType, "FetchByProductGroupId", "ProductGroupId");
        }
    }
}
