﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26270 : A.Kuszyk
//  Created.
// V8-27063 : A.Kuszyk
//  Amended to use TestBase.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramPerformanceMetricDal : TestBase<IPlanogramPerformanceMetricDal,PlanogramPerformanceMetricDto>
    {
        private List<PlanogramPerformanceMetricDto> _sourceDtoList;
        private List<PlanogramDto> _parentPlanogramDtoList;
        private List<PlanogramPerformanceDto> _parentPlanogramPerformanceDtoList;
        private List<PropertyInfo> _properties;
        private List<Type> _dalFactoryTypes = new List<Type>()
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory) // Mock is tested in framework. 
        };
        private List<String> _excludeProperties = new List<String>() { "ExtendedData" };


        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramPerformanceMetricDto));
            _properties = typeof(PlanogramPerformanceMetricDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1)[0].Id;
            List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity, 25);
            _parentPlanogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
            _parentPlanogramPerformanceDtoList = TestDataHelper.InsertPlanogramPerformanceDtos(dalContext, _parentPlanogramDtoList);           
            _sourceDtoList = TestDataHelper.InsertPlanogramPerformanceMetricDtos(dalContext, _parentPlanogramPerformanceDtoList);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramPerformanceId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestFetchByProperty(dalFactoryType, "FetchByPlanogramPerformanceId", "PlanogramPerformanceId", _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IPlanogramPerformanceMetricDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            IEnumerable<PlanogramPerformanceMetricDto> returnedDtoList = dal.FetchByPlanogramPerformanceId(_sourceDtoList[i].PlanogramPerformanceId);
                            PlanogramPerformanceMetricDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            TestFetchByPlanogramPerformanceId(dalFactoryType);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestUpdate(dalFactoryType, _excludeProperties);
                return;
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);
                    _excludeProperties.Add("MetricId");
                    _excludeProperties.Add("PlanogramPerformanceId");
                    
                    using (var dal = dalContext.GetDal<IPlanogramPerformanceMetricDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            var originalSourceDto = _sourceDtoList[0];
                            var alternativeSourceDto = _sourceDtoList[i];

                            // Update the DTO in memory.
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
                                {
                                    property.SetValue(
                                        originalSourceDto,
                                        property.GetValue(alternativeSourceDto, null),
                                        null);
                                }
                            }

                            // Update the DTO in database and verify success.
                            dal.Update(originalSourceDto);
                            var returnedDto = dal.FetchByPlanogramPerformanceId(originalSourceDto.PlanogramPerformanceId).ElementAt(0);
                            //CheckCreatedModifiedDeletedDates(originalSourceDto, returnedDto);
                            foreach (var property in _properties)
                            {
                                if (!_excludeProperties.Contains(property.Name))
                                {
                                    Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test]
        [TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext);

                    using (var dal = dalContext.GetDal<IPlanogramPerformanceMetricDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            //Ensure it exists
                            var returnedDto = dal.FetchByPlanogramPerformanceId(_sourceDtoList[i].PlanogramPerformanceId).ElementAt(0);

                            //Delete planogram
                            dal.DeleteById(_sourceDtoList[i].Id);
                        }

                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            IEnumerable<PlanogramPerformanceMetricDto> returnedDtoList = dal.FetchByPlanogramPerformanceId(_sourceDtoList[i].PlanogramPerformanceId);
                            PlanogramPerformanceMetricDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                            Assert.IsNull(returnedDto);
                        }
                    }
                }
            }
        }
    }
}
