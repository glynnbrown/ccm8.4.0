﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class EntityDal : TestBase<IEntityDal, EntityDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        #region Test Fixture Helpers

        private static List<EntityDto> InsertEntityDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<EntityDto> dtoList = new List<EntityDto>();

            //insert entities
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    //add 5 dtos
                    for (int i = 0; i < (numToInsert); i++)
                    {
                        dtoList.Add(
                        new EntityDto()
                        {
                            Name = "name" + i.ToString(),
                            Description = "description" + i.ToString(),
                            DateCreated = DateTime.UtcNow,
                            DateLastModified = DateTime.UtcNow,
                            DateDeleted = null
                        });
                    }
                    foreach (EntityDto dto in dtoList)
                    {
                        dal.Insert(dto);
                    }

                }
                dalContext.Commit();
            }

            return dtoList;
        }

        #endregion

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            base.TestFetchById(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchAll(Type dalFactoryType)
        {
            base.TestFetchAll(dalFactoryType);
        }


        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, null, /*canReinsert*/false);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            //base.TestDeleteById(dalFactoryType);
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    IEnumerable<EntityDto> dtoList = InsertEntityDtos(dalFactory, 3); 
                    using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                    {
                        //Delete all inserted objects
                        foreach (EntityDto dto in dtoList)
                        {
                            dal.DeleteById(dto.Id);
                        }
                    }
                    //ensure all were deleted
                    using (IEntityInfoDal dal = dalContext.GetDal<IEntityInfoDal>())
                    {
                        IEnumerable<EntityInfoDto> infoList = dal.FetchAll();
                        Assert.AreEqual(0, infoList.Count());
                    }
                }
            }
            
        }



        /// <summary>
        /// Tests that a dto inserted into the data source can also be fetched by name
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchDeletedByName(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                IEnumerable<EntityDto> dtoList = InsertEntityDtos(dalFactory, 3); ;

                #region Initial Insert
                //Insert
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                    {
                        //Ensure each dto was inserted correctly
                        foreach (EntityDto sourceDto in dtoList)
                        {
                            EntityDto returnedDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(returnedDto.Name, sourceDto.Name);
                        }
                    }
                }
                #endregion

                #region Delete
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                    {
                        //Delete all inserted objects
                        foreach (EntityDto dto in dtoList)
                        {
                            dal.DeleteById(dto.Id);
                        }
                    }
                    //ensure all were deleted
                    using (IEntityInfoDal dal = dalContext.GetDal<IEntityInfoDal>())
                    {
                        IEnumerable<EntityInfoDto> infoList = dal.FetchAll();
                        Assert.AreEqual(0, infoList.Count());
                    }
                }
                #endregion

                // return the dtos and compare
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                    {
                        foreach (EntityDto dto in dtoList)
                        {
                            EntityDto deletedDto = dal.FetchDeletedByName(dto.Name);
                            Assert.AreEqual(deletedDto.Name, dto.Name);
                        }
                    }
                }
            }
        }
    }
}
