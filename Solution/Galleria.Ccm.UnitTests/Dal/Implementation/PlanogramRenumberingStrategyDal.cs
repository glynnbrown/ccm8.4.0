﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27153 : A.Silva   ~ Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public sealed class PlanogramRenumberingStrategyDal : TestBase<IPlanogramRenumberingStrategyDal, PlanogramRenumberingStrategyDto>
    {
        #region Test Helpers

        /// <summary>
        ///     List of property names to exclude from testing.
        /// </summary>
        private static readonly List<String> PropertiesToExclude = new List<String>
        {
            "ExtendedData"
        };

        /// <summary>
        ///     List of DAL factory types to test.
        /// </summary>
        private static readonly List<Type> DalFactoryTypes = new List<Type>
        {
            typeof (Ccm.Dal.Mock.DalFactory),
            typeof (Ccm.Dal.Mssql.DalFactory)
        };

        #endregion

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dalFactoryType)
        {
            TestFetchByProperty(dalFactoryType, "FetchByPlanogramId", "PlanogramId", PropertiesToExclude);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, PropertiesToExclude);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            TestUpdate(dalFactoryType, PropertiesToExclude);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, PropertiesToExclude);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}