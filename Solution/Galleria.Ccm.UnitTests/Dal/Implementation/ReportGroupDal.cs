﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ReportGroupDal : TestBase<IReportGroupDal, ReportGroupDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fields
        List<DataModelDto> _dataModelDtos = new List<DataModelDto>();
        List<ReportDto> _reportDtos = new List<ReportDto>();
        #endregion

        #region Helpers

        private List<ReportGroupDto> InsertDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            _dataModelDtos = TestDataHelper.InsertDataModelDtos(dalFactory);
            _reportDtos = TestDataHelper.InsertReportDtos(dalFactory, numToInsert, _dataModelDtos);
            List<ReportGroupDto> reportGroupDtos = TestDataHelper.InsertReportGroupDtos(dalFactory, numToInsert, _reportDtos);
            return reportGroupDtos;
        }
        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportGroupDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportGroupDal dal = dalContext.GetDal<IReportGroupDal>())
                    {
                        foreach (ReportGroupDto sourceDto in sourceDtoList)
                        {
                            ReportGroupDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);

                            //Test return following deletion
                            dal.DeleteById(dalDto.Id);

                            ReportGroupDto returnedDto;
                            Assert.Throws<DtoDoesNotExistException>
                                (() => { returnedDto = dal.FetchById(dalDto.Id); },
                                "Deleted items should be returned when fetched by Id");
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByReportId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportGroupDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportGroupDal dal = dalContext.GetDal<IReportGroupDal>())
                    {
                        foreach (ReportDto reportDto in _reportDtos)
                        {
                            List<ReportGroupDto> dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            List<ReportGroupDto> sourceDtosInReport = sourceDtoList.Where(r => r.ReportId == reportDto.Id).ToList();

                            Assert.AreEqual(sourceDtosInReport.Count, dalDtos.Count, "Correct number of items should be returned");

                            foreach (ReportGroupDto sourceDto in sourceDtosInReport)
                            {
                                ReportGroupDto dalDto = dalDtos.FirstOrDefault(s => s.Id == sourceDto.Id);
                                Assert.IsNotNull(dalDto);
                                Assert.AreEqual(dalDto, sourceDto);
                            }
                        }
                    }
                }
            }
        }


        #endregion

        #region Insert
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportGroupDto> dtoList = InsertDtos(dalFactory, 2);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportGroupDal dal = dalContext.GetDal<IReportGroupDal>())
                    {
                        foreach (ReportGroupDto sourceDto in dtoList)
                        {
                            // fetch the dto
                            ReportGroupDto compareDto = dal.FetchById(sourceDto.Id);

                            // and assert that they are equal
                            Assert.AreEqual(sourceDto, compareDto);
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be updated
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportGroupDto> dtoList = InsertDtos(dalFactory, 2);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportGroupDal dal = dalContext.GetDal<IReportGroupDal>())
                    {
                        foreach (ReportGroupDto sourceDto in dtoList)
                        {
                            // fetch the dto
                            ReportGroupDto compareDto = dal.FetchById(sourceDto.Id);

                            // and assert that they are equal
                            Assert.AreEqual(sourceDto, compareDto);

                            // update the compare dto
                            compareDto.Name = "New name";
                            ReportDto reportDto = _reportDtos.FirstOrDefault(r => r.Id == compareDto.ReportId);
                            compareDto.GroupLevel = 25;
                            dal.Update(compareDto);

                            compareDto = dal.FetchById(sourceDto.Id);
                            Assert.AreNotEqual(sourceDto, compareDto);
                            Assert.AreEqual("New name", compareDto.Name);
                            Assert.AreEqual(25, compareDto.GroupLevel);
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Tests that a product universe can be deleted
        /// </summary>
        /// <param name="dalFactoryType">The dal factory to test</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestDeleteById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportGroupDto> dtoList = InsertDtos(dalFactory, 2);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportGroupDal dal = dalContext.GetDal<IReportGroupDal>())
                    {
                        foreach (ReportGroupDto sourceDto in dtoList)
                        {
                            // delete the dto
                            dal.DeleteById(sourceDto.Id);

                            ReportGroupDto returnedDto;
                            Assert.Throws<DtoDoesNotExistException>
                                (() => { returnedDto = dal.FetchById(sourceDto.Id); },
                                "Deleted child items should not be returned when fetched by Id");
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion
    }
}
