﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25630 : A.Kuszyk
//  Created.
// CCM-25242 : N.Haywood
//  Added fetch methods for imports
// V8-25632 : K. Pickup
//      Refactored to work with the framework version of the DalImplementationTestBase, which I introduced as part of 
//      the fix for a test in ConsumerDecisionTreeLevelDal.
// V8-26322 : A.Silva
//      Added missing TestFetchByMerchandisingGroupId and TestFetchByEntityIdSearchText as place holders.
//      Amended TestFetchByEntityIdProductGtins and TestFetchByEntityIdProductIds so that they compare dtos correctly.

#endregion
#region Version History: (CCM 8.3.0)
// V8-32356 : N.Haywood
//  Added FetchByEntityIdMultipleSearchText
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductDal : TestBase<IProductDal,ProductDto>
    {
        private readonly List<String> _excludeProperties = new List<String> { "Id" };

        #region Constructor

        public ProductDal() : base(typeof (Ccm.Dal.Mock.DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestFetchByEntityId(dbDalFactoryType);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByProductGroupId(Type dbDalFactoryType)
        {
            const String fetchMethodName = "FetchByProductGroupId";
            var propertyNames = new List<String> {"ProductGroupId"};
            TestFetchByProperties(dbDalFactoryType,fetchMethodName,propertyNames);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByProductIds(Type dbDalFactoryType)
        {
            const String fetchMethodName = "FetchByProductIds";
            const String listPropertyName = "Id";
            TestFetchByProperties<Int32>(dbDalFactoryType,fetchMethodName,_excludeProperties,listPropertyName);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityIdProductGtins(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                var entityId = TestDataHelper.InsertEntityDtos(dalFactory, 1).First().Id;
                var expectedDtos = TestDataHelper.InsertProductDtos(dalFactory, 10, entityId);

                List<ProductDto> actualDtos;
                using (var dalContext = dalFactory.CreateContext())
                using (var dal = dalContext.GetDal<IProductDal>())
                {
                    var productGtins = expectedDtos.Select(dto => dto.Gtin);
                    actualDtos = dal.FetchByEntityIdProductGtins(entityId, productGtins).ToList();
                }

                Assert.AreEqual(expectedDtos.Count, actualDtos.Count);
                foreach (var expected in expectedDtos)
                {
                    var actual = actualDtos.FirstOrDefault(dto => dto.Gtin == expected.Gtin);
                    AssertHelper.AssertModelObjectsAreEqual(expected, actual);
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityIdProductIds(Type dbDalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                var entityId = TestDataHelper.InsertEntityDtos(dalFactory, 1).First().Id;
                var expectedDtos = TestDataHelper.InsertProductDtos(dalFactory, 10, entityId);

                List<ProductDto> actualDtos;
                using (var dalContext = dalFactory.CreateContext())
                using (var dal = dalContext.GetDal<IProductDal>())
                {
                    var productIds = expectedDtos.Select(dto => dto.Id);
                    actualDtos = dal.FetchByProductIds(productIds).ToList();
                }

                Assert.AreEqual(expectedDtos.Count, actualDtos.Count);
                foreach (var expected in expectedDtos)
                {
                    var actual = actualDtos.FirstOrDefault(dto => dto.Id == expected.Id);
                    AssertHelper.AssertModelObjectsAreEqual(expected, actual);
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByProductUniverseId(Type dbDalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                // fetch each item from the dal
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    DtoCollection<ProductUniverseProductDto> dtos = CreateDtos<ProductUniverseProductDto>(dalContext);
                    List<ProductUniverseProductDto> sourceDtoList = dtos.Dtos;

                    dtos.InsertDtos(dalContext, _dtoSpec);

                    dalContext.Begin();

                    using (IProductDal dal = dalContext.GetDal<IProductDal>())
                    {
                        using (IProductUniverseDal dalUniverse = dalContext.GetDal<IProductUniverseDal>())
                        {
                            foreach (ProductUniverseDto universe in dalUniverse.FetchByEntityId(1))
                            {
                                using (
                                    IProductUniverseProductDal dalUniverseProduct =
                                        dalContext.GetDal<IProductUniverseProductDal>())
                                {
                                    IEnumerable<ProductUniverseProductDto> universeProducts =
                                        dalUniverseProduct.FetchByProductUniverseId(universe.Id);
                                    IEnumerable<ProductDto> products = dal.FetchByProductUniverseId(universe.Id);

                                    Assert.AreEqual(universeProducts.Count(), products.Count(),
                                        "The number of products within the universe and universe product filter should be equal.");
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByMerchandisingGroupId(Type dbDalFactoryType)
        {
            Assert.Inconclusive("TODO"); // TODO Implement TestFetchByMerchandisingGroupId.
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityIdSearchText(Type dbDalFactoryType)
        {
            Assert.Inconclusive("TODO"); // TODO Implement TestFetchByEntityIdSearchText.
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByEntityIdMultipleSearchText(Type dbDalFactoryType)
        {
            Assert.Inconclusive("TODO"); // TODO Implement TestFetchByEntityIdMultipleSearchText.
        }

        #endregion

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, null, false);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public new void TestUpsert(Type dbDalFactoryType)
        {
            base.TestUpsert<ProductIsSetDto>(dbDalFactoryType, new List<String> { "CanMerchandiseOnShelf", "CanMerchandiseOnBar", "CanMerchandiseOnPeg", "CanMerchandiseInChest" });

        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteByEntityId(Type dbDalFactoryType)
        {
            base.TestDeleteByEntityId(dbDalFactoryType);
        }
    }
}
