﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class CompressionDal : TestBase<ICompressionDal,CompressionDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchAll(Type dbDalFactoryType)
        {
            base.TestFetchAll(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchAllEnabled(Type dbDalFactoryType)
        {
            Int32 dtoCount = 10;
            using(var dalFactory = CreateDalFactory(dbDalFactoryType))
            using(var dalContext = dalFactory.CreateContext())
            {
                var dal = dalContext.GetDal<ICompressionDal>();
                var compressionDtos = TestDataHelper.InsertCompressionDtos(dalFactory, dtoCount);
                compressionDtos
                    .Take(dtoCount / 2)
                    .ToList()
                    .ForEach(dto => dto.Enabled = false);
                foreach (var dto in compressionDtos)
                {
                    dal.Update(dto);
                }

                var returnedDtos = dal.FetchAllEnabled();

                Assert.AreEqual(
                    compressionDtos.Where(dto => dto.Enabled == false).Count(), 
                    returnedDtos.Count);
            }
        }
    }
}
