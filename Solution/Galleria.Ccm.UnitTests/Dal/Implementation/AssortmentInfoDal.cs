﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0.0)

// V8-26140 : I.George
//      InitialVersion
// V8-27059 : J.Pickup
//      Made changes TestFetchByEntityIdAssortmentSearchCriteria as was failing (new basic implementation).
#endregion

#region Version History: (CCM V8.2.0)
// V8-30461 : M.Shelley
//  Added Entity_Id to FetchByProductGroupId
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class AssortmentInfoDal : TestBase<IAssortmentInfoDal, AssortmentInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<AssortmentDto>(dbDalFactoryType);
        }
        
        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductGroupId(Type dalFactoryType)
        {
            base.TestInfoFetchByProperties<AssortmentDto>(dalFactoryType, "FetchByProductGroupId", new List<String> { "EntityId", "ProductGroupId" }, /*includesDeleted*/false);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdAssortmentIds(Type dalFactoryType)
        {
            base.TestInfoFetchByProperties<AssortmentDto, Int32>(dalFactoryType, "FetchByEntityIdAssortmentIds",
                new List<String> { "EntityId" }, "Id", false);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdAssortmentSearchCriteria(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                var entity = TestDataHelper.InsertEntityDtos(dalFactory, 1)[0];
                var hrcy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.Id);
                var pdtlvl = TestDataHelper.InsertProductLevelDtos(dalFactory, hrcy.First().Id, 1);
                var pdtgrp = TestDataHelper.InsertProductGroupDtos(dalFactory, pdtlvl, 1);
                var assortments = TestDataHelper.InsertAssortmentDtos(dalFactory, 10, entity.Id, 1);
                List<AssortmentInfoDto> fetchedAssortments = null;

                using (var dal = dalFactory.CreateContext().GetDal<IAssortmentInfoDal>())
                {
                    fetchedAssortments = dal.FetchByEntityIdAssortmentSearchCriteria(entity.Id, assortments.First().Name).ToList();
                }

                Assert.IsNotNull(fetchedAssortments);
            }
        }

    }
}
