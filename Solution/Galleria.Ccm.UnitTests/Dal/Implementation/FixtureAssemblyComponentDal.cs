﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion


using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class FixtureAssemblyComponentDal : FixtureTestBase<IFixtureAssemblyComponentDal, FixtureAssemblyComponentDto>
    {
        internal override Ccm.Dal.Pogfx.SectionType SectionType
        {
            get { return Ccm.Dal.Pogfx.SectionType.FixtureAssemblyComponents; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "FixtureAssemblyId";
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByFixtureAssemblyId(Type dalFactoryType)
        {
            base.TestFetchByProperty(
                dalFactoryType,
                "FetchByFixtureAssemblyId",
                "FixtureAssemblyId",
                new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
