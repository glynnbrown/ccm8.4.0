﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)

// V8-27059 : J.Pickup
//      Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class AssortmentRegionDal : TestBase<IAssortmentRegionDal, AssortmentRegionDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dbDalFactoryType)
        {
            base.TestFetchById(dbDalFactoryType);
        }


        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByAssortmentId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType, "FetchByAssortmentId", "AssortmentId", null, null, false);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            if (dbDalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                base.TestDeleteById(dbDalFactoryType);
                return;
            }

            using (var dalFactory = CreateDalFactory(dbDalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);
                    var product = TestDataHelper.InsertProductDtos(dalFactory, 1, entity.First().Id);

                    var hierrarchy = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entity.First().Id);
                    var productLevel = TestDataHelper.InsertProductLevelDtos(dalFactory, hierrarchy.First().Id, 1);
                    var productGroup = TestDataHelper.InsertProductGroupDtos(dalFactory, productLevel, 1);

                    var assortment = TestDataHelper.InsertAssortmentDtos(dalFactory, 1, entity.First().Id, productGroup.First().Id);

                    var _sourceDtoList = TestDataHelper.InsertAssortmentRegionDtos(dalFactory, assortment.First().Id, 1);



                    using (var dal = dalContext.GetDal<IAssortmentRegionDal>())
                    {
                        for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                        {
                            //Check are correctly added.
                            IEnumerable<AssortmentRegionDto> returnedDtoList = dal.FetchByAssortmentId(_sourceDtoList[i].Id);
                            Assert.AreEqual(_sourceDtoList.Count(), returnedDtoList.Count());
                            Assert.AreEqual(_sourceDtoList.First().Id, returnedDtoList.First().Id);

                            //Check are now removed
                            dal.DeleteById(_sourceDtoList[i].Id);

                            IEnumerable<AssortmentRegionDto> returnedAfterDeletionDtos = returnedDtoList = dal.FetchByAssortmentId(_sourceDtoList[i].Id);
                            Assert.IsEmpty(returnedAfterDeletionDtos.ToList(), "Expected item to be deleted, but was not.");
                        }
                    }
                }
            }
        }
    }
}
