﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25478 : A. Kuszyk
//      Initial version.
// V8-25881 : A.Probyn
//  Updated after MetaData properties
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public class PlanogramFixtureItemDal : TestBase<IPlanogramFixtureItemDal,PlanogramFixtureItemDto>
    {
        private List<String> _excludeProperties = new List<String> { "ExtendedData" };

        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
        };

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType,
                "FetchByPlanogramId", "PlanogramId", _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _excludeProperties);
        }


        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestBulkInsert(Type dalFactoryType)
        {
            base.TestBulkInsert(dalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _excludeProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }


        #region OLD

        //#region Fields
        //private List<String> _excludeProperties = new List<String>() { "ExtendedData" };
        //private List<Type> _dbDalFactoryTypes; 
        //private List<PlanogramFixtureItemDto> _sourceDtoList;
        //private List<PlanogramDto> _parentPlanogramDtoList;
        //private new DtoSpec _dtoSpec;
        //private List<PropertyInfo> _properties;
        //#endregion

        //private void InitializeDtosAndProperties(IDalContext dalContext)
        //{
        //    _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramFixtureItemDto));
        //    _properties = typeof(PlanogramFixtureItemDto).GetProperties().Where(p => p.CanWrite).ToList();

        //    //Insert parents
        //    List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, 25);
        //    _parentPlanogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, 1, packageDtoList);
        //    List<PlanogramFixtureDto> planogramFixtureDtoList = TestDataHelper.InsertPlanogramFixtureDtos(dalContext, 1, _parentPlanogramDtoList);
        //    _sourceDtoList = TestDataHelper.InsertPlanogramFixtureItemDtos(dalContext, _parentPlanogramDtoList, planogramFixtureDtoList);
        //}

        //public PlanogramFixtureItemDal()
        //{
        //    _dbDalFactoryTypes = new List<Type>();
        //    _dbDalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.Mock.DalFactory));
        //    _dbDalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.Mssql.DalFactory));
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public void TestFetchByPlanogramId(Type dalFactoryType)
        //{
        //    if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //    {
        //        base.TestFetchByProperty(
        //        dalFactoryType,
        //        "FetchByPlanogramId",
        //        "PlanogramId",
        //        _excludeProperties);
        //        return;
        //    }

        //    using (var dalFactory = CreateDalFactory(dalFactoryType))
        //    {
        //        using (var dalContext = dalFactory.CreateContext())
        //        {
        //            InitializeDtosAndProperties(dalContext);

        //            using (var dal = dalContext.GetDal<IPlanogramFixtureItemDal>())
        //            {
        //                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //                {
        //                    IEnumerable<PlanogramFixtureItemDto> returnedDtoList = dal.FetchByPlanogramId(_sourceDtoList[i].PlanogramId);
        //                    PlanogramFixtureItemDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
        //                    foreach (var property in _properties)
        //                    {
        //                        if (!_excludeProperties.Contains(property.Name))
        //                        {
        //                            Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public void TestInsert(Type dalFactoryType)
        //{
        //    if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //    {
        //        base.TestInsert(
        //        dalFactoryType,
        //        _excludeProperties); 
        //        return;
        //    }

        //    using (var dalFactory = CreateDalFactory(dalFactoryType))
        //    {
        //        using (var dalContext = dalFactory.CreateContext())
        //        {
        //            InitializeDtosAndProperties(dalContext);

        //            using (var dal = dalContext.GetDal<IPlanogramFixtureItemDal>())
        //            {
        //                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //                {
        //                    IEnumerable<PlanogramFixtureItemDto> returnedDtoList = dal.FetchByPlanogramId(_sourceDtoList[i].PlanogramId);
        //                    PlanogramFixtureItemDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
        //                    foreach (var property in _properties)
        //                    {
        //                        if (!_excludeProperties.Contains(property.Name))
        //                        {
        //                            Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public void TestUpdate(Type dalFactoryType)
        //{
        //    if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //    {
        //        base.TestUpdate(
        //        dalFactoryType,
        //        _excludeProperties);
        //        return;
        //    }

        //    using (var dalFactory = CreateDalFactory(dalFactoryType))
        //    {
        //        using (var dalContext = dalFactory.CreateContext())
        //        {
        //            InitializeDtosAndProperties(dalContext);

        //            using (var dal = dalContext.GetDal<IPlanogramFixtureItemDal>())
        //            {
        //                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //                {
        //                    var originalSourceDto = _sourceDtoList[0];
        //                    var alternativeSourceDto = _sourceDtoList[i];

        //                    // Update the DTO in memory.
        //                    foreach (var property in _properties)
        //                    {
        //                        if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
        //                        {
        //                            property.SetValue(
        //                                originalSourceDto,
        //                                property.GetValue(alternativeSourceDto, null),
        //                                null);
        //                        }
        //                    }

        //                    // Update the DTO in database and verify success.
        //                    dal.Update(originalSourceDto);
        //                    IEnumerable<PlanogramFixtureItemDto> returnedDtoList = dal.FetchByPlanogramId(originalSourceDto.PlanogramId);
        //                    PlanogramFixtureItemDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(originalSourceDto.Id));

        //                    foreach (var property in _properties)
        //                    {
        //                        if (!_excludeProperties.Contains(property.Name))
        //                        {
        //                            Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //[Test, TestCaseSource("_dbDalFactoryTypes")]
        //public new void TestDeleteById(Type dalFactoryType)
        //{
        //    if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
        //    {
        //        base.TestDeleteById(dalFactoryType);
        //        return;
        //    }

        //    using (var dalFactory = CreateDalFactory(dalFactoryType))
        //    {
        //        using (var dalContext = dalFactory.CreateContext())
        //        {
        //            InitializeDtosAndProperties(dalContext);

        //            using (var dal = dalContext.GetDal<IPlanogramFixtureItemDal>())
        //            {
        //                for (Int32 i = 0; i < _sourceDtoList.Count; i++)
        //                {
        //                    //Call delete
        //                    dal.DeleteById(_sourceDtoList[i].Id);

        //                    IEnumerable<PlanogramFixtureItemDto> returnedDtoList = dal.FetchByPlanogramId(_sourceDtoList[i].PlanogramId);
        //                    PlanogramFixtureItemDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
        //                    Assert.IsNull(returnedDto);
        //                }
        //            }
        //        }
        //    }
        //}

        #endregion
    }
}
