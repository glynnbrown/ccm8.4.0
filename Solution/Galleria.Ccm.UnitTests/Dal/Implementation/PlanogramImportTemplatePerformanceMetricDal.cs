﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using System.Reflection;
using System.Linq;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class PlanogramImportTemplatePerformanceMetricDal : TestBase<IPlanogramImportTemplatePerformanceMetricDal, PlanogramImportTemplatePerformanceMetricDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };
        private List<PlanogramImportTemplatePerformanceMetricDto> _sourceDtoList;
        private List<PlanogramImportTemplateDto> _parentPlanogramImportTemplateDtoList;
        private List<PropertyInfo> _properties;

        private List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.User.DalFactory),
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory)
        };

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramImportTemplatePerformanceMetricDto));
            _properties = typeof(PlanogramImportTemplatePerformanceMetricDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalContext, 1);
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            _parentPlanogramImportTemplateDtoList = TestDataHelper.InsertPlanogramImportTemplateDtos(dalContext, 2, entityList[0].Id);
            _sourceDtoList = TestDataHelper.InsertPlanogramImportTemplatePerformanceMetricDtos(dalContext, _parentPlanogramImportTemplateDtoList);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramImportTemplateId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                IEnumerable<PlanogramImportTemplatePerformanceMetricDto> returnedDtoList = dal.FetchByPlanogramImportTemplateId(_sourceDtoList[i].PlanogramImportTemplateId);
                                PlanogramImportTemplatePerformanceMetricDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name))
                                    {
                                        Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                TestFetchByPlanogramImportTemplateId(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);
                        _excludeProperties.Add("MetricId");
                        _excludeProperties.Add("PlanogramImportTemplateId");

                        using (var dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                var originalSourceDto = _sourceDtoList[0];
                                var alternativeSourceDto = _sourceDtoList[i];

                                // Update the DTO in memory.
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
                                    {
                                        property.SetValue(
                                            originalSourceDto,
                                            property.GetValue(alternativeSourceDto, null),
                                            null);
                                    }
                                }

                                // Update the DTO in database and verify success.
                                dal.Update(originalSourceDto);
                                var returnedDto = dal.FetchByPlanogramImportTemplateId(originalSourceDto.PlanogramImportTemplateId).ElementAt(0);
                                //CheckCreatedModifiedDeletedDates(originalSourceDto, returnedDto);
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name))
                                    {
                                        Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                //Ensure it exists
                                var returnedDto = dal.FetchByPlanogramImportTemplateId(_sourceDtoList[i].PlanogramImportTemplateId).ElementAt(0);

                                //Delete planogram
                                dal.DeleteById(_sourceDtoList[i].Id);
                            }

                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                IEnumerable<PlanogramImportTemplatePerformanceMetricDto> returnedDtoList = dal.FetchByPlanogramImportTemplateId(_sourceDtoList[i].PlanogramImportTemplateId);
                                PlanogramImportTemplatePerformanceMetricDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                                Assert.IsNull(returnedDto);
                            }
                        }
                    }
                }
            }
        }
    }
}