﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25450 L.Hodson
//  Created
#endregion

#region Version History : CCM 802
// V8-29078 : A.Kuszyl
//  Added FetchByProductGroupCodes
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ProductGroupInfoDal : TestBase<IProductGroupInfoDal, ProductGroupInfoDto>
    {
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchDeletedByProductHierarchyId(Type dalFactoryType)
        {
            throw new InconclusiveException("TODO");
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByProductGroupIds(Type dalFactoryType)
        {
            base.TestInfoFetchByProperties<ProductGroupDto, Int32>(
               dalFactoryType,
               "FetchByProductGroupIds",
               new List<String> { },
               "Id",
               true);

        }
        
        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByProductGroupCodes(Type dalFactoryType)
        {
            base.TestInfoFetchByProperties<ProductGroupDto, String>(
               dalFactoryType,
               "FetchByProductGroupCodes",
               new List<String> { },
               "Code",
               true);

        }
    }
}
