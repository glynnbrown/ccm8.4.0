﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_A)]
    public sealed class PlanogramComparisonTemplateInfoDal : TestBase<IPlanogramComparisonTemplateInfoDal, PlanogramComparisonTemplateInfoDto>
    {
        #region Fields

        private static readonly Type MssqlDalFactoryType = typeof(Ccm.Dal.Mssql.DalFactory);
        private static readonly Type MockDalFactoryType = typeof(Ccm.Dal.Mock.DalFactory);
        private static readonly Type UserDalFactoryType = typeof (Ccm.Dal.User.DalFactory);

        private readonly List<Type> _dalFactoryTypes = new List<Type>
                                                       {
                                                           MockDalFactoryType,
                                                           MssqlDalFactoryType,
                                                           UserDalFactoryType
                                                       };

        #endregion

        #region Fetch

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByIds(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            TestInfoFetchByProperties<PlanogramComparisonTemplateDto, Object>(dalFactoryType, "FetchByIds", new List<String>(), "Id");
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            TestInfoFetchByEntityId<PlanogramComparisonTemplateDto>(dalFactoryType);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityIdIncludingDeleted(Type dalFactoryType)
        {
            if (dalFactoryType == UserDalFactoryType) Assert.Ignore("TestBase does not currently support User DAL implementation tests.");

            TestInfoFetchByProperty<PlanogramComparisonTemplateDto>(dalFactoryType, "FetchByEntityIdIncludingDeleted", "EntityId", true);
        }
        #endregion
    }
}