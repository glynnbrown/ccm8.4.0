﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25476 : A. Kuszyk
//      Initial version.
// V8-25632 : K. Pickup
//      Refactored to work with the framework version of the DalImplementationTestBase, which I introduced as part of 
//      the fix for a test in ConsumerDecisionTreeLevelDal.
// V8-28840 : L.Luong
//      Changed Tests in incorporate entityId 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PackageDal : TestBase<IPackageDal,PackageDto>
    {
        public PackageDal() : base(typeof(Galleria.Ccm.Dal.Mock.DalFactory), typeof(Galleria.Ccm.Dal.Mssql.DalFactory)) { }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                Assert.Inconclusive("Cannot test FetchById in the Mock Test Base as there are two overloaded FetchById Methods.");
            }

            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    using (var dal = dalContext.GetDal<IPackageDal>())
                    {
                        // Insert a planogram with Id
                        var dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramDto));
                        var planogramsInserted = CreateDtos<PlanogramDto>(dalContext);
                        planogramsInserted.InsertDtos(dalContext, dtoSpec);

                        // Fetch Package with Id, Assert that we get similar object.
                        foreach (var planogram in planogramsInserted.Dtos)
                        {
                            var package = dal.FetchById(planogram.Id, null);
                            //Assert.AreEqual(package.DateCreated, planogram.DateCreated);
                            //Assert.AreEqual(package.DateLastModified, planogram.DateLastModified);
                            //Assert.AreEqual(package.Name, planogram.Name);
                        }
                    }
                }
            }
        }

        [Test]
        public void TestLockById()
        {
            Assert.Pass();
        }

        [Test]
        public void TestUnlockById()
        {
            Assert.Pass();
        }

        [Test]
        public void TestInsert()
        {
            Assert.Pass();
        }

        [Test]
        public void TestUpdate()
        {
            Assert.Pass();
        }

        [Test]
        public void TestDeleteById()
        {
            Assert.Pass();
        }
    }
}
