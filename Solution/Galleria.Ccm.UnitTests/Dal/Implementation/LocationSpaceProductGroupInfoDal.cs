﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (SA 1.0)
// V8-29026 : D.Pleasance
//      Copied from SA
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationSpaceProductGroupInfoDal : TestBase<ILocationSpaceProductGroupInfoDal, LocationSpaceProductGroupInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByProductGroupId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {

                // insert some sample dtos into the data source
                List<EntityDto> entityDtoList = TestDataHelper.InsertEntityDtos(dalFactory, 1);
                Int32 entityId1 = entityDtoList[0].Id;

                List<ProductHierarchyDto> hierarchy1Dtos = TestDataHelper.InsertProductHierarchyDtos(dalFactory, 1, entityId1);
                List<ProductLevelDto> hierarchy1levelDtos = TestDataHelper.InsertProductLevelDtos(dalFactory, hierarchy1Dtos[0].Id, 3);
                List<ProductGroupDto> hierarchy1GroupDtos = TestDataHelper.InsertProductGroupDtos(dalFactory, hierarchy1levelDtos, 5);
                
                List<LocationHierarchyDto> locHierarchy1Dtos = TestDataHelper.InsertLocationHierarchyDtos(dalFactory, entityDtoList);
                List<LocationLevelDto> locHierarchy1levelDtos = TestDataHelper.InsertLocationLevelDtos(dalFactory, locHierarchy1Dtos[0].Id, 3);
                List<LocationGroupDto> locHierarchy1GroupDtos = TestDataHelper.InsertLocationGroupDtos(dalFactory, locHierarchy1levelDtos, 5);

                List<LocationDto> sourceLocationDtoList1 = TestDataHelper.InsertLocationDtos(dalFactory, entityId1, locHierarchy1GroupDtos.Select(p => p.Id), 5);

                List<LocationSpaceDto> sourceLocationSpaceDtoList = TestDataHelper.InsertLocationSpaceDtos(dalFactory, sourceLocationDtoList1, entityId1);
                List<LocationSpaceProductGroupDto> sourceLocationSpaceProductGroupDtoList = new List<LocationSpaceProductGroupDto>();
                foreach (LocationSpaceDto locationSpace in sourceLocationSpaceDtoList)
                {
                    sourceLocationSpaceProductGroupDtoList.AddRange(TestDataHelper.InsertLocationSpaceProductGroupDtos(dalFactory, locationSpace.Id, hierarchy1GroupDtos.Take(5)));
                }

                //Get distinct product group ids
                IEnumerable<Int32> distinctProductGroupIds = sourceLocationSpaceProductGroupDtoList.Select(p => p.ProductGroupId).Distinct();

                // fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    foreach (Int32 productGroupId in distinctProductGroupIds)
                    {
                        dalContext.Begin();
                        using (ILocationSpaceProductGroupInfoDal dal = dalContext.GetDal<ILocationSpaceProductGroupInfoDal>())
                        {
                            List<LocationSpaceProductGroupInfoDto> returnedDtoList = dal.FetchByProductGroupId(productGroupId).ToList();
                            Assert.AreEqual(sourceLocationSpaceDtoList.Count, returnedDtoList.Count, "Should be a 1 record for all 75 locations inserted");
                            Assert.IsTrue(returnedDtoList.All(p => p.ProductGroupId == productGroupId), "All product group ids should match the parameter");

                            //check to make sure that you can't fetch when deleted
                            using (ILocationSpaceProductGroupDal productGroupDal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                            {
                                foreach (LocationSpaceProductGroupInfoDto dto in returnedDtoList)
                                {
                                    productGroupDal.DeleteById(dto.Id);
                                }
                            }

                            returnedDtoList = dal.FetchByProductGroupId(productGroupId).ToList();

                            Assert.AreEqual(0, returnedDtoList.Count);
                        }
                        dalContext.Commit();
                    }
                }
            }
        }
    }
}