﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24265 : N.Haywood
//		Created
// V8-27940 : L.Luong
//      Test now use the Mssql dalfactory
//      added FetchByEntityId and FetchByEntityIdIncludingDeleted
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    public sealed class LabelInfoDal : TestBase<ILabelInfoDal, LabelInfoDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };

        private List<Type> _dbDalFactoryTypes = new List<Type> 
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory),
            typeof(Galleria.Ccm.Dal.User.DalFactory)
        };

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByIds(Type dbDalFactoryType)
        {
            if (dbDalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                using (var dalFactory = CreateDalFactory(dbDalFactoryType))
                using (var dalContext = dalFactory.CreateContext())
                using (var dal = dalContext.GetDal<ILabelInfoDal>())
                {
                    var dto = CreateDtos<LabelDto>(dalContext);
                    var list = dto.Dtos;

                    List<Object> ids = new List<Object>();

                    foreach (var id in list) 
                    {
                        ids.Add(id.Id);
                    }

                    IEnumerable<LabelInfoDto> labelInfos = dal.FetchByIds(ids);

                    foreach (LabelInfoDto labelInfo in labelInfos) 
                    {
                        var label = list.Where(l => Object.Equals(l.Id,labelInfo.Id));
                        Assert.IsNotNull(label);
                    }
                }
            }
            else
            {
                base.TestInfoFetchByProperties<LabelDto, Object>(
                    dbDalFactoryType,
                    "FetchByIds",
                    new List<String> { },
                    "Id",
                    /*includesDeleted*/true);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            if (dbDalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                TestInfoFetchByEntityId<LabelDto>(dbDalFactoryType);
            }
        }

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdIncludingDeleted(Type dbDalFactoryType)
        {
            if (dbDalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TestBase doesn't currently support user implementation tests. (Entity)");
            }
            else
            {
                TestInfoFetchByProperty<LabelDto>(dbDalFactoryType, "FetchByEntityIdIncludingDeleted", "EntityId", true);
            }
        }
    }
}
