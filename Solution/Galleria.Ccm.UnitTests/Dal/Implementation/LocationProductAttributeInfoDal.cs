﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// CCM-25449 : N.Haywood
//	Copied from SA
// V8-26322 : A.Silva
//  Made tests inconclusive as they are either not implemented or require some changes to TestBase.

#endregion

#endregion

using System;
using System.Collections.Generic;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationProductAttributeInfoDal : TestBase<ILocationProductAttributeInfoDal, LocationProductAttributeInfoDto>
    {
        #region Constructor

        public LocationProductAttributeInfoDal() : base(typeof(Ccm.Dal.Mock.DalFactory), typeof(Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            Assert.Inconclusive("Current Testbase does not taste this case correctly."); // TODO Amend Test base to allow running this test in this case.
            base.TestInfoFetchByEntityId<LocationProductAttributeDto>(dalFactoryType);
        }

       
        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityIdSearchCriteria(Type dalFactoryType)
        {
            Assert.Ignore("TODO");
        }

       
    }
}
