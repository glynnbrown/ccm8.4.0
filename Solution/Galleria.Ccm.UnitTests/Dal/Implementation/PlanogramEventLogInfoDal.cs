﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 810)
// V8-29861 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Reflection;
using Galleria.Ccm.Dal.Mock;
using Galleria.Ccm.Model;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    [Category(Categories.PlanogramDal_B)]
    public sealed class PlanogramEventLogInfoDal : TestBase<IPlanogramEventLogInfoDal, PlanogramEventLogInfoDto>
    {
        #region Fields

        private List<WorkpackageDto> _testWorkpackageDtos;
        private List<PlanogramEventLogDto> _testDtos;
        private UserDto _userDto;

        #endregion

        #region Constructor

        public PlanogramEventLogInfoDal()
            : base(typeof (DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        [Test, TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByWorkpackageId(Type dalFactoryType)
        {
            using (var dalFactory = CreateDalFactory(dalFactoryType))
            {
                using (var dalContext = dalFactory.CreateContext())
                {
                    InitializeDtosAndProperties(dalContext, 5, 5);

                    using (var dal = dalContext.GetDal<IPlanogramEventLogInfoDal>())
                    {
                        for (Int32 i = 0; i < this._testWorkpackageDtos.Count; i++)
                        {
                            IEnumerable<PlanogramEventLogInfoDto> returnedDtoList = dal.FetchByWorkpackageId(Convert.ToInt32(_testWorkpackageDtos[i].Id));

                            //Only 5 should exist
                            Assert.AreEqual(25, returnedDtoList.Count());
                            //Find corresponding
                            PlanogramEventLogInfoDto returnedDto = returnedDtoList.FirstOrDefault();
                            Assert.IsNotNull(returnedDto, "Dto should have been found");
                            //Check properties
                            base.IsInfoDtoEqualToDto<PlanogramEventLogDto>(returnedDto, _testDtos[i]);
                        }
                    }
                }
            }
        }

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            InitializeDtosAndProperties(dalContext, 25, 1);
        }

        private void InitializeDtosAndProperties(IDalContext dalContext, Int32 packageCount, Int32 planogramCount)
        {
            _userDto = TestDataHelper.InsertUserDtos(dalContext, 1).First();
            var userName = _userDto.UserName;
            var entity = TestDataHelper.InsertEntityDtos(dalContext, 1);

            List<PackageDto> packageDtoList = TestDataHelper.InsertPackageDtos(dalContext, userName, entity[0].Id, 5);
            List<WorkflowDto> workflowDtoList = TestDataHelper.InsertWorkflowDtos(dalContext, entity, 1);
            _testWorkpackageDtos = TestDataHelper.InsertWorkPackageDtos(dalContext, workflowDtoList, _userDto);
            List<PlanogramDto> planogramDtoList = TestDataHelper.InsertPlanogramDtos(dalContext, userName, 1, packageDtoList);
            List<WorkpackagePlanogramDto> workpackagePlanogramDtoList = TestDataHelper.InsertWorkpackagePlanogramDtos(dalContext, _testWorkpackageDtos, planogramDtoList);
            _testDtos = TestDataHelper.InsertPlanogramEventLogDtos(dalContext, 5, planogramDtoList);
        }

    }
}
