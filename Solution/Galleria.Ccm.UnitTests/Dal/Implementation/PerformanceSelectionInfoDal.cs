﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//     Created
// CCM-27172 : I.George
//    Added TestFetchByEntityIdIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PerformanceSelectionInfoDal : TestBase<IPerformanceSelectionInfoDal, PerformanceSelectionInfoDto>
    {
        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };


        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<PerformanceSelectionDto>(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityIdIncludingDeleted(Type dbDalFactoryType)
        {
            base.TestInfoFetchByProperty<PerformanceSelectionDto>(dbDalFactoryType, "FetchByEntityIdIncludingDeleted", "EntityId", true);
        }
    }
}

