﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.03)

// CCM-29268 : L.Ineson
//  Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class LocationProductIllegalDal : TestBase<ILocationProductIllegalDal, LocationProductIllegalDto>
    {
        private readonly List<String> _excludedProperties = new List<String> { "LocationId", "ProductId" };

        private List<Type> DalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByEntityId(Type dalFactoryType)
        {
            base.TestFetchByEntityId(dalFactoryType);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            TestDeleteByProperties(dalFactoryType, "DeleteById", _excludedProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteByEntityId(Type dalFactoryType)
        {
            base.TestDeleteByEntityId(dalFactoryType);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            Assert.Ignore("TODO");
            base.TestInsert(dalFactoryType, null);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            Assert.Ignore("TODO");
            base.TestUpdate(dalFactoryType);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public new void TestUpsert(Type dalFactoryType)
        {
            Assert.Ignore("TODO");
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            TestFetchByProperties(dalFactoryType, 
                "FetchById", 
                new List<String>{"LocationId", "ProductId"},
                _excludedProperties);
        }

        [Test, TestCaseSource("DalFactoryTypes")]
        public void TestFetchByLocationIdProductIdCombinations(Type dalFactoryType)
        {
            Assert.Ignore("TODO");
        }
    }
}
