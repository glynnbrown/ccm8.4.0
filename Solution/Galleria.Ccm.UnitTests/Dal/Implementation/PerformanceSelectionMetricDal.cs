﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    class PerformanceSelectionMetricDal : TestBase<IPerformanceSelectionMetricDal, PerformanceSelectionMetricDto>
    {
        private List<Type> DalFactoryTypes = new List<Type> 
        {
           typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
           typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestFetchByPerformanceSelectionId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(dbDalFactoryType, "FetchByPerformanceSelectionId", "PerformanceSelectionId");
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, null, true);
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType);
        }

        [Test]
        [TestCaseSource("DalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }
    }
}
