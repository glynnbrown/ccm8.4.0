﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : L.Luong
//      Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramImportTemplateInfoDal : TestBase<IPlanogramImportTemplateInfoDal, PlanogramImportTemplateInfoDto>
    {
        private readonly List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Ccm.Dal.Mssql.DalFactory),
            typeof(Ccm.Dal.Mock.DalFactory)
        };

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            TestInfoFetchByEntityId<PlanogramImportTemplateDto>(dbDalFactoryType);
        }

    }
}
