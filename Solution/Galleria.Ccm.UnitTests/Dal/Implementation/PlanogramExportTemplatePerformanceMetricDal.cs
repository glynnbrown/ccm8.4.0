﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using NUnit.Framework;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using System.Reflection;
using System.Linq;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public sealed class PlanogramExportTemplatePerformanceMetricDal : TestBase<IPlanogramExportTemplatePerformanceMetricDal, PlanogramExportTemplatePerformanceMetricDto>
    {
        private List<String> _excludeProperties = new List<String> { "Id" };
        private List<PlanogramExportTemplatePerformanceMetricDto> _sourceDtoList;
        private List<PlanogramExportTemplateDto> _parentPlanogramExportTemplateDtoList;
        private List<PropertyInfo> _properties;

        private List<Type> _dalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.User.DalFactory),
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory)
        };

        private void InitializeDtosAndProperties(IDalContext dalContext)
        {
            _dtoSpec = DtoSpec.CreateDtoSpec(typeof(PlanogramExportTemplatePerformanceMetricDto));
            _properties = typeof(PlanogramExportTemplatePerformanceMetricDto).GetProperties().Where(p => p.CanWrite).ToList();

            //Insert parents
            List<EntityDto> entityList = TestDataHelper.InsertEntityDtos(dalContext, 1);
            var userName = TestDataHelper.InsertUserDtos(dalContext, 1).First().UserName;
            _parentPlanogramExportTemplateDtoList = TestDataHelper.InsertPlanogramExportTemplateDtos(dalContext, 2, entityList[0].Id);
            _sourceDtoList = TestDataHelper.InsertPlanogramExportTemplatePerformanceMetricDtos(dalContext, _parentPlanogramExportTemplateDtoList);
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestFetchByPlanogramExportTemplateId(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IPlanogramExportTemplatePerformanceMetricDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                IEnumerable<PlanogramExportTemplatePerformanceMetricDto> returnedDtoList = dal.FetchByPlanogramExportTemplateId(_sourceDtoList[i].PlanogramExportTemplateId);
                                PlanogramExportTemplatePerformanceMetricDto returnedDto = returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name))
                                    {
                                        Assert.AreEqual(property.GetValue(_sourceDtoList[i], null), property.GetValue(returnedDto, null));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                TestFetchByPlanogramExportTemplateId(dalFactoryType);
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);
                        _excludeProperties.Add("MetricId");
                        _excludeProperties.Add("PlanogramExportTemplateId");

                        using (var dal = dalContext.GetDal<IPlanogramExportTemplatePerformanceMetricDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                var originalSourceDto = _sourceDtoList[0];
                                var alternativeSourceDto = _sourceDtoList[i];

                                // Update the DTO in memory.
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name) && property.Name != "Id")
                                    {
                                        property.SetValue(
                                            originalSourceDto,
                                            property.GetValue(alternativeSourceDto, null),
                                            null);
                                    }
                                }

                                // Update the DTO in database and verify success.
                                dal.Update(originalSourceDto);
                                var returnedDto = dal.FetchByPlanogramExportTemplateId(originalSourceDto.PlanogramExportTemplateId).ElementAt(0);
                                //CheckCreatedModifiedDeletedDates(originalSourceDto, returnedDto);
                                foreach (var property in _properties)
                                {
                                    if (!_excludeProperties.Contains(property.Name))
                                    {
                                        Assert.AreEqual(property.GetValue(originalSourceDto, null), property.GetValue(returnedDto, null));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("_dalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.User.DalFactory))
            {
                throw new InconclusiveException("TODO");
            }
            else
            {
                using (var dalFactory = CreateDalFactory(dalFactoryType))
                {
                    using (var dalContext = dalFactory.CreateContext())
                    {
                        InitializeDtosAndProperties(dalContext);

                        using (var dal = dalContext.GetDal<IPlanogramExportTemplatePerformanceMetricDal>())
                        {
                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                //Ensure it exists
                                var returnedDto = dal.FetchByPlanogramExportTemplateId(_sourceDtoList[i].PlanogramExportTemplateId).ElementAt(0);

                                //Delete planogram
                                dal.DeleteById(_sourceDtoList[i].Id);
                            }

                            for (Int32 i = 0; i < _sourceDtoList.Count; i++)
                            {
                                IEnumerable<PlanogramExportTemplatePerformanceMetricDto> returnedDtoList = 
                                    dal.FetchByPlanogramExportTemplateId(_sourceDtoList[i].PlanogramExportTemplateId);
                                PlanogramExportTemplatePerformanceMetricDto returnedDto = 
                                    returnedDtoList.FirstOrDefault(p => p.Id.Equals(_sourceDtoList[i].Id));
                                Assert.IsNull(returnedDto);
                            }
                        }
                    }
                }
            }
        }
    }
}
