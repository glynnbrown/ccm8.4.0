﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8.0)
// V8-25628 : A. Kuszyk
//      Initial version.
// V8-26140 : I.George
//      TestFetchByEntityIdIncludingDeleted
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ClusterSchemeInfoDal : TestBase<IClusterSchemeInfoDal, ClusterSchemeInfoDto>
    {
        private List<Type> _dbDalFactoryTypes = new List<Type>
        {
            typeof(Galleria.Ccm.Dal.Mssql.DalFactory),
            typeof(Galleria.Ccm.Dal.Mock.DalFactory)
        };

        [Test]
        [TestCaseSource("_dbDalFactoryTypes")]
        public void TestFetchByEntityId(Type dbDalFactoryType)
        {
            base.TestInfoFetchByEntityId<ClusterSchemeDto>(dbDalFactoryType);
        }
    }
}
