﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-25788 : M.Pettit
//  Created
// V8-26322 : A.Silva
//      Added missing TestDeleteById.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.Mock;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class DataModelDal : TestBase<IDataModelDal, DataModelDto>
    {
        #region Constructor

        public DataModelDal() : base(typeof (DalFactory), typeof (Ccm.Dal.Mssql.DalFactory)) {}

        #endregion

        #region Helpers

        private void AssertDtoAndDtoAreEqual(DataModelDto sourceDto, DataModelDto dto)
        {
            Assert.AreEqual(sourceDto.Id, dto.Id);
            Assert.AreEqual(sourceDto.RowVersion, dto.RowVersion);
            Assert.AreEqual(sourceDto.FileName, dto.FileName);
            Assert.AreEqual(sourceDto.Name, dto.Name);
            Assert.AreEqual(sourceDto.Description, dto.Description);
            Assert.AreEqual(sourceDto.Data, dto.Data);
            Assert.AreEqual(sourceDto.Version, dto.Version);
            Assert.AreEqual(sourceDto.FormatVersion, dto.FormatVersion);
            Assert.AreEqual(RoundToNearest((DateTime)sourceDto.DateCreated, TimeSpan.FromMinutes(1)), RoundToNearest((DateTime)dto.DateCreated, TimeSpan.FromMinutes(1)));
            Assert.AreEqual(RoundToNearest((DateTime)sourceDto.DateLastModified, TimeSpan.FromMinutes(1)), RoundToNearest((DateTime)dto.DateLastModified, TimeSpan.FromMinutes(1)));
        }

        private DateTime RoundToNearest(DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            bool roundUp = delta > d.Ticks / 2;

            if (roundUp)
            {
                var deltaU = (d.Ticks - (dt.Ticks % d.Ticks)) % d.Ticks;
                return new DateTime(dt.Ticks + deltaU);
            }
            else
            {
                var deltaD = dt.Ticks % d.Ticks;
                return new DateTime(dt.Ticks - deltaD);
            }
        }

        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                IEnumerable<DataModelDto> sourceDtoList = TestDataHelper.InsertDataModelDtos(dalFactory);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin(); 
                    
                    using (IDataModelDal dal = dalContext.GetDal<IDataModelDal>())
                    {
                        foreach (DataModelDto sourceDto in sourceDtoList)
                        {
                            DataModelDto dalDto = dal.FetchById(sourceDto.Id);
                            AssertDtoAndDtoAreEqual(dalDto, sourceDto);
                        }
                    }
                }
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                IEnumerable<DataModelDto> dtoList = TestDataHelper.InsertDataModelDtos(dalFactory);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IDataModelDal dal = dalContext.GetDal<IDataModelDal>())
                    {
                        foreach (DataModelDto sourceDto in dtoList)
                        {
                            DataModelDto compareDto = dal.FetchById(sourceDto.Id);
                            AssertDtoAndDtoAreEqual(sourceDto, compareDto);
                        }
                    }
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                IEnumerable<DataModelDto> dtoList = TestDataHelper.InsertDataModelDtos(dalFactory);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IDataModelDal dal = dalContext.GetDal<IDataModelDal>())
                    {
                        Int32 index = 1;
                        foreach (DataModelDto sourceDto in dtoList)
                        {
                            // fetch the dto
                            DataModelDto compareDto = dal.FetchById(sourceDto.Id);

                            // and assert that they are equal
                            AssertDtoAndDtoAreEqual(sourceDto, compareDto);

                            // change the compare dto
                            compareDto.FileName = String.Format("{0}_{1}", compareDto.FileName, index);
                            compareDto.Name = String.Format("{0}_{1}", compareDto.Name, index);
                            compareDto.Description = String.Format("{0}_{1}", compareDto.Description, index);

                            // update the
                            dal.Update(compareDto);

                            // fetch the dto from the database
                            DataModelDto newDto = dal.FetchById(sourceDto.Id);

                            // compare
                            Assert.AreNotEqual(sourceDto, compareDto);
                            Assert.AreEqual(compareDto, newDto);

                            // increase the index
                            index++;
                        }
                    }
                }
            }
        }
        #endregion

        #region FetchAll
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchAll(Type dalFactoryType)
        {
            Assert.Inconclusive("No FetchAll method or DataModel objects");
        }
        #endregion

        #region DeleteById

        /// <summary>
        ///     Test the DeleteBy method.
        /// </summary>
        /// <param name="dalFactoryType"></param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            Assert.Inconclusive("No DeleteById method.");
        }

        #endregion
    }
}
