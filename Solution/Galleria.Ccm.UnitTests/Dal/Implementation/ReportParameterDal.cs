﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM800
// V8-25788 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Dal;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Dal.Interfaces;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Reporting.Model;

namespace Galleria.Ccm.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class ReportParameterDal : TestBase<IReportParameterDal, ReportParameterDto>
    {
        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public new IEnumerable<Type> DbDalFactoryTypes
        {
            get
            {
                yield return typeof(Galleria.Ccm.Dal.Mock.DalFactory);
                yield return typeof(Galleria.Ccm.Dal.Mssql.DalFactory);
            }
        }
        #endregion

        #region Fields
        List<ReportDto> _reportDtos = new List<ReportDto>();
        #endregion

        #region Helpers

        private List<ReportParameterDto> InsertDtos(IDalFactory dalFactory, Int32 numToInsert)
        {
            List<DataModelDto> dataModelDtos = TestDataHelper.InsertDataModelDtos(dalFactory);
            _reportDtos = TestDataHelper.InsertReportDtos(dalFactory, numToInsert, dataModelDtos);
            List<ReportParameterDto> ReportParameterDtos = TestDataHelper.InsertReportParameterDtos(dalFactory, numToInsert, _reportDtos);
            return ReportParameterDtos;
        }
        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportParameterDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportParameterDal dal = dalContext.GetDal<IReportParameterDal>())
                    {
                        foreach (ReportParameterDto sourceDto in sourceDtoList)
                        {
                            ReportParameterDto dalDto = dal.FetchById(sourceDto.Id);
                            Assert.AreEqual(dalDto, sourceDto);

                            //Test return following deletion
                            dal.DeleteById(dalDto.Id);

                            ReportParameterDto returnedDto;
                            Assert.Throws<DtoDoesNotExistException>
                                (() => { returnedDto = dal.FetchById(dalDto.Id); },
                                "Deleted items should be returned when fetched by Id");
                        }
                    }
                }
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByReportId(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportParameterDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportParameterDal dal = dalContext.GetDal<IReportParameterDal>())
                    {
                        foreach (ReportDto reportDto in _reportDtos)
                        {
                            List<ReportParameterDto> dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            List<ReportParameterDto> sourceDtosInReport = sourceDtoList.Where(r => r.ReportId == reportDto.Id).ToList();

                            Assert.AreEqual(sourceDtosInReport.Count, dalDtos.Count, "Correct number of items should be returned");

                            foreach (ReportParameterDto sourceDto in sourceDtosInReport)
                            {
                                ReportParameterDto dalDto = dalDtos.FirstOrDefault(s => s.Id == sourceDto.Id);
                                Assert.IsNotNull(dalDto);
                                Assert.AreEqual(dalDto, sourceDto);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportParameterDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportParameterDal dal = dalContext.GetDal<IReportParameterDal>())
                    {
                        foreach (ReportDto reportDto in _reportDtos)
                        {
                            List<ReportParameterDto> dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            List<ReportParameterDto> sourceDtosInReport = sourceDtoList.
                                Where(r => r.ReportId == reportDto.Id).ToList();

                            Assert.AreEqual(sourceDtosInReport.Count, dalDtos.Count,
                                "Correct number of items should be returned");

                            foreach (ReportParameterDto sourceDto in sourceDtosInReport)
                            {
                                ReportParameterDto dalDto = dalDtos.FirstOrDefault(s => s.Id == sourceDto.Id);
                                Assert.IsNotNull(dalDto);
                                Assert.AreEqual(dalDto, sourceDto, "Each item should match");
                            }
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Tests that a dto inserted into the data source
        /// can also be fetched
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportParameterDto> sourceDtoList = InsertDtos(dalFactory, 2);

                //fetch each group of dtos from the dal that match the source dto
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportParameterDal dal = dalContext.GetDal<IReportParameterDal>())
                    {
                        foreach (ReportDto reportDto in _reportDtos)
                        {
                            List<ReportParameterDto> dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            List<ReportParameterDto> sourceDtosInReport = sourceDtoList.
                                Where(r => r.ReportId == reportDto.Id).ToList();

                            ReportParameterDto dalDto = dalDtos.First();
                            Int32 dtoId = dalDto.Id;
                            ReportParameterDto sourceDto = sourceDtoList.First(p => p.Id == dtoId);

                            //update the dalDto
                            dalDto.Name = "New Parameter Name";
                            dalDto.DataFieldId = "NewDataFieldId";

                            dal.Update(dalDto);

                            dalDtos = dal.FetchByReportId(reportDto.Id).ToList();
                            dalDto = dalDtos.First(p => p.Id == dtoId);
                            Assert.AreNotEqual(sourceDto, dalDto);
                            Assert.AreEqual("New Parameter Name", dalDto.Name);
                            Assert.AreEqual("NewDataFieldId", dalDto.DataFieldId);
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Tests that a product universe can be deleted
        /// </summary>
        /// <param name="dalFactoryType">The dal factory to test</param>
        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            using (IDalFactory dalFactory = this.CreateDalFactory(dalFactoryType))
            {
                //insert sample dtos into the data source
                List<ReportParameterDto> dtoList = InsertDtos(dalFactory, 2);

                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();
                    using (IReportParameterDal dal = dalContext.GetDal<IReportParameterDal>())
                    {
                        foreach (ReportParameterDto sourceDto in dtoList)
                        {
                            // delete the dto
                            dal.DeleteById(sourceDto.Id);

                            List<ReportParameterDto> returnedDtos = dal.FetchByReportId(sourceDto.ReportId).ToList();
                            ReportParameterDto returnedDto = returnedDtos.FirstOrDefault(p => p.Id == sourceDto.Id);
                            Assert.IsNull(returnedDto, "No item should be returned");
                        }
                    }
                    dalContext.Commit();
                }
            }
        }
        #endregion
    }
}
