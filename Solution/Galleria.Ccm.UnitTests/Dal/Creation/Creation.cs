﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: CCM830

// V8-31970 : A.Silva
//  Added test for duplicate and overlapping indexes.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Galleria.Ccm.Security;
using Galleria.Ccm.UnitTests.Helpers;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Processes;
using Microsoft.SqlServer.Management.Smo;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Dal.Creation
{
    [TestFixture]
    public class Creation
    {
        #region Fields
        private readonly String _dalMssqlDatabase = Guid.NewGuid().ToString();
        private readonly List<Type> _dalFactoryTypes;
        private readonly Type _mssqlDalFactoryType = typeof(Ccm.Dal.Mssql.DalFactory);
        private readonly Type _mockDalFactoryType = typeof (Ccm.Dal.Mock.DalFactory);
        //private string _databaseFilename;
        #endregion

        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DalFactoryTypes
        {
            get { return _dalFactoryTypes; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Creation()
        {
            // create our list of dal factory types to create
            _dalFactoryTypes = new List<Type> {_mssqlDalFactoryType};
            // _dalFactoryTypes.Add(typeof(Galleria.Ccm.Dal.VistaDb.DalFactory));
        }
        #endregion

        #region Tests
        /// <summary>
        /// Tests that the database creation scripts are working
        /// </summary>
        /// <param name="dalFactoryType">The dal factory type</param>
        [Category(Categories.SmokeDal)]
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Create(Type dalFactoryType)
        {
            // create the dal factory
            IDalFactory dalFactory = CreateDalFactory(dalFactoryType);

            // create the database
            dalFactory.CreateDatabase();

            // upgrade the database to the latest version
            dalFactory.Upgrade(true);

            #region Stored Procedure Checks
            // Check that all stored procedures referenced in the ProcedureNames file exist and appear to have correctly
            // typed parameters.
            Boolean allStoredProcsOk = true;
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                if (dalFactoryType == _mssqlDalFactoryType)
                {
                    #region MsSql
                    Server server = new Server(TestingConstants.MssqlServerInstance);
                    Database database = server.Databases[_dalMssqlDatabase];

                    Type type = typeof(Galleria.Ccm.Dal.Mssql.Schema.ProcedureNames);
                    foreach (FieldInfo field in type.GetFields())
                    {
                        String storedProcedureName = (String)field.GetValue(null);
                        Int32 periodIndex = storedProcedureName.IndexOf(".");
                        String storedProcedureSchema = storedProcedureName.Substring(0, periodIndex);
                        storedProcedureName = storedProcedureName.Substring(periodIndex + 1);
                        Boolean exists = database.StoredProcedures.Contains(storedProcedureName, storedProcedureSchema);
                        if (!exists)
                        {
                            // TODO: Readd this code when the database schema is more mature.  At the moment
                            // loads of stored procedures are missing just because they haven't been developed yet, 
                            // but the team is aware of this.
                            //allStoredProcsFound = false;
                            //Debug.WriteLine(String.Format("{0} stored procedure missing from Mssql database.",
                            //    storedProcedureName));
                        }
                        else
                        {
                            #region Check Parameter Types
                            StoredProcedure storedProcedure = database.StoredProcedures[storedProcedureName, storedProcedureSchema];
                            foreach (StoredProcedureParameter parameter in storedProcedure.Parameters)
                            {
                                if (parameter.Name.Contains("_"))
                                {
                                    String tableName = parameter.Name.Substring(1, parameter.Name.IndexOf("_") - 1);
                                    String tableSchema = GetTableSchema(database, tableName);
                                    if (tableSchema != null)
                                    {
                                        String columnName = parameter.Name.Substring(1);
                                        Table table = database.Tables[tableName, tableSchema];
                                        if (table.Columns.Contains(columnName))
                                        {
                                            Column column = table.Columns[columnName];
                                            if (!column.DataType.Equals(parameter.DataType))
                                            {
                                                allStoredProcsOk = false;
                                                Debug.WriteLine(String.Format(
                                                    "Parameter {0} in stored procedure {1} is of type {2}, length {3}, but column {4} in table {5} is of type {6}, length {7}",
                                                    parameter.Name,
                                                    storedProcedureName,
                                                    parameter.DataType.SqlDataType.ToString(),
                                                    parameter.DataType.MaximumLength,
                                                    columnName,
                                                    tableName,
                                                    column.DataType.SqlDataType.ToString(),
                                                    column.DataType.MaximumLength));
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            Assert.IsTrue(allStoredProcsOk, "Problems were found with the stored procedures.  See Text Output tab for details.");
            #endregion

            DalContainer.RegisterFactory("Server", dalFactory);

            CheckDefaultData(dalFactory);
        }

        /// <summary>
        /// Tests that the database scripts are repeatable
        /// </summary>
        /// <param name="dalFactoryType"></param>
        [Category(Categories.SmokeDal)]
        [Test, TestCaseSource("DalFactoryTypes")]
        public void Repeatablility(Type dalFactoryType)
        {
            // create the dal factory
            IDalFactory dalFactory = CreateDalFactory(dalFactoryType);

            // create the database
            dalFactory.CreateDatabase();

            // upgrade the database to the latest version
            dalFactory.Upgrade(true);

            // reset the version number
            ResetVersionNumber(dalFactory);

            // perform the upgrade again
            dalFactory.Upgrade(true);
        }

        /// <summary>
        /// Tests that the Create Sample data process
        /// executes correctly after a database has been created
        /// </summary>
        /// <param name="dalFactoryType"></param>
        [Category(Categories.SmokeDal)]
        [Test, TestCaseSource("DalFactoryTypes")]
        public void CreateSampleData(Type dalFactoryType)
        {
            #region User dal

            String templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                   Galleria.Ccm.Constants.AppDataFolderName, "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (String file in Directory.GetFiles(templateDir))
                {
                    try
                    {
                        if (LockedFileManager.WaitForFileToBeUnlocked(file))
                            File.Delete(file);
                    }
                    catch
                    {
                        // No point in crying over spilt milk
                    }
                }
            }


            String dalAssemblyName = "Galleria.Ccm.Dal.User.dll";
            var userFactory = new Ccm.Dal.User.DalFactory(new DalFactoryConfigElement(Constants.UserDal, dalAssemblyName), /*isTesting*/true);
            userFactory.UnitTestFolder = templateDir;
            DalContainer.RegisterFactory(Constants.UserDal, userFactory);

            #endregion

            // create the dal factory
            IDalFactory dalFactory = CreateDalFactory(dalFactoryType);

            // create the database
            dalFactory.CreateDatabase();

            // upgrade the database to the latest version
            dalFactory.Upgrade(true);

            // record the current dal factory name
            String previousDalName = DalContainer.DalName;

            // register the dal factory within the dal container
            // under a temporary dal name
            String dalName = Guid.NewGuid().ToString();
            DalContainer.RegisterFactory(dalName, dalFactory);
            DalContainer.DalName = dalName;

            // authenticate the user and assert that
            // the user has been successfully authenticated
            Assert.IsTrue(DomainPrincipal.Authenticate());

            // now create an instance of the sample data process
            // and execute it
            IProcess process = new Galleria.Ccm.Processes.DatabaseMaintenance.CreateSampleData();
            process.Execute();

            // remove the dal factory and reset the dal container
            DalContainer.RemoveFactory(dalName);
            DalContainer.DalName = previousDalName;
        }

        
       

        [Category(Categories.SmokeDal)]
        [Test, TestCaseSource("DalFactoryTypes")]
        public void IndexRedundancyCheck(Type dalFactoryType)
        {
            if (dalFactoryType != _mssqlDalFactoryType) Assert.Inconclusive("Only for MSSQL DAL");

            IDalFactory dalFactory = CreateDalFactory(dalFactoryType);
            dalFactory.CreateDatabase();
            dalFactory.Upgrade(true);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                var server = new Server(TestingConstants.MssqlServerInstance);
                Database database = server.Databases[_dalMssqlDatabase];
                const String index1 = "Index1";
                const String index2 = "Index2";
                const String type = "Type";
                const String duplicate = "Exact duplicate";
                const String includes = "Different includes";
                const String overlapping = "Overlapping columns";
                const String columnsIndexed1 = "Columns Indexed 1";
                const String columnsIndexed2 = "Columns Indexed 2";
                const String columnsIncluded1 = "Columns Included 1";
                const String columnsIncluded2 = "Columns Included 2";
                const String tablename = "TableName";
                DataSet results = database.ExecuteWithResults(
                    "WITH IndexColumns AS " +
                    "   (" +
                    "       SELECT I.object_id AS TableObjectId" +
                    "           , OBJECT_SCHEMA_NAME(I.object_id) + '.' + OBJECT_NAME(I.object_id) AS TableName" +
                    "           , I.index_id AS IndexId" +
                    "           , I.name AS IndexName" +
                    "           , (IndexUsage.user_seeks + IndexUsage.user_scans + IndexUsage.user_lookups) AS IndexUsage" +
                    "           , IndexUsage.user_updates AS IndexUpdates" +
                    "           , (" +
                    "               SELECT CASE is_included_column " +
                    "                       WHEN 1 THEN NULL " +
                    "                       ELSE column_id " +
                    "                   END AS [data()] " +
                    "               FROM sys.index_columns AS IndexColumns " +
                    "               WHERE IndexColumns.object_id = I.object_id " +
                    "                   AND IndexColumns.index_id = I.index_id " +
                    "               ORDER BY index_column_id, column_id " +
                    "               FOR XML PATH('')" +
                    "           ) AS ConcIndexColumnNrs" +
                    "           , (" +
                    "               SELECT CASE is_included_column " +
                    "                       WHEN 1 THEN NULL " +
                    "                       ELSE COL_NAME(I.object_id, column_id) " +
                    "                   END AS [data()] " +
                    "               FROM sys.index_columns AS IndexColumns " +
                    "               WHERE IndexColumns.object_id = I.object_id " +
                    "                   AND IndexColumns.index_id = I.index_id " +
                    "               ORDER BY index_column_id, column_id " +
                    "               FOR XML PATH('')" +
                    "           ) AS ConcIndexColumnNames" +
                    "           , (" +
                    "               SELECT CASE is_included_column " +
                    "                       WHEN 1 THEN column_id " +
                    "                       ELSE NULL " +
                    "                   END AS [data()] " +
                    "               FROM sys.index_columns AS IndexColumns " +
                    "               WHERE IndexColumns.object_id = I.object_id " +
                    "                   AND IndexColumns.index_id = I.index_id " +
                    "               ORDER BY column_id " +
                    "               FOR XML PATH('')" +
                    "           ) AS ConcIncludeColumnNrs" +
                    "           , (" +
                    "               SELECT CASE is_included_column " +
                    "                       WHEN 1 THEN COL_NAME(I.object_id, column_id) " +
                    "                       ELSE NULL " +
                    "                   END AS [data()] " +
                    "               FROM sys.index_columns AS IndexColumns " +
                    "               WHERE IndexColumns.object_id = I.object_id " +
                    "                   AND IndexColumns.index_id = I.index_id " +
                    "               ORDER BY column_id " +
                    "               FOR XML PATH('')" +
                    "           ) AS ConcIncludeColumnNames " +
                    "       FROM sys.indexes AS I " +
                    "           LEFT OUTER JOIN sys.dm_db_index_usage_stats AS IndexUsage " +
                    "               ON IndexUsage.object_id = I.object_id " +
                    "                   AND IndexUsage.index_id = I.index_id " +
                    "                   AND IndexUsage.Database_id = db_id()" +
                    "   )" +
                    "SELECT C1." + tablename +
                    "   , C1.IndexName AS '" + index1 + "'" +
                    "   , C2.IndexName AS '" + index2 + "'" +
                    "   , (CASE " +
                    "       WHEN (C1.ConcIndexColumnNrs = C2.ConcIndexColumnNrs) " +
                    "           AND (C1.ConcIncludeColumnNrs = C2.ConcIncludeColumnNrs) THEN '" + duplicate + "' " +
                    "       WHEN (C1.ConcIndexColumnNrs = C2.ConcIndexColumnNrs) THEN '" + includes + "' " +
                    "       ELSE '" + overlapping + "' " +
                    "   END) AS '" + type + "' " +
                    //"-- , C1.ConcIndexColumnNrs" +
                    //"-- , C2.ConcIndexColumnNrs" +
                    "   , C1.ConcIndexColumnNames AS '" + columnsIndexed1 + "'" +
                    "   , C2.ConcIndexColumnNames AS '" + columnsIndexed2 + "'" +
                    //"-- , C1.ConcIncludeColumnNrs " +
                    //"-- , C2.ConcIncludeColumnNrs" +
                    "   , C1.ConcIncludeColumnNames AS '" + columnsIncluded1 + "'" +
                    "   , C2.ConcIncludeColumnNames AS '" + columnsIncluded2 + "'" +
                    "   , C1.IndexUsage AS 'Index 1 Usage'" +
                    "   , C2.IndexUsage AS 'Index 2 Usage'" +
                    "   , C1.IndexUpdates AS 'Index 1 Updates'" +
                    "   , C2.IndexUpdates AS 'Index 2 Updates'" +
                    "   , 'DROP INDEX ' + C2.IndexName + ' ON ' + C2.TableName AS 'Drop2' " +
                    "   , 'DROP INDEX ' + C1.IndexName + ' ON ' + C1.TableName AS 'Drop1' " +
                    "FROM IndexColumns AS C1 " +
                    "   INNER JOIN IndexColumns AS C2 " +
                    "       ON (C1.TableObjectId = C2.TableObjectId) " +
                    "           AND ( " +
                    //"               -- exact: show lower IndexId as 1 (" +
                    "               (C1.IndexId < C2.IndexId " +
                    "                   AND C1.ConcIndexColumnNrs = C2.ConcIndexColumnNrs " +
                    "                   AND C1.ConcIncludeColumnNrs = C2.ConcIncludeColumnNrs) " +
                    //"               -- different includes: show longer include as 1 " +
                    "               OR (C1.ConcIndexColumnNrs = C2.ConcIndexColumnNrs " +
                    "                   AND LEN(C1.ConcIncludeColumnNrs) > LEN(C2.ConcIncludeColumnNrs)) " +
                    //"               -- overlapping: show longer index as 1 " +
                    "               OR (C1.IndexId <> C2.IndexId " +
                    "                   AND C1.ConcIndexColumnNrs <> C2.ConcIndexColumnNrs " +
                    "                   AND C1.ConcIndexColumnNrs like C2.ConcIndexColumnNrs + ' %')) " +
                    "ORDER BY C1.TableName, C1.ConcIndexColumnNrs");

                var duplicateIndexes = new List<String>();
                var overlappingIndexes = new List<String>();
                var includedIndexes = new List<String>();
                DataTableReader dtr = results.CreateDataReader();
                while (dtr.Read())
                {
                    var affectedTable = (String) dtr[tablename];
                    if (affectedTable.StartsWith("sys.queue_messages_")) continue;

                    var redundancyType = (String) dtr[type];
                    String indexInfo = String.Format(@"Table {1} [{2}]:{0}  {3} -> {4}{0}  {5} -> {6}",
                                                     Environment.NewLine,
                                                     affectedTable,
                                                     redundancyType,
                                                     (String) dtr[index1],
                                                     (String) dtr[index2],
                                                     (String) dtr[columnsIndexed1],
                                                     (String) dtr[columnsIndexed2]);
                    if (redundancyType == duplicate)
                    {
                        duplicateIndexes.Add(indexInfo);
                    }
                    if (redundancyType == overlapping)
                    {
                        overlappingIndexes.Add(indexInfo);
                    }
                    if (redundancyType == includes)
                    {
                        includedIndexes.Add(indexInfo);
                    }
                }

                if (duplicateIndexes.Count > 0)
                {
                    var message = new StringBuilder();
                    message.AppendLine(String.Format("Found {0} duplicate indexes", duplicateIndexes.Count));
                    foreach (String indexInfo in duplicateIndexes)
                    {
                        message.AppendLine(indexInfo);
                    }
                    Assert.Fail(message.ToString());
                }

                if (overlappingIndexes.Count > 0)
                {
                    var message = new StringBuilder();
                    message.AppendLine(String.Format("Found {0} overlapping indexes", overlappingIndexes.Count));
                    foreach (String indexInfo in overlappingIndexes)
                    {
                        message.AppendLine(indexInfo);
                    }
                    Assert.Fail(message.ToString());
                }

                if (includedIndexes.Count > 0)
                {
                    var message = new StringBuilder();
                    message.AppendLine(String.Format("Found {0} included indexes", includedIndexes.Count));
                    foreach (String indexInfo in includedIndexes)
                    {
                        message.AppendLine(indexInfo);
                    }
                    Assert.Inconclusive(message.ToString());
                }
            }
        }

        #endregion

        #region Methods

        private static void ResetVersionNumber(IDalFactory dalFactory)
        {
            Galleria.Ccm.Dal.Mssql.DalFactory mssqlFactory = dalFactory as Galleria.Ccm.Dal.Mssql.DalFactory;
            if (mssqlFactory != null)
            {
                mssqlFactory.ResetVersionNumber();
                return;
            }
            Assert.Inconclusive();
        }

        private static String GetTableSchema(Database database, String tableName)
        {
            String returnValue = null;
            foreach (Schema schema in database.Schemas)
            {
                if (database.Tables.Contains(tableName, schema.Name))
                {
                    returnValue = schema.Name;
                    break;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Creates our dal factory from the provided type
        /// </summary>
        /// <param name="dalFactoryType">The type of dal factory to create</param>
        protected IDalFactory CreateDalFactory(Type dalFactoryType)
        {
            IDalFactory dalFactory = null;
            if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mssql.DalFactory))
            {
                // create the test databse
                CreateDatabase(_dalMssqlDatabase);

                // create a dal configuration item for this factory
                DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement();
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                {
                    Name = "Server",
                    Value = TestingConstants.MssqlServerInstance
                });
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement()
                {
                    Name = "Database",
                    Value = _dalMssqlDatabase
                });

                // create the dal factory
                dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig);
            }
            else if (dalFactoryType == typeof(Galleria.Ccm.Dal.Mock.DalFactory))
            {
                // create the dal factory
                dalFactory = new Galleria.Ccm.Dal.Mock.DalFactory();
            }

            // and return the factory
            return dalFactory;
        }

        /// <summary>
        /// Called after a test has been executed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            // ensure the mssql data is detached
            KillDatabase(_dalMssqlDatabase);

            // ensure we delete the local database
            //if (_databaseFilename != null)
            //{
            //    File.Delete(_databaseFilename);
            //}
        }

        /// <summary>
        /// Creates the specified database on the local sql server
        /// </summary>
        /// <param name="databaseName">The database name</param>
        private static void CreateDatabase(String databaseName)
        {
            // kill the database just in case
            KillDatabase(databaseName);

            // connect to the local server
            Server server = new Server(TestingConstants.MssqlServerInstance);

            // create the database on the local server
            Database database = new Database(server, databaseName);
            database.Create();
        }

        /// <summary>
        /// Deletes the specified database from the local sql server
        /// </summary>
        /// <param name="databaseName">The database to delete</param>
        private static void KillDatabase(String databaseName)
        {
            SqlConnection.ClearAllPools();
            Server server = new Server(TestingConstants.MssqlServerInstance);
            if (server.Databases[databaseName] != null)
            {
                server.KillDatabase(databaseName);
            }
        }

        /// <summary>
        /// Checks the created database for the default data
        /// </summary>
        private void CheckDefaultData(IDalFactory dalFactory)
        {
            //Assert.Inconclusive();
        }
        #endregion
    }
}
