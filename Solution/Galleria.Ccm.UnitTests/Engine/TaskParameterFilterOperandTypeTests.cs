﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31559 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.UnitTests.Engine
{
    [TestFixture]
    public class TaskParameterFilterOperandTypeTests
    {
        private IEnumerable<Object> _equalValues = new List<Object>() { "1", (Byte)2, (Int16)3, 4, 5F, 6D, DateTime.Now, DateTime.Now.AddDays(1)};
        private IEnumerable<Object> _containsValues = new List<Object>() { "1", (Byte)2, (Int16)3, 4, 5F, 6D, "10", "123456" };
        private IEnumerable<Object> _inequalityValues = new List<Object>() { "1a", (Byte)2, (Int16)3, 4, 5F, 6D, "2a", (Byte)7, (Int16)8, 8, 10F, 11D, DateTime.Now, DateTime.Now.AddDays(1) };

        [Test]
        public void IsMatch_Equals([ValueSource("_equalValues")] Object value, [ValueSource("_equalValues")] Object filter)
        {
            Boolean expected = value.Equals(filter);

            Boolean actual = TaskParameterFilterOperandType.Equals.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_Contains([ValueSource("_containsValues")] Object value, [ValueSource("_containsValues")] Object filter)
        {
            Boolean expected = value.ToString().Contains(filter.ToString());

            Boolean actual = TaskParameterFilterOperandType.Contains.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_NotEquals([ValueSource("_equalValues")] Object value, [ValueSource("_equalValues")] Object filter)
        {
            Boolean expected = !value.Equals(filter);

            Boolean actual = TaskParameterFilterOperandType.NotEquals.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_NotContains([ValueSource("_containsValues")] Object value, [ValueSource("_containsValues")] Object filter)
        {
            Boolean expected = !value.ToString().Contains(filter.ToString());

            Boolean actual = TaskParameterFilterOperandType.NotContains.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_GreaterThan([ValueSource("_inequalityValues")] Object value, [ValueSource("_inequalityValues")] Object filter)
        {
            Boolean expected;
            try
            {
                expected = Convert.ToDecimal(value) > Convert.ToDecimal(filter);
            }
            catch
            {
                try
                {
                    expected = Convert.ToDateTime(value) > Convert.ToDateTime(filter);
                }
                catch
                {
                    expected = false;
                }
            }

            Boolean actual = TaskParameterFilterOperandType.GreaterThan.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_GreaterThanOrEqualTo([ValueSource("_inequalityValues")] Object value, [ValueSource("_inequalityValues")] Object filter)
        {
            Boolean expected;
            try
            {
                expected = Convert.ToDecimal(value) >= Convert.ToDecimal(filter);
            }
            catch
            {
                try
                {
                    expected = Convert.ToDateTime(value) >= Convert.ToDateTime(filter);
                }
                catch
                {
                    expected = false;
                }
            }

            Boolean actual = TaskParameterFilterOperandType.GreaterThanOrEqualTo.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_LessThan([ValueSource("_inequalityValues")] Object value, [ValueSource("_inequalityValues")] Object filter)
        {
            Boolean expected;
            try
            {
                expected = Convert.ToDecimal(value) < Convert.ToDecimal(filter);
            }
            catch
            {
                try
                {
                    expected = Convert.ToDateTime(value) < Convert.ToDateTime(filter);
                }
                catch
                {
                    expected = false;
                }
            }

            Boolean actual = TaskParameterFilterOperandType.LessThan.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_LessThanOrEqualTo([ValueSource("_inequalityValues")] Object value, [ValueSource("_inequalityValues")] Object filter)
        {
            Boolean expected;
            try
            {
                expected = Convert.ToDecimal(value) <= Convert.ToDecimal(filter);
            }
            catch
            {
                try
                {
                    expected = Convert.ToDateTime(value) <= Convert.ToDateTime(filter);
                }
                catch
                {
                    expected = false;
                }
            }

            Boolean actual = TaskParameterFilterOperandType.LessThanOrEqualTo.IsMatch(value, filter);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void IsMatch_ReturnsFalse_WhenMatchValueIsNull()
        {
            Assert.That(TaskParameterFilterOperandType.Equals.IsMatch(null, new Object()), Is.False);
        }

        [Test]
        public void IsMatch_ReturnsFalse_WhenMatchFilterIsNull()
        {
            Assert.That(TaskParameterFilterOperandType.Equals.IsMatch(new Object(), null), Is.False);
        }
    }
}
