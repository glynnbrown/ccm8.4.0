﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System.Linq;
using Galleria.Ccm.Engine;
using NUnit.Framework;

namespace Galleria.Ccm.UnitTests.Engine
{
    [TestFixture]
    public class TaskContainerTests
    {
        /// <summary>
        /// Tests that an assembly can be registered
        /// </summary>
        [Test]
        public void RegisterAssembly()
        {
            // register the task assembly
            TaskContainer.RegisterAssembly("Galleria.Ccm.Engine.Tasks.dll");

            // verify that some tasks were registered
            Assert.IsTrue(TaskContainer.RegisteredTaskTypes.Count() > 0);
        }
    }
}
