﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.CodeGeneration
{
    public class Program
    {

        public static void Main(String[] args)
        {
            Galleria.Framework.CodeGeneration.CodeGenerator.Generate(
                new String[] {
                    args[0],
                    "ObjectDefinitions",
                    args[1],
                    args[2],
                    args[3],
                    args[4],
                    args[5]
                });
        }
    }
}
