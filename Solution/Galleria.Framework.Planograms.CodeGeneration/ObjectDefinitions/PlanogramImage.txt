﻿Schema
None

Stored Procedures
FetchBy_PlanogramId
Insert
UpdateById
DeleteById

Properties
Planogram_Id, [INT], NOT NULL
FileName, [NVARCHAR](0), NOT NULL
Description, [NVARCHAR](0), NOT NULL
ImageData, [SQL_VARIANT], NULL