﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanPositionDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanPositionDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private T GetPlanPositionDto<T>(SqlDataReader dr, T dto) where T : PlanPositionDto
        {
            dto.Id = (Int32)GetValue(dr[FieldNames.PlanPosition_Id]);
            dto.PlanId = (Int32)GetValue(dr[FieldNames.PlanPosition_PlanId]);
            dto.PlanElementId = (Int32)GetValue(dr[FieldNames.PlanPosition_PlanElementId]);
            dto.ProductId = (Int32)GetValue(dr[FieldNames.PlanPosition_ProductId]);
            dto.OrientationId = (Int32)GetValue(dr[FieldNames.PlanPosition_OrientationId]);
            dto.XPosition = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_XPosition]));
            dto.YPosition = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_YPosition]));
            dto.Deep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_Deep]));
            dto.High = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_High]));
            dto.Wide = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_Wide]));
            dto.ProductHeight = (Double)GetValue(dr[FieldNames.PlanPosition_ProductHeight]);
            dto.ProductWidth = (Double)GetValue(dr[FieldNames.PlanPosition_ProductWidth]);
            dto.ProductDepth = (Double)GetValue(dr[FieldNames.PlanPosition_ProductDepth]);
            dto.Units = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_Units]));
            dto.SqueezeHeight = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_SqueezeHeight]));
            dto.SqueezeWidth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_SqueezeWidth]));
            dto.SqueezeDepth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_SqueezeDepth]));
            dto.BlockHeight = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_BlockHeight]));
            dto.BlockWidth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_BlockWidth]));
            dto.BlockDepth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_BlockDepth]));
            dto.NestHeight = (Single?)GetValue(dr[FieldNames.PlanPosition_NestHeight]);
            dto.NestWidth = (Single?)GetValue(dr[FieldNames.PlanPosition_NestWidth]);
            dto.NestDepth = (Single?)GetValue(dr[FieldNames.PlanPosition_NestDepth]);
            dto.LeadingDivider = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_LeadingDivider]));
            dto.LeadingGap = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_LeadingGap]));
            dto.LeadingGapVert = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_LeadingGapVert]));
            dto.CapTopDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapTopDeep]));
            dto.CapTopHigh = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapTopHigh]));
            dto.CapBotDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapBotDeep]));
            dto.CapBotHigh = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapBotHigh]));
            dto.CapLftDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapLftDeep]));
            dto.CapLftWide = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapLftWide]));
            dto.CapRgtDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRgtDeep]));
            dto.CapRgtWide = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRgtWide]));
            dto.TargetSC = (Int32?)GetValue(dr[FieldNames.PlanPosition_TargetSC]);
            dto.MaxCap = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_MaxCap]));
            dto.MaxStack = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_MaxStack]));
            dto.MinDeep = (Int32)GetValue(dr[FieldNames.PlanPosition_MinDeep]);
            dto.MaxDeep = (Int32)GetValue(dr[FieldNames.PlanPosition_MaxDeep]);
            dto.TrayHigh = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_TrayHigh]));
            dto.TrayWide = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_TrayWide]));
            dto.TrayDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_TrayDeep]));
            dto.TrayHeight = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_TrayHeight]));

            dto.TrayWidth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_TrayWidth]));
            dto.TrayDepth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_TrayDepth]));
            dto.TrayUnits = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_TrayUnits]));

            dto.TrayThickHeight = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_TrayThickHeight]));
            dto.TrayThickWidth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_TrayThickWidth]));
            dto.TrayThickDepth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_TrayThickDepth]));
            dto.TrayCountHigh = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_TrayCountHigh]));
            dto.TrayCountWide = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_TrayCountWide]));
            dto.TrayCountDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_TrayCountDeep]));
            dto.TrayProduct = (Byte)GetValue(dr[FieldNames.PlanPosition_TrayProduct]);
            dto.NestHigh = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_NestHigh]));
            dto.NestWide = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_NestWide]));
            dto.NestDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_NestDeep]));
            dto.MerchType = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_MerchType]));
            dto.Blocking = (Int32)GetValue(dr[FieldNames.PlanPosition_Blocking]);
            dto.BlockId = Convert.ToInt32(GetValue(dr[FieldNames.PlanPosition_BlockId]));
            dto.BrushStyle = (Int32)GetValue(dr[FieldNames.PlanPosition_BrushStyle]);
            dto.MinDrop = (Int32)GetValue(dr[FieldNames.PlanPosition_MinDrop]);
            dto.RemoveCount = (Int32?)GetValue(dr[FieldNames.PlanPosition_RemoveCount]);
            dto.CapRightWide = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRightWide]));
            dto.CapRightDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRightDeep]));
            dto.CapRightCapTopHigh = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRightCapTopHigh]));
            dto.CapRightCapTopDeep = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRightCapTopDeep]));
            dto.CapRightWideTrayCount = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRightWideTrayCount]));
            dto.CapRightDeepTrayCount = Convert.ToInt16(GetValue(dr[FieldNames.PlanPosition_CapRightDeepTrayCount]));
            dto.MultiSited = (Byte)GetValue(dr[FieldNames.PlanPosition_MultiSited]);
            dto.CasePack = (Int32)GetValue(dr[FieldNames.PlanPosition_CasePack]);
            dto.Colour = (Int32)GetValue(dr[FieldNames.PlanPosition_Colour]);
            dto.LastChance = (Byte)GetValue(dr[FieldNames.PlanPosition_LastChance]);
            dto.AddLocked = (Byte)GetValue(dr[FieldNames.PlanPosition_AddLocked]);
            dto.RemoveLocked = (Byte)GetValue(dr[FieldNames.PlanPosition_RemoveLocked]);
            dto.StripNumber = (Int32?)GetValue(dr[FieldNames.PlanPosition_StripNumber]);
            dto.BreakTrayUp = Convert.ToByte(GetValue(dr[FieldNames.PlanPosition_BreakTrayUp]));
            dto.BreakTrayDown = Convert.ToByte(GetValue(dr[FieldNames.PlanPosition_BreakTrayDown]));
            dto.BreakTrayBack = Convert.ToByte(GetValue(dr[FieldNames.PlanPosition_BreakTrayBack]));
            dto.BreakTrayTop = Convert.ToByte(GetValue(dr[FieldNames.PlanPosition_BreakTrayTop]));
            dto.FrontOnly = Convert.ToByte(GetValue(dr[FieldNames.PlanPosition_FrontOnly]));
            dto.MiddleCapping = Convert.ToByte(GetValue(dr[FieldNames.PlanPosition_MiddleCapping]));
            dto.MaxNestHigh = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxNestHigh]);
            dto.MaxNestDeep = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxNestDeep]);
            dto.PegDepth = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_PegDepth]));
            dto.PegX = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_PegX]));
            dto.PegY = Convert.ToSingle(GetValue(dr[FieldNames.PlanPosition_PegY]));
            dto.MaxRightCap = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxRightCap]);
            dto.Created = (DateTime)GetValue(dr[FieldNames.PlanPosition_Created]);
            dto.Modified = (DateTime)GetValue(dr[FieldNames.PlanPosition_Modified]);
            dto.PersonnelId = (Int32)GetValue(dr[FieldNames.PlanPosition_PersonnelId]);
            dto.PersonnelFlexi = (String)GetValue(dr[FieldNames.PlanPosition_PersonnelFlexi]);
            dto.CompanyId = (Int32)GetValue(dr[FieldNames.PlanPosition_CompanyId]);
            return dto;
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private ManualPlanPositionDto GetManualPositionDto(SqlDataReader dr)
        {
            ManualPlanPositionDto dto = new ManualPlanPositionDto();
            dto.Code = (String)GetValue(dr[FieldNames.PlanPosition_Code]);
            dto.PegZ = (Single?)GetValue(dr[FieldNames.PlanPosition_PegZ]);
            dto.PegStyle = (String)GetValue(dr[FieldNames.PlanPosition_PegStyle]);
            dto.IsSequenced = (Byte)GetValue(dr[FieldNames.PlanPosition_IsSequenced]);
            dto.NewProducts = Convert.ToByte(GetValue(dr[FieldNames.PlanPosition_NewProducts]));
            dto.Number1 = (Double)GetValue(dr[FieldNames.PlanPosition_Number1]);
            dto.Number2 = (Double)GetValue(dr[FieldNames.PlanPosition_Number2]);
            dto.Number3 = (Int32)GetValue(dr[FieldNames.PlanPosition_Number3]);
            dto.Number4 = (Int32)GetValue(dr[FieldNames.PlanPosition_Number4]);
            dto.Number5 = (Int32)GetValue(dr[FieldNames.PlanPosition_Number5]);
            dto.Text1 = (String)GetValue(dr[FieldNames.PlanPosition_Text1]);
            dto.Text2 = (String)GetValue(dr[FieldNames.PlanPosition_Text2]);
            dto.Text3 = (String)GetValue(dr[FieldNames.PlanPosition_Text3]);
            dto.Text4 = (String)GetValue(dr[FieldNames.PlanPosition_Text4]);
            dto.Text5 = (String)GetValue(dr[FieldNames.PlanPosition_Text5]);
            dto.IsFlexPreservedSpace = (Byte)GetValue(dr[FieldNames.PlanPosition_IsFlexPreservedSpace]);
            dto.HidePreserved = (Byte)GetValue(dr[FieldNames.PlanPosition_HidePreserved]);
            dto.AttachmentTempFile = (String)GetValue(dr[FieldNames.PlanPosition_AttachmentTempFile]);
            dto.AttachmentName = (String)GetValue(dr[FieldNames.PlanPosition_AttachmentName]);
            dto.AttachmentBaseElement = (Byte)GetValue(dr[FieldNames.PlanPosition_AttachmentBaseElement]);

            return GetPlanPositionDto<ManualPlanPositionDto>(dr, dto);
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private SpacePlanPositionDto GetSpacePositionDto(SqlDataReader dr)
        {
            SpacePlanPositionDto dto = new SpacePlanPositionDto();
            dto.StoreId = (Int32)GetValue(dr[FieldNames.PlanPosition_StoreId]);
            dto.PegFix = (Byte)GetValue(dr[FieldNames.PlanPosition_PegFix]);
            dto.PegShift = (Int32?)GetValue(dr[FieldNames.PlanPosition_PegShift]);
            dto.ElementPositionId = (Int32)GetValue(dr[FieldNames.PlanPosition_ElementPositionId]);
            dto.HistoricalDailyUnits = (Single)GetValue(dr[FieldNames.PlanPosition_HistoricalDailyUnits]);
            dto.ActualDaysSupplied = (Single)GetValue(dr[FieldNames.PlanPosition_ActualDaysSupplied]);
            dto.BlockTagName = (String)GetValue(dr[FieldNames.PlanPosition_BlockTagName]);
            dto.GlobalRank = (Int32?)GetValue(dr[FieldNames.PlanPosition_GlobalRank]);
            dto.FinalMin = (Int32?)GetValue(dr[FieldNames.PlanPosition_FinalMin]);
            dto.FinalMax = (Int32?)GetValue(dr[FieldNames.PlanPosition_FinalMax]);
            dto.MinPriority1 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MinPriority1]);
            dto.MinPriority2 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MinPriority2]);
            dto.MinPriority3 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MinPriority3]);
            dto.MinPriority4 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MinPriority4]);
            dto.MinPriority5 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MinPriority5]);
            dto.MaxPriority1 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxPriority1]);
            dto.MaxPriority2 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxPriority2]);
            dto.MaxPriority3 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxPriority3]);
            dto.MaxPriority4 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxPriority4]);
            dto.MaxPriority5 = (Int32?)GetValue(dr[FieldNames.PlanPosition_MaxPriority5]);
            dto.RecommendedInventory = (Int32?)GetValue(dr[FieldNames.PlanPosition_RecommendedInventory]);
            dto.CompromiseInventory = (Int32?)GetValue(dr[FieldNames.PlanPosition_CompromiseInventory]);
            dto.AverageWeeklySales = (Single?)GetValue(dr[FieldNames.PlanPosition_AverageWeeklySales]);
            dto.AverageWeeklyUnits = (Single?)GetValue(dr[FieldNames.PlanPosition_AverageWeeklyUnits]);
            dto.AverageWeeklyProfit = (Single?)GetValue(dr[FieldNames.PlanPosition_AverageWeeklyProfit]);
            dto.AchievedCases = (Single?)GetValue(dr[FieldNames.PlanPosition_AchievedCases]);
            dto.AchievedDOS = (Single?)GetValue(dr[FieldNames.PlanPosition_AchievedDOS]);
            dto.PerformanceValue01 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue01]);
            dto.PerformanceValue02 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue02]);
            dto.PerformanceValue03 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue03]);
            dto.PerformanceValue04 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue04]);
            dto.PerformanceValue05 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue05]);
            dto.PerformanceValue06 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue06]);
            dto.PerformanceValue07 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue07]);
            dto.PerformanceValue08 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue08]);
            dto.PerformanceValue09 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue09]);
            dto.PerformanceValue10 = (Single?)GetValue(dr[FieldNames.PlanPosition_PerformanceValue10]);
            dto.ListingPSI = (Single?)GetValue(dr[FieldNames.PlanPosition_ListingPSI]);
            dto.ExposurePSI = (Single?)GetValue(dr[FieldNames.PlanPosition_ExposurePSI]);
            dto.PegProng = (Double?)GetValue(dr[FieldNames.PlanPosition_PegProng]);
            dto.CanBeRefaced = (Boolean)GetValue(dr[FieldNames.PlanPosition_CanBeRefaced]);
            dto.AchievedInventoryLevelType = (Byte)GetValue(dr[FieldNames.PlanPosition_AchievedInventoryLevelType]);
            return GetPlanPositionDto<SpacePlanPositionDto>(dr, dto);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// 
        /// </summary>
        /// <returns>A list of dtos</returns>
        public List<PlanPositionDto> FetchByPlanId(Int32 planId)
        {
            List<PlanPositionDto> dtoList = new List<PlanPositionDto>();

            dtoList.AddRange(FetchManualByPlanId(planId));
            dtoList.AddRange(FetchMerchandisingByPlanId(planId));
            dtoList.AddRange(FetchClusterByPlanId(planId));
            dtoList.AddRange(FetchStoreByPlanId(planId));

            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan position records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanPosition dtos</returns>
        public List<ManualPlanPositionDto> FetchManualByPlanId(Int32 planId)
        {
            String script = PlanPositionScripts.FetchManualByPlanId;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[TrayWidth]", "([TrayWide] * [ProductWidth] + (2 * [TrayThickWidth]))");
                script = script.Replace("[TrayHeight]", "([TrayHigh] * [ProductHeight] + ([TrayThickHeight]))");
                script = script.Replace("[TrayDepth]", "([TrayDeep] * [ProductDepth] + (2 * [TrayThickDepth]))");
                script = script.Replace("[TrayUnits]", "([TrayWide] * [TrayHigh] * [TrayDeep])");
                script = script.Replace("[CapRightCapTopHigh]", "0");
                script = script.Replace("[CapRightCapTopDeep]", "0");
                script = script.Replace("[BreakTrayTop]", "0 AS BreakTrayTop");
            }
            List<ManualPlanPositionDto> dtoList = new List<ManualPlanPositionDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanPosition_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualPositionDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan position records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanPosition dtos</returns>
        public List<ManualPlanPositionDto> FetchMerchandisingByPlanId(Int32 planId)
        {
            String script = PlanPositionScripts.MerchandisingByPlanId;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("tpp.[TrayWidth]", "(tpp.[TrayWide] * tpp.[ProductWidth] + (2 * tpp.[TrayThickWidth]))");
                script = script.Replace("tpp.[TrayHeight]", "(tpp.[TrayHigh] * tpp.[ProductHeight] + (tpp.[TrayThickHeight]))");
                script = script.Replace("tpp.[TrayDepth]", "(tpp.[TrayDeep] * tpp.[ProductDepth] + (2 * tpp.[TrayThickDepth]))");
                script = script.Replace("tpp.[TrayUnits]", "(tpp.[TrayWide] * tpp.[TrayHigh] * tpp.[TrayDeep])");
                script = script.Replace("tpp.[CapRightCapTopHigh]", "0");
                script = script.Replace("tpp.[CapRightCapTopDeep]", "0");
                script = script.Replace("tpp.[BreakTrayTop]", "0 AS BreakTrayTop");
            }
            List<ManualPlanPositionDto> dtoList = new List<ManualPlanPositionDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanPosition_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualPositionDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of cluster plan position records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanPosition dtos</returns>
        public List<SpacePlanPositionDto> FetchClusterByPlanId(Int32 planId)
        {
            String script = PlanPositionScripts.FetchClusterByPlanId;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[TrayWidth]", "([TrayWide] * [ProductWidth] + (2 * [TrayThickWidth]))");
                script = script.Replace("[TrayHeight]", "([TrayHigh] * [ProductHeight] + ([TrayThickHeight]))");
                script = script.Replace("[TrayDepth]", "([TrayDeep] * [ProductDepth] + (2 * [TrayThickDepth]))");
                script = script.Replace("[TrayUnits]", "([TrayWide] * [TrayHigh] * [TrayDeep])");
                script = script.Replace("[CapRightCapTopHigh]", "0");
                script = script.Replace("[CapRightCapTopDeep]", "0");
                script = script.Replace("[BreakTrayTop]", "0 AS BreakTrayTop");
            }
            List<SpacePlanPositionDto> dtoList = new List<SpacePlanPositionDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanPosition_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpacePositionDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of store plan position records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanPosition dtos</returns>
        public List<SpacePlanPositionDto> FetchStoreByPlanId(Int32 planId)
        {
            String script = PlanPositionScripts.FetchStoreByPlanId;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("spp.[TrayWidth]", "(spp.[TrayWide] * spp.[ProductWidth] + (2 * spp.[TrayThickWidth]))");
                script = script.Replace("spp.[TrayHeight]", "(spp.[TrayHigh] * spp.[ProductHeight] + (spp.[TrayThickHeight]))");
                script = script.Replace("spp.[TrayDepth]", "(spp.[TrayDeep] * spp.[ProductDepth] + (2 * spp.[TrayThickDepth]))");
                script = script.Replace("spp.[TrayUnits]", "(spp.[TrayWide] * spp.[TrayHigh] * spp.[TrayDeep])");
                script = script.Replace("spp.[CapRightCapTopHigh]", "0");
                script = script.Replace("spp.[CapRightCapTopDeep]", "0");
                script = script.Replace("spp.[BreakTrayTop]", "0 AS BreakTrayTop");
            }

            List<SpacePlanPositionDto> dtoList = new List<SpacePlanPositionDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanPosition_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpacePositionDto(dr));
                    }
                }
            }
            return dtoList;
        }


        #endregion
    }
}
