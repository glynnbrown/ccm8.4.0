﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanBayDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanBayDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private T GetPlanBayDto<T>(SqlDataReader dr, T dto) where T : PlanBayDto
        {
            dto.Id = (Int32)GetValue(dr[FieldNames.PlanBay_Id]);
            dto.PlanId = (Int32)GetValue(dr[FieldNames.PlanBay_PlanId]);
            dto.Name = (String)GetValue(dr[FieldNames.PlanBay_Name]);
            dto.XPosition = (Double)GetValue(dr[FieldNames.PlanBay_XPosition]);
            dto.BaseDepth = (Double)GetValue(dr[FieldNames.PlanBay_BaseDepth]);
            dto.BaseHeight = (Double)GetValue(dr[FieldNames.PlanBay_BaseHeight]);
            dto.BaseWidth = (Double)GetValue(dr[FieldNames.PlanBay_BaseWidth]);
            dto.Height = (Double)GetValue(dr[FieldNames.PlanBay_Height]);
            dto.Width = (Double)GetValue(dr[FieldNames.PlanBay_Width]);
            dto.Depth = (Double)GetValue(dr[FieldNames.PlanBay_Depth]);
            dto.Text1 = (String)GetValue(dr[FieldNames.PlanBay_Text1]);
            dto.Text2 = (String)GetValue(dr[FieldNames.PlanBay_Text2]);
            dto.Text3 = (String)GetValue(dr[FieldNames.PlanBay_Text3]);
            dto.Text4 = (String)GetValue(dr[FieldNames.PlanBay_Text4]);
            dto.Text5 = (String)GetValue(dr[FieldNames.PlanBay_Text5]);
            dto.Number1 = (Double)GetValue(dr[FieldNames.PlanBay_Number1]);
            dto.Number2 = (Double)GetValue(dr[FieldNames.PlanBay_Number2]);
            dto.Number3 = (Int32)GetValue(dr[FieldNames.PlanBay_Number3]);
            dto.Number4 = (Int32)GetValue(dr[FieldNames.PlanBay_Number4]);
            dto.Number5 = (Int32)GetValue(dr[FieldNames.PlanBay_Number5]);
            dto.SourceSegments = (String)GetValue(dr[FieldNames.PlanBay_SourceSegments]);
            dto.DestinationSegments = (String)GetValue(dr[FieldNames.PlanBay_DestinationSegments]);
            dto.Created = (DateTime)GetValue(dr[FieldNames.PlanBay_Created]);
            dto.Modified = (DateTime)GetValue(dr[FieldNames.PlanBay_Modified]);
            dto.PersonnelId = (Int32)GetValue(dr[FieldNames.PlanBay_PersonnelId]);
            dto.PersonnelFlexi = (String)GetValue(dr[FieldNames.PlanBay_PersonnelFlexi]);
            dto.CompanyId = (Int32)GetValue(dr[FieldNames.PlanBay_CompanyId]);

            return dto;
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private ManualPlanBayDto GetManualBayDto(SqlDataReader dr)
        {
            ManualPlanBayDto dto = new ManualPlanBayDto();
            dto.Segments = (String)GetValue(dr[FieldNames.PlanBay_Segments]);
            return GetPlanBayDto<ManualPlanBayDto>(dr, dto);
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private SpacePlanBayDto GetSpaceBayDto(SqlDataReader dr)
        {
            SpacePlanBayDto dto = new SpacePlanBayDto();
            dto.StoreId = (Int32)GetValue(dr[FieldNames.PlanBay_StoreId]);
            dto.FixtureId = (Int32)GetValue(dr[FieldNames.PlanBay_FixtureId]);
            return GetPlanBayDto<SpacePlanBayDto>(dr, dto); ;
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Gets a list of All plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of PlanBay dtos</returns>
        public List<PlanBayDto> FetchByPlanId(Int32 planId)
        {
            List<PlanBayDto> dtoList = new List<PlanBayDto>();

            dtoList.AddRange(FetchManualByPlanId(planId));
            dtoList.AddRange(FetchMerchandisingByPlanId(planId));
            dtoList.AddRange(FetchClusterByPlanId(planId));
            dtoList.AddRange(FetchStoreByPlanId(planId));

            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanBay dtos</returns>
        public List<ManualPlanBayDto> FetchManualByPlanId(Int32 planId)
        {
            List<ManualPlanBayDto> dtoList = new List<ManualPlanBayDto>();
            using (DalCommand command = CreateCommand(PlanBayScripts.FetchManualByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanBay_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualBayDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanBay dtos</returns>
        public List<ManualPlanBayDto> FetchMerchandisingByPlanId(Int32 planId)
        {
            List<ManualPlanBayDto> dtoList = new List<ManualPlanBayDto>();
            using (DalCommand command = CreateCommand(PlanBayScripts.FetchMerchandisingByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanBay_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualBayDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Cluster plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanBay dtos</returns>
        public List<SpacePlanBayDto> FetchClusterByPlanId(Int32 planId)
        {
            List<SpacePlanBayDto> dtoList = new List<SpacePlanBayDto>();
            using (DalCommand command = CreateCommand(PlanBayScripts.FetchClusterByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanBay_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceBayDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of store plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanBay dtos</returns>
        public List<SpacePlanBayDto> FetchStoreByPlanId(Int32 planId)
        {
            List<SpacePlanBayDto> dtoList = new List<SpacePlanBayDto>();
            using (DalCommand command = CreateCommand(PlanBayScripts.FetchStoreByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanBay_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceBayDto(dr));
                    }
                }
            }
            return dtoList;
        }


        #endregion
    }
}
