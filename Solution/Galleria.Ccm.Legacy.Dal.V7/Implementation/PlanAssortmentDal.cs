﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;


namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanAssortmentDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanAssortmentDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private PlanAssortmentDto GetPlanAssortmentDto(SqlDataReader dr)
        {

            return new PlanAssortmentDto()
            {
                ProductId = (Int32)GetValue(dr[FieldNames.Assortment_ProductId]),
                Code = (String)GetValue(dr[FieldNames.Assortment_ProductCode]),
                FormattedCode = (String)GetValue(dr[FieldNames.Assortment_ProductFormattedCode]),
                Name = (String)GetValue(dr[FieldNames.Assortment_ProductName]),
                Recommended = Convert.ToBoolean(dr[FieldNames.Assortment_Recommended]),
                GlobalRank = Convert.ToInt32(dr[FieldNames.Assortment_GlobalRank]),
                RecomendedUnits = Convert.ToInt32(dr[FieldNames.Assortment_RecomendedUnits]),
                RecomendedFacings = Convert.ToInt32(dr[FieldNames.Assortment_RecomendedFacings]),
                Segmentation = Convert.ToString(dr[FieldNames.Assortment_Segmentation]),
                Rule1Type = Convert.ToInt32(dr[FieldNames.Assortment_Rule1Type]),
                RuleValue1 = Convert.ToDouble(dr[FieldNames.Assortment_RuleValue1]),
                Rule2Type = Convert.ToInt32(dr[FieldNames.Assortment_Rule2Type]),
                RuleValue2 = Convert.ToDouble(dr[FieldNames.Assortment_RuleValue2]),
                IsRuleDropped = Convert.ToBoolean(dr[FieldNames.Assortment_IsRuleDropped]),
                ViolationType = Convert.ToByte(dr[FieldNames.Assortment_ViolationType]),
            };
           
        }
        #endregion

        public List<PlanAssortmentDto> FetchManualByPlanId(Int32 planId)
        {
            return new List<PlanAssortmentDto>();
        }
        public List<PlanAssortmentDto> FetchMerchandisingByPlanId(Int32 planId)
        {
            return new List<PlanAssortmentDto>();
        }

        public List<PlanAssortmentDto> FetchStoreByPlanId(Int32 planId)
        {
            String script = AssortmentScripts.FetchByStorePlanId;

            List<PlanAssortmentDto> dtoList = new List<PlanAssortmentDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanPosition_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetPlanAssortmentDto(dr));
                    }
                }
            }
            return dtoList;
        }
        public List<PlanAssortmentDto> FetchClusterByPlanId(Int32 planId)
        {
            String script = AssortmentScripts.FetchByClusterPlanId;

            List<PlanAssortmentDto> dtoList = new List<PlanAssortmentDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanPosition_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetPlanAssortmentDto(dr));
                    }
                }
            }
            return dtoList;
        }
    }
}
