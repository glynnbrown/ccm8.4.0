﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Mssql;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanProcessDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanProcessDal
    {
        private const String fetchStoreIds =
                @"SELECT [StorePlanID] As PlanId
			            FROM 
				            [dbo].[tblStorePlan] INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblStorePlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]";
        private const String fetchStoreIdsByReviewIds =
            @"SELECT [StorePlanID] As PlanId
	                FROM [dbo].[tblStorePlan] INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblStorePlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]
                WHERE [dbo].[tblStorePlan].[CategoryReviewID] IN ({0})";

        private const String fetchClusterIds =
                @"SELECT [ClusterPlanID] As PlanId
			            FROM 
				            [dbo].[tblClusterPlan] INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblClusterPlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]";
        private const String fetchClusterIdsByReviewIds =
            @"SELECT [ClusterPlanID] As PlanId
	                FROM [dbo].[tblClusterPlan] INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblClusterPlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]
                WHERE [dbo].[tblClusterPlan].[CategoryReviewID] IN ({0})";

        private const String fetchManualIds =
                @"SELECT [ManualPlanID] As PlanId
			                    FROM 
				                    [dbo].[tblManualPlan]INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblManualPlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]";
        private const String fetchManualIdsByReviewIds =
            @"SELECT [ManualPlanID] As PlanId
	                FROM [dbo].[tblManualPlan] INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblManualPlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]
                WHERE [dbo].[tblManualPlan].[CategoryReviewID] IN ({0})";

        private const String fetchMerchandisingIds =
                @"SELECT [TemplatePlanID] As PlanId
			                    FROM 
				                    [dbo].[tblTemplatePlan] INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblTemplatePlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]";
        private const String fetchMerchandisingIdsByReviewIds =
            @"SELECT [TemplatePlanID] As PlanId
	                FROM [dbo].[tblTemplatePlan] INNER JOIN [dbo].[tblCategoryReview] ON
					[dbo].[tblTemplatePlan].[CategoryReviewID] = [dbo].[tblCategoryReview].[CategoryReviewID]
                WHERE [dbo].[tblTemplatePlan].[CategoryReviewID] IN ({0})";

        public List<Int32> FetchStorePlanIds(String[] categoryReviewIds)
        {
            String script = categoryReviewIds.Length > 0 ? fetchStoreIdsByReviewIds : fetchStoreIds;
            String[] paramNames;

            List<Int32> dtoList = new List<Int32>();
            using (DalCommand command = CreateCommand(UpdateScript(script, categoryReviewIds, out paramNames), CommandType.Text))
            {
                for (int i = 0; i < paramNames.Length; i++)
                {
                    command.Parameters.AddWithValue(paramNames[i], Convert.ToInt32(categoryReviewIds[i]));
                }

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add((Int32)GetValue(dr["PlanId"]));
                    }
                }
            }
            return dtoList;
        }

        public List<Int32> FetchClusterPlanIds(String[] categoryReviewIds)
        {
            String script = categoryReviewIds.Length > 0 ? fetchClusterIdsByReviewIds : fetchClusterIds;
            String[] paramNames;

            List<Int32> dtoList = new List<Int32>();
            using (DalCommand command = CreateCommand(UpdateScript(script, categoryReviewIds, out paramNames), CommandType.Text))
            {
                for (int i = 0; i < paramNames.Length; i++)
                {
                    command.Parameters.AddWithValue(paramNames[i], Convert.ToInt32(categoryReviewIds[i]));
                }

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add((Int32)GetValue(dr["PlanId"]));
                    }
                }
            }

            return dtoList;
        }

        public List<Int32> FetchManualPlanIds(String[] categoryReviewIds)
        {
            String script = categoryReviewIds.Length > 0 ? fetchManualIdsByReviewIds : fetchManualIds;
            String[] paramNames;

            List<Int32> dtoList = new List<Int32>();
            using (DalCommand command = CreateCommand(UpdateScript(script, categoryReviewIds, out paramNames), CommandType.Text))
            {
                for (int i = 0; i < paramNames.Length; i++)
                {
                    command.Parameters.AddWithValue(paramNames[i], Convert.ToInt32(categoryReviewIds[i]));
                }

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add((Int32)GetValue(dr["PlanId"]));
                    }
                }
            }

            return dtoList;
        }

        public List<Int32> FetchMerchandisingPlanIds(String[] categoryReviewIds)
        {
            String script = categoryReviewIds.Length > 0 ? fetchMerchandisingIdsByReviewIds : fetchMerchandisingIds;
            String[] paramNames;

            List<Int32> dtoList = new List<Int32>();
            using (DalCommand command = CreateCommand(UpdateScript(script, categoryReviewIds, out paramNames), CommandType.Text))
            {
                for (int i = 0; i < paramNames.Length; i++)
                {
                    command.Parameters.AddWithValue(paramNames[i], Convert.ToInt32(categoryReviewIds[i]));
                }
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add((Int32)GetValue(dr["PlanId"]));
                    }
                }
            }

            return dtoList;
        }

        private String UpdateScript(String script, String[] categoryReviewIds, out String[] paramNames)
        {
            if (categoryReviewIds.Length > 0)
            {
                paramNames = categoryReviewIds.Select(
                        (s, i) => "@id" + i.ToString()
                    ).ToArray();
                String inClause = String.Join(",", paramNames);

                return String.Format(script, inClause);
            }
            else
            {
                paramNames = new String[0];
                return script;
            }            
        }

        public Single GetDbVersion()
        {
            //Since V7 doesn't actually store an up to date version we have to take a guess
            //V7.2 doesn't have tray width so if we fail to run the command then we are probably V7.5
            try
            {
                using (DalCommand command = CreateCommand(@"SELECT [TrayWidth]
			                                        FROM  [dbo].[tblStorePlanPosition]", CommandType.Text))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            return 7.5F;
                        }
                        else
                        {
                            return 7.2F;
                        }
                    }
                }
            }
            catch
            {
                return 7.2F;
            }
        }

        private const String fixStorePlanOTEIds =
            @";WITH elementMatch AS (
			    SELECT SP.storeplanId, SPE.StorePlanElementID, TPFE.TemplatePlanElementID
			    from tblstoreplan SP
			    INNER JOIN tblStorePlanElement SPE
				    ON SP.StorePlanID = SPE.StorePlanID
			    INNER JOIN tblTemplatePlanFixPlanElement TPFE
				    ON SP.TemplatePlanID = TPFE.TemplatePlanID AND
				    SP.FixturePlanID = TPFE.FixturePlanID AND 
				    (CAST(SPE.XPosition AS REAL) >= CAST(TPFE.XPosition AS REAL) AND
				    CAST(SPE.XPosition AS REAL) < CAST(TPFE.XPosition AS REAL) + CAST(TPFE.ShelfWidth AS REAL) AND
				    CAST(SPE.YPosition AS REAL) = CAST(TPFE.YPosition AS REAL))
		    )

		    UPDATE tblStorePlanElement 
		    SET OriginalTemplateElementID = TemplatePlanElementID
		    FROM tblStorePlanElement 
		    INNER JOIN elementMatch
			    ON elementMatch.StorePlanID = tblStorePlanElement.StorePlanID
			    AND elementMatch.StorePlanElementID = tblStorePlanElement.StorePlanElementID
		    WHERE OriginalTemplateElementID is Null OR OriginalTemplateElementID = 0";

        private const String fixClusterPlanOTEIds =
            @";WITH elementMatch AS (
			    SELECT SP.ClusterplanId, SPE.ClusterPlanElementID, TPFE.TemplatePlanElementID
			    from tblClusterplan SP
			    INNER JOIN tblClusterPlanElement SPE
				    ON SP.ClusterPlanID = SPE.ClusterPlanID
			    INNER JOIN tblTemplatePlanFixPlanElement TPFE
				    ON SP.TemplatePlanID = TPFE.TemplatePlanID AND
				    SP.FixturePlanID = TPFE.FixturePlanID AND 
				    (CAST(SPE.XPosition AS REAL) >= CAST(TPFE.XPosition AS REAL) AND
				    CAST(SPE.XPosition AS REAL) < CAST(TPFE.XPosition AS REAL) + CAST(TPFE.ShelfWidth AS REAL) AND
				    CAST(SPE.YPosition AS REAL) = CAST(TPFE.YPosition AS REAL))
		    )

		    UPDATE tblClusterPlanElement 
		    SET OriginalTemplateElementID = TemplatePlanElementID
		    FROM tblClusterPlanElement 
		    INNER JOIN elementMatch
			    ON elementMatch.ClusterPlanID = tblClusterPlanElement.ClusterPlanID
			    AND elementMatch.ClusterPlanElementID = tblClusterPlanElement.ClusterPlanElementID
		    WHERE OriginalTemplateElementID is Null OR OriginalTemplateElementID = 0";

        public void FixOriginalTemplateElementIds()
        {
            using (DalCommand command = CreateCommand(fixStorePlanOTEIds, CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            using (DalCommand command = CreateCommand(fixClusterPlanOTEIds, CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }
    }
}
