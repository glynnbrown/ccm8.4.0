﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;
namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanTextBoxDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanTextBoxDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private PlanTextBoxDto GetPlanTextBoxDto(SqlDataReader dr)
        {
            PlanTextBoxDto dto = new PlanTextBoxDto();
            dto.Id = (Int32)GetValue(dr[FieldNames.PlanTextBox_Id]);
            dto.PlanId = (Int32)GetValue(dr[FieldNames.PlanTextBox_PlanId]);
            return new PlanTextBoxDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.PlanTextBox_Id]),
                PlanId = (Int32)GetValue(dr[FieldNames.PlanTextBox_PlanId]),
                PlanBayId = (Int32)GetValue(dr[FieldNames.PlanTextBox_PlanBayId]),
                PlanElementId = (Int32)GetValue(dr[FieldNames.PlanTextBox_PlanElementId]),
                XPosition = Convert.ToDouble(GetValue(dr[FieldNames.PlanTextBox_XPosition])),
                YPosition = Convert.ToDouble(GetValue(dr[FieldNames.PlanTextBox_YPosition])),
                ZPosition = Convert.ToDouble(GetValue(dr[FieldNames.PlanTextBox_ZPosition])),
                Height = Convert.ToDouble(GetValue(dr[FieldNames.PlanTextBox_Height])),
                Width = Convert.ToDouble(GetValue(dr[FieldNames.PlanTextBox_Width])),
                Depth = Convert.ToDouble(GetValue(dr[FieldNames.PlanTextBox_Depth])),
                Type = Convert.ToByte(GetValue(dr[FieldNames.PlanTextBox_TextboxType])),
                Description = (String)GetValue(dr[FieldNames.PlanTextBox_TextboxDescription]),
                FixturePlanBayNumber = (Int32)GetValue(dr[FieldNames.PlanTextBox_FixturePlanBayNumber])
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Gets a list of All plan TextBox records for the supplied plan Id
        /// </summary>
        /// <returns>A list of PlanTextBox dtos</returns>
        public List<PlanTextBoxDto> FetchByPlanId(Int32 planId)
        {
            List<PlanTextBoxDto> dtoList = new List<PlanTextBoxDto>();

            dtoList.AddRange(FetchManualByPlanId(planId));
            dtoList.AddRange(FetchMerchandisingByPlanId(planId));
            dtoList.AddRange(FetchClusterByPlanId(planId));
            dtoList.AddRange(FetchStoreByPlanId(planId));

            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan TextBox records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanTextBox dtos</returns>
        public List<PlanTextBoxDto> FetchManualByPlanId(Int32 planId)
        {
            List<PlanTextBoxDto> dtoList = new List<PlanTextBoxDto>();
            if (PlanScripts.dbVersion != 7.2F)
            {
                using (DalCommand command = CreateCommand(PlanTextBoxScripts.FetchManualByPlanId, CommandType.Text))
                {
                    CreateParameter(command, FieldNames.PlanTextBox_PlanId, SqlDbType.Int, planId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetPlanTextBoxDto(dr));
                        }
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan TextBox records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanTextBox dtos</returns>
        public List<PlanTextBoxDto> FetchMerchandisingByPlanId(Int32 planId)
        {
            List<PlanTextBoxDto> dtoList = new List<PlanTextBoxDto>();
            if (PlanScripts.dbVersion != 7.2F)
            {
                using (DalCommand command = CreateCommand(PlanTextBoxScripts.MerchandisingByPlanId, CommandType.Text))
                {
                    CreateParameter(command, FieldNames.PlanTextBox_PlanId, SqlDbType.Int, planId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetPlanTextBoxDto(dr));
                        }
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Cluster plan TextBox records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanTextBox dtos</returns>
        public List<PlanTextBoxDto> FetchClusterByPlanId(Int32 planId)
        {
            List<PlanTextBoxDto> dtoList = new List<PlanTextBoxDto>();
            if (PlanScripts.dbVersion != 7.2F)
            {
                using (DalCommand command = CreateCommand(PlanTextBoxScripts.FetchClusterByPlanId, CommandType.Text))
                {
                    CreateParameter(command, FieldNames.PlanTextBox_PlanId, SqlDbType.Int, planId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetPlanTextBoxDto(dr));
                        }
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of store plan TextBox records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanTextBox dtos</returns>
        public List<PlanTextBoxDto> FetchStoreByPlanId(Int32 planId)
        {
            List<PlanTextBoxDto> dtoList = new List<PlanTextBoxDto>();
            if (PlanScripts.dbVersion != 7.2F)
            {
                using (DalCommand command = CreateCommand(PlanTextBoxScripts.FetchStoreByPlanId, CommandType.Text))
                {
                    CreateParameter(command, FieldNames.PlanTextBox_PlanId, SqlDbType.Int, planId);

                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dtoList.Add(GetPlanTextBoxDto(dr));
                        }
                    }
                }
            }
            return dtoList;
        }


        #endregion
    }
}
