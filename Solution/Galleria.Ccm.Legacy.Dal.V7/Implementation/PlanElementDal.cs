﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion

#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 3. It was improved the way the combined direction for planogram elements are calculated. 
//   - It was ammend 2 scrips (FetchClusterByPlanId and FetchStoreByPlanId) to check the plan elements bloking type. If the blocking type was equal to 10 then it was a element to be combine.
//   - In the split plan it was added an addition step to re-calculate plan element combine direction based on the OriginalTemplateElementId.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanElementDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanElementDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private T GetPlanElementDto<T>(SqlDataReader dr, T dto) where T : PlanElementDto
        {
            dto.Id = (Int32)GetValue(dr[FieldNames.PlanElement_Id]);
            dto.PlanId = (Int32)GetValue(dr[FieldNames.PlanElement_PlanId]);
            dto.PlanBayId = (Int32)GetValue(dr[FieldNames.PlanElement_PlanBayId]);
            dto.Name = (String)GetValue(dr[FieldNames.PlanElement_Name]);
            dto.Combined = (String)GetValue(dr[FieldNames.PlanElement_Combined]);
            dto.XPosition = (Double)GetValue(dr[FieldNames.PlanElement_XPosition]);
            dto.YPosition = (Double)GetValue(dr[FieldNames.PlanElement_YPosition]);
            dto.ZPosition = (Double)GetValue(dr[FieldNames.PlanElement_ZPosition]);
            dto.ElementType = (String)GetValue(dr[FieldNames.PlanElement_ElementType]);
            dto.ShelfHeight = (Double)GetValue(dr[FieldNames.PlanElement_ShelfHeight]);
            dto.ShelfWidth = (Double)GetValue(dr[FieldNames.PlanElement_ShelfWidth]);
            dto.ShelfDepth = (Double)GetValue(dr[FieldNames.PlanElement_ShelfDepth]);
            dto.ShelfThick = (Double)GetValue(dr[FieldNames.PlanElement_ShelfThick]);
            dto.ShelfSlope = (Double)GetValue(dr[FieldNames.PlanElement_ShelfSlope]);
            dto.ShelfRiser = (Double)GetValue(dr[FieldNames.PlanElement_ShelfRiser]);
            dto.PegHeight = (Double)GetValue(dr[FieldNames.PlanElement_PegHeight]);
            dto.PegWidth = (Double)GetValue(dr[FieldNames.PlanElement_PegWidth]);
            dto.PegVertSpace = (Double)GetValue(dr[FieldNames.PlanElement_PegVertSpace]);
            dto.PegHorizSpace = (Double)GetValue(dr[FieldNames.PlanElement_PegHorzSpace]);
            dto.PegVertStart = (Double)GetValue(dr[FieldNames.PlanElement_PegVertStart]);
            dto.PegHorzStart = (Double)GetValue(dr[FieldNames.PlanElement_PegHorzStart]);
            dto.PegNotchDistance = (Double)GetValue(dr[FieldNames.PlanElement_PegNotchDistance]);
            dto.PegDepth = (Double)GetValue(dr[FieldNames.PlanElement_PegDepth]);
            dto.ChestHeight = (Double)GetValue(dr[FieldNames.PlanElement_ChestHeight]);
            dto.ChestWidth = (Double)GetValue(dr[FieldNames.PlanElement_ChestWidth]);
            dto.ChestDepth = (Double)GetValue(dr[FieldNames.PlanElement_ChestDepth]);
            dto.ChestWall = (Double)GetValue(dr[FieldNames.PlanElement_ChestWall]);
            dto.ChestInside = (Double)GetValue(dr[FieldNames.PlanElement_ChestInside]);
            dto.ChestMerchandising = (Double)GetValue(dr[FieldNames.PlanElement_ChestMerchandising]);
            dto.ChestDivider = (Double)GetValue(dr[FieldNames.PlanElement_ChestDivider]);
            dto.ChestAbove = (Double)GetValue(dr[FieldNames.PlanElement_ChestAbove]);
            dto.BarHeight = (Double)GetValue(dr[FieldNames.PlanElement_BarHeight]);
            dto.BarDepth = (Double)GetValue(dr[FieldNames.PlanElement_BarDepth]);
            dto.BarWidth = (Double)GetValue(dr[FieldNames.PlanElement_BarWidth]);
            dto.BarThick = (Double)GetValue(dr[FieldNames.PlanElement_BarThick]);
            dto.BarDistance = (Double)GetValue(dr[FieldNames.PlanElement_BarDistance]);
            dto.BarHangerDepth = (Double)GetValue(dr[FieldNames.PlanElement_BarHangerDepth]);
            dto.BarNotch = (Double)GetValue(dr[FieldNames.PlanElement_BarNotch]);
            dto.BarHorzStart = (Double)GetValue(dr[FieldNames.PlanElement_BarHorzStart]);
            dto.BarHorzSpace = (Double)GetValue(dr[FieldNames.PlanElement_BarHorzSpace]);
            dto.BarVertStart = (Double)GetValue(dr[FieldNames.PlanElement_BarVertStart]);
            dto.BarBackHeight = (Double)GetValue(dr[FieldNames.PlanElement_BarBackHeight]);
            dto.NonMerchandisable = (Byte)GetValue(dr[FieldNames.PlanElement_NonMerchandisable]);
            dto.FingerSpace = (Double)GetValue(dr[FieldNames.PlanElement_FingerSpace]);
            dto.TopOverhang = (Double)GetValue(dr[FieldNames.PlanElement_TopOverhang]);
            dto.BottomOverhang = (Double)GetValue(dr[FieldNames.PlanElement_BottomOverhang]);
            dto.LeftOverhang = (Double)GetValue(dr[FieldNames.PlanElement_LeftOverhang]);
            dto.RightOverhang = (Double)GetValue(dr[FieldNames.PlanElement_RightOverhang]);
            dto.FrontOverhang = (Double)GetValue(dr[FieldNames.PlanElement_FrontOverhang]);
            dto.BackOverhang = (Double)GetValue(dr[FieldNames.PlanElement_BackOverhang]);
            dto.Number1 = (Double)GetValue(dr[FieldNames.PlanElement_Number1]);
            dto.Number2 = (Double)GetValue(dr[FieldNames.PlanElement_Number2]);
            dto.Number3 = (Int32)GetValue(dr[FieldNames.PlanElement_Number3]);
            dto.Number4 = (Int32)GetValue(dr[FieldNames.PlanElement_Number4]);
            dto.Number5 = (Int32)GetValue(dr[FieldNames.PlanElement_Number5]);
            dto.Created = (DateTime)GetValue(dr[FieldNames.PlanElement_Created]);
            dto.Modified = (DateTime)GetValue(dr[FieldNames.PlanElement_Modified]);
            dto.PersonnelId = (Int32)GetValue(dr[FieldNames.PlanElement_PersonnelId]);
            dto.PersonnelFlexi = (String)GetValue(dr[FieldNames.PlanElement_PersonnelFlexi]);
            dto.CompanyId = (Int32)GetValue(dr[FieldNames.PlanElement_CompanyId]);
            dto.ChestWallRenderType =  Convert.ToByte(GetValue(dr[FieldNames.PlanElement_ChestWallRenderType]));
            dto.CombineDirection = Convert.ToByte(GetValue(dr[FieldNames.PlanElement_CombineDirection]));
            dto.OriginalTemplateElementId = (Int32)GetValue(dr[FieldNames.PlanElement_OriginalTemplateElementId]);

            return dto;
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private ManualPlanElementDto GetManualElementDto(SqlDataReader dr)
        {
            ManualPlanElementDto dto = new ManualPlanElementDto();
            dto.Text1 = (String)GetValue(dr[FieldNames.PlanElement_Text1]);
            dto.Text2 = (String)GetValue(dr[FieldNames.PlanElement_Text2]);
            dto.Text3 = (String)GetValue(dr[FieldNames.PlanElement_Text3]);
            dto.Text4 = (String)GetValue(dr[FieldNames.PlanElement_Text4]);
            dto.Text5 = (String)GetValue(dr[FieldNames.PlanElement_Text5]);
            return GetPlanElementDto<ManualPlanElementDto>(dr, dto);
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private SpacePlanElementDto GetSpaceElementDto(SqlDataReader dr)
        {
            SpacePlanElementDto dto = new SpacePlanElementDto();
            dto.StoreId = (Int32)GetValue(dr[FieldNames.PlanElement_StoreId]);
            dto.EmptyPegs = (Int32)GetValue(dr[FieldNames.PlanElement_EmptyPegs]);
            dto.RightEmptyPeg = (Int32)GetValue(dr[FieldNames.PlanElement_RightEmptyPeg]);
            dto.ProductCount = (Int32)GetValue(dr[FieldNames.PlanElement_ProductCount]);
            dto.PrimaryPegs = (Int32)GetValue(dr[FieldNames.PlanElement_PrimaryPegs]);
            dto.StartX = (Double)GetValue(dr[FieldNames.PlanElement_StartX]);
            dto.MaxHeight = (Double)GetValue(dr[FieldNames.PlanElement_MaxHeight]);
            return GetPlanElementDto<SpacePlanElementDto>(dr, dto);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Gets a list of all plan Element records for the supplied plan Id
        /// </summary>
        /// <returns>A list of PlanElement dtos</returns>
        public List<PlanElementDto> FetchByPlanId(Int32 planId)
        {
            List<PlanElementDto> dtoList = new List<PlanElementDto>();

            dtoList.AddRange(FetchManualByPlanId(planId));
            dtoList.AddRange(FetchMerchandisingByPlanId(planId));
            dtoList.AddRange(FetchClusterByPlanId(planId));
            dtoList.AddRange(FetchStoreByPlanId(planId));

            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan Element records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanElement dtos</returns>
        public List<ManualPlanElementDto> FetchManualByPlanId(Int32 planId)
        {
            List<ManualPlanElementDto> dtoList = new List<ManualPlanElementDto>();
            using (DalCommand command = CreateCommand(PlanElementScripts.FetchManualByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanElement_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualElementDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan Element records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanElement dtos</returns>
        public List<ManualPlanElementDto> FetchMerchandisingByPlanId(Int32 planId)
        {
            List<ManualPlanElementDto> dtoList = new List<ManualPlanElementDto>();
            using (DalCommand command = CreateCommand(PlanElementScripts.MerchandisingByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanElement_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualElementDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Cluster plan Element records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanElement dtos</returns>
        public List<SpacePlanElementDto> FetchClusterByPlanId(Int32 planId)
        {
            List<SpacePlanElementDto> dtoList = new List<SpacePlanElementDto>();
            using (DalCommand command = CreateCommand(PlanElementScripts.FetchClusterByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanElement_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceElementDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Space plan Element records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanElement dtos</returns>
        public List<SpacePlanElementDto> FetchStoreByPlanId(Int32 planId)
        {
            String script = PlanElementScripts.FetchStoreByPlanId;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("tblStorePlanElement.OriginalTemplateElementID,", "tblStorePlanElement.Number3 AS OriginalTemplateElementID,");
                script = script.Replace("tblStorePlanElement.OriginalTemplateElementID", "tblStorePlanElement.Number3");
            }
            

            List<SpacePlanElementDto> dtoList = new List<SpacePlanElementDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanElement_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceElementDto(dr));
                    }
                }
            }
            return dtoList;
        }


        #endregion
    }
}
