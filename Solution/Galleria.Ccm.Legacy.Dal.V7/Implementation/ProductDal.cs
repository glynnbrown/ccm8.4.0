﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    /// <summary>
    /// Product DAL Class
    /// </summary>
    public class ProductDal : Galleria.Framework.Dal.Mssql.DalBase, IProductDal
    {
        #region Constants

        private const String _importTableName = "#tmpProduct";

        private const String _createImportTableSql =
            "CREATE TABLE [{0}](" +
                "[Product_ProductId] [INT], " +
                "[Product_ProductLevel10Id] [INT], " +
                "[Product_Code] [VARCHAR](255), " +
                "[Product_SubCode] [VARCHAR](255), " +
                "[Product_FormattedCode] [VARCHAR](255), " +
                "[Product_Name] [VARCHAR](255), " +
                "[Product_ODSProductReference] [VARCHAR](255), " +
                "[Product_Height] [FLOAT], " +
                "[Product_Width] [FLOAT], " +
                "[Product_Depth] [FLOAT], " +
                "[Product_SqueezeHeight] [FLOAT], " +
                "[Product_SqueezeWidth] [FLOAT], " +
                "[Product_SqueezeDepth] [FLOAT], " +
                "[Product_TrayHigh] [FLOAT], " +
                "[Product_TrayWide] [FLOAT], " +
                "[Product_TrayDeep] [FLOAT], " +
                "[Product_TrayThickHeight] [FLOAT], " +
                "[Product_TrayThickWidth] [FLOAT], " +
                "[Product_TrayThickDepth] [FLOAT], " +
                "[Product_TrayHeight] [FLOAT], " +
                "[Product_TrayWidth] [FLOAT], " +
                "[Product_TrayDepth] [FLOAT], " +
                "[Product_TraypackUnits] [SMALLINT], " +
                "[Product_MaxCap] [INT], " +
                "[Product_MaxStack] [INT], " +
                "[Product_MinDeep] [INT], " +
                "[Product_MaxDeep] [INT], " +
                "[Product_CasepackUnits] [SMALLINT], " +
                "[Product_FrontOnly] [TINYINT], " +
                "[Product_TrayProduct] [TINYINT], " +
                "[Product_MerchType] [SMALLINT], " +
                "[Product_BarOverHang] [INT], " +
                "[Product_NestHeight] [FLOAT], " +
                "[Product_NestWidth] [FLOAT], " +
                "[Product_NestDepth] [FLOAT], " +
                "[Product_NestingHeight] [FLOAT], " +
                "[Product_NestingWidth] [FLOAT], " +
                "[Product_NestingDepth] [FLOAT], " +
                "[Product_LeftNestHeight] [FLOAT], " +
                "[Product_LeftNestWidth] [FLOAT], " +
                "[Product_LeftNestTopOffSet] [FLOAT], " +
                "[Product_RightNestHeight] [FLOAT], " +
                "[Product_RightNestWidth] [FLOAT], " +
                "[Product_RightNestTopOffSet] [FLOAT], " +
                "[Product_PegX] [FLOAT], " +
                "[Product_PegX2] [FLOAT], " +
                "[Product_PegY] [FLOAT], " +
                "[Product_PegY2] [FLOAT], " +
                "[Product_PegProng] [FLOAT], " +
                "[Product_PegDepth] [REAL], " +
                "[Product_BuddyAggregationType] [VARCHAR](255), " +
                "[BuddyMultiplier1_1] [FLOAT], " +
                "[BuddyMultiplier1_2] [FLOAT], " +
                "[BuddyMultiplier1_3] [FLOAT], " +
                "[Product_ProductLevel10Flexi] [VARCHAR](255), " +
                "[Product_Text1] [VARCHAR](255), " +
                "[Product_Text2] [VARCHAR](255), " +
                "[Product_Text3] [VARCHAR](255), " +
                "[Product_Text4] [VARCHAR](255), " +
                "[Product_Text5] [VARCHAR](255), " +
                "[Product_Text6] [VARCHAR](255), " +
                "[Product_Text7] [VARCHAR](255), " +
                "[Product_Text8] [VARCHAR](255), " +
                "[Product_Text9] [VARCHAR](255), " +
                "[Product_Text10] [VARCHAR](255), " +
                "[Product_Text11] [VARCHAR](255), " +
                "[Product_Text12] [VARCHAR](255), " +
                "[Product_Text13] [VARCHAR](255), " +
                "[Product_Text14] [VARCHAR](255), " +
                "[Product_Text15] [VARCHAR](255), " +
                "[Product_Text16] [VARCHAR](255), " +
                "[Product_Text17] [VARCHAR](255), " +
                "[Product_Text18] [VARCHAR](255), " +
                "[Product_Text19] [VARCHAR](255), " +
                "[Product_Text20] [VARCHAR](255), " +
                "[Product_Number1] [FLOAT], " +
                "[Product_Number2] [FLOAT], " +
                "[Product_Number3] [FLOAT], " +
                "[Product_Number4] [FLOAT], " +
                "[Product_Number5] [FLOAT], " +
                "[Product_Number6] [FLOAT], " +
                "[Product_Number7] [FLOAT], " +
                "[Product_Number8] [FLOAT], " +
                "[Product_Number9] [FLOAT], " +
                "[Product_Number10] [FLOAT], " +
                "[Product_Number11] [INT], " +
                "[Product_Number12] [INT], " +
                "[Product_Number13] [INT], " +
                "[Product_Number14] [INT], " +
                "[Product_Number15] [INT], " +
                "[Product_Number16] [INT], " +
                "[Product_Number17] [INT], " +
                "[Product_Number18] [INT], " +
                "[Product_Number19] [INT], " +
                "[Product_Number20] [INT], " +
                "[Product_BreakTrayUp] [TINYINT], " +
                "[Product_BreakTrayBack] [TINYINT], " +
                "[Product_BreaktrayDown] [TINYINT], " +
                "[Product_MaxRightCap] [INT], " +
                "[Product_MaxNestHigh] [INT], " +
                "[Product_MaxNestDeep] [INT], " +
                "[Product_Created] [SMALLDATETIME], " +
                "[Product_Modified] [SMALLDATETIME], " +
                "[Product_CanMiddleCap] [BIT], " +
                "[Product_IsNew] [BIT], " +
                "[Product_IsPlaceholder] [BIT] " +
            ")";

        private const String _dropImportTableSql = "DROP TABLE {0}";
        #endregion

        #region Nested Classes
        /// <summary>
        /// A simple data reader used to bulk insert data
        /// into the sql database
        /// </summary>
        private class ProductBulkCopyDataReader : IDataReader
        {
            #region Fields
            private IEnumerator<ProductDto> _enumerator; // the enumerator containing the values to insert
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="values">The values to insert</param>
            public ProductBulkCopyDataReader(IEnumerable<ProductDto> values)
            {
                _enumerator = values.GetEnumerator();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Returns the field count of the data reader
            /// </summary>
            public int FieldCount
            {
                get { return 106; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Advances the data reader to the next record
            /// </summary>
            /// <returns>True if there are more rows, else false</returns>
            public bool Read()
            {
                return _enumerator.MoveNext();
            }

            /// <summary>
            /// Gets the value of the specified column in its native format
            /// note that the field mappings must match the actual table layout
            /// </summary>
            /// <param name="i">The zero-based column ordinal</param>
            /// <returns>The column value</returns>
            public object GetValue(int i)
            {
                //All Single values that will end up as Doubles are converted
                //to Decimal to prevent "rounding" errors.
                object value = null;
                switch (i)
                {
                    case 0:
                        value = _enumerator.Current.Id;
                        break;
                    case 1:
                        value = _enumerator.Current.ProductLevel10Id;
                        break;
                    case 2:
                        value = _enumerator.Current.Code;
                        break;
                    case 3:
                        value = _enumerator.Current.SubCode;
                        break;
                    case 4:
                        value = _enumerator.Current.FormattedCode;
                        break;
                    case 5:
                        value = _enumerator.Current.Name;
                        break;
                    case 6:
                        value = _enumerator.Current.ODSProductReference;
                        break;
                    case 7:
                        value = (Decimal)_enumerator.Current.Height;
                        break;
                    case 8:
                        value = (Decimal)_enumerator.Current.Width;
                        break;
                    case 9:
                        value = (Decimal)_enumerator.Current.Depth;
                        break;
                    case 10:
                        value = (Decimal)_enumerator.Current.SqueezeHeight;
                        break;
                    case 11:
                        value = (Decimal)_enumerator.Current.SqueezeWidth;
                        break;
                    case 12:
                        value = (Decimal)_enumerator.Current.SqueezeDepth;
                        break;
                    case 13:
                        value = (Decimal)_enumerator.Current.TrayHigh;
                        break;
                    case 14:
                        value = (Decimal)_enumerator.Current.TrayWide;
                        break;
                    case 15:
                        value = (Decimal)_enumerator.Current.TrayDeep;
                        break;
                    case 16:
                        value = (Decimal)_enumerator.Current.TrayThickHeight;
                        break;
                    case 17:
                        value = (Decimal)_enumerator.Current.TrayThickWidth;
                        break;
                    case 18:
                        value = (Decimal)_enumerator.Current.TrayThickDepth;
                        break;
                    case 19:
                        value = (Decimal)_enumerator.Current.TrayHeight;//CCM:21084
                        break;
                    case 20:
                        value = (Decimal)_enumerator.Current.TrayWidth;//CCM:21084
                        break;
                    case 21:
                        value = (Decimal)_enumerator.Current.TrayDepth;//CCM:21084
                        break;
                    case 22:
                        value = _enumerator.Current.TraypackUnits;//CCM:21084
                        break;
                    case 23:
                        value = _enumerator.Current.MaxCap;
                        break;
                    case 24:
                        value = _enumerator.Current.MaxStack;
                        break;
                    case 25:
                        value = _enumerator.Current.MinDeep;
                        break;
                    case 26:
                        value = _enumerator.Current.MaxDeep;
                        break;
                    case 27:
                        value = _enumerator.Current.CasepackUnits;
                        break;
                    case 28:
                        value = _enumerator.Current.FrontOnly;
                        break;
                    case 29:
                        value = _enumerator.Current.TrayProduct;
                        break;
                    case 30:
                        value = _enumerator.Current.MerchType;
                        break;
                    case 31:
                        value = _enumerator.Current.BarOverHang;
                        break;
                    case 32:
                        value = (Decimal?)_enumerator.Current.NestHeight;
                        break;
                    case 33:
                        value = (Decimal?)_enumerator.Current.NestWidth;
                        break;
                    case 34:
                        value = (Decimal?)_enumerator.Current.NestDepth;
                        break;
                    case 35:
                        value = (Decimal?)_enumerator.Current.NestingHeight;
                        break;
                    case 36:
                        value = (Decimal?)_enumerator.Current.NestingWidth;
                        break;
                    case 37:
                        value = (Decimal?)_enumerator.Current.NestingDepth;
                        break;
                    case 38:
                        value = (Decimal?)_enumerator.Current.LeftNestHeight;
                        break;
                    case 39:
                        value = (Decimal?)_enumerator.Current.LeftNestWidth;
                        break;
                    case 40:
                        value = (Decimal?)_enumerator.Current.LeftNestTopOffSet;
                        break;
                    case 41:
                        value = (Decimal?)_enumerator.Current.RightNestHeight;
                        break;
                    case 42:
                        value = (Decimal?)_enumerator.Current.RightNestWidth;
                        break;
                    case 43:
                        value = (Decimal?)_enumerator.Current.RightNestTopOffSet;
                        break;
                    case 44:
                        value = (Decimal)_enumerator.Current.PegX;
                        break;
                    case 45:
                        value = (Decimal?)_enumerator.Current.PegX2;
                        break;
                    case 46:
                        value = (Decimal)_enumerator.Current.PegY;
                        break;
                    case 47:
                        value = (Decimal?)_enumerator.Current.PegY2;
                        break;
                    case 48:
                        value = (Decimal?)_enumerator.Current.PegProng;
                        break;
                    case 49:
                        value = (Decimal)_enumerator.Current.PegDepth;
                        break;
                    case 50:
                        value = _enumerator.Current.BuddyAggregationType;
                        break;
                    case 51:
                        value = (Decimal?)_enumerator.Current.BuddyMultiplier11;
                        break;
                    case 52:
                        value = (Decimal?)_enumerator.Current.BuddyMultiplier12;
                        break;
                    case 53:
                        value = (Decimal?)_enumerator.Current.BuddyMultiplier13;
                        break;
                    case 54:
                        value = _enumerator.Current.ProductLevel10Flexi;
                        break;
                    case 55:
                        value = _enumerator.Current.Text1;
                        break;
                    case 56:
                        value = _enumerator.Current.Text2;
                        break;
                    case 57:
                        value = _enumerator.Current.Text3;
                        break;
                    case 58:
                        value = _enumerator.Current.Text4;
                        break;
                    case 59:
                        value = _enumerator.Current.Text5;
                        break;
                    case 60:
                        value = _enumerator.Current.Text6;
                        break;
                    case 61:
                        value = _enumerator.Current.Text7;
                        break;
                    case 62:
                        value = _enumerator.Current.Text8;
                        break;
                    case 63:
                        value = _enumerator.Current.Text9;
                        break;
                    case 64:
                        value = _enumerator.Current.Text10;
                        break;
                    case 65:
                        value = _enumerator.Current.Text11;
                        break;
                    case 66:
                        value = _enumerator.Current.Text12;
                        break;
                    case 67:
                        value = _enumerator.Current.Text13;
                        break;
                    case 68:
                        value = _enumerator.Current.Text14;
                        break;
                    case 69:
                        value = _enumerator.Current.Text15;
                        break;
                    case 70:
                        value = _enumerator.Current.Text16;
                        break;
                    case 71:
                        value = _enumerator.Current.Text17;
                        break;
                    case 72:
                        value = _enumerator.Current.Text18;
                        break;
                    case 73:
                        value = _enumerator.Current.Text19;
                        break;
                    case 74:
                        value = _enumerator.Current.Text20;
                        break;
                    case 75:
                        value = (Decimal?)_enumerator.Current.Number1;
                        break;
                    case 76:
                        value = (Decimal?)_enumerator.Current.Number2;
                        break;
                    case 77:
                        value = (Decimal?)_enumerator.Current.Number3;
                        break;
                    case 78:
                        value = (Decimal?)_enumerator.Current.Number4;
                        break;
                    case 79:
                        value = (Decimal?)_enumerator.Current.Number5;
                        break;
                    case 80:
                        value = (Decimal?)_enumerator.Current.Number6;
                        break;
                    case 81:
                        value = (Decimal?)_enumerator.Current.Number7;
                        break;
                    case 82:
                        value = (Decimal?)_enumerator.Current.Number8;
                        break;
                    case 83:
                        value = (Decimal?)_enumerator.Current.Number9;
                        break;
                    case 84:
                        value = (Decimal?)_enumerator.Current.Number10;
                        break;
                    case 85:
                        value = (Decimal?)_enumerator.Current.Number11;
                        break;
                    case 86:
                        value = (Decimal?)_enumerator.Current.Number12;
                        break;
                    case 87:
                        value = (Decimal?)_enumerator.Current.Number13;
                        break;
                    case 88:
                        value = (Decimal?)_enumerator.Current.Number14;
                        break;
                    case 89:
                        value = (Decimal?)_enumerator.Current.Number15;
                        break;
                    case 90:
                        value = (Decimal?)_enumerator.Current.Number16;
                        break;
                    case 91:
                        value = (Decimal?)_enumerator.Current.Number17;
                        break;
                    case 92:
                        value = (Decimal?)_enumerator.Current.Number18;
                        break;
                    case 93:
                        value = (Decimal?)_enumerator.Current.Number19;
                        break;
                    case 94:
                        value = (Decimal?)_enumerator.Current.Number20;
                        break;
                    case 95:
                        value = _enumerator.Current.BreakTrayUp;
                        break;
                    case 96:
                        value = _enumerator.Current.BreakTrayBack;
                        break;
                    case 97:
                        value = _enumerator.Current.BreaktrayDown;
                        break;
                    case 98:
                        value = _enumerator.Current.MaxRightCap;
                        break;
                    case 99:
                        value = _enumerator.Current.MaxNestHigh;
                        break;
                    case 100:
                        value = _enumerator.Current.MaxNestDeep;
                        break;
                    case 101:
                        value = _enumerator.Current.Created;
                        break;
                    case 102:
                        value = _enumerator.Current.Modified;
                        break;
                    case 103:
                        value = _enumerator.Current.CanMiddleCap;
                        break;
                    case 104:
                        value = _enumerator.Current.IsNew;
                        break;
                    case 105:
                        value = _enumerator.Current.IsPlaceholder;
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (value == null)
                {
                    value = DBNull.Value;
                }
                return value;
            }
            #endregion

            #region Not Implemented
            public void Close()
            {
                throw new NotImplementedException();
            }

            public int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public bool IsClosed
            {
                get { throw new NotImplementedException(); }
            }

            public bool NextResult()
            {
                throw new NotImplementedException();
            }

            public int RecordsAffected
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public IDataReader GetData(int i)
            {
                throw new NotImplementedException();
            }

            public string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public string GetString(int i)
            {
                throw new NotImplementedException();
            }

            public int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public bool IsDBNull(int i)
            {
                throw new NotImplementedException();
            }

            public object this[string name]
            {
                get { throw new NotImplementedException(); }
            }

            public object this[int i]
            {
                get { throw new NotImplementedException(); }
            }
            #endregion
        }

        #endregion

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto from a data reader
        /// </summary>
        /// <param name="dr">The data reader to load from</param>
        /// <returns>A new dto</returns>
        public ProductDto GetDataTransferObject(SqlDataReader dr)
        {
            ProductDto output =  new ProductDto();
            
            output.Id = (Int32)GetValue(dr[FieldNames.ProductId]);
            output.ProductLevel10Id = (Int32?)GetValue(dr[FieldNames.ProductProductLevel10Id]);
            output.Code = (String)GetValue(dr[FieldNames.ProductCode]);
            output.SubCode = (String)GetValue(dr[FieldNames.ProductSubCode]);
            output.FormattedCode = (String)GetValue(dr[FieldNames.ProductFormattedCode]);
            output.Name = (String)GetValue(dr[FieldNames.ProductName]);
            output.ODSProductReference = (String)GetValue(dr[FieldNames.ProductODSProductReference]);
            output.Height = (float)GetNullableFloatValue(dr[FieldNames.ProductHeight]);
            output.Width = (float)GetNullableFloatValue(dr[FieldNames.ProductWidth]);
            output.Depth = (float)GetNullableFloatValue(dr[FieldNames.ProductDepth]);
            output.SqueezeHeight = (float)GetNullableFloatValue(dr[FieldNames.ProductSqueezeHeight]);
            output.SqueezeWidth = (float)GetNullableFloatValue(dr[FieldNames.ProductSqueezeWidth]);
            output.SqueezeDepth = (float)GetNullableFloatValue(dr[FieldNames.ProductSqueezeDepth]);
            output.TrayHigh = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayHigh]);
            output.TrayWide = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayWide]);
            output.TrayDeep = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayDeep]);
            output.TrayThickHeight = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayThickHeight]);
            output.TrayThickWidth = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayThickWidth]);
            output.TrayThickDepth = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayThickDepth]);
            output.TrayHeight = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayHeight]);//CCM:21084
            output.TrayWidth = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayWidth]);//CCM:21084
            output.TrayDepth = (float)GetNullableFloatValue(dr[FieldNames.ProductTrayDepth]);//CCM:21084
            output.TraypackUnits = Convert.ToInt16(GetValue(dr[FieldNames.ProductTraypackUnits]));//CCM:21084
            output.MaxCap = (Int32)GetValue(dr[FieldNames.ProductMaxCap]);
            output.MaxStack = (Int32)GetValue(dr[FieldNames.ProductMaxStack]);
            output.MinDeep = (Int32)GetValue(dr[FieldNames.ProductMinDeep]);
            output.MaxDeep = (Int32)GetValue(dr[FieldNames.ProductMaxDeep]);
            output.CasepackUnits =  Convert.ToInt16(GetValue(dr[FieldNames.ProductCasepackUnits]));
            output.FrontOnly = (Byte)GetValue(dr[FieldNames.ProductFrontOnly]);
            output.TrayProduct = (Byte)GetValue(dr[FieldNames.ProductTrayProduct]);
            output.MerchType = (Int16)GetValue(dr[FieldNames.ProductMerchType]);
            output.BarOverHang = (Int32?)GetValue(dr[FieldNames.ProductBarOverHang]);
            output.NestHeight = (float?)GetNullableFloatValue(dr[FieldNames.ProductNestHeight]);
            output.NestWidth = (float?)GetNullableFloatValue(dr[FieldNames.ProductNestWidth]);
            output.NestDepth = (float?)GetNullableFloatValue(dr[FieldNames.ProductNestDepth]);
            output.NestingHeight = (float?)GetNullableFloatValue(dr[FieldNames.ProductNestingHeight]);
            output.NestingWidth = (float?)GetNullableFloatValue(dr[FieldNames.ProductNestingWidth]);
            output.NestingDepth = (float?)GetNullableFloatValue(dr[FieldNames.ProductNestingDepth]);
            output.LeftNestHeight = (float?)GetNullableFloatValue(dr[FieldNames.ProductLeftNestHeight]);
            output.LeftNestWidth = (float?)GetNullableFloatValue(dr[FieldNames.ProductLeftNestWidth]);
            output.LeftNestTopOffSet = (float?)GetNullableFloatValue(dr[FieldNames.ProductLeftNestTopOffSet]);
            output.RightNestHeight = (float?)GetNullableFloatValue(dr[FieldNames.ProductRightNestHeight]);
            output.RightNestWidth = (float?)GetNullableFloatValue(dr[FieldNames.ProductRightNestWidth]);
            output.RightNestTopOffSet = (float?)GetNullableFloatValue(dr[FieldNames.ProductRightNestTopOffSet]);
            output.PegX = (float)GetNullableFloatValue(dr[FieldNames.ProductPegX]);
            output.PegX2 = (float?)GetNullableFloatValue(dr[FieldNames.ProductPegX2]);
            output.PegY = (float)GetNullableFloatValue(dr[FieldNames.ProductPegY]);
            output.PegY2 = (float?)GetNullableFloatValue(dr[FieldNames.ProductPegY2]);
            output.PegProng = (float?)GetNullableFloatValue(dr[FieldNames.ProductPegProng]);
            output.PegDepth = (float)GetNullableFloatValue(dr[FieldNames.ProductPegDepth]);
            output.BuddyAggregationType = (String)GetValue(dr[FieldNames.ProductBuddyAggregationType]);
            output.BuddyMultiplier11 = (float)GetNullableFloatValue(dr[FieldNames.ProductBuddyMultiplier11]);
            output.BuddyMultiplier12 = (float)GetNullableFloatValue(dr[FieldNames.ProductBuddyMultiplier12]);
            output.BuddyMultiplier13 = (float)GetNullableFloatValue(dr[FieldNames.ProductBuddyMultiplier13]);
            output.ProductLevel10Flexi = (String)GetValue(dr[FieldNames.ProductProductLevel10Flexi]);
            output.Text1 = (String)GetValue(dr[FieldNames.ProductText1]);
            output.Text2 = (String)GetValue(dr[FieldNames.ProductText2]);
            output.Text3 = (String)GetValue(dr[FieldNames.ProductText3]);
            output.Text4 = (String)GetValue(dr[FieldNames.ProductText4]);
            output.Text5 = (String)GetValue(dr[FieldNames.ProductText5]);
            output.Text6 = (String)GetValue(dr[FieldNames.ProductText6]);
            output.Text7 = (String)GetValue(dr[FieldNames.ProductText7]);
            output.Text8 = (String)GetValue(dr[FieldNames.ProductText8]);
            output.Text9 = (String)GetValue(dr[FieldNames.ProductText9]);
            output.Text10 = (String)GetValue(dr[FieldNames.ProductText10]);
            output.Text11 = (String)GetValue(dr[FieldNames.ProductText11]);
            output.Text12 = (String)GetValue(dr[FieldNames.ProductText12]);
            output.Text13 = (String)GetValue(dr[FieldNames.ProductText13]);
            output.Text14 = (String)GetValue(dr[FieldNames.ProductText14]);
            output.Text15 = (String)GetValue(dr[FieldNames.ProductText15]);
            output.Text16 = (String)GetValue(dr[FieldNames.ProductText16]);
            output.Text17 = (String)GetValue(dr[FieldNames.ProductText17]);
            output.Text18 = (String)GetValue(dr[FieldNames.ProductText18]);
            output.Text19 = (String)GetValue(dr[FieldNames.ProductText19]);
            output.Text20 = (String)GetValue(dr[FieldNames.ProductText20]);
            output.Number1 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber1]);
            output.Number2 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber2]);
            output.Number3 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber3]);
            output.Number4 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber4]);
            output.Number5 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber5]);
            output.Number6 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber6]);
            output.Number7 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber7]);
            output.Number8 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber8]);
            output.Number9 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber9]);
            output.Number10 = (float?)GetNullableFloatValue(dr[FieldNames.ProductNumber10]);
            output.Number11 = (Int32?)GetValue(dr[FieldNames.ProductNumber11]);
            output.Number12 = (Int32?)GetValue(dr[FieldNames.ProductNumber12]);
            output.Number13 = (Int32?)GetValue(dr[FieldNames.ProductNumber13]);
            output.Number14 = (Int32?)GetValue(dr[FieldNames.ProductNumber14]);
            output.Number15 = (Int32?)GetValue(dr[FieldNames.ProductNumber15]);
            output.Number16 = (Int32?)GetValue(dr[FieldNames.ProductNumber16]);
            output.Number17 = (Int32?)GetValue(dr[FieldNames.ProductNumber17]);
            output.Number18 = (Int32?)GetValue(dr[FieldNames.ProductNumber18]);
            output.Number19 = (Int32?)GetValue(dr[FieldNames.ProductNumber19]);
            output.Number20 = (Int32?)GetValue(dr[FieldNames.ProductNumber20]);
            output.BreakTrayUp = (Byte?)GetValue(dr[FieldNames.ProductBreakTrayUp]);
            output.BreakTrayBack = (Byte?)GetValue(dr[FieldNames.ProductBreakTrayBack]);
            output.BreaktrayDown = (Byte?)GetValue(dr[FieldNames.ProductBreaktrayDown]);
            output.MaxRightCap = (Int32)GetValue(dr[FieldNames.ProductMaxRightCap]);
            output.MaxNestHigh = (Int32)GetValue(dr[FieldNames.ProductMaxNestHigh]);
            output.MaxNestDeep = (Int32)GetValue(dr[FieldNames.ProductMaxNestDeep]);
            output.Created = (DateTime)GetValue(dr[FieldNames.ProductCreated]);
            output.Modified = (DateTime)GetValue(dr[FieldNames.ProductModified]);
            output.CanMiddleCap = (Boolean)GetValue(dr[FieldNames.ProductCanMiddleCap]);
            output.IsNew = (Boolean)GetValue(dr[FieldNames.ProductIsNew]);
            output.DeletedDate = (DateTime?)GetValue(dr[FieldNames.ProductDeletedDate]);
            output.IsPlaceholder = (Boolean)GetValue(dr[FieldNames.ProductIsPlaceholder]);
            return output;
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Returns a dto that matches the specified id
        /// </summary>
        /// <param name="id">specified id</param>
        /// <returns>matching dto</returns>
        public ProductDto FetchById(Int32 id)
        {
            ProductDto dto = null;
            try
            {
                using (DalCommand command = CreateCommand(ProductScripts.FetchById, CommandType.Text))
                {
                    //Id
                    CreateParameter(command, FieldNames.ProductId, SqlDbType.Int, id);

                    //Execute
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            dto = GetDataTransferObject(dr);
                        }
                        else
                        {
                            throw new DtoDoesNotExistException();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return dto;
        }

        #region FetchByProductCodes
        /// <summary>
        /// Fetches a list of products by a list of product codes
        /// </summary>
        /// <param name="productCodes">A list of product codes</param>
        /// <returns>A list of product dtos</returns>
        public IEnumerable<ProductDto> FetchByProductCodes(IEnumerable<string> productCodes)
        {
            String script = ProductScripts.FetchByCodes;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[tblProduct].[TrayWidth]", "([tblProduct].[TrayWide] * [tblProduct].[Width] + (2 * [tblProduct].[TrayThickWidth])) AS TrayWidth");
                script = script.Replace("[tblProduct].[TrayHeight]", "([tblProduct].[TrayHigh] * [tblProduct].[Height] + ([tblProduct].[TrayThickHeight])) AS TrayHeight");
                script = script.Replace("[tblProduct].[TrayDepth]", "([tblProduct].[TrayDeep] * [tblProduct].[Depth] + (2 * [tblProduct].[TrayThickDepth])) AS TrayDepth");
                script = script.Replace("[tblProduct].[TrayUnits]", "([tblProduct].[TrayWide] * [tblProduct].[TrayHigh] * [tblProduct].[TrayDeep]) AS TrayUnits");
                script = script.Replace("[tblProduct].[DeletedDate]", "NULL AS DeletedDate");
            }


            // create
            List<ProductDto> dtoList = new List<ProductDto>();

            // create a temp table for products
            CreateFetchByProductCodesTempTable(productCodes);

            // execute
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }

            // and drop the temporary table
            DropFetchByProductCodesTempTable();

            // and return the items
            return dtoList;
        }

        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateFetchByProductCodesTempTable(IEnumerable<string> productCodes)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpProducts (Product_Code varchar(30))", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpProducts (Product_Code) VALUES (@Product_Code)", CommandType.Text))
            {
                // product code
                SqlParameter productCodeParameter = CreateParameter(command,
                    "@Product_Code",
                    SqlDbType.NVarChar,
                    string.Empty);

                // insert the product codes
                foreach (string productCode in productCodes)
                {
                    if (productCode.Length <= 30)
                    {
                        productCodeParameter.Value = productCode;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Drops the store temporary table on the given connection
        /// </summary>
        /// <param name="connection">SQL connection to use</param>
        private void DropFetchByProductCodesTempTable()
        {
            using (DalCommand command = CreateCommand("DROP TABLE #tmpProducts", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #region FetchByProductIds
        /// <summary>
        /// Fetches a list of products by a list of product codes
        /// </summary>
        /// <param name="productIds">A list of product codes</param>
        /// <returns>A list of product dtos</returns>
        public List<ProductDto> FetchByProductIds(IEnumerable<Int32> productIds)
        {
            String script = ProductScripts.FetchByIds;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[tblProduct].[TrayWidth]", "([tblProduct].[TrayWide] * [tblProduct].[Width] + (2 * [tblProduct].[TrayThickWidth])) AS TrayWidth");
                script = script.Replace("[tblProduct].[TrayHeight]", "([tblProduct].[TrayHigh] * [tblProduct].[Height] + ([tblProduct].[TrayThickHeight])) AS TrayHeight");
                script = script.Replace("[tblProduct].[TrayDepth]", "([tblProduct].[TrayDeep] * [tblProduct].[Depth] + (2 * [tblProduct].[TrayThickDepth])) AS TrayDepth");
                script = script.Replace("[tblProduct].[TrayUnits]", "([tblProduct].[TrayWide] * [tblProduct].[TrayHigh] * [tblProduct].[TrayDeep]) AS TrayUnits");
                script = script.Replace("[tblProduct].[DeletedDate]", "NULL AS DeletedDate");
            }

            // create
            List<ProductDto> dtoList = new List<ProductDto>();

            // create a temp table for products
            CreateFetchByProductIdsTempTable(productIds);

            // execute
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetDataTransferObject(dr));
                    }
                }
            }

            // and drop the temporary table
            DropFetchByProductIdsTempTable();

            // and return the items
            return dtoList;
        }

        /// <summary>
        /// Creates the store temporary table on the given connection
        /// </summary>
        private void CreateFetchByProductIdsTempTable(IEnumerable<Int32> productIds)
        {
            // create the temp table
            using (DalCommand command = CreateCommand("CREATE TABLE #tmpProducts (Product_Id INT)", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }

            // populate the temp table
            using (DalCommand command = CreateCommand("INSERT INTO #tmpProducts (Product_Id) VALUES (@Product_Id)", CommandType.Text))
            {
                // product gtin
                SqlParameter productIdParameter = CreateParameter(command,
                    "@Product_Id",
                    SqlDbType.Int,
                    string.Empty);

                // insert the product gtins
                foreach (Int32 productId in productIds)
                {
                    productIdParameter.Value = productId;
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Drops the store temporary table on the given connection
        /// </summary>
        /// <param name="connection">SQL connection to use</param>
        private void DropFetchByProductIdsTempTable()
        {
            using (DalCommand command = CreateCommand("DROP TABLE #tmpProducts", CommandType.Text))
            {
                command.ExecuteNonQuery();
            }
        }
        #endregion

        #endregion
    }
}
