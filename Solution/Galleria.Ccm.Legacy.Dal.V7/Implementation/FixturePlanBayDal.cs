﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class FixturePlanBayDal : Galleria.Framework.Dal.Mssql.DalBase, IFixturePlanBayDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private FixturePlanBayDto GetPlanBayDto(SqlDataReader dr) 
        {

            return new FixturePlanBayDto()
            {
            Id = (Int32)GetValue(dr[FieldNames.FixturePlanBay_Id]),
            FixturePlanId = (Int32)GetValue(dr[FieldNames.FixturePlanBay_FixturePlanId]),
            BayNumber = (Int32)GetValue(dr[FieldNames.FixturePlanBay_BayNumber]),
            Name = (String)GetValue(dr[FieldNames.FixturePlanBay_Name]),
            XPosition = (Double)GetValue(dr[FieldNames.FixturePlanBay_XPosition]),
            BaseDepth = (Double)GetValue(dr[FieldNames.FixturePlanBay_BaseDepth]),
            BaseHeight = (Double)GetValue(dr[FieldNames.FixturePlanBay_BaseHeight]),
            BaseWidth = (Double)GetValue(dr[FieldNames.FixturePlanBay_BaseWidth]),
            Height = (Double)GetValue(dr[FieldNames.FixturePlanBay_Height]),
            Width = (Double)GetValue(dr[FieldNames.FixturePlanBay_Width]),
            Depth = (Double)GetValue(dr[FieldNames.FixturePlanBay_Depth]),
            Text1 = (String)GetValue(dr[FieldNames.FixturePlanBay_Text1]),
            Text2 = (String)GetValue(dr[FieldNames.FixturePlanBay_Text2]),
            Text3 = (String)GetValue(dr[FieldNames.FixturePlanBay_Text3]),
            Text4 = (String)GetValue(dr[FieldNames.FixturePlanBay_Text4]),
            Text5 = (String)GetValue(dr[FieldNames.FixturePlanBay_Text5]),
            Number1 = (Double?)GetValue(dr[FieldNames.FixturePlanBay_Number1]),
            Number2 = (Double?)GetValue(dr[FieldNames.FixturePlanBay_Number2]),
            Number3 = (Int32?)GetValue(dr[FieldNames.FixturePlanBay_Number3]),
            Number4 = (Int32?)GetValue(dr[FieldNames.FixturePlanBay_Number4]),
            Number5 = (Int32?)GetValue(dr[FieldNames.FixturePlanBay_Number5]),
            Created = (DateTime)GetValue(dr[FieldNames.FixturePlanBay_Created]),
            Modified = (DateTime)GetValue(dr[FieldNames.FixturePlanBay_Modified]),
            PersonnelId = (Int32)GetValue(dr[FieldNames.FixturePlanBay_PersonnelId]),
            PersonnelFlexi = (String)GetValue(dr[FieldNames.FixturePlanBay_PersonnelFlexi]),
            CompanyId = (Int32)GetValue(dr[FieldNames.FixturePlanBay_CompanyId])
        };
        }

       
        #endregion

        #region Fetch

        /// <summary>
        /// Gets a list of Merchandising plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanBay dtos</returns>
        public List<FixturePlanBayDto> FetchByFixturePlanId(Int32 fixturePlanId)
        {
            String script = FixturePlanBayScripts.FetchByFixturePlanId;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[BayNumber]", "0");
            }
            List<FixturePlanBayDto> dtoList = new List<FixturePlanBayDto>();
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.FixturePlanBay_FixturePlanId, SqlDbType.Int, fixturePlanId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetPlanBayDto(dr));
                    }
                }
            }
            return dtoList;
        }

        #endregion
    }
}
