﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class FixturePlanDal : Galleria.Framework.Dal.Mssql.DalBase, IFixturePlanDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private FixturePlanDto GetPlanDto(SqlDataReader dr)
        {
            return new FixturePlanDto()
            {
                Id = (Int32)GetValue(dr[FieldNames.FixturePlan_Id]),
                Name = (String)GetValue(dr[FieldNames.FixturePlan_Name]),
                ProductLevelId = (Int32)GetValue(dr[FieldNames.FixturePlan_ProductLevelId]),
                SourceFile = (String)GetValue(dr[FieldNames.FixturePlan_SourceFile]),
                Blocking = (String)GetValue(dr[FieldNames.FixturePlan_Blocking]),
                MerchShelfSpace = (Double?)GetValue(dr[FieldNames.FixturePlan_MerchShelfSpace]),
                MerchPegSpace = (Double?)GetValue(dr[FieldNames.FixturePlan_MerchPegSpace]),
                MerchBarSpace = (Double?)GetValue(dr[FieldNames.FixturePlan_MerchBarSpace]),
                MerchChestSpace = (Double?)GetValue(dr[FieldNames.FixturePlan_MerchChestSpace]),
                Number1 = (Double?)GetValue(dr[FieldNames.FixturePlan_Number1]),
                Number2 = (Double?)GetValue(dr[FieldNames.FixturePlan_Number2]),
                Number3 = (Int32?)GetValue(dr[FieldNames.FixturePlan_Number3]),
                Number4 = (Int32?)GetValue(dr[FieldNames.FixturePlan_Number4]),
                Number5 = (Int32?)GetValue(dr[FieldNames.FixturePlan_Number5]),
                Text1 = (String)GetValue(dr[FieldNames.FixturePlan_Text1]),
                Text2 = (String)GetValue(dr[FieldNames.FixturePlan_Text2]),
                Text3 = (String)GetValue(dr[FieldNames.FixturePlan_Text3]),
                Text4 = (String)GetValue(dr[FieldNames.FixturePlan_Text4]),
                Text5 = (String)GetValue(dr[FieldNames.FixturePlan_Text5]),
                ProductLvelFlexi = (String)GetValue(dr[FieldNames.FixturePlan_ProductLvelFlexi]),
                BayWidthValues = (String)GetValue(dr[FieldNames.FixturePlan_BayWidthValues]),
                PresentationTotalWidth = (String)GetValue(dr[FieldNames.FixturePlan_PresentationTotalWidth]),
                PresentationBayWidth = (String)GetValue(dr[FieldNames.FixturePlan_PresentationBayWidth]),
                MaxHeight = (Single?)GetValue(dr[FieldNames.FixturePlan_MaxHeight]),
                PresentationMaxHeight = (String)GetValue(dr[FieldNames.FixturePlan_PresentationMaxHeight]),
                CustomCalculationValue = (Double?)GetValue(dr[FieldNames.FixturePlan_CustomCalculationValue]),
                CustomPostFix = (String)GetValue(dr[FieldNames.FixturePlan_CustomPostFix]),
                TotalWidth = (Single?)GetValue(dr[FieldNames.FixturePlan_TotalWidth]),
                BayCount = (Int16)GetValue(dr[FieldNames.FixturePlan_BayCount]),                
                Created = (DateTime)GetValue(dr[FieldNames.FixturePlan_Created]),
                Modified = (DateTime)GetValue(dr[FieldNames.FixturePlan_Modified]),
                PersonnelId = (Int32)GetValue(dr[FieldNames.FixturePlan_PersonnelId]),
                PersonnelFlexi = (String)GetValue(dr[FieldNames.FixturePlan_PersonnelFlexi]),
                CompanyId = (Int32)GetValue(dr[FieldNames.FixturePlan_CompanyId])
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Gets a list of all fixture plan records
        /// </summary>
        /// <returns>A list of fixture dtos</returns>
        public List<FixturePlanDto> FetchAll()
        {
            List<FixturePlanDto> dtoList = new List<FixturePlanDto>();
            using (DalCommand command = CreateCommand(FixturePlanScripts.FetchAll, CommandType.Text))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetPlanDto(dr));
                    }
                }
            }
            return dtoList;
        }


        /// <summary>
        /// Gets a FixturePlan record
        /// </summary>
        /// <returns>A list of FixturePlan dtos</returns>
        public FixturePlanDto FetchById(Int32 fixturePlanId)
        {
            using (DalCommand command = CreateCommand(FixturePlanScripts.FetchById, CommandType.Text))
            {
                CreateParameter(command, FieldNames.FixturePlan_Id, SqlDbType.Int, fixturePlanId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        return GetPlanDto(dr);
                    }
                }
            }
            return null;
        }

      

        #endregion
    }
}
