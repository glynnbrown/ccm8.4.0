﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanDal
    {

        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private T GetPlanDto<T>(SqlDataReader dr, T dto) where T : PlanDto
        {
            dto.CategoryReviewName = (String)GetValue(dr[FieldNames.Plan_CategoryReviewName]);
            dto.Id = (Int32)GetValue(dr[FieldNames.Plan_Id]);
            dto.UniqueContentReference = (Guid)GetValue(dr[FieldNames.Plan_UniqueContentReference]);
            dto.PlanType = (Byte)((Int32)GetValue(dr[FieldNames.Plan_Type]));
            dto.Name = (String)GetValue(dr[FieldNames.Plan_Name]);
            dto.UniqueItemName = (String)GetValue(dr[FieldNames.Plan_UniqueItemName]);
            dto.StoreId = (Int32)GetValue(dr[FieldNames.Plan_StoreId]);
            dto.StoreFlexi = (String)GetValue(dr[FieldNames.Plan_StoreFlexi]);
            dto.ProductLevelId = (Int32)GetValue(dr[FieldNames.Plan_ProductLevelId]);
            dto.MerchandisingGroupFlexi = (String)GetValue(dr[FieldNames.Plan_MerchandisingGroupFlexi]);
            dto.CreatedDate = (DateTime?)GetValue(dr[FieldNames.Plan_CreatedDate]);
            dto.IssuedDate = (DateTime?)GetValue(dr[FieldNames.Plan_IssuedDate]);
            dto.ValidDate = (DateTime?)GetValue(dr[FieldNames.Plan_ValidDate]);
            dto.RangingDate = (DateTime?)GetValue(dr[FieldNames.Plan_RangingDate]);
            dto.PublishingDate = (DateTime?)GetValue(dr[FieldNames.Plan_PublishingDate]);
            dto.Deleted = (Byte)GetValue(dr[FieldNames.Plan_Deleted]);
            dto.Issued = (Byte)GetValue(dr[FieldNames.Plan_Issued]);
            dto.Failed = (Byte)GetValue(dr[FieldNames.Plan_Failed]);
            dto.MerchShelfSpace = (Double)GetValue(dr[FieldNames.Plan_MerchShelfSpace]);
            dto.MerchPegSpace = (Double)GetValue(dr[FieldNames.Plan_MerchPegSpace]);
            dto.MerchBarSpace = (Double)GetValue(dr[FieldNames.Plan_MerchBarSpace]);
            dto.MerchChestSpace = (Double)GetValue(dr[FieldNames.Plan_MerchChestSpace]);
            dto.Number1 = (Double)GetValue(dr[FieldNames.Plan_Number1]);
            dto.Number2 = (Double)GetValue(dr[FieldNames.Plan_Number2]);
            dto.Number3 = (Int32)GetValue(dr[FieldNames.Plan_Number3]);
            dto.Number4 = (Int32)GetValue(dr[FieldNames.Plan_Number4]);
            dto.Number5 = (Int32)GetValue(dr[FieldNames.Plan_Number5]);
            dto.Text1 = (String)GetValue(dr[FieldNames.Plan_Text1]);
            dto.Text2 = (String)GetValue(dr[FieldNames.Plan_Text2]);
            dto.Text3 = (String)GetValue(dr[FieldNames.Plan_Text3]);
            dto.Text4 = (String)GetValue(dr[FieldNames.Plan_Text4]);
            dto.Text5 = (String)GetValue(dr[FieldNames.Plan_Text5]);
            dto.Text6 = (String)GetValue(dr[FieldNames.Plan_Text6]);
            dto.Text7 = (String)GetValue(dr[FieldNames.Plan_Text7]);
            dto.Text8 = (String)GetValue(dr[FieldNames.Plan_Text8]);
            dto.Text9 = (String)GetValue(dr[FieldNames.Plan_Text9]);
            dto.Text10 = (String)GetValue(dr[FieldNames.Plan_Text10]);
            dto.ElementPresentation = (Byte?)GetValue(dr[FieldNames.Plan_ElementPresentation]);
            dto.NestedMerchHeight = (Byte?)GetValue(dr[FieldNames.Plan_NestedMerchHeight]);
            dto.FingerSpace = (Double?)GetValue(dr[FieldNames.Plan_FingerSpace]);
            dto.BreakMerchHeight = (Byte?)GetValue(dr[FieldNames.Plan_BreakMerchHeight]);
            dto.BreakMerchDepth = (Byte?)GetValue(dr[FieldNames.Plan_BreakMerchDepth]);
            dto.CategoryReviewId = (Int32)GetValue(dr[FieldNames.Plan_CategoryReviewId]);
            dto.ParentCategoryReviewId = (Int32?)GetValue(dr[FieldNames.Plan_ParentCategoryReviewId]);
            dto.Created = (DateTime)GetValue(dr[FieldNames.Plan_Created]);
            dto.Modified = (DateTime)GetValue(dr[FieldNames.Plan_Modified]);
            dto.PersonnelId = (Int32)GetValue(dr[FieldNames.Plan_PersonnelId]);
            dto.PersonnelFlexi = (String)GetValue(dr[FieldNames.Plan_PersonnelFlexi]);
            dto.CompanyId = (Int32)GetValue(dr[FieldNames.Plan_CompanyId]);

            return dto;
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private ManualPlanDto GetManualDto(SqlDataReader dr)
        {
            ManualPlanDto dto = new ManualPlanDto();

            dto.MerchandisingGroupId = (Int32?)GetValue(dr[FieldNames.Plan_MerchandisingGroupId]);
            dto.SourceFile = (String)GetValue(dr[FieldNames.Plan_SourceFile]);
            dto.PlanDescription1 = (String)GetValue(dr[FieldNames.Plan_PlanDescription1]);
            dto.PlanDescription2 = (String)GetValue(dr[FieldNames.Plan_PlanDescription2]);
            dto.PlanDescription3 = (String)GetValue(dr[FieldNames.Plan_PlanDescription3]);
            dto.PlanDescription4 = (String)GetValue(dr[FieldNames.Plan_PlanDescription4]);
            dto.PlanDescription5 = (String)GetValue(dr[FieldNames.Plan_PlanDescription5]);
            return GetPlanDto<ManualPlanDto>(dr, dto);
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private SpacePlanDto GetSpaceDto(SqlDataReader dr)
        {
            SpacePlanDto dto = new SpacePlanDto();

            dto.ClusterLevelId = (Int32)GetValue(dr[FieldNames.Plan_ClusterLevelId]);
            dto.TemplatePlanId = (Int32)GetValue(dr[FieldNames.Plan_TemplatePlanId]);
            dto.ClusterAssortmentId = (Int32)GetValue(dr[FieldNames.Plan_ClusterAssortmentId]);
            dto.FixturePlanId = (Int32)GetValue(dr[FieldNames.Plan_FixturePlanId]);
            dto.ScenarioId = (Int32)GetValue(dr[FieldNames.Plan_ScenarioId]);
            dto.ReviewDate = (DateTime)GetValue(dr[FieldNames.Plan_ReviewDate]);
            dto.Islive = (Byte)GetValue(dr[FieldNames.Plan_Islive]);
            dto.HasEngineReport = (Byte)GetValue(dr[FieldNames.Plan_HasEngineReport]);
            dto.RenderType = (Byte?)GetValue(dr[FieldNames.Plan_RenderType]);
            dto.OverrideBlockSpace = (Byte)GetValue(dr[FieldNames.Plan_OverrideBlockSpace]);
            dto.WorkpackageName = (String)GetValue(dr[FieldNames.Plan_WorkpackageName]);
            dto.AssortmentTriangleSetting = (String)GetValue(dr[FieldNames.Plan_AssortmentTriangleSetting]);
            dto.FlexPreservationAllowed = (Byte)GetValue(dr[FieldNames.Plan_FlexPreservationAllowed]);
            dto.PercentCompromise = (Int32)GetValue(dr[FieldNames.Plan_PercentCompromise]);
            dto.PercentDelistedProducts = (Int32)GetValue(dr[FieldNames.Plan_PercentDelistedProducts]);
            dto.PercentForceClusterFacingLevels = (Int32)GetValue(dr[FieldNames.Plan_PercentForceClusterFacingLevels]);
            dto.PercentClusterParticipationListing = (Int32)GetValue(dr[FieldNames.Plan_PercentClusterParticipationListing]);
            dto.ProductsPlacedCount = (Int32)GetValue(dr[FieldNames.Plan_ProductsPlacedCount]);
            dto.ProductsUnplacedCount = (Int32)GetValue(dr[FieldNames.Plan_ProductsUnplacedCount]);
            dto.ProductsNotRecommendedCount = (Int32)GetValue(dr[FieldNames.Plan_ProductsNotRecommendedCount]);
            dto.AuditValue1 = (Single)GetValue(dr[FieldNames.Plan_AuditValue1]);
            dto.AuditValue2 = (Single)GetValue(dr[FieldNames.Plan_AuditValue2]);
            dto.AuditValue3 = (Single)GetValue(dr[FieldNames.Plan_AuditValue3]);
            dto.AuditValue4 = (Single)GetValue(dr[FieldNames.Plan_AuditValue4]);
            dto.AuditValue5 = (Single)GetValue(dr[FieldNames.Plan_AuditValue5]);
            dto.AuditValue6 = (Single)GetValue(dr[FieldNames.Plan_AuditValue6]);
            dto.AuditValue7 = (Single)GetValue(dr[FieldNames.Plan_AuditValue7]);
            dto.AuditValue8 = (Single)GetValue(dr[FieldNames.Plan_AuditValue8]);
            dto.AuditValue9 = (Single)GetValue(dr[FieldNames.Plan_AuditValue9]);
            dto.AuditValue10 = (Single)GetValue(dr[FieldNames.Plan_AuditValue10]);
            dto.AuditValue11 = (Single)GetValue(dr[FieldNames.Plan_AuditValue11]);
            dto.AuditValue12 = (Single)GetValue(dr[FieldNames.Plan_AuditValue12]);
            dto.AuditValue13 = (Single)GetValue(dr[FieldNames.Plan_AuditValue13]);
            dto.AuditValue14 = (Single)GetValue(dr[FieldNames.Plan_AuditValue14]);
            dto.AuditValue15 = (Single)GetValue(dr[FieldNames.Plan_AuditValue15]);
            dto.ActionCount = (Int16)GetValue(dr[FieldNames.Plan_ActionCount]);
            dto.ActionsAppliedCount = (Int16)GetValue(dr[FieldNames.Plan_ActionsAppliedCount]);
            dto.ProductsAddedCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsAddedCount]);
            dto.ProductsRemovedCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsRemovedCount]);
            dto.ProductsMovedCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsMovedCount]);
            dto.ProductsReplacedCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsReplacedCount]);
            dto.ProductsIncreasedSpaceCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsIncreasedSpaceCount]);
            dto.ProductsDecreasedSpaceCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsDecreasedSpaceCount]);
            dto.AutomaticIncreasedSpaceCount = (Int16)GetValue(dr[FieldNames.Plan_AutomaticIncreasedSpaceCount]);
            dto.AutomaticDecreasedSpaceCount = (Int16)GetValue(dr[FieldNames.Plan_AutomaticDecreasedSpaceCount]);
            dto.SupplyabilityRemovedCount = (Int16)GetValue(dr[FieldNames.Plan_SupplyabilityRemovedCount]);
            dto.PercentageOfActionsNotAchieved = (Single)GetValue(dr[FieldNames.Plan_PercentageOfActionsNotAchieved]);
            dto.PercentageOfChangeOnPlan = (Single)GetValue(dr[FieldNames.Plan_PercentageOfChangeOnPlan]);
            dto.PercentageOfChangeFromOriginal = (Single)GetValue(dr[FieldNames.Plan_PercentageOfChangeFromOriginal]);
            dto.ProfileId = (Int32)GetValue(dr[FieldNames.Plan_ProfileId]);
            dto.ProfileName = (String)GetValue(dr[FieldNames.Plan_ProfileName]);
            dto.PlaceholderProductCount = (Int16)GetValue(dr[FieldNames.Plan_PlaceholderProductCount]);
            dto.ReplacementProductCount = (Int16)GetValue(dr[FieldNames.Plan_ReplacementProductCount]);
            dto.PlaceholderProductPlacedCount = (Int16)GetValue(dr[FieldNames.Plan_PlaceholderProductPlacedCount]);
            dto.ReplacementProductPlacedCount = (Int16)GetValue(dr[FieldNames.Plan_ReplacementProductPlacedCount]);
            dto.ProductCountChangedOnPlan = (Int16)GetValue(dr[FieldNames.Plan_ProductCountChangeOnPlan]);
            dto.PercentageDeviationFromBlocking = (Single)GetValue(dr[FieldNames.Plan_PercentageDeviationFromBlocking]);
            dto.PercentageWhiteSpace = (Single)GetValue(dr[FieldNames.Plan_PercentageWhiteSpace]);
            dto.BlocksDroppedCount = (Int16)GetValue(dr[FieldNames.Plan_BlocksDroppedCount]);
            dto.ProductsNotAchievedInventoryCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsNotAchievedInventoryCount]);
            dto.ProductsMovedFromTemplateCount = (Int16)GetValue(dr[FieldNames.Plan_ProductsMovedFromTemplateCount]);
            dto.AttributesChangedCount = (Int16)GetValue(dr[FieldNames.Plan_AttributesChangedCount]);
            dto.AttributesRemovedCount = (Int16)GetValue(dr[FieldNames.Plan_AttributesRemovedCount]);

            return GetPlanDto<SpacePlanDto>(dr, dto); ;
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private SpacePlanDto GetStorePlanDto(SqlDataReader dr)
        {
            SpacePlanDto dto = GetSpaceDto(dr);
            dto.StoreCode = (String)GetValue(dr[FieldNames.Plan_StoreCode]);
            return dto;
        }
        #endregion

        #region Fetch

        #region ALL
        /// <summary>
        /// Gets a list of all plan records
        /// </summary>
        /// <returns>A list of Plan dtos</returns>
        public List<PlanDto> FetchAll()
        {
            List<PlanDto> dtoList = new List<PlanDto>();

            dtoList.AddRange(FetchAllManual());
            dtoList.AddRange(FetchAllMerchandising());
            dtoList.AddRange(FetchAllCluster());
            dtoList.AddRange(FetchAllStore());

            return dtoList;
        }

        /// <summary>
        /// Gets a list of all Merchandising plan records
        /// </summary>
        /// <returns>A list of ManualPlan dtos</returns>
        public List<ManualPlanDto> FetchAllManual()
        {
            List<ManualPlanDto> dtoList = new List<ManualPlanDto>();
            using (DalCommand command = CreateCommand(PlanScripts.FetchAllManual, CommandType.Text))
            {                
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of all Merchandising plan records
        /// </summary>
        /// <returns>A list of ManualPlan dtos</returns>
        public List<ManualPlanDto> FetchAllMerchandising()
        {
            List<ManualPlanDto> dtoList = new List<ManualPlanDto>();
            using (DalCommand command = CreateCommand(PlanScripts.FetchAllMerchandising, CommandType.Text))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetManualDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of all Store plan records
        /// </summary>
        /// <returns>A list of SpacePlan dtos</returns>
        public List<SpacePlanDto> FetchAllStore()
        {
            List<SpacePlanDto> dtoList = new List<SpacePlanDto>();
            using (DalCommand command = CreateCommand(PlanScripts.FetchAllStore, CommandType.Text))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of all Cluste plan records
        /// </summary>
        /// <returns>A list of SpacePlan dtos</returns>
        public List<SpacePlanDto> FetchAllCluster()
        {
            List<SpacePlanDto> dtoList = new List<SpacePlanDto>();
            using (DalCommand command = CreateCommand(PlanScripts.FetchAllCluster, CommandType.Text))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceDto(dr));
                    }
                }
            }
            return dtoList;
        }
        #endregion

        #region By Plan Id

        /// <summary>
        /// Gets a list of all Merchandising plan records
        /// </summary>
        /// <returns>A list of ManualPlan dtos</returns>
        public ManualPlanDto FetchManualByPlanId(Int32 PlanId)
        {
            String script = PlanScripts.FetchManualById;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[dbo].[tblManualPlan].[Content_UniqueContentReference]", "NEWID()");
                script = script.Replace("[dbo].[tblManualPlan].[UniquePlanName]", "''");
            }
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.Plan_Id, SqlDbType.Int, PlanId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        return GetManualDto(dr);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets a list of all Merchandising plan records
        /// </summary>
        /// <returns>A list of ManualPlan dtos</returns>
        public ManualPlanDto FetchMerchandisingByPlanId(Int32 PlanId)
        {
            String script = PlanScripts.FetchMerchandisingById;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[dbo].[tblTemplatePlan].[Content_UniqueContentReference]", "NEWID()");
                script = script.Replace("[dbo].[tblTemplatePlan].[UniquePlanName]", "''");
            }
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.Plan_Id, SqlDbType.Int, PlanId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        return GetManualDto(dr);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets a list of all Store plan records
        /// </summary>
        /// <returns>A list of SpacePlan dtos</returns>
        public SpacePlanDto FetchStoreByPlanId(Int32 PlanId)
        {
            String script = PlanScripts.FetchStoreById;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[dbo].[tblStorePlan].[Content_UniqueContentReference]", "NEWID()");
                script = script.Replace("[dbo].[tblStorePlan].[UniquePlanName]", "''");
            }
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.Plan_Id, SqlDbType.Int, PlanId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        return GetStorePlanDto(dr);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets a list of all Cluste plan records
        /// </summary>
        /// <returns>A list of SpacePlan dtos</returns>
        public SpacePlanDto FetchClusterByPlanId(Int32 PlanId)
        {
            String script = PlanScripts.FetchClusterById;
            if (PlanScripts.dbVersion == 7.2F)
            {
                script = script.Replace("[dbo].[tblClusterPlan].[Content_UniqueContentReference]", "NEWID()");
                script = script.Replace("[dbo].[tblClusterPlan].[UniquePlanName]", "''");
            }
            using (DalCommand command = CreateCommand(script, CommandType.Text))
            {
                CreateParameter(command, FieldNames.Plan_Id, SqlDbType.Int, PlanId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        return GetSpaceDto(dr);
                    }
                }
            }
            return null;
        }

        #endregion

        #endregion

        #region Update
        /// <summary>
        /// Updates the specified dto in the database
        /// </summary>
        /// <param name="dto">The dto to update</param>
        public void Update(PlanDto dto)
        {
            try
            {
                using (DalCommand command = CreateCommand(ProcedureNames.Plan_Update))
                {
                    // id parameter
                    SqlParameter idParameter = CreateParameter(command,
                        FieldNames.Plan_Id,
                        SqlDbType.Int,
                        dto.Id);

                    // UniqueContentReference
                    CreateParameter(command,
                        FieldNames.Plan_UniqueContentReference,
                        SqlDbType.UniqueIdentifier,
                        dto.UniqueContentReference);

                    // PlanType
                    CreateParameter(command,
                        FieldNames.Plan_Type,
                        SqlDbType.TinyInt,
                        dto.PlanType);

                    // execute
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
        }
        #endregion
    }
}
