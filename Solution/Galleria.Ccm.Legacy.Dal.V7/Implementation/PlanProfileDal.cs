﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.Schema;
using Galleria.Framework.Dal.Mssql;
using Galleria.Ccm.Legacy.Dal.V7.Scripts;

namespace Galleria.Ccm.Legacy.Dal.V7.Implementation
{
    public class PlanProfileDal : Galleria.Framework.Dal.Mssql.DalBase, IPlanProfileDal
    {
        #region Data Transfer Object

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private T GetPlanBayDto<T>(SqlDataReader dr, T dto) where T : PlanProfileDto
        {
            dto.Id = (Int32)GetValue(dr[FieldNames.Profile_Id]);
            dto.PlanId = (Int32)GetValue(dr[FieldNames.Profile_PlanID]);
            dto.ProfileDetailId = (Int32)GetValue(dr[FieldNames.Profile_ProfileDetailID]);
            dto.ProfileGroupType = Convert.ToByte(GetValue(dr[FieldNames.Profile_ProfileGroupType]));
            dto.Name = (String)GetValue(dr[FieldNames.Profile_Name]);
            dto.IsActive = Convert.ToBoolean(GetValue(dr[FieldNames.Profile_IsActive]));
            dto.Priority = Convert.ToInt16(GetValue(dr[FieldNames.Profile_Priority]));
            dto.Ratio = Convert.ToInt16(GetValue(dr[FieldNames.Profile_Ratio]));
            dto.Percentage = Convert.ToSingle(GetValue(dr[FieldNames.Profile_Percentage]));
            dto.Minimum = Convert.ToSingle(GetValue(dr[FieldNames.Profile_Minimum]));
            dto.Maximum = Convert.ToSingle(GetValue(dr[FieldNames.Profile_Maximum]));
            dto.RatioLowest = Convert.ToInt16(GetValue(dr[FieldNames.Profile_RatioLowest]));
            dto.RatioHighest = Convert.ToInt16(GetValue(dr[FieldNames.Profile_RatioHighest]));
            dto.KeyField = (String)(GetValue(dr[FieldNames.Profile_KeyField]));
            dto.ProfileGroupValue = Convert.ToInt32(GetValue(dr[FieldNames.Profile_ProfileGroupValue]));
            return dto;
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private PlanProfileDto GetManualBayDto(SqlDataReader dr)
        {
            PlanProfileDto dto = new PlanProfileDto();
            return GetPlanBayDto<PlanProfileDto>(dr, dto);
        }

        /// <summary>
        /// Reutrns a data transfer object
        /// </summary>
        /// <param name="dr">A data reader</param>
        /// <returns>A dto</returns>
        private PlanProfileDto GetSpaceBayDto(SqlDataReader dr)
        {
            PlanProfileDto dto = new PlanProfileDto();
            return GetPlanBayDto<PlanProfileDto>(dr, dto); ;
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Gets a list of All plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of PlanBay dtos</returns>
        public List<PlanProfileDto> FetchByPlanId(Int32 planId)
        {
            List<PlanProfileDto> dtoList = new List<PlanProfileDto>();

            dtoList.AddRange(FetchManualByPlanId(planId));
            dtoList.AddRange(FetchMerchandisingByPlanId(planId));
            dtoList.AddRange(FetchClusterByPlanId(planId));
            dtoList.AddRange(FetchStoreByPlanId(planId));

            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanBay dtos</returns>
        public List<PlanProfileDto> FetchManualByPlanId(Int32 planId)
        {
            List<PlanProfileDto> dtoList = new List<PlanProfileDto>();
            //No Profile
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Merchandising plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of ManualPlanBay dtos</returns>
        public List<PlanProfileDto> FetchMerchandisingByPlanId(Int32 planId)
        {
            List<PlanProfileDto> dtoList = new List<PlanProfileDto>();
             // No Profile
            return dtoList;
        }

        /// <summary>
        /// Gets a list of Cluster plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanBay dtos</returns>
        public List<PlanProfileDto> FetchClusterByPlanId(Int32 planId)
        {
            List<PlanProfileDto> dtoList = new List<PlanProfileDto>();
            using (DalCommand command = CreateCommand(PlanProfileScripts.FetchClusterByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanBay_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceBayDto(dr));
                    }
                }
            }
            return dtoList;
        }

        /// <summary>
        /// Gets a list of store plan bay records for the supplied plan Id
        /// </summary>
        /// <returns>A list of SpacePlanBay dtos</returns>
        public List<PlanProfileDto> FetchStoreByPlanId(Int32 planId)
        {
            List<PlanProfileDto> dtoList = new List<PlanProfileDto>();
            using (DalCommand command = CreateCommand(PlanProfileScripts.FetchStoreByPlanId, CommandType.Text))
            {
                CreateParameter(command, FieldNames.PlanBay_PlanId, SqlDbType.Int, planId);

                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dtoList.Add(GetSpaceBayDto(dr));
                    }
                }
            }
            return dtoList;
        }


        #endregion
    }
}
