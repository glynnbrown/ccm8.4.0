﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Legacy.Dal.V7
{
    /// <summary>
    /// Dal factory class
    /// </summary>
    public class DalFactory : Galleria.Framework.Dal.Mssql.DalFactoryBase
    {
        #region Properties
        /// <summary>
        /// Returns the database type name
        /// </summary>
        protected override String DatabaseTypeName
        {
            get { return "Legacy V7"; }
        }
        #endregion

        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion
    }
}
