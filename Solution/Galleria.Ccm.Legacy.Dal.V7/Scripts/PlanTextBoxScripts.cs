﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class PlanTextBoxScripts
    {
        #region Cluster
        public const String FetchClusterByPlanId =
            @"SELECT [ClusterPlanID] AS PlanId
				,[ClusterPlanTextBoxId] AS Id
				,[ClusterPlanBayID] AS PlanBayId
				,[ClusterPlanElementID] AS PlanElementId
				,[FixturePlanBayNumber]
				,[XPosition]
				,[YPosition]
				,[ZPosition]
				,[Height]
				,[Width]
				,[Depth]
				,[TextboxType]
				,[TextboxDescription]
			FROM 
				[dbo].[tblClusterPlanTextBox]
			WHERE 
				ClusterPlanID = @PlanId";
        #endregion Cluster

        #region Manual
        public const String FetchManualByPlanId =
            @"SELECT [ManualPlanID] AS PlanId
				,[ManualPlanTextBoxId] AS Id
				,[ManualPlanBayID] AS PlanBayId
				,[ManualPlanElementID] AS PlanElementId
				,[ManualPlanBayID] AS FixturePlanBayNumber
				,[XPosition]
				,[YPosition]
				,[ZPosition]
				,[Height]
				,[Width]
				,[Depth]
				,[TextboxType]
				,[TextboxDescription]	
			FROM 
				[dbo].[tblManualPlanTextBox]
			WHERE 
				ManualPlanID = @PlanId";
        #endregion Manual

        #region Merchandising
        public const String MerchandisingByPlanId =
            @"SELECT [TemplatePlanID] AS PlanId
				,[TemplatePlanTextBoxId] AS Id
				,0 AS PlanBayId
				,0 AS PlanElementId
				,0 AS FixturePlanBayNumber
				,0 AS [XPosition]
				,0 AS [YPosition]
				,0 AS [ZPosition]
				,0 AS [Height]
				,0 AS [Width]
				,0 AS [Depth]
				,0 AS [TextboxType]
				,[TextboxDescription]
			FROM 
				[dbo].[tblTemplatePlanTextBox]
			WHERE 
				TemplatePlanID = @PlanId";
        #endregion Merchandising

        #region Store
        public const String FetchStoreByPlanId =
            @"SELECT [StorePlanID] AS PlanId
				,[StorePlanTextBoxId] AS Id
				,[StorePlanBayID] AS PlanBayId
				,[StorePlanElementID] AS PlanElementId
				,[FixturePlanBayNumber]
				,[XPosition]
				,[YPosition]
				,[ZPosition]
				,[Height]
				,[Width]
				,[Depth]
				,[TextboxType]
				,[TextboxDescription]				
			FROM 
				[dbo].[tblStorePlanTextBox]
			WHERE 
				StorePlanID = @PlanId";
        #endregion Store
    }
}
