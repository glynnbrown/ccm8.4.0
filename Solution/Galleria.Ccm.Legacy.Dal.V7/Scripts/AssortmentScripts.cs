﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class AssortmentScripts
    {
        #region Store
        public const String FetchByStorePlanId =
            @";WITH productRecomendation AS
                (
                  SELECT  StorePlanID, StoreID, ProductID,
		                MAX(CASE WHEN InventoryTarget > 0 THEN 1 ELSE 0 END) AS Recommended,
                        MAX(PerformanceRank) AS GlobalRank,
		                MAX(InventoryTarget) AS RecomendedUnits,
		                MAX(Blocking) AS Segmentation
                  FROM [dbo].[tblStorePlanUnplacedProduct] WHERE StorePlanID = @PlanId
                  GROUP BY StorePlanID, StoreID, ProductID
                UNION ALL
                SELECT  StorePlanID, StoreID, ProductID,
		                 MAX(CASE WHEN RecommendedInventory > 0 THEN 1 ELSE 0 END) AS Recommended,
		                MAX(GlobalRank) AS GlobalRank,
		                MAX(RecommendedInventory) AS RecomendedUnits,
		                MAX(Blocking) AS Segmentation
                  FROM [dbo].[tblStoreplanposition] WHERE StorePlanID = @PlanId
                 GROUP BY StorePlanID, StoreID, ProductID
                )

                SELECT 
                    [productRecomendation].[ProductID],
	                [dbo].[tblProduct].[Code],
	                [dbo].[tblProduct].[FormattedCode],
	                [dbo].[tblProduct].[Name],
	                [productRecomendation].[Recommended],
                    ROW_NUMBER() over (partition by [dbo].[tblStoreplan].[StorePlanID] order by [productRecomendation].[GlobalRank], [productRecomendation].[RecomendedUnits] DESC) AS [GlobalRank],
                    [productRecomendation].[RecomendedUnits],
                    CASE 
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID1] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide1]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID2] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide2]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID3] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide3]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID4] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide4]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID5] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide5]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID6] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide6]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID7] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide7]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID8] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide8]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID9] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide9]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID10] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide10]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID11] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide11]
						WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID12] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide12]
						ELSE 0 END AS RecomendedFacings,
                    [productRecomendation].[Segmentation],
	                ISNULL([dbo].[tblStorePlanProductRule].[Rule1Type], -2) AS Rule1Type,
	                ISNULL([dbo].[tblStorePlanProductRule].[RuleValue1], 0) AS RuleValue1,
	                ISNULL([dbo].[tblStorePlanProductRule].[Rule2Type], -2) AS Rule2Type,
	                ISNULL([dbo].[tblStorePlanProductRule].[RuleValue2], 0) AS RuleValue2,
	                ISNULL([dbo].[tblStorePlanProductRule].[IsRuleDropped], 0) AS IsRuleDropped,
	                ISNULL([dbo].[tblStorePlanProductRule].[ViolationType], 0) AS ViolationType
                FROM productRecomendation 
	                INNER JOIN [dbo].[tblProduct] ON
		                [dbo].[tblProduct].[ProductID] = [productRecomendation].[ProductID]
	                LEFT JOIN [dbo].[tblStorePlanProductRule] ON
		                [productRecomendation].[ProductID] = [dbo].[tblStorePlanProductRule].[ProductID] AND
		                [productRecomendation].[StorePlanID] = [dbo].[tblStorePlanProductRule].[StorePlanID] AND
		                [productRecomendation].[StoreID] = [dbo].[tblStorePlanProductRule].[StoreID]
                    INNER JOIN [dbo].[tblStoreplan] 
						ON [dbo].[tblStoreplan].[StorePlanID] = [productRecomendation].[StorePlanID]
					  LEFT JOIN [dbo].[tblClusterAstScenarioFixAst]
						ON [productRecomendation].[ProductID] = [dbo].[tblClusterAstScenarioFixAst].[ProductID] AND
							[dbo].[tblStoreplan].[ScenarioID] = IIF([dbo].[tblClusterAstScenarioFixAst].[ScenarioID] % 10 = 0, 10, [dbo].[tblClusterAstScenarioFixAst].[ScenarioID] % 10) AND
							[dbo].[tblStoreplan].[ProductLevelID] = [dbo].[tblClusterAstScenarioFixAst].[ProductLevelID] AND
							[dbo].[tblStoreplan].[ClusterAssortmentID] = [dbo].[tblClusterAstScenarioFixAst].[ClusterAssortmentID] AND
							CASE 
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID1] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID2] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID3] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID4] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID5] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID6] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID7] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID8] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID9] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID10] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID11] THEN 1
								WHEN [dbo].[tblStoreplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID12] THEN 1
								ELSE 0 END = 1";
        #endregion
        #region Cluster
        public const String FetchByClusterPlanId =
            @";WITH productRecomendation AS
                (
                  SELECT  ClusterPlanID, ProductID,
		                MAX(CASE WHEN InventoryTarget > 0 THEN 1 ELSE 0 END) AS Recommended,
                        MAX(PerformanceRank) AS GlobalRank,
		                MAX(InventoryTarget) AS RecomendedUnits,
		                MAX(Blocking) AS Segmentation
                  FROM [dbo].[tblClusterPlanUnplacedProduct] WHERE ClusterPlanID = @PlanId
                  GROUP BY ClusterPlanID, ProductID
                UNION ALL
                SELECT  ClusterPlanID, ProductID,
		                MAX(CASE WHEN RecommendedInventory > 0 THEN 1 ELSE 0 END) AS Recommended,
		                MAX(GlobalRank) AS GlobalRank,
		                MAX(RecommendedInventory) AS RecomendedUnits,
		                MAX(Blocking) AS Segmentation
                  FROM [dbo].[tblClusterplanposition] WHERE ClusterPlanID = @PlanId
                 GROUP BY ClusterPlanID, ProductID
                )

                SELECT 
                    [productRecomendation].[ProductID],
	                [dbo].[tblProduct].[Code],
	                [dbo].[tblProduct].[FormattedCode],
	                [dbo].[tblProduct].[Name],
	                [productRecomendation].[Recommended],
                    ROW_NUMBER() over (partition by [dbo].[tblClusterplan].[ClusterPlanID] order by [productRecomendation].[GlobalRank], [productRecomendation].[RecomendedUnits] DESC) AS [GlobalRank],
                    [productRecomendation].[RecomendedUnits],
                    CASE 
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID1] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide1]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID2] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide2]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID3] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide3]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID4] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide4]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID5] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide5]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID6] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide6]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID7] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide7]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID8] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide8]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID9] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide9]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID10] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide10]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID11] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide11]
						WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID12] THEN [dbo].[tblClusterAstScenarioFixAst].[RecWide12]
						ELSE 0 END AS RecomendedFacings,
                    [productRecomendation].[Segmentation],
	                ISNULL([dbo].[tblClusterPlanProductRule].[Rule1Type], -2) AS Rule1Type,
	                ISNULL([dbo].[tblClusterPlanProductRule].[RuleValue1], 0) AS RuleValue1,
	                ISNULL([dbo].[tblClusterPlanProductRule].[Rule2Type], -2) AS Rule2Type,
	                ISNULL([dbo].[tblClusterPlanProductRule].[RuleValue2], 0) AS RuleValue2,
	                ISNULL([dbo].[tblClusterPlanProductRule].[IsRuleDropped], 0) AS IsRuleDropped,
	                ISNULL([dbo].[tblClusterPlanProductRule].[ViolationType], 0) AS ViolationType
                FROM productRecomendation 
	                INNER JOIN [dbo].[tblProduct] ON
		                [dbo].[tblProduct].[ProductID] = [productRecomendation].[ProductID]
	                LEFT JOIN [dbo].[tblClusterPlanProductRule] ON
		                [productRecomendation].[ProductID] = [dbo].[tblClusterPlanProductRule].[ProductID] AND
		                [productRecomendation].[ClusterPlanID] = [dbo].[tblClusterPlanProductRule].[ClusterPlanID]
                    INNER JOIN [dbo].[tblClusterplan] 
						ON [dbo].[tblClusterplan].[ClusterPlanID] = [productRecomendation].[ClusterPlanID]
					  LEFT JOIN [dbo].[tblClusterAstScenarioFixAst]
						ON [productRecomendation].[ProductID] = [dbo].[tblClusterAstScenarioFixAst].[ProductID] AND
							[dbo].[tblClusterplan].[ScenarioID] = IIF([dbo].[tblClusterAstScenarioFixAst].[ScenarioID] % 10 = 0, 10, [dbo].[tblClusterAstScenarioFixAst].[ScenarioID] % 10) AND
							[dbo].[tblClusterplan].[ProductLevelID] = [dbo].[tblClusterAstScenarioFixAst].[ProductLevelID] AND
							[dbo].[tblClusterplan].[ClusterAssortmentID] = [dbo].[tblClusterAstScenarioFixAst].[ClusterAssortmentID] AND
							CASE 
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID1] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID2] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID3] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID4] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID5] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID6] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID7] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID8] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID9] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID10] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID11] THEN 1
								WHEN [dbo].[tblClusterplan].[FixturePlanID] = [dbo].[tblClusterAstScenarioFixAst].[FixturePlanID12] THEN 1
								ELSE 0 END = 1";

        #endregion
    }
}
