﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class PlanBayScripts
    {
        #region Cluster
        public const String FetchClusterByPlanId =
            @"SELECT [ClusterPlanID] As PlanId
				,[ClusterPlanBayID] AS Id
				,0 AS StoreId
				,[FixtureID]
				,[Name]
				,[XPosition]
				,[BaseDepth]
				,[BaseHeight]
				,[BaseWidth]
				,[Height]
				,[Width]
				,[Depth]
				,[Text1]
				,[Text2]
				,[Text3]
				,[Text4]
				,[Text5]
				,[Number1]
				,[Number2]
				,[Number3]
				,[Number4]
				,[Number5]
				,[SourceSegments]
				,[DestinationSegments]
				,[Created]
				,[Modified]
				,[PersonnelID]
				,[PersonnelFlexi]
				,[CompanyID]
			FROM 
				[dbo].[tblClusterPlanBay]
			WHERE 
				ClusterPlanID = @PlanId";
        #endregion Cluster

        #region Manual
        public const String FetchManualByPlanId =
            @"SELECT [ManualPlanID] AS PlanId
				,[ManualPlanBayID] AS Id	  
				,[Name]
				,[XPosition]
				,[BaseDepth]
				,[BaseHeight]
				,[BaseWidth]
				,[Height]
				,[Width]
				,[Depth]
				,[Segments]
				,[SourceSegments]
				,[DestinationSegments]
				,[Text1]
				,[Text2]
				,[Text3]
				,[Text4]
				,[Text5]
				,[Number1]
				,[Number2]
				,[Number3]
				,[Number4]
				,[Number5]
				,[Created]
				,[Modified]
				,[PersonnelID]
				,[PersonnelFlexi]
				,[CompanyID]
			FROM 
				[dbo].[tblManualPlanBay]
			WHERE 
				ManualPlanID = @PlanId";
        #endregion Manual

        #region Merchandising
        public const String FetchMerchandisingByPlanId =
            @"SELECT [TemplatePlanID] AS PlanId
				,[TemplatePlanBayID] AS Id	  
				,[Name]
				,[XPosition]
				,[BaseDepth]
				,[BaseHeight]
				,[BaseWidth]
				,[Height]
				,[Width]
				,[Depth]
				,[Segments]
				,[SourceSegments]
				,[DestinationSegments]
				,[Text1]
				,[Text2]
				,[Text3]
				,[Text4]
				,[Text5]
				,[Number1]
				,[Number2]
				,[Number3]
				,[Number4]
				,[Number5]
				,[Created]
				,[Modified]
				,[PersonnelID]
				,[PersonnelFlexi]
				,[CompanyID]
			FROM 
				[dbo].[tblTemplatePlanBay]
			WHERE 
				TemplatePlanID = @PlanId";
        #endregion Merchandising

        #region Store
        public const String FetchStoreByPlanId =
            @"SELECT spb.[StorePlanID] AS PlanId
				,spb.[StoreID]
				,spb.[StorePlanBayID] AS Id
				,spb.[FixtureID]
				,spb.[Name]
				,spb.[XPosition]
				,spb.[BaseDepth]
				,spb.[BaseHeight]
				,spb.[BaseWidth]
				,spb.[Height]
				,spb.[Width]
				,spb.[Depth]
				,spb.[Text1]
				,spb.[Text2]
				,spb.[Text3]
				,spb.[Text4]
				,spb.[Text5]
				,spb.[Number1]
				,spb.[Number2]
				,spb.[Number3]
				,spb.[Number4]
				,spb.[Number5]
				,spb.[SourceSegments]
				,spb.[DestinationSegments]
				,sp.Created
				,sp.Modified
				,sp.PersonnelId
				,sp.PersonnelFlexi 
				,sp.CompanyId 
			FROM 
				[dbo].[tblStorePlanBay] spb 
			INNER JOIN 
				[dbo].[tblStorePlan] sp ON sp.[StorePlanId] = spb.[StorePlanId]
			WHERE 
				spb.StorePlanId = @PlanId";
        #endregion Store
    }
}
