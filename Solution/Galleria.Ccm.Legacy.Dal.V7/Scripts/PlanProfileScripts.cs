﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class PlanProfileScripts
    {
        #region Cluster
        public const String FetchClusterByPlanId =
            @"SELECT [ClusterPlanProfileDetailID] AS Id
                  ,[ClusterPlanID] AS PlanId
                  ,[ProfileDetailID]
                  ,[ProfileGroupType]
                  ,[Name]
                  ,[IsActive]
                  ,[Priority]
                  ,[Ratio]
                  ,[Percentage]
                  ,[Minimum]
                  ,[Maximum]
                  ,[RatioLowest]
                  ,[RatioHighest]
                  ,[KeyField]
                  ,[ProfileGroupValue]
            FROM [dbo].[tblClusterPlanProfileDetail]
			WHERE 
				ClusterPlanID = @PlanId";
        #endregion Cluster

        #region Store
        public const String FetchStoreByPlanId =
            @"SELECT [StorePlanProfileDetailID] AS Id
                  ,[StorePlanID] AS PlanId
                  ,[StoreID]
                  ,[ProfileDetailID]
                  ,[ProfileGroupType]
                  ,[Name]
                  ,[IsActive]
                  ,[Priority]
                  ,[Ratio]
                  ,[Percentage]
                  ,[Minimum]
                  ,[Maximum]
                  ,[RatioLowest]
                  ,[RatioHighest]
                  ,[KeyField]
                  ,[ProfileGroupValue]
            FROM [dbo].[tblStorePlanProfileDetail]
			WHERE 
				StorePlanId = @PlanId";
        #endregion Store
    }
}
