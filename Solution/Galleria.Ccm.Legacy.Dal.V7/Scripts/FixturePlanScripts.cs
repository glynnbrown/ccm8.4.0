﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class FixturePlanScripts
    {
        public const String FetchAll =
            @"SELECT [FixturePlanID] AS FixturePlan_Id
			  ,[ProductLevelID] AS FixturePlan_ProductLevelID
			  ,[Name] AS FixturePlan_Name
			  ,[SourceFile] AS FixturePlan_SourceFile
			  ,[Blocking] AS FixturePlan_Blocking
			  ,[MerchShelfSpace] AS FixturePlan_MerchShelfSpace
			  ,[MerchPegSpace] AS FixturePlan_MerchPegSpace
			  ,[MerchBarSpace] AS FixturePlan_MerchBarSpace
			  ,[MerchChestSpace] AS FixturePlan_MerchChestSpace
			  ,[Text1] AS FixturePlan_Text1
			  ,[Text2] AS FixturePlan_Text2
			  ,[Text3] AS FixturePlan_Text3
			  ,[Text4] AS FixturePlan_Text4
			  ,[Text5] AS FixturePlan_Text5
			  ,[Number1] AS FixturePlan_Number1
			  ,[Number2] AS FixturePlan_Number2
			  ,[Number3] AS FixturePlan_Number3
			  ,[Number4] AS FixturePlan_Number4
			  ,[Number5] AS FixturePlan_Number5
			  ,[ProductLevelFlexi] AS FixturePlan_ProductLevelFlexi
			  ,[BayWidthValues] AS FixturePlan_BayWidthValues
			  ,[PresentationTotalWidth] AS FixturePlan_PresentationTotalWidth
			  ,[PresentationBayWidth] AS FixturePlan_PresentationBayWidth
			  ,[MaxHeight] AS FixturePlan_MaxHeight
			  ,[PresentationMaxHeight] AS FixturePlan_PresentationMaxHeight
			  ,[CustomCalculationValue] AS FixturePlan_CustomCalculationValue
			  ,[CustomPostFix] AS FixturePlan_CustomPostFix
			  ,[TotalWidth] AS FixturePlan_TotalWidth
			  ,[BayCount] AS FixturePlan_BayCount
			  ,[Created] AS FixturePlan_Created
			  ,[Modified] AS FixturePlan_Modified
			  ,[PersonnelID] AS FixturePlan_PersonnelID
			  ,[PersonnelFlexi] AS FixturePlan_PersonnelFlexi
			  ,[CompanyID] AS FixturePlan_CompanyID
		  FROM [dbo].[tblFixturePlan]";
        public const String FetchById =
            @"SELECT [FixturePlanID] AS FixturePlan_Id
			  ,[ProductLevelID] AS FixturePlan_ProductLevelID
			  ,[Name] AS FixturePlan_Name
			  ,[SourceFile] AS FixturePlan_SourceFile
			  ,[Blocking] AS FixturePlan_Blocking
			  ,[MerchShelfSpace] AS FixturePlan_MerchShelfSpace
			  ,[MerchPegSpace] AS FixturePlan_MerchPegSpace
			  ,[MerchBarSpace] AS FixturePlan_MerchBarSpace
			  ,[MerchChestSpace] AS FixturePlan_MerchChestSpace
			  ,[Text1] AS FixturePlan_Text1
			  ,[Text2] AS FixturePlan_Text2
			  ,[Text3] AS FixturePlan_Text3
			  ,[Text4] AS FixturePlan_Text4
			  ,[Text5] AS FixturePlan_Text5
			  ,[Number1] AS FixturePlan_Number1
			  ,[Number2] AS FixturePlan_Number2
			  ,[Number3] AS FixturePlan_Number3
			  ,[Number4] AS FixturePlan_Number4
			  ,[Number5] AS FixturePlan_Number5
			  ,[ProductLevelFlexi] AS FixturePlan_ProductLevelFlexi
			  ,[BayWidthValues] AS FixturePlan_BayWidthValues
			  ,[PresentationTotalWidth] AS FixturePlan_PresentationTotalWidth
			  ,[PresentationBayWidth] AS FixturePlan_PresentationBayWidth
			  ,[MaxHeight] AS FixturePlan_MaxHeight
			  ,[PresentationMaxHeight] AS FixturePlan_PresentationMaxHeight
			  ,[CustomCalculationValue] AS FixturePlan_CustomCalculationValue
			  ,[CustomPostFix] AS FixturePlan_CustomPostFix
			  ,[TotalWidth] AS FixturePlan_TotalWidth
			  ,[BayCount] AS FixturePlan_BayCount
			  ,[Created] AS FixturePlan_Created
			  ,[Modified] AS FixturePlan_Modified
			  ,[PersonnelID] AS FixturePlan_PersonnelID
			  ,[PersonnelFlexi] AS FixturePlan_PersonnelFlexi
			  ,[CompanyID] AS FixturePlan_CompanyID
		  FROM [dbo].[tblFixturePlan]
		  WHERE [FixturePlanID] = @FixturePlan_Id";
    }
}
