﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 3. It was improved the way the combined direction for planogram elements are calculated. 
//   - It was ammend 2 scrips (FetchClusterByPlanId and FetchStoreByPlanId) to check the plan elements bloking type. If the blocking type was equal to 10 then it was a element to be combine.
//   - In the split plan it was added an addition step to re-calculate plan element combine direction based on the OriginalTemplateElementId.
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class PlanElementScripts
    {
        #region Cluster
        public const String FetchClusterByPlanId =
            @";With ElementToBay(OriginalTemplateElementID,MinBayID,MaxBayID) AS
				(
				--Get the min and max element bay ID's
				SELECT 
					tblClusterPlanElement.OriginalTemplateElementID,
					min(tblClusterPlanBay.ClusterPlanBayID),
					max(tblClusterPlanBay.ClusterPlanBayID)
				FROM tblClusterPlanElement 
					INNER JOIN tblClusterPlanBay ON tblClusterPlanElement.ClusterPlanID = tblClusterPlanBay.ClusterPlanID
					AND tblClusterplanelement.ClusterPlanBayID = tblClusterPlanBay.ClusterPlanBayID
				WHERE tblClusterPlanElement.ClusterPlanID = @PlanId
				GROUP BY tblClusterPlanElement.OriginalTemplateElementID 
				),
                MergedElements(ClusterPlanElementID) AS
                (
                    select DISTINCT tblClusterPlanposition.ClusterPlanElementID from tblClusterPlanBlockingInfo inner join
                    tblClusterPlanposition ON tblClusterPlanposition.ClusterPlanID = tblClusterPlanBlockingInfo.ClusterPlanID
                    and tblClusterPlanposition.Blocking = tblClusterPlanBlockingInfo.ParentBlockID
                    where tblClusterPlanBlockingInfo.ClusterPlanID =  @PlanId and tblClusterPlanBlockingInfo.BlockType = 10
                )
                
            SELECT cpe.[ClusterPlanID] AS PlanId
				,cpe.[ClusterPlanElementID] AS Id
				,cpe.[ClusterPlanBayID] AS PlanBayId
				,0 AS StoreId
				,ISNULL(tpe.[Name],cpe.[Name]) AS [Name]
				,cpe.[Combined]
				,cpe.[XPosition]
				,cpe.[YPosition]
				,cpe.[ZPosition]
				,cpe.[ElementType]
				,cpe.[ShelfHeight]
				,cpe.[ShelfWidth]
				,cpe.[ShelfDepth]
				,cpe.[ShelfThick]
				,cpe.[ShelfSlope]
				,cpe.[ShelfRiser]
				,cpe.[PegHeight]
				,cpe.[PegWidth]
				,cpe.[PegVertSpace]
				,cpe.[PegHorzSpace]
				,cpe.[PegVertStart]
				,cpe.[PegHorzStart]
				,cpe.[PegNotchDistance]
				,cpe.[PegDepth]
				,cpe.[ChestHeight]
				,cpe.[ChestWidth]
				,cpe.[ChestDepth]
				,cpe.[ChestWall]
				,CAST(1 AS TINYINT) AS ChestWallRenderType
				,cpe.[ChestInside]
				,cpe.[ChestMerchandising]
				,cpe.[ChestDivider]
				,cpe.[ChestAbove]
				,cpe.[BarHeight]
				,cpe.[BarDepth]
				,cpe.[BarWidth]
				,cpe.[BarThick]
				,cpe.[BarDistance]
				,cpe.[BarHangerDepth]
				,cpe.[BarNotch]
				,cpe.[BarBackHeight]
				,cpe.[BarHorzStart]
				,cpe.[BarHorzSpace]
				,cpe.[BarVertStart]
				,cpe.[NonMerchandisable]
				,cpe.[FingerSpace]
				,cpe.[TopOverhang]
				,cpe.[BottomOverhang]
				,cpe.[LeftOverhang]
				,cpe.[RightOverhang]
				,cpe.[FrontOverhang]
				,cpe.[BackOverhang]
				,cpe.[EmptyPegs]
				,cpe.[RightEmptyPeg]
				,cpe.[ProductCount]
				,cpe.[PrimaryPegs]
				,cpe.[StartX]
				,cpe.[MaxHeight]
				,cpe.[Number1]
				,cpe.[Number2]
				,cpe.[Number3]
				,cpe.[Number4]
				,cpe.[Number5]
				,cpe.[Created]
				,cpe.[Modified]
				,cpe.[PersonnelID]
				,cpe.[PersonnelFlexi]
				,cpe.[CompanyID],
                CASE			
					--When bay is the start and end of the fixture profile section			
					WHEN	cpe.ClusterPlanBayID = ElementToBay.MinBayID AND
							cpe.ClusterPlanBayID = ElementToBay.MaxBayID
					THEN	'0'--Combine OFF
					--When bay is in the middle of the fixture profile section			
					WHEN	cpe.ClusterPlanBayID > ElementToBay.MinBayID AND
							cpe.ClusterPlanBayID < ElementToBay.MaxBayID
					THEN	'1'--Combine Both
					--When bay is at the end of the fixture profile section			
					WHEN	cpe.ClusterPlanBayID > ElementToBay.MinBayID AND
							cpe.ClusterPlanBayID = ElementToBay.MaxBayID
					THEN	'2'--Combine LEFT
					--When bay is at the beginning of the fixture profile section			
					WHEN	cpe.ClusterPlanBayID = ElementToBay.MinBayID AND
							cpe.ClusterPlanBayID < ElementToBay.MaxBayID
					THEN	'3'--Combine RIGHT
					ELSE	'0'--Combine OFF
				END AS CombineDirection
                ,cpe.[OriginalTemplateElementId]
			FROM 
				[dbo].[tblClusterPlanElement] cpe
            LEFT JOIN ElementToBay ON
				ElementToBay.OriginalTemplateElementID = cpe.OriginalTemplateElementID
			LEFT JOIN
				[dbo].[tblController06] c06 ON c06.[Controller06Id] = cpe.[ClusterPlanId]
			LEFT JOIN
				[dbo].[tblDecisionTree] dt ON dt.[DecisionTreeId] = c06.[ProductLevelId]
			LEFT JOIN
				[dbo].[tblTemplatePlanElement] tpe ON tpe.[TemplatePlanId] = dt.[TemplatePlanId01]
								AND tpe.[TemplatePlanElementId] = cpe.[OriginalTemplateElementId]
            LEFT JOIN MergedElements ON
                MergedElements.ClusterPlanElementID = cpe.ClusterPlanElementID
			WHERE 
				ClusterPlanID = @PlanId";
        #endregion Cluster

        #region Manual
        public const String FetchManualByPlanId =
            @"SELECT [ManualPlanID] AS PlanId
				,[ManualPlanElementID] AS Id
				,[ManualPlanBayID] AS PlanBayId
				,[Name]
				,[Combined]
				,[XPosition]
				,[YPosition]
				,[ZPosition]
				,[ElementType]
				,[ShelfHeight]
				,[ShelfWidth]
				,[ShelfDepth]
				,[ShelfThick]
				,[ShelfSlope]
				,[ShelfRiser]
				,[PegHeight]
				,[PegWidth]
				,[PegVertSpace]
				,[PegHorzSpace]
				,[PegVertStart]
				,[PegHorzStart]
				,[PegNotchDistance]
				,[PegDepth]
				,[ChestHeight]
				,[ChestWidth]
				,[ChestDepth]
				,[ChestWall]
				,[ChestWallRenderType]
				,[ChestInside]
				,[ChestMerchandising]
				,[ChestDivider]
				,[ChestAbove]
				,[BarHeight]
				,[BarWidth]
				,[BarDepth]
				,[BarThick]
				,[BarDistance]
				,[BarHangerDepth]
				,[BarNotch]
				,[BarBackHeight]
				,[BarHorzStart]
				,[BarHorzSpace]
				,[BarVertStart]
				,[NonMerchandisable]
				,[FingerSpace]
				,[TopOverhang]
				,[BottomOverhang]
				,[LeftOverhang]
				,[RightOverhang]
				,[FrontOverhang]
				,[BackOverhang]
				,[Text1]
				,[Text2]
				,[Text3]
				,[Text4]
				,[Text5]
				,[Number1]
				,[Number2]
				,[Number3]
				,[Number4]
				,[Number5]
				,[Created]
				,[Modified]
				,[PersonnelID]
				,[PersonnelFlexi]
				,[CompanyID]
                ,0 AS CombineDirection
                ,0 AS OriginalTemplateElementId
			FROM 
				[dbo].[tblManualPlanElement]
			WHERE 
				ManualPlanID = @PlanId";
        #endregion Manual

        #region Merchandising
        public const String MerchandisingByPlanId =
            @"SELECT [TemplatePlanID] AS PlanId
				,[TemplatePlanElementID] AS Id
				,[TemplatePlanBayID] AS PlanBayId
				,[Name]
				,[Combined]
				,[XPosition]
				,[YPosition]
				,[ZPosition]
				,[ElementType]
				,[ShelfHeight]
				,[ShelfWidth]
				,[ShelfDepth]
				,[ShelfThick]
				,[ShelfSlope]
				,[ShelfRiser]
				,[PegHeight]
				,[PegWidth]
				,[PegVertSpace]
				,[PegHorzSpace]
				,[PegVertStart]
				,[PegHorzStart]
				,[PegNotchDistance]
				,[PegDepth]
				,[ChestHeight]
				,[ChestWidth]
				,[ChestDepth]
				,[ChestWall]
				,1 AS ChestWallRenderType
				,[ChestInside]
				,[ChestMerchandising]
				,[ChestDivider]
				,[ChestAbove]
				,[BarHeight]
				,[BarWidth]
				,[BarDepth]
				,[BarThick]
				,[BarDistance]
				,[BarHangerDepth]
				,[BarNotch]
				,[BarBackHeight]
				,[BarHorzStart]
				,[BarHorzSpace]
				,[BarVertStart]
				,[NonMerchandisable]
				,[FingerSpace]
				,[TopOverhang]
				,[BottomOverhang]
				,[LeftOverhang]
				,[RightOverhang]
				,[FrontOverhang]
				,[BackOverhang]
				,[Text1]
				,[Text2]
				,[Text3]
				,[Text4]
				,[Text5]
				,[Number1]
				,[Number2]
				,[Number3]
				,[Number4]
				,[Number5]
				,[Created]
				,[Modified]
				,[PersonnelID]
				,[PersonnelFlexi]
				,[CompanyID]
                ,0 AS CombineDirection
                ,0 AS OriginalTemplateElementId
			FROM 
				[dbo].[tblTemplatePlanElement]
			WHERE 
				TemplatePlanID = @PlanId";
        #endregion Merchandising

        #region Store
        public const String FetchStoreByPlanId =
            @";With ElementToBay(OriginalTemplateElementID,MinBayID,MaxBayID) AS
				(
				--Get the min and max element bay ID's
				SELECT 
                    tblStorePlanElement.OriginalTemplateElementID,
					min(tblStorePlanBay.StorePlanBayID),
					max(tblStorePlanBay.StorePlanBayID)
				FROM tblStorePlanElement 
					INNER JOIN tblStorePlanBay ON tblStorePlanElement.StorePlanID = tblStorePlanBay.StorePlanID
					AND tblstoreplanelement.StorePlanBayID = tblStorePlanBay.StorePlanBayID
				WHERE tblStorePlanElement.StorePlanID = @PlanId
				GROUP BY tblStorePlanElement.OriginalTemplateElementID
				),
                MergedElements(StorePlanElementID) AS
                (
                    select DISTINCT tblStorePlanposition.StorePlanElementID from tblStorePlanBlockingInfo inner join
                    tblStorePlanposition ON tblStorePlanposition.StorePlanID = tblStorePlanBlockingInfo.StorePlanID
                    and tblStorePlanposition.Blocking = tblStorePlanBlockingInfo.ParentBlockID
                    where tblStorePlanBlockingInfo.StorePlanID =  @PlanId and tblStorePlanBlockingInfo.BlockType = 10
                )

                SELECT spe.[StorePlanID] AS PlanId
				,spe.[StoreID]
				,spe.[StorePlanElementID] AS Id
				,spe.[StorePlanBayID] AS PlanBayId
				,ISNULL(tpe.[Name], spe.[Name]) AS [Name]
				,spe.[Combined]
				,spe.[XPosition]
				,spe.[YPosition]
				,spe.[ZPosition]
				,spe.[ElementType]
				,spe.[ShelfHeight]
				,spe.[ShelfWidth]
				,spe.[ShelfDepth]
				,spe.[ShelfThick]
				,spe.[ShelfSlope]
				,spe.[ShelfRiser]
				,spe.[PegHeight]
				,spe.[PegWidth]
				,spe.[PegVertSpace]
				,spe.[PegHorzSpace]
				,spe.[PegVertStart]
				,spe.[PegHorzStart]
				,spe.[PegNotchDistance]
				,spe.[PegDepth]
				,spe.[ChestHeight]
				,spe.[ChestWidth]
				,spe.[ChestDepth]
				,spe.[ChestWall]
				,CAST(1 AS TINYINT) AS ChestWallRenderType
				,spe.[ChestInside]
				,spe.[ChestMerchandising]
				,spe.[ChestDivider]
				,spe.[ChestAbove]
				,spe.[BarHeight]
				,spe.[BarDepth]
				,spe.[BarWidth]
				,spe.[BarThick]
				,spe.[BarDistance]
				,spe.[BarHangerDepth]
				,spe.[BarNotch]
				,spe.[BarBackHeight]
				,spe.[BarHorzStart]
				,spe.[BarHorzSpace]
				,spe.[BarVertStart]
				,spe.[NonMerchandisable]
				,spe.[FingerSpace]
				,spe.[TopOverhang]
				,spe.[BottomOverhang]
				,spe.[LeftOverhang]
				,spe.[RightOverhang]
				,spe.[FrontOverhang]
				,spe.[BackOverhang]
				,spe.[EmptyPegs]
				,spe.[RightEmptyPeg]
				,spe.[ProductCount]
				,spe.[PrimaryPegs]
				,spe.[StartX]
				,spe.[MaxHeight]
				,spe.[Number1]
				,spe.[Number2]
				,spe.[Number3]
				,spe.[Number4]
				,spe.[Number5]
				,sp.Created
				,sp.Modified
				,sp.PersonnelId
				,sp.PersonnelFlexi 
				,sp.CompanyId,
                CASE
                    WHEN ISNULL(MergedElements.StorePlanElementID, 0) > 0
                    THEN  '1'--Combine Both
					--When bay is the start and end of the fixture profile section			
					WHEN	spe.StorePlanBayID = ElementToBay.MinBayID AND
							spe.StorePlanBayID = ElementToBay.MaxBayID
					THEN	'0'--Combine OFF
					--When bay is in the middle of the fixture profile section			
					WHEN	spe.StorePlanBayID > ElementToBay.MinBayID AND
							spe.StorePlanBayID < ElementToBay.MaxBayID
					THEN	'1'--Combine Both
					--When bay is at the end of the fixture profile section			
					WHEN	spe.StorePlanBayID > ElementToBay.MinBayID AND
							spe.StorePlanBayID = ElementToBay.MaxBayID
					THEN	'2'--Combine LEFT
					--When bay is at the beginning of the fixture profile section			
					WHEN	spe.StorePlanBayID = ElementToBay.MinBayID AND
							spe.StorePlanBayID < ElementToBay.MaxBayID
					THEN	'3'--Combine RIGHT
					ELSE	'0'--Combine OFF
				END AS CombineDirection
                ,spe.[OriginalTemplateElementId]
			FROM 
				[dbo].[tblStorePlanElement] spe 
			INNER JOIN 
				[dbo].[tblStorePlan] sp ON sp.[StorePlanId] = spe.[StorePlanId]
            LEFT JOIN ElementToBay ON
				ElementToBay.OriginalTemplateElementID = spe.OriginalTemplateElementID
			LEFT JOIN
				[dbo].[tblController05] c05 ON c05.[Controller05Id] = sp.[StorePlanId]
			LEFT JOIN
				[dbo].[tblDecisionTree] dt ON dt.[DecisionTreeId] = c05.[ProductLevelId]
			LEFT JOIN
				[dbo].[tblTemplatePlanElement] tpe ON tpe.[TemplatePlanId] = dt.[TemplatePlanId01]
								AND tpe.[TemplatePlanElementId] = spe.[OriginalTemplateElementId]
            LEFT JOIN MergedElements ON
                MergedElements.StorePlanElementID = spe.StorePlanElementID
			WHERE 
				spe.StorePlanId = @PlanId";
        #endregion Store
    }
}
