﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class FixturePlanBayScripts
    {
        public const String FetchByFixturePlanId =
            @"SELECT [FixturePlanID] AS FixturePlanBay_FixturePlanId
			  ,[FixturePlanBayID] AS FixturePlanBay_Id      
			  ,[BayNumber] AS FixturePlanBay_BayNumber
			  ,[Name] AS FixturePlanBay_Name
			  ,[XPosition] AS FixturePlanBay_XPosition
			  ,[BaseDepth] AS FixturePlanBay_BaseDepth
			  ,[BaseHeight] AS FixturePlanBay_BaseHeight
			  ,[BaseWidth] AS FixturePlanBay_BaseWidth
			  ,[Height] AS FixturePlanBay_Height
			  ,[Width] AS FixturePlanBay_Width
			  ,[Depth] AS FixturePlanBay_Depth
			  ,[Text1] AS FixturePlanBay_Text1
			  ,[Text2] AS FixturePlanBay_Text2
			  ,[Text3] AS FixturePlanBay_Text3
			  ,[Text4] AS FixturePlanBay_Text4
			  ,[Text5] AS FixturePlanBay_Text5
			  ,[Number1] AS FixturePlanBay_Number1
			  ,[Number2] AS FixturePlanBay_Number2
			  ,[Number3] AS FixturePlanBay_Number3
			  ,[Number4] AS FixturePlanBay_Number4
			  ,[Number5] AS FixturePlanBay_Number5
			  ,[Created] AS FixturePlanBay_Created
			  ,[Modified] AS FixturePlanBay_Modified
			  ,[PersonnelID] AS FixturePlanBay_PersonnelID
			  ,[PersonnelFlexi] AS FixturePlanBay_PersonnelFlexi
			  ,[CompanyID] AS FixturePlanBay_CompanyID
		  FROM [dbo].[tblFixturePlanBay]
		  WHERE [FixturePlanID] = @FixturePlanBay_FixturePlanId";
    }
}
