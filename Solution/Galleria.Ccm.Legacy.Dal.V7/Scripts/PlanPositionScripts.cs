﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Scripts
{
    public class PlanPositionScripts
    {
        #region Cluster
        public const String FetchClusterByPlanId =
            @"SELECT [ClusterPlanID] AS PlanId
				,[ClusterPlanPosition] AS Id
				,[ClusterPlanElementID] AS PlanElementId
				,0 AS StoreId
				,[ProductID]
				,[OrientationID]
				,CAST([XPosition] AS REAL) AS XPosition
				,CAST([YPosition] AS REAL) AS YPosition
				,CAST([Deep] AS INT) AS Deep
				,CAST([High] AS INT) AS High
				,CAST([Wide] AS INT) AS Wide
				,[ProductHeight]
				,[ProductWidth]
				,[ProductDepth]
				,CAST([Units] AS INT) AS Units
				,CAST([SqueezeHeight] AS REAL) AS SqueezeHeight
				,CAST([SqueezeWidth] AS REAL) AS SqueezeWidth
				,CAST([SqueezeDepth] AS REAL) AS SqueezeDepth
				,CAST([BlockHeight] AS REAL) AS BlockHeight
				,CAST([BlockTop] AS REAL) AS BlockTop
				,CAST([BlockWidth] AS REAL) AS BlockWidth
				,CAST([BlockDepth] AS REAL) AS BlockDepth
				,CAST([NestHeight] AS REAL) AS NestHeight
				,CAST([NestWidth] AS REAL) AS NestWidth
				,CAST([NestDepth] AS REAL) AS NestDepth
				,CAST([LeadingDivider] AS REAL) AS LeadingDivider
				,CAST([LeadingGap] AS REAL) AS LeadingGap
				,CAST([LeadingGapVert] AS REAL) AS LeadingGapVert
				,CAST([CapTopDeep] AS INT) AS CapTopDeep
				,CAST([CapTopHigh] AS INT) AS CapTopHigh
				,CAST([CapBotDeep] AS INT) AS CapBotDeep
				,CAST([CapBotHigh] AS INT) AS CapBotHigh
				,CAST([CapLftDeep] AS INT) AS CapLftDeep
				,CAST([CapLftWide] AS INT) AS CapLftWide
				,CAST([CapRgtDeep] AS INT) AS CapRgtDeep
				,CAST([CapRgtWide] AS INT) AS CapRgtWide
				,[TargetSC]
				,CAST([MaxCap] AS INT) AS MaxCap
				,CAST([MaxStack] AS INT) AS MaxStack
				,CAST([MinDeep] AS INT) AS MinDeep
				,CAST([MaxDeep] AS INT) AS MaxDeep
				,CAST([TrayHigh] AS INT) AS TrayHigh
				,CAST([TrayWide] AS INT) AS TrayWide
				,CAST([TrayDeep] AS INT) AS TrayDeep
				,CAST([TrayHeight] AS FLOAT) AS TrayHeight
				
				,CAST([TrayWidth] AS FLOAT) AS TrayWidth
				,CAST([TrayDepth] AS FLOAT) AS TrayDepth
				,CAST([TrayUnits] AS INT) AS TrayUnits						
				
				,CAST([TrayThickHeight] AS REAL) AS TrayThickHeight
				,CAST([TrayThickWidth] AS REAL) AS TrayThickWidth
				,CAST([TrayThickDepth] AS REAL) AS TrayThickDepth
				,CAST([TrayCountHigh] AS INT) AS TrayCountHigh
				,CAST([TrayCountWide] AS INT) AS TrayCountWide
				,CAST([TrayCountDeep] AS INT) AS TrayCountDeep
				,[TrayProduct]
				,CAST([NestHigh] AS INT) AS NestHigh
				,CAST([NestWide] AS INT) AS NestWide
				,CAST([NestDeep] AS INT) AS NestDeep
				,[MerchType]
				,[Blocking]
				,[BlockID]
				,[BrushStyle]
				,[MinDrop]
				,[RemoveCount]
				,CAST([CapRightWide] AS INT) AS CapRightWide
				,CAST([CapRightDeep] AS INT) AS CapRightDeep
				,CAST([CapRightCapTopHigh] AS INT) AS CapRightCapTopHigh
				,CAST([CapRightCapTopDeep] AS INT) AS CapRightCapTopDeep
				,CAST([CapRightWideTrayCount] AS INT) AS CapRightWideTrayCount
				,CAST([CapRightDeepTrayCount] AS INT) AS CapRightDeepTrayCount
				,[MultiSited]
				,[CasePack]
				,[Colour]
				,[LastChance]
				,[AddLocked]
				,[RemoveLocked]
				,[StripNumber]
				,[PegFix]
				,[PegShift]
				,[ElementPositionID]
				,[HistoricalDailyUnits]
				,[ActualDaysSupplied]
				,[BreakTrayUp]
				,[BreakTrayDown]
				,[BreakTrayBack]				
				,[BreakTrayTop]
				,[FrontOnly]
				,[MiddleCapping]
				,[BlockTagName]
				,[GlobalRank]
				,[FinalMin]
				,[FinalMax]
				,[MinPriority1]
				,[MinPriority2]
				,[MinPriority3]
				,[MinPriority4]
				,[MinPriority5]
				,[RecommendedInventory]
				,[CompromiseInventory]
				,[AverageWeeklySales]
				,[AverageWeeklyUnits]
				,[AverageWeeklyProfit]
				,[AchievedCases]
				,[AchievedDOS]
				,[PerformanceValue01]
				,[PerformanceValue02]
				,[PerformanceValue03]
				,[PerformanceValue04]
				,[PerformanceValue05]
				,[PerformanceValue06]
				,[PerformanceValue07]
				,[PerformanceValue08]
				,[PerformanceValue09]
				,[PerformanceValue10]
				,[MaxNestHigh]
				,[MaxNestDeep]
				,[PegDepth]
				,[ListingPSI]
				,[ExposurePSI]
				,[PegX]
				,[PegY]
				,[PegProng]
				,[MaxRightCap]
				,[CanBeRefaced]
				,[AchievedInventoryLevelType]
				,[Created]
				,[Modified]
				,[PersonnelID]
				,[PersonnelFlexi]
				,[CompanyID]
				,[MaxPriority1]
				,[MaxPriority2]
				,[MaxPriority3]
				,[MaxPriority4]
				,[MaxPriority5]
			FROM 
				[dbo].[tblClusterPlanPosition]
			WHERE 
				ClusterPlanID = @PlanId";
        #endregion Cluster

        #region Manual
        public const String FetchManualByPlanId =
            @"SELECT mpp.[ManualPlanID] AS PlanId
				,mpp.[ManualPlanPositionID] AS Id
				,mpp.[ManualPlanElementID] AS PlanElementId
				,mpp.[ProductID]
				,mpp.[OrientationID]
				,mpp.[Code]
				,CAST(mpp.[XPosition] AS REAL) AS XPosition
				,CAST(mpp.[YPosition] AS REAL) AS YPosition
				,CAST(mpp.[Deep] AS INT) AS Deep
				,CAST(mpp.[High] AS INT) AS High
				,CAST(mpp.[Wide] AS INT) AS Wide
				,CAST(mpp.[Units] AS INT) AS Units
				,mpp.[ProductHeight]
				,mpp.[ProductDepth]
				,mpp.[ProductWidth]
				,CAST(mpp.[SqueezeHeight] AS REAL) AS SqueezeHeight
				,CAST(mpp.[SqueezeWidth] AS REAL) AS SqueezeWidth
				,CAST(mpp.[SqueezeDepth] AS REAL) AS SqueezeDepth
				,CAST(mpp.[BlockHeight] AS REAL) AS BlockHeight
				,CAST(mpp.[BlockTop] AS REAL) AS BlockTop
				,CAST(mpp.[BlockWidth] AS REAL) AS BlockWidth
				,CAST(mpp.[BlockDepth] AS REAL) AS BlockDepth
				,CAST(mpp.[NestHeight] AS REAL) AS NestHeight
				,CAST(mpp.[NestWidth] AS REAL) AS NestWidth
				,CAST(mpp.[NestDepth] AS REAL) AS NestDepth
				,CAST(mpp.[LeadingDivider] AS REAL) AS LeadingDivider
				,CAST(mpp.[LeadingGap] AS REAL) AS LeadingGap
				,CAST(mpp.[LeadingGapVert] AS REAL) AS LeadingGapVert
				,CAST(mpp.[CapTopDeep] AS INT) AS CapTopDeep
				,CAST(mpp.[CapTopHigh] AS INT) AS CapTopHigh
				,CAST(mpp.[CapBotDeep] AS INT) AS CapBotDeep
				,CAST(mpp.[CapBotHigh] AS INT) AS CapBotHigh
				,CAST(mpp.[CapLftDeep] AS INT) AS CapLftDeep
				,CAST(mpp.[CapLftWide] AS INT) AS CapLftWide
				,CAST(mpp.[CapRgtDeep] AS INT) AS CapRgtDeep
				,CAST(mpp.[CapRgtWide] AS INT) AS CapRgtWide
				,mpp.[TargetSC]
				,CAST(mpp.[MaxCap] AS INT) AS MaxCap
				,CAST(mpp.[MaxStack] AS INT) AS MaxStack
				,CAST(mpp.[TrayHigh] AS INT) AS TrayHigh
				,CAST(mpp.[TrayWide] AS INT) AS TrayWide
				,CAST(mpp.[TrayDeep] AS INT) AS TrayDeep
				,CAST(mpp.[TrayHeight] AS FLOAT) AS TrayHeight
				
				,CAST(mpp.[TrayWidth] AS FLOAT) AS TrayWidth
				,CAST(mpp.[TrayDepth] AS FLOAT) AS TrayDepth
				,CAST(mpp.[TrayUnits] AS INT) AS TrayUnits				
				
				,CAST(mpp.[TrayThickHeight] AS REAL) AS TrayThickHeight
				,CAST(mpp.[TrayThickWidth] AS REAL) AS TrayThickWidth
				,CAST(mpp.[TrayThickDepth] AS REAL) AS TrayThickDepth
				,CAST(mpp.[TrayCountHigh] AS INT) AS TrayCountHigh
				,CAST(mpp.[TrayCountWide] AS INT) AS TrayCountWide
				,CAST(mpp.[TrayCountDeep] AS INT) AS TrayCountDeep
				,mpp.[TrayProduct]
				,CAST(mpp.[NestHigh] AS INT) AS NestHigh
				,CAST(mpp.[NestWide] AS INT) AS NestWide
				,CAST(mpp.[NestDeep] AS INT) AS NestDeep
				,mpp.[MerchType]
				,mpp.[FrontOnly]
				,mpp.[NewProducts]
				,mpp.[Blocking]
				,mpp.[BlockID]
				,mpp.[BrushStyle]
				,mpp.[MinDrop]
				,mpp.[RemoveCount]
				,CAST(mpp.[CapRightWide] AS INT) AS CapRightWide
				,CAST(mpp.[CapRightDeep] AS INT) AS CapRightDeep
				,CAST(mpp.[CapRightCapTopHigh] AS INT) AS CapRightCapTopHigh
				,CAST(mpp.[CapRightCapTopDeep] AS INT) AS CapRightCapTopDeep
				,CAST(mpp.[CapRightWideTrayCount] AS INT) AS CapRightWideTrayCount
				,CAST(mpp.[CapRightDeepTrayCount] AS INT) AS CapRightDeepTrayCount
				,mpp.[MultiSited]
				,mpp.[CasePack]
				,mpp.[Colour]
				,mpp.[IsSequenced]
				,mpp.[LastChance]
				,mpp.[AddLocked]
				,mpp.[RemoveLocked]
				,mpp.[StripNumber]
				,mpp.[Text1]
				,mpp.[Text2]
				,mpp.[Text3]
				,mpp.[Text4]
				,mpp.[Text5]
				,mpp.[Number1]
				,mpp.[Number2]
				,mpp.[Number3]
				,mpp.[Number4]
				,mpp.[Number5]
				,mpp.[BreakTrayUp]
				,mpp.[BreakTrayDown]
				,mpp.[BreakTrayBack]
				,mpp.[BreakTrayTop]
				,mpp.[IsFlexPreservedSpace]
				,mpp.[AttachmentTempFile]
				,mpp.[AttachmentName]
				,mpp.[AttachmentBaseElement]
				,mpp.[HidePreserved]
				,mpp.[MaxRightCap]
				,mpp.[MiddleCapping]
				,mpp.[MaxNestHigh]
				,mpp.[MaxNestDeep]
				,CAST(mpp.[MinDeep] AS INT) AS MinDeep
				,CAST(mpp.[MaxDeep] AS INT) AS MaxDeep
				,mpp.[PegDepth]
				,mpp.[PegX]
				,mpp.[PegY]
				,mpp.[PegZ]
				,mpp.[PegStyle]
				,mp.Created
				,mp.Modified
				,mp.PersonnelId
				,mp.PersonnelFlexi 
				,mp.CompanyId 
			FROM 
				[dbo].[tblManualPlanPosition] mpp 
			INNER JOIN 
				[dbo].[tblManualPlan] mp ON mp.[ManualPlanID] = mpp.[ManualPlanID]
			WHERE 
				mp.ManualPlanID = @PlanId";
        #endregion Manual

        #region Merchandising
        public const String MerchandisingByPlanId =
            @"SELECT tpp.[TemplatePlanID] AS PlanId
				,tpp.[TemplatePlanPositionID] AS Id
				,tpp.[TemplatePlanElementID] AS PlanElementId
				,tpp.[ProductID]
				,tpp.[OrientationID]
				,tpp.[Code]
				,CAST(tpp.[XPosition] AS REAL) AS XPosition
				,CAST(tpp.[YPosition] AS REAL) AS YPosition
				,CAST(tpp.[Deep] AS INT) AS Deep
				,CAST(tpp.[High] AS INT) AS High
				,CAST(tpp.[Wide] AS INT) AS Wide
				,CAST(tpp.[Units] AS INT) AS Units
				,tpp.[ProductHeight]
				,tpp.[ProductDepth]
				,tpp.[ProductWidth]
				,CAST(tpp.[SqueezeHeight] AS REAL) AS SqueezeHeight
				,CAST(tpp.[SqueezeWidth] AS REAL) AS SqueezeWidth
				,CAST(tpp.[SqueezeDepth] AS REAL) AS SqueezeDepth
				,CAST(tpp.[BlockHeight] AS REAL) AS BlockHeight
				,CAST(tpp.[BlockTop] AS REAL) AS BlockTop
				,CAST(tpp.[BlockWidth] AS REAL) AS BlockWidth
				,CAST(tpp.[BlockDepth] AS REAL) AS BlockDepth
				,CAST(tpp.[NestHeight] AS REAL) AS NestHeight
				,CAST(tpp.[NestWidth] AS REAL) AS NestWidth
				,CAST(tpp.[NestDepth] AS REAL) AS NestDepth
				,CAST(tpp.[LeadingDivider] AS REAL) AS LeadingDivider
				,CAST(tpp.[LeadingGap] AS REAL) AS LeadingGap
				,CAST(tpp.[LeadingGapVert] AS REAL) AS LeadingGapVert
				,CAST(tpp.[CapTopDeep] AS INT) AS CapTopDeep
				,CAST(tpp.[CapTopHigh] AS INT) AS CapTopHigh
				,CAST(tpp.[CapBotDeep] AS INT) AS CapBotDeep
				,CAST(tpp.[CapBotHigh] AS INT) AS CapBotHigh
				,CAST(tpp.[CapLftDeep] AS INT) AS CapLftDeep
				,CAST(tpp.[CapLftWide] AS INT) AS CapLftWide
				,CAST(tpp.[CapRgtDeep] AS INT) AS CapRgtDeep
				,CAST(tpp.[CapRgtWide] AS INT) AS CapRgtWide
				,tpp.[TargetSC]
				,CAST(tpp.[MaxCap] AS INT) AS MaxCap
				,CAST(tpp.[MaxStack] AS INT) AS MaxStack
				,CAST(tpp.[TrayHigh] AS INT) AS TrayHigh
				,CAST(tpp.[TrayWide] AS INT) AS TrayWide
				,CAST(tpp.[TrayDeep] AS INT) AS TrayDeep
				,CAST(tpp.[TrayHeight] AS FLOAT) AS TrayHeight
								
				,CAST(tpp.[TrayWidth] AS FLOAT) AS TrayWidth
				,CAST(tpp.[TrayDepth] AS FLOAT) AS TrayDepth
				,CAST(tpp.[TrayUnits] AS INT) AS TrayUnits						
								
				,CAST(tpp.[TrayThickHeight] AS REAL) AS TrayThickHeight
				,CAST(tpp.[TrayThickWidth] AS REAL) AS TrayThickWidth
				,CAST(tpp.[TrayThickDepth] AS REAL) AS TrayThickDepth
				,CAST(tpp.[TrayCountHigh] AS INT) AS TrayCountHigh
				,CAST(tpp.[TrayCountWide] AS INT) AS TrayCountWide
				,CAST(tpp.[TrayCountDeep] AS INT) AS TrayCountDeep
				,tpp.[TrayProduct]
				,CAST(tpp.[NestHigh] AS INT) AS NestHigh
				,CAST(tpp.[NestWide] AS INT) AS NestWide
				,CAST(tpp.[NestDeep] AS INT) AS NestDeep
				,tpp.[MerchType]
				,tpp.[FrontOnly]
				,tpp.[NewProducts]
				,tpp.[Blocking]
				,tpp.[BlockID]
				,tpp.[BrushStyle]
				,tpp.[MinDrop]
				,tpp.[RemoveCount]
				,CAST(tpp.[CapRightWide] AS INT) AS CapRightWide
				,CAST(tpp.[CapRightDeep] AS INT) AS CapRightDeep
				,CAST(tpp.[CapRightCapTopHigh] AS INT) AS CapRightCapTopHigh
				,CAST(tpp.[CapRightCapTopDeep] AS INT) AS CapRightCapTopDeep
				,CAST(tpp.[CapRightWideTrayCount] AS INT) AS CapRightWideTrayCount
				,CAST(tpp.[CapRightDeepTrayCount] AS INT) AS CapRightDeepTrayCount
				,tpp.[MultiSited]
				,tpp.[CasePack]
				,tpp.[Colour]
				,tpp.[IsSequenced]
				,tpp.[LastChance]
				,tpp.[AddLocked]
				,tpp.[RemoveLocked]
				,tpp.[StripNumber]
				,tpp.[Text1]
				,tpp.[Text2]
				,tpp.[Text3]
				,tpp.[Text4]
				,tpp.[Text5]
				,tpp.[Number1]
				,tpp.[Number2]
				,tpp.[Number3]
				,tpp.[Number4]
				,tpp.[Number5]
				,tpp.[BreakTrayUp]
				,tpp.[BreakTrayDown]
				,tpp.[BreakTrayBack]				
				,tpp.[BreakTrayTop]
				,tpp.[IsFlexPreservedSpace]
				,tpp.[AttachmentTempFile]
				,tpp.[AttachmentName]
				,tpp.[AttachmentBaseElement]
				,tpp.[HidePreserved]
				,tpp.[MaxRightCap]
				,tpp.[MiddleCapping]
				,tpp.[MaxNestHigh]
				,tpp.[MaxNestDeep]
				,CAST(tpp.[MinDeep] AS INT) AS MinDeep
				,CAST(tpp.[MaxDeep] AS INT) AS MaxDeep
				,tpp.[PegDepth]
				,tpp.[PegX]
				,tpp.[PegY]
				,tpp.[PegZ]
				,tpp.[PegStyle]
				,tp.Created
				,tp.Modified
				,tp.PersonnelId
				,tp.PersonnelFlexi 
				,tp.CompanyId 
			FROM 
				[dbo].[tblTemplatePlanPosition] tpp 
			INNER JOIN 
				[dbo].[tblTemplatePlan] tp ON tp.[TemplatePlanID] = tpp.[TemplatePlanID]
			WHERE 
				tp.TemplatePlanID = @PlanId";
        #endregion Merchandising

        #region Store
        public const String FetchStoreByPlanId =
            @"SELECT spp.[StorePlanID] AS PlanId
				,spp.[StoreID]
				,spp.[StorePlanPositionID] AS Id
				,spp.[StorePlanElementID] AS PlanElementId
				,spp.[ProductID]
				,spp.[OrientationID]
				,CAST(spp.[XPosition] AS REAL) AS XPosition
				,CAST(spp.[YPosition] AS REAL) AS YPosition
				,CAST(spp.[Deep] AS INT) AS Deep
				,CAST(spp.[High] AS INT) AS High
				,CAST(spp.[Wide] AS INT) AS Wide
				,spp.[ProductHeight]
				,spp.[ProductWidth]
				,spp.[ProductDepth]
				,CAST(spp.[Units] AS INT) AS Units
				,CAST(spp.[SqueezeHeight] AS REAL) AS SqueezeHeight
				,CAST(spp.[SqueezeWidth] AS REAL) AS SqueezeWidth
				,CAST(spp.[SqueezeDepth] AS REAL) AS SqueezeDepth
				,CAST(spp.[BlockHeight] AS REAL) AS BlockHeight
				,CAST(spp.[BlockTop] AS REAL) AS BlockTop
				,CAST(spp.[BlockWidth] AS REAL) AS BlockWidth
				,CAST(spp.[BlockDepth] AS REAL) AS BlockDepth
				,CAST(spp.[NestHeight] AS REAL) AS NestHeight
				,CAST(spp.[NestWidth] AS REAL) AS NestWidth
				,CAST(spp.[NestDepth] AS REAL) AS NestDepth
				,CAST(spp.[LeadingDivider] AS REAL) AS LeadingDivider
				,CAST(spp.[LeadingGap] AS REAL) AS LeadingGap
				,CAST(spp.[LeadingGapVert] AS REAL) AS LeadingGapVert
				,CAST(spp.[CapTopDeep] AS INT) AS CapTopDeep
				,CAST(spp.[CapTopHigh] AS INT) AS CapTopHigh
				,CAST(spp.[CapBotDeep] AS INT) AS CapBotDeep
				,CAST(spp.[CapBotHigh] AS INT) AS CapBotHigh
				,CAST(spp.[CapLftDeep] AS INT) AS CapLftDeep
				,CAST(spp.[CapLftWide] AS INT) AS CapLftWide
				,CAST(spp.[CapRgtDeep] AS INT) AS CapRgtDeep
				,CAST(spp.[CapRgtWide] AS INT) AS CapRgtWide
				,spp.[TargetSC]
				,CAST(spp.[MaxCap] AS INT) AS MaxCap
				,CAST(spp.[MaxStack] AS INT) AS MaxStack
				,CAST(spp.[MinDeep] AS INT) AS MinDeep
				,CAST(spp.[MaxDeep] AS INT) AS MaxDeep
				,CAST(spp.[TrayHigh] AS INT) AS TrayHigh
				,CAST(spp.[TrayWide] AS INT) AS TrayWide
				,CAST(spp.[TrayDeep] AS INT) AS TrayDeep
				,CAST(spp.[TrayHeight] AS FLOAT) AS TrayHeight
				
				,CAST(spp.[TrayWidth] AS FLOAT) AS TrayWidth
				,CAST(spp.[TrayDepth] AS FLOAT) AS TrayDepth
				,CAST(spp.[TrayUnits] AS INT) AS TrayUnits
				
				,CAST(spp.[TrayThickHeight] AS REAL) AS TrayThickHeight
				,CAST(spp.[TrayThickWidth] AS REAL) AS TrayThickWidth
				,CAST(spp.[TrayThickDepth] AS REAL) AS TrayThickDepth
				,CAST(spp.[TrayCountHigh] AS INT) AS TrayCountHigh
				,CAST(spp.[TrayCountWide] AS INT) AS TrayCountWide
				,CAST(spp.[TrayCountDeep] AS INT) AS TrayCountDeep
				,spp.[TrayProduct]
				,CAST(spp.[NestHigh] AS INT) AS NestHigh
				,CAST(spp.[NestWide] AS INT) AS NestWide
				,CAST(spp.[NestDeep] AS INT) AS NestDeep
				,spp.[MerchType]
				,spp.[Blocking]
				,spp.[BlockID]
				,spp.[BrushStyle]
				,spp.[MinDrop]
				,spp.[RemoveCount]
				,CAST(spp.[CapRightWide] AS INT) AS CapRightWide
				,CAST(spp.[CapRightDeep] AS INT) AS CapRightDeep
				,CAST(spp.[CapRightCapTopHigh] AS INT) AS CapRightCapTopHigh
				,CAST(spp.[CapRightCapTopDeep] AS INT) AS CapRightCapTopDeep				
				,CAST(spp.[CapRightWideTrayCount] AS INT) AS CapRightWideTrayCount
				,CAST(spp.[CapRightDeepTrayCount] AS INT) AS CapRightDeepTrayCount
				,spp.[MultiSited]
				,spp.[CasePack]
				,spp.[Colour]
				,spp.[AddLocked]
				,spp.[RemoveLocked]
				,spp.[StripNumber]
				,spp.[LastChance]
				,spp.[PegFix]
				,spp.[PegShift]
				,spp.[ElementPositionID]
				,spp.[HistoricalDailyUnits]
				,spp.[ActualDaysSupplied]
				,spp.[BreakTrayUp]
				,spp.[BreakTrayDown]
				,spp.[BreakTrayBack]				
				,spp.[BreakTrayTop]
				,spp.[FrontOnly]
				,spp.[MiddleCapping]
				,spp.[BlockTagName]
				,spp.[GlobalRank]
				,spp.[FinalMin]
				,spp.[FinalMax]
				,spp.[MinPriority1]
				,spp.[MinPriority2]
				,spp.[MinPriority3]
				,spp.[MinPriority4]
				,spp.[MinPriority5]
				,spp.[RecommendedInventory]
				,spp.[CompromiseInventory]
				,spp.[AverageWeeklySales]
				,spp.[AverageWeeklyUnits]
				,spp.[AverageWeeklyProfit]
				,spp.[AchievedCases]
				,spp.[AchievedDOS]
				,spp.[PerformanceValue01]
				,spp.[PerformanceValue02]
				,spp.[PerformanceValue03]
				,spp.[PerformanceValue04]
				,spp.[PerformanceValue05]
				,spp.[PerformanceValue06]
				,spp.[PerformanceValue07]
				,spp.[PerformanceValue08]
				,spp.[PerformanceValue09]
				,spp.[PerformanceValue10]
				,spp.[MaxNestHigh]
				,spp.[MaxNestDeep]
				,spp.[PegDepth]
				,spp.[ListingPSI]
				,spp.[ExposurePSI]
				,spp.[PegX]
				,spp.[PegY]
				,spp.[PegProng]
				,spp.[MaxRightCap]
				,spp.[CanBeRefaced]
				,spp.[AchievedInventoryLevelType]
				,spp.[MaxPriority1]
				,spp.[MaxPriority2]
				,spp.[MaxPriority3]
				,spp.[MaxPriority4]
				,spp.[MaxPriority5]
				,sp.Created
				,sp.Modified
				,sp.PersonnelId
				,sp.PersonnelFlexi 
				,sp.CompanyId 
			FROM 
				[dbo].[tblStorePlanPosition] spp 
			INNER JOIN 
				[dbo].[tblStorePlan] sp ON sp.[StorePlanId] = spp.[StorePlanId]
			WHERE 
				spp.StorePlanId = @PlanId";
        #endregion Store

    }
}
