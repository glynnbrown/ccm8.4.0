﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Schema
{
    internal class ProcedureNames
    {

        #region Fixture Plan

        public const String FixutrePlan_FetchAll = "usp_Publishing_FixutrePlan_FetchAll";
        public const String FixutrePlan_FetchById = "usp_Publishing_FixutrePlan_FetchById";

        #endregion

        #region Fixture Plan Bay

        public const String FixturePlanBay_FetchByFixturePlanId = "usp_Publishing_FixturePlanBay_FetchByFixturePlanId";

        #endregion

        #region Plan

        //public const String Plan_FetchAll = "usp_Plan_FetchAll";
        public const String Plan_FetchAllManual = "usp_Plan_FetchAllManual";
        public const String Plan_FetchAllMerchandising = "usp_Plan_FetchAllMerchandising";
        public const String Plan_FetchAllCluster = "usp_Plan_FetchAllCluster";
        public const String Plan_FetchAllStore = "usp_Plan_FetchAllStore";
        public const String Plan_FetchMerchandisingByPlanId = "usp_Plan_FetchAllMerchandisingByPlanId";
        public const String Plan_FetchClusterByPlanId = "usp_Plan_FetchAllClusterByPlanId";
        public const String Plan_FetchStoreByPlanId = "usp_Plan_FetchAllStoreByPlanId";
        public const String Plan_FetchManualByPlanId = "usp_Plan_FetchAllManualByPlanId";
        public const String Plan_Update = "usp_Plan_Update";
        #endregion

        #region PlanBay

        //public const String PlanBay_FetchByPlanId = "usp_PlanBay_FetchByPlanId";
        public const String PlanBay_FetchManualByPlanId = "usp_PlanBay_FetchManualByPlanId";
        public const String PlanBay_FetchMerchandisingByPlanId = "usp_PlanBay_FetchMerchandisingByPlanId";
        public const String PlanBay_FetchClusterByPlanId = "usp_PlanBay_FetchClusterByPlanId";
        public const String PlanBay_FetchStoreByPlanId = "usp_PlanBay_FetchStoreByPlanId";

        #endregion

        #region PlanElement

        //public const String PlanElement_FetchByPlanId = "usp_PlanElement_FetchByPlanId";
        public const String PlanElement_FetchManualByPlanId = "usp_PlanElement_FetchManualByPlanId";
        public const String PlanElement_FetchMerchandisingByPlanId = "usp_PlanElement_FetchMerchandisingByPlanId";
        public const String PlanElement_FetchClusterByPlanId = "usp_PlanElement_FetchClusterByPlanId";
        public const String PlanElement_FetchStoreByPlanId = "usp_PlanElement_FetchStoreByPlanId";

        #endregion

        #region PlanPosition

        //public const String PlanPosition_FetchByPlanId = "usp_PlanPosition_FetchByPlanId";
        public const String PlanPosition_FetchManualByPlanId = "usp_PlanPosition_FetchManualByPlanId";
        public const String PlanPosition_FetchMerchandisingByPlanId = "usp_PlanPosition_FetchMerchandisingByPlanId";
        public const String PlanPosition_FetchClusterByPlanId = "usp_PlanPosition_FetchClusterByPlanId";
        public const String PlanPosition_FetchStoreByPlanId = "usp_PlanPosition_FetchStoreByPlanId";

        #endregion

        #region PlanTextBox

        //public const String PlanTextBox_FetchByPlanId = "usp_PlanTextBox_FetchByPlanId";
        public const String PlanTextBox_FetchManualByPlanId = "usp_PlanTextBox_FetchManualByPlanId";
        public const String PlanTextBox_FetchMerchandisingByPlanId = "usp_PlanTextBox_FetchMerchandisingByPlanId";
        public const String PlanTextBox_FetchClusterByPlanId = "usp_PlanTextBox_FetchClusterByPlanId";
        public const String PlanTextBox_FetchStoreByPlanId = "usp_PlanTextBox_FetchStoreByPlanId";

        #endregion

        #region Product

        public const String ProductFetchById = "usp_Sync_Product_FetchById";
        public const String ProductFetchByProductCodes = "usp_Sync_Product_FetchByProductCodes";
        public const String ProductFetchByProductIds = "Product_FetchByProductIds";

        #endregion
    }
}
