﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 3. It was improved the way the combined direction for planogram elements are calculated. 
//   - It was ammend 2 scrips (FetchClusterByPlanId and FetchStoreByPlanId) to check the plan elements bloking type. If the blocking type was equal to 10 then it was a element to be combine.
//   - In the split plan it was added an addition step to re-calculate plan element combine direction based on the OriginalTemplateElementId.
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.Schema
{
    /// Defines constants for all fields within the database
    /// </summary>
    internal class FieldNames
    {
        #region Fixture Plan

        public const String FixturePlan_Id = "FixturePlan_Id";
        public const String FixturePlan_Name = "FixturePlan_Name";
        public const String FixturePlan_ProductLevelId = "FixturePlan_ProductLevelId";
        public const String FixturePlan_SourceFile = "FixturePlan_SourceFile";
        public const String FixturePlan_Blocking = "FixturePlan_Blocking";
        public const String FixturePlan_MerchShelfSpace = "FixturePlan_MerchShelfSpace";
        public const String FixturePlan_MerchPegSpace = "FixturePlan_MerchPegSpace";
        public const String FixturePlan_MerchBarSpace = "FixturePlan_MerchBarSpace";
        public const String FixturePlan_MerchChestSpace = "FixturePlan_MerchChestSpace";
        public const String FixturePlan_Number1 = "FixturePlan_Number1";
        public const String FixturePlan_Number2 = "FixturePlan_Number2";
        public const String FixturePlan_Number3 = "FixturePlan_Number3";
        public const String FixturePlan_Number4 = "FixturePlan_Number4";
        public const String FixturePlan_Number5 = "FixturePlan_Number5";
        public const String FixturePlan_Text1 = "FixturePlan_Text1";
        public const String FixturePlan_Text2 = "FixturePlan_Text2";
        public const String FixturePlan_Text3 = "FixturePlan_Text3";
        public const String FixturePlan_Text4 = "FixturePlan_Text4";
        public const String FixturePlan_Text5 = "FixturePlan_Text5";
        public const String FixturePlan_ProductLvelFlexi = "FixturePlan_ProductLvelFlexi";
        public const String FixturePlan_BayWidthValues = "FixturePlan_BayWidthValues";
        public const String FixturePlan_PresentationTotalWidth = "FixturePlan_PresentationTotalWidth";
        public const String FixturePlan_PresentationBayWidth = "FixturePlan_PresentationBayWidth";
        public const String FixturePlan_MaxHeight = "FixturePlan_MaxHeight";
        public const String FixturePlan_PresentationMaxHeight = "FixturePlan_PresentationMaxHeight";
        public const String FixturePlan_CustomCalculationValue = "FixturePlan_CustomCalculationValue";
        public const String FixturePlan_CustomPostFix = "FixturePlan_CustomPostFix";
        public const String FixturePlan_TotalWidth = "FixturePlan_TotalWidth";
        public const String FixturePlan_BayCount = "FixturePlan_BayCount";
        public const String FixturePlan_Created = "FixturePlan_Created";
        public const String FixturePlan_Modified = "FixturePlan_Modified";
        public const String FixturePlan_PersonnelId = "FixturePlan_PersonnelId";
        public const String FixturePlan_PersonnelFlexi = "FixturePlan_PersonnelFlexi";
        public const String FixturePlan_CompanyId = "FixturePlan_CompanyId";

        #endregion

        #region Fixture Plan Bay

        public const String FixturePlanBay_Id = "FixturePlanBay_Id";
        public const String FixturePlanBay_FixturePlanId = "FixturePlanBay_FixturePlanId";
        public const String FixturePlanBay_BayNumber = "FixturePlanBay_BayNumber";
        public const String FixturePlanBay_Name = "FixturePlanBay_Name";
        public const String FixturePlanBay_XPosition = "FixturePlanBay_XPosition";
        public const String FixturePlanBay_BaseDepth = "FixturePlanBay_BaseDepth";
        public const String FixturePlanBay_BaseHeight = "FixturePlanBay_BaseHeight";
        public const String FixturePlanBay_BaseWidth = "FixturePlanBay_BaseWidth";
        public const String FixturePlanBay_Height = "FixturePlanBay_Height";
        public const String FixturePlanBay_Width = "FixturePlanBay_Width";
        public const String FixturePlanBay_Depth = "FixturePlanBay_Depth";
        public const String FixturePlanBay_Text1 = "FixturePlanBay_Text1";
        public const String FixturePlanBay_Text2 = "FixturePlanBay_Text2";
        public const String FixturePlanBay_Text3 = "FixturePlanBay_Text3";
        public const String FixturePlanBay_Text4 = "FixturePlanBay_Text4";
        public const String FixturePlanBay_Text5 = "FixturePlanBay_Text5";
        public const String FixturePlanBay_Number1 = "FixturePlanBay_Number1";
        public const String FixturePlanBay_Number2 = "FixturePlanBay_Number2";
        public const String FixturePlanBay_Number3 = "FixturePlanBay_Number3";
        public const String FixturePlanBay_Number4 = "FixturePlanBay_Number4";
        public const String FixturePlanBay_Number5 = "FixturePlanBay_Number5";
        public const String FixturePlanBay_Created = "FixturePlanBay_Created";
        public const String FixturePlanBay_Modified = "FixturePlanBay_Modified";
        public const String FixturePlanBay_PersonnelId = "FixturePlanBay_PersonnelId";
        public const String FixturePlanBay_PersonnelFlexi = "FixturePlanBay_PersonnelFlexi";
        public const String FixturePlanBay_CompanyId = "FixturePlanBay_CompanyId";
        #endregion

        #region Plan

        #region BasePlan
        public const String Plan_CategoryReviewName = "CategoryReviewName";
        public const String Plan_Id = "PlanID";
        public const String Plan_UniqueContentReference = "UniqueContentReference";
        public const String Plan_Type = "PlanType";
        public const String Plan_Name = "PlanName";
        public const String Plan_UniqueItemName = "UniqueItemName";
        public const String Plan_StoreId = "StoreId";
        public const String Plan_StoreFlexi = "StoreFlexi";
        public const String Plan_StoreCode = "StoreCode";
        public const String Plan_ProductLevelId = "ProductLevelId";
        public const String Plan_MerchandisingGroupFlexi = "MerchandisingGroupFlexi";
        public const String Plan_CreatedDate = "CreatedDate";
        public const String Plan_IssuedDate = "IssuedDate";
        public const String Plan_ValidDate = "ValidDate";
        public const String Plan_RangingDate = "RangingDate";
        public const String Plan_PublishingDate = "PublishingDate";
        public const String Plan_Deleted = "Deleted";
        public const String Plan_Issued = "Issued";
        public const String Plan_Failed = "Failed";
        public const String Plan_MerchShelfSpace = "MerchShelfSpace";
        public const String Plan_MerchPegSpace = "MerchPegSpace";
        public const String Plan_MerchBarSpace = "MerchBarSpace";
        public const String Plan_MerchChestSpace = "MerchChestSpace";
        public const String Plan_Number1 = "Number1";
        public const String Plan_Number2 = "Number2";
        public const String Plan_Number3 = "Number3";
        public const String Plan_Number4 = "Number4";
        public const String Plan_Number5 = "Number5";
        public const String Plan_Text1 = "Text1";
        public const String Plan_Text2 = "Text1";
        public const String Plan_Text3 = "Text1";
        public const String Plan_Text4 = "Text1";
        public const String Plan_Text5 = "Text5";
        public const String Plan_Text6 = "Text6";
        public const String Plan_Text7 = "Text7";
        public const String Plan_Text8 = "Text8";
        public const String Plan_Text9 = "Text9";
        public const String Plan_Text10 = "Text10";
        public const String Plan_ElementPresentation = "ElementPresentation";
        public const String Plan_NestedMerchHeight = "NestedMerchHeight";
        public const String Plan_FingerSpace = "FingerSpace";
        public const String Plan_BreakMerchHeight = "BreakMerchHeight";
        public const String Plan_BreakMerchDepth = "BreakMerchDepth";
        public const String Plan_CategoryReviewId = "CategoryReviewId";
        public const String Plan_ParentCategoryReviewId = "ParentCategoryReviewId";
        public const String Plan_Created = "Created";
        public const String Plan_Modified = "Modified";
        public const String Plan_PersonnelId = "PersonnelId";
        public const String Plan_PersonnelFlexi = "PersonnelFlexi";
        public const String Plan_CompanyId = "CompanyId";
        #endregion

        #region Manual Plan
        public const String Plan_MerchandisingGroupId = "MerchandisingGroupId";
        public const String Plan_SourceFile = "SourceFile";
        public const String Plan_PlanDescription1 = "PlanDescription1";
        public const String Plan_PlanDescription2 = "PlanDescription2";
        public const String Plan_PlanDescription3 = "PlanDescription3";
        public const String Plan_PlanDescription4 = "PlanDescription4";
        public const String Plan_PlanDescription5 = "PlanDescription5";
        #endregion

        #region Space Plan
        public const String Plan_ClusterLevelId = "ClusterLevelId";
        public const String Plan_TemplatePlanId = "TemplatePlanId";
        public const String Plan_ClusterAssortmentId = "ClusterAssortmentId";
        public const String Plan_FixturePlanId = "FixturePlanId";
        public const String Plan_ScenarioId = "ScenarioId";
        public const String Plan_ReviewDate = "ReviewDate";
        public const String Plan_Islive = "Islive";
        public const String Plan_HasEngineReport = "HasEngineReport";
        public const String Plan_RenderType = "RenderType";
        public const String Plan_OverrideBlockSpace = "OverrideBlockSpace";
        public const String Plan_WorkpackageName = "WorkpackageName";
        public const String Plan_AssortmentTriangleSetting = "AssortmentTriangleSetting";
        public const String Plan_FlexPreservationAllowed = "FlexPreservationAllowed";
        public const String Plan_PercentCompromise = "PercentCompromise";
        public const String Plan_PercentDelistedProducts = "PercentDelistedProducts";
        public const String Plan_PercentForceClusterFacingLevels = "PercentForceClusterFacingLevels";
        public const String Plan_PercentClusterParticipationListing = "PercentClusterParticipationListing";
        public const String Plan_ProductsPlacedCount = "ProductsPlacedCount";
        public const String Plan_ProductsUnplacedCount = "ProductsUnplacedCount";
        public const String Plan_ProductsNotRecommendedCount = "ProductsNotRecommendedCount";
        public const String Plan_AuditValue1 = "AuditValue1";
        public const String Plan_AuditValue2 = "AuditValue2";
        public const String Plan_AuditValue3 = "AuditValue3";
        public const String Plan_AuditValue4 = "AuditValue4";
        public const String Plan_AuditValue5 = "AuditValue5";
        public const String Plan_AuditValue6 = "AuditValue6";
        public const String Plan_AuditValue7 = "AuditValue7";
        public const String Plan_AuditValue8 = "AuditValue8";
        public const String Plan_AuditValue9 = "AuditValue9";
        public const String Plan_AuditValue10 = "AuditValue10";
        public const String Plan_AuditValue11 = "AuditValue11";
        public const String Plan_AuditValue12 = "AuditValue12";
        public const String Plan_AuditValue13 = "AuditValue13";
        public const String Plan_AuditValue14 = "AuditValue14";
        public const String Plan_AuditValue15 = "AuditValue15";
        public const String Plan_ActionCount = "ActionCount";
        public const String Plan_ActionsAppliedCount = "ActionsAppliedCount";
        public const String Plan_ProductsAddedCount = "ProductsAddedCount";
        public const String Plan_ProductsRemovedCount = "ProductsRemovedCount";
        public const String Plan_ProductsMovedCount = "ProductsMovedCount";
        public const String Plan_ProductsReplacedCount = "ProductsReplacedCount";
        public const String Plan_ProductsIncreasedSpaceCount = "ProductsIncreasedSpaceCount";
        public const String Plan_ProductsDecreasedSpaceCount = "ProductsDecreasedSpaceCount";
        public const String Plan_AutomaticIncreasedSpaceCount = "AutomaticIncreasedSpaceCount";
        public const String Plan_AutomaticDecreasedSpaceCount = "AutomaticDecreasedSpaceCount";
        public const String Plan_SupplyabilityRemovedCount = "SupplyabilityRemovedCount";
        public const String Plan_PercentageOfActionsNotAchieved = "PercentageOfActionsNotAchieved";
        public const String Plan_PercentageOfChangeOnPlan = "PercentageOfChangeOnPlan";
        public const String Plan_PercentageOfChangeFromOriginal = "PercentageOfChangeFromOriginal";
        public const String Plan_ProfileId = "ProfileId";
        public const String Plan_ProfileName = "ProfileName";
        public const String Plan_PlaceholderProductCount = "PlaceholderProductCount";
        public const String Plan_ReplacementProductCount = "ReplacementProductCount";
        public const String Plan_PlaceholderProductPlacedCount = "PlaceholderProductPlacedCount";
        public const String Plan_ReplacementProductPlacedCount = "ReplacementProductPlacedCount";
        public const String Plan_ProductCountChangeOnPlan = "ProductCountChangeOnPlan";
        public const String Plan_PercentageDeviationFromBlocking = "PercentageDeviationFromBlocking";
        public const String Plan_PercentageWhiteSpace = "PercentageWhiteSpace";
        public const String Plan_BlocksDroppedCount = "BlocksDroppedCount";
        public const String Plan_ProductsNotAchievedInventoryCount = "ProductsNotAchievedInventoryCount";
        public const String Plan_ProductsMovedFromTemplateCount = "ProductsMovedFromTemplateCount";
        public const String Plan_AttributesChangedCount = "AttributesChangedCount";
        public const String Plan_AttributesRemovedCount = "AttributesRemovedCount";

        #endregion

        #endregion

        #region PlanBay

        #region BasePlan
        public const String PlanBay_Id = "Id";
        public const String PlanBay_PlanId = "PlanId";
        public const String PlanBay_Name = "Name";
        public const String PlanBay_XPosition = "XPosition";
        public const String PlanBay_BaseDepth = "BaseDepth";
        public const String PlanBay_BaseHeight = "BaseHeight";
        public const String PlanBay_BaseWidth = "BaseWidth";
        public const String PlanBay_Height = "Height";
        public const String PlanBay_Width = "Width";
        public const String PlanBay_Depth = "Depth";
        public const String PlanBay_Text1 = "Text1";
        public const String PlanBay_Text2 = "Text2";
        public const String PlanBay_Text3 = "Text3";
        public const String PlanBay_Text4 = "Text4";
        public const String PlanBay_Text5 = "Text5";
        public const String PlanBay_Number1 = "Number1";
        public const String PlanBay_Number2 = "Number2";
        public const String PlanBay_Number3 = "Number3";
        public const String PlanBay_Number4 = "Number4";
        public const String PlanBay_Number5 = "Number5";
        public const String PlanBay_SourceSegments = "SourceSegments";
        public const String PlanBay_DestinationSegments = "DestinationSegments";
        public const String PlanBay_Created = "Created";
        public const String PlanBay_Modified = "Modified";
        public const String PlanBay_PersonnelId = "PersonnelId";
        public const String PlanBay_PersonnelFlexi = "PersonnelFlexi";
        public const String PlanBay_CompanyId = "CompanyId";

        #endregion

        #region Manual Plan
        public const String PlanBay_Segments = "Segments";
        #endregion

        #region Space Plan
        public const String PlanBay_StoreId = "StoreId";
        public const String PlanBay_FixtureId = "FixtureId";
        #endregion

        #endregion

        #region PlanElement

        #region Base Plan
        public const String PlanElement_Id = "Id";
        public const String PlanElement_PlanId = "PlanId";
        public const String PlanElement_PlanBayId = "PlanBayId";
        public const String PlanElement_Name = "Name";
        public const String PlanElement_Combined = "Combined";
        public const String PlanElement_CombineDirection = "CombineDirection";
        public const String PlanElement_XPosition = "XPosition";
        public const String PlanElement_YPosition = "YPosition";
        public const String PlanElement_ZPosition = "ZPosition";
        public const String PlanElement_ElementType = "ElementType";
        public const String PlanElement_ShelfHeight = "ShelfHeight";
        public const String PlanElement_ShelfWidth = "ShelfWidth";
        public const String PlanElement_ShelfDepth = "ShelfDepth";
        public const String PlanElement_ShelfThick = "ShelfThick";
        public const String PlanElement_ShelfSlope = "ShelfSlope";
        public const String PlanElement_ShelfRiser = "ShelfRiser";
        public const String PlanElement_PegHeight = "PegHeight";
        public const String PlanElement_PegWidth = "PegWidth";
        public const String PlanElement_PegVertSpace = "PegVertSpace";
        public const String PlanElement_PegHorzSpace = "PegHorzSpace";
        public const String PlanElement_PegVertStart = "PegVertStart";
        public const String PlanElement_PegHorzStart = "PegHorzStart";
        public const String PlanElement_PegNotchDistance = "PegNotchDistance";
        public const String PlanElement_PegDepth = "PegDepth";
        public const String PlanElement_ChestHeight = "ChestHeight";
        public const String PlanElement_ChestWidth = "ChestWidth";
        public const String PlanElement_ChestDepth = "ChestDepth";
        public const String PlanElement_ChestWall = "ChestWall";
        public const String PlanElement_ChestInside = "ChestInside";
        public const String PlanElement_ChestMerchandising = "ChestMerchandising";
        public const String PlanElement_ChestDivider = "ChestDivider";
        public const String PlanElement_ChestAbove = "ChestAbove";
        public const String PlanElement_BarHeight = "BarHeight";
        public const String PlanElement_BarDepth = "BarDepth";
        public const String PlanElement_BarWidth = "BarWidth";
        public const String PlanElement_BarThick = "BarThick";
        public const String PlanElement_BarDistance = "BarDistance";
        public const String PlanElement_BarHangerDepth = "BarHangerDepth";
        public const String PlanElement_BarNotch = "BarNotch";
        public const String PlanElement_BarHorzStart = "BarHorzStart";
        public const String PlanElement_BarHorzSpace = "BarHorzSpace";
        public const String PlanElement_BarVertStart = "BarVertStart";
        public const String PlanElement_BarBackHeight = "BarBackHeight";
        public const String PlanElement_NonMerchandisable = "NonMerchandisable";
        public const String PlanElement_FingerSpace = "FingerSpace";
        public const String PlanElement_TopOverhang = "TopOverhang";
        public const String PlanElement_BottomOverhang = "BottomOverhang";
        public const String PlanElement_LeftOverhang = "LeftOverhang";
        public const String PlanElement_RightOverhang = "RightOverhang";
        public const String PlanElement_FrontOverhang = "FrontOverhang";
        public const String PlanElement_BackOverhang = "BackOverhang";
        public const String PlanElement_Number1 = "Number1";
        public const String PlanElement_Number2 = "Number2";
        public const String PlanElement_Number3 = "Number3";
        public const String PlanElement_Number4 = "Number4";
        public const String PlanElement_Number5 = "Number5";
        public const String PlanElement_Created = "Created";
        public const String PlanElement_Modified = "Modified";
        public const String PlanElement_PersonnelId = "PersonnelId";
        public const String PlanElement_PersonnelFlexi = "PersonnelFlexi";
        public const String PlanElement_CompanyId = "CompanyId";
        public const String PlanElement_ChestWallRenderType = "ChestWallRenderType";
        public const String PlanElement_OriginalTemplateElementId = "OriginalTemplateElementId";

        #endregion

        #region Manual Plan
        public const String PlanElement_Text1 = "Text1";
        public const String PlanElement_Text2 = "Text2";
        public const String PlanElement_Text3 = "Text3";
        public const String PlanElement_Text4 = "Text4";
        public const String PlanElement_Text5 = "Text5";
        #endregion

        #region Space Plan
        public const String PlanElement_StoreId = "StoreId";
        public const String PlanElement_EmptyPegs = "EmptyPegs";
        public const String PlanElement_RightEmptyPeg = "RightEmptyPeg";
        public const String PlanElement_ProductCount = "ProductCount";
        public const String PlanElement_PrimaryPegs = "PrimaryPegs";
        public const String PlanElement_StartX = "StartX";
        public const String PlanElement_MaxHeight = "MaxHeight";
        #endregion

        #endregion

        #region PlanPosition

        #region Base Plan
        public const String PlanPosition_Id = "Id";
        public const String PlanPosition_PlanId = "PlanId";
        public const String PlanPosition_PlanElementId = "PlanElementId";
        public const String PlanPosition_ProductId = "ProductId";
        public const String PlanPosition_OrientationId = "OrientationId";
        public const String PlanPosition_XPosition = "XPosition";
        public const String PlanPosition_YPosition = "YPosition";
        public const String PlanPosition_Deep = "Deep";
        public const String PlanPosition_High = "High";
        public const String PlanPosition_Wide = "Wide";
        public const String PlanPosition_ProductHeight = "ProductHeight";
        public const String PlanPosition_ProductWidth = "ProductWidth";
        public const String PlanPosition_ProductDepth = "ProductDepth";
        public const String PlanPosition_Units = "Units";
        public const String PlanPosition_SqueezeHeight = "SqueezeHeight";
        public const String PlanPosition_SqueezeWidth = "SqueezeWidth";
        public const String PlanPosition_SqueezeDepth = "SqueezeDepth";
        public const String PlanPosition_BlockHeight = "BlockHeight";
        public const String PlanPosition_BlockWidth = "BlockWidth";
        public const String PlanPosition_BlockDepth = "BlockDepth";
        public const String PlanPosition_NestHeight = "NestHeight";
        public const String PlanPosition_NestWidth = "NestWidth";
        public const String PlanPosition_NestDepth = "NestDepth";
        public const String PlanPosition_LeadingDivider = "LeadingDivider";
        public const String PlanPosition_LeadingGap = "LeadingGap";
        public const String PlanPosition_LeadingGapVert = "LeadingGapVert";
        public const String PlanPosition_CapTopDeep = "CapTopDeep";
        public const String PlanPosition_CapTopHigh = "CapTopHigh";
        public const String PlanPosition_CapBotDeep = "CapBotDeep";
        public const String PlanPosition_CapBotHigh = "CapBotHigh";
        public const String PlanPosition_CapLftDeep = "CapLftDeep";
        public const String PlanPosition_CapLftWide = "CapLftWide";
        public const String PlanPosition_CapRgtDeep = "CapRgtDeep";
        public const String PlanPosition_CapRgtWide = "CapRgtWide";
        public const String PlanPosition_TargetSC = "TargetSC";
        public const String PlanPosition_MaxCap = "MaxCap";
        public const String PlanPosition_MaxStack = "MaxStack";
        public const String PlanPosition_MinDeep = "MinDeep";
        public const String PlanPosition_MaxDeep = "MaxDeep";
        public const String PlanPosition_TrayHigh = "TrayHigh";
        public const String PlanPosition_TrayWide = "TrayWide";
        public const String PlanPosition_TrayDeep = "TrayDeep";
        public const String PlanPosition_TrayHeight = "TrayHeight";//CCM:21084
        public const String PlanPosition_TrayWidth = "TrayWidth";//CCM:21084
        public const String PlanPosition_TrayDepth = "TrayDepth";//CCM:21084
        public const String PlanPosition_TrayUnits = "TrayUnits";//CCM:21084
        public const String PlanPosition_TrayThickHeight = "TrayThickHeight";
        public const String PlanPosition_TrayThickWidth = "TrayThickWidth";
        public const String PlanPosition_TrayThickDepth = "TrayThickDepth";
        public const String PlanPosition_TrayCountHigh = "TrayCountHigh";
        public const String PlanPosition_TrayCountWide = "TrayCountWide";
        public const String PlanPosition_TrayCountDeep = "TrayCountDeep";
        public const String PlanPosition_TrayProduct = "TrayProduct";
        public const String PlanPosition_NestHigh = "NestHigh";
        public const String PlanPosition_NestWide = "NestWide";
        public const String PlanPosition_NestDeep = "NestDeep";
        public const String PlanPosition_MerchType = "MerchType";
        public const String PlanPosition_Blocking = "Blocking";
        public const String PlanPosition_BlockId = "BlockId";
        public const String PlanPosition_BrushStyle = "BrushStyle";
        public const String PlanPosition_MinDrop = "MinDrop";
        public const String PlanPosition_RemoveCount = "RemoveCount";
        public const String PlanPosition_CapRightWide = "CapRightWide";
        public const String PlanPosition_CapRightDeep = "CapRightDeep";
        public const String PlanPosition_CapRightWideTrayCount = "CapRightWideTrayCount";
        public const String PlanPosition_CapRightDeepTrayCount = "CapRightDeepTrayCount";
        public const String PlanPosition_CapRightCapTopHigh = "CapRightCapTopHigh";
        public const String PlanPosition_CapRightCapTopDeep = "CapRightCapTopDeep";
        public const String PlanPosition_MultiSited = "MultiSited";
        public const String PlanPosition_CasePack = "CasePack";
        public const String PlanPosition_Colour = "Colour";
        public const String PlanPosition_LastChance = "LastChance";
        public const String PlanPosition_AddLocked = "AddLocked";
        public const String PlanPosition_RemoveLocked = "RemoveLocked";
        public const String PlanPosition_StripNumber = "StripNumber";
        public const String PlanPosition_BreakTrayUp = "BreakTrayUp";
        public const String PlanPosition_BreakTrayDown = "BreakTrayDown";
        public const String PlanPosition_BreakTrayBack = "BreakTrayBack";
        public const String PlanPosition_BreakTrayTop = "BreakTrayTop";
        public const String PlanPosition_FrontOnly = "FrontOnly";
        public const String PlanPosition_MiddleCapping = "MiddleCapping";
        public const String PlanPosition_MaxNestHigh = "MaxNestHigh";
        public const String PlanPosition_MaxNestDeep = "MaxNestDeep";
        public const String PlanPosition_PegDepth = "PegDepth";
        public const String PlanPosition_PegX = "PegX";
        public const String PlanPosition_PegY = "PegY";
        public const String PlanPosition_MaxRightCap = "MaxRightCap";
        public const String PlanPosition_Created = "Created";
        public const String PlanPosition_Modified = "Modified";
        public const String PlanPosition_PersonnelId = "PersonnelId";
        public const String PlanPosition_PersonnelFlexi = "PersonnelFlexi";
        public const String PlanPosition_CompanyId = "CompanyId";

        #endregion

        #region Manual Plan
        public const String PlanPosition_Code = "Code";
        public const String PlanPosition_PegZ = "PegZ";
        public const String PlanPosition_PegStyle = "PegStyle";
        public const String PlanPosition_IsSequenced = "IsSequenced";
        public const String PlanPosition_NewProducts = "NewProducts";
        public const String PlanPosition_Number1 = "Number1";
        public const String PlanPosition_Number2 = "Number2";
        public const String PlanPosition_Number3 = "Number3";
        public const String PlanPosition_Number4 = "Number4";
        public const String PlanPosition_Number5 = "Number5";
        public const String PlanPosition_Text1 = "Text1";
        public const String PlanPosition_Text2 = "Text2";
        public const String PlanPosition_Text3 = "Text3";
        public const String PlanPosition_Text4 = "Text4";
        public const String PlanPosition_Text5 = "Text5";
        public const String PlanPosition_IsFlexPreservedSpace = "IsFlexPreservedSpace";
        public const String PlanPosition_HidePreserved = "HidePreserved";
        public const String PlanPosition_AttachmentTempFile = "AttachmentTempFile";
        public const String PlanPosition_AttachmentName = "AttachmentName";
        public const String PlanPosition_AttachmentBaseElement = "AttachmentBaseElement";

        #endregion

        #region Space Plan
        public const String PlanPosition_StoreId = "StoreId";
        public const String PlanPosition_PegFix = "PegFix";
        public const String PlanPosition_PegShift = "PegShift";
        public const String PlanPosition_ElementPositionId = "ElementPositionId";
        public const String PlanPosition_HistoricalDailyUnits = "HistoricalDailyUnits";
        public const String PlanPosition_ActualDaysSupplied = "ActualDaysSupplied";
        public const String PlanPosition_BlockTagName = "BlockTagName";
        public const String PlanPosition_GlobalRank = "GlobalRank";
        public const String PlanPosition_FinalMin = "FinalMin";
        public const String PlanPosition_FinalMax = "FinalMax";
        public const String PlanPosition_MinPriority1 = "MinPriority1";
        public const String PlanPosition_MinPriority2 = "MinPriority2";
        public const String PlanPosition_MinPriority3 = "MinPriority3";
        public const String PlanPosition_MinPriority4 = "MinPriority4";
        public const String PlanPosition_MinPriority5 = "MinPriority5";
        public const String PlanPosition_MaxPriority1 = "MaxPriority1";
        public const String PlanPosition_MaxPriority2 = "MaxPriority2";
        public const String PlanPosition_MaxPriority3 = "MaxPriority3";
        public const String PlanPosition_MaxPriority4 = "MaxPriority4";
        public const String PlanPosition_MaxPriority5 = "MaxPriority5";
        public const String PlanPosition_RecommendedInventory = "RecommendedInventory";
        public const String PlanPosition_CompromiseInventory = "CompromiseInventory";
        public const String PlanPosition_AverageWeeklySales = "AverageWeeklySales";
        public const String PlanPosition_AverageWeeklyUnits = "AverageWeeklyUnits";
        public const String PlanPosition_AverageWeeklyProfit = "AverageWeeklyProfit";
        public const String PlanPosition_AchievedCases = "AchievedCases";
        public const String PlanPosition_AchievedDOS = "AchievedDOS";
        public const String PlanPosition_PerformanceValue01 = "PerformanceValue01";
        public const String PlanPosition_PerformanceValue02 = "PerformanceValue02";
        public const String PlanPosition_PerformanceValue03 = "PerformanceValue03";
        public const String PlanPosition_PerformanceValue04 = "PerformanceValue04";
        public const String PlanPosition_PerformanceValue05 = "PerformanceValue05";
        public const String PlanPosition_PerformanceValue06 = "PerformanceValue06";
        public const String PlanPosition_PerformanceValue07 = "PerformanceValue07";
        public const String PlanPosition_PerformanceValue08 = "PerformanceValue08";
        public const String PlanPosition_PerformanceValue09 = "PerformanceValue09";
        public const String PlanPosition_PerformanceValue10 = "PerformanceValue10";
        public const String PlanPosition_ListingPSI = "ListingPSI";
        public const String PlanPosition_ExposurePSI = "ExposurePSI";
        public const String PlanPosition_PegProng = "PegProng";
        public const String PlanPosition_CanBeRefaced = "CanBeRefaced";
        public const String PlanPosition_AchievedInventoryLevelType = "AchievedInventoryLevelType";

        #endregion

        #endregion

        #region PlanTextBox

        public const String PlanTextBox_Id = "Id";
        public const String PlanTextBox_PlanId = "PlanID";
        public const String PlanTextBox_PlanBayId = "PlanBayId";
        public const String PlanTextBox_PlanElementId = "PlanElementID";
        public const String PlanTextBox_XPosition = "XPosition";
        public const String PlanTextBox_YPosition = "YPosition";
        public const String PlanTextBox_ZPosition = "ZPosition";
        public const String PlanTextBox_Height = "Height";
        public const String PlanTextBox_Width = "Width";
        public const String PlanTextBox_Depth = "Depth";
        public const String PlanTextBox_TextboxType = "TextboxType";
        public const String PlanTextBox_TextboxDescription = "TextboxDescription";
        public const String PlanTextBox_FixturePlanBayNumber = "FixturePlanBayNumber";
        #endregion

        #region Product

        public const String ProductId = "ProductId";
        public const String ProductProductLevel10Id = "ProductLevel10Id";
        public const String ProductCode = "Code";
        public const String ProductSubCode = "SubCode";
        public const String ProductFormattedCode = "FormattedCode";
        public const String ProductName = "Name";
        public const String ProductODSProductReference = "ODSProductReference";
        public const String ProductHeight = "Height";
        public const String ProductWidth = "Width";
        public const String ProductDepth = "Depth";
        public const String ProductSqueezeHeight = "SqueezeHeight";
        public const String ProductSqueezeWidth = "SqueezeWidth";
        public const String ProductSqueezeDepth = "SqueezeDepth";
        public const String ProductTrayHigh = "TrayHigh";
        public const String ProductTrayWide = "TrayWide";
        public const String ProductTrayDeep = "TrayDeep";
        public const String ProductTrayThickHeight = "TrayThickHeight";
        public const String ProductTrayThickWidth = "TrayThickWidth";
        public const String ProductTrayThickDepth = "TrayThickDepth";
        public const String ProductTrayHeight = "TrayHeight"; //CCM-21084
        public const String ProductTrayWidth = "TrayWidth"; //CCM-21084
        public const String ProductTrayDepth = "TrayDepth"; //CCM-21084
        public const String ProductTraypackUnits = "TrayUnits"; //CCM-21084
        public const String ProductMaxCap = "MaxCap";
        public const String ProductMaxStack = "MaxStack";
        public const String ProductMinDeep = "MinDeep";
        public const String ProductMaxDeep = "MaxDeep";
        public const String ProductCasepackUnits = "CasepackUnits";
        public const String ProductFrontOnly = "FrontOnly";
        public const String ProductTrayProduct = "TrayProduct";
        public const String ProductMerchType = "MerchType";
        public const String ProductBarOverHang = "BarOverHang";
        public const String ProductNestHeight = "NestHeight";
        public const String ProductNestWidth = "NestWidth";
        public const String ProductNestDepth = "NestDepth";
        public const String ProductNestingHeight = "NestingHeight";
        public const String ProductNestingWidth = "NestingWidth";
        public const String ProductNestingDepth = "NestingDepth";
        public const String ProductLeftNestHeight = "LeftNestHeight";
        public const String ProductLeftNestWidth = "LeftNestWidth";
        public const String ProductLeftNestTopOffSet = "LeftNestTopOffSet";
        public const String ProductRightNestHeight = "RightNestHeight";
        public const String ProductRightNestWidth = "RightNestWidth";
        public const String ProductRightNestTopOffSet = "RightNestTopOffSet";
        public const String ProductPegX = "PegX";
        public const String ProductPegX2 = "PegX2";
        public const String ProductPegY = "PegY";
        public const String ProductPegY2 = "PegY2";
        public const String ProductPegProng = "PegProng";
        public const String ProductPegDepth = "PegDepth";
        public const String ProductBuddyAggregationType = "BuddyAggregationType";
        public const String ProductBuddyMultiplier11 = "BuddyMultiplier1_1";
        public const String ProductBuddyMultiplier12 = "BuddyMultiplier1_2";
        public const String ProductBuddyMultiplier13 = "BuddyMultiplier1_3";
        public const String ProductProductLevel10Flexi = "ProductLevel10Flexi";
        public const String ProductText1 = "Text1";
        public const String ProductText2 = "Text2";
        public const String ProductText3 = "Text3";
        public const String ProductText4 = "Text4";
        public const String ProductText5 = "Text5";
        public const String ProductText6 = "Text6";
        public const String ProductText7 = "Text7";
        public const String ProductText8 = "Text8";
        public const String ProductText9 = "Text9";
        public const String ProductText10 = "Text10";
        public const String ProductText11 = "Text11";
        public const String ProductText12 = "Text12";
        public const String ProductText13 = "Text13";
        public const String ProductText14 = "Text14";
        public const String ProductText15 = "Text15";
        public const String ProductText16 = "Text16";
        public const String ProductText17 = "Text17";
        public const String ProductText18 = "Text18";
        public const String ProductText19 = "Text19";
        public const String ProductText20 = "Text20";
        public const String ProductNumber1 = "Number1";
        public const String ProductNumber2 = "Number2";
        public const String ProductNumber3 = "Number3";
        public const String ProductNumber4 = "Number4";
        public const String ProductNumber5 = "Number5";
        public const String ProductNumber6 = "Number6";
        public const String ProductNumber7 = "Number7";
        public const String ProductNumber8 = "Number8";
        public const String ProductNumber9 = "Number9";
        public const String ProductNumber10 = "Number10";
        public const String ProductNumber11 = "Number11";
        public const String ProductNumber12 = "Number12";
        public const String ProductNumber13 = "Number13";
        public const String ProductNumber14 = "Number14";
        public const String ProductNumber15 = "Number15";
        public const String ProductNumber16 = "Number16";
        public const String ProductNumber17 = "Number17";
        public const String ProductNumber18 = "Number18";
        public const String ProductNumber19 = "Number19";
        public const String ProductNumber20 = "Number20";
        public const String ProductBreakTrayUp = "BreakTrayUp";
        public const String ProductBreakTrayBack = "BreakTrayBack";
        public const String ProductBreaktrayDown = "BreaktrayDown";
        public const String ProductMaxRightCap = "MaxRightCap";
        public const String ProductMaxNestHigh = "MaxNestHigh";
        public const String ProductMaxNestDeep = "MaxNestDeep";
        public const String ProductCreated = "Created";
        public const String ProductModified = "Modified";
        public const String ProductCanMiddleCap = "CanMiddleCap";
        public const String ProductIsNew = "IsNew";
        public const String ProductDeletedDate = "DeletedDate";
        public const String ProductIsPlaceholder = "IsPlaceholder";

        #endregion

        #region Profile
        public const String Profile_Id = "Id";
        public const String Profile_PlanID = "PlanId";
        public const String Profile_ProfileDetailID = "ProfileDetailID";
        public const String Profile_ProfileGroupType = "ProfileGroupType";
        public const String Profile_Name = "Name";
        public const String Profile_IsActive = "IsActive";
        public const String Profile_Priority = "Priority";
        public const String Profile_Ratio = "Ratio";
        public const String Profile_Percentage = "Percentage";
        public const String Profile_Minimum = "Minimum";
        public const String Profile_Maximum = "Maximum";
        public const String Profile_RatioLowest = "RatioLowest";
        public const String Profile_RatioHighest = "RatioHighest";
        public const String Profile_KeyField = "KeyField";
        public const String Profile_ProfileGroupValue = "ProfileGroupValue";
        #endregion Profile

        #region Assortment
        public const String Assortment_ProductId = "ProductId";
        public const String Assortment_ProductCode = "Code";
        public const String Assortment_ProductFormattedCode = "FormattedCode";
        public const String Assortment_ProductName = "Name";
        public const String Assortment_Recommended = "Recommended";
        public const String Assortment_GlobalRank = "GlobalRank";
        public const String Assortment_RecomendedUnits = "RecomendedUnits";
        public const String Assortment_RecomendedFacings = "RecomendedFacings";
        public const String Assortment_Segmentation = "Segmentation";
        public const String Assortment_Rule1Type = "Rule1Type";
        public const String Assortment_RuleValue1 = "RuleValue1";
        public const String Assortment_Rule2Type = "Rule2Type";
        public const String Assortment_RuleValue2 = "RuleValue2";
        public const String Assortment_IsRuleDropped = "IsRuleDropped";
        public const String Assortment_ViolationType = "ViolationType";

        #endregion
    }
}
