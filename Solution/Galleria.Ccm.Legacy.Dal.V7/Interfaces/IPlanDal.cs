﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.V7.Interfaces
{
    public interface IPlanDal : IDal
    {
        List<PlanDto> FetchAll();
        List<ManualPlanDto> FetchAllManual();
        List<ManualPlanDto> FetchAllMerchandising();
        List<SpacePlanDto> FetchAllStore();
        List<SpacePlanDto> FetchAllCluster();

        ManualPlanDto FetchManualByPlanId(Int32 PlanId);
        ManualPlanDto FetchMerchandisingByPlanId(Int32 PlanId);
        SpacePlanDto FetchStoreByPlanId(Int32 PlanId);
        SpacePlanDto FetchClusterByPlanId(Int32 PlanId);

        void Update(PlanDto dto);

    }
}
