﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.V7.Interfaces
{
    public interface IPlanProcessDal : IDal
    {
        List<Int32> FetchManualPlanIds(String[] categoryReviewIds);
        List<Int32> FetchStorePlanIds(String[] categoryReviewIds);
        List<Int32> FetchClusterPlanIds(String[] categoryReviewIds);
        List<Int32> FetchMerchandisingPlanIds(String[] categoryReviewIds);
        Single GetDbVersion();
        void FixOriginalTemplateElementIds();
    }
}
