﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.V7.Interfaces
{
    public interface IPlanBayDal : IDal
    {
        List<PlanBayDto> FetchByPlanId(Int32 planId);
        List<ManualPlanBayDto> FetchManualByPlanId(Int32 planId);
        List<ManualPlanBayDto> FetchMerchandisingByPlanId(Int32 planId);
        List<SpacePlanBayDto> FetchStoreByPlanId(Int32 planId);
        List<SpacePlanBayDto> FetchClusterByPlanId(Int32 planId);
    }
}
