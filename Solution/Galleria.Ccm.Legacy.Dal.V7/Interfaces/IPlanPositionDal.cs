﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.V7.Interfaces
{
    public interface IPlanPositionDal : IDal
    {
        List<PlanPositionDto> FetchByPlanId(Int32 planId);
        List<ManualPlanPositionDto> FetchManualByPlanId(Int32 planId);
        List<ManualPlanPositionDto> FetchMerchandisingByPlanId(Int32 planId);
        List<SpacePlanPositionDto> FetchStoreByPlanId(Int32 planId);
        List<SpacePlanPositionDto> FetchClusterByPlanId(Int32 planId);
    }
}
