﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.V7.Interfaces
{
    public interface IPlanProfileDal : IDal
    {
        List<PlanProfileDto> FetchByPlanId(Int32 planId);
        List<PlanProfileDto> FetchManualByPlanId(Int32 planId);
        List<PlanProfileDto> FetchMerchandisingByPlanId(Int32 planId);
        List<PlanProfileDto> FetchStoreByPlanId(Int32 planId);
        List<PlanProfileDto> FetchClusterByPlanId(Int32 planId);
    }
}
