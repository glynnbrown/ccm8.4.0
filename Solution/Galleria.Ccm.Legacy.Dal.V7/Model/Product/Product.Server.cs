﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class Product
    {
        #region Constructor
        public Product() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the specified Product
        /// </summary>
        /// <param name="id">The unique Product id</param>
        public static Product GetProductById(int id)
        {
            return DataPortal.Fetch<Product>(new SingleCriteria<Product, int>(id));
        }

        /// <summary>
        /// Returns an existing Product object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A Product object</returns>
        internal static Product GetProduct(IDalContext dalContext, ProductDto dto)
        {
            return DataPortal.FetchChild<Product>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Objects

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ProductDto GetDataTransferObject()
        {
            return new ProductDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                ProductLevel10Id = ReadProperty<Int32?>(ProductLevel10IdProperty),
                Code = ReadProperty<String>(CodeProperty),
                SubCode = ReadProperty<String>(SubCodeProperty),
                FormattedCode = ReadProperty<String>(FormattedCodeProperty),
                Name = ReadProperty<String>(NameProperty),
                ODSProductReference = ReadProperty<String>(ODSProductReferenceProperty),
                Height = ReadProperty<float>(HeightProperty),
                Width = ReadProperty<float>(WidthProperty),
                Depth = ReadProperty<float>(DepthProperty),
                SqueezeHeight = ReadProperty<float>(SqueezeHeightProperty),
                SqueezeWidth = ReadProperty<float>(SqueezeWidthProperty),
                SqueezeDepth = ReadProperty<float>(SqueezeDepthProperty),
                TrayHigh = ReadProperty<float>(TrayHighProperty),
                TrayWide = ReadProperty<float>(TrayWideProperty),
                TrayDeep = ReadProperty<float>(TrayDeepProperty),
                TrayThickHeight = ReadProperty<float>(TrayThickHeightProperty),
                TrayThickWidth = ReadProperty<float>(TrayThickWidthProperty),
                TrayThickDepth = ReadProperty<float>(TrayThickDepthProperty),
                TrayHeight = ReadProperty<float>(TrayHeightProperty), //CCM-21084
                TrayWidth = ReadProperty<float>(TrayWidthProperty), //CCM-21084
                TrayDepth = ReadProperty<float>(TrayDepthProperty), //CCM-21084
                TraypackUnits = ReadProperty<Int16>(TraypackUnitsProperty), //CCM-21084
                MaxCap = ReadProperty<Int32>(MaxCapProperty),
                MaxStack = ReadProperty<Int32>(MaxStackProperty),
                MinDeep = ReadProperty<Int32>(MinDeepProperty),
                MaxDeep = ReadProperty<Int32>(MaxDeepProperty),
                CasepackUnits = ReadProperty<Int16>(CasepackUnitsProperty),
                FrontOnly = ReadProperty<Byte>(FrontOnlyProperty),
                TrayProduct = ReadProperty<Byte>(TrayProductProperty),
                MerchType = ReadProperty<Int16>(MerchTypeProperty),
                BarOverHang = ReadProperty<Int32?>(BarOverHangProperty),
                NestHeight = ReadProperty<float?>(NestHeightProperty),
                NestWidth = ReadProperty<float?>(NestWidthProperty),
                NestDepth = ReadProperty<float?>(NestDepthProperty),
                NestingHeight = ReadProperty<float?>(NestingHeightProperty),
                NestingWidth = ReadProperty<float?>(NestingWidthProperty),
                NestingDepth = ReadProperty<float?>(NestingDepthProperty),
                LeftNestHeight = ReadProperty<float?>(LeftNestHeightProperty),
                LeftNestWidth = ReadProperty<float?>(LeftNestWidthProperty),
                LeftNestTopOffSet = ReadProperty<float?>(LeftNestTopOffSetProperty),
                RightNestHeight = ReadProperty<float?>(RightNestHeightProperty),
                RightNestWidth = ReadProperty<float?>(RightNestWidthProperty),
                RightNestTopOffSet = ReadProperty<float?>(RightNestTopOffSetProperty),
                PegX = ReadProperty<float>(PegXProperty),
                PegX2 = ReadProperty<float?>(PegX2Property),
                PegY = ReadProperty<float>(PegYProperty),
                PegY2 = ReadProperty<float?>(PegY2Property),
                PegProng = ReadProperty<float?>(PegProngProperty),
                PegDepth = ReadProperty<Single>(PegDepthProperty),
                BuddyAggregationType = ReadProperty<String>(BuddyAggregationTypeProperty),
                BuddyMultiplier11 = ReadProperty<float>(BuddyMultiplier11Property),
                BuddyMultiplier12 = ReadProperty<float>(BuddyMultiplier12Property),
                BuddyMultiplier13 = ReadProperty<float>(BuddyMultiplier13Property),
                ProductLevel10Flexi = ReadProperty<String>(ProductLevel10FlexiProperty),
                Text1 = ReadProperty<String>(Text1Property),
                Text2 = ReadProperty<String>(Text2Property),
                Text3 = ReadProperty<String>(Text3Property),
                Text4 = ReadProperty<String>(Text4Property),
                Text5 = ReadProperty<String>(Text5Property),
                Text6 = ReadProperty<String>(Text6Property),
                Text7 = ReadProperty<String>(Text7Property),
                Text8 = ReadProperty<String>(Text8Property),
                Text9 = ReadProperty<String>(Text9Property),
                Text10 = ReadProperty<String>(Text10Property),
                Text11 = ReadProperty<String>(Text11Property),
                Text12 = ReadProperty<String>(Text12Property),
                Text13 = ReadProperty<String>(Text13Property),
                Text14 = ReadProperty<String>(Text14Property),
                Text15 = ReadProperty<String>(Text15Property),
                Text16 = ReadProperty<String>(Text16Property),
                Text17 = ReadProperty<String>(Text17Property),
                Text18 = ReadProperty<String>(Text18Property),
                Text19 = ReadProperty<String>(Text19Property),
                Text20 = ReadProperty<String>(Text20Property),
                Number1 = ReadProperty<float?>(Number1Property),
                Number2 = ReadProperty<float?>(Number2Property),
                Number3 = ReadProperty<float?>(Number3Property),
                Number4 = ReadProperty<float?>(Number4Property),
                Number5 = ReadProperty<float?>(Number5Property),
                Number6 = ReadProperty<float?>(Number6Property),
                Number7 = ReadProperty<float?>(Number7Property),
                Number8 = ReadProperty<float?>(Number8Property),
                Number9 = ReadProperty<float?>(Number9Property),
                Number10 = ReadProperty<float?>(Number10Property),
                Number11 = ReadProperty<Int32?>(Number11Property),
                Number12 = ReadProperty<Int32?>(Number12Property),
                Number13 = ReadProperty<Int32?>(Number13Property),
                Number14 = ReadProperty<Int32?>(Number14Property),
                Number15 = ReadProperty<Int32?>(Number15Property),
                Number16 = ReadProperty<Int32?>(Number16Property),
                Number17 = ReadProperty<Int32?>(Number17Property),
                Number18 = ReadProperty<Int32?>(Number18Property),
                Number19 = ReadProperty<Int32?>(Number19Property),
                Number20 = ReadProperty<Int32?>(Number20Property),
                BreakTrayUp = ReadProperty<Byte?>(BreakTrayUpProperty),
                BreakTrayBack = ReadProperty<Byte?>(BreakTrayBackProperty),
                BreaktrayDown = ReadProperty<Byte?>(BreaktrayDownProperty),
                MaxRightCap = ReadProperty<Int32>(MaxRightCapProperty),
                MaxNestHigh = ReadProperty<Int32>(MaxNestHighProperty),
                MaxNestDeep = ReadProperty<Int32>(MaxNestDeepProperty),
                Created = ReadProperty<DateTime>(CreatedProperty),
                Modified = ReadProperty<DateTime>(ModifiedProperty),
                CanMiddleCap = ReadProperty<Boolean>(CanMiddleCapProperty),
                IsNew = ReadProperty<Boolean>(IsNewProperty),
                DeletedDate = ReadProperty<DateTime?>(DeletedDateProperty),
                IsPlaceholder = ReadProperty<Boolean>(IsPlaceholderProperty),

            };
        }

        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, ProductDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32?>(ProductLevel10IdProperty, dto.ProductLevel10Id);
            LoadProperty<String>(CodeProperty, dto.Code);
            LoadProperty<String>(SubCodeProperty, dto.SubCode);
            LoadProperty<String>(FormattedCodeProperty, dto.FormattedCode);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(ODSProductReferenceProperty, dto.ODSProductReference);
            LoadProperty<float>(HeightProperty, dto.Height);
            LoadProperty<float>(WidthProperty, dto.Width);
            LoadProperty<float>(DepthProperty, dto.Depth);
            LoadProperty<float>(SqueezeHeightProperty, dto.SqueezeHeight);
            LoadProperty<float>(SqueezeWidthProperty, dto.SqueezeWidth);
            LoadProperty<float>(SqueezeDepthProperty, dto.SqueezeDepth);
            LoadProperty<float>(TrayHighProperty, dto.TrayHigh);
            LoadProperty<float>(TrayWideProperty, dto.TrayWide);
            LoadProperty<float>(TrayDeepProperty, dto.TrayDeep);
            LoadProperty<float>(TrayThickHeightProperty, dto.TrayThickHeight);
            LoadProperty<float>(TrayThickWidthProperty, dto.TrayThickWidth);
            LoadProperty<float>(TrayThickDepthProperty, dto.TrayThickDepth);
            LoadProperty<float>(TrayHeightProperty, dto.TrayHeight); //CCM-21084
            LoadProperty<float>(TrayWidthProperty, dto.TrayWidth); //CCM-21084
            LoadProperty<float>(TrayDepthProperty, dto.TrayDepth); //CCM-21084
            LoadProperty<Int16>(TraypackUnitsProperty, dto.TraypackUnits); //CCM-21084
            LoadProperty<Int32>(MaxCapProperty, dto.MaxCap);
            LoadProperty<Int32>(MaxStackProperty, dto.MaxStack);
            LoadProperty<Int32>(MinDeepProperty, dto.MinDeep);
            LoadProperty<Int32>(MaxDeepProperty, dto.MaxDeep);
            LoadProperty<Int16>(CasepackUnitsProperty, dto.CasepackUnits);
            LoadProperty<Byte>(FrontOnlyProperty, dto.FrontOnly);
            LoadProperty<Byte>(TrayProductProperty, dto.TrayProduct);
            LoadProperty<Int16>(MerchTypeProperty, dto.MerchType);
            LoadProperty<Int32?>(BarOverHangProperty, dto.BarOverHang);
            LoadProperty<float?>(NestHeightProperty, dto.NestHeight);
            LoadProperty<float?>(NestWidthProperty, dto.NestWidth);
            LoadProperty<float?>(NestDepthProperty, dto.NestDepth);
            LoadProperty<float?>(NestingHeightProperty, dto.NestingHeight);
            LoadProperty<float?>(NestingWidthProperty, dto.NestingWidth);
            LoadProperty<float?>(NestingDepthProperty, dto.NestingDepth);
            LoadProperty<float?>(LeftNestHeightProperty, dto.LeftNestHeight);
            LoadProperty<float?>(LeftNestWidthProperty, dto.LeftNestWidth);
            LoadProperty<float?>(LeftNestTopOffSetProperty, dto.LeftNestTopOffSet);
            LoadProperty<float?>(RightNestHeightProperty, dto.RightNestHeight);
            LoadProperty<float?>(RightNestWidthProperty, dto.RightNestWidth);
            LoadProperty<float?>(RightNestTopOffSetProperty, dto.RightNestTopOffSet);
            LoadProperty<float>(PegXProperty, dto.PegX);
            LoadProperty<float?>(PegX2Property, dto.PegX2);
            LoadProperty<float>(PegYProperty, dto.PegY);
            LoadProperty<float?>(PegY2Property, dto.PegY2);
            LoadProperty<float?>(PegProngProperty, dto.PegProng);
            LoadProperty<Single>(PegDepthProperty, dto.PegDepth);
            LoadProperty<String>(BuddyAggregationTypeProperty, dto.BuddyAggregationType);
            LoadProperty<float>(BuddyMultiplier11Property, dto.BuddyMultiplier11);
            LoadProperty<float>(BuddyMultiplier12Property, dto.BuddyMultiplier12);
            LoadProperty<float>(BuddyMultiplier13Property, dto.BuddyMultiplier13);
            LoadProperty<String>(ProductLevel10FlexiProperty, dto.ProductLevel10Flexi);
            LoadProperty<String>(Text1Property, dto.Text1);
            LoadProperty<String>(Text2Property, dto.Text2);
            LoadProperty<String>(Text3Property, dto.Text3);
            LoadProperty<String>(Text4Property, dto.Text4);
            LoadProperty<String>(Text5Property, dto.Text5);
            LoadProperty<String>(Text6Property, dto.Text6);
            LoadProperty<String>(Text7Property, dto.Text7);
            LoadProperty<String>(Text8Property, dto.Text8);
            LoadProperty<String>(Text9Property, dto.Text9);
            LoadProperty<String>(Text10Property, dto.Text10);
            LoadProperty<String>(Text11Property, dto.Text11);
            LoadProperty<String>(Text12Property, dto.Text12);
            LoadProperty<String>(Text13Property, dto.Text13);
            LoadProperty<String>(Text14Property, dto.Text14);
            LoadProperty<String>(Text15Property, dto.Text15);
            LoadProperty<String>(Text16Property, dto.Text16);
            LoadProperty<String>(Text17Property, dto.Text17);
            LoadProperty<String>(Text18Property, dto.Text18);
            LoadProperty<String>(Text19Property, dto.Text19);
            LoadProperty<String>(Text20Property, dto.Text20);
            LoadProperty<float?>(Number1Property, dto.Number1);
            LoadProperty<float?>(Number2Property, dto.Number2);
            LoadProperty<float?>(Number3Property, dto.Number3);
            LoadProperty<float?>(Number4Property, dto.Number4);
            LoadProperty<float?>(Number5Property, dto.Number5);
            LoadProperty<float?>(Number6Property, dto.Number6);
            LoadProperty<float?>(Number7Property, dto.Number7);
            LoadProperty<float?>(Number8Property, dto.Number8);
            LoadProperty<float?>(Number9Property, dto.Number9);
            LoadProperty<float?>(Number10Property, dto.Number10);
            LoadProperty<Int32?>(Number11Property, dto.Number11);
            LoadProperty<Int32?>(Number12Property, dto.Number12);
            LoadProperty<Int32?>(Number13Property, dto.Number13);
            LoadProperty<Int32?>(Number14Property, dto.Number14);
            LoadProperty<Int32?>(Number15Property, dto.Number15);
            LoadProperty<Int32?>(Number16Property, dto.Number16);
            LoadProperty<Int32?>(Number17Property, dto.Number17);
            LoadProperty<Int32?>(Number18Property, dto.Number18);
            LoadProperty<Int32?>(Number19Property, dto.Number19);
            LoadProperty<Int32?>(Number20Property, dto.Number20);
            LoadProperty<Byte?>(BreakTrayUpProperty, dto.BreakTrayUp);
            LoadProperty<Byte?>(BreakTrayBackProperty, dto.BreakTrayBack);
            LoadProperty<Byte?>(BreaktrayDownProperty, dto.BreaktrayDown);
            LoadProperty<Int32>(MaxRightCapProperty, dto.MaxRightCap);
            LoadProperty<Int32>(MaxNestHighProperty, dto.MaxNestHigh);
            LoadProperty<Int32>(MaxNestDeepProperty, dto.MaxNestDeep);
            LoadProperty<DateTime>(CreatedProperty, dto.Created);
            LoadProperty<DateTime>(ModifiedProperty, dto.Modified);
            LoadProperty<Boolean>(CanMiddleCapProperty, dto.CanMiddleCap);
            LoadProperty<Boolean>(IsNewProperty, dto.IsNew);
            LoadProperty<DateTime?>(DeletedDateProperty, dto.DeletedDate);
            LoadProperty<Boolean>(IsPlaceholderProperty, dto.IsPlaceholder);

        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when returning an existing peer group
        /// </summary>
        /// <param name="criteria">The criteria used to load the peer group</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Product, int> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
