﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Model
{
    /// <summary>
    /// Represents a list holding Product objects
    /// (root object)
    /// </summary>
    [Serializable]
    public sealed partial class ProductList : ModelList<ProductList, Product>
    {
    }
}
