﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Linq;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;

using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    /// <summary>
    /// Model object representing a Product
    /// </summary>
    [Serializable]
    public partial class Product : ModelObject<Product>
    {
        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> ProductLevel10IdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductLevel10Id);
        public Int32? ProductLevel10Id
        {
            get { return GetProperty<Int32?>(ProductLevel10IdProperty); }
            set { SetProperty<Int32?>(ProductLevel10IdProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
            set { SetProperty<String>(CodeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> SubCodeProperty =
            RegisterModelProperty<String>(c => c.SubCode);
        public String SubCode
        {
            get { return GetProperty<String>(SubCodeProperty); }
            set { SetProperty<String>(SubCodeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> FormattedCodeProperty =
            RegisterModelProperty<String>(c => c.FormattedCode);
        public String FormattedCode
        {
            get { return GetProperty<String>(FormattedCodeProperty); }
            set { SetProperty<String>(FormattedCodeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> ODSProductReferenceProperty =
            RegisterModelProperty<String>(c => c.ODSProductReference);
        public String ODSProductReference
        {
            get { return GetProperty<String>(ODSProductReferenceProperty); }
            set { SetProperty<String>(ODSProductReferenceProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> HeightProperty =
            RegisterModelProperty<float>(c => c.Height);
        public float Height
        {
            get { return GetProperty<float>(HeightProperty); }
            set { SetProperty<float>(HeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> WidthProperty =
            RegisterModelProperty<float>(c => c.Width);
        public float Width
        {
            get { return GetProperty<float>(WidthProperty); }
            set { SetProperty<float>(WidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> DepthProperty =
            RegisterModelProperty<float>(c => c.Depth);
        public float Depth
        {
            get { return GetProperty<float>(DepthProperty); }
            set { SetProperty<float>(DepthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> SqueezeHeightProperty =
            RegisterModelProperty<float>(c => c.SqueezeHeight);
        public float SqueezeHeight
        {
            get { return GetProperty<float>(SqueezeHeightProperty); }
            set { SetProperty<float>(SqueezeHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> SqueezeWidthProperty =
            RegisterModelProperty<float>(c => c.SqueezeWidth);
        public float SqueezeWidth
        {
            get { return GetProperty<float>(SqueezeWidthProperty); }
            set { SetProperty<float>(SqueezeWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> SqueezeDepthProperty =
            RegisterModelProperty<float>(c => c.SqueezeDepth);
        public float SqueezeDepth
        {
            get { return GetProperty<float>(SqueezeDepthProperty); }
            set { SetProperty<float>(SqueezeDepthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> TrayHighProperty =
            RegisterModelProperty<float>(c => c.TrayHigh);
        public float TrayHigh
        {
            get { return GetProperty<float>(TrayHighProperty); }
            set { SetProperty<float>(TrayHighProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> TrayWideProperty =
            RegisterModelProperty<float>(c => c.TrayWide);
        public float TrayWide
        {
            get { return GetProperty<float>(TrayWideProperty); }
            set { SetProperty<float>(TrayWideProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> TrayDeepProperty =
            RegisterModelProperty<float>(c => c.TrayDeep);
        public float TrayDeep
        {
            get { return GetProperty<float>(TrayDeepProperty); }
            set { SetProperty<float>(TrayDeepProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> TrayThickHeightProperty =
            RegisterModelProperty<float>(c => c.TrayThickHeight);
        public float TrayThickHeight
        {
            get { return GetProperty<float>(TrayThickHeightProperty); }
            set { SetProperty<float>(TrayThickHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> TrayThickWidthProperty =
            RegisterModelProperty<float>(c => c.TrayThickWidth);
        public float TrayThickWidth
        {
            get { return GetProperty<float>(TrayThickWidthProperty); }
            set { SetProperty<float>(TrayThickWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> TrayThickDepthProperty =
            RegisterModelProperty<float>(c => c.TrayThickDepth);
        public float TrayThickDepth
        {
            get { return GetProperty<float>(TrayThickDepthProperty); }
            set { SetProperty<float>(TrayThickDepthProperty, value); }
        }


        //>>CCM-21084
        public static readonly ModelPropertyInfo<float> TrayHeightProperty =
            RegisterModelProperty<float>(c => c.TrayHeight);
        public float TrayHeight
        {
            get { return GetProperty<float>(TrayHeightProperty); }
            set { SetProperty<float>(TrayHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> TrayWidthProperty =
            RegisterModelProperty<float>(c => c.TrayWidth);
        public float TrayWidth
        {
            get { return GetProperty<float>(TrayWidthProperty); }
            set { SetProperty<float>(TrayWidthProperty, value); }
        }

        public static readonly ModelPropertyInfo<float> TrayDepthProperty =
            RegisterModelProperty<float>(c => c.TrayDepth);
        public float TrayDepth
        {
            get { return GetProperty<float>(TrayDepthProperty); }
            set { SetProperty<float>(TrayDepthProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int16> TraypackUnitsProperty =
            RegisterModelProperty<Int16>(c => c.TraypackUnits);
        public Int16 TraypackUnits
        {
            get { return GetProperty<Int16>(TraypackUnitsProperty); }
            set { SetProperty<Int16>(TraypackUnitsProperty, value); }
        }
        //<<CCM-21084


        public static readonly ModelPropertyInfo<Int32> MaxCapProperty =
            RegisterModelProperty<Int32>(c => c.MaxCap);
        public Int32 MaxCap
        {
            get { return GetProperty<Int32>(MaxCapProperty); }
            set { SetProperty<Int32>(MaxCapProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32> MaxStackProperty =
            RegisterModelProperty<Int32>(c => c.MaxStack);
        public Int32 MaxStack
        {
            get { return GetProperty<Int32>(MaxStackProperty); }
            set { SetProperty<Int32>(MaxStackProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32> MinDeepProperty =
            RegisterModelProperty<Int32>(c => c.MinDeep);
        public Int32 MinDeep
        {
            get { return GetProperty<Int32>(MinDeepProperty); }
            set { SetProperty<Int32>(MinDeepProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32> MaxDeepProperty =
            RegisterModelProperty<Int32>(c => c.MaxDeep);
        public Int32 MaxDeep
        {
            get { return GetProperty<Int32>(MaxDeepProperty); }
            set { SetProperty<Int32>(MaxDeepProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int16> CasepackUnitsProperty =
            RegisterModelProperty<Int16>(c => c.CasepackUnits);
        public Int16 CasepackUnits
        {
            get { return GetProperty<Int16>(CasepackUnitsProperty); }
            set { SetProperty<Int16>(CasepackUnitsProperty, value); }
        }


        public static readonly ModelPropertyInfo<Byte> FrontOnlyProperty =
            RegisterModelProperty<Byte>(c => c.FrontOnly);
        public Byte FrontOnly
        {
            get { return GetProperty<Byte>(FrontOnlyProperty); }
            set { SetProperty<Byte>(FrontOnlyProperty, value); }
        }


        public static readonly ModelPropertyInfo<Byte> TrayProductProperty =
            RegisterModelProperty<Byte>(c => c.TrayProduct);
        public Byte TrayProduct
        {
            get { return GetProperty<Byte>(TrayProductProperty); }
            set { SetProperty<Byte>(TrayProductProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int16> MerchTypeProperty =
            RegisterModelProperty<Int16>(c => c.MerchType);
        public Int16 MerchType
        {
            get { return GetProperty<Int16>(MerchTypeProperty); }
            set { SetProperty<Int16>(MerchTypeProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> BarOverHangProperty =
            RegisterModelProperty<Int32?>(c => c.BarOverHang);
        public Int32? BarOverHang
        {
            get { return GetProperty<Int32?>(BarOverHangProperty); }
            set { SetProperty<Int32?>(BarOverHangProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> NestHeightProperty =
            RegisterModelProperty<float?>(c => c.NestHeight);
        public float? NestHeight
        {
            get { return GetProperty<float?>(NestHeightProperty); }
            set { SetProperty<float?>(NestHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> NestWidthProperty =
            RegisterModelProperty<float?>(c => c.NestWidth);
        public float? NestWidth
        {
            get { return GetProperty<float?>(NestWidthProperty); }
            set { SetProperty<float?>(NestWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> NestDepthProperty =
            RegisterModelProperty<float?>(c => c.NestDepth);
        public float? NestDepth
        {
            get { return GetProperty<float?>(NestDepthProperty); }
            set { SetProperty<float?>(NestDepthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> NestingHeightProperty =
            RegisterModelProperty<float?>(c => c.NestingHeight);
        public float? NestingHeight
        {
            get { return GetProperty<float?>(NestingHeightProperty); }
            set { SetProperty<float?>(NestingHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> NestingWidthProperty =
            RegisterModelProperty<float?>(c => c.NestingWidth);
        public float? NestingWidth
        {
            get { return GetProperty<float?>(NestingWidthProperty); }
            set { SetProperty<float?>(NestingWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> NestingDepthProperty =
            RegisterModelProperty<float?>(c => c.NestingDepth);
        public float? NestingDepth
        {
            get { return GetProperty<float?>(NestingDepthProperty); }
            set { SetProperty<float?>(NestingDepthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> LeftNestHeightProperty =
            RegisterModelProperty<float?>(c => c.LeftNestHeight);
        public float? LeftNestHeight
        {
            get { return GetProperty<float?>(LeftNestHeightProperty); }
            set { SetProperty<float?>(LeftNestHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> LeftNestWidthProperty =
            RegisterModelProperty<float?>(c => c.LeftNestWidth);
        public float? LeftNestWidth
        {
            get { return GetProperty<float?>(LeftNestWidthProperty); }
            set { SetProperty<float?>(LeftNestWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> LeftNestTopOffSetProperty =
            RegisterModelProperty<float?>(c => c.LeftNestTopOffSet);
        public float? LeftNestTopOffSet
        {
            get { return GetProperty<float?>(LeftNestTopOffSetProperty); }
            set { SetProperty<float?>(LeftNestTopOffSetProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> RightNestHeightProperty =
            RegisterModelProperty<float?>(c => c.RightNestHeight);
        public float? RightNestHeight
        {
            get { return GetProperty<float?>(RightNestHeightProperty); }
            set { SetProperty<float?>(RightNestHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> RightNestWidthProperty =
            RegisterModelProperty<float?>(c => c.RightNestWidth);
        public float? RightNestWidth
        {
            get { return GetProperty<float?>(RightNestWidthProperty); }
            set { SetProperty<float?>(RightNestWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> RightNestTopOffSetProperty =
            RegisterModelProperty<float?>(c => c.RightNestTopOffSet);
        public float? RightNestTopOffSet
        {
            get { return GetProperty<float?>(RightNestTopOffSetProperty); }
            set { SetProperty<float?>(RightNestTopOffSetProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> PegXProperty =
            RegisterModelProperty<float>(c => c.PegX);
        public float PegX
        {
            get { return GetProperty<float>(PegXProperty); }
            set { SetProperty<float>(PegXProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> PegX2Property =
            RegisterModelProperty<float?>(c => c.PegX2);
        public float? PegX2
        {
            get { return GetProperty<float?>(PegX2Property); }
            set { SetProperty<float?>(PegX2Property, value); }
        }


        public static readonly ModelPropertyInfo<float> PegYProperty =
            RegisterModelProperty<float>(c => c.PegY);
        public float PegY
        {
            get { return GetProperty<float>(PegYProperty); }
            set { SetProperty<float>(PegYProperty, value); }
        }


        public static readonly ModelPropertyInfo<float?> PegY2Property =
            RegisterModelProperty<float?>(c => c.PegY2);
        public float? PegY2
        {
            get { return GetProperty<float?>(PegY2Property); }
            set { SetProperty<float?>(PegY2Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> PegProngProperty =
            RegisterModelProperty<float?>(c => c.PegProng);
        public float? PegProng
        {
            get { return GetProperty<float?>(PegProngProperty); }
            set { SetProperty<float?>(PegProngProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single> PegDepthProperty =
            RegisterModelProperty<Single>(c => c.PegDepth);
        public Single PegDepth
        {
            get { return GetProperty<Single>(PegDepthProperty); }
            set { SetProperty<Single>(PegDepthProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> BuddyAggregationTypeProperty =
            RegisterModelProperty<String>(c => c.BuddyAggregationType);
        public String BuddyAggregationType
        {
            get { return GetProperty<String>(BuddyAggregationTypeProperty); }
            set { SetProperty<String>(BuddyAggregationTypeProperty, value); }
        }


        public static readonly ModelPropertyInfo<float> BuddyMultiplier11Property =
            RegisterModelProperty<float>(c => c.BuddyMultiplier11);
        public float BuddyMultiplier11
        {
            get { return GetProperty<float>(BuddyMultiplier11Property); }
            set { SetProperty<float>(BuddyMultiplier11Property, value); }
        }


        public static readonly ModelPropertyInfo<float> BuddyMultiplier12Property =
            RegisterModelProperty<float>(c => c.BuddyMultiplier12);
        public float BuddyMultiplier12
        {
            get { return GetProperty<float>(BuddyMultiplier12Property); }
            set { SetProperty<float>(BuddyMultiplier12Property, value); }
        }


        public static readonly ModelPropertyInfo<float> BuddyMultiplier13Property =
            RegisterModelProperty<float>(c => c.BuddyMultiplier13);
        public float BuddyMultiplier13
        {
            get { return GetProperty<float>(BuddyMultiplier13Property); }
            set { SetProperty<float>(BuddyMultiplier13Property, value); }
        }


        public static readonly ModelPropertyInfo<String> ProductLevel10FlexiProperty =
            RegisterModelProperty<String>(c => c.ProductLevel10Flexi);
        public String ProductLevel10Flexi
        {
            get { return GetProperty<String>(ProductLevel10FlexiProperty); }
            set { SetProperty<String>(ProductLevel10FlexiProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> Text1Property =
            RegisterModelProperty<String>(c => c.Text1);
        public String Text1
        {
            get { return GetProperty<String>(Text1Property); }
            set { SetProperty<String>(Text1Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text2Property =
            RegisterModelProperty<String>(c => c.Text2);
        public String Text2
        {
            get { return GetProperty<String>(Text2Property); }
            set { SetProperty<String>(Text2Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text3Property =
            RegisterModelProperty<String>(c => c.Text3);
        public String Text3
        {
            get { return GetProperty<String>(Text3Property); }
            set { SetProperty<String>(Text3Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text4Property =
            RegisterModelProperty<String>(c => c.Text4);
        public String Text4
        {
            get { return GetProperty<String>(Text4Property); }
            set { SetProperty<String>(Text4Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text5Property =
            RegisterModelProperty<String>(c => c.Text5);
        public String Text5
        {
            get { return GetProperty<String>(Text5Property); }
            set { SetProperty<String>(Text5Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text6Property =
            RegisterModelProperty<String>(c => c.Text6);
        public String Text6
        {
            get { return GetProperty<String>(Text6Property); }
            set { SetProperty<String>(Text6Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text7Property =
            RegisterModelProperty<String>(c => c.Text7);
        public String Text7
        {
            get { return GetProperty<String>(Text7Property); }
            set { SetProperty<String>(Text7Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text8Property =
            RegisterModelProperty<String>(c => c.Text8);
        public String Text8
        {
            get { return GetProperty<String>(Text8Property); }
            set { SetProperty<String>(Text8Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text9Property =
            RegisterModelProperty<String>(c => c.Text9);
        public String Text9
        {
            get { return GetProperty<String>(Text9Property); }
            set { SetProperty<String>(Text9Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text10Property =
            RegisterModelProperty<String>(c => c.Text10);
        public String Text10
        {
            get { return GetProperty<String>(Text10Property); }
            set { SetProperty<String>(Text10Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text11Property =
            RegisterModelProperty<String>(c => c.Text11);
        public String Text11
        {
            get { return GetProperty<String>(Text11Property); }
            set { SetProperty<String>(Text11Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text12Property =
            RegisterModelProperty<String>(c => c.Text12);
        public String Text12
        {
            get { return GetProperty<String>(Text12Property); }
            set { SetProperty<String>(Text12Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text13Property =
            RegisterModelProperty<String>(c => c.Text13);
        public String Text13
        {
            get { return GetProperty<String>(Text13Property); }
            set { SetProperty<String>(Text13Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text14Property =
            RegisterModelProperty<String>(c => c.Text14);
        public String Text14
        {
            get { return GetProperty<String>(Text14Property); }
            set { SetProperty<String>(Text14Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text15Property =
            RegisterModelProperty<String>(c => c.Text15);
        public String Text15
        {
            get { return GetProperty<String>(Text15Property); }
            set { SetProperty<String>(Text15Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text16Property =
            RegisterModelProperty<String>(c => c.Text16);
        public String Text16
        {
            get { return GetProperty<String>(Text16Property); }
            set { SetProperty<String>(Text16Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text17Property =
            RegisterModelProperty<String>(c => c.Text17);
        public String Text17
        {
            get { return GetProperty<String>(Text17Property); }
            set { SetProperty<String>(Text17Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text18Property =
            RegisterModelProperty<String>(c => c.Text18);
        public String Text18
        {
            get { return GetProperty<String>(Text18Property); }
            set { SetProperty<String>(Text18Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text19Property =
            RegisterModelProperty<String>(c => c.Text19);
        public String Text19
        {
            get { return GetProperty<String>(Text19Property); }
            set { SetProperty<String>(Text19Property, value); }
        }


        public static readonly ModelPropertyInfo<String> Text20Property =
            RegisterModelProperty<String>(c => c.Text20);
        public String Text20
        {
            get { return GetProperty<String>(Text20Property); }
            set { SetProperty<String>(Text20Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number1Property =
            RegisterModelProperty<float?>(c => c.Number1);
        public float? Number1
        {
            get { return GetProperty<float?>(Number1Property); }
            set { SetProperty<float?>(Number1Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number2Property =
            RegisterModelProperty<float?>(c => c.Number2);
        public float? Number2
        {
            get { return GetProperty<float?>(Number2Property); }
            set { SetProperty<float?>(Number2Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number3Property =
            RegisterModelProperty<float?>(c => c.Number3);
        public float? Number3
        {
            get { return GetProperty<float?>(Number3Property); }
            set { SetProperty<float?>(Number3Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number4Property =
            RegisterModelProperty<float?>(c => c.Number4);
        public float? Number4
        {
            get { return GetProperty<float?>(Number4Property); }
            set { SetProperty<float?>(Number4Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number5Property =
            RegisterModelProperty<float?>(c => c.Number5);
        public float? Number5
        {
            get { return GetProperty<float?>(Number5Property); }
            set { SetProperty<float?>(Number5Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number6Property =
            RegisterModelProperty<float?>(c => c.Number6);
        public float? Number6
        {
            get { return GetProperty<float?>(Number6Property); }
            set { SetProperty<float?>(Number6Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number7Property =
            RegisterModelProperty<float?>(c => c.Number7);
        public float? Number7
        {
            get { return GetProperty<float?>(Number7Property); }
            set { SetProperty<float?>(Number7Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number8Property =
            RegisterModelProperty<float?>(c => c.Number8);
        public float? Number8
        {
            get { return GetProperty<float?>(Number8Property); }
            set { SetProperty<float?>(Number8Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number9Property =
            RegisterModelProperty<float?>(c => c.Number9);
        public float? Number9
        {
            get { return GetProperty<float?>(Number9Property); }
            set { SetProperty<float?>(Number9Property, value); }
        }


        public static readonly ModelPropertyInfo<float?> Number10Property =
            RegisterModelProperty<float?>(c => c.Number10);
        public float? Number10
        {
            get { return GetProperty<float?>(Number10Property); }
            set { SetProperty<float?>(Number10Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number11Property =
            RegisterModelProperty<Int32?>(c => c.Number11);
        public Int32? Number11
        {
            get { return GetProperty<Int32?>(Number11Property); }
            set { SetProperty<Int32?>(Number11Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number12Property =
            RegisterModelProperty<Int32?>(c => c.Number12);
        public Int32? Number12
        {
            get { return GetProperty<Int32?>(Number12Property); }
            set { SetProperty<Int32?>(Number12Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number13Property =
            RegisterModelProperty<Int32?>(c => c.Number13);
        public Int32? Number13
        {
            get { return GetProperty<Int32?>(Number13Property); }
            set { SetProperty<Int32?>(Number13Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number14Property =
            RegisterModelProperty<Int32?>(c => c.Number14);
        public Int32? Number14
        {
            get { return GetProperty<Int32?>(Number14Property); }
            set { SetProperty<Int32?>(Number14Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number15Property =
            RegisterModelProperty<Int32?>(c => c.Number15);
        public Int32? Number15
        {
            get { return GetProperty<Int32?>(Number15Property); }
            set { SetProperty<Int32?>(Number15Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number16Property =
            RegisterModelProperty<Int32?>(c => c.Number16);
        public Int32? Number16
        {
            get { return GetProperty<Int32?>(Number16Property); }
            set { SetProperty<Int32?>(Number16Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number17Property =
            RegisterModelProperty<Int32?>(c => c.Number17);
        public Int32? Number17
        {
            get { return GetProperty<Int32?>(Number17Property); }
            set { SetProperty<Int32?>(Number17Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number18Property =
            RegisterModelProperty<Int32?>(c => c.Number18);
        public Int32? Number18
        {
            get { return GetProperty<Int32?>(Number18Property); }
            set { SetProperty<Int32?>(Number18Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number19Property =
            RegisterModelProperty<Int32?>(c => c.Number19);
        public Int32? Number19
        {
            get { return GetProperty<Int32?>(Number19Property); }
            set { SetProperty<Int32?>(Number19Property, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> Number20Property =
            RegisterModelProperty<Int32?>(c => c.Number20);
        public Int32? Number20
        {
            get { return GetProperty<Int32?>(Number20Property); }
            set { SetProperty<Int32?>(Number20Property, value); }
        }


        public static readonly ModelPropertyInfo<Byte?> BreakTrayUpProperty =
            RegisterModelProperty<Byte?>(c => c.BreakTrayUp);
        public Byte? BreakTrayUp
        {
            get { return GetProperty<Byte?>(BreakTrayUpProperty); }
            set { SetProperty<Byte?>(BreakTrayUpProperty, value); }
        }


        public static readonly ModelPropertyInfo<Byte?> BreakTrayBackProperty =
            RegisterModelProperty<Byte?>(c => c.BreakTrayBack);
        public Byte? BreakTrayBack
        {
            get { return GetProperty<Byte?>(BreakTrayBackProperty); }
            set { SetProperty<Byte?>(BreakTrayBackProperty, value); }
        }


        public static readonly ModelPropertyInfo<Byte?> BreaktrayDownProperty =
            RegisterModelProperty<Byte?>(c => c.BreaktrayDown);
        public Byte? BreaktrayDown
        {
            get { return GetProperty<Byte?>(BreaktrayDownProperty); }
            set { SetProperty<Byte?>(BreaktrayDownProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32> MaxRightCapProperty =
            RegisterModelProperty<Int32>(c => c.MaxRightCap);
        public Int32 MaxRightCap
        {
            get { return GetProperty<Int32>(MaxRightCapProperty); }
            set { SetProperty<Int32>(MaxRightCapProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32> MaxNestHighProperty =
            RegisterModelProperty<Int32>(c => c.MaxNestHigh);
        public Int32 MaxNestHigh
        {
            get { return GetProperty<Int32>(MaxNestHighProperty); }
            set { SetProperty<Int32>(MaxNestHighProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32> MaxNestDeepProperty =
            RegisterModelProperty<Int32>(c => c.MaxNestDeep);
        public Int32 MaxNestDeep
        {
            get { return GetProperty<Int32>(MaxNestDeepProperty); }
            set { SetProperty<Int32>(MaxNestDeepProperty, value); }
        }


        public static readonly ModelPropertyInfo<DateTime> CreatedProperty =
            RegisterModelProperty<DateTime>(c => c.Created);
        public DateTime Created
        {
            get { return GetProperty<DateTime>(CreatedProperty); }
            set { SetProperty<DateTime>(CreatedProperty, value); }
        }


        public static readonly ModelPropertyInfo<DateTime> ModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.Modified);
        public DateTime Modified
        {
            get { return GetProperty<DateTime>(ModifiedProperty); }
            set { SetProperty<DateTime>(ModifiedProperty, value); }
        }


        public static readonly ModelPropertyInfo<Boolean> CanMiddleCapProperty =
            RegisterModelProperty<Boolean>(c => c.CanMiddleCap);
        public Boolean CanMiddleCap
        {
            get { return GetProperty<Boolean>(CanMiddleCapProperty); }
            set { SetProperty<Boolean>(CanMiddleCapProperty, value); }
        }


        public static readonly ModelPropertyInfo<Boolean> IsNewProperty =
            RegisterModelProperty<Boolean>(c => c.IsNew);
        public Boolean IsNew
        {
            get { return GetProperty<Boolean>(IsNewProperty); }
            set { SetProperty<Boolean>(IsNewProperty, value); }
        }

        public static readonly ModelPropertyInfo<DateTime?> DeletedDateProperty =
            RegisterModelProperty<DateTime?>(c => c.DeletedDate);
    
        public DateTime? DeletedDate
        {
            get { return GetProperty<DateTime?>(DeletedDateProperty); }
            set { SetProperty<DateTime?>(DeletedDateProperty, value); }
        }

        public static readonly ModelPropertyInfo<Boolean> IsPlaceholderProperty =
        RegisterModelProperty<Boolean>(c => c.IsPlaceholder);
        public Boolean IsPlaceholder
        {
            get { return GetProperty<Boolean>(IsPlaceholderProperty); }
            set { SetProperty<Boolean>(IsPlaceholderProperty, value); }
        }
        #endregion
    }
}
