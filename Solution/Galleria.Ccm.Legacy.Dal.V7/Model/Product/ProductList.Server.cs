﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class ProductList
    {
        #region Constructors
        public ProductList() { } // force the use of factory methods
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria used when fetching products by product code
        /// </summary>
        [Serializable]
        public class FetchByProductCodesCriteria : CriteriaBase<FetchByProductCodesCriteria>
        {
            #region Properties

            /// <summary>
            /// Codes
            /// </summary>
            private static readonly PropertyInfo<List<String>> ProductCodesProperty = RegisterProperty<List<String>>(c => c.ProductCodes);
            public List<String> ProductCodes
            {
                get { return ReadProperty<List<String>>(ProductCodesProperty); }
                set { LoadProperty<List<String>>(ProductCodesProperty, value); }
            }

            #region Constructors
            /// <summary>
            /// Creates a new criteria objects
            /// </summary>
            /// <param name="storeIds">The list of the aisle ids</param>
            public FetchByProductCodesCriteria(List<String> codes)
            {
                LoadProperty<List<String>>(ProductCodesProperty, codes);
            }
            #endregion

            #endregion
        }

        /// <summary>
        /// Criteria used when fetching products by product ids
        /// </summary>
        [Serializable]
        public class FetchByProductIdsCriteria : CriteriaBase<FetchByProductIdsCriteria>
        {
            #region Properties

            /// <summary>
            /// Ids
            /// </summary>
            private static readonly PropertyInfo<List<Int32>> ProductIdsProperty = RegisterProperty<List<Int32>>(c => c.ProductIds);
            public List<Int32> ProductIds
            {
                get { return ReadProperty<List<Int32>>(ProductIdsProperty); }
                set { LoadProperty<List<Int32>>(ProductIdsProperty, value); }
            }

            #region Constructors
            /// <summary>
            /// Creates a new criteria objects
            /// </summary>
            /// <param name="storeIds">The list of the aisle ids</param>
            public FetchByProductIdsCriteria(List<Int32> codes)
            {
                LoadProperty<List<Int32>>(ProductIdsProperty, codes);
            }
            #endregion

            #endregion
        }


        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of products from a list of product codes
        /// </summary>
        /// <param name="productCodes">The list of product codes to return</param>
        /// <returns>A list of products</returns>
        public static ProductList GetProductsByCode(List<String> productCodes)
        {
            return DataPortal.Fetch<ProductList>(new FetchByProductCodesCriteria(productCodes));
        }

        /// <summary>
        /// Returns a list of products from the list of given ids
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public static ProductList FetchByProductIds(List<Int32> productIds)
        {
            return DataPortal.Fetch<ProductList>(new FetchByProductIdsCriteria(productIds));
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called to return a list of products that match the list of codes submitted
        /// </summary>
        /// <param name="criteria">The fethc criteria - list of product codes</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByProductCodesCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    IEnumerable<ProductDto> dtoList = dal.FetchByProductCodes(criteria.ProductCodes);
                    foreach (ProductDto dto in dtoList)
                    {
                        this.Add(Product.GetProduct(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called to return a list of products that match the list of codes submitted
        /// </summary>
        /// <param name="criteria">The fethc criteria - list of product codes</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByProductIdsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    IEnumerable<ProductDto> dtoList = dal.FetchByProductIds(criteria.ProductIds);
                    foreach (ProductDto dto in dtoList)
                    {
                        this.Add(Product.GetProduct(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
