﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Legacy.Dal.V7.Model
{
    public class PlanProcess
    {
        private String[] _categoryReviewIds;

        #region Constructor
        public PlanProcess(String ids)
        {
            if (String.IsNullOrWhiteSpace(ids))
            {
                _categoryReviewIds = new String[0];
            }
            else
            {
                _categoryReviewIds = ids.Split(',');
            }
        }
        #endregion
        #region Methods
        public List<Int32> FetchStorePlanIds()
        {

            List<Int32> dtoList = new List<Int32>();
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProcessDal dal = dalContext.GetDal<IPlanProcessDal>())
                {
                    dtoList.AddRange(dal.FetchStorePlanIds(_categoryReviewIds));
                }
            }
            return dtoList;
        }
        public List<Int32> FetchClusterPlanIds()
        {

            List<Int32> dtoList = new List<Int32>();
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProcessDal dal = dalContext.GetDal<IPlanProcessDal>())
                {
                    dtoList.AddRange(dal.FetchClusterPlanIds(_categoryReviewIds));
                }
            }
            return dtoList;
        }
        public List<Int32> FetchManualPlanIds()
        {

            List<Int32> dtoList = new List<Int32>();
           IDalFactory dalFactory = DalContainer.GetDalFactory();
           using (IDalContext dalContext = dalFactory.CreateContext())
           {
               using (IPlanProcessDal dal = dalContext.GetDal<IPlanProcessDal>())
               {
                   dtoList.AddRange(dal.FetchManualPlanIds(_categoryReviewIds));
               }
           }
            return dtoList;
        }
        public List<Int32> FetchMerchandisingPlanIds()
        {

            List<Int32> dtoList = new List<Int32>();
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProcessDal dal = dalContext.GetDal<IPlanProcessDal>())
                {
                    dtoList.AddRange(dal.FetchMerchandisingPlanIds(_categoryReviewIds));
                }
            }
            return dtoList;
        }

        public Single GetDbVersion()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProcessDal dal = dalContext.GetDal<IPlanProcessDal>())
                {
                    return dal.GetDbVersion();
                }
            }           
        }

        public void FixOriginalTemplateElementIds()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProcessDal dal = dalContext.GetDal<IPlanProcessDal>())
                {
                    dal.FixOriginalTemplateElementIds();
                }
            }
        }
        #endregion

    }
}
