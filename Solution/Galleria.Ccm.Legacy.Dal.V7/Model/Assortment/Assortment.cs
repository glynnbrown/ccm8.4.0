﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;

using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class Assortment : ModelObject<Assortment>
    {
        #region Properties
        

        /// <summary>
        /// The ProductId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
        }

        /// <summary>
        /// The Product Code
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductCodeProperty =
            RegisterModelProperty<String>(c => c.ProductCode);
        public String ProductCode
        {
            get { return GetProperty<String>(ProductCodeProperty); }
        }

        /// <summary>
        /// The Product Formatted Code
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductFormattedCodeProperty =
            RegisterModelProperty<String>(c => c.ProductFormattedCode);
        public String ProductFormattedCode
        {
            get { return GetProperty<String>(ProductFormattedCodeProperty); }
        }

        /// <summary>
        /// The Product Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductNameProperty =
            RegisterModelProperty<String>(c => c.ProductName);
        public String ProductName
        {
            get { return GetProperty<String>(ProductNameProperty); }
        }

        /// <summary>
        /// Product Recommended
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> RecommendedProperty =
            RegisterModelProperty<Boolean>(c => c.Recommended);
        public Boolean Recommended
        {
            get { return GetProperty<Boolean>(RecommendedProperty); }
        }

        /// <summary>
        /// Product GlobalRank
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> GlobalRankProperty =
            RegisterModelProperty<Int32>(c => c.GlobalRank);
        public Int32 GlobalRank
        {
            get { return GetProperty<Int32>(GlobalRankProperty); }
        }

        /// <summary>
        /// Product RecomendedUnits
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RecomendedUnitsProperty =
            RegisterModelProperty<Int32>(c => c.RecomendedUnits);
        public Int32 RecomendedUnits
        {
            get { return GetProperty<Int32>(RecomendedUnitsProperty); }
        }

        /// <summary>
        /// Product RecomendedFacings
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RecomendedFacingsProperty =
            RegisterModelProperty<Int32>(c => c.RecomendedFacings);
        public Int32 RecomendedFacings
        {
            get { return GetProperty<Int32>(RecomendedFacingsProperty); }
        }

        
        /// <summary>
        /// Product Segmentation
        /// </summary>
        public static readonly ModelPropertyInfo<String> SegmentationProperty =
            RegisterModelProperty<String>(c => c.Segmentation);
        public String Segmentation
        {
            get { return GetProperty<String>(SegmentationProperty); }
        }
        
        /// <summary>
        /// Product Rule1Type
        /// </summary>
        public static readonly ModelPropertyInfo<ProductRuleType> Rule1TypeProperty =
            RegisterModelProperty<ProductRuleType>(c => c.Rule1Type);
        public ProductRuleType Rule1Type
        {
            get { return GetProperty<ProductRuleType>(Rule1TypeProperty); }
        }

        /// <summary>
        /// Product RuleValue1
        /// </summary>
        public static readonly ModelPropertyInfo<Double> RuleValue1Property =
            RegisterModelProperty<Double>(c => c.RuleValue1);
        public Double RuleValue1
        {
            get { return GetProperty<Double>(RuleValue1Property); }
        }

        /// <summary>
        /// Product Rule2Type
        /// </summary>
        public static readonly ModelPropertyInfo<ProductRuleType> Rule2TypeProperty =
            RegisterModelProperty<ProductRuleType>(c => c.Rule2Type);
        public ProductRuleType Rule2Type
        {
            get { return GetProperty<ProductRuleType>(Rule2TypeProperty); }
        }

        /// <summary>
        /// Product RuleValue2
        /// </summary>
        public static readonly ModelPropertyInfo<Double> RuleValue2Property =
            RegisterModelProperty<Double>(c => c.RuleValue2);
        public Double RuleValue2
        {
            get { return GetProperty<Double>(RuleValue2Property); }
        }

        /// <summary>
        /// Product IsRuleDropped
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsRuleDroppedProperty =
            RegisterModelProperty<Boolean>(c => c.IsRuleDropped);
        public Boolean IsRuleDropped
        {
            get { return GetProperty<Boolean>(IsRuleDroppedProperty); }
        }

        /// <summary>
        /// Product ViolationType
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> ViolationTypeProperty =
            RegisterModelProperty<Byte>(c => c.ViolationType);
        public Byte ViolationType
        {
            get { return GetProperty<Byte>(ViolationTypeProperty); }
        }
        #endregion
    }

    public enum ProductRuleType
    {
        None = -2,
        MaximumList = 4,
        PreserveList = 3,
        MinimumDeList = 2,
        Exact = 1,
        ExactZero = -1
    }
}
