﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class AssortmentList
    {
        #region Constructors
        private AssortmentList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static AssortmentList GetPlanProfileRowsByPlanId(Int32 planId, PlanType planType)
        {
            return DataPortal.Fetch<AssortmentList>(new GetAssortmentByPlanIdCriteria(planId, planType));
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetAssortmentByPlanIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanAssortmentDal dal = dalContext.GetDal<IPlanAssortmentDal>())
                {
                    IEnumerable<PlanAssortmentDto> dtoList;

                    switch (criteria.PlanType)
                    {
                        case PlanType.Manual:
                            dtoList = dal.FetchManualByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Merchandising:
                            dtoList = dal.FetchMerchandisingByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Cluster:
                            dtoList = dal.FetchClusterByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Store:
                            dtoList = dal.FetchStoreByPlanId(criteria.PlanId);
                            break;
                        default:
                            dtoList = null;
                            break;
                    }
                    if (dtoList != null)
                    {
                        foreach (PlanAssortmentDto dto in dtoList)
                        {
                            this.Add(Assortment.GetAssortment(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
