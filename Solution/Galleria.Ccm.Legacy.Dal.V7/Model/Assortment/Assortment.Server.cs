﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class Assortment
    {
        #region Constructor
        private Assortment() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static Assortment GetAssortment(IDalContext dalContext, PlanAssortmentDto dto)
        {
            return DataPortal.FetchChild<Assortment>(dalContext, dto);
        }

        #endregion Factory Methods

        #region Data Transfer Objects

        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanAssortmentDto dto)
        {
            this.LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            this.LoadProperty<String>(ProductCodeProperty, dto.Code);
            this.LoadProperty<String>(ProductFormattedCodeProperty, dto.FormattedCode);
            this.LoadProperty<String>(ProductNameProperty, dto.Name);
            this.LoadProperty<Boolean>(RecommendedProperty, dto.Recommended);
            this.LoadProperty<Int32>(GlobalRankProperty, dto.GlobalRank);
            this.LoadProperty<Int32>(RecomendedUnitsProperty, dto.RecomendedUnits);
            this.LoadProperty<Int32>(RecomendedFacingsProperty, dto.RecomendedFacings);            
            this.LoadProperty<String>(SegmentationProperty, dto.Segmentation);
            this.LoadProperty<ProductRuleType>(Rule1TypeProperty, (ProductRuleType)dto.Rule1Type);
            this.LoadProperty<Double>(RuleValue1Property, dto.RuleValue1);
            this.LoadProperty<ProductRuleType>(Rule2TypeProperty, (ProductRuleType)dto.Rule2Type);
            this.LoadProperty<Double>(RuleValue2Property, dto.RuleValue2);
            this.LoadProperty<Boolean>(IsRuleDroppedProperty, dto.IsRuleDropped);
            this.LoadProperty<Byte>(ViolationTypeProperty, dto.ViolationType);
        }

        #endregion Data Transfer Objects

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanAssortmentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion Fetch
    }
}
