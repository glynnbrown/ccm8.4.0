﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class AssortmentList : ModelList<AssortmentList, Assortment>
    {
        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class GetAssortmentByPlanIdCriteria : Csla.CriteriaBase<GetAssortmentByPlanIdCriteria>
        {
            public static readonly PropertyInfo<Int32> PlanIdProperty =
            RegisterProperty<Int32>(c => c.PlanId);
            public Int32 PlanId
            {
                get { return ReadProperty<Int32>(PlanIdProperty); }
            }

            public static readonly PropertyInfo<PlanType> PlanTypeProperty =
            RegisterProperty<PlanType>(c => c.PlanType);
            public PlanType PlanType
            {
                get { return ReadProperty<PlanType>(PlanTypeProperty); }
            }


            public GetAssortmentByPlanIdCriteria(Int32 planId, PlanType planType)
            {
                LoadProperty<Int32>(PlanIdProperty, planId);
                LoadProperty<PlanType>(PlanTypeProperty, planType);

            }
        }

        #endregion
    }
}
