﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-16377 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 1.0 - CCM7.3.1)
// CCM-17909 : M.Brumby
//  Annotation offsets now work the same as position
#endregion
#region Version History: (CCM.Net 7.4)
// CCM-18877 : D.Pleasance
//  Plan changes so that CCM Bay information is a fixture rather than assembly
#endregion
#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Added FixturePlanBayNumber
#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
//  CCM-22561 : M.Brumby
//      Preserve space for shelves now ignores CCM YPosition in favour of
//      working it out ourselves.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanTextBox : ModelObject<PlanTextBox>
    {
        #region Properties

        /// <summary>
        /// Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
        RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get
            {
                return GetProperty<Int32>(IdProperty);
            }
        }

        /// <summary>
        /// PlanId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanId);
        public Int32 PlanId
        {
            get
            {
                return GetProperty<Int32>(PlanIdProperty);
            }
        }

        /// <summary>
        /// PlanBayId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanBayIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanBayId);
        public Int32 PlanBayId
        {
            get
            {
                return GetProperty<Int32>(PlanBayIdProperty);
            }
        }

        /// <summary>
        /// PlanElementId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanElementIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanElementId);
        public Int32 PlanElementId
        {
            get
            {
                return GetProperty<Int32>(PlanElementIdProperty);
            }
        }
        /// <summary>
        /// FixturePlanBayNumber
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixturePlanBayNumberProperty =
        RegisterModelProperty<Int32>(c => c.FixturePlanBayNumber);
        public Int32 FixturePlanBayNumber
        {
            get
            {
                return GetProperty<Int32>(FixturePlanBayNumberProperty);
            }
        }
        
        /// <summary>
        /// XPosition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> XPositionProperty =
        RegisterModelProperty<Double>(c => c.XPosition);
        public Double XPosition
        {
            get
            {
                return GetProperty<Double>(XPositionProperty);
            }
        }

        /// <summary>
        /// YPosition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> YPositionProperty =
        RegisterModelProperty<Double>(c => c.YPosition);
        public Double YPosition
        {
            get
            {
                return GetProperty<Double>(YPositionProperty);
            }
        }

        /// <summary>
        /// ZPosition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ZPositionProperty =
        RegisterModelProperty<Double>(c => c.ZPosition);
        public Double ZPosition
        {
            get
            {
                return GetProperty<Double>(ZPositionProperty);
            }
        }

        /// <summary>
        /// Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> HeightProperty =
        RegisterModelProperty<Double>(c => c.Height);
        public Double Height
        {
            get
            {
                return GetProperty<Double>(HeightProperty);
            }
        }

        /// <summary>
        /// Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> WidthProperty =
        RegisterModelProperty<Double>(c => c.Width);
        public Double Width
        {
            get
            {
                return GetProperty<Double>(WidthProperty);
            }
        }

        /// <summary>
        /// Depth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> DepthProperty =
        RegisterModelProperty<Double>(c => c.Depth);
        public Double Depth
        {
            get
            {
                return GetProperty<Double>(DepthProperty);
            }
        }

        /// <summary>
        /// Type
        /// </summary>
        public static readonly ModelPropertyInfo<PlanTexboxType> TypeProperty =
        RegisterModelProperty<PlanTexboxType>(c => c.Type);
        public PlanTexboxType Type
        {
            get
            {
                return GetProperty<PlanTexboxType>(TypeProperty);
            }
        }

        /// <summary>
        /// Description
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
        RegisterModelProperty<String>(c => c.Description);
        public String Description
        {
            get
            {
                return GetProperty<String>(DescriptionProperty);
            }
        }
        #endregion

        #region Overrides

        public override string ToString()
        {
            return Description;
        }

        #endregion
    }
}
