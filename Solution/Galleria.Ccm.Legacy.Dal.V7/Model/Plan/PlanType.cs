﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Model
{
    public enum PlanType
    {
        None = 0,
        Manual = 1,
        Merchandising = 2,
        Cluster = 3,
        Store = 4
    }
}
