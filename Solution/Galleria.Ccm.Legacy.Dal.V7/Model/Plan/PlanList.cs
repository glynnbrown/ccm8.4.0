﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanList : ModelList<PlanList, Plan>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //BusinessRules.AddRule(typeof(PlanList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.CategoryFetch.ToString()));
        }
        #endregion
    }
}
