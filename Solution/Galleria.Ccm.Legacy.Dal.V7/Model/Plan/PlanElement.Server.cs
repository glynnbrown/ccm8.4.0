﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion

#region Version History: (CCM v7.5.3)
//  CCM-23123 : M.Brumby
//      Added default name to to use if an element name is not null or containing white space.
//  CCM-23206 : M.Brumby
//      Added ChestWallRenderType
#endregion

#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 3. It was improved the way the combined direction for planogram elements are calculated. 
//   - It was ammend 2 scrips (FetchClusterByPlanId and FetchStoreByPlanId) to check the plan elements bloking type. If the blocking type was equal to 10 then it was a element to be combine.
//   - In the split plan it was added an addition step to re-calculate plan element combine direction based on the OriginalTemplateElementId.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanElement
    {
        #region Constructor
        private PlanElement() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanElement GetPlanElement(IDalContext dalContext, PlanElementDto dto)
        {
            return DataPortal.FetchChild<PlanElement>(dalContext, dto);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanElementDto dto)
        {
            #region Base Properties
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanIdProperty, dto.PlanId);
            this.LoadProperty<Int32>(PlanBayIdProperty, dto.PlanBayId);
            this.LoadProperty<String>(NameProperty, String.IsNullOrWhiteSpace(dto.Name) ? String.Format("Component {0}", dto.Id) : dto.Name);
            this.LoadProperty<String>(CombinedProperty, dto.Combined);
            this.LoadProperty<Double>(XPositionProperty, dto.XPosition);
            this.LoadProperty<Double>(YPositionProperty, dto.YPosition);
            this.LoadProperty<Double>(ZPositionProperty, dto.ZPosition);
            this.LoadProperty<String>(ElementTypeProperty, dto.ElementType);
            this.LoadProperty<Double>(ShelfHeightProperty, dto.ShelfHeight);
            this.LoadProperty<Double>(ShelfWidthProperty, dto.ShelfWidth);
            this.LoadProperty<Double>(ShelfDepthProperty, dto.ShelfDepth);
            this.LoadProperty<Double>(ShelfThickProperty, dto.ShelfThick);
            this.LoadProperty<Double>(ShelfSlopeProperty, dto.ShelfSlope);
            this.LoadProperty<Double>(ShelfRiserProperty, dto.ShelfRiser);
            this.LoadProperty<Double>(PegHeightProperty, dto.PegHeight);
            this.LoadProperty<Double>(PegWidthProperty, dto.PegWidth);
            this.LoadProperty<Double>(PegVertSpaceProperty, dto.PegVertSpace);
            this.LoadProperty<Double>(PegHorizSpaceProperty, dto.PegHorizSpace);
            this.LoadProperty<Double>(PegVertStartProperty, dto.PegVertStart);
            this.LoadProperty<Double>(PegHorzStartProperty, dto.PegHorzStart);
            this.LoadProperty<Double>(PegNotchDistanceProperty, dto.PegNotchDistance);
            this.LoadProperty<Double>(PegDepthProperty, dto.PegDepth);
            this.LoadProperty<Double>(ChestHeightProperty, dto.ChestHeight);
            this.LoadProperty<Double>(ChestWidthProperty, dto.ChestWidth);
            this.LoadProperty<Double>(ChestDepthProperty, dto.ChestDepth);
            this.LoadProperty<Double>(ChestWallProperty, dto.ChestWall);
            this.LoadProperty<Double>(ChestInsideProperty, dto.ChestInside);
            this.LoadProperty<Double>(ChestMerchandisingProperty, dto.ChestMerchandising);
            this.LoadProperty<Double>(ChestDividerProperty, dto.ChestDivider);
            this.LoadProperty<Double>(ChestAboveProperty, dto.ChestAbove);
            this.LoadProperty<Double>(BarHeightProperty, dto.BarHeight);
            this.LoadProperty<Double>(BarDepthProperty, dto.BarDepth);
            this.LoadProperty<Double>(BarWidthProperty, dto.BarWidth);
            this.LoadProperty<Double>(BarThickProperty, dto.BarThick);
            this.LoadProperty<Double>(BarDistanceProperty, dto.BarDistance);
            this.LoadProperty<Double>(BarHangerDepthProperty, dto.BarHangerDepth);
            this.LoadProperty<Double>(BarNotchProperty, dto.BarNotch);
            this.LoadProperty<Double>(BarHorzStartProperty, dto.BarHorzStart);
            this.LoadProperty<Double>(BarHorzSpaceProperty, dto.BarHorzSpace);
            this.LoadProperty<Double>(BarVertStartProperty, dto.BarVertStart);
            this.LoadProperty<Double>(BarBackHeightProperty, dto.BarBackHeight);
            this.LoadProperty<Byte>(NonMerchandisableProperty, dto.NonMerchandisable);
            this.LoadProperty<Double>(FingerSpaceProperty, dto.FingerSpace);
            this.LoadProperty<Double>(TopOverhangProperty, dto.TopOverhang);
            this.LoadProperty<Double>(BottomOverhangProperty, dto.BottomOverhang);
            this.LoadProperty<Double>(LeftOverhangProperty, dto.LeftOverhang);
            this.LoadProperty<Double>(RightOverhangProperty, dto.RightOverhang);
            this.LoadProperty<Double>(FrontOverhangProperty, dto.FrontOverhang);
            this.LoadProperty<Double>(BackOverhangProperty, dto.BackOverhang);
            this.LoadProperty<Double>(Number1Property, dto.Number1);
            this.LoadProperty<Double>(Number2Property, dto.Number2);
            this.LoadProperty<Int32>(Number3Property, dto.Number3);
            this.LoadProperty<Int32>(Number4Property, dto.Number4);
            this.LoadProperty<Int32>(Number5Property, dto.Number5);
            this.LoadProperty<DateTime>(CreatedProperty, dto.Created);
            this.LoadProperty<DateTime>(ModifiedProperty, dto.Modified);
            this.LoadProperty<Int32>(PersonnelIdProperty, dto.PersonnelId);
            this.LoadProperty<String>(PersonnelFlexiProperty, dto.PersonnelFlexi);
            this.LoadProperty<Int32>(CompanyIdProperty, dto.CompanyId);
            this.LoadProperty<ChestWallRenderType>(ChestWallRenderTypeProperty, (ChestWallRenderType)dto.ChestWallRenderType);
            this.LoadProperty<Byte>(CombineDirectionProperty, dto.CombineDirection);
            this.LoadProperty<Int32>(OriginalTemplateElementIdProperty, dto.OriginalTemplateElementId);
            #endregion
            
            #region Space Plans
            if (dto is SpacePlanElementDto)
            {
                this.LoadProperty<Int32>(StoreIdProperty, (dto as SpacePlanElementDto).StoreId);
                this.LoadProperty<Int32>(EmptyPegsProperty, (dto as SpacePlanElementDto).EmptyPegs);
                this.LoadProperty<Int32>(RightEmptyPegProperty, (dto as SpacePlanElementDto).RightEmptyPeg);
                this.LoadProperty<Int32>(ProductCountProperty, (dto as SpacePlanElementDto).ProductCount);
                this.LoadProperty<Int32>(PrimaryPegsProperty, (dto as SpacePlanElementDto).PrimaryPegs);
                this.LoadProperty<Double>(StartXProperty, (dto as SpacePlanElementDto).StartX);
                this.LoadProperty<Double>(MaxHeightProperty, (dto as SpacePlanElementDto).MaxHeight);
            }
            #endregion

            #region Manual Plan
            if (dto is ManualPlanElementDto)
            {
                this.LoadProperty<String>(Text1Property, (dto as ManualPlanElementDto).Text1);
                this.LoadProperty<String>(Text2Property, (dto as ManualPlanElementDto).Text2);
                this.LoadProperty<String>(Text3Property, (dto as ManualPlanElementDto).Text3);
                this.LoadProperty<String>(Text4Property, (dto as ManualPlanElementDto).Text4);
                this.LoadProperty<String>(Text5Property, (dto as ManualPlanElementDto).Text5);
            }
            #endregion
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanElementDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion
    }
}
