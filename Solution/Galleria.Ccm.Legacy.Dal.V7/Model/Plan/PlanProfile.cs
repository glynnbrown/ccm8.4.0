﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
// CCM-17428 : M.Brumby
//  Content description changed to plan name from workpackage name
#endregion
#region Version History: (CCM.Net 7.4)
// CCM-18877 : D.Pleasance
//  Plan changes so that CCM Bay information is a fixture rather than assembly
#endregion
#region Version History: (CCM.Net 7.4.1)
// CCM-20107 : M.Brumby
//  Publish middle capping flag
#endregion
#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Use SplitPlan if the plan has a fixturePlan assigned.
// CCM-20762 : M.Brumby
//  UniqueContentReference can be updated
// CCM-20928 : L.Bailey
//    "Units In Tray" field to be used in CCM.net Publish to GFS
//      Added TrayHeight, TrayWidth, TrayDepth, TrayUnits 
#endregion
#region Version History: (CCM.Net 7.5.1)
//  CCM-22192 : M.Brumby
//      Added StoreCode so that for store specific plans the location code 
//      field can be populated when publishing to GFS
#endregion
#region Version History: (CCM 7.5.2)

//  CCM-22440 : M.Brumby
//      Store and Cluster plans now fetch template plan positions. This list
//      can be used to add in any dropped products to the product list.
//  CCM-22483 : M.Brumby
//      Chest fixes
#endregion
#region Version History: (CCM.Net 7.5.3)
//  CCM-22944 : M.Brumby
//      Added break tray top
//  CCM-22962 : M.Toomer
//      Added  TotalPostionCount, BayCount, ComponentCount, MerchandisableLinearSpace, MerchandisableAreaSpace,
//      MerchandisableCubicSpace, TotalFacingCount, ProductsDroppedCount, WhiteSpacePercentage
//  CCM-23137 : M.Toomer
//      Increased accuracy of WhiteSpacePercentage passed to GFS to 4 decimal places
//  CCM-23143 : M.Toomer
//      Corrected TotalFacingCount calculation 
//  CCM-23182 : M.Toomer
//      Added TotalProductLinearSpace
//  CCM-23273 : M.Toomer
//      Corrected typo in selection for non-tray products for TotalFacingsCount
#endregion
#region Version History: (CCM v7.5.4)
//  CCM-23580 : M.Brumby
//      Added merge positions for Manual plans.
#endregion
#region Version History: (CCM v7.5.8)
//  CCM-26309 : M.Brumby (FWD integrated by Rob Cooper)
//      Use split plan positions when determining if there are invalid tray/unit products.
//  CCM-26877 : M.Brumby
//      Squeezed positions were being squeezed agian when turned into product records.
#endregion
#endregion

using System;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;

using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanProfile : ModelObject<PlanProfile>
    {
        #region Properties

        /// <summary>
        /// The profile row id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The Plan Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanId);
        public Int32 PlanId
        {
            get { return GetProperty<Int32>(PlanIdProperty); }
        }

        /// <summary>
        /// The Profile Detail Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProfileDetailIdProperty =
            RegisterModelProperty<Int32>(c => c.ProfileDetailId);
        public Int32 ProfileDetailId
        {
            get { return GetProperty<Int32>(ProfileDetailIdProperty); }
        }

        /// <summary>
        /// The Profile Group Type
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> ProfileGroupTypeProperty =
            RegisterModelProperty<Byte>(c => c.ProfileGroupType);
        public Byte ProfileGroupType
        {
            get { return GetProperty<Byte>(ProfileGroupTypeProperty); }
        }

        /// <summary>
        /// The Profile Group Type
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }
        
        /// <summary>
        /// IsActive
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsActiveProperty =
            RegisterModelProperty<Boolean>(c => c.IsActive);
        public Boolean IsActive
        {
            get { return GetProperty<Boolean>(IsActiveProperty); }
        }

        /// <summary>
        /// The Priority
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> PriorityProperty =
            RegisterModelProperty<Int16>(c => c.Priority);
        public Int16 Priority
        {
            get { return GetProperty<Int16>(PriorityProperty); }
        }

        /// <summary>
        /// The Ratio
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> RatioProperty =
            RegisterModelProperty<Int16>(c => c.Ratio);
        public Int16 Ratio
        {
            get { return GetProperty<Int16>(RatioProperty); }
        }

        /// <summary>
        /// The Percentage
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PercentageProperty =
            RegisterModelProperty<Single>(c => c.Percentage);
        public Single Percentage
        {
            get { return GetProperty<Single>(PercentageProperty); }
        }

        /// <summary>
        /// The Minimum
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MinimumProperty =
            RegisterModelProperty<Single>(c => c.Minimum);
        public Single Minimum
        {
            get { return GetProperty<Single>(MinimumProperty); }
        }

        /// <summary>
        /// The Maximum
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MaximumProperty =
            RegisterModelProperty<Single>(c => c.Maximum);
        public Single Maximum
        {
            get { return GetProperty<Single>(MaximumProperty); }
        }

        /// <summary>
        /// The RatioLowest
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> RatioLowestProperty =
            RegisterModelProperty<Int16>(c => c.RatioLowest);
        public Int16 RatioLowest
        {
            get { return GetProperty<Int16>(RatioLowestProperty); }
        }

        /// <summary>
        /// The RatioHighest
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> RatioHighestProperty =
            RegisterModelProperty<Int16>(c => c.RatioHighest);
        public Int16 RatioHighest
        {
            get { return GetProperty<Int16>(RatioHighestProperty); }
        }

        /// <summary>
        /// The KeyField
        /// </summary>
        public static readonly ModelPropertyInfo<String> KeyFieldProperty =
            RegisterModelProperty<String>(c => c.KeyField);
        public String KeyField
        {
            get { return GetProperty<String>(KeyFieldProperty); }
        }

        /// <summary>
        /// The ProfileGroupValue
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProfileGroupValueProperty =
            RegisterModelProperty<Int32>(c => c.ProfileGroupValue);
        public Int32 ProfileGroupValue
        {
            get { return GetProperty<Int32>(ProfileGroupValueProperty); }
        }
       

        #endregion
    }
}
