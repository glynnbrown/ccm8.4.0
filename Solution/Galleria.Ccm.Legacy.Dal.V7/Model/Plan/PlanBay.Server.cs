﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanBay
    {
        #region Constructor
        private PlanBay() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanBay GetPlanBay(IDalContext dalContext, PlanBayDto dto)
        {
            return DataPortal.FetchChild<PlanBay>(dalContext, dto);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanBayDto dto)
        {
            #region Base Properties
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanIdProperty, dto.PlanId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Double>(XPositionProperty, dto.XPosition);
            this.LoadProperty<Double>(BaseDepthProperty, dto.BaseDepth);
            this.LoadProperty<Double>(BaseHeightProperty, dto.BaseHeight);
            this.LoadProperty<Double>(BaseWidthProperty, dto.BaseWidth);
            this.LoadProperty<Double>(HeightProperty, dto.Height);
            this.LoadProperty<Double>(WidthProperty, dto.Width);
            this.LoadProperty<Double>(DepthProperty, dto.Depth);
            this.LoadProperty<String>(Text1Property, dto.Text1);
            this.LoadProperty<String>(Text2Property, dto.Text2);
            this.LoadProperty<String>(Text3Property, dto.Text3);
            this.LoadProperty<String>(Text4Property, dto.Text4);
            this.LoadProperty<String>(Text5Property, dto.Text5);
            this.LoadProperty<Double>(Number1Property, dto.Number1);
            this.LoadProperty<Double>(Number2Property, dto.Number2);
            this.LoadProperty<Int32>(Number3Property, dto.Number3);
            this.LoadProperty<Int32>(Number4Property, dto.Number4);
            this.LoadProperty<Int32>(Number5Property, dto.Number5);
            this.LoadProperty<String>(SourceSegmentsProperty, dto.SourceSegments);
            this.LoadProperty<String>(DestinationSegmentsProperty, dto.DestinationSegments);
            this.LoadProperty<DateTime>(CreatedProperty, dto.Created);
            this.LoadProperty<DateTime>(ModifiedProperty, dto.Modified);
            this.LoadProperty<Int32>(PersonnelIdProperty, dto.PersonnelId);
            this.LoadProperty<String>(PersonnelFlexiProperty, dto.PersonnelFlexi);
            this.LoadProperty<Int32>(CompanyIdProperty, dto.CompanyId);

            #endregion

            #region Space Plans
            if (dto is SpacePlanBayDto)
            {
                this.LoadProperty<Int32>(StoreIdProperty, (dto as SpacePlanBayDto).StoreId);
                this.LoadProperty<Int32>(FixtureIdProperty, (dto as SpacePlanBayDto).FixtureId);
            }
            #endregion

            #region Manual Plan
            if (dto is ManualPlanBayDto)
            {
                this.LoadProperty<String>(SegmentsProperty, (dto as ManualPlanBayDto).Segments);
            }
            #endregion
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanBayDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion
    }
}
