﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion

#region Version History: (CCM 7.5.2)

//  CCM-22440 : M.Brumby
//      Added empty list method
//  CCM-22483 : M.Brumby
//      Chest fixes
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanPositionList
    {
        #region Constructors
        private PlanPositionList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanPositionList GetPlanPositionsByPlanId(Int32 planId, PlanType planType, PlanElementList elements)
        {
            return DataPortal.Fetch<PlanPositionList>(new GetPlanPositionsByPlanIdCriteria(planId, planType, elements));
        }

        public static PlanPositionList EmptyList()
        {
            return new PlanPositionList();
        }
        #endregion

        #region Data Access


        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetPlanPositionsByPlanIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanPositionDal dal = dalContext.GetDal<IPlanPositionDal>())
                {
                    IEnumerable<PlanPositionDto> dtoList;

                    switch (criteria.PlanType)
                    {
                        case PlanType.Manual:
                            dtoList = dal.FetchManualByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Merchandising:
                            dtoList = dal.FetchMerchandisingByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Cluster:
                            dtoList = dal.FetchClusterByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Store:
                            dtoList = dal.FetchStoreByPlanId(criteria.PlanId);
                            break;
                        default:
                            dtoList = null;
                            break;
                    }
                    if (dtoList != null)
                    {
                        foreach (PlanPositionDto dto in dtoList)
                        {
                            this.Add(PlanPosition.GetPlanPosition(dalContext, dto, criteria.PlanElements));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
