﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#region Version History: (CCM v7.5.3)
//  CCM-23206 : M.Brumby
//      Added ChestWallRenderType
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Model
{
    public enum PlanElementType
    {
        Shelf,
        Bar,
        Peg,
        Chest,
        Pallet
    }

    public static class PlanElementTypeHelper
    {
        public static Dictionary<PlanElementType, String> InternalCCMNames = new Dictionary<PlanElementType, string>()
        {
            {PlanElementType.Shelf, "SHELF"},
            {PlanElementType.Bar, "BAR"},
            {PlanElementType.Peg, "PEG"},
            {PlanElementType.Chest, "CHEST"},
            {PlanElementType.Pallet, "PALLET"}
        };


        public static PlanElementType InternalNameToEnum(String InternalName)
        {
            foreach(KeyValuePair<PlanElementType, String> d in InternalCCMNames)
            {
                if (d.Value.ToUpperInvariant() == InternalName.ToUpperInvariant())
                {
                    return d.Key;
                }
            }

            return PlanElementType.Shelf;
        }
    }

    public enum ChestWallRenderType : byte
    {
        /// <summary>
        /// Front and back walls can render
        /// </summary>
        None = 0,
        /// <summary>
        /// Front, left, right and back walls can render
        /// </summary>
        LeftAndRight = 1,
        /// <summary>
        /// Front, left and back walls can render
        /// </summary>
        LeftOnly  = 2,
        /// <summary>
        /// Front, right and back walls can render
        /// </summary>
        RightOnly = 3

    }
}
