﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
// CCM-17518 : M.Brumby
//  Updated XYZ postion mappings
#endregion
#region Version History: (CCM.Net 1.0.1 - CCM730 PAtch 1)
// CCM-17824  : M.Brumby
//  Tray Front/Right Top cap values need publishing
#endregion
#region Version History: (CCM V7.5.0)
// CCM-20928 : L.Bailey
//    "Units In Tray" field to be used in CCM.net Publish to GFS
//      Added TrayHeight, TrayWidth, TrayDepth, TrayUnits 
// CCM-21164 :L.Bailey
//       Remove old reference for TrayHeight

#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
#endregion
#region Version History: (CCM.Net 7.5.3)
//  CCM-22944 : M.Brumby
//      Added break tray top
#endregion
#endregion
using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;
namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanPosition
    {
        #region Constructor
        private PlanPosition() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanPosition GetPlanPosition(IDalContext dalContext, PlanPositionDto dto, PlanElementList elements)
        {
            return DataPortal.FetchChild<PlanPosition>(dalContext, dto, elements);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanPositionDto dto, PlanElementList elements)
        {
            Boolean isInChest = false;

            PlanElement element = elements.FirstOrDefault(e => e.Id == dto.PlanElementId);
            if (element != null)
            {
                isInChest = PlanElementTypeHelper.InternalNameToEnum(element.ElementType) == PlanElementType.Chest;
            }


            #region Base Properties
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanIdProperty, dto.PlanId);
            this.LoadProperty<Int32>(PlanElementIdProperty, dto.PlanElementId);
            this.LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            this.LoadProperty<Int32>(OrientationIdProperty, dto.OrientationId);
            switch (dto.OrientationId)
            {
                case 0:// Front 0

                    if (!isInChest)
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductDepth);
                    }
                    else
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductHeight);
                    }

                    
                        this.LoadProperty<Int16>(TrayHighProperty, dto.TrayHigh);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayWide);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayDeep);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickHeight);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickWidth);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickDepth);
                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayHeight);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayWidth);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayDepth);
                    break;
                case 1: //Front 90
                    if (!isInChest)
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductDepth);
                        this.LoadProperty<Int16>(TrayHighProperty, dto.TrayWide);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayHigh);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayDeep);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickWidth / 2);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickHeight * 2);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickDepth);

                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayWidth);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayHeight);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayDepth);
                    }
                    else
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductWidth);
                    }
                    
                        this.LoadProperty<Int16>(TrayHighProperty, dto.TrayWide);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayHigh);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayDeep);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickWidth / 2);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickHeight * 2);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickDepth);

                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayWidth);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayHeight);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayDepth);
                    break;
                case 2: //Right 0
                    if (!isInChest)
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductWidth);
                        this.LoadProperty<Int16>(TrayHighProperty, dto.TrayHigh);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayDeep);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayWide);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickHeight);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickDepth);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickWidth);

                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayHeight);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayDepth);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayWidth);
                    }
                    else
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductHeight);
                    }
                        this.LoadProperty<Int16>(TrayHighProperty, dto.TrayHigh);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayDeep);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayWide);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickHeight);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickDepth);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickWidth);

                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayHeight);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayDepth);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayWidth);
                    break;
                case 3: //Right 90
                    if (!isInChest)
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductHeight);
                        
                    }
                    else
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductWidth);
                    }
                    this.LoadProperty<Int16>(TrayHighProperty, dto.TrayWide);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayDeep);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayHigh);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickWidth / 2);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickDepth);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickHeight * 2);

                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayWidth);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayDepth);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayHeight);
                    break;
                case 4: //Top 0
                    if (!isInChest)
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductHeight);
                    }
                    else
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductDepth);
                    }
                        this.LoadProperty<Int16>(TrayHighProperty, dto.TrayDeep);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayWide);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayHigh);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickDepth * 2);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickWidth);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickHeight / 2);

                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayDepth);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayWidth);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayHeight);
                    break;
                case 5: //Top 90
                    if (!isInChest)
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductDepth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductWidth);
                    }
                    else
                    {
                        this.LoadProperty<Double>(ProductHeightProperty, dto.ProductWidth);
                        this.LoadProperty<Double>(ProductWidthProperty, dto.ProductHeight);
                        this.LoadProperty<Double>(ProductDepthProperty, dto.ProductDepth);
                    }
                        this.LoadProperty<Int16>(TrayHighProperty, dto.TrayDeep);
                        this.LoadProperty<Int16>(TrayWideProperty, dto.TrayHigh);
                        this.LoadProperty<Int16>(TrayDeepProperty, dto.TrayWide);
                        this.LoadProperty<Single>(TrayThickHeightProperty, dto.TrayThickDepth / 2);
                        this.LoadProperty<Single>(TrayThickWidthProperty, dto.TrayThickHeight * 2);
                        this.LoadProperty<Single>(TrayThickDepthProperty, dto.TrayThickWidth);

                        this.LoadProperty<Single>(TrayHeightProperty, dto.TrayDepth);
                        this.LoadProperty<Single>(TrayWidthProperty, dto.TrayHeight);
                        this.LoadProperty<Single>(TrayDepthProperty, dto.TrayWidth);
                    break;
            }
            this.LoadProperty<Single>(XPositionProperty, dto.XPosition);
            this.LoadProperty<Single>(YPositionProperty, dto.YPosition);

            this.LoadProperty<Int16>(UnitsProperty, dto.Units);

            this.LoadProperty<Int16>(TrayUnitsProperty, dto.TrayUnits);
            if (!isInChest)
            {
                this.LoadProperty<Int16>(DeepProperty, dto.Deep);
                this.LoadProperty<Int16>(HighProperty, dto.High);
                this.LoadProperty<Int16>(WideProperty, dto.Wide);
               

            }
            else
            {
                this.LoadProperty<Int16>(DeepProperty, dto.High);
                this.LoadProperty<Int16>(HighProperty, dto.Deep);
                this.LoadProperty<Int16>(WideProperty, dto.Wide);
            }
            this.LoadProperty<Single>(SqueezeHeightProperty, dto.SqueezeHeight);
            this.LoadProperty<Single>(SqueezeWidthProperty, dto.SqueezeWidth);
            this.LoadProperty<Single>(SqueezeDepthProperty, dto.SqueezeDepth);
            this.LoadProperty<Single>(BlockHeightProperty, dto.BlockHeight);
            this.LoadProperty<Single>(BlockWidthProperty, dto.BlockWidth);
            this.LoadProperty<Single>(BlockDepthProperty, dto.BlockDepth);
            this.LoadProperty<Single?>(NestHeightProperty, dto.NestHeight);
            this.LoadProperty<Single?>(NestWidthProperty, dto.NestWidth);
            this.LoadProperty<Single?>(NestDepthProperty, dto.NestDepth);
            this.LoadProperty<Int16>(CapTopDeepProperty, dto.CapTopDeep);
            this.LoadProperty<Int16>(CapTopHighProperty, dto.CapTopHigh);
            this.LoadProperty<Int16>(CapRightCapTopDeepProperty, dto.CapRightCapTopDeep);
            this.LoadProperty<Int16>(CapRightCapTopHighProperty, dto.CapRightCapTopHigh);
            this.LoadProperty<Int16>(CapBotDeepProperty, dto.CapBotDeep);
            this.LoadProperty<Int16>(CapBotHighProperty, dto.CapBotHigh);
            this.LoadProperty<Int16>(TrayCountHighProperty, dto.TrayCountHigh);
            this.LoadProperty<Int16>(TrayCountWideProperty, dto.TrayCountWide);
            this.LoadProperty<Int16>(TrayCountDeepProperty, dto.TrayCountDeep);
            this.LoadProperty<Int16>(NestHighProperty, dto.NestHigh);
            this.LoadProperty<Int16>(NestWideProperty, dto.NestWide);
            this.LoadProperty<Int16>(NestDeepProperty, dto.NestDeep);

            this.LoadProperty<Int16>(CapLftDeepProperty, dto.CapLftDeep);
            this.LoadProperty<Int16>(CapLftWideProperty, dto.CapLftWide);
            this.LoadProperty<Int16>(CapRgtDeepProperty, dto.CapRgtDeep);
            this.LoadProperty<Int16>(CapRgtWideProperty, dto.CapRgtWide);
            this.LoadProperty<Single>(LeadingDividerProperty, dto.LeadingDivider);
            this.LoadProperty<Single>(LeadingGapProperty, dto.LeadingGap);
            this.LoadProperty<Single>(LeadingGapVertProperty, dto.LeadingGapVert);

            this.LoadProperty<Int32?>(TargetSCProperty, dto.TargetSC);
            this.LoadProperty<Int16>(MaxCapProperty, dto.MaxCap);
            this.LoadProperty<Int16>(MaxStackProperty, dto.MaxStack);
            this.LoadProperty<Int32>(MinDeepProperty, dto.MinDeep);
            this.LoadProperty<Int32>(MaxDeepProperty, dto.MaxDeep);

            this.LoadProperty<Byte>(TrayProductProperty, dto.TrayProduct);

            this.LoadProperty<Int16>(MerchTypeProperty, dto.MerchType);
            this.LoadProperty<Int32>(BlockingProperty, dto.Blocking);
            this.LoadProperty<Int32>(BlockIdProperty, dto.BlockId);
            this.LoadProperty<Int32>(BrushStyleProperty, dto.BrushStyle);
            this.LoadProperty<Int32>(MinDropProperty, dto.MinDrop);
            this.LoadProperty<Int32?>(RemoveCountProperty, dto.RemoveCount);
            this.LoadProperty<Int16>(CapRightWideProperty, dto.CapRightWide);
            this.LoadProperty<Int16>(CapRightDeepProperty, dto.CapRightDeep);
            this.LoadProperty<Int16>(CapRightWideTrayCountProperty, dto.CapRightWideTrayCount);
            this.LoadProperty<Int16>(CapRightDeepTrayCountProperty, dto.CapRightDeepTrayCount);
            this.LoadProperty<Byte>(MultiSitedProperty, dto.MultiSited);
            this.LoadProperty<Int32>(CasePackProperty, dto.CasePack);
            this.LoadProperty<Int32>(ColourProperty, dto.Colour);
            this.LoadProperty<Byte>(LastChanceProperty, dto.LastChance);
            this.LoadProperty<Byte>(AddLockedProperty, dto.AddLocked);
            this.LoadProperty<Byte>(RemoveLockedProperty, dto.RemoveLocked);
            this.LoadProperty<Int32?>(StripNumberProperty, dto.StripNumber);
            this.LoadProperty<Byte>(BreakTrayUpProperty, dto.BreakTrayUp);
            this.LoadProperty<Byte>(BreakTrayDownProperty, dto.BreakTrayDown);
            this.LoadProperty<Byte>(BreakTrayBackProperty, dto.BreakTrayBack);
            this.LoadProperty<Byte>(BreakTrayTopProperty, dto.BreakTrayTop);
            this.LoadProperty<Byte>(FrontOnlyProperty, dto.FrontOnly);
            this.LoadProperty<Byte>(MiddleCappingProperty, dto.MiddleCapping);
            this.LoadProperty<Int32?>(MaxNestHighProperty, dto.MaxNestHigh);
            this.LoadProperty<Int32?>(MaxNestDeepProperty, dto.MaxNestDeep);
            this.LoadProperty<Single>(PegDepthProperty, dto.PegDepth);
            this.LoadProperty<Single>(PegXProperty, dto.PegX);
            this.LoadProperty<Single>(PegYProperty, dto.PegY);
            this.LoadProperty<Int32?>(MaxRightCapProperty, dto.MaxRightCap);
            this.LoadProperty<DateTime>(CreatedProperty, dto.Created);
            this.LoadProperty<DateTime>(ModifiedProperty, dto.Modified);
            this.LoadProperty<Int32>(PersonnelIdProperty, dto.PersonnelId);
            this.LoadProperty<String>(PersonnelFlexiProperty, dto.PersonnelFlexi);
            this.LoadProperty<Int32>(CompanyIdProperty, dto.CompanyId);

            #endregion

            #region Space Plans
            if (dto is SpacePlanPositionDto)
            {
                this.LoadProperty<Int32>(StoreIdProperty, (dto as SpacePlanPositionDto).StoreId);
                this.LoadProperty<Byte>(PegFixProperty, (dto as SpacePlanPositionDto).PegFix);
                this.LoadProperty<Int32?>(PegShiftProperty, (dto as SpacePlanPositionDto).PegShift);
                this.LoadProperty<Int32>(ElementPositionIdProperty, (dto as SpacePlanPositionDto).ElementPositionId);
                this.LoadProperty<Single>(HistoricalDailyUnitsProperty, (dto as SpacePlanPositionDto).HistoricalDailyUnits);
                this.LoadProperty<Single>(ActualDaysSuppliedProperty, (dto as SpacePlanPositionDto).ActualDaysSupplied);
                this.LoadProperty<String>(BlockTagNameProperty, (dto as SpacePlanPositionDto).BlockTagName);
                this.LoadProperty<Int32?>(GlobalRankProperty, (dto as SpacePlanPositionDto).GlobalRank);
                this.LoadProperty<Int32?>(FinalMinProperty, (dto as SpacePlanPositionDto).FinalMin);
                this.LoadProperty<Int32?>(FinalMaxProperty, (dto as SpacePlanPositionDto).FinalMax);
                this.LoadProperty<Int32?>(MinPriority1Property, (dto as SpacePlanPositionDto).MinPriority1);
                this.LoadProperty<Int32?>(MinPriority2Property, (dto as SpacePlanPositionDto).MinPriority2);
                this.LoadProperty<Int32?>(MinPriority3Property, (dto as SpacePlanPositionDto).MinPriority3);
                this.LoadProperty<Int32?>(MinPriority4Property, (dto as SpacePlanPositionDto).MinPriority4);
                this.LoadProperty<Int32?>(MinPriority5Property, (dto as SpacePlanPositionDto).MinPriority5);
                this.LoadProperty<Int32?>(MaxPriority1Property, (dto as SpacePlanPositionDto).MaxPriority1);
                this.LoadProperty<Int32?>(MaxPriority2Property, (dto as SpacePlanPositionDto).MaxPriority2);
                this.LoadProperty<Int32?>(MaxPriority3Property, (dto as SpacePlanPositionDto).MaxPriority3);
                this.LoadProperty<Int32?>(MaxPriority4Property, (dto as SpacePlanPositionDto).MaxPriority4);
                this.LoadProperty<Int32?>(MaxPriority5Property, (dto as SpacePlanPositionDto).MaxPriority5);
                this.LoadProperty<Int32?>(RecommendedInventoryProperty, (dto as SpacePlanPositionDto).RecommendedInventory);
                this.LoadProperty<Int32?>(CompromiseInventoryProperty, (dto as SpacePlanPositionDto).CompromiseInventory);
                this.LoadProperty<Single?>(AverageWeeklySalesProperty, (dto as SpacePlanPositionDto).AverageWeeklySales);
                this.LoadProperty<Single?>(AverageWeeklyUnitsProperty, (dto as SpacePlanPositionDto).AverageWeeklyUnits);
                this.LoadProperty<Single?>(AverageWeeklyProfitProperty, (dto as SpacePlanPositionDto).AverageWeeklyProfit);
                this.LoadProperty<Single?>(AchievedCasesProperty, (dto as SpacePlanPositionDto).AchievedCases);
                this.LoadProperty<Single?>(AchievedDOSProperty, (dto as SpacePlanPositionDto).AchievedDOS);
                this.LoadProperty<Single?>(PerformanceValue01Property, (dto as SpacePlanPositionDto).PerformanceValue01);
                this.LoadProperty<Single?>(PerformanceValue02Property, (dto as SpacePlanPositionDto).PerformanceValue02);
                this.LoadProperty<Single?>(PerformanceValue03Property, (dto as SpacePlanPositionDto).PerformanceValue03);
                this.LoadProperty<Single?>(PerformanceValue04Property, (dto as SpacePlanPositionDto).PerformanceValue04);
                this.LoadProperty<Single?>(PerformanceValue05Property, (dto as SpacePlanPositionDto).PerformanceValue05);
                this.LoadProperty<Single?>(PerformanceValue06Property, (dto as SpacePlanPositionDto).PerformanceValue06);
                this.LoadProperty<Single?>(PerformanceValue07Property, (dto as SpacePlanPositionDto).PerformanceValue07);
                this.LoadProperty<Single?>(PerformanceValue08Property, (dto as SpacePlanPositionDto).PerformanceValue08);
                this.LoadProperty<Single?>(PerformanceValue09Property, (dto as SpacePlanPositionDto).PerformanceValue09);
                this.LoadProperty<Single?>(PerformanceValue10Property, (dto as SpacePlanPositionDto).PerformanceValue10);
                this.LoadProperty<Single?>(ListingPSIProperty, (dto as SpacePlanPositionDto).ListingPSI);
                this.LoadProperty<Single?>(ExposurePSIProperty, (dto as SpacePlanPositionDto).ExposurePSI);
                this.LoadProperty<Double?>(PegProngProperty, (dto as SpacePlanPositionDto).PegProng);
                this.LoadProperty<Boolean>(CanBeRefacedProperty, (dto as SpacePlanPositionDto).CanBeRefaced);
                this.LoadProperty<Byte>(AchievedInventoryLevelTypeProperty, (dto as SpacePlanPositionDto).AchievedInventoryLevelType);

            }
            #endregion

            #region Manual Plan
            if (dto is ManualPlanPositionDto)
            {
                this.LoadProperty<String>(CodeProperty, (dto as ManualPlanPositionDto).Code);
                this.LoadProperty<Single?>(PegZProperty, (dto as ManualPlanPositionDto).PegZ);
                this.LoadProperty<String>(PegStyleProperty, (dto as ManualPlanPositionDto).PegStyle);
                this.LoadProperty<Byte>(IsSequencedProperty, (dto as ManualPlanPositionDto).IsSequenced);
                this.LoadProperty<Byte>(NewProductsProperty, (dto as ManualPlanPositionDto).NewProducts);
                this.LoadProperty<Double>(Number1Property, (dto as ManualPlanPositionDto).Number1);
                this.LoadProperty<Double>(Number2Property, (dto as ManualPlanPositionDto).Number2);
                this.LoadProperty<Int32>(Number3Property, (dto as ManualPlanPositionDto).Number3);
                this.LoadProperty<Int32>(Number4Property, (dto as ManualPlanPositionDto).Number4);
                this.LoadProperty<Int32>(Number5Property, (dto as ManualPlanPositionDto).Number5);
                this.LoadProperty<String>(Text1Property, (dto as ManualPlanPositionDto).Text1);
                this.LoadProperty<String>(Text2Property, (dto as ManualPlanPositionDto).Text2);
                this.LoadProperty<String>(Text3Property, (dto as ManualPlanPositionDto).Text3);
                this.LoadProperty<String>(Text4Property, (dto as ManualPlanPositionDto).Text4);
                this.LoadProperty<String>(Text5Property, (dto as ManualPlanPositionDto).Text5);
                this.LoadProperty<Byte>(IsFlexPreservedSpaceProperty, (dto as ManualPlanPositionDto).IsFlexPreservedSpace);
                this.LoadProperty<Byte>(HidePreservedProperty, (dto as ManualPlanPositionDto).HidePreserved);
                this.LoadProperty<String>(AttachmentTempFileProperty, (dto as ManualPlanPositionDto).AttachmentTempFile);
                this.LoadProperty<String>(AttachmentNameProperty, (dto as ManualPlanPositionDto).AttachmentName);
                this.LoadProperty<Byte>(AttachmentBaseElementProperty, (dto as ManualPlanPositionDto).AttachmentBaseElement);

            }
            #endregion

        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanPositionDto dto, PlanElementList elements)
        {
            this.LoadDataTransferObject(dalContext, dto, elements);
        }

        #endregion
    }
}
