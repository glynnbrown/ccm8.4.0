﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
// CCM-17198 : M.Brumby
//  Position validation changed to total Units instead of High/Wide/Deep
#endregion
#region Version History: (CCM.Net 1.0.1 - CCM7.3.1)
// CCM-17909 : M.Brumby
//  Annotation offsets now work the same as position
#endregion
#region Version History: (CCM.Net 7.4)
// CCM-18877 : D.Pleasance
//  Plan changes so that CCM Bay information is a fixture rather than assembly
#endregion
#region Version History: (CCM.Net 7.3.5)
// CCM-22347 : D.Pleasance
//  Amended to set new GFS property SubComponentType
#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
//  CCM-22561 : M.Brumby
//      Set SubComponentType for chest walls
//      Preserve space for shelves now ignores CCM YPosition in favour of
//      working it out ourselves.
#endregion
#region Version History: (CCM v7.5.3)
//  CCM-23206 : M.Brumby
//      Added ChestWallRenderType
#endregion

#region Version History: (CCM v7.5.4)
//  CCM-23527 : M.Brumby
//      Changed chest product position order.
//  CCM-23815 : D.Pleasance
//      Added new method to create sequence for chest \ peg plans.
#endregion
#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 3. It was improved the way the combined direction for planogram elements are calculated. 
//   - It was ammend 2 scrips (FetchClusterByPlanId and FetchStoreByPlanId) to check the plan elements bloking type. If the blocking type was equal to 10 then it was a element to be combine.
//   - In the split plan it was added an addition step to re-calculate plan element combine direction based on the OriginalTemplateElementId.
#endregion
#endregion
using System;
using System.Linq;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;

using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanElement : ModelObject<PlanElement>
    {
        #region Properties

        #region Base Plan
        /// <summary>
        /// Id (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
        RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get
            {
                return GetProperty<Int32>(IdProperty);
            }
        }

        /// <summary>
        /// PlanId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanId);
        public Int32 PlanId
        {
            get
            {
                return GetProperty<Int32>(PlanIdProperty);
            }
        }

        /// <summary>
        /// PlanBayId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanBayIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanBayId);
        public Int32 PlanBayId
        {
            get
            {
                return GetProperty<Int32>(PlanBayIdProperty);
            }
        }

        /// <summary>
        /// Name (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
        RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get
            {
                return GetProperty<String>(NameProperty);
            }
        }

        /// <summary>
        /// Combined (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> CombinedProperty =
        RegisterModelProperty<String>(c => c.Combined);
        public String Combined
        {
            get
            {
                return GetProperty<String>(CombinedProperty);
            }
        }

        /// <summary>
        /// XPosition (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> XPositionProperty =
        RegisterModelProperty<Double>(c => c.XPosition);
        public Double XPosition
        {
            get
            {
                return GetProperty<Double>(XPositionProperty);
            }
        }

        /// <summary>
        /// YPosition (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> YPositionProperty =
        RegisterModelProperty<Double>(c => c.YPosition);
        public Double YPosition
        {
            get
            {
                return GetProperty<Double>(YPositionProperty);
            }
        }

        /// <summary>
        /// ZPosition (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ZPositionProperty =
        RegisterModelProperty<Double>(c => c.ZPosition);
        public Double ZPosition
        {
            get
            {
                return GetProperty<Double>(ZPositionProperty);
            }
        }

        /// <summary>
        /// ElementType (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> ElementTypeProperty =
        RegisterModelProperty<String>(c => c.ElementType);
        public String ElementType
        {
            get
            {
                return GetProperty<String>(ElementTypeProperty);
            }
        }

        /// <summary>
        /// ShelfHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ShelfHeightProperty =
        RegisterModelProperty<Double>(c => c.ShelfHeight);
        public Double ShelfHeight
        {
            get
            {
                return GetProperty<Double>(ShelfHeightProperty);
            }
        }

        /// <summary>
        /// ShelfWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ShelfWidthProperty =
        RegisterModelProperty<Double>(c => c.ShelfWidth);
        public Double ShelfWidth
        {
            get
            {
                return GetProperty<Double>(ShelfWidthProperty);
            }
        }

        /// <summary>
        /// ShelfDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ShelfDepthProperty =
        RegisterModelProperty<Double>(c => c.ShelfDepth);
        public Double ShelfDepth
        {
            get
            {
                return GetProperty<Double>(ShelfDepthProperty);
            }
        }

        /// <summary>
        /// ShelfThick (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ShelfThickProperty =
        RegisterModelProperty<Double>(c => c.ShelfThick);
        public Double ShelfThick
        {
            get
            {
                return GetProperty<Double>(ShelfThickProperty);
            }
        }

        /// <summary>
        /// ShelfSlope (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ShelfSlopeProperty =
        RegisterModelProperty<Double>(c => c.ShelfSlope);
        public Double ShelfSlope
        {
            get
            {
                return GetProperty<Double>(ShelfSlopeProperty);
            }
        }

        /// <summary>
        /// ShelfRiser (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ShelfRiserProperty =
        RegisterModelProperty<Double>(c => c.ShelfRiser);
        public Double ShelfRiser
        {
            get
            {
                return GetProperty<Double>(ShelfRiserProperty);
            }
        }

        /// <summary>
        /// PegHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegHeightProperty =
        RegisterModelProperty<Double>(c => c.PegHeight);
        public Double PegHeight
        {
            get
            {
                return GetProperty<Double>(PegHeightProperty);
            }
        }

        /// <summary>
        /// PegWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegWidthProperty =
        RegisterModelProperty<Double>(c => c.PegWidth);
        public Double PegWidth
        {
            get
            {
                return GetProperty<Double>(PegWidthProperty);
            }
        }

        /// <summary>
        /// PegVertSpace (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegVertSpaceProperty =
        RegisterModelProperty<Double>(c => c.PegVertSpace);
        public Double PegVertSpace
        {
            get
            {
                return GetProperty<Double>(PegVertSpaceProperty);
            }
        }

        /// <summary>
        /// PegHorizSpace (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegHorizSpaceProperty =
        RegisterModelProperty<Double>(c => c.PegHorizSpace);
        public Double PegHorizSpace
        {
            get
            {
                return GetProperty<Double>(PegHorizSpaceProperty);
            }
        }

        /// <summary>
        /// PegVertStart (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegVertStartProperty =
        RegisterModelProperty<Double>(c => c.PegVertStart);
        public Double PegVertStart
        {
            get
            {
                return GetProperty<Double>(PegVertStartProperty);
            }
        }

        /// <summary>
        /// PegHorzStart (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegHorzStartProperty =
        RegisterModelProperty<Double>(c => c.PegHorzStart);
        public Double PegHorzStart
        {
            get
            {
                return GetProperty<Double>(PegHorzStartProperty);
            }
        }

        /// <summary>
        /// PegNotchDistance (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegNotchDistanceProperty =
        RegisterModelProperty<Double>(c => c.PegNotchDistance);
        public Double PegNotchDistance
        {
            get
            {
                return GetProperty<Double>(PegNotchDistanceProperty);
            }
        }

        /// <summary>
        /// PegDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PegDepthProperty =
        RegisterModelProperty<Double>(c => c.PegDepth);
        public Double PegDepth
        {
            get
            {
                return GetProperty<Double>(PegDepthProperty);
            }
        }

        /// <summary>
        /// ChestHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestHeightProperty =
        RegisterModelProperty<Double>(c => c.ChestHeight);
        public Double ChestHeight
        {
            get
            {
                return GetProperty<Double>(ChestHeightProperty);
            }
        }

        /// <summary>
        /// ChestWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestWidthProperty =
        RegisterModelProperty<Double>(c => c.ChestWidth);
        public Double ChestWidth
        {
            get
            {
                return GetProperty<Double>(ChestWidthProperty);
            }
        }

        /// <summary>
        /// ChestDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestDepthProperty =
        RegisterModelProperty<Double>(c => c.ChestDepth);
        public Double ChestDepth
        {
            get
            {
                return GetProperty<Double>(ChestDepthProperty);
            }
        }

        /// <summary>
        /// ChestWall (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestWallProperty =
        RegisterModelProperty<Double>(c => c.ChestWall);
        public Double ChestWall
        {
            get
            {
                return GetProperty<Double>(ChestWallProperty);
            }
        }

        /// <summary>
        /// ChestInside (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestInsideProperty =
        RegisterModelProperty<Double>(c => c.ChestInside);
        public Double ChestInside
        {
            get
            {
                return GetProperty<Double>(ChestInsideProperty);
            }
        }

        /// <summary>
        /// ChestMerchandising (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestMerchandisingProperty =
        RegisterModelProperty<Double>(c => c.ChestMerchandising);
        public Double ChestMerchandising
        {
            get
            {
                return GetProperty<Double>(ChestMerchandisingProperty);
            }
        }

        /// <summary>
        /// ChestDivider (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestDividerProperty =
        RegisterModelProperty<Double>(c => c.ChestDivider);
        public Double ChestDivider
        {
            get
            {
                return GetProperty<Double>(ChestDividerProperty);
            }
        }

        /// <summary>
        /// ChestAbove (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ChestAboveProperty =
        RegisterModelProperty<Double>(c => c.ChestAbove);
        public Double ChestAbove
        {
            get
            {
                return GetProperty<Double>(ChestAboveProperty);
            }
        }

        /// <summary>
        /// BarHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarHeightProperty =
        RegisterModelProperty<Double>(c => c.BarHeight);
        public Double BarHeight
        {
            get
            {
                return GetProperty<Double>(BarHeightProperty);
            }
        }

        /// <summary>
        /// BarDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarDepthProperty =
        RegisterModelProperty<Double>(c => c.BarDepth);
        public Double BarDepth
        {
            get
            {
                return GetProperty<Double>(BarDepthProperty);
            }
        }

        /// <summary>
        /// BarWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarWidthProperty =
        RegisterModelProperty<Double>(c => c.BarWidth);
        public Double BarWidth
        {
            get
            {
                return GetProperty<Double>(BarWidthProperty);
            }
        }

        /// <summary>
        /// BarThick (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarThickProperty =
        RegisterModelProperty<Double>(c => c.BarThick);
        public Double BarThick
        {
            get
            {
                return GetProperty<Double>(BarThickProperty);
            }
        }

        /// <summary>
        /// BarDistance (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarDistanceProperty =
        RegisterModelProperty<Double>(c => c.BarDistance);
        public Double BarDistance
        {
            get
            {
                return GetProperty<Double>(BarDistanceProperty);
            }
        }

        /// <summary>
        /// BarHangerDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarHangerDepthProperty =
        RegisterModelProperty<Double>(c => c.BarHangerDepth);
        public Double BarHangerDepth
        {
            get
            {
                return GetProperty<Double>(BarHangerDepthProperty);
            }
        }

        /// <summary>
        /// BarNotch (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarNotchProperty =
        RegisterModelProperty<Double>(c => c.BarNotch);
        public Double BarNotch
        {
            get
            {
                return GetProperty<Double>(BarNotchProperty);
            }
        }

        /// <summary>
        /// BarHorzStart (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarHorzStartProperty =
        RegisterModelProperty<Double>(c => c.BarHorzStart);
        public Double BarHorzStart
        {
            get
            {
                return GetProperty<Double>(BarHorzStartProperty);
            }
        }

        /// <summary>
        /// BarHorzSpace (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarHorzSpaceProperty =
        RegisterModelProperty<Double>(c => c.BarHorzSpace);
        public Double BarHorzSpace
        {
            get
            {
                return GetProperty<Double>(BarHorzSpaceProperty);
            }
        }

        /// <summary>
        /// BarVertStart (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarVertStartProperty =
        RegisterModelProperty<Double>(c => c.BarVertStart);
        public Double BarVertStart
        {
            get
            {
                return GetProperty<Double>(BarVertStartProperty);
            }
        }

        /// <summary>
        /// BarBackHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BarBackHeightProperty =
        RegisterModelProperty<Double>(c => c.BarBackHeight);
        public Double BarBackHeight
        {
            get
            {
                return GetProperty<Double>(BarBackHeightProperty);
            }
        }

        /// <summary>
        /// NonMerchandisable (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> NonMerchandisableProperty =
        RegisterModelProperty<Byte>(c => c.NonMerchandisable);
        public Byte NonMerchandisable
        {
            get
            {
                return GetProperty<Byte>(NonMerchandisableProperty);
            }
        }

        /// <summary>
        /// FingerSpace (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> FingerSpaceProperty =
        RegisterModelProperty<Double>(c => c.FingerSpace);
        public Double FingerSpace
        {
            get
            {
                return GetProperty<Double>(FingerSpaceProperty);
            }
        }

        /// <summary>
        /// TopOverhang (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> TopOverhangProperty =
        RegisterModelProperty<Double>(c => c.TopOverhang);
        public Double TopOverhang
        {
            get
            {
                return GetProperty<Double>(TopOverhangProperty);
            }
        }

        /// <summary>
        /// BottomOverhang (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BottomOverhangProperty =
        RegisterModelProperty<Double>(c => c.BottomOverhang);
        public Double BottomOverhang
        {
            get
            {
                return GetProperty<Double>(BottomOverhangProperty);
            }
        }

        /// <summary>
        /// LeftOverhang (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> LeftOverhangProperty =
        RegisterModelProperty<Double>(c => c.LeftOverhang);
        public Double LeftOverhang
        {
            get
            {
                return GetProperty<Double>(LeftOverhangProperty);
            }
        }

        /// <summary>
        /// RightOverhang (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> RightOverhangProperty =
        RegisterModelProperty<Double>(c => c.RightOverhang);
        public Double RightOverhang
        {
            get
            {
                return GetProperty<Double>(RightOverhangProperty);
            }
        }

        /// <summary>
        /// FrontOverhang (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> FrontOverhangProperty =
        RegisterModelProperty<Double>(c => c.FrontOverhang);
        public Double FrontOverhang
        {
            get
            {
                return GetProperty<Double>(FrontOverhangProperty);
            }
        }

        /// <summary>
        /// BackOverhang (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BackOverhangProperty =
        RegisterModelProperty<Double>(c => c.BackOverhang);
        public Double BackOverhang
        {
            get
            {
                return GetProperty<Double>(BackOverhangProperty);
            }
        }

        /// <summary>
        /// Number1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number1Property =
        RegisterModelProperty<Double>(c => c.Number1);
        public Double Number1
        {
            get
            {
                return GetProperty<Double>(Number1Property);
            }
        }

        /// <summary>
        /// Number2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number2Property =
        RegisterModelProperty<Double>(c => c.Number2);
        public Double Number2
        {
            get
            {
                return GetProperty<Double>(Number2Property);
            }
        }

        /// <summary>
        /// Number3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number3Property =
        RegisterModelProperty<Int32>(c => c.Number3);
        public Int32 Number3
        {
            get
            {
                return GetProperty<Int32>(Number3Property);
            }
        }

        /// <summary>
        /// Number4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number4Property =
        RegisterModelProperty<Int32>(c => c.Number4);
        public Int32 Number4
        {
            get
            {
                return GetProperty<Int32>(Number4Property);
            }
        }

        /// <summary>
        /// Number5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number5Property =
        RegisterModelProperty<Int32>(c => c.Number5);
        public Int32 Number5
        {
            get
            {
                return GetProperty<Int32>(Number5Property);
            }
        }

        /// <summary>
        /// Created (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> CreatedProperty =
        RegisterModelProperty<DateTime>(c => c.Created);
        public DateTime Created
        {
            get
            {
                return GetProperty<DateTime>(CreatedProperty);
            }
        }

        /// <summary>
        /// Modified (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> ModifiedProperty =
        RegisterModelProperty<DateTime>(c => c.Modified);
        public DateTime Modified
        {
            get
            {
                return GetProperty<DateTime>(ModifiedProperty);
            }
        }

        /// <summary>
        /// PersonnelId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PersonnelIdProperty =
        RegisterModelProperty<Int32>(c => c.PersonnelId);
        public Int32 PersonnelId
        {
            get
            {
                return GetProperty<Int32>(PersonnelIdProperty);
            }
        }

        /// <summary>
        /// PersonnelFlexi (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PersonnelFlexiProperty =
        RegisterModelProperty<String>(c => c.PersonnelFlexi);
        public String PersonnelFlexi
        {
            get
            {
                return GetProperty<String>(PersonnelFlexiProperty);
            }
        }

        /// <summary>
        /// CompanyId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CompanyIdProperty =
        RegisterModelProperty<Int32>(c => c.CompanyId);
        public Int32 CompanyId
        {
            get
            {
                return GetProperty<Int32>(CompanyIdProperty);
            }
        }

        /// <summary>
        /// ChestWallRenderType, denotes which walls if any should be drawn
        /// if this is a chest. (Front and back walls are always drawn).
        /// </summary>
        public static readonly ModelPropertyInfo<ChestWallRenderType> ChestWallRenderTypeProperty =
        RegisterModelProperty<ChestWallRenderType>(c => c.ChestWallRenderType);
        public ChestWallRenderType ChestWallRenderType
        {
            get
            {
                return GetProperty<ChestWallRenderType>(ChestWallRenderTypeProperty);
            }
        }

        public static readonly ModelPropertyInfo<Byte> CombineDirectionProperty =
        RegisterModelProperty<Byte>(c => c.CombineDirection);
        public Byte CombineDirection
        {
            get
            {
                return GetProperty<Byte>(CombineDirectionProperty);
            }
            set
            {
                SetProperty<Byte>(CombineDirectionProperty, value);
            }
        }

        /// <summary>
        /// OriginalTemplateElementId 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> OriginalTemplateElementIdProperty =
        RegisterModelProperty<Int32>(c => c.OriginalTemplateElementId);
        public Int32 OriginalTemplateElementId
        {
            get
            {
                return GetProperty<Int32>(OriginalTemplateElementIdProperty);
            }
        }
        #endregion

        #region Manual Plan
        /// <summary>
        /// Text1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
        RegisterModelProperty<String>(c => c.Text1);
        public String Text1
        {
            get
            {
                return GetProperty<String>(Text1Property);
            }
        }

        /// <summary>
        /// Text2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
        RegisterModelProperty<String>(c => c.Text2);
        public String Text2
        {
            get
            {
                return GetProperty<String>(Text2Property);
            }
        }

        /// <summary>
        /// Text3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
        RegisterModelProperty<String>(c => c.Text3);
        public String Text3
        {
            get
            {
                return GetProperty<String>(Text3Property);
            }
        }

        /// <summary>
        /// Text4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
        RegisterModelProperty<String>(c => c.Text4);
        public String Text4
        {
            get
            {
                return GetProperty<String>(Text4Property);
            }
        }

        /// <summary>
        /// Text5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
        RegisterModelProperty<String>(c => c.Text5);
        public String Text5
        {
            get
            {
                return GetProperty<String>(Text5Property);
            }
        }


        #endregion

        #region Space Plan
        /// <summary>
        /// StoreId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> StoreIdProperty =
        RegisterModelProperty<Int32>(c => c.StoreId);
        public Int32 StoreId
        {
            get
            {
                return GetProperty<Int32>(StoreIdProperty);
            }
        }

        /// <summary>
        /// EmptyPegs (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EmptyPegsProperty =
        RegisterModelProperty<Int32>(c => c.EmptyPegs);
        public Int32 EmptyPegs
        {
            get
            {
                return GetProperty<Int32>(EmptyPegsProperty);
            }
        }

        /// <summary>
        /// RightEmptyPeg (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RightEmptyPegProperty =
        RegisterModelProperty<Int32>(c => c.RightEmptyPeg);
        public Int32 RightEmptyPeg
        {
            get
            {
                return GetProperty<Int32>(RightEmptyPegProperty);
            }
        }

        /// <summary>
        /// ProductCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductCountProperty =
        RegisterModelProperty<Int32>(c => c.ProductCount);
        public Int32 ProductCount
        {
            get
            {
                return GetProperty<Int32>(ProductCountProperty);
            }
        }

        /// <summary>
        /// PrimaryPegs (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PrimaryPegsProperty =
        RegisterModelProperty<Int32>(c => c.PrimaryPegs);
        public Int32 PrimaryPegs
        {
            get
            {
                return GetProperty<Int32>(PrimaryPegsProperty);
            }
        }

        /// <summary>
        /// StartX (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> StartXProperty =
        RegisterModelProperty<Double>(c => c.StartX);
        public Double StartX
        {
            get
            {
                return GetProperty<Double>(StartXProperty);
            }
        }

        /// <summary>
        /// MaxHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MaxHeightProperty =
        RegisterModelProperty<Double>(c => c.MaxHeight);
        public Double MaxHeight
        {
            get
            {
                return GetProperty<Double>(MaxHeightProperty);
            }
        }


        #endregion

        #endregion

        #region Methods

        #endregion
    }
}
