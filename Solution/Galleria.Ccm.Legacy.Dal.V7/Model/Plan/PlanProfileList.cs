﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanProfileList : ModelList<PlanProfileList, PlanProfile>
    {
        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class GetPlanProfileRowsByPlanIdCriteria : Csla.CriteriaBase<GetPlanProfileRowsByPlanIdCriteria>
        {
            public static readonly PropertyInfo<Int32> PlanIdProperty =
            RegisterProperty<Int32>(c => c.PlanId);
            public Int32 PlanId
            {
                get { return ReadProperty<Int32>(PlanIdProperty); }
            }

            public static readonly PropertyInfo<PlanType> PlanTypeProperty =
            RegisterProperty<PlanType>(c => c.PlanType);
            public PlanType PlanType
            {
                get { return ReadProperty<PlanType>(PlanTypeProperty); }
            }

            
            public GetPlanProfileRowsByPlanIdCriteria(Int32 planId, PlanType planType)
            {
                LoadProperty<Int32>(PlanIdProperty, planId);
                LoadProperty<PlanType>(PlanTypeProperty, planType);
                
            }
        }

        #endregion
    }
}
