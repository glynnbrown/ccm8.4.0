﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 7.5.1)
//  CCM-21707 : M.Brumby
//      Changed how the Bay name is created
#endregion
#region Version History: (CCM v7.5.4)
//  CCM-23580 : M.Brumby
//      Added merge positions for Manual plans.
#endregion
#region Version History: (CCM 7.5.5)
// CCM-24699 : M.Brumby
//  Backboard fixture assembly id now set from helper
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Legacy.Model
{
    /// <summary>
    /// Creates a split bay from a mixture of the supplied plan bay and
    /// fixtureplan bay
    /// </summary>
    public class SplitBay
    {
        #region Fields

        private Int32 _id = -1;
        private PlanBay _parentPlanBay;
        private GenericBay _parentGenericPlanBay;

        #endregion

        #region Properties

        public Int32 Id
        {
            get { return _id; }
        }

        public PlanBay ParentPlanBay
        {
            get { return _parentPlanBay; }
        }
        public GenericBay ParentGenericPlanBay
        {
            get { return _parentGenericPlanBay; }
        }

        /// <summary>
        /// Parent Fixture Plan Bay XPosition
        /// </summary>
        public Double XPosition
        {
            get { return ParentGenericPlanBay.XPosition; }
        }

        /// <summary>
        /// Parent Fixture Plan Bay Width
        /// </summary>
        public Double Width
        {
            get { return ParentGenericPlanBay.Width; }
        }

        /// <summary>
        /// ParentPlan Bay name truncated to 50 chars
        /// </summary>
        public String Name
        {
            get
            {
                //Rename the bay so the same bay name isn't repeated
                return String.Format("Bay {0}", this.Id);

                // Truncate name to 50 characters if length exceeds 50
                //return ParentPlanBay.Name.Substring(0, ParentPlanBay.Name.Length > 50 ? 50 : ParentPlanBay.Name.Length);
            }
        }

        /// <summary>
        /// Parent plan bay height
        /// </summary>
        public Double Height
        {
            get { return ParentPlanBay.Height; }
        }
        /// <summary>
        /// Parent plan bay height
        /// </summary>
        public Double Depth
        {
            get { return ParentPlanBay.Depth; }
        }
        #endregion

        #region Constructor

        public SplitBay(Int32 id, PlanBay planBay, GenericBay genericPlanBay)
        {
            _id = id;
            _parentPlanBay = planBay;
            _parentGenericPlanBay = genericPlanBay;

        }

        #endregion

        #region Method

        public void Dispose()
        {
            _parentGenericPlanBay = null;
            _parentPlanBay = null;            
        }


        #endregion
    }

    /// <summary>
    /// Class that holds some generic bay information
    /// </summary>
    public class GenericBay
    {
        #region Properties

        /// <summary>
        /// Parent Fixture Plan Bay XPosition
        /// </summary>
        public Double XPosition { get; set; }

        /// <summary>
        /// Parent Fixture Plan Bay Width
        /// </summary>
        public Double Width { get; set; }

        /// <summary>
        /// Bay name truncated
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Parent plan bay height
        /// </summary>
        public Double Height { get; set; }
        /// <summary>
        /// Parent plan bay height
        /// </summary>
        public Double Depth { get; set; }

        /// <summary>
        /// The bay number
        /// </summary>
        public Int32 BayNumber { get; set; }

        /// <summary>
        /// Determines if elements created using this bay should use
        /// their original element XPosition or if the element should
        /// start from the beginning of the bay.
        /// </summary>
        public Boolean UseOriginalElementX { get; set; }
        #endregion

        #region Constructor

        /// <summary>
        /// Create a generic bay item from a Plan Bay. A bay number must be
        /// provided so that it will still match up with any bay textboxes.
        /// </summary>
        /// <param name="bay"></param>
        /// <param name="bayNumber"></param>
        public GenericBay(PlanBay bay, Int32 bayNumber)
        {
            Name = bay.Name;
            XPosition = bay.XPosition;
            Width = bay.Width;
            Height = bay.Height;
            Depth = bay.Depth;
            BayNumber = bayNumber;
            UseOriginalElementX = true;
        }

        /// <summary>
        /// Create a generic bay item from a fixture plan bay.
        /// </summary>
        /// <param name="bay"></param>
        public GenericBay(FixturePlanBay bay)
        {
            Name = bay.Name;
            XPosition = bay.XPosition;
            Width = bay.Width;
            Height = bay.Height;
            Depth = bay.Depth;
            BayNumber = bay.BayNumber;
            UseOriginalElementX = false;
        }
        #endregion
    }
}
