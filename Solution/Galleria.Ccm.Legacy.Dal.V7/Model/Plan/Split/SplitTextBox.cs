﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
//  CCM-22561 : M.Brumby
//      Preserve space for shelves now ignores CCM YPosition in favour of
//      working it out ourselves.
#endregion
#endregion
using System;

namespace Galleria.Ccm.Legacy.Model
{
    public class SplitTextBox
    {
        #region Fields

        public PlanTextBox _parent;
        public Int32 _splitBayId = -1;
        public Int32 _id = -1;
        public Int32 _elementId = -1;
        public Double _XPosition = 0;
        public Double _width = 0;
        #endregion

        #region Properties

        public PlanTextBox Parent
        {
            get { return _parent; }
        }

        /// <summary>
        /// Id
        /// </summary>
        public Int32 Id
        {
            get
            {
                return _id;
            }
        }

        /// <summary>
        /// PlanId
        /// </summary>
        public Int32 PlanId
        {
            get
            {
                return Parent.PlanId;
            }
        }

        /// <summary>
        /// PlanBayId
        /// </summary>
        public Int32 SplitBayId
        {
            get
            {
                return _splitBayId;
            }
        }

        /// <summary>
        /// PlanElementId
        /// </summary>
        public Int32 SplitElementId
        {
            get
            {
                return _elementId;
            }
        }

        /// <summary>
        /// XPosition
        /// </summary>
        public Double XPosition
        {
            get
            {
                return _XPosition;
            }
        }

        /// <summary>
        /// YPosition
        /// </summary>
        public Double YPosition
        {
            get
            {
                return Parent.YPosition;
            }
        }

        /// <summary>
        /// ZPosition
        /// </summary>
        public Double ZPosition
        {
            get
            {
                return Parent.ZPosition;
            }
        }

        /// <summary>
        /// Height
        /// </summary>
        public Double Height
        {
            get
            {
                return Parent.Height;
            }
        }

        /// <summary>
        /// Width
        /// </summary>
        public Double Width
        {
            get
            {
                return _width;
            }
        }

        /// <summary>
        /// Depth
        /// </summary>
        public Double Depth
        {
            get
            {
                return Parent.Depth;
            }
        }

        /// <summary>
        /// Type
        /// </summary>
        public PlanTexboxType Type
        {
            get
            {
                return Parent.Type;
            }
        }

        /// <summary>
        /// Description
        /// </summary>
        public String Description
        {
            get
            {
                return Parent.Description;
            }
        }
        #endregion

        #region Constructor

        public SplitTextBox(Int32 id, PlanTextBox parent, Int32 splitBayId)
        {
            _id = id;
            _parent = parent;
            _splitBayId = splitBayId;
        }

        public SplitTextBox(Int32 id, PlanTextBox parent, Int32 splitBayId, Int32 splitElementId, Double XPosition, Double width)
        {
            _id = id;
            _parent = parent;
            _splitBayId = splitBayId;
            _elementId = splitElementId;
            _XPosition = XPosition;
            _width = width;
        }

        #endregion

        public void Dispose()
        {
            _parent = null;
        }

        #region Overrides

        public override string ToString()
        {
            return Description;
        }

        #endregion
    }
}
