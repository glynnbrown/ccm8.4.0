﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Created
// CCM-21075 : M.Brumby
//  Defaulted positions so a position is not split up across bays.
// CCM-21410 : M.Brumby
//  Non split positions break tray up values corrected.
//  CCM-21409 : M.Brumby
//      Units wide for trays now includes right cap
//  CCM-21495 : M.Brumby
//      Fixes for capped trays
#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22547 : M.Brumby
//      Right no longer added to wide as it is not included in PlanPosition wide.
#endregion
#region Version History: (CCM v7.5.4)
//  CCM-23580 : M.Brumby
//      Added merge positions for Manual plans.
#endregion
#region Version History: (CCM v7.5.5)
//  CCM-24575 : M.Brumby
//      Changed PositionFitsInBay so that the centre is valid at the start
//      and end of the bay.
//  CCM-24590 : M.Brumby
//      Changed PositionFitsInBay to only use Decimals due to rounding issues.
#endregion
#region Version History: (CCM v7.5.6)
//  CCM-25004 : Rob Cooper (FWD integration from CCM v7.5.3 CP08 by M.Brumby)
//      Changed PositionFitsInBay to have a tollerance as rounding differences
//      can still occure between PlanSignIn and Publishing. yay! rounding :/
#endregion
#region Version History: (CCM v7.5.7)
//  CCM-26104 : M.Brumby
//      Check element type before joining positions together as some positions
//      shouldn't be joined.
#endregion
#region Version History: (CCM v7.5.8)
//  CCM-26889 : M.Brumby
//      Added more validation on joining positions
#endregion
#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 3. It was improved the way the combined direction for planogram elements are calculated. 
//   - It was ammend 2 scrips (FetchClusterByPlanId and FetchStoreByPlanId) to check the plan elements bloking type. If the blocking type was equal to 10 then it was a element to be combine.
//   - In the split plan it was added an addition step to re-calculate plan element combine direction based on the OriginalTemplateElementId.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Legacy.Model
{
    /// <summary>
    /// Creates a plan structure where by a plan is split down to
    /// match its fixture plan bays.
    /// </summary>
    public class SplitPlan
    {

        #region Fields

        private Boolean _splitPositionsByBay = false;
        private Plan _parentPlan;
        private List<SplitBay> _bays = new List<SplitBay>();
        private List<SplitElement> _elements = new List<SplitElement>();
        private List<SplitPosition> _splitPositionList = new List<SplitPosition>();
        private List<SplitTextBox> _splitTextBoxList = new List<SplitTextBox>();
        private List<GenericBay> _genericBayList = new List<GenericBay>();

        #endregion

        #region Properties

        public Plan ParentPlan
        {
            get { return _parentPlan; }
        }

        public List<GenericBay> GenericBayList
        {
            get { return _genericBayList; }
        }

        public List<SplitBay> Bays
        {
            get { return _bays; }
        }

        public List<SplitElement> Elements
        {
            get { return _elements; }
        }

        public List<SplitPosition> Positions
        {
            get { return _splitPositionList; }
        }

        public List<SplitTextBox> TextBoxes
        {
            get { return _splitTextBoxList; }
        }

        #endregion

        #region Constructor

        public SplitPlan(Plan plan, Boolean useFixturePlanBays)
        {
            _parentPlan = plan;
            if (useFixturePlanBays)
            {
                foreach (FixturePlanBay bay in FixturePlanBayList.GetPlanBaysByFixturePlanId(plan.FixturePlanId))
                {
                    _genericBayList.Add(new GenericBay(bay));
                }
            }
            else
            {
                Int32 bayNumber = 1;
                foreach (PlanBay bay in plan.PlanBays.OrderBy(b => b.XPosition))
                {
                    _genericBayList.Add(new GenericBay(bay, bayNumber));
                    bayNumber++;
                }
            }

            Initialise();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Split the plan up
        /// </summary>
        private void Initialise()
        {
            Int32 planBayIndex = 0;
            Int32 fixtureBayIndex = 0;
            Int32 elementID = 1;
            Int32 positionID = 1;
            Int32 textboxID = 1;

            List<JoinPosition> joinedPositions = JoinPositions();

            //Loop through all fixture plan bays as these are the smallest "bay unit"
            for (fixtureBayIndex = 0; fixtureBayIndex < GenericBayList.Count; fixtureBayIndex++)
            {
                //Each fixture plan bay is a new split bay using the dimensions for the current plan bay
                SplitBay splitBay = new SplitBay(fixtureBayIndex + 1, ParentPlan.PlanBays[planBayIndex], GenericBayList[fixtureBayIndex]);
                Bays.Add(splitBay);

                //Map Informational text boxes to the current fixture plan bay
                foreach (var textBox in ParentPlan.PlanTextBoxs.Where(t => t.Type == PlanTexboxType.Informational && GenericBayList[fixtureBayIndex].BayNumber == t.FixturePlanBayNumber))
                {
                    this.TextBoxes.Add(new SplitTextBox(textboxID, textBox, splitBay.Id));
                    textboxID++;
                }

                //A copy of each element from the original bay should be present on the new split bays
                foreach (var element in ParentPlan.PlanElements.Where(e => e.PlanBayId == ParentPlan.PlanBays[planBayIndex].Id))
                {                    
                    SplitElement splitElement = new SplitElement(elementID, splitBay.Id, element, GenericBayList[fixtureBayIndex]);
                    Elements.Add(splitElement);
                    elementID++;

                    #region PlanPositions
                    //Process all the plan positions for the current element.
                    foreach (var position in joinedPositions.Where(p => p.PlanElementIds.Contains(element.Id)))
                    {                
                        //Set the position element id to the current element
                        position.PlanElementId = element.Id;

                        if (_splitPositionsByBay)
                        {
                            //This will place any individual facings onto the bay element that have a centre point
                            //that fits within the bay.
                            positionID = SplitPositionsByBay(position, splitElement, planBayIndex, fixtureBayIndex, positionID);
                        }
                        else
                        {
                            //This will place the whole posiiton onto the bay element if the centre point
                            //fits within the bay.
                            positionID = PlacePosition(position, splitElement, planBayIndex, fixtureBayIndex, positionID);
                        }
                        
                    }
                    #endregion

                    #region Textboxes
                    //Create element text Boxes
                    foreach (var textBox in ParentPlan.PlanTextBoxs
                        .Where(t => t.Type == PlanTexboxType.Preserve &&
                            ParentPlan.PlanBays[planBayIndex].Id == t.PlanBayId &&
                            element.Id == t.PlanElementId))
                    {
                        Double textBoxStartX = textBox.XPosition + ParentPlan.PlanBays[planBayIndex].XPosition;
                        Double textBoxEndX = textBox.XPosition + textBox.Width + ParentPlan.PlanBays[planBayIndex].XPosition;
                        Double bayEnd = GenericBayList[fixtureBayIndex].XPosition + GenericBayList[fixtureBayIndex].Width;

                        Double finalX = 0;
                        //if the start of the text box is on this bay then calculate finalX
                        //otherwise finalX is always 0 (i.e the start of the current bay)
                        if (GenericBayList[fixtureBayIndex].XPosition < textBoxStartX)
                        {
                            finalX = textBoxStartX - GenericBayList[fixtureBayIndex].XPosition;
                        }

                        Double finalEndX = 0;
                        if (bayEnd >= textBoxEndX)
                        {
                            finalEndX = textBoxEndX - GenericBayList[fixtureBayIndex].XPosition;
                        }
                        else
                        {
                            finalEndX = bayEnd - GenericBayList[fixtureBayIndex].XPosition;
                        }

                        Double finalWidth = finalEndX - finalX;

                        if (finalWidth > 0)
                        {
                            this.TextBoxes.Add(new SplitTextBox(textboxID, textBox, splitBay.Id, splitElement.Id, finalX, finalWidth));
                            textboxID++;
                        }
                    }

                    #endregion
                }

                //re-calculate plan element combine direction
                foreach (var elementGroup in Elements.GroupBy(e => e.ParentPlanElement.OriginalTemplateElementId))
                {
                    Int32 minBayid = elementGroup.Min(e => e.SplitBayId);
                    Int32 maxBayid = elementGroup.Max(e => e.SplitBayId);

                    foreach (SplitElement element in elementGroup)
                    {
                        // don't re-calculate the combine Direction if it was already marked in V7 as a shelf to be combined, i.e. it was a plan element with the block type equal to 10.
                        if (element.ParentPlanElement.CombineDirection != 1) 
                        {
                            element.ParentPlanElement.CombineDirection = GetCombineDirection(element.SplitBayId, minBayid, maxBayid);
                        }                        
                    }

                }

                //If the end of the current fixture bay exceeds or equals the end of the current plan bay then
                //we move on to the next plan bay.
                if ((GenericBayList[fixtureBayIndex].XPosition + GenericBayList[fixtureBayIndex].Width) >=
                 (ParentPlan.PlanBays[planBayIndex].XPosition + ParentPlan.PlanBays[planBayIndex].Width))
                {
                    planBayIndex++;
                }
            }
        }

        private byte GetCombineDirection(int splitBayId, int minBayid, int maxBayid)
        {
            //When bay is the start and end of the fixture profile section
            if (splitBayId == minBayid && splitBayId == maxBayid)
            {
                return 0; //Combine OFF
            }
            //When bay is in the middle of the fixture profile section
            else if (splitBayId > minBayid && splitBayId < maxBayid)
            {
                return 1; //Combine Both
            }
            //When bay is at the end of the fixture profile section
            else if (splitBayId > minBayid && splitBayId == maxBayid)
            {
                return 2; //Combine LEFT
            }
            //When bay is at the beginning of the fixture profile section
            else if (splitBayId == minBayid && splitBayId < maxBayid)
            {
                return 3; // Combine RIGHT
            }
            else
            {
                return 0; //Combine OFF
            }
        }

        /// <summary>
        /// Join adjacent product positions together
        /// </summary>
        /// <returns></returns>
        private List<JoinPosition> JoinPositions()
        {
            List<JoinPosition> output = new List<JoinPosition>();
            List<PlanPosition> orderedPositions = ParentPlan.PlanPositions.Where(p => p.PlanElementId > 0)
                .OrderBy(p =>
                    ParentPlan.PlanElements.FirstOrDefault(e => e.Id == p.PlanElementId).XPosition +
                    p.XPosition).OrderBy(p => p.YPosition).ToList();
            PlanPosition lastPosition = null;
            PlanElement lastElement = null;
            PlanBay lastBay = null;
            List<PlanBay> orderedBays = ParentPlan.PlanBays.OrderBy(b => b.XPosition).ToList();

            foreach (PlanPosition position in orderedPositions)
            {
                PlanElement element = ParentPlan.PlanElements.FirstOrDefault(e => e.Id == position.PlanElementId);
                PlanBay bay = ParentPlan.PlanBays.FirstOrDefault(e => element != null && e.Id == element.PlanBayId);

                Boolean isJoinable = true;

                if (element != null)
                {
                    //Set the joinability of this product position based on element type.
                    switch (PlanElementTypeHelper.InternalNameToEnum(element.ElementType))
                    {
                        case PlanElementType.Bar:
                        case PlanElementType.Peg:
                        case PlanElementType.Chest:
                            isJoinable = false;
                            break;
                        case PlanElementType.Pallet:
                        case PlanElementType.Shelf:
                            isJoinable = true;
                            break;
                    }
                }

                //Only join if we are on adjacent bays
                if (orderedBays.IndexOf(lastBay) + 1 < orderedBays.IndexOf(bay)) isJoinable = false;

                //If a product is adjacent to itself and placed on an element
                //at the same height then it can be combined into a single position.
                //***NOTE: For now the orientation must be the same for both positions.***
                if (lastPosition != null &&
                    lastPosition.ProductId == position.ProductId &&
                    lastPosition.YPosition == position.YPosition &&
                    lastPosition.OrientationId == position.OrientationId &&
                    isJoinable)
                {
                    output.Last().Add(position);
                }
                else
                {
                    output.Add(new JoinPosition(position));
                }
                lastPosition = position;
                lastElement = element;
                lastBay = bay;
            }

            return output;
        }

        /// <summary>
        /// Checks to see if any of the facings of a position will fit on the current element and places them even
        /// if it would split up the position to do so.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="splitElement"></param>
        /// <param name="planBayIndex"></param>
        /// <param name="fixtureBayIndex"></param>
        /// <param name="positionID"></param>
        /// <returns></returns>
        private Int32 SplitPositionsByBay(JoinPosition position, SplitElement splitElement, Int32 planBayIndex, Int32 fixtureBayIndex, Int32 positionID)
        {
            Int16 firstFrontFit = -1;
            Int16 lastFrontFit = -1;
            Double frontXPosition = 0;
            Double orientedFrontWidth = GetFrontFacingWidth(position);
            //Offset the position XPosition by the original bay Xposition
            Double frontStartX = position.XPosition + ParentPlan.PlanBays[planBayIndex].XPosition;
            Int32 frontWide = position.TrayProduct == 0 ? position.Wide : position.TrayCountWide;


            //check if front facings fit and populate front facing values
            Boolean frontFitsInBay = FacingSpanFitsInBay(fixtureBayIndex, frontStartX, frontWide, orientedFrontWidth,
                out firstFrontFit, out lastFrontFit, out frontXPosition);

            Int16 firstRightFit = -1;
            Int16 lastRightFit = -1;
            Double rightXPosition = 0;
            Double orientedRightCapWidth = GetRightCapWidth(position);
            Double rightCapStartX = frontStartX + (orientedFrontWidth * frontWide);
            Int32 rightWide = position.TrayProduct == 0 ? position.CapRightWide : position.CapRightWideTrayCount;

            //check if right cap facings fit and populate right cap facing values
            Boolean rightCapFitsInBay = FacingSpanFitsInBay(fixtureBayIndex, rightCapStartX, rightWide, orientedRightCapWidth,
                out firstRightFit, out lastRightFit, out rightXPosition);

            Int16 firstBreakTrayUpFit = -1;
            Int16 lastBreakTrayUpFit = -1;
            Double breakTrayUpXPosition = 0;
            Double orientedBreakTrayUpWidth = GetFrontFacingWidth(position, true);
            Double breakTrayUpCapStartX = rightCapStartX + (orientedFrontWidth * rightWide);

            //check if break tray up facings fit and populate break tray up facings values
            Boolean breakTrayUpFitsInBay = FacingSpanFitsInBay(fixtureBayIndex, rightCapStartX,
                GetBreakTrayUpWide(position), orientedBreakTrayUpWidth,
                out firstBreakTrayUpFit, out lastBreakTrayUpFit, out breakTrayUpXPosition);

            //if any facing has fit at all then we create a new position
            if (frontFitsInBay || rightCapFitsInBay || breakTrayUpFitsInBay)
            {
                Single finalX = 0;
                if (frontFitsInBay)
                {
                    finalX = (Single)frontXPosition;
                }
                else if (rightCapFitsInBay)
                {
                    finalX = (Single)rightXPosition;
                }
                else if (breakTrayUpFitsInBay)
                {
                    finalX = (Single)breakTrayUpXPosition;
                }

                finalX -= (Single)(GenericBayList[fixtureBayIndex].XPosition);

                Int16 facingsWide = 0;
                Int16 facingsRightWide = 0;
                Int16 facingsBreakTrayUpWide = 0;

                if (frontFitsInBay) facingsWide = (Int16)(lastFrontFit - firstFrontFit + 1);
                if (rightCapFitsInBay) facingsRightWide = (Int16)(lastRightFit - firstRightFit + 1);
                if (breakTrayUpFitsInBay) facingsBreakTrayUpWide = (Int16)(lastBreakTrayUpFit - firstBreakTrayUpFit + 1);

                CreateSplitPosition(positionID, splitElement.Id, position, finalX, facingsWide, facingsRightWide, facingsBreakTrayUpWide);

                positionID++;
            }

            return positionID;
        }

        /// <summary>
        /// Places an entire position onto the current element if the centre of the position is placed within the
        /// current bay/element
        /// </summary>
        /// <param name="position"></param>
        /// <param name="splitElement"></param>
        /// <param name="planBayIndex"></param>
        /// <param name="fixtureBayIndex"></param>
        /// <param name="positionID"></param>
        /// <returns></returns>
        private Int32 PlacePosition(JoinPosition position, SplitElement splitElement, Int32 planBayIndex, Int32 fixtureBayIndex, Int32 positionID)
        {
            
            //if the centre of the position is in hte bay then we create a new position
            if (!position.ElementChosen && PositionFitsInBay(position, fixtureBayIndex, planBayIndex))
            {

                Int16 facingsWide = 0;
                Int16 facingsRightWide = 0;
                Int16 facingsBreakTrayUpWide = 0;

                if (position.TrayProduct == 0 || (position.TrayWide == 0 &&  position.CapRightWideTrayCount == 0))
                {
                    facingsWide = position.Wide;
                    facingsRightWide = position.CapRightWide;
                    facingsBreakTrayUpWide = 0;
                }
                else
                {
                    facingsWide = position.TrayCountWide;
                    facingsRightWide = position.CapRightWideTrayCount;
                    //calculate units broken up based on total units wide
                    facingsBreakTrayUpWide = (Int16)(position.Wide - (facingsWide * PlanPosition.OrientDimension<Int32>(PlanPosition.Dimension.Width, position.TrayHigh, position.TrayWide, position.TrayDeep, position.OrientationId)));
                }

                 Single finalX = (Single)(position.XPosition + ParentPlan.PlanBays[planBayIndex].XPosition - GenericBayList[fixtureBayIndex].XPosition);
                

                CreateSplitPosition(positionID, splitElement.Id, position, finalX, facingsWide, facingsRightWide, facingsBreakTrayUpWide);
                
                //This position has been placed so we don't want it to be placed again.
                position.ElementChosen = true;

                positionID++;            
            }

            return positionID;
        }


        /// <summary>
        /// Gets the width that would be used up if a single
        /// front facing of the supplied position was placed.
        /// </summary>
        /// <param name="position">source data</param>
        /// <param name="ignoreTrayFlag">defaulted to false, set to true to ignore tray flag</param>
        /// <returns></returns>
        /// <example>ignoreTrayFlag should only realy be used when working with
        /// trays that have broken up or down as when dealing with broken up or down
        /// facings we do not want to use the tray width.</example>
        private Double GetFrontFacingWidth(JoinPosition position, Boolean ignoreTrayFlag = false)
        {
            if (position.TrayProduct == 0 || position.TrayWide == 0 || ignoreTrayFlag)
            {
                return PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Width,
                                position.ProductHeight, position.ProductWidth, position.ProductDepth, position.OrientationId);
            }
            else
            {
                return PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Width,
                        position.TrayHeight,
                        position.TrayWidth,
                        position.TrayDepth,
                        position.OrientationId);
            }
        }

        /// <summary>
        /// Gets the width that would be used up if a single
        /// right cap of the supplied position was placed.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private Double GetRightCapWidth(JoinPosition position)
        {
            //Product depth and width switched round before orientation to simulate
            //right cap dimensions.
            if (position.TrayProduct == 0)
            {
                return PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Width,
                                position.ProductHeight,
                                position.ProductDepth,
                                position.ProductWidth,
                                position.OrientationId);
            }
            else
            {
                return PlanPosition.OrientDimension<Double>(PlanPosition.Dimension.Width,
                        position.TrayHeight,
                        position.TrayDepth,
                        position.TrayWidth,
                        position.OrientationId);
            }
        }

        /// <summary>
        /// Gets the number of units wide that a tray position has broken up by.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private Int32 GetBreakTrayUpWide(JoinPosition position)
        {
            Int32 unitsWide = (position.TrayCountWide * PlanPosition.OrientDimension<Int32>(PlanPosition.Dimension.Width, position.TrayHigh, position.TrayWide, position.TrayDeep, position.OrientationId));
            //Tray has broken up
            if (position.TrayWide > 0 && position.Wide > unitsWide)
            {
                return (position.Wide - unitsWide);
            }

            return 0;
        }

        /// <summary>
        /// Checks to see if part of a facing fits onto the current bay. A separate check is needed for
        /// front, right and break facings.
        /// </summary>
        /// <param name="bayIndex"></param>
        /// <param name="spanStartX">start of the span</param>
        /// <param name="spanWide">number of units wide in the span</param>
        /// <param name="unitWidth">width of each unit in the span (e.g. Tray width for trays)</param>
        /// <param name="firstFit">outputs the first unit wide that fits in the bay</param>
        /// <param name="lastFit">outputs the last unit wide that fits in the bay</param>
        /// <param name="XPosition">output the XPosition of the first unit wide that fits</param>
        /// <returns>If the span fits then return will be true.</returns>
        private Boolean FacingSpanFitsInBay(Int32 bayIndex, Double spanStartX, Int32 spanWide, Double unitWidth, out Int16 firstFit, out Int16 lastFit, out Double XPosition)
        {
            Boolean output = false;
            firstFit = -1;
            lastFit = -1;
            XPosition = 0;
            for (Int16 x = 0; x < spanWide; x++)
            {
                Double facingStart = spanStartX + (unitWidth * x);
                Double facingEnd = spanStartX + (unitWidth * (x + 1));
                Double facingCentre = facingStart + (facingEnd - facingStart) / 2;
                Boolean fitsInBay = GenericBayList[bayIndex].XPosition < facingCentre &&
                   (GenericBayList[bayIndex].XPosition + GenericBayList[bayIndex].Width) >= facingCentre;

                if (fitsInBay)
                {
                    output = true;
                    if (firstFit == -1)
                    {
                        firstFit = x;
                        XPosition = facingStart;
                    }
                    lastFit = x;
                }
            }

            return output;
        }

        /// <summary>
        /// Check to see if the centre of a position fits in a fixture plan bay
        /// </summary>
        /// <param name="position">the position to check</param>
        /// <param name="fixtureBayIndex">the index to retrive the fixture plan bay info</param>
        /// <returns></returns>
        private Boolean PositionFitsInBay(JoinPosition position, Int32 fixtureBayIndex, Int32 planBayIndex)
        {
            Boolean output = false;
            Decimal tollerance = (Decimal)0.0001;

            //Use Decimal to avoid stupid rounding issues
            Decimal positionWidth = (Decimal)position.BlockWidth;
            Decimal facingStart = (Decimal)(ParentPlan.PlanBays[planBayIndex].XPosition + position.XPosition);
            Decimal facingEnd = (Decimal)ParentPlan.PlanBays[planBayIndex].XPosition + (Decimal)position.XPosition + positionWidth;
            Decimal facingCentre = (Decimal)(facingStart + (facingEnd - facingStart) / 2);
            Boolean fitsInBay = (Decimal)GenericBayList[fixtureBayIndex].XPosition - tollerance <= facingCentre &&
               ((Decimal)GenericBayList[fixtureBayIndex].XPosition + (Decimal)GenericBayList[fixtureBayIndex].Width) + tollerance >= facingCentre;

            if (fitsInBay)
            {
                output = true;
            }

            return output;
        }

        /// <summary>
        /// Creates a new split position object based on the supplied values.
        /// </summary>
        private void CreateSplitPosition(Int32 positionID, Int32 elementId, JoinPosition position, Single XPosition, Int16 frontWide, Int16 rightWide, Int16 breakUpWide)
        {
            Int16 wide = 0;
            Int16 right = 0;
            Int16 breakup = 0;
            Int16 traysWide = 0;
            Int16 traysRight = 0;

            if (position.TrayProduct == 0)
            {
                wide = frontWide;
                right = rightWide;

            }
            else
            {
                //Tray has broken down
                if (position.TrayWide == 0 && position.CapRightWideTrayCount == 0)
                {
                    wide = frontWide;
                    traysWide = 0;
                    traysRight = rightWide;
                    breakup = breakUpWide;
                }
                else
                {
                    //Add front units
                    wide = (Int16)(frontWide * PlanPosition.OrientDimension<Int32>(PlanPosition.Dimension.Width, position.TrayHigh, position.TrayWide, position.TrayDeep, position.OrientationId));
                    right = (Int16)(rightWide * PlanPosition.OrientDimension<Int32>(PlanPosition.Dimension.Depth, position.TrayHigh, position.TrayWide, position.TrayDeep, position.OrientationId));

                    traysWide = frontWide;
                    traysRight = rightWide;
                    breakup = breakUpWide;
                }
            }

            Positions.Add(new SplitPosition(positionID, elementId, position, XPosition, wide, right, breakup, traysWide, traysRight));

        }


        public void Dispose()
        {
            _parentPlan = null;
            _splitTextBoxList.ForEach(s => s.Dispose());
            _splitPositionList.ForEach(s => s.Dispose());
            _elements.ForEach(s => s.Dispose());
            _bays.ForEach(s => s.Dispose());

            _genericBayList.Clear();            
            _splitPositionList.Clear();
            _splitTextBoxList.Clear();
            _elements.Clear();
            _bays.Clear();
        }
        #endregion
    }
}
