﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Created
// CCM-20928 : L.Bailey
//    "Units In Tray" field to be used in CCM.net Publish to GFS
//      Added TrayHeight, TrayWidth, TrayDepth, TrayUnits 
// CCM-21075 : M.Brumby
//  Added Joined Position as positions now need to be joined before being split
//  to overcome ccm's split by bays.
//  CCM-21409 : M.Brumby
//      Tray data mappings updated
//  CCM-21495 : M.Brumby
//      Fixes for capped trays
#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
//  CCM-22547 : M.Brumby
//      Facing tidy up
#endregion
#region Version History: (CCM.Net 7.5.3)
//  CCM-23268 : M.Brumby
//      Made use of new TrayreakTopRightHigh/Wide/Deep values.
#endregion
#region Version History: (CCM v7.5.4)
//  CCM-23580 : M.Brumby
//      Added merge positions for Manual plans.
#endregion
#region Version History: (CCM.Net 7.5.5)
//  CCM-24484 : M.Brumby
//      Include nesting units in facings
#endregion
#region Version History: (CCM V7.5.6)
//  CCM-24910 : M.Brumby
//      Tray break up now handles no front trays.
//  CCM-25615 : D.Pleasance
//      Break tray up deep units now considered when calculating units deep.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    /// <summary>
    /// Creates a split psoition from a mixture of the supplied plan psoition and
    /// a  number of pre caculated values
    /// </summary>
    public class SplitPosition
    {
        #region Fields

        private JoinPosition _parentPlanPosition;
        private Int32 _id = -1;
        private Int32 _splitElementId = -1;
        private Int16 _wide = 0;
        private Int16 _right = 0;
        private Int16 _breakup = 0;
        private Int16 _traysWide = 0;
        private Int16 _traysRight = 0;
        private Single _XPosition = 0;

        #endregion

        #region Properties

        public Int32 ProductId
        {
            get { return _parentPlanPosition.ProductId; }
        }
        public JoinPosition ParentPlanPosition
        {
            get { return _parentPlanPosition; }
        }
        public Int32 SplitElementId
        {
            get { return _splitElementId; }
        }
        public Single XPosition
        {
            get { return _XPosition; }
        }

        public Single YPosition
        {
            get { return ParentPlanPosition.YPosition; }
        }

        public Double ProductHeight
        {
            get { return ParentPlanPosition.ProductHeight; }
        }
        public Double ProductWidth
        {
            get { return ParentPlanPosition.ProductWidth; }
        }
        public Double ProductDepth
        {
            get { return ParentPlanPosition.ProductDepth; }
        }

        public Single SqueezeHeight
        {
            get { return ParentPlanPosition.SqueezeHeight; }
        }
        public Single SqueezeWidth
        {
            get { return ParentPlanPosition.SqueezeWidth; }
        }
        public Single SqueezeDepth
        {
            get { return ParentPlanPosition.SqueezeDepth; }
        }

        public Int16 High
        {
            get { return ParentPlanPosition.High; }
        }
        public Int16 Wide
        {
            get { return (Int16)(_wide + _breakup); }
        }
        public Int16 Deep
        {
            get { return ParentPlanPosition.Deep; }
        }
        public Int16 NestHigh
        {
            get { return ParentPlanPosition.NestHigh; }
        }
        public Int16 NestWide
        {
            get { return ParentPlanPosition.NestWide; }
        }
        public Int16 NestDeep
        {
            get { return ParentPlanPosition.NestDeep; }
        }
        public Int16 CapRightCapTopHigh
        {
            get { return ParentPlanPosition.CapRightCapTopHigh; }
        }
        public Int16 CapRightCapTopDeep
        {
            get { return ParentPlanPosition.CapRightCapTopDeep; }
        }

        public Int16 CapRightWide
        {
            get { return _right; }
        }
        public Int16 CapRightDeep
        {
            get { return ParentPlanPosition.CapRightDeep; }
        }

        public Byte TrayProduct
        {
            get { return ParentPlanPosition.TrayProduct; }
        }

        public Int16 TrayCountHigh
        {
            get { return ParentPlanPosition.TrayCountHigh; }
        }
        public Int16 TrayCountWide
        {
            get { return _traysWide; }
        }
        public Int16 TrayCountDeep
        {
            get { return ParentPlanPosition.TrayCountDeep; }
        }

        public Int16 TrayHigh
        {
            get { return ParentPlanPosition.TrayHigh; }
        }
        public Int16 TrayWide
        {
            get { return ParentPlanPosition.TrayWide; }
        }
        public Int16 TrayDeep
        {
            get { return ParentPlanPosition.TrayDeep; }
        }

        public Int16 CapRightDeepTrayCount
        {
            get { return ParentPlanPosition.CapRightDeepTrayCount; }
        }
        public Int16 CapRightWideTrayCount
        {
            get { return _traysRight; }
        }

        public Int16 CapTopDeep
        {
            get { return ParentPlanPosition.CapTopDeep; }
        }
        public Int16 CapTopHigh
        {
            get { return ParentPlanPosition.CapTopHigh; }
        }

        public Int32 Colour
        {
            get { return ParentPlanPosition.Colour; }
        }
        public Int32 OrientationId
        {
            get { return ParentPlanPosition.OrientationId; }
        }

        public Int16 Units
        {
            get { return ParentPlanPosition.Units; }
        }

        public Int16 TrayUnits
        {
            get { return ParentPlanPosition.TrayUnits; }
        }

        public Single TrayHeight
        {
            get { return ParentPlanPosition.TrayHeight; }
        }

        public Single TrayDepth
        {
            get { return ParentPlanPosition.TrayDepth; }
        }

        public Single TrayWidth
        {
            get { return ParentPlanPosition.TrayWidth; }
        }

        public Single BlockWidth
        {
            get { return ParentPlanPosition.BlockWidth; }
        }

        #endregion

        #region Constructor

        public SplitPosition(Int32 id, Int32 splitElementId, JoinPosition planPosition, Single XPosition,
                        Int16 wide, Int16 right, Int16 breakup, Int16 traysWide, Int16 traysRight)
        {
            _id = id;
            _splitElementId = splitElementId;
            _parentPlanPosition = planPosition;
            _XPosition = XPosition;
            _wide = wide;
            _right = right;
            _breakup = breakup;
            _traysWide = traysWide;
            _traysRight = traysRight;
        }

        #endregion

        #region Methods
        public void Dispose()
        {
            _parentPlanPosition = null;

        }
 
        #endregion
    }

    

    /// <summary>
    /// Combines multiple planposition records into a single record.
    /// </summary>
    public class JoinPosition
    {
        #region Fields

        private List<PlanPosition> _parentPlanPositions;
        private Int32? _planElementId = -1;
        private Boolean _elementChosen = false;
        #endregion

        #region Properties

        public List<PlanPosition> ParentPlanPositions
        {
            get { return _parentPlanPositions; }
        }

        public IEnumerable<Int32> PlanElementIds
        {
            get { return ParentPlanPositions.Select(p => p.PlanElementId); }
        }

        public Int32 PlanElementId
        {
            get 
            { 
                if(!_planElementId.HasValue)
                {
                    _planElementId = ParentPlanPositions.Min(p => p.PlanElementId); 
                }
                return _planElementId.Value;
            }
            set
            {
                _planElementId = value;
            }
        }

        /// <summary>
        /// Check to see if the combined position has been assigned to an element or not.
        /// </summary>
        public Boolean ElementChosen { get { return _elementChosen; } set { _elementChosen = value; } }


        public Int32 ProductId
        {
            get { return ParentPlanPositions.Min(p => p.ProductId); }
        }

        public Double XPosition
        {           
            get 
            {

                Double positionWidths = 0;

                //Add up each block width before we find the position for the element
                //the join position belongs to. This total is then removed off the Xposition
                //of that position as an extra overhang.
                foreach (var position in ParentPlanPositions.OrderBy(p =>p.PlanElementId))
                {
                    if (position.PlanElementId == this.PlanElementId)
                    {
                        //Use Double-Decimal to fix rounding issues
                        return (Double)(Decimal)position.XPosition - positionWidths;
                    }
                    else
                    {
                        positionWidths += position.BlockWidth;
                    }
                }

                return ParentPlanPositions.FirstOrDefault(p => p.PlanElementId == this.PlanElementId).XPosition; 
            }
        }

        public Single YPosition
        {
            get { return ParentPlanPositions.Min(p => p.YPosition); }
        }

        public Single BlockWidth
        {
            get { return ParentPlanPositions.Sum(p => p.BlockWidth); }
        }

        public Double ProductHeight
        {
            get { return ParentPlanPositions.Min(p => p.ProductHeight); }
        }
        public Double ProductWidth
        {
            get { return ParentPlanPositions.Min(p => p.ProductWidth); }
        }
        public Double ProductDepth
        {
            get { return ParentPlanPositions.Min(p => p.ProductDepth); }
        }

        public Single SqueezeHeight
        {
            get { return ParentPlanPositions.Max(p => p.SqueezeHeight); }
        }
        public Single SqueezeWidth
        {
            get { return ParentPlanPositions.Max(p => p.SqueezeWidth); }
        }
        public Single SqueezeDepth
        {
            get { return ParentPlanPositions.Max(p => p.SqueezeDepth); }
        }

        public Int16 High
        {
            get { return ParentPlanPositions.Max(p => p.High); }
        }
        public Int16 Wide
        {
            get { return (Int16)ParentPlanPositions.Sum(p => p.Wide); }
        }
        public Int16 Deep
        {
            get { return ParentPlanPositions.Max(p => p.Deep); }
        }
        public Int16 NestHigh
        {
            get { return ParentPlanPositions.Max(p => p.NestHigh); }
        }
        public Int16 NestWide
        {
            get { return ParentPlanPositions.Max(p => p.NestWide); }
        }
        public Int16 NestDeep
        {
            get { return ParentPlanPositions.Max(p => p.NestDeep); }
        }
        public Int16 CapRightCapTopHigh
        {
            get { return ParentPlanPositions.Max(p => p.CapRightCapTopHigh); }
        }
        public Int16 CapRightCapTopDeep
        {
            get { return ParentPlanPositions.Max(p => p.CapRightCapTopDeep); }
        }

        public Int16 CapRightWide
        {
            get { return (Int16)ParentPlanPositions.Sum(p => p.CapRightWide); }
        }
        public Int16 CapRightDeep
        {
            get { return ParentPlanPositions.Max(p => p.CapRightDeep); }
        }

        public Byte TrayProduct
        {
            get { return ParentPlanPositions.Max(p => p.TrayProduct); }
        }

        public Int16 TrayCountHigh
        {
            get { return ParentPlanPositions.Max(p => p.TrayCountHigh); }
        }
        public Int16 TrayCountWide
        {
            get { return (Int16)ParentPlanPositions.Sum(p => p.TrayCountWide); }
        }
        public Int16 TrayCountDeep
        {
            get { return ParentPlanPositions.Max(p => p.TrayCountDeep); }
        }

        public Int16 TrayHigh
        {
            get { return ParentPlanPositions.Max(p => p.TrayHigh); }
        }
        public Int16 TrayWide
        {
            get { return ParentPlanPositions.Max(p => p.TrayWide); }
        }
        public Int16 TrayDeep
        {
            get { return ParentPlanPositions.Max(p => p.TrayDeep); }
        }

        public Int16 CapRightDeepTrayCount
        {
            get { return ParentPlanPositions.Max(p => p.CapRightDeepTrayCount); }
        }
        public Int16 CapRightWideTrayCount
        {
            get { return (Int16)ParentPlanPositions.Sum(p => p.CapRightWideTrayCount); }
        }

        public Byte BreakTrayUp
        {
            get { return (Byte)ParentPlanPositions.Sum(p => p.BreakTrayUp); }
        }

        public Byte BreakTrayDown
        {
            get { return (Byte)ParentPlanPositions.Sum(p => p.BreakTrayDown); }
        }

        public Int16 CapTopDeep
        {
            get { return ParentPlanPositions.Max(p => p.CapTopDeep); }
        }
        public Int16 CapTopHigh
        {
            get { return ParentPlanPositions.Max(p => p.CapTopHigh); }
        }

        public Int32 Colour
        {
            get { return ParentPlanPositions.Max(p => p.Colour); }
        }
        public Int32 OrientationId
        {
            get { return ParentPlanPositions.Max(p => p.OrientationId); }
        }

        //TODO!
        public Int16 Units
        {
            get { return (Int16)ParentPlanPositions.Sum(p => p.Units); }
        }

        //TODO!
        public Int16 TrayUnits
        {
            get { return ParentPlanPositions.Max(p => p.TrayUnits); }
        }

        public Single TrayHeight
        {
            get { return ParentPlanPositions.Max(p => p.TrayHeight); }
        }

        public Single TrayDepth
        {
            get { return ParentPlanPositions.Max(p => p.TrayDepth); }
        }

        public Single TrayWidth
        {
            get { return ParentPlanPositions.Max(p => p.TrayWidth); }
        }

        #endregion

        #region Constructor

        public JoinPosition(PlanPosition planPosition)
        {
            _parentPlanPositions = new List<PlanPosition>();
            _parentPlanPositions.Add(planPosition);
        }

        #endregion

        #region Methods

        internal void Add(PlanPosition planPosition)
        {
            _parentPlanPositions.Add(planPosition);
        }

        public void Dispose()
        {
            _parentPlanPositions.Clear();
        }

        #endregion

        #region Overrides
        public override string ToString()
        {
            return String.Format("ProductID = {0}", this.ProductId);
        }
        #endregion
    }
}
