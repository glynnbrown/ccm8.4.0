﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Created
#endregion

#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
//  CCM-22561 : M.Brumby
//      Set SubComponentType
//      Preserve space for shelves now ignores CCM YPosition in favour of
//      working it out ourselves.
#endregion

#region Version History: (CCM v7.5.4)
//  CCM-23527 : M.Brumby
//      Changed chest product position order.
//  CCM-23815 : D.Pleasance
//      Added new method to create sequence for chest \ peg plans.
//  CCM-23842 : M.Brumby
//      Product offset for chests was set incorrectly.
//  CCM-23580 : M.Brumby
//      Added merge positions for Manual plans.
#endregion

#region Version History: (CCM v7.5.5)
//  CCM-24778 : M.Toomer
//      Changed GetChestSubComponents to match PlanElement GetChestSubComponents.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Legacy.Model
{
    /// <summary>
    /// Creates a split element from a mixture of the supplied plan element and
    /// fixtureplan bay
    /// </summary>
    public class SplitElement
    {
        #region Fields

        private Int32 _id = -1;
        private Int32 _splitBayId = -1;
        private PlanElement _parentPlanElement;
        private GenericBay _parentGenericBay;

        #endregion

        #region Properties

        public Int32 Id
        {
            get { return _id; }
        }
        public Int32 SplitBayId
        {
            get { return _splitBayId; }
        }

        public PlanElement ParentPlanElement
        {
            get { return _parentPlanElement; }
        }
        public GenericBay ParentGenericBay
        {
            get { return _parentGenericBay; }
        }

        public String Name
        {
            get { return ParentPlanElement.Name; }
        }

        public String ElementType
        {
            get { return ParentPlanElement.ElementType; }
        }

        public Single YPosition
        {
            get { return (Single)ParentPlanElement.YPosition; }
        }

        public Single ZPosition
        {
            get { return (Single)ParentPlanElement.ZPosition; }
        }

        #region Shelf
        public Single ShelfHeight
        {
            get { return (Single)ParentPlanElement.ShelfHeight; }
        }
        public Single ShelfDepth
        {
            get { return (Single)ParentPlanElement.ShelfDepth; }
        }
        public Single ShelfWidth
        {
            get
            {
                if (ParentGenericBay.UseOriginalElementX)
                {
                    return (Single)ParentPlanElement.ShelfWidth;
                }
                return (Single)ParentGenericBay.Width;
            }
        }
        public Single ShelfRiser
        {
            get { return (Single)ParentPlanElement.ShelfRiser; }
        }
        public Single ShelfThick
        {
            get { return (Single)ParentPlanElement.ShelfThick; }
        }
        public Single ShelfSlope
        {
            get { return (Single)ParentPlanElement.ShelfSlope; }
        }
        #endregion

        #region Bar
        public Single BarHeight
        {
            get { return (Single)ParentPlanElement.BarHeight; }
        }
        public Single BarDepth
        {
            get { return (Single)ParentPlanElement.BarDepth; }
        }
        public Single BarWidth
        {
            get
            {
                if (ParentGenericBay.UseOriginalElementX)
                {
                    return (Single)ParentPlanElement.BarWidth;
                }
                return (Single)ParentGenericBay.Width;
            }
        }
        public Single BarThick
        {
            get { return (Single)ParentPlanElement.BarThick; }
        }
        #endregion

        #region Peg
        public Single PegHeight
        {
            get { return (Single)ParentPlanElement.PegHeight; }
        }
        public Single PegDepth
        {
            get { return (Single)ParentPlanElement.PegDepth; }
        }
        public Single PegWidth
        {
            get
            {
                if (ParentGenericBay.UseOriginalElementX)
                {
                    return (Single)ParentPlanElement.PegWidth;
                }
                return (Single)ParentGenericBay.Width;
            }
        }
        #endregion

        #region Chest
        public Single ChestHeight
        {
            get { return (Single)ParentPlanElement.ChestHeight; }
        }
        public Single ChestDepth
        {
            get { return (Single)ParentPlanElement.ChestDepth; }
        }
        public Single ChestWidth
        {
            get
            {
                if (ParentGenericBay.UseOriginalElementX)
                {
                    return (Single)ParentPlanElement.ChestWidth;
                }
                return (Single)ParentGenericBay.Width;
            }
        }
        public Single ChestInside
        {
            get { return (Single)ParentPlanElement.ChestInside; }
        }
        public Single ChestMerchandising
        {
            get { return (Single)ParentPlanElement.ChestMerchandising; }
        }
        public Single ChestWall
        {
            get { return (Single)ParentPlanElement.ChestWall; }
        }
        #endregion
        #endregion

        #region Constructor

        public SplitElement(Int32 id, Int32 splitBayId, PlanElement planElement, GenericBay genericBay)
        {
            _id = id;
            _splitBayId = splitBayId;
            _parentPlanElement = planElement;
            _parentGenericBay = genericBay;

        }
        #endregion

        #region Methods

        public void Dispose()
        {
            _parentGenericBay = null;
            _parentPlanElement = null;
        }

        #endregion
    }
}
