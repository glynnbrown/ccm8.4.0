﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanPositionList : ModelList<PlanPositionList, PlanPosition>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //BusinessRules.AddRule(typeof(PlanPositionList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.CategoryFetch.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class GetPlanPositionsByPlanIdCriteria : Csla.CriteriaBase<GetPlanPositionsByPlanIdCriteria>
        {
            public static readonly PropertyInfo<Int32> PlanIdProperty =
            RegisterProperty<Int32>(c => c.PlanId);
            public Int32 PlanId
            {
                get { return ReadProperty<Int32>(PlanIdProperty); }
            }

            public static readonly PropertyInfo<PlanType> PlanTypeProperty =
            RegisterProperty<PlanType>(c => c.PlanType);
            public PlanType PlanType
            {
                get { return ReadProperty<PlanType>(PlanTypeProperty); }
            }

            public static readonly PropertyInfo<PlanElementList> PlanElementsProperty =
            RegisterProperty<PlanElementList>(c => c.PlanElements);
            public PlanElementList PlanElements
            {
                get { return ReadProperty<PlanElementList>(PlanElementsProperty); }
            }

            public GetPlanPositionsByPlanIdCriteria(Int32 planId, PlanType planType, PlanElementList elements)
            {
                LoadProperty<Int32>(PlanIdProperty, planId);
                LoadProperty<PlanType>(PlanTypeProperty, planType);
                LoadProperty<PlanElementList>(PlanElementsProperty, elements);
            }
        }

        #endregion
    }
}
