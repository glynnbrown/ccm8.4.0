﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 7.5)
// CCM-20762 : M.Brumby
//  Added update
#endregion
#region Version History: (CCM.Net 7.5.1)
//  CCM-22192 : M.Brumby
//      Added StoreCode so that for store specific plans the location code 
//      field can be populated when publishing to GFS
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanProfile
    {
        #region Constructor
        private PlanProfile() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanProfile GetPlanProfile(IDalContext dalContext, PlanProfileDto dto)
        {
            return DataPortal.FetchChild<PlanProfile>(dalContext, dto);
        }

        #endregion Factory Methods

        #region Data Transfer Objects
        
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanProfileDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanIdProperty, dto.PlanId);
            this.LoadProperty<Int32>(ProfileDetailIdProperty, dto.ProfileDetailId);
            this.LoadProperty<Byte>(ProfileGroupTypeProperty, dto.ProfileGroupType);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Boolean>(IsActiveProperty, dto.IsActive);
            this.LoadProperty<Int16>(PriorityProperty, dto.Priority);
            this.LoadProperty<Int16>(RatioProperty, dto.Ratio);
            this.LoadProperty<Single>(PercentageProperty, dto.Percentage);
            this.LoadProperty<Single>(MinimumProperty, dto.Minimum);
            this.LoadProperty<Single>(MaximumProperty, dto.Maximum);
            this.LoadProperty<Int16>(RatioLowestProperty, dto.RatioLowest);
            this.LoadProperty<Int16>(RatioHighestProperty, dto.RatioHighest);
            this.LoadProperty<String>(KeyFieldProperty, dto.KeyField);
            this.LoadProperty<Int32>(ProfileGroupValueProperty, dto.ProfileGroupValue);
        }

        #endregion Data Transfer Objects

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanProfileDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion Fetch
    }
}
