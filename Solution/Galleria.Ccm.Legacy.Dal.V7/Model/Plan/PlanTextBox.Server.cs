﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-16377 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Added FixturePlanBayNumber
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanTextBox
    {
        #region Constructor
        private PlanTextBox() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanTextBox GetPlanTextBox(IDalContext dalContext, PlanTextBoxDto dto)
        {
            return DataPortal.FetchChild<PlanTextBox>(dalContext, dto);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanTextBoxDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanBayIdProperty, dto.PlanBayId);
            this.LoadProperty<Int32>(PlanElementIdProperty, dto.PlanElementId);
            this.LoadProperty<Int32>(FixturePlanBayNumberProperty, dto.FixturePlanBayNumber);
            this.LoadProperty<Double>(XPositionProperty, dto.XPosition);
            this.LoadProperty<Double>(YPositionProperty, dto.YPosition);
            this.LoadProperty<Double>(ZPositionProperty, dto.ZPosition);
            this.LoadProperty<Double>(HeightProperty, dto.Height);
            this.LoadProperty<Double>(WidthProperty, dto.Width);
            this.LoadProperty<Double>(DepthProperty, dto.Depth);
            this.LoadProperty<PlanTexboxType>(TypeProperty, (PlanTexboxType)Enum.Parse(typeof(PlanTexboxType),dto.Type.ToString()));
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanTextBoxDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion
    }
}
