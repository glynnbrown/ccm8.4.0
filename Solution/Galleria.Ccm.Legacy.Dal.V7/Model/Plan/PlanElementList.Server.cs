﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanElementList
    {
        #region Constructors
        private PlanElementList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanElementList GetPlanElementsByPlanId(Int32 planId, PlanType planType)
        {
            return DataPortal.Fetch<PlanElementList>(new GetPlanElementsByPlanIdCriteria(planId, planType));
        }
        #endregion

        #region Data Access


        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetPlanElementsByPlanIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanElementDal dal = dalContext.GetDal<IPlanElementDal>())
                {
                    IEnumerable<PlanElementDto> dtoList;

                    switch (criteria.PlanType)
                    {
                        case PlanType.Manual:
                            dtoList = dal.FetchManualByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Merchandising:
                            dtoList = dal.FetchMerchandisingByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Cluster:
                            dtoList = dal.FetchClusterByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Store:
                            dtoList = dal.FetchStoreByPlanId(criteria.PlanId);
                            break;
                        default:
                            dtoList = null;
                            break;
                    }
                    if (dtoList != null)
                    {
                        foreach (PlanElementDto dto in dtoList)
                        {
                            this.Add(PlanElement.GetPlanElement(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
