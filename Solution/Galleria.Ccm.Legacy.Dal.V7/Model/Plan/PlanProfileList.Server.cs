﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanProfileList
    {
        #region Constructors
        private PlanProfileList() { } // force the use of factory methods
        #endregion


        #region Factory Methods
        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanProfileList GetPlanProfileRowsByPlanId(Int32 planId, PlanType planType)
        {
            return DataPortal.Fetch<PlanProfileList>(new GetPlanProfileRowsByPlanIdCriteria(planId, planType));
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetPlanProfileRowsByPlanIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProfileDal dal = dalContext.GetDal<IPlanProfileDal>())
                {
                    IEnumerable<PlanProfileDto> dtoList;

                    switch (criteria.PlanType)
                    {
                        case PlanType.Manual:
                            dtoList = dal.FetchManualByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Merchandising:
                            dtoList = dal.FetchMerchandisingByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Cluster:
                            dtoList = dal.FetchClusterByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Store:
                            dtoList = dal.FetchStoreByPlanId(criteria.PlanId);
                            break;
                        default:
                            dtoList = null;
                            break;
                    }
                    if (dtoList != null)
                    {
                        foreach (PlanProfileDto dto in dtoList)
                        {
                            this.Add(PlanProfile.GetPlanProfile(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
