﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-16377 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanTextBoxList
    {
        #region Constructors
        private PlanTextBoxList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanTextBoxList GetPlanTextBoxsByPlanId(Int32 planId, PlanType planType)
        {
            return DataPortal.Fetch<PlanTextBoxList>(new GetPlanTextBoxsByPlanIdCriteria(planId, planType));
        }
        #endregion

        #region Data Access


        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetPlanTextBoxsByPlanIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanTextBoxDal dal = dalContext.GetDal<IPlanTextBoxDal>())
                {
                    IEnumerable<PlanTextBoxDto> dtoList;

                    switch (criteria.PlanType)
                    {
                        case PlanType.Manual:
                            dtoList = dal.FetchManualByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Merchandising:
                            dtoList = dal.FetchMerchandisingByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Cluster:
                            dtoList = dal.FetchClusterByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Store:
                            dtoList = dal.FetchStoreByPlanId(criteria.PlanId);
                            break;
                        default:
                            dtoList = null;
                            break;
                    }
                    if (dtoList != null)
                    {
                        foreach (PlanTextBoxDto dto in dtoList)
                        {
                            this.Add(PlanTextBox.GetPlanTextBox(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
