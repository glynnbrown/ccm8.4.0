﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 7.4)
// CCM-18877 : D.Pleasance
//  Plan changes so that CCM Bay information is a fixture rather than assembly
// CCM-18767 : G.Haime
//  Truncate PlanogramFixture.Name to 50 characters to match business rule in GFS (GetFixture)
#endregion
#region Version History: (CCM 7.5.5)
// CCM-24699 : M.Brumby
//  Backboard fixture assembly id now set from helper
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;



namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanBay : ModelObject<PlanBay>
    {
        #region Properties
        #region Base Plan
        /// <summary>
        /// Id (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
        RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get
            {
                return GetProperty<Int32>(IdProperty);
            }
        }

        /// <summary>
        /// PlanId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanId);
        public Int32 PlanId
        {
            get
            {
                return GetProperty<Int32>(PlanIdProperty);
            }
        }

        /// <summary>
        /// Name (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
        RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get
            {
                return GetProperty<String>(NameProperty);
            }
        }

        /// <summary>
        /// XPosition (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> XPositionProperty =
        RegisterModelProperty<Double>(c => c.XPosition);
        public Double XPosition
        {
            get
            {
                return GetProperty<Double>(XPositionProperty);
            }
        }

        /// <summary>
        /// BaseDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BaseDepthProperty =
        RegisterModelProperty<Double>(c => c.BaseDepth);
        public Double BaseDepth
        {
            get
            {
                return GetProperty<Double>(BaseDepthProperty);
            }
        }

        /// <summary>
        /// BaseHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BaseHeightProperty =
        RegisterModelProperty<Double>(c => c.BaseHeight);
        public Double BaseHeight
        {
            get
            {
                return GetProperty<Double>(BaseHeightProperty);
            }
        }

        /// <summary>
        /// BaseWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BaseWidthProperty =
        RegisterModelProperty<Double>(c => c.BaseWidth);
        public Double BaseWidth
        {
            get
            {
                return GetProperty<Double>(BaseWidthProperty);
            }
        }

        /// <summary>
        /// Height (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> HeightProperty =
        RegisterModelProperty<Double>(c => c.Height);
        public Double Height
        {
            get
            {
                return GetProperty<Double>(HeightProperty);
            }
        }

        /// <summary>
        /// Width (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> WidthProperty =
        RegisterModelProperty<Double>(c => c.Width);
        public Double Width
        {
            get
            {
                return GetProperty<Double>(WidthProperty);
            }
        }

        /// <summary>
        /// Depth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> DepthProperty =
        RegisterModelProperty<Double>(c => c.Depth);
        public Double Depth
        {
            get
            {
                return GetProperty<Double>(DepthProperty);
            }
        }

        /// <summary>
        /// Text1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
        RegisterModelProperty<String>(c => c.Text1);
        public String Text1
        {
            get
            {
                return GetProperty<String>(Text1Property);
            }
        }

        /// <summary>
        /// Text2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
        RegisterModelProperty<String>(c => c.Text2);
        public String Text2
        {
            get
            {
                return GetProperty<String>(Text2Property);
            }
        }

        /// <summary>
        /// Text3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
        RegisterModelProperty<String>(c => c.Text3);
        public String Text3
        {
            get
            {
                return GetProperty<String>(Text3Property);
            }
        }

        /// <summary>
        /// Text4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
        RegisterModelProperty<String>(c => c.Text4);
        public String Text4
        {
            get
            {
                return GetProperty<String>(Text4Property);
            }
        }

        /// <summary>
        /// Text5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
        RegisterModelProperty<String>(c => c.Text5);
        public String Text5
        {
            get
            {
                return GetProperty<String>(Text5Property);
            }
        }

        /// <summary>
        /// Number1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number1Property =
        RegisterModelProperty<Double>(c => c.Number1);
        public Double Number1
        {
            get
            {
                return GetProperty<Double>(Number1Property);
            }
        }

        /// <summary>
        /// Number2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number2Property =
        RegisterModelProperty<Double>(c => c.Number2);
        public Double Number2
        {
            get
            {
                return GetProperty<Double>(Number2Property);
            }
        }

        /// <summary>
        /// Number3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number3Property =
        RegisterModelProperty<Int32>(c => c.Number3);
        public Int32 Number3
        {
            get
            {
                return GetProperty<Int32>(Number3Property);
            }
        }

        /// <summary>
        /// Number4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number4Property =
        RegisterModelProperty<Int32>(c => c.Number4);
        public Int32 Number4
        {
            get
            {
                return GetProperty<Int32>(Number4Property);
            }
        }

        /// <summary>
        /// Number5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number5Property =
        RegisterModelProperty<Int32>(c => c.Number5);
        public Int32 Number5
        {
            get
            {
                return GetProperty<Int32>(Number5Property);
            }
        }

        /// <summary>
        /// SourceSegments (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceSegmentsProperty =
        RegisterModelProperty<String>(c => c.SourceSegments);
        public String SourceSegments
        {
            get
            {
                return GetProperty<String>(SourceSegmentsProperty);
            }
        }

        /// <summary>
        /// DestinationSegments (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> DestinationSegmentsProperty =
        RegisterModelProperty<String>(c => c.DestinationSegments);
        public String DestinationSegments
        {
            get
            {
                return GetProperty<String>(DestinationSegmentsProperty);
            }
        }

        /// <summary>
        /// Created (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> CreatedProperty =
        RegisterModelProperty<DateTime>(c => c.Created);
        public DateTime Created
        {
            get
            {
                return GetProperty<DateTime>(CreatedProperty);
            }
        }

        /// <summary>
        /// Modified (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> ModifiedProperty =
        RegisterModelProperty<DateTime>(c => c.Modified);
        public DateTime Modified
        {
            get
            {
                return GetProperty<DateTime>(ModifiedProperty);
            }
        }

        /// <summary>
        /// PersonnelId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PersonnelIdProperty =
        RegisterModelProperty<Int32>(c => c.PersonnelId);
        public Int32 PersonnelId
        {
            get
            {
                return GetProperty<Int32>(PersonnelIdProperty);
            }
        }

        /// <summary>
        /// PersonnelFlexi (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PersonnelFlexiProperty =
        RegisterModelProperty<String>(c => c.PersonnelFlexi);
        public String PersonnelFlexi
        {
            get
            {
                return GetProperty<String>(PersonnelFlexiProperty);
            }
        }

        /// <summary>
        /// CompanyId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CompanyIdProperty =
        RegisterModelProperty<Int32>(c => c.CompanyId);
        public Int32 CompanyId
        {
            get
            {
                return GetProperty<Int32>(CompanyIdProperty);
            }
        }
        #endregion

        #region Space Plan
        /// <summary>
        /// StoreId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> StoreIdProperty =
        RegisterModelProperty<Int32>(c => c.StoreId);
        public Int32 StoreId
        {
            get
            {
                return GetProperty<Int32>(StoreIdProperty);
            }
        }

        /// <summary>
        /// FixtureId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixtureIdProperty =
        RegisterModelProperty<Int32>(c => c.FixtureId);
        public Int32 FixtureId
        {
            get
            {
                return GetProperty<Int32>(FixtureIdProperty);
            }
        }
        #endregion

        #region Manual Plan

        /// <summary>
        /// Segments (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> SegmentsProperty =
        RegisterModelProperty<String>(c => c.Segments);
        public String Segments
        {
            get
            {
                return GetProperty<String>(SegmentsProperty);
            }
        }

        #endregion

        #endregion

        #region Helper Properties

        public List<Single> SegmentList
        {
            get
            {
                return new List<Single>(this
                .DestinationSegments.Split('|')
                .Select(s => Single.Parse(s)));
            }
        }

        #endregion

        #region Method

        
        #endregion
    }
}
