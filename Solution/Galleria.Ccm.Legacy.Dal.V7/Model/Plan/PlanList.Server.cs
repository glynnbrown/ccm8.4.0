﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class PlanList
    {
        #region Constructors
        private PlanList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanList GetAllPlans()
        {
            return DataPortal.Fetch<PlanList>();
        }

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanList GetAllPlans(PlanType planType)
        {
            return DataPortal.Fetch<PlanList>(new SingleCriteria<PlanType>(planType));
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch()
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanDal dal = dalContext.GetDal<IPlanDal>())
                {
                    IEnumerable<PlanDto> dtoList = dal.FetchAll();
                    foreach (PlanDto dto in dtoList)
                    {
                        this.Add(Plan.GetPlan(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<PlanType> planType)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanDal dal = dalContext.GetDal<IPlanDal>())
                {
                    IEnumerable<PlanDto> dtoList;

                    switch (planType.Value)
                    {
                        case PlanType.Manual:
                            dtoList = dal.FetchAllManual();
                            break;
                        case PlanType.Merchandising:
                            dtoList = dal.FetchAllMerchandising();
                            break;
                        case PlanType.Cluster:
                            dtoList = dal.FetchAllCluster();
                            break;
                        case PlanType.Store:
                            dtoList = dal.FetchAllStore();
                            break;
                        default:
                            dtoList = dal.FetchAll();
                            break;
                    }

                    foreach (PlanDto dto in dtoList)
                    {
                        this.Add(Plan.GetPlan(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
