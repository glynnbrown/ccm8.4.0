﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
// CCM-17518 : M.Brumby
//  Updated XYZ postion mappings
#endregion
#region Version History: (CCM.Net 1.0.1 - CCM730 PAtch 1)
// CCM-17824  : M.Brumby
//  Tray Front/Right Top cap values need publishing
#endregion
#region Version History: (CCM.Net 7.4)
// CCM-18877 : D.Pleasance
//  Plan changes so that CCM Bay information is a fixture rather than assembly
#endregion
#region Version History: (CCM.Net 7.5)
// CCM-20525 : M.Brumby
//  Units wide now includes all units wide not just front facings
// CCM-20928 : L.Bailey
//    "Units In Tray" field to be used in CCM.net Publish to GFS
//      Added TrayHeight, TrayWidth, TrayDepth, TrayUnits 
//  CCM-21409 : M.Brumby
//      Tray data mappings updated
//  CCM-21495 : M.Brumby
//      Fixes for capped trays
#endregion
#region Version History: (CCM.Net 7.5.2)
//  CCM-22483 : M.Brumby
//      Chest fixes
//  CCM-22547 : M.Brumby
//      Facing tidy up
#endregion
#region Version History: (CCM.Net 7.5.3)
//  CCM-22944 : M.Brumby
//      Added break tray top
//  CCM-23268 : M.Brumby
//      Made use of new TrayreakTopRightHigh/Wide/Deep values.
#endregion
#region Version History: (CCM.Net 7.5.5)
//  CCM-24484 : M.Brumby
//      Include nesting units in facings
#endregion
#region Version History: (CCM V7.5.6)
//  CCM-24910 : M.Brumby
//      Tray break up now handles no front trays.
//  CCM-25615 : D.Pleasance
//      Break tray up deep units now considered when calculating units deep.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;


namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class PlanPosition : ModelObject<PlanPosition>
    {
        public enum Dimension
        {
            Height,
            Width,
            Depth
        };

        public PlanPositionList ParentList
        {
            get
            {
                if (this.Parent != null)
                {
                    if (this.Parent is PlanPositionList)
                    {
                        return this.Parent as PlanPositionList;
                    }
                }

                return null;
            }

        }
        public Plan ParentPlan
        {
            get
            {
                if (ParentList != null)
                {
                    if (this.ParentList.Parent is Plan)
                    {
                        return this.ParentList.Parent as Plan;
                    }
                }

                return null;
            }
        }

        #region Position

        #region Base
        /// <summary>
        /// Id (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
        RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get
            {
                return GetProperty<Int32>(IdProperty);
            }
        }

        /// <summary>
        /// PlanId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanId);
        public Int32 PlanId
        {
            get
            {
                return GetProperty<Int32>(PlanIdProperty);
            }
        }

        /// <summary>
        /// PlanElementId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanElementIdProperty =
        RegisterModelProperty<Int32>(c => c.PlanElementId);
        public Int32 PlanElementId
        {
            get
            {
                return GetProperty<Int32>(PlanElementIdProperty);
            }
        }

        /// <summary>
        /// ProductId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
        RegisterModelProperty<Int32>(c => c.ProductId);
        public Int32 ProductId
        {
            get
            {
                return GetProperty<Int32>(ProductIdProperty);
            }
        }

        /// <summary>
        /// OrientationId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> OrientationIdProperty =
        RegisterModelProperty<Int32>(c => c.OrientationId);
        public Int32 OrientationId
        {
            get
            {
                return GetProperty<Int32>(OrientationIdProperty);
            }
        }

        /// <summary>
        /// XPosition (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XPositionProperty =
        RegisterModelProperty<Single>(c => c.XPosition);
        public Single XPosition
        {
            get
            {
                return GetProperty<Single>(XPositionProperty);
            }
        }

        /// <summary>
        /// YPosition (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YPositionProperty =
        RegisterModelProperty<Single>(c => c.YPosition);
        public Single YPosition
        {
            get
            {
                return GetProperty<Single>(YPositionProperty);
            }
        }

        /// <summary>
        /// Deep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> DeepProperty =
        RegisterModelProperty<Int16>(c => c.Deep);
        public Int16 Deep
        {
            get
            {
                return GetProperty<Int16>(DeepProperty);
            }
        }

        /// <summary>
        /// High (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> HighProperty =
        RegisterModelProperty<Int16>(c => c.High);
        public Int16 High
        {
            get
            {
                return GetProperty<Int16>(HighProperty);
            }
        }

        /// <summary>
        /// Wide (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> WideProperty =
        RegisterModelProperty<Int16>(c => c.Wide);
        public Int16 Wide
        {
            get
            {
                return GetProperty<Int16>(WideProperty);
            }
        }

        /// <summary>
        /// ProductHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ProductHeightProperty =
        RegisterModelProperty<Double>(c => c.ProductHeight);
        public Double ProductHeight
        {
            get
            {
                return GetProperty<Double>(ProductHeightProperty);
            }
        }

        /// <summary>
        /// ProductWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ProductWidthProperty =
        RegisterModelProperty<Double>(c => c.ProductWidth);
        public Double ProductWidth
        {
            get
            {
                return GetProperty<Double>(ProductWidthProperty);
            }
        }

        /// <summary>
        /// ProductDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> ProductDepthProperty =
        RegisterModelProperty<Double>(c => c.ProductDepth);
        public Double ProductDepth
        {
            get
            {
                return GetProperty<Double>(ProductDepthProperty);
            }
        }

        /// <summary>
        /// Units (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> UnitsProperty =
        RegisterModelProperty<Int16>(c => c.Units);
        public Int16 Units
        {
            get
            {
                return GetProperty<Int16>(UnitsProperty);
            }
        }

        /// <summary>
        /// SqueezeHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SqueezeHeightProperty =
        RegisterModelProperty<Single>(c => c.SqueezeHeight);
        public Single SqueezeHeight
        {
            get
            {
                return GetProperty<Single>(SqueezeHeightProperty);
            }
        }

        /// <summary>
        /// SqueezeWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SqueezeWidthProperty =
        RegisterModelProperty<Single>(c => c.SqueezeWidth);
        public Single SqueezeWidth
        {
            get
            {
                return GetProperty<Single>(SqueezeWidthProperty);
            }
        }

        /// <summary>
        /// SqueezeDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SqueezeDepthProperty =
        RegisterModelProperty<Single>(c => c.SqueezeDepth);
        public Single SqueezeDepth
        {
            get
            {
                return GetProperty<Single>(SqueezeDepthProperty);
            }
        }

        /// <summary>
        /// BlockHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BlockHeightProperty =
        RegisterModelProperty<Single>(c => c.BlockHeight);
        public Single BlockHeight
        {
            get
            {
                return GetProperty<Single>(BlockHeightProperty);
            }
        }

        /// <summary>
        /// BlockWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BlockWidthProperty =
        RegisterModelProperty<Single>(c => c.BlockWidth);
        public Single BlockWidth
        {
            get
            {
                return GetProperty<Single>(BlockWidthProperty);
            }
        }

        /// <summary>
        /// BlockDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BlockDepthProperty =
        RegisterModelProperty<Single>(c => c.BlockDepth);
        public Single BlockDepth
        {
            get
            {
                return GetProperty<Single>(BlockDepthProperty);
            }
        }

        /// <summary>
        /// NestHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> NestHeightProperty =
        RegisterModelProperty<Single?>(c => c.NestHeight);
        public Single? NestHeight
        {
            get
            {
                return GetProperty<Single?>(NestHeightProperty);
            }
        }

        /// <summary>
        /// NestWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> NestWidthProperty =
        RegisterModelProperty<Single?>(c => c.NestWidth);
        public Single? NestWidth
        {
            get
            {
                return GetProperty<Single?>(NestWidthProperty);
            }
        }

        /// <summary>
        /// NestDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> NestDepthProperty =
        RegisterModelProperty<Single?>(c => c.NestDepth);
        public Single? NestDepth
        {
            get
            {
                return GetProperty<Single?>(NestDepthProperty);
            }
        }

        /// <summary>
        /// LeadingDivider (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LeadingDividerProperty =
        RegisterModelProperty<Single>(c => c.LeadingDivider);
        public Single LeadingDivider
        {
            get
            {
                return GetProperty<Single>(LeadingDividerProperty);
            }
        }

        /// <summary>
        /// LeadingGap (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LeadingGapProperty =
        RegisterModelProperty<Single>(c => c.LeadingGap);
        public Single LeadingGap
        {
            get
            {
                return GetProperty<Single>(LeadingGapProperty);
            }
        }

        /// <summary>
        /// LeadingGapVert (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LeadingGapVertProperty =
        RegisterModelProperty<Single>(c => c.LeadingGapVert);
        public Single LeadingGapVert
        {
            get
            {
                return GetProperty<Single>(LeadingGapVertProperty);
            }
        }

        /// <summary>
        /// CapTopDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapTopDeepProperty =
        RegisterModelProperty<Int16>(c => c.CapTopDeep);
        public Int16 CapTopDeep
        {
            get
            {
                return GetProperty<Int16>(CapTopDeepProperty);
            }
        }

        /// <summary>
        /// CapTopHigh (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapTopHighProperty =
        RegisterModelProperty<Int16>(c => c.CapTopHigh);
        public Int16 CapTopHigh
        {
            get
            {
                return GetProperty<Int16>(CapTopHighProperty);
            }
        }


        #region Cap right Cap Top
        /// <summary>
        /// CapRightCapTopHigh
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRightCapTopHighProperty =
        RegisterModelProperty<Int16>(c => c.CapRightCapTopHigh);
        public Int16 CapRightCapTopHigh
        {
            get
            {
                return GetProperty<Int16>(CapRightCapTopHighProperty);
            }
        }

        /// <summary>
        /// CapRightCapTopDeep
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRightCapTopDeepProperty =
        RegisterModelProperty<Int16>(c => c.CapRightCapTopDeep);
        public Int16 CapRightCapTopDeep
        {
            get
            {
                return GetProperty<Int16>(CapRightCapTopDeepProperty);
            }
        }
        #endregion


        /// <summary>
        /// CapBotDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapBotDeepProperty =
        RegisterModelProperty<Int16>(c => c.CapBotDeep);
        public Int16 CapBotDeep
        {
            get
            {
                return GetProperty<Int16>(CapBotDeepProperty);
            }
        }

        /// <summary>
        /// CapBotHigh (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapBotHighProperty =
        RegisterModelProperty<Int16>(c => c.CapBotHigh);
        public Int16 CapBotHigh
        {
            get
            {
                return GetProperty<Int16>(CapBotHighProperty);
            }
        }

        /// <summary>
        /// CapLftDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapLftDeepProperty =
        RegisterModelProperty<Int16>(c => c.CapLftDeep);
        public Int16 CapLftDeep
        {
            get
            {
                return GetProperty<Int16>(CapLftDeepProperty);
            }
        }

        /// <summary>
        /// CapLftWide (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapLftWideProperty =
        RegisterModelProperty<Int16>(c => c.CapLftWide);
        public Int16 CapLftWide
        {
            get
            {
                return GetProperty<Int16>(CapLftWideProperty);
            }
        }

        /// <summary>
        /// CapRgtDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRgtDeepProperty =
        RegisterModelProperty<Int16>(c => c.CapRgtDeep);
        public Int16 CapRgtDeep
        {
            get
            {
                return GetProperty<Int16>(CapRgtDeepProperty);
            }
        }

        /// <summary>
        /// CapRgtWide (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRgtWideProperty =
        RegisterModelProperty<Int16>(c => c.CapRgtWide);
        public Int16 CapRgtWide
        {
            get
            {
                return GetProperty<Int16>(CapRgtWideProperty);
            }
        }

        /// <summary>
        /// TargetSC (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> TargetSCProperty =
        RegisterModelProperty<Int32?>(c => c.TargetSC);
        public Int32? TargetSC
        {
            get
            {
                return GetProperty<Int32?>(TargetSCProperty);
            }
        }

        /// <summary>
        /// MaxCap (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> MaxCapProperty =
        RegisterModelProperty<Int16>(c => c.MaxCap);
        public Int16 MaxCap
        {
            get
            {
                return GetProperty<Int16>(MaxCapProperty);
            }
        }

        /// <summary>
        /// MaxStack (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> MaxStackProperty =
        RegisterModelProperty<Int16>(c => c.MaxStack);
        public Int16 MaxStack
        {
            get
            {
                return GetProperty<Int16>(MaxStackProperty);
            }
        }

        /// <summary>
        /// MinDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MinDeepProperty =
        RegisterModelProperty<Int32>(c => c.MinDeep);
        public Int32 MinDeep
        {
            get
            {
                return GetProperty<Int32>(MinDeepProperty);
            }
        }

        /// <summary>
        /// MaxDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MaxDeepProperty =
        RegisterModelProperty<Int32>(c => c.MaxDeep);
        public Int32 MaxDeep
        {
            get
            {
                return GetProperty<Int32>(MaxDeepProperty);
            }
        }

        /// <summary>
        /// TrayHigh (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayHighProperty =
        RegisterModelProperty<Int16>(c => c.TrayHigh);
        public Int16 TrayHigh
        {
            get
            {
                return GetProperty<Int16>(TrayHighProperty);
            }
        }

        /// <summary>
        /// TrayWide (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayWideProperty =
        RegisterModelProperty<Int16>(c => c.TrayWide);
        public Int16 TrayWide
        {
            get
            {
                return GetProperty<Int16>(TrayWideProperty);
            }
        }

        /// <summary>
        /// TrayDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayDeepProperty =
        RegisterModelProperty<Int16>(c => c.TrayDeep);
        public Int16 TrayDeep
        {
            get
            {
                return GetProperty<Int16>(TrayDeepProperty);
            }
        }

        /// <summary>
        /// TrayHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayHeightProperty =
        RegisterModelProperty<Single>(c => c.TrayHeight);
        public Single TrayHeight
        {
            get
            {
                return GetProperty<Single>(TrayHeightProperty);
            }
        }

        /// <summary>
        /// TrayWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayWidthProperty =
        RegisterModelProperty<Single>(c => c.TrayWidth);
        public Single TrayWidth
        {
            get
            {
                return GetProperty<Single>(TrayWidthProperty);
            }
        }

        /// <summary>
        /// TrayWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayDepthProperty =
        RegisterModelProperty<Single>(c => c.TrayDepth);
        public Single TrayDepth
        {
            get
            {
                return GetProperty<Single>(TrayDepthProperty);
            }
        }

        /// <summary>
        /// TrayUnits (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayUnitsProperty =
        RegisterModelProperty<Int16>(c => c.TrayUnits);
        public Int16 TrayUnits
        {
            get
            {
                return GetProperty<Int16>(TrayUnitsProperty);
            }
        }

        /// <summary>
        /// TrayThickHeight (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayThickHeightProperty =
        RegisterModelProperty<Single>(c => c.TrayThickHeight);
        public Single TrayThickHeight
        {
            get
            {
                return GetProperty<Single>(TrayThickHeightProperty);
            }
        }

        /// <summary>
        /// TrayThickWidth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayThickWidthProperty =
        RegisterModelProperty<Single>(c => c.TrayThickWidth);
        public Single TrayThickWidth
        {
            get
            {
                return GetProperty<Single>(TrayThickWidthProperty);
            }
        }

        /// <summary>
        /// TrayThickDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TrayThickDepthProperty =
        RegisterModelProperty<Single>(c => c.TrayThickDepth);
        public Single TrayThickDepth
        {
            get
            {
                return GetProperty<Single>(TrayThickDepthProperty);
            }
        }

        /// <summary>
        /// TrayCountHigh (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayCountHighProperty =
        RegisterModelProperty<Int16>(c => c.TrayCountHigh);
        public Int16 TrayCountHigh
        {
            get
            {
                return GetProperty<Int16>(TrayCountHighProperty);
            }
        }

        /// <summary>
        /// TrayCountWide (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayCountWideProperty =
        RegisterModelProperty<Int16>(c => c.TrayCountWide);
        public Int16 TrayCountWide
        {
            get
            {
                return GetProperty<Int16>(TrayCountWideProperty);
            }
        }

        /// <summary>
        /// TrayCountDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> TrayCountDeepProperty =
        RegisterModelProperty<Int16>(c => c.TrayCountDeep);
        public Int16 TrayCountDeep
        {
            get
            {
                return GetProperty<Int16>(TrayCountDeepProperty);
            }
        }

        /// <summary>
        /// TrayProduct (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> TrayProductProperty =
        RegisterModelProperty<Byte>(c => c.TrayProduct);
        public Byte TrayProduct
        {
            get
            {
                return GetProperty<Byte>(TrayProductProperty);
            }
        }

        /// <summary>
        /// NestHigh (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NestHighProperty =
        RegisterModelProperty<Int16>(c => c.NestHigh);
        public Int16 NestHigh
        {
            get
            {
                return GetProperty<Int16>(NestHighProperty);
            }
        }

        /// <summary>
        /// NestWide (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NestWideProperty =
        RegisterModelProperty<Int16>(c => c.NestWide);
        public Int16 NestWide
        {
            get
            {
                return GetProperty<Int16>(NestWideProperty);
            }
        }

        /// <summary>
        /// NestDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NestDeepProperty =
        RegisterModelProperty<Int16>(c => c.NestDeep);
        public Int16 NestDeep
        {
            get
            {
                return GetProperty<Int16>(NestDeepProperty);
            }
        }

        /// <summary>
        /// MerchType (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> MerchTypeProperty =
        RegisterModelProperty<Int16>(c => c.MerchType);
        public Int16 MerchType
        {
            get
            {
                return GetProperty<Int16>(MerchTypeProperty);
            }
        }

        /// <summary>
        /// Blocking (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BlockingProperty =
        RegisterModelProperty<Int32>(c => c.Blocking);
        public Int32 Blocking
        {
            get
            {
                return GetProperty<Int32>(BlockingProperty);
            }
        }

        /// <summary>
        /// BlockId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BlockIdProperty =
        RegisterModelProperty<Int32>(c => c.BlockId);
        public Int32 BlockId
        {
            get
            {
                return GetProperty<Int32>(BlockIdProperty);
            }
        }

        /// <summary>
        /// BrushStyle (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BrushStyleProperty =
        RegisterModelProperty<Int32>(c => c.BrushStyle);
        public Int32 BrushStyle
        {
            get
            {
                return GetProperty<Int32>(BrushStyleProperty);
            }
        }

        /// <summary>
        /// MinDrop (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MinDropProperty =
        RegisterModelProperty<Int32>(c => c.MinDrop);
        public Int32 MinDrop
        {
            get
            {
                return GetProperty<Int32>(MinDropProperty);
            }
        }

        /// <summary>
        /// RemoveCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> RemoveCountProperty =
        RegisterModelProperty<Int32?>(c => c.RemoveCount);
        public Int32? RemoveCount
        {
            get
            {
                return GetProperty<Int32?>(RemoveCountProperty);
            }
        }

        /// <summary>
        /// CapRightWide (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRightWideProperty =
        RegisterModelProperty<Int16>(c => c.CapRightWide);
        public Int16 CapRightWide
        {
            get
            {
                return GetProperty<Int16>(CapRightWideProperty);
            }
        }

        /// <summary>
        /// CapRightDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRightDeepProperty =
        RegisterModelProperty<Int16>(c => c.CapRightDeep);
        public Int16 CapRightDeep
        {
            get
            {
                return GetProperty<Int16>(CapRightDeepProperty);
            }
        }

        /// <summary>
        /// CapRightWideTrayCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRightWideTrayCountProperty =
        RegisterModelProperty<Int16>(c => c.CapRightWideTrayCount);
        public Int16 CapRightWideTrayCount
        {
            get
            {
                return GetProperty<Int16>(CapRightWideTrayCountProperty);
            }
        }

        /// <summary>
        /// CapRightDeepTrayCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CapRightDeepTrayCountProperty =
        RegisterModelProperty<Int16>(c => c.CapRightDeepTrayCount);
        public Int16 CapRightDeepTrayCount
        {
            get
            {
                return GetProperty<Int16>(CapRightDeepTrayCountProperty);
            }
        }

        /// <summary>
        /// MultiSited (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MultiSitedProperty =
        RegisterModelProperty<Byte>(c => c.MultiSited);
        public Byte MultiSited
        {
            get
            {
                return GetProperty<Byte>(MultiSitedProperty);
            }
        }

        /// <summary>
        /// CasePack (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CasePackProperty =
        RegisterModelProperty<Int32>(c => c.CasePack);
        public Int32 CasePack
        {
            get
            {
                return GetProperty<Int32>(CasePackProperty);
            }
        }

        /// <summary>
        /// Colour (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ColourProperty =
        RegisterModelProperty<Int32>(c => c.Colour);
        public Int32 Colour
        {
            get
            {
                return GetProperty<Int32>(ColourProperty);
            }
        }

        /// <summary>
        /// LastChance (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> LastChanceProperty =
        RegisterModelProperty<Byte>(c => c.LastChance);
        public Byte LastChance
        {
            get
            {
                return GetProperty<Byte>(LastChanceProperty);
            }
        }

        /// <summary>
        /// AddLocked (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> AddLockedProperty =
        RegisterModelProperty<Byte>(c => c.AddLocked);
        public Byte AddLocked
        {
            get
            {
                return GetProperty<Byte>(AddLockedProperty);
            }
        }

        /// <summary>
        /// RemoveLocked (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> RemoveLockedProperty =
        RegisterModelProperty<Byte>(c => c.RemoveLocked);
        public Byte RemoveLocked
        {
            get
            {
                return GetProperty<Byte>(RemoveLockedProperty);
            }
        }

        /// <summary>
        /// StripNumber (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> StripNumberProperty =
        RegisterModelProperty<Int32?>(c => c.StripNumber);
        public Int32? StripNumber
        {
            get
            {
                return GetProperty<Int32?>(StripNumberProperty);
            }
        }

        /// <summary>
        /// Position Can BreakTrayUp 
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> BreakTrayUpProperty =
        RegisterModelProperty<Byte>(c => c.BreakTrayUp);
        public Byte BreakTrayUp
        {
            get
            {
                return GetProperty<Byte>(BreakTrayUpProperty);
            }
        }

        /// <summary>
        /// Position Can BreakTrayDown 
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> BreakTrayDownProperty =
        RegisterModelProperty<Byte>(c => c.BreakTrayDown);
        public Byte BreakTrayDown
        {
            get
            {
                return GetProperty<Byte>(BreakTrayDownProperty);
            }
        }

        /// <summary>
        /// Position Can BreakTrayBack
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> BreakTrayBackProperty =
        RegisterModelProperty<Byte>(c => c.BreakTrayBack);
        public Byte BreakTrayBack
        {
            get
            {
                return GetProperty<Byte>(BreakTrayBackProperty);
            }
        }

        /// <summary>
        /// Position Can BreakTrayTop
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> BreakTrayTopProperty =
        RegisterModelProperty<Byte>(c => c.BreakTrayTop);
        public Byte BreakTrayTop
        {
            get
            {
                return GetProperty<Byte>(BreakTrayTopProperty);
            }
        }

        /// <summary>
        /// FrontOnly (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> FrontOnlyProperty =
        RegisterModelProperty<Byte>(c => c.FrontOnly);
        public Byte FrontOnly
        {
            get
            {
                return GetProperty<Byte>(FrontOnlyProperty);
            }
        }

        /// <summary>
        /// MiddleCapping (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MiddleCappingProperty =
        RegisterModelProperty<Byte>(c => c.MiddleCapping);
        public Byte MiddleCapping
        {
            get
            {
                return GetProperty<Byte>(MiddleCappingProperty);
            }
        }

        /// <summary>
        /// MaxNestHigh (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxNestHighProperty =
        RegisterModelProperty<Int32?>(c => c.MaxNestHigh);
        public Int32? MaxNestHigh
        {
            get
            {
                return GetProperty<Int32?>(MaxNestHighProperty);
            }
        }

        /// <summary>
        /// MaxNestDeep (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxNestDeepProperty =
        RegisterModelProperty<Int32?>(c => c.MaxNestDeep);
        public Int32? MaxNestDeep
        {
            get
            {
                return GetProperty<Int32?>(MaxNestDeepProperty);
            }
        }

        /// <summary>
        /// PegDepth (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegDepthProperty =
        RegisterModelProperty<Single>(c => c.PegDepth);
        public Single PegDepth
        {
            get
            {
                return GetProperty<Single>(PegDepthProperty);
            }
        }

        /// <summary>
        /// PegX (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegXProperty =
        RegisterModelProperty<Single>(c => c.PegX);
        public Single PegX
        {
            get
            {
                return GetProperty<Single>(PegXProperty);
            }
        }

        /// <summary>
        /// PegY (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegYProperty =
        RegisterModelProperty<Single>(c => c.PegY);
        public Single PegY
        {
            get
            {
                return GetProperty<Single>(PegYProperty);
            }
        }

        /// <summary>
        /// MaxRightCap (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxRightCapProperty =
        RegisterModelProperty<Int32?>(c => c.MaxRightCap);
        public Int32? MaxRightCap
        {
            get
            {
                return GetProperty<Int32?>(MaxRightCapProperty);
            }
        }

        /// <summary>
        /// Created (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> CreatedProperty =
        RegisterModelProperty<DateTime>(c => c.Created);
        public DateTime Created
        {
            get
            {
                return GetProperty<DateTime>(CreatedProperty);
            }
        }

        /// <summary>
        /// Modified (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> ModifiedProperty =
        RegisterModelProperty<DateTime>(c => c.Modified);
        public DateTime Modified
        {
            get
            {
                return GetProperty<DateTime>(ModifiedProperty);
            }
        }

        /// <summary>
        /// PersonnelId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PersonnelIdProperty =
        RegisterModelProperty<Int32>(c => c.PersonnelId);
        public Int32 PersonnelId
        {
            get
            {
                return GetProperty<Int32>(PersonnelIdProperty);
            }
        }

        /// <summary>
        /// PersonnelFlexi (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PersonnelFlexiProperty =
        RegisterModelProperty<String>(c => c.PersonnelFlexi);
        public String PersonnelFlexi
        {
            get
            {
                return GetProperty<String>(PersonnelFlexiProperty);
            }
        }

        /// <summary>
        /// CompanyId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CompanyIdProperty =
        RegisterModelProperty<Int32>(c => c.CompanyId);
        public Int32 CompanyId
        {
            get
            {
                return GetProperty<Int32>(CompanyIdProperty);
            }
        }
        #endregion

        #region Manual Plan
        /// <summary>
        /// Code (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
        RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get
            {
                return GetProperty<String>(CodeProperty);
            }
        }

        /// <summary>
        /// PegZ (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PegZProperty =
        RegisterModelProperty<Single?>(c => c.PegZ);
        public Single? PegZ
        {
            get
            {
                return GetProperty<Single?>(PegZProperty);
            }
        }

        /// <summary>
        /// PegStyle (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PegStyleProperty =
        RegisterModelProperty<String>(c => c.PegStyle);
        public String PegStyle
        {
            get
            {
                return GetProperty<String>(PegStyleProperty);
            }
        }

        /// <summary>
        /// IsSequenced (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> IsSequencedProperty =
        RegisterModelProperty<Byte>(c => c.IsSequenced);
        public Byte IsSequenced
        {
            get
            {
                return GetProperty<Byte>(IsSequencedProperty);
            }
        }

        /// <summary>
        /// NewProducts (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> NewProductsProperty =
        RegisterModelProperty<Byte>(c => c.NewProducts);
        public Byte NewProducts
        {
            get
            {
                return GetProperty<Byte>(NewProductsProperty);
            }
        }

        /// <summary>
        /// Number1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number1Property =
        RegisterModelProperty<Double>(c => c.Number1);
        public Double Number1
        {
            get
            {
                return GetProperty<Double>(Number1Property);
            }
        }

        /// <summary>
        /// Number2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number2Property =
        RegisterModelProperty<Double>(c => c.Number2);
        public Double Number2
        {
            get
            {
                return GetProperty<Double>(Number2Property);
            }
        }

        /// <summary>
        /// Number3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number3Property =
        RegisterModelProperty<Int32>(c => c.Number3);
        public Int32 Number3
        {
            get
            {
                return GetProperty<Int32>(Number3Property);
            }
        }

        /// <summary>
        /// Number4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number4Property =
        RegisterModelProperty<Int32>(c => c.Number4);
        public Int32 Number4
        {
            get
            {
                return GetProperty<Int32>(Number4Property);
            }
        }

        /// <summary>
        /// Number5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number5Property =
        RegisterModelProperty<Int32>(c => c.Number5);
        public Int32 Number5
        {
            get
            {
                return GetProperty<Int32>(Number5Property);
            }
        }

        /// <summary>
        /// Text1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
        RegisterModelProperty<String>(c => c.Text1);
        public String Text1
        {
            get
            {
                return GetProperty<String>(Text1Property);
            }
        }

        /// <summary>
        /// Text2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
        RegisterModelProperty<String>(c => c.Text2);
        public String Text2
        {
            get
            {
                return GetProperty<String>(Text2Property);
            }
        }

        /// <summary>
        /// Text3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
        RegisterModelProperty<String>(c => c.Text3);
        public String Text3
        {
            get
            {
                return GetProperty<String>(Text3Property);
            }
        }

        /// <summary>
        /// Text4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
        RegisterModelProperty<String>(c => c.Text4);
        public String Text4
        {
            get
            {
                return GetProperty<String>(Text4Property);
            }
        }

        /// <summary>
        /// Text5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
        RegisterModelProperty<String>(c => c.Text5);
        public String Text5
        {
            get
            {
                return GetProperty<String>(Text5Property);
            }
        }

        /// <summary>
        /// IsFlexPreservedSpace (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> IsFlexPreservedSpaceProperty =
        RegisterModelProperty<Byte>(c => c.IsFlexPreservedSpace);
        public Byte IsFlexPreservedSpace
        {
            get
            {
                return GetProperty<Byte>(IsFlexPreservedSpaceProperty);
            }
        }

        /// <summary>
        /// HidePreserved (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> HidePreservedProperty =
        RegisterModelProperty<Byte>(c => c.HidePreserved);
        public Byte HidePreserved
        {
            get
            {
                return GetProperty<Byte>(HidePreservedProperty);
            }
        }

        /// <summary>
        /// AttachmentTempFile (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> AttachmentTempFileProperty =
        RegisterModelProperty<String>(c => c.AttachmentTempFile);
        public String AttachmentTempFile
        {
            get
            {
                return GetProperty<String>(AttachmentTempFileProperty);
            }
        }

        /// <summary>
        /// AttachmentName (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> AttachmentNameProperty =
        RegisterModelProperty<String>(c => c.AttachmentName);
        public String AttachmentName
        {
            get
            {
                return GetProperty<String>(AttachmentNameProperty);
            }
        }

        /// <summary>
        /// AttachmentBaseElement (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> AttachmentBaseElementProperty =
        RegisterModelProperty<Byte>(c => c.AttachmentBaseElement);
        public Byte AttachmentBaseElement
        {
            get
            {
                return GetProperty<Byte>(AttachmentBaseElementProperty);
            }
        }
        #endregion

        #region Space Plan
        /// <summary>
        /// StoreId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> StoreIdProperty =
        RegisterModelProperty<Int32>(c => c.StoreId);
        public Int32 StoreId
        {
            get
            {
                return GetProperty<Int32>(StoreIdProperty);
            }
        }

        /// <summary>
        /// PegFix (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> PegFixProperty =
        RegisterModelProperty<Byte>(c => c.PegFix);
        public Byte PegFix
        {
            get
            {
                return GetProperty<Byte>(PegFixProperty);
            }
        }

        /// <summary>
        /// PegShift (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> PegShiftProperty =
        RegisterModelProperty<Int32?>(c => c.PegShift);
        public Int32? PegShift
        {
            get
            {
                return GetProperty<Int32?>(PegShiftProperty);
            }
        }

        /// <summary>
        /// ElementPositionId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ElementPositionIdProperty =
        RegisterModelProperty<Int32>(c => c.ElementPositionId);
        public Int32 ElementPositionId
        {
            get
            {
                return GetProperty<Int32>(ElementPositionIdProperty);
            }
        }

        /// <summary>
        /// HistoricalDailyUnits (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HistoricalDailyUnitsProperty =
        RegisterModelProperty<Single>(c => c.HistoricalDailyUnits);
        public Single HistoricalDailyUnits
        {
            get
            {
                return GetProperty<Single>(HistoricalDailyUnitsProperty);
            }
        }

        /// <summary>
        /// ActualDaysSupplied (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ActualDaysSuppliedProperty =
        RegisterModelProperty<Single>(c => c.ActualDaysSupplied);
        public Single ActualDaysSupplied
        {
            get
            {
                return GetProperty<Single>(ActualDaysSuppliedProperty);
            }
        }

        /// <summary>
        /// BlockTagName (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> BlockTagNameProperty =
        RegisterModelProperty<String>(c => c.BlockTagName);
        public String BlockTagName
        {
            get
            {
                return GetProperty<String>(BlockTagNameProperty);
            }
        }

        /// <summary>
        /// GlobalRank (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> GlobalRankProperty =
        RegisterModelProperty<Int32?>(c => c.GlobalRank);
        public Int32? GlobalRank
        {
            get
            {
                return GetProperty<Int32?>(GlobalRankProperty);
            }
        }

        /// <summary>
        /// FinalMin (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> FinalMinProperty =
        RegisterModelProperty<Int32?>(c => c.FinalMin);
        public Int32? FinalMin
        {
            get
            {
                return GetProperty<Int32?>(FinalMinProperty);
            }
        }

        /// <summary>
        /// FinalMax (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> FinalMaxProperty =
        RegisterModelProperty<Int32?>(c => c.FinalMax);
        public Int32? FinalMax
        {
            get
            {
                return GetProperty<Int32>(FinalMaxProperty);
            }
        }

        /// <summary>
        /// MinPriority1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MinPriority1Property =
        RegisterModelProperty<Int32?>(c => c.MinPriority1);
        public Int32? MinPriority1
        {
            get
            {
                return GetProperty<Int32?>(MinPriority1Property);
            }
        }

        /// <summary>
        /// MinPriority2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MinPriority2Property =
        RegisterModelProperty<Int32?>(c => c.MinPriority2);
        public Int32? MinPriority2
        {
            get
            {
                return GetProperty<Int32?>(MinPriority2Property);
            }
        }

        /// <summary>
        /// MinPriority3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MinPriority3Property =
        RegisterModelProperty<Int32?>(c => c.MinPriority3);
        public Int32? MinPriority3
        {
            get
            {
                return GetProperty<Int32?>(MinPriority3Property);
            }
        }

        /// <summary>
        /// MinPriority4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MinPriority4Property =
        RegisterModelProperty<Int32?>(c => c.MinPriority4);
        public Int32? MinPriority4
        {
            get
            {
                return GetProperty<Int32?>(MinPriority4Property);
            }
        }

        /// <summary>
        /// MinPriority5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MinPriority5Property =
        RegisterModelProperty<Int32?>(c => c.MinPriority5);
        public Int32? MinPriority5
        {
            get
            {
                return GetProperty<Int32?>(MinPriority5Property);
            }
        }

        /// <summary>
        /// MaxPriority1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxPriority1Property =
        RegisterModelProperty<Int32?>(c => c.MaxPriority1);
        public Int32? MaxPriority1
        {
            get
            {
                return GetProperty<Int32?>(MaxPriority1Property);
            }
        }

        /// <summary>
        /// MaxPriority2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxPriority2Property =
        RegisterModelProperty<Int32?>(c => c.MaxPriority2);
        public Int32? MaxPriority2
        {
            get
            {
                return GetProperty<Int32?>(MaxPriority2Property);
            }
        }

        /// <summary>
        /// MaxPriority3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxPriority3Property =
        RegisterModelProperty<Int32?>(c => c.MaxPriority3);
        public Int32? MaxPriority3
        {
            get
            {
                return GetProperty<Int32?>(MaxPriority3Property);
            }
        }

        /// <summary>
        /// MaxPriority4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxPriority4Property =
        RegisterModelProperty<Int32?>(c => c.MaxPriority4);
        public Int32? MaxPriority4
        {
            get
            {
                return GetProperty<Int32?>(MaxPriority4Property);
            }
        }

        /// <summary>
        /// MaxPriority5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MaxPriority5Property =
        RegisterModelProperty<Int32?>(c => c.MaxPriority5);
        public Int32? MaxPriority5
        {
            get
            {
                return GetProperty<Int32?>(MaxPriority5Property);
            }
        }

        /// <summary>
        /// RecommendedInventory (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> RecommendedInventoryProperty =
        RegisterModelProperty<Int32?>(c => c.RecommendedInventory);
        public Int32? RecommendedInventory
        {
            get
            {
                return GetProperty<Int32?>(RecommendedInventoryProperty);
            }
        }

        /// <summary>
        /// CompromiseInventory (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> CompromiseInventoryProperty =
        RegisterModelProperty<Int32?>(c => c.CompromiseInventory);
        public Int32? CompromiseInventory
        {
            get
            {
                return GetProperty<Int32?>(CompromiseInventoryProperty);
            }
        }

        /// <summary>
        /// AverageWeeklySales (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AverageWeeklySalesProperty =
        RegisterModelProperty<Single?>(c => c.AverageWeeklySales);
        public Single? AverageWeeklySales
        {
            get
            {
                return GetProperty<Single?>(AverageWeeklySalesProperty);
            }
        }

        /// <summary>
        /// AverageWeeklyUnits (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AverageWeeklyUnitsProperty =
        RegisterModelProperty<Single?>(c => c.AverageWeeklyUnits);
        public Single? AverageWeeklyUnits
        {
            get
            {
                return GetProperty<Single?>(AverageWeeklyUnitsProperty);
            }
        }

        /// <summary>
        /// AverageWeeklyProfit (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AverageWeeklyProfitProperty =
        RegisterModelProperty<Single?>(c => c.AverageWeeklyProfit);
        public Single? AverageWeeklyProfit
        {
            get
            {
                return GetProperty<Single?>(AverageWeeklyProfitProperty);
            }
        }

        /// <summary>
        /// AchievedCases (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedCasesProperty =
        RegisterModelProperty<Single?>(c => c.AchievedCases);
        public Single? AchievedCases
        {
            get
            {
                return GetProperty<Single?>(AchievedCasesProperty);
            }
        }

        /// <summary>
        /// AchievedDOS (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedDOSProperty =
        RegisterModelProperty<Single?>(c => c.AchievedDOS);
        public Single? AchievedDOS
        {
            get
            {
                return GetProperty<Single?>(AchievedDOSProperty);
            }
        }

        /// <summary>
        /// PerformanceValue01 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue01Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue01);
        public Single? PerformanceValue01
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue01Property);
            }
        }

        /// <summary>
        /// PerformanceValue02 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue02Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue02);
        public Single? PerformanceValue02
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue02Property);
            }
        }

        /// <summary>
        /// PerformanceValue03 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue03Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue03);
        public Single? PerformanceValue03
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue03Property);
            }
        }

        /// <summary>
        /// PerformanceValue04 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue04Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue04);
        public Single? PerformanceValue04
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue04Property);
            }
        }

        /// <summary>
        /// PerformanceValue05 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue05Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue05);
        public Single? PerformanceValue05
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue05Property);
            }
        }

        /// <summary>
        /// PerformanceValue06 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue06Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue06);
        public Single? PerformanceValue06
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue06Property);
            }
        }

        /// <summary>
        /// PerformanceValue07 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue07Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue07);
        public Single? PerformanceValue07
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue07Property);
            }
        }

        /// <summary>
        /// PerformanceValue08 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue08Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue08);
        public Single? PerformanceValue08
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue08Property);
            }
        }

        /// <summary>
        /// PerformanceValue09 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue09Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue09);
        public Single? PerformanceValue09
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue09Property);
            }
        }

        /// <summary>
        /// PerformanceValue10 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> PerformanceValue10Property =
        RegisterModelProperty<Single?>(c => c.PerformanceValue10);
        public Single? PerformanceValue10
        {
            get
            {
                return GetProperty<Single?>(PerformanceValue10Property);
            }
        }

        /// <summary>
        /// ListingPSI (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> ListingPSIProperty =
        RegisterModelProperty<Single?>(c => c.ListingPSI);
        public Single? ListingPSI
        {
            get
            {
                return GetProperty<Single?>(ListingPSIProperty);
            }
        }

        /// <summary>
        /// ExposurePSI (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> ExposurePSIProperty =
        RegisterModelProperty<Single?>(c => c.ExposurePSI);
        public Single? ExposurePSI
        {
            get
            {
                return GetProperty<Single?>(ExposurePSIProperty);
            }
        }

        /// <summary>
        /// PegProng (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double?> PegProngProperty =
        RegisterModelProperty<Double?>(c => c.PegProng);
        public Double? PegProng
        {
            get
            {
                return GetProperty<Double?>(PegProngProperty);
            }
        }

        /// <summary>
        /// CanBeRefaced (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanBeRefacedProperty =
        RegisterModelProperty<Boolean>(c => c.CanBeRefaced);
        public Boolean CanBeRefaced
        {
            get
            {
                return GetProperty<Boolean>(CanBeRefacedProperty);
            }
        }

        /// <summary>
        /// AchievedInventoryLevelType (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> AchievedInventoryLevelTypeProperty =
        RegisterModelProperty<Byte>(c => c.AchievedInventoryLevelType);
        public Byte AchievedInventoryLevelType
        {
            get
            {
                return GetProperty<Byte>(AchievedInventoryLevelTypeProperty);
            }
        }
        #endregion

        #endregion

        #region Methods


        public static T OrientDimension<T>(Dimension dim, T height, T width, T depth, Int32 OrientationId)
        {
            switch (OrientationId)
            {
                case 0: // Front 0

                    switch (dim)
                    {
                        case Dimension.Height:
                            return height;
                        case Dimension.Width:
                            return width;
                        case Dimension.Depth:
                            return depth;
                    }
                    break;
                case 1: // Front 90

                    switch (dim)
                    {
                        case Dimension.Height:
                            return width;
                        case Dimension.Width:
                            return height;
                        case Dimension.Depth:
                            return depth;
                    }
                    break;
                case 2: // Right 0

                    switch (dim)
                    {
                        case Dimension.Height:
                            return height;
                        case Dimension.Width:
                            return depth;
                        case Dimension.Depth:
                            return width;
                    }
                    break;
                case 3: // Right 90

                    switch (dim)
                    {
                        case Dimension.Height:
                            return depth;
                        case Dimension.Width:
                            return height;
                        case Dimension.Depth:
                            return width;
                    }
                    break;
                case 4: // Top 0

                    switch (dim)
                    {
                        case Dimension.Height:
                            return depth;
                        case Dimension.Width:
                            return width;
                        case Dimension.Depth:
                            return height;
                    }
                    break;
                case 5: // Top 90

                    switch (dim)
                    {
                        case Dimension.Height:
                            return width;
                        case Dimension.Width:
                            return depth;
                        case Dimension.Depth:
                            return height;
                    }
                    break;
            }
            return width;
        }
        #endregion
    }
}
