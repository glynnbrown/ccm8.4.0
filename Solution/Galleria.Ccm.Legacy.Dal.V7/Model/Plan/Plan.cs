﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
// CCM-17428 : M.Brumby
//  Content description changed to plan name from workpackage name
#endregion
#region Version History: (CCM.Net 7.4)
// CCM-18877 : D.Pleasance
//  Plan changes so that CCM Bay information is a fixture rather than assembly
#endregion
#region Version History: (CCM.Net 7.4.1)
// CCM-20107 : M.Brumby
//  Publish middle capping flag
#endregion
#region Version History: (CCM.Net 7.5)
// CCM-20418 : M.Brumby
//  Use SplitPlan if the plan has a fixturePlan assigned.
// CCM-20762 : M.Brumby
//  UniqueContentReference can be updated
// CCM-20928 : L.Bailey
//    "Units In Tray" field to be used in CCM.net Publish to GFS
//      Added TrayHeight, TrayWidth, TrayDepth, TrayUnits 
#endregion
#region Version History: (CCM.Net 7.5.1)
//  CCM-22192 : M.Brumby
//      Added StoreCode so that for store specific plans the location code 
//      field can be populated when publishing to GFS
#endregion
#region Version History: (CCM 7.5.2)

//  CCM-22440 : M.Brumby
//      Store and Cluster plans now fetch template plan positions. This list
//      can be used to add in any dropped products to the product list.
//  CCM-22483 : M.Brumby
//      Chest fixes
#endregion
#region Version History: (CCM.Net 7.5.3)
//  CCM-22944 : M.Brumby
//      Added break tray top
//  CCM-22962 : M.Toomer
//      Added  TotalPostionCount, BayCount, ComponentCount, MerchandisableLinearSpace, MerchandisableAreaSpace,
//      MerchandisableCubicSpace, TotalFacingCount, ProductsDroppedCount, WhiteSpacePercentage
//  CCM-23137 : M.Toomer
//      Increased accuracy of WhiteSpacePercentage passed to GFS to 4 decimal places
//  CCM-23143 : M.Toomer
//      Corrected TotalFacingCount calculation 
//  CCM-23182 : M.Toomer
//      Added TotalProductLinearSpace
//  CCM-23273 : M.Toomer
//      Corrected typo in selection for non-tray products for TotalFacingsCount
#endregion
#region Version History: (CCM v7.5.4)
//  CCM-23580 : M.Brumby
//      Added merge positions for Manual plans.
#endregion
#region Version History: (CCM v7.5.8)
//  CCM-26309 : M.Brumby (FWD integrated by Rob Cooper)
//      Use split plan positions when determining if there are invalid tray/unit products.
//  CCM-26877 : M.Brumby
//      Squeezed positions were being squeezed agian when turned into product records.
#endregion
#endregion

using System;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;

using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class Plan : ModelObject<Plan>
    {
        #region LockObjects
        private Object planBayLock = new Object();
        private Object planElementLock = new Object();
        private Object planPositionLock = new Object();
        private Object planTextBoxLock = new Object();
        private Object templatePlanPositionLock = new Object();
        #endregion

        #region Properties
      

        #region BasePlanProperties

        
        /// <summary>
        /// The CategoryReviewName
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryReviewNameProperty =
            RegisterModelProperty<String>(c => c.CategoryReviewName);
        public String CategoryReviewName
        {
            get { return GetProperty<String>(CategoryReviewNameProperty); }
        }

        /// <summary>
        /// The Plan Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        internal static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);

        /// <summary>
        /// The unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
            set { SetProperty<Guid>(UniqueContentReferenceProperty, value); }
        }

        /// <summary>
        /// The Plan Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The Unique Item Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> UniqueItemNameProperty =
            RegisterModelProperty<String>(c => c.UniqueItemName);
        public String UniqueItemName
        {
            get { return GetProperty<String>(UniqueItemNameProperty); }
        }

        /// <summary>
        /// The Plan Type
        /// </summary>
        public static readonly ModelPropertyInfo<PlanType> PlanTypeProperty =
            RegisterModelProperty<PlanType>(c => c.PlanType);
        public PlanType PlanType
        {
            get { return GetProperty<PlanType>(PlanTypeProperty); }
        }

        /// <summary>
        /// The Related StoreId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> StoreIdProperty =
            RegisterModelProperty<Int32>(c => c.StoreId);
        public Int32 StoreId
        {
            get { return GetProperty<Int32>(StoreIdProperty); }
        }

        /// <summary>
        ///  The Related StoreFlexi
        /// </summary>
        public static readonly ModelPropertyInfo<String> StoreFlexiProperty =
            RegisterModelProperty<String>(c => c.StoreFlexi);
        public String StoreFlexi
        {
            get { return GetProperty<String>(StoreFlexiProperty); }
        }

        /// <summary>
        ///  The Related Store code
        /// </summary>
        public static readonly ModelPropertyInfo<String> StoreCodeProperty =
            RegisterModelProperty<String>(c => c.StoreCode);
        public String StoreCode
        {
            get { return GetProperty<String>(StoreCodeProperty); }
        }

        /// <summary>
        /// The Product level Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductLevelIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductLevelId);
        public Int32 ProductLevelId
        {
            get { return GetProperty<Int32>(ProductLevelIdProperty); }
        }

        /// <summary>
        ///  The MerchandisingGroupFlexi
        /// </summary>
        public static readonly ModelPropertyInfo<String> MerchandisingGroupFlexiProperty =
            RegisterModelProperty<String>(c => c.MerchandisingGroupFlexi);
        public String MerchandisingGroupFlexi
        {
            get { return GetProperty<String>(MerchandisingGroupFlexiProperty); }
        }

        /// <summary>
        ///  The CreatedDate
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> CreatedDateProperty =
            RegisterModelProperty<DateTime?>(c => c.CreatedDate);
        public DateTime? CreatedDate
        {
            get { return GetProperty<DateTime?>(CreatedDateProperty); }
        }

        /// <summary>
        ///  The IssuedDate
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> IssuedDateProperty =
            RegisterModelProperty<DateTime?>(c => c.IssuedDate);
        public DateTime? IssuedDate
        {
            get { return GetProperty<DateTime?>(IssuedDateProperty); }
        }

        /// <summary>
        ///  The ValidDate
        public static readonly ModelPropertyInfo<DateTime?> ValidDateProperty =
            RegisterModelProperty<DateTime?>(c => c.ValidDate);
        public DateTime? ValidDate
        {
            get { return GetProperty<DateTime?>(ValidDateProperty); }
        }

        /// <summary>
        ///  The RangingDate
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> RangingDateProperty =
            RegisterModelProperty<DateTime?>(c => c.RangingDate);
        public DateTime? RangingDate
        {
            get { return GetProperty<DateTime?>(RangingDateProperty); }
        }

        /// <summary>
        ///  The PublishingDate
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> PublishingDateProperty =
            RegisterModelProperty<DateTime?>(c => c.PublishingDate);
        public DateTime? PublishingDate
        {
            get { return GetProperty<DateTime?>(PublishingDateProperty); }
        }

        /// <summary>
        ///  Deleted
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> DeletedProperty =
            RegisterModelProperty<Byte>(c => c.Deleted);
        public Byte Deleted
        {
            get { return GetProperty<Byte>(DeletedProperty); }
        }

        /// <summary>
        ///  Issued
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> IssuedProperty =
            RegisterModelProperty<Byte>(c => c.Issued);
        public Byte Issued
        {
            get { return GetProperty<Byte>(IssuedProperty); }
        }

        /// <summary>
        ///  Failed
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> FailedProperty =
            RegisterModelProperty<Byte>(c => c.Failed);
        public Byte Failed
        {
            get { return GetProperty<Byte>(FailedProperty); }
        }

        /// <summary>
        ///  MerchShelfSapce
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchShelfSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchShelfSpace);
        public Double MerchShelfSpace
        {
            get { return GetProperty<Double>(MerchShelfSpaceProperty); }
        }
        /// <summary>
        ///  MerchPegSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchPegSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchPegSpace);
        public Double MerchPegSpace
        {
            get { return GetProperty<Double>(MerchPegSpaceProperty); }
        }
        /// <summary>
        ///  MerchBarSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchBarSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchBarSpace);
        public Double MerchBarSpace
        {
            get { return GetProperty<Double>(MerchBarSpaceProperty); }
        }
        /// <summary>
        ///  MerchChestSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchChestSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchChestSpace);
        public Double MerchChestSpace
        {
            get { return GetProperty<Double>(MerchChestSpaceProperty); }
        }
        /// <summary>
        ///  Number1
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number1Property =
            RegisterModelProperty<Double>(c => c.Number1);
        public Double Number1
        {
            get { return GetProperty<Double>(Number1Property); }
        }
        /// <summary>
        ///  Number2
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number2Property =
            RegisterModelProperty<Double>(c => c.Number2);
        public Double Number2
        {
            get { return GetProperty<Double>(Number2Property); }
        }
        /// <summary>
        ///  Number3
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number3Property =
            RegisterModelProperty<Int32>(c => c.Number3);
        public Int32 Number3
        {
            get { return GetProperty<Int32>(Number3Property); }
        }
        /// <summary>
        ///  Number4
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number4Property =
            RegisterModelProperty<Int32>(c => c.Number4);
        public Int32 Number4
        {
            get { return GetProperty<Int32>(Number4Property); }
        }
        /// <summary>
        ///  Number5
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number5Property =
            RegisterModelProperty<Int32>(c => c.Number5);
        public Int32 Number5
        {
            get { return GetProperty<Int32>(Number5Property); }
        }

        /// <summary>
        ///  Text1
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
            RegisterModelProperty<String>(c => c.Text1);
        public String Text1
        {
            get { return GetProperty<String>(Text1Property); }
        }
        /// <summary>
        ///  Text2
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
            RegisterModelProperty<String>(c => c.Text2);
        public String Text2
        {
            get { return GetProperty<String>(Text2Property); }
        }
        /// <summary>
        ///  Text3
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
            RegisterModelProperty<String>(c => c.Text3);
        public String Text3
        {
            get { return GetProperty<String>(Text3Property); }
        }
        /// <summary>
        ///  Text4
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
            RegisterModelProperty<String>(c => c.Text4);
        public String Text4
        {
            get { return GetProperty<String>(Text4Property); }
        }
        /// <summary>
        ///  Text5
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
            RegisterModelProperty<String>(c => c.Text5);
        public String Text5
        {
            get { return GetProperty<String>(Text5Property); }
        }
        /// <summary>
        ///  Text6
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text6Property =
            RegisterModelProperty<String>(c => c.Text6);
        public String Text6
        {
            get { return GetProperty<String>(Text6Property); }
        }
        /// <summary>
        ///  Text7
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text7Property =
            RegisterModelProperty<String>(c => c.Text7);
        public String Text7
        {
            get { return GetProperty<String>(Text7Property); }
        }
        /// <summary>
        ///  Text8
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text8Property =
            RegisterModelProperty<String>(c => c.Text8);
        public String Text8
        {
            get { return GetProperty<String>(Text8Property); }
        }
        /// <summary>
        ///  Text9
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text9Property =
            RegisterModelProperty<String>(c => c.Text9);
        public String Text9
        {
            get { return GetProperty<String>(Text9Property); }
        }
        /// <summary>
        ///  Text10
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text10Property =
            RegisterModelProperty<String>(c => c.Text10);
        public String Text10
        {
            get { return GetProperty<String>(Text10Property); }
        }
        /// <summary>
        ///  ElementPresentation
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> ElementPresentationProperty =
            RegisterModelProperty<Byte?>(c => c.ElementPresentation);
        public Byte? ElementPresentation
        {
            get { return GetProperty<Byte?>(ElementPresentationProperty); }
        }
        /// <summary>
        ///  NestedMerchHeight
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> NestedMerchHeightProperty =
            RegisterModelProperty<Byte?>(c => c.NestedMerchHeight);
        public Byte? NestedMerchHeight
        {
            get { return GetProperty<Byte?>(NestedMerchHeightProperty); }
        }
        /// <summary>
        ///  FingerSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Double?> FingerSpaceProperty =
            RegisterModelProperty<Double?>(c => c.FingerSpace);
        public Double? FingerSpace
        {
            get { return GetProperty<Double?>(FingerSpaceProperty); }
        }
        /// <summary>
        ///  BreakMerchHeight
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> BreakMerchHeightProperty =
            RegisterModelProperty<Byte?>(c => c.BreakMerchHeight);
        public Byte? BreakMerchHeight
        {
            get { return GetProperty<Byte?>(BreakMerchHeightProperty); }
        }
        /// <summary>
        ///  BreakMerchDepth
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> BreakMerchDepthProperty =
            RegisterModelProperty<Byte?>(c => c.BreakMerchDepth);
        public Byte? BreakMerchDepth
        {
            get { return GetProperty<Byte?>(BreakMerchDepthProperty); }
        }
        /// <summary>
        ///  CategoryReviewId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CategoryReviewIdProperty =
            RegisterModelProperty<Int32>(c => c.CategoryReviewId);
        public Int32 CategoryReviewId
        {
            get { return GetProperty<Int32>(CategoryReviewIdProperty); }
        }
        /// <summary>
        ///  ParentCategoryReviewId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ParentCategoryReviewIdProperty =
            RegisterModelProperty<Int32?>(c => c.ParentCategoryReviewId);
        public Int32? ParentCategoryReviewId
        {
            get { return GetProperty<Int32?>(ParentCategoryReviewIdProperty); }
        }
        /// <summary>
        ///  Created
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> CreatedProperty =
            RegisterModelProperty<DateTime>(c => c.Created);
        public DateTime Created
        {
            get { return GetProperty<DateTime>(CreatedProperty); }
        }
        /// <summary>
        ///  Modified
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> ModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.Modified);
        public DateTime Modified
        {
            get { return GetProperty<DateTime>(ModifiedProperty); }
        }
        /// <summary>
        ///  PersonnelId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PersonnelIdProperty =
            RegisterModelProperty<Int32>(c => c.PersonnelId);
        public Int32 PersonnelId
        {
            get { return GetProperty<Int32>(PersonnelIdProperty); }
        }
        /// <summary>
        ///  PersonnelFlexi
        /// </summary>
        public static readonly ModelPropertyInfo<String> PersonnelFlexiProperty =
            RegisterModelProperty<String>(c => c.PersonnelFlexi);
        public String PersonnelFlexi
        {
            get { return GetProperty<String>(PersonnelFlexiProperty); }
        }
        /// <summary>
        ///  CompanyId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CompanyIdProperty =
            RegisterModelProperty<Int32>(c => c.CompanyId);
        public Int32 CompanyId
        {
            get { return GetProperty<Int32>(CompanyIdProperty); }
        }
        #endregion

        #region Manual Plan Properties
        
        /// <summary>
        /// The MerchandisingGroup Id (Currently Manual Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MerchandisingGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.MerchandisingGroupId);
        public Int32? MerchandisingGroupId
        {
            get { return GetProperty<Int32?>(MerchandisingGroupIdProperty); }
        }

        /// <summary>
        /// SourceFile (Currently Manual Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceFileProperty =
            RegisterModelProperty<String>(c => c.SourceFile);
        public String SourceFile
        {
            get { return GetProperty<String>(SourceFileProperty); }
        }
        /// <summary>
        /// PlanDescription1 (Currently Manual Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanDescription1Property =
            RegisterModelProperty<String>(c => c.PlanDescription1);
        public String PlanDescription1
        {
            get { return GetProperty<String>(PlanDescription1Property); }
        }
        /// <summary>
        /// PlanDescription2 (Currently Manual Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanDescription2Property =
            RegisterModelProperty<String>(c => c.PlanDescription2);
        public String PlanDescription2
        {
            get { return GetProperty<String>(PlanDescription2Property); }
        }
        /// <summary>
        /// PlanDescription3 (Currently Manual Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanDescription3Property =
            RegisterModelProperty<String>(c => c.PlanDescription3);
        public String PlanDescription3
        {
            get { return GetProperty<String>(PlanDescription3Property); }
        }
        /// <summary>
        /// PlanDescription4 (Currently Manual Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanDescription4Property =
            RegisterModelProperty<String>(c => c.PlanDescription4);
        public String PlanDescription4
        {
            get { return GetProperty<String>(PlanDescription4Property); }
        }
        /// <summary>
        /// PlanDescription5 (Currently Manual Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanDescription5Property =
            RegisterModelProperty<String>(c => c.PlanDescription5);
        public String PlanDescription5
        {
            get { return GetProperty<String>(PlanDescription5Property); }
        }
        #endregion

        #region Space Plan Properties
        /// <summary>
        /// ClusterLevelId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ClusterLevelIdProperty =
        RegisterModelProperty<Int32>(c => c.ClusterLevelId);
        public Int32 ClusterLevelId
        {
            get
            {
                return GetProperty<Int32>(ClusterLevelIdProperty);
            }
        }
        /// <summary>
        /// TemplatePlanId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TemplatePlanIdProperty =
        RegisterModelProperty<Int32>(c => c.TemplatePlanId);
        public Int32 TemplatePlanId
        {
            get
            {
                return GetProperty<Int32>(TemplatePlanIdProperty);
            }
        }
        /// <summary>
        /// ClusterAssortmentId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ClusterAssortmentIdProperty =
        RegisterModelProperty<Int32>(c => c.ClusterAssortmentId);
        public Int32 ClusterAssortmentId
        {
            get
            {
                return GetProperty<Int32>(ClusterAssortmentIdProperty);
            }
        }

        /// <summary>
        /// FixturePlanId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixturePlanIdProperty =
        RegisterModelProperty<Int32>(c => c.FixturePlanId);
        public Int32 FixturePlanId
        {
            get
            {
                return GetProperty<Int32>(FixturePlanIdProperty);
            }
        }

        /// <summary>
        /// ScenarioId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ScenarioIdProperty =
        RegisterModelProperty<Int32>(c => c.ScenarioId);
        public Int32 ScenarioId
        {
            get
            {
                return GetProperty<Int32>(ScenarioIdProperty);
            }
        }

        /// <summary>
        /// ReviewDate (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> ReviewDateProperty =
        RegisterModelProperty<DateTime>(c => c.ReviewDate);
        public DateTime ReviewDate
        {
            get
            {
                return GetProperty<DateTime>(ReviewDateProperty);
            }
        }

        /// <summary>
        /// Islive (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> IsliveProperty =
        RegisterModelProperty<Byte>(c => c.Islive);
        public Byte Islive
        {
            get
            {
                return GetProperty<Byte>(IsliveProperty);
            }
        }

        /// <summary>
        /// HasEngineReport (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> HasEngineReportProperty =
        RegisterModelProperty<Byte>(c => c.HasEngineReport);
        public Byte HasEngineReport
        {
            get
            {
                return GetProperty<Byte>(HasEngineReportProperty);
            }
        }

        /// <summary>
        /// RenderType (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> RenderTypeProperty =
        RegisterModelProperty<Byte?>(c => c.RenderType);
        public Byte? RenderType
        {
            get
            {
                return GetProperty<Byte?>(RenderTypeProperty);
            }
        }

        /// <summary>
        /// OverrideBlockSpace (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> OverrideBlockSpaceProperty =
        RegisterModelProperty<Byte>(c => c.OverrideBlockSpace);
        public Byte OverrideBlockSpace
        {
            get
            {
                return GetProperty<Byte>(OverrideBlockSpaceProperty);
            }
        }

        /// <summary>
        /// WorkpackageName (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> WorkpackageNameProperty =
        RegisterModelProperty<String>(c => c.WorkpackageName);
        public String WorkpackageName
        {
            get
            {
                return GetProperty<String>(WorkpackageNameProperty);
            }
        }

        /// <summary>
        /// AssortmentTriangleSetting (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> AssortmentTriangleSettingProperty =
        RegisterModelProperty<String>(c => c.AssortmentTriangleSetting);
        public String AssortmentTriangleSetting
        {
            get
            {
                return GetProperty<String>(AssortmentTriangleSettingProperty);
            }
        }

        /// <summary>
        /// FlexPreservationAllowed (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> FlexPreservationAllowedProperty =
        RegisterModelProperty<Byte>(c => c.FlexPreservationAllowed);
        public Byte FlexPreservationAllowed
        {
            get
            {
                return GetProperty<Byte>(FlexPreservationAllowedProperty);
            }
        }

        /// <summary>
        /// PercentCompromise (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PercentCompromiseProperty =
        RegisterModelProperty<Int32>(c => c.PercentCompromise);
        public Int32 PercentCompromise
        {
            get
            {
                return GetProperty<Int32>(PercentCompromiseProperty);
            }
        }

        /// <summary>
        /// PercentDelistedProducts (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PercentDelistedProductsProperty =
        RegisterModelProperty<Int32>(c => c.PercentDelistedProducts);
        public Int32 PercentDelistedProducts
        {
            get
            {
                return GetProperty<Int32>(PercentDelistedProductsProperty);
            }
        }

        /// <summary>
        /// PercentForceClusterFacingLevels (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PercentForceClusterFacingLevelsProperty =
        RegisterModelProperty<Int32>(c => c.PercentForceClusterFacingLevels);
        public Int32 PercentForceClusterFacingLevels
        {
            get
            {
                return GetProperty<Int32>(PercentForceClusterFacingLevelsProperty);
            }
        }

        /// <summary>
        /// PercentClusterParticipationListing (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PercentClusterParticipationListingProperty =
        RegisterModelProperty<Int32>(c => c.PercentClusterParticipationListing);
        public Int32 PercentClusterParticipationListing
        {
            get
            {
                return GetProperty<Int32>(PercentClusterParticipationListingProperty);
            }
        }

        /// <summary>
        /// ProductsPlacedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductsPlacedCountProperty =
        RegisterModelProperty<Int32>(c => c.ProductsPlacedCount);
        public Int32 ProductsPlacedCount
        {
            get
            {
                return GetProperty<Int32>(ProductsPlacedCountProperty);
            }
        }

        /// <summary>
        /// ProductsUnplacedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductsUnplacedCountProperty =
        RegisterModelProperty<Int32>(c => c.ProductsUnplacedCount);
        public Int32 ProductsUnplacedCount
        {
            get
            {
                return GetProperty<Int32>(ProductsUnplacedCountProperty);
            }
        }

        /// <summary>
        /// ProductsNotRecommendedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductsNotRecommendedCountProperty =
        RegisterModelProperty<Int32>(c => c.ProductsNotRecommendedCount);
        public Int32 ProductsNotRecommendedCount
        {
            get
            {
                return GetProperty<Int32>(ProductsNotRecommendedCountProperty);
            }
        }

        /// <summary>
        /// AuditValue1 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue1Property =
        RegisterModelProperty<Single>(c => c.AuditValue1);
        public Single AuditValue1
        {
            get
            {
                return GetProperty<Single>(AuditValue1Property);
            }
        }

        /// <summary>
        /// AuditValue2 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue2Property =
        RegisterModelProperty<Single>(c => c.AuditValue2);
        public Single AuditValue2
        {
            get
            {
                return GetProperty<Single>(AuditValue2Property);
            }
        }

        /// <summary>
        /// AuditValue3 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue3Property =
        RegisterModelProperty<Single>(c => c.AuditValue3);
        public Single AuditValue3
        {
            get
            {
                return GetProperty<Single>(AuditValue3Property);
            }
        }

        /// <summary>
        /// AuditValue4 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue4Property =
        RegisterModelProperty<Single>(c => c.AuditValue4);
        public Single AuditValue4
        {
            get
            {
                return GetProperty<Single>(AuditValue4Property);
            }
        }

        /// <summary>
        /// AuditValue5 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue5Property =
        RegisterModelProperty<Single>(c => c.AuditValue5);
        public Single AuditValue5
        {
            get
            {
                return GetProperty<Single>(AuditValue5Property);
            }
        }

        /// <summary>
        /// AuditValue6 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue6Property =
        RegisterModelProperty<Single>(c => c.AuditValue6);
        public Single AuditValue6
        {
            get
            {
                return GetProperty<Single>(AuditValue6Property);
            }
        }

        /// <summary>
        /// AuditValue7 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue7Property =
        RegisterModelProperty<Single>(c => c.AuditValue7);
        public Single AuditValue7
        {
            get
            {
                return GetProperty<Single>(AuditValue7Property);
            }
        }

        /// <summary>
        /// AuditValue8 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue8Property =
        RegisterModelProperty<Single>(c => c.AuditValue8);
        public Single AuditValue8
        {
            get
            {
                return GetProperty<Single>(AuditValue8Property);
            }
        }

        /// <summary>
        /// AuditValue9 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue9Property =
        RegisterModelProperty<Single>(c => c.AuditValue9);
        public Single AuditValue9
        {
            get
            {
                return GetProperty<Single>(AuditValue9Property);
            }
        }

        /// <summary>
        /// AuditValue10 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue10Property =
        RegisterModelProperty<Single>(c => c.AuditValue10);
        public Single AuditValue10
        {
            get
            {
                return GetProperty<Single>(AuditValue10Property);
            }
        }

        /// <summary>
        /// AuditValue11 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue11Property =
        RegisterModelProperty<Single>(c => c.AuditValue11);
        public Single AuditValue11
        {
            get
            {
                return GetProperty<Single>(AuditValue11Property);
            }
        }

        /// <summary>
        /// AuditValue12 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue12Property =
        RegisterModelProperty<Single>(c => c.AuditValue12);
        public Single AuditValue12
        {
            get
            {
                return GetProperty<Single>(AuditValue12Property);
            }
        }

        /// <summary>
        /// AuditValue13 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue13Property =
        RegisterModelProperty<Single>(c => c.AuditValue13);
        public Single AuditValue13
        {
            get
            {
                return GetProperty<Single>(AuditValue13Property);
            }
        }

        /// <summary>
        /// AuditValue14 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue14Property =
        RegisterModelProperty<Single>(c => c.AuditValue14);
        public Single AuditValue14
        {
            get
            {
                return GetProperty<Single>(AuditValue14Property);
            }
        }

        /// <summary>
        /// AuditValue15 (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AuditValue15Property =
        RegisterModelProperty<Single>(c => c.AuditValue15);
        public Single AuditValue15
        {
            get
            {
                return GetProperty<Single>(AuditValue15Property);
            }
        }

        /// <summary>
        /// ActionCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ActionCountProperty =
        RegisterModelProperty<Int16>(c => c.ActionCount);
        public Int16 ActionCount
        {
            get
            {
                return GetProperty<Int16>(ActionCountProperty);
            }
        }

        /// <summary>
        /// ActionsAppliedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ActionsAppliedCountProperty =
        RegisterModelProperty<Int16>(c => c.ActionsAppliedCount);
        public Int16 ActionsAppliedCount
        {
            get
            {
                return GetProperty<Int16>(ActionsAppliedCountProperty);
            }
        }

        /// <summary>
        /// ProductsAddedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsAddedCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsAddedCount);
        public Int16 ProductsAddedCount
        {
            get
            {
                return GetProperty<Int16>(ProductsAddedCountProperty);
            }
        }

        /// <summary>
        /// ProductsRemovedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsRemovedCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsRemovedCount);
        public Int16 ProductsRemovedCount
        {
            get
            {
                return GetProperty<Int16>(ProductsRemovedCountProperty);
            }
        }

        /// <summary>
        /// ProductsMovedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsMovedCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsMovedCount);
        public Int16 ProductsMovedCount
        {
            get
            {
                return GetProperty<Int16>(ProductsMovedCountProperty);
            }
        }

        /// <summary>
        /// ProductsReplacedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsReplacedCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsReplacedCount);
        public Int16 ProductsReplacedCount
        {
            get
            {
                return GetProperty<Int16>(ProductsReplacedCountProperty);
            }
        }

        /// <summary>
        /// ProductsIncreasedSpaceCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsIncreasedSpaceCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsIncreasedSpaceCount);
        public Int16 ProductsIncreasedSpaceCount
        {
            get
            {
                return GetProperty<Int16>(ProductsIncreasedSpaceCountProperty);
            }
        }

        /// <summary>
        /// ProductsDecreasedSpaceCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsDecreasedSpaceCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsDecreasedSpaceCount);
        public Int16 ProductsDecreasedSpaceCount
        {
            get
            {
                return GetProperty<Int16>(ProductsDecreasedSpaceCountProperty);
            }
        }

        /// <summary>
        /// AutomaticIncreasedSpaceCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> AutomaticIncreasedSpaceCountProperty =
        RegisterModelProperty<Int16>(c => c.AutomaticIncreasedSpaceCount);
        public Int16 AutomaticIncreasedSpaceCount
        {
            get
            {
                return GetProperty<Int16>(AutomaticIncreasedSpaceCountProperty);
            }
        }

        /// <summary>
        /// AutomaticDecreasedSpaceCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> AutomaticDecreasedSpaceCountProperty =
        RegisterModelProperty<Int16>(c => c.AutomaticDecreasedSpaceCount);
        public Int16 AutomaticDecreasedSpaceCount
        {
            get
            {
                return GetProperty<Int16>(AutomaticDecreasedSpaceCountProperty);
            }
        }

        /// <summary>
        /// SupplyabilityRemovedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> SupplyabilityRemovedCountProperty =
        RegisterModelProperty<Int16>(c => c.SupplyabilityRemovedCount);
        public Int16 SupplyabilityRemovedCount
        {
            get
            {
                return GetProperty<Int16>(SupplyabilityRemovedCountProperty);
            }
        }

        /// <summary>
        /// PercentageOfActionsNotAchieved (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PercentageOfActionsNotAchievedProperty =
        RegisterModelProperty<Single>(c => c.PercentageOfActionsNotAchieved);
        public Single PercentageOfActionsNotAchieved
        {
            get
            {
                return GetProperty<Single>(PercentageOfActionsNotAchievedProperty);
            }
        }

        /// <summary>
        /// PercentageOfChangeOnPlan (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PercentageOfChangeOnPlanProperty =
        RegisterModelProperty<Single>(c => c.PercentageOfChangeOnPlan);
        public Single PercentageOfChangeOnPlan
        {
            get
            {
                return GetProperty<Single>(PercentageOfChangeOnPlanProperty);
            }
        }

        /// <summary>
        /// PercentageOfChangeFromOriginal (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PercentageOfChangeFromOriginalProperty =
        RegisterModelProperty<Single>(c => c.PercentageOfChangeFromOriginal);
        public Single PercentageOfChangeFromOriginal
        {
            get
            {
                return GetProperty<Single>(PercentageOfChangeFromOriginalProperty);
            }
        }

        /// <summary>
        /// ProfileId (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProfileIdProperty =
        RegisterModelProperty<Int32>(c => c.ProfileId);
        public Int32 ProfileId
        {
            get
            {
                return GetProperty<Int32>(ProfileIdProperty);
            }
        }

        /// <summary>
        /// ProfileName (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProfileNameProperty =
        RegisterModelProperty<String>(c => c.ProfileName);
        public String ProfileName
        {
            get
            {
                return GetProperty<String>(ProfileNameProperty);
            }
        }

        /// <summary>
        /// PlaceholderProductCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> PlaceholderProductCountProperty =
        RegisterModelProperty<Int16>(c => c.PlaceholderProductCount);
        public Int16 PlaceholderProductCount
        {
            get
            {
                return GetProperty<Int16>(PlaceholderProductCountProperty);
            }
        }

        /// <summary>
        /// ReplacementProductCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ReplacementProductCountProperty =
        RegisterModelProperty<Int16>(c => c.ReplacementProductCount);
        public Int16 ReplacementProductCount
        {
            get
            {
                return GetProperty<Int16>(ReplacementProductCountProperty);
            }
        }

        /// <summary>
        /// PlaceholderProductPlacedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> PlaceholderProductPlacedCountProperty =
        RegisterModelProperty<Int16>(c => c.PlaceholderProductPlacedCount);
        public Int16 PlaceholderProductPlacedCount
        {
            get
            {
                return GetProperty<Int16>(PlaceholderProductPlacedCountProperty);
            }
        }

        /// <summary>
        /// ReplacementProductPlacedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ReplacementProductPlacedCountProperty =
        RegisterModelProperty<Int16>(c => c.ReplacementProductPlacedCount);
        public Int16 ReplacementProductPlacedCount
        {
            get
            {
                return GetProperty<Int16>(ReplacementProductPlacedCountProperty);
            }
        }

        /// <summary>
        /// ProductCountChangedOnPlan (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductCountChangedOnPlanProperty =
        RegisterModelProperty<Int16>(c => c.ProductCountChangedOnPlan);
        public Int16 ProductCountChangedOnPlan
        {
            get
            {
                return GetProperty<Int16>(ProductCountChangedOnPlanProperty);
            }
        }

        /// <summary>
        /// PercentageDeviationFromBlocking (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PercentageDeviationFromBlockingProperty =
        RegisterModelProperty<Single>(c => c.PercentageDeviationFromBlocking);
        public Single PercentageDeviationFromBlocking
        {
            get
            {
                return GetProperty<Single>(PercentageDeviationFromBlockingProperty);
            }
        }

        /// <summary>
        /// PercentageWhiteSpace (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PercentageWhiteSpaceProperty =
        RegisterModelProperty<Single>(c => c.PercentageWhiteSpace);
        public Single PercentageWhiteSpace
        {
            get
            {
                return GetProperty<Single>(PercentageWhiteSpaceProperty);
            }
        }

        /// <summary>
        /// BlocksDroppedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> BlocksDroppedCountProperty =
        RegisterModelProperty<Int16>(c => c.BlocksDroppedCount);
        public Int16 BlocksDroppedCount
        {
            get
            {
                return GetProperty<Int16>(BlocksDroppedCountProperty);
            }
        }

        /// <summary>
        /// ProductsNotAchievedInventoryCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsNotAchievedInventoryCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsNotAchievedInventoryCount);
        public Int16 ProductsNotAchievedInventoryCount
        {
            get
            {
                return GetProperty<Int16>(ProductsNotAchievedInventoryCountProperty);
            }
        }

        /// <summary>
        /// ProductsMovedFromTemplateCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> ProductsMovedFromTemplateCountProperty =
        RegisterModelProperty<Int16>(c => c.ProductsMovedFromTemplateCount);
        public Int16 ProductsMovedFromTemplateCount
        {
            get
            {
                return GetProperty<Int16>(ProductsMovedFromTemplateCountProperty);
            }
        }

        /// <summary>
        /// AttributesChangedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> AttributesChangedCountProperty =
        RegisterModelProperty<Int16>(c => c.AttributesChangedCount);
        public Int16 AttributesChangedCount
        {
            get
            {
                return GetProperty<Int16>(AttributesChangedCountProperty);
            }
        }

        /// <summary>
        /// AttributesRemovedCount (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> AttributesRemovedCountProperty =
        RegisterModelProperty<Int16>(c => c.AttributesRemovedCount);
        public Int16 AttributesRemovedCount
        {
            get
            {
                return GetProperty<Int16>(AttributesRemovedCountProperty);
            }
        }

        #endregion

        #region ListProperties

        /// <summary>
        ///  
        /// </summary>
        public static readonly ModelPropertyInfo<PlanBayList> PlanBaysProperty =
        RegisterModelProperty<PlanBayList>(c => c.PlanBays);
        public PlanBayList PlanBays
        {
            get
            {
                if (GetProperty<PlanBayList>(PlanBaysProperty) == null)
                {
                    if (planBayLock == null) planBayLock = new Object();

                    lock (planBayLock)
                    {
                        if (GetProperty<PlanBayList>(PlanBaysProperty) == null)
                        {
                            LoadProperty<PlanBayList>(PlanBaysProperty, PlanBayList.GetPlanBaysByPlanId(Id, PlanType));
                            OnPropertyChanged(PlanBaysProperty);
                        }
                    }

                }
                return GetProperty<PlanBayList>(PlanBaysProperty);
            }
            private set
            {
                SetProperty<PlanBayList>(PlanBaysProperty, value);
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static readonly ModelPropertyInfo<PlanElementList> PlanElementsProperty =
        RegisterModelProperty<PlanElementList>(c => c.PlanElements);
        public PlanElementList PlanElements
        {
            get
            {
                if (GetProperty<PlanElementList>(PlanElementsProperty) == null)
                {
                    if (planElementLock == null) planElementLock = new Object();

                    lock (planElementLock)
                    {
                        if (GetProperty<PlanElementList>(PlanElementsProperty) == null)
                        {
                            LoadProperty<PlanElementList>(PlanElementsProperty, PlanElementList.GetPlanElementsByPlanId(Id, PlanType));
                            OnPropertyChanged(PlanElementsProperty);
                        }
                    }

                }
                return GetProperty<PlanElementList>(PlanElementsProperty);
            }
            private set
            {
                SetProperty<PlanElementList>(PlanElementsProperty, value);
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static readonly ModelPropertyInfo<PlanPositionList> PlanPositionsProperty =
        RegisterModelProperty<PlanPositionList>(c => c.PlanPositions);
        public PlanPositionList PlanPositions
        {
            get
            {
                if (GetProperty<PlanPositionList>(PlanPositionsProperty) == null)
                {
                    if (planPositionLock == null) planPositionLock = new Object();

                    lock (planPositionLock)
                    {
                        if (GetProperty<PlanPositionList>(PlanPositionsProperty) == null)
                        {
                            LoadProperty<PlanPositionList>(PlanPositionsProperty, PlanPositionList.GetPlanPositionsByPlanId(Id, PlanType, this.PlanElements));
                           // OnPropertyChanged(PlanPositionsProperty);
                        }
                    }

                }
                return GetProperty<PlanPositionList>(PlanPositionsProperty);
            }
            private set
            {
                SetProperty<PlanPositionList>(PlanPositionsProperty, value);
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static readonly ModelPropertyInfo<PlanTextBoxList> PlanTextBoxsProperty =
        RegisterModelProperty<PlanTextBoxList>(c => c.PlanTextBoxs);
        public PlanTextBoxList PlanTextBoxs
        {
            get
            {
                if (GetProperty<PlanTextBoxList>(PlanTextBoxsProperty) == null)
                {
                    if (planTextBoxLock == null) planTextBoxLock = new Object();

                    lock (planTextBoxLock)
                    {
                        if (GetProperty<PlanTextBoxList>(PlanTextBoxsProperty) == null)
                        {
                            LoadProperty<PlanTextBoxList>(PlanTextBoxsProperty, PlanTextBoxList.GetPlanTextBoxsByPlanId(Id, PlanType));
                            OnPropertyChanged(PlanTextBoxsProperty);
                        }
                    }

                }
                return GetProperty<PlanTextBoxList>(PlanTextBoxsProperty);
            }
            private set
            {
                SetProperty<PlanTextBoxList>(PlanTextBoxsProperty, value);
            }
        }

        /// <summary>
        ///  Template Plan Positions
        /// </summary>
        public static readonly ModelPropertyInfo<PlanPositionList> TemplatePlanPositionsProperty =
        RegisterModelProperty<PlanPositionList>(c => c.TemplatePlanPositions);
        public PlanPositionList TemplatePlanPositions
        {
            get
            {
                if (GetProperty<PlanPositionList>(TemplatePlanPositionsProperty) == null)
                {
                    if (templatePlanPositionLock == null) templatePlanPositionLock = new Object();

                    lock (templatePlanPositionLock)
                    {
                        if (GetProperty<PlanPositionList>(TemplatePlanPositionsProperty) == null)
                        {
                            if (this.PlanType == Model.PlanType.Cluster || this.PlanType == Model.PlanType.Store)
                            {

                                LoadProperty<PlanPositionList>(TemplatePlanPositionsProperty, PlanPositionList.GetPlanPositionsByPlanId(this.TemplatePlanId, Model.PlanType.Merchandising, this.PlanElements));
                            }
                            else
                            {
                                LoadProperty<PlanPositionList>(TemplatePlanPositionsProperty, PlanPositionList.EmptyList());
                            }
                            OnPropertyChanged(TemplatePlanPositionsProperty);
                        }
                    }

                }
                return GetProperty<PlanPositionList>(TemplatePlanPositionsProperty);
            }
            private set
            {
                SetProperty<PlanPositionList>(TemplatePlanPositionsProperty, value);
            }
        }


        /// <summary>
        ///  
        /// </summary>
        public static readonly ModelPropertyInfo<PlanProfileList> PlanProfileRowsProperty =
        RegisterModelProperty<PlanProfileList>(c => c.PlanProfileRows);
        public PlanProfileList PlanProfileRows
        {
            get
            {
                return GetProperty<PlanProfileList>(PlanProfileRowsProperty);
            }
        }

        public static readonly ModelPropertyInfo<AssortmentList> AssortmentProperty =
            RegisterModelProperty<AssortmentList>(c => c.Assortment);
        public AssortmentList Assortment
        {
            get
            {
                return GetProperty<AssortmentList>(AssortmentProperty);
            }
        }

        #endregion

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class FetchByPlanIdAndTypeCriteria : Csla.CriteriaBase<FetchByPlanIdAndTypeCriteria>
        {
            public static readonly PropertyInfo<Int32> PlanIdProperty =
                RegisterProperty<Int32>(c => c.PlanId);
            public Int32 PlanId
            {
                get { return ReadProperty<Int32>(PlanIdProperty); }
            }

            public static readonly PropertyInfo<PlanType> PlanTypeProperty =
                    RegisterProperty<PlanType>(c => c.PlanType);
            public PlanType PlanType
            {
                get { return ReadProperty<PlanType>(PlanTypeProperty); }
            }

            public FetchByPlanIdAndTypeCriteria(Int32 PlanId, PlanType planType)
            {
                LoadProperty<Int32>(PlanIdProperty, PlanId);
                LoadProperty<PlanType>(PlanTypeProperty, planType);
            }
        }

        #endregion

        #region Methods

        #region Planogram Id Helpers
        private Int32 _currentSubComponentId = 0;
        private Int32 NextSubComponentId
        {
            get
            {
                _currentSubComponentId++;
                return _currentSubComponentId;
            }
        }
        private Int32 CurrentSubComponentId
        {
            get
            {
                return _currentSubComponentId;
            }
            set
            {
                _currentSubComponentId = value;
            }
        }

        private Int16 _baySequenceId = 0;
        private Int16 NextBaySequenceId
        {
            get
            {
                _baySequenceId++;
                return _baySequenceId;
            }
        }
        private Int16 CurrenBaySequenceId
        {
            get
            {
                return _baySequenceId;
            }
            set
            {
                _baySequenceId = value;
            }
        }
        #endregion

        ///// <summary>
        ///// Creates a list of products using PlanPosition as a basis
        ///// </summary>
        ///// <returns>Unique list of products</returns>
        //private List<PlanogramProduct> CreateProducts(SplitPlan splitPlan, out Int32 placedProductsCount, out Int32 allProductsCount)
        //{
        //    IEnumerable<PlanPosition> uniquePositions = PlanPositions.Distinct(new ProductIdComparer());
        //    // according to Mark this is the placed products count (condition excludes preserved space)
        //    placedProductsCount = uniquePositions.Where(p => p.ProductId > 0).Count();

        //    List<PlanPosition> uniqueTemplatePositions = TemplatePlanPositions.Distinct(new ProductIdComparer()).ToList();
        //    //Remove all positions from the template list that already exist in the plan list.
        //    //this could be doen using the distinct function but I want to make sure that any plan
        //    //records take precident.
        //    uniqueTemplatePositions.RemoveAll(t => uniquePositions.Where(p => p.ProductId == t.ProductId).Count() > 0);

        //    //union the two lists together to get the full list of all products
        //    uniquePositions = uniquePositions.Union(uniqueTemplatePositions);
        //    // according to Mark this is the all products count (condition excludes preserved space)
        //    allProductsCount = uniquePositions.Where(p => p.ProductId > 0 ).Count();

        //    List<PlanogramProduct> output = new List<PlanogramProduct>(uniquePositions.Count());

        //    foreach (PlanPosition uniqueProduct in uniquePositions)
        //    {
                
        //        if (uniqueProduct.ProductId > 0)
        //        {                   
        //            if (splitPlan.Positions.Where(p => p.ProductId == uniqueProduct.ProductId).GroupBy(pg => pg.TrayProduct).Count() > 1)
        //            {
        //                throw new Exception(Message.PlanProduct_UnitTrayError);
        //            }

        //            PlanogramProduct product = new PlanogramProduct();
        //            Product currentProduct = null;
        //            PlanElement firstElement = PlanElements.FirstOrDefault(e => e.Id == uniqueProduct.PlanElementId);
                    
                    
        //            //TODO: Handle error better
        //            try
        //            {
        //                currentProduct = Product.GetProductById(uniqueProduct.ProductId);
        //            }
        //            catch { }
        //            if (currentProduct == null) throw new Exception(Message.PlanProduct_DoesnNotExist);
        //            product.Name = currentProduct.Name;
        //            //TODO: Handle code length better (requires proper data!)
        //            if (currentProduct.Code.Length > 14)
        //            {
        //                product.GTIN = currentProduct.Code.Substring(currentProduct.Code.Length - 14, 14);
        //            }
        //            else
        //            {
        //                product.GTIN = currentProduct.Code;
        //            }
        //            product.Brand = currentProduct.Text1;

        //            product.Id = uniqueProduct.ProductId;

        //            //CCM products start sqeezed.
        //            product.Depth = (Single)uniqueProduct.ProductDepth / uniqueProduct.SqueezeDepth;
        //            product.Height = (Single)uniqueProduct.ProductHeight / uniqueProduct.SqueezeHeight;
        //            product.Width = (Single)uniqueProduct.ProductWidth / uniqueProduct.SqueezeWidth;
        //            product.SqueezeHeight = (Single)uniqueProduct.ProductHeight;
        //            product.SqueezeWidth = (Single)uniqueProduct.ProductWidth;
        //            product.SqueezeDepth = (Single)uniqueProduct.ProductDepth;
                    

        //            product.AlternateDepth = 0;
        //            product.AlternateHeight = 0;
        //            product.AlternateWidth = 0;
        //            product.CanBreakTrayBack = uniqueProduct.BreakTrayBack > 0;
        //            product.CanBreakTrayDown = uniqueProduct.BreakTrayDown > 0;
        //            product.CanBreakTrayTop = uniqueProduct.BreakTrayTop > 0;
        //            product.CanBreakTrayUp = uniqueProduct.BreakTrayUp > 0;
        //            product.CasePackUnits = (Int16)uniqueProduct.CasePack;
        //            product.DisplayDepth = 0;
        //            product.DisplayHeight = 0;
        //            product.DisplayWidth = 0;
        //            product.ExtensionData = null;
        //            product.FingerSpaceAbove = 0; //FROM ELEMENT?????
        //            product.FingerSpaceToTheSide = 0;
        //            product.ForceBottomCap = false;
        //            product.ForceMiddleCap = uniqueProduct.MiddleCapping > 0;
        //            product.FrontOverhang = 0; //FROM ELEMENT?????
        //            product.IsActive = true;
        //            product.IsFrontOnly = uniqueProduct.FrontOnly != 0;
        //            product.IsPlaceHolderProduct = false; //TODO
        //            product.IsTrayProduct = uniqueProduct.TrayProduct != 0;
        //            product.MaxDeep = (Byte)uniqueProduct.MaxDeep;
        //            product.MaxNestingDeep = uniqueProduct.MaxNestDeep == null ? Byte.MinValue : (Byte)uniqueProduct.MaxNestDeep;
        //            product.MaxNestingHigh = uniqueProduct.MaxNestHigh == null ? Byte.MinValue : (Byte)uniqueProduct.MaxNestHigh;
        //            product.MaxRightCap = uniqueProduct.MaxRightCap == null ? Byte.MinValue : (Byte)uniqueProduct.MaxRightCap;
        //            product.MaxStack = (Byte)uniqueProduct.MaxStack;
        //            product.MaxTopCap = (Byte)uniqueProduct.MaxCap;
        //            product.MerchandisingStyle = uniqueProduct.TrayProduct != 0 ? "Tray" : "Unit";
        //            product.MinDeep = (Byte)uniqueProduct.MinDeep;
        //            product.NestingDepth = uniqueProduct.NestDepth == null ? 0 : (Single)uniqueProduct.NestDepth;
        //            product.NestingHeight = uniqueProduct.NestHeight == null ? 0 : (Single)uniqueProduct.NestHeight;
        //            product.NestingWidth = uniqueProduct.NestWidth == null ? 0 : (Single)uniqueProduct.NestWidth;
        //            product.NumberOfPegHoles = uniqueProduct.PegX > 0 ? (Byte)1 : (Byte)0; //TODO
        //            product.OrientationType = "Front";//(Byte)uniqueProduct.OrientationId; //TODO
        //            product.PegDepth = uniqueProduct.PegDepth;
        //            product.PegProngOffset = uniqueProduct.PegProng == null ? 0 : (Single)uniqueProduct.PegProng; //CHECK THIS
        //            product.PegX = uniqueProduct.PegX;
        //            product.PegX2 = 0;
        //            product.PegX3 = 0;
        //            product.PegY = uniqueProduct.PegY;
        //            product.PegY2 = 0;
        //            product.PegY3 = 0;
        //            product.PointOfPurchaseDepth = 0;
        //            product.PointOfPurchaseHeight = 0;
        //            product.PointOfPurchaseWidth = 0;
                    
        //            product.StatusType = "Active";
        //            product.TrayDeep = (Byte)uniqueProduct.TrayDeep;
        //            product.TrayHigh = (Byte)uniqueProduct.TrayHigh;
        //            product.TrayWide = (Byte)uniqueProduct.TrayWide;
        //            product.TrayThickDepth = uniqueProduct.TrayThickDepth;
        //            product.TrayThickHeight = uniqueProduct.TrayThickHeight;
        //            product.TrayThickWidth = uniqueProduct.TrayThickWidth;
        //            product.TrayWidth = (Single)(uniqueProduct.TrayWidth);
        //            product.TrayHeight = (Single)(uniqueProduct.TrayHeight);
        //            product.TrayDepth = (Single)(uniqueProduct.TrayDepth);
        //            product.TrayPackUnits = (Int16)(uniqueProduct.TrayUnits);
        //            product.CasePackUnits = (Int16)uniqueProduct.CasePack;
                    
        //            output.Add(product);
        //        }
        //    }

        //    return output;
        //}
        #endregion


        public void Dispose()
        {
            this.PlanBays = null;
            this.PlanElements = null;
            this.PlanPositions = null;
            this.PlanTextBoxs = null;
            this.TemplatePlanPositions = null;
            SetParent(null);
        }
    }

}
