﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#region Version History: (CCM.Net 7.5)
// CCM-20762 : M.Brumby
//  Added update
#endregion
#region Version History: (CCM.Net 7.5.1)
//  CCM-22192 : M.Brumby
//      Added StoreCode so that for store specific plans the location code 
//      field can be populated when publishing to GFS
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class Plan
    {
        #region Constructor
        private Plan() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing plan
        /// </summary>
        /// <returns>A list of plans</returns>
        public static Plan GetPlanById(Int32 PlanId, PlanType planType)
        {
            return DataPortal.Fetch<Plan>(new FetchByPlanIdAndTypeCriteria(PlanId, planType));
        }

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static Plan GetPlan(IDalContext dalContext, PlanDto dto)
        {
            return DataPortal.FetchChild<Plan>(dalContext, dto);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanDto dto)
        {
            #region Base Properties
            this.LoadProperty<String>(CategoryReviewNameProperty, dto.CategoryReviewName);
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            this.LoadProperty<PlanType>(PlanTypeProperty, (PlanType)dto.PlanType);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(UniqueItemNameProperty, dto.UniqueItemName);
            this.LoadProperty<Int32>(StoreIdProperty, dto.StoreId);
            this.LoadProperty<String>(StoreCodeProperty, dto.StoreCode);
            this.LoadProperty<String>(StoreFlexiProperty, dto.StoreFlexi);
            this.LoadProperty<Int32>(ProductLevelIdProperty, dto.ProductLevelId);
            this.LoadProperty<String>(MerchandisingGroupFlexiProperty, dto.MerchandisingGroupFlexi);
            this.LoadProperty<DateTime?>(CreatedDateProperty, dto.CreatedDate);
            this.LoadProperty<DateTime?>(IssuedDateProperty, dto.IssuedDate);
            this.LoadProperty<DateTime?>(ValidDateProperty, dto.ValidDate);
            this.LoadProperty<DateTime?>(RangingDateProperty, dto.RangingDate);
            this.LoadProperty<DateTime?>(PublishingDateProperty, dto.PublishingDate);
            this.LoadProperty<Byte>(DeletedProperty, dto.Deleted);
            this.LoadProperty<Byte>(IssuedProperty, dto.Issued);
            this.LoadProperty<Byte>(FailedProperty, dto.Failed);
            this.LoadProperty<Double>(MerchShelfSpaceProperty, dto.MerchShelfSpace);
            this.LoadProperty<Double>(MerchPegSpaceProperty, dto.MerchPegSpace);
            this.LoadProperty<Double>(MerchBarSpaceProperty, dto.MerchBarSpace);
            this.LoadProperty<Double>(MerchChestSpaceProperty, dto.MerchChestSpace);
            this.LoadProperty<Double>(Number1Property, dto.Number1);
            this.LoadProperty<Double>(Number2Property, dto.Number2);
            this.LoadProperty<Int32>(Number3Property, dto.Number3);
            this.LoadProperty<Int32>(Number4Property, dto.Number4);
            this.LoadProperty<Int32>(Number5Property, dto.Number5);
            this.LoadProperty<String>(Text1Property, dto.Text1);
            this.LoadProperty<String>(Text2Property, dto.Text2);
            this.LoadProperty<String>(Text3Property, dto.Text3);
            this.LoadProperty<String>(Text4Property, dto.Text4);
            this.LoadProperty<String>(Text5Property, dto.Text5);
            this.LoadProperty<String>(Text6Property, dto.Text6);
            this.LoadProperty<String>(Text7Property, dto.Text7);
            this.LoadProperty<String>(Text8Property, dto.Text8);
            this.LoadProperty<String>(Text9Property, dto.Text9);
            this.LoadProperty<String>(Text10Property, dto.Text10);
            this.LoadProperty<Byte?>(ElementPresentationProperty, dto.ElementPresentation);
            this.LoadProperty<Byte?>(NestedMerchHeightProperty, dto.NestedMerchHeight);
            this.LoadProperty<Double?>(FingerSpaceProperty, dto.FingerSpace);
            this.LoadProperty<Byte?>(BreakMerchHeightProperty, dto.BreakMerchHeight);
            this.LoadProperty<Byte?>(BreakMerchDepthProperty, dto.BreakMerchDepth);
            this.LoadProperty<Int32>(CategoryReviewIdProperty, dto.CategoryReviewId);
            this.LoadProperty<Int32?>(ParentCategoryReviewIdProperty, dto.ParentCategoryReviewId);
            this.LoadProperty<DateTime>(CreatedProperty, dto.Created);
            this.LoadProperty<DateTime>(ModifiedProperty, dto.Modified);
            this.LoadProperty<Int32>(PersonnelIdProperty, dto.PersonnelId);
            this.LoadProperty<String>(PersonnelFlexiProperty, dto.PersonnelFlexi);
            this.LoadProperty<Int32>(CompanyIdProperty, dto.CompanyId);
            #endregion

            #region Space Plans
            if (dto is SpacePlanDto)
            {
                this.LoadProperty<Int32>(ClusterLevelIdProperty, (dto as SpacePlanDto).ClusterLevelId);
                this.LoadProperty<Int32>(TemplatePlanIdProperty, (dto as SpacePlanDto).TemplatePlanId);
                this.LoadProperty<Int32>(ClusterAssortmentIdProperty, (dto as SpacePlanDto).ClusterAssortmentId);
                this.LoadProperty<Int32>(FixturePlanIdProperty, (dto as SpacePlanDto).FixturePlanId);
                this.LoadProperty<Int32>(ScenarioIdProperty, (dto as SpacePlanDto).ScenarioId);
                this.LoadProperty<DateTime>(ReviewDateProperty, (dto as SpacePlanDto).ReviewDate);
                this.LoadProperty<Byte>(IsliveProperty, (dto as SpacePlanDto).Islive);
                this.LoadProperty<Byte>(HasEngineReportProperty, (dto as SpacePlanDto).HasEngineReport);
                this.LoadProperty<Byte?>(RenderTypeProperty, (dto as SpacePlanDto).RenderType);
                this.LoadProperty<Byte>(OverrideBlockSpaceProperty, (dto as SpacePlanDto).OverrideBlockSpace);
                this.LoadProperty<String>(WorkpackageNameProperty, (dto as SpacePlanDto).WorkpackageName);
                this.LoadProperty<String>(AssortmentTriangleSettingProperty, (dto as SpacePlanDto).AssortmentTriangleSetting);
                this.LoadProperty<Byte>(FlexPreservationAllowedProperty, (dto as SpacePlanDto).FlexPreservationAllowed);
                this.LoadProperty<Int32>(PercentCompromiseProperty, (dto as SpacePlanDto).PercentCompromise);
                this.LoadProperty<Int32>(PercentDelistedProductsProperty, (dto as SpacePlanDto).PercentDelistedProducts);
                this.LoadProperty<Int32>(PercentForceClusterFacingLevelsProperty, (dto as SpacePlanDto).PercentForceClusterFacingLevels);
                this.LoadProperty<Int32>(PercentClusterParticipationListingProperty, (dto as SpacePlanDto).PercentClusterParticipationListing);
                this.LoadProperty<Int32>(ProductsPlacedCountProperty, (dto as SpacePlanDto).ProductsPlacedCount);
                this.LoadProperty<Int32>(ProductsUnplacedCountProperty, (dto as SpacePlanDto).ProductsUnplacedCount);
                this.LoadProperty<Int32>(ProductsNotRecommendedCountProperty, (dto as SpacePlanDto).ProductsNotRecommendedCount);
                this.LoadProperty<Single>(AuditValue1Property, (dto as SpacePlanDto).AuditValue1);
                this.LoadProperty<Single>(AuditValue2Property, (dto as SpacePlanDto).AuditValue2);
                this.LoadProperty<Single>(AuditValue3Property, (dto as SpacePlanDto).AuditValue3);
                this.LoadProperty<Single>(AuditValue4Property, (dto as SpacePlanDto).AuditValue4);
                this.LoadProperty<Single>(AuditValue5Property, (dto as SpacePlanDto).AuditValue5);
                this.LoadProperty<Single>(AuditValue6Property, (dto as SpacePlanDto).AuditValue6);
                this.LoadProperty<Single>(AuditValue7Property, (dto as SpacePlanDto).AuditValue7);
                this.LoadProperty<Single>(AuditValue8Property, (dto as SpacePlanDto).AuditValue8);
                this.LoadProperty<Single>(AuditValue9Property, (dto as SpacePlanDto).AuditValue9);
                this.LoadProperty<Single>(AuditValue10Property, (dto as SpacePlanDto).AuditValue10);
                this.LoadProperty<Single>(AuditValue11Property, (dto as SpacePlanDto).AuditValue11);
                this.LoadProperty<Single>(AuditValue12Property, (dto as SpacePlanDto).AuditValue12);
                this.LoadProperty<Single>(AuditValue13Property, (dto as SpacePlanDto).AuditValue13);
                this.LoadProperty<Single>(AuditValue14Property, (dto as SpacePlanDto).AuditValue14);
                this.LoadProperty<Single>(AuditValue15Property, (dto as SpacePlanDto).AuditValue15);
                this.LoadProperty<Int16>(ActionCountProperty, (dto as SpacePlanDto).ActionCount);
                this.LoadProperty<Int16>(ActionsAppliedCountProperty, (dto as SpacePlanDto).ActionsAppliedCount);
                this.LoadProperty<Int16>(ProductsAddedCountProperty, (dto as SpacePlanDto).ProductsAddedCount);
                this.LoadProperty<Int16>(ProductsRemovedCountProperty, (dto as SpacePlanDto).ProductsRemovedCount);
                this.LoadProperty<Int16>(ProductsMovedCountProperty, (dto as SpacePlanDto).ProductsMovedCount);
                this.LoadProperty<Int16>(ProductsReplacedCountProperty, (dto as SpacePlanDto).ProductsReplacedCount);
                this.LoadProperty<Int16>(ProductsIncreasedSpaceCountProperty, (dto as SpacePlanDto).ProductsIncreasedSpaceCount);
                this.LoadProperty<Int16>(ProductsDecreasedSpaceCountProperty, (dto as SpacePlanDto).ProductsDecreasedSpaceCount);
                this.LoadProperty<Int16>(AutomaticIncreasedSpaceCountProperty, (dto as SpacePlanDto).AutomaticIncreasedSpaceCount);
                this.LoadProperty<Int16>(AutomaticDecreasedSpaceCountProperty, (dto as SpacePlanDto).AutomaticDecreasedSpaceCount);
                this.LoadProperty<Int16>(SupplyabilityRemovedCountProperty, (dto as SpacePlanDto).SupplyabilityRemovedCount);
                this.LoadProperty<Single>(PercentageOfActionsNotAchievedProperty, (dto as SpacePlanDto).PercentageOfActionsNotAchieved);
                this.LoadProperty<Single>(PercentageOfChangeOnPlanProperty, (dto as SpacePlanDto).PercentageOfChangeOnPlan);
                this.LoadProperty<Single>(PercentageOfChangeFromOriginalProperty, (dto as SpacePlanDto).PercentageOfChangeFromOriginal);
                this.LoadProperty<Int32>(ProfileIdProperty, (dto as SpacePlanDto).ProfileId);
                this.LoadProperty<String>(ProfileNameProperty, (dto as SpacePlanDto).ProfileName);
                this.LoadProperty<Int16>(PlaceholderProductCountProperty, (dto as SpacePlanDto).PlaceholderProductCount);
                this.LoadProperty<Int16>(ReplacementProductCountProperty, (dto as SpacePlanDto).ReplacementProductCount);
                this.LoadProperty<Int16>(PlaceholderProductPlacedCountProperty, (dto as SpacePlanDto).PlaceholderProductPlacedCount);
                this.LoadProperty<Int16>(ReplacementProductPlacedCountProperty, (dto as SpacePlanDto).ReplacementProductPlacedCount);
                this.LoadProperty<Int16>(ProductCountChangedOnPlanProperty, (dto as SpacePlanDto).ProductCountChangedOnPlan);
                this.LoadProperty<Single>(PercentageDeviationFromBlockingProperty, (dto as SpacePlanDto).PercentageDeviationFromBlocking);
                this.LoadProperty<Single>(PercentageWhiteSpaceProperty, (dto as SpacePlanDto).PercentageWhiteSpace);
                this.LoadProperty<Int16>(BlocksDroppedCountProperty, (dto as SpacePlanDto).BlocksDroppedCount);
                this.LoadProperty<Int16>(ProductsNotAchievedInventoryCountProperty, (dto as SpacePlanDto).ProductsNotAchievedInventoryCount);
                this.LoadProperty<Int16>(ProductsMovedFromTemplateCountProperty, (dto as SpacePlanDto).ProductsMovedFromTemplateCount);
                this.LoadProperty<Int16>(AttributesChangedCountProperty, (dto as SpacePlanDto).AttributesChangedCount);
                this.LoadProperty<Int16>(AttributesRemovedCountProperty, (dto as SpacePlanDto).AttributesRemovedCount);
            }
            #endregion

            #region Manual Plan
            if (dto is ManualPlanDto)
            {
                this.LoadProperty<Int32?>(MerchandisingGroupIdProperty, (dto as ManualPlanDto).MerchandisingGroupId);
                this.LoadProperty<String>(SourceFileProperty, (dto as ManualPlanDto).SourceFile);
                this.LoadProperty<String>(PlanDescription1Property, (dto as ManualPlanDto).PlanDescription1);
                this.LoadProperty<String>(PlanDescription2Property, (dto as ManualPlanDto).PlanDescription2);
                this.LoadProperty<String>(PlanDescription3Property, (dto as ManualPlanDto).PlanDescription3);
                this.LoadProperty<String>(PlanDescription4Property, (dto as ManualPlanDto).PlanDescription4);
                this.LoadProperty<String>(PlanDescription5Property, (dto as ManualPlanDto).PlanDescription5);
            }
            #endregion

            this.LoadProperty<PlanProfileList>(PlanProfileRowsProperty, PlanProfileList.GetPlanProfileRowsByPlanId(dto.Id, this.PlanType));
            this.LoadProperty<AssortmentList>(AssortmentProperty, AssortmentList.GetPlanProfileRowsByPlanId(dto.Id, this.PlanType));
            
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        /// <returns>A new data transfer object</returns>
        private PlanDto GetDataTransferObject()
        {
            return new PlanDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty),
                PlanType = (Byte)ReadProperty<PlanType>(PlanTypeProperty),
                Name = ReadProperty<String>(NameProperty),
                UniqueItemName = ReadProperty<String>(UniqueItemNameProperty),
                StoreId = ReadProperty<Int32>(StoreIdProperty),
                StoreFlexi = ReadProperty<String>(StoreFlexiProperty),
                ProductLevelId = ReadProperty<Int32>(ProductLevelIdProperty),
                MerchandisingGroupFlexi = ReadProperty<String>(MerchandisingGroupFlexiProperty),
                CreatedDate = ReadProperty<DateTime?>(CreatedDateProperty),
                IssuedDate = ReadProperty<DateTime?>(IssuedDateProperty),
                ValidDate = ReadProperty<DateTime?>(ValidDateProperty),
                RangingDate = ReadProperty<DateTime?>(RangingDateProperty),
                PublishingDate = ReadProperty<DateTime?>(PublishingDateProperty),
                Deleted = ReadProperty<Byte>(DeletedProperty),
                Issued = ReadProperty<Byte>(IssuedProperty),
                Failed = ReadProperty<Byte>(FailedProperty),
                MerchShelfSpace = ReadProperty<Double>(MerchShelfSpaceProperty),
                MerchPegSpace = ReadProperty<Double>(MerchPegSpaceProperty),
                MerchBarSpace = ReadProperty<Double>(MerchBarSpaceProperty),
                MerchChestSpace = ReadProperty<Double>(MerchChestSpaceProperty),
                Number1 = ReadProperty<Double>(Number1Property),
                Number2 = ReadProperty<Double>(Number2Property),
                Number3 = ReadProperty<Int32>(Number3Property),
                Number4 = ReadProperty<Int32>(Number4Property),
                Number5 = ReadProperty<Int32>(Number5Property),
                Text1 = ReadProperty<String>(Text1Property),
                Text2 = ReadProperty<String>(Text2Property),
                Text3 = ReadProperty<String>(Text3Property),
                Text4 = ReadProperty<String>(Text4Property),
                Text5 = ReadProperty<String>(Text5Property),
                Text6 = ReadProperty<String>(Text6Property),
                Text7 = ReadProperty<String>(Text7Property),
                Text8 = ReadProperty<String>(Text8Property),
                Text9 = ReadProperty<String>(Text9Property),
                Text10 = ReadProperty<String>(Text10Property),
                ElementPresentation = ReadProperty<Byte?>(ElementPresentationProperty),
                NestedMerchHeight = ReadProperty<Byte?>(NestedMerchHeightProperty),
                FingerSpace = ReadProperty<Double?>(FingerSpaceProperty),
                BreakMerchHeight = ReadProperty<Byte?>(BreakMerchHeightProperty),
                BreakMerchDepth = ReadProperty<Byte?>(BreakMerchDepthProperty),
                CategoryReviewId = ReadProperty<Int32>(CategoryReviewIdProperty),
                ParentCategoryReviewId = ReadProperty<Int32?>(ParentCategoryReviewIdProperty),
                Created = ReadProperty<DateTime>(CreatedProperty),
                Modified = ReadProperty<DateTime>(ModifiedProperty),
                PersonnelId = ReadProperty<Int32>(PersonnelIdProperty),
                PersonnelFlexi = ReadProperty<String>(PersonnelFlexiProperty),
                CompanyId = ReadProperty<Int32>(CompanyIdProperty)
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }


        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanIdAndTypeCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanDal dal = dalContext.GetDal<IPlanDal>())
                {
                    PlanDto dto = null;

                    switch (criteria.PlanType)
                    {
                        case PlanType.Manual:
                            dto = dal.FetchManualByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Merchandising:
                            dto = dal.FetchMerchandisingByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Cluster:
                            dto = dal.FetchClusterByPlanId(criteria.PlanId);
                            break;
                        case PlanType.Store:
                            dto = dal.FetchStoreByPlanId(criteria.PlanId);
                            break;
                    }

                    if (dto != null) LoadDataTransferObject(dalContext, dto);
                }
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPlanDal dal = dalContext.GetDal<IPlanDal>())
                {
                    PlanDto dto = GetDataTransferObject();
                    dal.Update(dto);
                    LoadProperty<DateTime>(ModifiedProperty, dto.Modified);
                }

                //Child update not required/Implemented yet.
                //FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion
    }
}
