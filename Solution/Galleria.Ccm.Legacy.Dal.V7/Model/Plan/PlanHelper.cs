﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.4)
// CCM-18877 : D.Pleasance
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Model
{
    public class PlanHelper
    {
        #region Fields

        private static Int16 _currentFixtureItemId = 0;
        private static Int16 _currentFixtureId = 0;
        private static Int16 _currentFixtureAssemblyId = 0;
        private static Int32 _currentAssemblyId = 0;
        private static Int16 _currentAssemblyComponentId = 0;
        private static Int16 _currentComponentId = 0;
        private static Int16 _currentSubComponentId = 0;
        private static Int16 _currentPositionId = 0;
        private static Int16 _currentPositionSequence = 0;
        private static Int16 _currentElementSequence = 0;

        #endregion

        #region Properties

        public static Int16 CurrentFixtureItemId
        {
            get
            {
                return _currentFixtureItemId;
            }
        }

        public static Int16 CurrentFixtureId
        {
            get
            {
                return _currentFixtureId;
            }
        }

        public static Int16 CurrentFixtureAssemblyId
        {
            get
            {
                return _currentFixtureAssemblyId;
            }
        }

        public static Int32 CurrentAssemblyId
        {
            get
            {
                return _currentAssemblyId;
            }
        }

        public static Int16 CurrentAssemblyComponentId
        {
            get
            {
                return _currentAssemblyComponentId;
            }
        }

        public static Int16 CurrentComponentId
        {
            get
            {
                return _currentComponentId;
            }
        }

        public static Int16 CurrentSubComponentId
        {
            get
            {
                return _currentSubComponentId;
            }
        }

        public static Int16 CurrentElementSequence
        {
            get
            {
                return _currentElementSequence;
            }
        }

        #endregion

        #region Methods

        public static Int16 GetNextFixtureItemId()
        {
            _currentFixtureItemId--;
            _currentElementSequence = 0;
            return _currentFixtureItemId;
        }

        public static Int16 GetNextFixtureId()
        {
            _currentFixtureId--;
            return _currentFixtureId;
        }

        public static Int16 GetNextFixtureAssemblyId()
        {
            _currentFixtureAssemblyId--;
            return _currentFixtureAssemblyId;
        }
        
        public static Int32 GetNextAssemblyId()
        {
            _currentAssemblyId--;
            return _currentAssemblyId;
        }

        public static Int16 GetNextAssemblyComponentId()
        {
            _currentAssemblyComponentId--;
            return _currentAssemblyComponentId;
        }

        public static Int16 GetNextComponentId()
        {
            _currentComponentId--;
            _currentPositionSequence = 0;
            return _currentComponentId;
        }

        public static Int16 GetNextSubComponentId()
        {
            _currentSubComponentId--;
            return _currentSubComponentId;
        }

        public static Int16 GetNextPositionId()
        {
            _currentPositionId++;
            return _currentPositionId;
        }

        public static Int16 GetNextElementSequence()
        {
            _currentElementSequence--;
            return _currentElementSequence;
        }

        public static Int16 GetNextPositionSequence()
        {
            _currentPositionSequence--;
            return _currentPositionSequence;
        }
        
        public static void ResetIds()
        {
            _currentFixtureItemId = 0;
            _currentFixtureId = 0;
            _currentFixtureAssemblyId = 0;
            _currentAssemblyId = 0;
            _currentAssemblyComponentId = 0;
            _currentComponentId = 0;
            _currentSubComponentId = 0;
            _currentPositionId = 0;
            _currentPositionSequence = 0;
        }

        #endregion
    }
}
