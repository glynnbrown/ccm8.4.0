﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;

using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class FixturePlan : ModelObject<FixturePlan>
    {
        #region LockObjects
        private Object planBayLock = new Object();
        #endregion

        #region Properties

        #region BasePlanProperties
        /// <summary>
        /// The Fixture Plan Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The Fixture Plan Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The Plan ProductLevelId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductLevelIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductLevelId);
        public Int32 ProductLevelId
        {
            get { return GetProperty<Int32>(ProductLevelIdProperty); }
        }

        /// <summary>
        ///  The SourceFile
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceFileProperty =
            RegisterModelProperty<String>(c => c.SourceFile);
        public String SourceFile
        {
            get { return GetProperty<String>(SourceFileProperty); }
        }

        /// <summary>
        ///  The Blocking
        /// </summary>
        public static readonly ModelPropertyInfo<String> BlockingProperty =
            RegisterModelProperty<String>(c => c.Blocking);
        public String Blocking
        {
            get { return GetProperty<String>(BlockingProperty); }
        }

        /// <summary>
        ///  MerchShelfSapce
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchShelfSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchShelfSpace);
        public Double MerchShelfSpace
        {
            get { return GetProperty<Double>(MerchShelfSpaceProperty); }
        }
        /// <summary>
        ///  MerchPegSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchPegSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchPegSpace);
        public Double MerchPegSpace
        {
            get { return GetProperty<Double>(MerchPegSpaceProperty); }
        }
        /// <summary>
        ///  MerchBarSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchBarSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchBarSpace);
        public Double MerchBarSpace
        {
            get { return GetProperty<Double>(MerchBarSpaceProperty); }
        }
        /// <summary>
        ///  MerchChestSpace
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MerchChestSpaceProperty =
            RegisterModelProperty<Double>(c => c.MerchChestSpace);
        public Double MerchChestSpace
        {
            get { return GetProperty<Double>(MerchChestSpaceProperty); }
        }
        /// <summary>
        ///  Number1
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number1Property =
            RegisterModelProperty<Double>(c => c.Number1);
        public Double Number1
        {
            get { return GetProperty<Double>(Number1Property); }
        }
        /// <summary>
        ///  Number2
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number2Property =
            RegisterModelProperty<Double>(c => c.Number2);
        public Double Number2
        {
            get { return GetProperty<Double>(Number2Property); }
        }
        /// <summary>
        ///  Number3
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number3Property =
            RegisterModelProperty<Int32>(c => c.Number3);
        public Int32 Number3
        {
            get { return GetProperty<Int32>(Number3Property); }
        }
        /// <summary>
        ///  Number4
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number4Property =
            RegisterModelProperty<Int32>(c => c.Number4);
        public Int32 Number4
        {
            get { return GetProperty<Int32>(Number4Property); }
        }
        /// <summary>
        ///  Number5
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number5Property =
            RegisterModelProperty<Int32>(c => c.Number5);
        public Int32 Number5
        {
            get { return GetProperty<Int32>(Number5Property); }
        }

        /// <summary>
        ///  Text1
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
            RegisterModelProperty<String>(c => c.Text1);
        public String Text1
        {
            get { return GetProperty<String>(Text1Property); }
        }
        /// <summary>
        ///  Text2
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
            RegisterModelProperty<String>(c => c.Text2);
        public String Text2
        {
            get { return GetProperty<String>(Text2Property); }
        }
        /// <summary>
        ///  Text3
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
            RegisterModelProperty<String>(c => c.Text3);
        public String Text3
        {
            get { return GetProperty<String>(Text3Property); }
        }
        /// <summary>
        ///  Text4
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
            RegisterModelProperty<String>(c => c.Text4);
        public String Text4
        {
            get { return GetProperty<String>(Text4Property); }
        }
        /// <summary>
        ///  Text5
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
            RegisterModelProperty<String>(c => c.Text5);
        public String Text5
        {
            get { return GetProperty<String>(Text5Property); }
        }

        /// <summary>
        ///  ProductLvelFlexi
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductLvelFlexiProperty =
            RegisterModelProperty<String>(c => c.ProductLvelFlexi);
        public String ProductLvelFlexi
        {
            get { return GetProperty<String>(ProductLvelFlexiProperty); }
        }

        /// <summary>
        ///  BayWidthValues
        /// </summary>
        public static readonly ModelPropertyInfo<String> BayWidthValuesProperty =
            RegisterModelProperty<String>(c => c.BayWidthValues);
        public String BayWidthValues
        {
            get { return GetProperty<String>(BayWidthValuesProperty); }
        }

        /// <summary>
        ///  PresentationTotalWidth
        /// </summary>
        public static readonly ModelPropertyInfo<String> PresentationTotalWidthProperty =
            RegisterModelProperty<String>(c => c.PresentationTotalWidth);
        public String PresentationTotalWidth
        {
            get { return GetProperty<String>(PresentationTotalWidthProperty); }
        }

        /// <summary>
        ///  PresentationBayWidth
        /// </summary>
        public static readonly ModelPropertyInfo<String> PresentationBayWidthProperty =
            RegisterModelProperty<String>(c => c.PresentationBayWidth);
        public String PresentationBayWidth
        {
            get { return GetProperty<String>(PresentationBayWidthProperty); }
        }

        /// <summary>
        ///  MaxHeight
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MaxHeightProperty =
            RegisterModelProperty<Single>(c => c.MaxHeight);
        public Single MaxHeight
        {
            get { return GetProperty<Single>(MaxHeightProperty); }
        }

        /// <summary>
        ///  PresentationMaxHeight
        /// </summary>
        public static readonly ModelPropertyInfo<String> PresentationMaxHeightProperty =
            RegisterModelProperty<String>(c => c.PresentationMaxHeight);
        public String PresentationMaxHeight
        {
            get { return GetProperty<String>(PresentationMaxHeightProperty); }
        }

        /// <summary>
        ///  CustomCalculationValue
        /// </summary>
        public static readonly ModelPropertyInfo<Double> CustomCalculationValueProperty =
            RegisterModelProperty<Double>(c => c.CustomCalculationValue);
        public Double CustomCalculationValue
        {
            get { return GetProperty<Double>(CustomCalculationValueProperty); }
        }

        /// <summary>
        ///  CustomPostFix
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomPostFixProperty =
            RegisterModelProperty<String>(c => c.CustomPostFix);
        public String CustomPostFix
        {
            get { return GetProperty<String>(CustomPostFixProperty); }
        }

        /// <summary>
        ///  TotalWidth
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TotalWidthProperty =
            RegisterModelProperty<Single>(c => c.TotalWidth);
        public Single TotalWidth
        {
            get { return GetProperty<Single>(TotalWidthProperty); }
        }

        /// <summary>
        ///  BayCount
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> BayCountProperty =
            RegisterModelProperty<Int16>(c => c.BayCount);
        public Int16 BayCount
        {
            get { return GetProperty<Int16>(BayCountProperty); }
        }

        /// <summary>
        ///  Created
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> CreatedProperty =
            RegisterModelProperty<DateTime>(c => c.Created);
        public DateTime Created
        {
            get { return GetProperty<DateTime>(CreatedProperty); }
        }
        /// <summary>
        ///  Modified
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> ModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.Modified);
        public DateTime Modified
        {
            get { return GetProperty<DateTime>(ModifiedProperty); }
        }
        /// <summary>
        ///  PersonnelId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PersonnelIdProperty =
            RegisterModelProperty<Int32>(c => c.PersonnelId);
        public Int32 PersonnelId
        {
            get { return GetProperty<Int32>(PersonnelIdProperty); }
        }
        /// <summary>
        ///  PersonnelFlexi
        /// </summary>
        public static readonly ModelPropertyInfo<String> PersonnelFlexiProperty =
            RegisterModelProperty<String>(c => c.PersonnelFlexi);
        public String PersonnelFlexi
        {
            get { return GetProperty<String>(PersonnelFlexiProperty); }
        }
        /// <summary>
        ///  CompanyId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CompanyIdProperty =
            RegisterModelProperty<Int32>(c => c.CompanyId);
        public Int32 CompanyId
        {
            get { return GetProperty<Int32>(CompanyIdProperty); }
        }
        #endregion

        #region ListProperties

        /// <summary>
        ///  
        /// </summary>
        public static readonly ModelPropertyInfo<FixturePlanBayList> BaysProperty =
        RegisterModelProperty<FixturePlanBayList>(c => c.Bays);
        public FixturePlanBayList Bays
        {
            get
            {
                if (GetProperty<FixturePlanBayList>(BaysProperty) == null)
                {
                    if (planBayLock == null) planBayLock = new Object();

                    lock (planBayLock)
                    {
                        if (GetProperty<FixturePlanBayList>(BaysProperty) == null)
                        {
                            LoadProperty<FixturePlanBayList>(BaysProperty, FixturePlanBayList.GetPlanBaysByFixturePlanId(Id));
                            OnPropertyChanged(BaysProperty);
                        }
                    }

                }
                return GetProperty<FixturePlanBayList>(BaysProperty);
            }
        }

        #endregion

        #endregion

        //#region Criteria

        ///// <summary>
        ///// Criteria for FetchByCategoryReviewId
        ///// </summary>
        //[Serializable]
        //public class FetchByPlanIdAndTypeCriteria : Csla.CriteriaBase<FetchByPlanIdAndTypeCriteria>
        //{
        //    public static readonly PropertyInfo<Int32> PlanIdProperty =
        //        RegisterProperty<Int32>(c => c.PlanId);
        //    public Int32 PlanId
        //    {
        //        get { return ReadProperty<Int32>(PlanIdProperty); }
        //    }

        //    public static readonly PropertyInfo<PlanType> PlanTypeProperty =
        //            RegisterProperty<PlanType>(c => c.PlanType);
        //    public PlanType PlanType
        //    {
        //        get { return ReadProperty<PlanType>(PlanTypeProperty); }
        //    }

        //    public FetchByPlanIdAndTypeCriteria(Int32 PlanId, PlanType planType)
        //    {
        //        LoadProperty<Int32>(PlanIdProperty, PlanId);
        //        LoadProperty<PlanType>(PlanTypeProperty, planType);
        //    }
        //}

        //#endregion
    }
}
