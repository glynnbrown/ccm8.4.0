﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class FixturePlanList
    {
        #region Constructors
        private FixturePlanList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static FixturePlanList GetAllFixturePlans()
        {
            return DataPortal.Fetch<FixturePlanList>();
        }

        #endregion

        #region Data Access
        /// <summary>
        /// Called when retrieving a list of all fixture plans
        /// </summary>
        private void DataPortal_Fetch()
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFixturePlanDal dal = dalContext.GetDal<IFixturePlanDal>())
                {
                    IEnumerable<FixturePlanDto> dtoList = dal.FetchAll();
                    foreach (FixturePlanDto dto in dtoList)
                    {
                        this.Add(FixturePlan.GetPlan(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
