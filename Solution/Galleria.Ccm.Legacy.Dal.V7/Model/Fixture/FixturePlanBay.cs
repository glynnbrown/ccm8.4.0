﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;

using Galleria.Framework.Model;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class FixturePlanBay : ModelObject<FixturePlanBay>
    {
        #region Properties

        /// <summary>
        /// Id (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
        RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get
            {
                return GetProperty<Int32>(IdProperty);
            }
        }

        /// <summary>
        /// FixturePlanId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixturePlanIdProperty =
        RegisterModelProperty<Int32>(c => c.FixturePlanId);
        public Int32 FixturePlanId
        {
            get
            {
                return GetProperty<Int32>(FixturePlanIdProperty);
            }
        }

        /// <summary>
        /// BayNumber
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BayNumberProperty =
        RegisterModelProperty<Int32>(c => c.BayNumber);
        public Int32 BayNumber
        {
            get
            {
                return GetProperty<Int32>(BayNumberProperty);
            }
        }

        /// <summary>
        /// Name (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
        RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get
            {
                return GetProperty<String>(NameProperty);
            }
        }

        /// <summary>
        /// XPosition (Currently Space Plan Only)
        /// </summary>
        public static readonly ModelPropertyInfo<Double> XPositionProperty =
        RegisterModelProperty<Double>(c => c.XPosition);
        public Double XPosition
        {
            get
            {
                return GetProperty<Double>(XPositionProperty);
            }
        }

        /// <summary>
        /// BaseDepth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BaseDepthProperty =
        RegisterModelProperty<Double>(c => c.BaseDepth);
        public Double BaseDepth
        {
            get
            {
                return GetProperty<Double>(BaseDepthProperty);
            }
        }

        /// <summary>
        /// BaseHeight
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BaseHeightProperty =
        RegisterModelProperty<Double>(c => c.BaseHeight);
        public Double BaseHeight
        {
            get
            {
                return GetProperty<Double>(BaseHeightProperty);
            }
        }

        /// <summary>
        /// BaseWidth 
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BaseWidthProperty =
        RegisterModelProperty<Double>(c => c.BaseWidth);
        public Double BaseWidth
        {
            get
            {
                return GetProperty<Double>(BaseWidthProperty);
            }
        }

        /// <summary>
        /// Height 
        /// </summary>
        public static readonly ModelPropertyInfo<Double> HeightProperty =
        RegisterModelProperty<Double>(c => c.Height);
        public Double Height
        {
            get
            {
                return GetProperty<Double>(HeightProperty);
            }
        }

        /// <summary>
        /// Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> WidthProperty =
        RegisterModelProperty<Double>(c => c.Width);
        public Double Width
        {
            get
            {
                return GetProperty<Double>(WidthProperty);
            }
        }

        /// <summary>
        /// Depth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> DepthProperty =
        RegisterModelProperty<Double>(c => c.Depth);
        public Double Depth
        {
            get
            {
                return GetProperty<Double>(DepthProperty);
            }
        }

        /// <summary>
        /// Text1
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
        RegisterModelProperty<String>(c => c.Text1);
        public String Text1
        {
            get
            {
                return GetProperty<String>(Text1Property);
            }
        }

        /// <summary>
        /// Text2
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
        RegisterModelProperty<String>(c => c.Text2);
        public String Text2
        {
            get
            {
                return GetProperty<String>(Text2Property);
            }
        }

        /// <summary>
        /// Text3
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
        RegisterModelProperty<String>(c => c.Text3);
        public String Text3
        {
            get
            {
                return GetProperty<String>(Text3Property);
            }
        }

        /// <summary>
        /// Text4 
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
        RegisterModelProperty<String>(c => c.Text4);
        public String Text4
        {
            get
            {
                return GetProperty<String>(Text4Property);
            }
        }

        /// <summary>
        /// Text5
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
        RegisterModelProperty<String>(c => c.Text5);
        public String Text5
        {
            get
            {
                return GetProperty<String>(Text5Property);
            }
        }

        /// <summary>
        /// Number1
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number1Property =
        RegisterModelProperty<Double>(c => c.Number1);
        public Double Number1
        {
            get
            {
                return GetProperty<Double>(Number1Property);
            }
        }

        /// <summary>
        /// Number2
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Number2Property =
        RegisterModelProperty<Double>(c => c.Number2);
        public Double Number2
        {
            get
            {
                return GetProperty<Double>(Number2Property);
            }
        }

        /// <summary>
        /// Number3
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number3Property =
        RegisterModelProperty<Int32>(c => c.Number3);
        public Int32 Number3
        {
            get
            {
                return GetProperty<Int32>(Number3Property);
            }
        }

        /// <summary>
        /// Number4
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number4Property =
        RegisterModelProperty<Int32>(c => c.Number4);
        public Int32 Number4
        {
            get
            {
                return GetProperty<Int32>(Number4Property);
            }
        }

        /// <summary>
        /// Number5
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number5Property =
        RegisterModelProperty<Int32>(c => c.Number5);
        public Int32 Number5
        {
            get
            {
                return GetProperty<Int32>(Number5Property);
            }
        }

        /// <summary>
        /// Created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> CreatedProperty =
        RegisterModelProperty<DateTime>(c => c.Created);
        public DateTime Created
        {
            get
            {
                return GetProperty<DateTime>(CreatedProperty);
            }
        }

        /// <summary>
        /// Modified
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> ModifiedProperty =
        RegisterModelProperty<DateTime>(c => c.Modified);
        public DateTime Modified
        {
            get
            {
                return GetProperty<DateTime>(ModifiedProperty);
            }
        }

        /// <summary>
        /// PersonnelId 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PersonnelIdProperty =
        RegisterModelProperty<Int32>(c => c.PersonnelId);
        public Int32 PersonnelId
        {
            get
            {
                return GetProperty<Int32>(PersonnelIdProperty);
            }
        }

        /// <summary>
        /// PersonnelFlexi 
        /// </summary>
        public static readonly ModelPropertyInfo<String> PersonnelFlexiProperty =
        RegisterModelProperty<String>(c => c.PersonnelFlexi);
        public String PersonnelFlexi
        {
            get
            {
                return GetProperty<String>(PersonnelFlexiProperty);
            }
        }

        /// <summary>
        /// CompanyId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CompanyIdProperty =
        RegisterModelProperty<Int32>(c => c.CompanyId);
        public Int32 CompanyId
        {
            get
            {
                return GetProperty<Int32>(CompanyIdProperty);
            }
        }

        #endregion

    }
}
