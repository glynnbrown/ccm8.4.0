﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.V7;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class FixturePlanBayList
    {
        #region Constructors
        private FixturePlanBayList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static FixturePlanBayList GetPlanBaysByFixturePlanId(Int32 id)
        {
            return DataPortal.Fetch<FixturePlanBayList>(new SingleCriteria<Int32>(id));
        }
        #endregion

        #region Data Access


        /// <summary>
        /// Called when retrieving a list of all fixture plan bays
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFixturePlanBayDal dal = dalContext.GetDal<IFixturePlanBayDal>())
                {
                    IEnumerable<FixturePlanBayDto> dtoList= dal.FetchByFixturePlanId(criteria.Value);
                       
                    if (dtoList != null)
                    {
                        foreach (FixturePlanBayDto dto in dtoList)
                        {
                            this.Add(FixturePlanBay.GetPlanBay(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
