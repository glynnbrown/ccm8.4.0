﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Legacy.Model
{
    [Serializable]
    public partial class FixturePlanList : ModelList<FixturePlanList, FixturePlan>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //BusinessRules.AddRule(typeof(FixturePlanList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.CategoryFetch.ToString()));
        }
        #endregion
    }
}
