﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.V7.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;

namespace Galleria.Ccm.Legacy.Model
{
    public partial class FixturePlan
    {
        #region Constructor
        private FixturePlan() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing plan
        /// </summary>
        /// <returns>A list of plans</returns>
        public static FixturePlan GetFixturePlanById(Int32 fixturePlanId)
        {
            return DataPortal.Fetch<FixturePlan>(new SingleCriteria<Int32>(fixturePlanId));
        }

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static FixturePlan GetPlan(IDalContext dalContext, FixturePlanDto dto)
        {
            return DataPortal.FetchChild<FixturePlan>(dalContext, dto);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, FixturePlanDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Int32>(ProductLevelIdProperty, dto.ProductLevelId);
            this.LoadProperty<String>(SourceFileProperty, dto.SourceFile);
            this.LoadProperty<String>(BlockingProperty, dto.Blocking);
            if (dto.MerchShelfSpace.HasValue) this.LoadProperty<Double>(MerchShelfSpaceProperty, dto.MerchShelfSpace.Value);
            if (dto.MerchPegSpace.HasValue) this.LoadProperty<Double>(MerchPegSpaceProperty, dto.MerchPegSpace.Value);
            if (dto.MerchBarSpace.HasValue) this.LoadProperty<Double>(MerchBarSpaceProperty, dto.MerchBarSpace.Value);
            if (dto.MerchChestSpace.HasValue) this.LoadProperty<Double>(MerchChestSpaceProperty, dto.MerchChestSpace.Value);
            if (dto.Number1.HasValue) this.LoadProperty<Double>(Number1Property, dto.Number1.Value);
            if (dto.Number2.HasValue) this.LoadProperty<Double>(Number2Property, dto.Number2.Value);
            if (dto.Number3.HasValue) this.LoadProperty<Int32>(Number3Property, dto.Number3.Value);
            if (dto.Number4.HasValue) this.LoadProperty<Int32>(Number4Property, dto.Number4.Value);
            if (dto.Number5.HasValue) this.LoadProperty<Int32>(Number5Property, dto.Number5.Value);
            this.LoadProperty<String>(Text1Property, dto.Text1);
            this.LoadProperty<String>(Text2Property, dto.Text2);
            this.LoadProperty<String>(Text3Property, dto.Text3);
            this.LoadProperty<String>(Text4Property, dto.Text4);
            this.LoadProperty<String>(Text5Property, dto.Text5);
            this.LoadProperty<String>(ProductLvelFlexiProperty, dto.ProductLvelFlexi);
            this.LoadProperty<String>(BayWidthValuesProperty, dto.BayWidthValues);
            this.LoadProperty<String>(PresentationTotalWidthProperty, dto.PresentationTotalWidth);
            this.LoadProperty<String>(PresentationBayWidthProperty, dto.PresentationBayWidth);
            if (dto.MaxHeight.HasValue) this.LoadProperty<Single>(MaxHeightProperty, dto.MaxHeight.Value);
            this.LoadProperty<String>(PresentationMaxHeightProperty, dto.PresentationMaxHeight);
            if (dto.CustomCalculationValue.HasValue) this.LoadProperty<Double>(CustomCalculationValueProperty, dto.CustomCalculationValue.Value);
            this.LoadProperty<String>(CustomPostFixProperty, dto.CustomPostFix);
            if (dto.TotalWidth.HasValue) this.LoadProperty<Single>(TotalWidthProperty, dto.TotalWidth.Value);
            this.LoadProperty<Int16>(BayCountProperty, dto.BayCount);
            this.LoadProperty<DateTime>(CreatedProperty, dto.Created);
            this.LoadProperty<DateTime>(ModifiedProperty, dto.Modified);
            this.LoadProperty<Int32>(PersonnelIdProperty, dto.PersonnelId);
            this.LoadProperty<String>(PersonnelFlexiProperty, dto.PersonnelFlexi);
            this.LoadProperty<Int32>(CompanyIdProperty, dto.CompanyId);
          
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, FixturePlanDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFixturePlanDal dal = dalContext.GetDal<IFixturePlanDal>())
                {
                    FixturePlanDto dto = dal.FetchById(criteria.Value);

                    if (dto != null) LoadDataTransferObject(dalContext, dto);
                }
            }
        }
        #endregion
    }
}
