﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class FixturePlanDto
    {

        public Int32 Id { get; set; }
        public Int32 ProductLevelId { get; set; }
        public String Name { get; set; }
        public String SourceFile { get; set; }
        public String Blocking { get; set; }
        public Double? MerchShelfSpace { get; set; }
        public Double? MerchPegSpace { get; set; }
        public Double? MerchBarSpace { get; set; }
        public Double? MerchChestSpace { get; set; }
        public Double? Number1 { get; set; }
        public Double? Number2 { get; set; }
        public Int32? Number3 { get; set; }
        public Int32? Number4 { get; set; }
        public Int32? Number5 { get; set; }
        public String Text1 { get; set; }
        public String Text2 { get; set; }
        public String Text3 { get; set; }
        public String Text4 { get; set; }
        public String Text5 { get; set; }
        public String ProductLvelFlexi { get; set; }
        public String BayWidthValues { get; set; }
        public String PresentationTotalWidth { get; set; }
        public String PresentationBayWidth { get; set; }
        public Single? MaxHeight { get; set; }
        public String PresentationMaxHeight { get; set; }
        public Double? CustomCalculationValue { get; set; }
        public String CustomPostFix { get; set; }
        public Single? TotalWidth { get; set; }
        public Int16 BayCount { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public Int32 PersonnelId { get; set; }
        public String PersonnelFlexi { get; set; }
        public Int32 CompanyId { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            FixturePlanDto other = obj as FixturePlanDto;
            if (other != null)
            {
                //check each parameter
                if (other.Id != this.Id) return false;
                if (other.ProductLevelId != this.ProductLevelId) return false;
                if (other.Name != this.Name) return false;
                if (other.SourceFile != this.SourceFile) return false;
                if (other.Blocking != this.Blocking) return false;
                if (other.MerchShelfSpace != this.MerchShelfSpace) return false;
                if (other.MerchPegSpace != this.MerchPegSpace) return false;
                if (other.MerchBarSpace != this.MerchBarSpace) return false;
                if (other.MerchChestSpace != this.MerchChestSpace) return false;
                if (other.Number1 != this.Number1) return false;
                if (other.Number2 != this.Number2) return false;
                if (other.Number3 != this.Number3) return false;
                if (other.Number4 != this.Number4) return false;
                if (other.Number5 != this.Number5) return false;
                if (other.Text1 != this.Text1) return false;
                if (other.Text2 != this.Text2) return false;
                if (other.Text3 != this.Text3) return false;
                if (other.Text4 != this.Text4) return false;
                if (other.Text5 != this.Text5) return false;
                if (other.ProductLvelFlexi != this.ProductLvelFlexi) return false;
                if (other.BayWidthValues != this.BayWidthValues) return false;
                if (other.PresentationTotalWidth != this.PresentationTotalWidth) return false;
                if (other.PresentationBayWidth != this.PresentationBayWidth) return false;
                if (other.MaxHeight != this.MaxHeight) return false;
                if (other.PresentationMaxHeight != this.PresentationMaxHeight) return false;
                if (other.CustomCalculationValue != this.CustomCalculationValue) return false;
                if (other.CustomPostFix != this.CustomPostFix) return false;
                if (other.TotalWidth != this.TotalWidth) return false;
                if (other.BayCount != this.BayCount) return false;
                if (other.Created != this.Created) return false;
                if (other.Modified != this.Modified) return false;
                if (other.PersonnelId != this.PersonnelId) return false;
                if (other.PersonnelFlexi != this.PersonnelFlexi) return false;
                if (other.CompanyId != this.CompanyId) return false;
            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
