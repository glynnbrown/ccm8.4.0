﻿#region Header Information
// Copyright © Galleria RTS Ltd 2012

#region Version History: (CCM.Net 7.5)
// CCM-CCM-20418 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class FixturePlanBayDto
    {
        public Int32 Id { get; set; }
        public Int32 FixturePlanId { get; set; }
        public Int32 BayNumber { get; set; }
        public String Name { get; set; }
        public Double XPosition { get; set; }
        public Double BaseDepth { get; set; }
        public Double BaseHeight { get; set; }
        public Double BaseWidth { get; set; }
        public Double Height { get; set; }
        public Double Width { get; set; }
        public Double Depth { get; set; }
        public String Text1 { get; set; }
        public String Text2 { get; set; }
        public String Text3 { get; set; }
        public String Text4 { get; set; }
        public String Text5 { get; set; }
        public Double? Number1 { get; set; }
        public Double? Number2 { get; set; }
        public Int32? Number3 { get; set; }
        public Int32? Number4 { get; set; }
        public Int32? Number5 { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public Int32 PersonnelId { get; set; }
        public String PersonnelFlexi { get; set; }
        public Int32 CompanyId { get; set; }
        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            FixturePlanBayDto other = obj as FixturePlanBayDto;
            if (other != null )
            {
                //check each parameter
                if (other.Id != this.Id) return false;
                if (other.FixturePlanId != this.FixturePlanId) return false;
                if (other.BayNumber != this.BayNumber) return false;
                if (other.Name != this.Name) return false;
                if (other.XPosition != this.XPosition) return false;
                if (other.BaseDepth != this.BaseDepth) return false;
                if (other.BaseHeight != this.BaseHeight) return false;
                if (other.BaseWidth != this.BaseWidth) return false;
                if (other.Depth != this.Depth) return false;
                if (other.Height != this.Height) return false;
                if (other.Width != this.Width) return false;
                if (other.Number1 != this.Number1) return false;
                if (other.Number2 != this.Number2) return false;
                if (other.Number3 != this.Number3) return false;
                if (other.Number4 != this.Number4) return false;
                if (other.Number5 != this.Number5) return false;
                if (other.Text1 != this.Text1) return false;
                if (other.Text2 != this.Text2) return false;
                if (other.Text3 != this.Text3) return false;
                if (other.Text4 != this.Text4) return false;
                if (other.Text5 != this.Text5) return false;
                if (other.Created != this.Created) return false;
                if (other.Modified != this.Modified) return false;
                if (other.PersonnelId != this.PersonnelId) return false;
                if (other.PersonnelFlexi != this.PersonnelFlexi) return false;
                if (other.CompanyId != this.CompanyId) return false;

            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
