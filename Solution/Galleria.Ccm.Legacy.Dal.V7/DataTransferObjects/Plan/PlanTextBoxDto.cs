﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class PlanTextBoxDto
    {

        public Int32 Id { get; set; }
        public Int32 PlanId { get; set; }
        public Int32 PlanBayId { get; set; }
        public Int32 PlanElementId { get; set; }
        public Int32 FixturePlanBayNumber { get; set; }
        public Double XPosition { get; set; }
        public Double YPosition { get; set; }
        public Double ZPosition { get; set; }
        public Double Height { get; set; }
        public Double Width { get; set; }
        public Double Depth { get; set; }
        public Byte Type { get; set; }
        public String Description { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            PlanTextBoxDto other = obj as PlanTextBoxDto;
            if (other != null)
            {
                if (this.Id != other.Id) return false;
                if (this.PlanId != other.PlanId) return false;
                if (this.PlanBayId != other.PlanBayId) return false;
                if (this.PlanElementId != other.PlanElementId) return false;
                if (this.FixturePlanBayNumber != other.FixturePlanBayNumber) return false;
                if (this.XPosition != other.XPosition) return false;
                if (this.YPosition != other.YPosition) return false;
                if (this.ZPosition != other.ZPosition) return false;
                if (this.Height != other.Height) return false;
                if (this.Width != other.Width) return false;
                if (this.Depth != other.Depth) return false;
                if (this.Type != other.Type) return false;
                if (this.Description != other.Description) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
