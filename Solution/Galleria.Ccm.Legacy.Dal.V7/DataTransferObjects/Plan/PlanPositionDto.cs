﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class PlanPositionDto
    {
        public Int32 Id { get; set; }
        public Int32 PlanId { get; set; }
        public Int32 PlanElementId { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 OrientationId { get; set; }
        public Single XPosition { get; set; }
        public Single YPosition { get; set; }
        public Int16 Deep { get; set; }
        public Int16 High { get; set; }
        public Int16 Wide { get; set; }
        public Double ProductHeight { get; set; }
        public Double ProductWidth { get; set; }
        public Double ProductDepth { get; set; }
        public Int16 Units { get; set; }
        public Single SqueezeHeight { get; set; }
        public Single SqueezeWidth { get; set; }
        public Single SqueezeDepth { get; set; }
        public Single BlockHeight { get; set; }
        public Single BlockWidth { get; set; }
        public Single BlockDepth { get; set; }
        public Single? NestHeight { get; set; }
        public Single? NestWidth { get; set; }
        public Single? NestDepth { get; set; }
        public Single LeadingDivider { get; set; }
        public Single LeadingGap { get; set; }
        public Single LeadingGapVert { get; set; }
        public Int16 CapTopDeep { get; set; }
        public Int16 CapTopHigh { get; set; }
        public Int16 CapBotDeep { get; set; }
        public Int16 CapBotHigh { get; set; }
        public Int16 CapLftDeep { get; set; }
        public Int16 CapLftWide { get; set; }
        public Int16 CapRgtDeep { get; set; }
        public Int16 CapRgtWide { get; set; }
        public Int32? TargetSC { get; set; }
        public Int16 MaxCap { get; set; }
        public Int16 MaxStack { get; set; }
        public Int32 MinDeep { get; set; }
        public Int32 MaxDeep { get; set; }
        public Int16 TrayHigh { get; set; }
        public Int16 TrayWide { get; set; }
        public Int16 TrayDeep { get; set; }
        public Single TrayHeight { get; set; }
        public Single TrayWidth { get; set; }
        public Single TrayDepth { get; set; }
        public Int16 TrayUnits { get; set; }
        public Single TrayThickHeight { get; set; }
        public Single TrayThickWidth { get; set; }
        public Single TrayThickDepth { get; set; }
        public Int16 TrayCountHigh { get; set; }
        public Int16 TrayCountWide { get; set; }
        public Int16 TrayCountDeep { get; set; }
        public Byte TrayProduct { get; set; }
        public Int16 NestHigh { get; set; }
        public Int16 NestWide { get; set; }
        public Int16 NestDeep { get; set; }
        public Int16 MerchType { get; set; }
        public Int32 Blocking { get; set; }
        public Int32 BlockId { get; set; }
        public Int32 BrushStyle { get; set; }
        public Int32 MinDrop { get; set; }
        public Int32? RemoveCount { get; set; }
        public Int16 CapRightWide { get; set; }
        public Int16 CapRightDeep { get; set; }
        public Int16 CapRightWideTrayCount { get; set; }
        public Int16 CapRightDeepTrayCount { get; set; }
        public Int16 CapRightCapTopHigh { get; set; }
        public Int16 CapRightCapTopDeep { get; set; }
        public Byte MultiSited { get; set; }
        public Int32 CasePack { get; set; }
        public Int32 Colour { get; set; }
        public Byte LastChance { get; set; }
        public Byte AddLocked { get; set; }
        public Byte RemoveLocked { get; set; }
        public Int32? StripNumber { get; set; }
        public Byte BreakTrayUp { get; set; }
        public Byte BreakTrayDown { get; set; }
        public Byte BreakTrayBack { get; set; }
        public Byte BreakTrayTop { get; set; }
        public Byte FrontOnly { get; set; }
        public Byte MiddleCapping { get; set; }
        public Int32? MaxNestHigh { get; set; }
        public Int32? MaxNestDeep { get; set; }
        public Single PegDepth { get; set; }
        public Single PegX { get; set; }
        public Single PegY { get; set; }
        public Int32? MaxRightCap { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public Int32 PersonnelId { get; set; }
        public String PersonnelFlexi { get; set; }
        public Int32 CompanyId { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            PlanPositionDto other = obj as PlanPositionDto;
            if (other != null)
            {
                //check each parameter
                if (other.Id != this.Id) return false;
                if (other.PlanId != this.PlanId) return false;
                if (other.PlanElementId != this.PlanElementId) return false;
                if (other.ProductId != this.ProductId) return false;
                if (other.OrientationId != this.OrientationId) return false;
                if (other.XPosition != this.XPosition) return false;
                if (other.YPosition != this.YPosition) return false;
                if (other.Deep != this.Deep) return false;
                if (other.High != this.High) return false;
                if (other.Wide != this.Wide) return false;
                if (other.ProductHeight != this.ProductHeight) return false;
                if (other.ProductWidth != this.ProductWidth) return false;
                if (other.ProductDepth != this.ProductDepth) return false;
                if (other.Units != this.Units) return false;
                if (other.SqueezeHeight != this.SqueezeHeight) return false;
                if (other.SqueezeWidth != this.SqueezeWidth) return false;
                if (other.SqueezeDepth != this.SqueezeDepth) return false;
                if (other.BlockHeight != this.BlockHeight) return false;
                if (other.BlockWidth != this.BlockWidth) return false;
                if (other.BlockDepth != this.BlockDepth) return false;
                if (other.NestHeight != this.NestHeight) return false;
                if (other.NestWidth != this.NestWidth) return false;
                if (other.NestDepth != this.NestDepth) return false;
                if (other.LeadingDivider != this.LeadingDivider) return false;
                if (other.LeadingGap != this.LeadingGap) return false;
                if (other.LeadingGapVert != this.LeadingGapVert) return false;
                if (other.CapTopDeep != this.CapTopDeep) return false;
                if (other.CapTopHigh != this.CapTopHigh) return false;
                if (other.CapBotDeep != this.CapBotDeep) return false;
                if (other.CapBotHigh != this.CapBotHigh) return false;
                if (other.CapLftDeep != this.CapLftDeep) return false;
                if (other.CapLftWide != this.CapLftWide) return false;
                if (other.CapRgtDeep != this.CapRgtDeep) return false;
                if (other.CapRgtWide != this.CapRgtWide) return false;
                if (other.TargetSC != this.TargetSC) return false;
                if (other.MaxCap != MaxCap) return false;
                if (other.MaxStack != this.MaxStack) return false;
                if (other.MinDeep != this.MinDeep) return false;
                if (other.MaxDeep != this.MaxDeep) return false;
                if (other.TrayHigh != this.TrayHigh) return false;
                if (other.TrayWide != this.TrayWide) return false;
                if (other.TrayDeep != TrayDeep) return false;
                if (other.TrayHeight != this.TrayHeight) return false;
                if (other.TrayWidth != this.TrayWidth) return false;
                if (other.TrayDepth != this.TrayDepth) return false;
                if (other.TrayUnits != this.TrayUnits) return false;
                if (other.TrayThickHeight != this.TrayThickHeight) return false;
                if (other.TrayThickWidth != this.TrayThickWidth) return false;
                if (other.TrayThickDepth != this.TrayThickDepth) return false;
                if (other.TrayCountHigh != this.TrayCountHigh) return false;
                if (other.TrayCountWide != TrayCountWide) return false;
                if (other.TrayCountDeep != this.TrayCountDeep) return false;
                if (other.TrayProduct != this.TrayProduct) return false;
                if (other.NestHigh != this.NestHigh) return false;
                if (other.NestWide != this.NestWide) return false;
                if (other.NestDeep != this.NestDeep) return false;
                if (other.MerchType != MerchType) return false;
                if (other.Blocking != this.Blocking) return false;
                if (other.BlockId != this.BlockId) return false;
                if (other.BrushStyle != this.BrushStyle) return false;
                if (other.MinDrop != this.MinDrop) return false;
                if (other.RemoveCount != RemoveCount) return false;
                if (other.CapRightWide != this.CapRightWide) return false;
                if (other.CapRightDeep != this.CapRightDeep) return false;
                if (other.CapRightWideTrayCount != this.CapRightWideTrayCount) return false;
                if (other.CapRightDeepTrayCount != this.CapRightDeepTrayCount) return false;
                if (other.CapRightCapTopHigh != this.CapRightCapTopHigh) return false;
                if (other.CapRightCapTopDeep != this.CapRightCapTopDeep) return false;
                if (other.MultiSited != this.MultiSited) return false;
                if (other.Colour != Colour) return false;
                if (other.LastChance != this.LastChance) return false;
                if (other.AddLocked != this.AddLocked) return false;
                if (other.RemoveLocked != this.RemoveLocked) return false;
                if (other.StripNumber != this.StripNumber) return false;
                if (other.BreakTrayUp != this.BreakTrayUp) return false;
                if (other.BreakTrayDown != BreakTrayDown) return false;
                if (other.BreakTrayBack != this.BreakTrayBack) return false;
                if (other.FrontOnly != this.FrontOnly) return false;
                if (other.MiddleCapping != this.MiddleCapping) return false;
                if (other.MaxNestHigh != this.MaxNestHigh) return false;
                if (other.MaxNestDeep != this.MaxNestDeep) return false;
                if (other.PegDepth != PegDepth) return false;
                if (other.PegX != this.PegX) return false;
                if (other.PegY != this.PegY) return false;
                if (other.MaxRightCap != this.MaxRightCap) return false;
                if (other.Created != this.Created) return false;
                if (other.Modified != this.Modified) return false;
                if (other.PersonnelId != this.PersonnelId) return false;
                if (other.PersonnelFlexi != this.PersonnelFlexi) return false;
                if (other.CompanyId != this.CompanyId) return false;
                if (other.BreakTrayTop != this.BreakTrayTop) return false;
            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
