﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class SpacePlanPositionDto : PlanPositionDto
    {
        public Int32 StoreId { get; set; }
        public Byte PegFix { get; set; }
        public Int32? PegShift { get; set; }
        public Int32 ElementPositionId { get; set; }
        public Single HistoricalDailyUnits { get; set; }
        public Single ActualDaysSupplied { get; set; }
        public String BlockTagName { get; set; }
        public Int32? GlobalRank { get; set; }
        public Int32? FinalMin { get; set; }
        public Int32? FinalMax { get; set; }
        public Int32? MinPriority1 { get; set; }
        public Int32? MinPriority2 { get; set; }
        public Int32? MinPriority3 { get; set; }
        public Int32? MinPriority4 { get; set; }
        public Int32? MinPriority5 { get; set; }
        public Int32? MaxPriority1 { get; set; }
        public Int32? MaxPriority2 { get; set; }
        public Int32? MaxPriority3 { get; set; }
        public Int32? MaxPriority4 { get; set; }
        public Int32? MaxPriority5 { get; set; }
        public Int32? RecommendedInventory { get; set; }
        public Int32? CompromiseInventory { get; set; }
        public Single? AverageWeeklySales { get; set; }
        public Single? AverageWeeklyUnits { get; set; }
        public Single? AverageWeeklyProfit { get; set; }
        public Single? AchievedCases { get; set; }
        public Single? AchievedDOS { get; set; }
        public Single? PerformanceValue01 { get; set; }
        public Single? PerformanceValue02 { get; set; }
        public Single? PerformanceValue03 { get; set; }
        public Single? PerformanceValue04 { get; set; }
        public Single? PerformanceValue05 { get; set; }
        public Single? PerformanceValue06 { get; set; }
        public Single? PerformanceValue07 { get; set; }
        public Single? PerformanceValue08 { get; set; }
        public Single? PerformanceValue09 { get; set; }
        public Single? PerformanceValue10 { get; set; }
        public Single? ListingPSI { get; set; }
        public Single? ExposurePSI { get; set; }
        public Double? PegProng { get; set; }
        public Boolean CanBeRefaced { get; set; }
        public Byte AchievedInventoryLevelType { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            SpacePlanPositionDto other = obj as SpacePlanPositionDto;
            if (other != null && base.Equals(obj))
            {
                //check each parameter
                if (other.StoreId != this.StoreId) return false;
                if (other.PegFix != this.PegFix) return false;
                if (other.PegShift != this.PegShift) return false;
                if (other.ElementPositionId != this.ElementPositionId) return false;
                if (other.HistoricalDailyUnits != this.HistoricalDailyUnits) return false;
                if (other.ActualDaysSupplied != this.ActualDaysSupplied) return false;
                if (other.BlockTagName != this.BlockTagName) return false;
                if (other.GlobalRank != this.GlobalRank) return false;
                if (other.FinalMin != this.FinalMin) return false;
                if (other.FinalMax != this.FinalMax) return false;
                if (other.MinPriority1 != this.MinPriority1) return false;
                if (other.MinPriority2 != this.MinPriority2) return false;
                if (other.MinPriority3 != this.MinPriority3) return false;
                if (other.MinPriority4 != this.MinPriority4) return false;
                if (other.MinPriority5 != this.MinPriority5) return false;
                if (other.MaxPriority1 != this.MaxPriority1) return false;
                if (other.MaxPriority2 != this.MaxPriority2) return false;
                if (other.MaxPriority3 != this.MaxPriority3) return false;
                if (other.MaxPriority4 != this.MaxPriority4) return false;
                if (other.MaxPriority5 != this.MaxPriority5) return false;
                if (other.RecommendedInventory != this.RecommendedInventory) return false;
                if (other.CompromiseInventory != this.CompromiseInventory) return false;
                if (other.AverageWeeklySales != this.AverageWeeklySales) return false;
                if (other.AverageWeeklyUnits != this.AverageWeeklyUnits) return false;
                if (other.AverageWeeklyProfit != this.AverageWeeklyProfit) return false;
                if (other.AchievedCases != this.AchievedCases) return false;
                if (other.AchievedDOS != this.AchievedDOS) return false;
                if (other.PerformanceValue01 != this.PerformanceValue01) return false;
                if (other.PerformanceValue02 != this.PerformanceValue02) return false;
                if (other.PerformanceValue03 != this.PerformanceValue03) return false;
                if (other.PerformanceValue04 != this.PerformanceValue04) return false;
                if (other.PerformanceValue05 != this.PerformanceValue05) return false;
                if (other.PerformanceValue06 != this.PerformanceValue06) return false;
                if (other.PerformanceValue07 != this.PerformanceValue07) return false;
                if (other.PerformanceValue08 != this.PerformanceValue08) return false;
                if (other.PerformanceValue09 != this.PerformanceValue09) return false;
                if (other.PerformanceValue10 != this.PerformanceValue10) return false;
                if (other.ListingPSI != this.ListingPSI) return false;
                if (other.ExposurePSI != this.ExposurePSI) return false;
                if (other.PegProng != this.PegProng) return false;
                if (other.CanBeRefaced != this.CanBeRefaced) return false;
                if (other.AchievedInventoryLevelType != this.AchievedInventoryLevelType) return false;
            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
