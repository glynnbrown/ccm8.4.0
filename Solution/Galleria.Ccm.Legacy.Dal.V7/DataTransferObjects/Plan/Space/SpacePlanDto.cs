﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class SpacePlanDto : PlanDto
    {
        public Int32 ClusterLevelId { get; set; }
        public Int32 TemplatePlanId { get; set; }
        public Int32 ClusterAssortmentId { get; set; }
        public Int32 FixturePlanId { get; set; }
        public Int32 ScenarioId { get; set; }
        public DateTime ReviewDate { get; set; }
        public Byte Islive { get; set; }
        public Byte HasEngineReport { get; set; }
        public Byte? RenderType { get; set; }
        public Byte OverrideBlockSpace { get; set; }
        public String WorkpackageName { get; set; }
        public String AssortmentTriangleSetting { get; set; }
        public Byte FlexPreservationAllowed { get; set; }
        public Int32 PercentCompromise { get; set; }
        public Int32 PercentDelistedProducts { get; set; }
        public Int32 PercentForceClusterFacingLevels { get; set; }
        public Int32 PercentClusterParticipationListing { get; set; }
        public Int32 ProductsPlacedCount { get; set; }
        public Int32 ProductsUnplacedCount { get; set; }
        public Int32 ProductsNotRecommendedCount { get; set; }
        public Single AuditValue1 { get; set; }
        public Single AuditValue2 { get; set; }
        public Single AuditValue3 { get; set; }
        public Single AuditValue4 { get; set; }
        public Single AuditValue5 { get; set; }
        public Single AuditValue6 { get; set; }
        public Single AuditValue7 { get; set; }
        public Single AuditValue8 { get; set; }
        public Single AuditValue9 { get; set; }
        public Single AuditValue10 { get; set; }
        public Single AuditValue11 { get; set; }
        public Single AuditValue12 { get; set; }
        public Single AuditValue13 { get; set; }
        public Single AuditValue14 { get; set; }
        public Single AuditValue15 { get; set; }
        public Int16 ActionCount { get; set; }
        public Int16 ActionsAppliedCount { get; set; }
        public Int16 ProductsAddedCount { get; set; }
        public Int16 ProductsRemovedCount { get; set; }
        public Int16 ProductsMovedCount { get; set; }
        public Int16 ProductsReplacedCount { get; set; }
        public Int16 ProductsIncreasedSpaceCount { get; set; }
        public Int16 ProductsDecreasedSpaceCount { get; set; }
        public Int16 AutomaticIncreasedSpaceCount { get; set; }
        public Int16 AutomaticDecreasedSpaceCount { get; set; }
        public Int16 SupplyabilityRemovedCount { get; set; }
        public Single PercentageOfActionsNotAchieved { get; set; }
        public Single PercentageOfChangeOnPlan { get; set; }
        public Single PercentageOfChangeFromOriginal { get; set; }
        public Int32 ProfileId { get; set; }
        public String ProfileName { get; set; }
        public Int16 PlaceholderProductCount { get; set; }
        public Int16 ReplacementProductCount { get; set; }
        public Int16 PlaceholderProductPlacedCount { get; set; }
        public Int16 ReplacementProductPlacedCount { get; set; }
        public Int16 ProductCountChangedOnPlan { get; set; }
        public Single PercentageDeviationFromBlocking { get; set; }
        public Single PercentageWhiteSpace { get; set; }
        public Int16 BlocksDroppedCount { get; set; }
        public Int16 ProductsNotAchievedInventoryCount { get; set; }
        public Int16 ProductsMovedFromTemplateCount { get; set; }
        public Int16 AttributesChangedCount { get; set; }
        public Int16 AttributesRemovedCount { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            SpacePlanDto other = obj as SpacePlanDto;
            if (other != null && base.Equals(obj))
            {
                //check each parameter
                if (other.ClusterLevelId != this.ClusterLevelId) return false;
                if (other.TemplatePlanId != this.TemplatePlanId) return false;
                if (other.ClusterAssortmentId != this.ClusterAssortmentId) return false;
                if (other.FixturePlanId != this.FixturePlanId) return false;
                if (other.ScenarioId != this.ScenarioId) return false;
                if (other.ReviewDate != this.ReviewDate) return false;
                if (other.Islive != this.Islive) return false;
                if (other.HasEngineReport != this.HasEngineReport) return false;
                if (other.RenderType != this.RenderType) return false;
                if (other.OverrideBlockSpace != this.OverrideBlockSpace) return false;
                if (other.WorkpackageName != this.WorkpackageName) return false;
                if (other.AssortmentTriangleSetting != this.AssortmentTriangleSetting) return false;
                if (other.FlexPreservationAllowed != this.FlexPreservationAllowed) return false;
                if (other.PercentCompromise != this.PercentCompromise) return false;
                if (other.PercentDelistedProducts != this.PercentDelistedProducts) return false;
                if (other.PercentForceClusterFacingLevels != this.PercentForceClusterFacingLevels) return false;
                if (other.PercentClusterParticipationListing != this.PercentClusterParticipationListing) return false;
                if (other.ProductsPlacedCount != this.ProductsPlacedCount) return false;
                if (other.ProductsUnplacedCount != this.ProductsUnplacedCount) return false;
                if (other.ProductsNotRecommendedCount != this.ProductsNotRecommendedCount) return false;
                if (other.AuditValue1 != this.AuditValue1) return false;
                if (other.AuditValue2 != this.AuditValue2) return false;
                if (other.AuditValue3 != this.AuditValue3) return false;
                if (other.AuditValue4 != this.AuditValue4) return false;
                if (other.AuditValue5 != this.AuditValue5) return false;
                if (other.AuditValue6 != this.AuditValue6) return false;
                if (other.AuditValue7 != this.AuditValue7) return false;
                if (other.AuditValue8 != this.AuditValue8) return false;
                if (other.AuditValue9 != this.AuditValue9) return false;
                if (other.AuditValue10 != this.AuditValue10) return false;
                if (other.AuditValue11 != this.AuditValue11) return false;
                if (other.AuditValue12 != this.AuditValue12) return false;
                if (other.AuditValue13 != this.AuditValue13) return false;
                if (other.AuditValue14 != this.AuditValue14) return false;
                if (other.AuditValue15 != this.AuditValue15) return false;
                if (other.ActionCount != this.ActionCount) return false;
                if (other.ActionsAppliedCount != this.ActionsAppliedCount) return false;
                if (other.ProductsAddedCount != this.ProductsAddedCount) return false;
                if (other.ProductsRemovedCount != this.ProductsRemovedCount) return false;
                if (other.ProductsMovedCount != this.ProductsMovedCount) return false;
                if (other.ProductsReplacedCount != this.ProductsReplacedCount) return false;
                if (other.ProductsIncreasedSpaceCount != this.ProductsIncreasedSpaceCount) return false;
                if (other.ProductsDecreasedSpaceCount != this.ProductsDecreasedSpaceCount) return false;
                if (other.AutomaticIncreasedSpaceCount != this.AutomaticIncreasedSpaceCount) return false;
                if (other.AutomaticDecreasedSpaceCount != this.AutomaticDecreasedSpaceCount) return false;
                if (other.SupplyabilityRemovedCount != this.SupplyabilityRemovedCount) return false;
            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
