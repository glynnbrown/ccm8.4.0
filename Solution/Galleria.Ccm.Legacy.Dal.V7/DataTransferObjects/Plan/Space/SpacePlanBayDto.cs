﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class SpacePlanBayDto : PlanBayDto
    {
        public Int32 StoreId { get; set; }
        public Int32 FixtureId { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            SpacePlanBayDto other = obj as SpacePlanBayDto;
            if (other != null && base.Equals(obj))
            {
                //check each parameter
                if (other.StoreId != this.StoreId) return false;
                if (other.FixtureId != this.FixtureId) return false;
            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
