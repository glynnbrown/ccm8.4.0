﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class SpacePlanElementDto : PlanElementDto
    {
        public Int32 StoreId { get; set; }
        public Int32 EmptyPegs { get; set; }
        public Int32 RightEmptyPeg { get; set; }
        public Int32 ProductCount { get; set; }
        public Int32 PrimaryPegs { get; set; }
        public Double StartX { get; set; }
        public Double MaxHeight { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            SpacePlanElementDto other = obj as SpacePlanElementDto;
            if (other != null && base.Equals(obj))
            {
                //check each parameter
                if (other.StoreId != this.StoreId) return false;
                if (other.EmptyPegs != this.EmptyPegs) return false;
                if (other.RightEmptyPeg != this.RightEmptyPeg) return false;
                if (other.ProductCount != this.ProductCount) return false;
                if (other.PrimaryPegs != this.PrimaryPegs) return false;
                if (other.StartX != this.StartX) return false;
                if (other.MaxHeight != this.MaxHeight) return false;

            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
