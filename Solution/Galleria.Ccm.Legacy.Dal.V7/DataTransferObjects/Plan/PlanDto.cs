﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class PlanDto
    {
        public Int32 Id { get; set; }
        public Guid UniqueContentReference { get; set; }
        public Byte PlanType { get; set; }
        public String Name { get; set; }
        public String UniqueItemName { get; set; }
        public Int32 StoreId { get; set; }
        public String StoreCode { get; set; }
        public String StoreFlexi { get; set; }
        public Int32 ProductLevelId { get; set; }
        public String MerchandisingGroupFlexi { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? IssuedDate { get; set; }
        public DateTime? ValidDate { get; set; }
        public DateTime? RangingDate { get; set; }
        public DateTime? PublishingDate { get; set; }
        public Byte Deleted { get; set; }
        public Byte Issued { get; set; }
        public Byte Failed { get; set; }
        public Double MerchShelfSpace { get; set; }
        public Double MerchPegSpace { get; set; }
        public Double MerchBarSpace { get; set; }
        public Double MerchChestSpace { get; set; }
        public Double Number1 { get; set; }
        public Double Number2 { get; set; }
        public Int32 Number3 { get; set; }
        public Int32 Number4 { get; set; }
        public Int32 Number5 { get; set; }
        public String Text1 { get; set; }
        public String Text2 { get; set; }
        public String Text3 { get; set; }
        public String Text4 { get; set; }
        public String Text5 { get; set; }
        public String Text6 { get; set; }
        public String Text7 { get; set; }
        public String Text8 { get; set; }
        public String Text9 { get; set; }
        public String Text10 { get; set; }
        public Byte? ElementPresentation { get; set; }
        public Byte? NestedMerchHeight { get; set; }
        public Double? FingerSpace { get; set; }
        public Byte? BreakMerchHeight { get; set; }
        public Byte? BreakMerchDepth { get; set; }
        public Int32 CategoryReviewId { get; set; }
        public Int32? ParentCategoryReviewId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public Int32 PersonnelId { get; set; }
        public String PersonnelFlexi { get; set; }
        public Int32 CompanyId { get; set; }
        public String CategoryReviewName { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            PlanDto other = obj as PlanDto;
            if (other != null)
            {
                //check each parameter
                if (other.Id != this.Id) return false;
                if (other.UniqueContentReference != this.UniqueContentReference) return false;
                if (other.PlanType != this.PlanType) return false;
                if (other.Name != this.Name) return false;
                if (other.UniqueItemName != this.UniqueItemName) return false;
                if (other.StoreId != this.StoreId) return false;
                if (other.StoreCode != this.StoreCode) return false;
                if (other.StoreFlexi != this.StoreFlexi) return false;
                if (other.ProductLevelId != this.ProductLevelId) return false;
                if (other.MerchandisingGroupFlexi != this.MerchandisingGroupFlexi) return false;
                if (other.CreatedDate != this.CreatedDate) return false;
                if (other.IssuedDate != this.IssuedDate) return false;
                if (other.ValidDate != this.ValidDate) return false;
                if (other.RangingDate != this.RangingDate) return false;
                if (other.PublishingDate != this.PublishingDate) return false;
                if (other.Deleted != this.Deleted) return false;
                if (other.Issued != this.Issued) return false;
                if (other.Failed != this.Failed) return false;
                if (other.MerchShelfSpace != this.MerchShelfSpace) return false;
                if (other.MerchPegSpace != this.MerchPegSpace) return false;
                if (other.MerchBarSpace != this.MerchBarSpace) return false;
                if (other.MerchChestSpace != this.MerchChestSpace) return false;
                if (other.Number1 != this.Number1) return false;
                if (other.Number2 != this.Number2) return false;
                if (other.Number3 != this.Number3) return false;
                if (other.Number4 != this.Number4) return false;
                if (other.Number5 != this.Number5) return false;
                if (other.Text1 != this.Text1) return false;
                if (other.Text2 != this.Text2) return false;
                if (other.Text3 != this.Text3) return false;
                if (other.Text4 != this.Text4) return false;
                if (other.Text5 != this.Text5) return false;
                if (other.Text6 != this.Text6) return false;
                if (other.Text7 != this.Text7) return false;
                if (other.Text8 != this.Text8) return false;
                if (other.Text9 != this.Text9) return false;
                if (other.Text10 != this.Text10) return false;
                if (other.ElementPresentation != this.ElementPresentation) return false;
                if (other.NestedMerchHeight != this.NestedMerchHeight) return false;
                if (other.FingerSpace != this.FingerSpace) return false;
                if (other.BreakMerchHeight != this.BreakMerchHeight) return false;
                if (other.BreakMerchDepth != this.BreakMerchDepth) return false;
                if (other.CategoryReviewId != this.CategoryReviewId) return false;
                if (other.ParentCategoryReviewId != this.ParentCategoryReviewId) return false;
                if (other.Created != this.Created) return false;
                if (other.Modified != this.Modified) return false;
                if (other.PersonnelId != this.PersonnelId) return false;
                if (other.PersonnelFlexi != this.PersonnelFlexi) return false;
                if (other.CompanyId != this.CompanyId) return false;
                if (other.CategoryReviewName != this.CategoryReviewName) return false;
            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
