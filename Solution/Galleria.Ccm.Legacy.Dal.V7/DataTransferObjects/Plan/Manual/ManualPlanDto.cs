﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class ManualPlanDto : PlanDto
    {
        public Int32? MerchandisingGroupId { get; set; }
        public String SourceFile { get; set; }
        public String PlanDescription1 { get; set; }
        public String PlanDescription2 { get; set; }
        public String PlanDescription3 { get; set; }
        public String PlanDescription4 { get; set; }
        public String PlanDescription5 { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            ManualPlanDto other = obj as ManualPlanDto;
            if (other != null && base.Equals(obj))
            {
                //check each parameter
                if (other.MerchandisingGroupId != this.MerchandisingGroupId) return false;
                if (other.SourceFile != this.SourceFile) return false;
                if (other.PlanDescription1 != this.PlanDescription1) return false;
                if (other.PlanDescription2 != this.PlanDescription2) return false;
                if (other.PlanDescription3 != this.PlanDescription3) return false;
                if (other.PlanDescription4 != this.PlanDescription4) return false;
                if (other.PlanDescription5 != this.PlanDescription5) return false;

            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
