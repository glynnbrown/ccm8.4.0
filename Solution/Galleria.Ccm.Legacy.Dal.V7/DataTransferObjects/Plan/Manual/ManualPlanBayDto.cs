﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class ManualPlanBayDto : PlanBayDto
    {
        public String Segments { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            ManualPlanBayDto other = obj as ManualPlanBayDto;
            if (other != null && base.Equals(obj))
            {
                //check each parameter
                if (other.Segments != this.Segments)
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
