﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class ManualPlanPositionDto : PlanPositionDto
    {
        public String Code { get; set; }
        public Single? PegZ { get; set; }
        public String PegStyle { get; set; }
        public Byte IsSequenced { get; set; }
        public Byte NewProducts { get; set; }
        public Double Number1 { get; set; }
        public Double Number2 { get; set; }
        public Int32 Number3 { get; set; }
        public Int32 Number4 { get; set; }
        public Int32 Number5 { get; set; }
        public String Text1 { get; set; }
        public String Text2 { get; set; }
        public String Text3 { get; set; }
        public String Text4 { get; set; }
        public String Text5 { get; set; }
        public Byte IsFlexPreservedSpace { get; set; }
        public Byte HidePreserved { get; set; }
        public String AttachmentTempFile { get; set; }
        public String AttachmentName { get; set; }
        public Byte AttachmentBaseElement { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            ManualPlanPositionDto other = obj as ManualPlanPositionDto;
            if (other != null && base.Equals(obj))
            {
                //check each parameter
                if (other.Code != this.Code) return false;
                if (other.PegZ != this.PegZ) return false;
                if (other.PegStyle != this.PegStyle) return false;
                if (other.IsSequenced != this.IsSequenced) return false;
                if (other.NewProducts != this.NewProducts) return false;
                if (other.Number1 != this.Number1) return false;
                if (other.Number2 != this.Number2) return false;
                if (other.Number3 != this.Number3) return false;
                if (other.Number4 != this.Number4) return false;
                if (other.Number5 != this.Number5) return false;
                if (other.Text1 != this.Text1) return false;
                if (other.Text2 != this.Text2) return false;
                if (other.Text3 != this.Text3) return false;
                if (other.Text4 != this.Text4) return false;
                if (other.Text5 != this.Text5) return false;
                if (other.IsFlexPreservedSpace != this.IsFlexPreservedSpace) return false;
                if (other.HidePreserved != this.HidePreserved) return false;
                if (other.AttachmentTempFile != this.AttachmentTempFile) return false;
                if (other.AttachmentName != this.AttachmentName) return false;
                if (other.AttachmentBaseElement != this.AttachmentBaseElement) return false;

            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
