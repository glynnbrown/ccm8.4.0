﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#region Version History: (CCM833)
// CCM-19150 : J.Mendes
// 3. It was improved the way the combined direction for planogram elements are calculated. 
//   - It was ammend 2 scrips (FetchClusterByPlanId and FetchStoreByPlanId) to check the plan elements bloking type. If the blocking type was equal to 10 then it was a element to be combine.
//   - In the split plan it was added an addition step to re-calculate plan element combine direction based on the OriginalTemplateElementId.
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class PlanElementDto
    {
        public Int32 Id { get; set; }
        public Int32 PlanId { get; set; }
        public Int32 PlanBayId { get; set; }
        public String Name { get; set; }
        public String Combined { get; set; }
        public Double XPosition { get; set; }
        public Double YPosition { get; set; }
        public Double ZPosition { get; set; }
        public String ElementType { get; set; }
        public Double ShelfHeight { get; set; }
        public Double ShelfWidth { get; set; }
        public Double ShelfDepth { get; set; }
        public Double ShelfThick { get; set; }
        public Double ShelfSlope { get; set; }
        public Double ShelfRiser { get; set; }
        public Double PegHeight { get; set; }
        public Double PegWidth { get; set; }
        public Double PegVertSpace { get; set; }
        public Double PegHorizSpace { get; set; }
        public Double PegVertStart { get; set; }
        public Double PegHorzStart { get; set; }
        public Double PegNotchDistance { get; set; }
        public Double PegDepth { get; set; }
        public Double ChestHeight { get; set; }
        public Double ChestWidth { get; set; }
        public Double ChestDepth { get; set; }
        public Double ChestWall { get; set; }
        public Double ChestInside { get; set; }
        public Double ChestMerchandising { get; set; }
        public Double ChestDivider { get; set; }
        public Double ChestAbove { get; set; }
        public Double BarHeight { get; set; }
        public Double BarDepth { get; set; }
        public Double BarWidth { get; set; }
        public Double BarThick { get; set; }
        public Double BarDistance { get; set; }
        public Double BarHangerDepth { get; set; }
        public Double BarNotch { get; set; }
        public Double BarHorzStart { get; set; }
        public Double BarHorzSpace { get; set; }
        public Double BarVertStart { get; set; }
        public Double BarBackHeight { get; set; }
        public Byte NonMerchandisable { get; set; }
        public Double FingerSpace { get; set; }
        public Double TopOverhang { get; set; }
        public Double BottomOverhang { get; set; }
        public Double LeftOverhang { get; set; }
        public Double RightOverhang { get; set; }
        public Double FrontOverhang { get; set; }
        public Double BackOverhang { get; set; }
        public Double Number1 { get; set; }
        public Double Number2 { get; set; }
        public Int32 Number3 { get; set; }
        public Int32 Number4 { get; set; }
        public Int32 Number5 { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public Int32 PersonnelId { get; set; }
        public String PersonnelFlexi { get; set; }
        public Int32 CompanyId { get; set; }
        public Byte ChestWallRenderType { get; set; }
        public Byte CombineDirection { get; set; }
        public Int32 OriginalTemplateElementId { get; set; }
        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            PlanElementDto other = obj as PlanElementDto;
            if (other != null)
            {
                //check each parameter
                if (other.Id != this.Id) return false;
                if (other.PlanId != this.PlanId) return false;
                if (other.PlanBayId != this.PlanBayId) return false;
                if (other.Name != this.Name) return false;
                if (other.Combined != this.Combined) return false;
                if (other.XPosition != this.XPosition) return false;
                if (other.YPosition != this.YPosition) return false;
                if (other.ZPosition != this.ZPosition) return false;
                if (other.ElementType != this.ElementType) return false;
                if (other.ShelfHeight != this.ShelfHeight) return false;
                if (other.ShelfWidth != this.ShelfWidth) return false;
                if (other.ShelfDepth != this.ShelfDepth) return false;
                if (other.ShelfThick != this.ShelfThick) return false;
                if (other.ShelfSlope != this.ShelfSlope) return false;
                if (other.ShelfRiser != ShelfRiser) return false;

                if (other.PegHeight != this.PegHeight) return false;
                if (other.PegWidth != this.PegWidth) return false;
                if (other.PegVertSpace != this.PegVertSpace) return false;
                if (other.PegHorizSpace != this.PegHorizSpace) return false;
                if (other.PegVertStart != this.PegVertStart) return false;
                if (other.PegHorzStart != PegHorzStart) return false;
                if (other.PegNotchDistance != PegNotchDistance) return false;
                if (other.PegDepth != PegDepth) return false;

                if (other.ChestHeight != this.ChestHeight) return false;
                if (other.ChestWidth != this.ChestWidth) return false;
                if (other.ChestDepth != this.ChestDepth) return false;
                if (other.ChestWall != this.ChestWall) return false;
                if (other.ChestInside != this.ChestInside) return false;
                if (other.ChestMerchandising != ChestMerchandising) return false;
                if (other.ChestDivider != ChestDivider) return false;
                if (other.ChestAbove != ChestAbove) return false;

                if (other.BarHeight != this.BarHeight) return false;
                if (other.BarDepth != this.BarDepth) return false;
                if (other.BarWidth != this.BarWidth) return false;
                if (other.BarThick != this.BarThick) return false;
                if (other.BarDistance != this.BarDistance) return false;
                if (other.BarHangerDepth != BarHangerDepth) return false;
                if (other.BarNotch != BarNotch) return false;
                if (other.BarHorzStart != BarHorzStart) return false;
                if (other.BarHorzSpace != BarHorzSpace) return false;
                if (other.BarVertStart != BarVertStart) return false;
                if (other.BarBackHeight != BarBackHeight) return false;

                if (other.NonMerchandisable != this.NonMerchandisable) return false;
                if (other.FingerSpace != this.FingerSpace) return false;
                if (other.TopOverhang != this.TopOverhang) return false;
                if (other.BottomOverhang != this.BottomOverhang) return false;
                if (other.LeftOverhang != this.LeftOverhang) return false;
                if (other.RightOverhang != this.RightOverhang) return false;
                if (other.FrontOverhang != this.FrontOverhang) return false;
                if (other.BackOverhang != this.BackOverhang) return false;


                if (other.Number1 != this.Number1) return false;
                if (other.Number2 != this.Number2) return false;
                if (other.Number3 != this.Number3) return false;
                if (other.Number4 != this.Number4) return false;
                if (other.Number5 != this.Number5) return false;
                if (other.FingerSpace != this.FingerSpace) return false;
                if (other.Created != this.Created) return false;
                if (other.Modified != this.Modified) return false;
                if (other.PersonnelId != this.PersonnelId) return false;
                if (other.PersonnelFlexi != this.PersonnelFlexi) return false;
                if (other.CompanyId != this.CompanyId) return false;
                if (other.CombineDirection != this.CombineDirection) return false;
                if (other.OriginalTemplateElementId != this.OriginalTemplateElementId) return false;
            }
            else
            {
                return false;
            }
            return true;

        }

    }
}
