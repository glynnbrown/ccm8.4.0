﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class PlanProfileDto
    {
        public Int32 Id { get; set; }
        public Int32 PlanId { get; set; }
        public Int32 ProfileDetailId { get; set; }
        public Byte ProfileGroupType { get; set; }
        public String Name { get; set; }
        public Boolean IsActive { get; set; }
        public Int16 Priority { get; set; }
        public Int16 Ratio { get; set; }
        public Single Percentage { get; set; }
        public Single Minimum { get; set; }
        public Single Maximum { get; set; }
        public Int16 RatioLowest { get; set; }
        public Int16 RatioHighest { get; set; }
        public String KeyField { get; set; }
        public Int32 ProfileGroupValue { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            PlanProfileDto other = obj as PlanProfileDto;
            if (other != null)
            {
                //check each parameter
                if (other.Id != this.Id) return false;
                if (other.PlanId != this.PlanId) return false;
                if (other.ProfileDetailId != this.ProfileDetailId) return false;
                if (other.ProfileGroupType != this.ProfileGroupType) return false;
                if (other.Name != this.Name) return false;
                if (other.IsActive != this.IsActive) return false;
                if (other.Priority != this.Priority) return false;
                if (other.Ratio != this.Ratio) return false;
                if (other.Percentage != this.Percentage) return false;
                if (other.Minimum != this.Minimum) return false;
                if (other.Maximum != this.Maximum) return false;
                if (other.RatioLowest != this.RatioLowest) return false;
                if (other.RatioHighest != this.RatioHighest) return false;
                if (other.KeyField != this.KeyField) return false;
                if (other.ProfileGroupValue != this.ProfileGroupValue) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
