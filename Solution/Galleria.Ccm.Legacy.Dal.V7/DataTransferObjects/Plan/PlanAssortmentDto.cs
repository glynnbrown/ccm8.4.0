﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    [Serializable]
    public class PlanAssortmentDto
    {
        public Int32 ProductId { get; set; }
        public String Code { get; set; }
        public String FormattedCode { get; set; }
        public String Name { get; set; }
        public Boolean Recommended { get; set; }
        public Int32 GlobalRank { get; set; }
        public Int32 RecomendedUnits { get; set; }
        public Int32 RecomendedFacings { get; set; }
        public String Segmentation { get; set; }
        public Int32 Rule1Type { get; set; }
        public Double RuleValue1 { get; set; }
        public Int32 Rule2Type { get; set; }
        public Double RuleValue2 { get; set; }
        public Boolean IsRuleDropped { get; set; }
        public Byte ViolationType { get; set; }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            PlanAssortmentDto other = obj as PlanAssortmentDto;
            if (other != null)
            {
                //check each parameter
                if (other.ProductId != this.ProductId) return false;
                if (other.Code != this.Code) return false;
                if (other.FormattedCode != this.FormattedCode) return false;
                if (other.Name != this.Name) return false;
                if (other.Recommended != this.Recommended) return false;
                if (other.GlobalRank != this.GlobalRank) return false;
                if (other.RecomendedUnits != this.RecomendedUnits) return false;
                if (other.RecomendedFacings != this.RecomendedFacings) return false;                
                if (other.Segmentation != this.Segmentation) return false;
                if (other.Rule1Type != this.Rule1Type) return false;
                if (other.RuleValue1 != this.RuleValue1) return false;
                if (other.Rule2Type != this.Rule2Type) return false;
                if (other.RuleValue2 != this.RuleValue2) return false;
                if (other.IsRuleDropped != this.IsRuleDropped) return false;
                if (other.ViolationType != this.ViolationType) return false;
            }
            else
            {
                return false;
            }
            return true;

        }
    }
}
