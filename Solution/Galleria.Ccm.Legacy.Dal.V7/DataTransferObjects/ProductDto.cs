﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31549 : M.Brumby
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Legacy.Dal.V7.DataTransferObjects
{
    /// <summary>
    /// Our Product Data Definition Dto
    /// </summary>
    [Serializable]
    public class ProductDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32? ProductLevel10Id { get; set; }
        public String Code { get; set; }
        public String SubCode { get; set; }
        public String FormattedCode { get; set; }
        public String Name { get; set; }
        public String ODSProductReference { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public float Depth { get; set; }
        public float SqueezeHeight { get; set; }
        public float SqueezeWidth { get; set; }
        public float SqueezeDepth { get; set; }
        public float TrayHigh { get; set; }
        public float TrayWide { get; set; }
        public float TrayDeep { get; set; }
        public float TrayThickHeight { get; set; }
        public float TrayThickWidth { get; set; }
        public float TrayThickDepth { get; set; }
        public float TrayHeight { get; set; } //CCM-21084
        public float TrayWidth { get; set; } //CCM-21084
        public float TrayDepth { get; set; } //CCM-21084
        public Int16 TraypackUnits { get; set; } //CCM-21084
        public Int32 MaxCap { get; set; }
        public Int32 MaxStack { get; set; }
        public Int32 MinDeep { get; set; }
        public Int32 MaxDeep { get; set; }
        public Int16 CasepackUnits { get; set; }
        public Byte FrontOnly { get; set; }
        public Byte TrayProduct { get; set; }
        public Int16 MerchType { get; set; }
        public Int32? BarOverHang { get; set; }
        public float? NestHeight { get; set; }
        public float? NestWidth { get; set; }
        public float? NestDepth { get; set; }
        public float? NestingHeight { get; set; }
        public float? NestingWidth { get; set; }
        public float? NestingDepth { get; set; }
        public float? LeftNestHeight { get; set; }
        public float? LeftNestWidth { get; set; }
        public float? LeftNestTopOffSet { get; set; }
        public float? RightNestHeight { get; set; }
        public float? RightNestWidth { get; set; }
        public float? RightNestTopOffSet { get; set; }
        public float PegX { get; set; }
        public float? PegX2 { get; set; }
        public float PegY { get; set; }
        public float? PegY2 { get; set; }
        public float? PegProng { get; set; }
        public Single PegDepth { get; set; }
        public String BuddyAggregationType { get; set; }
        public float BuddyMultiplier11 { get; set; }
        public float BuddyMultiplier12 { get; set; }
        public float BuddyMultiplier13 { get; set; }
        public String ProductLevel10Flexi { get; set; }
        public String Text1 { get; set; }
        public String Text2 { get; set; }
        public String Text3 { get; set; }
        public String Text4 { get; set; }
        public String Text5 { get; set; }
        public String Text6 { get; set; }
        public String Text7 { get; set; }
        public String Text8 { get; set; }
        public String Text9 { get; set; }
        public String Text10 { get; set; }
        public String Text11 { get; set; }
        public String Text12 { get; set; }
        public String Text13 { get; set; }
        public String Text14 { get; set; }
        public String Text15 { get; set; }
        public String Text16 { get; set; }
        public String Text17 { get; set; }
        public String Text18 { get; set; }
        public String Text19 { get; set; }
        public String Text20 { get; set; }
        public float? Number1 { get; set; }
        public float? Number2 { get; set; }
        public float? Number3 { get; set; }
        public float? Number4 { get; set; }
        public float? Number5 { get; set; }
        public float? Number6 { get; set; }
        public float? Number7 { get; set; }
        public float? Number8 { get; set; }
        public float? Number9 { get; set; }
        public float? Number10 { get; set; }
        public Int32? Number11 { get; set; }
        public Int32? Number12 { get; set; }
        public Int32? Number13 { get; set; }
        public Int32? Number14 { get; set; }
        public Int32? Number15 { get; set; }
        public Int32? Number16 { get; set; }
        public Int32? Number17 { get; set; }
        public Int32? Number18 { get; set; }
        public Int32? Number19 { get; set; }
        public Int32? Number20 { get; set; }
        public Byte? BreakTrayUp { get; set; }
        public Byte? BreakTrayBack { get; set; }
        public Byte? BreaktrayDown { get; set; }
        public Int32 MaxRightCap { get; set; }
        public Int32 MaxNestHigh { get; set; }
        public Int32 MaxNestDeep { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public Boolean CanMiddleCap { get; set; }
        public Boolean IsNew { get; set; }
        public DateTime? DeletedDate { get; set; }
        public Boolean IsPlaceholder { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(object obj)
        {
            ProductDto other = obj as ProductDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ProductLevel10Id != this.ProductLevel10Id) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.SubCode != this.SubCode) { return false; }
                if (other.FormattedCode != this.FormattedCode) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ODSProductReference != this.ODSProductReference) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.SqueezeHeight != this.SqueezeHeight) { return false; }
                if (other.SqueezeWidth != this.SqueezeWidth) { return false; }
                if (other.SqueezeDepth != this.SqueezeDepth) { return false; }
                if (other.TrayHigh != this.TrayHigh) { return false; }
                if (other.TrayWide != this.TrayWide) { return false; }
                if (other.TrayDeep != this.TrayDeep) { return false; }
                if (other.TrayThickHeight != this.TrayThickHeight) { return false; }
                if (other.TrayThickWidth != this.TrayThickWidth) { return false; }
                if (other.TrayThickDepth != this.TrayThickDepth) { return false; }
                if (other.TrayHeight != this.TrayHeight) { return false; } //CCM-21084
                if (other.TrayWidth != this.TrayWidth) { return false; } //CCM-21084
                if (other.TrayDepth != this.TrayDepth) { return false; } //CCM-21084
                if (other.TraypackUnits != this.TraypackUnits) { return false; } //CCM-21084
                if (other.MaxCap != this.MaxCap) { return false; }
                if (other.MaxStack != this.MaxStack) { return false; }
                if (other.MinDeep != this.MinDeep) { return false; }
                if (other.MaxDeep != this.MaxDeep) { return false; }
                if (other.CasepackUnits != this.CasepackUnits) { return false; }
                if (other.FrontOnly != this.FrontOnly) { return false; }
                if (other.TrayProduct != this.TrayProduct) { return false; }
                if (other.MerchType != this.MerchType) { return false; }
                if (other.BarOverHang != this.BarOverHang) { return false; }
                if (other.NestHeight != this.NestHeight) { return false; }
                if (other.NestWidth != this.NestWidth) { return false; }
                if (other.NestDepth != this.NestDepth) { return false; }
                if (other.NestingHeight != this.NestingHeight) { return false; }
                if (other.NestingWidth != this.NestingWidth) { return false; }
                if (other.NestingDepth != this.NestingDepth) { return false; }
                if (other.LeftNestHeight != this.LeftNestHeight) { return false; }
                if (other.LeftNestWidth != this.LeftNestWidth) { return false; }
                if (other.LeftNestTopOffSet != this.LeftNestTopOffSet) { return false; }
                if (other.RightNestHeight != this.RightNestHeight) { return false; }
                if (other.RightNestWidth != this.RightNestWidth) { return false; }
                if (other.RightNestTopOffSet != this.RightNestTopOffSet) { return false; }
                if (other.PegX != this.PegX) { return false; }
                if (other.PegX2 != this.PegX2) { return false; }
                if (other.PegY != this.PegY) { return false; }
                if (other.PegY2 != this.PegY2) { return false; }
                if (other.PegProng != this.PegProng) { return false; }
                if (other.PegDepth != this.PegDepth) { return false; }
                if (other.BuddyAggregationType != this.BuddyAggregationType) { return false; }
                if (other.BuddyMultiplier11 != this.BuddyMultiplier11) { return false; }
                if (other.BuddyMultiplier12 != this.BuddyMultiplier12) { return false; }
                if (other.BuddyMultiplier13 != this.BuddyMultiplier13) { return false; }
                if (other.ProductLevel10Flexi != this.ProductLevel10Flexi) { return false; }
                if (other.Text1 != this.Text1) { return false; }
                if (other.Text2 != this.Text2) { return false; }
                if (other.Text3 != this.Text3) { return false; }
                if (other.Text4 != this.Text4) { return false; }
                if (other.Text5 != this.Text5) { return false; }
                if (other.Text6 != this.Text6) { return false; }
                if (other.Text7 != this.Text7) { return false; }
                if (other.Text8 != this.Text8) { return false; }
                if (other.Text9 != this.Text9) { return false; }
                if (other.Text10 != this.Text10) { return false; }
                if (other.Text11 != this.Text11) { return false; }
                if (other.Text12 != this.Text12) { return false; }
                if (other.Text13 != this.Text13) { return false; }
                if (other.Text14 != this.Text14) { return false; }
                if (other.Text15 != this.Text15) { return false; }
                if (other.Text16 != this.Text16) { return false; }
                if (other.Text17 != this.Text17) { return false; }
                if (other.Text18 != this.Text18) { return false; }
                if (other.Text19 != this.Text19) { return false; }
                if (other.Text20 != this.Text20) { return false; }
                if (other.Number1 != this.Number1) { return false; }
                if (other.Number2 != this.Number2) { return false; }
                if (other.Number3 != this.Number3) { return false; }
                if (other.Number4 != this.Number4) { return false; }
                if (other.Number5 != this.Number5) { return false; }
                if (other.Number6 != this.Number6) { return false; }
                if (other.Number7 != this.Number7) { return false; }
                if (other.Number8 != this.Number8) { return false; }
                if (other.Number9 != this.Number9) { return false; }
                if (other.Number10 != this.Number10) { return false; }
                if (other.Number11 != this.Number11) { return false; }
                if (other.Number12 != this.Number12) { return false; }
                if (other.Number13 != this.Number13) { return false; }
                if (other.Number14 != this.Number14) { return false; }
                if (other.Number15 != this.Number15) { return false; }
                if (other.Number16 != this.Number16) { return false; }
                if (other.Number17 != this.Number17) { return false; }
                if (other.Number18 != this.Number18) { return false; }
                if (other.Number19 != this.Number19) { return false; }
                if (other.Number20 != this.Number20) { return false; }
                if (other.BreakTrayUp != this.BreakTrayUp) { return false; }
                if (other.BreakTrayBack != this.BreakTrayBack) { return false; }
                if (other.BreaktrayDown != this.BreaktrayDown) { return false; }
                if (other.MaxRightCap != this.MaxRightCap) { return false; }
                if (other.MaxNestHigh != this.MaxNestHigh) { return false; }
                if (other.MaxNestDeep != this.MaxNestDeep) { return false; }
                if (other.Created != this.Created) { return false; }
                if (other.Modified != this.Modified) { return false; }
                if (other.CanMiddleCap != this.CanMiddleCap) { return false; }
                if (other.IsNew != this.IsNew) { return false; }
                if (other.DeletedDate != this.DeletedDate) { return false; }
                if (other.IsPlaceholder != this.IsPlaceholder) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion 
    }
}
