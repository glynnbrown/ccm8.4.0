/*
* This query displays the metric
* details captured by the engine
*/
SELECT
	mt.[MetricType_Type] AS [Type],
	mt.[MetricType_Name] AS [Name],
	COUNT(*) AS [Occurrances],
	CAST(SUM(m.[Metric_Count]) AS FLOAT) AS [Count],
	CAST(SUM(m.[Metric_Duration]/1000) AS FLOAT) AS [Duration (Seconds)],
	CASE
		WHEN CAST(SUM(m.[Metric_Count]) AS FLOAT) = 0 THEN 0
		ELSE CAST(SUM(m.[Metric_Duration]/1000) AS FLOAT)/CAST(SUM(m.[Metric_Count]) AS FLOAT)
	END AS [Average Duration (Seconds)]
FROM
	[Engine].[Metric] AS m WITH (NOLOCK)
	INNER JOIN [Engine].[MetricType] AS mt WITH (NOLOCK) ON mt.[MetricType_Id] = m.[MetricType_Id]
GROUP BY
	mt.[MetricType_Type],
	mt.[MetricType_Name]
ORDER BY
	[Average Duration (Seconds)] DESC
