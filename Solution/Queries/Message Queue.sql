/*
* This query displays the message
* queue in the order that they will
* be processed, with the currently
* processing messages at the top
*/
SELECT
	m.[Message_Id],
	m.[Message_Priority],
	mdata.[Message_Data],
	mdata.[Message_DateCreated],
	m.[Message_DateScheduled],
	m.[Message_DateProcessed]
FROM
	[Engine].[Message] AS m WITH (NOLOCK)
	INNER JOIN [Engine].[MessageData] AS mdata WITH (NOLOCK) ON mdata.[Message_Id] = m.[Message_Id]
	LEFT JOIN [Engine].[MessageDependency] AS mdep WITH (NOLOCK) ON mdep.[MessageDependency_ChildMessageId] = m.[Message_Id]
GROUP BY
	m.[Message_Id],
	m.[Message_Priority],
	mdata.[Message_Data],
	mdata.[Message_DateCreated],
	m.[Message_DateScheduled],
	m.[Message_DateProcessed]
ORDER BY
	ISNULL([Message_DateProcessed], '2079-06-06') ASC,
	COUNT(*) ASC,
	m.[Message_Priority] ASC,
	m.[Message_Id] ASC