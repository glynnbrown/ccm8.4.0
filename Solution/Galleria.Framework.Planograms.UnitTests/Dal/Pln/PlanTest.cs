﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24779 : D.Pleasance
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal.Configuration;
using System.IO;
using Galleria.Framework.Dal;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using System.Diagnostics;
using Galleria.Ccm.Services.Planogram;
using Galleria.Framework.Planograms.UnitTests.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Pln
{
    public class PlanTest
    {
        public PlanTest ()
	    {
	    }

        //[Test]
        public void LoadPlan()
        {
            // \\devcentral\Morrisons_CCM755\Space\Micro\MerchandisingPlan\Source
            // \\devcentral\Maxima_CCM737\Space\Micro\MerchandisingPlan\Source
            String dir = @"\\pluto\CustData\PnS\";

            LoadPlansWithinDirectory(dir);
        }

        public void LoadPlansWithinDirectory(String directory)
        {
            object[] fetchArg = new object[] { new List<PlanogramFieldMappingDto>(), 
                                                       new List<PlanogramMetricMappingDto>()};

            foreach (String dir in Directory.GetDirectories(directory))
            {
                foreach (String planFile in Directory.GetFiles(dir, "*.pln"))
                {
                    Debug.WriteLine(planFile);
                    
                    DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(
                        planFile,
                        "Galleria.Framework.Planograms.Dal.Pln");

                    // create the dal factory
                    var dalFactory = new Galleria.Framework.Planograms.Dal.Pln.DalFactory(dalFactoryConfig);

                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                        {
                            dal.LockById(planFile, 0, 0, false);
                            PackageDto packageDto = dal.FetchById(planFile, fetchArg);
                        }
                    }
                }
                LoadPlansWithinDirectory(dir);
            }
        }

        [Test]
        public void TestLoadPlnFile()
        {
            object[] fetchArg = new object[] { new List<PlanogramFieldMappingDto>(), 
                                                       new List<PlanogramMetricMappingDto>()};

            String planFile = Path.Combine(Environment.CurrentDirectory, "Environment\\TestPlnFile.pln");
            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(
                planFile,
                "Galleria.Framework.Planograms.Dal.Pln");

            // create the dal factory
            var dalFactory = new Galleria.Framework.Planograms.Dal.Pln.DalFactory(dalFactoryConfig);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto = null;

                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(planFile, 0, 0, false);
                    packageDto = dal.FetchById(planFile, fetchArg);
                }

                IEnumerable<PlanogramDto> planogramDtos;
                using (IPlanogramDal dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDtos = dal.FetchByPackageId(packageDto.Id);
                }

                foreach (PlanogramDto planogramDto in planogramDtos)
                {
                    IEnumerable<PlanogramFixtureItemDto> planogramFixtureItemDtos;
                    IEnumerable<PlanogramFixtureDto> planogramFixtureDtos;
                    List<PlanogramFixtureComponentDto> planogramFixtureComponentDtos = new List<PlanogramFixtureComponentDto>();
                    IEnumerable<PlanogramComponentDto> planogramComponentDtos;
                    List<PlanogramSubComponentDto> planogramSubComponentDtos = new List<PlanogramSubComponentDto>();
                    using (IPlanogramFixtureItemDal dal = dalContext.GetDal<IPlanogramFixtureItemDal>())
                    {
                        planogramFixtureItemDtos = dal.FetchByPlanogramId(planogramDto.Id);
                    }                    
                    using (IPlanogramFixtureDal dal = dalContext.GetDal<IPlanogramFixtureDal>())
                    {
                        planogramFixtureDtos = dal.FetchByPlanogramId(planogramDto.Id);
                    }
                    foreach (PlanogramFixtureDto planogramFixtureDto in planogramFixtureDtos)
                    {
                        using (IPlanogramFixtureComponentDal dal = dalContext.GetDal<IPlanogramFixtureComponentDal>())
                        {
                            planogramFixtureComponentDtos.AddRange(dal.FetchByPlanogramFixtureId(planogramFixtureDto.Id));
                        }
                    }
                    using (IPlanogramComponentDal dal = dalContext.GetDal<IPlanogramComponentDal>())
                    {
                        planogramComponentDtos = dal.FetchByPlanogramId(1);
                    }
                    foreach (PlanogramComponentDto planogramComponentDto in planogramComponentDtos)
                    {
                        using (IPlanogramSubComponentDal dal = dalContext.GetDal<IPlanogramSubComponentDal>())
                        {
                            planogramSubComponentDtos.AddRange(dal.FetchByPlanogramComponentId(planogramComponentDto.Id));
                        }
                    }

                    Assert.AreEqual(planogramFixtureItemDtos.Count(), 5);
                    Assert.AreEqual(planogramFixtureDtos.Count(), 5);
                    Assert.AreEqual(planogramFixtureComponentDtos.Count, 47);
                    Assert.AreEqual(planogramComponentDtos.Count(), 47);
                    Assert.AreEqual(planogramSubComponentDtos.Count, 47);
                }
            }
        }

        /// <summary>
        /// A Test to see if a file does get created.
        /// </summary>
        /// <remarks>Copletion of ths test is on hold due to huge amounts of critical work for Safeway</remarks>
        [Test]
        public void TestExportCreatesAnExportedFile()
        {
            //// Create a planogram.
            //var plan = "TestPlanogram".CreatePackage().AddPlanogram();
            //var bay = plan.AddFixtureItem();
            //var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            //shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            //var position1 = shelf.AddPosition(bay, plan.AddProduct());
            //var position2 = shelf.AddPosition(bay, plan.AddProduct());
            //var position3 = shelf.AddPosition(bay, plan.AddProduct());
            //var position4 = shelf.AddPosition(bay, plan.AddProduct());
            
            ////Check the plan is in a valid state to save. 
            //Boolean isPlanValid = plan.IsValid;
            //Assert.IsTrue(isPlanValid, "The test failed because the plan wasn't in a valid state to save down.");

            ////Save - Aquire some default / blank planogramFieldMappings
            //List<PlanogramFieldMapping> planogramFieldMappings = Ccm.Model.PlanogramExportTemplate.NewPlanogramExportTemplate(1, PlanogramExportFileType.Spaceman).GetPlanogramFieldMappings();
            //List<PlanogramFieldMappingDto> planogramFieldMappingDtoList = new List<PlanogramFieldMappingDto>();
            //planogramFieldMappings.ForEach(m =>
            //{
            //    planogramFieldMappingDtoList.Add(m.GetDataTransferObject());
            //});

            ////Aquire some default / blank planogramFieldMappings
            //List<PlanogramMetricMapping> planogramMetricMappings = Ccm.Model.PlanogramExportTemplate.NewPlanogramExportTemplate(1, PlanogramExportFileType.Spaceman).GetPlanogramMetricMappings();
            //List<PlanogramMetricMappingDto> planogramMetricMappingDtoList = new List<PlanogramMetricMappingDto>();
            //planogramMetricMappings.ForEach(m =>
            //{
            //    planogramMetricMappingDtoList.Add(m.GetDataTransferObject());
            //});

            ////Create the context with our mappings.
            //ExportContext myContext = new ExportContext(PlanogramSettings.NewPlanogramSettings(),
            //    planogramFieldMappingDtoList, planogramMetricMappingDtoList, false,false,true,false);
          
            ////Save
            //const String cfilename = @"C:\MyTestExportedPlan";
            //plan.Parent.ExportToExternalFile(1, PlanogramExportFileType.Spaceman, cfilename, myContext);

            ////Check Exists
            //File.Exists(cfilename);
            //File.Delete(cfilename);
           
        }
    }
}
