﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Initial version.
#endregion

#region Version History: CCM810
// V8-29860 : M.Shelley
//  Added the PlanogramType property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Dal
{
    [TestFixture]
    public class DataTransferObjects
    {
        #region Fields
        private List<Type> _dtoAndKeyTypes = new List<Type>(); // holds a list of all dto types and dto key types in SA
        private List<Type> _dtoTypes = new List<Type>(); // holds a list of all dto types in SA
        private static Byte[] _byteList = { 1, 2, 3, 4 };
        private static Byte[] _rowVersion = { 1, 2, 3, 4, 5, 6, 7, 8 };
        #endregion

        #region Properties
        /// <summary>
        /// Returns all available dto and dto key types in the database
        /// </summary>
        public IEnumerable<Type> DtoAndKeyTypes
        {
            get { return _dtoAndKeyTypes; }
        }
        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<Type> DtoTypes
        {
            get { return _dtoTypes; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DataTransferObjects()
        {
            foreach (Type dtoType in
                Assembly.GetAssembly(typeof(Galleria.Framework.Planograms.Constants)).GetTypes()
                .Where(t => t.Namespace == "Galleria.Framework.Planograms.Dal.DataTransferObjects"))
            {

                _dtoAndKeyTypes.Add(dtoType);
                if (!dtoType.Name.EndsWith("Key"))
                {
                    _dtoTypes.Add(dtoType);
                }
            }
            _dtoAndKeyTypes.Sort(delegate(Type x, Type y)
            {
                return x.Name.CompareTo(y.Name);
            });
            _dtoTypes.Sort(delegate(Type x, Type y)
            {
                return x.Name.CompareTo(y.Name);
            });
        }
        #endregion

        #region Methods
        /// <summary>
        /// Tests that the dto is marked as serializable
        /// </summary>
        [Category(Categories.Smoke)]
        [Test, TestCaseSource("DtoTypes")]
        public void Serializable(Type dtoType)
        {
            object obj = Activator.CreateInstance(dtoType);
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
            }
        }

        /// <summary>
        /// Tests the equality of a dto
        /// </summary>
        [Category(Categories.Smoke)]
        [Test, TestCaseSource("DtoAndKeyTypes")]
        public void Equality(Type dtoType)
        {
            if (!dtoType.Name.EndsWith("SearchCriteriaDto"))
            {

                // now create two new instances of the test type
                object instanceOne = Activator.CreateInstance(dtoType);
                object instanceTwo = Activator.CreateInstance(dtoType);

                // and assert that they are equal after creation
                Assert.AreEqual(instanceOne, instanceTwo);

                // now, we run through all the public properties
                // on the two instances, changing their values
                // as we go and comparing whether the objects
                // are now equal or not
                foreach (PropertyInfo propertyInfo in dtoType.GetProperties().Where(p => p.CanWrite))
                {
                    // write the property name
                    Debug.WriteLine(propertyInfo.Name);

                    // assign a value to this property of instance one
                    SetProperty(instanceOne, propertyInfo);

                    // assert that the instances are no longer equal
                    Assert.AreNotEqual(instanceOne, instanceTwo);

                    // assign a value to this property of instance two
                    SetProperty(instanceTwo, propertyInfo);

                    // assert that the instance are equal again
                    Assert.AreEqual(instanceOne, instanceTwo);

                    // clear the value of this property of instance one
                    ClearProperty(instanceOne, propertyInfo);

                    // assert that the instance are no longer equal
                    Assert.AreNotEqual(instanceOne, instanceTwo);

                    // clear the property of instance two
                    ClearProperty(instanceTwo, propertyInfo);

                    // and assert that the instances are equal again
                    Assert.AreEqual(instanceOne, instanceTwo);
                }
            }
        }

        [Category(Categories.Smoke)]
        [Test, TestCaseSource("DtoTypes")]
        public void HasDtoKeyProperty(Type dtoType)
        {
            //if not an info object then check the dto has a dtokey property
            if (!dtoType.Name.EndsWith("InfoDto")
                && !dtoType.Name.EndsWith("IsSetDto")
                && !dtoType.Name.EndsWith("SearchCriteriaDto"))
            {
                PropertyInfo property = dtoType.GetProperty("DtoKey");
                if (property == null)
                {
                    throw new InconclusiveException("DtoKey property missing");
                }
            }
        }

        /// <summary>
        /// Sets a test value for the specified property
        /// on the specified instance
        /// </summary>
        /// <param name="instance">The instance to set a value for</param>
        /// <param name="propertyInfo">The property to set</param>
        private static void SetProperty(object instance, PropertyInfo propertyInfo)
        {
            // assign some default value based on the property type
            bool propertySet = false;
            object propertyValue = null;
            Type propertyType = propertyInfo.PropertyType;
            if ((propertyType == typeof(int)) || (propertyType == typeof(int?)))
            {
                propertyValue = 219382;
                propertySet = true;
            }
            else if ((propertyType == typeof(float)) || 
                (propertyType == typeof(float?)) ||
                (propertyType == typeof(Double)) ||
                (propertyType == typeof(Double?)))
            {
                propertyValue = 213.123123F;
                propertySet = true;
            }
            else if (propertyType == typeof(string))
            {
                propertyValue = "C4FC766A-E741-43E1-A69B-E40CC4107D51";
                propertySet = true;
            }
            else if ((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?)))
            {
                propertyValue = DateTime.Today;
                propertySet = true;
            }
            else if ((propertyType == typeof(bool)) || (propertyType == typeof(bool?)))
            {
                propertyValue = true;
                propertySet = true;
            }
            else if ((propertyType == typeof(short)) || (propertyType == typeof(short?)))
            {
                propertyValue = (short)12312;
                propertySet = true;
            }
            else if ((propertyType == typeof(byte)) || (propertyType == typeof(byte?)))
            {
                propertyValue = (byte)189;
                propertySet = true;
            }
            else if ((propertyType == typeof(Guid)) || (propertyType == typeof(Guid?)))
            {
                propertyValue = new Guid("738B4722-5975-42B9-B8AD-19CDE92C8C20");
                propertySet = true;
            }
            else if ((propertyType == typeof(Int64)) || (propertyType == typeof(Int64?)))
            {
                propertyValue = 34543535353;
                propertySet = true;
            }
            else if (propertyType == typeof(List<List<object>>))
            {
                List<List<object>> newListOfLists = new List<List<object>>();
                List<object> newList2 = new List<object>();
                List<object> newList3 = new List<object>();
                newList2.Add("C4FC766A-E741-43E1-A69B-E40CC4107D51");
                newList2.Add("34682ff6-426c-471d-8002-5dbea10aeb14");
                newList3.Add("ABCD766A-E741-43E1-A69B-E40CC4107D51");
                newList3.Add("ABCDE");
                newListOfLists.Add(newList2);
                newListOfLists.Add(newList3);
                propertyValue = newListOfLists;
                propertySet = true;
            }
            else if (propertyType == typeof(List<string>))
            {
                List<string> newList = new List<string>();
                newList.Add("EFGHI66A-E741-43E1-A69B-E40CC4107D51");
                newList.Add("JKLMNff6-426c-471d-8002-5dbea10aeb14");
                propertyValue = newList;
                propertySet = true;
            }
            else if (propertyType == typeof(Byte[]))
            {
                byte[] propertyArray = new Byte[_byteList.Length];
                Array.Copy(_byteList, propertyArray, propertyArray.Length);
                propertyValue = propertyArray;
                propertySet = true;
            }
            else if (propertyType == typeof(RowVersion))
            {
                propertyValue = new RowVersion(_rowVersion);
                propertySet = true;
            }
            else if (propertyType == typeof(PlanogramStatusType))
            {
                propertyValue = PlanogramStatusType.Approved;
                propertySet = true;
            }
            else if (propertyType == typeof(PlanogramType))
            {
                propertyValue = PlanogramType.ReferencePlanogram;
                propertySet = true;
            }
            else if (propertyType == typeof(object))
            {
                propertyValue = 5;
                propertySet = true;
            }

            // ensure that a value was actually set
            if (!propertySet)
            {
                throw new Exception(String.Format("An unrecognised property type has been identified : {0}", propertyType.ToString()));
            }

            // now set the value
            propertyInfo.SetValue(instance, propertyValue, null);
        }

        /// <summary>
        /// Clears a test value for the specified property
        /// on the specified instance
        /// </summary>
        /// <param name="instance">The instance to clear a value for</param>
        /// <param name="propertyInfo">The property to clear</param>
        private static void ClearProperty(object instance, PropertyInfo propertyInfo)
        {
            object propertyValue;
            if (propertyInfo.PropertyType.IsValueType)
            {
                propertyValue = Activator.CreateInstance(propertyInfo.PropertyType);
            }
            else
            {
                propertyValue = null;
            }
            propertyInfo.SetValue(instance, propertyValue, null);
        }
        #endregion
    }
}
