﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820
// V8-30725 : M.Brumby
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Dal.Configuration;
using System.IO;
using Galleria.Framework.Dal;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Xmz
{
    public class PlanTest
    {
        public PlanTest ()
	    {
	    }


        [Test]
        public void TestLoadXmzFile()
        {
            Object[] fetchArg =
            {
                new List<PlanogramFieldMappingDto>(),
                new List<PlanogramMetricMappingDto>()
            };

            String planFile = Path.Combine(Environment.CurrentDirectory, "Environment\\TextXmzFilev11.Xmz");
            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(
                planFile,
                "Galleria.Framework.Planograms.Dal.Xmz");

            // create the dal factory
            var dalFactory = new Galleria.Framework.Planograms.Dal.Xmz.DalFactory(dalFactoryConfig);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto = null;

                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(planFile, 0, 0, false);
                    packageDto = dal.FetchById(planFile, fetchArg);
                }

                IEnumerable<PlanogramDto> planogramDtos;
                using (IPlanogramDal dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDtos = dal.FetchByPackageId(packageDto.Id);
                }

                foreach (PlanogramDto planogramDto in planogramDtos)
                {
                    List<PlanogramFixtureItemDto> planogramFixtureItemDtos;
                    List<PlanogramFixtureDto> planogramFixtureDtos;
                    List<PlanogramFixtureComponentDto> planogramFixtureComponentDtos = new List<PlanogramFixtureComponentDto>();
                    List<PlanogramComponentDto> planogramComponentDtos;
                    List<PlanogramSubComponentDto> planogramSubComponentDtos = new List<PlanogramSubComponentDto>();
                    using (IPlanogramFixtureItemDal dal = dalContext.GetDal<IPlanogramFixtureItemDal>())
                    {
                        planogramFixtureItemDtos = dal.FetchByPlanogramId(planogramDto.Id).ToList();
                    }
                    using (IPlanogramFixtureDal dal = dalContext.GetDal<IPlanogramFixtureDal>())
                    {
                        planogramFixtureDtos = dal.FetchByPlanogramId(planogramDto.Id).ToList();
                    }
                    foreach (PlanogramFixtureDto planogramFixtureDto in planogramFixtureDtos)
                    {
                        using (IPlanogramFixtureComponentDal dal = dalContext.GetDal<IPlanogramFixtureComponentDal>())
                        {
                            planogramFixtureComponentDtos.AddRange(dal.FetchByPlanogramFixtureId(planogramFixtureDto.Id));
                        }
                    }
                    using (IPlanogramComponentDal dal = dalContext.GetDal<IPlanogramComponentDal>())
                    {
                        planogramComponentDtos = dal.FetchByPlanogramId(planogramDto.Id).ToList();
                    }
                    foreach (PlanogramComponentDto planogramComponentDto in planogramComponentDtos)
                    {
                        using (IPlanogramSubComponentDal dal = dalContext.GetDal<IPlanogramSubComponentDal>())
                        {
                            planogramSubComponentDtos.AddRange(dal.FetchByPlanogramComponentId(planogramComponentDto.Id));
                        }
                    }

                    Assert.AreEqual(planogramFixtureItemDtos.Count, 1);
                    Assert.AreEqual(planogramFixtureDtos.Count, 1);
                    Assert.AreEqual(planogramFixtureComponentDtos.Count, 16);
                    Assert.AreEqual(planogramComponentDtos.Count, 16);
                    Assert.AreEqual(planogramSubComponentDtos.Count, 16);
                }

                
            }
        }
    }
}
