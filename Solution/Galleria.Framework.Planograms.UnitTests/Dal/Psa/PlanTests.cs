﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM820

// V8-30724 : A.Silva
//  Created
// V8-31002 : A.Silva
//  Added some tests for import.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Psa;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Psa
{
    [TestFixture]
    public class PlanTests
    {
        private const String ProjectName = "ProjectName";
        private const String PlanogramName = "PlanogramName";
        private const Single PlanogramSingleValue = 12.45F;
        private const Single PlanogramSingleNotTested = 10F;
        private const Int32 PlanCount = 3;
        private const PlanogramProductPlacementXType PlanogramProductPlacementXType = Planograms.Model.PlanogramProductPlacementXType.Manual;
        private const PlanogramProductPlacementYType PlanogramProductPlacementYType = Planograms.Model.PlanogramProductPlacementYType.Manual;
        private const PlanogramProductPlacementZType PlanogramProductPlacementZType = Planograms.Model.PlanogramProductPlacementZType.Manual;

        #region Expectations

        private static class Expectations
        {
            private static ExpectationDictionary _items;

            public static ExpectationDictionary Items
            {
                get
                {
                    return _items ??
                           (_items =
                            new ExpectationDictionary
                            {
                                {
                                    Expectation.PackageDalShouldFetchProjectName,
                                    "When fetching the PackageDto, the name should be that of the Project."
                                },
                                {
                                    Expectation.PackageDalShouldFetchProjectNameFromPlanogram,
                                    "When fetching the PackageDto, the name should be that of the first planogram if the name is missing in for the project."
                                },
                                {
                                    Expectation.PackageDalShouldFetchProjectNameFromDefault,
                                    "When fetching the PackageDto, the name should be defaulted if there are no planograms and it is missing for the project."
                                },
                                {
                                    Expectation.PackageDalShouldFetchIdFromPath,
                                    "When fetching the PackageDto, the Id should be the path."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchAllExistingPlanograms,
                                    "When fetching the package's PlanogramDtos, all existing should be fetched."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchName,
                                    "When fetching a PlanogramDto, the name should be correctly fetched."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchHeight,
                                    "When fetching a PlanogramDto, the height should be correctly fetched."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchWidth,
                                    "When fetching a PlanogramDto, the width should be correctly fetched."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchDepth,
                                    "When fetching a PlanogramDto, the depth should be correctly fetched."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchManualProductPlacementX,
                                    "When fetching a PlanogramDto, the Product Placement X should be set to Manual."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchManualProductPlacementY,
                                    "When fetching a PlanogramDto, the Product Placement Y should be set to Manual."
                                },
                                {
                                    Expectation.PlanogramDalShouldFetchManualProductPlacementZ,
                                    "When fetching a PlanogramDto, the Product Placement Z should be set to Manual."
                                },
                                {
                                    Expectation.PlanogramProductDalShouldFetchMaxStackFromProduct,
                                    "When fetching a PlanogramProductDto, if the product has a value it should be fetched."
                                },
                            });
                }
            }
        }

        private class ExpectationDictionary : Dictionary<Expectation, String> {}

        private enum Expectation
        {
            PackageDalShouldFetchProjectName,
            PackageDalShouldFetchProjectNameFromPlanogram,
            PackageDalShouldFetchProjectNameFromDefault,
            PackageDalShouldFetchIdFromPath,
            PlanogramDalShouldFetchAllExistingPlanograms,
            PlanogramDalShouldFetchName,
            PlanogramDalShouldFetchHeight,
            PlanogramDalShouldFetchWidth,
            PlanogramDalShouldFetchDepth,
            PlanogramDalShouldFetchManualProductPlacementX,
            PlanogramDalShouldFetchManualProductPlacementY,
            PlanogramDalShouldFetchManualProductPlacementZ,
            PlanogramProductDalShouldFetchMaxStackFromProduct,
        }

        #endregion

        #region Fields

        private static readonly String AssemblyName = typeof (DalFactory).Namespace;
        private static readonly String TestFilesPath = Path.Combine(Environment.CurrentDirectory, "TestFiles");

        #endregion

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            InitializeTestFolder();
        } 

        #region PackageDal

        [Test]
        public void PackageDal_ShouldFetchIdFromPath()
        {
            const Expectation expectation = Expectation.PackageDalShouldFetchIdFromPath;
            String name = CreateTestFile(expectation);
            PackageDto packageDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPackageDal>())
            {
                dal.LockById(name, 0, 0, false);
                packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
            }

            String actual = (String) packageDto.Id;
            Assert.AreEqual(name, actual, Expectations.Items[expectation]);
        }

        [Test]
        public void PackageDal_ShouldFetchProjectName()
        {
            const Expectation expectation = Expectation.PackageDalShouldFetchProjectName;
            String name = CreateTestFile(expectation);
            PackageDto packageDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPackageDal>())
            {
                dal.LockById(name, 0, 0, false);
                packageDto = dal.FetchById(name, new Object[] {new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>()});
            }

            String actual = packageDto.Name;
            Assert.AreEqual(ProjectName, actual, Expectations.Items[expectation]);
        }

        [Test]
        public void PackageDal_ShouldFetchProjectNameFromPlanogram()
        {
            const Expectation expectation = Expectation.PackageDalShouldFetchProjectNameFromPlanogram;
            String name = CreateTestFile(expectation);
            PackageDto packageDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPackageDal>())
            {
                dal.LockById(name, 0, 0, false);
                packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
            }

            String actual = packageDto.Name;
            Assert.AreEqual(PlanogramName + "00", actual, Expectations.Items[expectation]);
        }

        [Test]
        public void PackageDal_ShouldFetchProjectNameFromDefault()
        {
            const Expectation expectation = Expectation.PackageDalShouldFetchProjectNameFromDefault;
            String name = CreateTestFile(expectation);
            PackageDto packageDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPackageDal>())
            {
                dal.LockById(name, 0, 0, false);
                packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
            }

            String actual = packageDto.Name;
            Assert.AreEqual(Message.Planogram_DefaultPlanName, actual, Expectations.Items[expectation]);
        }

        #endregion

        #region PlanogramDal

        [Test]
        public void PlanogramDal_ShouldFetchAllExistingPlanograms()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchAllExistingPlanograms;
            String name = CreateTestFile(expectation);
            IEnumerable<PlanogramDto> planogramDtos;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] {new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>()});
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDtos = dal.FetchByPackageId(packageDto.Id);
                }
            }
            Int32 actual = planogramDtos.Count();
            Assert.AreEqual(PlanCount, actual, Expectations.Items[expectation]);
        }

        [Test]
        public void PlanogramDal_ShouldFetchName()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchName;
            String name = CreateTestFile(expectation);
            PlanogramDto planogramDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDto = dal.FetchByPackageId(packageDto.Id).FirstOrDefault();
                }
            }
            if (planogramDto == null) Assert.Inconclusive("There should be a planogram in the source file to test this.");

            Assert.AreEqual(PlanogramName + "00", planogramDto.Name, Expectations.Items[expectation]);
        }

        [Test]
        public void PlanogramDal_ShouldFetchHeight()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchHeight;
            String name = CreateTestFile(expectation);
            PlanogramDto planogramDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDto = dal.FetchByPackageId(packageDto.Id).FirstOrDefault();
                }
            }
            if (planogramDto == null) Assert.Inconclusive("There should be a planogram in the source file to test this.");

            Assert.AreEqual(PlanogramSingleValue, planogramDto.Height, Expectations.Items[expectation]);
        }

        [Test]
        public void PlanogramDal_ShouldFetchWidth()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchWidth;
            String name = CreateTestFile(expectation);
            PlanogramDto planogramDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDto = dal.FetchByPackageId(packageDto.Id).FirstOrDefault();
                }
            }
            if (planogramDto == null) Assert.Inconclusive("There should be a planogram in the source file to test this.");

            Assert.AreEqual(PlanogramSingleValue, planogramDto.Width, Expectations.Items[expectation]);
        }

        [Test]
        public void PlanogramDal_ShouldFetchDepth()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchDepth;
            String name = CreateTestFile(expectation);
            PlanogramDto planogramDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDto = dal.FetchByPackageId(packageDto.Id).FirstOrDefault();
                }
            }
            if (planogramDto == null) Assert.Inconclusive("There should be a planogram in the source file to test this.");

            Assert.AreEqual(PlanogramSingleValue, planogramDto.Depth, Expectations.Items[expectation]);
        }

        [Test]
        public void PlanogramDal_ShouldFetchManualProductPlacementX()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchManualProductPlacementX;
            String name = CreateTestFile(expectation);
            PlanogramDto planogramDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDto = dal.FetchByPackageId(packageDto.Id).FirstOrDefault();
                }
            }
            if (planogramDto == null) Assert.Inconclusive("There should be a planogram in the source file to test this.");

            Assert.AreEqual((Byte)PlanogramProductPlacementXType, planogramDto.ProductPlacementX, Expectations.Items[expectation]);
        }

        [Test]
        public void PlanogramDal_ShouldFetchManualProductPlacementY()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchManualProductPlacementY;
            String name = CreateTestFile(expectation);
            PlanogramDto planogramDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDto = dal.FetchByPackageId(packageDto.Id).FirstOrDefault();
                }
            }
            if (planogramDto == null) Assert.Inconclusive("There should be a planogram in the source file to test this.");

            Assert.AreEqual((Byte)PlanogramProductPlacementYType, planogramDto.ProductPlacementY, Expectations.Items[expectation]);
        }

        [Test]
        public void PlanogramDal_ShouldFetchManualProductPlacementZ()
        {
            const Expectation expectation = Expectation.PlanogramDalShouldFetchManualProductPlacementZ;
            String name = CreateTestFile(expectation);
            PlanogramDto planogramDto;
            using (var dalFactory = new DalFactory(new DalFactoryConfigElement(name, AssemblyName)))
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                PackageDto packageDto;
                using (var dal = dalContext.GetDal<IPackageDal>())
                {
                    dal.LockById(name, 0, 0, false);
                    packageDto = dal.FetchById(name, new Object[] { new List<PlanogramFieldMappingDto>(), new List<PlanogramMetricMappingDto>() });
                }
                using (var dal = dalContext.GetDal<IPlanogramDal>())
                {
                    planogramDto = dal.FetchByPackageId(packageDto.Id).FirstOrDefault();
                }
            }
            if (planogramDto == null) Assert.Inconclusive("There should be a planogram in the source file to test this.");

            Assert.AreEqual((Byte)PlanogramProductPlacementZType, planogramDto.ProductPlacementZ, Expectations.Items[expectation]);
        }

        #endregion

        #region Static Helper Methods

        private static void InitializeTestFolder()
        {
            if (!Directory.Exists(TestFilesPath)) Directory.CreateDirectory(TestFilesPath);
            var directoryInfo = new DirectoryInfo(TestFilesPath);
            foreach (FileInfo file in directoryInfo.GetFiles())
                file.Delete();
        }

        private static String CreateTestFile(Expectation expectation)
        {
            switch (expectation)
            {
                case Expectation.PackageDalShouldFetchProjectName:
                    return CreatePackageDalShouldFetchProjectNameFile();
                case Expectation.PackageDalShouldFetchProjectNameFromPlanogram:
                    return CreatePackageDalShouldFetchProjectNameFromPlanogramFile();
                case Expectation.PackageDalShouldFetchProjectNameFromDefault:
                    return CreatePackageDalShouldFetchProjectNameFromDefaultFile();
                case Expectation.PackageDalShouldFetchIdFromPath:
                    return CreatePackageDalShouldFetchIdFromPathFile();
                case Expectation.PlanogramDalShouldFetchAllExistingPlanograms:
                    return CreatePlanogramDalShouldFetchAllExistingPlanogramsFile();
                case Expectation.PlanogramDalShouldFetchName:
                    return CreatePlanogramDalShouldFetchNameFile();
                case Expectation.PlanogramDalShouldFetchHeight:
                    return CreatePlanogramDalShouldFetchHeightFile();
                case Expectation.PlanogramDalShouldFetchWidth:
                    return CreatePlanogramDalShouldFetchWidthFile();
                case Expectation.PlanogramDalShouldFetchDepth:
                    return CreatePlanogramDalShouldFetchDepthFile();
                case Expectation.PlanogramDalShouldFetchManualProductPlacementX:
                    return CreatePlanogramDalShouldFetchManualProductPlacementXFile();
                case Expectation.PlanogramDalShouldFetchManualProductPlacementY:
                    return CreatePlanogramDalShouldFetchManualProductPlacementYFile();
                case Expectation.PlanogramDalShouldFetchManualProductPlacementZ:
                    return CreatePlanogramDalShouldFetchManualProductPlacementZFile();
                default:
                    Assert.Fail("There was no custom file creator for the given expectation.");
                    return String.Empty;
            }
        }

        private static void WriteFileHeader(TextWriter streamWriter, String projectName = "")
        {
            streamWriter.WriteLine("PROSPACE SCHEMATIC FILE");
            streamWriter.WriteLine("; Version 2006.3.2");
            streamWriter.WriteLine("Project,{0},,0,,1,1.5,2,7,1,0,,0,1,0,0,0,0,3,1,1,0,1,0,0,0,0,2,3,1,0,1,0,0,0,0,3,3,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,0,0,0,0,0,0,0,0,0,0,,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,2,2,2,2,,0,0,0,0,0,0,0,0,,,0,", projectName);
            streamWriter.WriteLine("Project2,-1");
        }

        private static void WritePlanograms(TextWriter streamWriter, String planogramName = "", WidthHeightDepthValue size = default(WidthHeightDepthValue), Int32 planCount = 1)
        {
            if (Equals(size, default(WidthHeightDepthValue))) size = new WidthHeightDepthValue(PlanogramSingleNotTested, PlanogramSingleNotTested, PlanogramSingleNotTested);

            for (var i = 0; i < planCount; i++)
            {
                streamWriter.WriteLine(
                    "Planogram,{0}{1:00},,{2},{3},{4},16777215,1,1,49,5,24,1,16777215,1,0,0,1,-1,1,0,0,1,0,,,-1,-1,-1,-1,0,0,0,-1,-1,-1,-1,-1,-1,0,0,0,-1,-1,-1,-1,-1,-1,0,0,0,-1,-1,0,1,0,TOASTER PASTRY 4 FT SECTION,EUROPE DeCA PLANOGRAM,CLASS D STORES,HQ DeCA/MBU PLANOGRAM APPROVED BY CATEGORY MANAGER BARBARA MERRIWEATHER.,FACINGS MAY BE ADJUSTED TO ACCOMMODATE LOCAL AND REGIONAL ITEMS ( END OF FLOW). FACINGS MAY BE ADJUSTED TO MEET CUSTOMER DEMAND-CAO MUST BE INVOLVED IN THE THE PROCESS ALONG WITH STORE MANAGEMENT APPROVAL. ITEM POSITIONS MUST NOT BE CHANGED AT ANY TIME.,,,,,,,27  February 2015,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,,test.psa,0,,,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,,,,0,0,0,0,0,0,0,0,,,,0.5,12,12,0,0,0,0,0,0,1.5,1.5,1,1,0,0,0,0,0,0,0,0,,,,,,AD94EDDA-86CA-4b45-911E-222544C05994,,,,,0",
                    planogramName,i, size.Width, size.Height, size.Depth);
            }
        }

        #region PackageDalTestFiles

        private static String CreatePackageDalShouldFetchIdFromPathFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PackageDalShouldFetchIdFromPath));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter, ProjectName);
            }
            return path;
        }

        private static String CreatePackageDalShouldFetchProjectNameFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PackageDalShouldFetchProjectName));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter, ProjectName);
            }
            return path;
        }

        private static String CreatePackageDalShouldFetchProjectNameFromPlanogramFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PackageDalShouldFetchProjectNameFromPlanogram));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter);
                WritePlanograms(streamWriter, PlanogramName);
            }
            return path;
        }

        private static String CreatePackageDalShouldFetchProjectNameFromDefaultFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PackageDalShouldFetchProjectNameFromDefault));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter);
            }
            return path;
        }

        #endregion

        #region PlanogramDalTestFiles

        private static String CreatePlanogramDalShouldFetchAllExistingPlanogramsFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchAllExistingPlanograms));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName, planCount:PlanCount);
            }
            return path;
        }

        private static String CreatePlanogramDalShouldFetchNameFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchName));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName);
            }
            return path;
        }

        private static String CreatePlanogramDalShouldFetchHeightFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchHeight));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                var size = new WidthHeightDepthValue(PlanogramSingleNotTested, PlanogramSingleValue, PlanogramSingleNotTested);
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName, size);
            }
            return path;
        }

        private static String CreatePlanogramDalShouldFetchWidthFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchWidth));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                var size = new WidthHeightDepthValue(PlanogramSingleValue, PlanogramSingleNotTested, PlanogramSingleNotTested);
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName, size);
            }
            return path;
        }

        private static String CreatePlanogramDalShouldFetchDepthFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchDepth));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                var size = new WidthHeightDepthValue(PlanogramSingleNotTested, PlanogramSingleNotTested, PlanogramSingleValue);
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName, size);
            }
            return path;
        }

        private static String CreatePlanogramDalShouldFetchManualProductPlacementXFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchManualProductPlacementX));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName);
            }
            return path;
        }

        private static String CreatePlanogramDalShouldFetchManualProductPlacementYFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchManualProductPlacementY));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName);
            }
            return path;
        }

        private static String CreatePlanogramDalShouldFetchManualProductPlacementZFile()
        {
            String path = Path.Combine(TestFilesPath, String.Format("{0}.psa", Expectation.PlanogramDalShouldFetchManualProductPlacementZ));
            using (var fileStream = new FileStream(path, FileMode.Create))
            using (TextWriter streamWriter = new StreamWriter(fileStream))
            {
                WriteFileHeader(streamWriter, ProjectName);
                WritePlanograms(streamWriter, PlanogramName);
            }
            return path;
        }

        #endregion

        #endregion
    }
}