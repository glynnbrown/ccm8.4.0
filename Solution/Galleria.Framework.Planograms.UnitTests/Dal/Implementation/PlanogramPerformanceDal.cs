﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramPerformanceDal : TestBase<IPlanogramPerformanceDal,PlanogramPerformanceDto>
    {
        private List<String> _propertiesToExclude = new List<String>() { "ExtendedData" };

        internal override Planograms.Dal.Pog.SectionType SectionType
        {
            get { return Planograms.Dal.Pog.SectionType.PlanogramPerformances; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "PlanogramId";
            }
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(
                dbDalFactoryType,
                "FetchByPlanogramId",
                "PlanogramId",
                _propertiesToExclude);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _propertiesToExclude);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _propertiesToExclude);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);
        }
    }
}
