﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramComparisonFieldValueDal : TestBase<IPlanogramComparisonFieldValueDal, PlanogramComparisonFieldValueDto>
    {
        #region Constants

        private const String FetchByIdPropertyName = "PlanogramComparisonItemId";
        private const String FetchMethodName = "FetchByPlanogramComparisonItemId";

        #endregion

        #region Fields

        private static readonly List<String> PropertiesToExclude = new List<String> { "ExtendedData" };

        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramComparisonItemId(Type dalFactoryType)
        {
            TestFetchByProperty(dalFactoryType, FetchMethodName, FetchByIdPropertyName, PropertiesToExclude);
        }

        #endregion

        #region Insert

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, PropertiesToExclude);
        }

        #endregion

        #region Update

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            TestUpdate(dalFactoryType, PropertiesToExclude);
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }

        #endregion

        #region TestBase<IPlanogramComparisonFieldValueDal,PlanogramComparisonFieldValueDto> Support

        internal override SectionType SectionType { get { return SectionType.PlanogramComparisonFieldValues; } }

        internal override IEnumerable<String> RequiredIds { get { yield return FetchByIdPropertyName; } }

        #endregion
    }
}