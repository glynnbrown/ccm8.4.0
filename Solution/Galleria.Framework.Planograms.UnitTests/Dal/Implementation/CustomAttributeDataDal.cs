﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26147 : L.Ineson
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class CustomAttributeDataDal : TestBase<ICustomAttributeDataDal, CustomAttributeDataDto>
    {
        internal override Planograms.Dal.Pog.SectionType SectionType
        {
            get { return Planograms.Dal.Pog.SectionType.CustomAttributeData; }
        }

        internal override IEnumerable<string> RequiredIds
        {
            get
            {
                yield return "ParentId";
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByParentTypeParentId(Type dalFactoryType)
        {
            base.TestFetchByProperties(
                dalFactoryType,
                "FetchByParentTypeParentId",
                new List<String> { "ParentType", "ParentId" },
                new List<String> { "ExtendedData" });

        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByParentTypeParentIds(Type dalFactoryType)
        {
            Assert.Inconclusive();
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestUpsert(Type dbDalFactoryType)
        {
            //not required by the pog dal.
            if (dbDalFactoryType != typeof(Galleria.Framework.Planograms.Dal.Pog.DalFactory))
            {
                base.TestUpsert<CustomAttributeDataIsSetDto>(dbDalFactoryType, new List<String> { "ExtendedData" });
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
