﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.IO;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog;
using Galleria.Framework.Planograms.Dal.Pog.Schema;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.UnitTesting.TestBases;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [Category(Categories.PlanogramDal)]
    public abstract class TestBase<DAL, DTO> : DalImplementationTestBase<DAL, DTO>
        where DAL : IDal
        where DTO : new()
    {

        #region Fields

        private List<Type> _dbDalFactoryTypes;

        //private String _pogFileName;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TestBase()
        {
            // create our list of dal factory types to create

            //db
            _dbDalFactoryTypes = new List<Type>();
            _dbDalFactoryTypes.Add(typeof(Galleria.Framework.Planograms.Dal.Mock.DalFactory));
            _dbDalFactoryTypes.Add(typeof(Galleria.Framework.Planograms.Dal.Pog.DalFactory));
        }
        #endregion

        #region Properties
        /// <summary>
        /// A enumerable list of dal factories that will be tested
        /// </summary>
        public IEnumerable<Type> DbDalFactoryTypes
        {
            get { return _dbDalFactoryTypes; }
        }

        internal abstract SectionType SectionType { get; }

        internal abstract IEnumerable<String> RequiredIds { get; }

        #endregion

        #region Tests

        [Test]
        public void SchemaTest()
        {
            // The name to use for database templates.
            string templateName = Guid.NewGuid().ToString() + ".pog";

            // The directory in which database templates will be created.
            string templateDir = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                Galleria.Ccm.Constants.AppDataFolderName,
                "Test Files");
            if (!Directory.Exists(templateDir))
            {
                // Directory doesn't exist yet: create it.
                Directory.CreateDirectory(templateDir);
            }
            else
            {
                // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                // this just to be tidy.
                foreach (string file in Directory.GetFiles(templateDir))
                {
                    if (!Path.GetFileName(file).StartsWith(templateName))
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch
                        {
                            // No point in crying over spilt milk
                        }
                    }
                }
            }

            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(
                Path.Combine(templateDir, templateName),
                "Galleria.Framework.Planograms.Dal.Pog");

            // create the dal factory
            var dalFactory = new Galleria.Framework.Planograms.Dal.Pog.DalFactory(dalFactoryConfig);
            dalFactory.Upgrade();

            Type dtoType = typeof(DTO);
            using (GalleriaBinaryFile file = new GalleriaBinaryFile(Path.Combine(templateDir, templateName), dalFactory.DatabaseTypeName, false))
            {
                Assert.IsTrue(file.SectionExists((UInt16)SectionType), SectionType.ToString());
                foreach (PropertyInfo property in typeof(DTO).GetProperties().Where(p => p.CanWrite && p.Name != "ExtendedData"))
                {
                    String fieldConstantName = dtoType.Name.Substring(0, dtoType.Name.Length - 3) + property.Name;
                    String fieldName = (String)typeof(FieldNames).GetField(fieldConstantName).GetValue(null);
                    Assert.IsTrue(file.ItemPropertyExists((UInt16)SectionType, fieldName), property.Name);
                    Assert.AreEqual(GetItemPropertyType(property.Name, property.PropertyType), file.GetItemPropertyType((UInt16)SectionType, fieldName), property.Name);
                }
            }
        }

        private GalleriaBinaryFile.ItemPropertyType GetItemPropertyType(String propertyName, Type type)
        {
            if (type.IsGenericType)
            {
                switch (type.GetGenericArguments()[0].Name)
                {
                    case "Boolean": return GalleriaBinaryFile.ItemPropertyType.NullableBoolean;
                    case "Byte": return GalleriaBinaryFile.ItemPropertyType.NullableByte;
                    //case "NullableChar": return GalleriaBinaryFile.ItemPropertyType.NullableChar; // TODO: Learn more about surrogate pairs before implementing
                    case "DateTime": return GalleriaBinaryFile.ItemPropertyType.NullableDateTime;
                    case "Decimal": return GalleriaBinaryFile.ItemPropertyType.NullableDecimal;
                    case "Double": return GalleriaBinaryFile.ItemPropertyType.NullableDouble;
                    case "Int32": return GalleriaBinaryFile.ItemPropertyType.NullableInt32;
                    case "Int64": return GalleriaBinaryFile.ItemPropertyType.NullableInt64;
                    case "SByte": return GalleriaBinaryFile.ItemPropertyType.NullableSByte;
                    case "Int16": return GalleriaBinaryFile.ItemPropertyType.NullableInt16;
                    case "Single": return GalleriaBinaryFile.ItemPropertyType.NullableSingle;
                    case "UInt32": return GalleriaBinaryFile.ItemPropertyType.NullableUInt32;
                    case "UInt64": return GalleriaBinaryFile.ItemPropertyType.NullableUInt64;
                    case "UInt16": return GalleriaBinaryFile.ItemPropertyType.NullableUInt16;
                }
            }
            else
            {
                switch (type.Name)
                {
                    case "Boolean": return GalleriaBinaryFile.ItemPropertyType.Boolean;
                    case "Byte": return GalleriaBinaryFile.ItemPropertyType.Byte;
                    //case "Char": return GalleriaBinaryFile.ItemPropertyType.Char; // TODO: Learn more about surrogate pairs before implementing
                    case "DateTime": return GalleriaBinaryFile.ItemPropertyType.DateTime;
                    case "Decimal": return GalleriaBinaryFile.ItemPropertyType.Decimal;
                    case "Double": return GalleriaBinaryFile.ItemPropertyType.Double;
                    case "Int32": return GalleriaBinaryFile.ItemPropertyType.Int32;
                    case "Int64": return GalleriaBinaryFile.ItemPropertyType.Int64;
                    case "SByte": return GalleriaBinaryFile.ItemPropertyType.SByte;
                    case "Int16": return GalleriaBinaryFile.ItemPropertyType.Int16;
                    case "Single": return GalleriaBinaryFile.ItemPropertyType.Single;
                    case "String": return GalleriaBinaryFile.ItemPropertyType.String;
                    case "UInt32": return GalleriaBinaryFile.ItemPropertyType.UInt32;
                    case "UInt64": return GalleriaBinaryFile.ItemPropertyType.UInt64;
                    case "UInt16": return GalleriaBinaryFile.ItemPropertyType.UInt16;
                    case "Boolean[]": return GalleriaBinaryFile.ItemPropertyType.BooleanArray;
                    case "Byte[]": return GalleriaBinaryFile.ItemPropertyType.ByteArray;
                    //case "Char[]": return GalleriaBinaryFile.ItemPropertyType.CharArray; // TODO: Learn more about surrogate pairs before implementing
                    case "Decimal[]": return GalleriaBinaryFile.ItemPropertyType.DecimalArray;
                    case "Double[]": return GalleriaBinaryFile.ItemPropertyType.DoubleArray;
                    case "Int32[]": return GalleriaBinaryFile.ItemPropertyType.Int32Array;
                    case "Int64[]": return GalleriaBinaryFile.ItemPropertyType.Int64Array;
                    case "SByte[]": return GalleriaBinaryFile.ItemPropertyType.SByteArray;
                    case "Int16[]": return GalleriaBinaryFile.ItemPropertyType.Int16Array;
                    case "Single[]": return GalleriaBinaryFile.ItemPropertyType.SingleArray;
                    case "UInt32[]": return GalleriaBinaryFile.ItemPropertyType.UInt32Array;
                    case "UInt64[]": return GalleriaBinaryFile.ItemPropertyType.UInt64Array;
                    case "UInt16Array[]": return GalleriaBinaryFile.ItemPropertyType.UInt64Array;
                }
            }
            if (propertyName == "PackageId")
            {
                return GalleriaBinaryFile.ItemPropertyType.String;
            }
            if (propertyName == "Id" || RequiredIds.Contains(propertyName))
            {
                return GalleriaBinaryFile.ItemPropertyType.Int32;
            }
            else
            {
                return GalleriaBinaryFile.ItemPropertyType.NullableInt32;
            }
        }

        [Test]
        public void DtoItemTest()
        {
            foreach (PropertyInfo property in typeof(DTO).GetProperties().Where(p => p.CanWrite && p.Name != "ExtendedData"))
            {
                DtoItemTest(property.Name);
            }
        }

        private void DtoItemTest(String propertyName)
        {
            Type dtoType = typeof(DTO);
            Type dtoItemType = Assembly.GetAssembly(typeof(FieldNames)).GetType("Galleria.Framework.Planograms.Dal.Pog.Implementation.Items." +
                dtoType.Name + "Item");

            PropertyInfo dtoProperty = dtoType.GetProperty(propertyName);
            PropertyInfo dtoItemProperty = dtoItemType.GetProperty(propertyName);
            
            DTO dto = new DTO();
            dtoProperty.SetValue(dto, TestDataHelper.GetValue2(dtoProperty.PropertyType), null);
            Object dtoItem = Activator.CreateInstance(dtoItemType, dto);
            Assert.AreEqual(dtoProperty.GetValue(dto, null), dtoItemProperty.GetValue(dtoItem, null), propertyName);

            String fieldConstantName = dtoType.Name.Substring(0, dtoType.Name.Length - 3) + dtoProperty.Name;
            String fieldName = (String)typeof(FieldNames).GetField(fieldConstantName).GetValue(null);
            Object value = TestDataHelper.GetValue2(dtoProperty.PropertyType);
            if (propertyName == "PackageId") value = value.ToString();
            Item item = new Item(fieldName, value);
            dtoItem = Activator.CreateInstance(dtoItemType, item);
            Assert.AreEqual(item.Value, dtoItemProperty.GetValue(dtoItem, null), propertyName);

            dto = (DTO)dtoItemType.GetMethod("CopyDto").Invoke(dtoItem, null);
            Assert.AreEqual(dtoItemProperty.GetValue(dtoItem, null), dtoProperty.GetValue(dto, null), propertyName);
        }

        private class Item : GalleriaBinaryFile.IItem
        {
            private String _propertyName;
            private Object _value;

            public Item(String propertyName, Object value)
            {
                _propertyName = propertyName;
                _value = value;
            }

            public Object Value
            {
                get { return _value; }
            }

            #region IItem Members

            public Object GetItemPropertyValue(String itemPropertyName)
            {
                Object returnValue = null;
                if (itemPropertyName == _propertyName)
                {
                    returnValue = _value;
                }
                return returnValue;
            }

            #endregion
        }

        #endregion

        #region Misc Helpers

        /// <summary>
        /// Creates our dal factory from the provided type
        /// </summary>
        /// <param name="dalFactoryType">The type of dal factory to create</param>
        protected override IDalFactory CreateDalFactory(Type dalFactoryType)
        {
            try
            {
                // The name to use for database templates.
                string templateName = Guid.NewGuid().ToString() + ".pog";

                // The directory in which database templates will be created.
                string templateDir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    Galleria.Ccm.Constants.AppDataFolderName,
                    "Test Files");
                if (!Directory.Exists(templateDir))
                {
                    // Directory doesn't exist yet: create it.
                    Directory.CreateDirectory(templateDir);
                }
                else
                {
                    // Directory does exist, so is probably populated with databases from a previous test run.  Delete
                    // this just to be tidy.
                    foreach (string file in Directory.GetFiles(templateDir))
                    {
                        if (!Path.GetFileName(file).StartsWith(templateName))
                        {
                            try
                            {
                                File.Delete(file);
                            }
                            catch
                            {
                                // No point in crying over spilt milk
                            }
                        }
                    }
                }

                IDalFactory dalFactory = null;
                if (dalFactoryType == typeof(Galleria.Framework.Planograms.Dal.Pog.DalFactory))
                {
                    #region Pog

                    //[SA-13322] in order to reduce the amount of time taken to run the
                    // unit tests, we create 'template' database for this series
                    // of unit tests. Then, for each test, simply copy the file
                    // rather than re-creating the database each time

                    // create a dal configuration item for this factory
                    DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(
                        Path.Combine(templateDir, templateName),
                        "Galleria.Framework.Planograms.Dal.Pog");

                    // create the dal factory
                    dalFactory = new Galleria.Framework.Planograms.Dal.Pog.DalFactory(dalFactoryConfig);
                    dalFactory.Upgrade();

                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                        {
                            dal.LockById(null, 1, (Byte)PackageLockType.User, false);
                        }
                    }

                    #endregion
                }
                else if (dalFactoryType == typeof(Galleria.Framework.Planograms.Dal.Mock.DalFactory))
                {
                    // create the dal factory
                    dalFactory = new Galleria.Framework.Planograms.Dal.Mock.DalFactory();
                }

                // and return the factory
                return dalFactory;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                throw;
            }
        }

        #endregion

        #region SetUp

        [SetUp]
        public void SetUp()
        {
        }

        #endregion

        #region Teardown

        /// <summary>
        /// Called after a test has been executed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
        }

        #endregion
    }
}
