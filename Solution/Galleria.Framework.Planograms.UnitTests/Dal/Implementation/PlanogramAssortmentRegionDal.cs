﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26426 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramAssortmentRegionDal : TestBase<IPlanogramAssortmentRegionDal, PlanogramAssortmentRegionDto>
    {
        internal override Planograms.Dal.Pog.SectionType SectionType
        {
            get { return Planograms.Dal.Pog.SectionType.PlanogramAssortmentRegions; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "PlanogramAssortmentId";
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramAssortmentId(Type dalFactoryType)
        {
            base.TestFetchByProperty(
                dalFactoryType,
                "FetchByPlanogramAssortmentId",
                "PlanogramAssortmentId",
                new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
