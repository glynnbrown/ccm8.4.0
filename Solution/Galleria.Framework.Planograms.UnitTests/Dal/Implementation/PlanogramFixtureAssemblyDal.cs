﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24658 : K.Pickup
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;

using NUnit.Framework;

using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramFixtureAssemblyDal : TestBase<IPlanogramFixtureAssemblyDal, PlanogramFixtureAssemblyDto>
    {
        internal override Planograms.Dal.Pog.SectionType SectionType
        {
            get { return Planograms.Dal.Pog.SectionType.PlanogramFixtureAssemblies; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "PlanogramFixtureId";
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramFixtureId(Type dalFactoryType)
        {
            base.TestFetchByProperty(
                dalFactoryType,
                "FetchByPlanogramFixtureId",
                "PlanogramFixtureId",
                new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
