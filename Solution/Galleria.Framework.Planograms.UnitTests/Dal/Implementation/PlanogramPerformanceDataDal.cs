﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26267 : A.Kuszyk
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramPerformanceDataDal : TestBase<IPlanogramPerformanceDataDal, PlanogramPerformanceDataDto>
    {
        private List<String> _propertiesToExclude = new List<String>() { "ExtendedData" };

        internal override Planograms.Dal.Pog.SectionType SectionType
        {
            get { return Planograms.Dal.Pog.SectionType.PlanogramPerformanceDatas; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                return new List<String>()
                {
                    "PlanogramPerformanceId",
                    "PlanogramProductId"
                };
            }
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramPerformanceId(Type dbDalFactoryType)
        {
            base.TestFetchByProperty(
                dbDalFactoryType,
                "FetchByPlanogramPerformanceId",
                "PlanogramPerformanceId",
                _propertiesToExclude);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dbDalFactoryType)
        {
            base.TestInsert(dbDalFactoryType, _propertiesToExclude);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dbDalFactoryType)
        {
            base.TestUpdate(dbDalFactoryType, _propertiesToExclude);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dbDalFactoryType)
        {
            base.TestDeleteById(dbDalFactoryType);            
        }


        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestBulkInsert(Type dbDalFactoryType)
        {
            base.TestBulkInsert(dbDalFactoryType, _propertiesToExclude);
        }

        [Test]
        [TestCaseSource("DbDalFactoryTypes")]
        public void TestBulkUpdate(Type dbDalFactoryType)
        {
            throw new InconclusiveException("Not tested");
            //base.TestBulkU(dbDalFactoryType, _propertiesToExclude);
        }
    }
}
