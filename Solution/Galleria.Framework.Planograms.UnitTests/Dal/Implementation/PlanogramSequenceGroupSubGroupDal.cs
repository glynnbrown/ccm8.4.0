﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 830
// V8-32504 : A.Kuszyk
//	Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Pog;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramSequenceGroupSubGroupDal : TestBase<IPlanogramSequenceGroupSubGroupDal, PlanogramSequenceGroupSubGroupDto>
    {
        private static readonly List<String> ExcludedProperties = new List<String> { "ExtendedData" };

        #region Test Helper Methods

        internal override SectionType SectionType
        {
            get { return SectionType.PlanogramSequenceGroupSubGroups; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get { yield return "PlanogramSequenceGroupId"; }
        }

        #endregion

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramSequenceGroupId(Type dalFactoryType)
        {
            this.TestFetchByProperty(dalFactoryType, "FetchByPlanogramSequenceGroupId", "PlanogramSequenceGroupId", ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            this.TestInsert(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            this.TestUpdate(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
