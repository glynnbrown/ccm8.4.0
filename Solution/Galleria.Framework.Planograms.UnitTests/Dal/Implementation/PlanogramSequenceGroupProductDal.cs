﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramSequenceGroupProductDal : TestBase<IPlanogramSequenceGroupProductDal,PlanogramSequenceGroupProductDto>
    {
        private static readonly List<String> ExcludedProperties = new List<String> { "ExtendedData" };
        
        #region Test Helper Methods

        internal override SectionType SectionType
        {
            get { return SectionType.PlanogramSequenceGroupProducts; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get { yield return "PlanogramSequenceGroupId"; }
        }

        #endregion

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramSequenceGroupId(Type dalFactoryType)
        {
            this.TestFetchByProperty(dalFactoryType, "FetchByPlanogramSequenceGroupId", "PlanogramSequenceGroupId", ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            this.TestInsert(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            this.TestUpdate(dalFactoryType, ExcludedProperties);
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
