﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.Pog;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramComparisonResultDal : TestBase<IPlanogramComparisonResultDal, PlanogramComparisonResultDto>
    {
        #region Constants

        private const String FetchByIdPropertyName = "PlanogramComparisonId";
        private const String FetchMethodName = "FetchByPlanogramComparisonId";

        #endregion

        #region Fields

        private static readonly List<String> PropertiesToExclude = new List<String> { "ExtendedData" };

        #endregion

        #region Fetch

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramComparisonId(Type dalFactoryType)
        {
            TestFetchByProperty(dalFactoryType, FetchMethodName, FetchByIdPropertyName, PropertiesToExclude);
        }

        #endregion

        #region Insert

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            TestInsert(dalFactoryType, PropertiesToExclude);
        }

        #endregion

        #region Update

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            TestUpdate(dalFactoryType, PropertiesToExclude);
        }

        #endregion

        #region Delete

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }

        #endregion

        #region TestBase<IPlanogramComparisonItemDal,PlanogramComparisonItemDto> Support

        internal override SectionType SectionType { get { return SectionType.PlanogramComparisonResults; } }

        internal override IEnumerable<String> RequiredIds { get { yield return FetchByIdPropertyName; } }

        #endregion
    }
}