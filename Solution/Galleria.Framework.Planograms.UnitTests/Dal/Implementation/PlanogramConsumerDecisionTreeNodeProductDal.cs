﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Dal.Implementation
{
    [TestFixture]
    public class PlanogramConsumerDecisionTreeNodeProductDal : TestBase<IPlanogramConsumerDecisionTreeNodeProductDal, PlanogramConsumerDecisionTreeNodeProductDto>
    {
        internal override Planograms.Dal.Pog.SectionType SectionType
        {
            get { return Planograms.Dal.Pog.SectionType.PlanogramConsumerDecisionTreeNodeProducts; }
        }

        internal override IEnumerable<String> RequiredIds
        {
            get
            {
                yield return "PlanogramConsumerDecisionTreeNodeId";
            }
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestFetchByPlanogramConsumerDecisionTreeNodeId(Type dalFactoryType)
        {
            base.TestFetchByProperty(
                dalFactoryType,
                "FetchByPlanogramConsumerDecisionTreeNodeId",
                "PlanogramConsumerDecisionTreeNodeId",
                new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestInsert(Type dalFactoryType)
        {
            base.TestInsert(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public void TestUpdate(Type dalFactoryType)
        {
            base.TestUpdate(dalFactoryType, new List<String> { "ExtendedData" });
        }

        [Test, TestCaseSource("DbDalFactoryTypes")]
        public new void TestDeleteById(Type dalFactoryType)
        {
            base.TestDeleteById(dalFactoryType);
        }
    }
}
