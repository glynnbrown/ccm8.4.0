﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-27647 : L.Luong 
//   Created

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Security;
using Galleria.Ccm.Model;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramInventoryTests : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramInventory.NewPlanogramInventory());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewInventory()
        {
            // new inventory 
            var model = PlanogramInventory.NewPlanogramInventory();

            //check if Inventory is child
            Assert.That(model.IsChild);
            Assert.IsNotNull(model.Id);
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // save
            package = package.Save();

            //fetch
            Planograms.Model.Package packageModel = Planograms.Model.Package.FetchById(
                package.Id, 1, PackageLockType.User);
            PlanogramInventory model = packageModel.Planograms[0].Inventory;
            Assert.AreEqual(model.Id, package.Planograms[0].Inventory.Id);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // insert values to inventory
            package.Planograms[0].Inventory.MinCasePacks = 5F;
            package = package.Save();

            //check if inventory is not null
            Assert.IsNotNull(package.Planograms[0].Inventory);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // save
            package = package.Save();
            PackageDto dto;

            // fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check first value is equal
            Assert.AreEqual(1.5F, package.Planograms[0].Inventory.MinCasePacks);

            // change value
            package.Planograms[0].Inventory.MinCasePacks = 3F;
            package = package.Save();

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if value has changed
            Assert.AreEqual(3F, package.Planograms[0].Inventory.MinCasePacks);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            //save
            package = package.Save();
            PackageDto dto;

            // get dto
            var model = package.Planograms[0];
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }
            Assert.AreEqual(1, package.Planograms.Count);

            // remove planogram
            package.Planograms.Remove(model);
            package = package.Save();

            // inventory dto should be null
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            Assert.AreEqual(0, package.Planograms.Count);

        }

        #endregion

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        #endregion
    }
}
