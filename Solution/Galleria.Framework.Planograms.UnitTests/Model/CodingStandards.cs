﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Csla.Core;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class CodingStandards
    {
        #region Fields
        private List<Type> _testTypes = new List<Type>();
        #endregion

        #region Properties
        /// <summary>
        /// Returns all available dto types in the database
        /// </summary>
        public IEnumerable<Type> TestTypes
        {
            get { return _testTypes; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public CodingStandards()
        {
            foreach (Type testType in Assembly.GetAssembly(typeof(Galleria.Framework.Planograms.Constants)).GetTypes())
            {
                if (testType.Namespace != null)
                {
                    if ((testType.Namespace.Contains("Galleria.Framework.Planograms.Model")) && (!testType.IsAbstract))
                    {
                        if ((testType.GetInterfaces().Contains(typeof(ISavable))) ||
                            (testType.GetInterfaces().Contains(typeof(ICommandObject))) ||
                            (testType.GetInterfaces().Contains(typeof(IReadOnlyObject))) ||
                            (testType.GetInterfaces().Contains(typeof(IReadOnlyCollection))))
                        {
                            _testTypes.Add(testType);
                        }
                    }
                }
            }

            _testTypes.Sort(delegate(Type x, Type y)
            {
                return x.Name.CompareTo(y.Name);
            });
        }
        #endregion

        #region Methods
        /// <summary>
        /// Tests that the object has the correct authorization rules.
        /// </summary>
        //[Test, TestCaseSource("TestTypes")]
        //public void HasCorrectAuthorizationRules(Type testType)
        //{
        //    if (testType.IsGenericType) throw new InconclusiveException("Cannot test generic types");
        //    ((Galleria.Ccm.Security.DomainIdentity)Csla.ApplicationContext.User.Identity).Roles.Clear();
        //    Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.CreateObject, testType));
        //    Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.GetObject, testType));
        //    Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.EditObject, testType));
        //    Assert.IsFalse(BusinessRules.HasPermission(AuthorizationActions.DeleteObject, testType));
        //}

        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        //[SetUp]
        //public void Setup()
        //{
        //    // authenticate
        //    Galleria.Ccm.Security.DomainPrincipal.Authenticate();
        //}

        [Test, TestCaseSource("TestTypes")]
        public void MatchesDto(Type testType)
        {
            Type dtoType = Assembly.GetAssembly(typeof(Galleria.Framework.Planograms.Constants)).GetType(
                String.Format("Galleria.Framework.Planograms.Dal.DataTransferObjects.{0}Dto", testType.Name));
            if (dtoType != null)
            {
                List<String> modelObjectProperties = new List<String>();
                List<String> dtoProperties = new List<String>();
                List<String> propertyInclusionList = new List<String>() { "Id", "RowVersion", "DateCreated", "DateLastModified", "DateDeleted" };
                List<String> propertyExclusionList = new List<String>() { "OperationInProgress" , "ParentId", "ParentType", "ChildLevel"};
                foreach (PropertyInfo property in testType.GetProperties().Where(p => (p.CanWrite || propertyInclusionList.Contains(p.Name)) && (!propertyExclusionList.Contains(p.Name))))
                {
                    modelObjectProperties.Add(property.Name);
                }
                modelObjectProperties.Add("ExtendedData");
                PropertyInfo parentProperty = testType.GetProperty(
                    "Parent",
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
                if (parentProperty != null)
                {
                    modelObjectProperties.Add(String.Format("{0}Id", parentProperty.PropertyType.Name));
                }
                modelObjectProperties = modelObjectProperties.OrderBy(p => p).ToList();
                foreach (PropertyInfo property in dtoType.GetProperties().Where(p => (p.CanWrite || propertyInclusionList.Contains(p.Name)) && (!propertyExclusionList.Contains(p.Name))).OrderBy(p => p.Name))
                {
                    dtoProperties.Add(property.Name);
                }
                if (dtoProperties.Count != modelObjectProperties.Count)
                {
                    for (Int32 i = 0; i < Math.Max(dtoProperties.Count, modelObjectProperties.Count); i++)
                    {
                        String dtoProperty = "";
                        String modelObjectProperty = "";
                        if (i < dtoProperties.Count)
                        {
                            dtoProperty = dtoProperties[i];
                        }
                        if (i < modelObjectProperties.Count)
                        {
                            modelObjectProperty = modelObjectProperties[i];
                        }
                        Debug.WriteLine(String.Format("{0}\t{1}", dtoProperty, modelObjectProperty));
                    }
                }
                else
                {
                    Assert.AreEqual(dtoProperties.Count, modelObjectProperties.Count);
                    for (Int32 i = 0; i < dtoProperties.Count; i++)
                    {
                        Assert.AreEqual(dtoProperties[i], modelObjectProperties[i]);
                    }
                }
            }
        }

        //[Test, TestCaseSource("TestTypes")]
        //public void MatchesCcm(Type testType)
        //{
        //    Type dtoType = Assembly.GetAssembly(typeof(Galleria.Framework.Planograms.Constants)).GetType(
        //        String.Format("Galleria.Framework.Planograms.Dal.DataTransferObjects.{0}Dto", testType.Name));
        //    if (dtoType != null)
        //    {
        //        Type ccmType = Assembly.GetAssembly(typeof(Galleria.Ccm.Model.AnnotationType)).GetType(
        //            String.Format("Galleria.Ccm.Model.{0}", testType.Name));
        //        if (ccmType != null)
        //        {
        //            List<String> modelObjectProperties = new List<String>();
        //            List<String> ccmProperties = new List<String>();

        //            foreach (PropertyInfo property in testType.GetProperties().Where(p => !(p.Name.EndsWith("Id")) && p.CanWrite))
        //            {
        //                modelObjectProperties.Add(property.Name);
        //            }
        //            modelObjectProperties = modelObjectProperties.OrderBy(p => p).ToList();
        //            foreach (PropertyInfo property in ccmType.GetProperties().Where(
        //                p => (!p.Name.EndsWith("Id")) && p.Name != "UniqueContentReference" && (!p.Name.StartsWith("Linked")) && p.CanWrite).OrderBy(p => p.Name))
        //            {
        //                ccmProperties.Add(property.Name);
        //            }
        //            if (ccmProperties.Count != modelObjectProperties.Count)
        //            {
        //                for (Int32 i = 0; i < Math.Max(ccmProperties.Count, modelObjectProperties.Count); i++)
        //                {
        //                    String ccmProperty = "";
        //                    String modelObjectProperty = "";
        //                    if (i < ccmProperties.Count)
        //                    {
        //                        ccmProperty = ccmProperties[i];
        //                    }
        //                    if (i < modelObjectProperties.Count)
        //                    {
        //                        modelObjectProperty = modelObjectProperties[i];
        //                    }
        //                    Debug.WriteLine(String.Format("{0}\t{1}", ccmProperty, modelObjectProperty));
        //                }
        //            }
        //            Assert.AreEqual(ccmProperties.Count, modelObjectProperties.Count);
        //            for (Int32 i = 0; i < ccmProperties.Count; i++)
        //            {
        //                Assert.AreEqual(ccmProperties[i], modelObjectProperties[i]);
        //            }
        //        }
        //    }
        //}
        #endregion
    }
}
