﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created
// V8-29072 : A.Kuszyk
//  Added test for LoadFrom.
// V8-29105 : A.Silva
//      Added unit testing for GetSequenceGroup.
// V8-29309 : A.Silva
//      Added ApplyFromBlocking_WhenCombinedShelves_ShouldAddProductsInBlockingGroup test.

#endregion
#region Version History: CCM 820
// V8-30983 : D.Pleasance
//      Added ApplyFromBlocking_ShouldAddOverlappingProductsInBlockingGroup, ApplyFromBlocking_ShouldIgnoreCarparkProducts.
// V8-31172 : A.Kuszyk
//  Added tests for a variety of ApplyFromBlocking and ApplyFromMerchandisingGroup scenarios, including multi-sited products.
#endregion

#region Version History: CCM830

// V8-32327 : A.Silva
//  Removed call to Mercchandising Group obsolete method ResequenceSequenceValuesIncludingCombinedGroupings().

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using System.Collections.Generic;
using Galleria.Framework.Helpers;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramSequenceTests : TestBase
    {
        private Package _package;
        private PlanogramSequence _testModel;
        private Planogram _planogram;

        [SetUp]
        public void TestSetup()
        {
            _package = "Test".CreatePackage();
            _planogram = _package.AddPlanogram();
            _testModel = _planogram.Sequence;
        }

        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(_testModel);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewPlanogramSequence()
        {
            Assert.IsTrue(_testModel.IsNew);
            Assert.IsTrue(_testModel.IsChild);
        }

        [Test]
        public void Fetch()
        {
            _package = _package.Save();
            _testModel = _package.Planograms[0].Sequence;

            PlanogramSequenceDto dto = FetchDtoByModel(_testModel);

            Assert.AreEqual(_testModel.Id, dto.Id);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            // Already tested when testing Fetch.
        }

        [Test]
        public void DataAccess_Update()
        {
            Assert.Inconclusive("Cannot test properly as there are no direct properties to update.");
        }

        [Test]
        public void DataAcces_Delete()
        {
            Package package = _package.Save();
            _planogram = package.Planograms[0];
            _testModel = _planogram.Sequence;
            PlanogramSequenceDto dto = FetchDtoByModel(_testModel);
            if (dto == null) Assert.Inconclusive("Should be able to save and fetch a dto to test Delete.");

            package.Planograms.Remove(_planogram);
            package.Save();

            Assert.Throws<DtoDoesNotExistException>(() => FetchDtoByModel(_testModel));
        }

        #endregion

        #region Misc Methods

        [Test]
        public void LoadFrom_CopiesAllData()
        {
            // Build source.
            PlanogramSequence source = PlanogramSequence.NewPlanogramSequence();
            for (Int32 i = 0; i < 10; i++)
            {
                var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup();
                PlanogramSequenceGroup group = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                group.SubGroups.Add(subGroup);
                for (Int32 j = 0; j < 10; j++)
                {
                    PlanogramSequenceGroupProduct groupProduct =
                        PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
                    groupProduct.SequenceNumber = j;
                    groupProduct.Gtin = String.Format("{0}{1}", i, j);
                    groupProduct.PlanogramSequenceGroupSubGroupId = subGroup.Id;
                    group.Products.Add(groupProduct);
                }
                source.Groups.Add(group);
            }

            // Prepare target
            PlanogramSequence target = "".CreatePackage().AddPlanogram().Sequence;
            for (Int32 i = 11; i < 13; i++)
            {
                PlanogramSequenceGroup group = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                for (Int32 j = 11; j < 15; j++)
                {
                    PlanogramSequenceGroupProduct groupProduct =
                        PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
                    groupProduct.SequenceNumber = j;
                    groupProduct.Gtin = String.Format("{0}{1}", i, j);
                    group.Products.Add(groupProduct);
                }
                target.Groups.Add(group);
            }

            target.LoadFrom(source);

            Assert.AreEqual(source.Groups.Count, target.Groups.Count);
            Int32 groupIndex = 0;
            foreach (PlanogramSequenceGroup group in source.Groups)
            {
                Assert.That(group.SubGroups, Has.Count.EqualTo(1));
                Int32 productIndex = 0;
                foreach (PlanogramSequenceGroupProduct groupProduct in group.Products)
                {
                    Assert.That(groupProduct.PlanogramSequenceGroupSubGroupId, Is.Not.Null);
                    Assert.AreEqual(groupProduct.Gtin, source.Groups[groupIndex].Products[productIndex].Gtin);
                    Assert.AreEqual(groupProduct.SequenceNumber,
                        source.Groups[groupIndex].Products[productIndex].SequenceNumber);
                    productIndex++;
                }
                groupIndex++;
            }
        }

        [Test]
        public void GetSequenceGroup_WhenGtinInGroup_ShouldReturnGroup()
        {
            const String expectation = "The containing planogram sequence group should be returned.";
            PlanogramSequenceGroup expected = _planogram.AddSequenceGroups(5).Skip(2).First();
            foreach (PlanogramSequenceGroup sequenceGroup in _testModel.Groups)
            {
                sequenceGroup.Products.Add(
                    PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(_planogram.AddProduct()));
            }
            String productGtin = expected.Products.First().Gtin;

            PlanogramSequenceGroup actual = _testModel.GetSequenceGroup(productGtin);

            Assert.AreEqual(expected, actual, expectation);
        }
        
        #endregion

        #region ApplyFromBlocking
        [Test]
        public void ApplyFromBlocking_WhenCombinedShelves_ShouldAddProductsInBlockingGroup()
        {
            const String expectation =
                "ApplyFromBlocking should get all and only the the products in the blocking group.";
            CreateApplyFromBlockingTestPlanogram();
            PlanogramSequenceGroupProductList expected =
                PlanogramSequenceGroupProductList.NewPlanogramSequenceGroupProductList();
            foreach (PlanogramProduct product in _planogram.Products)
                expected.Add(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(product));

            _testModel.ApplyFromBlocking(_planogram.Blocking.First());

            PlanogramSequenceGroupProductList actual = _testModel.Groups.First().Products;
            CollectionAssert.AreEquivalent(expected.Select(o => o.Gtin).ToList(), actual.Select(o => o.Gtin).ToList(),
                expectation);
        }

        [Test]
        public void ApplyFromBlocking_ShouldAddOverlappingProductsInBlockingGroup(
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)]
            PlanogramSubComponentXMerchStrategyType merchStrategyX)
        {
            const String expectation =
                "ApplyFromBlocking should get all and only the the products in the blocking group.";

            PlanogramFixtureItem aBayViewModel = _planogram.AddFixtureItem();
            PlanogramFixtureComponent aShelf = aBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 3; i++)
            {
                // all products should be assigned to the blocking group even though they overlap
                aShelf.AddPosition(aBayViewModel, _planogram.AddProduct(new DataStructures.WidthHeightDepthValue(100, 20, 20)));
            }

            aShelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = merchStrategyX;

            using (PlanogramMerchandisingGroupList merchandisingGroups = _planogram.GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchandisingGroups)
                {
                    merchandisingGroup.Process();
                    merchandisingGroup.ApplyEdit();
                }
            }

            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            _planogram.Blocking.Add(blocking);
            blocking.Dividers.Add(0.5F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramSequenceGroupProductList expected =
                PlanogramSequenceGroupProductList.NewPlanogramSequenceGroupProductList();
            foreach (PlanogramProduct product in _planogram.Products)
                expected.Add(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(product));

            _testModel.ApplyFromBlocking(_planogram.Blocking.First());

            PlanogramSequenceGroupProductList actual = _testModel.Groups.First().Products;
            CollectionAssert.AreEquivalent(expected.Select(o => o.Gtin).ToList(), actual.Select(o => o.Gtin).ToList(),
                expectation);
        }

        [Test]
        public void ApplyFromBlocking_ShouldIgnoreCarparkProducts()
        {
            const String expectation =
                "ApplyFromBlocking should get all and only the the products in the blocking group.";

            PlanogramFixtureItem aBayViewModel = _planogram.AddFixtureItem();
            PlanogramFixtureComponent aShelf = aBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent carParkShelfAbove = aBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf, new DataStructures.PointValue(0, 210, 0));
            PlanogramFixtureComponent carParkShelfLeft = aBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf, new DataStructures.PointValue(-200, 0, 0));
            PlanogramFixtureComponent carParkShelfRight = aBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf, new DataStructures.PointValue(200, 0, 0));
            List<PlanogramProduct> planogramProducts = new List<PlanogramProduct>();

            for (int i = 0; i < 3; i++)
            {
                PlanogramProduct planogramProduct = _planogram.AddProduct(new DataStructures.WidthHeightDepthValue(100, 20, 20));
                planogramProducts.Add(planogramProduct);

                // all products should be assigned to the blocking group even though they overlap
                aShelf.AddPosition(aBayViewModel, planogramProduct);

                // all products on carpark should be ignored
                carParkShelfAbove.AddPosition(aBayViewModel, _planogram.AddProduct(new DataStructures.WidthHeightDepthValue(20, 20, 20)));
                carParkShelfLeft.AddPosition(aBayViewModel, _planogram.AddProduct(new DataStructures.WidthHeightDepthValue(20, 20, 20)));
                carParkShelfRight.AddPosition(aBayViewModel, _planogram.AddProduct(new DataStructures.WidthHeightDepthValue(20, 20, 20)));
            }

            using (PlanogramMerchandisingGroupList merchandisingGroups = _planogram.GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchandisingGroups)
                {
                    merchandisingGroup.Process();
                    merchandisingGroup.ApplyEdit();
                }
            }

            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            _planogram.Blocking.Add(blocking);
            blocking.Dividers.Add(0.5F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramSequenceGroupProductList expected =
                PlanogramSequenceGroupProductList.NewPlanogramSequenceGroupProductList();
            foreach (PlanogramProduct product in planogramProducts)
                expected.Add(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(product));

            _testModel.ApplyFromBlocking(_planogram.Blocking.First());

            PlanogramSequenceGroupProductList actual = PlanogramSequenceGroupProductList.NewPlanogramSequenceGroupProductList();

            foreach (PlanogramSequenceGroup planogramSequenceGroup in _testModel.Groups)
            {
                actual.AddRange(planogramSequenceGroup.Products);
            }

            CollectionAssert.AreEquivalent(expected.Select(o => o.Gtin).ToList(), actual.Select(o => o.Gtin).ToList(),
                expectation);
        }         

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffRightOfPlan_ShouldBeSequenced([Values(0f,94f)] Single y)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct((new DataStructures.WidthHeightDepthValue(10,10,10)));
            var position = shelf.AddPosition(bay, product);
            position.X = 240;
            position.Y = y;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffLeftOfPlan_ShouldBeSequenced([Values(0f, 94f)] Single y)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct();
            var position = shelf.AddPosition(bay, product);
            position.X = -240;
            position.Y = y;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffTopOfPlan_ShouldBeSequenced([Values(0f,54f)] Single x)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct();
            var position = shelf.AddPosition(bay, product);
            position.X = x;
            position.Y = 400;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffBottomOfPlan_ShouldBeSequenced([Values(0f, 54f)] Single x)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct();
            var position = shelf.AddPosition(bay, product);
            position.X = x;
            position.Y = -400;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffTopRightOfPlan_ShouldBeSequenced()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct();
            var position = shelf.AddPosition(bay, product);
            position.X = 400;
            position.Y = 400;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffTopLeftOfPlan_ShouldBeSequenced()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct();
            var position = shelf.AddPosition(bay, product);
            position.X = -400;
            position.Y = 400;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffBottomLeftOfPlan_ShouldBeSequenced()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct();
            var position = shelf.AddPosition(bay, product);
            position.X = -400;
            position.Y = -400;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_WhenProductsAreOffBottomRightOfPlan_ShouldBeSequenced()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            var product = plan.AddProduct();
            var position = shelf.AddPosition(bay, product);
            position.X = 400;
            position.Y = -400;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var expectedProductColour = plan.Blocking.First().Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup().Colour;
            var actualProductColour = plan.Sequence.Groups.First(g => g.Products.Select(p => p.Gtin).Contains(product.Gtin)).Colour;
            Assert.AreEqual(expectedProductColour, actualProductColour);
        }

        [Test]
        public void ApplyFromBlocking_MultisitedProductsAreSequencedOncePerGroup()
        {
            const Int32 numberOfShelves = 4;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            for (Int32 i = 0; i < numberOfShelves; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, i * (bay.GetPlanogramFixture().Height / (Single)numberOfShelves), 0));
                shelf.AddPosition(bay, product);
                shelf.AddPosition(bay, product);
            }
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.75f, PlanogramBlockingDividerType.Horizontal);
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0f)).Colour = 0;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.25f)).Colour = 1;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.5f)).Colour = 2;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.75f)).Colour = 3;

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            Assert.AreEqual(4, plan.Sequence.Groups.Count);
            foreach (var sequenceGroup in plan.Sequence.Groups)
            {
                var expected = new List<String> { product.Gtin };
                var actual = sequenceGroup.Products.Select(p => p.Gtin).ToList();
                CollectionAssert.AreEqual(expected, actual);
            }
        }

        [Test]
        public void ApplyFromBlocking_MultisitedProductsHaveCorrectPositionSequenceNumberAndColours()
        {
            const Int32 numberOfShelves = 4;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            var expectedColoursByPosition = new Dictionary<PlanogramPosition, Int32?>();
            var expectedNumbersByPosition = new Dictionary<PlanogramPosition, Int32?>();
            for (Int32 i = 0; i < numberOfShelves; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, i * (bay.GetPlanogramFixture().Height / (Single)numberOfShelves), 0));
                var position1 = shelf.AddPosition(bay, product);
                var position2 = shelf.AddPosition(bay, product);
                expectedColoursByPosition.Add(position1, i);
                expectedNumbersByPosition.Add(position1, 1);
                expectedColoursByPosition.Add(position2, i);
                expectedNumbersByPosition.Add(position2, 1);
            }
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.75f, PlanogramBlockingDividerType.Horizontal);
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0f)).Colour = 0;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.25f)).Colour = 1;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.5f)).Colour = 2;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.75f)).Colour = 3;

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            foreach (var position in plan.Positions)
            {
                Assert.AreEqual(expectedColoursByPosition[position], position.SequenceColour);
                Assert.AreEqual(expectedNumbersByPosition[position], position.SequenceNumber);
            }
        }

        #endregion

        #region ApplyFromMerchandisingGroups

        [Test]
        public void ApplyFromMerchandisingGroups_PreservesSubGroups()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            using (var mgs = plan.GetMerchandisingGroups())
            {
                plan.Sequence.ApplyFromMerchandisingGroups(mgs);
                var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(plan.Sequence.Groups[0].Products);
                plan.Sequence.Groups[0].SubGroups.Add(subGroup);
                String expectedName = subGroup.Name;
                var expectedAlignment = subGroup.Alignment;
                var expectedProductGtins = subGroup.EnumerateProducts().Select(p => p.Gtin).ToList();

                plan.Sequence.ApplyFromMerchandisingGroups(mgs);

                Assert.That(plan.Sequence.Groups, Has.Count.EqualTo(1));
                Assert.That(plan.Sequence.Groups[0].SubGroups, Has.Count.EqualTo(1));
                var newSubGroup = plan.Sequence.Groups[0].SubGroups[0];
                Assert.That(newSubGroup.Name, Is.EqualTo(expectedName));
                Assert.That(newSubGroup.Alignment, Is.EqualTo(expectedAlignment));
                Assert.That(newSubGroup.EnumerateProducts().Select(p => p.Gtin).ToList(), Is.EquivalentTo(expectedProductGtins));
            }
        }

        [Test]
        public void ApplyFromMerchandisingGroups_GroupsProductsByComponent()
        {
            const Int32 numberOfShelves = 4;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var coloursByGtin = new Dictionary<String, Int32>();
            for (Int32 i = 0; i < numberOfShelves; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,i * (bay.GetPlanogramFixture().Height / (Single)numberOfShelves),0));
                SetFillColour(shelf.GetPlanogramComponent().SubComponents.First(), i);
                var product = plan.AddProduct();
                shelf.AddPosition(bay, product);
                coloursByGtin.Add(product.Gtin, i);
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                plan.Sequence.ApplyFromMerchandisingGroups(merchGroups); 
            }

            foreach (var sequenceGroup in plan.Sequence.Groups)
            {
                foreach (var sequenceGroupProduct in sequenceGroup.Products)
                {
                    Assert.AreEqual(sequenceGroup.Colour, coloursByGtin[sequenceGroupProduct.Gtin]);
                }
            }
        }

        [Test]
        public void ApplyFromMerchandisingGroups_MultisitedProductsAreSequencedOncePerGroup()
        {
            const Int32 numberOfShelves = 4;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            for (Int32 i = 0; i < numberOfShelves; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, i * (bay.GetPlanogramFixture().Height / (Single)numberOfShelves), 0));
                SetFillColour(shelf.GetPlanogramComponent().SubComponents.First(), i);
                shelf.AddPosition(bay, product);
                shelf.AddPosition(bay, product);
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                plan.Sequence.ApplyFromMerchandisingGroups(merchGroups);
            }

            foreach (var sequenceGroup in plan.Sequence.Groups)
            {
                var expected = new List<String> { product.Gtin };
                var actual = sequenceGroup.Products.Select(p => p.Gtin).ToList();
                CollectionAssert.AreEqual(expected, actual);
            }
        }

        [Test]
        public void ApplyFromMerchandisingGroups_MultisitedProductsHaveCorrectPositionSequenceNumbersAndColours()
        {
            const Int32 numberOfShelves = 4;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            var expectedColoursByPosition = new Dictionary<PlanogramPosition, Int32?>();
            var expectedNumbersByPosition = new Dictionary<PlanogramPosition, Int32?>();
            for (Int32 i = 0; i < numberOfShelves; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, i * (bay.GetPlanogramFixture().Height / (Single)numberOfShelves), 0));
                SetFillColour(shelf.GetPlanogramComponent().SubComponents.First(), i);
                var position1 = shelf.AddPosition(bay, product);
                var position2 = shelf.AddPosition(bay, product);
                expectedColoursByPosition.Add(position1, i);
                expectedNumbersByPosition.Add(position1, 1);
                expectedColoursByPosition.Add(position2, i);
                expectedNumbersByPosition.Add(position2, 1);
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                plan.Sequence.ApplyFromMerchandisingGroups(merchGroups);
            }

            foreach (var position in plan.Positions)
            {
                Assert.AreEqual(expectedColoursByPosition[position], position.SequenceColour);
                Assert.AreEqual(expectedNumbersByPosition[position], position.SequenceNumber);
            }
        }

        [Test]
        public void ApplyFromMerchandisingGroups_MultisitedProductsAreSequencedCorrectly()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var product1 = plan.AddProduct();
            var product2 = plan.AddProduct();
            var expectedColoursByPosition = new Dictionary<PlanogramPosition, Int32?>();
            var expectedNumbersByPosition = new Dictionary<PlanogramPosition, Int32?>();
            
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            SetFillColour(shelf1.GetPlanogramComponent().SubComponents.First(), 0);
            var shelf1Prod1Pos1 = shelf1.AddPosition(bay, product1);
            var shelf1Prod2Pos1 = shelf1.AddPosition(bay, product2);
            var shelf1Prod2Pos2 = shelf1.AddPosition(bay, product2);
            expectedNumbersByPosition.Add(shelf1Prod1Pos1, 1);
            expectedNumbersByPosition.Add(shelf1Prod2Pos1, 2);
            expectedNumbersByPosition.Add(shelf1Prod2Pos2, 2);
            expectedColoursByPosition.Add(shelf1Prod1Pos1, 0);
            expectedColoursByPosition.Add(shelf1Prod2Pos1, 0);
            expectedColoursByPosition.Add(shelf1Prod2Pos2, 0);

            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            SetFillColour(shelf2.GetPlanogramComponent().SubComponents.First(), 1);
            var shelf2Prod2Pos1 = shelf2.AddPosition(bay, product2);
            var shelf2Prod2Pos2 = shelf2.AddPosition(bay, product2);
            var shelf2Prod1Pos1 = shelf2.AddPosition(bay, product1);
            expectedNumbersByPosition.Add(shelf2Prod2Pos1, 1);
            expectedNumbersByPosition.Add(shelf2Prod2Pos2, 1);
            expectedNumbersByPosition.Add(shelf2Prod1Pos1, 2);
            expectedColoursByPosition.Add(shelf2Prod1Pos1, 1);
            expectedColoursByPosition.Add(shelf2Prod2Pos1, 1);
            expectedColoursByPosition.Add(shelf2Prod2Pos2, 1);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                plan.Sequence.ApplyFromMerchandisingGroups(merchGroups);
            }

            foreach (var position in plan.Positions)
            {
                Assert.AreEqual(expectedColoursByPosition[position], position.SequenceColour);
                Assert.AreEqual(expectedNumbersByPosition[position], position.SequenceNumber);
            }
        }

        #endregion

        #region GetMaxIntersectingBlockingGroupForPosition

        [Test]
        public void GetMaxIntersectingBlockingGroupForPosition_GetsCorrectBlockForTopDownComponents()
        {
            /*       ___________
             *      |           |
             *      |           |               Expecting the "top" block to be spaced "behind" the "bottom" block.
             *      |           |
             *      |+=========+|
             *   -~-||-~-~-~-~-||-~ Divider
             *      ||_________||
             *       +=========+    (Chest)
             * 
             */

            var productSize = new WidthHeightDepthValue(10, 15, 5);
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Backboard,new PointValue(0,0,-1));
            var chest = bay1.AddFixtureComponent(PlanogramComponentType.Chest);
            chest.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            chest.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
            var blocking = plan.AddBlocking();
            Single dividerY = 0.167f;
            blocking.Dividers.Add(0f, dividerY, PlanogramBlockingDividerType.Horizontal);
            var locationsByGroup = blocking.Locations.ToLookup(l => l.GetPlanogramBlockingGroup());
            var frontBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var backBlock = blocking.Locations.First(l => l.Y.EqualTo(0.167f)).GetPlanogramBlockingGroup();
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            var locationSpacesByLocation = blocking.GetLocationSpacesByLocation(planWidth, planHeight);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var chestMerchGroup = merchGroups.First();
                var frontPosition = chestMerchGroup.InsertPositionPlacement(plan.AddProduct(productSize));
                var backPosition = chestMerchGroup.InsertPositionPlacement(plan.AddProduct(productSize), frontPosition, PlanogramPositionAnchorDirection.Behind);
                chestMerchGroup.Process();
                chestMerchGroup.ApplyEdit();
                plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

                //plan.Parent.SaveAs(1, @"C:\users\usr145\before.pog");

                var frontPositionBlock = plan.Sequence.GetMaxIntersectingBlockingGroupForPosition(
                    frontPosition,
                    locationsByGroup,
                    locationSpacesByLocation,
                    planWidth,
                    planHeight,
                    planHeightOffset);
                var backPositionBlock = plan.Sequence.GetMaxIntersectingBlockingGroupForPosition(
                    backPosition,
                    locationsByGroup,
                    locationSpacesByLocation,
                    planWidth,
                    planHeight,
                    planHeightOffset);

                Assert.That(frontPositionBlock, Is.SameAs(frontBlock));
                Assert.That(backPositionBlock, Is.SameAs(backBlock));
            }
        }

        #endregion

        #region Test Helper Methods

        private void SetFillColour(PlanogramSubComponent subComp, Int32 colour)
        {
            subComp.FillColourBack = colour;
            subComp.FillColourBottom = colour;
            subComp.FillColourTop = colour;
            subComp.FillColourFront = colour;
            subComp.FillColourLeft = colour;
            subComp.FillColourRight = colour;
        }

        private PlanogramSequenceDto FetchDtoByModel(PlanogramSequence model)
        {
            PlanogramSequenceDto dto;
            using (IDalContext dalContext = DalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramSequenceDal>())
                dto = dal.FetchByPlanogramId(model.Parent.Id);
            return dto;
        }

        private void CreateApplyFromBlockingTestPlanogram()
        {
            PlanogramFixtureItem aBayViewModel = _planogram.AddFixtureItem();
            PlanogramFixtureItem bBayViewModel = _planogram.AddFixtureItem();
            PlanogramFixtureItem cBayViewModel = _planogram.AddFixtureItem();
            PlanogramFixtureComponent aShelf = aBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent bShelf = bBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent cShelf = cBayViewModel.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 3; i++)
            {
                aShelf.AddPosition(aBayViewModel);
                bShelf.AddPosition(bBayViewModel);
                cShelf.AddPosition(cBayViewModel);
            }
            aShelf.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            bShelf.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            cShelf.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            using (PlanogramMerchandisingGroupList merchandisingGroups = _planogram.GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchandisingGroups)
                {
                    merchandisingGroup.Process();
                    merchandisingGroup.ApplyEdit();
                }
            }
           
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            _planogram.Blocking.Add(blocking);
            blocking.Dividers.Add(0.5F, 0.5F, PlanogramBlockingDividerType.Horizontal);
        }
        
        #endregion
    }
}