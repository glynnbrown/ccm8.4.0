﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.UnitTests.Helpers;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramBlockingLocationTests : TestBase<PlanogramBlockingLocation, PlanogramBlockingLocationDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        [Test]
        public void Property_CalculatedProperties_UpdatedOnDividerMove()
        {
            PlanogramBlocking item = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);

            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlockingLocation loc1 = item.Locations[0];


            Boolean allocatedSpacePropertyChangedFired = false;

            loc1.PropertyChanged +=
                (s, e) =>
                {
                    if (e.PropertyName == PlanogramBlockingLocation.SpacePercentageProperty.Name) allocatedSpacePropertyChangedFired = true;
                };

            //move the first divider
            item.Dividers[0].MoveTo(0.3F);
            Assert.IsTrue(allocatedSpacePropertyChangedFired, "allocated space Property change not fired");
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Get Blocking methods
        [Test]
        public void ExpandToFill_ChangesCorrectBorders_WhenBoundedByDividers()
        {
            var planogram = Planogram.NewPlanogram();
            
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var expectedLeftDivider = TestDataHelper.CreateBlockingDivider(1, 1, 9, PlanogramBlockingDividerType.Vertical,1, blocking.Dividers);
            var expectedRightDivider = TestDataHelper.CreateBlockingDivider(10, 1, 9, PlanogramBlockingDividerType.Vertical,1, blocking.Dividers);
            var expectedTopDivider = TestDataHelper.CreateBlockingDivider(1, 10, 9, PlanogramBlockingDividerType.Horizontal,2, blocking.Dividers);
            var expectedBottomDivider = TestDataHelper.CreateBlockingDivider(1, 1, 9, PlanogramBlockingDividerType.Horizontal,2, blocking.Dividers);
            var middleVerticalDivider = TestDataHelper.CreateBlockingDivider(5, 1, 9, PlanogramBlockingDividerType.Vertical, 3,blocking.Dividers);
            var middleLeftDivider = TestDataHelper.CreateBlockingDivider(1, 5, 4.5f, PlanogramBlockingDividerType.Horizontal, 4,blocking.Dividers);
            var middleRightDivider = TestDataHelper.CreateBlockingDivider(5, 5, 4.5f, PlanogramBlockingDividerType.Horizontal, 4,blocking.Dividers);

            var location = TestDataHelper.CreateBlockingLocation(expectedLeftDivider,middleVerticalDivider,middleLeftDivider,expectedBottomDivider,blocking.Locations);
            var expansionLocations = new List<PlanogramBlockingLocation>() 
            {
                location,
                TestDataHelper.CreateBlockingLocation(expectedLeftDivider,middleVerticalDivider,expectedTopDivider,middleLeftDivider, blocking.Locations),
                TestDataHelper.CreateBlockingLocation(middleVerticalDivider,expectedRightDivider,expectedTopDivider,middleRightDivider, blocking.Locations),
                TestDataHelper.CreateBlockingLocation(middleVerticalDivider,expectedRightDivider,middleRightDivider,expectedBottomDivider, blocking.Locations)
            };

            location.ExpandToFill(expansionLocations);

            Assert.AreEqual(expectedLeftDivider.Id, location.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(expectedRightDivider.Id, location.PlanogramBlockingDividerRightId);
            Assert.AreEqual(expectedTopDivider.Id, location.PlanogramBlockingDividerTopId);
            Assert.AreEqual(expectedBottomDivider.Id, location.PlanogramBlockingDividerBottomId);
        }

        [Test]
        public void ExpandToFill_ChangesCorrectBorders_WhenBoundedByNull()
        {
            //   _______________
            //  |       |       |
            //  |   D  (1)  C   |
            //  |_(2)___|__(3)__|
            //  |       |       |
            //  |   A  (1)  B   |
            //  |_______|_______|
            //     | 
            //     +--> Location to expand 
            
            // Create Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Create Dividers
            var divider1 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);

            // Create Locations
            var locationA = TestDataHelper.CreateBlockingLocation(null, divider1, divider2, null, blocking.Locations);
            var locationB = TestDataHelper.CreateBlockingLocation(divider1, null, divider3, null, blocking.Locations);
            var locationC = TestDataHelper.CreateBlockingLocation(divider1, null, null, divider3, blocking.Locations);
            var locationD = TestDataHelper.CreateBlockingLocation(null, divider1, null, divider2, blocking.Locations);

            locationA.ExpandToFill(new PlanogramBlockingLocation[] { locationB, locationC, locationD });

            Assert.IsNull(locationA.PlanogramBlockingDividerLeftId);
            Assert.IsNull(locationA.PlanogramBlockingDividerRightId);
            Assert.IsNull(locationA.PlanogramBlockingDividerTopId);
            Assert.IsNull(locationA.PlanogramBlockingDividerBottomId);
        }

        private PlanogramFixtureItem _fixtureItem;
        private Planogram _planogram;
        private PlanogramFixture _fixture;
        private const Single _fixtureSize = 10f;

        private void GetFixtureComponentsSetup()
        {
            _planogram = Planogram.NewPlanogram();
            _planogram.Height = _fixtureSize;
            _fixture = PlanogramFixture.NewPlanogramFixture();
            _fixture.Width = _fixtureSize;
            _fixture.Height = _fixtureSize;
            _planogram.Fixtures.Add(_fixture);
            _fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(_fixtureItem);
        }

        [Test]
        public void GetMerchandisableFixtureComponents_ReturnsComponents_WhenLocationIsOver()
        {
            //..:...........:..
            //  :           :
            //  :           :
            //  :___________:
            //  :           :
            //  :           :
            //..:...........:..
            //  :           :

            GetFixtureComponentsSetup();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            _planogram.Blocking.Add(blocking);
            var location = TestDataHelper.CreateBlockingLocation(null, null, null, null, blocking.Locations);
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(
                0f, _fixtureSize/2f, _fixtureSize, 1f, PlanogramComponentType.Shelf, _fixture.Components, _planogram.Components);

            var expected = new List<PlanogramFixtureComponent>(){ fixtureComponent };

            var actual = location.GetMerchandisableFixtureAndAssemblyComponents().ToList();

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GetMerchandisableFixtureComponents_DoesNotReturnComponent_WhenBase()
        {
            GetFixtureComponentsSetup();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            _planogram.Blocking.Add(blocking);
            var location = TestDataHelper.CreateBlockingLocation(null, null, null, null, blocking.Locations);
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(
                0f, _fixtureSize / 2f, _fixtureSize, 1f, PlanogramComponentType.Base, _fixture.Components, _planogram.Components);

            var actual = location.GetMerchandisableFixtureAndAssemblyComponents().ToList();

            CollectionAssert.DoesNotContain(actual, fixtureComponent);
        }

        [Test]
        public void GetMerchandisableFixtureComponents_DoesNotReturnComponent_WhenBackboard()
        {
            GetFixtureComponentsSetup();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            _planogram.Blocking.Add(blocking);
            var location = TestDataHelper.CreateBlockingLocation(null, null, null, null, blocking.Locations);
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(
                0f, _fixtureSize / 2f, _fixtureSize, 1f, PlanogramComponentType.Backboard, _fixture.Components, _planogram.Components);

            var actual = location.GetMerchandisableFixtureAndAssemblyComponents().ToList();

            CollectionAssert.DoesNotContain(actual, fixtureComponent);
        }

        [Test]
        public void GetMerchandisableFixtureComponents_ReturnsNoComponents_WhenLocationIsNotOver()
        {
            //..:...........:..
            //  :           :
            //  :           :
            //  :           :             ___________
            //  :           :
            //  :           :
            //..:...........:..
            //  :           :

            GetFixtureComponentsSetup();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            _planogram.Blocking.Add(blocking);
            var location = TestDataHelper.CreateBlockingLocation(null, null, null, null, blocking.Locations);
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(
                _fixtureSize*2, _fixtureSize / 2f, _fixtureSize, 1f, PlanogramComponentType.Shelf, _fixture.Components, _planogram.Components);

            var expected = new List<PlanogramFixtureComponent>();

            var actual = location.GetMerchandisableFixtureAndAssemblyComponents().ToList();

            CollectionAssert.AreEquivalent(expected, actual);
        }

        #endregion
    }
}
