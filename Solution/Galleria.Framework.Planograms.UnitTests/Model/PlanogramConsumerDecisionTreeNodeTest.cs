﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)
// V8-27132 : A.Kuszyk
//  Added test for factory method that accepts interface.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Moq;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Ccm.Security;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramConsumerDecisionTreeNodeTest : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(1));
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTreeNode()
        {
            var model = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode("Node");
            Assert.That(model.IsChild);
            Assert.IsNotNull(model.Name);
        }

        [Test]
        public void NewConsumerDecisionTreeNode_CopiesInterfaceProperties()
        {
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Gtin = "Gtin";
            planogram.Products.Add(product);
            var level = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel(Guid.NewGuid().ToString());
            level.ChildLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();
            level.ChildLevel.ChildLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();
            var productMock = new Mock<IPlanogramConsumerDecisionTreeNodeProduct>();
            productMock.Setup(o => o.ProductGtin).Returns(product.Gtin);
            var nodeMock = new Mock<IPlanogramConsumerDecisionTreeNode>();
            var node = nodeMock.Object;
            nodeMock.Setup(o => o.Name).Returns(Guid.NewGuid().ToString());
            nodeMock.Setup(o => o.ChildList).Returns(new IPlanogramConsumerDecisionTreeNode[] { new Mock<IPlanogramConsumerDecisionTreeNode>().Object });
            nodeMock.Setup(o => o.Products).Returns(new IPlanogramConsumerDecisionTreeNodeProduct[] { productMock.Object });

            var model = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(node, level, planogram);

            AssertHelper.AreEqual(node, model);
        }

        [Test]
        public void NewConsumerDecisionTreeNode_OnlyAddsProductsFromPlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Gtin = "Gtin";
            planogram.Products.Add(product);
            var product2 = PlanogramProduct.NewPlanogramProduct();
            product2.Gtin = "Gtin2";
            planogram.Products.Add(product2);
            var level = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel(Guid.NewGuid().ToString());
            level.ChildLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();
            level.ChildLevel.ChildLevel = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();
            var productMock = new Mock<IPlanogramConsumerDecisionTreeNodeProduct>();
            productMock.Setup(o => o.ProductGtin).Returns(product.Gtin);
            var nodeMock = new Mock<IPlanogramConsumerDecisionTreeNode>();
            var node = nodeMock.Object;
            nodeMock.Setup(o => o.Name).Returns(Guid.NewGuid().ToString());
            nodeMock.Setup(o => o.ChildList).Returns(new IPlanogramConsumerDecisionTreeNode[] { new Mock<IPlanogramConsumerDecisionTreeNode>().Object });
            nodeMock.Setup(o => o.Products).Returns(new IPlanogramConsumerDecisionTreeNodeProduct[] { productMock.Object });

            var model = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode(node, level, planogram);

            Assert.AreEqual(1, model.Products.Count());
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set consumer decision tree name
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "RootLevel";

            // add set root node name
            package.Planograms[0].ConsumerDecisionTree.RootNode.Name = "RootNode";
            package = package.Save();

            var Node = package.Planograms[0].ConsumerDecisionTree.RootNode;
            var dto = FetchDtoByModel(Node);

            //fetch
            Assert.AreEqual(dto.Name, package.Planograms[0].ConsumerDecisionTree.RootNode.Name);
        }

        #endregion

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            //create new consumer decision tree
            var newPlanogramConsumerDecisionTree = PlanogramConsumerDecisionTree.NewPlanogramConsumerDecisionTree();
            //create new consumer decision tree level
            var newPlanogramConsumerDecisionTreeNode = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode("Root");
            package = package.Save();

            //check if consumer decision tree is not null
            Assert.IsNotNull(newPlanogramConsumerDecisionTreeNode);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            String check = "Root";

            // set consumer decision tree name
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "RootLevel";

            // add set root node name
            package.Planograms[0].ConsumerDecisionTree.RootNode.Name = check;

            // save
            package = package.Save();
            PackageDto dto;

            // fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check first value is equal
            Assert.AreEqual(check, package.Planograms[0].ConsumerDecisionTree.RootNode.Name);

            // change value
            check = "Child";
            package.Planograms[0].ConsumerDecisionTree.RootNode.Name = check;
            package = package.Save();

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if value has changed
            Assert.AreEqual(check, package.Planograms[0].ConsumerDecisionTree.RootNode.Name);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set consumer decision tree name
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "RootLevel";

            // add set root node name
            package.Planograms[0].ConsumerDecisionTree.RootNode.Name = "RootNode";

            var model = package.Planograms[0].ConsumerDecisionTree.RootNode;
            package.Planograms[0].ConsumerDecisionTree.RootNode.AddNewChildNode();
            package = package.Save();
            PackageDto dto;

            // fetch
            var modelPackage = package.Planograms[0].ConsumerDecisionTree.RootLevel;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // remove planogram
            package.Planograms[0].ConsumerDecisionTree.RootNode.ChildList[0].RemoveNode();
            package = package.Save();

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // root childlist should be empty
            Assert.IsEmpty(package.Planograms[0].ConsumerDecisionTree.RootNode.ChildList);

        }

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        private PlanogramConsumerDecisionTreeNodeDto FetchDtoByModel(PlanogramConsumerDecisionTreeNode model)
        {
            PlanogramConsumerDecisionTreeNodeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeDal>())
                {
                    dto = dal.FetchByPlanogramConsumerDecisionTreeId(model.ParentPlanogramConsumerDecisionTree.Id).FirstOrDefault();
                }
            }
            return dto;
        }

        #endregion
    }
}
