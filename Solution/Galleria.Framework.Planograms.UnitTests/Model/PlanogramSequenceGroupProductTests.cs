﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramSequenceGroupProductTests : TestBase
    {
        private Package _package;
        private Entity _entity;

        private Planogram TestPlanogram
        {
            get { return _package.Planograms.FirstOrDefault(); }
        }

        [SetUp]
        public void TestSetup()
        {
            _package = CreatePackage("PackageName");
            AddPlanogram(_package);
        }

        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewPlanogramSequenceGroupProduct()
        {
            //  Create a new planogram sequence group product.
            PlanogramSequenceGroupProduct testModel = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();

            Assert.IsTrue(testModel.IsNew);
            Assert.IsTrue(testModel.IsChild);
        }

        [Test]
        public void Fetch()
        {
            PlanogramSequenceGroup sequenceGroup = AddSequenceGroup(TestPlanogram);
            PlanogramSequenceGroupProduct expected = AddGroupProduct(sequenceGroup);
            SavePackage();

            ReloadPackage();

            PlanogramSequenceGroupProductList actualProducts = TestPlanogram.Sequence.Groups[0].Products;
            CollectionAssert.IsNotEmpty(actualProducts);
            PlanogramSequenceGroupProduct actual = actualProducts[0];
            Assert.AreEqual(expected.Gtin, actual.Gtin);
            Assert.AreEqual(expected.SequenceNumber, actual.SequenceNumber);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            // Already tested when testing Fetch.
        }

        [Test]
        public void DataAccess_Update()
        {
            //  Save the original, update and reload to test.
            PlanogramSequenceGroup sequenceGroup = AddSequenceGroup(TestPlanogram);
            PlanogramSequenceGroupProduct original = AddGroupProduct(sequenceGroup);
            SavePackage();
            PlanogramSequenceGroupProduct expected = TestPlanogram.Sequence.Groups[0].Products[0];
            expected.Gtin = "UpdatedGtin";
            expected.SequenceNumber = 2;
            SavePackage();

            ReloadPackage();

            PlanogramSequenceGroupProductList actualProducts = TestPlanogram.Sequence.Groups[0].Products;
            CollectionAssert.IsNotEmpty(actualProducts);
            PlanogramSequenceGroupProduct actual = actualProducts[0];
            Assert.AreNotEqual(original.Gtin, actual.Gtin);
            Assert.AreNotEqual(original.SequenceNumber, actual.SequenceNumber);
            Assert.AreEqual(expected.Gtin, actual.Gtin);
            Assert.AreEqual(expected.SequenceNumber, actual.SequenceNumber);
        }

        private void ReloadPackage()
        {
            _package = Package.FetchById(_package.Id);
        }

        private void SavePackage()
        {
            _package = _package.Save();
        }

        [Test]
        public void DataAcces_Delete()
        {
            PlanogramSequenceGroup sequenceGroup = AddSequenceGroup(TestPlanogram);
            PlanogramSequenceGroupProduct expected = AddGroupProduct(sequenceGroup);
            SavePackage();

            //  Delete the Planogram.
            _package.Planograms.Remove(TestPlanogram);
            SavePackage();

            Assert.IsNull(FetchDtoByModel(expected));
        }

        #endregion

        #region Test Helper Methods

        private static Package CreatePackage(String name)
        {
            Package package = Package.NewPackage(1, PackageLockType.User);
            package.Name = name;
            return package;
        }

        private static Planogram AddPlanogram(Package parent)
        {
            Planogram planogram = Planogram.NewPlanogram();
            planogram.Name = String.Format("{0}.Planogram[{1}]", parent.Name, parent.Planograms.Count);
            parent.Planograms.Add(planogram);
            return planogram;
        }

        private static PlanogramSequenceGroup AddSequenceGroup(Planogram parent)
        {
            PlanogramSequenceGroupList groups = parent.Sequence.Groups;
            PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            sequenceGroup.Name = String.Format("{0}.SequenceGroup[{1}]", parent.Name, parent.Sequence.Groups.Count);
            sequenceGroup.Colour = 1;
            groups.Add(sequenceGroup);
            return sequenceGroup;
        }

        private static PlanogramSequenceGroupProduct AddGroupProduct(PlanogramSequenceGroup parent)
        {
            PlanogramSequenceGroupProduct groupProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
            groupProduct.Gtin = String.Format("{0}.Product[{1}]", parent.Name, parent.Products.Count);
            groupProduct.SequenceNumber = 1;
            parent.Products.Add(groupProduct);
            return groupProduct;
        }

        private PlanogramSequenceGroupProductDto FetchDtoByModel(PlanogramSequenceGroupProduct model)
        {
            PlanogramSequenceGroupProductDto dto;
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramSequenceGroupProductDal>())
                {
                    dto = dal.FetchByPlanogramSequenceGroupId(model.Parent.Id).FirstOrDefault();
                }
            }
            return dto;
        }

        #endregion
    }
}
