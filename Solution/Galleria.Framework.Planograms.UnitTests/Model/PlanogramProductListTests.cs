﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32132 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramProductListTests
    {
        private const String NewGtin = "NewGtin";

        #region Helper Static Methods

        /// <summary>
        ///     Sets up the target and source lists for append tests (source list will contain one existing and one new product).
        /// </summary>
        /// <param name="targetList">Contains only the 'ExistingGtin' product.</param>
        /// <param name="sourceList">Contains both 'ExistingGtin' and 'NewGting' products.</param>
        private static void SetupTargetAndSource(out PlanogramProductList targetList, out PlanogramProductList sourceList)
        {
            targetList = PlanogramProductList.NewPlanogramProductList();
            sourceList = PlanogramProductList.NewPlanogramProductList();
            PlanogramProduct item = PlanogramProduct.NewPlanogramProduct();
            item.Gtin = "ExistingGtin";
            targetList.Add(item);
            sourceList.Add(item.Copy());
            item = PlanogramProduct.NewPlanogramProduct();
            item.Gtin = NewGtin;
            sourceList.Add(item);
        }

        #endregion

        [Test]
        public void AppendProductsShouldAddNewGtins()
        {
            const String expectation = "The new product should have been added.";
            PlanogramProductList targetList;
            PlanogramProductList sourceList;
            SetupTargetAndSource(out targetList, out sourceList);

            targetList.AppendNew(sourceList);

            CollectionAssert.Contains(targetList.Select(item => item.Gtin), NewGtin, expectation);
        }

        [Test]
        public void AppendProductsShouldNotAddExistingGtins()
        {
            const String expectation = "The existing product should not have been added.";
            PlanogramProductList targetList;
            PlanogramProductList sourceList;
            SetupTargetAndSource(out targetList, out sourceList);

            targetList.AppendNew(sourceList);

            CollectionAssert.AllItemsAreUnique(targetList.Select(item => item.Gtin), expectation);
        }
    }
}