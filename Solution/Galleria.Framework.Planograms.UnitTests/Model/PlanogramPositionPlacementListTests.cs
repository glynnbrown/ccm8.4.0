﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-29280 : A.Silva
//		Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.DataStructures;
using FluentAssertions;
using Galleria.Framework.Planograms.UnitTests.Helpers;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramPositionPlacementListTests
    {
        [Test]
        public void GroupByAxis_ShouldReturnCorrectNumberOfGroups()
        {
            PlanogramFixtureItem bay = Planogram.NewPlanogram().AddFixtureItem();
            PlanogramFixtureComponent component = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            const Int32 expectedGroupByAxisCount = 10;
            for (Int16 sequenceX = 1; sequenceX <= expectedGroupByAxisCount; sequenceX++)
            {
                PlanogramPosition position = component.AddPosition(bay);
                position.SequenceX = sequenceX;
            }

            Int32 groupByAxisCount;
            using (
                PlanogramMerchandisingGroup merchandisingGroup =
                    PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(
                        component.GetPlanogramSubComponentPlacements()))
            {
                Dictionary<Tuple<Int16, Int16>, List<PlanogramPositionPlacement>> groupedByAxis = merchandisingGroup.PositionPlacements.GroupByAxis(AxisType.Y);
                groupByAxisCount = groupedByAxis.Count();
            }

            Assert.AreEqual(expectedGroupByAxisCount, groupByAxisCount);
        }

        #region GetReservedSpace

        [Test]
        public void GetReservedSpace_ReturnsCorrectSpace_ForSimpleProductsOnShelf_LeftStacked()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            plan.ReprocessAllMerchandisingGroups();
            using(var mgs = plan.GetMerchandisingGroups())
            {
                var positionPlacementList = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(mgs.First().PositionPlacements);

                RectValue actualSpace = positionPlacementList.GetReservedSpace();

                AssertHelper.AreRectValuesEqual(new RectValue(0, 4, 65, 30, 10, 10), actualSpace);
            }
        }

        [Test]
        public void GetReservedSpace_ReturnsCorrectSpace_ForComplexProductsOnShelf_LeftStacked()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(20, 20, 20)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(30, 30, 30)));
            plan.ReprocessAllMerchandisingGroups();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var positionPlacementList = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(mgs.First().PositionPlacements);

                RectValue actualSpace = positionPlacementList.GetReservedSpace();

                AssertHelper.AreRectValuesEqual(new RectValue(0, 4, 45, 60, 30, 30), actualSpace);
            }
        }

        [Test]
        public void GetReservedSpace_ReturnsCorrectSpace_ForSimpleProductsOnShelf_Even()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            plan.ReprocessAllMerchandisingGroups();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var positionPlacementList = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(mgs.First().PositionPlacements);

                RectValue actualSpace = positionPlacementList.GetReservedSpace();

                AssertHelper.AreRectValuesEqual(new RectValue(45, 4, 65, 30, 10, 10), actualSpace);
            }
        }

        [Test]
        public void GetReservedSpace_ReturnsCorrectSpace_ForComplexProductsOnShelf_Even()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(20, 20, 20)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(30, 30, 30)));
            plan.ReprocessAllMerchandisingGroups();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var positionPlacementList = PlanogramPositionPlacementList.NewPlanogramPositionPlacementList(mgs.First().PositionPlacements);

                RectValue actualSpace = positionPlacementList.GetReservedSpace();

                AssertHelper.AreRectValuesEqual(new RectValue(30, 4, 45, 60, 30, 30), actualSpace);
            }
        }

        #endregion
    }
}
