﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Initial version.
// V8-24972 : L.Hodson
//  Added product image fields for display, tray,  pointofpurchase, alternate and case
// V8-26041 : A.Kuszyk
//  Added additional Product properties for PlanogramProduct.
// V8-26271 : A.Kuszyk
//  Added Planogram Performance and child models.
// V8-26426 : A.Kuszyk
//  Added Planogram Assortment and child models.
// V8-26952 : A.Kuszyk 
//  Added test for lazy loaded properties.
// V8-27058 : A.Probyn
//  Extended to test CalculateMetadata down structure
#endregion

#region Version History: CCM803
// V8-29369 : L.Luong
//      Separated MetaData Test to separate smaller tests.
#endregion

#region Version History: CCM820
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
#endregion
#region Version History: CCM 830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using System.Reflection;
using Csla;
using Csla.Core;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Ccm.Helpers;
using FluentAssertions;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PackageTests : TestBase
    {
        private List<Type> _modelTypes
        {
            get
            {
                var assembly = Assembly.GetAssembly(typeof(Planogram));
                return assembly.GetExportedTypes().
                    Where(t =>
                        t.IsClass &&
                        t.Name.StartsWith("Planogram") &&
                        !t.Name.Contains("List") &&
                        !t.Name.Contains("Helper") &&
                        t.Namespace.Contains("Model")).
                    OrderBy(t => t.Name).
                    ToList();
            }
        }

        [Test]
        public void ReturnsAllIdsGivenNoUcrExceptions()
        {
            var fetchedIds = Package.FetchIdsExceptFor(new List<String>());

            fetchedIds.Should().HaveCount(5, $"there are {5} plans and no ucr exception was provided");
        }

        /// <summary>
        /// Tests that a Package and all its children can be created, saved, and loaded, and that in so doing all 
        /// properties values are preserved and all IDs resolved.
        /// </summary>
        [Test]
        public void Test()
        {
            Object id1;
            Object id2;
            Object id3;
            Object id4;
            PlanogramList planograms = PlanogramList.NewPlanogramList();
            planograms.Add(Planogram.NewPlanogram());
            Assert.IsNotNull(planograms.FindById(planograms[0].Id));
            planograms.Add(Planogram.NewPlanogram());
            Assert.IsNotNull(planograms.FindById(planograms[0].Id));
            Assert.IsNotNull(planograms.FindById(planograms[1].Id));
            planograms.AddRange(new List<Planogram> { Planogram.NewPlanogram(), Planogram.NewPlanogram() });
            Assert.IsNotNull(planograms.FindById(planograms[0].Id));
            Assert.IsNotNull(planograms.FindById(planograms[1].Id));
            Assert.IsNotNull(planograms.FindById(planograms[2].Id));
            Assert.IsNotNull(planograms.FindById(planograms[3].Id));
            id1 = planograms[0].Id;
            id2 = planograms[1].Id;
            id3 = planograms[2].Id;
            id4 = planograms[3].Id;
            planograms.RemoveAt(3);
            Assert.IsNotNull(planograms.FindById(id1));
            Assert.IsNotNull(planograms.FindById(id2));
            Assert.IsNotNull(planograms.FindById(id3));
            Assert.IsNull(planograms.FindById(id4));
            planograms.RemoveList(new List<Planogram> { planograms[1], planograms[2] });
            Assert.IsNotNull(planograms.FindById(id1));
            Assert.IsNull(planograms.FindById(id2));
            Assert.IsNull(planograms.FindById(id3));
            Assert.IsNull(planograms.FindById(id4));

            Int32 planogramCount = 0;
            Int32 planogramFixtureItemCount = 0;
            Int32 planogramFixtureCount = 0;
            Int32 planogramFixtureAssemblyCount = 0;
            Int32 planogramFixtureComponentCount = 0;
            Int32 planogramAssemblyCount = 0;
            Int32 planogramAssemblyComponentCount = 0;
            Int32 planogramComponentCount = 0;
            Int32 planogramSubComponentCount = 0;
            Int32 planogramProductCount = 0;
            Int32 planogramPositionCount = 0;
            Int32 planogramAnnotationCount = 0;
            Int32 planogramImageCount = 0;
            Int32 planogramId = 0;
            Int32 planogramFixtureItemId = 0;
            Int32 planogramFixtureId = 0;
            Int32 planogramFixtureAssemblyId = 0;
            Int32 planogramFixtureComponentId = 0;
            Int32 planogramAssemblyId = 0;
            Int32 planogramAssemblyComponentId = 0;
            Int32 planogramComponentId = 0;
            Int32 planogramSubComponentId = 0;
            Int32 planogramProductId = 0;
            Int32 planogramPositionId = 0;
            Int32 planogramAnnotationId = 0;
            Int32 planogramImageId = 0;
            Int32 planogramPerformanceMetricId = 0;
            Int32 planogramPerformanceDataId = 0;
            Int32 planogramAssortmentRegionLocationId = 0;
            Int32 planogramAssortmentRegionProductId = 0;
            Dictionary<Object, Object> ids = new Dictionary<Object, Object>();

            Galleria.Framework.Planograms.Model.Package package =
                Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "Test";
            ids[package.Id] = 1;
            package.Planograms.AddRange(CreateModelObjects<Planogram>(ids, ref planogramId));
            planogramCount = package.Planograms.Count;
            Planogram planogram = package.Planograms[0];
            planogram.Name = "Planogram";
            planogram.CustomAttributes.Text1 = "CustAttValue";
            planogram.FixtureItems.AddRange(CreateModelObjects<PlanogramFixtureItem>(ids, ref planogramFixtureItemId));
            planogramFixtureItemCount = planogram.FixtureItems.Count;
            planogram.Fixtures.AddRange(CreateModelObjects<PlanogramFixture>(ids, ref planogramFixtureId));
            planogramFixtureCount = planogram.Fixtures.Count;
            foreach (PlanogramFixture planogramFixture in planogram.Fixtures)
            {
                planogramFixture.Assemblies.AddRange(CreateModelObjects<PlanogramFixtureAssembly>(ids, ref planogramFixtureAssemblyId));
                planogramFixture.Components.AddRange(CreateModelObjects<PlanogramFixtureComponent>(ids, ref planogramFixtureComponentId));
            }
            planogramFixtureAssemblyCount = planogramFixtureCount * planogram.Fixtures[0].Assemblies.Count;
            planogramFixtureComponentCount = planogramFixtureCount * planogram.Fixtures[0].Components.Count;
            planogram.Assemblies.AddRange(CreateModelObjects<PlanogramAssembly>(ids, ref planogramAssemblyId));
            planogramAssemblyCount = planogram.Assemblies.Count;
            foreach (PlanogramAssembly planogramAssembly in planogram.Assemblies)
            {
                planogramAssembly.Components.AddRange(CreateModelObjects<PlanogramAssemblyComponent>(ids, ref planogramAssemblyComponentId));
            }
            planogramAssemblyComponentCount = planogramAssemblyCount * planogram.Assemblies[0].Components.Count;
            planogram.Components.AddRange(CreateModelObjects<PlanogramComponent>(ids, ref planogramComponentId));
            planogramComponentCount = planogram.Components.Count;
            foreach (PlanogramComponent planogramComponent in planogram.Components)
            {
                planogramComponent.SubComponents.AddRange(CreateModelObjects<PlanogramSubComponent>(ids, ref planogramSubComponentId));
            }
            planogramSubComponentCount = planogramComponentCount * planogram.Components[0].SubComponents.Count;
            planogram.Products.AddRange(CreateModelObjects<PlanogramProduct>(ids, ref planogramProductId));
            //clear performance
            planogram.Performance.PerformanceData.Clear();
            planogramProductCount = planogram.Products.Count;
            planogram.Positions.AddRange(CreateModelObjects<PlanogramPosition>(ids, ref planogramPositionId));
            planogramPositionCount = planogram.Positions.Count;
            planogram.Annotations.AddRange(CreateModelObjects<PlanogramAnnotation>(ids, ref planogramAnnotationId));
            planogramAnnotationCount = planogram.Annotations.Count;
            planogram.Images.AddRange(CreateModelObjects<PlanogramImage>(ids, ref planogramImageId));
            planogramImageCount = planogram.Images.Count;
            planogram.Performance.Name = "Performance";
            ids.Add(planogram.Performance.Id, 1);
            for (Byte i = 1; i <= 20; i++)
            {
                planogramPerformanceMetricId++;
                var planogramPerformanceMetric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(i);
                planogramPerformanceMetric.Name = String.Format("PerformanceMetric{0}", i);
                planogram.Performance.Metrics.Add(planogramPerformanceMetric);
                ids.Add(planogramPerformanceMetric.Id, planogramPerformanceMetricId);
            }
            
            planogram.Assortment.Name = "Assortment";
            ids.Add(planogram.Assortment.Id, 1);
            for (Int32 i = 1; i <= 10; i++)
            {
                var item = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
                item.Gtin = String.Format("Gtin{0}", i);
                //item.LocationCode= String.Format("LocationCode{0}", i);
                planogram.Assortment.Products.Add(item);
                ids.Add(item.Id, i);
            }

            for (Int32 i = 1; i <= 10; i++)
            {
                var item = PlanogramAssortmentLocalProduct.NewPlanogramAssortmentLocalProduct();
                item.ProductGtin = String.Format("Gtin{0}", i);
                item.LocationCode = String.Format("LocationCode{0}", i);
                planogram.Assortment.LocalProducts.Add(item);
                ids.Add(item.Id, i);
            }

            for (Int32 i = 1; i <= 10; i++)
            {
                var item = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();
                item.Name = String.Format("AssortmentRegion{0}", i);
                planogram.Assortment.Regions.Add(item);
                ids.Add(item.Id, i);
            }

            foreach (var region in planogram.Assortment.Regions)
            {
                for (Int32 i = 1; i <= 10; i++)
                {
                    planogramAssortmentRegionLocationId++;
                    var item = PlanogramAssortmentRegionLocation.NewPlanogramAssortmentRegionLocation();
                    item.LocationCode = String.Format("LocationCode{0}", i);
                    region.Locations.Add(item);
                    ids.Add(item.Id, planogramAssortmentRegionLocationId);
                }

                for (Int32 i = 1; i <= 10; i++)
                {
                    planogramAssortmentRegionProductId++;
                    var item = PlanogramAssortmentRegionProduct.NewPlanogramAssortmentRegionProduct();
                    item.PrimaryProductGtin = String.Format("ProductGtin{0}", i);
                    region.Products.Add(item);
                    ids.Add(item.Id, planogramAssortmentRegionProductId);
                }
            }

            Int32 fixtureItemIdCounter = 0;
            Int32 fixtureAssemblyIdCounter1 = 0;
            Int32 fixtureAssemblyIdCounter2 = 0;
            Int32 fixtureComponentIdCounter1 = 0;
            Int32 fixtureComponentIdCounter2 = 0;
            Int32 assemblyComponentIdCounter1 = 0;
            Int32 assemblyComponentIdCounter2 = 0;
            Int32 subComponentIdCounter1 = 0;
            Int32 subComponentIdCounter2 = 0;
            Int32 positionCounter = 0;
            Int32 componentIdCounter = 0;
            Int32 imageIdCounter = 0;
            Int32 assemblyIdCounter = 0;
            Int32 fixtureIdCounter = 0;
            Int32 productIdCounter = 0;

            #region Annotations
            foreach (PlanogramAnnotation planogramAnnotation in planogram.Annotations)
            {
                planogramAnnotation.PlanogramFixtureItemId = planogram.FixtureItems[fixtureItemIdCounter].Id;
                fixtureItemIdCounter++;
                if (fixtureItemIdCounter == planogram.FixtureItems.Count)
                {
                    fixtureItemIdCounter = 0;
                }
                planogramAnnotation.PlanogramFixtureAssemblyId = planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies[fixtureAssemblyIdCounter2].Id;
                fixtureAssemblyIdCounter2++;
                if (fixtureAssemblyIdCounter2 == planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies.Count)
                {
                    fixtureAssemblyIdCounter2 = 0;
                    fixtureAssemblyIdCounter1++;
                    if (fixtureAssemblyIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureAssemblyIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramFixtureComponentId = planogram.Fixtures[fixtureComponentIdCounter1].Components[fixtureComponentIdCounter2].Id;
                fixtureComponentIdCounter2++;
                if (fixtureComponentIdCounter2 == planogram.Fixtures[fixtureComponentIdCounter1].Components.Count)
                {
                    fixtureComponentIdCounter2 = 0;
                    fixtureComponentIdCounter1++;
                    if (fixtureComponentIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureComponentIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramAssemblyComponentId = planogram.Assemblies[assemblyComponentIdCounter1].Components[assemblyComponentIdCounter2].Id;
                assemblyComponentIdCounter2++;
                if (assemblyComponentIdCounter2 == planogram.Assemblies[assemblyComponentIdCounter1].Components.Count)
                {
                    assemblyComponentIdCounter2 = 0;
                    assemblyComponentIdCounter1++;
                    if (assemblyComponentIdCounter1 == planogram.Assemblies.Count)
                    {
                        assemblyComponentIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramSubComponentId = planogram.Components[subComponentIdCounter1].SubComponents[subComponentIdCounter2].Id;
                subComponentIdCounter2++;
                if (subComponentIdCounter2 == planogram.Components[subComponentIdCounter1].SubComponents.Count)
                {
                    subComponentIdCounter2 = 0;
                    subComponentIdCounter1++;
                    if (subComponentIdCounter1 == planogram.Components.Count)
                    {
                        subComponentIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramPositionId = planogram.Positions[positionCounter].Id;
                positionCounter++;
                if (positionCounter == planogram.Positions.Count)
                {
                    positionCounter = 0;
                }                 
            }
            #endregion
            #region AssemblyComponents
            foreach (PlanogramAssembly planogramAssembly in planogram.Assemblies)
            {
                foreach (PlanogramAssemblyComponent planogramAssemblyComponent in planogramAssembly.Components)
                {
                    planogramAssemblyComponent.PlanogramComponentId = planogram.Components[componentIdCounter].Id;
                    componentIdCounter++;
                    if (componentIdCounter == planogram.Components.Count)
                    {
                        componentIdCounter = 0;
                    } 
                }
            }
            #endregion
            #region Components/SubComponents
            foreach (PlanogramComponent planogramComponent in planogram.Components)
            {
                planogramComponent.ImageIdFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                foreach (PlanogramSubComponent planogramSubComponent in planogramComponent.SubComponents)
                {
                    planogramSubComponent.ImageIdFront = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdBack = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdTop = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdBottom = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdLeft = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdRight = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                }
            }
            #endregion
            #region FixtureAssemblies/FixtureComponents
            foreach (PlanogramFixture planogramFixture in planogram.Fixtures)
            {
                foreach (PlanogramFixtureAssembly planogramFixtureAssembly in planogramFixture.Assemblies)
                {
                    planogramFixtureAssembly.PlanogramAssemblyId = planogram.Assemblies[assemblyIdCounter].Id;
                    assemblyIdCounter++;
                    if (assemblyIdCounter == planogram.Assemblies.Count)
                    {
                        assemblyIdCounter = 0;
                    }
                }
                foreach (PlanogramFixtureComponent planogramFixtureComponent in planogramFixture.Components)
                {
                    planogramFixtureComponent.PlanogramComponentId = planogram.Components[componentIdCounter].Id;
                    componentIdCounter++;
                    if (componentIdCounter == planogram.Components.Count)
                    {
                        componentIdCounter = 0;
                    }
                }
            }
            #endregion
            #region FixtureItems
            foreach (PlanogramFixtureItem planogramFixtureItem in planogram.FixtureItems)
            {
                planogramFixtureItem.PlanogramFixtureId = planogram.Fixtures[fixtureIdCounter].Id;
                fixtureIdCounter++;
                if (fixtureIdCounter == planogram.Fixtures.Count)
                {
                    fixtureIdCounter = 0;
                }
            }
            #endregion
            #region Positions
            foreach (PlanogramPosition planogramPosition in planogram.Positions)
            {
                planogramPosition.PlanogramProductId = planogram.Products[productIdCounter].Id;
                productIdCounter++;
                if (productIdCounter == planogram.Products.Count)
                {
                    productIdCounter = 0;
                }
                planogramPosition.PlanogramFixtureItemId = planogram.FixtureItems[fixtureItemIdCounter].Id;
                fixtureItemIdCounter++;
                if (fixtureItemIdCounter == planogram.FixtureItems.Count)
                {
                    fixtureItemIdCounter = 0;
                }
                planogramPosition.PlanogramFixtureAssemblyId = planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies[fixtureAssemblyIdCounter2].Id;
                fixtureAssemblyIdCounter2++;
                if (fixtureAssemblyIdCounter2 == planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies.Count)
                {
                    fixtureAssemblyIdCounter2 = 0;
                    fixtureAssemblyIdCounter1++;
                    if (fixtureAssemblyIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureAssemblyIdCounter1 = 0;
                    }
                }
                planogramPosition.PlanogramAssemblyComponentId = planogram.Assemblies[assemblyComponentIdCounter1].Components[assemblyComponentIdCounter2].Id;
                assemblyComponentIdCounter2++;
                if (assemblyComponentIdCounter2 == planogram.Assemblies[assemblyComponentIdCounter1].Components.Count)
                {
                    assemblyComponentIdCounter2 = 0;
                    assemblyComponentIdCounter1++;
                    if (assemblyComponentIdCounter1 == planogram.Assemblies.Count)
                    {
                        assemblyComponentIdCounter1 = 0;
                    }
                }
                planogramPosition.PlanogramFixtureComponentId = planogram.Fixtures[fixtureComponentIdCounter1].Components[fixtureComponentIdCounter2].Id;
                fixtureComponentIdCounter2++;
                if (fixtureComponentIdCounter2 == planogram.Fixtures[fixtureComponentIdCounter1].Components.Count)
                {
                    fixtureComponentIdCounter2 = 0;
                    fixtureComponentIdCounter1++;
                    if (fixtureComponentIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureComponentIdCounter1 = 0;
                    }
                }
                planogramPosition.PlanogramSubComponentId = planogram.Components[subComponentIdCounter1].SubComponents[subComponentIdCounter2].Id;
                subComponentIdCounter2++;
                if (subComponentIdCounter2 == planogram.Components[subComponentIdCounter1].SubComponents.Count)
                {
                    subComponentIdCounter2 = 0;
                    subComponentIdCounter1++;
                    if (subComponentIdCounter1 == planogram.Components.Count)
                    {
                        subComponentIdCounter1 = 0;
                    }
                }
            }
            #endregion
            #region Products
            foreach (PlanogramProduct planogramProduct in planogram.Products)
            {
                //Unit Images
                planogramProduct.PlanogramImageIdFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Display Images
                planogramProduct.PlanogramImageIdDisplayFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Tray Images
                planogramProduct.PlanogramImageIdTrayFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }


                //PointOfPurchase Images
                planogramProduct.PlanogramImageIdPointOfPurchaseFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Alternate Images
                planogramProduct.PlanogramImageIdAlternateFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Case Images
                planogramProduct.PlanogramImageIdCaseFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

            }
            #endregion
            
            //TestDataHelper.IsPackageValid(package);

            ////Save package to get product ids
            //package = package.Save();
            //planogram = package.Planograms[0];

            //foreach (var planogramProduct in planogram.Products)
            //{
            //    planogramPerformanceDataId++;
            //    var performanceData = PlanogramPerformanceData.NewPlanogramPerformanceData(planogramProduct);
            //    planogram.Performance.PerformanceData.Add(performanceData);
            //    ids.Add(performanceData.Id, planogramPerformanceDataId);
            //}
            //foreach (var plan in package.Planograms)
            //{
            //    plan.Performance.Name = "PerformanceName";
            //}

            Debug.WriteLine(String.Format("{0}: {1}", "planogramCount", planogramCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramFixtureItemCount", planogramFixtureItemCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramFixtureCount", planogramFixtureCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramFixtureAssemblyCount", planogramFixtureAssemblyCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramFixtureComponentCount", planogramFixtureComponentCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramAssemblyCount", planogramAssemblyCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramAssemblyComponentCount", planogramAssemblyComponentCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramComponentCount", planogramComponentCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramSubComponentCount", planogramSubComponentCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramProductCount", planogramProductCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramPositionCount", planogramPositionCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramAnnotationCount", planogramAnnotationCount));
            Debug.WriteLine(String.Format("{0}: {1}", "planogramImageCount", planogramImageCount));

            Galleria.Framework.Planograms.Model.Package savedPackage = null;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            savedPackage = package.Save();
            stopwatch.Stop();
            Debug.WriteLine(String.Format("Milliseconds to save: {0}", stopwatch.ElapsedMilliseconds));
            stopwatch.Reset();
            stopwatch.Start();
            savedPackage = Galleria.Framework.Planograms.Model.Package.FetchById(savedPackage.Id, 1, PackageLockType.User);
            stopwatch.Stop();
            Debug.WriteLine(String.Format("Milliseconds to load: {0}", stopwatch.ElapsedMilliseconds));

            CompareModelObjects<Galleria.Framework.Planograms.Model.Package>(package, savedPackage, ids);
            CompareModelObjects<Planogram>(package.Planograms, savedPackage.Planograms, ids);
            Planogram savedPlanogram = savedPackage.Planograms[0];
            Assert.AreEqual("CustAttValue", planogram.CustomAttributes.Text1); 
            CompareModelObjects<PlanogramFixtureItem>(planogram.FixtureItems, savedPlanogram.FixtureItems, ids);
            CompareModelObjects<PlanogramFixture>(planogram.Fixtures, savedPlanogram.Fixtures, ids);
            for (Int32 i = 0; i < planogram.Fixtures.Count; i++)
            {
                CompareModelObjects<PlanogramFixtureAssembly>(planogram.Fixtures[i].Assemblies, savedPlanogram.Fixtures[i].Assemblies, ids);
                CompareModelObjects<PlanogramFixtureComponent>(planogram.Fixtures[i].Components, savedPlanogram.Fixtures[i].Components, ids);
            }
            CompareModelObjects<PlanogramAssembly>(planogram.Assemblies, savedPlanogram.Assemblies, ids);
            for (Int32 i = 0; i < planogram.Assemblies.Count; i++)
            {
                CompareModelObjects<PlanogramAssemblyComponent>(planogram.Assemblies[i].Components, savedPlanogram.Assemblies[i].Components, ids);
            }
            CompareModelObjects<PlanogramComponent>(planogram.Components, savedPlanogram.Components, ids);
            for (Int32 i = 0; i < planogram.Components.Count; i++)
            {
                CompareModelObjects<PlanogramSubComponent>(planogram.Components[i].SubComponents, savedPlanogram.Components[i].SubComponents, ids);
            }
            CompareModelObjects<PlanogramProduct>(planogram.Products, savedPlanogram.Products, ids);
            CompareModelObjects<PlanogramPosition>(planogram.Positions, savedPlanogram.Positions, ids);
            CompareModelObjects<PlanogramAnnotation>(planogram.Annotations, savedPlanogram.Annotations, ids);
            CompareModelObjects<PlanogramImage>(planogram.Images, savedPlanogram.Images, ids);

            CompareModelObjects<PlanogramPerformance>(planogram.Performance, savedPlanogram.Performance, ids);
            CompareModelObjects<PlanogramPerformanceData>(planogram.Performance.PerformanceData, savedPlanogram.Performance.PerformanceData, ids);
            CompareModelObjects<PlanogramPerformanceMetric>(planogram.Performance.Metrics, savedPlanogram.Performance.Metrics, ids);

            CompareModelObjects<PlanogramAssortment>(planogram.Assortment, savedPlanogram.Assortment, ids);
            CompareModelObjects<PlanogramAssortmentLocalProduct>(planogram.Assortment.LocalProducts, savedPlanogram.Assortment.LocalProducts, ids);
            CompareModelObjects<PlanogramAssortmentProduct>(planogram.Assortment.Products, savedPlanogram.Assortment.Products, ids);
            CompareModelObjects<PlanogramAssortmentRegion>(planogram.Assortment.Regions, savedPlanogram.Assortment.Regions, ids);
            for (Int32 i = 0; i < planogram.Assortment.Regions.Count; i++)
            {
                CompareModelObjects<PlanogramAssortmentRegionLocation>(planogram.Assortment.Regions[i].Locations, savedPlanogram.Assortment.Regions[i].Locations, ids);
                CompareModelObjects<PlanogramAssortmentRegionProduct>(planogram.Assortment.Regions[i].Products, savedPlanogram.Assortment.Regions[i].Products, ids);
            }
        }

        /// <summary>
        /// Checks properties on Planogram model objects for appropriate RelationshipType.
        /// </summary>
        /// <remarks>
        /// The process this method uses to identify lazy loaded properties is not perfect and will pick
        /// up properties that refer to other Planogram models that are not lazy loaded. It is, nonetheless
        /// usefull for ensuring most properties are appropriately labelled.
        /// </remarks>
        [Test]
        [TestCaseSource("_modelTypes")]
        [Explicit]
        public void AllLazyLoadPropertiesHaveRelationshipType(Type model)
        {
            foreach (var property in model.GetProperties())
            {
                if (!(property.PropertyType is Object) || 
                    !property.PropertyType.Name.StartsWith("Planogram") ||
                    property.PropertyType.Name.Contains("Type")) continue;
                var staticProperty = model.GetField(String.Format("{0}Property", property.Name));
                if (staticProperty == null) continue;
                var cslaStaticProperty = staticProperty.GetValue(model) as IPropertyInfo;
                if (cslaStaticProperty == null) continue;
                Assert.AreEqual(RelationshipTypes.LazyLoad,cslaStaticProperty.RelationshipType,String.Format("Lazy load not set for {0}",property.Name));
            }
        }

        /// <summary>
        /// Tests that a Package and all its children calculate their meta data correctly.
        /// </summary>
        [Test]
        public void TestCalculateMetadata()
        {
            throw new InconclusiveException("TODO: Split into smaller useful tests");

            //Calculate meta data to ensure everything is running/saving etc with no errors.
            #region Setup data
            Object id1;
            Object id2;
            Object id3;
            Object id4;
            PlanogramList planograms = PlanogramList.NewPlanogramList();
            planograms.Add(Planogram.NewPlanogram());
            Assert.IsNotNull(planograms.FindById(planograms[0].Id));
            planograms.Add(Planogram.NewPlanogram());
            Assert.IsNotNull(planograms.FindById(planograms[0].Id));
            Assert.IsNotNull(planograms.FindById(planograms[1].Id));
            planograms.AddRange(new List<Planogram> { Planogram.NewPlanogram(), Planogram.NewPlanogram() });
            Assert.IsNotNull(planograms.FindById(planograms[0].Id));
            Assert.IsNotNull(planograms.FindById(planograms[1].Id));
            Assert.IsNotNull(planograms.FindById(planograms[2].Id));
            Assert.IsNotNull(planograms.FindById(planograms[3].Id));
            id1 = planograms[0].Id;
            id2 = planograms[1].Id;
            id3 = planograms[2].Id;
            id4 = planograms[3].Id;
            planograms.RemoveAt(3);
            Assert.IsNotNull(planograms.FindById(id1));
            Assert.IsNotNull(planograms.FindById(id2));
            Assert.IsNotNull(planograms.FindById(id3));
            Assert.IsNull(planograms.FindById(id4));
            planograms.RemoveList(new List<Planogram> { planograms[1], planograms[2] });
            Assert.IsNotNull(planograms.FindById(id1));
            Assert.IsNull(planograms.FindById(id2));
            Assert.IsNull(planograms.FindById(id3));
            Assert.IsNull(planograms.FindById(id4));

            Int32 planogramCount = 0;
            Int32 planogramFixtureItemCount = 0;
            Int32 planogramFixtureCount = 0;
            Int32 planogramFixtureAssemblyCount = 0;
            Int32 planogramFixtureComponentCount = 0;
            Int32 planogramAssemblyCount = 0;
            Int32 planogramAssemblyComponentCount = 0;
            Int32 planogramComponentCount = 0;
            Int32 planogramSubComponentCount = 0;
            Int32 planogramProductCount = 0;
            Int32 planogramPositionCount = 0;
            Int32 planogramAnnotationCount = 0;
            Int32 planogramImageCount = 0;
            Int32 planogramId = 0;
            Int32 planogramFixtureItemId = 0;
            Int32 planogramFixtureId = 0;
            Int32 planogramFixtureAssemblyId = 0;
            Int32 planogramFixtureComponentId = 0;
            Int32 planogramAssemblyId = 0;
            Int32 planogramAssemblyComponentId = 0;
            Int32 planogramComponentId = 0;
            Int32 planogramSubComponentId = 0;
            Int32 planogramProductId = 0;
            Int32 planogramPositionId = 0;
            Int32 planogramAnnotationId = 0;
            Int32 planogramImageId = 0;
            Int32 planogramPerformanceMetricId = 0;
            Int32 planogramPerformanceDataId = 0;
            Int32 planogramAssortmentRegionLocationId = 0;
            Int32 planogramAssortmentRegionProductId = 0;
            Dictionary<Object, Object> ids = new Dictionary<Object, Object>();

            Galleria.Framework.Planograms.Model.Package package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "Test";
            ids[package.Id] = 1;
            package.Planograms.AddRange(CreateModelObjects<Planogram>(ids, ref planogramId));
            planogramCount = package.Planograms.Count;
            Planogram planogram = package.Planograms[0];
            planogram.Name = "Planogram";
            planogram.CustomAttributes.Text1 = "CustAttValue";
            planogram.FixtureItems.AddRange(CreateModelObjects<PlanogramFixtureItem>(ids, ref planogramFixtureItemId));
            planogramFixtureItemCount = planogram.FixtureItems.Count;
            planogram.Fixtures.AddRange(CreateModelObjects<PlanogramFixture>(ids, ref planogramFixtureId));
            planogramFixtureCount = planogram.Fixtures.Count;
            foreach (PlanogramFixture planogramFixture in planogram.Fixtures)
            {
                planogramFixture.Assemblies.AddRange(CreateModelObjects<PlanogramFixtureAssembly>(ids, ref planogramFixtureAssemblyId));
                planogramFixture.Components.AddRange(CreateModelObjects<PlanogramFixtureComponent>(ids, ref planogramFixtureComponentId));
            }
            planogramFixtureAssemblyCount = planogramFixtureCount * planogram.Fixtures[0].Assemblies.Count;
            planogramFixtureComponentCount = planogramFixtureCount * planogram.Fixtures[0].Components.Count;
            planogram.Assemblies.AddRange(CreateModelObjects<PlanogramAssembly>(ids, ref planogramAssemblyId));
            planogramAssemblyCount = planogram.Assemblies.Count;
            foreach (PlanogramAssembly planogramAssembly in planogram.Assemblies)
            {
                planogramAssembly.Components.AddRange(CreateModelObjects<PlanogramAssemblyComponent>(ids, ref planogramAssemblyComponentId));
            }
            planogramAssemblyComponentCount = planogramAssemblyCount * planogram.Assemblies[0].Components.Count;
            planogram.Components.AddRange(CreateModelObjects<PlanogramComponent>(ids, ref planogramComponentId));
            planogramComponentCount = planogram.Components.Count;
            foreach (PlanogramComponent planogramComponent in planogram.Components)
            {
                planogramComponent.SubComponents.AddRange(CreateModelObjects<PlanogramSubComponent>(ids, ref planogramSubComponentId));
            }
            planogramSubComponentCount = planogramComponentCount * planogram.Components[0].SubComponents.Count;
            planogram.Products.AddRange(CreateModelObjects<PlanogramProduct>(ids, ref planogramProductId));
            planogram.Products.AddRange(CreateModelObjects<PlanogramProduct>(ids, ref planogramProductId));
            planogram.Products.AddRange(CreateModelObjects<PlanogramProduct>(ids, ref planogramProductId));
            planogram.Products.AddRange(CreateModelObjects<PlanogramProduct>(ids, ref planogramProductId));
            planogram.Products.AddRange(CreateModelObjects<PlanogramProduct>(ids, ref planogramProductId));
            //clear performance
            planogram.Performance.PerformanceData.Clear();
            planogramProductCount = planogram.Products.Count;
            planogram.Positions.AddRange(CreateModelObjects<PlanogramPosition>(ids, ref planogramPositionId));
            planogram.Positions.AddRange(CreateModelObjects<PlanogramPosition>(ids, ref planogramPositionId));
            planogramPositionCount = planogram.Positions.Count;
            planogram.Annotations.AddRange(CreateModelObjects<PlanogramAnnotation>(ids, ref planogramAnnotationId));
            planogramAnnotationCount = planogram.Annotations.Count;
            planogram.Images.AddRange(CreateModelObjects<PlanogramImage>(ids, ref planogramImageId));
            planogramImageCount = planogram.Images.Count;
            planogram.Performance.Name = "Performance";
            ids.Add(planogram.Performance.Id, 1);
            for (Byte i = 1; i <= 20; i++)
            {
                planogramPerformanceMetricId++;
                var planogramPerformanceMetric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(i);
                planogramPerformanceMetric.Name = String.Format("PerformanceMetric{0}", i);
                planogram.Performance.Metrics.Add(planogramPerformanceMetric);
                ids.Add(planogramPerformanceMetric.Id, planogramPerformanceMetricId);
            }

            planogram.Assortment.Name = "Assortment";
            ids.Add(planogram.Assortment.Id, 1);
            for (Int32 i = 1; i <= 10; i++)
            {
                var item = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
                item.Gtin = String.Format("Gtin{0}", i);
                //item.LocationCode= String.Format("LocationCode{0}", i);
                planogram.Assortment.Products.Add(item);
                ids.Add(item.Id, i);
            }

            for (Int32 i = 1; i <= 10; i++)
            {
                var item = PlanogramAssortmentLocalProduct.NewPlanogramAssortmentLocalProduct();
                item.ProductGtin = String.Format("Gtin{0}", i);
                item.LocationCode = String.Format("LocationCode{0}", i);
                planogram.Assortment.LocalProducts.Add(item);
                ids.Add(item.Id, i);
            }

            for (Int32 i = 1; i <= 10; i++)
            {
                var item = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();
                item.Name = String.Format("AssortmentRegion{0}", i);
                planogram.Assortment.Regions.Add(item);
                ids.Add(item.Id, i);
            }

            foreach (var region in planogram.Assortment.Regions)
            {
                for (Int32 i = 1; i <= 10; i++)
                {
                    planogramAssortmentRegionLocationId++;
                    var item = PlanogramAssortmentRegionLocation.NewPlanogramAssortmentRegionLocation();
                    item.LocationCode = String.Format("LocationCode{0}", i);
                    region.Locations.Add(item);
                    ids.Add(item.Id, planogramAssortmentRegionLocationId);
                }

                for (Int32 i = 1; i <= 10; i++)
                {
                    planogramAssortmentRegionProductId++;
                    var item = PlanogramAssortmentRegionProduct.NewPlanogramAssortmentRegionProduct();
                    item.PrimaryProductGtin = String.Format("ProductGtin{0}", i);
                    region.Products.Add(item);
                    ids.Add(item.Id, planogramAssortmentRegionProductId);
                }
            }

            Int32 fixtureItemIdCounter = 0;
            Int32 fixtureAssemblyIdCounter1 = 0;
            Int32 fixtureAssemblyIdCounter2 = 0;
            Int32 fixtureComponentIdCounter1 = 0;
            Int32 fixtureComponentIdCounter2 = 0;
            Int32 assemblyComponentIdCounter1 = 0;
            Int32 assemblyComponentIdCounter2 = 0;
            Int32 subComponentIdCounter1 = 0;
            Int32 subComponentIdCounter2 = 0;
            Int32 positionCounter = 0;
            Int32 componentIdCounter = 0;
            Int32 imageIdCounter = 0;
            Int32 assemblyIdCounter = 0;
            Int32 fixtureIdCounter = 0;
            Int32 productIdCounter = 0;

            #region Annotations
            foreach (PlanogramAnnotation planogramAnnotation in planogram.Annotations)
            {
                planogramAnnotation.PlanogramFixtureItemId = planogram.FixtureItems[fixtureItemIdCounter].Id;
                fixtureItemIdCounter++;
                if (fixtureItemIdCounter == planogram.FixtureItems.Count)
                {
                    fixtureItemIdCounter = 0;
                }
                planogramAnnotation.PlanogramFixtureAssemblyId = planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies[fixtureAssemblyIdCounter2].Id;
                fixtureAssemblyIdCounter2++;
                if (fixtureAssemblyIdCounter2 == planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies.Count)
                {
                    fixtureAssemblyIdCounter2 = 0;
                    fixtureAssemblyIdCounter1++;
                    if (fixtureAssemblyIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureAssemblyIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramFixtureComponentId = planogram.Fixtures[fixtureComponentIdCounter1].Components[fixtureComponentIdCounter2].Id;
                fixtureComponentIdCounter2++;
                if (fixtureComponentIdCounter2 == planogram.Fixtures[fixtureComponentIdCounter1].Components.Count)
                {
                    fixtureComponentIdCounter2 = 0;
                    fixtureComponentIdCounter1++;
                    if (fixtureComponentIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureComponentIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramAssemblyComponentId = planogram.Assemblies[assemblyComponentIdCounter1].Components[assemblyComponentIdCounter2].Id;
                assemblyComponentIdCounter2++;
                if (assemblyComponentIdCounter2 == planogram.Assemblies[assemblyComponentIdCounter1].Components.Count)
                {
                    assemblyComponentIdCounter2 = 0;
                    assemblyComponentIdCounter1++;
                    if (assemblyComponentIdCounter1 == planogram.Assemblies.Count)
                    {
                        assemblyComponentIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramSubComponentId = planogram.Components[subComponentIdCounter1].SubComponents[subComponentIdCounter2].Id;
                subComponentIdCounter2++;
                if (subComponentIdCounter2 == planogram.Components[subComponentIdCounter1].SubComponents.Count)
                {
                    subComponentIdCounter2 = 0;
                    subComponentIdCounter1++;
                    if (subComponentIdCounter1 == planogram.Components.Count)
                    {
                        subComponentIdCounter1 = 0;
                    }
                }
                planogramAnnotation.PlanogramPositionId = planogram.Positions[positionCounter].Id;
                positionCounter++;
                if (positionCounter == planogram.Positions.Count)
                {
                    positionCounter = 0;
                }
            }
            #endregion
            #region AssemblyComponents
            foreach (PlanogramAssembly planogramAssembly in planogram.Assemblies)
            {
                foreach (PlanogramAssemblyComponent planogramAssemblyComponent in planogramAssembly.Components)
                {
                    planogramAssemblyComponent.PlanogramComponentId = planogram.Components[componentIdCounter].Id;
                    componentIdCounter++;
                    if (componentIdCounter == planogram.Components.Count)
                    {
                        componentIdCounter = 0;
                    }
                }
            }
            #endregion
            #region Components/SubComponents
            foreach (PlanogramComponent planogramComponent in planogram.Components)
            {
                planogramComponent.ImageIdFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramComponent.ImageIdRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                foreach (PlanogramSubComponent planogramSubComponent in planogramComponent.SubComponents)
                {
                    planogramSubComponent.ImageIdFront = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdBack = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdTop = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdBottom = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdLeft = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                    planogramSubComponent.ImageIdRight = planogram.Images[imageIdCounter].Id;
                    imageIdCounter++;
                    if (imageIdCounter == planogram.Images.Count)
                    {
                        imageIdCounter = 0;
                    }
                }
            }
            #endregion
            #region FixtureAssemblies/FixtureComponents
            foreach (PlanogramFixture planogramFixture in planogram.Fixtures)
            {
                foreach (PlanogramFixtureAssembly planogramFixtureAssembly in planogramFixture.Assemblies)
                {
                    planogramFixtureAssembly.PlanogramAssemblyId = planogram.Assemblies[assemblyIdCounter].Id;
                    assemblyIdCounter++;
                    if (assemblyIdCounter == planogram.Assemblies.Count)
                    {
                        assemblyIdCounter = 0;
                    }
                }
                foreach (PlanogramFixtureComponent planogramFixtureComponent in planogramFixture.Components)
                {
                    planogramFixtureComponent.PlanogramComponentId = planogram.Components[componentIdCounter].Id;
                    componentIdCounter++;
                    if (componentIdCounter == planogram.Components.Count)
                    {
                        componentIdCounter = 0;
                    }
                }
            }
            #endregion
            #region FixtureItems
            foreach (PlanogramFixtureItem planogramFixtureItem in planogram.FixtureItems)
            {
                planogramFixtureItem.PlanogramFixtureId = planogram.Fixtures[fixtureIdCounter].Id;
                fixtureIdCounter++;
                if (fixtureIdCounter == planogram.Fixtures.Count)
                {
                    fixtureIdCounter = 0;
                }
            }
            #endregion
            #region Positions
            foreach (PlanogramPosition planogramPosition in planogram.Positions)
            {
                planogramPosition.PlanogramProductId = planogram.Products[productIdCounter].Id;
                productIdCounter++;
                if (productIdCounter == planogram.Products.Count)
                {
                    productIdCounter = 0;
                }
                planogramPosition.PlanogramFixtureItemId = planogram.FixtureItems[fixtureItemIdCounter].Id;
                fixtureItemIdCounter++;
                if (fixtureItemIdCounter == planogram.FixtureItems.Count)
                {
                    fixtureItemIdCounter = 0;
                }
                planogramPosition.PlanogramFixtureAssemblyId = planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies[fixtureAssemblyIdCounter2].Id;
                fixtureAssemblyIdCounter2++;
                if (fixtureAssemblyIdCounter2 == planogram.Fixtures[fixtureAssemblyIdCounter1].Assemblies.Count)
                {
                    fixtureAssemblyIdCounter2 = 0;
                    fixtureAssemblyIdCounter1++;
                    if (fixtureAssemblyIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureAssemblyIdCounter1 = 0;
                    }
                }
                planogramPosition.PlanogramAssemblyComponentId = planogram.Assemblies[assemblyComponentIdCounter1].Components[assemblyComponentIdCounter2].Id;
                assemblyComponentIdCounter2++;
                if (assemblyComponentIdCounter2 == planogram.Assemblies[assemblyComponentIdCounter1].Components.Count)
                {
                    assemblyComponentIdCounter2 = 0;
                    assemblyComponentIdCounter1++;
                    if (assemblyComponentIdCounter1 == planogram.Assemblies.Count)
                    {
                        assemblyComponentIdCounter1 = 0;
                    }
                }
                planogramPosition.PlanogramFixtureComponentId = planogram.Fixtures[fixtureComponentIdCounter1].Components[fixtureComponentIdCounter2].Id;
                fixtureComponentIdCounter2++;
                if (fixtureComponentIdCounter2 == planogram.Fixtures[fixtureComponentIdCounter1].Components.Count)
                {
                    fixtureComponentIdCounter2 = 0;
                    fixtureComponentIdCounter1++;
                    if (fixtureComponentIdCounter1 == planogram.Fixtures.Count)
                    {
                        fixtureComponentIdCounter1 = 0;
                    }
                }
                planogramPosition.PlanogramSubComponentId = planogram.Components[subComponentIdCounter1].SubComponents[subComponentIdCounter2].Id;
                subComponentIdCounter2++;
                if (subComponentIdCounter2 == planogram.Components[subComponentIdCounter1].SubComponents.Count)
                {
                    subComponentIdCounter2 = 0;
                    subComponentIdCounter1++;
                    if (subComponentIdCounter1 == planogram.Components.Count)
                    {
                        subComponentIdCounter1 = 0;
                    }
                }
            }
            #endregion
            #region Products
            foreach (PlanogramProduct planogramProduct in planogram.Products)
            {
                //Unit Images
                planogramProduct.PlanogramImageIdFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Display Images
                planogramProduct.PlanogramImageIdDisplayFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdDisplayRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Tray Images
                planogramProduct.PlanogramImageIdTrayFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdTrayRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }


                //PointOfPurchase Images
                planogramProduct.PlanogramImageIdPointOfPurchaseFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdPointOfPurchaseRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Alternate Images
                planogramProduct.PlanogramImageIdAlternateFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdAlternateRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

                //Case Images
                planogramProduct.PlanogramImageIdCaseFront = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseBack = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseTop = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseBottom = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseLeft = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }
                planogramProduct.PlanogramImageIdCaseRight = planogram.Images[imageIdCounter].Id;
                imageIdCounter++;
                if (imageIdCounter == planogram.Images.Count)
                {
                    imageIdCounter = 0;
                }

            }
            #endregion

            package.UserName = "UserName";
            #endregion

            Galleria.Framework.Planograms.Model.Package savedPackage = null;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            savedPackage = package;//.Save();
            //stopwatch.Stop();
            //Debug.WriteLine(String.Format("Milliseconds to save: {0}", stopwatch.ElapsedMilliseconds));
            //stopwatch.Reset();
            //stopwatch.Start();
            //savedPackage = Galleria.Framework.Planograms.Model.Package.FetchById(savedPackage.Id, 1, PackageLockType.User);
            //stopwatch.Stop();
            //Debug.WriteLine(String.Format("Milliseconds to load: {0}", stopwatch.ElapsedMilliseconds));

            //register the image renderer
            PlanogramImagesHelper.RegisterPlanogramImageRenderer<Galleria.Framework.Planograms.Controls.Wpf.Helpers.PlanogramImageRenderer>();
            
            //Run through calculate meta data
            stopwatch.Start();
            savedPackage.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            stopwatch.Stop();
            Debug.WriteLine(String.Format("Milliseconds to calculate meta data: {0}", stopwatch.ElapsedMilliseconds));

            //refresh planogram to test
            planogram = savedPackage.Planograms[0];

            //Test properties throughout structure
            Assert.IsNotNull(savedPackage.DateMetadataCalculated);
            Assert.AreEqual(DateTime.UtcNow.Date, savedPackage.DateMetadataCalculated.Value.Date);
            Assert.AreEqual(planogramCount, savedPackage.MetaPlanogramCount);

            #region Planogram value tests
            Assert.IsNotNull(planogram.MetaAverageCases);
            Assert.AreEqual(17, planogram.MetaAverageCases);

            Assert.IsNotNull(planogram.MetaTotalComponentCollisions);
            Assert.AreEqual(2311920, planogram.MetaTotalComponentCollisions);

            Assert.IsNotNull(planogram.MetaTotalPositionCollisions);
            Assert.AreEqual(82, planogram.MetaTotalPositionCollisions);

            Assert.IsNotNull(planogram.MetaBayCount);
            Assert.AreEqual(planogram.FixtureItems.Count, planogram.MetaBayCount);

            Assert.IsNotNull(planogram.MetaUniqueProductCount);
            Assert.AreEqual(planogram.Products.Count, planogram.MetaUniqueProductCount);

            Assert.IsNotNull(planogram.MetaComponentCount);
            Assert.AreEqual(planogram.Components.Count, planogram.MetaComponentCount);

            Assert.IsNotNull(planogram.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(5995.45f, planogram.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(planogram.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(planogram.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(planogram.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(0, planogram.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(planogram.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(0, planogram.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(planogram.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(0, planogram.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(planogram.MetaProductsPlaced);
            Assert.AreEqual(82, planogram.MetaProductsPlaced);

            Assert.IsNotNull(planogram.MetaProductsUnplaced);
            Assert.AreEqual(538, planogram.MetaProductsUnplaced);

            Assert.IsNotNull(planogram.MetaNotAchievedInventory);
            Assert.AreEqual(0, planogram.MetaNotAchievedInventory);

            Assert.IsNotNull(planogram.MetaTotalFacings);
            Assert.AreEqual(15580, planogram.MetaTotalFacings);

            Assert.IsNotNull(planogram.MetaAverageFacings);
            Assert.AreEqual(190, planogram.MetaAverageFacings);

            Assert.IsNotNull(planogram.MetaTotalUnits);
            Assert.AreEqual(17989406, planogram.MetaTotalUnits);

            Assert.IsNotNull(planogram.MetaAverageUnits);
            Assert.AreEqual(219383, planogram.MetaAverageUnits);

            Assert.IsNotNull(planogram.MetaMinCases);
            Assert.AreEqual(17, planogram.MetaMinCases);

            Assert.IsNotNull(planogram.MetaMaxCases);
            Assert.AreEqual(17, planogram.MetaMaxCases);

            Assert.IsNotNull(planogram.MetaTotalComponentsOverMerchandisedWidth);
            Assert.AreEqual(82, planogram.MetaTotalComponentsOverMerchandisedWidth);

            Assert.IsNotNull(planogram.MetaTotalComponentsOverMerchandisedHeight);
            Assert.AreEqual(82, planogram.MetaTotalComponentsOverMerchandisedHeight);

            Assert.IsNotNull(planogram.MetaTotalComponentsOverMerchandisedDepth);
            Assert.AreEqual(82, planogram.MetaTotalComponentsOverMerchandisedDepth);

            Assert.IsNotNull(planogram.MetaTotalFrontFacings);
            Assert.AreEqual(15580, planogram.MetaTotalFrontFacings);

            Assert.IsNotNull(planogram.MetaAverageFrontFacings);
            Assert.AreEqual(190, planogram.MetaAverageFrontFacings);

            #endregion

            #region Planogram assembly component value tests

            PlanogramAssemblyComponent planogramAssComp = planogram.Assemblies[0].Components[0];

            Assert.IsNotNull(planogramAssComp.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(214.12f, planogramAssComp.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(planogramAssComp.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(4447325, planogramAssComp.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(planogramAssComp.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(952275136, planogramAssComp.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(planogramAssComp.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(100, planogramAssComp.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(planogramAssComp.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(100, planogramAssComp.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(planogramAssComp.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(100, planogramAssComp.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(planogramAssComp.MetaProductsPlaced);
            Assert.AreEqual(0, planogramAssComp.MetaProductsPlaced);

            Assert.IsNotNull(planogramAssComp.MetaNotAchievedInventory);
            Assert.AreEqual(0, planogramAssComp.MetaNotAchievedInventory);

            Assert.IsNotNull(planogramAssComp.MetaTotalFacings);
            Assert.AreEqual(0, planogramAssComp.MetaTotalFacings);

            Assert.IsNotNull(planogramAssComp.MetaAverageFacings);
            Assert.AreEqual(0, planogramAssComp.MetaAverageFacings);

            Assert.IsNotNull(planogramAssComp.MetaTotalUnits);
            Assert.AreEqual(0, planogramAssComp.MetaTotalUnits);

            Assert.IsNotNull(planogramAssComp.MetaAverageUnits);
            Assert.AreEqual(0, planogramAssComp.MetaAverageUnits);

            Assert.IsNotNull(planogramAssComp.MetaMinCases);
            Assert.AreEqual(0, planogramAssComp.MetaMinCases);

            Assert.IsNotNull(planogramAssComp.MetaAverageCases);
            Assert.AreEqual(0, planogramAssComp.MetaAverageCases);

            Assert.IsNotNull(planogramAssComp.MetaMaxCases);
            Assert.AreEqual(0, planogramAssComp.MetaMaxCases);

            Assert.IsNotNull(planogramAssComp.MetaTotalFrontFacings);
            Assert.AreEqual(0, planogramAssComp.MetaTotalFrontFacings);

            Assert.IsNotNull(planogramAssComp.MetaAverageFrontFacings);
            Assert.AreEqual(0, planogramAssComp.MetaAverageFrontFacings);
            #endregion

            #region Planogram component value tests
                        
            PlanogramComponent planogramComp = planogram.Components[0];
            
            Assert.IsNotNull(planogramComp.MetaNumberOfSubComponents);
            Assert.AreEqual(97, planogramComp.MetaNumberOfSubComponents);

            Assert.IsNotNull(planogramComp.MetaNumberOfMerchandisedSubComponents);
            Assert.AreEqual(97, planogramComp.MetaNumberOfMerchandisedSubComponents);

            #endregion

            #region Planogram fixture value tests
            
            PlanogramFixture planogramFix = planogram.Fixtures[0];

            Assert.IsNotNull(planogramFix.MetaNumberOfMerchandisedSubComponents);
            Assert.AreEqual(3686, planogramFix.MetaNumberOfMerchandisedSubComponents);

            Assert.IsNotNull(planogramFix.MetaTotalFixtureCost);
            Assert.AreEqual(8136.68f, planogramFix.MetaTotalFixtureCost);
            
            #endregion

            #region Planogram fixture assembly value tests

            PlanogramFixtureAssembly planogramFixAss = planogram.Fixtures[0].Assemblies[0];


            Assert.IsNotNull(planogramFixAss.MetaComponentCount);
            Assert.AreEqual(33, planogramFixAss.MetaComponentCount);

            Assert.IsNotNull(planogramFixAss.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(214.12f, planogramFixAss.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(planogramFixAss.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(planogramFixAss.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(planogramFixAss.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(100, planogramFixAss.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(planogramFixAss.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(100, planogramFixAss.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(planogramFixAss.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(100, planogramFixAss.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(planogramFixAss.MetaProductsPlaced);
            Assert.AreEqual(0, planogramFixAss.MetaProductsPlaced);

            Assert.IsNotNull(planogramFixAss.MetaNotAchievedInventory);
            Assert.AreEqual(0, planogramFixAss.MetaNotAchievedInventory);

            Assert.IsNotNull(planogramFixAss.MetaTotalFacings);
            Assert.AreEqual(0, planogramFixAss.MetaTotalFacings);

            Assert.IsNotNull(planogramFixAss.MetaAverageFacings);
            Assert.AreEqual(0, planogramFixAss.MetaAverageFacings);

            Assert.IsNotNull(planogramFixAss.MetaTotalUnits);
            Assert.AreEqual(0, planogramFixAss.MetaTotalUnits);

            Assert.IsNotNull(planogramFixAss.MetaAverageUnits);
            Assert.AreEqual(0, planogramFixAss.MetaAverageUnits);

            Assert.IsNotNull(planogramFixAss.MetaMinCases);
            Assert.AreEqual(0, planogramFixAss.MetaMinCases);

            Assert.IsNotNull(planogramFixAss.MetaAverageCases);
            Assert.AreEqual(0, planogramFixAss.MetaAverageCases);

            Assert.IsNotNull(planogramFixAss.MetaMaxCases);
            Assert.AreEqual(0, planogramFixAss.MetaMaxCases);

            Assert.IsNotNull(planogramFixAss.MetaTotalFrontFacings);
            Assert.AreEqual(0, planogramFixAss.MetaTotalFrontFacings);

            Assert.IsNotNull(planogramFixAss.MetaAverageFrontFacings);
            Assert.AreEqual(0, planogramFixAss.MetaAverageFrontFacings);
            
            #endregion
            
            #region Planogram fixture component values tests

            PlanogramFixtureComponent planogramFixComp = planogram.Fixtures[0].Components[0];

            Assert.IsNotNull(planogramFixComp.MetaTotalComponentCollisions);
            Assert.AreEqual(272, planogramFixComp.MetaTotalComponentCollisions);

            Assert.IsNotNull(planogramFixComp.MetaTotalPositionCollisions);
            Assert.AreEqual(0, planogramFixComp.MetaTotalPositionCollisions);

            Assert.IsNotNull(planogramFixComp.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(214.12f, planogramFixComp.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(planogramFixComp.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(4447325.0f, planogramFixComp.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(planogramFixComp.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(952275136.0f, planogramFixComp.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(planogramFixComp.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(100, planogramFixComp.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(planogramFixComp.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(100, planogramFixComp.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(planogramFixComp.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(100, planogramFixComp.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(planogramFixComp.MetaProductsPlaced);
            Assert.AreEqual(0, planogramFixComp.MetaProductsPlaced);

            Assert.IsNotNull(planogramFixComp.MetaNotAchievedInventory);
            Assert.AreEqual(0, planogramFixComp.MetaNotAchievedInventory);

            Assert.IsNotNull(planogramFixComp.MetaTotalFacings);
            Assert.AreEqual(0, planogramFixComp.MetaTotalFacings);

            Assert.IsNotNull(planogramFixComp.MetaAverageFacings);
            Assert.AreEqual(0, planogramFixComp.MetaAverageFacings);

            Assert.IsNotNull(planogramFixComp.MetaTotalUnits);
            Assert.AreEqual(0, planogramFixComp.MetaTotalUnits);

            Assert.IsNotNull(planogramFixComp.MetaAverageUnits);
            Assert.AreEqual(0, planogramFixComp.MetaAverageUnits);

            Assert.IsNotNull(planogramFixComp.MetaMinCases);
            Assert.AreEqual(0, planogramFixComp.MetaMinCases);

            Assert.IsNotNull(planogramFixComp.MetaAverageCases);
            Assert.AreEqual(0, planogramFixComp.MetaAverageCases);

            Assert.IsNotNull(planogramFixComp.MetaMaxCases);
            Assert.AreEqual(0, planogramFixComp.MetaMaxCases);

            Assert.IsNotNull(planogramFixComp.MetaIsOverMerchandisedWidth);
            Assert.AreEqual(false, planogramFixComp.MetaIsOverMerchandisedWidth);

            Assert.IsNotNull(planogramFixComp.MetaIsOverMerchandisedHeight);
            Assert.AreEqual(false, planogramFixComp.MetaIsOverMerchandisedHeight);

            Assert.IsNotNull(planogramFixComp.MetaIsOverMerchandisedDepth);
            Assert.AreEqual(false, planogramFixComp.MetaIsOverMerchandisedDepth);

            Assert.IsNotNull(planogramFixComp.MetaIsComponentSlopeWithNoRiser);
            Assert.AreEqual(true, planogramFixComp.MetaIsComponentSlopeWithNoRiser);

            Assert.IsNotNull(planogramFixComp.MetaTotalFrontFacings);
            Assert.AreEqual(0, planogramFixComp.MetaTotalFrontFacings);

            Assert.IsNotNull(planogramFixComp.MetaAverageFrontFacings);
            Assert.AreEqual(0, planogramFixComp.MetaAverageFrontFacings);

            #endregion

            #region Planogram Fixture Item value tests

            PlanogramFixtureItem planogramFixItem = planogram.FixtureItems[0];

            Assert.IsNotNull(planogramFixItem.MetaTotalComponentCollisions);
            Assert.AreEqual(10336, planogramFixItem.MetaTotalComponentCollisions);

            Assert.IsNotNull(planogramFixItem.MetaTotalPositionCollisions);
            Assert.AreEqual(2, planogramFixItem.MetaTotalPositionCollisions);

            Assert.IsNotNull(planogramFixItem.MetaComponentCount);
            Assert.AreEqual(38, planogramFixItem.MetaComponentCount);

            Assert.IsNotNull(planogramFixItem.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(214.12f, planogramFixItem.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(planogramFixItem.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(45848.71f, planogramFixItem.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(planogramFixItem.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(9817269.0f, planogramFixItem.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(planogramFixItem.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(0, planogramFixItem.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(planogramFixItem.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(0, planogramFixItem.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(planogramFixItem.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(0, planogramFixItem.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(planogramFixItem.MetaProductsPlaced);
            Assert.AreEqual(2, planogramFixItem.MetaProductsPlaced);

            Assert.IsNotNull(planogramFixItem.MetaNotAchievedInventory);
            Assert.AreEqual(0, planogramFixItem.MetaNotAchievedInventory);

            Assert.IsNotNull(planogramFixItem.MetaTotalFacings);
            Assert.AreEqual(380, planogramFixItem.MetaTotalFacings);

            Assert.IsNotNull(planogramFixItem.MetaAverageFacings);
            Assert.AreEqual(190, planogramFixItem.MetaAverageFacings);

            Assert.IsNotNull(planogramFixItem.MetaTotalUnits);
            Assert.AreEqual(438766, planogramFixItem.MetaTotalUnits);

            Assert.IsNotNull(planogramFixItem.MetaAverageUnits);
            Assert.AreEqual(219383, planogramFixItem.MetaAverageUnits);

            Assert.IsNotNull(planogramFixItem.MetaMinCases);
            Assert.AreEqual(17, planogramFixItem.MetaMinCases);

            Assert.IsNotNull(planogramFixItem.MetaAverageCases);
            Assert.AreEqual(17, planogramFixItem.MetaAverageCases);

            Assert.IsNotNull(planogramFixItem.MetaMaxCases);
            Assert.AreEqual(17, planogramFixItem.MetaMaxCases);

            Assert.IsNotNull(planogramFixItem.MetaTotalComponentsOverMerchandisedWidth);
            Assert.AreEqual(22, planogramFixItem.MetaTotalComponentsOverMerchandisedWidth);

            Assert.IsNotNull(planogramFixItem.MetaTotalComponentsOverMerchandisedHeight);
            Assert.AreEqual(22, planogramFixItem.MetaTotalComponentsOverMerchandisedHeight);

            Assert.IsNotNull(planogramFixItem.MetaTotalComponentsOverMerchandisedDepth);
            Assert.AreEqual(22, planogramFixItem.MetaTotalComponentsOverMerchandisedDepth);

            Assert.IsNotNull(planogramFixItem.MetaTotalFrontFacings);
            Assert.AreEqual(380, planogramFixItem.MetaTotalFrontFacings);

            Assert.IsNotNull(planogramFixItem.MetaAverageFrontFacings);
            Assert.AreEqual(190, planogramFixItem.MetaAverageFrontFacings);

            #endregion

            #region Planogram Position value tests

            PlanogramPosition planogramPos = planogram.Positions[0];

            Assert.IsNotNull(planogramPos.MetaIsPositionCollisions);
            Assert.AreEqual(true, planogramPos.MetaIsPositionCollisions);

            Assert.IsNotNull(planogramPos.MetaAchievedCases);
            Assert.AreEqual(17, planogramPos.MetaAchievedCases);

            Assert.IsNotNull(planogramPos.MetaFrontFacingsWide);
            Assert.AreEqual(190, planogramPos.MetaFrontFacingsWide);

            #endregion

            #region Planogram product value tests

            PlanogramProduct planoProduct = planogram.Products[0];

            Assert.IsNotNull(planoProduct.MetaNotAchievedInventory);
            Assert.AreEqual(false, planoProduct.MetaNotAchievedInventory);

            #endregion
        }

        #region Meta Data Tests
        
        [Test]
        public void TestPlanogramMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];

            Assert.IsNotNull(planogram.MetaAverageCases);
            Assert.AreEqual(17, planogram.MetaAverageCases);

            Assert.IsNotNull(planogram.MetaTotalComponentCollisions);
            Assert.AreEqual(0, planogram.MetaTotalComponentCollisions);

            Assert.IsNotNull(planogram.MetaTotalPositionCollisions);
            Assert.AreEqual(10, planogram.MetaTotalPositionCollisions);

            Assert.IsNotNull(planogram.MetaBayCount);
            Assert.AreEqual(planogram.FixtureItems.Count, planogram.MetaBayCount);

            Assert.IsNotNull(planogram.MetaUniqueProductCount);
            Assert.AreEqual(planogram.Products.Count, planogram.MetaUniqueProductCount);

            Assert.IsNotNull(planogram.MetaComponentCount);
            Assert.AreEqual(planogram.Components.Count, planogram.MetaComponentCount);

            Assert.IsNotNull(planogram.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(200f, planogram.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(planogram.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(2000F, planogram.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(planogram.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(150000F, planogram.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(planogram.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(0.75F, planogram.MetaTotalLinearWhiteSpace);
            
            Assert.IsNotNull(planogram.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(0.75F, planogram.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(planogram.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(0.99F, planogram.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(planogram.MetaProductsPlaced);
            Assert.AreEqual(10, planogram.MetaProductsPlaced);

            Assert.IsNotNull(planogram.MetaProductsUnplaced);
            Assert.AreEqual(0, planogram.MetaProductsUnplaced);

            Assert.IsNotNull(planogram.MetaNotAchievedInventory);
            Assert.AreEqual(0, planogram.MetaNotAchievedInventory);

            Assert.IsNotNull(planogram.MetaTotalFacings);
            Assert.AreEqual(10, planogram.MetaTotalFacings);

            Assert.IsNotNull(planogram.MetaAverageFacings);
            Assert.AreEqual(1, planogram.MetaAverageFacings);

            Assert.IsNotNull(planogram.MetaTotalUnits);
            Assert.AreEqual(170, planogram.MetaTotalUnits);

            Assert.IsNotNull(planogram.MetaAverageUnits);
            Assert.AreEqual(17, planogram.MetaAverageUnits);

            Assert.IsNotNull(planogram.MetaMinCases);
            Assert.AreEqual(17, planogram.MetaMinCases);

            Assert.IsNotNull(planogram.MetaMaxCases);
            Assert.AreEqual(17, planogram.MetaMaxCases);

            Assert.IsNotNull(planogram.MetaTotalComponentsOverMerchandisedWidth);
            Assert.AreEqual(0, planogram.MetaTotalComponentsOverMerchandisedWidth);

            Assert.IsNotNull(planogram.MetaTotalComponentsOverMerchandisedHeight);
            Assert.AreEqual(1, planogram.MetaTotalComponentsOverMerchandisedHeight);

            Assert.IsNotNull(planogram.MetaTotalComponentsOverMerchandisedDepth);
            Assert.AreEqual(0, planogram.MetaTotalComponentsOverMerchandisedDepth);

            Assert.IsNotNull(planogram.MetaTotalFrontFacings);
            Assert.AreEqual(5, planogram.MetaTotalFrontFacings);

            Assert.IsNotNull(planogram.MetaAverageFrontFacings);
            Assert.AreEqual(0.5F, planogram.MetaAverageFrontFacings);

            Assert.IsNotNull(planogram.MetaNewProducts);
            Assert.AreEqual(0, planogram.MetaNewProducts);

            Assert.IsNotNull(planogram.MetaChangesFromPreviousCount);
            Assert.AreEqual(0, planogram.MetaChangesFromPreviousCount);

            Assert.IsNotNull(planogram.MetaChangeFromPreviousStarRating);
            Assert.AreEqual(0, planogram.MetaChangeFromPreviousStarRating);

            Assert.IsNotNull(planogram.MetaBlocksDropped);
            Assert.AreEqual(0, planogram.MetaBlocksDropped);

            Assert.IsNotNull(planogram.MetaMinDos);
            Assert.AreEqual(1.19f, planogram.MetaMinDos);

            Assert.IsNotNull(planogram.MetaMaxDos);
            Assert.AreEqual(11.9F, planogram.MetaMaxDos);

            Assert.IsNotNull(planogram.MetaAverageDos);
            Assert.AreEqual(3.485f, planogram.MetaAverageDos);
        }

        [Test]
        public void TestPlanogramPositionMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramPosition planogramPos = planogram.Positions[0];

            Assert.IsNotNull(planogramPos.MetaIsPositionCollisions);
            Assert.AreEqual(true, planogramPos.MetaIsPositionCollisions);

            Assert.IsNotNull(planogramPos.MetaAchievedCases);
            Assert.AreEqual(17, planogramPos.MetaAchievedCases);

            Assert.IsNotNull(planogramPos.MetaFrontFacingsWide);
            Assert.AreEqual(1, planogramPos.MetaFrontFacingsWide);

            Assert.IsNotNull(planogramPos.MetaIsUnderMinDeep);
            Assert.AreEqual(false, planogramPos.MetaIsUnderMinDeep);

            Assert.IsNotNull(planogramPos.MetaIsOverMaxDeep);
            Assert.AreEqual(false, planogramPos.MetaIsOverMaxDeep);

            Assert.IsNotNull(planogramPos.MetaIsOverMaxRightCap);
            Assert.AreEqual(false, planogramPos.MetaIsOverMaxRightCap);

            Assert.IsNotNull(planogramPos.MetaIsOverMaxStack);
            Assert.AreEqual(false, planogramPos.MetaIsOverMaxStack);

            Assert.IsNotNull(planogramPos.MetaIsOverMaxTopCap);
            Assert.AreEqual(false, planogramPos.MetaIsOverMaxTopCap);

            Assert.IsNotNull(planogramPos.MetaIsInvalidMerchandisingStyle);
            Assert.AreEqual(false, planogramPos.MetaIsInvalidMerchandisingStyle);

            Assert.IsNotNull(planogramPos.MetaIsHangingTray);
            Assert.AreEqual(false, planogramPos.MetaIsHangingTray);

            Assert.IsNotNull(planogramPos.MetaIsPegOverfilled);
            Assert.AreEqual(false, planogramPos.MetaIsPegOverfilled);

            Assert.IsNotNull(planogramPos.MetaIsOutsideMerchandisingSpace);
            Assert.AreEqual(true, planogramPos.MetaIsOutsideMerchandisingSpace);
        }

        [Test]
        public void TestPlanogramProductMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramProduct product = planogram.Products[0];

            Assert.IsNotNull(product.MetaNotAchievedInventory);
            Assert.AreEqual(false, product.MetaNotAchievedInventory);

            Assert.IsNotNull(product.MetaTotalUnits);
            Assert.AreEqual(17, product.MetaTotalUnits);

            Assert.IsNotNull(product.MetaPlanogramLinearSpacePercentage);
            Assert.AreEqual(0.025F, product.MetaPlanogramLinearSpacePercentage);

            Assert.IsNotNull(product.MetaPlanogramAreaSpacePercentage);
            Assert.AreEqual(0.025F, product.MetaPlanogramAreaSpacePercentage);

            Assert.IsNotNull(product.MetaPlanogramVolumetricSpacePercentage);
            Assert.AreEqual(0.0007F, product.MetaPlanogramVolumetricSpacePercentage);

            Assert.IsNotNull(product.MetaIsInMasterData);
            Assert.AreEqual(false, product.MetaIsInMasterData);
        }

        [Test]
        public void TestPlanogramAssemblyComponentMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramAssemblyComponent assemblyComponent = planogram.Assemblies[0].Components[0];


            Assert.IsNotNull(assemblyComponent.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(100F, assemblyComponent.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(assemblyComponent.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(1000F, assemblyComponent.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(assemblyComponent.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(75000F, assemblyComponent.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(assemblyComponent.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(1f, assemblyComponent.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(assemblyComponent.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(1f, assemblyComponent.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(assemblyComponent.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(1f, assemblyComponent.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(assemblyComponent.MetaProductsPlaced);
            Assert.AreEqual(0, assemblyComponent.MetaProductsPlaced);

            Assert.IsNotNull(assemblyComponent.MetaNotAchievedInventory);
            Assert.AreEqual(0, assemblyComponent.MetaNotAchievedInventory);

            Assert.IsNotNull(assemblyComponent.MetaTotalFacings);
            Assert.AreEqual(0, assemblyComponent.MetaTotalFacings);

            Assert.IsNotNull(assemblyComponent.MetaAverageFacings);
            Assert.AreEqual(0, assemblyComponent.MetaAverageFacings);

            Assert.IsNotNull(assemblyComponent.MetaTotalUnits);
            Assert.AreEqual(0, assemblyComponent.MetaTotalUnits);

            Assert.IsNotNull(assemblyComponent.MetaAverageUnits);
            Assert.AreEqual(0, assemblyComponent.MetaAverageUnits);

            Assert.IsNotNull(assemblyComponent.MetaMinCases);
            Assert.AreEqual(0, assemblyComponent.MetaMinCases);

            Assert.IsNotNull(assemblyComponent.MetaAverageCases);
            Assert.AreEqual(0, assemblyComponent.MetaAverageCases);

            Assert.IsNotNull(assemblyComponent.MetaMaxCases);
            Assert.AreEqual(0, assemblyComponent.MetaMaxCases);

            Assert.IsNotNull(assemblyComponent.MetaTotalFrontFacings);
            Assert.AreEqual(0, assemblyComponent.MetaTotalFrontFacings);

            Assert.IsNotNull(assemblyComponent.MetaAverageFrontFacings);
            Assert.AreEqual(0, assemblyComponent.MetaAverageFrontFacings);

            Assert.IsNotNull(assemblyComponent.MetaNewProducts);
            Assert.AreEqual(0, assemblyComponent.MetaNewProducts);

            Assert.IsNotNull(assemblyComponent.MetaChangesFromPreviousCount);
            Assert.AreEqual(0, assemblyComponent.MetaChangesFromPreviousCount);

            Assert.IsNotNull(assemblyComponent.MetaChangeFromPreviousStarRating);
            Assert.AreEqual(0, assemblyComponent.MetaChangeFromPreviousStarRating);

            Assert.IsNotNull(assemblyComponent.MetaBlocksDropped);
            Assert.AreEqual(0, assemblyComponent.MetaBlocksDropped);

            Assert.IsNotNull(assemblyComponent.MetaMinDos);
            Assert.AreEqual(0, assemblyComponent.MetaMinDos);

            Assert.IsNotNull(assemblyComponent.MetaMaxDos);
            Assert.AreEqual(0, assemblyComponent.MetaMaxDos);

            Assert.IsNotNull(assemblyComponent.MetaAverageDos);
            Assert.AreEqual(0, assemblyComponent.MetaAverageDos);

            Assert.IsNotNull(assemblyComponent.MetaSpaceToUnitsIndex);
            Assert.AreEqual(0, assemblyComponent.MetaSpaceToUnitsIndex);

            Assert.IsNotNull(assemblyComponent.MetaIsComponentSlopeWithNoRiser);
            Assert.AreEqual(false, assemblyComponent.MetaIsComponentSlopeWithNoRiser);

            Assert.IsNotNull(assemblyComponent.MetaIsOverMerchandisedDepth);
            Assert.AreEqual(false, assemblyComponent.MetaIsOverMerchandisedDepth);

            Assert.IsNotNull(assemblyComponent.MetaIsOverMerchandisedHeight);
            Assert.AreEqual(false, assemblyComponent.MetaIsOverMerchandisedHeight);

            Assert.IsNotNull(assemblyComponent.MetaIsOverMerchandisedWidth);
            Assert.AreEqual(false, assemblyComponent.MetaIsOverMerchandisedWidth);

            Assert.IsNotNull(assemblyComponent.MetaTotalPositionCollisions);
            Assert.AreEqual(0, assemblyComponent.MetaTotalPositionCollisions);

            Assert.IsNotNull(assemblyComponent.MetaTotalComponentCollisions);
            Assert.AreEqual(0, assemblyComponent.MetaTotalComponentCollisions);

            Assert.IsNotNull(assemblyComponent.MetaPercentageLinearSpaceFilled);
            Assert.AreEqual(0, assemblyComponent.MetaPercentageLinearSpaceFilled);
        }

        [Test]
        public void TestPlanogramComponentMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramComponent component = planogram.Components[0];

            Assert.IsNotNull(component.MetaNumberOfSubComponents);
            Assert.AreEqual(1, component.MetaNumberOfSubComponents);

            Assert.IsNotNull(component.MetaNumberOfMerchandisedSubComponents);
            Assert.AreEqual(1, component.MetaNumberOfMerchandisedSubComponents);
        }

        [Test]
        public void TestPlanogramFixtureMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramFixture fixture = planogram.Fixtures[0];

            Assert.IsNotNull(fixture.MetaNumberOfMerchandisedSubComponents);
            Assert.AreEqual(1, fixture.MetaNumberOfMerchandisedSubComponents);

            Assert.IsNotNull(fixture.MetaTotalFixtureCost);
            Assert.AreEqual(10F, fixture.MetaTotalFixtureCost);
        }

        [Test]
        public void TestPlanogramFixtureAssemblyMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramFixtureAssembly fixtureAssembly = planogram.Fixtures[0].Assemblies[0];

            Assert.IsNotNull(fixtureAssembly.MetaComponentCount);
            Assert.AreEqual(1, fixtureAssembly.MetaComponentCount);

            Assert.IsNotNull(fixtureAssembly.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(100F, fixtureAssembly.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(fixtureAssembly.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(1000F, fixtureAssembly.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(fixtureAssembly.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(75000F, fixtureAssembly.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(fixtureAssembly.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(1F, fixtureAssembly.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(fixtureAssembly.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(1F, fixtureAssembly.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(fixtureAssembly.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(1F, fixtureAssembly.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(fixtureAssembly.MetaNotAchievedInventory);
            Assert.AreEqual(0, fixtureAssembly.MetaNotAchievedInventory);

            Assert.IsNotNull(fixtureAssembly.MetaTotalFacings);
            Assert.AreEqual(0, fixtureAssembly.MetaTotalFacings);

            Assert.IsNotNull(fixtureAssembly.MetaAverageFacings);
            Assert.AreEqual(0, fixtureAssembly.MetaAverageFacings);

            Assert.IsNotNull(fixtureAssembly.MetaTotalUnits);
            Assert.AreEqual(0, fixtureAssembly.MetaTotalUnits);

            Assert.IsNotNull(fixtureAssembly.MetaAverageUnits);
            Assert.AreEqual(0, fixtureAssembly.MetaAverageUnits);

            Assert.IsNotNull(fixtureAssembly.MetaMinCases);
            Assert.AreEqual(0, fixtureAssembly.MetaMinCases);

            Assert.IsNotNull(fixtureAssembly.MetaAverageCases);
            Assert.AreEqual(0, fixtureAssembly.MetaAverageCases);

            Assert.IsNotNull(fixtureAssembly.MetaMaxCases);
            Assert.AreEqual(0, fixtureAssembly.MetaMaxCases);

            Assert.IsNotNull(fixtureAssembly.MetaTotalFrontFacings);
            Assert.AreEqual(0, fixtureAssembly.MetaTotalFrontFacings);

            Assert.IsNotNull(fixtureAssembly.MetaAverageFrontFacings);
            Assert.AreEqual(0, fixtureAssembly.MetaAverageFrontFacings);

            Assert.IsNotNull(fixtureAssembly.MetaProductsPlaced);
            Assert.AreEqual(0, fixtureAssembly.MetaProductsPlaced);

            Assert.IsNotNull(fixtureAssembly.MetaAverageDos);
            Assert.AreEqual(0, fixtureAssembly.MetaAverageDos);

            Assert.IsNotNull(fixtureAssembly.MetaBlocksDropped);
            Assert.AreEqual(0, fixtureAssembly.MetaBlocksDropped);

            Assert.IsNotNull(fixtureAssembly.MetaChangeFromPreviousStarRating);
            Assert.AreEqual(0, fixtureAssembly.MetaChangeFromPreviousStarRating);

            Assert.IsNotNull(fixtureAssembly.MetaChangesFromPreviousCount);
            Assert.AreEqual(0, fixtureAssembly.MetaChangesFromPreviousCount);

            Assert.IsNotNull(fixtureAssembly.MetaMaxDos);
            Assert.AreEqual(0, fixtureAssembly.MetaMaxDos);

            Assert.IsNotNull(fixtureAssembly.MetaMinDos);
            Assert.AreEqual(0, fixtureAssembly.MetaMinDos);

            Assert.IsNotNull(fixtureAssembly.MetaNewProducts);
            Assert.AreEqual(0, fixtureAssembly.MetaNewProducts);

            Assert.IsNotNull(fixtureAssembly.MetaSpaceToUnitsIndex);
            Assert.AreEqual(0, fixtureAssembly.MetaSpaceToUnitsIndex);
        }

        [Test]
        public void TestPlanogramFixtureComponentMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramFixtureComponent fixtureComponent = planogram.Fixtures[0].Components[0];

            Assert.IsNotNull(fixtureComponent.MetaTotalComponentCollisions);
            Assert.AreEqual(0, fixtureComponent.MetaTotalComponentCollisions);

            Assert.IsNotNull(fixtureComponent.MetaTotalPositionCollisions);
            Assert.AreEqual(10, fixtureComponent.MetaTotalPositionCollisions);

            Assert.IsNotNull(fixtureComponent.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(100F, fixtureComponent.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(fixtureComponent.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(1000F, fixtureComponent.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(fixtureComponent.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(75000F, fixtureComponent.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(fixtureComponent.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(0.5F, fixtureComponent.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(fixtureComponent.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(0.5F, fixtureComponent.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(fixtureComponent.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(0.99F, fixtureComponent.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(fixtureComponent.MetaProductsPlaced);
            Assert.AreEqual(10, fixtureComponent.MetaProductsPlaced);

            Assert.IsNotNull(fixtureComponent.MetaNotAchievedInventory);
            Assert.AreEqual(0, fixtureComponent.MetaNotAchievedInventory);

            Assert.IsNotNull(fixtureComponent.MetaTotalFacings);
            Assert.AreEqual(10, fixtureComponent.MetaTotalFacings);

            Assert.IsNotNull(fixtureComponent.MetaAverageFacings);
            Assert.AreEqual(1, fixtureComponent.MetaAverageFacings);

            Assert.IsNotNull(fixtureComponent.MetaTotalUnits);
            Assert.AreEqual(170, fixtureComponent.MetaTotalUnits);

            Assert.IsNotNull(fixtureComponent.MetaAverageUnits);
            Assert.AreEqual(17, fixtureComponent.MetaAverageUnits);

            Assert.IsNotNull(fixtureComponent.MetaMinCases);
            Assert.AreEqual(17, fixtureComponent.MetaMinCases);

            Assert.IsNotNull(fixtureComponent.MetaAverageCases);
            Assert.AreEqual(17, fixtureComponent.MetaAverageCases);

            Assert.IsNotNull(fixtureComponent.MetaMaxCases);
            Assert.AreEqual(17, fixtureComponent.MetaMaxCases);

            Assert.IsNotNull(fixtureComponent.MetaIsOverMerchandisedWidth);
            Assert.AreEqual(false, fixtureComponent.MetaIsOverMerchandisedWidth);

            Assert.IsNotNull(fixtureComponent.MetaIsOverMerchandisedHeight);
            Assert.AreEqual(true, fixtureComponent.MetaIsOverMerchandisedHeight);

            Assert.IsNotNull(fixtureComponent.MetaIsOverMerchandisedDepth);
            Assert.AreEqual(false, fixtureComponent.MetaIsOverMerchandisedDepth);

            Assert.IsNotNull(fixtureComponent.MetaIsComponentSlopeWithNoRiser);
            Assert.AreEqual(false, fixtureComponent.MetaIsComponentSlopeWithNoRiser);

            Assert.IsNotNull(fixtureComponent.MetaTotalFrontFacings);
            Assert.AreEqual(5, fixtureComponent.MetaTotalFrontFacings);

            Assert.IsNotNull(fixtureComponent.MetaAverageFrontFacings);
            Assert.AreEqual(0.5F, fixtureComponent.MetaAverageFrontFacings);

            Assert.IsNotNull(fixtureComponent.MetaAverageDos);
            Assert.AreEqual(0F, fixtureComponent.MetaAverageDos);

            Assert.IsNotNull(fixtureComponent.MetaBlocksDropped);
            Assert.AreEqual(0F, fixtureComponent.MetaBlocksDropped);

            Assert.IsNotNull(fixtureComponent.MetaChangeFromPreviousStarRating);
            Assert.AreEqual(0F, fixtureComponent.MetaChangeFromPreviousStarRating);

            Assert.IsNotNull(fixtureComponent.MetaChangesFromPreviousCount);
            Assert.AreEqual(0F, fixtureComponent.MetaChangesFromPreviousCount);

            Assert.IsNotNull(fixtureComponent.MetaMaxDos);
            Assert.AreEqual(0F, fixtureComponent.MetaMaxDos);

            Assert.IsNotNull(fixtureComponent.MetaMinDos);
            Assert.AreEqual(0F, fixtureComponent.MetaMinDos);

            Assert.IsNotNull(fixtureComponent.MetaNewProducts);
            Assert.AreEqual(0F, fixtureComponent.MetaNewProducts);

            Assert.IsNotNull(fixtureComponent.MetaSpaceToUnitsIndex);
            Assert.AreEqual(0F, fixtureComponent.MetaSpaceToUnitsIndex);

            Assert.IsNotNull(fixtureComponent.MetaTotalFrontFacings);
            Assert.AreEqual(5, fixtureComponent.MetaTotalFrontFacings);

            Assert.IsNotNull(fixtureComponent.MetaPercentageLinearSpaceFilled);
            Assert.AreEqual(0.5F, fixtureComponent.MetaPercentageLinearSpaceFilled);
        }

        [Test]
        public void TestPlanogramFixtureItemMetaData()
        {
            Package package = CreatePackage();
            Planogram planogram = package.Planograms[0];
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems[0];

            Assert.IsNotNull(fixtureItem.MetaTotalComponentCollisions);
            Assert.AreEqual(0, fixtureItem.MetaTotalComponentCollisions);

            Assert.IsNotNull(fixtureItem.MetaTotalPositionCollisions);
            Assert.AreEqual(10, fixtureItem.MetaTotalPositionCollisions);

            Assert.IsNotNull(fixtureItem.MetaComponentCount);
            Assert.AreEqual(1, fixtureItem.MetaComponentCount);

            Assert.IsNotNull(fixtureItem.MetaTotalMerchandisableLinearSpace);
            Assert.AreEqual(200F, fixtureItem.MetaTotalMerchandisableLinearSpace);

            Assert.IsNotNull(fixtureItem.MetaTotalMerchandisableAreaSpace);
            Assert.AreEqual(2000F, fixtureItem.MetaTotalMerchandisableAreaSpace);

            Assert.IsNotNull(fixtureItem.MetaTotalMerchandisableVolumetricSpace);
            Assert.AreEqual(150000F, fixtureItem.MetaTotalMerchandisableVolumetricSpace);

            Assert.IsNotNull(fixtureItem.MetaTotalLinearWhiteSpace);
            Assert.AreEqual(0.75F, fixtureItem.MetaTotalLinearWhiteSpace);

            Assert.IsNotNull(fixtureItem.MetaTotalAreaWhiteSpace);
            Assert.AreEqual(0.75F, fixtureItem.MetaTotalAreaWhiteSpace);

            Assert.IsNotNull(fixtureItem.MetaTotalVolumetricWhiteSpace);
            Assert.AreEqual(0.99F, fixtureItem.MetaTotalVolumetricWhiteSpace);

            Assert.IsNotNull(fixtureItem.MetaProductsPlaced);
            Assert.AreEqual(10, fixtureItem.MetaProductsPlaced);

            Assert.IsNotNull(fixtureItem.MetaNotAchievedInventory);
            Assert.AreEqual(0, fixtureItem.MetaNotAchievedInventory);

            Assert.IsNotNull(fixtureItem.MetaTotalFacings);
            Assert.AreEqual(10, fixtureItem.MetaTotalFacings);

            Assert.IsNotNull(fixtureItem.MetaAverageFacings);
            Assert.AreEqual(1, fixtureItem.MetaAverageFacings);

            Assert.IsNotNull(fixtureItem.MetaTotalUnits);
            Assert.AreEqual(170, fixtureItem.MetaTotalUnits);

            Assert.IsNotNull(fixtureItem.MetaAverageUnits);
            Assert.AreEqual(17, fixtureItem.MetaAverageUnits);

            Assert.IsNotNull(fixtureItem.MetaMinCases);
            Assert.AreEqual(17, fixtureItem.MetaMinCases);

            Assert.IsNotNull(fixtureItem.MetaAverageCases);
            Assert.AreEqual(17, fixtureItem.MetaAverageCases);

            Assert.IsNotNull(fixtureItem.MetaMaxCases);
            Assert.AreEqual(17, fixtureItem.MetaMaxCases);

            Assert.IsNotNull(fixtureItem.MetaTotalComponentsOverMerchandisedWidth);
            Assert.AreEqual(0, fixtureItem.MetaTotalComponentsOverMerchandisedWidth);

            Assert.IsNotNull(fixtureItem.MetaTotalComponentsOverMerchandisedHeight);
            Assert.AreEqual(1, fixtureItem.MetaTotalComponentsOverMerchandisedHeight);

            Assert.IsNotNull(fixtureItem.MetaTotalComponentsOverMerchandisedDepth);
            Assert.AreEqual(0, fixtureItem.MetaTotalComponentsOverMerchandisedDepth);

            Assert.IsNotNull(fixtureItem.MetaTotalFrontFacings);
            Assert.AreEqual(5, fixtureItem.MetaTotalFrontFacings);

            Assert.IsNotNull(fixtureItem.MetaAverageFrontFacings);
            Assert.AreEqual(0.5F, fixtureItem.MetaAverageFrontFacings);

            Assert.IsNotNull(fixtureItem.MetaNewProducts);
            Assert.AreEqual(0, fixtureItem.MetaNewProducts);

            Assert.IsNotNull(fixtureItem.MetaChangesFromPreviousCount);
            Assert.AreEqual(0, fixtureItem.MetaChangesFromPreviousCount);

            Assert.IsNotNull(fixtureItem.MetaChangeFromPreviousStarRating);
            Assert.AreEqual(0, fixtureItem.MetaChangeFromPreviousStarRating);

            Assert.IsNotNull(fixtureItem.MetaBlocksDropped);
            Assert.AreEqual(0, fixtureItem.MetaBlocksDropped);

            Assert.IsNotNull(fixtureItem.MetaMinDos);
            Assert.AreEqual(0, fixtureItem.MetaMinDos);

            Assert.IsNotNull(fixtureItem.MetaMaxDos);
            Assert.AreEqual(0, fixtureItem.MetaMaxDos);

            Assert.IsNotNull(fixtureItem.MetaAverageDos);
            Assert.AreEqual(0, fixtureItem.MetaAverageDos);
        }

        #endregion

        #region Test Data Helper

        /// <summary>
        /// Creates package to test Meta data
        /// </summary>
        /// <returns></returns>
        private Package CreatePackage()
        {
            // create package
            Int32 count = 1;
            Package package = PlanogramTestHelper.CreatePackage("Package");
            Planogram planogram = package.AddPlanogram();
            PlanogramFixtureItem fixtureItem = planogram.AddFixtureItem();
            PlanogramFixtureComponent fixtureComponent = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(planogram.Components[0]);
            planogram.Fixtures[0].Assemblies.Add();
            planogram.Assemblies.Add(PlanogramAssembly.NewPlanogramAssembly("Assembly"));
            planogram.Assemblies[0].Components.Add(assemblyComponent);

            planogram.Components[0].SupplierCostPrice = 10F;
            planogram.Components[0].SubComponents[0].Width = 100F;
            planogram.Components[0].SubComponents[0].MerchandisableDepth = 100F;
            planogram.Components[0].SubComponents[0].MerchandisableHeight = 10F;
            planogram.Fixtures[0].Assemblies[0].Y = 100;
            planogram.Assemblies[0].Width = 100F;
            planogram.Assemblies[0].Height = 50F;
            planogram.Assemblies[0].Depth = 60F;


            for (Int32 i = 0; i < 10; i++) 
            {
                planogram.AddProduct();
                planogram.Products[i].CasePackUnits = 1;
                fixtureComponent.AddPosition(fixtureItem, planogram.Products[i]);
                planogram.Positions[i].TotalUnits = 17;
                planogram.Positions[i/2].OrientationType = PlanogramPositionOrientationType.Front0;
            }

            foreach (PlanogramPerformanceData performanceData in planogram.Performance.PerformanceData) 
            {
                performanceData.P1 = 10;
                performanceData.P2 = count * 10;
                performanceData.P3 = 30;
                count++;
            }

            package.CalculateMetadata(false, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));

            return package;
        }

        #endregion
    }
}
