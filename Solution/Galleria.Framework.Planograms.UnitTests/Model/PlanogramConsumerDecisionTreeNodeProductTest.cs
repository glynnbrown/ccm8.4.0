﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.
// V8-26956 : L.Luong
//   Modified Tests to take in PlanogramProductId instead of ProductGtin
// V8-27132 : A.Kuszyk
//  Added test for factory method that accepts interface.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Moq;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Ccm.Security;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramConsumerDecisionTreeNodeProductTest : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct("1"));
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTreeNodeProductGroup()
        {
            var planogramProduct = CreatePlanogramProduct();
            var model = PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(planogramProduct.Id);
            Assert.That(model.IsChild);
            Assert.IsNotNull(model.PlanogramProductId);
        }

        [Test]
        public void NewPlanogramConsumerDecisionTreeNodeProduct_CopiesInterfaceProperties()
        {
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Gtin = "Gtin";
            planogram.Products.Add(product);
            var nodeProductMock = new Mock<IPlanogramConsumerDecisionTreeNodeProduct>();
            nodeProductMock.Setup(o => o.ProductGtin).Returns(product.Gtin);
            var nodeProduct = nodeProductMock.Object;

            var model = PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(nodeProduct, planogram);
            planogram.ConsumerDecisionTree.RootNode.Products.Add(model);

            AssertHelper.AreEqual(nodeProduct, model);
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogramProduct = CreatePlanogramProduct();
            var planogram = CreatePlanogram(planogramProduct);
            var package = CreatePackage(planogram);

            // set validation template to planogram
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "RootLevel";

            // add set root node name
            package.Planograms[0].ConsumerDecisionTree.RootNode.Name = "RootNode";

            var newPlanogramConsumerDecisionTreeNodeProduct = PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(planogramProduct.Id);
            package.Planograms[0].ConsumerDecisionTree.RootNode.Products.Add(newPlanogramConsumerDecisionTreeNodeProduct);
            package = package.Save();

            var Node = package.Planograms[0].ConsumerDecisionTree.RootNode.Products[0];
            var dto = FetchDtoByModel(Node);

            //fetch
            Assert.AreEqual(dto.PlanogramProductId, package.Planograms[0].ConsumerDecisionTree.RootNode.Products[0].PlanogramProductId);
        }

        #endregion

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogramProduct = CreatePlanogramProduct();
            var planogram = CreatePlanogram(planogramProduct);
            var package = CreatePackage(planogram);

            //create new consumer decision tree
            var newPlanogramConsumerDecisionTree = PlanogramConsumerDecisionTree.NewPlanogramConsumerDecisionTree();
            //create new consumer decision tree level
            var newPlanogramConsumerDecisionTreeNode = PlanogramConsumerDecisionTreeNode.NewPlanogramConsumerDecisionTreeNode("Root");

            //check there are no products
            Assert.IsEmpty(package.Planograms[0].ConsumerDecisionTree.RootNode.Products);

            var newPlanogramConsumerDecisionTreeNodeProduct = PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(planogramProduct.Id);
            package.Planograms[0].ConsumerDecisionTree.RootNode.Products.Add(newPlanogramConsumerDecisionTreeNodeProduct);
            package = package.Save();

            //check if consumer decision tree is not null
            Assert.IsNotEmpty(package.Planograms[0].ConsumerDecisionTree.RootNode.Products);
        }

        [Test]
        public void DataAccess_Update()
        {
            throw new InconclusiveException("No properties to change");

            ////create new package
            //var planogramProduct = CreatePlanogramProduct();
            //planogramProduct.Id = 1;
            //var planogramProduct2 = CreatePlanogramProduct();
            //planogramProduct2.Id = 2;
            //var planogram = CreatePlanogram(planogramProduct);
            //planogram.Products.Add(planogramProduct2);
            //var package = CreatePackage(planogram);

            //// set consumer decision tree name
            //package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            //// add set root level name
            //package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "RootLevel";

            //// add set root node name
            //package.Planograms[0].ConsumerDecisionTree.RootNode.Name = "RootNode";

            //// add a product to node
            //package.Planograms[0].ConsumerDecisionTree.RootNode.Products.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(planogramProduct.Id));
            
            //// save
            //package = package.Save();
            //PackageDto dto;

            //// fetch
            //IDalFactory dalFactory = DalContainer.GetDalFactory();
            //using (IDalContext dalContext = dalFactory.CreateContext())
            //{
            //    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
            //    {
            //        dto = dal.FetchById(package.Id);
            //    }
            //}

            //// check first value is equal
            //Assert.AreEqual(planogramProduct.Id, package.Planograms[0].ConsumerDecisionTree.RootNode.Products[0].PlanogramProductId);

            //// change value
            //package.Planograms[0].ConsumerDecisionTree.RootNode.Products[0].PlanogramProductId = planogramProduct2.Id;
            //package = package.Save();

            //// refetch
            //using (IDalContext dalContext = dalFactory.CreateContext())
            //{
            //    using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
            //    {
            //        dto = dal.FetchById(package.Id);
            //    }
            //}

            //// check if value has changed
            //Assert.AreEqual(planogramProduct2.Id, package.Planograms[0].ConsumerDecisionTree.RootNode.Products[0].PlanogramProductId);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogramProduct = CreatePlanogramProduct();
            var planogram = CreatePlanogram(planogramProduct);
            var package = CreatePackage(planogram);

            // set consumer decision tree name
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "RootLevel";

            // add set root node name
            package.Planograms[0].ConsumerDecisionTree.RootNode.Name = "RootNode";

            // add a product to node
            var newPlanogramConsumerDecisionTreeNodeProduct = PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(planogramProduct.Id);
            package.Planograms[0].ConsumerDecisionTree.RootNode.Products.Add(newPlanogramConsumerDecisionTreeNodeProduct);
            package = package.Save();
            PackageDto dto;

            // fetch
            var modelPackage = package.Planograms[0].ConsumerDecisionTree.RootLevel;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // remove planogram
            var model = package.Planograms[0].ConsumerDecisionTree.RootNode.Products[0];
            package.Planograms[0].ConsumerDecisionTree.RootNode.Products.Remove(model);
            package = package.Save();

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // root childlist should be empty
            Assert.IsEmpty(package.Planograms[0].ConsumerDecisionTree.RootNode.Products);

        }

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram(PlanogramProduct planogramProduct)
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Products.Add(planogramProduct);
            planogram.Name = "PlanogramName";
            return planogram;
        }

        private PlanogramProduct CreatePlanogramProduct()
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Gtin = "ProductGTIN";
            product.Name = "PlanogramName";
            return product;
        }

        private PlanogramConsumerDecisionTreeNodeProductDto FetchDtoByModel(PlanogramConsumerDecisionTreeNodeProduct model)
        {
            PlanogramConsumerDecisionTreeNodeProductDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeNodeProductDal>())
                {
                    dto = dal.FetchByPlanogramConsumerDecisionTreeNodeId(model.ParentPlanogramConsumerDecisionTreeNode.Id).FirstOrDefault();
                }
            }
            return dto;
        }

        #endregion
    }
}
