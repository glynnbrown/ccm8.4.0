﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramComparisonResultTests : TestBase<PlanogramComparisonResult, PlanogramComparisonResultDto>
    {
        #region Coding Standards

        [Test]
        public void Serializable()
        {
            TestSerialize();
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        [Test]
        public void NewFactoryMethod_FromPlanogram()
        {
            Planogram planogram = "test".CreatePackage().AddPlanogram();

            PlanogramComparisonResult testModel = PlanogramComparisonResult.NewPlanogramComparisonResult(planogram);

            Assert.IsTrue(testModel.IsNew);
            Assert.IsTrue(testModel.IsInitialized);
            Assert.AreEqual(planogram.Name, testModel.PlanogramName);
        }

        [Test, TestCaseSource("GetMatchingDtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("GetMatchingDtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion
    }
}