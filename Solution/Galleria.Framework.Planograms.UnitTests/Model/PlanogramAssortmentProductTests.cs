﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26789 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM 830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Moq;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramAssortmentProductTests
    {
        [Test]
        public void NewPlanogramAssortmentProduct_Interface_AddsProperties()
        {
            var mock = new Mock<IPlanogramAssortmentProduct>();
            mock.SetupGet(p => p.Gtin).Returns("Gtin");
            mock.SetupGet(p=>p.Name).Returns("Name");
            mock.SetupGet(p=>p.IsRanged).Returns(true);
            mock.SetupGet(p=>p.Rank).Returns(1);
            mock.SetupGet(p=>p.Segmentation).Returns("Segmentation");
            mock.SetupGet(p=>p.Facings).Returns(1);
            mock.SetupGet(p=>p.Units).Returns(1);
            mock.SetupGet(p=>p.ProductTreatmentType).Returns(PlanogramAssortmentProductTreatmentType.Normal);
            mock.SetupGet(p=>p.ProductLocalizationType).Returns(PlanogramAssortmentProductLocalizationType.Regional);
            mock.SetupGet(p=>p.Comments).Returns("Comments");
            mock.SetupGet(p=>p.ExactListFacings).Returns(1);
            mock.SetupGet(p=>p.ExactListUnits).Returns(1);
            mock.SetupGet(p=>p.PreserveListFacings).Returns(1);
            mock.SetupGet(p=>p.PreserveListUnits).Returns(1);
            mock.SetupGet(p=>p.MaxListFacings).Returns(1);
            mock.SetupGet(p=>p.MaxListUnits).Returns(1);
            mock.SetupGet(p=>p.MinListFacings).Returns(1);
            mock.SetupGet(p=>p.MinListUnits).Returns(1);
            mock.SetupGet(p=>p.FamilyRuleName).Returns("FamilyRuleName");
            mock.SetupGet(p=>p.FamilyRuleType).Returns(PlanogramAssortmentProductFamilyRuleType.MaximumProductCount);
            mock.SetupGet(p=>p.FamilyRuleValue).Returns(1);
            mock.SetupGet(p=>p.IsPrimaryRegionalProduct).Returns(true);
            var mockInterface = mock.Object;

            var model = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(mockInterface);

            AssertHelper.AreEqual<IPlanogramAssortmentProduct>(model, mockInterface);
        }
    }
}
