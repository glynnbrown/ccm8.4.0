﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.
// V8-27062 : A.Silva ~ Added unit tests for Validation Calculations.

#endregion

#region Version History: CCM802

// V8-28878 : A.Silva
//      Commented out Test CalculateValidationData_WhenAllMetricsScore_ShouldReturnGreen as it needs complete rewrite.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using TestHelper = Galleria.Framework.Planograms.UnitTests.Model.PlanogramValidationTemplateTestsHelper;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramValidationTemplateGroupTests : TestBase
    {
        #region Constants

        private const Single GreaterThanThreshold1Value = 10;
        private const Single GreaterThanThreshold2Value = 5;
        private const Single GreaterThanGreenValue = 11;
        private const Single GreaterThanYellowValue = 6;
        private const Single GreaterThanRedValue = 3;

        private const Single LessThanThreshold1Value = 5;
        private const Single LessThanThreshold2Value = 10;
        private const Single LessThanGreenValue = 3;
        private const Single LessThanYellowValue = 6;
        private const Single LessThanRedValue = 11;

        #endregion

        #region Fields

        private readonly string _planogramField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof (Planogram), Planogram.FriendlyName,
                Planogram.MetaBayCountProperty).FieldPlaceholder;

        private readonly string _planogramComponentField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof (PlanogramComponent), PlanogramComponent.FriendlyName,
                PlanogramComponent.CapacityProperty).FieldPlaceholder;

        private readonly string _planogramFixtureField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof (PlanogramFixture), PlanogramFixture.FriendlyName,
                PlanogramFixture.MetaTotalFixtureCostProperty).FieldPlaceholder;

        #endregion

        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewValidationTemplateGroup()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = "Name";
            planogram.ValidationTemplate.Name = newPlanogramValidationTemplate.Name;

            // add group to validation template
            var newPlanogramValidationTemplateGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            newPlanogramValidationTemplateGroup.Name = "Group";
            package.Planograms[0].ValidationTemplate.Groups.Add(newPlanogramValidationTemplateGroup);
            package = package.Save();
            newPlanogramValidationTemplateGroup = planogram.ValidationTemplate.Groups[0];

            //check if validation is new and is child
            Assert.AreEqual(newPlanogramValidationTemplateGroup.Parent.Id, planogram.ValidationTemplate.Id);
            Assert.IsTrue(newPlanogramValidationTemplateGroup.IsNew);
            Assert.IsTrue(newPlanogramValidationTemplateGroup.IsChild, "A newly created Planogram Validation Template should be a child object.");
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = "Name";

            // add group to validation template
            var newPlanogramValidationTemplateGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            newPlanogramValidationTemplateGroup.Name = "Group";
            package.Planograms[0].ValidationTemplate.Groups.Add(newPlanogramValidationTemplateGroup);
            package = package.Save();
            newPlanogramValidationTemplateGroup = planogram.ValidationTemplate.Groups[0];

            //fetch
            Planograms.Model.Package packageModel = Planograms.Model.Package.FetchById(package.Id, 1, PackageLockType.User);
            PlanogramValidationTemplateGroup model = packageModel.Planograms[0].ValidationTemplate.Groups[0];
            Assert.AreEqual(newPlanogramValidationTemplateGroup.Name, packageModel.Planograms[0].ValidationTemplate.Groups[0].Name);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();

            // add group to validation template
            var newPlanogramValidationTemplateGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            newPlanogramValidationTemplateGroup.Name = "Group";
            package.Planograms[0].ValidationTemplate.Groups.Add(newPlanogramValidationTemplateGroup);
            package = package.Save();
            newPlanogramValidationTemplateGroup = planogram.ValidationTemplate.Groups[0];

            //check if the object is not null
            Assert.IsNotNull(planogram.ValidationTemplate.Groups[0]);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            String check = "Group1";

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = "Name";

            // add group to validation template
            var newPlanogramValidationTemplateGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            newPlanogramValidationTemplateGroup.Name = check;
            package.Planograms[0].ValidationTemplate.Groups.Add(newPlanogramValidationTemplateGroup);

            // save
            package = package.Save();
            newPlanogramValidationTemplateGroup = planogram.ValidationTemplate.Groups[0];
            PackageDto dto;

            // fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if first value is equal
            Assert.AreEqual(check, package.Planograms[0].ValidationTemplate.Groups[0].Name);

            // change value
            check = "Group2";
            newPlanogramValidationTemplateGroup.Name = check;
            package.Planograms[0].ValidationTemplate.Groups[0].Name = newPlanogramValidationTemplateGroup.Name;
            package = package.Save();
            newPlanogramValidationTemplate = planogram.ValidationTemplate;

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if value has changed
            Assert.AreEqual(check, package.Planograms[0].ValidationTemplate.Groups[0].Name);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            planogram.Id = 1;

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = "Name";

            // add group to validation template
            var newPlanogramValidationTemplateGroup = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            newPlanogramValidationTemplateGroup.Name = "Group";
            package.Planograms[0].ValidationTemplate.Groups.Add(newPlanogramValidationTemplateGroup);
            package = package.Save();
            newPlanogramValidationTemplateGroup = planogram.ValidationTemplate.Groups[0];

            // get dto
            var model = package.Planograms[0].ValidationTemplate.Groups[0];
            var dto = FetchDtoByModel(model);

            // remove group
            package.Planograms[0].ValidationTemplate.Groups.Remove(model);
            package.Save();

            // refetch dto
            dto = FetchDtoByModel(model);

            // check if dto is null
            Assert.IsNull(dto);

        }

        #endregion

        #region IValidationTemplate Members

        [Test]
        public void IValidationTemplateGroupMetrics_GetAccessor_GetsMetrics()
        {
            const string expectation = "When getting Metrics through the interface the actual metrics are returned.";
            var testModel = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            var expected = testModel.Metrics;
            expected.Add(PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric());

            var actual = ((IValidationTemplateGroup)testModel).Metrics;

            CollectionAssert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void IValidationTemplateGroupAddNewMetric_Invoked_AddsNewMetric()
        {
            const string expectation = "When AddNewMetric is invoked the Metrics collection gets a new metric.";
            var testModel = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();

            ((IValidationTemplateGroup)testModel).AddNewMetric();

            var actual = testModel.Metrics.Any();
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void IValidationTemplateGroupRemoveMEtric_ContainsMetric_RemovesMetric()
        {
            const string expectation = "When RemoveMetric is invoked with an existing metric the Metrics collection removes it.";
            var testModel = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            var itemToRemove = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
            testModel.Metrics.Add(itemToRemove);

            ((IValidationTemplateGroup)testModel).RemoveMetric(itemToRemove);

            var actual = !testModel.Metrics.Contains(itemToRemove);
            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void IValidationTemplateGroupRemoveMetric_DoesNotContainMetric_DoesNotChangeMetrics()
        {
            const string expectation = "When RemoveMetric is invoked with a non existing metric the Metrics collection remains unchanged.";
            var testModel = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            var itemToRemove = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
            testModel.Metrics.Add(itemToRemove);
            var unrelatedItem = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();

            ((IValidationTemplateGroup)testModel).RemoveMetric(unrelatedItem);

            var actual = testModel.Metrics.Contains(itemToRemove);
            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region CalculateValidationData

        [Test]
        public void CalculateValidationData_WhenAllMetricsScore_ShouldReturnGreen()
        {
            Assert.Inconclusive("To test this we need to create a full valid planogram as the metadata is now calculated before running the validation.");
            //const String expectation =
            //    "When all metrics in group's scores are green, ResultType should be 'Green'.";
            //const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            //var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            //{
            //    TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
            //        GreaterThanThreshold2Value, 11),
            //        TestHelper.NewValidationMetric(_planogramComponentField, GreaterThanThreshold1Value, GreaterThanThreshold2Value, 5,3, aggregationType: PlanogramValidationAggregationType.Sum),
            //        TestHelper.NewValidationMetric(_planogramFixtureField, GreaterThanThreshold1Value, GreaterThanThreshold2Value, 3,2,1, aggregationType: PlanogramValidationAggregationType.Max)
            //};
            //var validationGroup = PlanogramValidationTemplateTestsHelper.NewValidationGroup(metrics:
            //    testMetrics);
            //validationGroup.Threshold1 = GreaterThanThreshold1Value;
            //validationGroup.Threshold2 = GreaterThanThreshold2Value;
            //var package = PlanogramValidationTemplateTestsHelper.NewPackage(planograms: new List<Planogram>
            //{
            //    PlanogramValidationTemplateTestsHelper.NewPlanogram(
            //        validationTemplate:
            //            PlanogramValidationTemplateTestsHelper.NewValidationTemplate(groups:
            //                new List<PlanogramValidationTemplateGroup>
            //                {
            //                    validationGroup
            //                }))
            //});
            //var planogram = package.Planograms.First();
            //for (Int32 i = 0; i<=GreaterThanGreenValue; i++)
            //{
            //    PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            //    planogram.Fixtures.Add(fixture);
            //    PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add(fixture);
            //}
            ////planogram.MetaBayCount = Convert.ToInt32(GreaterThanGreenValue);
            //var component = PlanogramComponent.NewPlanogramComponent("TestComponent", PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            //component.Capacity = Convert.ToSByte(GreaterThanRedValue);
            //planogram.Components.Add(component);
            //planogram.Components.Add(component.Copy());
            //var component2 = component.Copy();
            //component2.Capacity = Convert.ToSByte(GreaterThanYellowValue);
            //planogram.Components.Add(component2);
            //var item = PlanogramFixture.NewPlanogramFixture("TestFixture", PlanogramSettings.NewPlanogramSettings());
            //item.MetaTotalFixtureCost = Convert.ToSByte(GreaterThanGreenValue);
            //planogram.Fixtures.Add(item);
            //var item2 = item.Copy();
            //item2.MetaTotalFixtureCost = Convert.ToSByte(GreaterThanYellowValue);

            //var mut = planogram.ValidationTemplate.Groups.First();

            //planogram.Parent.CalculateValidationData();
            //var actual = mut.ResultType;

            //Assert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        private static PlanogramValidationTemplateGroupDto FetchDtoByModel(PlanogramValidationTemplateGroup model)
        {
            PlanogramValidationTemplateGroupDto dto;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramValidationTemplateGroupDal>())
            {
                dto = dal.FetchByPlanogramValidationTemplateId(model.Parent.Id).FirstOrDefault(groupDto => groupDto.Id == model.Id);
            }
            return dto;
        }

        #endregion
    }
}
