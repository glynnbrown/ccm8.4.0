﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: v800
// V8-27605 : A.Silva
//      Created.
#endregion

#region Version History: v830
// V8-32521 : J.Pickup
//      Added Metadata test : MetaComponentIsOutsideOfFixtureArea
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramAssemblyComponentTests : TestBase<PlanogramAssemblyComponent, PlanogramAssemblyComponentDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable

        [Test]
        public void Serializable()
        {
            TestSerialize();
        }

        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region MetaData
        [Category(Categories.Smoke)]
        [Test]
        public void MetaComponentIsOutsideOfFixtureArea()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);


            PlanogramFixtureAssembly assembly1 = fixture.Assemblies.Add();
            PlanogramAssemblyComponent assemComp = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(shelf1C);
            assembly1.GetPlanogramAssembly().Components.Add(assemComp);

            PlanogramAssemblyComponentList assemblyComponentList = assembly1.GetPlanogramAssembly().Components;
 

            //calc metadata
            package.CalculateMetadata(false, null);

           // PlanogramFixtureComponent fixtureComponent = fixture.Components.First(c => c.PlanogramComponentId == shelf1C.Id);

            Assert.IsNotNull(assemComp.MetaIsOutsideOfFixtureArea, "should not be null: MetaIsOutsideOfFixtureArea");
            Assert.IsFalse(assemComp.MetaIsOutsideOfFixtureArea.Value, "should be false: MetaIsOutsideOfFixtureArea");

            //Make a change - move the component out.
            assemComp.X = 200f;

            package.CalculateMetadata(false, null);

            Assert.IsNotNull(assemComp.MetaIsOutsideOfFixtureArea, "should not be null: MetaIsOutsideOfFixtureArea");
            Assert.IsTrue(assemComp.MetaIsOutsideOfFixtureArea.Value, "should be true: MetaIsOutsideOfFixtureArea");
        }

        #endregion
    }
}