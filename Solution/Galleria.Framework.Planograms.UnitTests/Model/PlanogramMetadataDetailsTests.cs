﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM810

// V8-30013 : A.Silva.
//  Created.
// V8-28878 : A.Silva
//  Amended Overfilled tests to account for updated rules on overfill for pegs, clipstrips, rods and bars.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramMetadataDetailsTests : TestBase
    {
        #region Fields

        private readonly List<PlanogramComponentType> _allMerchandisableComponentTypesExceptCustom =
            Enum.GetValues(typeof (PlanogramComponentType))
                .Cast<PlanogramComponentType>()
                .Where(v => v != PlanogramComponentType.Custom && v != PlanogramComponentType.Backboard &&
                            v != PlanogramComponentType.Base && v != PlanogramComponentType.Panel)
                .ToList();

        private readonly List<AxisType> _allAxisTypes =
            Enum.GetValues(typeof (AxisType))
                .Cast<AxisType>()
                .ToList();

        #endregion

        #region IsMerchandisingGroupOverfilled Unit Tests

        [Test]
        public void IsMerchandisingGroupOverfilled_WhenNoPlacements_ShouldNotBeOverfilled(
            [ValueSource("_allAxisTypes")] AxisType axis,
            [ValueSource("_allMerchandisableComponentTypesExceptCustom")] PlanogramComponentType type)
        {
            const String expectation = "The Merchandising Group should not be overfilled when there are no placements.";
            Package package = "TestPackage".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            PlanogramFixtureItem fixtureA = planogram.AddFixtureItem();
            PlanogramFixture fixture = fixtureA.GetPlanogramFixture();
            Single fixtureCenterX = fixtureA.X + fixture.Width / 2;
            Single fixtureCenterY = fixtureA.Y + fixture.Height / 2;
            PlanogramFixtureComponent componentA = fixtureA.AddFixtureComponent(type);
            PlanogramComponent component = componentA.GetPlanogramComponent();
            componentA.X = fixtureCenterX - component.Width / 2;
            componentA.Y = fixtureCenterY - component.Height / 2;
            IPlanogramMetadataHelper planogramMetadataHelper = PlanogramMetadataHelper.NewPlanogramMetadataHelper(Convert.ToInt32(package.EntityId), package);
            Object subComponentPlacementId = componentA.GetPlanogramSubComponentPlacements().First().Id;

            PlanogramMetadataDetails planogramMetadataDetails = PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, planogramMetadataHelper);
            Boolean condition = planogramMetadataDetails.IsMerchandisingGroupOverfilled(subComponentPlacementId);

            Assert.IsFalse(condition, expectation);
        }

        [Test]
        public void IsMerchandisingGroupOverfilled_WhenAllPlacementsFit_ShouldNotBeOverfilled(
            [ValueSource("_allAxisTypes")] AxisType axis,
            [ValueSource("_allMerchandisableComponentTypesExceptCustom")] PlanogramComponentType type)
        {
            const String expectation = "The Merchandising Group should not be overfilled when there are no placements.";
            Package package = "TestPackage".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            PlanogramFixtureItem fixtureA = planogram.AddFixtureItem();
            PlanogramFixture fixture = fixtureA.GetPlanogramFixture();
            Single fixtureCenterX = fixtureA.X + fixture.Width / 2;
            Single fixtureCenterY = fixtureA.Y + fixture.Height / 2;
            PlanogramFixtureComponent componentA = fixtureA.AddFixtureComponent(type);
            PlanogramComponent component = componentA.GetPlanogramComponent();
            componentA.X = fixtureCenterX - component.Width / 2;
            componentA.Y = fixtureCenterY - component.Height / 2;
            PlanogramSubComponentPlacement subComponentPlacement = componentA.GetPlanogramSubComponentPlacements().First();
            subComponentPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            subComponentPlacement.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            subComponentPlacement.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
            var currentPositionSize = 0.0F;
            Int16 nextSeq = 1;
            Boolean componentHasSpace;
            do
            {
                PlanogramPosition position = componentA.AddPosition(fixtureA);
                RectValue merchandisingSpaceBounds =
                    componentA.GetPlanogramSubComponentPlacements().First().GetWorldMerchandisingSpaceBounds(true, planogram.GetPlanogramSubComponentPlacements());
                Single positionSize;
                switch (axis)
                {
                    case AxisType.X:
                        position.SequenceX = nextSeq++;
                        positionSize = position.GetPlanogramProduct().Width;
                        currentPositionSize += positionSize;
                        componentHasSpace = fixture.Width > currentPositionSize + positionSize && (type != PlanogramComponentType.ClipStrip && type != PlanogramComponentType.Peg || nextSeq < 1);
                        if (merchandisingSpaceBounds.Width < currentPositionSize)
                            Assert.Inconclusive("The positions should not overfill for this unit test.");
                        break;
                    case AxisType.Y:
                        position.SequenceY = nextSeq++;
                        positionSize = position.GetPlanogramProduct().Height;
                        currentPositionSize += positionSize;
                        componentHasSpace = fixture.Height > currentPositionSize + positionSize && (type != PlanogramComponentType.ClipStrip && type != PlanogramComponentType.Peg || nextSeq < 1);
                        if (merchandisingSpaceBounds.Height < currentPositionSize)
                            Assert.Inconclusive("The positions should not overfill for this unit test.");
                        break;
                    case AxisType.Z:
                        position.SequenceZ = nextSeq++;
                        positionSize = position.GetPlanogramProduct().Depth;
                        currentPositionSize += positionSize;
                        componentHasSpace = fixture.Depth > currentPositionSize + positionSize;
                        if (merchandisingSpaceBounds.Depth < currentPositionSize)
                            Assert.Inconclusive("The positions should not overfill for this unit test.");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("axis");
                }
            } while (componentHasSpace);
            planogram.ReprocessAllMerchandisingGroups();
            IPlanogramMetadataHelper planogramMetadataHelper = PlanogramMetadataHelper.NewPlanogramMetadataHelper(Convert.ToInt32(package.EntityId), package);
            Object subComponentPlacementId = componentA.GetPlanogramSubComponentPlacements().First().Id;

            PlanogramMetadataDetails planogramMetadataDetails = PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, planogramMetadataHelper);
            Boolean condition = planogramMetadataDetails.IsMerchandisingGroupOverfilled(subComponentPlacementId);

            Assert.IsFalse(condition, expectation);
        }

        [Test]
        public void IsMerchandisingGroupOverfilled_WhenPlacementsDoNotFit_ShouldBeOverfilled(
            [ValueSource("_allAxisTypes")] AxisType axis,
            [ValueSource("_allMerchandisableComponentTypesExceptCustom")] PlanogramComponentType type)
        {
            const String expectation = "The Merchandising Group should be overfilled when placements do not fit.";
            Package package = "TestPackage".CreatePackage();
            Planogram planogram = package.AddPlanogram();
            PlanogramFixtureItem fixtureA = planogram.AddFixtureItem();
            PlanogramFixture fixture = fixtureA.GetPlanogramFixture();
            Single fixtureCenterX = fixtureA.X + fixture.Width / 2;
            Single fixtureCenterY = fixtureA.Y + fixture.Height / 2;
            PlanogramFixtureComponent componentA = fixtureA.AddFixtureComponent(type);
            PlanogramComponent component = componentA.GetPlanogramComponent();
            componentA.X = fixtureCenterX - component.Width / 2;
            componentA.Y = fixtureCenterY - component.Height / 2;
            PlanogramSubComponentPlacement subComponentPlacement = componentA.GetPlanogramSubComponentPlacements().First();
            subComponentPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            subComponentPlacement.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            subComponentPlacement.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
            var currentPositionSize = 0.0F;
            Int16 nextSeq = 1;
            Boolean componentHasSpace;
            do
            {
                PlanogramPosition position = componentA.AddPosition(fixtureA);
                Single positionSize;
                switch (axis)
                {
                    case AxisType.X:
                        position.SequenceX = nextSeq++;
                        positionSize = position.GetPlanogramProduct().Width;
                        currentPositionSize += positionSize;
                        componentHasSpace = fixture.Width >= currentPositionSize - (type == PlanogramComponentType.Bar ? positionSize : 0);
                        break;
                    case AxisType.Y:
                        position.SequenceY = nextSeq++;
                        positionSize = position.GetPlanogramProduct().Height;
                        currentPositionSize += positionSize;
                        componentHasSpace = fixture.Height  >= currentPositionSize;
                        break;
                    case AxisType.Z:
                        position.SequenceZ = nextSeq++;
                        positionSize = position.GetPlanogramProduct().Depth;
                        currentPositionSize += positionSize;
                        componentHasSpace = fixture.Depth >= currentPositionSize;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("axis");
                }
            } while (componentHasSpace);
            planogram.ReprocessAllMerchandisingGroups();
            IPlanogramMetadataHelper planogramMetadataHelper = PlanogramMetadataHelper.NewPlanogramMetadataHelper(Convert.ToInt32(package.EntityId), package);
            Object subComponentPlacementId = subComponentPlacement.Id;

            PlanogramMetadataDetails planogramMetadataDetails = PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, planogramMetadataHelper);
            Boolean condition = planogramMetadataDetails.IsMerchandisingGroupOverfilled(subComponentPlacementId);

            Assert.IsTrue(condition, expectation);
        }

        #endregion
    }
}