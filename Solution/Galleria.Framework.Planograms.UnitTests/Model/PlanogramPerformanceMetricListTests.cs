﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramPerformanceMetricListTests : TestBase
    {
        [Test]
        public void Add_WhenCountExceeds20_BreaksRules()
        {
            var planogram = Planogram.NewPlanogram();

            for (Byte i = 1; i <= 20; i++)
            {
                var item = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(i);
                item.Name = String.Format("Metric{0}", i);
                planogram.Performance.Metrics.Add(item);
            }
            Assert.That(planogram.Performance.IsValid);
            planogram.Performance.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(20));

            Assert.IsFalse(planogram.Performance.IsValid);
        }

        [Test]
        public void Add_WhenItemContainsDuplicateMetricId_BreaksRules()
        {
            var planogram = Planogram.NewPlanogram();
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Name = "Metric1";
            var metric2 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric2.Name = "Metric2";
            planogram.Performance.Metrics.Add(metric1);
            Assert.That(planogram.Performance.IsValid);

            planogram.Performance.Metrics.Add(metric2);

            Assert.IsFalse(planogram.Performance.IsValid);
        }

        [Test]
        public void Add_WhenItemContainsDuplicateSpecialType_BreaksRules()
        {
            var planogram = Planogram.NewPlanogram();
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Name = "Metric1";
            metric1.SpecialType = MetricSpecialType.RegularSalesUnits;
            var metric2 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(2);
            metric2.Name = "Metric2";
            metric2.SpecialType = MetricSpecialType.RegularSalesUnits;

            planogram.Performance.Metrics.Add(metric1);
            Assert.That(planogram.Performance.IsValid);

            planogram.Performance.Metrics.Add(metric2);
            Assert.IsFalse(planogram.Performance.IsValid);
        }

        [Test]
        public void Add_WhenItemContainsDuplicateSpecialType_ValueNone_DoesNotBreakRules()
        {
            var planogram = Planogram.NewPlanogram();
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Name = "Metric1";
            metric1.SpecialType = MetricSpecialType.None;
            var metric2 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(2);
            metric2.Name = "Metric2";
            metric2.SpecialType = MetricSpecialType.None;

            planogram.Performance.Metrics.Add(metric1);
            Assert.That(planogram.Performance.IsValid);

            planogram.Performance.Metrics.Add(metric2);
            Assert.That(planogram.Performance.IsValid);
        }
    }
}
