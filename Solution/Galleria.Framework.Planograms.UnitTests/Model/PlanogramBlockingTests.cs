﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
// V8-27467 : A.Kuszyk
//  Adjusted the way snapping boundaries are selected.
// V8-27485 : A.Kuszyk
//  Moved fixture snapping code to Engine.Tasks assembly.
// V8-27439 : A.Kuszyk
//  Added tests for GetLocationsBetweenDividers method.
#endregion

#region Version History : CCM810
// V8-30188 : A.Kuszyk
//  Updated and fixed tests for recent changes.
#endregion

#region Version History : CCM830
// V8-32018 : A.Kuszyk
//  Added test for always snapping when sufficient space behaviour.
// V8-32449 : A.Kuszyk
//  Added tests for inventory weighting in the space calculation.
// V8-32539 : A.Kuszyk
//  Added test for CanMerge behaviour.
// V8-32660 : A.Kuszyk
//  Added tests for CanOptimise behaviour.
// V8-32686 : A.Kuszyk
//  Added tests for IsLimited behaviour.
// V8-18320 : A.Kuszyk
//  Added tests for EnumerateGroupsInOptimisationOrder.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Interfaces;
using Moq;
using Galleria.Framework.DataStructures;
using System.Diagnostics;
using FluentAssertions;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramBlockingTests : TestBase<PlanogramBlocking, PlanogramBlockingDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod_Type()
        {
            TestNewFactoryMethod(new String[] { "Type" });
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region PlanogramBlocking Behaviours

        private PlanogramBlocking CreateBehaviourTestData()
        {
            PlanogramBlocking item = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);

            //add groups
            PlanogramBlockingGroup group1 = item.Groups.AddNew();
            PlanogramBlockingGroup group2 = item.Groups.AddNew();
            PlanogramBlockingGroup group3 = item.Groups.AddNew();
            PlanogramBlockingGroup group4 = item.Groups.AddNew();

            //add dividers
            PlanogramBlockingDivider divider1 = PlanogramBlockingDivider.NewPlanogramBlockingDivider((Byte)1, PlanogramBlockingDividerType.Vertical, 0.45F, 0, 1);
            item.Dividers.Add(divider1);

            PlanogramBlockingDivider divider2 = PlanogramBlockingDivider.NewPlanogramBlockingDivider((Byte)1, PlanogramBlockingDividerType.Vertical, 0.80F, 0, 1);
            item.Dividers.Add(divider2);

            PlanogramBlockingDivider divider3 = PlanogramBlockingDivider.NewPlanogramBlockingDivider((Byte)2, PlanogramBlockingDividerType.Horizontal, 0.80F, 0.50F, 0.2F);
            item.Dividers.Add(divider3);

            //add locations
            PlanogramBlockingLocation loc1 = item.Locations.Add(group1);
            loc1.PlanogramBlockingDividerRightId = divider1.Id;

            PlanogramBlockingLocation loc2 = item.Locations.Add(group2);
            loc2.PlanogramBlockingDividerLeftId = divider1.Id;
            loc2.PlanogramBlockingDividerRightId = divider2.Id;

            PlanogramBlockingLocation loc3 = item.Locations.Add(group3);
            loc3.PlanogramBlockingDividerLeftId = divider2.Id;
            loc3.PlanogramBlockingDividerBottomId = divider3.Id;

            PlanogramBlockingLocation loc4 = item.Locations.Add(group4);
            loc4.PlanogramBlockingDividerLeftId = divider2.Id;
            loc4.PlanogramBlockingDividerTopId = divider3.Id;

            return item;
        }

        [Test]
        public void Behaviour_CreateFullAndSave()
        {
            var pk = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            pk.Name = Guid.NewGuid().ToString();
            var plan = pk.Planograms.Add();
            plan.Name = pk.Name;

            PlanogramBlocking item = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            plan.Blocking.Add(item);


            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            Action<PlanogramBlocking> checkPlanogramBlocking =
            (b) =>
            {
                Assert.AreEqual(3, b.Dividers.Count);

                PlanogramBlockingDivider divider1 = b.Dividers[0];
                PlanogramBlockingDivider divider2 = b.Dividers[1];
                PlanogramBlockingDivider divider3 = b.Dividers[2];

                //check dividers
                Assert.AreEqual(0.45F, divider1.X);
                Assert.AreEqual(0F, divider1.Y);
                Assert.AreEqual(1F, divider1.Length);

                Assert.AreEqual(0.8F, divider2.X);
                Assert.AreEqual(0F, divider2.Y);
                Assert.AreEqual(1F, divider2.Length);

                Assert.AreEqual(0.8F, divider3.X);
                Assert.AreEqual(0.5F, divider3.Y);
                Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(divider3.Length, 5)));

                //check groups
                Assert.AreEqual(4, b.Groups.Count, "Should have 4 groups");
                Assert.AreEqual(4, b.Groups.Select(g => g.Colour).Distinct().Count(),
                    "Each group should have a distinct colour");

                //check locations.
                Assert.AreEqual(4, b.Locations.Count, "Should have 4 locations");

                PlanogramBlockingLocation loc1 = b.Locations[0];
                PlanogramBlockingLocation loc2 = b.Locations[1];
                PlanogramBlockingLocation loc3 = b.Locations[2];
                PlanogramBlockingLocation loc4 = b.Locations[3];

                Assert.AreEqual(b.Groups[0].Id, loc1.PlanogramBlockingGroupId);
                Assert.AreEqual(divider1.Id, loc1.PlanogramBlockingDividerRightId);
                Assert.AreEqual(0, loc1.X);
                Assert.AreEqual(0, loc1.Y);
                Assert.AreEqual(0.45F, loc1.Width);
                Assert.AreEqual(1, loc1.Height);

                Assert.AreEqual(b.Groups[1].Id, loc2.PlanogramBlockingGroupId);
                Assert.AreEqual(divider1.Id, loc2.PlanogramBlockingDividerLeftId);
                Assert.AreEqual(divider2.Id, loc2.PlanogramBlockingDividerRightId);
                Assert.AreEqual(0.45F, loc2.X);
                Assert.AreEqual(0, loc2.Y);
                Assert.AreEqual(0.35F, Convert.ToSingle(Math.Round(loc2.Width, 5)));
                Assert.AreEqual(1, Convert.ToSingle(Math.Round(loc2.Height, 5)));

                Assert.AreEqual(b.Groups[2].Id, loc3.PlanogramBlockingGroupId);
                Assert.AreEqual(divider2.Id, loc3.PlanogramBlockingDividerLeftId);
                Assert.AreEqual(divider3.Id, loc3.PlanogramBlockingDividerTopId);
                Assert.AreEqual(0.8F, loc3.X);
                Assert.AreEqual(0F, loc3.Y);
                Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc3.Width, 5)));
                Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc3.Height, 5)));

                Assert.AreEqual(b.Groups[3].Id, loc4.PlanogramBlockingGroupId);
                Assert.AreEqual(divider2.Id, loc4.PlanogramBlockingDividerLeftId);
                Assert.AreEqual(divider3.Id, loc4.PlanogramBlockingDividerBottomId);
                Assert.AreEqual(0.8F, loc4.X);
                Assert.AreEqual(0.5, loc4.Y);
                Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc4.Width, 5)));
                Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc4.Height, 5)));
            };

            //execute the check
            checkPlanogramBlocking(item);


            //Save
            pk = pk.Save();

            //check
            checkPlanogramBlocking(item);

            //re-fetch
            pk = Galleria.Framework.Planograms.Model.Package.FetchById(pk.Id, 1, PackageLockType.User);

            //check
            checkPlanogramBlocking(pk.Planograms[0].Blocking[0]);
        }

        [Test]
        public void Behaviour_MoveDividerVertical()
        {
            PlanogramBlocking blocking = CreateBehaviourTestData();

            //SIMPLE MOVING:

            //move the first divider left
            PlanogramBlockingDivider div = blocking.Dividers[0];
            Assert.AreEqual(PlanogramBlockingDividerType.Vertical, div.Type);
            div.MoveTo(0.40F);

            //check 
            Assert.AreEqual(0.40F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);

            //move right
            div.MoveTo(0.50F);
            Assert.AreEqual(0.50F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);

            //HIT LIMITS:

            //try to move beyond next divider - should get stopped.
            div.MoveTo(0.9F);
            Assert.AreEqual(0.79F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);

            //check the divider we hit has not moved.
            PlanogramBlockingDivider divider2 = blocking.Dividers[1];
            Assert.AreEqual(0.8F, divider2.X);
            Assert.AreEqual(0, divider2.Y);
            Assert.AreEqual(1F, divider2.Length);

            //try to move to 0 - should get stopped
            div.MoveTo(0);
            Assert.AreEqual(0.01F, div.X);
            Assert.AreEqual(0, div.Y);
            Assert.AreEqual(1F, div.Length);
        }

        [Test]
        public void Behaviour_MoveDividerHorizontal()
        {
            PlanogramBlocking blocking = CreateBehaviourTestData();

            //move the divider down
            PlanogramBlockingDivider div = blocking.Dividers[2];
            Assert.AreEqual(PlanogramBlockingDividerType.Horizontal, div.Type);

            //SIMPLE MOVING:
            div.MoveTo(0.40F);

            //check 
            Assert.AreEqual(0.40F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);

            //check that the adjacent vert divider was not affected
            PlanogramBlockingDivider adjDiv = blocking.Dividers[1];
            Assert.AreEqual(0.8F, adjDiv.X);
            Assert.AreEqual(0F, adjDiv.Y);
            Assert.AreEqual(1F, adjDiv.Length);

            //move up
            div.MoveTo(0.70F);
            Assert.AreEqual(0.70F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);

            //check that the adjacent vert divider was not affected
            Assert.AreEqual(0.8F, adjDiv.X);
            Assert.AreEqual(0F, adjDiv.Y);
            Assert.AreEqual(1F, adjDiv.Length);


            //HIT LIMITS:

            //move to 0 - should get stopped
            div.MoveTo(0);
            Assert.AreEqual(0.01F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);

            //move to bottom - should get stopped
            div.MoveTo(1);
            Assert.AreEqual(0.99F, div.Y);
            Assert.AreEqual(0.8F, div.X);
            Assert.AreEqual(0.2F, div.Length);
        }

        [Test]
        public void Behaviour_MoveDividerDifferentVerticalBands()
        {
            PlanogramBlocking blocking = CreateBehaviourTestData();

            PlanogramBlockingDivider div1 = blocking.Dividers[0];
            PlanogramBlockingDivider div2 = blocking.Dividers[1];
            PlanogramBlockingDivider div3 = blocking.Dividers[2];

            //add a new horizontal divider in the vert block before
            PlanogramBlockingDivider div4 = blocking.Dividers.Add(div1.X, 0.6F, PlanogramBlockingDividerType.Horizontal);

            //try to move div 4 past div 3
            // should be allowed as div 3 is not in same vert band
            div4.MoveTo(0.3F);
            Assert.AreEqual(0.3F, div4.Y);

            //div 3 should be unaffected
            Assert.AreEqual(0.5F, div3.Y);
        }

        [Test]
        public void Behaviour_AddDividerHorizontal()
        {
            PlanogramBlocking blocking = CreateBehaviourTestData();
            PlanogramBlockingDivider div1 = blocking.Dividers[0];
            PlanogramBlockingDivider div2 = blocking.Dividers[1];
            PlanogramBlockingDivider div3 = blocking.Dividers[2];

            //add a new horizontal divider
            PlanogramBlockingDivider div4 = blocking.Dividers.Add(blocking.Dividers[0].X, 0.6F, PlanogramBlockingDividerType.Horizontal);
            Assert.AreEqual((Byte)2, div4.Level);

            Assert.AreEqual(blocking.Groups.Count, 5, "A new group should have been added");
            Assert.AreEqual(blocking.Locations.Count, 5, "A new loc should have been added");

            //check the new loc
            PlanogramBlockingLocation newLoc = blocking.Locations[4];
            Assert.AreEqual(blocking.Groups[4].Id, newLoc.PlanogramBlockingGroupId);
            Assert.AreEqual(div1.Id, newLoc.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(div2.Id, newLoc.PlanogramBlockingDividerRightId);
            Assert.AreEqual(null, newLoc.PlanogramBlockingDividerTopId);
            Assert.AreEqual(div4.Id, newLoc.PlanogramBlockingDividerBottomId);

            //check the existing loc
            PlanogramBlockingLocation loc2 = blocking.Locations[1];
            Assert.AreEqual(blocking.Groups[1].Id, loc2.PlanogramBlockingGroupId);
            Assert.AreEqual(div1.Id, loc2.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(div2.Id, loc2.PlanogramBlockingDividerRightId);
            Assert.AreEqual(div4.Id, loc2.PlanogramBlockingDividerTopId);
            Assert.AreEqual(null, loc2.PlanogramBlockingDividerBottomId);
        }

        [Test]
        public void Behaviour_AddDividerVertical()
        {
            PlanogramBlocking blocking = CreateBehaviourTestData();
            PlanogramBlockingDivider div1 = blocking.Dividers[0];
            PlanogramBlockingDivider div2 = blocking.Dividers[1];
            PlanogramBlockingDivider div3 = blocking.Dividers[2];

            //Add a new vertical divider
            PlanogramBlockingDivider div4 = blocking.Dividers.Add(0.6F, 0, PlanogramBlockingDividerType.Vertical);
            Assert.AreEqual((Byte)1, div4.Level);

            Assert.AreEqual(blocking.Groups.Count, 5, "A new group should have been added");
            Assert.AreEqual(blocking.Locations.Count, 5, "A new loc should have been added");

            //check the new loc
            PlanogramBlockingLocation newLoc = blocking.Locations[4];
            Assert.AreEqual(blocking.Groups[4].Id, newLoc.PlanogramBlockingGroupId);
            Assert.AreEqual(div4.Id, newLoc.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(div2.Id, newLoc.PlanogramBlockingDividerRightId);
            Assert.AreEqual(null, newLoc.PlanogramBlockingDividerTopId);
            Assert.AreEqual(null, newLoc.PlanogramBlockingDividerBottomId);

            //check the existing loc
            PlanogramBlockingLocation loc2 = blocking.Locations[1];
            Assert.AreEqual(blocking.Groups[1].Id, loc2.PlanogramBlockingGroupId);
            Assert.AreEqual(div1.Id, loc2.PlanogramBlockingDividerLeftId);
            Assert.AreEqual(div4.Id, loc2.PlanogramBlockingDividerRightId);
            Assert.AreEqual(null, loc2.PlanogramBlockingDividerTopId);
            Assert.AreEqual(null, loc2.PlanogramBlockingDividerBottomId);
        }

        [Test]
        public void Behaviour_RemoveDivider1()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramBlocking b = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            plan.Blocking.Add(b);
            PlanogramBlockingDivider div1 = b.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            PlanogramBlockingDivider div2 = b.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            PlanogramBlockingDivider div3 = b.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);
            PlanogramBlockingLocation loc1 = b.Locations[0];
            PlanogramBlockingLocation loc2 = b.Locations[1];
            PlanogramBlockingLocation loc3 = b.Locations[2];
            PlanogramBlockingLocation loc4 = b.Locations[3];
            #region Check Initial State
            //check initial state
            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.45F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);

            Assert.AreEqual(0.45F, loc2.X);
            Assert.AreEqual(0, loc2.Y);
            Assert.AreEqual(0.35F, Convert.ToSingle(Math.Round(loc2.Width, 5)));
            Assert.AreEqual(1, Convert.ToSingle(Math.Round(loc2.Height, 5)));

            Assert.AreEqual(0.8F, loc3.X);
            Assert.AreEqual(0F, loc3.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc3.Width, 5)));
            Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc3.Height, 5)));

            Assert.AreEqual(0.8F, loc4.X);
            Assert.AreEqual(0.5, loc4.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc4.Width, 5)));
            Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc4.Height, 5))); 
            #endregion

            //plan.TakeSnapshot("Behaviour_RemoveDivider1_Before");
            b.Dividers.Remove(div1);
            //plan.TakeSnapshot("Behaviour_RemoveDivider1_After");

            Assert.AreEqual(2, b.Dividers.Count);
            Assert.AreEqual(3, b.Groups.Count);
            Assert.AreEqual(3, b.Locations.Count);

            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.8F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);
            Assert.AreEqual(div2.Id, loc1.PlanogramBlockingDividerRightId, "div2 should not be loc1 right divider");

            //loc2 should have been removed.
            Assert.AreEqual(0.8F, loc3.X);
            Assert.AreEqual(0F, loc3.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc3.Width, 5)));
            Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc3.Height, 5)));

            Assert.AreEqual(0.8F, loc4.X);
            Assert.AreEqual(0.5, loc4.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc4.Width, 5)));
            Assert.AreEqual(0.5F, Convert.ToSingle(Math.Round(loc4.Height, 5)));
        }

        [Test]
        public void Behaviour_RemoveDivider2()
        {
            PlanogramBlocking b = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            b.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlockingDivider div1 = b.Dividers[0];
            PlanogramBlockingDivider div2 = b.Dividers[1];
            PlanogramBlockingDivider div3 = b.Dividers[2];

            PlanogramBlockingLocation loc1 = b.Locations[0];
            PlanogramBlockingLocation loc2 = b.Locations[1];
            PlanogramBlockingLocation loc3 = b.Locations[2];
            PlanogramBlockingLocation loc4 = b.Locations[3];

            //remove divider 2
            b.Dividers.Remove(b.Dividers[1]);
            Assert.AreEqual(1, b.Dividers.Count);
            Assert.AreEqual(2, b.Groups.Count);
            Assert.AreEqual(2, b.Locations.Count);

            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.45F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);

            Assert.AreEqual(0.45F, loc2.X);
            Assert.AreEqual(0, loc2.Y);
            Assert.AreEqual(0.55F, Convert.ToSingle(Math.Round(loc2.Width, 5)));
            Assert.AreEqual(1, Convert.ToSingle(Math.Round(loc2.Height, 5)));
            Assert.AreEqual(loc2.PlanogramBlockingDividerRightId, null, "loc2 should not be at the edge");

            //loc3 - removed
            //loc4 - removed
        }

        [Test]
        public void Behaviour_RemoveDivider3()
        {
            PlanogramBlocking b = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            b.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlockingDivider div1 = b.Dividers[0];
            PlanogramBlockingDivider div2 = b.Dividers[1];
            PlanogramBlockingDivider div3 = b.Dividers[2];

            PlanogramBlockingLocation loc1 = b.Locations[0];
            PlanogramBlockingLocation loc2 = b.Locations[1];
            PlanogramBlockingLocation loc3 = b.Locations[2];
            PlanogramBlockingLocation loc4 = b.Locations[3];

            //remove divider 3
            b.Dividers.Remove(b.Dividers[2]);
            Assert.AreEqual(b.Dividers.Count, 2);
            Assert.AreEqual(3, b.Groups.Count);
            Assert.AreEqual(3, b.Locations.Count);

            Assert.AreEqual(0, loc1.X);
            Assert.AreEqual(0, loc1.Y);
            Assert.AreEqual(0.45F, loc1.Width);
            Assert.AreEqual(1, loc1.Height);

            Assert.AreEqual(0.45F, loc2.X);
            Assert.AreEqual(0, loc2.Y);
            Assert.AreEqual(0.35F, Convert.ToSingle(Math.Round(loc2.Width, 5)));
            Assert.AreEqual(1, Convert.ToSingle(Math.Round(loc2.Height, 5)));

            Assert.AreEqual(0.8F, loc3.X);
            Assert.AreEqual(0F, loc3.Y);
            Assert.AreEqual(0.2F, Convert.ToSingle(Math.Round(loc3.Width, 5)));
            Assert.AreEqual(1F, Convert.ToSingle(Math.Round(loc3.Height, 5)));
            Assert.AreEqual(loc3.PlanogramBlockingDividerTopId, null, "loc 3 should now hit the top");

            //loc 4 removed
        }

        [Test]
        public void Behaviour_MoveL2HorizResizesL3Vert()
        {
            PlanogramBlocking b = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);

            b.Dividers.Add(0.50F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.50F, 0.6F, PlanogramBlockingDividerType.Horizontal);
            b.Dividers.Add(0.70F, 0, PlanogramBlockingDividerType.Vertical);

            PlanogramBlockingDivider divider1 = b.Dividers[0];
            PlanogramBlockingDivider divider2 = b.Dividers[1];
            PlanogramBlockingDivider divider3 = b.Dividers[2];

            Assert.AreEqual(1, divider1.Level);
            Assert.AreEqual(2, divider2.Level);
            Assert.AreEqual(3, divider3.Level);

            //move div2 down
            divider2.MoveTo(0.5F);

            Assert.AreEqual(0.7F, divider3.X, "Divider 3 should not have moved.");
            Assert.AreEqual(0.5F, divider3.Length, "Divider 3 should have resized");

        }

        [Test]
        public void Behaviour_MoveVertResizesAdjacent()
        {
            PlanogramBlocking b = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);


            //V30, V60, h0 75, h0 50
            b.Dividers.Add(0.30F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0.60F, 0, PlanogramBlockingDividerType.Vertical);
            b.Dividers.Add(0, 0.75F, PlanogramBlockingDividerType.Horizontal);
            b.Dividers.Add(0.3F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlockingDivider divider1 = b.Dividers[0];
            PlanogramBlockingDivider divider2 = b.Dividers[1];
            PlanogramBlockingDivider divider3 = b.Dividers[2];
            PlanogramBlockingDivider divider4 = b.Dividers[3];

            //move v1 to 70
            divider1.MoveTo(0.7F);

            Assert.AreEqual(0.59F, divider3.Length);
            Assert.AreEqual(0.59F, divider4.X, 5);
            Assert.AreEqual(0.01F, divider4.Length);


            //move v1 to 50
            divider1.MoveTo(0.5F);

            Assert.AreEqual(0.5F, divider3.Length);
            Assert.AreEqual(0.5F, divider4.X);
            Assert.AreEqual(0.1F, divider4.Length);
        }

        [Test]
        public void Behaviour_GEM27422DeleteTShape()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramBlocking b = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            plan.Blocking.Add(b);

            //add dividers to create the shape
            PlanogramBlockingDivider d1 = b.Dividers.Add(0.25F, 0, PlanogramBlockingDividerType.Vertical);
            PlanogramBlockingDivider d2 = b.Dividers.Add(0.25F, 0.75F, PlanogramBlockingDividerType.Horizontal);
            PlanogramBlockingDivider d3 = b.Dividers.Add(0.25F, 0.5F, PlanogramBlockingDividerType.Horizontal);
            PlanogramBlockingDivider d4 = b.Dividers.Add(0.6F, 0, PlanogramBlockingDividerType.Vertical);

            PlanogramBlockingLocation l1 = b.Locations[0];
            PlanogramBlockingLocation l2 = b.Locations[1];
            PlanogramBlockingLocation l3 = b.Locations[2];
            PlanogramBlockingLocation l4 = b.Locations[3];
            PlanogramBlockingLocation l5 = b.Locations[4];


            //remove divider 3
            //plan.TakeSnapshot("Behaviour_GEM27422DeleteTShape_Before");
            b.Dividers.Remove(d3);
            //plan.TakeSnapshot("Behaviour_GEM27422DeleteTShape_After");

            Assert.That(d3.IsDeleted);
            Assert.That(d4.IsDeleted);
            Assert.That(b.Dividers, Has.Count.EqualTo(2));
            Assert.That(b.Locations, Has.Count.EqualTo(3));
            Assert.That(b.Locations.First(l => l.X.EqualTo(0.25f) && l.Y.EqualTo(0)).GetTopDivider(), Is.SameAs(d2));
        }

        #endregion

        #region Get Blocking methods

        [Test]
        public void GetLocationsBetweenDividers_ReturnsLocationsBetweenHorizontalRootDividers()
        {
            //       _______________
            //      |      [B]      |
            //      |______(2)______|
            //      |               |
            //      |   Location A  |
            //      |               |
            //      |______(1)______|
            //      |      [C]      |
            //      |_______________|
            //      

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.3f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.7f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);

            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider2, divider1, blocking.Locations);
            TestDataHelper.CreateBlockingLocation(null, null, null, divider2, blocking.Locations);
            TestDataHelper.CreateBlockingLocation(null, null, divider1, null, blocking.Locations);

            var actual = blocking.GetLocationsBetweenDividers(divider1, divider2);
            CollectionAssert.AreEqual(new[] { locationA }, actual.ToList());

            actual = blocking.GetLocationsBetweenDividers(divider2, divider1);
            CollectionAssert.AreEqual(new[] { locationA }, actual.ToList());
        }

        [Test]
        public void GetLocationsBetweenDividers_ReturnsAllLocationsWhenGivenNulls()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.3f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.7f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);

            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider2, divider1, blocking.Locations);
            var locationB = TestDataHelper.CreateBlockingLocation(null, null, null, divider2, blocking.Locations);
            var locationC = TestDataHelper.CreateBlockingLocation(null, null, divider1, null, blocking.Locations);

            var actual = blocking.GetLocationsBetweenDividers(null, null);
            CollectionAssert.AreEquivalent(blocking.Locations, actual.ToList());
        }

        [Test]
        public void GetLocationsBetweenDividers_ReturnsLocationsBetweenHorizontalRootDividerAndEdge()
        {
            //       _______________
            //      |      [B]      |
            //      |______(2)______|
            //      |               |
            //      |   Location A  |
            //      |               |
            //      |______(1)______|
            //      |      [C]      |
            //      |_______________|
            //      

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.3f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.7f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);

            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider2, divider1, blocking.Locations);
            var locationB = TestDataHelper.CreateBlockingLocation(null, null, null, divider2, blocking.Locations);
            var locationC = TestDataHelper.CreateBlockingLocation(null, null, divider1, null, blocking.Locations);

            var actual = blocking.GetLocationsBetweenDividers(divider1, null);
            CollectionAssert.AreEquivalent(new[] { locationA, locationB }, actual.ToList());
            actual = blocking.GetLocationsBetweenDividers(null, divider1);
            CollectionAssert.AreEquivalent(new[] { locationC }, actual.ToList());

            actual = blocking.GetLocationsBetweenDividers(null, divider2);
            CollectionAssert.AreEquivalent(new[] { locationA, locationC }, actual.ToList());
            actual = blocking.GetLocationsBetweenDividers(divider2, null);
            CollectionAssert.AreEquivalent(new[] { locationB }, actual.ToList());
        }

        [Test]
        public void GetLocationsBetweenDividers_ReturnsLocationsBetweenVerticalRootDividerAndEdge()
        {
            //       ___________________________
            //      |       |           |       |
            //      |       |           |       |   
            //      |   C  (1)    A    (2)  B   |
            //      |       |           |       |
            //      |_______|___________|_______|
            //      

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0.3f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0.7f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);

            var locationA = TestDataHelper.CreateBlockingLocation(divider1, divider2, null, null, blocking.Locations);
            var locationB = TestDataHelper.CreateBlockingLocation(divider2, null, null, null, blocking.Locations);
            var locationC = TestDataHelper.CreateBlockingLocation(null, divider1, null, null, blocking.Locations);

            var actual = blocking.GetLocationsBetweenDividers(divider1, null);
            CollectionAssert.AreEquivalent(new[] { locationA, locationB }, actual.ToList());
            actual = blocking.GetLocationsBetweenDividers(null, divider1);
            CollectionAssert.AreEquivalent(new[] { locationC }, actual.ToList());

            actual = blocking.GetLocationsBetweenDividers(divider2, null);
            CollectionAssert.AreEquivalent(new[] { locationB }, actual.ToList());
            actual = blocking.GetLocationsBetweenDividers(null, divider2);
            CollectionAssert.AreEquivalent(new[] { locationA, locationC }, actual.ToList());
        }

        [Test]
        public void GetLocationsBetweenDividers_ReturnsLocationsBetweenVerticalRootDividers()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0.3f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0.7f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);

            var locationA = TestDataHelper.CreateBlockingLocation(divider1, divider2, null, null, blocking.Locations);
            TestDataHelper.CreateBlockingLocation(null, divider1, null, null, blocking.Locations);
            TestDataHelper.CreateBlockingLocation(divider2, null, null, null, blocking.Locations);

            var actual = blocking.GetLocationsBetweenDividers(divider1, divider2);
            CollectionAssert.AreEqual(new[] { locationA }, actual.ToList());

            actual = blocking.GetLocationsBetweenDividers(divider2, divider1);
            CollectionAssert.AreEqual(new[] { locationA }, actual.ToList());
        }

        [Test]
        public void GetLocationsBetweenDividers_GetsLocationsWhenSinglesYieldRoundingErrors()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.2854f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.4659f, PlanogramBlockingDividerType.Horizontal);
            var verticalDivider = blocking.Dividers.Add(0.251f, 0.4659f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.749f, 0.4659f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5219f, 0.4659f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.1882f, 0.2854f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.749f, 0.5878f, PlanogramBlockingDividerType.Horizontal);
            var topLeftLocation = blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0.466f));

            var actualLocations = blocking.GetLocationsBetweenDividers(null, verticalDivider);

            actualLocations.Should().HaveCount(1, "because there is one location between the divider and the edge of the plan");
            actualLocations.First().Should().BeSameAs(topLeftLocation, "because the location between the vertical divider and the edge of the plan is the top left one.");
        }

        [Test]
        public void GetLocationsBetweenDividers_ReturnsLocationsBetweenComplexDividers()
        {
            //       _______________
            //      |        C      |  
            //      |___(1)_________|  
            //      |       |__(4)_D|  
            //      |      (3)     E|  
            //      |   B   |__(5)__|  
            //      |       |      F|  
            //      |__(2)__|_______|  
            //      |       A       |  
            //      |_______________|  
            //

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0.5f, 0.2f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var divider4 = TestDataHelper.CreateBlockingDivider(0.5f, 0.7f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var divider5 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);

            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider2, null, blocking.Locations);
            var locationB = TestDataHelper.CreateBlockingLocation(null, divider3, divider1, divider2, blocking.Locations);
            var locationC = TestDataHelper.CreateBlockingLocation(null, null, null, divider1, blocking.Locations);
            var locationD = TestDataHelper.CreateBlockingLocation(divider3, null, divider1, divider4, blocking.Locations);
            var locationE = TestDataHelper.CreateBlockingLocation(divider3, null, divider4, divider5, blocking.Locations);
            var locationF = TestDataHelper.CreateBlockingLocation(divider3, null, divider5, divider2, blocking.Locations);

            var actual = blocking.GetLocationsBetweenDividers(divider2, divider4);
            CollectionAssert.AreEquivalent(new[] { locationF, locationE }, actual.ToList());

            actual = blocking.GetLocationsBetweenDividers(divider4, divider2);
            CollectionAssert.AreEquivalent(new[] { locationF, locationE }, actual.ToList());
        }

        #endregion

        #region ApplyToFixtures

        private void AddFixtures(Int32 numberOfFixtures, Planogram planogram)
        {
            var fixture = PlanogramFixture.NewPlanogramFixture();
            planogram.Fixtures.Add(fixture);
            fixture.Width = 10;
            fixture.Height = 10;
            for (Int32 i = 0; i < numberOfFixtures; i++)
            {
                planogram.FixtureItems.Add(fixture);
            }
        }

        [Test]
        public void ApplyToFixtures_WhenTooManyDividersForFixtures_ShouldDropMinimumDividersToSnap()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 53, 0));
            bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 141, 0));
            var initial = plan.AddBlocking(PlanogramBlockingType.Initial);
            initial.Dividers.Add(0, 0.207f, PlanogramBlockingDividerType.Horizontal);
            initial.Dividers.Add(0, 0.493f, PlanogramBlockingDividerType.Horizontal);
            initial.Dividers.Add(0.512f, 0.1f, PlanogramBlockingDividerType.Vertical);
            initial.Dividers.Add(0.502f, 0.8f, PlanogramBlockingDividerType.Vertical);
            initial.Dividers.Add(0.25f, 0.102f, PlanogramBlockingDividerType.Horizontal);
            initial.Dividers.Add(0.75f, 0.802f, PlanogramBlockingDividerType.Horizontal);
            initial.Dividers.Add(0.773f, 0.9f, PlanogramBlockingDividerType.Vertical);
            var final = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initial);
            plan.Blocking.Add(final);

            final.ApplyToFixtures();
            plan.TakeSnapshot("ApplyToFixtures_WhenTooManyDividersForFixtures_ShouldDropMinimumDividersToSnap");

            Assert.That(final.Dividers, Has.Count.EqualTo(2));
            Assert.That(final.Locations, Has.Count.EqualTo(3));
            Assert.That(final.Dividers.Count(d => d.Type == PlanogramBlockingDividerType.Horizontal), Is.EqualTo(1));
            Assert.That(final.Dividers.Count(d => d.Type == PlanogramBlockingDividerType.Vertical), Is.EqualTo(1));
        }

        [Test]
        public void ApplyToFixtures_MultiBayShelvesWithSingleBayBlocking()
        {
            var planogram = CreateMultiBayShelvesWithSingleBayBlockingPlanogram();
            var initial = CreateSingleBayShelfBlocking(planogram);
            initial.Type = PlanogramBlockingType.Initial;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initial);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.TakeSnapshot("ApplyToFixtures_MultiBayShelvesWithSingleBayBlocking");

            AssertDividerPositionsInSequence(DividerPosition.Y, new[] { 161f / 200f, 121f / 200f, 79f / 200f, 47f / 200f }, blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_ShelvesWithMatchingBlocking()
        {
            var planogram = CreateShelvesWithMatchingBlockingPlanogram();
            var blocking = CreateSingleBayShelfBlocking(planogram);

            blocking.ApplyToFixtures();

            AssertDividerPositionsInSequence(DividerPosition.Y, new[] { 165f / 200f, 128f / 200f, 90f / 200f, 51f / 200f }, blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_ShelvesInAssemblyWithMatchingBlocking()
        {
            var planogram = CreateShelvesInAssemblyWithMatchingBlockingPlanogram();
            var blocking = CreateSingleBayShelfBlocking(planogram);

            blocking.ApplyToFixtures();

            AssertDividerPositionsInSequence(DividerPosition.Y, new[] { 165f / 200f, 128f / 200f, 90f / 200f, 51f / 200f }, blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_ThreeBayShelvesWithThreeBayBlocking()
        {
            var planogram = CreateThreeBayShelvesWithThreeBayBlockingPlanogram();
            var blocking = CreateThreeBayShelfBlocking(planogram);

            blocking.ApplyToFixtures();

            AssertDividerPositionsInSequence(DividerPosition.X, new[] { 0.333333f, 0.6666666f }, blocking.Dividers);
            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[] 
                { 
                    0f,0f, 
                    165f/200f, 128f/200f,90f/200f,51f/200f,
                    172f/200f,134f/200f,93f/200f,56f/200f,
                    161f/200f,121f/200f,79f/200f,47f/200f 
                },
                blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_FourBayShelvesWithThreeBayBlocking()
        {
            var planogram = CreateFourBayShelvesWithThreeBayBlockingPlanogram();
            var initial = CreateThreeBayShelfBlocking(planogram);
            initial.Type = PlanogramBlockingType.Initial;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initial);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.TakeSnapshot("ApplyToFixtures_FourBayShelvesWithThreeBayBlocking");

            AssertDividerPositionsInSequence(DividerPosition.X, new[] { 0.25f, 0.5f }, blocking.Dividers);
            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[] 
                { 
                    0f, // 0
                    0f, // 1
                    165f / 200f, // 2
                    128f / 200f, // 3
                    90f / 200f, // 4
                    51f / 200f, // 5
                    172f / 200f, // 6
                    134f / 200f, // 7
                    93f / 200f, // 8
                    56f / 200f, // 9
                    161f / 200f, // 10
                    121f / 200f, // 11
                    79f / 200f, // 12
                    47f / 200f // 13
                },
                blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_TwoBayShelvesWithThreeBayBlocking()
        {
            var planogram = CreateTwoBayShelvesWithThreeBayBlockingPlanogram();
            var blocking = CreateThreeBayShelfBlocking(planogram);

            blocking.ApplyToFixtures();

            AssertDividerPositionsInSequence(DividerPosition.X, new[] { 0.208333f, 0.5f }, blocking.Dividers);
            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[]
                {
                    0f, 0f, 
                    165f / 200f, 128f / 200f, 90f / 200f, 51f / 200f, 
                    165f / 200f, 128f / 200f, 90f / 200f, 51f / 200f, 
                    172f/200f,134f/200f,93f/200f,56f/200f
                },
                blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_OneBayShelvesWithThreeBayBlocking()
        {
            var planogram = CreateOneBayShelvesWithThreeBayBlockingPlanogram();
            var initialBlocking = CreateThreeBayShelfBlocking(planogram);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.Parent.SaveAs(0, @"C:\users\usr145\after.pog");

            AssertDividerPositionsInSequence(DividerPosition.X, new[] { 0.25f, 0.6f }, blocking.Dividers);
            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[]
                {
                    0f, 0f, 
                    165f / 200f, 128f / 200f, 90f / 200f, 51f / 200f, 
                    165f / 200f, 128f / 200f, 90f / 200f, 51f / 200f, 
                    165f / 200f, 128f / 200f, 90f / 200f, 51f / 200f
                },
                blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_ShelvesThatDontMatchBlockingInsufficientComponentSpace_DividersArePreserved()
        {
            var planogram = CreateShelvesThatDontMatchBlockingInsufficientComponentSpacePlanogram();
            var initialBlocking = CreateOneBayShelvesBigBlockInTheMiddleBlocking(planogram);
            initialBlocking.Type = PlanogramBlockingType.Initial;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.TakeSnapshot("ApplyToFixtures_ShelvesThatDontMatchBlockingInsufficientComponentSpace_DividersArePreserved");

            Assert.AreEqual(3, blocking.Dividers.Count);
            Assert.AreEqual(4, blocking.Locations.Count());
            Assert.AreEqual(4, blocking.Groups.Count());
        }

        [Test]
        public void ApplyToFixtures_ShelvesThatDontMatchBlockingSufficientComponentSpace_DividersArePreserved()
        {
            var planogram = CreateShelvesThatDontMatchBlockingSufficientComponentSpacePlanogram();
            var initialBlocking = CreateOneBayShelvesBigBlockInTheMiddleBlocking(planogram);
            initialBlocking.Type = PlanogramBlockingType.Initial;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.Parent.SaveAs(0, @"C:\users\usr145\after.pog");

            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[] { 184f / 200f, 153f / 200f, 124f / 200f, 86f / 200f },
                blocking.Dividers);
            Assert.AreEqual(4, blocking.Dividers.Count);
            Assert.AreEqual(5, blocking.Locations.Count());
            Assert.AreEqual(5, blocking.Groups.Count());
        }

        [Test]
        public void ApplyToFixtures_PegBoardWithDividersOneBayBlocking()
        {
            var planogram = CreatePegBoardWithDividersOneBayBlockingPlanogram();
            var initialBlocking = CreateOneBayPegboardBlocking(planogram);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.Parent.SaveAs(0, @"C:\users\usr145\after.pog");

            AssertDividerPositionsInSequence(
                DividerPosition.X,
                new[] { 0f, 0f, 0.417f, 0.75f, 0f, 0.417f, 0.75f },
                blocking.Dividers);
            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[] { 0.48f, 0.215f, 0.48f, 0.48f, 0.73f, 0.73f, 0.73f },
                blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_PegBoardNoDividersOneBayBlocking()
        {
            var planogram = CreatePegBoardNoDividersOneBayBlockingPlanogram();
            var initialBlocking = CreateOneBayPegboardBlocking(planogram);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.Parent.SaveAs(0, @"C:\users\usr145\after.pog");

            AssertDividerPositionsInSequence(
                DividerPosition.X,
                new[] { 0f, 0f, 0.28f, 0.682f, 0f, 0.28f, 0.682f },
                blocking.Dividers);
            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[] { 0.48f, 0.215f, 0.48f, 0.48f, 0.68f, 0.72f, 0.76f },
                blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_PegBoardWithInsufficientComponentSpaceForBlocking_DividersArePreserved()
        {
            var planogram = CreatePegBoardNoDividersOneBayBlockingPlanogram();
            var initial = CreatePegBoardBlockingWithTooManyDividers(planogram);
            initial.Type = PlanogramBlockingType.Initial;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initial);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.TakeSnapshot("ApplyToFixtures_PegBoardWithInsufficientComponentSpaceForBlocking_DividersAreDropped");

            Assert.AreEqual(3, blocking.Dividers.Count);
            Assert.AreEqual(4, blocking.Locations.Count());
            Assert.AreEqual(4, blocking.Groups.Count());
        }

        [Test]
        public void ApplyToFixtures_MultiBayPegBoardOneBayBlocking()
        {
            var planogram = CreateMultiBayPegBoardOneBayBlockingPlanogram();
            var initialBlocking = CreateOneBayPegboardBlocking(planogram);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.Parent.SaveAs(0, @"C:\users\usr145\after.pog");

            AssertDividerPositionsInSequence(
                DividerPosition.X,
                new[] { 0f, 0f, 0.28f, 0.682f, 0f, 0.28f, 0.682f },
                blocking.Dividers);
            AssertDividerPositionsInSequence(
                DividerPosition.Y,
                new[] { 0.48f, 0.215f, 0.48f, 0.48f, 0.68f, 0.72f, 0.76f },
                blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_ChestWithHorizontalDividers()
        {
            var planogram = CreateChestWithHorizontalDividersPlanogram();
            var initialBlocking = CreateChestWithHorizontalDividersBlocking(planogram);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.Parent.SaveAs(0, @"C:\Users\usr145\after.pog");

            AssertDividerPositionsInSequence(DividerPosition.X, new[] { 0f, 0f, 0f, 0f }, blocking.Dividers);
            AssertDividerPositionsInSequence(DividerPosition.Y, new[] { 0.827f, 0.584f, 0.229f, 0.126f }, blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_ChestWithVerticalDividers()
        {
            var planogram = CreateChestWithVerticalDividersPlanogram();
            var initialBlocking = CreateChestWithVertcalDividersBlocking(planogram);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            planogram.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //planogram.Parent.SaveAs(0, @"C:\users\usr145\after.pog");

            AssertDividerPositionsInSequence(DividerPosition.X, new[] { 0f, 0f, 0.342f, 0.65f }, blocking.Dividers);
            AssertDividerPositionsInSequence(DividerPosition.Y, new[] { 0.827f, 0.584f, 0f, 0f }, blocking.Dividers);
        }

        [Test]
        public void ApplyToFixtures_SlopedChestWithHorizontalDividers_PreservesDividers()
        {
            var planogram = CreateSlopedChestWithHorizontalDividersPlanogram();
            var blocking = CreateChestWithHorizontalDividersBlocking(planogram);

            blocking.ApplyToFixtures();

            Assert.AreEqual(4, blocking.Dividers.Count);
            Assert.AreEqual(5, blocking.Locations.Count());
            Assert.AreEqual(5, blocking.Groups.Count());
        }

        [Test]
        public void ApplyToFixtures_Vertical_WhenCloseToBoundary()
        {
            //   _________________:_____
            //  |           |     :     |   Fixtures A and B.
            //  |    A      |  B  :     |   Divider (1).
            //  |           |    (1)    |
            //  |___________|_____:_____|
            //                    :

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            AddFixtures(2, planogram);
            var divider = TestDataHelper.CreateBlockingDivider(
                0.6f, 0f, 1, PlanogramBlockingDividerType.Vertical, PlanogramBlockingDivider.RootLevelNumber, blocking.Dividers);

            blocking.ApplyToFixtures();

            Assert.AreEqual(0.5f, divider.X);
        }

        [Test]
        public void ApplyToFixtures_Vertical_WhenCloseToEdge()
        {
            //   __________________:____
            //  |           |      :    |   Fixtures A and B.
            //  |    A      |  B   :    |   Divider (1).
            //  |           |     (1)   |
            //  |___________|______:____|
            //                     :

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            AddFixtures(2, planogram);
            var divider = TestDataHelper.CreateBlockingDivider(
                0.8f, 0f, 1, PlanogramBlockingDividerType.Vertical, PlanogramBlockingDivider.RootLevelNumber, blocking.Dividers);

            blocking.ApplyToFixtures();

            Assert.AreEqual(0.5f, divider.X);
        }

        [Test]
        public void ApplyToFixtures_Vertical_WhenInsufficientBoundaries()
        {
            //   _________:________:____
            //  |         : |      :    |   Fixtures A and B.
            //  |    A    : |  B   :    |   Dividers (1) and (2).
            //  |        (1)|     (2)   |
            //  |_________:_|______:____|
            //            :        :

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            AddFixtures(2, planogram);
            var divider1 = TestDataHelper.CreateBlockingDivider(
                0.4f, 0f, 1, PlanogramBlockingDividerType.Vertical, PlanogramBlockingDivider.RootLevelNumber, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(
                0.7f, 0f, 1, PlanogramBlockingDividerType.Vertical, PlanogramBlockingDivider.RootLevelNumber, blocking.Dividers);

            blocking.ApplyToFixtures();

            Assert.AreEqual(0.5f, divider1.X);
            Assert.AreEqual(0.75f, divider2.X);
        }

        [Test]
        public void ApplyToFixtures_Horizontal_WhenDividersCloseToComponents()
        {
            //   ___________
            //  |           |
            //..|....(1)....|..     Divider (1).
            //  |_____A_____|       Component A: 10 wide, 5 deep, 0 high.
            //  |           |
            //  |_____B_____|
            //  |___________|
            //

            var planogram = CreatePlanogram(1);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            var divider = blocking.Dividers.Add(0f, 0.7f, PlanogramBlockingDividerType.Horizontal);
            GetBay(planogram, 1).GetPlanogramFixture().Components.Add(NewFixtureComponent(_shelf, 0f, 100f, 0f));
            GetBay(planogram, 1).GetPlanogramFixture().Components.Add(NewFixtureComponent(_shelf, 0f, 50f, 0f));

            blocking.ApplyToFixtures();

            Assert.AreEqual(0.5f, divider.Y);
        }

        [Test]
        public void ApplyToFixtures_Horizontal_WhenDividersCloseToEdge()
        {
            //   ___________
            //..|....(1)....|..     Divider (1).
            //  |           |
            //  |_____A_____|       Component A: 10 wide, 5 deep, 0 high.
            //  |           |
            //  |           |
            //  |___________|
            //

            var planogram = CreatePlanogram(1);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            var divider = blocking.Dividers.Add(0f, 0.9f, PlanogramBlockingDividerType.Horizontal);
            GetBay(planogram, 1).GetPlanogramFixture().Components.Add(NewFixtureComponent(_shelf, 0f, 100f, 0f));
            GetBay(planogram, 1).GetPlanogramFixture().Components.Add(NewFixtureComponent(_shelf, 0f, 50f, 0f));

            blocking.ApplyToFixtures();

            Assert.AreEqual(0.5f, divider.Y);
        }

        [Test]
        public void ApplyToFixtures_Vertical_WhenIsSnappedFalseButDeltaNegativeLessThanMargin()
        {
            Assert.Ignore("Snapping within threshold has been removed, see V8-30999");
            var planogram = Planogram.NewPlanogram();
            planogram.Height = 10;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            AddFixtures(2, planogram);
            var divider = TestDataHelper.CreateBlockingDivider(0.49f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            divider.IsSnapped = false;

            blocking.ApplyToFixtures();

            AssertHelper.AreSinglesEqual(0.5f, divider.X);
        }

        [Test]
        public void ApplyToFixtures_Vertical_WhenIsSnappedFalseButDeltaPositiveLessThanMargin()
        {
            Assert.Ignore("Snapping within threshold has been removed, see V8-30999");
            var planogram = Planogram.NewPlanogram();
            planogram.Height = 10;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            AddFixtures(2, planogram);
            var divider = TestDataHelper.CreateBlockingDivider(0.53f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            divider.IsSnapped = false;

            blocking.ApplyToFixtures();

            Assert.AreEqual(Math.Round(0.500f, 3), Math.Round(divider.X, 3));
        }

        [Test]
        public void ApplyToFixtures_Vertical_WhenIsSnappedFalseButDeltaPositiveMoreThanMargin()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Height = 10;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            AddFixtures(2, planogram);
            var divider = TestDataHelper.CreateBlockingDivider(0.61f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            divider.IsSnapped = false;

            blocking.ApplyToFixtures();

            Assert.AreEqual(Math.Round(0.610f, 3), Math.Round(divider.X, 3));
        }

        [Test]
        public void ApplyToFixtures_Vertical_WhenIsSnappedFalseButDeltaNegativeMoreThanMargin()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Height = 10;
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            planogram.Blocking.Add(blocking);
            AddFixtures(2, planogram);
            var divider = TestDataHelper.CreateBlockingDivider(0.43f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            divider.IsSnapped = false;

            blocking.ApplyToFixtures();

            Assert.AreEqual(Math.Round(0.430f, 3), Math.Round(divider.X, 3));
        }

        [Test]
        public void ApplyToFixtures_Horizontal_WhenInsufficientComponentsForEachLocation_LocationsAreDropped()
        {
            //       ___________
            //      |           |
            //      |           |
            //    ..|...........|..      <-- Divider
            //      |===========|        <-- Shelf
            //      |___________|
            //

            var plan = "Package".CreatePackage().AddPlanogram();
            plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            var initialBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            plan.Blocking.Add(initialBlocking);
            initialBlocking.Dividers.Add(0f, 0.6f, PlanogramBlockingDividerType.Horizontal);
            var finalBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            plan.Blocking.Add(finalBlocking);

            finalBlocking.ApplyToFixtures();
            //plan.TakeSnapshot("ApplyToFixtures_Horizontal_WhenInsufficientComponentsForEachLocation_LocationsAreDropped");

            Assert.That(finalBlocking.Locations, Has.Count.EqualTo(1));
            Assert.That(finalBlocking.Dividers, Is.Empty);
        }

        [Test]
        public void ApplyToFixtures_Horizontal_WhenInsufficientComponents_DividersAreDropped()
        {
            //   ___________
            //..|....(2)....|..     Divider (2).
            //..|....(1)....|..     Divider (1).
            //  |_____A_____|       Component A: 10 wide, 5 deep, 0 high.
            //  |           |
            //  |_____B_____|
            //  |___________|
            //

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelfA = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var shelfB = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0));
            var initialBlocking = plan.AddBlocking();
            initialBlocking.Dividers.Add(0, 0.8f, PlanogramBlockingDividerType.Horizontal);
            initialBlocking.Dividers.Add(0, 0.9f, PlanogramBlockingDividerType.Horizontal);
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            plan.Blocking.Add(blocking);

            blocking.ApplyToFixtures();
            //plan.TakeSnapshot("ApplyToFixtures_Horizontal_WhenInsufficientComponents_DividersArePreserved");

            Assert.AreEqual(2, blocking.Locations.Count);
            Assert.That(blocking.Dividers, Has.Count.EqualTo(1));
            Assert.AreEqual(0.5f, blocking.Dividers[0].Y);
        }

        [Test]
        public void ApplyToFixtures_WhenSufficientComponentSpace_ShouldAlwaysSnapDividers()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var blocking = plan.AddBlocking();
            var topDivider = blocking.Dividers.Add(0, 0.95f, PlanogramBlockingDividerType.Horizontal);
            var bottomDivider = blocking.Dividers.Add(0, 0.9f, PlanogramBlockingDividerType.Horizontal);

            blocking.ApplyToFixtures();
            //plan.Parent.SaveAs(0, @"C:\users\usr145\after.pog");

            Assert.That(topDivider.Y, Is.EqualTo(0.75f));
            Assert.That(bottomDivider.Y, Is.EqualTo(0.25f));
        }

        [Test]
        public void ApplyToFixtures_WhenSufficientComponentSpaceInLowerLevels_ShouldAlwaysSnapDividers()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var blocking = plan.AddBlocking();
            var topDivider = blocking.Dividers.Add(0, 0.3f, PlanogramBlockingDividerType.Horizontal);
            var verticalDivider = blocking.Dividers.Add(0.5f, 0.15f, PlanogramBlockingDividerType.Vertical);
            var bottomDivider = blocking.Dividers.Add(0, 0.15f, PlanogramBlockingDividerType.Horizontal);

            blocking.ApplyToFixtures();
            //plan.TakeSnapshot("ApplyToFixtures_WhenSufficientComponentSpaceInLowerLevels_ShouldAlwaysSnapDividers");

            Assert.That(blocking.Dividers, Has.Count.EqualTo(3));
            Assert.That(topDivider.Y, Is.EqualTo(0.75f));
            Assert.That(bottomDivider.Y, Is.EqualTo(0.25f));
        }

        [Test]
        public void ApplyToFixtures_ShouldMerge_WhenInsufficientComponentSpace_AndBlocksCanMerge()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            foreach (var g in blocking.Groups) g.CanMerge = true;

            blocking.ApplyToFixtures();

            Assert.That(blocking.Dividers, Is.Empty);
            Assert.That(blocking.Groups, Has.Count.EqualTo(1));
            Assert.That(blocking.Locations, Has.Count.EqualTo(1));
        }

        [Test]
        public void ApplyToFixtures_ShouldNotMerge_WhenInsufficientComponentSpace_AndBlocksCannotMerge()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            foreach (var g in blocking.Groups) g.CanMerge = false;

            blocking.ApplyToFixtures();

            Assert.That(blocking.Dividers, Has.Count.EqualTo(1));
            Assert.That(blocking.Groups, Has.Count.EqualTo(2));
            Assert.That(blocking.Locations, Has.Count.EqualTo(2));
        }

        [Test]
        public void ApplyToFixtures_ShouldMergeBlocksAndSequenceGroups_WhenInsufficientComponentSpace_AndNoMatchingSequenceGroup()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            for (var i = 0; i < 4; i++) plan.AddProduct();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            foreach (var g in blocking.Groups) g.CanMerge = true;
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(2));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(2));

            blocking.ApplyToFixtures();

            Assert.That(blocking.Dividers, Is.Empty);
            Assert.That(blocking.Groups, Has.Count.EqualTo(1));
            Assert.That(blocking.Locations, Has.Count.EqualTo(1));
            Assert.That(plan.Sequence.Groups, Has.Count.EqualTo(3));
            Assert.That(plan.Sequence.Groups.Last().Colour, Is.EqualTo(blocking.Groups.First().Colour));
            Assert.That(plan.Sequence.Groups.Last().Products, Has.Count.EqualTo(4));
        }

        [Test]
        public void ApplyToFixtures_ShouldMergeBlocksUsingMatchingSequenceGroup_WhenInsufficientComponentSpace()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            for (var i = 0; i < 4; i++) plan.AddProduct();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            foreach (var g in blocking.Groups) g.CanMerge = true;
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(2));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(2));
            var mergeSequenceGroup = plan.AddSequenceGroup();
            mergeSequenceGroup.AddSequenceGroupProducts(plan.Products);

            blocking.ApplyToFixtures();

            Assert.That(blocking.Dividers, Is.Empty);
            Assert.That(blocking.Groups, Has.Count.EqualTo(1));
            Assert.That(blocking.Locations, Has.Count.EqualTo(1));
            Assert.That(blocking.Groups.First().Colour, Is.EqualTo(mergeSequenceGroup.Colour));
        }

        [Test]
        public void ApplyToFixtures_LowerLevelGroupsAreCorrectlyCheckedForAvailableBoundaries()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            foreach (var bay in plan.FixtureItems)
            {
                bay.AddFixtureComponent(PlanogramComponentType.Backboard);
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 20, 0));
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            }
            var initialBlocking = plan.AddBlocking(PlanogramBlockingType.Initial);
            initialBlocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            initialBlocking.Dividers.Add(0.75f, 0.52f, PlanogramBlockingDividerType.Horizontal);
            initialBlocking.Dividers.Add(0.25f, 0.12f, PlanogramBlockingDividerType.Horizontal);
            initialBlocking.Dividers.Add(0.2f, 0.5f, PlanogramBlockingDividerType.Vertical);
            initialBlocking.Dividers.Add(0.35f, 0.77f, PlanogramBlockingDividerType.Horizontal);
            var finalBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initialBlocking);
            plan.Blocking.Add(finalBlocking);

            finalBlocking.ApplyToFixtures();

            Assert.That(finalBlocking.Dividers, Has.Some.Matches<PlanogramBlockingDivider>(d => d.X.EqualTo(0.5f) && d.IsVertical()));
            Assert.That(finalBlocking.Dividers, Has.Some.Matches<PlanogramBlockingDivider>(d => d.Y.EqualTo(0.5f) && d.IsHorizontal()));
            Assert.That(finalBlocking.Dividers, Has.Some.Matches<PlanogramBlockingDivider>(d => d.Y.EqualTo(0.1f) && d.IsHorizontal()));
            Assert.That(finalBlocking.Dividers, Has.Some.Matches<PlanogramBlockingDivider>(d => d.X.EqualTo(0.2f) && d.IsVertical()));
            Assert.That(finalBlocking.Dividers, Has.Some.Matches<PlanogramBlockingDivider>(d => d.Y.EqualTo(0.75f) && d.IsHorizontal()));
        }

        #endregion

        #region MoveDivider
        [Test]
        public void MoveDivider_MovesAssociatedHorizontalDivider_WhenSurroundedByDividersAndMovePositive()
        {
            //           _______________
            //          |  (1)     (2)  |
            //          |   |__(3)__|   |
            //          |   |       |   |
            //          |   |__(4)__|   |
            //          |   |       |   |
            //          |   |       |   |
            //          |   |       |   |
            //          |___|_______|___|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0.2f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0.8f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var div3 = TestDataHelper.CreateBlockingDivider(0.2f, 0.75f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var div4 = TestDataHelper.CreateBlockingDivider(0.2f, 0.5f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div4, 0.1f, context);

            Assert.AreEqual(Math.Round(0.800f, 3), Math.Round(div3.Y, 3));
        }

        [Test]
        public void MoveDivider_MovesAssociatedHorizontalDivider_WhenSurroundedByDividersAndMoveNegative()
        {
            //           _______________
            //          |  (1)     (2)  |
            //          |   |__(3)__|   |
            //          |   |       |   |
            //          |   |__(4)__|   |
            //          |   |       |   |
            //          |   |       |   |
            //          |   |       |   |
            //          |___|_______|___|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0.2f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0.8f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var div3 = TestDataHelper.CreateBlockingDivider(0.2f, 0.75f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var div4 = TestDataHelper.CreateBlockingDivider(0.2f, 0.5f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div4, -0.1f, context);

            Assert.AreEqual(Math.Round(0.700f, 3), Math.Round(div3.Y, 3));
        }

        [Test]
        public void MoveDivider_MovesAssociatedHorizontalDivider_WhenSurroundedByNullsAndMovePositive()
        {
            //           _______________
            //          |               |
            //          |______(2)______|
            //          |               |
            //          |______(1)______|
            //          |               |
            //          |               |
            //          |               |
            //          |_______________|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0f, 0.75f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var snappedList = new List<PlanogramBlockingDivider>();
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div1, 0.1f, context);

            Assert.AreEqual(Math.Round(0.800f, 3), Math.Round(div2.Y, 3));
        }

        [Test]
        public void MoveDivider_MovesAssociatedHorizontalDivider_WhenSurroundedByNullsAndMoveNegative()
        {
            //           _______________
            //          |               |
            //          |______(2)______|
            //          |               |
            //          |______(1)______|
            //          |               |
            //          |               |
            //          |               |
            //          |_______________|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0f, 0.75f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div1, -0.1f, context);

            Assert.AreEqual(Math.Round(0.700f, 3), Math.Round(div2.Y, 3));
        }

        [Test]
        public void MoveDivider_MovesAssociatedVerticalDivider_WhenSurroundedByDividersAndMovePositive()
        {
            //           _______________________
            //          |                       |
            //          |___(1)_________________|
            //          |           |      |    |
            //          |          (3)    (4)   |
            //          |           |      |    |
            //          |___(2)_____|______|____|
            //          |                       |
            //          |_______________________|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div3 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div4 = TestDataHelper.CreateBlockingDivider(0.75f, 0f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div3, 0.1f, context);

            Assert.AreEqual(Math.Round(0.800f, 3), Math.Round(div4.X, 3));
        }

        [Test]
        public void MoveDivider_MovesAssociatedVerticalDivider_WhenSurroundedByDividersAndMoveNegative()
        {
            //           _______________________
            //          |                       |
            //          |___(1)_________________|
            //          |           |      |    |
            //          |          (3)    (4)   |
            //          |           |      |    |
            //          |___(2)_____|______|____|
            //          |                       |
            //          |_______________________|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div3 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div4 = TestDataHelper.CreateBlockingDivider(0.75f, 0f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div3, -0.1f, context);

            Assert.AreEqual(Math.Round(0.700f, 3), Math.Round(div4.X, 3));
        }

        [Test]
        public void MoveDivider_MovesAssociatedVerticalDivider_WhenSurroundedByNullsAndMovePositive()
        {
            //           ___________________
            //          |       |       |   |
            //          |       |       |   |
            //          |      (1)     (2)  |
            //          |       |       |   |
            //          |       |       |   |
            //          |_______|_______|___|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0.4f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0.7f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div1, 0.1f, context);

            Assert.AreEqual(Math.Round(0.750f, 3), Math.Round(div2.X, 3));
        }

        [Test]
        public void MoveDivider_MovesAssociatedVerticalDivider_WhenSurroundedByNullsAndMoveNegative()
        {
            //           ___________________
            //          |       |       |   |
            //          |       |       |   |
            //          |      (1)     (2)  |
            //          |       |       |   |
            //          |       |       |   |
            //          |_______|_______|___|
            //

            var blocking = "".CreatePackage().AddPlanogram().AddBlocking();
            var div1 = TestDataHelper.CreateBlockingDivider(0.4f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0.7f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var context = new PlanogramBlocking.ApplyBlockingContext(blocking);

            PlanogramBlocking.MoveDivider(blocking, div1, -0.1f, context);

            Assert.AreEqual(Math.Round(0.650f, 3), Math.Round(div2.X, 3));
        }
        #endregion

        #region GetClosestValue
        [Test]
        public void GetClosestValue_WhenLessThanValue()
        {
            Single value = 0.5f;
            Single expected = 0.4f;
            var possibleValues = new List<Single>() { 0f, 0.2f, expected, 0.7f, 0.9f };

            var result = PlanogramBlocking.GetClosestValue(value, possibleValues);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetClosestValue_WhenGreaterThanValue()
        {
            Single value = 0.5f;
            Single expected = 0.6f;
            var possibleValues = new List<Single>() { 0f, 0.2f, expected, 0.7f, 0.9f };

            var result = PlanogramBlocking.GetClosestValue(value, possibleValues);

            Assert.AreEqual(expected, result);
        }
        #endregion

        #region CombineGroups
        //private const PlanogramBlockingProductAssignType _static = PlanogramBlockingProductAssignType.Static;
        //private const PlanogramBlockingProductAssignType _dynamic = PlanogramBlockingProductAssignType.Dynamic;

        //[Test]
        //public void CombineGroups_CombinesStaticProducts()
        //{
        //    var planogram = Planogram.NewPlanogram();
        //    planogram.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));
        //    var groupsToAdd = new PlanogramBlockingGroup[] { TestDataHelper.CreateBlockingGroup(_static, 5), TestDataHelper.CreateBlockingGroup(_static, 5) };
        //    var groups = planogram.Blocking.First().Groups;
        //    groups.AddRange(groupsToAdd);
        //    var expected = groupsToAdd.SelectMany(g => g.Products).Select(p => p.Gtin).ToList();

        //    var result = GetBlockingHelper.CombineGroups(groupsToAdd, planogram.Blocking.First(), 0);
        //    var actual = result.Products.Select(p => p.Gtin).ToList();

        //    CollectionAssert.AreEquivalent(expected, actual);
        //}

        //[Test]
        //public void CombineGroups_CombinesDynamicProducts()
        //{
        //    var planogram = Planogram.NewPlanogram();
        //    planogram.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));
        //    var groupsToAdd = new PlanogramBlockingGroup[] { TestDataHelper.CreateBlockingGroup(_dynamic, 5), TestDataHelper.CreateBlockingGroup(_dynamic, 5) };
        //    var groups = planogram.Blocking.First().Groups;
        //    groups.AddRange(groupsToAdd);
        //    var expected = groupsToAdd.SelectMany(g => g.Fields).Select(f => f.TextValue).ToList();

        //    var result = GetBlockingHelper.CombineGroups(groupsToAdd, planogram.Blocking.First(), 0);
        //    var actual = result.Fields.Select(f => f.TextValue).ToList();

        //    CollectionAssert.AreEquivalent(expected, actual);
        //}

        //[Test]
        //public void CombineGroups_CombinesDynamicAndStaticProducts()
        //{
        //    var planogram = Planogram.NewPlanogram();
        //    planogram.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));
        //    var groupsToAdd = new PlanogramBlockingGroup[] { TestDataHelper.CreateBlockingGroup(_static, 5), TestDataHelper.CreateBlockingGroup(_dynamic, 5) };
        //    var groups = planogram.Blocking.First().Groups;
        //    groups.AddRange(groupsToAdd);

        //    var expected = groupsToAdd[0].Products.Select(p => p.Gtin).
        //        Union(groupsToAdd[1].Fields.Select(f => f.TextValue));

        //    var result = GetBlockingHelper.CombineGroups(groupsToAdd, planogram.Blocking.First(), 0);
        //    var actual = result.Fields.Select(f => f.TextValue).ToList();

        //    CollectionAssert.AreEquivalent(expected, actual);
        //}

        //[Test]
        //public void CombineGroups_CombinesNamesIntoOne()
        //{
        //    var planogram = Planogram.NewPlanogram();
        //    planogram.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));
        //    var groupsToAdd = new PlanogramBlockingGroup[] 
        //    { 
        //        TestDataHelper.CreateBlockingGroup(_static, 5, "Name1"), 
        //        TestDataHelper.CreateBlockingGroup(_static, 5, "Name2"), 
        //        TestDataHelper.CreateBlockingGroup(_static, 5, "Name3"), 
        //        TestDataHelper.CreateBlockingGroup(_static, 5, "Name4")
        //    };
        //    var groups = planogram.Blocking.First().Groups;
        //    groups.AddRange(groupsToAdd);
        //    var expected = String.Format("{0} including {1}, {2} and {3}", groups[0].Name, groups[1].Name, groups[2].Name, groups[3].Name);

        //    var result = GetBlockingHelper.CombineGroups(groupsToAdd, planogram.Blocking.First(), 0);
        //    var actual = result.Name;

        //    Assert.AreEqual(expected, actual);
        //}

        //[Test]
        //public void CombineGroups_Throws_WhenSomeGroupsCannotMerge()
        //{
        //    var planogram = Planogram.NewPlanogram();
        //    planogram.Blocking.Add(PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial));
        //    var groupsToAdd = new PlanogramBlockingGroup[] 
        //    { 
        //        TestDataHelper.CreateBlockingGroup(_static, 5,String.Empty,true,false),
        //        TestDataHelper.CreateBlockingGroup(_static, 5,String.Empty,true,false),
        //        TestDataHelper.CreateBlockingGroup(_static, 5,String.Empty,false,false)
        //    };
        //    var groups = planogram.Blocking.First().Groups;
        //    groups.AddRange(groupsToAdd);

        //    TestDelegate test = () => GetBlockingHelper.CombineGroups(groupsToAdd, planogram.Blocking.First(), 0);

        //    Assert.Throws<GetBlockingException>(test);
        //}

        //[Test]
        //public void CombineGroups_Throws_WhenSomeGroupsAreRestrictedByComponentType_AndMultipleTypesExist()
        //{
        //    //       ___________
        //    //      |           |
        //    //      |-----------|   Bar
        //    //    ..|...........|.. Divider 1
        //    //      |           |
        //    //      |===========|   Shelf
        //    //      |___________|
        //    //

        //    // Create Planogram
        //    var planogram = Planogram.NewPlanogram();
        //    var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
        //    planogram.Blocking.Add(blocking);
        //    var fixture = PlanogramFixture.NewPlanogramFixture();
        //    fixture.Width = 10f;
        //    fixture.Height = 10f;
        //    planogram.Fixtures.Add(fixture);
        //    TestDataHelper.CreateFixtureComponent(0f, 0.33f, 1f, 0.1f, PlanogramComponentType.Shelf, fixture.Components, planogram.Components);
        //    TestDataHelper.CreateFixtureComponent(0f, 0.66f, 1f, 0.1f, PlanogramComponentType.Bar, fixture.Components, planogram.Components);
        //    planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture));

        //    // Create dividers, locations and groups
        //    var divider1 = TestDataHelper.CreateBlockingDivider(
        //        0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, PlanogramBlockingDivider.RootLevelNumber, blocking.Dividers);
        //    blocking.Groups.AddRange(new PlanogramBlockingGroup[]
        //    { 
        //        TestDataHelper.CreateBlockingGroup(_static, 5,String.Empty,true,false),
        //        TestDataHelper.CreateBlockingGroup(_static, 5,String.Empty,true,true)
        //    });
        //    TestDataHelper.CreateBlockingLocation(null, null, null, divider1, blocking.Locations);
        //    TestDataHelper.CreateBlockingLocation(null, null, divider1, null, blocking.Locations);
        //    blocking.Locations[0].PlanogramBlockingGroupId = blocking.Groups[0].Id;
        //    blocking.Locations[1].PlanogramBlockingGroupId = blocking.Groups[1].Id;

        //    TestDelegate test = () => GetBlockingHelper.CombineGroups(blocking.Groups, blocking, 0);

        //    Assert.Throws<GetBlockingException>(test);
        //}
        #endregion

        #region GetLocationsAffectedByDividerRemoval

        [Test]
        public void GetLocationsAffectedByDividerRemoval_ThrowsWhenDividerIsNull()
        {
            var list = PlanogramBlockingLocationList.NewPlanogramBlockingLocationList();
            TestDelegate test = () => PlanogramBlocking.GetLocationsAffectedByDividerRemoval(null);
            Assert.Throws<ArgumentNullException>(test);
        }

        [Test]
        public void GetLocationsAffectedByDividerRemoval_WhenNoBorderingLocations()
        {
            //   _______________
            //  |      (2)   D  |
            //  |   B   |____(3)|
            //  |       |   C   |
            //  |_(1)___|_______| <-- divider to remove
            //  |               |
            //  |       A       |
            //  |_______________|
            //

            // Create Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            //  Create dividers
            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0.5f, 0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);

            // Create Locations
            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider1, null, blocking.Locations);
            var locationB = TestDataHelper.CreateBlockingLocation(null, divider2, null, divider1, blocking.Locations);
            var locationC = TestDataHelper.CreateBlockingLocation(divider2, null, divider3, divider1, blocking.Locations);
            var locationD = TestDataHelper.CreateBlockingLocation(divider2, null, null, divider3, blocking.Locations);

            var actual = PlanogramBlocking.GetLocationsAffectedByDividerRemoval(blocking.Dividers.First(d => d.Level == 1));

            CollectionAssert.AreEquivalent(blocking.Locations, actual);
        }

        [Test]
        public void GetLocationsAffectedByDividerRemoval_WhenBorderedByLocations()
        {
            // 1 ^   _____________________
            //   |  |\\|_\_(6)_\_\_\_\_|\\|      Locations A, B and C are directly affected.
            //0.8|  |\\|      (2)   D  |\\|      Location D should also be picked up.
            //   |  |\\|   B   |____(3)|\\|      The locations that are shaded are not affected and should not be returned.
            //   |  |\(4)      |   C  (5)\|
            //0.5|  |\\|_(1)___|_______|\\|      Divider (1) is to be removed.
            //   |  |\\|               |\\|
            //   |  |\\|       A       |\\|
            //0.2|  |\\|____(7)________|\\|
            //   |  |\_|_\_\_\_\_\_\_\_|_\|
            // 0 +
            //      +---------------------->
            //     0  0.2     0.5     0.8  1

            // Create Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            //  Create dividers
            var divider1 = TestDataHelper.CreateBlockingDivider(0.2f, 0.5f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.3f, PlanogramBlockingDividerType.Vertical, 3, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0.5f, 0.7f, 0.3f, PlanogramBlockingDividerType.Horizontal, 4, blocking.Dividers);
            var divider4 = TestDataHelper.CreateBlockingDivider(0.2f, 0, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var divider5 = TestDataHelper.CreateBlockingDivider(0.8f, 0, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var divider6 = TestDataHelper.CreateBlockingDivider(0.2f, 0.8f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider7 = TestDataHelper.CreateBlockingDivider(0.2f, 0.2f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);

            // Create Locations
            var locationA = TestDataHelper.CreateBlockingLocation(divider4, divider5, divider1, divider7, blocking.Locations);
            var locationB = TestDataHelper.CreateBlockingLocation(divider4, divider2, divider6, divider1, blocking.Locations);
            var locationC = TestDataHelper.CreateBlockingLocation(divider2, divider5, divider3, divider1, blocking.Locations);
            var locationD = TestDataHelper.CreateBlockingLocation(divider2, divider5, divider6, divider3, blocking.Locations);
            var expected = new PlanogramBlockingLocation[] { locationA, locationB, locationC, locationD };

            var actual = PlanogramBlocking.GetLocationsAffectedByDividerRemoval(divider1);

            CollectionAssert.AreEquivalent(expected, actual);
        }

        #endregion

        #region GetCommaSeparatedGroupNames
        [Test]
        public void GetCommaSeparatedGroupNames_FormatsListCorrectly()
        {
            var group1 = PlanogramBlockingGroup.NewPlanogramBlockingGroup("Group1", 0);
            var group2 = PlanogramBlockingGroup.NewPlanogramBlockingGroup("Group2", 0);
            var group3 = PlanogramBlockingGroup.NewPlanogramBlockingGroup("Group3", 0);
            String expected = "Group1, Group2, Group3";

            String actual = PlanogramBlocking.GetCommaSeparatedGroupNames(new[] { group1, group2, group3 });

            Assert.AreEqual(expected, actual);
        }
        #endregion

        #region Helper methods

        private enum DividerPosition { X, Y }

        private void AssertDividerPositionsInSequence(DividerPosition position, IEnumerable<Single> positions, IEnumerable<PlanogramBlockingDivider> dividers)
        {
            if (positions.Count() > dividers.Count()) Assert.Fail("more positions than dividers were supplied");

            for (Int32 i = 0; i < positions.Count(); i++)
            {
                Single dividerPosition;
                if (position == DividerPosition.X)
                {
                    dividerPosition = dividers.ElementAt(i).X;
                }
                else
                {
                    dividerPosition = dividers.ElementAt(i).Y;
                }
                Assert.That(
                    positions.ElementAt(i).EqualTo(dividerPosition, 2),
                    String.Format("Divider {0} was expected at {1}, but was at {2}", i, positions.ElementAt(i), dividerPosition));
                Debug.WriteLine("Divider {0} correctly appeared at {1}", i, positions.ElementAt(i));
            }
        }

        private void AssertDividerPositionsInAnyOrder(DividerPosition position, IEnumerable<Single> positions, IEnumerable<PlanogramBlockingDivider> dividers)
        {
            if (positions.Count() != dividers.Count()) Assert.Fail("the count of positions and dividers must be the same");

            var dividerPositions = dividers.Select(d => position == DividerPosition.X ? d.X : d.Y).ToList();
            foreach (var pos in positions)
            {
                Boolean match = false;
                foreach (var dividerPosition in dividerPositions)
                {
                    if (dividerPosition.EqualTo(pos))
                    {
                        match = true;
                        break;
                    }
                }
                if (!match)
                {
                    Assert.Fail("Expected a collection containing {0}, but was {1}", pos, GetStringRepresentationOfCollection(dividerPositions));
                }
            }
            Assert.Pass();
        }

        private String GetStringRepresentationOfCollection<T>(IEnumerable<T> collection)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("{ ");

            foreach (T item in collection)
            {
                stringBuilder.AppendFormat("{0}, ", item);
            }

            stringBuilder.Append("}");
            return stringBuilder.ToString();
        }

        #region Planogram Setup
        private PlanogramComponent _shelf;
        private PlanogramComponent _pegBoard;
        private PlanogramComponent _chest;

        private PlanogramFixtureComponent NewFixtureComponent(PlanogramComponent component, Single x, Single y, Single z, Single slope = 0f, Single angle = 0f, Single roll = 0f)
        {
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.X = x;
            fixtureComponent.Y = y;
            fixtureComponent.Z = z;
            fixtureComponent.Slope = slope;
            fixtureComponent.Angle = angle;
            fixtureComponent.Roll = roll;
            return fixtureComponent;
        }

        private PlanogramAssemblyComponent NewAssemblyComponent(PlanogramComponent component, Single x, Single y, Single z, Single slope = 0f, Single angle = 0f, Single roll = 0f)
        {
            var assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(component);
            assemblyComponent.X = x;
            assemblyComponent.Y = y;
            assemblyComponent.Z = z;
            assemblyComponent.Slope = slope;
            assemblyComponent.Angle = angle;
            assemblyComponent.Roll = roll;
            return assemblyComponent;
        }

        private PlanogramFixtureItem GetBay(Planogram planogram, Int16 baySequenceNumber)
        {
            return planogram.FixtureItems.FirstOrDefault(fi => fi.BaySequenceNumber == baySequenceNumber);
        }

        private Planogram CreatePlanogram(Byte numberOfBays)
        {
            _shelf = PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, 120f, 4f, 75f);
            _pegBoard = PlanogramComponent.NewPlanogramComponent("PegBoard", PlanogramComponentType.Peg, 120f, 120f, 1f);
            _chest = PlanogramComponent.NewPlanogramComponent("Chest", PlanogramComponentType.Chest, 120f, 50f, 75f);

            var planogram = "Package".CreatePackage().AddPlanogram();

            for (Byte i = 1; i <= numberOfBays; i++)
            {
                var fixture = PlanogramFixture.NewPlanogramFixture();
                fixture.Width = 120;
                fixture.Height = 200;
                fixture.Depth = 76;
                planogram.Fixtures.Add(fixture);

                var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
                fixtureItem.BaySequenceNumber = i;
                fixtureItem.X = (i - 1) * 120;
                planogram.FixtureItems.Add(fixtureItem);
            }

            planogram.Components.AddRange(new[] { _shelf, _chest, _pegBoard });

            return planogram;
        }
        #endregion

        #region Planogram Cases
        private Planogram CreateShelvesWithMatchingBlockingPlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 11, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 51, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 90, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 128, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 165, 0));

            return planogram;
        }

        private Planogram CreateShelvesInAssemblyWithMatchingBlockingPlanogram()
        {
            var planogram = CreatePlanogram(1);

            // Create assembly
            var assembly = PlanogramAssembly.NewPlanogramAssembly();
            planogram.Assemblies.Add(assembly);
            assembly.Components.Add(NewAssemblyComponent(_shelf, 0, 11, 0));
            assembly.Components.Add(NewAssemblyComponent(_shelf, 0, 51, 0));
            assembly.Components.Add(NewAssemblyComponent(_shelf, 0, 90, 0));
            assembly.Components.Add(NewAssemblyComponent(_shelf, 0, 128, 0));
            assembly.Components.Add(NewAssemblyComponent(_shelf, 0, 165, 0));

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            var fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
            fixtureAssembly.PlanogramAssemblyId = assembly.Id;
            bay1.Assemblies.Add(fixtureAssembly);

            return planogram;
        }

        private Planogram CreateMultiBayShelvesWithSingleBayBlockingPlanogram()
        {
            var planogram = CreatePlanogram(3);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 11, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 51, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 90, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 128, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 165, 0));

            var bay2 = GetBay(planogram, 2).GetPlanogramFixture();
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 18, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 56, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 93, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 134, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 172, 0));

            var bay3 = GetBay(planogram, 3).GetPlanogramFixture();
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 9f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 47f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 79f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 121f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 161f, 0));

            return planogram;
        }

        private Planogram CreateThreeBayShelvesWithThreeBayBlockingPlanogram()
        {
            return CreateMultiBayShelvesWithSingleBayBlockingPlanogram();
        }

        private Planogram CreateTwoBayShelvesWithThreeBayBlockingPlanogram()
        {
            var planogram = CreatePlanogram(2);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 11, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 51, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 90, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 128, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 165, 0));

            var bay2 = GetBay(planogram, 2).GetPlanogramFixture();
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 18, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 56, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 93, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 134, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 172, 0));

            return planogram;
        }

        private Planogram CreateShelvesThatDontMatchBlockingInsufficientComponentSpacePlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 14, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 86, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 153, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 184, 0));

            return planogram;
        }

        private Planogram CreatePegBoardWithDividersOneBayBlockingPlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_pegBoard, 0, 96, 0));
            var pegBoard = bay1.Components.First().GetPlanogramComponent().SubComponents.First();
            pegBoard.DividerObstructionStartX = 10;
            pegBoard.DividerObstructionSpacingX = 40;
            pegBoard.DividerObstructionStartY = 50;
            pegBoard.DividerObstructionSpacingY = 70;
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 43, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 12, 0));

            return planogram;
        }

        private Planogram CreatePegBoardNoDividersOneBayBlockingPlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_pegBoard, 0, 96, 0));
            var pegBoard = bay1.Components.First().GetPlanogramComponent().SubComponents.First();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 43, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 12, 0));

            return planogram;
        }

        private Planogram CreateChestWithHorizontalDividersPlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0f, 163f, 0f));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0f, 111f, 0f));
            bay1.Components.Add(NewFixtureComponent(_chest, 0f, 11f, 0f));
            var subComponent = _chest.SubComponents.First(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack);
            subComponent.DividerObstructionStartZ = 22;
            subComponent.DividerObstructionSpacingZ = 22;

            return planogram;
        }

        private Planogram CreateSlopedChestWithHorizontalDividersPlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0f, 163f, 0f));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0f, 128.54f, 0f));
            bay1.Components.Add(NewFixtureComponent(_chest, 0f, 62.45003f, 0f, Convert.ToSingle(315 * ((Math.PI * 2) / 360f)), 0f, 0f));

            var subComponent = _chest.SubComponents.First(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack);
            subComponent.DividerObstructionStartZ = 22;
            subComponent.DividerObstructionSpacingZ = 22;
            subComponent.DividerObstructionDepth = subComponent.DividerObstructionHeight = subComponent.DividerObstructionWidth = 1;

            return planogram;
        }

        private Planogram CreateChestWithVerticalDividersPlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0f, 163f, 0f));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0f, 111f, 0f));
            bay1.Components.Add(NewFixtureComponent(_chest, 0f, 11f, 0f));
            var subComponent = _chest.SubComponents.First(s => s.MerchandisingType == PlanogramSubComponentMerchandisingType.Stack);
            subComponent.DividerObstructionStartX = 37;
            subComponent.DividerObstructionSpacingX = 37;
            subComponent.DividerObstructionWidth = 1;
            subComponent.DividerObstructionHeight = 1;
            subComponent.DividerObstructionDepth = 1;

            return planogram;
        }

        public Planogram CreateMultiBayPegBoardOneBayBlockingPlanogram()
        {
            var planogram = CreatePlanogram(3);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_pegBoard, 0, 96, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 43, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 12, 0));

            var bay2 = GetBay(planogram, 2).GetPlanogramFixture();
            bay2.Components.Add(NewFixtureComponent(_pegBoard, 0, 96, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 43, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 12, 0));

            var bay3 = GetBay(planogram, 3).GetPlanogramFixture();
            bay3.Components.Add(NewFixtureComponent(_pegBoard, 0, 96, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 43, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 12, 0));

            return planogram;
        }

        private Planogram CreateShelvesThatDontMatchBlockingSufficientComponentSpacePlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 14, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 86, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 124, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 153, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 184, 0));

            return planogram;
        }

        private Planogram CreateOneBayShelvesWithThreeBayBlockingPlanogram()
        {
            var planogram = CreatePlanogram(1);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 11, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 51, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 90, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 128, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 165, 0));

            return planogram;
        }

        private Planogram CreateFourBayShelvesWithThreeBayBlockingPlanogram()
        {
            var planogram = CreatePlanogram(4);

            var bay1 = GetBay(planogram, 1).GetPlanogramFixture();
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 11, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 51, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 90, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 128, 0));
            bay1.Components.Add(NewFixtureComponent(_shelf, 0, 165, 0));

            var bay2 = GetBay(planogram, 2).GetPlanogramFixture();
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 18, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 56, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 93, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 134, 0));
            bay2.Components.Add(NewFixtureComponent(_shelf, 0, 172, 0));

            var bay3 = GetBay(planogram, 3).GetPlanogramFixture();
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 9f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 47f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 79f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 121f, 0));
            bay3.Components.Add(NewFixtureComponent(_shelf, 0, 161f, 0));

            var bay4 = GetBay(planogram, 4).GetPlanogramFixture();
            bay4.Components.Add(NewFixtureComponent(_shelf, 0, 22f, 0));
            bay4.Components.Add(NewFixtureComponent(_shelf, 0, 94f, 0));
            bay4.Components.Add(NewFixtureComponent(_shelf, 0, 149f, 0));

            return planogram;
        }
        #endregion

        #region Blocking Cases

        /// <summary>
        /// Creates a simple blocking for pegboards.
        /// </summary>
        /// <param name="planogram"></param>
        /// <remarks>
        ///       2      3
        ///  ___________________
        /// |     |      |      |
        /// |     |      |___6__|
        /// |     |___5__|      |
        /// |__4__|      |      |
        /// |     |      |      |
        /// |_____|______|______|   0
        /// |                   |
        /// |                   |
        /// |___________________|   1
        /// |                   |
        /// |___________________|
        /// 
        /// </remarks>
        private PlanogramBlocking CreateOneBayPegboardBlocking(Planogram planogram)
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            blocking.Dividers.Add(0f, 0.443f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.199f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Add(0.28f, 0.443f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.682f, 0.443f, PlanogramBlockingDividerType.Vertical);

            blocking.Dividers.Add(0f, 0.65f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.28f, 0.692f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.682f, 0.751f, PlanogramBlockingDividerType.Horizontal);

            foreach (var d in blocking.Dividers) d.IsSnapped = true;

            return blocking;
        }

        /// <summary>
        /// Creates blocking for a chest with horizontal dividers.
        /// </summary>
        /// <param name="planogram"></param>
        /// <remarks>
        ///  ___________
        /// |           |
        /// |___________|   0
        /// |           |
        /// |___________|   1
        /// |           |
        /// |           |
        /// |___________|   2
        /// |___________|   3
        /// |___________|
        /// 
        /// </remarks>
        private PlanogramBlocking CreateChestWithHorizontalDividersBlocking(Planogram planogram)
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            blocking.Dividers.Add(0f, 0.796f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.466f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.267f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.094f, PlanogramBlockingDividerType.Horizontal);

            foreach (var group in blocking.Groups)
            {
                group.IsRestrictedByComponentType = false;
            }

            foreach (var d in blocking.Dividers) d.IsSnapped = true;

            return blocking;
        }

        /// <summary>
        /// Creates blocking for a chest with vertical dividers.
        /// </summary>
        /// <param name="planogram"></param>
        /// <remarks>
        ///  ___________
        /// |           |
        /// |___________|   0
        /// |           |
        /// |___________|   1
        /// | |   |     |
        /// | |   |     |
        /// |_|___|_____|
        ///   2   3
        /// </remarks>
        private PlanogramBlocking CreateChestWithVertcalDividersBlocking(Planogram planogram)
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            blocking.Dividers.Add(0f, 0.796f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.544f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Add(0.151f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.488f, 0f, PlanogramBlockingDividerType.Vertical);

            foreach (var group in blocking.Groups)
            {
                group.IsRestrictedByComponentType = false;
            }

            foreach (var d in blocking.Dividers) d.IsSnapped = true;

            return blocking;
        }

        /// <summary>
        /// Creates blocking for a peg board and shelves with too many dividers.
        /// </summary>
        /// <param name="planogram"></param>
        /// <remarks>
        ///  ___________
        /// |           |
        /// |           |
        /// |___________|   0
        /// |___________|   1
        /// |___________|   2
        /// |___________|   
        /// 
        /// </remarks>
        private PlanogramBlocking CreatePegBoardBlockingWithTooManyDividers(Planogram planogram)
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            blocking.Dividers.Add(0f, 0.454f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.315f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.166f, PlanogramBlockingDividerType.Horizontal);

            foreach (var group in blocking.Groups)
            {
                group.IsRestrictedByComponentType = false;
            }

            foreach (var d in blocking.Dividers) d.IsSnapped = true;

            return blocking;
        }

        /// <summary>
        /// Blocking for a single bay with 4 horizontal dividers.
        /// </summary>
        /// <remarks>
        ///  ___________
        /// |           |
        /// |___________|  0
        /// |           |
        /// |___________|  1
        /// |           |
        /// |___________|  2
        /// |           |
        /// |___________|  3
        /// |           |
        /// |___________|
        /// 
        /// </remarks>
        private PlanogramBlocking CreateSingleBayShelfBlocking(Planogram planogram)
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            blocking.Dividers.Add(0f, 0.233f + 0.185f + 0.187f + 0.181f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.233f + 0.185f + 0.187f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.233f + 0.185f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.233f, PlanogramBlockingDividerType.Horizontal);
            foreach (var d in blocking.Dividers) d.IsSnapped = true;
            return blocking;
        }

        /// <summary>
        /// Blocking for three bays, each with 4 horizontal dividers.
        /// </summary>
        /// <remarks>
        ///             0           1
        ///  ___________________________________
        /// |           |_____6_____|           |
        /// |_____2_____|           |           |
        /// |           |           |____10_____|
        /// |           |_____7_____|           |
        /// |_____3_____|           |____11_____|
        /// |           |_____8_____|           |
        /// |_____4_____|           |           |
        /// |           |           |____12_____|
        /// |           |_____9_____|           |
        /// |_____5_____|           |           |
        /// |           |           |____13_____|
        /// |___________|___________|___________|
        /// 
        /// </remarks>
        private PlanogramBlocking CreateThreeBayShelfBlocking(Planogram planogram, Single? vertDivX1 = null, Single? vertDivX2 = null)
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            blocking.Dividers.Add(vertDivX1.HasValue ? vertDivX1.Value : 0.25f, 0f, PlanogramBlockingDividerType.Vertical).IsSnapped = true;
            blocking.Dividers.Add(vertDivX2.HasValue ? vertDivX2.Value : 0.6f, 0f, PlanogramBlockingDividerType.Vertical).IsSnapped = true;

            blocking.Dividers.Add(0.2f, 0.808f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.2f, 0.590f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.2f, 0.401f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.2f, 0.214f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Add(0.4f, 0.909f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.686f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.499f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.294f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Add(0.8f, 0.784f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.8f, 0.579f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.8f, 0.372f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.8f, 0.192f, PlanogramBlockingDividerType.Horizontal);

            foreach (var d in blocking.Dividers) d.IsSnapped = true;
            return blocking;
        }

        /// <summary>
        /// Blocking for one bay with a large block in the middle.
        /// </summary>
        /// <param name="planogram"></param>
        /// <remarks>
        ///  _______________
        /// |_______________|   0
        /// |               |
        /// |               |
        /// |               |
        /// |_______________|   1
        /// |               |
        /// |_______________|   2
        /// |_______________|   3
        /// |               |
        /// |_______________|
        /// 
        /// </remarks>
        private PlanogramBlocking CreateOneBayShelvesBigBlockInTheMiddleBlocking(Planogram planogram)
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            blocking.Dividers.Add(0f, 0.895f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.41f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.278f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0f, 0.196f, PlanogramBlockingDividerType.Horizontal);

            return blocking;
        }

        #endregion

        #endregion

        [Test]
        public void EnumerateGroupsInOptimisationOrder_EnumeratesBlocksInRankOrder()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            plan.AddProduct();
            plan.AddProduct();
            plan.AddProduct();
            plan.CreateAssortmentFromProducts();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.33f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.66f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var middleBlock = blocking.Locations.First(l => l.Y.EqualTo(0.33f)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.66f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Skip(2).Take(1));
            plan.AddSequenceGroup(middleBlock, plan.Products.Skip(1).Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Take(1));

            var blocksInOrder = blocking.EnumerateGroupsInOptimisationOrder();
                
            blocksInOrder.Should().Equal(
                new[] { topBlock, middleBlock, bottomBlock },
                "because the blocks should be ordered by product rank");
        }

        [Test]
        public void EnumerateGroupsInOptimisationOrder_EnumeratesBlocksNotInAssortment()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            plan.AddProduct();
            plan.AddProduct();
            plan.AddProduct();
            plan.CreateAssortmentFromProducts(plan.Products.Take(1).Union(plan.Products.Skip(2)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.33f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.66f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var middleBlock = blocking.Locations.First(l => l.Y.EqualTo(0.33f)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.66f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Skip(2).Take(1));
            plan.AddSequenceGroup(middleBlock, plan.Products.Skip(1).Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Take(1));

            var blocksInOrder = blocking.EnumerateGroupsInOptimisationOrder();

            blocksInOrder.Should().Equal(
                new[] { topBlock, bottomBlock, middleBlock },
                "because the blocks not in the assortment should still appear, but at the end");
        }

        [Test]
        public void EnumerateGroupsInOptimisationOrder_EnumeratesBlocksNotInSequence()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            plan.AddProduct();
            plan.AddProduct();
            plan.AddProduct();
            plan.CreateAssortmentFromProducts();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.33f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.66f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var middleBlock = blocking.Locations.First(l => l.Y.EqualTo(0.33f)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.66f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Skip(2).Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Take(1));

            var blocksInOrder = blocking.EnumerateGroupsInOptimisationOrder();

            blocksInOrder.Should().Equal(
                new[] { topBlock, bottomBlock, middleBlock },
                "because the blocks not in the sequence should still appear, but at the end");
        }

        private Byte[] _oneToTwenty
        {
            get
            {
                return new Byte[]
                {
                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
                };
            }
        }

        #region GetUnNormalisedPsi
        [Test]
        public void GetUnNormalisedPsi_Throws_WhenNullProduct()
        {
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            var performanceDataTotals = new PlanogramPerformanceDataTotals(new List<PlanogramPerformanceData>());

            TestDelegate test = () => PlanogramBlocking.GetUnNormalisedPsi(metricProfile, null, performanceDataTotals);

            Assert.Throws<ArgumentNullException>(test);
        }

        [Test]
        public void GetUnNormalisedPsi_Throws_WhenNullMetricProfile()
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            var performanceDataTotals = new PlanogramPerformanceDataTotals(new List<PlanogramPerformanceData>());

            TestDelegate test = () => PlanogramBlocking.GetUnNormalisedPsi(null, product, performanceDataTotals);

            Assert.Throws<ArgumentNullException>(test);
        }

        [Test]
        public void GetUnNormalisedPsi_Throws_WhenNullPerformanceDataTotals()
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;

            TestDelegate test = () => PlanogramBlocking.GetUnNormalisedPsi(metricProfile, product, null);

            Assert.Throws<ArgumentNullException>(test);
        }

        [Test]
        public void GetUnNormalisedPsi_ReturnsNull_WhenNoMatchingPerformanceData()
        {
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            planogram.Products.Add(product);
            planogram.Performance.PerformanceData.Clear();
            var metricProfile = new Mock<IPlanogramMetricProfile>().Object;
            var performanceDataTotals = new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData);

            var actual = PlanogramBlocking.GetUnNormalisedPsi(metricProfile, product, performanceDataTotals);

            Assert.IsNull(actual);
        }

        [Test]
        [TestCaseSource("_oneToTwenty")]
        public void GetUnNormalisedPsi_MetricCalculated_WhenMaximumIncrease(Byte metricNumber)
        {
            var random = new Random();
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            planogram.Products.Add(product);

            var performanceData = planogram.Performance.PerformanceData.First();
            typeof(PlanogramPerformanceData).GetProperty(String.Format("P{0}", metricNumber)).SetValue(performanceData, (Single?)random.Next(1, 10), null);

            var metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(metricNumber);
            metric.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric);

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).SetValue(metricProfile, (Single?)random.Next(1, 100) / 100, null);
            var performanceDataTotals = new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData);

            var metricRatio = (Single)metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).GetValue(metricProfile, null);
            var dataValue = (Single)performanceData.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceData, null);
            var dataValueTotal = (Single)performanceDataTotals.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceDataTotals, null);
            var expected = metricRatio * (dataValue / dataValueTotal);
            var actual = PlanogramBlocking.GetUnNormalisedPsi(metricProfile, product, performanceDataTotals);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource("_oneToTwenty")]
        public void GetUnNormalisedPsi_MetricCalculated_WhenMaximumDecrease(Byte metricNumber)
        {
            var random = new Random();
            var planogram = Planogram.NewPlanogram();
            var product = PlanogramProduct.NewPlanogramProduct();
            planogram.Products.Add(product);

            var performanceData = planogram.Performance.PerformanceData.First();
            typeof(PlanogramPerformanceData).GetProperty(String.Format("P{0}", metricNumber)).SetValue(performanceData, (Single?)random.Next(1, 10), null);

            var metric = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(metricNumber);
            metric.Direction = MetricDirectionType.MaximiseDecrease;
            planogram.Performance.Metrics.Clear();
            planogram.Performance.Metrics.Add(metric);

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).SetValue(metricProfile, (Single?)random.Next(1, 100) / 100, null);
            var performanceDataTotals = new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData);

            var metricRatio = (Single)metricProfile.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber)).GetValue(metricProfile, null);
            var dataValue = (Single)performanceData.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceData, null);
            var dataValueTotal = (Single)performanceDataTotals.GetType().GetProperty(String.Format("P{0}", metricNumber)).GetValue(performanceDataTotals, null);
            var expected = metricRatio * (1 - (dataValue / dataValueTotal));
            var actual = PlanogramBlocking.GetUnNormalisedPsi(metricProfile, product, performanceDataTotals);

            Assert.AreEqual(expected, actual);
        }
        #endregion

        #region ApplyPerformanceData

        [Test]
        public void ApplyPerformanceData_ObservesSizeLimitations_WithMultipleGroupConstraints()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);
            var topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            var topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            var bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            topLeftBlock.IsLimited = true;
            topLeftBlock.LimitedPercentage = 0.1f;
            topRightBlock.IsLimited = true;
            topRightBlock.LimitedPercentage = 0.05f;
            bottomLeftBlock.IsLimited = true;
            bottomLeftBlock.LimitedPercentage = 0.05f;
            bottomRightBlock.IsLimited = true;
            bottomRightBlock.LimitedPercentage = 0.1f;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 4;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 1;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 4;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 1;
            plan.CreateAssortmentFromProducts();
            plan.AddSequenceGroup(topLeftBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(1).Take(1));
            plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(2).Take(1));
            plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Skip(3).Take(1));
            var metricProfileMock = new Mock<IPlanogramMetricProfile>();
            metricProfileMock.Setup(m => m.Metric1Ratio).Returns(1f);

            blocking.ApplyPerformanceData(metricProfileMock.Object);

            topLeftBlock.TotalSpacePercentage.Should().BeApproximately(
                0.3f, 2,  "because the top left block is constrained by the top right block.");
            topRightBlock.TotalSpacePercentage.Should().BeApproximately(
                0.2f, 2, "because the top right block is constrained by its limited percentage.");
            bottomLeftBlock.TotalSpacePercentage.Should().BeApproximately(
                0.2f, 2, "because the bottom right block is constrained by its limited percentage.");
            bottomRightBlock.TotalSpacePercentage.Should().BeApproximately(
                0.3f, 2, "because the bottom left block is constrained by the bottom right block.");
        }

        [Test]
        public void ApplyPerformanceData_DoesNotResizeBlock_WhenIsLimitedIsFalse_OneOtherBlock()
        {
            var plan = nameof(Package).CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var initialBlocking = plan.AddBlocking(PlanogramBlockingType.Initial);
            initialBlocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var topBlock = initialBlocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            topBlock.IsLimited = true;
            var bottomBlock = initialBlocking.Locations.First(l => l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            bottomBlock.IsLimited = false;
            for (Int32 i = 0; i < 2; i++) plan.AddProduct();
            plan.Products[0].GetPlanogramPerformanceData().P1 = 2;
            plan.Products[1].GetPlanogramPerformanceData().P1 = 1;
            plan.CreateAssortmentFromProducts();
            plan.AddSequenceGroup(topBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(bottomBlock, plan.Products.Skip(1));
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.PerformanceApplied, initialBlocking);
            plan.Blocking.Add(performanceBlocking);
            var metricProfileMock = new Mock<IPlanogramMetricProfile>();
            metricProfileMock.Setup(m => m.Metric1Ratio).Returns(1f);

            performanceBlocking.ApplyPerformanceData(metricProfileMock.Object);

            Assert.That(performanceBlocking.Dividers[0].Y, Is.EqualTo(0.5f));
        }

        [Test]
        public void ApplyPerformanceData_DoesNotResizeBlock_WhenIsLimitedIsTrue_TwoOtherBlocks()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var initialBlocking = plan.AddBlocking(PlanogramBlockingType.Initial);
            initialBlocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            initialBlocking.Dividers.Add(0, 0.6f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = initialBlocking.Locations.First(l => l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            bottomBlock.IsLimited = true;
            var middleBlock = initialBlocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            middleBlock.IsLimited = false;
            var topBlock = initialBlocking.Locations.First(l => l.Y.EqualTo(0.6f)).GetPlanogramBlockingGroup();
            topBlock.IsLimited = false;
            for (Int32 i = 0; i < 3; i++) plan.AddProduct().GetPlanogramPerformanceData().P1 = 1;
            plan.CreateAssortmentFromProducts();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(middleBlock, plan.Products.Skip(1).Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(2).Take(1));
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.PerformanceApplied, initialBlocking);
            plan.Blocking.Add(performanceBlocking);
            var metricProfileMock = new Mock<IPlanogramMetricProfile>();
            metricProfileMock.Setup(m => m.Metric1Ratio).Returns(1f);

            performanceBlocking.ApplyPerformanceData(metricProfileMock.Object);
            //plan.TakeSnapshot("ApplyPerformanceData_DoesNotResizeBlock_WhenCanOptimiseIsFalse_TwoOtherBlocks");

            Assert.That(performanceBlocking.Dividers[0].Y, Is.EqualTo(0.5f));
            Assert.That(performanceBlocking.Dividers[1].Y, Is.EqualTo(0.75f));
        }

        [Test]
        public void ApplyPerformanceData_CanOptimiseFalseOnBlockInMiddle_PreventsAllDividersFromMoving()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var initialBlocking = plan.AddBlocking(PlanogramBlockingType.Initial);
            initialBlocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);
            initialBlocking.Dividers.Add(0, 0.75f, PlanogramBlockingDividerType.Horizontal);
            initialBlocking.Dividers.Add(0.25f, 0.5f, PlanogramBlockingDividerType.Vertical);
            initialBlocking.Dividers.Add(0.75f, 0.5f, PlanogramBlockingDividerType.Vertical);
            initialBlocking.Locations.First(l => l.X.EqualTo(0.25f)).GetPlanogramBlockingGroup().IsLimited = true;
            for (Int32 i = 0; i < initialBlocking.Groups.Count; i++) plan.AddProduct().GetPlanogramPerformanceData().P1 = 1;
            plan.CreateAssortmentFromProducts();
            foreach (var group in initialBlocking.Groups)
            {
                plan.AddSequenceGroup(group, new[] { plan.Products[initialBlocking.Groups.IndexOf(group)] });
            }
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.PerformanceApplied, initialBlocking);
            plan.Blocking.Add(performanceBlocking);
            var metricProfileMock = new Mock<IPlanogramMetricProfile>();
            metricProfileMock.Setup(m => m.Metric1Ratio).Returns(1f);

            performanceBlocking.ApplyPerformanceData(metricProfileMock.Object);

            Int32 dividerIndex = 0;
            foreach (var divider in initialBlocking.Dividers)
            {
                Assert.That(performanceBlocking.Dividers[dividerIndex].X, Is.EqualTo(divider.X), String.Format("Divider locations should not have changed for divider {0}", dividerIndex));
                Assert.That(performanceBlocking.Dividers[dividerIndex].Y, Is.EqualTo(divider.Y), String.Format("Divider locations should not have changed for divider {0}", dividerIndex));
                dividerIndex++;
            }
        }

        [Test]
        public void ApplyPerformanceData_MovesDividersToExpectedPositions_VerticalRootDividers()
        {
            //       _______________
            //      |        C      |  
            //      |___(1)_________|  
            //      |       |__(4)_D|  
            //      |      (3)     E|  
            //      |   B   |__(5)__|  
            //      |       |      F|  
            //      |__(2)__|_______|  
            //      |       A       |  
            //      |_______________|  
            //

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Products
            var productA = TestDataHelper.CreatePlanogramProduct(planogram);
            var productB = TestDataHelper.CreatePlanogramProduct(planogram);
            var productC = TestDataHelper.CreatePlanogramProduct(planogram);
            var productD = TestDataHelper.CreatePlanogramProduct(planogram);
            var productE = TestDataHelper.CreatePlanogramProduct(planogram);
            var productF = TestDataHelper.CreatePlanogramProduct(planogram);

            // Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productA.Id).P1 = 10f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productB.Id).P1 = 5f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productC.Id).P1 = 10f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productD.Id).P1 = 1f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productE.Id).P1 = 2f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productF.Id).P1 = 2f;

            // Groups
            var groupA = TestDataHelper.CreateBlockingGroup(new[] { productA.Gtin }, blocking);
            var groupB = TestDataHelper.CreateBlockingGroup(new[] { productB.Gtin }, blocking);
            var groupC = TestDataHelper.CreateBlockingGroup(new[] { productC.Gtin }, blocking);
            var groupD = TestDataHelper.CreateBlockingGroup(new[] { productD.Gtin }, blocking);
            var groupE = TestDataHelper.CreateBlockingGroup(new[] { productE.Gtin }, blocking);
            var groupF = TestDataHelper.CreateBlockingGroup(new[] { productF.Gtin }, blocking);

            // Locations & divider
            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0.4f, 0.2f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var divider4 = TestDataHelper.CreateBlockingDivider(0.5f, 0.7f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var divider5 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider2, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(null, divider3, divider1, divider2, blocking.Locations, groupB);
            var locationC = TestDataHelper.CreateBlockingLocation(null, null, null, divider1, blocking.Locations, groupC);
            var locationD = TestDataHelper.CreateBlockingLocation(divider3, null, divider1, divider4, blocking.Locations, groupD);
            var locationE = TestDataHelper.CreateBlockingLocation(divider3, null, divider4, divider5, blocking.Locations, groupE);
            var locationF = TestDataHelper.CreateBlockingLocation(divider3, null, divider5, divider2, blocking.Locations, groupF);

            planogram.CreateAssortmentFromProducts();

            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));

            blocking.ApplyPerformanceData(metricProfile);

            var totalPsi = productPsis.Values.Sum();
            var divider1Position = 1 - productPsis[productC];
            var divider2Position = productPsis[productA];
            var divider3Position = productPsis[productB] / (productPsis[productB] + productPsis[productD] + productPsis[productE] + productPsis[productF]);
            var divider4Position = divider1Position - (divider1Position - divider2Position) * (productPsis[productD] / (productPsis[productD] + productPsis[productE] + productPsis[productF]));
            var divider5Position = divider2Position + (divider1Position - divider2Position) * (productPsis[productF] / (productPsis[productD] + productPsis[productE] + productPsis[productF]));

            Debug.WriteLine(String.Format("Expected: {0}, Actual: {1}", Math.Round(divider1Position, 5), Math.Round(divider1.Y, 5)));
            Debug.WriteLine(String.Format("Expected: {0}, Actual: {1}", Math.Round(divider2Position, 5), Math.Round(divider2.Y, 5)));
            Debug.WriteLine(String.Format("Expected: {0}, Actual: {1}", Math.Round(divider3Position, 5), Math.Round(divider3.X, 5)));
            Debug.WriteLine(String.Format("Expected: {0}, Actual: {1}", Math.Round(divider4Position, 5), Math.Round(divider4.Y, 5)));
            Debug.WriteLine(String.Format("Expected: {0}, Actual: {1}", Math.Round(divider5Position, 5), Math.Round(divider5.Y, 5)));

            Assert.AreEqual(Math.Round(divider1Position, 5), Math.Round(divider1.Y, 5));
            Assert.AreEqual(Math.Round(divider2Position, 5), Math.Round(divider2.Y, 5));
            Assert.AreEqual(Math.Round(divider3Position, 5), Math.Round(divider3.X, 5));
            Assert.AreEqual(Math.Round(divider4Position, 5), Math.Round(divider4.Y, 5));
            Assert.AreEqual(Math.Round(divider5Position, 5), Math.Round(divider5.Y, 5));
        }

        [Test]
        public void ApplyPerformanceData_MovesDividersToExpectedPositions_HorizontalRootDividers()
        {
            //       _______________________________
            //      |[E]|  [F]  |  [G]  |    [H]    |
            //      |__(7)_(3)__|       |____(6)____|
            //      |           |__(4)__|           |
            //      |          (1) [D] (2)          |
            //      |           |__(5)__|           |
            //      |   [A]     |  [B]  |     [C]   |
            //      |___________|_______|___________|

            #region Create Planogram
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            #endregion

            #region Create Products
            // Products
            var productA = TestDataHelper.CreatePlanogramProduct(planogram);
            var productB = TestDataHelper.CreatePlanogramProduct(planogram);
            var productC = TestDataHelper.CreatePlanogramProduct(planogram);
            var productD = TestDataHelper.CreatePlanogramProduct(planogram);
            var productE = TestDataHelper.CreatePlanogramProduct(planogram);
            var productF = TestDataHelper.CreatePlanogramProduct(planogram);
            var productG = TestDataHelper.CreatePlanogramProduct(planogram);
            var productH = TestDataHelper.CreatePlanogramProduct(planogram);
            #endregion

            #region Create Performance
            // Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productA.Id).P1 = 0.15f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productB.Id).P1 = 0.065f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productC.Id).P1 = 0.35f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productD.Id).P1 = 0.06f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productE.Id).P1 = 0.02f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productF.Id).P1 = 0.08f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productG.Id).P1 = 0.125f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productH.Id).P1 = 0.15f;
            #endregion

            #region Create Blocking Groups
            // Groups
            var groupA = TestDataHelper.CreateBlockingGroup(new[] { productA.Gtin }, blocking);
            var groupB = TestDataHelper.CreateBlockingGroup(new[] { productB.Gtin }, blocking);
            var groupC = TestDataHelper.CreateBlockingGroup(new[] { productC.Gtin }, blocking);
            var groupD = TestDataHelper.CreateBlockingGroup(new[] { productD.Gtin }, blocking);
            var groupE = TestDataHelper.CreateBlockingGroup(new[] { productE.Gtin }, blocking);
            var groupF = TestDataHelper.CreateBlockingGroup(new[] { productF.Gtin }, blocking);
            var groupG = TestDataHelper.CreateBlockingGroup(new[] { productG.Gtin }, blocking);
            var groupH = TestDataHelper.CreateBlockingGroup(new[] { productH.Gtin }, blocking);
            #endregion

            #region Create locations and dividers
            // Locations & divider
            var divider1 = TestDataHelper.CreateBlockingDivider(0.3f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0.6f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 0.3f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider4 = TestDataHelper.CreateBlockingDivider(0.3f, 0.5f, 0.3f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider5 = TestDataHelper.CreateBlockingDivider(0.3f, 0.2f, 0.3f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider6 = TestDataHelper.CreateBlockingDivider(0.6f, 0.7f, 0.4f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider7 = TestDataHelper.CreateBlockingDivider(0.1f, 0.8f, 0.2f, PlanogramBlockingDividerType.Vertical, 3, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, divider1, divider3, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(divider1, divider2, divider5, null, blocking.Locations, groupB);
            var locationC = TestDataHelper.CreateBlockingLocation(divider2, null, divider6, null, blocking.Locations, groupC);
            var locationD = TestDataHelper.CreateBlockingLocation(divider1, divider2, divider4, divider5, blocking.Locations, groupD);
            var locationE = TestDataHelper.CreateBlockingLocation(null, divider7, null, divider3, blocking.Locations, groupE);
            var locationF = TestDataHelper.CreateBlockingLocation(divider7, divider1, null, divider3, blocking.Locations, groupF);
            var locationG = TestDataHelper.CreateBlockingLocation(divider1, divider2, null, divider4, blocking.Locations, groupG);
            var locationH = TestDataHelper.CreateBlockingLocation(divider2, null, null, divider6, blocking.Locations, groupH);
            #endregion

            planogram.CreateAssortmentFromProducts();
            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));


            blocking.ApplyPerformanceData(metricProfile);

            AssertDividerPosition(divider1, 0.25f, 0f, 1f);
            AssertDividerPosition(divider2, 0.5f, 0f, 1f);
            AssertDividerPosition(divider3, 0f, 0.6f, 0.25f);
            AssertDividerPosition(divider4, 0.25f, 0.5f, 0.25f);
            AssertDividerPosition(divider5, 0.25f, 0.26f, 0.25f);
            AssertDividerPosition(divider6, 0.5f, 0.7f, 0.5f);
            AssertDividerPosition(divider7, 0.05f, 0.6f, 0.4f);
        }

        [Test]
        public void ApplyPerformanceData_MovesDividersToExpectedPositions_MultiplePerformanceDatas()
        {
            //       _______________
            //      |       |       |
            //      |      (1)      |
            //      |  [A]  |  [B]  |
            //      |_______|_______|

            #region Create Planogram
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 0.5F;
            metricProfile.Metric2Ratio = 0.5F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            #endregion

            #region Create Products
            var productA = TestDataHelper.CreatePlanogramProduct(planogram);
            var productB = TestDataHelper.CreatePlanogramProduct(planogram);
            #endregion

            #region Create Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            var metric2 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(2);
            metric2.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.AddRange(new[] { metric1, metric2 });
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productA.Id).P1 = 0.5f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productB.Id).P1 = 0.5f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productA.Id).P2 = 0.5f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productB.Id).P2 = 0.5f;
            #endregion

            #region Create Blocking Groups
            var groupA = TestDataHelper.CreateBlockingGroup(new[] { productA.Gtin }, blocking);
            var groupB = TestDataHelper.CreateBlockingGroup(new[] { productB.Gtin }, blocking);
            #endregion

            #region Create locations and dividers
            var divider1 = TestDataHelper.CreateBlockingDivider(0.8f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, divider1, null, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(divider1, null, null, null, blocking.Locations, groupB);
            #endregion

            planogram.CreateAssortmentFromProducts();
            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));

            blocking.ApplyPerformanceData(metricProfile);

            AssertDividerPosition(divider1, 0.5f, 0f, 1f);
        }

        [Test]
        public void ApplyPerformanceData_MovesDividersToExpectedPositions_MultipleProducts()
        {
            //       _______________
            //      |       |       |
            //      |      (1)      |
            //      |  [A]  |  [B]  |
            //      |_______|_______|

            #region Create Planogram
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            #endregion

            #region Create Products
            var productA1 = TestDataHelper.CreatePlanogramProduct(planogram);
            var productA2 = TestDataHelper.CreatePlanogramProduct(planogram);
            var productB1 = TestDataHelper.CreatePlanogramProduct(planogram);
            var productB2 = TestDataHelper.CreatePlanogramProduct(planogram);
            #endregion

            #region Create Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productA1.Id).P1 = 0.5f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productA2.Id).P1 = 0.5f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productB1.Id).P1 = 0.5f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == productB2.Id).P1 = 0.5f;
            #endregion

            #region Create Blocking Groups
            var groupA = TestDataHelper.CreateBlockingGroup(new[] { productA1.Gtin, productA2.Gtin }, blocking);
            var groupB = TestDataHelper.CreateBlockingGroup(new[] { productB1.Gtin, productB2.Gtin }, blocking);
            #endregion

            #region Create locations and dividers
            var divider1 = TestDataHelper.CreateBlockingDivider(0.8f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, divider1, null, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(divider1, null, null, null, blocking.Locations, groupB);
            #endregion

            planogram.CreateAssortmentFromProducts();
            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));

            blocking.ApplyPerformanceData(metricProfile);

            AssertDividerPosition(divider1, 0.5f, 0f, 1f);
        }

        [Test]
        public void ApplyPerformanceData_WhenPerformanceRatioZero_ProductsWithMoreVolumeGetMoreSpace()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bigProduct = plan.AddProduct(new WidthHeightDepthValue(30, 10, 10));
            var smallProduct = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, new[] { bigProduct });
            plan.AddSequenceGroup(rightBlock, new[] { smallProduct });
            plan.CreateAssortmentFromProducts(plan.Products);
            foreach (var ap in plan.Assortment.Products) { ap.IsRanged = true; ap.Units = 1; }
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            blocking.ApplyPerformanceData(metricProfile, performanceRatio: 0f);

            Assert.That(leftBlock.GetBlockingLocations().First().Width, Is.EqualTo(0.75f));
            Assert.That(rightBlock.GetBlockingLocations().First().Width, Is.EqualTo(0.25f));
        }

        [Test]
        public void ApplyPerformanceData_WhenPerformanceRatioNotZero_ProductsVolumeIsWeightedAgainstPerformance()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bigProduct = plan.AddProduct(new WidthHeightDepthValue(30, 10, 10));
            bigProduct.GetPlanogramPerformanceData().P1 = 1;
            var smallProduct = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            smallProduct.GetPlanogramPerformanceData().P1 = 3;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.1f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.1f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, new[] { bigProduct });
            plan.AddSequenceGroup(rightBlock, new[] { smallProduct });
            plan.CreateAssortmentFromProducts(plan.Products);
            foreach (var ap in plan.Assortment.Products) { ap.IsRanged = true; ap.Units = 1; }
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            blocking.ApplyPerformanceData(metricProfile, performanceRatio: 0.5f);

            Assert.That(leftBlock.GetBlockingLocations().First().Width, Is.EqualTo(0.5f));
            Assert.That(rightBlock.GetBlockingLocations().First().Width, Is.EqualTo(0.5f));
        }

        [Test]
        public void ApplyPerformanceData_WhenDividerHasLimitedPercentage_DoesNotExceedPercentage()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 3f;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 1f;
            var blocking = plan.AddBlocking();
            var divider = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            divider.IsLimited = true;
            divider.LimitedPercentage = 0.1f;
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(1));
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, blocking);
            plan.Blocking.Add(performanceBlocking);
            plan.CreateAssortmentFromProducts(plan.Products);
            foreach (var ap in plan.Assortment.Products) { ap.IsRanged = true; ap.Units = 1; }
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            performanceBlocking.ApplyPerformanceData(metricProfile);

            performanceBlocking.Dividers[0].Y.Should()
                .BeLessOrEqualTo(0.6f, "because the divider should not exceed its original size plus its limited percentage")
                .And.BeGreaterOrEqualTo(0.4f, "because the divider should not be less than its original size minus its limited percentage");
        }

        [Test]
        public void ApplyPerformanceData_WhenDividerHasLimitedPercentage_DoesNotFallBelowPercentage()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 1f;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 3f;
            var blocking = plan.AddBlocking();
            var divider = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            divider.IsLimited = true;
            divider.LimitedPercentage = 0.1f;
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(1));
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, blocking);
            plan.Blocking.Add(performanceBlocking);
            plan.CreateAssortmentFromProducts(plan.Products);
            foreach (var ap in plan.Assortment.Products) { ap.IsRanged = true; ap.Units = 1; }
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            performanceBlocking.ApplyPerformanceData(metricProfile);

            performanceBlocking.Dividers[0].Y.Should()
                .BeLessOrEqualTo(0.6f, "because the divider should not exceed its original size plus its limited percentage")
                .And.BeGreaterOrEqualTo(0.4f, "because the divider should not be less than its original size minus its limited percentage");
        }

        [Test]
        public void ApplyPerformanceData_WhenBlockHasLimitedPercentage_DoesNotExceedPercentage()
        {
            var plan = nameof(Package).CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 3f;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 1f;
            var blocking = plan.AddBlocking();
            var divider = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            bottomBlock.IsLimited = true;
            bottomBlock.LimitedPercentage = 0.1f;
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(1));
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, blocking);
            plan.Blocking.Add(performanceBlocking);
            plan.CreateAssortmentFromProducts(plan.Products);
            foreach (var ap in plan.Assortment.Products) { ap.IsRanged = true; ap.Units = 1; }
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            performanceBlocking.ApplyPerformanceData(metricProfile);

            var performanceAppliedBottomBlock = performanceBlocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            performanceAppliedBottomBlock.TotalSpacePercentage.Should()
                .BeLessOrEqualTo(0.6f, "because the block's size should not exceed its original size plus its limited percentage")
                .And.BeGreaterOrEqualTo(0.4f, "because the block's size should not be less than its original size minus its limited percentage");
        }

        [Test]
        public void ApplyPerformanceData_WhenBlockHasLimitedPercentage_DoesNotFallBelowPercentage()
        {
            var plan = nameof(Package).CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 1f;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 3f;
            var blocking = plan.AddBlocking();
            var divider = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            bottomBlock.IsLimited = true;
            bottomBlock.LimitedPercentage = 0.1f;
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(1));
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, blocking);
            plan.Blocking.Add(performanceBlocking);
            plan.CreateAssortmentFromProducts(plan.Products);
            foreach (var ap in plan.Assortment.Products) { ap.IsRanged = true; ap.Units = 1; }
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            performanceBlocking.ApplyPerformanceData(metricProfile);

            var performanceAppliedBottomBlock = performanceBlocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            performanceAppliedBottomBlock.TotalSpacePercentage.Should()
                .BeLessOrEqualTo(0.6f, "because the block's size should not exceed its original size plus its limited percentage")
                .And.BeGreaterOrEqualTo(0.4f, "because the block's size should not be less than its original size minus its limited percentage");
        }

        [Test]
        public void ApplyPerformanceData_WhenBlockHasLimitedPercentageThatDoesNotConstraintGrowth_ShouldGrowWithPerformanceData()
        {
            var plan = nameof(Package).CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 3f;
            plan.AddProduct().GetPlanogramPerformanceData().P1 = 1f;
            var blocking = plan.AddBlocking();
            var divider = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            bottomBlock.IsLimited = true;
            bottomBlock.LimitedPercentage = 0.3f;
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Skip(1));
            var performanceBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, blocking);
            plan.Blocking.Add(performanceBlocking);
            plan.CreateAssortmentFromProducts(plan.Products);
            foreach (var ap in plan.Assortment.Products) { ap.IsRanged = true; ap.Units = 1; }
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            performanceBlocking.ApplyPerformanceData(metricProfile);

            var performanceAppliedBottomBlock = performanceBlocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            performanceAppliedBottomBlock.TotalSpacePercentage.Should()
                .Be(0.75f, "because the performance calculation should be unaffected when the limited percentage is greater than the required growth");
        }

        private void AssertDividerPosition(PlanogramBlockingDivider divider, Single x, Single y, Single length)
        {
            Assert.That(divider.X.EqualTo(x));
            Assert.That(divider.Y.EqualTo(y));
            Assert.That(divider.Length.EqualTo(length));
        }
        #endregion

        #region GroupLevelDividers
        [Test]
        public void GroupLevelDividers_GroupsCorrectly_ForLevelOne()
        {
            //       _______________________
            //      |____(4)____|           |
            //      |____(5)___(2)          |
            //      |           |           |
            //      |--------------(1)------|
            //      |____(6)____|           |
            //      |____(7)___(3)          |
            //      |___________|___________|
            //

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var div1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div3 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 0.5f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div4 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div5 = TestDataHelper.CreateBlockingDivider(0f, 0.6f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div6 = TestDataHelper.CreateBlockingDivider(0f, 0.4f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div7 = TestDataHelper.CreateBlockingDivider(0f, 0.3f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);

            IEnumerable<IEnumerable<PlanogramBlockingDivider>> expected = new[]
            {
                new[] { div1 }
            };

            var actual = PlanogramBlocking.ApplyBlockingContext.GroupLevelDividers(blocking.Dividers.Where(d => d.Level == 1));

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GroupLevelDividers_GroupsCorrectly_ForLevelTwo()
        {
            //       _______________________
            //      |____(4)____|           |
            //      |____(5)___(2)          |
            //      |           |           |
            //      |--------------(1)------|
            //      |____(6)____|           |
            //      |____(7)___(3)          |
            //      |___________|___________|
            //

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var div1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div3 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 0.5f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div4 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div5 = TestDataHelper.CreateBlockingDivider(0f, 0.6f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div6 = TestDataHelper.CreateBlockingDivider(0f, 0.4f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div7 = TestDataHelper.CreateBlockingDivider(0f, 0.3f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);

            IEnumerable<IEnumerable<PlanogramBlockingDivider>> expected = new[]
            {
                new[] { div2 },
                new[] { div3 }
            };

            var actual = PlanogramBlocking.ApplyBlockingContext.GroupLevelDividers(blocking.Dividers.Where(d => d.Level == 2));

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GroupLevelDividers_GroupsCorrectly_ForLevelThree()
        {
            //       _______________________
            //      |____(4)____|           |
            //      |____(5)___(2)          |
            //      |           |           |
            //      |--------------(1)------|
            //      |____(6)____|           |
            //      |____(7)___(3)          |
            //      |___________|___________|
            //

            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            var div1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var div2 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div3 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 0.5f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var div4 = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div5 = TestDataHelper.CreateBlockingDivider(0f, 0.6f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div6 = TestDataHelper.CreateBlockingDivider(0f, 0.4f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);
            var div7 = TestDataHelper.CreateBlockingDivider(0f, 0.3f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);

            IEnumerable<IEnumerable<PlanogramBlockingDivider>> expected = new[]
            {
                new[] { div4, div5 },
                new[] { div6, div7 }
            };

            var actual = PlanogramBlocking.ApplyBlockingContext.GroupLevelDividers(blocking.Dividers.Where(d => d.Level == 3));

            CollectionAssert.AreEquivalent(expected, actual);
        }
        #endregion

        #region GetAndUpdateProductStacksByColour

        [Test]
        public void GetAndUpdateProductStacksByColour_GetCorrectSequenceGroup()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            for (Int32 i = 0; i < 3; i++) shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));

            plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf2.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.AddPosition(bay, plan.Products[0]);
            shelf2.AddPosition(bay, plan.Products[2]);
            shelf2.AddPosition(bay, plan.Products[3]);

            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, new[] { plan.Products[0], plan.Products[1], plan.Products[2] });
            plan.AddSequenceGroup(topBlock, new[] { plan.Products[0], plan.Products[2], plan.Products[3] });
            plan.UpdatePositionSequenceData();
            plan.ReprocessAllMerchandisingGroups();

            PlanogramPosition planogramPosition = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().FirstOrDefault(p => p.PlanogramProductId.Equals(plan.Products[2].Id));
            planogramPosition.SequenceColour = null;
            planogramPosition.SequenceNumber = null;

            var expectedProductStacksByColour = new Dictionary<Int32, List<PlanogramProduct>>()
            {
                { bottomBlock.Colour, new List<PlanogramProduct>() { plan.Products[0], plan.Products[1], plan.Products[2] } },
                { topBlock.Colour, new List<PlanogramProduct>() { plan.Products[0], plan.Products[2], plan.Products[3] } }
            };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var productStacks = blocking.GetAndUpdateProductStacksByColour(merchGroups);

                foreach (var productStack in productStacks)
                {
                    Assert.AreEqual(expectedProductStacksByColour[productStack.Key].Count, productStack.Value.Count(), "The number of product stacks created should be the same.");
                }
            }
        }

        [Test]
        public void GetAndUpdateProductStacksByColour_GetCorrectSequenceGroupWhenSequencesMatchWithinGroups()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            for (Int32 i = 0; i < 3; i++) shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));

            plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf2.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.AddPosition(bay, plan.Products[0]);
            shelf2.AddPosition(bay, plan.Products[2]);
            shelf2.AddPosition(bay, plan.Products[3]);

            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, new[] { plan.Products[0], plan.Products[1], plan.Products[2], plan.Products[3], plan.Products[4] });
            plan.AddSequenceGroup(topBlock, new[] { plan.Products[0], plan.Products[1], plan.Products[2], plan.Products[3] });
            plan.UpdatePositionSequenceData();
            plan.ReprocessAllMerchandisingGroups();

            PlanogramPosition planogramPosition = shelf.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().FirstOrDefault(p => p.PlanogramProductId.Equals(plan.Products[1].Id));
            planogramPosition.SequenceColour = null;
            planogramPosition.SequenceNumber = null;

            var expectedProductStacksByColour = new Dictionary<Int32, List<PlanogramProduct>>()
            {
                { bottomBlock.Colour, new List<PlanogramProduct>() { plan.Products[0], plan.Products[2] } },
                { topBlock.Colour, new List<PlanogramProduct>() { plan.Products[0], plan.Products[1], plan.Products[2], plan.Products[3] } }
            };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var productStacks = blocking.GetAndUpdateProductStacksByColour(merchGroups);

                foreach (var productStack in productStacks)
                {
                    Assert.AreEqual(expectedProductStacksByColour[productStack.Key].Count, productStack.Value.Count(), "The number of product stacks created should be the same.");
                }
            }
        }

        #endregion
    }

    internal static class DividerTestExtensions
    {
        public static Boolean IsHorizontal(this PlanogramBlockingDivider divider)
        {
            return divider.Type == PlanogramBlockingDividerType.Horizontal;
        }

        public static Boolean IsVertical(this PlanogramBlockingDivider divider)
        {
            return divider.Type == PlanogramBlockingDividerType.Vertical;
        }
    }
}
