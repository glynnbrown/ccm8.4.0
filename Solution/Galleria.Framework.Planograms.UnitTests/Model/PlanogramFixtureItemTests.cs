﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26986 : A.Kuszyk
//  Created.
// V8-27397 : A.Kuszyk
//  Change references to fixture width and planogram height to use GetDesignSize method on Planogram.
// V8-27510 : A.Kuszyk
//  Removed FlattenedX tests.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramFixtureItemTests
    {
        [Test]
        public void PercentageX_ReturnsCorrectPosition()
        {
            const Int32 noOfFixtureItems = 10;
            const Single fixtureWidth = 10f;

            var planogramFixture = PlanogramFixture.NewPlanogramFixture();
            planogramFixture.Width = fixtureWidth;
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(planogramFixture);
            for (Int32 i = 1; i < noOfFixtureItems; i++)
            {
                planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(planogramFixture));
            }
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(planogramFixture);
            planogram.FixtureItems.Add(fixtureItem);
            Single planogramHeight, planogramWidth;
            planogram.GetBlockingAreaSize(out planogramHeight, out planogramWidth);

            Single expected = ((noOfFixtureItems-1) * fixtureWidth) / planogramWidth;

            var result = fixtureItem.PercentageX;

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void PercentageWidth_ReturnsCorrectValue()
        {
            const Int32 noOfFixtureItems = 10;
            const Single fixtureWidth = 10;

            var planogramFixture = PlanogramFixture.NewPlanogramFixture();
            planogramFixture.Width = fixtureWidth;
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(planogramFixture);
            for (Int32 i = 1; i < noOfFixtureItems; i++)
            {
                planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(planogramFixture));
            }
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(planogramFixture);
            planogram.FixtureItems.Add(fixtureItem);
            Single planogramHeight, planogramWidth;
            planogram.GetBlockingAreaSize(out planogramHeight, out planogramWidth);

            Single expected = fixtureWidth / planogramWidth;

            var result = fixtureItem.PercentageWidth;

            Assert.AreEqual(expected, result);
        }
    }
}
