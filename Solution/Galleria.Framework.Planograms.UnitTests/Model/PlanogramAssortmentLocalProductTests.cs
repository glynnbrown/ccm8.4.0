﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26789 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.Planograms.Model;
using Moq;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramAssortmentLocalProductTests
    {
        [Test]
        public void NewPlanogramAssortmentLocalProduct_Interface_AddsProperties()
        {
            var mock = new Mock<IPlanogramAssortmentLocalProduct>();
            mock.SetupGet(p => p.LocationCode).Returns("LocationCode");
            mock.SetupGet(p => p.ProductGtin).Returns("ProductGtin");
            var mockInterface = mock.Object;

            var model = PlanogramAssortmentLocalProduct.NewPlanogramAssortmentLocalProduct(mockInterface);

            AssertHelper.AreEqual<IPlanogramAssortmentLocalProduct>(model, mockInterface);
        }
    }
}
