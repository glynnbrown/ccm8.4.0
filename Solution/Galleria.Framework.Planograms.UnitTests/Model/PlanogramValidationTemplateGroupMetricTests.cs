﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.
// V8-27062 : A.Silva ~ Added unit tests for Validation Calculations.
// V8-27456 : A.Silva
//      Added unit tests for Validation Calculation when Equal To.

#endregion

#region Version History: CCM803
// V8-29596 : L.Luong
//  Added Tests to Boolean and String fields validation
#endregion

#region Version History : CCM820
// V8-30844 : A.Kuszyk
//  Added tests for LessThanOrEqualTo and GreaterThanOrEqualTo options.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;
using TestHelper = Galleria.Framework.Planograms.UnitTests.Model.PlanogramValidationTemplateTestsHelper;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramValidationTemplateGroupMetricTests : TestBase
    {
        #region Constants

        private const Single GreaterThanThreshold1Value = 10;
        private const Single GreaterThanThreshold2Value = 5;
        private const Single GreaterThanGreenValue = 11;
        private const Single GreaterThanYellowValue = 6;
        private const Single GreaterThanRedValue = 3;

        private const Single LessThanThreshold1Value = 5;
        private const Single LessThanThreshold2Value = 10;
        private const Single LessThanGreenValue = 3;
        private const Single LessThanYellowValue = 6;
        private const Single LessThanRedValue = 11;

        private const Single EqualToThreshold1Value = 5;
        private const Single EqualToThreshold2Value = 2;
        private const Single EqualToGreenValue = 5;
        private const Single EqualToYellowValue = 7;
        private const Single EqualToRedValue = 10;

        #endregion

        #region Fields

        private readonly string _planogramField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof (Planogram), Planogram.FriendlyName,
                Planogram.HeightProperty).FieldPlaceholder;

        private readonly string _planogramComponentField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof (PlanogramComponent), PlanogramComponent.FriendlyName,
                PlanogramComponent.CapacityProperty).FieldPlaceholder;

        private readonly string _planogramComponentStringField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramComponent), PlanogramComponent.FriendlyName,
                PlanogramComponent.NameProperty).FieldPlaceholder;

        private readonly string _planogramComponentBooleanField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof(PlanogramComponent), PlanogramComponent.FriendlyName,
                PlanogramComponent.IsMerchandisedTopDownProperty).FieldPlaceholder;

        private readonly string _planogramFixtureField =
            ObjectFieldInfo.NewObjectFieldInfo(typeof (PlanogramFixture), PlanogramFixture.FriendlyName,
                PlanogramFixture.MetaTotalFixtureCostProperty).FieldPlaceholder;

        private PlanogramValidationTemplateGroupMetric _testModel;

        #endregion

        [SetUp]
        public void TestSetUp()
        {
            _testModel = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
            _testModel.Field = "ValidField";
        }

        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric());
        }

        #endregion

        #region BusinessRules

        [Test]
        public void IsValid_WhenValid_ShouldBeTrue()
        {
            Assert.IsTrue(
                _testModel.IsValid,
                "The initial test model should be valid.");
        }

        [Test]
        public void IsValid_WhenMissingField_ShouldBeFalse()
        {
            _testModel.Field = String.Empty;

            Assert.IsFalse(
                _testModel.IsValid, 
                "IsValid should be false when the field value is missing..");
        }

        [Test]
        public void IsValid_WhenFieldOver255_ShouldBeFalse()
        {
            _testModel.Field = new String('a', 256);

            Assert.IsFalse(
                _testModel.IsValid,
                "IsValid should be false when the field is longer than 255.");
        }

        [Test]
        public void IsValid_WhenGreaterThanAndThreshold1LessOrEqualThan2_ShouldBeFalse()
        {
            _testModel.SetUpValidation(PlanogramValidationType.GreaterThan, 0, 10);

            Assert.IsFalse(
                _testModel.IsValid,
                "IsValid should be false when ValidationType is GreaterThan and Threshold1 is less or equal than Threshold2.");
        }

        [Test]
        public void IsValid_WhenGreaterThanAndThreshold1GreaterThan2_ShouldBeTrue()
        {
            _testModel.SetUpValidation(PlanogramValidationType.GreaterThan, 10, 0);

            Assert.IsTrue(
                _testModel.IsValid,
                "IsValid should be true when ValidationType is GreaterThan and Threshold1 is greater than Threshold2.");
        }

        [Test]
        public void IsValid_WhenLessThanAndThreshold1GreaterOrEqualThan2_ShouldBeFalse()
        {
            _testModel.SetUpValidation(PlanogramValidationType.LessThan, 10, 0);

            Assert.IsFalse(
                _testModel.IsValid,
                "IsValid should be false when ValidationType is LessThan and Threshold1 is greater or equal than Threshold2.");
        }

        [Test]
        public void IsValid_WhenLessThanAndThreshold1LessThan2_ShouldBeTrue()
        {
            _testModel.SetUpValidation(PlanogramValidationType.LessThan, 0, 10);

            Assert.IsTrue(
                _testModel.IsValid,
                "IsValid should be true when ValidationType is LessThan and Threshold1 is less than Threshold2.");
        }

        [Test]
        public void IsValid_WhenEqualThanAndAnyThreshold1And2_ShouldBeTrue()
        {
            _testModel.SetUpValidation(PlanogramValidationType.EqualTo, 10, 0);

            Assert.IsTrue(
                _testModel.IsValid,
                "IsValid should be true when ValidationType is EqualThan for any Threshold1 and Threshold2.");
        }

        #endregion

        #region Factory Methods

        [Test]
        public void Fetch()
        {
            const string expected = "[Expected.Field]";
            //create new package
            var package = TestHelper.NewPackage(planograms:
                new List<Planogram>
                {
                    TestHelper.NewPlanogram(
                        validationTemplate:
                            TestHelper.NewValidationTemplate(groups:
                                new List<PlanogramValidationTemplateGroup>
                                {
                                    TestHelper.NewValidationGroup(metrics:
                                        new List<PlanogramValidationTemplateGroupMetric>
                                        {
                                            TestHelper.NewValidationMetric(expected)
                                        })
                                }))
                }).Save();

            //fetch
            var packageModel = Package.FetchById(package.Id, 1, PackageLockType.User);
            var validationMetric = packageModel.Planograms.First().ValidationTemplate.Groups.First().Metrics.First();
            var actual = validationMetric.Field;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void NewValidationTemplateGroup()
        {
            //create new package
            var validationMetric = TestHelper.NewValidationMetric();
            var validationGroup =
                TestHelper.NewValidationGroup(metrics:
                    new List<PlanogramValidationTemplateGroupMetric> { validationMetric });
            var expected = validationGroup.Id;

            var actual = validationMetric.Parent.Id;

            //check if validation is new and is child
            Assert.AreEqual(expected, actual);
            Assert.IsTrue(validationMetric.IsNew);
            Assert.IsTrue(validationMetric.IsChild,
                "A newly created Planogram Validation Template should be a child object.");
        }

        #endregion

        #region DataAccess

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var package = TestHelper.NewPackage(planograms:
                new List<Planogram>
                {
                    TestHelper.NewPlanogram(
                        validationTemplate:
                            TestHelper.NewValidationTemplate(groups:
                                new List<PlanogramValidationTemplateGroup>
                                {
                                    TestHelper.NewValidationGroup(metrics:
                                        new List<PlanogramValidationTemplateGroupMetric>
                                        {
                                            TestHelper.NewValidationMetric()
                                        })
                                }))
                });
            Debug.WriteLine(package.BuildErrorsDescription());
            package = package.Save();
            Object packageId = package.Id;
            package.Dispose();
            package = null;

            //fetch
            package = Package.FetchById(packageId, 1, PackageLockType.User);
            var validationGroup = package.Planograms.First().ValidationTemplate.Groups.First();
            var actual = validationGroup.Metrics.FirstOrDefault();

            Assert.IsNotNull(actual);

            // remove and save changes.
            validationGroup.Metrics.Remove(actual);
            Debug.WriteLine(package.BuildErrorsDescription());
            package = package.Save();
            packageId = package.Id;
            package.Dispose();
            package = null;


            // refetch and assert it is no longer fetched.
            package = Package.FetchById(packageId);
            validationGroup = package.Planograms.First().ValidationTemplate.Groups.First();
            actual = validationGroup.Metrics.FirstOrDefault();

            Assert.IsNull(actual);

            package.Dispose();
            package = null;
        }

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var package = TestHelper.NewPackage(planograms:
                new List<Planogram>
                {
                    TestHelper.NewPlanogram(
                        validationTemplate:
                            TestHelper.NewValidationTemplate(groups:
                                new List<PlanogramValidationTemplateGroup>
                                {
                                    TestHelper.NewValidationGroup(metrics:
                                        new List<PlanogramValidationTemplateGroupMetric>
                                        {
                                            TestHelper.NewValidationMetric()
                                        })
                                }))
                }).Save();

            var actual = package.Planograms.First().ValidationTemplate.Groups.First().Metrics.First();

            // check if the value is not null
            Assert.IsNotNull(actual);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            const string expectedFirst = "[Expected.Field]";
            const string expectedSecond = "[Expected.EditedField]";
            var package = TestHelper.NewPackage(planograms:
                new List<Planogram>
                {
                    TestHelper.NewPlanogram(
                        validationTemplate:
                            TestHelper.NewValidationTemplate(groups:
                                new List<PlanogramValidationTemplateGroup>
                                {
                                    TestHelper.NewValidationGroup(metrics:
                                        new List<PlanogramValidationTemplateGroupMetric>
                                        {
                                            TestHelper.NewValidationMetric(expectedFirst)
                                        })
                                }))
                }).Save();

            package = Package.FetchById(package.Id, 1, PackageLockType.User);
            var validationMetric = package.Planograms.First().ValidationTemplate.Groups.First().Metrics.First();
            var actual = validationMetric.Field;

            Assert.AreEqual(expectedFirst, actual);

            // Change the value and save.
            validationMetric.Field = expectedSecond;
            package.Save();

            package = Package.FetchById(package.Id);
            validationMetric = package.Planograms.First().ValidationTemplate.Groups.First().Metrics.First();
            actual = validationMetric.Field;

            Assert.AreNotEqual(expectedFirst, actual);
            Assert.AreEqual(expectedSecond, actual);
        }

        #endregion

        #region IValidation Members

        [Test]
        public void IValidationTemplateGroupMetricParent_Invoked_ReturnsParent()
        {
            const string expectation = "When Parent is invoked the actual parent is returned.";
            var expected = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            var testModel = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
            expected.Metrics.Add(testModel);

            var actual = ((IValidationTemplateGroupMetric)testModel).Parent;

            Assert.AreEqual(expected, actual, expectation);
        }

        #endregion

        #region CalculateValidationData

        #region Misc
        [Test]
        public void CalculateValidationData_WhenFieldAndValueInPlanogram_ShouldNotReturnUnknown()
        {
            const String expectation =
                "When field and value are in a Planogram model, ResultType should mot be unknown.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Unknown;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = GreaterThanGreenValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            ((IModelObject)mut).CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreNotEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenFieldAndValueInPlanogramComponent_ShouldNotReturnUnknown()
        {
            const String expectation =
                "When field and value are in a PlanogramComponent model, ResultType should mot be unknown.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Unknown;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var newPlanogramSettings = PlanogramSettings.NewPlanogramSettings();
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, newPlanogramSettings);
            component.Capacity = Convert.ToSByte(GreaterThanGreenValue);
            planogram.Components.Add(component);

            planogram.Height = GreaterThanGreenValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            ((IModelObject)mut).CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreNotEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenFieldAndValueInPlanogramFixture_ShouldNotReturnUnknown()
        {
            const String expectation =
                "When field and value are in a PlanogramFixture model, ResultType should mot be unknown.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Unknown;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramFixtureField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var item = PlanogramFixture.NewPlanogramFixture("TestFixture", PlanogramSettings.NewPlanogramSettings());
            item.MetaTotalFixtureCost = Convert.ToSByte(GreaterThanGreenValue);
            planogram.Fixtures.Add(item);

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            ((IModelObject)mut).CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreNotEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValuesInPlanogramComponentWouldBeGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When values in a PlanogramComponent model would result in green, ResultType should be 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value, aggregationType: PlanogramValidationAggregationType.Sum)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Capacity = Convert.ToSByte(GreaterThanRedValue);
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            planogram.MetaBayCount = Convert.ToInt32(GreaterThanGreenValue);
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            ((IModelObject)mut).CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenCountInPlanogramComponentWouldBeGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When values in a PlanogramComponent model would result in green, ResultType should be 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value, aggregationType: PlanogramValidationAggregationType.Count)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var newPlanogramSettings = PlanogramSettings.NewPlanogramSettings();
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, newPlanogramSettings);
            component.Capacity = Convert.ToSByte(GreaterThanRedValue);
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            planogram.MetaBayCount = Convert.ToInt32(GreaterThanGreenValue);
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        } 
        #endregion

        #region Numeric

        #region GreaterThan
        [Test]
        public void CalculateValidationData_WhenValueGreaterThanIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When GreaterThan and the metric's value is over the upper threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = GreaterThanGreenValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueGreaterThanIsYellow_ShouldReturnYellow()
        {
            const String expectation =
                "When GreaterThan and the metric's value falls between the threshold values, ResultType should return 'Yellow'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Yellow;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = GreaterThanYellowValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueGreaterThanIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When GreaterThan and the metric's value is less than the lower threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = GreaterThanRedValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        } 
        #endregion

        #region GreaterThanOrEqualTo
        [Test]
        public void CalculateValidationData_WhenValueGreaterThanOrEqualToIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When GreaterThanOrEqualTo and the metric's value is over the upper threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value, validationType: PlanogramValidationType.GreaterThanOrEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = GreaterThanGreenValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueGreaterThanOrEqualToIsYellow_ShouldReturnYellow()
        {
            const String expectation =
                "When GreaterThanOrEqualTo and the metric's value falls between the threshold values, ResultType should return 'Yellow'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Yellow;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value, validationType: PlanogramValidationType.GreaterThanOrEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = GreaterThanYellowValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueGreaterThanOrEqualToIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When GreaterThanOrEqualTo and the metric's value is less than the lower threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, GreaterThanThreshold1Value,
                    GreaterThanThreshold2Value, validationType: PlanogramValidationType.GreaterThanOrEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = GreaterThanRedValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }
        #endregion

        #region LessThan
        [Test]
        public void CalculateValidationData_WhenValueLessThanIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When GreaterThan and the metric's value is over threshold1 value1, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, LessThanThreshold1Value,
                    LessThanThreshold2Value, validationType:PlanogramValidationType.LessThan)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = LessThanGreenValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueLessThanIsYellow_ShouldReturnYellow()
        {
            const String expectation =
                "When LessThan and the metric's value falls between the threshold values, ResultType should return 'Yellow'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Yellow;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, LessThanThreshold1Value,
                    LessThanThreshold2Value, validationType:PlanogramValidationType.LessThan)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = LessThanYellowValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueLessThanIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When LessThan and the metric's value is less than the lower threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, LessThanThreshold1Value,
                    LessThanThreshold2Value, validationType:PlanogramValidationType.LessThan)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = LessThanRedValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        } 
        #endregion

        #region LessThanOrEqualTo
        [Test]
        public void CalculateValidationData_WhenValueLessThanOrEqualToIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When LessThanOrEqualTo and the metric's value is over threshold1 value1, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, LessThanThreshold1Value,
                    LessThanThreshold2Value, validationType:PlanogramValidationType.LessThanOrEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = LessThanGreenValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueLessThanOrEqualToIsYellow_ShouldReturnYellow()
        {
            const String expectation =
                "When LessThanOrEqualTo and the metric's value falls between the threshold values, ResultType should return 'Yellow'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Yellow;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, LessThanThreshold1Value,
                    LessThanThreshold2Value, validationType:PlanogramValidationType.LessThanOrEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = LessThanYellowValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueLessThanOrEqualToIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When LessThanOrEqualTo and the metric's value is less than the lower threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, LessThanThreshold1Value,
                    LessThanThreshold2Value, validationType:PlanogramValidationType.LessThanOrEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = LessThanRedValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();

            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }
        #endregion

        #region EqualTo
        [Test]
        public void CalculateValidationData_WhenValueEqualToIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When EqualTo and the metric's value is equal to threshold1 value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, EqualToThreshold1Value,
                    EqualToThreshold2Value, validationType:PlanogramValidationType.EqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = EqualToGreenValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueEqualToIsYellow_ShouldReturnYellow()
        {
            const String expectation =
                "When EqualTo and the metric's value is almost equal to threshold1 value, ResultType should return 'Yellow'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Yellow;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, EqualToThreshold1Value,
                    EqualToThreshold2Value, validationType:PlanogramValidationType.EqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = EqualToYellowValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;

            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueEqualToIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When EqualTo and the metric's value is not equal to threshold1 value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramField, EqualToThreshold1Value,
                    EqualToThreshold2Value, validationType:PlanogramValidationType.EqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            planogram.Height = EqualToRedValue;
            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        } 
        #endregion

        #region NotEqualTo
        [Test]
        public void CalculateValidationData_WhenValueNotEqualToIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When the count of metric values not equal to Citeria is the same as the threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.NotEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Criteria Name";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueNotEqualToIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When the count of metric values not equal to Citeria is not the same as the threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.NotEqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Criteria";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }  
        #endregion

        #endregion

        #region String
        [Test]
        public void CalculateValidationData_WhenValueEqualToStringIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When the count of metric values equal to Citeria is the same as the threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.EqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Criteria";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueEqualToStringIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When the count of metric values equal to Citeria is not the same as the threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.EqualTo)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Not Criteria";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueContainsIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When the count of metric values which contains the Citeria is the same as the threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.Contains)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Criteria Name";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueContainsStringIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When the count of metric values which contains the Citeria is not the same as the threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.Contains)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Other Name";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueDoesNotContainsIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When the count of metric values which does not contains the Citeria is the same as the threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.DoesNotContain)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Name";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueDoesNotContainsIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When the count of metric values which does not contains the Citeria is not the same as the threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentStringField, EqualToThreshold1Value, validationType:PlanogramValidationType.DoesNotContain)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.Name = "Criteria Name";
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        } 
        #endregion

        #region Boolean
        [Test]
        public void CalculateValidationData_WhenValueTrueIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When the count of metric values which is true is the same as the threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentBooleanField, EqualToThreshold1Value, validationType:PlanogramValidationType.True)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.IsMerchandisedTopDown = true;
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueTrueIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When the count of metric values which is true is not the same as the threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentBooleanField, EqualToThreshold1Value, validationType:PlanogramValidationType.True)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.IsMerchandisedTopDown = false;
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueFalseIsGreen_ShouldReturnGreen()
        {
            const String expectation =
                "When the count of metric values which is false is the same as the threshold value, ResultType should return 'Green'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Green;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentBooleanField, EqualToThreshold1Value, validationType:PlanogramValidationType.False)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.IsMerchandisedTopDown = false;
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void CalculateValidationData_WhenValueFalseIsRed_ShouldReturnRed()
        {
            const String expectation =
                "When the count of metric values which is false is not the same as the threshold value, ResultType should return 'Red'.";
            const PlanogramValidationTemplateResultType expected = PlanogramValidationTemplateResultType.Red;
            var testMetrics = new List<PlanogramValidationTemplateGroupMetric>
            {
                TestHelper.NewValidationMetric(_planogramComponentBooleanField, EqualToThreshold1Value, validationType:PlanogramValidationType.False)
            };
            var planogram = PlanogramValidationTemplateTestsHelper.GetTestPlanogram(testMetrics);
            var component = PlanogramComponent.NewPlanogramComponent("TestComponent",
                PlanogramComponentType.Chest, PlanogramSettings.NewPlanogramSettings());
            component.IsMerchandisedTopDown = true;
            planogram.Components.Add(component);
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());
            planogram.Components.Add(component.Copy());

            var mut = planogram.ValidationTemplate.Groups.First().Metrics.First();

            planogram.Parent.CalculateValidationData();
            var actual = mut.ResultType;
            Assert.AreEqual(expected, actual, expectation);
        } 
        #endregion

        #endregion
    }

    internal static class PlanogramValidationTemplateTestsHelper
    {
        internal static Package NewPackage(String name = "TestPackage",
            IEnumerable<Planogram> planograms = null)
        {
            var item = Package.NewPackage(1, PackageLockType.User);
            item.UserName = "ValidUserName";
            item.Name = name;
            if (planograms != null) item.Planograms.AddRange(planograms);
            return item;
        }

        internal static Planogram NewPlanogram(String name = "TestPlanogram",
            PlanogramValidationTemplate validationTemplate = null)
        {
            var item = Planogram.NewPlanogram();
            item.UserName = "ValidUserName";
            item.Name = name;
            if (validationTemplate != null) item.ValidationTemplate.LoadFrom(validationTemplate);
            return item;
        }

        internal static PlanogramValidationTemplateGroup NewValidationGroup(String name = "TestValidationGroup",
            Single threshold1 = 0, Single threshold2 = 0,
            PlanogramValidationType validationType = PlanogramValidationType.GreaterThan,
            PlanogramValidationTemplateResultType resultType = PlanogramValidationTemplateResultType.Unknown,
            IEnumerable<PlanogramValidationTemplateGroupMetric> metrics = null)
        {
            var item = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            item.Name = name;
            item.Threshold1 = threshold1;
            item.Threshold2 = threshold2;
            item.ValidationType = validationType;
            item.ResultType = resultType;
            if (metrics != null) item.Metrics.AddRange(metrics);
            return item;
        }

        internal static PlanogramValidationTemplateGroupMetric NewValidationMetric(String field = "[Test.Property]",
            Single threshold1 = 0, Single threshold2 = 0, Single score1 = 0, Single score2 = 0, Single score3 = 0, String Criteria = "Criteria",
            PlanogramValidationType validationType = PlanogramValidationType.GreaterThan,
            PlanogramValidationTemplateResultType resultType = PlanogramValidationTemplateResultType.Unknown,
            PlanogramValidationAggregationType aggregationType = PlanogramValidationAggregationType.Avg)
        {
            var item = PlanogramValidationTemplateGroupMetric.NewPlanogramValidationTemplateGroupMetric();
            item.Field = field;
            item.Threshold1 = threshold1;
            item.Threshold2 = threshold2;
            item.Score1 = score1;
            item.Score2 = score2;
            item.Score3 = score3;
            item.Criteria = Criteria;
            item.ValidationType = validationType;
            item.AggregationType = aggregationType;
            item.ResultType = resultType;
            return item;
        }

        internal static PlanogramValidationTemplate NewValidationTemplate(String name = "TestValidationTemplate",
            IEnumerable<PlanogramValidationTemplateGroup> groups = null)
        {
            var item = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            item.Name = name;
            if (groups != null) item.Groups.AddRange(groups);
            return item;
        }

        internal static Planogram GetTestPlanogram(IEnumerable<PlanogramValidationTemplateGroupMetric> testMetrics)
        {
            var package = NewPackage(planograms: new List<Planogram>
            {
                NewPlanogram(
                    validationTemplate:
                        NewValidationTemplate(groups:
                            new List<PlanogramValidationTemplateGroup>
                            {
                                NewValidationGroup(metrics:
                                    testMetrics)
                            }))
            });
            var planogram = package.Planograms.First();
            return planogram;
        }

        internal static void SetUpValidation(this PlanogramValidationTemplateGroupMetric obj, PlanogramValidationType planogramValidationType, Int32 threshold1, Int32 threshold2)
        {
            obj.ValidationType = planogramValidationType;
            obj.Threshold1 = threshold1;
            obj.Threshold2 = threshold2;
        }
    }
}