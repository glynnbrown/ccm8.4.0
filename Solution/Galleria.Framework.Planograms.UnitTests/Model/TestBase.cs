﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Initial version.
// V8-26041 : A.Kuszyk
//      Amended GetMethod for the New method on child types to ensure a method is retrieved
//      if it has been overloaded.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using NUnit.Framework;

using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal;
using Galleria.Framework.Dal;

using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.UnitTesting.TestBases;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    public abstract class TestBase
    {
        #region Fields
        private string _originalDalName; // stores the original dal name
        private string _dalName; // the dal name
        private IDalFactory _dalFactory; // the dal factory to use for testing
        #endregion

        #region Properties
        /// <summary>
        /// The dal factory to use for testing
        /// </summary>
        protected IDalFactory DalFactory
        {
            get { return _dalFactory; }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public virtual void Setup()
        {
            // store the original dal name
            _originalDalName = DalContainer.DalName;

            // generate a new dal name
            _dalName = Guid.NewGuid().ToString();

            // create a new, in memory dal factory
            _dalFactory = new Galleria.Framework.Planograms.Dal.Mock.DalFactory();

            // register this factory within the dal container
            DalContainer.RegisterFactory(_dalName, _dalFactory);

            // set this factory to be the default of the container
            DalContainer.DalName = _dalName;

            //insert the auth data for this user
            //TestDataHelper.InsertUserData(_dalFactory);

            //authenticate
            //bool authenticated = Galleria.Framework.Planograms.Security.DomainPrincipal.Authenticate();
        }

        /// <summary>
        /// Called when a test is completed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            // remove the registered dal
            DalContainer.RemoveFactory(_dalName);

            // and restore the original dal name
            DalContainer.DalName = _originalDalName;
        }

        /// <summary>
        /// Tests that a dto is serializable
        /// </summary>
        protected static void Serialize(object obj)
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
            }
        }

        /// <summary>
        /// Creates a collection of model objects of a particular type and sets all settable properties on each one
        /// except references to IDs of other model objects.  The number of model objects returned will be one more 
        /// than the number of properties set so that each one can be toggled separately, yielding a set of objects
        /// that, if successfully saved and reloaded, proves that the save and load code is working correctly for each
        /// property.
        /// </summary>
        /// <typeparam name="T">The type of model object to create.</typeparam>
        /// <param name="ids">A dictionary mapping Guid ids of objects to the integer ids they'll get when they're
        /// saved using the Mock Dal.  This method adds the ids for any objects it creates to this dictionary, based on
        /// the value of id.</param>
        /// <param name="id">The number of model objects of type T that have been created already.</param>
        /// <returns></returns>
        protected List<T> CreateModelObjects<T>(Dictionary<Object, Object> ids, ref Int32 id)
        {
            List<T> returnValue = new List<T>();
            Type type = typeof(T);

            List<PropertyInfo> properties = new List<PropertyInfo>();
            properties.AddRange(type.GetProperties().Where(p => p.CanWrite && p.PropertyType != typeof(Object)));
            //properties.Add(type.GetProperty("ExtendedData", BindingFlags.Public | BindingFlags.Instance));

            PropertyInfo idProperty = type.GetProperty("Id");

            for (Int32 i = 0; i <= properties.Count; i++)
            {
                // V8-26041
                // Changed GetMethod(String) to GetMethod(String, Type[]) so that New method with
                // no arguments is picked up when it has been overloaded.
                T t = (T)type.GetMethod(String.Format("New{0}", type.Name), Type.EmptyTypes).Invoke(null, null);
                id++;
                ids[idProperty.GetValue(t, null)] = id;
                for (Int32 j = 0; j < properties.Count; j++)
                {
                    if (i == j)
                    {
                        properties[j].SetValue(t, TestDataHelper.GetValue2(properties[j].PropertyType), null);
                    }
                    else
                    {
                        properties[j].SetValue(t, TestDataHelper.GetValue2(properties[j].PropertyType), null);
                    }
                }
                returnValue.Add(t);
            }

            return returnValue;
        }

        /// <summary>
        /// Compares two collections of model objects of a certain type.
        /// </summary>
        /// <typeparam name="T">The type of model objects we're comparing.</typeparam>
        /// <param name="collection1">A collection of model objects that haven't been saved yet.</param>
        /// <param name="collection2">A collection of model objects that have been saved.</param>
        /// <param name="ids">A dictionary mapping Guid ids of unsaved model objects to integer ids of saved model
        /// objects.  This will be used to determine whether properties that represent ids of linked objects have 
        /// "matching" values.</param>
        protected void CompareModelObjects<T>(
            IEnumerable<T> collection1,
            IEnumerable<T> collection2,
            Dictionary<Object, Object> ids)
        {
            List<T> list1 = new List<T>(collection1);
            List<T> list2 = new List<T>(collection2);
            Assert.AreEqual(list1.Count, list2.Count);
            for (Int32 i = 0; i < list1.Count; i++)
            {
                CompareModelObjects<T>(list1[i], list2[i], ids);
            }
        }

        /// <summary>
        /// Compares two model objects of a certain type.
        /// </summary>
        /// <typeparam name="T">The type of model objects we're comparing.</typeparam>
        /// <param name="collection1">A model object that hasn't been saved yet.</param>
        /// <param name="collection2">A model object that has been saved.</param>
        /// <param name="ids">A dictionary mapping Guid ids of unsaved model objects to integer ids of saved model
        /// objects.  This will be used to determine whether properties that represent ids of linked objects have 
        /// "matching" values.</param>
        protected void CompareModelObjects<T>(T object1, T object2, Dictionary<Object, Object> ids)
        {
            Type type = typeof(T);

            List<PropertyInfo> properties = new List<PropertyInfo>();
            properties.AddRange(type.GetProperties().Where(p => p.CanWrite));
            //properties.Add(type.GetProperty("ExtendedData", BindingFlags.Public | BindingFlags.Instance));

            foreach (PropertyInfo property in properties)
            {
                if ((property.Name.EndsWith("Id") || property.Name.Contains("ImageId")) &&
                    property.PropertyType == typeof(Object) &&
                    property.Name != "Mesh3DId")
                {
                    Assert.AreEqual(ids[property.GetValue(object1, null)], property.GetValue(object2, null), property.Name);
                }
                else
                {
                    Assert.AreEqual(property.GetValue(object1, null), property.GetValue(object2, null), property.Name);
                }
            }
        }
        #endregion

        #region Test Helpers

        protected static void TestPropertySetters<T>(T modelObject)
        {
            TestPropertySetters<T>(modelObject, new List<String>());
        }

        protected static void TestPropertySetters<T>(T modelObject, List<String> propertiesToExclude)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>(typeof(T).GetProperties().Where(p => p.CanWrite));
            foreach (String propertyName in propertiesToExclude)
            {
                properties.RemoveAll(p => p.Name == propertyName);
            }
            foreach (PropertyInfo property in properties)
            {
                Object value1 = TestDataHelper.GetValue1(property.PropertyType);
                property.SetValue(modelObject, value1, null);
                Assert.AreEqual(value1, property.GetValue(modelObject, null));
            }
            // OK, so setting each individual property seemed to work.  But how do we know that setting one property
            // didn't inadvertantly affect another?  Let's double-check...
            foreach (PropertyInfo property in properties)
            {
                Object value1 = TestDataHelper.GetValue1(property.PropertyType);
                Object value2 = TestDataHelper.GetValue2(property.PropertyType);
                property.SetValue(modelObject, value2, null);
                Assert.AreEqual(value2, property.GetValue(modelObject, null));
                // This property is now set to value 2--check that all other properties aren't:
                foreach (PropertyInfo otherProperty in properties)
                {
                    if (otherProperty.Name != property.Name)
                    {
                        Assert.AreNotEqual(value2, otherProperty.GetValue(modelObject, null));
                    }
                }
                // Revert back to value 1 before moving on the next property.
                property.SetValue(modelObject, value1, null);
            }
        }

        #endregion
    }


    public abstract class TestBase<TMODEL, TDTO> : ModelTestBase<TMODEL, TDTO>
        where TMODEL : ModelObject<TMODEL>
    {
        #region Fields
        private string _originalDalName; // stores the original dal name
        private string _dalName; // the dal name
        #endregion

        #region Methods
        /// <summary>
        /// Called prior to the execution of a test
        /// </summary>
        [SetUp]
        public virtual void Setup()
        {
            // store the original dal name
            _originalDalName = DalContainer.DalName;

            // generate a new dal name
            _dalName = Guid.NewGuid().ToString();

            // create a new, in memory dal factory
            base.DalFactory = new Galleria.Framework.Planograms.Dal.Mock.DalFactory();

            // register this factory within the dal container
            DalContainer.RegisterFactory(_dalName, base.DalFactory);

            // set this factory to be the default of the container
            DalContainer.DalName = _dalName;

            //insert the auth data for this user
            //TestDataHelper.InsertUserData(_dalFactory);

            //authenticate
            //bool authenticated = Galleria.Framework.Planograms.Security.DomainPrincipal.Authenticate();
        }

        /// <summary>
        /// Called when a test is completed
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            // remove the registered dal
            DalContainer.RemoveFactory(_dalName);

            // and restore the original dal name
            DalContainer.DalName = _originalDalName;
        }


        /// <summary>
        /// Creates a collection of model objects of a particular type and sets all settable properties on each one
        /// except references to IDs of other model objects.  The number of model objects returned will be one more 
        /// than the number of properties set so that each one can be toggled separately, yielding a set of objects
        /// that, if successfully saved and reloaded, proves that the save and load code is working correctly for each
        /// property.
        /// </summary>
        /// <typeparam name="T">The type of model object to create.</typeparam>
        /// <param name="ids">A dictionary mapping Guid ids of objects to the integer ids they'll get when they're
        /// saved using the Mock Dal.  This method adds the ids for any objects it creates to this dictionary, based on
        /// the value of id.</param>
        /// <param name="id">The number of model objects of type T that have been created already.</param>
        /// <returns></returns>
        protected List<T> CreateModelObjects<T>(Dictionary<Object, Object> ids, ref Int32 id)
        {
            List<T> returnValue = new List<T>();
            Type type = typeof(T);

            List<PropertyInfo> properties = new List<PropertyInfo>();
            properties.AddRange(type.GetProperties().Where(p => p.CanWrite && p.PropertyType != typeof(Object)));
            //properties.Add(type.GetProperty("ExtendedData", BindingFlags.Public | BindingFlags.Instance));

            PropertyInfo idProperty = type.GetProperty("Id");

            for (Int32 i = 0; i <= properties.Count; i++)
            {
                // V8-26041
                // Changed GetMethod(String) to GetMethod(String, Type[]) so that New method with
                // no arguments is picked up when it has been overloaded.
                T t = (T)type.GetMethod(String.Format("New{0}", type.Name), Type.EmptyTypes).Invoke(null, null);
                id++;
                ids[idProperty.GetValue(t, null)] = id;
                for (Int32 j = 0; j < properties.Count; j++)
                {
                    if (i == j)
                    {
                        properties[j].SetValue(t, TestDataHelper.GetValue2(properties[j].PropertyType), null);
                    }
                    else
                    {
                        properties[j].SetValue(t, TestDataHelper.GetValue2(properties[j].PropertyType), null);
                    }
                }
                returnValue.Add(t);
            }

            return returnValue;
        }

        /// <summary>
        /// Compares two collections of model objects of a certain type.
        /// </summary>
        /// <typeparam name="T">The type of model objects we're comparing.</typeparam>
        /// <param name="collection1">A collection of model objects that haven't been saved yet.</param>
        /// <param name="collection2">A collection of model objects that have been saved.</param>
        /// <param name="ids">A dictionary mapping Guid ids of unsaved model objects to integer ids of saved model
        /// objects.  This will be used to determine whether properties that represent ids of linked objects have 
        /// "matching" values.</param>
        protected void CompareModelObjects<T>(
            IEnumerable<T> collection1,
            IEnumerable<T> collection2,
            Dictionary<Object, Object> ids)
        {
            List<T> list1 = new List<T>(collection1);
            List<T> list2 = new List<T>(collection2);
            Assert.AreEqual(list1.Count, list2.Count);
            for (Int32 i = 0; i < list1.Count; i++)
            {
                CompareModelObjects<T>(list1[i], list2[i], ids);
            }
        }

        /// <summary>
        /// Compares two model objects of a certain type.
        /// </summary>
        /// <typeparam name="T">The type of model objects we're comparing.</typeparam>
        /// <param name="collection1">A model object that hasn't been saved yet.</param>
        /// <param name="collection2">A model object that has been saved.</param>
        /// <param name="ids">A dictionary mapping Guid ids of unsaved model objects to integer ids of saved model
        /// objects.  This will be used to determine whether properties that represent ids of linked objects have 
        /// "matching" values.</param>
        protected void CompareModelObjects<T>(T object1, T object2, Dictionary<Object, Object> ids)
        {
            Type type = typeof(T);

            List<PropertyInfo> properties = new List<PropertyInfo>();
            properties.AddRange(type.GetProperties().Where(p => p.CanWrite));
            //properties.Add(type.GetProperty("ExtendedData", BindingFlags.Public | BindingFlags.Instance));

            foreach (PropertyInfo property in properties)
            {
                if ((property.Name.EndsWith("Id") || property.Name.Contains("ImageId")) &&
                    property.PropertyType == typeof(Object) &&
                    property.Name != "Mesh3DId")
                {
                    Assert.AreEqual(ids[property.GetValue(object1, null)], property.GetValue(object2, null), property.Name);
                }
                else
                {
                    Assert.AreEqual(property.GetValue(object1, null), property.GetValue(object2, null), property.Name);
                }
            }
        }
        #endregion

    }
}
