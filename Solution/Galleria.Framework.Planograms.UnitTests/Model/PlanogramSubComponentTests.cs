﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27606 : L.Ineson
//      Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentAssertions;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramSubComponentTests : TestBase<PlanogramSubComponent, PlanogramSubComponentDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Data Access

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        [Test]
        public void CheckThatPegHolesAreGeneratedCorrectly()
        {
            PlanogramPegHoles expectedValues = new PlanogramPegHoles(new List<PointValue>()
            {
                // Col 0 - Row 1
                new PointValue(1F, 7.775F, 1F),
                new PointValue(1F, 2.775F, 1F),
                
                // Col 1 - Row 1
                new PointValue(2F, 7.775F, 1F),
                new PointValue(2F, 2.775F, 1F),

                // Col 2 - Row 1
                new PointValue(3F, 7.775F, 1F),
                new PointValue(3F, 2.775F, 1F),

                // Col 3 - Row 1
                new PointValue(4F, 7.775F, 1F),
                new PointValue(4F, 2.775F, 1F),

                // Col 4 - Row 1
                new PointValue(5F, 7.775F, 1F),
                new PointValue(5F, 2.775F, 1F),

                // Col 5 - Row 1
                new PointValue(6F, 7.775F, 1F),
                new PointValue(6F, 2.775F, 1F),

                // Col 6 - Row 1
                new PointValue(7F, 7.775F, 1F),
                new PointValue(7F, 2.775F, 1F),

                // Col 7 - Row 1
                new PointValue(8F, 7.775F, 1F),
                new PointValue(8F, 2.775F, 1F),

                // Col 8 - Row 1
                new PointValue(9F, 7.775F, 1F),
                new PointValue(9F, 2.775F, 1F),
            }, 
            new List<PointValue>()
            {
                // Col 0 - Row 2
                new PointValue(1F, 5.575F, 1F),
                new PointValue(1F, 0.275F, 1F),
                
                // Col 1 - Row 2
                new PointValue(2F, 5.575F, 1F),
                new PointValue(2F, 0.275F, 1F),

                // Col 2 - Row 2
                new PointValue(3F, 5.575F, 1F),
                new PointValue(3F, 0.275F, 1F),

                // Col 3 - Row 2
                new PointValue(4F, 5.575F, 1F),
                new PointValue(4F, 0.275F, 1F),

                // Col 4 - Row 2
                new PointValue(5F, 5.575F, 1F),
                new PointValue(5F, 0.275F, 1F),

                // Col 5 - Row 2
                new PointValue(6F, 5.575F, 1F),
                new PointValue(6F, 0.275F, 1F),

                // Col 6 - Row 2
                new PointValue(7F, 5.575F, 1F),
                new PointValue(7F, 0.275F, 1F),

                // Col 7 - Row 2
                new PointValue(8F, 5.575F, 1F),
                new PointValue(8F, 0.275F, 1F),

                // Col 8 - Row 2
                new PointValue(9F, 5.575F, 1F),
                new PointValue(9F, 0.275F, 1F),
            });

            PlanogramSubComponent componentToTest = PlanogramSubComponent.NewPlanogramSubComponent("PegBoard", PlanogramComponentType.Peg,
                10.0F, 10.0F, 1.0F, (int) ConsoleColor.Blue);

            // Row 2
            componentToTest.MerchConstraintRow1StartX = 1.0F;
            componentToTest.MerchConstraintRow1SpacingX = 1.0F;

            componentToTest.MerchConstraintRow1StartY = 2.225F;
            componentToTest.MerchConstraintRow1SpacingY = 2.5F;
            
            componentToTest.MerchConstraintRow1Height = 0.5F;
            componentToTest.MerchConstraintRow1Width = 2.5F;

            // Row 2
            componentToTest.MerchConstraintRow2StartX = 1.0F;
            componentToTest.MerchConstraintRow2SpacingX = 1.0F;

            componentToTest.MerchConstraintRow2StartY = 0.975F;
            componentToTest.MerchConstraintRow2SpacingY = 2.5F;

            componentToTest.MerchConstraintRow2Height = 0.5F;
            componentToTest.MerchConstraintRow2Width = 2.5F;

            var generatedPegs = componentToTest.GetPegHoles();

            generatedPegs.Row1.Should().BeEquivalentTo(expectedValues.Row1, "Invalid Row 1 position generated");
            generatedPegs.Row2.Should().BeEquivalentTo(expectedValues.Row2, "Invalid Row 2 position generated");

            Assert.That(generatedPegs, Is.EqualTo(expectedValues), "Invalid peg positions generated");
        }

        #endregion
    }
}
