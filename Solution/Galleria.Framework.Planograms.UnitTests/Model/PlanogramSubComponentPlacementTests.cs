﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM v8.0)

// V8-28613 : A.Silva
//      Created

#endregion

#region Version History: CCM803

// V8-29589 : A.Silva
//  Added Unit Tests for GetCombinedWithList.

#endregion

#region Version History : CCM8111

// V8-30334 : A.Silva
//  Updated usage of GetWorldMerchandisingSpaceBounds in tests using it, so that a prefetched collection of sub component placements is used for the planogram.

#endregion

#endregion

using System;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramSubComponentPlacementTests
    {
        // V8-28484 : This behaviour has been changed so that the merch space of a hang component is just the front face of the component.
        //[Test]
        //public void GetWorldMerchandisingSpaceBounds_WhenHangNoSetHeightNoCollision_ShouldSetHeightToFixture()
        //{
        //    // Setup a test planogram.
        //    var planogram = Planogram.NewPlanogram();
        //    var fixtureItem = NewFixtureItem(planogram);
        //    var barComponent = NewBarComponent(fixtureItem);
        //    var planogramComponent = barComponent.GetPlanogramComponent();
        //    var placement = NewPosition(1, NewProduct(planogram), barComponent).GetPlanogramSubComponentPlacement();
        //    var expectedX = barComponent.X; // X origin as the component X itself.
        //    var expectedY = fixtureItem.Y; // Y origin on the fixture Y itself.
        //    var expectedZ = barComponent.Z + planogramComponent.Depth; // Z origin on the front face of the component.
        //    var expectedWidth = planogramComponent.Width; // As wide as the component.
        //    var expectedHeight = barComponent.Y; // As tall as the height the component is placed at.
        //    var expectedDepth = fixtureItem.GetPlanogramFixture().Depth - planogramComponent.Depth; // As deep as from the front of the component to the limit of the fixture.

        //    var actual = placement.GetWorldMerchandisingSpaceBounds(false);

        //    Assert.AreEqual(expectedX, actual.X, "X differ.");
        //    Debug.Print("Expected X: {0}", expectedX);
        //    Assert.AreEqual(expectedY, actual.Y, "Y differ.");
        //    Debug.Print("Expected Y: {0}", expectedY);
        //    Assert.AreEqual(expectedZ, actual.Z, "Z differ.");
        //    Debug.Print("Expected Z: {0}", expectedZ);
        //    Assert.AreEqual(expectedWidth, actual.Width, "Width differ.");
        //    Debug.Print("Expected Width: {0}", expectedWidth);
        //    Assert.AreEqual(expectedHeight, actual.Height, "Height differ");
        //    Debug.Print("Expected Height: {0}", expectedHeight);
        //    Assert.AreEqual(expectedDepth, actual.Depth, "Depth differ");
        //    Debug.Print("Expected Depth: {0}", expectedDepth);
        //}

        [Test]
        public void GetWorldMerchandisingSpaceBounds_WhenHangNoSetHeightWithCollision_ShouldSetHeightToCollision()
        {
            Assert.Inconclusive("Review to see if this is what is intended");
            // Setup a test planogram.
            var planogram = Planogram.NewPlanogram();
            var fixtureItem = NewFixtureItem(planogram);
            var barComponent = NewBarComponent(fixtureItem);
            var planogramComponent = barComponent.GetPlanogramComponent();
            var placement = NewPosition(1, NewProduct(planogram), barComponent).GetPlanogramSubComponentPlacement();
            var collisionComponent = NewBarComponent(fixtureItem);
            collisionComponent.Y = barComponent.Y - 10;
            var expectedX = barComponent.X; // X origin as the component X itself.
            var expectedY = collisionComponent.Y + collisionComponent.GetPlanogramComponent().Height; // Y origin on the colliding component top face.
            var expectedZ = barComponent.Z + planogramComponent.Depth; // Z origin on the front face of the component.
            var expectedWidth = planogramComponent.Width; // As wide as the component.
            var expectedHeight = barComponent.Y - expectedY; // As tall as the distance to the top face of the colliding component.
            var expectedDepth = fixtureItem.GetPlanogramFixture().Depth - planogramComponent.Depth; // As deep as from the front of the component to the limit of the fixture.

            var actual = placement.GetWorldMerchandisingSpaceBounds(false, planogram.GetPlanogramSubComponentPlacements());

            Assert.AreEqual(expectedX, actual.X, "X differ.");
            Assert.AreEqual(expectedY, actual.Y, "Y differ.");
            Assert.AreEqual(expectedZ, actual.Z, "Z differ.");
            Assert.AreEqual(expectedWidth, actual.Width, "Width differ.");
            Assert.AreEqual(expectedHeight, actual.Height, "Height differ");
            Assert.AreEqual(expectedDepth, actual.Depth, "Depth differ");
        }

        [Test]
        public void GetWorldMerchandisingSpaceBounds_WhenHangFromBottomNoSetHeightNoCollision_ShouldSetHeightToFixture()
        {
            // Setup a test planogram.
            var planogram = Planogram.NewPlanogram();
            var fixtureItem = NewFixtureItem(planogram);
            var rodComponent = NewRodComponent(fixtureItem);
            var planogramComponent = rodComponent.GetPlanogramComponent();
            var placement = NewPosition(1, NewProduct(planogram), rodComponent).GetPlanogramSubComponentPlacement();
            var expectedX = rodComponent.X; // X origin as the component X itself.
            var expectedY = fixtureItem.Y; // Y origin on the fixture Y itself.
            var expectedZ = rodComponent.Z; // Z origin on the back face of the component.
            var expectedWidth = planogramComponent.Width; // As wide as the component.
            var expectedHeight = rodComponent.Y; // As tall as the height the component is placed at.
            var expectedDepth = planogramComponent.Depth; // As deep as the component.

            var actual = placement.GetWorldMerchandisingSpaceBounds(false, planogram.GetPlanogramSubComponentPlacements());

            Assert.AreEqual(expectedX, actual.X, "X differ.");
            Debug.Print("Expected X: {0}", expectedX);
            Assert.AreEqual(expectedY, actual.Y, "Y differ.");
            Debug.Print("Expected Y: {0}", expectedY);
            Assert.AreEqual(expectedZ, actual.Z, "Z differ.");
            Debug.Print("Expected Z: {0}", expectedZ);
            Assert.AreEqual(expectedWidth, actual.Width, "Width differ.");
            Debug.Print("Expected Width: {0}", expectedWidth);
            Assert.AreEqual(expectedHeight, actual.Height, "Height differ");
            Debug.Print("Expected Height: {0}", expectedHeight);
            Assert.AreEqual(expectedDepth, actual.Depth, "Depth differ");
            Debug.Print("Expected Depth: {0}", expectedDepth);
        }

        [Test]
        public void GetWorldMerchandisingSpaceBounds_WhenHangFromBottomNoSetHeightWithCollision_ShouldSetHeightToCollision()
        {
            // Setup a test planogram.
            var planogram = Planogram.NewPlanogram();
            var fixtureItem = NewFixtureItem(planogram);
            var rodComponent = NewRodComponent(fixtureItem);
            var planogramComponent = rodComponent.GetPlanogramComponent();
            var placement = NewPosition(1, NewProduct(planogram), rodComponent).GetPlanogramSubComponentPlacement();
            var collisionComponent = NewShelfComponent(fixtureItem);
            collisionComponent.Y = rodComponent.Y - 10;
            var expectedX = rodComponent.X; // X origin as the component X itself.
            var expectedY = collisionComponent.Y + collisionComponent.GetPlanogramComponent().Height; // Y origin on the colliding component top face.
            var expectedZ = rodComponent.Z; // Z origin on the back face of the component.
            var expectedWidth = planogramComponent.Width; // As wide as the component.
            var expectedHeight = rodComponent.Y - expectedY; // As tall as the distance to the top face of the colliding component.
            var expectedDepth = planogramComponent.Depth; // As deep as the component.

            var actual = placement.GetWorldMerchandisingSpaceBounds(false, planogram.GetPlanogramSubComponentPlacements());

            Assert.AreEqual(expectedX, actual.X, "X differ.");
            Debug.Print("Expected X: {0}", expectedX);
            Assert.AreEqual(expectedY, actual.Y, "Y differ.");
            Debug.Print("Expected Y: {0}", expectedY);
            Assert.AreEqual(expectedZ, actual.Z, "Z differ.");
            Debug.Print("Expected Z: {0}", expectedZ);
            Assert.AreEqual(expectedWidth, actual.Width, "Width differ.");
            Debug.Print("Expected Width: {0}", expectedWidth);
            Assert.AreEqual(expectedHeight, actual.Height, "Height differ");
            Debug.Print("Expected Height: {0}", expectedHeight);
            Assert.AreEqual(expectedDepth, actual.Depth, "Depth differ");
            Debug.Print("Expected Depth: {0}", expectedDepth);
        }

        #region GetCombinedWithList

        [Test]
        public void GetCombineWithList_WhenCalledFromAnyInCombined_ShouldGiveSameResults()
        {
            Planogram planogram = Planogram.NewPlanogram();
            for (var bayCount = 0; bayCount < 3; bayCount++)
            {
                PlanogramFixtureItem bay = planogram.AddFixtureItem();
                for (var shelfCount = 0; shelfCount < 3; shelfCount++)
                {
                    Int32 y = 20*(shelfCount + 1);
                    PlanogramFixtureComponent component = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, y, 0));
                    PlanogramSubComponent subComponent = component.GetPlanogramComponent().SubComponents.First();
                    subComponent.CombineType = PlanogramSubComponentCombineType.Both;
                    subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
                }
            }
            var placementCoordinatesByYOrderedByX = planogram.GetPlanogramSubComponentPlacements()
                                                             .Select(
                                                                 p =>
                                                                 new Tuple<PlanogramSubComponentPlacement, PointValue>(
                                                                     p,
                                                                     p.GetPlanogramRelativeCoordinates()))
                                                             .OrderBy(p => p.Item2.X)
                                                             .GroupBy(p => p.Item2.Y);
            foreach (var item in placementCoordinatesByYOrderedByX)
            {
                CollectionAssert.AreEquivalent(item.First().Item1.GetCombinedWithList(), item.Last().Item1.GetCombinedWithList());
            }
        }

        [Test]
        public void GetCombineWithList_WhenCalledFromAnyInCombinedAndDifferentSize_ShouldGiveSameResults()
        {
            Planogram planogram = Planogram.NewPlanogram();
            for (var bayCount = 0; bayCount < 3; bayCount++)
            {
                PlanogramFixtureItem bay = planogram.AddFixtureItem();
                for (var shelfCount = 0; shelfCount < 3; shelfCount++)
                {
                    Int32 y = 20 * (shelfCount + 1);
                    PlanogramFixtureComponent component = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, y, 0));
                    PlanogramSubComponent subComponent = component.GetPlanogramComponent().SubComponents.First();
                    subComponent.CombineType = PlanogramSubComponentCombineType.Both;
                    subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
                }
                bay.GetPlanogramSubComponentPlacements().Last().Component.Width = 60;
            }
            var placementCoordinatesByYOrderedByX = planogram.GetPlanogramSubComponentPlacements()
                                                             .Select(
                                                                 p =>
                                                                 new Tuple<PlanogramSubComponentPlacement, PointValue>(
                                                                     p,
                                                                     p.GetPlanogramRelativeCoordinates()))
                                                             .OrderBy(p => p.Item2.X)
                                                             .GroupBy(p => p.Item2.Y);
            foreach (var item in placementCoordinatesByYOrderedByX)
            {
                CollectionAssert.AreEquivalent(item.First().Item1.GetCombinedWithList(), item.Last().Item1.GetCombinedWithList());
            }
        }

        #endregion

        #region Test Helper Methods

        private static PlanogramPosition NewPosition(Int16 sequence, PlanogramProduct product,
            PlanogramFixtureComponent component)
        {
            var subComponent = component.GetPlanogramComponent().SubComponents.First();
            var planogramFixture = component.Parent;
            var planogram = planogramFixture.Parent;
            var fixtureItem = planogram.FixtureItems.FirstOrDefault(o => o.PlanogramFixtureId == planogramFixture.Id);
            var planogramSubComponentPlacement =
                PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(subComponent, fixtureItem, component);
            var position = PlanogramPosition.NewPlanogramPosition(sequence, product, planogramSubComponentPlacement);
            planogram.Positions.Add(position);
            return position;
        }

        private static PlanogramFixtureComponent NewBarComponent(PlanogramFixtureItem fixtureItem)
        {
            var fixture = fixtureItem.GetPlanogramFixture();
            var planogramComponent = PlanogramComponent.NewPlanogramComponent("Bar", PlanogramComponentType.Bar,
                fixture.Width, 2, 2);
            fixture.Parent.Components.Add(planogramComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogramComponent);
            fixture.Components.Add(fixtureComponent);
            fixtureComponent.Y = fixture.Height / 2;
            return fixtureComponent;
        }

        private static PlanogramFixtureComponent NewRodComponent(PlanogramFixtureItem fixtureItem)
        {
            var fixture = fixtureItem.GetPlanogramFixture();
            var planogramComponent = PlanogramComponent.NewPlanogramComponent("Rod", PlanogramComponentType.Rod,
                2, 2, fixture.Depth);
            fixture.Parent.Components.Add(planogramComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogramComponent);
            fixture.Components.Add(fixtureComponent);
            fixtureComponent.X = fixture.Width / 2;
            fixtureComponent.Y = fixture.Height / 2;
            return fixtureComponent;
        }

        private static PlanogramFixtureComponent NewShelfComponent(PlanogramFixtureItem fixtureItem)
        {
            var fixture = fixtureItem.GetPlanogramFixture();
            var planogramComponent = PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf,
                fixture.Width, 4, fixture.Depth);
            fixture.Parent.Components.Add(planogramComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogramComponent);
            fixture.Components.Add(fixtureComponent);
            fixtureComponent.Y = fixture.Height / 2;
            return fixtureComponent;
        }

        private static PlanogramFixtureItem NewFixtureItem(Planogram testPlanogram)
        {
            var planogramFixture = PlanogramFixture.NewPlanogramFixture();
            planogramFixture.Width = 120;
            planogramFixture.Height = 200;
            planogramFixture.Depth = 75;
            testPlanogram.Fixtures.Add(planogramFixture);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(planogramFixture);
            testPlanogram.FixtureItems.Add(fixtureItem);
            return fixtureItem;
        }

        private static PlanogramProduct NewProduct(Planogram planogram)
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Height = 10;
            product.Width = 5;
            product.Depth = 2;
            planogram.Products.Add(product);
            return product;
        }

        #endregion

    }
}