﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.

#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Security;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramValidationTemplateTests : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramValidationTemplate.NewPlanogramValidationTemplate());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewValidationTemplate()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = "Name";
            package.Planograms[0].ValidationTemplate.Name = newPlanogramValidationTemplate.Name;
            package = package.Save();

            newPlanogramValidationTemplate = planogram.ValidationTemplate;

            //check if validation template is new and is child
            Assert.AreEqual(newPlanogramValidationTemplate.Parent.Id, planogram.Id);
            Assert.IsTrue(newPlanogramValidationTemplate.IsNew);
            Assert.IsTrue(newPlanogramValidationTemplate.IsChild, "A newly created Planogram Validation Template should be a child object.");
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = "Name";
            package.Planograms[0].ValidationTemplate.Name = newPlanogramValidationTemplate.Name;
            package = package.Save();
            newPlanogramValidationTemplate = planogram.ValidationTemplate;

            //fetch
            Planograms.Model.Package packageModel =
                Planograms.Model.Package.FetchById(package.Id, 1, PackageLockType.User);
            PlanogramValidationTemplate model = packageModel.Planograms[0].ValidationTemplate;
            Assert.AreEqual(newPlanogramValidationTemplate.Name, packageModel.Planograms[0].ValidationTemplate.Name);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            //create new validation template
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            package = package.Save();

            //check if Validation is not null
            Assert.IsNotNull(newPlanogramValidationTemplate);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            String check = "Name";

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = check;
            package.Planograms[0].ValidationTemplate.Name = newPlanogramValidationTemplate.Name;

            // save
            package = package.Save();
            newPlanogramValidationTemplate = planogram.ValidationTemplate;
            PackageDto dto;

            // fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check first value is equal
            Assert.AreEqual(newPlanogramValidationTemplate.Name, package.Planograms[0].ValidationTemplate.Name);

            // change value
            check = "Name2";
            newPlanogramValidationTemplate.Name = check;
            package.Planograms[0].ValidationTemplate.Name = newPlanogramValidationTemplate.Name;
            package = package.Save();
            newPlanogramValidationTemplate = planogram.ValidationTemplate;

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if value has changed
            Assert.AreEqual(check, package.Planograms[0].ValidationTemplate.Name);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            var newPlanogramValidationTemplate = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            newPlanogramValidationTemplate.Name = "Name";
            package.Planograms[0].ValidationTemplate.Name = newPlanogramValidationTemplate.Name;
            package = package.Save();
            newPlanogramValidationTemplate = planogram.ValidationTemplate;
            PackageDto dto;

            // get dto
            var model = package.Planograms[0];
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // remove planogram 
            package.Planograms.Remove(model);
            package = package.Save();

            // consumer decision tree dto should be null
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            Assert.AreEqual(0, package.Planograms.Count);

        }

        #endregion

        #region IValidationTemplate Members

        [Test]
        public void IValidationTemplateGroups_GetAccessor_GetsGroups()
        {
            const string expectation = "When getting Groups through the interface the actual groups are returned.";
            var testmodel = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            var expected = testmodel.Groups;
            expected.Add(PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup());

            var actual = ((IValidationTemplate) testmodel).Groups;

            CollectionAssert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void IValidationTemplateAddNewGroup_Invoked_AddsNewGroup()
        {
            const string expectation = "When AddNewGroup is invoked the Groups collection gets a new group.";
            var testModel = PlanogramValidationTemplate.NewPlanogramValidationTemplate();

            ((IValidationTemplate) testModel).AddNewGroup("TEST");

            var actual = testModel.Groups.Any();
            Assert.IsTrue(actual,expectation);
        }

        [Test]
        public void IValidationTemplateRemoveGroup_ContainsGroup_RemovesGroup()
        {
            const string expectation = "When RemoveGroup is invoked with an existing group the groups collection removes it.";
            var testModel = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            var itemToRemove = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            testModel.Groups.Add(itemToRemove);

            ((IValidationTemplate)testModel).RemoveGroup(itemToRemove);

            var actual = !testModel.Groups.Contains(itemToRemove);
            Assert.IsTrue(actual,expectation);
        }

        [Test]
        public void IValidationTemplateRemoveGroup_DoesNotContainGroup_DoesNotChangeGroups()
        {
            const string expectation = "When RemoveGroup is invoked with a non existing group the groups collection remains unchanged.";
            var testModel = PlanogramValidationTemplate.NewPlanogramValidationTemplate();
            var itemToRemove = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();
            testModel.Groups.Add(itemToRemove);
            var unrelatedItem = PlanogramValidationTemplateGroup.NewPlanogramValidationTemplateGroup();

            ((IValidationTemplate)testModel).RemoveGroup(unrelatedItem);

            var actual = testModel.Groups.Contains(itemToRemove);
            Assert.IsTrue(actual, expectation);
        }

        #endregion

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        #endregion

    }
}
