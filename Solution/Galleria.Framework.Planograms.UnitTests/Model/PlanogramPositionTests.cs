﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM800

// V8-27606 : L.Ineson
//  Created
// V8-27926 : A.Silva
//  Added IsMaxCapExceeded tests.
// V8-27924 : A.Silva
//  Added IsRightMaxCapExceeded and IsTopMaxCapExceeded tests.
// V8-27780 : A.Kuszyk
//  Added basic FillHigh tests.

#endregion

#region Version History : CCM801

// V8-28767 : A.Kuszyk
//  Added tests for Increase/Decrease facings methods.

#endregion

#region Version History : CCM803

// V8-28766 : J.Pickup
//  Added Increase/decrease unit tests.

#endregion

#region Version History : CCM810

// V8-30103 : N.Foster
//  Autofill performance enhancements
// V8-30118 : A.Silva
//  Updated Increase tests for tray cases, as the merchandising style should be Default at the end, not Tray.

#endregion

#region Version History : CCM 820
// V8-31004 : D.Pleasance
//	Added test for IPlanogramSubComponentSequencedItem implementation.
#endregion

#region Version History: CCM 830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-32787 : A.Silva
//  Added tests for Increase/Decrease/Set Units with broken rules.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using NUnit.Framework;
using FluentAssertions;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Helpers;
using Moq;
using Galleria.Ccm.Security;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public sealed class PlanogramPositionTests : TestBase<PlanogramPosition, PlanogramPositionDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        [Test, Category(Categories.Smoke)]
        public void MetaIsOutsideMerchandisingSpace_FalseWhenOnCombined()
        {
            //CCM-13580 - Client raised.
            // Checks that when a product is placed centrally over the edge of 2 combined shelves,
            // it does not erroneously flag up as outside of merch space.


            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add 2 bays
            PlanogramFixtureItem fixtureItem1 = plan.AddBayWithBackboardAndBase(76, 48, 24, 3.15F, 5.5F);
            PlanogramFixtureItem fixtureItem2 = plan.AddBayWithBackboardAndBase(76, 48, 24, 3.15F, 5.5F);
            fixtureItem2.X = 48;

            //Add a shelf to both
            PlanogramSubComponentPlacement shelf1 = fixtureItem1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 40, 0), new WidthHeightDepthValue(48, 1.57F, 16F)).GetPlanogramSubComponentPlacements().First();
            shelf1.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf1.SubComponent.CombineType = PlanogramSubComponentCombineType.Both;

            PlanogramSubComponentPlacement shelf2 = fixtureItem2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 40, 0), new WidthHeightDepthValue(48, 1.57F, 16F)).GetPlanogramSubComponentPlacements().First();
            shelf2.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            shelf2.SubComponent.CombineType = PlanogramSubComponentCombineType.Both;

            //add a product to shelf 1.
            PlanogramPosition pos1 = shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(3.98F, 2.64F, 2.64F)));
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 10;
            pos1.FacingsDeep = 1;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;

            //remerch
            plan.ReprocessAllMerchandisingGroups();

            //now recalc metadata
            plan.CalculateMetadataAndGetValidationWarnings(false, null);

            //check the flag is not set
            Assert.AreEqual(28.1F, pos1.MetaWorldX, "Placement should be central over the 2 shelves.");
            Assert.IsFalse(pos1.MetaIsOutsideMerchandisingSpace.Value, "Should not be flagged as outside of merch space.");
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Data Access

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

     
        #endregion

        #region Methods

        #region IsMaxCapExceeded

        [Test]
        public void IsMaxRightCapExceeded_WhenMaxCap0AndCap1_ShouldReturnTrue()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxRightCap = 0;
            testModel.FacingsXWide = 1;
            testModel.FacingsXHigh = 1;
            testModel.FacingsXDeep = 1;
            Boolean actual = testModel.IsMaxRightCapExceeded(testProduct);

            Assert.IsTrue(actual, "Product has XCap = 1,1,1 and max right cap = 0. Max right cap does exceed.");
        }

        [Test]
        public void IsMaxRightCapExceeded_WhenMaxCap1AndCap1_ShouldReturnFalse()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxRightCap = 1;
            testModel.FacingsXWide = 1;
            testModel.FacingsXHigh = 1;
            testModel.FacingsXDeep = 1;
            Boolean actual = testModel.IsMaxRightCapExceeded(testProduct);

            Assert.IsFalse(actual, "Product has XCap = 1,1,1 and max right cap = 1. Max right cap does not exceed.");
        }

        [Test]
        public void IsMaxRightCapExceeded_WhenMaxCap1AndCapHasOne0_ShouldReturnFalse()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxRightCap = 1;
            testModel.FacingsXWide = 1;
            testModel.FacingsXHigh = 1;
            testModel.FacingsXDeep = 0;
            Boolean actual = testModel.IsMaxRightCapExceeded(testProduct);

            Assert.IsFalse(actual, "Product has Cap(x,y,z) = 1,1,0 and max right cap = 1. Max right cap does not exceed.");
        }

        [Test]
        public void IsMaxRightCapExceeded_WhenMaxCap1AndCapHasOtherAxisFacingsOver1_ShouldReturnFalse()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxRightCap = 1;
            testModel.FacingsXWide = 1;
            testModel.FacingsXHigh = 1;
            testModel.FacingsXDeep = 5;
            Boolean actual = testModel.IsMaxRightCapExceeded(testProduct);

            Assert.IsFalse(actual, "Product has Cap(x,y,z) = 1,1,5 and max right cap = 1. Max right cap does not exceed.");
        }

        [Test]
        public void IsMaxRightCapExceeded_WhenMaxCap1AndCapHasMoreUnitsThanXFacings_ShouldReturnFalse()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxRightCap = 1;
            testModel.FacingsXWide = 1;
            testModel.FacingsXHigh = 5;
            testModel.FacingsXDeep = 5;
            Boolean actual = testModel.IsMaxRightCapExceeded(testProduct);

            Assert.IsFalse(actual, "Product has RightCap(x,y,z) = 1,5,5 and max right cap = 1. Max right cap does not exceed.");
        }

        [Test]
        public void IsMaxRightCapExceeded_WhenExceededOnTheRight_ShouldReturnTrue()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxRightCap = 1;
            testModel.FacingsXWide = 2;
            testModel.FacingsXHigh = 1;
            testModel.FacingsXDeep = 1;
            Boolean actual = testModel.IsMaxRightCapExceeded(testProduct);

            Assert.IsTrue(actual, "Product has RightCap(x,y,z) = 2,1,1 and max right cap = 1. Max right cap does exceed.");
        }

        [Test]
        public void IsMaxRightCapExceeded_WhenExceededOnTheLeft_ShouldReturnTrue()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxRightCap = 1;
            testModel.FacingsXDeep = testModel.FacingsXHigh = testModel.FacingsXWide = 1;
            testModel.FacingsXWide = 2;
            testModel.IsXPlacedLeft = true;
            Boolean actual = testModel.IsMaxRightCapExceeded(testProduct);

            Assert.IsTrue(actual, "Product has Cap(x,y,z) = 2,1,1 (on the left) and max right cap = 1. Max right cap does exceed.");
        }
        
        [Test]
        public void IsMaxTopCapExceeded_WhenExceededTop_ShouldReturnTrue()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxTopCap = 1;
            testModel.FacingsYWide = 1;
            testModel.FacingsYHigh = 2;
            testModel.FacingsYDeep = 1;
            Boolean actual = testModel.IsMaxTopCapExceeded(testProduct);

            Assert.IsTrue(actual, "Product has TopCap(x,y,z) = 1,2,1 and max top cap = 1. Max top cap does exceed.");
        }

        [Test]
        public void IsMaxTopCapExceeded_WhenExceededBottom_ShouldReturnTrue()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramProduct testProduct = testModel.GetPlanogramProduct();

            testProduct.MaxTopCap = 1;
            testModel.FacingsYWide = 1;
            testModel.FacingsYHigh = 2;
            testModel.FacingsYDeep = 1;
            testModel.IsYPlacedBottom = true;
            Boolean actual = testModel.IsMaxTopCapExceeded(testProduct);

            Assert.IsTrue(actual, "Product has TopCap(x,y,z) = 1,2,1 (on the bottom) and max top cap = 1. Max top cap does exceed.");
        }

        #endregion

        #region FillHigh

        [Test]
        public void FillHigh_FillsMainBlock()
        {
            var planogram = CreatePlanogram();
            var product = CreateProduct(planogram);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());
            var merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            var position = CreatePosition(planogram, product, subCompPlacement, 1,1,1,1,1,1,1,1,1,1,1,1);

            position.FillHigh(product, subCompPlacement, merchGroup, 10 * product.Height);

            Assert.AreEqual(10, position.FacingsHigh);
        }

        [Test]
        public void FillHigh_FillsXBlock()
        {
            var planogram = CreatePlanogram();
            var product = CreateProduct(planogram);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());
            var merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            var position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

            position.FillHigh(product, subCompPlacement, merchGroup, 10 * product.Height);

            Assert.AreEqual(10, position.FacingsXHigh);
        }

        [Test]
        public void FillHigh_FillsZBlock()
        {
            var planogram = CreatePlanogram();
            var product = CreateProduct(planogram);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());
            var merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            var position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

            position.FillHigh(product, subCompPlacement, merchGroup, 10 * product.Height);

            Assert.AreEqual(10, position.FacingsZHigh);
        }
        #endregion

        #region HasCollisions

        [Test]
        public void HasCollisions_PositionInChest()
        {
            //V8-28422 - check that positions in chests do not flag as having a collision

            //create a plan with 1 position in a chest
            Package package = Package.NewPackage(1, PackageLockType.Unknown);
            Planogram planogram = Planogram.NewPlanogram();
            package.Planograms.Add(planogram);
            var component = PlanogramComponent.NewPlanogramComponent("Chest", PlanogramComponentType.Chest, 120, 60, 75);
            planogram.Components.Add(component);

            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 200;
            fixture.Depth = 75;
            fixture.Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(component));
            planogram.Fixtures.Add(fixture);
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture));

             var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            var prod = CreateProduct(planogram);
            var position = CreatePosition(planogram, prod, subCompPlacement, 1, 1, 1);

            //place the position as left back
            position.X = subCompPlacement.SubComponent.FaceThicknessLeft;
            position.Y = subCompPlacement.SubComponent.FaceThicknessBottom;
            position.Z = subCompPlacement.SubComponent.FaceThicknessBack;
            Assert.IsFalse(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Position should not collide with chest.");

            //put pos in left wall
            position.X = 0;
            Assert.IsTrue(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Position is colliding with left wall");

            //turn off left wall
            subCompPlacement.SubComponent.FaceThicknessLeft = 0;
            Assert.IsFalse(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Chest has no left wall");

            subCompPlacement.SubComponent.FaceThicknessLeft = 1;

            //put pos in right wall
            position.X = subCompPlacement.SubComponent.Width - position.GetPositionDetails().TotalSize.Width;
            Assert.IsTrue(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Position is colliding with right wall");

            //turn off right wall
            subCompPlacement.SubComponent.FaceThicknessRight = 0;
            Assert.IsFalse(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Chest has no right wall");
            
            subCompPlacement.SubComponent.FaceThicknessRight = 1;
            position.X = subCompPlacement.SubComponent.FaceThicknessLeft;

            //put pos in back wall
            position.Z = 0;
            Assert.IsTrue(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Position is colliding with back wall");

            //turn off back wall
            subCompPlacement.SubComponent.FaceThicknessBack = 0;
            Assert.IsFalse(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Chest has no back wall");

            subCompPlacement.SubComponent.FaceThicknessBack = 1;
            position.Z = subCompPlacement.SubComponent.FaceThicknessBack;

            //put pos in front wall
            position.Z = subCompPlacement.SubComponent.Depth - subCompPlacement.SubComponent.FaceThicknessFront;
            Assert.IsTrue(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Position is colliding with front wall");

            //turn off front wall
            subCompPlacement.SubComponent.FaceThicknessFront = 0;
            Assert.IsFalse(position.HasCollisions(PlanogramMetadataDetails.NewPlanogramMetadataDetails(planogram, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package))), "Chest has no front wall");
        }

        #endregion

        #region IncreaseFacings

        #region In X
        [Test]
        public void IncreaseFacings_InX_IncreasesMainBlock()
        {
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide);
        }

        [Test]
        public void IncreaseFacings_InX_IncreasesYBlock_WhenSufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Right0;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual((width * 2) / depth, position.FacingsYWide);
        }

        [Test]
        public void IncreaseFacings_InX_DoesNotIncreaseYBlock_WhenInsufficentSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Front90;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsYWide);
        }

        [Test]
        public void IncreaseFacings_InX_IncreasesZBlock_WhenSufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1);
            position.OrientationTypeZ = PlanogramPositionOrientationType.Right0;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual((width * 2) / depth, position.FacingsZWide);
        }

        [Test]
        public void IncreaseFacings_InX_DoesNotIncreaseZBlock_WhenInsufficentSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1);
            position.OrientationTypeZ = PlanogramPositionOrientationType.Front90;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsZWide);
        } 
        #endregion

        // In Y can't be tested yet, because subcomponents can't have a Y facing axis. (16/01/15, 801)

        #region In Z
        [Test]
        public void IncreaseFacings_InZ_IncreasesMainBlock()
        {
            Planogram planogram = CreatePlanogram(chest:true);
            PlanogramProduct product = CreateProduct(planogram);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsDeep);
        }

        [Test]
        public void IncreaseFacings_InZ_IncreasesXBlock_WhenSufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 10;

            Planogram planogram = CreatePlanogram(chest:true);
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0);
            position.OrientationTypeX = PlanogramPositionOrientationType.Right0;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual((depth * 2) / width, position.FacingsXDeep);
        }

        [Test]
        public void IncreaseFacings_InZ_DoesNotIncreaseXBlock_WhenInsufficentSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 4;

            Planogram planogram = CreatePlanogram(chest:true);
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0);
            position.OrientationTypeX = PlanogramPositionOrientationType.Right0;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsXDeep);
        }

        [Test]
        public void IncreaseFacings_InZ_IncreasesYBlock_WhenSufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram(chest:true);
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 0,0,0,1,1,1, 0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Front0;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual((depth * 2) / depth, position.FacingsYDeep);
        }

        [Test]
        public void IncreaseFacings_InZ_DoesNotIncreaseYBlock_WhenInsufficentSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 4;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1, 0, 0, 0, 1,1,1,0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Right0;

            position.IncreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsYDeep);
        }
        #endregion

        #endregion

        #region DecreaseFacings

        #region In X
        [Test]
        public void DecreaseFacings_InX_DecreasesMainBlock()
        {
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 2, 1, 1);

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsWide);
        }

        [Test]
        public void DecreaseFacings_InX_DoesNotDecreaseYBlock_WhenSufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 2, 1, 1, 0, 0, 0, 2, 1, 1, 0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Right0;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsYWide);
        }

        [Test]
        public void DecreaseFacings_InX_DecreasesYBlock_WhenInsufficentSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 2, 1, 1, 0, 0, 0, 2, 1, 1, 0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Front0;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsYWide);
        }

        [Test]
        public void DecreaseFacings_InX_DoesNotDecreaseZBlock_WhenSufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 2, 1, 1, 0, 0, 0, 0, 0, 0, 2, 1, 1);
            position.OrientationTypeZ = PlanogramPositionOrientationType.Right0;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsZWide);
        }

        [Test]
        public void DecreaseFacings_InX_DecreasesZBlock_WhenInsufficentSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 2, 0, 0, 0, 0, 0, 0, 1, 1, 2);
            position.OrientationTypeZ = PlanogramPositionOrientationType.Front90;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsZWide);
        }
        #endregion

        // In Y can't be tested yet, because subcomponents can't have a Y facing axis. (16/01/15, 801)

        #region In Z
        [Test]
        public void DecreaseFacings_InZ_DecreasesMainBlock()
        {
            Planogram planogram = CreatePlanogram(chest: true);
            PlanogramProduct product = CreateProduct(planogram);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 2);

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsDeep);
        }

        [Test]
        public void DecreaseFacings_InZ_DecreasesXBlock_WhenInsufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 10;

            Planogram planogram = CreatePlanogram(chest: true);
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 2, 1, 1, 2, 0, 0, 0, 0, 0, 0);
            position.OrientationTypeX = PlanogramPositionOrientationType.Front0;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsXDeep);
        }

        [Test]
        public void DecreaseFacings_InZ_DoesNotDecreaseXBlock_WhenSufficentSpace()
        {
            const Single height = 10;
            const Single width = 4;
            const Single depth = 10;

            Planogram planogram = CreatePlanogram(chest: true);
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 2, 1, 1, 2, 0, 0, 0, 0, 0, 0);
            position.OrientationTypeX = PlanogramPositionOrientationType.Right0;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsXDeep);
        }

        [Test]
        public void DecreaseFacings_InZ_DecreasesYBlock_WhenInsufficientSpace()
        {
            const Single height = 10;
            const Single width = 5;
            const Single depth = 2;

            Planogram planogram = CreatePlanogram(chest: true);
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 2, 0, 0, 0, 1, 1, 2, 0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Front0;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsYDeep);
        }

        [Test]
        public void DecreaseFacings_InZ_DoesNotDecreaseYBlock_WhenSufficentSpace()
        {
            const Single height = 10;
            const Single width = 4;
            const Single depth = 10;

            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram, height, width, depth);
            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 2, 0, 0, 0, 1, 1, 2, 0, 0, 0);
            position.OrientationTypeY = PlanogramPositionOrientationType.Right0;

            position.DecreaseFacings(product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsYDeep);
        }
        #endregion

        #endregion

        #region Increase / Decrease Units

        [Test]
        public void IncreaseUnitsX_IncreasesTrayProductWithBreakTrayUp()
        {
            const String expectationPositionShouldBeDefault = "Main position should have Default Merchandising Style, was {0}";
            //Tests that a tray product increases correctly when can break tray up...
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
         
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            product.CanBreakTrayUp = true;
            product.TrayWide = 2;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreNotEqual(2, position.FacingsWide, "when we can break tray up I would expect that the position breaks out of the tray.");
            Assert.AreEqual(1, position.FacingsWide, "Main position body (aka tray) should be one wide with a right cap.");
            Assert.AreEqual(1, position.FacingsXWide, "Position should have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
            Assert.IsTrue(position.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Unit, "FacingsX should be a unit");

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "should be a second full tray now (product tray wide 2).");
            Assert.AreEqual(0, position.FacingsXWide, "No extra cap should be here at this point.");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
            Assert.IsTrue(position.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Unit, "FacingsX should be a unit");

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "should still be two facings wide.");
            Assert.AreEqual(1, position.FacingsXWide, "Should have broken out of the tray again.");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
            Assert.IsTrue(position.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Unit, "FacingsX should be a unit");
        }

        [Test]
        public void IncreaseUnitsX_IncreasesTrayProductWithoutBreakTrayUp()
        {
            const String expectationPositionShouldBeDefault = "Main position should have Default Merchandising Style, was {0}";
            //Tests that a tray product increases correctly when can break tray up...
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
           
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            product.CanBreakTrayUp = false;
            product.TrayWide = 2;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(3, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(4, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
        }

        [Test]
        public void DecreaseUnitsX_DecreasesTrayProductWithBreakTrayUp()
        {
            const String expectationPositionShouldBeDefault = "Main position should have Default Merchandising Style, was {0}";
            //Tests that a tray product increases correctly when can break tray up...
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
          
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            product.CanBreakTrayUp = true;
            product.TrayWide = 2;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreNotEqual(2, position.FacingsWide, "when we can break tray up I would expect that the position breaks out of the tray.");
            Assert.AreEqual(1, position.FacingsWide, "Main position body (aka tray) should be one wide with a right cap.");
            Assert.AreEqual(1, position.FacingsXWide, "Position should have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
            Assert.IsTrue(position.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Unit, "FacingsX should be a unit");

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "Should be a second full tray now (product tray wide 2).");
            Assert.AreEqual(0, position.FacingsXWide, "No extra cap should be here at this point.");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
            Assert.IsTrue(position.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Unit, "FacingsX should be a unit");

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "Should still be two facings wide.");
            Assert.AreEqual(1, position.FacingsXWide, "Should have broken out of the tray again.");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
            Assert.IsTrue(position.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Unit, "FacingsX should be a unit");

            //Now we can decrease and expect the reverse.

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "Should still be two facings wide.");
            Assert.AreEqual(0, position.FacingsXWide, "Should have removed the facings x.");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsWide, "Main position body (aka tray) should be one wide with a right cap.");
            Assert.AreEqual(1, position.FacingsXWide, "Position should have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsWide, "Main position body (aka tray) should be one wide without a right cap.");
            Assert.AreEqual(0, position.FacingsXWide, "Position should have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

        }

        [Test]
        public void DecreaseUnitsX_DecreasesTrayProductWithoutBreakTrayUp()
        {
            const String expectationPositionShouldBeDefault = "Main position should have Default Merchandising Style, was {0}";
            //Tests that a tray product increases correctly when can break tray up...
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
           
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            product.CanBreakTrayUp = false;
            product.TrayWide = 2;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(3, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            position.IncreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(4, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            //Now we can decrease and expect the reverse...

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(3, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "Cannot break tray product up so I should only have full trays");
            Assert.AreEqual(0, position.FacingsXWide, "Position should not have broken out of tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default, String.Format(expectationPositionShouldBeDefault, position.MerchandisingStyle));
        }

        [Test]
        public void DecreaseUnitsX_DecreasesTrayProductWithBreakTrayDown()
        {
            //Tests that a tray product increases correctly when can break tray up...
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
          
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            product.CanBreakTrayUp = false;
            product.CanBreakTrayDown = true;
            product.TrayWide = 3;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;

            //We start of with one full tray product facing and decrease from there.
            Assert.AreEqual(1, position.FacingsWide, "We should start with one tray facing wide");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Tray, "Should start off as a tray");

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "should have broken down into tray to be less than a single tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit, "Now that we have broken down should have become a unit. (Less than one single tray).");

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Unit, "Should be a unit now");
            Assert.AreEqual(1, position.FacingsWide, "should have decreased to now become a single unit");
        }

        [Test]
        public void DecreaseUnitsX_DecreasesTrayProductWithoutBreakTrayDown()
        {
            //Tests that a tray product increases correctly when can break tray up...
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
            
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            product.CanBreakTrayUp = false;
            product.CanBreakTrayDown = false;
            product.TrayWide = 3;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());
            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);
            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsWide = 3;

            //We start of with one full tray product facing and decrease from there.
            Assert.AreEqual(3, position.FacingsWide, "We should start with three tray facing wide");
            Assert.AreEqual(0, position.FacingsXWide, "should have no facings x");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Tray, "Should start off as 3 trays");

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(2, position.FacingsWide, "We should be 2 faings wide on main position");
            Assert.AreEqual(0, position.FacingsXWide, "Still shoud have not broken up out of the tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Tray, "Should remain a tray in main block");

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsWide, "We should be 2 faings wide on main position");
            Assert.AreEqual(0, position.FacingsXWide, "Still shoud have not broken up out of the tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Tray, "Should remain a tray in main block");

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsWide, "We should be 2 faings wide on main position");
            Assert.AreEqual(0, position.FacingsXWide, "Still shoud have not broken up out of the tray");
            Assert.IsTrue(position.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Tray, "Should remain a tray in main block");
        }

        [Test]
        public void DecreaseUnitsX_DecreasingPeggedPositionsDoesNotSubceedOneWide()
        {
            //Tests that a tray product increases correctly when can break tray up...
            Planogram planogram = CreatePlanogram();
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First().SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.Hang;
            subCompPlacement.SubComponent.MerchConstraintRow1Height = 4;
            subCompPlacement.SubComponent.MerchConstraintRow1SpacingX = 4;
            subCompPlacement.SubComponent.MerchConstraintRow1SpacingY = 4;
            subCompPlacement.SubComponent.MerchConstraintRow1StartX = 4;
            subCompPlacement.SubComponent.MerchConstraintRow1StartY = 4;
            subCompPlacement.SubComponent.MerchConstraintRow1Width = 4;
            subCompPlacement.SubComponent.Height = 100;
            subCompPlacement.SubComponent.Width = 100;

            var merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement);

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);

            PlanogramPegHoles pegHolesOnPegboard = subCompPlacement.SubComponent.GetPegHoles();

            Assert.IsTrue(pegHolesOnPegboard.All.Count() != 0);
            Assert.AreEqual(1, position.FacingsWide, "We expect to start the test with one facing wide on the pegboard");

            position.DecreaseUnits(AxisType.X, product, subCompPlacement, merchandisingGroup);

            Assert.AreEqual(1, position.FacingsWide, "Can't decrease to less than one.");
        }

        #endregion

        #region SetUnits

        [Test]
        public void SetUnits_WhenTargetIsCurrent_ShouldNotChangeUnits()
        {
            const String expectation = "When current units are target units should return correct units.";
            const Int32 expected = 1;
            PlanogramFixtureItem bay = Planogram.NewPlanogram().AddFixtureItem();
            PlanogramPosition position = bay.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay);
            PlanogramProduct product = position.GetPlanogramProduct();
            PlanogramSubComponentPlacement subComponentPlacement = position.GetPlanogramSubComponentPlacement();

            position.SetUnits(product, subComponentPlacement, null, expected, PlanogramPositionInventoryTargetType.LessThanOrEqualTo, new AssortmentRuleEnforcer());

            Int32 actual = position.GetPositionDetails().TotalUnitCount;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void SetUnits_WhenTargetUnitsAreHigherAndClosest_ShouldIncreaseToClosestUnits()
        {
            const String expectation = "When current units are lower than target units should increase and return correct units.";
            const Int32 expected = 111;
            PlanogramFixtureItem bay = Planogram.NewPlanogram().AddFixtureItem();
            PlanogramPosition position = bay.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay);
            PlanogramProduct product = position.GetPlanogramProduct();
            PlanogramSubComponentPlacement subComponentPlacement = position.GetPlanogramSubComponentPlacement();

            position.SetUnits(product, subComponentPlacement, null, expected - 1, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, new AssortmentRuleEnforcer());

            Int32 actual = position.GetPositionDetails().TotalUnitCount;
            Assert.AreEqual(expected, actual, expectation);
        }

        [Test]
        public void SetUnits_IncreasesTrayProductsToClosest()
        {
            const String expectation = "When a product in a tray needs increasing it will increase to closest.";
            const Int32 expected = 108;
            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixtureItem bay = planogram.AddFixtureItem();
            PlanogramProduct trayProduct = planogram.AddProduct();
            trayProduct.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            trayProduct.TrayWide = 3;
            trayProduct.TrayHigh = 3;
            trayProduct.TrayDeep = 3;
            PlanogramPosition position = bay.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay, trayProduct);
            PlanogramProduct product = position.GetPlanogramProduct();
            PlanogramSubComponentPlacement subComponentPlacement = position.GetPlanogramSubComponentPlacement();

            position.SetUnits(product, subComponentPlacement, null, expected - 1, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, new AssortmentRuleEnforcer());

            Int32 actual = position.GetPositionDetails().TotalUnitCount;
            Assert.AreEqual(expected, actual, expectation);
        } 

        [Test]
        public void SetUnits_WhenMaxUnitsRule_ShouldEnforceRule()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 25;
            var prod = plan.AddProduct(new WidthHeightDepthValue(10, 10, 20));
            plan.CreateAssortmentFromProducts();
            plan.Assortment.Products[0].MaxListUnits = 12;
            var pos = shelf.AddPosition(prod);
            pos.SetFacings(new WideHighDeepValue(3, 1, 3));
            plan.ReprocessAllMerchandisingGroups();

            using (var mgs = plan.GetMerchandisingGroups())
            {
                pos.SetUnits(
                    prod,
                    mgs[0].SubComponentPlacements.ElementAt(0),
                    mgs[0],
                    12,
                    PlanogramPositionInventoryTargetType.ClosestOrEqualTo,
                    new AssortmentRuleEnforcer());
                mgs.ApplyEdit();
                pos.RecalculateUnits(prod, mgs[0].SubComponentPlacements.ElementAt(0));
            }

            pos.TotalUnits.Should().Be(12, "because the max units can be achieved by set units");
            pos.FacingsWide.Should().Be(4, "because the max units should be achieved with 4 facings wide");
            pos.FacingsHigh.Should().Be(1, "because the max units should be achieved with 1 facing high");
            pos.FacingsDeep.Should().Be(3, "because the max units should be achieved with 3 facings deep");
        }

        #endregion

        #region HasMatchingNormalSequences

        [Test]
        public void HasMatchingNormalSequences_ReturnsTrue_WhenNormalSequencesMatch([Values(AxisType.X, AxisType.Y, AxisType.Z)] AxisType axis)
        {
            var position = PlanogramPosition.NewPlanogramPosition();
            position.SequenceX = 1;
            position.SequenceY = 2;
            position.SequenceZ = 3;

            Int16 seqX = axis == AxisType.X ? (Int16)10 : position.SequenceX;
            Int16 seqY = axis == AxisType.Y ? (Int16)10 : position.SequenceY;
            Int16 seqZ = axis == AxisType.Z ? (Int16)10 : position.SequenceZ;

            Boolean result = position.HasMatchingNormalSequences(axis, seqX, seqY, seqZ);

            Assert.IsTrue(result);
        }

        [Test]
        public void HasMatchingNormalSequences_ReturnsFalse_WhenNormalSequencesDoNotMatch([Values(AxisType.X, AxisType.Y, AxisType.Z)] AxisType axis)
        {
            var position = PlanogramPosition.NewPlanogramPosition();
            position.SequenceX = 1;
            position.SequenceY = 2;
            position.SequenceZ = 3;

            Int16 seqX = axis != AxisType.X ? (Int16)10 : position.SequenceX;
            Int16 seqY = axis != AxisType.Y ? (Int16)10 : position.SequenceY;
            Int16 seqZ = axis != AxisType.Z ? (Int16)10 : position.SequenceZ;

            Boolean result = position.HasMatchingNormalSequences(axis, seqX, seqY, seqZ);

            Assert.IsFalse(result);
        }

        #endregion

        #region Increase/Decrease units assortment rule enforcement
        [Test]
        public void IncreaseUnitsGetsBrokenRules()
        {
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns(() => new List<IBrokenAssortmentRule>());

            sut.IncreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object);

            mockAssortmentRuleEnforcer.VerifyAll();
        }

        [Test]
        public void DecreaseUnitsGetsBrokenRules()
        {
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.SetFacings(new WideHighDeepValue(10, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns(() => new List<IBrokenAssortmentRule>());

            sut.DecreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object);

            mockAssortmentRuleEnforcer.VerifyAll();
        }

        [Test]
        public void IncreaseUnitsWillNotIncreaseAboveProductMaxUnits()
        {
            const Int32 productMaxUnitLimit = 50;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MaxListUnits = productMaxUnitLimit;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.GetTotalUnitCount()) <=
                                                       productMaxUnitLimit)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMaxUnits);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.IncreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, new AssortmentRuleEnforcer()))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeLessOrEqualTo(productMaxUnitLimit,
                                because: "the product has a max unit limit set to {0}",
                                reasonArgs: productMaxUnitLimit);
        }

        [Test]
        public void IncreaseUnitsWillNotIncreaseAboveProductMaxFacings()
        {
            const Int32 productMaxFacings = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MaxListFacings = productMaxFacings;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.FacingsWide) <= productMaxFacings)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMaxFacings);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.IncreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.FacingsWide.Should()
               .BeLessOrEqualTo(productMaxFacings,
                                because: "the product has a max unit limit set to {0}",
                                reasonArgs: productMaxFacings);
        }

        [Test]
        public void IncreaseUnitsWillNotIncreaseMuchAboveProductExactUnits()
        {
            const Int32 productExactUnits = 50;
            const Int32 closestUnitsToExact = 37;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListUnits = productExactUnits;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = merchGroup.PositionPlacements.Where(p => p.Product.Gtin.Equals(product.Gtin)).Select(p => p.Position);
                                                   if (positions.Sum(position => position.GetTotalUnitCount()) <= productExactUnits)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactUnits);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.IncreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .Be(closestUnitsToExact,
                   because: "the product has a max unit limit set to {0}",
                   reasonArgs: productExactUnits);
        }

        [Test]
        public void IncreaseUnitsWillNotIncreaseAboveProductExactFacings()
        {
            const Int32 productExactFacings = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListFacings = productExactFacings;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.FacingsWide) <= productExactFacings)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactFacings);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.IncreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.FacingsWide.Should()
               .Be(productExactFacings,
                   because: "the product has a max unit limit set to {0}",
                   reasonArgs: productExactFacings);
        }

        [Test]
        public void DecreaseUnitsWillNotDecreaseUnderProductPreserveUnits()
        {
            const Int32 productPreserve = 50;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).PreserveListUnits = productPreserve;
            sut.SetFacings(new WideHighDeepValue(1, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.GetTotalUnitCount()) >= productPreserve)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductPreserveUnits);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.DecreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeGreaterOrEqualTo(productPreserve,
                                   because: "the product has Preserve Units set to {0}",
                                   reasonArgs: productPreserve)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseUnitsWillNotDecreaseUnderProductPreserveFacings()
        {
            const Int32 productPreseve = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).PreserveListFacings = productPreseve;
            sut.SetFacings(new WideHighDeepValue(1, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.FacingsWide) >= productPreseve)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductPreserveFacings);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.DecreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.FacingsWide.Should()
               .BeGreaterOrEqualTo(productPreseve,
                                   because: "the product has a Preserve Units set to {0}",
                                   reasonArgs: productPreseve)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseUnitsWillNotDecreaseUnderProductMinHurdleUnits()
        {
            const Int32 productMinHurdle = 50;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MinListUnits = productMinHurdle;
            sut.SetFacings(new WideHighDeepValue(1, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.GetTotalUnitCount()) >= productMinHurdle)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMinimumHurdleUnits);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.DecreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeGreaterOrEqualTo(productMinHurdle,
                                   because: "the product has a Minimum Hurdle set to {0}",
                                   reasonArgs: productMinHurdle)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseUnitsWillNotDecreaseUnderProductMinHurdleFacings()
        {
            const Int32 productMinHurdle = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MinListFacings = productMinHurdle;
            sut.SetFacings(new WideHighDeepValue(1, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.FacingsWide) >= productMinHurdle)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMinimumHurdleFacings);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.DecreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.FacingsWide.Should()
               .BeGreaterOrEqualTo(productMinHurdle,
                                   because: "the product has a Minimum Hurdle set to {0}",
                                   reasonArgs: productMinHurdle)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseUnitsWillNotDecreaseUnderProductExactUnits()
        {
            const Int32 productExact = 50;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListUnits = productExact;
            sut.SetFacings(new WideHighDeepValue(1, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.GetTotalUnitCount()) >= productExact)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactUnits);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.DecreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeGreaterOrEqualTo(productExact,
                                   because: "the product has Exact Units set to {0}",
                                   reasonArgs: productExact)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseUnitsWillNotDecreaseUnderProductExactFacings()
        {
            const Int32 productExact = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListFacings = productExact;
            sut.SetFacings(new WideHighDeepValue(1, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                               {
                                                   var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                                   if (positions.Sum(position => position.FacingsWide) >= productExact)
                                                       return new List<IBrokenAssortmentRule>();
                                                   var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                                   mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactFacings);
                                                   return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                               });

            while (sut.DecreaseUnits(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.FacingsWide.Should()
               .Be(productExact,
                   because: "the product has Exact Units set to {0}",
                   reasonArgs: productExact);
        }

        #endregion

        #region Increase/Decrease facings assortment rule enforcement
        [Test]
        public void IncreaseFacingsGetsBrokenRules()
        {
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns(() => new List<IBrokenAssortmentRule>());

            sut.IncreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object);

            mockAssortmentRuleEnforcer.VerifyAll();
        }

        [Test]
        public void DecreaseFacingsGetsBrokenRules()
        {
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.SetFacings(new WideHighDeepValue(10, 10, 10));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns(() => new List<IBrokenAssortmentRule>());

            sut.DecreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object);

            mockAssortmentRuleEnforcer.VerifyAll();
        }

        [Test]
        public void IncreaseFacingsWillNotIncreaseAboveProductMaxUnits()
        {
            const Int32 productMaxUnitLimit = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MaxListUnits = productMaxUnitLimit;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.GetTotalUnitCount()) <=
                                              productMaxUnitLimit)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMaxUnits);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.IncreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, new AssortmentRuleEnforcer()))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeLessOrEqualTo(productMaxUnitLimit,
                                because: "the product has a max unit limit set to {0}",
                                reasonArgs: productMaxUnitLimit);
        }

        [Test]
        public void IncreaseFacingsWillNotIncreaseAboveProductMaxFacings()
        {
            const Int32 productMaxFacings = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MaxListFacings = productMaxFacings;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.FacingsWide) <= productMaxFacings)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMaxFacings);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.IncreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.FacingsWide.Should()
               .BeLessOrEqualTo(productMaxFacings,
                                because: "the product has a max unit limit set to {0}",
                                reasonArgs: productMaxFacings);
        }

        [Test]
        public void IncreaseFacingsWillNotIncreaseMuchAboveProductExactUnits()
        {
            const Int32 productExactUnits = 7;
            const Int32 closestUnitsToExact = 6;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.FacingsDeep = 3;
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListUnits = productExactUnits;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.GetTotalUnitCount()) <= productExactUnits)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactUnits);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.IncreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .Be(closestUnitsToExact,
                   because: "the product has a max unit limit set to {0}",
                   reasonArgs: productExactUnits);
        }

        [Test]
        public void IncreaseFacingsWillNotIncreaseAboveProductExactFacings()
        {
            const Int32 productExactFacings = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListFacings = productExactFacings;
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.FacingsWide) <= productExactFacings)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactFacings);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.IncreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object))
            {
                if (PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(sut.GetPlanogramSubComponentPlacement()).IsOverfilled()) break;
            }

            sut.FacingsWide.Should()
               .Be(productExactFacings,
                   because: "the product has a max unit limit set to {0}",
                   reasonArgs: productExactFacings);
        }

        [Test]
        public void DecreaseFacingsWillNotDecreaseUnderProductPreserveUnits()
        {
            const Int32 productPreserve = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).PreserveListUnits = productPreserve;
            sut.SetFacings(new WideHighDeepValue(10, 1, 1));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.GetTotalUnitCount()) >= productPreserve)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductPreserveUnits);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.DecreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeGreaterOrEqualTo(productPreserve,
                                   because: "the product has Preserve Units set to {0}",
                                   reasonArgs: productPreserve)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseFacingsWillNotDecreaseUnderProductPreserveFacings()
        {
            const Int32 productPreseve = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).PreserveListFacings = productPreseve;
            sut.SetFacings(new WideHighDeepValue(10, 1, 1));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.FacingsWide) >= productPreseve)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductPreserveFacings);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.DecreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.FacingsWide.Should()
               .BeGreaterOrEqualTo(productPreseve,
                                   because: "the product has a Preserve Units set to {0}",
                                   reasonArgs: productPreseve)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseFacingsWillNotDecreaseUnderProductMinHurdleUnits()
        {
            const Int32 productMinHurdle = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MinListUnits = productMinHurdle;
            sut.SetFacings(new WideHighDeepValue(10, 1, 1));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.GetTotalUnitCount()) >= productMinHurdle)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMinimumHurdleUnits);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.DecreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeGreaterOrEqualTo(productMinHurdle,
                                   because: "the product has a Minimum Hurdle set to {0}",
                                   reasonArgs: productMinHurdle)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseFacingsWillNotDecreaseUnderProductMinHurdleFacings()
        {
            const Int32 productMinHurdle = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).MinListFacings = productMinHurdle;
            sut.SetFacings(new WideHighDeepValue(10, 1, 1));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.FacingsWide) >= productMinHurdle)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductMinimumHurdleFacings);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.DecreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.FacingsWide.Should()
               .BeGreaterOrEqualTo(productMinHurdle,
                                   because: "the product has a Minimum Hurdle set to {0}",
                                   reasonArgs: productMinHurdle)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseFacingsWillNotDecreaseUnderProductExactUnits()
        {
            const Int32 productExact = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListUnits = productExact;
            sut.SetFacings(new WideHighDeepValue(10, 1, 1));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.GetTotalUnitCount()) >= productExact)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactUnits);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.DecreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.GetPositionDetails().TotalUnitCount.Should()
               .BeGreaterOrEqualTo(productExact,
                                   because: "the product has Exact Units set to {0}",
                                   reasonArgs: productExact)
               .And.BeGreaterThan(1,
                                  because: "there should be more than a few units");
        }

        [Test]
        public void DecreaseFacingsWillNotDecreaseUnderProductExactFacings()
        {
            const Int32 productExact = 5;
            PlanogramPosition sut =
                "Test Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.Parent.Assortment.Products.Add(sut.GetPlanogramProduct()).ExactListFacings = productExact;
            sut.SetFacings(new WideHighDeepValue(10, 1, 1));
            var mockAssortmentRuleEnforcer = new Mock<IAssortmentRuleEnforcer>();
            mockAssortmentRuleEnforcer.Setup(enforcer =>
                                             enforcer.GetBrokenRulesFor(It.IsAny<PlanogramAssortmentProduct>(),
                                                                        It.IsAny<PlanogramMerchandisingGroup>()))
                                      .Returns((PlanogramAssortmentProduct product, PlanogramMerchandisingGroup merchGroup) =>
                                      {
                                          var positions = product.GetPlanogramProduct().GetPlanogramPositions();
                                          if (positions.Sum(position => position.FacingsWide) >= productExact)
                                              return new List<IBrokenAssortmentRule>();
                                          var mockBrokenRule = new Mock<IBrokenAssortmentRule>();
                                          mockBrokenRule.Setup(rule => rule.Type).Returns(BrokenRuleType.ProductExactFacings);
                                          return new List<IBrokenAssortmentRule> { mockBrokenRule.Object };
                                      });

            while (sut.DecreaseFacings(sut.GetPlanogramProduct(), sut.GetPlanogramSubComponentPlacement(), null, mockAssortmentRuleEnforcer.Object)) { }

            sut.FacingsWide.Should()
               .Be(productExact,
                   because: "the product has Exact Units set to {0}",
                   reasonArgs: productExact);
        }
        #endregion

        #endregion

        #region Metadata

        private PlanogramPosition CreateMetadataPosition()
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 2;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;

            pos1.FacingsYHigh = 1;
            pos1.FacingsYWide = 1;
            pos1.FacingsYDeep = 2;
            pos1.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeY = PlanogramPositionOrientationType.Top0;

            pos1.FacingsXHigh = 2;
            pos1.FacingsXWide = 2;
            pos1.FacingsXDeep = 1;
            pos1.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeX = PlanogramPositionOrientationType.Right0;

            pos1.FacingsZHigh = 1;
            pos1.FacingsZWide = 1;
            pos1.FacingsZDeep = 2;
            pos1.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeZ = PlanogramPositionOrientationType.Back0;

            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 2.7F;
            pos1.TotalUnits = 9;
            pos1.Sequence = 1;

            return pos1;
        }

        [Test]
        public void MetadataAllPropertiesTested()
        {
            Boolean testsMissing = false;
            foreach (var property in typeof(PlanogramPosition).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    var method =
                        this.GetType().GetMethod(property.Name);

                    if (method == null)
                    {
                        testsMissing = true;
                        Debug.WriteLine(property.Name);
                    }
                }
            }

            if (testsMissing)
            {
                Assert.Ignore();
            }
        }

        [Test]
        public void ClearMetadata()
        {
            PlanogramPosition pos = CreateMetadataPosition();
            Package pk = pos.Parent.Parent;

            foreach (var property in typeof(PlanogramPosition).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    //set it
                    property.SetValue(pos, TestDataHelper.GetValue1(property.PropertyType), null);

                    //call clear
                    pk.ClearMetadata();

                    if (property.PropertyType.IsEnum)
                    {
                        Convert.ToByte(property.GetValue(pos, null)).Should().Be(0);
                    }
                    else if (property.PropertyType == typeof(String))
                    {
                        property.GetValue(pos, null).ToString().Should().BeNullOrEmpty(String.Format("cleared metadata for {0} should be null or default", property.Name));
                    }
                    else
                    {
                        property.GetValue(pos, null).Should().BeNull(String.Format("cleared metadata for {0} should be null or default", property.Name));
                    }
                }
            }
        }

        [Test]
        public void CalculateMetadata_WhenPositionInSubGroup_ShouldStoreNameAsMetadata()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var position = bay.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            var group = plan.AddSequenceGroup();
            position.SetSequenceGroup(group.AddSequenceGroupProduct(position.GetPlanogramProduct()));
            var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(group.Products);
            group.SubGroups.Add(subGroup);

            plan.Parent.CalculateMetadata(false, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, plan.Parent));

            position.MetaSequenceSubGroupName.Should().Be(
                subGroup.Name,
                "the name of the sub group should have been retrieved from the sequence");
        }

        [Test]
        public void CalculateMetadata_WhenPositionNotInSubGroup_ShouldClearNameAsMetadata()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var position1 = shelf.AddPosition();
            var position2 = shelf.AddPosition();
            var group = plan.AddSequenceGroup();
            position1.SetSequenceGroup(group.AddSequenceGroupProduct(position1.GetPlanogramProduct()));
            position2.SetSequenceGroup(group.AddSequenceGroupProduct(position2.GetPlanogramProduct()));
            var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(group.Products.Take(1));
            group.SubGroups.Add(subGroup);

            plan.Parent.CalculateMetadata(false, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, plan.Parent));

            position2.MetaSequenceSubGroupName.Should().BeNullOrEmpty("the position was not in a sub group");
        }

        [Test]
        public void CalculateMetadata_WhenPositionInSequenceGroup_ShouldStoreNameAsMetadata()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var position = bay.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            var group = plan.AddSequenceGroup();
            position.SetSequenceGroup(group.AddSequenceGroupProduct(position.GetPlanogramProduct()));
            var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(group.Products);
            group.SubGroups.Add(subGroup);

            plan.Parent.CalculateMetadata(false, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, plan.Parent));

            position.MetaSequenceGroupName.Should().Be(
                group.Name,
                "the name of the sequence group should have been retrieved from the sequence");
        }

        [Test]
        public void CalculateMetadata_WhenPositionNotInSequenceGroup_ShouldClearNameAsMetadata()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var position1 = shelf.AddPosition();
            var position2 = shelf.AddPosition();
            var group = plan.AddSequenceGroup();
            position1.SetSequenceGroup(group.AddSequenceGroupProduct(position1.GetPlanogramProduct()));
            var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(group.Products.Take(1));
            group.SubGroups.Add(subGroup);

            plan.Parent.CalculateMetadata(false, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, plan.Parent));

            position2.MetaSequenceGroupName.Should().BeNullOrEmpty("the position was not in a sequence group");
        }

        #endregion

        #region Helper Methods

        private static Planogram CreatePlanogram(Boolean chest = false)
        {
            Planogram planogram = Planogram.NewPlanogram();
            PlanogramComponent component = chest
                                               ? PlanogramComponent.NewPlanogramComponent("Chest",
                                                   PlanogramComponentType.Chest, 120, 4, 75)
                                               : PlanogramComponent.NewPlanogramComponent("Shelf",
                                                   PlanogramComponentType.Shelf, 120, 4, 75);
            planogram.Components.Add(component);
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 200;
            fixture.Depth = 75;
            fixture.Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(component));
            planogram.Fixtures.Add(fixture);
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture));

            return planogram;
        }

        private static PlanogramProduct CreateProduct(Planogram planogram, Single height = 10, Single width = 5, Single depth =2)
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Height = height;
            product.Width = width;
            product.Depth = depth;
            planogram.Products.Add(product);
            return product;
        }

        private static PlanogramPosition CreatePosition(
            Planogram planogram,
            PlanogramProduct product,
            PlanogramSubComponentPlacement subCompPlacement,
            Byte wide, Byte high, Byte deep,
            Byte xWide = 0, Byte xHigh = 0, Byte xDeep = 0,
            Byte yWide = 0, Byte yHigh = 0, Byte yDeep = 0,
            Byte zWide = 0, Byte zHigh = 0, Byte zDeep = 0)
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position.FacingsWide = wide;
            position.FacingsXWide = xWide;
            position.FacingsZWide = zWide;
            position.FacingsYWide = yWide;
            position.FacingsHigh = high;
            position.FacingsXHigh = xHigh;
            position.FacingsZHigh = zHigh;
            position.FacingsYHigh = yHigh;
            position.FacingsDeep = deep;
            position.FacingsXDeep = xDeep;
            position.FacingsZDeep = zDeep;
            position.FacingsYDeep = yDeep;
            planogram.Positions.Add(position);
            return position;
        } 
        #endregion

        #region IPlanogramSubComponentSequencedItem Members

        [Test]
        public void Position_GetParentSubComponentPlacement()
        {
            PlanogramPosition testModel = TestDataHelper.CreateTestPosition();
            PlanogramSubComponentPlacement planogramSubComponentPlacement = ((IPlanogramSubComponentSequencedItem)testModel).GetParentSubComponentPlacement();

            Boolean result = planogramSubComponentPlacement.Equals(testModel.GetPlanogramSubComponentPlacement());

            Assert.IsTrue(result);
        }

        #endregion
    }
}