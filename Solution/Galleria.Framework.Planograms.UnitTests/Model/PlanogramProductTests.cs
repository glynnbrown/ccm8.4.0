﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26041 : A.Kuszyk
//  Created
// V8-27411 : M.Pettit
//  Set Package.NewPackage() to be a user locktype-creation
// V8-27606 : L.Ineson
//  Amended to implement the new testbase
#endregion

#region Version Hisory : CCM 801
// V8-27494 : A.Kuszyk
//  Added UpdateFrom tests.
#endregion
#region Version Hisory : CCM 802
// V8-29030 : J.Pickup
//  extended UpdateFrom tests.
#endregion
#region Version History: CCM 820
// V830705 : A.Probyn
//  Extended ClearMetadata to support new string meta data
#endregion

#region Version History : CCM 830

// V8-31947 : A.Silva
//  Added tests for MetadataComparisonStatus.
// V8-32107 : A.Silva
//  Removed tests for obsolete Comparison status types.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Moq;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramProductTests : TestBase<PlanogramProduct, PlanogramProductDto>
    {
        #region Helpers

        private IEnumerable<PropertyInfo> _properties
        {
            get { return typeof(IPlanogramProduct).GetProperties().Where(p => p.Name != "Id").ToList(); }
        }

        private IEnumerable<PropertyInfo> _customAttributeProperties
        {
            get { return typeof(ICustomAttributeData).GetProperties(); }
        }

        private IEnumerable<PropertyInfo> _performanceDataProperties
        {
            get { return typeof(IPlanogramPerformanceData).GetProperties(); }
        }

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        private IEnumerable<PlanogramImageDto> FetchPlanogramImageDtos(Planogram planogram)
        {
            IEnumerable<PlanogramImageDto> planogramImageDtos;
            using (var dalContext = DalFactory.CreateContext())
            {
                var dal = dalContext.GetDal<IPlanogramImageDal>();
                planogramImageDtos = dal.FetchByPlanogramId(planogram.Id);
            }
            return planogramImageDtos;
        }

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        private void setPerformanceDataValues(ProductLibraryProductPerformanceData product)
        {
            product.P1 = 1;
            product.P2 = 2;
            product.P3 = 6;
            product.P4 = 4;
            product.P5 = 5;
            product.P6 = 6;
            product.P7 = 7;
            product.P8 = 8;
            product.P9 = 9;
            product.P10 = 10;
            product.P11 = 11;
            product.P12 = 12;
            product.P13 = 13;
            product.P14 = 14;
            product.P15 = 15;
            product.P16 = 16;
            product.P17 = 17;
            product.P18 = 18;
            product.P19 = 19;
            product.P20 = 20;
            product.AchievedCasePacks = 21;
            product.AchievedDeliveries = 22;
            product.AchievedDos = 23;
            product.AchievedShelfLife = 24;
            product.UnitsSoldPerDay = 25;
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters(new List<String>
                {
                    PlanogramProduct.SqueezeHeightActualProperty.Name,
                    PlanogramProduct.SqueezeWidthActualProperty.Name,
                    PlanogramProduct.SqueezeDepthActualProperty.Name,
                    PlanogramProduct.MetaCDTNodeProperty.Name
                });
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        [Test]
        public void NewFactoryMethod_IPlanogramProduct()
        {
            PlanogramProduct prod1 = base.NewModel();
            prod1.Gtin = "Test";

            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct(prod1);
            Assert.AreEqual(prod1.Gtin, prod2.Gtin);

        }

        #endregion

        #region Data Access

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

      
        #endregion

        #region Methods

        #region LoadProductImages

        private Mock<IPlanogramProductImageHelper> CreateMock(Byte[] imageData)
        {
            var productMock = new Mock<IPlanogramProductImageHelper>();
            productMock
                .Setup(m => m.GetImageData(It.IsAny<PlanogramImageType>(), It.IsAny<PlanogramImageFacingType>()))
                .Returns(imageData);
            return productMock;
        }

        private Byte[] GetImageData()
        {
            Byte[] imageData;
            using (var fileStream = new FileStream(Path.Combine("Environment", "TestImage.png"), FileMode.Open))
            {
                Int32 length = (Int32)fileStream.Length;
                imageData = new Byte[length];
                fileStream.Read(imageData, 0, length);
            }
            return imageData;
        }

        [Test]
        public void LoadProductImages_DoesntLoadWhenGetImageThrows()
        {
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            var productMock = new Mock<IPlanogramProductImageHelper>();
            productMock
                .Setup(m => m.GetImageData(It.IsAny<PlanogramImageType>(), It.IsAny<PlanogramImageFacingType>()))
                .Throws<ArgumentOutOfRangeException>();

            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Name = "Name";
            planogramProduct.Gtin = "Gtin";
            planogram.Products.Add(planogramProduct);
            planogramProduct.LoadProductImages(productMock.Object);
            package = package.Save();

            Assert.AreEqual(0, FetchPlanogramImageDtos(package.Planograms.First()).Count());
        }

        [Test]
        public void LoadProductImages_ThrowsWhenParentIsNull()
        {
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            var productMock = new Mock<IPlanogramProductImageHelper>();

            TestDelegate test = () => planogramProduct.LoadProductImages(productMock.Object);

            Assert.Throws<InvalidOperationException>(test);
        }

        [Test]
        public void LoadProductImages_CopiesImageDataToDal()
        {
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            var imageData = GetImageData();
            var productMock = CreateMock(imageData);

            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Name = "Name";
            planogramProduct.Gtin = "Gtin";
            planogram.Products.Add(planogramProduct);
            planogramProduct.LoadProductImages(productMock.Object);
            package = package.Save();

            var planogramImageDtos = FetchPlanogramImageDtos(package.Planograms.First());
            Assert.That(planogramImageDtos.Count() > 0);
            foreach (var planogramImageDto in planogramImageDtos)
            {
                CollectionAssert.AreEqual(imageData, planogramImageDto.ImageData);
            }
        }

        [Test]
        public void LoadProductImages_LoadsAllImages()
        {
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            var imageData = GetImageData();
            var productMock = CreateMock(imageData);

            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Name = "Name";
            planogramProduct.Gtin = "Gtin";
            planogram.Products.Add(planogramProduct);
            planogramProduct.LoadProductImages(productMock.Object);
            package = package.Save();

            var numberOfImages =
                Enum.GetNames(typeof(PlanogramImageType)).Count() *
                Enum.GetNames(typeof(PlanogramImageFacingType)).Count();
            Assert.AreEqual(numberOfImages, FetchPlanogramImageDtos(package.Planograms.First()).Count());
        }

        #endregion 

        #region UpdateFrom

        [Test]
        [TestCaseSource("_customAttributeProperties")]
        public void UpdateFrom_WhenCustomAttributeProperty_UpdatesValue(PropertyInfo customAttributeProperty)
        {
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            var product = Product.NewProduct();
            TestDataHelper.SetPropertyValues(product.CustomAttributes);
            if (customAttributeProperty.PropertyType.Equals(typeof(DateTime?)))
            {
                customAttributeProperty.SetValue(product.CustomAttributes, (DateTime?)DateTime.Today, null);
            }

            planogramProduct.UpdateFrom(
                product, 
                new[] { String.Format("{0}.{1}", PlanogramProduct.CustomAttributesProperty.Name, customAttributeProperty.Name) });

            Object expected = customAttributeProperty.GetValue(product.CustomAttributes, null);
            Object actual = customAttributeProperty.GetValue(planogramProduct.CustomAttributes, null);
            Assert.That(expected.Equals(actual), String.Format("Expected {0}, but was {1}", expected, actual));
        }

        [Test]
        [TestCaseSource("_performanceDataProperties")]
        public void UpdateFrom_WhenPerformanceDataProperty_UpdatesValue(PropertyInfo performanceDataProperty)
        {
            //Create the main collection
            var planogram = Planogram.NewPlanogram();
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();

            //Performance data is automatically created when we add a new product but it must have an id
            planogramProduct.Id = 1;
            planogram.Products.Add(planogramProduct);

            //Now create the object we wish to update from and make into productLibraryProduct:
            var actualproduct = Product.NewProduct();
            ProductLibraryProduct product = ProductLibraryProduct.NewProductLibraryProduct(actualproduct);
            
            //Lets performance it some values:
            setPerformanceDataValues(product.PerformanceData);

            //call the update From works.
            planogramProduct.UpdateFrom(product, new[] { String.Format("{0}.{1}", PlanogramPerformance.PerformanceDataProperty.Name, performanceDataProperty.Name) });

            //Add in some values.
            var productPerformance = product.PerformanceData;
            var planogramProductPerformance = ((Planogram)planogramProduct.Parent).Performance;

            //Check are same
            object expected = performanceDataProperty.GetValue(product.PerformanceData, null);
            object actual = performanceDataProperty.GetValue(planogramProductPerformance.PerformanceData.First(), null);
            Assert.That(expected.Equals(actual), String.Format("Expected {0}, but was {1}", expected, actual));
        }

        [Test]
        public void UpdateFrom_WhenNoPropertiesSpecified_UpdatesAllValues()
        {
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            var product = Product.NewProduct();
            TestDataHelper.SetPropertyValues(product);

            planogramProduct.UpdateFrom(product);

            AssertHelper.AreEqual<IPlanogramProduct>(product, planogramProduct, "Id");
        }

        [Test]
        [TestCaseSource("_properties")]
        public void UpdateFrom_WhenPropertySpecified_ValueChanges(PropertyInfo property)
        {
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            var product = Product.NewProduct();
            TestDataHelper.SetPropertyValues(product);

            planogramProduct.UpdateFrom(product, property.Name);

            Object expected = property.GetValue(product, null);
            Object actual = property.GetValue(planogramProduct, null);
            if (expected is ICustomAttributeData)
            {
                AssertHelper.AreEqual<ICustomAttributeData>((ICustomAttributeData)expected, (ICustomAttributeData)actual, "Id");
            }
            else
            {
                Assert.AreEqual(expected, actual, String.Format("Assert failed for {0} property", property.Name));
            }
        }

        [Test]
        public void UpdateFrom_WhenAllPropertiesSpecified_AllValuesUpdate()
        {
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            var oldPlanogramProduct = planogramProduct.Copy();
            var product = Product.NewProduct();
            TestDataHelper.SetPropertyValues(product);

            planogramProduct.UpdateFrom(product, _properties.Select(p=>p.Name).ToArray());

            AssertHelper.AreEqual<IPlanogramProduct>(product, planogramProduct, "Id");
        }

        #endregion

        #endregion

        #region Metadata

        private PlanogramProduct CreateMetadataProduct()
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);
  
            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 2;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;

            pos1.FacingsYHigh = 1;
            pos1.FacingsYWide = 1;
            pos1.FacingsYDeep = 2;
            pos1.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeY = PlanogramPositionOrientationType.Top0;

            pos1.FacingsXHigh = 2;
            pos1.FacingsXWide = 2;
            pos1.FacingsXDeep = 1;
            pos1.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeX = PlanogramPositionOrientationType.Right0;

            pos1.FacingsZHigh = 1;
            pos1.FacingsZWide = 1;
            pos1.FacingsZDeep = 2;
            pos1.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeZ = PlanogramPositionOrientationType.Back0;

            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 2.7F;
            pos1.TotalUnits = 9;
            pos1.Sequence = 1;

            return prod1;
        }

        [Test]
        public void MetadataAllPropertiesTested()
        {
            String[] testMethodNames = this.GetType().GetMethods().Select(m => m.Name).ToArray();

            Boolean testsMissing = false;
            foreach (var property in typeof(PlanogramProduct).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    if (!testMethodNames.Any(m => m.StartsWith(property.Name)))
                    {
                        testsMissing = true;
                        Debug.WriteLine(property.Name);
                    }
                }
            }

            if (testsMissing)
            {
                Assert.Ignore();
            }
        }

        [Test]
        public void ClearMetadata()
        {
            PlanogramProduct prod = CreateMetadataProduct();
            Package pk = prod.Parent.Parent;

            foreach (var property in typeof(PlanogramProduct).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    //set it
                    property.SetValue(prod, TestDataHelper.GetValue2(property.PropertyType), null);

                    //call clear
                    pk.ClearMetadata();

                    if (property.PropertyType == typeof(String))
                    {
                        Assert.IsNullOrEmpty((String)property.GetValue(prod, null), property.Name);
                    }
                    else
                    {
                        Assert.IsNull(property.GetValue(prod, null), property.Name);
                    }
                }
            }
        }

        [Test]
        public void MetaTotalMainFacings()
        {
            PlanogramProduct prod = CreateMetadataProduct();
            Package pk = prod.Parent.Parent;
            pk.CalculateMetadata(false, null);

            Assert.AreEqual(1, prod.MetaTotalMainFacings);

            Assert.Contains("MetaTotalMainFacings",
                PlanogramProduct.EnumerateDisplayableFieldInfos(true, false, false, null).Select(o => o.PropertyName).ToList(),
                "Property should be displayed.");
        }

        [Test]
        public void MetaTotalXFacings()
        {
            PlanogramProduct prod = CreateMetadataProduct();
            Package pk = prod.Parent.Parent;
            pk.CalculateMetadata(false, null);

            Assert.AreEqual(4, prod.MetaTotalXFacings);

            Assert.Contains("MetaTotalXFacings",
                PlanogramProduct.EnumerateDisplayableFieldInfos(true, false, false, null).Select(o => o.PropertyName).ToList(),
                "Property should be displayed.");
        }

        [Test]
        public void MetaTotalYFacings()
        {
            PlanogramProduct prod = CreateMetadataProduct();
            Package pk = prod.Parent.Parent;
            pk.CalculateMetadata(false, null);

            Assert.AreEqual(1, prod.MetaTotalYFacings);

            Assert.Contains("MetaTotalYFacings",
                PlanogramProduct.EnumerateDisplayableFieldInfos(true, false, false, null).Select(o => o.PropertyName).ToList(),
                "Property should be displayed.");
        }

        [Test]
        public void MetaTotalZFacings()
        {
            PlanogramProduct prod = CreateMetadataProduct();
            Package pk = prod.Parent.Parent;
            pk.CalculateMetadata(false, null);

            Assert.AreEqual(1, prod.MetaTotalZFacings);

            Assert.Contains("MetaTotalZFacings",
                PlanogramProduct.EnumerateDisplayableFieldInfos(true, false, false, null).Select(o => o.PropertyName).ToList(),
                "Property should be displayed.");
        }

        #region MetaComparisonStatus

        [Test]
        public void MetaComparisonStatus_WhenAllUnchanged_ShouldBeUnchanged()
        {
            const String expectation = "When no planogram has any changes for the product it should be flagged as {0} but was {1}";
            const PlanogramItemComparisonStatusType expected = PlanogramItemComparisonStatusType.Unchanged;
            var targetPlans = new List<Planogram> {SetupComparisonPlan("TargetOne"), SetupComparisonPlan("TargetTwo")};
            Planogram masterPlan = SetupComparisonPlan("SourceOne");
            masterPlan.Parent.CalculateMetadata(false, null);
            targetPlans.ForEach(planogram => planogram.Parent.CalculateMetadata(false, null));
            masterPlan.Comparison.UpdateResults(targetPlans);

            masterPlan.Parent.CalculateMetadata(false, null);

            PlanogramItemComparisonStatusType actual = masterPlan.Products.First().MetaComparisonStatus;
            Assert.AreEqual(expected, actual, expectation, expected ,actual);
        }

        [Test]
        public void MetaComparisonStatus_WhenAllMissing_ShouldBeAdded()
        {
            const String expectation = "When all planograms are missing the product it should be flagged as {0} but was {1}";
            const PlanogramItemComparisonStatusType expected = PlanogramItemComparisonStatusType.New;
            const Int32 missingProducts = 0;
            var targetPlans = new List<Planogram> { SetupComparisonPlan("TargetOne", missingProducts), SetupComparisonPlan("TargetTwo", missingProducts) };
            Planogram masterPlan = SetupComparisonPlan("SourceOne");
            masterPlan.Parent.CalculateMetadata(false, null);
            targetPlans.ForEach(planogram => planogram.Parent.CalculateMetadata(false, null));
            masterPlan.Comparison.UpdateResults(targetPlans);

            masterPlan.Parent.CalculateMetadata(false, null);

            PlanogramItemComparisonStatusType actual = masterPlan.Products.First().MetaComparisonStatus;
            Assert.AreEqual(expected, actual, expectation, expected, actual);
        }

        [Test]
        public void MetaComparisonStatus_WhenMasterMissingFromAll_ShouldBeRemoved()
        {
            const String expectation = "When all planograms are missing the product it should be flagged as {0} but was {1}";
            const PlanogramItemComparisonStatusType expected = PlanogramItemComparisonStatusType.NotFound;
            const Int32 missingProducts = 0;
            var targetPlans = new List<Planogram> { SetupComparisonPlan("TargetOne"), SetupComparisonPlan("TargetTwo") };
            Planogram masterPlan = SetupComparisonPlan("SourceOne", missingProducts);
            // The master plan must have the product in the list but not placed.
            masterPlan.AddProduct();
            masterPlan.Comparison.IgnoreNonPlacedProducts = true;
            masterPlan.Parent.CalculateMetadata(false, null);
            targetPlans.ForEach(planogram => planogram.Parent.CalculateMetadata(false, null));
            masterPlan.Comparison.UpdateResults(targetPlans);

            masterPlan.Parent.CalculateMetadata(false, null);

            PlanogramItemComparisonStatusType actual = masterPlan.Products.First().MetaComparisonStatus;
            Assert.AreEqual(expected, actual, expectation, expected, actual);
        }

        [Test]
        public void MetaComparisonStatus_WhenMixedChanges_ShouldBeChanged()
        {
            const String expectation = "When no planogram has any changes for the product it should be flagged as {0} but was {1}";
            const PlanogramItemComparisonStatusType expected = PlanogramItemComparisonStatusType.Changed;
            var targetPlans = new List<Planogram> { SetupComparisonPlan("TargetOne"), SetupComparisonPlan("TargetTwo",0) };
            Planogram masterPlan = SetupComparisonPlan("SourceOne");
            PlanogramFixtureItem sourceBayOne = masterPlan.FixtureItems.First();
            PlanogramProduct sourceProductOne = masterPlan.Products.First();
            sourceBayOne.GetPlanogramSubComponentPlacements().First().FixtureComponent.AddPosition(sourceBayOne, sourceProductOne);
            masterPlan.Parent.CalculateMetadata(false, null);
            targetPlans.ForEach(planogram => planogram.Parent.CalculateMetadata(false, null));
            masterPlan.Comparison.UpdateResults(targetPlans);

            masterPlan.Parent.CalculateMetadata(false, null);

            PlanogramItemComparisonStatusType actual = masterPlan.Products.First().MetaComparisonStatus;
            Assert.AreEqual(expected, actual, expectation, expected, actual);
        }

        private static Planogram SetupComparisonPlan(String packageName, Int32 productCount = 1)
        {
            Planogram targetPlanOne = packageName.CreatePackage().AddPlanogram();
            PlanogramFixtureItem targetBayOne = targetPlanOne.AddFixtureItem();
            PlanogramFixtureComponent targetShelfOne = targetBayOne.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (var i = 0; i < productCount; i++)
            {
                targetShelfOne.AddPosition(targetBayOne, targetPlanOne.AddProduct());
            }
            return targetPlanOne;
        }

        #endregion

        #endregion
    }
}