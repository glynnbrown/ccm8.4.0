﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27606 : L.Ineson
//      Create
// V8-27745 : A.Kuszyk
//  Added GetMerchandisingGroups method.
// V8-28228 : A.Kuszyk
//  Added CalculateMetadata tests.

#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added GetImageAsync tests.
// V8-2879 : J.Pickup
//      Added CalculateMetadata_MetaDos_EnsureFieldsRepresentAMinimumAndMaximumAndAverageValues.

#endregion

#region Version History: CCM802

// V8-29232: A.Silva
//      Added unit tests for Planogram.UpdateProductSequenceNumbers.

#endregion

#region Version History : CCM820
// V8-30873 : L.Ineson
//  Removed IsCarPark flag
// V8-31172 : A.Kuszyk
//  Added tests for UpdatePositionSequenceData.
// V8-32143 : A.Silva
//  Amended renumbering bays tests.

#endregion

#region Version History: v830
// V8-32521 : J.Pickup
//  Added Metadata test : MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added MetaCountOfProductsBuddied metadata test
// V8-32953 : L.Ineson
//  Updated following image changes
// CCM-18563 : A.Silva
//  Added tests to cover the work of the issue.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using Galleria.Ccm.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Moq;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramTests : TestBase<Planogram, PlanogramDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }
        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Data Access

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            //Had to hand write because the getdatatransfer takes in package dto

            PropertyInfo dtoProperty = typeof(PlanogramDto).GetProperty(propertyName);
            PropertyInfo modelProperty = typeof(Planogram).GetProperty(propertyName);

            Object value1 = TestDataHelper.GetValue1(modelProperty.PropertyType);

            Planogram model = NewModel();
            modelProperty.SetValue(model, value1, null);

            //create the dto
            PlanogramDto dto = GetDataTransferObject(model);

            //check the dto property value
            Object dtoValue = dtoProperty.GetValue(dto, null);
            if (modelProperty.PropertyType.IsEnum)
            {
                value1 = Convert.ToByte(value1);
            }
            Assert.AreEqual(value1, dtoValue, "Property value does not match");
        }

        protected new PlanogramDto GetDataTransferObject(Planogram model)
        {
            MethodInfo getDtoMethod = typeof(Planogram).GetMethod("GetDataTransferObject", BindingFlags.NonPublic | BindingFlags.Instance);

            PackageDto parentValue = new PackageDto()
                {
                    Name = "pk"
                };
            return (PlanogramDto)getDtoMethod.Invoke(model, new Object[] { parentValue });
        }

        #endregion

        #region Methods

        #region GetMerchandisableGroups

        [Test]
        public void GetMerchandisableGroups_ReturnsOneGroup_WhenShelvesInALine()
        {
            var planogram = CreatePlanogram(5);
            for (int i = 0; i < 5; i++)
			{
                var shelf = CreateShelf(planogram);
                var fixture = planogram.Fixtures.ElementAt(i);
                var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(shelf);
                fixtureComponent.Y = 100;
                fixture.Components.Add(fixtureComponent);
			}

            using (var groups = planogram.GetMerchandisingGroups())
            {
                Assert.AreEqual(1, groups.Count());
            }
        }

        [Test]
        public void GetMerchandisableGroups_GroupContainsAllSubComps_WhenShelvesInALine()
        {
            var planogram = CreatePlanogram(5);
            for (int i = 0; i < 5; i++)
            {
                var shelf = CreateShelf(planogram);
                var fixture = planogram.Fixtures.ElementAt(i);
                var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(shelf);
                fixtureComponent.Y = 100;
                fixture.Components.Add(fixtureComponent);
            }

            using (var groups = planogram.GetMerchandisingGroups())
            {
                var groupSubComponents = groups.First().SubComponentPlacements.Select(sp => sp.SubComponent);
                var planogramSubComponents = planogram.Components.Reverse().Take(5).SelectMany(c => c.SubComponents);
                CollectionAssert.AreEquivalent(planogramSubComponents, groupSubComponents);
            }
        }

        [Test]
        public void GetMerchandisableGroups_ReturnsTwoGroups_WhenShelvesInTwoLines()
        {
            var planogram = CreatePlanogram(5);
            for (int i = 0; i < 5; i++)
            {
                var shelf = CreateShelf(planogram);
                var fixture = planogram.Fixtures.ElementAt(i);
                var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(shelf);
                fixtureComponent.Y = i<2 ? 100 : 200;
                fixture.Components.Add(fixtureComponent);
            }

            using (var groups = planogram.GetMerchandisingGroups())
            {
                Assert.AreEqual(2, groups.Count());
            }
        }

        [Test]
        public void GetMerchandisableGroups_GroupsContainAllSubComps_WhenShelvesInTwoLines()
        {
            var planogram = CreatePlanogram(5);
            for (int i = 0; i < 5; i++)
            {
                var shelf = CreateShelf(planogram);
                var fixture = planogram.Fixtures.ElementAt(i);
                var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(shelf);
                fixtureComponent.Y = i<2 ? 100 : 200;
                fixture.Components.Add(fixtureComponent);
            }

            using (var groups = planogram.GetMerchandisingGroups())
            {
                var group1SubComponents = groups.First().SubComponentPlacements.Select(sp => sp.SubComponent).ToList();
                var expectedGroup1SubComponents = planogram.Components.Reverse().Take(5).Reverse().Take(2).SelectMany(c => c.SubComponents).ToList();
                CollectionAssert.AreEquivalent(expectedGroup1SubComponents, group1SubComponents);

                var group2SubComponents = groups.Last().SubComponentPlacements.Select(sp => sp.SubComponent).ToList();
                var expectedGroup2SubComponents = planogram.Components.Reverse().Take(3).SelectMany(c => c.SubComponents).ToList();
                CollectionAssert.AreEquivalent(expectedGroup2SubComponents, group2SubComponents);
            }
        }

        [Test]
        public void GetMerchandisableGroups_ReturnsAsManyGroupsAsShelves_WhenShelvesNotInLine()
        {
            var planogram = CreatePlanogram(5);
            for (int i = 0; i < 5; i++)
            {
                var shelf = CreateShelf(planogram);
                var fixture = planogram.Fixtures.ElementAt(i);
                var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(shelf);
                fixtureComponent.Y = i *10;
                fixture.Components.Add(fixtureComponent);
            }

            using (var groups = planogram.GetMerchandisingGroups())
            {
                Assert.AreEqual(5, groups.Count());
            }
        }

        [Test]
        public void GetMerchandisableGroups_GroupsContainAllSubComps_WhenShelvesNotInLine()
        {
            var planogram = CreatePlanogram(5);
            for (int i = 0; i < 5; i++)
            {
                var shelf = CreateShelf(planogram);
                var fixture = planogram.Fixtures.ElementAt(i);
                var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(shelf);
                fixtureComponent.Y = i * 10;
                fixture.Components.Add(fixtureComponent);
            }

            using (var groups = planogram.GetMerchandisingGroups())
            {
                var actual = groups.SelectMany(g => g.SubComponentPlacements.Select(sp => sp.SubComponent)).ToList();
                var expected = planogram.Components.Reverse().Take(5).SelectMany(c => c.SubComponents).ToList();
                CollectionAssert.AreEquivalent(expected, actual);
            }
        }

        [Test]
        public void GetMerchandisableGroups_DoesNotReturnGroupsForNonMerchandisableTypes()
        {
            // V8-29028 - checks that we do not get groups returned for non merchandisable subcomponents.
            var planogram = Planogram.NewPlanogram();
            planogram.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Panel);

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                Assert.AreEqual(0, merchandisingGroups.Count, "No groups should have been returned.");
            }
        }

        private static PlanogramComponent CreateShelf(Planogram planogram)
        {
            var component = PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, 120, 10, 50);
            component.SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            component.SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            planogram.Components.Add(component);
            return component;
        }

        private Planogram CreatePlanogram(Int16 bays)
        {
            var planogram = Planogram.NewPlanogram();

            for (Int16 i = 0; i < bays; i++)
            {
                var fixture = PlanogramFixture.NewPlanogramFixture();
                fixture.Height = 200;
                fixture.Width = 120;
                planogram.Fixtures.Add(fixture);

                var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
                fixtureItem.X = i * 120;
                fixtureItem.BaySequenceNumber = (Int16)(i+1);
                planogram.FixtureItems.Add(fixtureItem);
            }

            return planogram;
        }

        #endregion

        #region GetImageAsync

        [Test]
        public void GetImageLazy_WhenNoId_ShouldCallRealImageProvider()
        {
            Mock<IRealImageProvider> realImageProviderMock = new Mock<IRealImageProvider>();
            realImageProviderMock.Setup(imageProvider => imageProvider.GetImageAsync(It.IsAny<Object>(), PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null)).Returns(() => null);
            IRealImageProvider realImageProvider = realImageProviderMock.Object;
            PlanogramImagesHelper.RegisterRealImageProvider(realImageProvider);
            Planogram planogram = CreatePlanogram(1);

            planogram.GetImageLazy(null, null, PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null);

            realImageProviderMock.Verify(imageProvider => imageProvider.GetImageAsync(It.IsAny<Object>(), PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null), Times.Once);
        }

        [Test]
        public void GetImageLazy_WhenNoId_ShouldReturnIsLoadingFalse()
        {
            Planogram planogram = CreatePlanogram(1);

            Boolean isLoadingImages;
            planogram.GetImageLazy(out isLoadingImages, null, null, PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null);

            Assert.IsFalse(isLoadingImages);
        }

        [Test]
        public void GetImageLazy_WhenIdExists_ShouldReturnCorrectImage()
        {
            PlanogramImage expected = PlanogramImage.NewPlanogramImage();
            expected.Id = 0;
            PlanogramImage anotherImage = PlanogramImage.NewPlanogramImage();
            anotherImage.Id = 1;
            Planogram planogram = CreatePlanogram(1);
            planogram.Images.AddRange(new List<PlanogramImage>{expected,anotherImage});

            PlanogramImage actual = planogram.GetImageLazy(null, 0, PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null);

            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GetImageLazy_WhenIdNotExists_ShouldNotCallRealImageProvider()
        {
            PlanogramImage expected = PlanogramImage.NewPlanogramImage();
            expected.Id = 0;
            PlanogramImage anotherImage = PlanogramImage.NewPlanogramImage();
            anotherImage.Id = 1;
            Planogram planogram = CreatePlanogram(1);
            planogram.Images.AddRange(new List<PlanogramImage> { expected, anotherImage });
            Mock<IRealImageProvider> realImageProviderMock = new Mock<IRealImageProvider>();
            realImageProviderMock.Setup(imageProvider => imageProvider.GetImageAsync(It.IsAny<Object>(), PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null)).Returns(() => null);
            IRealImageProvider realImageProvider = realImageProviderMock.Object;
            PlanogramImagesHelper.RegisterRealImageProvider(realImageProvider);

            planogram.GetImageLazy(null, 2, PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null);

            realImageProviderMock.Verify(imageProvider => imageProvider.GetImageAsync(It.IsAny<Object>(), PlanogramImageFacingType.Front, PlanogramImageType.None, It.IsAny<Action<Object>>(), null), Times.Never);
        }

        //[Test]
        //public void GetImageLazy_WhenNoId_ShouldReturnRealImage()
        //{
        //    PlanogramImage expected = PlanogramImage.NewPlanogramImage();
        //    expected.Id = 0;
        //    Planogram planogram = CreatePlanogram(1);
        //    Mock<IRealImageProvider> realImageProviderMock = new Mock<IRealImageProvider>();
        //    realImageProviderMock.Setup(imageProvider => imageProvider.GetImageAsync()).Returns(() => null);
        //    IRealImageProvider realImageProvider = realImageProviderMock.Object;
        //    PlanogramHelper.RegisterRealImageProvider(realImageProvider);

        //    PlanogramImage actual = planogram.GetImageAsync(null);

        //    Assert.AreEqual(expected, actual);
        //}

        #endregion

        #region UpdatePositionSequenceData
        [Test]
        public void UpdatePositionSequenceData_UpdatesColoursIfProductIsInSequence()
        {
            Planogram plan = "Test".CreatePackage().AddPlanogram();

            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            shelf.AddPosition(bay);

            PlanogramSequenceGroup group1 = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            group1.Colour = 1;
            group1.Products.Add(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(plan.Products[0]));
            PlanogramSequenceGroup group2 = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            group2.Colour = 2;
            group2.Products.Add(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(plan.Products[1]));
            plan.Sequence.Groups.AddRange(new[] { group1, group2 });

            plan.UpdatePositionSequenceData();

            Assert.AreEqual(group1.Colour, plan.Positions[0].SequenceColour);
            Assert.AreEqual(group2.Colour, plan.Positions[1].SequenceColour);
        }

        [Test]
        public void UpdatePositionSequenceData_NullsColoursIfProductIsNotInSequence()
        {
            Planogram plan = "Test".CreatePackage().AddPlanogram();

            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay);
            shelf.AddPosition(bay);

            PlanogramSequenceGroup group1 = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            group1.Colour = 1;
            PlanogramSequenceGroup group2 = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            group2.Colour = 2;
            plan.Sequence.Groups.AddRange(new[] { group1, group2 });

            plan.UpdatePositionSequenceData();

            Assert.IsNull(plan.Positions[0].SequenceColour);
            Assert.IsNull(plan.Positions[1].SequenceColour);
        }

        [Test]
        public void UpdatePositionSequenceData_WhenProductInSequence_AssignsSequenceNumber()
        {
            //  The test planogram contains some planograms.
            Planogram plan = "Test".CreatePackage().AddPlanogram();
            IList<PlanogramSequenceGroup> sequenceGroups = plan.AddSequenceGroups(10);
            //  Each Sequence Group contains some products in sequence order.
            foreach (PlanogramSequenceGroup sequenceGroup in sequenceGroups)
            {
                sequenceGroup.AddSequenceGroupProducts(plan.AddProducts(10));
                var sequenceNumber = 0;
                foreach (PlanogramSequenceGroupProduct product in sequenceGroup.Products)
                {
                    product.SequenceNumber = ++sequenceNumber;
                }
            }
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            foreach (var prod in plan.Products)
            {
                shelf.AddPosition(bay, prod);
            }
            //  Confirm our testing props are in the right state for the test.
            Boolean groupsHaveUniqueSequenceNumbers = sequenceGroups.All(g => g.Products.Select(p => p.SequenceNumber).Distinct().Count() == g.Products.Count);
            if (!groupsHaveUniqueSequenceNumbers) Assert.Inconclusive("The sequence groups should have different colours to test correctly.");
            Boolean productsHaveNoSequenceNumber = plan.Positions.All(p => p.SequenceNumber == null);
            if (!productsHaveNoSequenceNumber) Assert.Inconclusive("The products should start without sequence numbers to test correctly.");

            plan.UpdatePositionSequenceData();

            IEnumerable<PlanogramSequenceGroupProduct> sequenceProducts = plan.Sequence.Groups.SelectMany(g => g.Products).ToList();
            Assert.IsTrue(plan.Positions.All(p => IsCorrectSequenceNumber(p, sequenceProducts)));
        }

        private static Boolean IsCorrectSequenceNumber(PlanogramPosition position, IEnumerable<PlanogramSequenceGroupProduct> sequenceProducts)
        {
            return sequenceProducts.SingleOrDefault(p => p.Gtin == position.GetPlanogramProduct().Gtin && p.SequenceNumber == position.SequenceNumber) != null;
        }

        [Test]
        public void UpdatePositionSequenceData_WhenMultisitedProductsAndBlockingExists_PositionsHaveCorrectSequenceNumberAndColour()
        {
            const Int32 numberOfShelves = 4;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            var expectedColoursByPosition = new Dictionary<PlanogramPosition, Int32?>();
            var expectedNumbersByPosition = new Dictionary<PlanogramPosition, Int32?>();
            for (Int32 i = 0; i < numberOfShelves; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, i * (bay.GetPlanogramFixture().Height / (Single)numberOfShelves), 0));
                var position1 = shelf.AddPosition(bay, product);
                var position2 = shelf.AddPosition(bay, product);
                expectedColoursByPosition.Add(position1, i);
                expectedNumbersByPosition.Add(position1, 1);
                expectedColoursByPosition.Add(position2, i);
                expectedNumbersByPosition.Add(position2, 1);
                var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = i;
                sequenceGroup.AddSequenceGroupProduct(product).SequenceNumber = 1;
                plan.Sequence.Groups.Add(sequenceGroup);
            }
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.25f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.75f, PlanogramBlockingDividerType.Horizontal);
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0f)).Colour = 0;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.25f)).Colour = 1;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.5f)).Colour = 2;
            blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.75f)).Colour = 3;

            plan.UpdatePositionSequenceData();

            foreach (var position in plan.Positions)
            {
                Assert.AreEqual(expectedColoursByPosition[position], position.SequenceColour);
                Assert.AreEqual(expectedNumbersByPosition[position], position.SequenceNumber);
            }
        }

        [Test]
        public void UpdatePositionSequenceData_WhenMultisitedProductsAndNoBlockingExists_PositionsHaveNullSequenceNumberAndColour()
        {
            const Int32 numberOfShelves = 4;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var product = plan.AddProduct();
            var expectedColoursByPosition = new Dictionary<PlanogramPosition, Int32?>();
            var expectedNumbersByPosition = new Dictionary<PlanogramPosition, Int32?>();
            for (Int32 i = 0; i < numberOfShelves; i++)
            {
                var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, i * (bay.GetPlanogramFixture().Height / (Single)numberOfShelves), 0));
                var position1 = shelf.AddPosition(bay, product);
                var position2 = shelf.AddPosition(bay, product);
                expectedColoursByPosition.Add(position1, null);
                expectedNumbersByPosition.Add(position1, null);
                expectedColoursByPosition.Add(position2, null);
                expectedNumbersByPosition.Add(position2, null);
                var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = i;
                sequenceGroup.AddSequenceGroupProduct(product).SequenceNumber = 1;
                plan.Sequence.Groups.Add(sequenceGroup);
            }

            plan.UpdatePositionSequenceData();

            foreach (var position in plan.Positions)
            {
                Assert.AreEqual(expectedColoursByPosition[position], position.SequenceColour);
                Assert.AreEqual(expectedNumbersByPosition[position], position.SequenceNumber);
            }
        }

        #endregion

        #region ResequenceBays

        [Test]
        public void ResequenceBaysShouldOrderSimplePlanByPlanogramCoordinates()
        {
            Planogram plan = "TestPackage".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItemOne = plan.AddFixtureItem(new RectValue(0, 0, 0, 10, 10, 10));
            PlanogramFixtureItem fixtureItemTwo = plan.AddFixtureItem(new RectValue(10, 0, 0, 10, 10, 10));

            plan.RenumberingStrategy.RenumberBays();

            Assert.AreEqual(1, fixtureItemOne.BaySequenceNumber);
            Assert.AreEqual(2, fixtureItemTwo.BaySequenceNumber);
        }

        [Test]
        public void ResequenceBaysShouldOrderSimplePlanByPlanogramCoordinatesRegardlessOfWhenFixtureWasAdded()
        {
            Planogram plan = "TestPackage".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItemTwo = plan.AddFixtureItem(new RectValue(10, 0, 0, 10, 10, 10));
            PlanogramFixtureItem fixtureItemOne = plan.AddFixtureItem(new RectValue(0, 0, 0, 10, 10, 10));

            plan.RenumberingStrategy.RenumberBays();

            Assert.AreEqual(1, fixtureItemOne.BaySequenceNumber);
            Assert.AreEqual(2, fixtureItemTwo.BaySequenceNumber);
        }

        [Test]
        public void ResequenceBaysShouldOrderSimplePlanByPlanogramCoordinatesIncludingfRotation()
        {
            Planogram plan = "TestPackage".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItemTwo = plan.AddFixtureItem(new RectValue(0, 0, 0, 10, 10, 10));
            PlanogramFixtureItem fixtureItemOne = plan.AddFixtureItem(new RectValue(0, 0, 0, 10, 10, 10));
            fixtureItemOne.Angle = 90;

            plan.RenumberingStrategy.RenumberBays();

            Assert.AreEqual(1, fixtureItemOne.BaySequenceNumber);
            Assert.AreEqual(2, fixtureItemTwo.BaySequenceNumber);
        }

        #endregion

        #region NormalizeBays

        [Test]
        public void NormalizeBaysShouldSetMinXCoordinateToZero()
        {
            Planogram plan = "TestPackage".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItemOne = plan.AddFixtureItem(new RectValue(-10, 0, 0, 10, 10, 10));
            PlanogramFixtureItem fixtureItemTwo = plan.AddFixtureItem(new RectValue(0, 0, 0, 10, 10, 10));

            plan.NormalizeBays();

            Assert.AreEqual(0, fixtureItemOne.X, "The first bay should have been moved to the X=0 coordinate.");
            Assert.AreEqual(fixtureItemOne.X + fixtureItemOne.GetPlanogramFixture().Width,
                            fixtureItemTwo.X,
                            "The second bay should have been moved next to the new position of the first bay.");
        }

        [Test]
        public void NormalizeBaysShouldSetMinYCoordinateToZero()
        {
            Planogram plan = "TestPackage".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItemOne = plan.AddFixtureItem(new RectValue(0, -10, 0, 10, 10, 10));
            PlanogramFixtureItem fixtureItemTwo = plan.AddFixtureItem(new RectValue(0, 0, 0, 10, 10, 10));

            plan.NormalizeBays();

            Assert.AreEqual(0, fixtureItemOne.Y, "The first bay should have been moved to the Y=0 coordinate.");
            Assert.AreEqual(fixtureItemOne.Y + fixtureItemOne.GetPlanogramFixture().Height,
                            fixtureItemTwo.Y,
                            "The second bay should have been moved next to the new position of the first bay.");
        }

        [Test]
        public void NormalizeBaysShouldSetMinZCoordinateToZero()
        {
            Planogram plan = "TestPackage".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItemOne = plan.AddFixtureItem(new RectValue(0, 0, -10, 10, 10, 10));
            PlanogramFixtureItem fixtureItemTwo = plan.AddFixtureItem(new RectValue(0, 0, 0, 10, 10, 10));

            plan.NormalizeBays();

            Assert.AreEqual(0, fixtureItemOne.Z, "The first bay should have been moved to the Z=0 coordinate.");
            Assert.AreEqual(fixtureItemOne.Z + fixtureItemOne.GetPlanogramFixture().Depth,
                            fixtureItemTwo.Z,
                            "The second bay should have been moved next to the new position of the first bay.");
        }

        #endregion

        [Test]
        public void GetMerchandisableGroupsInBlockingAreaReturnsOnlyMerchandiseGroupsInArea()
        {
            IEnumerable<RectValue> expectedMerchGroupRectValues;
            Planogram planogram = MerchandisableGroupsInBlockingAreaTestPlanogram(out expectedMerchGroupRectValues);

            IEnumerable<RectValue> actualMerchGroupRectValues;
            using (PlanogramMerchandisingGroupList merchGroups = planogram.GetMerchandisingGroupsInBlockingArea())
            {
                actualMerchGroupRectValues = merchGroups.Select(mg => mg.DesignViewPosition.ToRectValue2D());
            }

            expectedMerchGroupRectValues.Should().BeEquivalentTo(actualMerchGroupRectValues, because: "the merchandising blocks in the blocking area should have been the only ones returned");
        }

        private Planogram MerchandisableGroupsInBlockingAreaTestPlanogram(out IEnumerable<RectValue> expectedMerchGroupRectValues)
        {
            Planogram planogram = $"{nameof(MerchandisableGroupsInBlockingAreaTestPlanogram)}".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItem = planogram.AddFixtureItem();
            Single width = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf).SetCombined().GetPlanogramComponent().Width;
            fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(fixtureItem.X - width, 0, 0)).SetCombined();
            fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(fixtureItem.X - width, 50, 0));
            expectedMerchGroupRectValues = new List<RectValue> {new RectValue(-width, 0, 0, 2*width, 4, 1), new RectValue(0, 50, 0, width, 4, 1)};
            return planogram;
        }

        #endregion

        #region Metadata

        private Planogram CreateMetadataPlanogram()
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //add a position
            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 2;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;

            pos1.FacingsYHigh = 1;
            pos1.FacingsYWide = 1;
            pos1.FacingsYDeep = 2;
            pos1.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeY = PlanogramPositionOrientationType.Top0;

            pos1.FacingsXHigh = 2;
            pos1.FacingsXWide = 2;
            pos1.FacingsXDeep = 1;
            pos1.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeX = PlanogramPositionOrientationType.Right0;

            pos1.FacingsZHigh = 1;
            pos1.FacingsZWide = 1;
            pos1.FacingsZDeep = 2;
            pos1.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationTypeZ = PlanogramPositionOrientationType.Back0;

            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 2.7F;
            pos1.TotalUnits = 9;
            pos1.Sequence = 1;

            return planogram;
        }

        [Test]
        public void MetadataAllPropertiesTested()
        {
            String[] testMethodNames = this.GetType().GetMethods().Select(m => m.Name).ToArray();

            Boolean testsMissing = false;
            foreach (var property in typeof(Planogram).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    if (!testMethodNames.Any(m=> m.StartsWith(property.Name)))
                    {
                        testsMissing = true;
                        Debug.WriteLine(property.Name);
                    }
                }
            }

            if (testsMissing)
            {
                Assert.Ignore();
            }
        }

        [Test]
        public void ClearMetadata()
        {
            Planogram plan = CreateMetadataPlanogram();
            Package pk = plan.Parent;

            foreach (var property in typeof(Planogram).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    //set it
                    property.SetValue(plan, TestDataHelper.GetValue2(property.PropertyType), null);

                    //call clear
                    pk.ClearMetadata();

                    Assert.IsNull(property.GetValue(plan, null), property.Name);
                }
            }
        }

        [Test]
        public void MetaTotalMerchandisableLinearSpace_IsSumOfComponentWidths()
        {
            const Single componentWidth = 120;
            const Single noOfComponents = 5;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 0;
            const Single componentDepth = 75;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single actual = package.Planograms[0].MetaTotalMerchandisableLinearSpace.Value;
            Single expected = noOfComponents * componentWidth;

            Assert.That(expected.EqualTo(actual));
        }

        [Test]
        public void MetaTotalMerchandisableLinearSpace_IgnoresCarParkShelfBaseAndBackboard()
        {
            const Single componentWidth = 120;
            const Single noOfComponents = 5;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 0;
            const Single componentDepth = 75;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);


            var comp = PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, componentWidth, 4, 75);
            package.Planograms[0].Components.Add(comp);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single actual = package.Planograms[0].MetaTotalMerchandisableLinearSpace.Value;
            Single expected = noOfComponents * componentWidth;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalMerchandisableAreaSpace_IsPlanogramAreaMinusComponentArea_WhenCompMerchHeightIsZero()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single noOfComponents = 5;
            const Single componentDepth = 75;
            const Single componentMerchHeight = 0;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            var planogram = package.Planograms[0];
            Single actual = planogram.MetaTotalMerchandisableAreaSpace.Value;
            Single expected = planogram.Height * planogram.Width - noOfComponents * componentWidth * componentHeight;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalMerchandisableAreaSpace_IsSumOfComponentMerchandisableAreas()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single componentDepth = 75;
            const Single componentMerchHeight = 10;
            const Single noOfComponents = 5;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single actual = package.Planograms[0].MetaTotalMerchandisableAreaSpace.Value;
            Single expected = noOfComponents * componentWidth * componentMerchHeight;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalMerchandisableVolumetricSpace_IsPlanogramVolumeMinusComponentVolume_WhenCompMerchHeightIsZero()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single componentDepth = 75;
            const Single noOfComponents = 5;
            const Single componentMerchHeight = 0;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            var planogram = package.Planograms[0];
            Single actual = planogram.MetaTotalMerchandisableVolumetricSpace.Value;
            Single expected = planogram.Height * planogram.Width * planogram.Depth - noOfComponents * componentWidth * componentHeight * componentDepth;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalMerchandisableVolumetricSpace_IsSumOfComponentMerchandisableVolumes()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 10;
            const Single componentDepth = 75;
            const Single noOfComponents = 5;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single actual = package.Planograms[0].MetaTotalMerchandisableVolumetricSpace.Value;
            Single expected = noOfComponents * componentWidth * componentMerchHeight * componentDepth;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalLinearWhiteSpace_IsComponentWidthMinusPositionWidths()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 10;
            const Single componentDepth = 75;
            const Single noOfComponents = 1;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);
            var planogram = package.Planograms[0];

            var product = PlanogramProduct.NewPlanogramProduct();
            product.Width = 10;
            product.Height = 10;
            product.Depth = 10;
            planogram.Products.Add(product);

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                planogram.Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents.First(),
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First(c => c.PlanogramComponentId == planogram.Components.First(d => d.ComponentType == PlanogramComponentType.Shelf).Id));
            var position = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            planogram.Positions.Add(position);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single expected = 1 - product.Width / componentWidth;
            Single actual = planogram.MetaTotalLinearWhiteSpace.Value;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalLinearWhiteSpace_IsComponentWidthMinusPositionWidthsThatAreAtFront()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 10;
            const Single componentDepth = 75;
            const Single noOfComponents = 1;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);
            var planogram = package.Planograms[0];

            var product = PlanogramProduct.NewPlanogramProduct();
            product.Width = 10;
            product.Height = 10;
            product.Depth = 10;
            planogram.Products.Add(product);

            var subComponent = planogram.Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents.First();
            subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                subComponent,
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First(c => c.PlanogramComponentId == planogram.Components.First(d => d.ComponentType == PlanogramComponentType.Shelf).Id));

            var position1 = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position1.X = 0;
            position1.Z = 65;
            planogram.Positions.Add(position1);

            var position2 = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position2.X = 0;
            position2.Z = 0;
            planogram.Positions.Add(position2);


            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single expected = 1 - product.Width / componentWidth;
            Single actual = planogram.MetaTotalLinearWhiteSpace.Value;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalAreaWhiteSpace_IsComponentAreaMinusPositionAreasThatAreAtFront()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 10;
            const Single componentDepth = 75;
            const Single noOfComponents = 1;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);
            var planogram = package.Planograms[0];

            var product = PlanogramProduct.NewPlanogramProduct();
            product.Width = 10;
            product.Height = 10;
            product.Depth = 10;
            planogram.Products.Add(product);

            var subComponent = planogram.Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents.First();
            subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                subComponent,
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First(c => c.PlanogramComponentId == planogram.Components.First(d => d.ComponentType == PlanogramComponentType.Shelf).Id));

            var position1 = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position1.X = 0;
            position1.Z = 65;
            planogram.Positions.Add(position1);

            var position2 = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position2.X = 0;
            position2.Z = 0;
            planogram.Positions.Add(position2);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single expected = 1 - (product.Width * product.Height) / (componentWidth * componentMerchHeight);
            Single actual = planogram.MetaTotalAreaWhiteSpace.Value;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaTotalVolumetricWhiteSpace_IsComponentVolumeMinusSumOfPositionVolumes()
        {
            const Single componentWidth = 120;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 10;
            const Single componentDepth = 75;
            const Single noOfComponents = 1;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);
            var planogram = package.Planograms[0];

            var product = PlanogramProduct.NewPlanogramProduct();
            product.Width = 10;
            product.Height = 10;
            product.Depth = 10;
            planogram.Products.Add(product);

            var subComponent = planogram.Components.First(c => c.ComponentType == PlanogramComponentType.Shelf).SubComponents.First();
            subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                subComponent,
                planogram.FixtureItems.First(),
                planogram.Fixtures.First().Components.First(c => c.PlanogramComponentId == planogram.Components.First(d => d.ComponentType == PlanogramComponentType.Shelf).Id));

            var position1 = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position1.X = 0;
            position1.Z = 65;
            planogram.Positions.Add(position1);

            var position2 = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position2.X = 0;
            position2.Z = 0;
            planogram.Positions.Add(position2);

            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));
            Single expected = 1 - (product.Width * product.Height * product.Depth * 2) / (componentWidth * componentMerchHeight * componentDepth);
            Single actual = planogram.MetaTotalVolumetricWhiteSpace.Value;

            Assert.That(expected.EqualTo(actual), "Expected {0}, but was {1}", expected, actual);
        }

        [Test]
        public void MetaDos_EnsureFieldsRepresentAMinimumAndMaximumAndAverageValues()
        {
            //Create Package
            const Single componentWidth = 120;
            const Single noOfComponents = 5;
            const Single componentHeight = 4;
            const Single componentMerchHeight = 0;
            const Single componentDepth = 75;
            const Single fixtureHeight = 200;
            const Single fixtureDepth = 75;
            const Single fixtureWidth = 120;

            var package = CreateMetadataPackage(
                componentWidth, componentHeight, componentMerchHeight, componentDepth, noOfComponents,
                fixtureHeight, fixtureDepth, fixtureWidth);


            //Set values (DOS relies on caculated fields):
            package.Planograms[0].Performance.Metrics.FirstOrDefault().SpecialType = MetricSpecialType.RegularSalesUnits;
            package.Planograms[0].Inventory.DaysOfPerformance = 1;

            //Add 3 positions
            for (Int32 i = 1; i <= 3; i++)
            {
                PlanogramPosition newPosition = PlanogramPosition.NewPlanogramPosition();
                newPosition.TotalUnits = 100;
                newPosition.PlanogramProductId = i;
                package.Planograms[0].Positions.Add(newPosition);
            }

            //Add 3 performance data's
            for (Int32 i = 1; i <= 3; i++)
            {
                PlanogramPerformanceData newPerformanceData = PlanogramPerformanceData.NewPlanogramPerformanceData();
                newPerformanceData.UnitsSoldPerDay = i;
                newPerformanceData.PlanogramProductId = i;
                package.Planograms[0].Performance.PerformanceData.Add(newPerformanceData);
            }

            //set metric to trigger the update of DOS
            Int32 metricValue = 1;
            foreach (PlanogramPerformanceData currentData in package.Planograms[0].Performance.PerformanceData)
            {
                currentData.P1 = metricValue;
                metricValue++;
            }

            //No longer need posistions as were only used in calculating other fields triggered by P1 set.
            package.Planograms[0].Positions.Clear();

            //Calculate Metadata
            package.CalculateMetadata(true, PlanogramMetadataHelper.NewPlanogramMetadataHelper(0, package));

            Single actualMin = package.Planograms[0].MetaMinDos.Value;
            Single actualMax = package.Planograms[0].MetaMaxDos.Value;
            Single actualAvg = package.Planograms[0].MetaAverageDos.Value;

            //Min must be of upmost minimum value
            Assert.IsTrue(actualMin <= actualMax);
            Assert.IsTrue(actualMin <= actualAvg);

            //Max must be of upmost maximum value
            Assert.IsTrue(actualMax >= actualMin);
            Assert.IsTrue(actualMax >= actualAvg);

            //Average must be in the middle somehwere
            Assert.IsTrue(actualAvg >= actualMin);
            Assert.IsTrue(actualAvg <= actualMax);

            if (actualAvg == 0)
            {
                Assert.IsTrue(actualMin == 0);
                Assert.IsTrue(actualMax == 0);
            }
        }

        [Test]
        public void MetaPercentOfPlacedProductsRecommendedInAssortment()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //Add some assortment products & products
            planogram.AddAssortmentProducts(10);

            //range only the first 3
            for (Int32 i = 0; i < 4; i++) planogram.Assortment.Products[i].IsRanged = true;
            for (Int32 i = 4; i < 10; i++) planogram.Assortment.Products[i].IsRanged = false;

            //place the first 2 and the last 6  (ie. 2 ranged, 6 non ranged, 2 left unplaced)
            subComponentPlacement.AddPosition(planogram.Products[0]);
            subComponentPlacement.AddPosition(planogram.Products[1]);
            subComponentPlacement.AddPosition(planogram.Products[4]);
            subComponentPlacement.AddPosition(planogram.Products[5]);
            subComponentPlacement.AddPosition(planogram.Products[6]);
            subComponentPlacement.AddPosition(planogram.Products[7]);
            subComponentPlacement.AddPosition(planogram.Products[8]);
            subComponentPlacement.AddPosition(planogram.Products[9]);


            //calc metadata
            package.CalculateMetadata(false, null);

            //check
            Assert.AreEqual(0.5F, planogram.MetaPercentOfPlacedProductsRecommendedInAssortment);
        }

        [Test]
        public void MetaCountOfPlacedProductsRecommendedInAssortment()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //Add some assortment products & products
            planogram.AddAssortmentProducts(10);

            //range only the first 5
            for (Int32 i = 0; i < 5; i++) planogram.Assortment.Products[i].IsRanged = true;
            for (Int32 i = 5; i < 10; i++) planogram.Assortment.Products[i].IsRanged = false;

            //place the first 3 and the last 4 (ie. 3 ranged, 5 non ranged, 2 left unplaced)
            subComponentPlacement.AddPosition(planogram.Products[0]);
            subComponentPlacement.AddPosition(planogram.Products[1]);
            subComponentPlacement.AddPosition(planogram.Products[2]);
            subComponentPlacement.AddPosition(planogram.Products[5]);
            subComponentPlacement.AddPosition(planogram.Products[6]);
            subComponentPlacement.AddPosition(planogram.Products[7]);
            subComponentPlacement.AddPosition(planogram.Products[8]);
            subComponentPlacement.AddPosition(planogram.Products[9]);


            //calc metadata
            package.CalculateMetadata(false, null);

            //check
            Assert.AreEqual(3, planogram.MetaCountOfPlacedProductsRecommendedInAssortment);
        }

        [Test]
        public void MetaCountOfPlacedProductsNotRecommendedInAssortment()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //Add some assortment products & products
            planogram.AddAssortmentProducts(10);

            //range only the first 5
            for (Int32 i = 0; i < 5; i++) planogram.Assortment.Products[i].IsRanged = true;
            for (Int32 i = 5; i < 10; i++) planogram.Assortment.Products[i].IsRanged = false;

            //place the first 3 and the last 4 (ie. 3 ranged, 5 non ranged, 2 left unplaced)
            subComponentPlacement.AddPosition(planogram.Products[0]);
            subComponentPlacement.AddPosition(planogram.Products[1]);
            subComponentPlacement.AddPosition(planogram.Products[2]);
            subComponentPlacement.AddPosition(planogram.Products[5]);
            subComponentPlacement.AddPosition(planogram.Products[6]);
            subComponentPlacement.AddPosition(planogram.Products[7]);
            subComponentPlacement.AddPosition(planogram.Products[8]);
            subComponentPlacement.AddPosition(planogram.Products[9]);


            //calc metadata
            package.CalculateMetadata(false, null);

            //check
            Assert.AreEqual(5, planogram.MetaCountOfPlacedProductsNotRecommendedInAssortment);
        }


        [Test]
        public void MetaCountOfRecommendedProductsInAssortment()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //Add some assortment products & products
            planogram.AddAssortmentProducts(10);

            //range only the first 5
            for (Int32 i = 0; i < 5; i++) planogram.Assortment.Products[i].IsRanged = true;
            for (Int32 i = 5; i < 10; i++) planogram.Assortment.Products[i].IsRanged = false;


            //calc metadata
            package.CalculateMetadata(false, null);

            //check
            Assert.AreEqual(5, planogram.MetaCountOfRecommendedProductsInAssortment);

            //range another and recalc
            planogram.Assortment.Products[9].IsRanged = true;

            package.CalculateMetadata(false, null);
            Assert.AreEqual(6, planogram.MetaCountOfRecommendedProductsInAssortment);
        }

        private static Package CreateMetadataPackage(Single componentWidth, Single componentHeight, Single componentMerchHeight, Single componentDepth, Single noOfComponents, Single fixtureHeight, Single fixtureDepth, Single fixtureWidth)
        {
            var package = Package.NewPackage(1, PackageLockType.User);
            var planogram = Planogram.NewPlanogram();
            package.Planograms.Add(planogram);
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = fixtureWidth;
            fixture.Height = fixtureHeight;
            fixture.Depth = fixtureDepth;
            planogram.Fixtures.Add(fixture);
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture));
            planogram.Height = fixtureHeight;
            planogram.Width = fixtureWidth;
            planogram.Depth = fixtureDepth;

            for (Int32 i = 0; i < noOfComponents; i++)
            {
                var component = PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, componentWidth, componentHeight, componentDepth);
                foreach (PlanogramSubComponent subComponent in component.SubComponents)
                {
                    subComponent.MerchandisableHeight = componentMerchHeight;
                }
                planogram.Components.Add(component);
                var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
                fixtureComponent.Y = fixture.Height / noOfComponents * i;
                fixture.Components.Add(fixtureComponent);
            }
            return package;
        }

        [Category(Categories.Smoke)]
        [Test]
        public void MetaHasComponentsOutsideOfFixtureArea()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //Add some assortment products & products
            planogram.AddAssortmentProducts(10);

            //range only the first 5
            for (Int32 i = 0; i < 5; i++) planogram.Assortment.Products[i].IsRanged = true;
            for (Int32 i = 5; i < 10; i++) planogram.Assortment.Products[i].IsRanged = false;


            //calc metadata
            package.CalculateMetadata(false, null);

            Assert.IsNotNull(planogram.MetaHasComponentsOutsideOfFixtureArea, "MetaHasComponentsOutsideOfFixtureArea was null after being calculated");
            Assert.IsFalse(planogram.MetaHasComponentsOutsideOfFixtureArea.Value, "No components are currently outside fixture, yet is MetaHasComponentsOutsideOfFixtureArea returning true");

            //Make a change - move the component out.
            shelf1FC.X = 200f;

            package.CalculateMetadata(false, null);

            Assert.IsNotNull(planogram.MetaHasComponentsOutsideOfFixtureArea, "MetaHasComponentsOutsideOfFixtureArea was null after being calculated");
            Assert.IsTrue(planogram.MetaHasComponentsOutsideOfFixtureArea.Value, "component is MetaHasComponentsOutsideOfFixtureArea returning false.");
        }

        [Category(Categories.Smoke)]
        [Test]
        public void MetaCountOfProductsBuddied()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //Add some assortment products & products
            planogram.AddAssortmentProducts(10);

            //calc metadata
            package.CalculateMetadata(false, null);

            Assert.IsNotNull(planogram.MetaCountOfProductsBuddied, "MetaCountOfProductsBuddied was null after being calculated");

            //make the change - apply a buddy
            PlanogramAssortmentProduct prod = planogram.Assortment.Products[0];
            PlanogramAssortmentProduct buddiedProd = planogram.Assortment.Products[1];
            PlanogramAssortmentProductBuddy buddy = PlanogramAssortmentProductBuddy.NewPlanogramAssortmentProductBuddy(prod.Gtin);
            buddy.S1ProductGtin = buddiedProd.Gtin;
            buddy.S1Percentage = 1;
            planogram.Assortment.ProductBuddies.Add(buddy);

            package = package.Save();

            //calc metadata
            package.CalculateMetadata(false, null);

            planogram = package.Planograms[0];

            Assert.IsNotNull(planogram.MetaCountOfProductsBuddied, "MetaCountOfProductsBuddied was null after being calculated");
            Assert.IsTrue(planogram.MetaCountOfProductsBuddied.Value > 0, "a buddied product exists but meta field returns false.");
        }

        #endregion
    }
}