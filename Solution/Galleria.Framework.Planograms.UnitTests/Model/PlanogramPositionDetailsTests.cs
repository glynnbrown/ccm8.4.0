﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24971 : L.Ineson
//  Created
// V8-27946 : A.Kuszyk
//  Added GetReservedSpace tests.
// V8-28256 : A.Kuszyk
//  Added additional GetReservedSpace tests for coordinate values.
#endregion

#region Version History : CCM 801
// V8-28828 : A.Kuszyk
//  Fixed GetReservedSpace tests and added a larger range of parameters for better coverage.
#endregion

#region Version History : CCM 820
// V8-30818 : A.Kuszyk
//  Moved GetReserved tests for PlanogramPositionPlacement.
#endregion

#region Version History : CCM 830
// V8-32460 : M.Brumby
//  Added PegPositionSize_ProductDimensionMultipleOfPegSpaceing. Peg products that are an exact multiple of
//  the peg spacing value should have a spacing of 0 instead of the full peg spacing value.
#endregion
#endregion

using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.Enums;
using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramPositionDetailsTests : TestBase
    {
        private Planogram _planogram;
        private PlanogramFixture _fixture;
        private PlanogramFixtureItem _fixtureItem;

        #region Test Fixture Helpers

        private PlanogramPosition CreatePosition()
        {
            Package package = Package.NewPackage(1, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponent shelfSub = shelf1C.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            shelfSub.IsProductOverlapAllowed = true;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelfSub, fixtureItem, shelf1FC);

            //Product 1
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 9F;
            prod1.Width = 10F;
            prod1.Depth = 8F;
            prod1.TrayHigh = 1;
            prod1.TrayWide = 2;
            prod1.TrayDeep = 3;
            prod1.TrayHeight = 9F;
            prod1.TrayWidth = 19F;
            prod1.TrayDepth = 31F;
            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 0F;

            return pos1;
        }

        private PlanogramPosition CreatePegPosition()
        {
            Single testWidth = 8;
            Single testHeight = 9;
            Package package = Package.NewPackage(1, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent peg1FC = fixture.Components.Add(PlanogramComponentType.Peg, fixture.Width, 4, 76.5F);
            PlanogramComponent pegf1C = plan.Components.FindById(peg1FC.PlanogramComponentId);

            PlanogramSubComponent shelfSub = pegf1C.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            shelfSub.MerchConstraintRow1SpacingX = testWidth;
            shelfSub.MerchConstraintRow1SpacingY = testHeight;
            shelfSub.IsProductOverlapAllowed = true;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelfSub, fixtureItem, peg1FC);

            //Product 1
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = testHeight;
            prod1.Width = testWidth;
            prod1.Depth = 8F;
            prod1.NumberOfPegHoles = 1;
            prod1.PegX = testWidth / 2;
            prod1.PegY = 1;
            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 3;
            pos1.FacingsWide = 3;
            pos1.FacingsDeep = 1;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 0F;

            return pos1;
        }
        [SetUp]
        public void SetUp()
        {
            _planogram = Planogram.NewPlanogram();
            _fixture = CreateFixture(_planogram, 200, 120, 75);
            _fixtureItem = AddFixtureItem(_planogram, _fixture);
        }

        private static void AssertRectValues(RectValue expected, RectValue actual)
        {
                Assert.AreEqual(expected, actual,
                String.Format("Expected XYZ: [{0},{1},{2}] WHD: [{3},{4},{5}], but was XYZ: [{6},{7},{8}] WHD: [{9},{10},{11}]",
                expected.X, expected.Y, expected.Z,
                expected.Width, expected.Height, expected.Depth,
                actual.X, actual.Y, actual.Z,
                actual.Width, actual.Height, actual.Depth));
        }

        private static PlanogramComponent AddComponent(
            Single dividerSize, 
            Single dividerSpacing, 
            Single dividerStart,
            Boolean divAtStart, 
            Boolean divAtEnd, 
            Boolean divByFacing, 
            Single compWidth, 
            Single compHeight, 
            Single compDepth, 
            PlanogramComponentType compType, 
            Planogram planogram)
        {
            var component = PlanogramComponent.NewPlanogramComponent("Shelf", compType, compWidth, compHeight, compDepth);
            var subComponent = component.SubComponents.First();
            planogram.Components.Add(component);
            subComponent = component.SubComponents.First();
            subComponent.IsDividerObstructionAtStart = divAtStart;
            subComponent.IsDividerObstructionByFacing = divByFacing;
            subComponent.IsDividerObstructionAtEnd = divAtEnd;
            subComponent.DividerObstructionDepth = dividerSize;
            subComponent.DividerObstructionHeight = dividerSize;
            subComponent.DividerObstructionWidth = dividerSize;
            subComponent.DividerObstructionSpacingX = dividerSpacing;
            subComponent.DividerObstructionSpacingY = dividerSpacing;
            subComponent.DividerObstructionSpacingZ = dividerSpacing;
            subComponent.DividerObstructionStartX = dividerStart;
            subComponent.DividerObstructionStartY = dividerStart;
            subComponent.DividerObstructionStartZ = dividerStart;
            return component;
        }

        private static PlanogramPosition AddPosition(
            Byte facings, Planogram planogram, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, Int16 sequence = 1)
        {
            var position = PlanogramPosition.NewPlanogramPosition(sequence, product, subComponentPlacement);
            position.FacingsDeep = facings;
            position.FacingsHigh = facings;
            position.FacingsWide = facings;
            position.SequenceX = sequence;
            position.SequenceY = sequence;
            position.SequenceZ = sequence;
            planogram.Positions.Add(position);
            return position;
        }

        private static PlanogramProduct AddProduct(Single productSize, Single fingerSpacing, Planogram planogram)
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Height = productSize;
            product.Width = productSize;
            product.Depth = productSize;
            product.FingerSpaceToTheSide = fingerSpacing;
            planogram.Products.Add(product);
            return product;
        }

        private static PlanogramFixtureItem AddFixtureItem(Planogram planogram, PlanogramFixture fixture)
        {
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);
            return fixtureItem;
        }

        private static PlanogramFixtureComponent AddFixtureComponent(PlanogramComponent component, PlanogramFixture fixture)
        {
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixture.Components.Add(fixtureComponent);
            return fixtureComponent;
        }

        private static PlanogramFixture CreateFixture(Planogram planogram, Single fixHeight, Single fixWidth, Single fixDepth)
        {
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Height = fixHeight;
            fixture.Width = fixWidth;
            fixture.Depth = fixDepth;
            planogram.Fixtures.Add(fixture);
            return fixture;
        }

        #endregion

        [Test]
        public void PositionAndSize_MainBlockOnly()
        {
            PlanogramPosition pos = CreatePosition();
            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsFalse(info.HasXBlock);
            Assert.IsFalse(info.HasYBlock);
            Assert.IsFalse(info.HasZBlock);

            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(0, info.MainCoordinates.Z);

            Assert.AreEqual(10F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.MainTotalSize.Width);
            Assert.AreEqual(9F, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            Assert.AreEqual(10F, info.TotalSize.Width);
            Assert.AreEqual(9F, info.TotalSize.Height);
            Assert.AreEqual(8F, info.TotalSize.Depth);
        }

        [Test]
        public void PositionAndSize_XLeft()
        {
            PlanogramPosition pos = CreatePosition();

            pos.IsXPlacedLeft = true;
            pos.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            pos.FacingsXHigh = 1;
            pos.FacingsXWide = 1;
            pos.FacingsXDeep = 1;

            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsTrue(info.HasXBlock);
            Assert.IsFalse(info.HasYBlock);
            Assert.IsFalse(info.HasZBlock);

            //Main block
            Assert.AreEqual(8F, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(2F, info.MainCoordinates.Z);

            Assert.AreEqual(10F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.MainTotalSize.Width);
            Assert.AreEqual(9F, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            //X block
            Assert.AreEqual(0, info.XCoordinates.X);
            Assert.AreEqual(0, info.XCoordinates.Y);
            Assert.AreEqual(0, info.XCoordinates.Z);

            Assert.AreEqual(8F, info.XRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.XRotatedUnitSize.Height);
            Assert.AreEqual(10F, info.XRotatedUnitSize.Depth);

            Assert.AreEqual(8F, info.XTotalSize.Width);
            Assert.AreEqual(9F, info.XTotalSize.Height);
            Assert.AreEqual(10F, info.XTotalSize.Depth);

            Assert.AreEqual(18F, info.TotalSize.Width);
            Assert.AreEqual(9F, info.TotalSize.Height);
            Assert.AreEqual(10F, info.TotalSize.Depth);
        }

        [Test]
        public void PositionAndSize_XRight()
        {
            PlanogramPosition pos = CreatePosition();

            pos.IsXPlacedLeft = false;
            pos.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            pos.FacingsXHigh = 1;
            pos.FacingsXWide = 1;
            pos.FacingsXDeep = 1;

            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsTrue(info.HasXBlock);
            Assert.IsFalse(info.HasYBlock);
            Assert.IsFalse(info.HasZBlock);

            //Main block
            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(2F, info.MainCoordinates.Z);

            Assert.AreEqual(10F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.MainTotalSize.Width);
            Assert.AreEqual(9F, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            //X block
            Assert.AreEqual(10F, info.XCoordinates.X);
            Assert.AreEqual(0, info.XCoordinates.Y);
            Assert.AreEqual(0, info.XCoordinates.Z);

            Assert.AreEqual(8F, info.XRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.XRotatedUnitSize.Height);
            Assert.AreEqual(10F, info.XRotatedUnitSize.Depth);

            Assert.AreEqual(8F, info.XTotalSize.Width);
            Assert.AreEqual(9F, info.XTotalSize.Height);
            Assert.AreEqual(10F, info.XTotalSize.Depth);

            Assert.AreEqual(18F, info.TotalSize.Width);
            Assert.AreEqual(9F, info.TotalSize.Height);
            Assert.AreEqual(10F, info.TotalSize.Depth);
        }

        [Test]
        public void PositionAndSize_YTop()
        {
            PlanogramPosition pos = CreatePosition();

            pos.IsYPlacedBottom = false;
            pos.OrientationTypeY = PlanogramPositionOrientationType.Top0;
            pos.FacingsYHigh = 1;
            pos.FacingsYWide = 1;
            pos.FacingsYDeep = 1;

            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsFalse(info.HasXBlock);
            Assert.IsTrue(info.HasYBlock);
            Assert.IsFalse(info.HasZBlock);

            //Main block
            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(1F, info.MainCoordinates.Z);

            Assert.AreEqual(10F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.MainTotalSize.Width);
            Assert.AreEqual(9F, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            //Y Block
            Assert.AreEqual(0, info.YCoordinates.X);
            Assert.AreEqual(9F, info.YCoordinates.Y);
            Assert.AreEqual(0, info.YCoordinates.Z);

            Assert.AreEqual(10F, info.YRotatedUnitSize.Width);
            Assert.AreEqual(8F, info.YRotatedUnitSize.Height);
            Assert.AreEqual(9F, info.YRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.YTotalSize.Width);
            Assert.AreEqual(8F, info.YTotalSize.Height);
            Assert.AreEqual(9F, info.YTotalSize.Depth);

            Assert.AreEqual(10F, info.TotalSize.Width);
            Assert.AreEqual(17F, info.TotalSize.Height);
            Assert.AreEqual(9F, info.TotalSize.Depth);
        }

        [Test]
        public void PositionAndSize_YBottom()
        {
            PlanogramPosition pos = CreatePosition();

            pos.IsYPlacedBottom = true;
            pos.OrientationTypeY = PlanogramPositionOrientationType.Top0;
            pos.FacingsYHigh = 1;
            pos.FacingsYWide = 1;
            pos.FacingsYDeep = 1;

            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsFalse(info.HasXBlock);
            Assert.IsTrue(info.HasYBlock);
            Assert.IsFalse(info.HasZBlock);

            //Main block
            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(8F, info.MainCoordinates.Y);
            Assert.AreEqual(1F, info.MainCoordinates.Z);

            Assert.AreEqual(10F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.MainTotalSize.Width);
            Assert.AreEqual(9F, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            //Y Block
            Assert.AreEqual(0, info.YCoordinates.X);
            Assert.AreEqual(0, info.YCoordinates.Y);
            Assert.AreEqual(0, info.YCoordinates.Z);

            Assert.AreEqual(10F, info.YRotatedUnitSize.Width);
            Assert.AreEqual(8F, info.YRotatedUnitSize.Height);
            Assert.AreEqual(9F, info.YRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.YTotalSize.Width);
            Assert.AreEqual(8F, info.YTotalSize.Height);
            Assert.AreEqual(9F, info.YTotalSize.Depth);

            Assert.AreEqual(10F, info.TotalSize.Width);
            Assert.AreEqual(17F, info.TotalSize.Height);
            Assert.AreEqual(9F, info.TotalSize.Depth);
        }

        [Test]
        public void PositionAndSize_ZBack()
        {
            PlanogramPosition pos = CreatePosition();

            pos.IsZPlacedFront = false;
            pos.OrientationTypeZ = PlanogramPositionOrientationType.Front0;
            pos.FacingsZHigh = 1;
            pos.FacingsZWide = 1;
            pos.FacingsZDeep = 1;

            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsFalse(info.HasXBlock);
            Assert.IsFalse(info.HasYBlock);
            Assert.IsTrue(info.HasZBlock);

            //Main block
            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(8F, info.MainCoordinates.Z);

            Assert.AreEqual(10F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.MainTotalSize.Width);
            Assert.AreEqual(9F, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            //Z Block
            Assert.AreEqual(0, info.ZCoordinates.X);
            Assert.AreEqual(0, info.ZCoordinates.Y);
            Assert.AreEqual(0, info.ZCoordinates.Z);

            Assert.AreEqual(10F, info.ZRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.ZRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.ZRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.ZTotalSize.Width);
            Assert.AreEqual(9F, info.ZTotalSize.Height);
            Assert.AreEqual(8F, info.ZTotalSize.Depth);

            Assert.AreEqual(10F, info.TotalSize.Width);
            Assert.AreEqual(9F, info.TotalSize.Height);
            Assert.AreEqual(16F, info.TotalSize.Depth);
        }

        [Test]
        public void PositionAndSize_ZFront()
        {
            PlanogramPosition pos = CreatePosition();

            pos.IsZPlacedFront = true;
            pos.OrientationTypeZ = PlanogramPositionOrientationType.Front0;
            pos.FacingsZHigh = 1;
            pos.FacingsZWide = 1;
            pos.FacingsZDeep = 1;

            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsFalse(info.HasXBlock);
            Assert.IsFalse(info.HasYBlock);
            Assert.IsTrue(info.HasZBlock);

            //Main block
            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(0, info.MainCoordinates.Z);

            Assert.AreEqual(10F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.MainTotalSize.Width);
            Assert.AreEqual(9F, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            //Z Block
            Assert.AreEqual(0, info.ZCoordinates.X);
            Assert.AreEqual(0, info.ZCoordinates.Y);
            Assert.AreEqual(8F, info.ZCoordinates.Z);

            Assert.AreEqual(10F, info.ZRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.ZRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.ZRotatedUnitSize.Depth);

            Assert.AreEqual(10F, info.ZTotalSize.Width);
            Assert.AreEqual(9F, info.ZTotalSize.Height);
            Assert.AreEqual(8F, info.ZTotalSize.Depth);

            Assert.AreEqual(10F, info.TotalSize.Width);
            Assert.AreEqual(9F, info.TotalSize.Height);
            Assert.AreEqual(16F, info.TotalSize.Depth);
        }

        [Test]
        public void TopCapOverRight()
        {
            PlanogramPosition pos = CreatePosition();

            pos.FacingsHigh = 1;
            pos.FacingsWide = 1;
            pos.FacingsDeep = 1;

            pos.FacingsXHigh = 1;
            pos.FacingsXWide = 2;
            pos.FacingsXDeep = 1;

            pos.FacingsYHigh = 1;
            pos.FacingsYWide = 2;
            pos.FacingsYDeep = 1;

            PlanogramPositionDetails info = pos.GetPositionDetails();

            //check overall size
            Assert.AreEqual(26F, info.TotalSize.Width);
            Assert.AreEqual(17F, info.TotalSize.Height);
            Assert.AreEqual(10F, info.TotalSize.Depth);

            //check block positions
            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(2, info.MainCoordinates.Z);

            Assert.AreEqual(10, info.XCoordinates.X);
            Assert.AreEqual(0, info.XCoordinates.Y);
            Assert.AreEqual(0, info.XCoordinates.Z);

            Assert.AreEqual(0, info.YCoordinates.X);
            Assert.AreEqual(9, info.YCoordinates.Y);
            Assert.AreEqual(1, info.YCoordinates.Z);


            //increase the x block height and check that the y block gets pushed up.
            pos.FacingsXHigh = 2;
            info = pos.GetPositionDetails();
            Assert.AreEqual(18, info.YCoordinates.Y);
        }

        [Test]
        public void PegPositionSize_ProductDimensionMultipleOfPegSpaceing()
        {
            PlanogramPosition pos = CreatePegPosition();
            PlanogramPositionDetails info = pos.GetPositionDetails();

            Assert.IsFalse(info.HasXBlock);
            Assert.IsFalse(info.HasYBlock);
            Assert.IsFalse(info.HasZBlock);

            Assert.AreEqual(0, info.MainCoordinates.X);
            Assert.AreEqual(0, info.MainCoordinates.Y);
            Assert.AreEqual(0, info.MainCoordinates.Z);

            Assert.AreEqual(8F, info.MainRotatedUnitSize.Width);
            Assert.AreEqual(9F, info.MainRotatedUnitSize.Height);
            Assert.AreEqual(8F, info.MainRotatedUnitSize.Depth);

            Assert.AreEqual(8 * 3, info.MainTotalSize.Width);
            Assert.AreEqual(9 * 3, info.MainTotalSize.Height);
            Assert.AreEqual(8F, info.MainTotalSize.Depth);

            Assert.AreEqual(8 * 3, info.TotalSize.Width);
            Assert.AreEqual(9 * 3, info.TotalSize.Height);
            Assert.AreEqual(8F, info.TotalSize.Depth);
        }
    }
}
