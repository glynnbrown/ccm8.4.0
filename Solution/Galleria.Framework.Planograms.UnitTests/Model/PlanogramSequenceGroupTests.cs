﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created
// V8-28999 : A.Silva
//      Added IsColourMatch and SequenceProducts tests.
// V8-29105 : A.Silva
//      Added unit tests for GetPreviousSequenceProduct and GetNextSequenceProduct.
// V8-29309 : A.Silva
//      Added SequenceProducts_WhenCombinedShelves_ShouldSequenceAllProducts test.

#endregion

#region Version History: CCM810

// V8-29585 : A.Silva
//  Refactored IsReversed so that !IsPositive is used instead.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramSequenceGroupTests : TestBase
    {
        private Planogram _planogram;
        private Package _package;

        private PlanogramSequenceGroupList TestItems
        {
            get
            {
                if (_package == null) return null;
                Planogram planogram = _package.Planograms.FirstOrDefault();
                return planogram != null
                           ? planogram.Sequence.Groups
                           : null;
            }
        }

        private PlanogramSequenceGroup TestItem
        {
            get
            {
                return TestItems != null
                           ? TestItems.FirstOrDefault()
                           : null;
            }
        }

        [SetUp]
        public void TestSetup()
        {
            _package = "Test".CreatePackage();
            _planogram = _package.AddPlanogram();
        }

        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramSequenceGroup.NewPlanogramSequenceGroup());
        }

        [Test]
        public void WhenNewSubGroupAdded_GetsAssignedNewName()
        {
            PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            PlanogramSequenceGroupSubGroup subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup();

            sequenceGroup.SubGroups.Add(subGroup);

            Assert.That(String.IsNullOrWhiteSpace(subGroup.Name), Is.False);
        }

        [Test]
        public void WhenSubGroupNamesAreDuplicate_DuplicateNamesAreResolved()
        {
            PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            sequenceGroup.SubGroups.Add(PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup());
            PlanogramSequenceGroupSubGroup subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup();
            sequenceGroup.SubGroups.Add(subGroup);

            subGroup.Name = sequenceGroup.SubGroups.First().Name;

            Assert.That(sequenceGroup.SubGroups.Select(g => g.Name).Distinct().Count(), Is.EqualTo(sequenceGroup.SubGroups.Count));
        }

        [Test]
        public void WhenSubGroupProductIsUnAssigned_EmptySubGroupsAreRemoved()
        {
            PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup();
            sequenceGroup.SubGroups.Add(subGroup);
            var sequenceProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
            sequenceProduct.PlanogramSequenceGroupSubGroupId = subGroup.Id;
            sequenceGroup.Products.Add(sequenceProduct);

            sequenceProduct.PlanogramSequenceGroupSubGroupId = null;

            Assert.That(sequenceGroup.SubGroups, Is.Empty);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewPlanogramSequenceGroup()
        {
            //  Create a new planogram sequence group.
            PlanogramSequenceGroup testModel = PlanogramSequenceGroup.NewPlanogramSequenceGroup();

            Assert.IsTrue(testModel.IsNew);
            Assert.IsTrue(testModel.IsChild);
        }

        [Test]
        public void Fetch()
        {
            PlanogramSequenceGroup expected = _planogram.AddSequenceGroup();
            expected.Colour = 1;
            SavePackage();

            RefetchPackage();

            CollectionAssert.IsNotEmpty(TestItems);
            PlanogramSequenceGroup actual = TestItem;
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Colour, actual.Colour);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            // Already tested when testing Fetch.
        }

        [Test]
        public void DataAccess_Update()
        {
            PlanogramSequenceGroup original = _planogram.AddSequenceGroup();
            original.Colour = 1;
            SavePackage();

            //  Update the group.
            PlanogramSequenceGroup expected = TestItem;
            expected.Name = "UpdatedName";
            expected.Colour = 2;
            SavePackage();

            RefetchPackage();

            CollectionAssert.IsNotEmpty(TestItems);
            PlanogramSequenceGroup actual = TestItems[0];
            Assert.AreNotEqual(original.Name, actual.Name);
            Assert.AreNotEqual(original.Colour, actual.Colour);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Colour, actual.Colour);
        }

        [Test]
        public void DataAcces_Delete()
        {
            PlanogramSequenceGroup expected = _planogram.AddSequenceGroup();
            expected.Colour = 1;
            SavePackage();

            //  Delete the Planogram.
            _package.Planograms.Remove(_planogram);
            SavePackage();

            Assert.IsNull(FetchDtoByModel(expected));
        }

        #endregion

        #region Methods

        #region IsColourMatch

        [Test]
        public void IsColourMatch_WhenSameColour_ShouldBeTrue()
        {
            const String expectation = "When the colours are the same, IsColourMatch should be true.";
            _planogram.AddSequenceGroup();
            PlanogramBlocking blocking = _planogram.AddBlocking();
            PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
            TestItem.Colour = BlockingHelper.GetNextBlockingGroupColour(blocking);
            blockingGroup.Colour = TestItem.Colour;

            Boolean actual = TestItem.IsColourMatch(blockingGroup);

            Assert.IsTrue(actual, expectation);
        }

        [Test]
        public void IsColourMatch_WhenDifferentColour_ShouldBeFalse()
        {
            const String expectation = "When the colours are different, IsColourMatch should be false.";
            _planogram.AddSequenceGroup();
            PlanogramBlocking blocking = _planogram.AddBlocking();
            PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
            TestItem.Colour = BlockingHelper.GetNextBlockingGroupColour(blocking);
            blocking.Groups.Add(blockingGroup);

            Boolean actual = TestItem.IsColourMatch(blockingGroup);

            Assert.IsFalse(actual, expectation);
        }

        [Test]
        public void IsColourMatch_WhenBlockingGroupNull_ShouldBeFalse()
        {
            const String expectation = "When the the blocking group is null, IsColourMatch should be false.";
            _planogram.AddSequenceGroup();
            PlanogramBlocking blocking = _planogram.AddBlocking();
            TestItem.Colour = BlockingHelper.GetNextBlockingGroupColour(blocking);
            
            Boolean actual = TestItem.IsColourMatch(null);

            Assert.IsFalse(actual, expectation);
        }

        #endregion

        #region SequenceProducts

        [Test]
        public void SequenceProducts_WhenCalled_ShouldSequenceProducts_InChest(
            [Values(PlanogramBlockingGroupPlacementType.LeftToRight, PlanogramBlockingGroupPlacementType.RightToLeft)]
            PlanogramBlockingGroupPlacementType sequenceX,
            [Values(PlanogramBlockingGroupPlacementType.BackToFront, PlanogramBlockingGroupPlacementType.FrontToBack)]
            PlanogramBlockingGroupPlacementType sequenceZ,
            [Values(PlanogramSubComponentXMerchStrategyType.LeftStacked, PlanogramSubComponentXMerchStrategyType.RightStacked)]
            PlanogramSubComponentXMerchStrategyType xStrategy,
            [Values(PlanogramSubComponentZMerchStrategyType.FrontStacked, PlanogramSubComponentZMerchStrategyType.BackStacked)]
            PlanogramSubComponentZMerchStrategyType zStrategy)
        {
            const Int16 rows = 3;
            const Int16 products = 5;
            const String expectation = "When called should sequence products correctly.";
            PlanogramFixtureItem bay = _planogram.AddFixtureItem();
            PlanogramFixtureComponent chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            chest.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = xStrategy;
            chest.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyZ = zStrategy;
            Random random = new Random();
            for (Int16 r = 1; r <= rows; r++)
            {
                for (Int16 p = 1; p <= products; p++)
                {
                    Single sizeOffset = (random.Next(1, 10) - 5) / 2f;
                    var position = chest.AddPosition(bay, _planogram.AddProduct(
                        new WidthHeightDepthValue(20+sizeOffset, 20+sizeOffset, 20+sizeOffset)));
                    position.SequenceX = p;
                    position.SequenceZ = r;
                }
            }
            var orderedRows = _planogram.Positions.GroupBy(p => p.SequenceZ);
            orderedRows = sequenceZ.IsPositive()
                              ? orderedRows.OrderBy(p => p.Key)
                              : orderedRows.OrderByDescending(p => p.Key);
            var expectedGtins = SequenceRows(sequenceX, orderedRows)
                .SelectMany(ps => ps.Select(p => p.GetPlanogramProduct().Gtin))
                .ToList();
            using (var merchGroups = _planogram.GetMerchandisingGroups()) { merchGroups.ForEach(g => { g.Process(); g.ApplyEdit(); }); }
            _planogram.AddBlocking().AddGroup().AddLocation(0, 1, 0, 1);
            _planogram.Blocking.First().Groups.First().BlockPlacementPrimaryType = sequenceX;
            _planogram.Blocking.First().Groups.First().BlockPlacementSecondaryType = sequenceZ;
            _planogram.Blocking.First().Groups.First().BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;

            _planogram.Sequence.ApplyFromBlocking(_planogram.Blocking.First()); // This calls SequenceProducts

            WriteList("Expected:", expectedGtins);
            var sequencedGtins = _planogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
            WriteList("Actual:", sequencedGtins);
            CollectionAssert.AreEqual(expectedGtins, sequencedGtins, expectation);
        }
            
        [Test]
        public void SequenceProduct_WhenPegboardTopToBottom_ShouldSequenceCorrectly()
        {
            PlanogramFixtureItem bay = _planogram.AddFixtureItem();
            PlanogramFixtureComponent pegboard = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            PlanogramSubComponent component = pegboard.GetPlanogramComponent().SubComponents.First();
            component.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            component.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            IList<PlanogramProduct> products = _planogram.AddProducts(20);
            Int16 seqX = 0;
            Int16 seqY = 0;
            List<PlanogramPosition> positions = products.Select(product => pegboard.AddPosition(bay, product)).ToList();
            foreach (PlanogramPosition position in positions) 
            {
                position.SequenceX = ++seqX;
                position.SequenceY = seqY;
                if (seqX != 6) continue;
                seqX = 0;
                seqY++;
            }
            using (var merchGroups = _planogram.GetMerchandisingGroups()) { merchGroups.ForEach(g => { g.Process(); g.ApplyEdit(); }); }
            PlanogramBlockingGroup blockingGroup = _planogram.AddBlocking(1, true).Groups.First();
            blockingGroup.AddLocation(0,1,0,1);
            blockingGroup.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            blockingGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.TopToBottom;
            blockingGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            _planogram.Sequence.Groups.First().AddSequenceGroupProducts(products);
            List<string> expected = positions.GroupBy(p => p.SequenceY).SelectMany((grouping, i) => i % 2 == 0 ? grouping : grouping.Reverse()).Select(p => p.GetPlanogramProduct().Gtin).ToList();

            using (PlanogramMerchandisingGroupList groups = _planogram.GetMerchandisingGroups()) 
            {
                _planogram.Sequence.Groups.First().SequenceProducts(blockingGroup, groups);
            }

            List<String> actual = _planogram.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin).ToList();
            CollectionAssert.AreEqual(expected, actual, "Products should have the correct sequence.");
        }

        [Test]
        public void SequenceProducts_WhenCalled_ShouldSequenceProducts_InShelves()
        {
            const String expectation = "When called should sequence products correctly.";
            PlanogramFixtureItem bay = _planogram.AddFixtureItem();
            PlanogramFixtureComponent shelfA = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramProduct productA1 = shelfA.AddPosition(bay).GetPlanogramProduct();
            PlanogramProduct productA2 =
                shelfA.AddPosition(bay).SetOrigin(new PointValue(productA1.Width, 0, 0)).GetPlanogramProduct();
            PlanogramFixtureComponent shelfB = bay.AddFixtureComponent(PlanogramComponentType.Shelf,
                new PointValue(0, 20, 0));
            PlanogramProduct productB1 = shelfB.AddPosition(bay).GetPlanogramProduct();
            PlanogramProduct productB2 = shelfB.AddPosition(bay).SetOrigin(productB1.Width, 0, 0).GetPlanogramProduct();
            PlanogramFixtureComponent shelfC = bay.AddFixtureComponent(PlanogramComponentType.Shelf,
                new PointValue(0, 40, 0));
            var placedProducts = new List<PlanogramProduct> {productA2, productA1};
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            foreach (PlanogramProduct placedProduct in placedProducts)
            {
                sequenceGroup.AddSequenceGroupProduct(placedProduct);
            }
            PlanogramBlocking blocking = _planogram.AddBlocking();
            blocking.Dividers.Add(0F, 0F, PlanogramBlockingDividerType.Horizontal);
            PlanogramBlockingGroup blockingGroup = blocking.Groups.First();
            blockingGroup.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.RightToLeft;

            using (PlanogramMerchandisingGroupList merchandisingGroups = _planogram.GetMerchandisingGroups())
            {
                sequenceGroup.Colour = blockingGroup.Colour;
                IEnumerable<String> expected = placedProducts.Select(p => p.Gtin);

                TestItem.SequenceProducts(blockingGroup, merchandisingGroups);

                IEnumerable<String> actual = TestItem.Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin);
                CollectionAssert.AreEqual(expected.ToList(), actual.ToList(), expectation);
            }
        }

        [Test]
        public void SequenceProducts_WhenCombinedShelves_ShouldSequenceAllProducts()
        {
            const String expectation = "When called should sequence combined shelves products correctly.";
            PlanogramFixtureItem bayA = _planogram.AddFixtureItem();
            PlanogramFixtureItem bayB = _planogram.AddFixtureItem();
            PlanogramFixtureItem bayC = _planogram.AddFixtureItem();
            PlanogramFixtureComponent shelfA = bayA.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelfB = bayB.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramFixtureComponent shelfC = bayC.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramComponent shelfComponentA = shelfA.GetPlanogramComponent();
            PlanogramComponent shelfComponentB = shelfB.GetPlanogramComponent();
            PlanogramComponent shelfComponentC = shelfC.GetPlanogramComponent();
            shelfComponentA.SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            shelfComponentB.SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            shelfComponentC.SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            PlanogramProduct prodA1 = shelfA.AddPosition(bayA).GetPlanogramProduct();
            PlanogramProduct prodA2 = shelfA.AddPosition(bayA).SetOrigin(new PointValue(prodA1.Width, 0, 0)).GetPlanogramProduct();
            PlanogramProduct prodB1 = shelfB.AddPosition(bayB).GetPlanogramProduct();
            PlanogramProduct prodB2 = shelfB.AddPosition(bayB).SetOrigin(new PointValue(prodB1.Width, 0, 0)).GetPlanogramProduct();
            PlanogramProduct prodC1 = shelfC.AddPosition(bayC).GetPlanogramProduct();
            PlanogramProduct prodC2 = shelfC.AddPosition(bayC).SetOrigin(new PointValue(prodC1.Width, 0, 0)).GetPlanogramProduct();
            var placedProducts = new List<PlanogramProduct> { prodA1, prodA2, prodB1, prodB2, prodC1, prodC2 };
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            foreach (PlanogramProduct placedProduct in placedProducts)
            {
                sequenceGroup.AddSequenceGroupProduct(placedProduct);
            }
            PlanogramBlocking blocking = _planogram.AddBlocking();
            blocking.Dividers.Add(0F, 0F, PlanogramBlockingDividerType.Horizontal);
            PlanogramBlockingGroup blockingGroup = blocking.Groups.First();
            blockingGroup.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.RightToLeft;

            using (PlanogramMerchandisingGroupList merchandisingGroups = _planogram.GetMerchandisingGroups())
            {
                sequenceGroup.Colour = blockingGroup.Colour;
                IEnumerable<String> expected = placedProducts.Select(p => p.Gtin);

                TestItem.SequenceProducts(blockingGroup, merchandisingGroups);

                IEnumerable<String> actual = TestItem.Products.OrderBy(p => p.SequenceNumber).Select(p => p.Gtin);
                CollectionAssert.AreEquivalent(expected.ToList(), actual.ToList(), expectation);
            }
        }

        [Test]
        public void SequenceProducts_WhenBlockingGroupNull_ShouldNotThrow()
        {
            const String expectation = "When no Blocking Group should not throw.";
            _planogram.AddSequenceGroup();
            var merchandisingGroups = new PlanogramMerchandisingGroupList();

            TestDelegate code = () => TestItem.SequenceProducts(null, merchandisingGroups);

            Assert.DoesNotThrow(code, expectation);
        }

        [Test]
        public void SequenceProducts_WhenMerchandisingGroupNull_ShouldNotThrow()
        {
            const String expectation = "When missing Merchandising Groups should not throw.";
            _planogram.AddSequenceGroup();
            PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();

            TestDelegate code = () => TestItem.SequenceProducts(blockingGroup, null);

            Assert.DoesNotThrow(code, expectation);
        }

        #endregion

        #region GetPreviousSequenceProduct

        [Test]
        public void GetPreviousSequenceProduct_WhenThereIsPrevious_ShouldReturnPrevious()
        {
            const String expectation = "When there is a previous product the correct GTIN should be returned.";
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            IEnumerable<PlanogramProduct> products = _planogram.AddProducts(5);
            PlanogramProduct expected = products.Skip(2).First();
            sequenceGroup.AddSequenceGroupProducts(products);

            PlanogramSequenceGroupProduct actual =
                sequenceGroup.GetPreviousSequenceProduct(products.Skip(3).First().Gtin);

            Assert.AreEqual(expected.Gtin, actual.Gtin, expectation);
        }

        [Test]
        public void GetPreviousSequenceProduct_WhenThereIsNoPrevious_ShouldReturnNull()
        {
            const String expectation = "When there is no previous product the default value should be returned.";
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            IEnumerable<PlanogramProduct> products = _planogram.AddProducts(5);
            sequenceGroup.AddSequenceGroupProducts(products);

            PlanogramSequenceGroupProduct actual = sequenceGroup.GetPreviousSequenceProduct(products.First().Gtin);

            Assert.IsNull(actual, expectation);
        }

        [Test]
        public void GetPreviousSequenceProduct_WhenThereIsNoGtin_ShouldReturnNull()
        {
            const String expectation = "When there is no product for the GTIN the default value should be returned.";
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            IEnumerable<PlanogramProduct> products = _planogram.AddProducts(5);
            sequenceGroup.AddSequenceGroupProducts(products);

            PlanogramSequenceGroupProduct actual = sequenceGroup.GetPreviousSequenceProduct("NonExistingGtin");

            Assert.IsNull(actual, expectation);
        }

        #endregion

        #region GetNextSequenceProduct

        [Test]
        public void GetNextSequenceProduct_WhenThereIsNext_ShouldReturnNext()
        {
            const String expectation = "When there is a next product the correct GTIN should be returned.";
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            IEnumerable<PlanogramProduct> products = _planogram.AddProducts(5);
            PlanogramProduct expected = products.Skip(2).First();
            sequenceGroup.AddSequenceGroupProducts(products);

            PlanogramSequenceGroupProduct actual =
                sequenceGroup.GetNextSequenceProduct(products.Skip(1).First().Gtin);

            Assert.AreEqual(expected.Gtin, actual.Gtin, expectation);
        }

        [Test]
        public void GetNextSequenceProduct_WhenThereIsNoNext_ShouldReturnNull()
        {
            const String expectation = "When there is no next product the default value should be returned.";
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            IEnumerable<PlanogramProduct> products = _planogram.AddProducts(5);
            sequenceGroup.AddSequenceGroupProducts(products);

            PlanogramSequenceGroupProduct actual = sequenceGroup.GetNextSequenceProduct(products.Last().Gtin);

            Assert.IsNull(actual, expectation);
        }

        [Test]
        public void GetNextSequenceProduct_WhenThereIsNoGtin_ShouldReturnNull()
        {
            const String expectation = "When there is no product for the GTIN the default value should be returned.";
            PlanogramSequenceGroup sequenceGroup = _planogram.AddSequenceGroup();
            IEnumerable<PlanogramProduct> products = _planogram.AddProducts(5);
            sequenceGroup.AddSequenceGroupProducts(products);

            PlanogramSequenceGroupProduct actual = sequenceGroup.GetNextSequenceProduct("NonExistingGtin");

            Assert.IsNull(actual, expectation);
        }

        #endregion

        #endregion

        #region Test Helper Methods

        private static List<IEnumerable<PlanogramPosition>> SequenceRows(PlanogramBlockingGroupPlacementType sequenceDirectionX, IEnumerable<IEnumerable<PlanogramPosition>> rowsOfPositions)
        {
            List<IEnumerable<PlanogramPosition>> sequencedRowsOfPositions = new List<IEnumerable<PlanogramPosition>>();
            Boolean reverseRow = false;
            foreach (IEnumerable<PlanogramPosition> row in rowsOfPositions)
            {
                IEnumerable<PlanogramPosition> orderedRow =
                    sequenceDirectionX.IsPositive() ?
                    row.OrderBy(p => p.SequenceX) :
                    row.OrderByDescending(p => p.SequenceX);
                if (reverseRow) orderedRow = orderedRow.Reverse();
                reverseRow = !reverseRow;
                sequencedRowsOfPositions.Add(orderedRow);
            }
            return sequencedRowsOfPositions;
        }

        private void WriteList(String caption, IEnumerable<String> items)
        {
            Debug.WriteLine(caption);
            foreach (String item in items)
            {
                Debug.WriteLine(item);
            }
        }

        private void SavePackage()
        {
            _package = _package.Save();
        }

        private void RefetchPackage()
        {
            _package = Package.FetchById(_package.Id);
        }

        private PlanogramSequenceGroupDto FetchDtoByModel(PlanogramSequenceGroup model)
        {
            PlanogramSequenceGroupDto dto;
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramSequenceGroupDal>())
                {
                    dto = dal.FetchByPlanogramSequenceId(model.Parent.Id).FirstOrDefault();
                }
            }
            return dto;
        }

        #endregion
    }
}