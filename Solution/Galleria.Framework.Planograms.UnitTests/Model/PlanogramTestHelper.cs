#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-28996 : A.Silva
//		Created
// V8-28999 : A.Silva
//      Added some extra helpers.
// V8-29015 : A.Silva
//      Added some more.
// V8-29309 : A.Silva
//      Added auto sequence to AddPosition. Sequence for new positions will be generated if not provided.

#endregion

#region Version History: CCM830

// V8-32143 : A.Silva
//  Added AddFixtureItem overload to allow passing the dimensions for the new fixture.
// V8-32189 : A.Silva
//  Added default pegged products data when creating them for a pegged component.
// CCM-18563 : A.Silva
//  Added SetCombined to PlanogramTestHelper.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    internal static class PlanogramTestHelper
    {
        private const String PlanogramNameTemplate = "{0}.Planogram[{1}]";
        private const String FixtureNameTemplate = "{0}.Fixture[{1}]";
        private const String SequenceGroupNameTemplate = "{0}.SequenceGroup[{1}]";

        internal static Package CreatePackage(this String name)
        {
            Package package = Package.NewPackage(1, PackageLockType.User);
            package.Name = name;
            return package;
        }

        internal static Planogram AddPlanogram(this Package parent)
        {
            Planogram planogram = Planogram.NewPlanogram();
            planogram.Name = String.Format(PlanogramNameTemplate, parent.Name, parent.Planograms.Count);
            parent.Planograms.Add(planogram);
            return planogram;
        }

        internal static PlanogramFixtureItem AddFixtureItem(this Planogram parent)
        {
            PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Name = String.Format(FixtureNameTemplate, parent.Name, parent.Fixtures.Count);
            fixture.Width = 120;
            fixture.Height = 200;
            fixture.Depth = 75;
            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            parent.Fixtures.Add(fixture);
            // Ensure that the fixture item has the correct X.
            fixtureItem.X = parent.FixtureItems.Sum(fi => fi.GetPlanogramFixture().Width);
            parent.FixtureItems.Add(fixtureItem);
            return fixtureItem;
        }

        /// <summary>
        /// Adds a new bay to the plangoram complete with backboard and base.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="depth"></param>
        /// <param name="backboardDepth"></param>
        /// <param name="baseHeight"></param>
        /// <returns></returns>
        internal static PlanogramFixtureItem AddBayWithBackboardAndBase(this Planogram plan, Single height, Single width, Single depth, Single backboardDepth, Single baseHeight)
        {
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = height;
            fixture.Width = width;
            fixture.Depth = depth;
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, backboardDepth);
            backboardFc.Z = -backboardDepth;
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, baseHeight, depth- backboardDepth);

            return fixtureItem;
        }

        /// <summary>
        ///     Add a <see cref="PlanogramFixtureItem"/> with dimension as per the given <paramref name="boundingBox"/> to the <paramref name="parent"/> <see cref="Planogram"/>.
        /// </summary>
        /// <param name="parent">The parent <see cref="Planogram"/> that will contain the new instance.</param>
        /// <param name="boundingBox">A <see cref="RectValue"/> defining the dimensions of the new instance.</param>
        /// <returns>The newly created and added instance.</returns>
        internal static PlanogramFixtureItem AddFixtureItem(this Planogram parent, RectValue boundingBox)
        {
            PlanogramFixtureItem item = parent.AddFixtureItem();
            item.X = boundingBox.X;
            item.Y = boundingBox.Y;
            item.Z = boundingBox.Z;
            PlanogramFixture fixture = item.GetPlanogramFixture();
            fixture.Width = boundingBox.Width;
            fixture.Height = boundingBox.Height;
            fixture.Depth = boundingBox.Depth;

            return item;
        }

        internal static PlanogramFixtureComponent AddFixtureComponent(this PlanogramFixtureItem parent, PlanogramComponentType type, PointValue origin = default(PointValue), WidthHeightDepthValue size = default(WidthHeightDepthValue))
        {
            PlanogramFixture fixture = parent.GetPlanogramFixture();
            String name = String.Format("{0}.Component[{1}]", fixture.Name, fixture.Components.Count);
            switch (type)
            {
                case PlanogramComponentType.Peg:
                case PlanogramComponentType.SlotWall:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 120, 4);
                    break;
                case PlanogramComponentType.Rod:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(4, 4, 75);
                    break;
                case PlanogramComponentType.Chest:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 50, 75);
                    break;
                case PlanogramComponentType.Bar:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 2, 2);
                    break;
                case PlanogramComponentType.ClipStrip:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(4, 100, 1);
                    break;
                case PlanogramComponentType.Backboard:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 200, 1);
                    break;
                default:
                    if (size == default(WidthHeightDepthValue)) size = new WidthHeightDepthValue(120, 4, 75);
                    break;
            }
            PlanogramComponent component = PlanogramComponent.NewPlanogramComponent(name, type, size.Width, size.Height, size.Depth);
            PlanogramFixtureComponent fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.X = origin.X;
            fixtureComponent.Y = origin.Y;
            fixtureComponent.Z = origin.Z;
            parent.Parent.Components.Add(component);
            fixture.Components.Add(fixtureComponent);
            return fixtureComponent;
        }


        internal static PlanogramSubComponentPlacement AddShelf(this PlanogramFixtureItem parent, PointValue origin, WidthHeightDepthValue size)
        {
            return AddFixtureComponent(parent, PlanogramComponentType.Shelf, origin, size).GetPlanogramSubComponentPlacements().First();
        }

        internal static PlanogramFixtureComponent SetOverhang(this PlanogramFixtureComponent component,
                                                              Single left = 0,
                                                              Single right = 0,
                                                              Single top = 0,
                                                              Single bottom = 0,
                                                              Single front = 0,
                                                              Single back = 0)
        {
            PlanogramSubComponent subComponent = component.GetPlanogramComponent().SubComponents.First();
            subComponent.LeftOverhang = left;
            subComponent.RightOverhang = right;
            subComponent.TopOverhang = top;
            subComponent.BottomOverhang = bottom;
            subComponent.FrontOverhang = front;
            subComponent.BackOverhang = back;
            return component;
        }

        internal static PlanogramFixtureComponent SetDividers(this PlanogramFixtureComponent component,
                                                              PointValue spacing = default(PointValue),
                                                              Boolean dividerAtStart = false,
                                                              Boolean dividerAtEnd = false,
                                                              PointValue start = default(PointValue))
        {
            PlanogramSubComponent subComponent = component.GetPlanogramComponent().SubComponents.First();
            subComponent.IsDividerObstructionAtStart = dividerAtStart;
            subComponent.IsDividerObstructionAtEnd = dividerAtEnd;
            subComponent.DividerObstructionSpacingX = spacing.X;
            subComponent.DividerObstructionSpacingY = spacing.Y;
            subComponent.DividerObstructionSpacingZ = spacing.Z;
            subComponent.DividerObstructionWidth = 2;
            subComponent.DividerObstructionHeight = 2;
            subComponent.DividerObstructionDepth = 1;
            subComponent.DividerObstructionStartX = start.X;
            subComponent.DividerObstructionStartY = start.Y;
            subComponent.DividerObstructionStartZ = start.Z;
            return component;
        }

        internal static PlanogramFixtureComponent SetMerchStrategyType(this PlanogramFixtureComponent component,
                                                              PlanogramSubComponentXMerchStrategyType xMerchStrategy = PlanogramSubComponentXMerchStrategyType.LeftStacked,
                                                              PlanogramSubComponentYMerchStrategyType yMerchStrategy = PlanogramSubComponentYMerchStrategyType.Bottom,
                                                              PlanogramSubComponentZMerchStrategyType zMerchStrategy = PlanogramSubComponentZMerchStrategyType.Front)
        {
            PlanogramSubComponent subComponent = component.GetPlanogramComponent().SubComponents.First();
            subComponent.MerchandisingStrategyX = xMerchStrategy;
            subComponent.MerchandisingStrategyY = yMerchStrategy;
            subComponent.MerchandisingStrategyZ = zMerchStrategy;
            return component;
        }

        /// <summary>
        ///     Sets the first subcomponent in the <paramref name="component"/> to the given <paramref name="combineType"/>.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="combineType"></param>
        /// <returns></returns>
        internal static IPlanogramFixtureComponent SetCombined(
            this IPlanogramFixtureComponent component,
            PlanogramSubComponentCombineType combineType = PlanogramSubComponentCombineType.Both)
        {
            component.GetPlanogramSubComponentPlacements().First().SubComponent.CombineType = combineType;
            return component;
        }

        /// <summary>
        ///     Sets the <c>Planogram Placement Types</c> for the <paramref name="planogram"/> on all three axes.
        /// </summary>
        /// <param name="planogram">The planogram to set the <c>Planogram Placement Types</c> for.</param>
        /// <param name="placementXType">Placement type for the X axis. Manual if not provided.</param>
        /// <param name="placementYType">Placement type for the Y axis. Manual if not provided.</param>
        /// <param name="placementZType">Placement type for the Z axis. Manual if not provided.</param>
        /// <returns>The source <paramref name="planogram"/> with its <c>Planogram Placement Types</c> set.</returns>
        internal static Planogram SetAutoFill(
            this Planogram planogram,
            PlanogramProductPlacementXType placementXType = PlanogramProductPlacementXType.Manual,
            PlanogramProductPlacementYType placementYType = PlanogramProductPlacementYType.Manual,
            PlanogramProductPlacementZType placementZType = PlanogramProductPlacementZType.Manual)
        {
            planogram.ProductPlacementX = placementXType;
            planogram.ProductPlacementY = placementYType;
            planogram.ProductPlacementZ = placementZType;
            return planogram;
        }
        
        internal static PlanogramPosition AddPosition(this PlanogramFixtureComponent parent, PlanogramProduct product = null, Int16 sequence = 0)
        {
            PlanogramFixtureItem fixtureItem = parent.Parent.Parent.FixtureItems.FirstOrDefault(item => Equals(item.PlanogramFixtureId, parent.Parent.Id));
            return AddPosition(parent, fixtureItem, product, sequence);
        }

        internal static PlanogramPosition AddPosition(
            this PlanogramFixtureComponent parent, PlanogramFixtureItem parentFixtureItem, PlanogramProduct product = null, Int16 sequence = 0)
        {
            PlanogramSubComponent subComponent = parent.GetPlanogramComponent().SubComponents.First();
            Planogram planogram = parent.Parent.Parent;
            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(subComponent, parentFixtureItem, parent);
            if (product == null)
            {
                product = planogram.AddProduct();

                // Make sure default products for pegged components are valid.
                if (parent.GetPlanogramComponent().ComponentType == PlanogramComponentType.SlotWall ||
                    parent.GetPlanogramComponent().ComponentType == PlanogramComponentType.Bar ||
                    (subComponent.MerchConstraintRow1SpacingX.GreaterThan(0) && subComponent.MerchConstraintRow1Height.GreaterThan(0) && subComponent.MerchConstraintRow1Width.GreaterThan(0)) ||
                    (subComponent.MerchConstraintRow2SpacingX.GreaterThan(0) && subComponent.MerchConstraintRow2Height.GreaterThan(0) && subComponent.MerchConstraintRow2Width.GreaterThan(0)))
                {
                    product.PegDepth = parentFixtureItem.GetPlanogramFixture().Depth - subComponent.X - subComponent.Depth;
                    product.NumberOfPegHoles = 1;
                }
            }
            if (sequence == 0) sequence = Convert.ToInt16(subComponentPlacement.GetPlanogramPositions().Count() + 1);
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(sequence, product, subComponentPlacement);
            var facings = new WideHighDeepValue(1,1,1);
            var blockFacings = new WideHighDeepValue(0, 0, 0);
            position.FacingsWide = facings.Wide;
            position.FacingsHigh = facings.High;
            position.FacingsDeep = facings.Deep;
            position.FacingsXWide = blockFacings.Wide;
            position.FacingsXHigh = blockFacings.High;
            position.FacingsXDeep = blockFacings.Deep;
            position.FacingsYWide = blockFacings.Wide;
            position.FacingsYHigh = blockFacings.High;
            position.FacingsYDeep = blockFacings.Deep;
            position.FacingsZWide = blockFacings.Wide;
            position.FacingsZHigh = blockFacings.High;
            position.FacingsZDeep = blockFacings.Deep;
            planogram.Positions.Add(position);
            return position;
        }

        internal static PlanogramProduct AddProduct(this Planogram parent, WidthHeightDepthValue dimensions = default(WidthHeightDepthValue))
        {
            if (dimensions == default(WidthHeightDepthValue)) dimensions = new WidthHeightDepthValue(5,10,2);
            PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
            product.Name = String.Format("Product[{0}]", parent.Products.Count);
            product.Gtin = String.Format("Product[{0}]", parent.Products.Count);
            product.Width = dimensions.Width;
            product.Height = dimensions.Height;
            product.Depth = dimensions.Depth;
            parent.Products.Add(product);
            return product;
        }

        internal static IList<PlanogramProduct> AddProducts(this Planogram parent, Byte itemCount)
        {
            var items = new List<PlanogramProduct>();
            for (Byte i = 0; i < itemCount; i++)
            {
                items.Add(parent.AddProduct());
            }
            return items;
        }

        internal static PlanogramProduct SetSqueeze(this PlanogramProduct prod, Single width, Single height, Single depth)
        {
            prod.SqueezeWidth = width;
            prod.SqueezeHeight = height;
            prod.SqueezeDepth = depth;

            return prod;
        }

        internal static PlanogramPosition SetMainFacings(this PlanogramPosition pos, Int16 high, Int16 wide, Int16 deep)
        {
            pos.FacingsHigh = high;
            pos.FacingsWide = wide;
            pos.FacingsDeep = deep;
            return pos;
        }

        internal static void SetYCapFacings(this PlanogramPosition pos, Int16 high, Int16 wide, Int16 deep, 
            PlanogramPositionOrientationType orientation = PlanogramPositionOrientationType.Top0)
        {
            pos.FacingsYHigh = high;
            pos.FacingsYWide = wide;
            pos.FacingsYDeep = deep;
            pos.OrientationTypeY = orientation;
        }

        internal static void SetXCapFacings(this PlanogramPosition pos, Int16 high, Int16 wide, Int16 deep,
            PlanogramPositionOrientationType orientation = PlanogramPositionOrientationType.Right0)
        {
            pos.FacingsXHigh = high;
            pos.FacingsXWide = wide;
            pos.FacingsXDeep = deep;
            pos.OrientationTypeX = orientation;
        }

        internal static void SetZCapFacings(this PlanogramPosition pos, Int16 high, Int16 wide, Int16 deep,
            PlanogramPositionOrientationType orientation = PlanogramPositionOrientationType.Front0)
        {
            pos.FacingsZHigh = high;
            pos.FacingsZWide = wide;
            pos.FacingsZDeep = deep;
            pos.OrientationTypeZ = orientation;
        }

        #region Assortment related

        internal static PlanogramAssortmentProduct AddAssortmentProduct(this Planogram parent, WidthHeightDepthValue dimensions = default(WidthHeightDepthValue))
        {
            PlanogramProduct product = AddProduct(parent, dimensions);
            PlanogramAssortmentProduct ap = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(product);
            parent.Assortment.Products.Add(ap);
            return ap;
        }

        internal static IList<PlanogramAssortmentProduct> AddAssortmentProducts(this Planogram parent, Byte itemCount)
        {
            var items = new List<PlanogramAssortmentProduct>();
            for (Byte i = 0; i < itemCount; i++)
            {
                items.Add(parent.AddAssortmentProduct());
            }
            return items;
        }

        public static void AddSequenceProducts(this PlanogramSequenceGroup sequenceGroup, IEnumerable<PlanogramProduct> planogramProducts)
        {
            PlanogramSequenceGroupProductList products = sequenceGroup.Products;
            products.Clear();
            foreach (PlanogramSequenceGroupProduct sequenceProduct in
                    planogramProducts.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct))
            {
                sequenceProduct.SequenceNumber = products.Count + 1;
                products.Add(sequenceProduct);
            }
        }

        internal static IEnumerable<PlanogramAssortmentProduct> CreateAssortmentFromProducts(
            this Planogram parent, IEnumerable<PlanogramProduct> orderedProducts = null)
        {
            IEnumerable<PlanogramAssortmentProduct> assortmentProducts =
                (orderedProducts ?? parent.Products.OrderBy(p => p.Gtin))
                      .Select(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct)
                      .Select((product, i) =>
                      {
                          product.Rank = (Int16)(i + 1);
                          product.IsRanged = true;
                          return product;
                      }).ToList();
            parent.Assortment.Products.AddRange(assortmentProducts);

            return assortmentProducts;
        }

        #endregion

        #region Blocking Related

        internal static PlanogramBlocking AddBlocking(this Planogram parent, PlanogramBlockingType type = PlanogramBlockingType.Final)
        {
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(type);
            parent.Blocking.Add(blocking);
            return blocking;
        }

        /// <summary>
        ///     Adds Blocking Groups and optionally Sequence Groups.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="groupCount">The number of groups to add.</param>
        /// <param name="includeSequenceGroups">Whether to add corresponding Sequence Groups for each Blocking Group.</param>
        internal static PlanogramBlocking AddBlocking(this Planogram parent, Int32 groupCount, Boolean includeSequenceGroups)
        {
            PlanogramBlocking blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
            parent.Blocking.Add(blocking);
            for (var i = 0; i < groupCount; i++)
            {
                PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
                blockingGroup.Colour = blocking.GetNextBlockingGroupColour();
                blocking.Groups.Add(blockingGroup);
                if (!includeSequenceGroups) continue;

                PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = blockingGroup.Colour;
                parent.Sequence.Groups.Add(sequenceGroup);
            }
            return blocking;
        }

        /// <summary>
        ///     Creates a Blocking Strategy from the collection of provided <paramref name="dividers"/>.
        /// </summary>
        /// <param name="planogram">The source planogram to add the new Blocking Strategy to.</param>
        /// <param name="dividers">The collection of (Width, Height) percentages to place each divider.</param>
        /// <remarks>
        ///     If a single divider with (0.0F, 0.0F) is provided the Blocking Strategy will be created with a single Block.
        /// </remarks>
        internal static PlanogramBlocking AddBlocking(this Planogram planogram, IEnumerable<Tuple<Single, Single>> dividers)
        {
            PlanogramBlocking blocking = planogram.AddBlocking();
            foreach (Tuple<Single, Single> divider in dividers)
            {
                Boolean isHorizontal = divider.Item1.EqualTo(0.0F);
                Boolean isVertical = divider.Item2.EqualTo(0.0F);
                Boolean isSingleBlock = isHorizontal && isVertical;
                if (isSingleBlock)
                {
                    Boolean hasBlocks = blocking.Groups.Any();
                    if (hasBlocks)
                        throw new InvalidOperationException("Cannot add a single block if there are other blocks present, review parameters.");
                    // Add a single block, sequence group and location.
                    PlanogramBlockingGroup blockingGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
                    blockingGroup.Colour = blocking.GetNextBlockingGroupColour();
                    blockingGroup.Name = "SingleBlockingGroup";
                    blocking.Groups.Add(blockingGroup);
                    PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
                    planogram.Sequence.Groups.Add(sequenceGroup);
                    PlanogramBlockingLocation blockingLocation = PlanogramBlockingLocation.NewPlanogramBlockingLocation(blockingGroup);
                    blocking.Locations.Add(blockingLocation);
                }
                else
                {
                    PlanogramBlockingDividerType dividerType = isHorizontal
                                                                   ? PlanogramBlockingDividerType.Horizontal
                                                                   : PlanogramBlockingDividerType.Vertical;
                    IEnumerable<PlanogramSequenceGroup> sequenceGroups = blocking
                        .Dividers.Add(divider.Item1, divider.Item2, dividerType)
                        .GetLocations()
                        .Select(l => l.GetPlanogramBlockingGroup())
                        .Where(blockingGroup => planogram.Sequence.Groups.All(g => !g.IsColourMatch(blockingGroup)))
                        .Select(PlanogramSequenceGroup.NewPlanogramSequenceGroup);
                    planogram.Sequence.Groups.AddRange(sequenceGroups);
                }
            }
            return blocking;
        }

        internal static PlanogramBlockingGroup AddGroup(this PlanogramBlocking blocking)
        {
            PlanogramBlockingGroup group = PlanogramBlockingGroup.NewPlanogramBlockingGroup(
                Guid.NewGuid().ToString(), BlockingHelper.GetNextBlockingGroupColour(blocking));
            blocking.Groups.Add(group);
            return group;
        }

        internal static PlanogramBlockingLocation AddLocation(this PlanogramBlockingGroup group, Single startX, Single endX, Single startY, Single endY)
        {
            PlanogramBlockingLocation location = PlanogramBlockingLocation.NewPlanogramBlockingLocation(group);
            group.Parent.Locations.Add(location);

            // Left
            if (startX.EqualTo(0))
            {
                location.PlanogramBlockingDividerLeftId = null;
            }
            else
            {
                location.PlanogramBlockingDividerLeftId =
                    group.Parent.Dividers.Add(startX, startY, PlanogramBlockingDividerType.Vertical).Id;
            }

            // Right
            if (endX.EqualTo(1))
            {
                location.PlanogramBlockingDividerRightId = null;
            }
            else
            {
                location.PlanogramBlockingDividerRightId =
                    group.Parent.Dividers.Add(endX, startY, PlanogramBlockingDividerType.Vertical).Id;
            }

            // Bottom
            if (startY.EqualTo(0))
            {
                location.PlanogramBlockingDividerBottomId = null;
            }
            else
            {
                location.PlanogramBlockingDividerBottomId =
                    group.Parent.Dividers.Add(startX, startY, PlanogramBlockingDividerType.Horizontal).Id;
            }

            // Top
            if (endY.EqualTo(1))
            {
                location.PlanogramBlockingDividerTopId = null;
            }
            else
            {
                location.PlanogramBlockingDividerTopId =
                    group.Parent.Dividers.Add(startX, endY, PlanogramBlockingDividerType.Horizontal).Id;
            }

            return location;
        }

        #endregion

        #region Sequence related

        internal static PlanogramSequenceGroup AddSequenceGroup(this Planogram parent)
        {
            PlanogramSequenceGroup planogramSequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            planogramSequenceGroup.Name = String.Format(SequenceGroupNameTemplate, parent.Name, parent.Sequence.Groups.Count);
            parent.Sequence.Groups.Add(planogramSequenceGroup);
            return planogramSequenceGroup;
        }

        internal static IEnumerable<PlanogramPosition> GetPositions(this PlanogramFixtureComponent fc, Boolean orderXAsc = true)
        {
            var positions = fc.GetPlanogramSubComponentPlacements().SelectMany(s => s.GetPlanogramPositions());
            if (orderXAsc)
            {
                return positions.OrderBy(p => p.SequenceX);
            }
            else
            {
                return positions.OrderByDescending(p => p.SequenceX);
            }
        }

        internal static IEnumerable<String> GetPositionGtins(this PlanogramFixtureComponent fc, Boolean orderXAsc)
        {
            return fc.GetPositions(orderXAsc).Select(p => p.GetPlanogramProduct().Gtin);
        }

        internal static PlanogramSequenceGroup AddSequenceGroup(this Planogram parent, PlanogramBlockingGroup blockingGroup, IEnumerable<PlanogramProduct> productsInSequence)
        {
            PlanogramSequenceGroup planogramSequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
            planogramSequenceGroup.Name = String.Format(SequenceGroupNameTemplate, parent.Name, parent.Sequence.Groups.Count);
            parent.Sequence.Groups.Add(planogramSequenceGroup);
            Int32 sequence = 1;
            foreach (var productInSequence in productsInSequence)
            {
                var sequenceProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(productInSequence);
                sequenceProduct.SequenceNumber = sequence++;
                planogramSequenceGroup.Products.Add(sequenceProduct);
            }
            return planogramSequenceGroup;
        }

        internal static IList<PlanogramSequenceGroup> AddSequenceGroups(this Planogram parent, Byte itemCount)
        {
            var items = new List<PlanogramSequenceGroup>();
            for (Byte i = 0; i < itemCount; i++)
            {
                items.Add(parent.AddSequenceGroup());
            }
            return items;
        }

        internal static PlanogramSequenceGroupProduct AddSequenceGroupProduct(this PlanogramSequenceGroup parent, PlanogramProduct source = null)
        {
            PlanogramSequenceGroupProduct sequenceGroupProduct;
            if (source == null)
            {
                sequenceGroupProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
                sequenceGroupProduct.Gtin = String.Format("Product[{0}]", parent.Products.Count);
            }
            else
            {
                sequenceGroupProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct(source);
            }
            sequenceGroupProduct.SequenceNumber = parent.Products.Count + 1;
            parent.Products.Add(sequenceGroupProduct);
            return sequenceGroupProduct;
        }

        internal static IList<PlanogramSequenceGroupProduct> AddSequenceGroupProducts(this PlanogramSequenceGroup parent, IEnumerable<PlanogramProduct> source)
        {
            return source.Select(product => parent.AddSequenceGroupProduct(product)).ToList();
        }

        #endregion

        internal static PlanogramPosition SetOrigin(this PlanogramPosition source, PointValue newValue)
        {
            source.X = newValue.X;
            source.Y = newValue.Y;
            source.Z = newValue.Z;
            return source;
        }

        public static PlanogramPosition SetOrigin(this PlanogramPosition source, Single newX, Single newY, Single newZ)
        {
            return source.SetOrigin(new PointValue(newX, newY, newZ));
        }

        /// <summary>
        /// Gets all merch groups for the planogram, processes them and applies them.
        /// </summary>
        internal static void ReprocessAllMerchandisingGroups(this Planogram plan)
        {
            using (PlanogramMerchandisingGroupList merchGroupList = plan.GetMerchandisingGroups())
            {
                foreach (PlanogramMerchandisingGroup group in merchGroupList)
                {
                    group.Process();
                    group.ApplyEdit();
                }
            }
        }


        internal static String TakeSnapshot(this Planogram target, String fileName = null)
        {
            target.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            target.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            target.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            if (String.IsNullOrWhiteSpace(fileName)) fileName = target.Name;
            fileName = Path.ChangeExtension(fileName, ".pog");
            String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), target.Parent.Name, fileName);
            String directoryName = Path.GetDirectoryName(path);
            if (directoryName == null) return "Could not take snapshot, invalid directory.";
            if (!Directory.Exists(directoryName)) Directory.CreateDirectory(directoryName);
            target.Parent.SaveAs(1, path);

            return path;
        }
    }
}