﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-31963 : A.Silva
//  Added Display Property Tests.

#endregion

#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramComparisonFieldTests : TestBase<PlanogramComparisonField, PlanogramComparisonFieldDto>
    {
        #region Coding Standards

        [Test]
        public void Serializable()
        {
            TestSerialize();
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        [Test]
        public void NewFactoryMethod_FromFieldInfo()
        {
            ObjectFieldInfo fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(typeof (PlanogramProduct), "Product", "Name", "Name", typeof(String), ModelPropertyDisplayType.None);

            PlanogramComparisonField testModel = PlanogramComparisonField.NewPlanogramComparisonField(PlanogramItemType.Product, fieldInfo, true);

            Assert.IsTrue(testModel.IsNew);
            Assert.IsTrue(testModel.IsInitialized);
            Assert.AreEqual(PlanogramItemType.Product, testModel.ItemType);
            Assert.AreEqual(fieldInfo.FieldPlaceholder, testModel.FieldPlaceholder);
            Assert.AreEqual(fieldInfo.FieldFriendlyName, testModel.DisplayName);
            Assert.IsTrue(testModel.Display);
        }

        #endregion

        #region Data Transfer Object

        [Test, TestCaseSource("GetMatchingDtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("GetMatchingDtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #endregion
    }
}