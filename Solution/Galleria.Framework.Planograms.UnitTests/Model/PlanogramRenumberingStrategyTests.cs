﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva
//      Created.
// V8-27411 : M.Pettit
//  Adjusted call to Package.NewPackage() to pass in lock requirements
// V8-27623 : A.Silva
//      Amended tests to get accurate order expectations.

#endregion

#region Version History: CCM801

// V8-28878 : A.Silva
//      Amended name of RestartComponentRenumberingPerBayProperty to make it consistent with the database field name.

#endregion

#region Version History: CCM830

// V8-32145 : A.Silva
//  Amended Renumber Bays tests.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramRenumberingStrategyTests : TestBase
    {
        #region Fields

        private Planogram _planogram;

        #region BayA

        private PlanogramFixtureItem _frontLeftBay;
        private PlanogramFixtureComponent _frontLeftBayBackboard;
        private PlanogramFixtureComponent _frontLeftBayBase;
        private PlanogramFixtureComponent _frontLeftBayShelf;
        private PlanogramPosition _frontLeftBayShelfPosition111WithProd0;
        private PlanogramPosition _frontLeftBayShelfPosition211WithProd0;
        private PlanogramPosition _frontLeftBayShelfPosition212WithProd0;
        private PlanogramPosition _frontLeftBayShelfPosition112WithProd1;
        private PlanogramPosition _frontLeftBayShelfPosition312WithProd1;

        #endregion

        #region BayB

        private PlanogramFixtureItem _frontMiddleBay;
        private PlanogramFixtureComponent _frontMiddleBayBackBoard;
        private PlanogramFixtureComponent _frontMiddleBayBase;
        private PlanogramAssemblyComponent _frontMiddleBayLowerShelfInAssembly;
        private PlanogramAssemblyComponent _frontMiddleBayUpperShelfInAssembly;
        private PlanogramPosition _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0;
        private PlanogramPosition _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0;
        private PlanogramPosition _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1;

        #endregion

        private PlanogramFixtureItem _frontRightBay;
        private PlanogramFixtureItem _backLeftBay;
        private PlanogramFixtureItem _backMiddleBay;

        #endregion

        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramRenumberingStrategy.NewPlanogramRenumberingStrategy());
        }

        #endregion

        #region Factory Methods
        #endregion

        #region Data Access
        #endregion

        [Test]
        public void RenumberBays_WhenPlanogramEmpty_ShouldNotThrow()
        {
            Planogram planogram = InitializePlanogram();

            TestDelegate test = () => planogram.RenumberingStrategy.RenumberBays(forced: true);

            Assert.DoesNotThrow(test, "Renumbering an empty planogram (no bays or other components) should not throw an exception.");
        }

        #region Bay renumbering

        [Test]
        public void RenumberBays_WhenStrategyIs_X_Y_Z()
        {
            Planogram planogram = InitializePlanogram();
            List<PlanogramFixtureItem> expected = new List<PlanogramFixtureItem> { _frontLeftBay, _backLeftBay, _frontMiddleBay, _backMiddleBay, _frontRightBay };

            planogram.RenumberingStrategy.RenumberBays();

            AssertBayRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberBays_WhenStrategyIs_X_Y_Z_ReverseX()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentXRenumberStrategy = PlanogramRenumberingStrategyXRenumberType.RightToLeft;
            List<PlanogramFixtureItem> expected = new List<PlanogramFixtureItem> { _frontRightBay, _frontMiddleBay, _backMiddleBay, _frontLeftBay, _backLeftBay };

            planogram.RenumberingStrategy.RenumberBays();

            AssertBayRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberBays_WhenStrategyIs_X_Y_Z_ReverseZ()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentZRenumberStrategy = PlanogramRenumberingStrategyZRenumberType.BackToFront;
            List<PlanogramFixtureItem> expected = new List<PlanogramFixtureItem> { _backLeftBay, _frontLeftBay, _backMiddleBay, _frontMiddleBay, _frontRightBay };

            planogram.RenumberingStrategy.RenumberBays();

            AssertBayRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberBays_WhenStrategyIs_X_Y_Z_ReverseX_Z()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentXRenumberStrategy = PlanogramRenumberingStrategyXRenumberType.RightToLeft;
            planogram.RenumberingStrategy.BayComponentZRenumberStrategy = PlanogramRenumberingStrategyZRenumberType.BackToFront;
            List<PlanogramFixtureItem> expected = new List<PlanogramFixtureItem> { _frontRightBay, _backMiddleBay, _frontMiddleBay, _backLeftBay, _frontLeftBay };

            planogram.RenumberingStrategy.RenumberBays();

            AssertBayRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberBays_WhenStrategyIs_Y_Z_X()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentXRenumberStrategyPriority = 3;
            planogram.RenumberingStrategy.BayComponentYRenumberStrategyPriority = 1;
            planogram.RenumberingStrategy.BayComponentZRenumberStrategyPriority = 2;
            List<PlanogramFixtureItem> expected = new List<PlanogramFixtureItem> { _frontLeftBay, _frontMiddleBay, _frontRightBay, _backLeftBay, _backMiddleBay };

            planogram.RenumberingStrategy.RenumberBays();

            AssertBayRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberBays_WhenStrategyIs_Z_X_Y()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentXRenumberStrategyPriority = 2;
            planogram.RenumberingStrategy.BayComponentYRenumberStrategyPriority = 3;
            planogram.RenumberingStrategy.BayComponentZRenumberStrategyPriority = 1;
            List<PlanogramFixtureItem> expected = new List<PlanogramFixtureItem> { _frontLeftBay, _frontMiddleBay, _frontRightBay, _backLeftBay, _backMiddleBay };

            planogram.RenumberingStrategy.RenumberBays();

            AssertBayRenumbering(planogram, expected);
        }

        #endregion

        #region Component renumbering

        [Test]
        public void RenumberComponents_WhenStrategy_X_Y_Z()
        {
            Planogram planogram = InitializePlanogram();
            List<IPlanogramFixtureComponent> expected = new List<IPlanogramFixtureComponent>
            {
                _frontLeftBayShelf, _frontMiddleBayUpperShelfInAssembly, _frontMiddleBayLowerShelfInAssembly
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertComponentRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberComponents_WhenStrategy_X_Y_Z_YBackwards()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentYRenumberStrategy = PlanogramRenumberingStrategyYRenumberType.BottomToTop;
            List<IPlanogramFixtureComponent> expected = new List<IPlanogramFixtureComponent>
            {
                _frontLeftBayShelf, _frontMiddleBayLowerShelfInAssembly, _frontMiddleBayUpperShelfInAssembly
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertComponentRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberComponents_WhenStrategy_X_Y_Z_DoNotRestartComponentRenumbering()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.RestartComponentRenumberingPerBay = false;
            List<IPlanogramFixtureComponent> expected = new List<IPlanogramFixtureComponent>
            {
                _frontLeftBayShelf, _frontMiddleBayUpperShelfInAssembly, _frontMiddleBayLowerShelfInAssembly
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertComponentRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberComponents_WhenStrategy_X_Y_Z_DoNotIgnoreNonMerchandising()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.IgnoreNonMerchandisingComponents = false;
            List<IPlanogramFixtureComponent> expected = new List<IPlanogramFixtureComponent>
            {
                _frontLeftBayShelf, _frontLeftBayBase, _frontLeftBayBackboard,
                _frontMiddleBayUpperShelfInAssembly, _frontMiddleBayLowerShelfInAssembly, _frontMiddleBayBase, _frontMiddleBayBackBoard
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertComponentRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberComponents_WhenStrategy_Z_X_Y()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentXRenumberStrategyPriority = 2;
            planogram.RenumberingStrategy.BayComponentYRenumberStrategyPriority = 3;
            planogram.RenumberingStrategy.BayComponentZRenumberStrategyPriority = 1;
            List<IPlanogramFixtureComponent> expected = new List<IPlanogramFixtureComponent>
            {
                _frontLeftBayShelf, _frontMiddleBayUpperShelfInAssembly, _frontMiddleBayLowerShelfInAssembly
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertComponentRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberComponents_WhenStrategy_Y_Z_X()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.BayComponentXRenumberStrategyPriority = 3;
            planogram.RenumberingStrategy.BayComponentYRenumberStrategyPriority = 1;
            planogram.RenumberingStrategy.BayComponentZRenumberStrategyPriority = 2;
            List<IPlanogramFixtureComponent> expected = new List<IPlanogramFixtureComponent>
            {
                _frontLeftBayShelf, _frontMiddleBayUpperShelfInAssembly, _frontMiddleBayLowerShelfInAssembly
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertComponentRenumbering(planogram, expected);
        }

        #endregion

        #region Position renumbering

        [Test]
        public void RenumberPositions_WhenStrategy_X_Y_Z()
        {
            Planogram planogram = InitializePlanogram();
            List<PlanogramPosition> expected = new List<PlanogramPosition>
            {
                _frontLeftBayShelfPosition112WithProd1,
                _frontLeftBayShelfPosition111WithProd0,
                _frontLeftBayShelfPosition212WithProd0,
                _frontLeftBayShelfPosition211WithProd0,
                _frontLeftBayShelfPosition312WithProd1, 
                _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0, 
                _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1,
                _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertPositionRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberPositions_WhenStrategy_X_Y_Z_DoNotRestartPositionPerComponent()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.RestartPositionRenumberingPerComponent = false;
            List<PlanogramPosition> expected = new List<PlanogramPosition>
            {
                _frontLeftBayShelfPosition112WithProd1,
                _frontLeftBayShelfPosition111WithProd0,
                _frontLeftBayShelfPosition212WithProd0,
                _frontLeftBayShelfPosition211WithProd0,
                _frontLeftBayShelfPosition312WithProd1, 
                _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0, 
                _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1,
                _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertPositionRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberPositions_WhenStrategy_X_Y_Z_DoNotUniqueNumberPerProductOnComponent()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.UniqueNumberMultiPositionProductsPerComponent = false;
            List<PlanogramPosition> expected = new List<PlanogramPosition>
            {
                _frontLeftBayShelfPosition112WithProd1,
                _frontLeftBayShelfPosition312WithProd1, 
                _frontLeftBayShelfPosition111WithProd0,
                _frontLeftBayShelfPosition211WithProd0,
                _frontLeftBayShelfPosition212WithProd0,
                _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0, 
                _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1,
                _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertPositionRenumbering(planogram, expected);
        }

        [Test]
        public void RenumberPositions_WhenStrategy_X_Y_Z_UniqueNumberIncludingAdjacent()
        {
            Planogram planogram = InitializePlanogram();
            planogram.RenumberingStrategy.ExceptAdjacentPositions = false;
            List<PlanogramPosition> expected = new List<PlanogramPosition>
            {
                _frontLeftBayShelfPosition112WithProd1,
                _frontLeftBayShelfPosition111WithProd0,
                _frontLeftBayShelfPosition212WithProd0,
                _frontLeftBayShelfPosition211WithProd0,
                _frontLeftBayShelfPosition312WithProd1, 
                _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0, 
                _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1,
                _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0
            };

            planogram.RenumberingStrategy.RenumberBays();

            AssertPositionRenumbering(planogram, expected);
        }

        #endregion

        #region Test Planogram

        private Planogram InitializePlanogram()
        {
            Planogram planogram = Planogram.NewPlanogram();
            Package.NewPackage(1, PackageLockType.User).Planograms.Add(planogram);

            InitializeProducts(planogram);
            InitializeBays(planogram);
            InitializeComponents(planogram);
            return planogram;
        }

        private static void InitializeProducts(Planogram planogram)
        {
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 32F;
            prod2.Width = 8.5F;
            prod2.Depth = 8.5F;
            planogram.Products.Add(prod2);

            PlanogramProduct prod3 = PlanogramProduct.NewPlanogramProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 23F;
            prod3.Width = 24.5F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 23.2F;
            prod3.TrayWidth = 25;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 1;
            prod3.TrayDeep = 3;
            planogram.Products.Add(prod3);
        }

        private void InitializeBays(Planogram planogram)
        {
            _frontLeftBay = planogram.FixtureItems.Add();
            _frontLeftBay.X = 0;
            _frontLeftBay.Y = 0;
            _frontLeftBay.Z = 300;
            PlanogramFixture fixture = _frontLeftBay.GetPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 191;
            fixture.Depth = 100;

            _frontMiddleBay = planogram.FixtureItems.Add();
            _frontMiddleBay.X = 120;
            _frontMiddleBay.Y = 0;
            _frontMiddleBay.Z = 300;
            fixture = _frontMiddleBay.GetPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 191;
            fixture.Depth = 100;

            _frontRightBay = planogram.FixtureItems.Add();
            _frontRightBay.X = 240;
            _frontRightBay.Y = 0;
            _frontRightBay.Z = 300;
            fixture = _frontRightBay.GetPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 191;
            fixture.Depth = 100;

            _backLeftBay = planogram.FixtureItems.Add();
            _backLeftBay.X = 0;
            _backLeftBay.Y = 0;
            _backLeftBay.Z = 0;
            fixture = _backLeftBay.GetPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 191;
            fixture.Depth = 100;

            _backMiddleBay = planogram.FixtureItems.Add();
            _backMiddleBay.X = 120;
            _backMiddleBay.Y = 0;
            _backMiddleBay.Z = 0;
            fixture = _backMiddleBay.GetPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 191;
            fixture.Depth = 100;
        }

        private void InitializeComponents(Planogram planogram)
        {
            #region Bay A

            PlanogramFixture fixture = _frontLeftBay.GetPlanogramFixture();
            // Add the first component, a backboard, default values.
            _frontLeftBayBackboard = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            _frontLeftBayBackboard.Z = -1;

            // Add the second component, a base, default values.
            _frontLeftBayBase = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            // Add the third component, a shelf, default values.
            _frontLeftBayShelf = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            _frontLeftBayShelf.Y = 105;

            #region Bay A Component C

            PlanogramComponent bayAComponentC = _frontLeftBayShelf.GetPlanogramComponent();
            PlanogramSubComponent bayAComponentCSub = bayAComponentC.SubComponents.First();
            bayAComponentCSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            bayAComponentCSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            bayAComponentCSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            bayAComponentCSub.IsProductOverlapAllowed = true;
            PlanogramSubComponentPlacement bayAComponentCSubPlacement =
                PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(bayAComponentCSub, _frontLeftBay,
                    _frontLeftBayShelf);

            _frontLeftBayShelfPosition111WithProd0 = bayAComponentCSubPlacement.AddPosition(planogram.Products[0]);
            _frontLeftBayShelfPosition111WithProd0.FacingsWide = 1;
            _frontLeftBayShelfPosition111WithProd0.FacingsHigh = 1;
            _frontLeftBayShelfPosition111WithProd0.FacingsDeep = 1;
            _frontLeftBayShelfPosition111WithProd0.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontLeftBayShelfPosition111WithProd0.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontLeftBayShelfPosition111WithProd0.X = 0;
            _frontLeftBayShelfPosition111WithProd0.Y = 0;
            _frontLeftBayShelfPosition111WithProd0.Z = 0;
            _frontLeftBayShelfPosition111WithProd0.TotalUnits = 1;

            _frontLeftBayShelfPosition211WithProd0 = bayAComponentCSubPlacement.AddPosition(planogram.Products[0]);
            _frontLeftBayShelfPosition211WithProd0.FacingsWide = 1;
            _frontLeftBayShelfPosition211WithProd0.FacingsHigh = 1;
            _frontLeftBayShelfPosition211WithProd0.FacingsDeep = 1;
            _frontLeftBayShelfPosition211WithProd0.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontLeftBayShelfPosition211WithProd0.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontLeftBayShelfPosition211WithProd0.X = 10;
            _frontLeftBayShelfPosition211WithProd0.Y = 0;
            _frontLeftBayShelfPosition211WithProd0.Z = 0;
            _frontLeftBayShelfPosition211WithProd0.TotalUnits = 1;

            _frontLeftBayShelfPosition212WithProd0 = bayAComponentCSubPlacement.AddPosition(planogram.Products[0]);
            _frontLeftBayShelfPosition212WithProd0.FacingsWide = 1;
            _frontLeftBayShelfPosition212WithProd0.FacingsHigh = 1;
            _frontLeftBayShelfPosition212WithProd0.FacingsDeep = 1;
            _frontLeftBayShelfPosition212WithProd0.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontLeftBayShelfPosition212WithProd0.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontLeftBayShelfPosition212WithProd0.X = 10;
            _frontLeftBayShelfPosition212WithProd0.Y = 0;
            _frontLeftBayShelfPosition212WithProd0.Z = 10;
            _frontLeftBayShelfPosition212WithProd0.TotalUnits = 1;

            _frontLeftBayShelfPosition112WithProd1 = bayAComponentCSubPlacement.AddPosition(planogram.Products[1]);
            _frontLeftBayShelfPosition112WithProd1.FacingsWide = 1;
            _frontLeftBayShelfPosition112WithProd1.FacingsHigh = 1;
            _frontLeftBayShelfPosition112WithProd1.FacingsDeep = 1;
            _frontLeftBayShelfPosition112WithProd1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontLeftBayShelfPosition112WithProd1.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontLeftBayShelfPosition112WithProd1.X = 0;
            _frontLeftBayShelfPosition112WithProd1.Y = 0;
            _frontLeftBayShelfPosition112WithProd1.Z = 10;
            _frontLeftBayShelfPosition112WithProd1.TotalUnits = 1;

            _frontLeftBayShelfPosition312WithProd1 = bayAComponentCSubPlacement.AddPosition(planogram.Products[1]);
            _frontLeftBayShelfPosition312WithProd1.FacingsWide = 1;
            _frontLeftBayShelfPosition312WithProd1.FacingsHigh = 1;
            _frontLeftBayShelfPosition312WithProd1.FacingsDeep = 1;
            _frontLeftBayShelfPosition312WithProd1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontLeftBayShelfPosition312WithProd1.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontLeftBayShelfPosition312WithProd1.X = 20;
            _frontLeftBayShelfPosition312WithProd1.Y = 0;
            _frontLeftBayShelfPosition312WithProd1.Z = 10;
            _frontLeftBayShelfPosition312WithProd1.TotalUnits = 1;

            Int16 sequence = 1;
            List<PlanogramPosition> positions =
                _frontLeftBay.GetPlanogramSubComponentPlacements()
                    .SelectMany(placement => placement.GetPlanogramPositions())
                    .ToList();
            foreach (IGrouping<float, PlanogramPosition> group in positions.OrderBy(p => p.X).GroupBy(p => p.X))
            {
                foreach (PlanogramPosition position in group)
                {
                    position.SequenceX = sequence;
                }
                sequence++;
            }

            sequence = 1;
            foreach (IGrouping<float, PlanogramPosition> group in positions.OrderBy(p => p.Y).GroupBy(p => p.Y))
            {
                foreach (PlanogramPosition position in group)
                {
                    position.SequenceY = sequence;
                }
                sequence++;
            }

            sequence = 1;
            foreach (IGrouping<float, PlanogramPosition> group in positions.OrderBy(p => p.Z).GroupBy(p => p.Z))
            {
                foreach (PlanogramPosition position in group)
                {
                    position.SequenceZ = sequence;
                }
                sequence++;
            }

            #endregion

            #endregion

            #region Bay B

            fixture = _frontMiddleBay.GetPlanogramFixture();
            // Add the first component, a backboard, default values.
            _frontMiddleBayBackBoard = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            _frontMiddleBayBackBoard.Z = -1;

            // Add the second component, a base, default values.
            _frontMiddleBayBase = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            // Add the third component, an assemby with two shelves, default values.
            PlanogramFixtureAssembly bayBComponentAssembly = fixture.Assemblies.Add();
            PlanogramAssembly planogramAssembly = bayBComponentAssembly.GetPlanogramAssembly();
            _frontMiddleBayLowerShelfInAssembly = planogramAssembly.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            _frontMiddleBayLowerShelfInAssembly.Y = 50;
            _frontMiddleBayUpperShelfInAssembly = planogramAssembly.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            _frontMiddleBayUpperShelfInAssembly.Y = 100;

            #region Bay B Component C

            PlanogramComponent bayBComponentC = _frontMiddleBayLowerShelfInAssembly.GetPlanogramComponent();
            PlanogramSubComponent bayBComponentCSub = bayBComponentC.SubComponents.First();
            bayBComponentCSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            bayBComponentCSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            bayBComponentCSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            bayBComponentCSub.IsProductOverlapAllowed = true;
            PlanogramSubComponentPlacement bayBComponentCSubPlacement =
                PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(bayBComponentCSub, _frontMiddleBay, bayBComponentAssembly,
                    _frontMiddleBayLowerShelfInAssembly);

            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0 = bayBComponentCSubPlacement.AddPosition(planogram.Products[0]);
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.FacingsWide = 1;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.FacingsHigh = 1;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.FacingsDeep = 1;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.X = 0;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.Y = 0;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.Z = 0;
            _frontMiddleBayLowerShelfInAssemblyPosition111WithProd0.TotalUnits = 1;

            #endregion

            #region Bay B Component D

            PlanogramComponent bayBComponentD = _frontMiddleBayUpperShelfInAssembly.GetPlanogramComponent();
            PlanogramSubComponent bayBComponentDSub = bayBComponentD.SubComponents.First();
            bayBComponentDSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            bayBComponentDSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            bayBComponentDSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            bayBComponentDSub.IsProductOverlapAllowed = true;
            PlanogramSubComponentPlacement bayBComponentDSubPlacement =
                PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(bayBComponentDSub, _frontMiddleBay, bayBComponentAssembly,
                    _frontMiddleBayUpperShelfInAssembly);

            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0 = bayBComponentDSubPlacement.AddPosition(planogram.Products[0]);
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.FacingsWide = 1;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.FacingsHigh = 1;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.FacingsDeep = 1;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.X = 0;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.Y = 0;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.Z = 0;
            _frontMiddleBayUpperShelfInAssemblyPosition111WithProd0.TotalUnits = 1;

            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1 = bayBComponentDSubPlacement.AddPosition(planogram.Products[1]);
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.FacingsWide = 1;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.FacingsHigh = 1;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.FacingsDeep = 1;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.OrientationType = PlanogramPositionOrientationType.Front0;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.X = 10;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.Y = 0;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.Z = 0;
            _frontMiddleBayUpperShelfInAssemblyPosition211WithProd1.TotalUnits = 1;

            sequence = 1;
            positions =
                _frontMiddleBay.GetPlanogramSubComponentPlacements()
                    .SelectMany(placement => placement.GetPlanogramPositions())
                    .ToList();
            foreach (IGrouping<float, PlanogramPosition> group in positions.OrderBy(p => p.X).GroupBy(p => p.X))
            {
                foreach (PlanogramPosition position in group)
                {
                    position.SequenceX = sequence;
                }
                sequence++;
            }

            sequence = 1;
            foreach (IGrouping<float, PlanogramPosition> group in positions.OrderBy(p => p.Y).GroupBy(p => p.Y))
            {
                foreach (PlanogramPosition position in group)
                {
                    position.SequenceY = sequence;
                }
                sequence++;
            }

            sequence = 1;
            foreach (IGrouping<float, PlanogramPosition> group in positions.OrderBy(p => p.Z).GroupBy(p => p.Z))
            {
                foreach (PlanogramPosition position in group)
                {
                    position.SequenceZ = sequence;
                }
                sequence++;
            }

            #endregion

            #endregion
        }

        private static List<PlanogramFixtureItem> GetRenumberedBaysOrderedBySequence(Planogram planogram)
        {
            return planogram.FixtureItems
                .Where(item => item.BaySequenceNumber != 0)
                .OrderBy(item => item.BaySequenceNumber)
                .ToList();
        }

        private static List<IPlanogramFixtureComponent> GetRenumberedComponentsOrderedBySequence(Planogram planogram)
        {
            return GetRenumberedBaysOrderedBySequence(planogram)
                .SelectMany(item =>
                    item.GetPlanogramFixture().Components.Cast<IPlanogramFixtureComponent>()
                    .Union(item.GetPlanogramFixture().Assemblies
                        .SelectMany(assembly => assembly.GetPlanogramAssembly().Components))
                    .Where(component => component.ComponentSequenceNumber != null)
                    .OrderBy(component => component.ComponentSequenceNumber))
                    .ToList();
        }

        private static List<PlanogramPosition> GetRenumberedPositionsOrderedBySequence(Planogram planogram)
        {
            return GetRenumberedComponentsOrderedBySequence(planogram)
                .SelectMany(component => component.GetPlanogramSubComponentPlacements()
                    .SelectMany(placement => placement.GetPlanogramPositions())
                    .Where(position => position.PositionSequenceNumber != null)
                    .OrderBy(position => position.PositionSequenceNumber)).ToList();
        }

        private static void AssertBayRenumbering(Planogram planogram, List<PlanogramFixtureItem> expected)
        {
            List<PlanogramFixtureItem> actual = GetRenumberedBaysOrderedBySequence(planogram);
            int expectedCount = expected.Count;
            int actualCount = actual.GroupBy(item => item.BaySequenceNumber).Count();

            Assert.AreEqual(
                expectedCount,
                actualCount,
                "Incorrect number of bays renumbered, or duplicate sequence numbers.");

            CollectionAssert.AreEqual(
                expected,
                actual,
                "Bays are not correctly renumbered in the expected order.");
        }

        private static void AssertComponentRenumbering(Planogram planogram, List<IPlanogramFixtureComponent> expected)
        {
            List<IPlanogramFixtureComponent> actual = GetRenumberedComponentsOrderedBySequence(planogram);
            int expectedCount = expected.Count;
            int actualCount = actual.Count;

            Assert.AreEqual(
                expectedCount,
                actualCount,
                "Incorrect number of components renumbered.");

            CollectionAssert.AreEqual(
                expected,
                actual,
                "Components are not correctly renumbered in the expected order.");

            List<IPlanogramFixtureComponent> firstComponentInBays = new List<IPlanogramFixtureComponent>(GetRenumberedBaysOrderedBySequence(planogram)
                .Select(item =>
                    item.GetPlanogramFixture().Components.Cast<IPlanogramFixtureComponent>()
                    .Union(item.GetPlanogramFixture().Assemblies
                        .SelectMany(assembly => assembly.GetPlanogramAssembly().Components))
                    .Where(component => component.ComponentSequenceNumber != null)
                    .OrderBy(component => component.ComponentSequenceNumber).FirstOrDefault()).Where(component => component != null)
                    .ToList());
            if (planogram.RenumberingStrategy.RestartComponentRenumberingPerBay)
            {
                Assert.IsTrue(firstComponentInBays.All(component => component.ComponentSequenceNumber == 1));
            }
            else
            {
                Assert.IsTrue(firstComponentInBays.Count(component => component.ComponentSequenceNumber == 1) <= 1);
            }

        }

        private static void AssertPositionRenumbering(Planogram planogram, List<PlanogramPosition> expected)
        {
            List<PlanogramPosition> actual = GetRenumberedPositionsOrderedBySequence(planogram);
            List<IPlanogramFixtureComponent> components = GetRenumberedComponentsOrderedBySequence(planogram);
            int expectedCount = expected.Count;
            int actualCount = actual.Count;

            Assert.AreEqual(
                expectedCount,
                actualCount,
                "Incorrect number of positions renumbered.");

            CollectionAssert.AreEqual(
                expected, 
                actual,
                "Positions are not correctly renumbered in the expected order.");

            List<PlanogramPosition> firstPositionInComponents =
                new List<PlanogramPosition>(components
                    .Select(component =>
                        component.GetPlanogramSubComponentPlacements()
                            .SelectMany(placement => placement.GetPlanogramPositions())
                                .Where(position => position.PositionSequenceNumber != null)
                                .OrderBy(position => position.PositionSequenceNumber).FirstOrDefault()).Where(position => position != null).ToList());
            if (planogram.RenumberingStrategy.RestartPositionRenumberingPerComponent)
            {
                Assert.IsTrue(firstPositionInComponents.All(position => position.PositionSequenceNumber == 1));
            }
            else
            {
                Assert.IsTrue(firstPositionInComponents.GroupBy(position => position.PlanogramFixtureItemId).All(grouping => grouping.Count(position => position.PositionSequenceNumber == 1) <= 1));
            }

            // TODO figure out a good method to test adjacent positions.
            //if (planogram.RenumberingStrategy.UniqueNumberMultiPositionProductsPerComponent)
            //{
            //    if (planogram.RenumberingStrategy.ExceptAdjacentPositions)
            //    {
            //        // There is ONE adjacent product in the planogram, so substract 1 from the actual count to simulate it not getting renumbered.
            //        Assert.AreEqual(components.Select(GetPositionsFrom).Sum(list => GetTotalDistinctSequenceNumbers(list)), actualCount - 1, "There are less unique sequence numbers than positions when all positions should get unique sequence numbers.");
            //    }
            //    else
            //    {
            //        Assert.AreEqual(components.Select(GetPositionsFrom).Sum(list => GetTotalDistinctSequenceNumbers(list)), actualCount, "There are less unique sequence numbers than positions when all positions should get unique sequence numbers.");
            //    }
            //}
            //else
            //{
            //    Assert.IsTrue(components.Select(GetPositionsFrom).All(positions => GetTotalDistinctProducts(positions) == GetTotalDistinctSequenceNumbers(positions)));
            //}
        }

        private static Int32 GetTotalDistinctSequenceNumbers(IEnumerable<PlanogramPosition> componentPositions)
        {
            return componentPositions.GroupBy(position => position.PositionSequenceNumber).Count();
        }

        private static List<PlanogramPosition> GetPositionsFrom(IPlanogramFixtureComponent component)
        {
            return component.GetPlanogramSubComponentPlacements().SelectMany(placement => placement.GetPlanogramPositions().ToList()).ToList();
        }

        private static Int32 GetTotalDistinctProducts(IEnumerable<PlanogramPosition> componentPositions)
        {
            return componentPositions.GroupBy(position => position.PlanogramProductId).Count();
        }

        #endregion
    }
}
