﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26573 : L.Luong 
//   Created (Auto-generated)
// V8-26721 : A.Silva ~ Added unit tests for IValidationTemplate Members.
// V8-27132 : A.Kuszyk
//  Added test for factory method that accepts interface.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Moq;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Ccm.Security;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramConsumerDecisionTreeLevelTest : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTreeLevel()
        {
            var model = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel();
            Assert.That(model.IsChild);

            model = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel("Level");
            Assert.That(model.IsChild);
            Assert.IsNotNull(model.Name);
        }

        [Test]
        public void NewPlanogramConsumerDecisionTreeLevel_CopiesInterfaceProperties()
        {
            var levelMock = new Mock<IPlanogramConsumerDecisionTreeLevel>();
            var childLevelMock = new Mock<IPlanogramConsumerDecisionTreeLevel>();
            childLevelMock.Setup(o => o.Name).Returns(Guid.NewGuid().ToString());
            levelMock.Setup(o => o.Name).Returns(Guid.NewGuid().ToString());
            levelMock.Setup(o => o.ChildLevel).Returns(childLevelMock.Object);
            var level = levelMock.Object;

            var model = PlanogramConsumerDecisionTreeLevel.NewPlanogramConsumerDecisionTreeLevel(level);

            AssertHelper.AreEqual(level, model);
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "Root";
            package = package.Save();

            var level = package.Planograms[0].ConsumerDecisionTree.RootLevel;
            var dto = FetchDtoByModel(level);

            //fetch
            Assert.AreEqual(dto.Name, package.Planograms[0].ConsumerDecisionTree.RootLevel.Name);
        }

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            package.Planograms[0].ConsumerDecisionTree.Name = "Name";
            //create new consumer decision tree level
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "Root";
            package = package.Save();

            //check if consumer decision tree is not null
            Assert.IsNotNull(package.Planograms[0].ConsumerDecisionTree.RootLevel);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            String check = "Root";

            // set consumer decision tree Name
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = check;

            // save
            package = package.Save();
            PackageDto dto;

            // fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check first value is equal
            Assert.AreEqual(check, package.Planograms[0].ConsumerDecisionTree.RootLevel.Name);

            // change value
            check = "Child";
            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = check;
            package = package.Save();

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if value has changed
            Assert.AreEqual(check, package.Planograms[0].ConsumerDecisionTree.RootLevel.Name);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set consumer decision tree to planogram
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";

            // add set root level name

            package.Planograms[0].ConsumerDecisionTree.RootLevel.Name = "Root";
            var model = package.Planograms[0].ConsumerDecisionTree.RootLevel;
            package.Planograms[0].ConsumerDecisionTree.InsertNewChildLevel(model);
            package.Planograms[0].ConsumerDecisionTree.RootLevel.ChildLevel.ParentLevelId = package.Planograms[0].ConsumerDecisionTree.RootLevel.Id;

            //save
            package = package.Save();
            PackageDto dto;

            // get dto
            var modelPackage = package.Planograms[0].ConsumerDecisionTree.RootLevel;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // remove child level
            var level = package.Planograms[0].ConsumerDecisionTree.RootLevel.ChildLevel;
            Object levelId = level.Id;
            package.Planograms[0].ConsumerDecisionTree.RemoveLevel(package.Planograms[0].ConsumerDecisionTree.RootLevel.ChildLevel, true);
            package = package.Save();

            // consumer decision tree dto should be null
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            Assert.IsNull(package.Planograms[0].ConsumerDecisionTree.RootLevel.ChildLevel);

        }

        #endregion

        #endregion

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        private PlanogramConsumerDecisionTreeLevelDto FetchDtoByModel(PlanogramConsumerDecisionTreeLevel model)
        {
            PlanogramConsumerDecisionTreeLevelDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeLevelDal>())
                {
                    dto = dal.FetchByPlanogramConsumerDecisionTreeId(model.ParentPlanogramConsumerDecisionTree.Id).FirstOrDefault();
                }
            }
            return dto;
        }

        #endregion
    }
}
