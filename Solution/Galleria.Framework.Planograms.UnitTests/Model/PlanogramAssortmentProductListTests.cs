﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32132 : A.Silva
//  Created.
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramAssortmentProductListTests
    {
        private const String NewGtin = "NewGtin";

        #region Helper Static Methods

        /// <summary>
        ///     Sets up the target and source lists for append tests (source list will contain one existing and one new product).
        /// </summary>
        /// <param name="targetList">Contains only the 'ExistingGtin' product.</param>
        /// <param name="sourceList">Contains both 'ExistingGtin' and 'NewGting' products.</param>
        private static void SetupTargetAndSource(out PlanogramAssortmentProductList targetList, out PlanogramAssortmentProductList sourceList)
        {
            targetList = PlanogramAssortmentProductList.NewPlanogramAssortmentProductList();
            sourceList = PlanogramAssortmentProductList.NewPlanogramAssortmentProductList();
            PlanogramAssortmentProduct item = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            item.Gtin = "ExistingGtin";
            targetList.Add(item);
            sourceList.Add(item.Copy());
            item = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();
            item.Gtin = NewGtin;
            sourceList.Add(item);
        }

        #endregion

        [Test]
        public void AppendAssortmentProductsShouldAddNewGtins()
        {
            const String expectation = "The new assortment product should have been added.";
            PlanogramAssortmentProductList targetList;
            PlanogramAssortmentProductList sourceList;
            SetupTargetAndSource(out targetList, out sourceList);

            targetList.AppendNew(sourceList);

            CollectionAssert.Contains(targetList.Select(item => item.Gtin), NewGtin, expectation);
        }

        [Test]
        public void AppendAssortmentProductsShouldNotAddExistingGtins()
        {
            const String expectation = "The existing assortment product should not have been added.";
            PlanogramAssortmentProductList targetList;
            PlanogramAssortmentProductList sourceList;
            SetupTargetAndSource(out targetList, out sourceList);

            targetList.AppendNew(sourceList);

            CollectionAssert.AllItemsAreUnique(targetList.Select(item => item.Gtin), expectation);
        }
    }
}