﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-27494 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using System.Reflection;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class CustomAttributeDataTests
    {
        private IEnumerable<PropertyInfo> _properties
        {
            get { return typeof(ICustomAttributeData).GetProperties(); }
        }

        #region UpdateFrom

        [Test]
        public void UpdateFrom_WhenNoPropertiesSpecified_UpdatesAllValues()
        {
            var model = Planograms.Model.CustomAttributeData.NewCustomAttributeData();
            var source = Planograms.Model.CustomAttributeData.NewCustomAttributeData();
            TestDataHelper.SetPropertyValues(source);

            model.UpdateFrom(source);

            AssertHelper.AreEqual<ICustomAttributeData>(source, model, "Id");
        }

        [Test]
        [TestCaseSource("_properties")]
        public void UpdateFrom_WhenPropertySpecified_ValueChanges(PropertyInfo property)
        {
            var model = Planograms.Model.CustomAttributeData.NewCustomAttributeData();
            var source = Planograms.Model.CustomAttributeData.NewCustomAttributeData();
            TestDataHelper.SetPropertyValues(source);

            model.UpdateFrom(source, property.Name );

            Object expected = property.GetValue(source, null);
            Object actual = property.GetValue(model, null);
            Assert.AreEqual(expected, actual, String.Format("Assert failed for {0} property", property.Name));
        }

        [Test]
        public void UpdateFrom_WhenAllPropertiesSpecified_AllValuesUpdate()
        {
            var model = Planograms.Model.CustomAttributeData.NewCustomAttributeData();
            var source = Planograms.Model.CustomAttributeData.NewCustomAttributeData();
            TestDataHelper.SetPropertyValues(source);

            model.UpdateFrom(source, _properties.Select(p => p.Name).ToArray());

            AssertHelper.AreEqual<ICustomAttributeData>(source, model, "Id");
        }

        #endregion
    }
}
