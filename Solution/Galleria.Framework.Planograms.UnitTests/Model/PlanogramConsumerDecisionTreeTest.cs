﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26706 : L.Luong 
//   Created (Auto-generated)
// V8-27132 : A.Kuszyk
//  Added test for factory method that accepts interface.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Moq;
using Galleria.Ccm.Security;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramConsumerDecisionTreeTest : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramConsumerDecisionTree.NewPlanogramConsumerDecisionTree());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewConsumerDecisionTree()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set consumer decision tree to planogram
            var newConsumerDecisionTree = PlanogramConsumerDecisionTree.NewPlanogramConsumerDecisionTree();
            newConsumerDecisionTree.Name = "Name";
            package.Planograms[0].ConsumerDecisionTree.Name = newConsumerDecisionTree.Name;
            package = package.Save();

            newConsumerDecisionTree = planogram.ConsumerDecisionTree;

            //check if consumer decision tree is new and is child
            Assert.AreEqual(newConsumerDecisionTree.Parent.Id, planogram.Id);
            Assert.IsTrue(newConsumerDecisionTree.IsNew);
            Assert.IsTrue(newConsumerDecisionTree.IsChild, "A newly created Planogram Consumer Decision Tree should be a child object.");
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set consumer decision tree to planogram
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";
            package = package.Save();

            var consumerDecisionTree = package.Planograms[0].ConsumerDecisionTree;
            var dto = FetchDtoByModel(consumerDecisionTree);

            //fetch
            Assert.AreEqual(dto.Name, package.Planograms[0].ConsumerDecisionTree.Name);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            //create new consumer decision tree
            package.Planograms[0].ConsumerDecisionTree.Name = "name";
            package = package.Save();

            //check if consumer decision tree is not null
            Assert.IsNotNull(package.Planograms[0].ConsumerDecisionTree);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            String check = "Name";

            // set consumer decision tree name
            package.Planograms[0].ConsumerDecisionTree.Name = check;

            // save
            package = package.Save();
            PackageDto dto;

            // fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check first value is equal
            Assert.AreEqual(check, package.Planograms[0].ConsumerDecisionTree.Name);

            // change value
            check = "Name2";
            package.Planograms[0].ConsumerDecisionTree.Name = check;
            package = package.Save();

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if value has changed
            Assert.AreEqual(check, package.Planograms[0].ConsumerDecisionTree.Name);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set consumer decision tree to planogram
            package.Planograms[0].ConsumerDecisionTree.Name = "Name";
            package = package.Save();
            PackageDto dto;

            // get dto
            var model = package.Planograms[0];
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // remove planogram 
            package.Planograms.Remove(model);
            package = package.Save();

            // consumer decision tree dto should be null
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            Assert.AreEqual(0, package.Planograms.Count);

        }

        #endregion

        #region Methods
        [Test]
        public void ReplaceConsumerDecisionTree_ReplacesProperties()
        {
            var planogram = Planogram.NewPlanogram();
            var cdt = planogram.ConsumerDecisionTree;
            var cdtMock = new Mock<IPlanogramConsumerDecisionTree>();
            var cdtLevel = new Mock<IPlanogramConsumerDecisionTreeLevel>();
            cdtLevel.Setup(o => o.Name).Returns(Guid.NewGuid().ToString());
            var cdtNode = new Mock<IPlanogramConsumerDecisionTreeNode>();
            cdtNode.Setup(o => o.Name).Returns(Guid.NewGuid().ToString());
            cdtMock.Setup(o => o.Name).Returns(Guid.NewGuid().ToString());
            cdtMock.Setup(o => o.RootLevel).Returns(cdtLevel.Object);
            cdtMock.Setup(o => o.RootNode).Returns(cdtNode.Object);
            var replacementCdt = cdtMock.Object;

            cdt.ReplaceConsumerDecisionTree(replacementCdt);

            AssertHelper.AreEqual<IPlanogramConsumerDecisionTree>(replacementCdt, cdt);
        }
        #endregion

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        private PlanogramConsumerDecisionTreeDto FetchDtoByModel(PlanogramConsumerDecisionTree model)
        {
            PlanogramConsumerDecisionTreeDto dto;
            using (IDalContext dalContext = base.DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramConsumerDecisionTreeDal>())
                {
                    dto = dal.FetchByPlanogramId(model.Parent.Id);
                }
            }
            return dto;
        }
        
        #endregion
    }
}
