﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
// V8-27467 : A.Kuszyk
//  Adjusted the way snapping boundaries are selected.
// V8-27485 : A.Kuszyk
//  Moved fixture snapping code to Engine.Tasks assembly.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.UnitTests.Helpers;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramBlockingDividerTests : TestBase<PlanogramBlockingDivider, PlanogramBlockingDividerDto>
    {
        private PlanogramFixture _fixture;
        private Planogram _planogram;
        private PlanogramBlocking _blocking;
        private const Single _fixtureSize = 10f;

        [SetUp]
        public void PlanogramBlockingDividerSetup()
        {
            _fixture = PlanogramFixture.NewPlanogramFixture();
            _fixture.Width = _fixtureSize;
            _fixture.Height = _fixtureSize;
            _blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            _planogram = Planogram.NewPlanogram();
            _planogram.Fixtures.Add(_fixture);
            _planogram.Blocking.Add(_blocking);
            _planogram.Width = _fixtureSize;
            _planogram.Height = _fixtureSize;
        }

        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion

        #region Get Blocking Tests

        [Test]
        public void GetAmountOfFixtureSpanned_WhenDividerVerticalAndOverlap_ReturnsOne()
        {
            var fixture = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture);

            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(1,PlanogramBlockingDividerType.Vertical,0.5f,0f,1f);
            _blocking.Dividers.Add(divider);

            var result = divider.GetAmountOfFixtureSpanned(fixture);

            Assert.AreEqual(1f, result);
        }

        [Test]
        public void GetAmountOfFixtureSpanned_WhenDividerVerticalAndNoOverlap_ReturnsZero()
        {
            var fixture1 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture1);
            var fixture2 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture2);

            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(1,PlanogramBlockingDividerType.Vertical,0.25f,0f,1f);
            _blocking.Dividers.Add(divider);

            var result = divider.GetAmountOfFixtureSpanned(fixture2);

            Assert.AreEqual(0f, result);
        }

        [Test]
        public void GetAmountOfFixtureSpanned_WhenDividerHorizontalAndOverlap_ReturnsOne()
        {
            var fixture1 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture1);
            var fixture2 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture2);

            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(1, PlanogramBlockingDividerType.Horizontal, 0f, 0f, 0.5f);
            _blocking.Dividers.Add(divider);

            var result = divider.GetAmountOfFixtureSpanned(fixture1);

            Assert.AreEqual(1f, result);
        }

        [Test]
        public void GetAmountOfFixtureSpanned_WhenDividerHorizontalAndNoOverlap_ReturnsZero()
        {
            var fixture1 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture,1);
            _planogram.FixtureItems.Add(fixture1);
            var fixture2 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture,2);
            _planogram.FixtureItems.Add(fixture2);

            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(1, PlanogramBlockingDividerType.Horizontal,0.5f,0.5f,0.5f);
            _blocking.Dividers.Add(divider);

            var result = divider.GetAmountOfFixtureSpanned(fixture1);

            Assert.AreEqual(0f, result);
        }

        [Test]
        public void GetAmountOfFixtureSpanned_WhenDividerHorizontalAndPartialOverlap_ReturnsPercentage()
        {
            var fixture1 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture1);
            var fixture2 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture2);

            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(1, PlanogramBlockingDividerType.Horizontal,0.25f,0f,0.5f);
            _blocking.Dividers.Add(divider);

            var result1 = divider.GetAmountOfFixtureSpanned(fixture1);
            var result2 = divider.GetAmountOfFixtureSpanned(fixture2);

            Assert.AreEqual(0.5f, result1);
            Assert.AreEqual(0.5f, result2);
        }

        [Test]
        public void GetParentFixtureItems_WhenVerticalAndOverOneFixture_ReturnsCorrectFixture()
        {
            var fixture1 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture1);
            var fixture2 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture2);

            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(1, PlanogramBlockingDividerType.Horizontal,0f,0f,0.5f);
            _blocking.Dividers.Add(divider);

            var result = divider.GetParentFixtureItems().ToList();

            //Assert.That(result == fixture1);
            CollectionAssert.AreEquivalent(new PlanogramFixtureItem[] { fixture1 }, result);
        }

        [Test]
        public void GetParentFixtureItems_WhenVerticalAndOverTwoFixtureItems_ReturnsCorrectFixture()
        {
            var fixture1 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture1);
            var fixture2 = PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture);
            _planogram.FixtureItems.Add(fixture2);

            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(1, PlanogramBlockingDividerType.Horizontal,0f,0f,0.7f);
            _blocking.Dividers.Add(divider);

            var result = divider.GetParentFixtureItems().ToList();

            //Assert.That(result == fixture1);
            CollectionAssert.AreEquivalent(new PlanogramFixtureItem[] { fixture1, fixture2 }, result);
        }

        [Test]
        public void GetMaxDistanceToNeighbour_ReturnsCorrectValue_Horizontal_WhenBoundaryIsDivider()
        {
            //1.0_^      ___________
            //    |     |           |
            //    |     |           |
            //0.5-|   ..|...........|.. Divider1
            //    |     |           |
            //0.2-|   ..|...........|.. Divider2
            //0.0_|_    |___________|

            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 0, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 0, blocking.Dividers);
            var expected = 0.3f;

            var actual = divider2.GetMaxDistanceToNeighbour();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetMaxDistanceToNeighbour_ReturnsCorrectValue_Vertical_WhenBoundaryIsDivider()
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            var divider1 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 0, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0.2f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 0, blocking.Dividers);
            var expected = 0.3f;

            var actual = divider2.GetMaxDistanceToNeighbour();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetMaxDistanceToNeighbour_ReturnsCorrectValue_Horizontal_WhenBoundaryIsNull()
        {
            //1.0_^      ___________
            //    |     |           |
            //    |     |           |
            //0.6-|   ..|...........|.. Divider1
            //    |     |           |
            //0.2-|   ..|...........|.. Divider2
            //0.0_|_    |___________|

            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.6f, 1f, PlanogramBlockingDividerType.Horizontal, 0, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 0, blocking.Dividers);
            var expected = Convert.ToSingle(Math.Abs(divider1.Y - 1f));
            
            var actual = divider1.GetMaxDistanceToNeighbour();

            Assert.AreEqual(Math.Round(expected,5), Math.Round(actual,5));
        }

        [Test]
        public void GetMaxDistanceToNeighbour_ReturnsCorrectValue_Vertical_WhenBoundaryIsNull()
        {
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            var divider1 = TestDataHelper.CreateBlockingDivider(0.6f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 0, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0.2f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 0, blocking.Dividers);
            var expected = Convert.ToSingle(Math.Abs(divider1.X - 1f));

            var actual = divider1.GetMaxDistanceToNeighbour();

            Assert.AreEqual(Math.Round(expected, 5), Math.Round(actual, 5));
        }

        [Test]
        public void GetParallelParentDivider_ReturnsNull_WhenHorizontalRootLevel()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);

            var result = divider.GetParallelParentDivider(false);

            Assert.IsNull(result);
        }

        [Test]
        public void GetParallelParentDivider_ReturnsNull_WhenVerticalRootLevel()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);

            var result = divider.GetParallelParentDivider(false);

            Assert.IsNull(result);
        }

        [Test]
        public void GetParallelParentDivider_ReturnsFloor_WhenHorizontalLevelThree()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            TestDataHelper.CreateBlockingDivider(0f, 0.1f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var floorDivider = TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0f, 0.8f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0.5f, 0.2f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);

            var result = divider.GetParallelParentDivider(false);

            Assert.AreSame(floorDivider, result);
        }

        [Test]
        public void GetParallelParentDivider_ReturnsCeiling_WhenHorizontalLevelThree()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            TestDataHelper.CreateBlockingDivider(0f, 0.2f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var ceilingDivider = TestDataHelper.CreateBlockingDivider(0f, 0.8f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0f, 0.9f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0.5f, 0.2f, 0.6f, PlanogramBlockingDividerType.Vertical, 2, blocking.Dividers);
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 3, blocking.Dividers);

            var result = divider.GetParallelParentDivider(true);

            Assert.AreSame(ceilingDivider, result);
        }

        [Test]
        public void GetParallelParentDivider_ReturnsFloor_WhenVerticalLevelThree()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            TestDataHelper.CreateBlockingDivider(0.1f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var floorDivider = TestDataHelper.CreateBlockingDivider(0.2f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0.8f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0.2f, 0.5f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Vertical, 3, blocking.Dividers);

            var result = divider.GetParallelParentDivider(false);

            Assert.AreSame(floorDivider, result);
        }

        [Test]
        public void GetParallelParentDivider_ReturnsCeiling_WhenVerticalLevelThree()
        {
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            TestDataHelper.CreateBlockingDivider(0.2f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var ceilingDivider = TestDataHelper.CreateBlockingDivider(0.8f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0.9f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            TestDataHelper.CreateBlockingDivider(0.2f, 0.5f, 0.6f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Vertical, 3, blocking.Dividers);

            var result = divider.GetParallelParentDivider(true);

            Assert.AreSame(ceilingDivider, result);
        }

        #endregion
    }
}
