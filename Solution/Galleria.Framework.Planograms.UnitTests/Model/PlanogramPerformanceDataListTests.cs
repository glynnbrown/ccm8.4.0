﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26271 : A.Kuszyk
//  Created.
// V8-27647 : L.Luong
//  Added CalculatedInventory tests
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramPerformanceDataListTests : TestBase
    {
        [Test]
        public void Add_WhenItemContainsDuplicatePlanogramProduct_BreaksRules()
        {
            var planogram = Planogram.NewPlanogram();
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Gtin = "Gtin";
            planogram.Products.Add(planogramProduct);

            Assert.AreEqual(1, planogram.Performance.PerformanceData.Count);
            Assert.That(planogram.Performance.IsValid);

            planogram.Performance.PerformanceData.Add(
                    PlanogramPerformanceData.NewPlanogramPerformanceData(planogramProduct));

            Assert.AreEqual(2, planogram.Performance.PerformanceData.Count);
            Assert.IsFalse(planogram.Performance.IsValid);
        }

        [Test]
        public void InventoryCalculations()
        {
            // Create Planogram
            var planogram = Planogram.NewPlanogram();

            // Create product
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Gtin = "Gtin";
            planogramProduct.ShelfLife = 20;
            planogramProduct.DeliveryFrequencyDays = 3;
            planogramProduct.CasePackUnits = 5;

            // Add product to planogram
            planogram.Products.Add(planogramProduct);

            // create positions
            planogram.Positions.Add();
            planogram.Positions[0].PlanogramProductId = planogramProduct.Id;
            planogram.Positions[0].TotalUnits = 10;

            planogram.Positions.Add();
            planogram.Positions[1].PlanogramProductId = planogramProduct.Id;
            planogram.Positions[1].TotalUnits = 20;

            // Set performance data
            planogram.Performance.PerformanceData[0].P1 = 88;

            // Create metrics
            planogram.Performance.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1));
            planogram.Performance.Metrics[0].Name = "MetricName";
            planogram.Performance.Metrics[0].SpecialType = MetricSpecialType.RegularSalesUnits;

            // Call method
            planogram.Performance.PerformanceData[0].UpdateInventoryValues();

            // checks results
            AssertHelper.AreSinglesEqual(12.57F, planogram.Performance.PerformanceData[0].UnitsSoldPerDay.Value);
            AssertHelper.AreSinglesEqual(6F, planogram.Performance.PerformanceData[0].AchievedCasePacks.Value);
            AssertHelper.AreSinglesEqual(2.39F, planogram.Performance.PerformanceData[0].AchievedDos.Value);
            AssertHelper.AreSinglesEqual(0.1193F, planogram.Performance.PerformanceData[0].AchievedShelfLife.Value);
            AssertHelper.AreSinglesEqual(0.80F, planogram.Performance.PerformanceData[0].AchievedDeliveries.Value);

        }

        [Test]
        public void InventoryCalculations_PropertyChangeCheck()
        {
            // Create Planogram
            var planogram = Planogram.NewPlanogram();

            // Create product
            var planogramProduct = PlanogramProduct.NewPlanogramProduct();
            planogramProduct.Gtin = "Gtin";
            planogramProduct.ShelfLife = 20;
            planogramProduct.DeliveryFrequencyDays = 3;
            planogramProduct.CasePackUnits = 5;

            // Add product to planogram
            planogram.Products.Add(planogramProduct);

            // create positions
            planogram.Positions.Add();
            planogram.Positions[0].PlanogramProductId = planogramProduct.Id;
            planogram.Positions[0].TotalUnits = 10;

            planogram.Positions.Add();
            planogram.Positions[1].PlanogramProductId = planogramProduct.Id;
            planogram.Positions[1].TotalUnits = 20;

            // Set performance data
            planogram.Performance.PerformanceData[0].P1 = 88;

            // Create metrics
            planogram.Performance.Metrics.Add(PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1));
            planogram.Performance.Metrics[0].Name = "MetricName";
            planogram.Performance.Metrics[0].SpecialType = MetricSpecialType.RegularSalesUnits;

            // Call method
            planogram.Performance.PerformanceData[0].UpdateInventoryValues();

            // check results
            AssertHelper.AreSinglesEqual(12.57F, planogram.Performance.PerformanceData[0].UnitsSoldPerDay.Value);
            AssertHelper.AreSinglesEqual(6F, planogram.Performance.PerformanceData[0].AchievedCasePacks.Value);
            AssertHelper.AreSinglesEqual(2.39F, planogram.Performance.PerformanceData[0].AchievedDos.Value);
            AssertHelper.AreSinglesEqual(0.1193F, planogram.Performance.PerformanceData[0].AchievedShelfLife.Value);
            AssertHelper.AreSinglesEqual(0.80F, planogram.Performance.PerformanceData[0].AchievedDeliveries.Value);

            // change total units in position
            planogram.Positions[0].TotalUnits = 20;

            // check results
            AssertHelper.AreSinglesEqual(12.57F, planogram.Performance.PerformanceData[0].UnitsSoldPerDay.Value);
            AssertHelper.AreSinglesEqual(8F, planogram.Performance.PerformanceData[0].AchievedCasePacks.Value);
            AssertHelper.AreSinglesEqual(3.18F, planogram.Performance.PerformanceData[0].AchievedDos.Value);
            AssertHelper.AreSinglesEqual(0.1591F, planogram.Performance.PerformanceData[0].AchievedShelfLife.Value);
            AssertHelper.AreSinglesEqual(1.06F, planogram.Performance.PerformanceData[0].AchievedDeliveries.Value);

            // change CasePackUnits in product 
            planogram.Products[0].CasePackUnits = 4;

            // check result affected
            Assert.AreEqual(10F, planogram.Performance.PerformanceData[0].AchievedCasePacks);

            // change ShelfLife in product
            planogram.Products[0].ShelfLife = 30;

            // check result affected
            Assert.AreEqual(0.1061F, planogram.Performance.PerformanceData[0].AchievedShelfLife);

            // change DeliveryFrequencyDays in product
            planogramProduct.DeliveryFrequencyDays = 4;

            // check result affected
            Assert.AreEqual(0.80F, planogram.Performance.PerformanceData[0].AchievedDeliveries);

            // change affected PerformanceData in PerformanceData
            planogram.Performance.PerformanceData[0].P1 = 100;

            // check results
            AssertHelper.AreSinglesEqual(14.29F, planogram.Performance.PerformanceData[0].UnitsSoldPerDay.Value);
            AssertHelper.AreSinglesEqual(2.80F, planogram.Performance.PerformanceData[0].AchievedDos.Value);
            AssertHelper.AreSinglesEqual(0.0933F, planogram.Performance.PerformanceData[0].AchievedShelfLife.Value);
            AssertHelper.AreSinglesEqual(0.70F, planogram.Performance.PerformanceData[0].AchievedDeliveries.Value);

            // change DaysOfPerformance in Inventory
            planogram.Inventory.DaysOfPerformance = 10;

            // check results
            AssertHelper.AreSinglesEqual(10F, planogram.Performance.PerformanceData[0].UnitsSoldPerDay.Value);
            AssertHelper.AreSinglesEqual(4F, planogram.Performance.PerformanceData[0].AchievedDos.Value);
            AssertHelper.AreSinglesEqual(0.1333F, planogram.Performance.PerformanceData[0].AchievedShelfLife.Value);
            AssertHelper.AreSinglesEqual(1F, planogram.Performance.PerformanceData[0].AchievedDeliveries.Value);

            // change a value which should not change calculations
            planogram.Performance.PerformanceData[0].P2 = 100;

            // check results
            AssertHelper.AreSinglesEqual(10F, planogram.Performance.PerformanceData[0].UnitsSoldPerDay.Value);
            AssertHelper.AreSinglesEqual(10F, planogram.Performance.PerformanceData[0].AchievedCasePacks.Value);
            AssertHelper.AreSinglesEqual(4F, planogram.Performance.PerformanceData[0].AchievedDos.Value);
            AssertHelper.AreSinglesEqual(0.1333F, planogram.Performance.PerformanceData[0].AchievedShelfLife.Value);
            AssertHelper.AreSinglesEqual(1F, planogram.Performance.PerformanceData[0].AchievedDeliveries.Value);

            // change SpecialType in metrics
            planogram.Performance.Metrics[0].SpecialType = MetricSpecialType.None;

            // check results
            AssertHelper.AreSinglesEqual(0F, planogram.Performance.PerformanceData[0].UnitsSoldPerDay.Value);
            AssertHelper.AreSinglesEqual(10F, planogram.Performance.PerformanceData[0].AchievedCasePacks.Value);
            AssertHelper.AreSinglesEqual(0F, planogram.Performance.PerformanceData[0].AchievedDos.Value);
            AssertHelper.AreSinglesEqual(0F, planogram.Performance.PerformanceData[0].AchievedShelfLife.Value);
            AssertHelper.AreSinglesEqual(0F, planogram.Performance.PerformanceData[0].AchievedDeliveries.Value);


        }
    }
}
