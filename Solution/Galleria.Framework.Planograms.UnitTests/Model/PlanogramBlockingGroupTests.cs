﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson
//  Created.
#endregion

#region Version History : CCM 802
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion

#region Version History : CCM 803
// V8-29512 : A.Kuszyk
//	Added test for new from source constructors.
#endregion

#region Version History : CCM 820
// V8-31004 : D.Pleasance
//	Added tests for EnumerateSubComponentPositionsInSequence.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Interfaces;
using Moq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramBlockingGroupTests : TestBase<PlanogramBlockingGroup, PlanogramBlockingGroupDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        [Test]
        public void Property_TotalAllocatedSpace_UpdatedOnLocationCombine()
        {
            PlanogramBlocking item = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);

            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlockingGroup group1 = item.Groups[0];

            Boolean propertyChangedFired = false;

            group1.PropertyChanged +=
                (s, e) =>
                {
                    if (e.PropertyName == PlanogramBlockingGroup.TotalSpacePercentageProperty.Name)
                    {
                        propertyChangedFired = true;
                    }
                };

            //combine the locations
            item.Locations[1].CombineWith(item.Locations[0]);
            Assert.IsTrue(propertyChangedFired, "Property change not fired");
        }

        [Test]
        public void Property_TotalAllocatedSpace_UpdatedLocationAllocatedSpaceChanged()
        {
            PlanogramBlocking item = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);

            //add dividers
            item.Dividers.Add(0.45F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0, PlanogramBlockingDividerType.Vertical);
            item.Dividers.Add(0.8F, 0.5F, PlanogramBlockingDividerType.Horizontal);

            PlanogramBlockingGroup group1 = item.Groups[0];

            Boolean propertyChangedFired = false;

            group1.PropertyChanged +=
                (s, e) =>
                {
                    if (e.PropertyName == PlanogramBlockingGroup.TotalSpacePercentageProperty.Name)
                    {
                        propertyChangedFired = true;
                    }
                };

            //move the first divider
            item.Dividers[0].MoveTo(0.3F);
            Assert.IsTrue(propertyChangedFired, "Property change not fired");
        }

        [Test]
        public void BlockPlacementType_ValidWhenTypesNotDuplicated()
        {
            PlanogramBlockingGroup group = PlanogramBlockingGroup.NewPlanogramBlockingGroup("Name",1);
            Assert.That(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.BackToFront;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            Assert.That(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.TopToBottom;
            Assert.That(group.IsValid);
        }

        [Test]
        public void BlockPlacementPrimaryType_InvalidWhenTypesDuplicated()
        {
            PlanogramBlockingGroup group = PlanogramBlockingGroup.NewPlanogramBlockingGroup("Name", 1);
            Assert.That(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.BackToFront;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            Assert.IsFalse(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BackToFront;
            Assert.IsFalse(group.IsValid);
        }

        [Test]
        public void BlockPlacementSecondaryType_InvalidWhenTypesDuplicated()
        {
            PlanogramBlockingGroup group = PlanogramBlockingGroup.NewPlanogramBlockingGroup("Name", 1);
            Assert.That(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.BackToFront;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            Assert.IsFalse(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            Assert.IsFalse(group.IsValid);
        }

        [Test]
        public void BlockPlacementTertiaryType_InvalidWhenTypesDuplicated()
        {
            PlanogramBlockingGroup group = PlanogramBlockingGroup.NewPlanogramBlockingGroup("Name", 1);
            Assert.That(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.TopToBottom;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            Assert.IsFalse(group.IsValid);

            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            Assert.IsFalse(group.IsValid);
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        [Test]
        public void NewFromSource_InvalidBlockPlacementValues_SetsDefaults()
        {
            var sourceMock = new Mock<IPlanogramBlockingGroup>();
            sourceMock.Setup(g => g.Name).Returns("Group");
            sourceMock.Setup(g => g.BlockPlacementPrimaryType).Returns(PlanogramBlockingGroupPlacementType.LeftToRight);
            sourceMock.Setup(g => g.BlockPlacementSecondaryType).Returns(PlanogramBlockingGroupPlacementType.LeftToRight);
            sourceMock.Setup(g => g.BlockPlacementTertiaryType).Returns(PlanogramBlockingGroupPlacementType.LeftToRight);

            var newGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup(sourceMock.Object, new Mock<IResolveContext>().Object);

            Assert.AreEqual(PlanogramBlockingGroupPlacementType.LeftToRight, newGroup.BlockPlacementPrimaryType);
            Assert.AreEqual(PlanogramBlockingGroupPlacementType.BottomToTop, newGroup.BlockPlacementSecondaryType);
            Assert.AreEqual(PlanogramBlockingGroupPlacementType.FrontToBack, newGroup.BlockPlacementTertiaryType);
        }

        [Test]
        public void NewFromSource_ValidBlockPlacementValues_SetsValues()
        {
            var sourceMock = new Mock<IPlanogramBlockingGroup>();
            sourceMock.Setup(g => g.Name).Returns("Group");
            sourceMock.Setup(g => g.BlockPlacementPrimaryType).Returns(PlanogramBlockingGroupPlacementType.RightToLeft);
            sourceMock.Setup(g => g.BlockPlacementSecondaryType).Returns(PlanogramBlockingGroupPlacementType.TopToBottom);
            sourceMock.Setup(g => g.BlockPlacementTertiaryType).Returns(PlanogramBlockingGroupPlacementType.BackToFront);

            var newGroup = PlanogramBlockingGroup.NewPlanogramBlockingGroup(sourceMock.Object, new Mock<IResolveContext>().Object);

            Assert.AreEqual(PlanogramBlockingGroupPlacementType.RightToLeft, newGroup.BlockPlacementPrimaryType);
            Assert.AreEqual(PlanogramBlockingGroupPlacementType.TopToBottom, newGroup.BlockPlacementSecondaryType);
            Assert.AreEqual(PlanogramBlockingGroupPlacementType.BackToFront, newGroup.BlockPlacementTertiaryType);
        }

        #endregion

        #region Dto

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        #endregion
        
        #region Enumerate Positions in Sequence
        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForOneShelf()
        {
            const Int32 numberOfProducts = 3;
            var plan = "".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (Int32 i = 0; i < numberOfProducts; i++)
            {
                shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            var sequencedSubComp = group.EnumerateSequencableItemsInSequence<PlanogramSubComponentPlacement>(plan.GetPlanogramSubComponentPlacements()).First();

            var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, plan.Positions);

            var expectedItems = new[]
            {
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[2], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
            };

            for (Int32 i = 0; i < sequencedPositions.Count(); i++)
            {
                var expected = expectedItems.ElementAt(i);
                var actual = sequencedPositions.ElementAt(i);
                Assert.AreEqual(expected.Position, actual.Position);
                Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);

            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForTwoShelves_LeftToRight()
        {
            const Int32 numberOfProducts = 6;
            var plan = "Package".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            for (Int32 i = 0; i < numberOfProducts; i++)
            {
                var shelf = i < 3 ? shelf1 : shelf2;
                shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            using (var mgs = plan.GetMerchandisingGroups()) { foreach (var mg in mgs) { mg.Process(); mg.ApplyEdit(); } }
            
            var expectedItems = new[]
            {
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[2], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[5], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[4], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);
                } 
            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForThreeShelves_LeftToRight()
        {
            const Int32 numberOfProducts = 9;
            var plan = "Package".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 200, 0));
            for (Int32 i = 0; i < numberOfProducts; i++)
            {
                var shelf = i < 3 ? shelf1 : shelf2;
                shelf = i >=6 ? shelf3 : shelf;
                shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            using (var mgs = plan.GetMerchandisingGroups()) { foreach (var mg in mgs) { mg.Process(); mg.ApplyEdit(); } }
            
            var expectedItems = new[]
            {
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[2], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[5], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[4], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[6], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[7], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[8], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);

                }
            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForTwoShelves_RightToLeft()
        {
            const Int32 numberOfProducts = 6;
            var plan = "".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BackToFront;
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < numberOfProducts; i++)
            {
                var shelf = i < 3 ? shelf1 : shelf2;
                shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            using (var mgs = plan.GetMerchandisingGroups()) { foreach (var mg in mgs) { mg.Process(); mg.ApplyEdit(); } }

            var expectedItems = new[]
            {
                new { Position = plan.Positions[2], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[4], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[5], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);

                }
            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForThreeShelves_RightToLeft()
        {
            const Int32 numberOfProducts = 9;
            var plan = "".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BackToFront;
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 200, 0));
            for (Int32 i = 0; i < numberOfProducts; i++)
            {
                var shelf = i < 3 ? shelf1 : shelf2;
                shelf = i >= 6 ? shelf3 : shelf;
                shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            using (var mgs = plan.GetMerchandisingGroups()) { foreach (var mg in mgs) { mg.Process(); mg.ApplyEdit(); } }

            var expectedItems = new[]
            {
                new { Position = plan.Positions[2], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[4], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[5], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[8], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[7], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[6], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);

                }
            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForOnePeg()
        {
            var plan = "Pack".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
                        
            using (var mgs = plan.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)));
                    PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)), planogramPositionPlacement, PlanogramPositionAnchorDirection.ToRight);
                    PlanogramPositionPlacement planogramPositionPlacement2 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)), planogramPositionPlacement, PlanogramPositionAnchorDirection.Above);
                    PlanogramPositionPlacement planogramPositionPlacement3 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)), planogramPositionPlacement2, PlanogramPositionAnchorDirection.ToRight);
                    mg.Process(); mg.ApplyEdit();
                }
            }

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
                        
            var expectedItems = new[]
            {
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[2], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },                
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);

                }
            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForOneShelfOnePeg()
        {
            const Int32 numberOfProducts = 3;

            var plan = "Pack".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 100, 0));

            for (Int32 i = 0; i < numberOfProducts; i++)
            {
                shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }

            using (var mgs = plan.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.MerchandisingType == PlanogramSubComponentMerchandisingType.Hang)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)));
                        PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)), planogramPositionPlacement, PlanogramPositionAnchorDirection.ToRight);
                        PlanogramPositionPlacement planogramPositionPlacement2 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)), planogramPositionPlacement, PlanogramPositionAnchorDirection.Above);
                        PlanogramPositionPlacement planogramPositionPlacement3 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(50, 10, 10)), planogramPositionPlacement2, PlanogramPositionAnchorDirection.ToRight);
                    }
                    mg.Process(); mg.ApplyEdit();
                }
            }

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            
            var expectedItems = new[]
            {
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[2], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[4], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[5], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[6], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },                
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);

                }
            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForOneChest()
        {
            var plan = "Pack".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.TopToBottom;
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Chest);

            using (var mgs = plan.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)));
                    PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)), planogramPositionPlacement, PlanogramPositionAnchorDirection.Behind);
                    PlanogramPositionPlacement planogramPositionPlacement2 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)), planogramPositionPlacement, PlanogramPositionAnchorDirection.ToRight);
                    PlanogramPositionPlacement planogramPositionPlacement3 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)), planogramPositionPlacement2, PlanogramPositionAnchorDirection.Behind);
                    mg.Process(); mg.ApplyEdit();
                }
            }

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            
            var expectedItems = new[]
            {
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[2], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },                
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed);

                }
            }
        }

        [Test]
        public void EnumerateSubComponentPositionsInSequence_EnumeratesPositionsInSequence_ForOneShelfOneChest()
        {
            const Int32 numberOfProducts = 3;
            var plan = "Pack".CreatePackage().AddPlanogram();
            PlanogramBlockingGroup group = plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) }).Groups.First();
            group.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            group.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.LeftToRight;
            group.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.TopToBottom;
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));

            for (Int32 i = 0; i < numberOfProducts; i++)
            {
                shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }

            using (var mgs = plan.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)));
                        PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)), planogramPositionPlacement, PlanogramPositionAnchorDirection.Behind);
                        PlanogramPositionPlacement planogramPositionPlacement2 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)), planogramPositionPlacement, PlanogramPositionAnchorDirection.ToRight);
                        PlanogramPositionPlacement planogramPositionPlacement3 = mg.InsertPositionPlacement(plan.AddProduct(new WidthHeightDepthValue(10, 10, 30)), planogramPositionPlacement2, PlanogramPositionAnchorDirection.Behind);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }

            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            
            var expectedItems = new[]
            {
                new { Position = plan.Positions[0], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[1], ReversePrimary = true, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[2], ReversePrimary = false, ReverseSecondary = false, ReverseTertiary = false },
                new { Position = plan.Positions[6], ReversePrimary = true, ReverseSecondary = true, ReverseTertiary = false },                
                new { Position = plan.Positions[5], ReversePrimary = true, ReverseSecondary = true, ReverseTertiary = false },
                new { Position = plan.Positions[3], ReversePrimary = false, ReverseSecondary = true, ReverseTertiary = false },
                new { Position = plan.Positions[4], ReversePrimary = false, ReverseSecondary = true, ReverseTertiary = false },
            };
            Int32 expectedIndex = 0;
            foreach (var sequencedSubComp in group.EnumerateSequencableItemsInSequence(plan.GetPlanogramSubComponentPlacements()))
            {
                var sequencedPositions = group.EnumerateSubComponentPositionsInSequence(sequencedSubComp, sequencedSubComp.Item.GetPlanogramPositions());

                for (Int32 i = 0; i < sequencedPositions.Count(); i++)
                {
                    var expected = expectedItems.ElementAt(expectedIndex++);
                    var actual = sequencedPositions.ElementAt(i);
                    Assert.AreEqual(
                        expected.Position,
                        actual.Position,
                        String.Format("Expected {0}, but was {1}", expected.Position.GetPlanogramProduct().Gtin, ((PlanogramPosition)actual.Position).GetPlanogramProduct().Gtin));
                    Assert.AreEqual(expected.ReversePrimary, actual.IsPrimaryReversed, expected.Position.GetPlanogramProduct().Gtin);
                    Assert.AreEqual(expected.ReverseSecondary, actual.IsSecondaryReversed, expected.Position.GetPlanogramProduct().Gtin);
                    Assert.AreEqual(expected.ReverseTertiary, actual.IsTertiaryReversed, expected.Position.GetPlanogramProduct().Gtin);

                }
            }
        }
        #endregion
    }
}
