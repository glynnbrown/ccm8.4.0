﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-32587 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramBlockingDividerListTests
    {
        [Test]
        public void Remove_RemovesSurroundingDividersOfALowerLevel_Below()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var blocking = plan.AddBlocking();
            var dividerToRemove = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.25f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Remove(dividerToRemove);

            Assert.That(blocking.Dividers, Is.Empty);
            Assert.That(blocking.Groups, Has.Count.EqualTo(1));
            Assert.That(blocking.Locations, Has.Count.EqualTo(1));
            Assert.That(blocking.Locations.First().SpacePercentage, Is.EqualTo(1));
        }

        [Test]
        public void Remove_RemovesSurroundingDividersOfALowerLevel_Above()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var blocking = plan.AddBlocking();
            var dividerToRemove = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.75f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Remove(dividerToRemove);

            Assert.That(blocking.Dividers, Is.Empty);
            Assert.That(blocking.Groups, Has.Count.EqualTo(1));
            Assert.That(blocking.Locations, Has.Count.EqualTo(1));
            Assert.That(blocking.Locations.First().SpacePercentage, Is.EqualTo(1));
        }

        [Test]
        public void Remove_WhenDividerIsSurroundedByManyDividers_RemovesCorrectDividers()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var blocking = plan.AddBlocking();
            var d1 = blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var d2 = blocking.Dividers.Add(0.33f, 0.75f, PlanogramBlockingDividerType.Vertical);
            var d3 = blocking.Dividers.Add(0.66f, 0.75f, PlanogramBlockingDividerType.Horizontal);
            var d4 = blocking.Dividers.Add(0.66f, 0.85f, PlanogramBlockingDividerType.Vertical);

            blocking.Dividers.Remove(d3);

            Assert.That(blocking.Dividers, Is.EquivalentTo(new[] { d1, d2 }));
        }

        [Test]
        public void Remove_DoesntRemoveTooManyDividers()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var blocking = plan.AddBlocking();
            var d1 = blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var d2 = blocking.Dividers.Add(0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Remove(d2);

            Assert.That(blocking.Dividers, Has.Count.EqualTo(1));
            CollectionAssert.Contains(blocking.Dividers, d1);
        }
    }
}
