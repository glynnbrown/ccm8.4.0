﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM810

// V8-30033 : A.Silva
//  Created

#endregion

#region Version History : CCM 820
// V8-30818 : A.Kuszyk
//  Moved GetReserved tests from PlanogramPositionDetails.
#endregion

#region Version History : CCM 830
// V8-31856 : J.Pickup
//  SetMinimumUnits_IgnoresManuallyPlacedProductsByDefault
#endregion


#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using FluentAssertions;
using Galleria.Framework.Helpers;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public class PlanogramPositionPlacementTests : TestBase
    {
        #region Test Fixture Helpers

        private Planogram _planogram;
        private PlanogramFixture _fixture;
        private PlanogramFixtureItem _fixtureItem;

        [SetUp]
        public void SetUp()
        {
            _planogram = Planogram.NewPlanogram();
            _fixture = CreateFixture(_planogram, 200, 120, 75);
            _fixtureItem = AddFixtureItem(_planogram, _fixture);
        }

        private PlanogramPosition CreatePosition()
        {
            Package package = Package.NewPackage(1, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFC = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFC.Z = -1;

            //Add a base
            PlanogramFixtureComponent baseFC = fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponent shelfSub = shelf1C.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            shelfSub.IsProductOverlapAllowed = true;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelfSub, fixtureItem, shelf1FC);

            //Product 1
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 9F;
            prod1.Width = 10F;
            prod1.Depth = 8F;
            prod1.TrayHigh = 1;
            prod1.TrayWide = 2;
            prod1.TrayDeep = 3;
            prod1.TrayHeight = 9F;
            prod1.TrayWidth = 19F;
            prod1.TrayDepth = 31F;
            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 1;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 0F;

            return pos1;
        }

        private static void AssertRectValues(RectValue expected, RectValue actual)
        {
            AssertHelper.AreRectValuesEqual(expected, actual);
        }

        private static PlanogramComponent AddComponent(
            Single dividerSize,
            Single dividerSpacing,
            Single dividerStart,
            Boolean divAtStart,
            Boolean divAtEnd,
            Boolean divByFacing,
            Single compWidth,
            Single compHeight,
            Single compDepth,
            PlanogramComponentType compType,
            Planogram planogram)
        {
            var component = PlanogramComponent.NewPlanogramComponent("Shelf", compType, compWidth, compHeight, compDepth);
            var subComponent = component.SubComponents.First();
            planogram.Components.Add(component);
            subComponent = component.SubComponents.First();
            subComponent.IsDividerObstructionAtStart = divAtStart;
            subComponent.IsDividerObstructionByFacing = divByFacing;
            subComponent.IsDividerObstructionAtEnd = divAtEnd;
            subComponent.DividerObstructionDepth = dividerSize;
            subComponent.DividerObstructionHeight = dividerSize;
            subComponent.DividerObstructionWidth = dividerSize;
            subComponent.DividerObstructionSpacingX = dividerSpacing;
            subComponent.DividerObstructionSpacingY = dividerSpacing;
            subComponent.DividerObstructionSpacingZ = dividerSpacing;
            subComponent.DividerObstructionStartX = dividerStart;
            subComponent.DividerObstructionStartY = dividerStart;
            subComponent.DividerObstructionStartZ = dividerStart;
            return component;
        }

        private static PlanogramPosition AddPosition(
            Byte facings, Planogram planogram, PlanogramProduct product, PlanogramSubComponentPlacement subComponentPlacement, Int16 sequence = 1)
        {
            var position = PlanogramPosition.NewPlanogramPosition(sequence, product, subComponentPlacement);
            position.FacingsDeep = facings;
            position.FacingsHigh = facings;
            position.FacingsWide = facings;
            position.SequenceX = sequence;
            position.SequenceY = sequence;
            position.SequenceZ = sequence;
            planogram.Positions.Add(position);
            return position;
        }

        private static PlanogramProduct AddProduct(Single productSize, Single fingerSpacing, Planogram planogram)
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Height = productSize;
            product.Width = productSize;
            product.Depth = productSize;
            product.FingerSpaceToTheSide = fingerSpacing;
            planogram.Products.Add(product);
            return product;
        }

        private static PlanogramFixtureItem AddFixtureItem(Planogram planogram, PlanogramFixture fixture)
        {
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);
            return fixtureItem;
        }

        private static PlanogramFixtureComponent AddFixtureComponent(PlanogramComponent component, PlanogramFixture fixture)
        {
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixture.Components.Add(fixtureComponent);
            return fixtureComponent;
        }

        private static PlanogramFixture CreateFixture(Planogram planogram, Single fixHeight, Single fixWidth, Single fixDepth)
        {
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Height = fixHeight;
            fixture.Width = fixWidth;
            fixture.Depth = fixDepth;
            planogram.Fixtures.Add(fixture);
            return fixture;
        }

        private readonly List<PlanogramComponentType> _allMerchandisableComponentTypesExceptCustom =
            Enum.GetValues(typeof (PlanogramComponentType))
                .Cast<PlanogramComponentType>()
                .Where(
                    v =>
                    v != PlanogramComponentType.Custom && v != PlanogramComponentType.Backboard &&
                    v != PlanogramComponentType.Base && v != PlanogramComponentType.Panel)
                .ToList();
        private readonly List<AxisType> _allAxisTypes =
            Enum.GetValues(typeof(AxisType))
                .Cast<AxisType>()
                .ToList();

        #endregion


        [Test]
        public void GetPegHoles_WhenOneFacing_ShouldReturnPegHoleCoordinates([ValueSource("_allMerchandisableComponentTypesExceptCustom")] PlanogramComponentType type)
        {
            if (type == PlanogramComponentType.SlotWall) Assert.Ignore("This test currently fails for slot walls, so it has been excluded from the smoke tests");
            const String requirementHasPegs = "Type {0} does not support pegs.";
            const String expectation = "The placement should return the correct coordinates for each facing peghole.";
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItem = planogram.AddFixtureItem();
            PlanogramFixtureComponent fixtureComponent = fixtureItem.AddFixtureComponent(type);
            PlanogramSubComponent subComponent = fixtureComponent.GetPlanogramComponent().SubComponents.First();
            Boolean hasRow1 = (subComponent.MerchConstraintRow1SpacingY > 0 && subComponent.MerchConstraintRow1Height > 0 && subComponent.MerchConstraintRow1Width > 0);
            Boolean hasRow2 = (subComponent.MerchConstraintRow2SpacingY > 0 && subComponent.MerchConstraintRow2Height > 0 && subComponent.MerchConstraintRow2Width > 0);
            if (!hasRow1 && !hasRow2)
            {
                return;
                Assert.Inconclusive(requirementHasPegs, type.GetType().Name);
            }
            PlanogramPosition position = fixtureComponent.AddPosition(fixtureItem);
            planogram.ReprocessAllMerchandisingGroups();

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                PlanogramPositionPlacement placement = merchandisingGroups.First().PositionPlacements.First();
                List<Tuple<Single, Single>> actual = placement.GetPegHoles().Select(o => new Tuple<Single, Single>(o.X, o.Y)).ToList();

                List<Tuple<Single, Single>> pegs = subComponent.GetPegHoles().All.Select(o => new Tuple<Single, Single>(o.X, o.Y)).ToList();
                CollectionAssert.IsSubsetOf(actual, pegs, expectation);
            }
        }

        [Test]
        public void GetPegHoles_WhenMultipleFacingInOneRow_ShouldReturnPegHoleCoordinates([ValueSource("_allMerchandisableComponentTypesExceptCustom")] PlanogramComponentType type)
        {
            if (type == PlanogramComponentType.SlotWall) Assert.Ignore("This test currently fails for slot walls, so it has been excluded from the smoke tests");
            const String requirementHasPegs = "Type {0} does not support pegs.";
            const String expectation = "The placement should return the correct coordinates for each facing peghole.";
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItem = planogram.AddFixtureItem();
            PlanogramFixtureComponent fixtureComponent = fixtureItem.AddFixtureComponent(type);
            PlanogramSubComponent subComponent = fixtureComponent.GetPlanogramComponent().SubComponents.First();
            Boolean hasRow1 = (subComponent.MerchConstraintRow1SpacingY > 0 && subComponent.MerchConstraintRow1Height > 0 && subComponent.MerchConstraintRow1Width > 0);
            Boolean hasRow2 = (subComponent.MerchConstraintRow2SpacingY > 0 && subComponent.MerchConstraintRow2Height > 0 && subComponent.MerchConstraintRow2Width > 0);
            if (!hasRow1 && !hasRow2)
            {
                return;
                Assert.Inconclusive(requirementHasPegs, type.GetType().Name);
            }
            PlanogramPosition position = fixtureComponent.AddPosition(fixtureItem);
            if (subComponent.GetPegHoles().All.GroupBy(p => p.X).Count() == 1)
            {
                position.FacingsHigh = 5;
            }
            else
            {
                position.FacingsWide = 5;
            }
            planogram.ReprocessAllMerchandisingGroups();

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                PlanogramPositionPlacement placement = merchandisingGroups.First().PositionPlacements.First();
                List<Tuple<Single, Single>> actual = placement.GetPegHoles().Select(o => new Tuple<Single, Single>(o.X, o.Y)).ToList();

                List<Tuple<Single, Single>> pegs = subComponent.GetPegHoles().All.Select(o => new Tuple<Single, Single>(o.X, o.Y)).ToList();
                Int32 countItems = 0;

                foreach (Tuple<Single, Single> oActualItem in actual)
                {
                    foreach (Tuple<Single, Single> oPegItem in pegs)
                    {
                        if (MathHelper.EqualTo(oActualItem.Item1, oPegItem.Item1) && MathHelper.EqualTo(oActualItem.Item2, oPegItem.Item2))
                        {
                            countItems = countItems + 1;
                            break;
                        }
                    }
                }

                Assert.AreEqual(countItems, actual.Count);
            }
        }

        #region GetReservedSpace

        private IEnumerable<Byte> _facings
        {
            get
            {
                yield return 1;
                yield return 2;
                yield return 3;
                yield return 4;
                yield return 5;
                yield return 6;
                yield return 7;
                yield return 8;
                yield return 9;
                yield return 10;
            }
        }

        private IEnumerable<Object[]> _getReservedSpaceSource
        {
            get
            {
                for (Byte facings = 1; facings <= 5; facings++)
                {
                    for (Single productSize = 5f; productSize <= 10f; productSize++)
                    {
                        for (Single fingerSpacing = 1f; fingerSpacing <= 3f; fingerSpacing++)
                        {
                            for (Single dividerSize = 1f; dividerSize <= 3f; dividerSize++)
                            {
                                for (Single dividerSpacing = 2f; dividerSpacing <= 4f; dividerSpacing++)
                                {
                                    yield return new Object[] { facings, productSize, fingerSpacing, dividerSize, dividerSpacing };
                                }
                            }
                        }
                    }
                }
                yield break;
            }
        }

        [Test]
        [TestCaseSource("_getReservedSpaceSource")]
        public void GetReservedSpace_LeftStacked_DividersAtStartEndFacings_OnePosition(
            Byte facings, Single productSize, Single fingerSpacing, Single dividerSize, Single dividerSpacing)
        {
            var component = AddComponent(dividerSize, dividerSpacing, 0, true, true, true, 120, 4, 75, PlanogramComponentType.Shelf, _planogram);
            var fixtureComponent = AddFixtureComponent(component, _fixture);
            var product = AddProduct(productSize, fingerSpacing, _planogram);
            var subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(component.SubComponents.First(), _fixtureItem, fixtureComponent);
            var position = AddPosition(facings, _planogram, product, subComponentPlacement);

            PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subComponentPlacement);
            merchGroup.Process();

            var positionPlacement = merchGroup.PositionPlacements.First(p => Object.Equals(p.Position.Id, position.Id));
            RectValue actual = positionPlacement.GetReservedSpace();

            RectValue expected = new RectValue(
                0, 4, 75 - productSize * facings,
                dividerSize * (facings + 1) + productSize * facings + fingerSpacing * facings,
                productSize * facings,
                productSize * facings);
            AssertRectValues(expected, actual);
        }

        [Test]
        [TestCaseSource("_getReservedSpaceSource")]
        public void GetReservedSpace_LeftStacked_DividersAtStartEnd_OnePosition(
            Byte facings, Single productSize, Single fingerSpacing, Single dividerSize, Single dividerSpacing)
        {
            var component = AddComponent(dividerSize, dividerSpacing, 0, true, true, false, 120, 4, 75, PlanogramComponentType.Shelf, _planogram);
            var fixtureComponent = AddFixtureComponent(component, _fixture);
            var product = AddProduct(productSize, fingerSpacing, _planogram);
            var subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(component.SubComponents.First(), _fixtureItem, fixtureComponent);
            var position = AddPosition(facings, _planogram, product, subComponentPlacement);

            PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subComponentPlacement);
            merchGroup.Process();

            var positionPlacement = merchGroup.PositionPlacements.First(p => Object.Equals(p.Position.Id, position.Id));
            RectValue actual = positionPlacement.GetReservedSpace();

            RectValue expected = new RectValue(
                0, 4, 75 - productSize * facings - dividerSize,
                dividerSize + Convert.ToSingle(
                    Math.Ceiling((productSize * facings + fingerSpacing * facings + dividerSize) / dividerSpacing)) * dividerSpacing,
                productSize * facings,
                productSize * facings);
            AssertRectValues(expected, actual);
        }

        [Test]
        [TestCaseSource("_getReservedSpaceSource")]
        public void GetReservedSpace_LeftStacked_DividerStartValue_TwoPositions(
            Byte facings, Single productSize, Single fingerSpacing, Single dividerSize, Single dividerSpacing)
        {
            const Single dividerStart = 5;

            var component = AddComponent(dividerSize, dividerSpacing, dividerStart, false, false, false, 120, 4, 75, PlanogramComponentType.Shelf, _planogram);
            var fixtureComponent = AddFixtureComponent(component, _fixture);
            var product = AddProduct(productSize, fingerSpacing, _planogram);
            var subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(component.SubComponents.First(), _fixtureItem, fixtureComponent);
            var singlePosition = AddPosition(1, _planogram, product, subComponentPlacement, 1);
            var position = AddPosition(facings, _planogram, product, subComponentPlacement, 2);

            PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subComponentPlacement);
            merchGroup.Process();

            var positionPlacement = merchGroup.PositionPlacements.First(p => Object.Equals(p.Position.Id, position.Id));
            RectValue actual = positionPlacement.GetReservedSpace();

            // Calculate expected value. For this formula to work, product size must be greater or equal to divider start.
            Assert.GreaterOrEqual(productSize, dividerStart);
            Single singlePositionWidth = Convert.ToSingle(
                Math.Ceiling((productSize + fingerSpacing - dividerStart) / dividerSpacing) * dividerSpacing + dividerStart);
            RectValue expected = new RectValue(
                singlePositionWidth + dividerSize, 4, 75 - productSize * facings,
                Convert.ToSingle(Math.Ceiling(((productSize + fingerSpacing) * facings + dividerSize) / dividerSpacing)) * dividerSpacing - dividerSize,
                productSize * facings,
                productSize * facings);
            AssertRectValues(expected, actual);
        }

        [Test]
        [TestCaseSource("_getReservedSpaceSource")]
        public void GetReservedSpace_RightStacked_DividersAtStartEndFacings_OnePosition(
            Byte facings, Single productSize, Single fingerSpacing, Single dividerSize, Single dividerSpacing)
        {
            var component = AddComponent(dividerSize, dividerSpacing, 0, true, true, true, 120, 4, 75, PlanogramComponentType.Shelf, _planogram);
            component.SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            var fixtureComponent = AddFixtureComponent(component, _fixture);
            var product = AddProduct(productSize, fingerSpacing, _planogram);
            var subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(component.SubComponents.First(), _fixtureItem, fixtureComponent);
            var position = AddPosition(facings, _planogram, product, subComponentPlacement);

            PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subComponentPlacement);
            merchGroup.Process();

            var positionPlacement = merchGroup.PositionPlacements.First(p => Object.Equals(p.Position.Id, position.Id));
            RectValue actual = positionPlacement.GetReservedSpace();

            RectValue expected = new RectValue(
                120 - (dividerSize * (facings + 1) + productSize * facings + fingerSpacing * facings), 4, 75 - productSize * facings,
                dividerSize * (facings + 1) + productSize * facings + fingerSpacing * facings,
                productSize * facings,
                productSize * facings);
            AssertRectValues(expected, actual);
        }

        [Test]
        [TestCaseSource("_getReservedSpaceSource")]
        public void GetReservedSpace_RightStacked_DividersAtStartEnd_OnePosition(
            Byte facings, Single productSize, Single fingerSpacing, Single dividerSize, Single dividerSpacing)
        {
            var component = AddComponent(
                dividerSize, dividerSpacing, 0,
                /*divAtStart*/true, /*divAtEnd*/true, /*divByFacing*/false,
                120, 4, 75, PlanogramComponentType.Shelf, _planogram);
            component.SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            var fixtureComponent = AddFixtureComponent(component, _fixture);
            var product = AddProduct(productSize, fingerSpacing, _planogram);
            var subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(component.SubComponents.First(), _fixtureItem, fixtureComponent);
            var position = AddPosition(facings, _planogram, product, subComponentPlacement);

            PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subComponentPlacement);
            merchGroup.Process();

            var positionPlacement = merchGroup.PositionPlacements.First(p => Object.Equals(p.Position.Id, position.Id));
            RectValue actual = positionPlacement.GetReservedSpace();

            RectValue expected = new RectValue(
                120 - (dividerSize + Convert.ToSingle(Math.Ceiling((productSize * facings + fingerSpacing * facings + dividerSize) / dividerSpacing)) * dividerSpacing),
                4, 75 - productSize * facings - dividerSize,
                dividerSize + Convert.ToSingle(Math.Ceiling((productSize * facings + fingerSpacing * facings + dividerSize) / dividerSpacing)) * dividerSpacing,
                productSize * facings,
                productSize * facings);
            AssertRectValues(expected, actual);
        }

        [Test]
        [TestCaseSource("_getReservedSpaceSource")]
        public void GetReservedSpace_RightStacked_DividerStartValue_TwoPositions(
            Byte facings, Single productSize, Single fingerSpacing, Single dividerSize, Single dividerSpacing)
        {
            const Single dividerStart = 5;

            var component = AddComponent(dividerSize, dividerSpacing, dividerStart, false, false, false, 120, 4, 75, PlanogramComponentType.Shelf, _planogram);
            component.SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.RightStacked;
            var fixtureComponent = AddFixtureComponent(component, _fixture);
            var product = AddProduct(productSize, fingerSpacing, _planogram);
            var subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(component.SubComponents.First(), _fixtureItem, fixtureComponent);
            var singlePosition = AddPosition(1, _planogram, product, subComponentPlacement, 2);
            var position = AddPosition(facings, _planogram, product, subComponentPlacement, 1);

            PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subComponentPlacement);
            merchGroup.Process();

            var positionPlacement = merchGroup.PositionPlacements.First(p => Object.Equals(p.Position.Id, position.Id));
            RectValue actual = positionPlacement.GetReservedSpace();

            // Calculate expected value. For this formula to work, product size must be greater or equal to divider start.
            Assert.GreaterOrEqual(productSize, dividerStart);
            Single singlePositionWidth = Convert.ToSingle(
                Math.Ceiling((productSize + fingerSpacing - dividerStart) / dividerSpacing) * dividerSpacing + dividerStart);
            Single expectedPositionWidth = Convert.ToSingle(
                Math.Ceiling(((productSize + fingerSpacing) * facings + dividerSize) / dividerSpacing)) * dividerSpacing;
            Single expectedPositionX = 120 - singlePositionWidth - expectedPositionWidth;
            RectValue expected = new RectValue(
                expectedPositionX,
                4, 75 - productSize * facings,
                expectedPositionWidth,
                productSize * facings,
                productSize * facings);
            AssertRectValues(expected, actual);
        }

        [Test]
        public void GetReservedSpace_OnEvenShelfWithDividers_ShouldReturnCorrectX()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionHeight = 2;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionWidth = 1f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionSpacingX = 3f;
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            plan.ReprocessAllMerchandisingGroups();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var mg = mgs.First();

                var reservedSpace = mg.PositionPlacements.ElementAt(1).GetReservedSpace();

                reservedSpace.Should().Be(
                    new RectValue(109, 4, 65, 11, 10, 10),
                    "because the reserved space should start at the nearest divider slot to the left of the position");
            }
        }

        [Test]
        public void GetReservedSpace_OnEvenShelfWithDividersThreePositions_ShouldReturnCorrectX()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(177.5f,4.2f, 57f));
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionHeight = 2;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionWidth = 0.28f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionSpacingX = 1.82f;
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 20;
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(26, 6, 7.5f)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(26, 6, 7.5f)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(28, 6, 7.5f)));
            plan.ReprocessAllMerchandisingGroups();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                foreach (var p in mg.PositionPlacements) p.SetUnits(21, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, new Planograms.Helpers.AssortmentRuleEnforcer());
                mg.Process();
                mg.ApplyEdit();
                //plan.TakeSnapshot("GetReservedSpace_OnEvenShelfWithDividersThreePositions_ShouldReturnCorrectX");

                var actualReservedSpace = mg.PositionPlacements.Select(p => p.GetReservedSpace()).ToList();

                actualReservedSpace[0].Should().Be(new RectValue(0, 4.2f, 4.5f, 27.58f, 18f, 52.5f));
                actualReservedSpace[1].Should().Be(new RectValue(73.08f, 4.2f, 4.5f, 27.3f, 18f, 52.5f));
                actualReservedSpace[2].Should().Be(new RectValue(145.88f, 4.2f, 4.5f, 28.84f, 18f, 52.5f));
            }
        }

        [Test]
        public void GetReservedSpace_OnEvenShelfWithNegativeOverhangAndDividers_ShouldReturnCorrectWidth()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(180, 4.2f, 57));
            shelf.GetPlanogramComponent().SubComponents[0].LeftOverhang = -1.25f;
            shelf.GetPlanogramComponent().SubComponents[0].RightOverhang = -1.25f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionWidth = 0.28f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionHeight = 0.2f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionSpacingX = 1.82f;
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 20f;
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            var pos1 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(14.6f, 14.6f, 2.7f)));
            var pos2 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(22.2f, 7.7f, 3.8f)));
            var pos3 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(22.2f, 7.7f, 3.8f)));
            var pos4 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10.5f, 19f, 6f)));
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                mg.PositionPlacements[0].SetUnits(42, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, null);
                mg.PositionPlacements[1].SetUnits(90, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, null);
                mg.PositionPlacements[2].SetUnits(60, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, null);
                mg.PositionPlacements[3].SetUnits(27, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, null);
                mg.Process();
                mg.ApplyEdit();
                //plan.TakeSnapshot("GetReservedSpace_OnEvenShelfWithNegativeOverhangAndDividers_ShouldReturnCorrectWidth");

                var reservedSpaces = mg.PositionPlacements.Select(p=> p.GetReservedSpace()).ToList();

                reservedSpaces[0].X.Should().Be(1.25f, "because the first product should be positioned up against the left negative overhang");
                reservedSpaces[0].Width.Should().BeApproximately(29.97f,0.01f, "because the width of the first product should start from the negative overhang and end at the right hand side of the next divider");
                reservedSpaces[1].X.Should().BeApproximately(31.22f, 0.01f, "because the second product's space should start at the end of the first product's space");
                reservedSpaces[1].Width.Should().BeApproximately(67.34f, 0.01f, "because the width of the second product should not exceed the dividers it covers");
                mg.IsOverfilled().Should().BeFalse("because the merchandising group should be full, but not overfilled");
            }
        }

        #endregion

        [Test]
        public void SetMinimumUnits_IgnoresManuallyPlacedProductsByDefault()
        { 
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixtureItem = planogram.AddFixtureItem();
            PlanogramFixtureComponent fixtureComponent = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramSubComponent subComponent = fixtureComponent.GetPlanogramComponent().SubComponents.First();
            
            PlanogramPosition position = fixtureComponent.AddPosition(fixtureItem);
            planogram.ReprocessAllMerchandisingGroups();

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                PlanogramPositionPlacement placement = merchandisingGroups.First().PositionPlacements.First();

                placement.Position.IsManuallyPlaced = true;
                placement.Position.FacingsDeep = 5;

                placement.SetMinimumUnits();

                Assert.IsTrue(placement.Position.FacingsDeep == 5);

                placement.Position.IsManuallyPlaced = false;

                placement.SetMinimumUnits();
                Assert.IsTrue(position.FacingsDeep == 1);
            }
        }
    }
}
