﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26986 : A.Kuszyk
//  Created.
// V8-27397 : A.Kuszyk
//  Change references to fixture width and planogram height to use GetDesignSize method on Planogram.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramFixtureItemListTests
    {
        private Planogram _planogram;
        private PlanogramFixture _fixture;
        private const Single _fixtureWidth = 10f;
        
        [SetUp]
        public void PlanogramFixtureItemListSetup()
        {
            _planogram = Planogram.NewPlanogram();
            _fixture = PlanogramFixture.NewPlanogramFixture();
            _fixture.Width = _fixtureWidth;
            _planogram.Fixtures.Add(_fixture);
        }

        [Test]
        public void GetRelativeFixtureBoundaries_GetsBoundaries()
        {
            const Int32 numberOfFixtures = 5;
            var testList = _planogram.FixtureItems;
            var expected = new List<Single>();
            for (Single i = 0; i < numberOfFixtures; i++)
            {
                testList.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
                var expectedValue = i / numberOfFixtures;
                if(expectedValue>0f && expectedValue<1f) expected.Add(expectedValue);
            }

            var result = testList.GetRelativeFixtureBoundaries().ToList();

            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void GetRelativeFixtureBoundaries_DoesNotReturnOne()
        {
            const Int32 numberOfFixtures = 5;
            var testList = _planogram.FixtureItems;
            for (Single i = 0; i < numberOfFixtures; i++)
            {
                testList.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            }

            var result = testList.GetRelativeFixtureBoundaries().ToList();

            CollectionAssert.DoesNotContain(result, 1f);
        }

        [Test]
        public void GetRelativeFixtureBoundaries_DoesNotReturnZero()
        {
            const Int32 numberOfFixtures = 5;
            var testList = _planogram.FixtureItems;
            for (Single i = 0; i < numberOfFixtures; i++)
            {
                testList.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            }

            var result = testList.GetRelativeFixtureBoundaries().ToList();

            CollectionAssert.DoesNotContain(result,0f);
        }
    }
}
