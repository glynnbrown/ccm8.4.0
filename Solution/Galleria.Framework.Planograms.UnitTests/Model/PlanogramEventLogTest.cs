﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26836 : L.Luong 
//   Created (Auto-generated)

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;
using Galleria.Ccm.Model;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public sealed class PlanogramEventLogTest : TestBase
    {
        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramEventLog.NewPlanogramEventLog());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewEventLog()
        {
            // new Event log 
            var model = PlanogramEventLog.NewPlanogramEventLog();

            //check if Event Log is child
            Assert.That(model.IsChild);
            Assert.IsNotNull(model.Id);
        }

        [Test]
        public void Fetch()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            package.Planograms[0].EventLogs.Add(PlanogramEventLog.NewPlanogramEventLog());
            package = package.Save();

            //fetch
            Planograms.Model.Package packageModel = Planograms.Model.Package.FetchById(
                package.Id, 1, PackageLockType.User);
            PlanogramEventLog model = packageModel.Planograms[0].EventLogs[0];
            Assert.AreEqual(model.Id, package.Planograms[0].EventLogs[0].Id);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set validation template to planogram
            package.Planograms[0].EventLogs.Add(PlanogramEventLog.NewPlanogramEventLog());
            package = package.Save();

            //check if consumer decision tree is not null
            Assert.IsNotNull(package.Planograms[0].EventLogs[0]);
        }

        [Test]
        public void DataAccess_Update()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);
            String check = "Event1";

            // set Eventlog to planogram
            package.Planograms[0].EventLogs.Add(PlanogramEventLog.NewPlanogramEventLog());
            package.Planograms[0].EventLogs[0].Description = check;

            // save
            package = package.Save();
            PackageDto dto;

            // fetch
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check first value is equal
            Assert.AreEqual(check, package.Planograms[0].EventLogs[0].Description);

            // change value
            check = "Event2";
            package.Planograms[0].EventLogs[0].Description = check;
            package = package.Save();

            // refetch
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            // check if value has changed
            Assert.AreEqual(check, package.Planograms[0].EventLogs[0].Description);
        }

        [Test]
        public void DataAccess_Delete()
        {
            //create new package
            var planogram = CreatePlanogram();
            var package = CreatePackage(planogram);

            // set Eventlog to planogram
            package.Planograms[0].EventLogs.Add(PlanogramEventLog.NewPlanogramEventLog());

            //save
            package = package.Save();
            PackageDto dto;

            // get dto
            var modelPackage = package.Planograms[0].EventLogs[0];
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }
            Assert.AreEqual(1, package.Planograms[0].EventLogs.Count);

            // remove child level
            package.Planograms[0].EventLogs.Remove(modelPackage);
            package = package.Save();

            // consumer decision tree dto should be null
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPackageDal dal = dalContext.GetDal<IPackageDal>())
                {
                    dto = dal.FetchById(package.Id);
                }
            }

            Assert.AreEqual(0, package.Planograms[0].EventLogs.Count);

        }

        #endregion

        #region Test Helper Methods

        private Galleria.Framework.Planograms.Model.Package CreatePackage(Planogram planogram)
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Planograms.Add(planogram);
            package.Name = "PackageName";
            return package;
        }

        private Planogram CreatePlanogram()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            return planogram;
        }

        #endregion
    }
}
