﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-27748 : A.Kuszyk
//  Created.
// V8-27773 : A.Kuszyk
//  Added tests for GetPositionsThatIntersectLine.

#endregion

#region Version History: CCM802

// V8-29105 : A.Silva
//      Removed obsolete tests for the GetAvailableSpace, GetPositionsThatIntersectLine methods removed from PlanogramMerchandisingGroup.

#endregion

#region Version History: CCM803

// V8-29280 : A.Silva
//      Added IsOverfilled and HasOverlapping tests.

#endregion

#region Version History : CCM810

// V8-29137 : A.Kuszyk
//  Added tests for CorrectPositionSequencing().
// V8-28766 : J.Pickup
//  Added autofill. unit tests.
// V8-29938 : A.Kuszyk
//  Added test for inserting products onto an Even Chest.
// V8-30003 : A.Silva
//  Amended test to test overfill for all components.
// V8-30004 : A.Kuszyk
//  Added min/max tests for Initialize method.
// V8-30206 : A.Kuszyk
//  Added a test for peghole snapping issues.
// V8-30238 : J.Pickup
//  Added test for overfill for when not using a stacked merch type.
// V8-28878 : A.Silva
//  Amended IsOverfilled_WhenNotOverfilled_ShouldReturnFalse for Axis.X and pegs, when the test would inadvertently overfill and think it had not.

#endregion

#region Version History : CCM8111

// V8-30334 : A.Silva
//  Updated usage of GetWorldMerchandisingSpaceBounds in tests using it, so that a prefetched collection of sub component placements is used for the planogram.

#endregion

#region Version History : CCM820

// V8-31525 : J.Pickup
//  Max stack overfill test added.

#endregion

#region Version History: CCM830

// V8-32189 : A.Silva
//  Refactored tests for Overfill check, and added category to run tests together.
// V8-32775 : A.Silva
//  Added test for Placement with Dividers on the X axis so that the dividers snap correctly.
// V8-18307 : J.Pickup
//  Adjusted expected sequence number due to changes in how sequence number is applied. 

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FluentAssertions;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using NUnit.Framework;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture, Category(Categories.Smoke)]
    public class PlanogramMerchandisingGroupTests
    {
        private const String MerchandisingGroupIsOverfilledCheck = "Merchandising Group IsOverfilled Check";
        private const Single AnySize = 10F;

        #region Test Fixture Helpers

        private readonly List<PlanogramComponentType> _allMerchandisableComponentTypesExceptCustom =
            Enum.GetValues(typeof (PlanogramComponentType))
                .Cast<PlanogramComponentType>()
                .Where(
                    v =>
                    v != PlanogramComponentType.Custom && v != PlanogramComponentType.Backboard &&
                    v != PlanogramComponentType.Base && v != PlanogramComponentType.Panel)
                .ToList();
        private readonly List<AxisType> _allAxisTypes =
            Enum.GetValues(typeof(AxisType))
                .Cast<AxisType>()
                .ToList();

        private static Planogram CreateShelfTestPlan()
        {
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a backboard
            PlanogramFixtureComponent backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);

            shelf1Fc.Y = 105;

            PlanogramSubComponent shelfSub = shelf1C.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            shelfSub.IsProductOverlapAllowed = true;

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelfSub, fixtureItem, shelf1Fc);

            //Product 1 (w: 8.2, h: 32.2, d: 73.8)
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            plan.Products.Add(prod1);

            PlanogramPosition pos1 = subComponentPlacement.AddPosition(prod1);
            pos1.FacingsHigh = 1;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 9;
            pos1.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos1.OrientationType = PlanogramPositionOrientationType.Front0;
            pos1.X = 0;
            pos1.Y = 4;
            pos1.Z = 2.7F;
            pos1.TotalUnits = 9;
            pos1.Sequence = 1;

            //Product 2- (w: 17, h: 32, d: 76.5)
            PlanogramProduct prod2 = PlanogramProduct.NewPlanogramProduct();
            prod2.Gtin = "p2";
            prod2.Name = "p2";
            prod2.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod2.Height = 32F;
            prod2.Width = 8.5F;
            prod2.Depth = 8.5F;
            plan.Products.Add(prod2);

            PlanogramPosition pos2 = subComponentPlacement.AddPosition(prod2);
            pos2.FacingsHigh = 1;
            pos2.FacingsWide = 2;
            pos2.FacingsDeep = 9;
            pos2.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos2.OrientationType = PlanogramPositionOrientationType.Front0;
            pos2.X = 17.37F;
            pos2.Y = 4;
            pos2.Z = 0;
            pos2.TotalUnits = 9;
            pos2.Sequence = 2;

            //Product 3 - (w: 25, h:23.2 , d:39 )
            PlanogramProduct prod3 = PlanogramProduct.NewPlanogramProduct();
            prod3.Gtin = "p3";
            prod3.Name = "p3";
            prod3.MerchandisingStyle = PlanogramProductMerchandisingStyle.Tray;
            prod3.Height = 23F;
            prod3.Width = 24.5F;
            prod3.Depth = 12F;
            prod3.TrayHeight = 23.2F;
            prod3.TrayWidth = 25;
            prod3.TrayDepth = 39;
            prod3.TrayHigh = 1;
            prod3.TrayWide = 1;
            prod3.TrayDeep = 3;
            plan.Products.Add(prod3);

            PlanogramPosition pos3 = subComponentPlacement.AddPosition(prod3);
            pos3.FacingsHigh = 1;
            pos3.FacingsWide = 1;
            pos3.FacingsDeep = 1;
            pos3.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos3.OrientationType = PlanogramPositionOrientationType.Front0;
            pos3.X = 44.05F;
            pos3.Y = 4;
            pos3.Z = 37.5F;
            pos3.TotalUnits = 1;
            pos3.Sequence = 3;

            //Product 4 - (w: 8.5, h: 30.8, d: 76.5)
            PlanogramProduct prod4 = PlanogramProduct.NewPlanogramProduct();
            prod4.Gtin = "p4";
            prod4.Name = "p4";
            prod4.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod4.Height = 30.8F;
            prod4.Width = 8.5F;
            prod4.Depth = 8.5F;
            plan.Products.Add(prod4);

            PlanogramPosition pos4 = subComponentPlacement.AddPosition(prod4);
            pos4.FacingsHigh = 1;
            pos4.FacingsWide = 1;
            pos4.FacingsDeep = 9;
            pos4.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos4.OrientationType = PlanogramPositionOrientationType.Front0;
            pos4.X = 77.92F;
            pos4.Y = 4;
            pos4.Z = 0F;
            pos4.TotalUnits = 9;
            pos4.Sequence = 4;

            //Product 5 - (w: 25, h: 30.9, d: 34)
            PlanogramProduct prod5 = PlanogramProduct.NewPlanogramProduct();
            prod5.Gtin = "p5";
            prod5.Name = "p5";
            prod5.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod5.Height = 30.9F;
            prod5.Width = 25F;
            prod5.Depth = 17F;
            plan.Products.Add(prod5);

            PlanogramPosition pos5 = subComponentPlacement.AddPosition(prod5);
            pos5.FacingsHigh = 1;
            pos5.FacingsWide = 1;
            pos5.FacingsDeep = 2;
            pos5.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
            pos5.OrientationType = PlanogramPositionOrientationType.Front0;
            pos5.X = 95F;
            pos5.Y = 4;
            pos5.Z = 42.5F;
            pos5.TotalUnits = 2;
            pos5.Sequence = 5;

            
            return plan;
        }

        private static Planogram CreateCombinedShelfTestPlan()
        {
            Planogram plan = CreateShelfTestPlan();

            //add a fixture
            PlanogramFixtureItem fixtureItem = plan.FixtureItems.Add();
            PlanogramFixture fixture = plan.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;
            fixtureItem.X = 120;

            //add a backboard
            PlanogramFixtureComponent backboardFc =
                fixture.Components.Add(PlanogramComponentType.Backboard, fixture.Width, fixture.Height, 1);
            backboardFc.Z = -1;

            //Add a base
            fixture.Components.Add(PlanogramComponentType.Base, fixture.Width, 10, 76.5F);

            //Add a shelf
            PlanogramFixtureComponent shelf1Fc = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = plan.Components.FindById(shelf1Fc.PlanogramComponentId);

            shelf1Fc.Y = 105;

            PlanogramSubComponent shelfSub = shelf1C.SubComponents[0];
            shelfSub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            shelfSub.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
            shelfSub.IsProductOverlapAllowed = true;

            shelfSub.CombineType = PlanogramSubComponentCombineType.Both;


            return plan;
        }

        /// <summary>
        ///     Centers <paramref name="fixtureComponent"/> in <paramref name="fixtureItem"/>.
        /// </summary>
        /// <param name="fixtureItem"></param>
        /// <param name="fixtureComponent"></param>
        private static void CenterComponent(PlanogramFixtureItem fixtureItem, IPlanogramFixtureComponent fixtureComponent)
        {
            PlanogramFixture fixture = fixtureItem.GetPlanogramFixture();
            Single fixtureCenterX = fixtureItem.X + fixture.Width / 2;
            Single fixtureCenterY = fixtureItem.Y + fixture.Height / 2;
            PlanogramComponent component = fixtureComponent.GetPlanogramComponent();
            fixtureComponent.X = fixtureCenterX - component.Width / 2;
            fixtureComponent.Y = fixtureCenterY - component.Height / 2;
        }

        private static void SetMerchStrategy(IPlanogramFixtureComponent component, AxisType axis)
        {
            PlanogramSubComponent subComponent = component.GetPlanogramComponent().SubComponents.First();
            subComponent.MerchandisingStrategyX = axis == AxisType.X
                                                      ? PlanogramSubComponentXMerchStrategyType.LeftStacked
                                                      : PlanogramSubComponentXMerchStrategyType.Left;
            subComponent.MerchandisingStrategyY = axis == AxisType.Y
                                                      ? PlanogramSubComponentYMerchStrategyType.BottomStacked
                                                      : PlanogramSubComponentYMerchStrategyType.Bottom;
            subComponent.MerchandisingStrategyZ = axis == AxisType.Z
                                                      ? PlanogramSubComponentZMerchStrategyType.FrontStacked
                                                      : PlanogramSubComponentZMerchStrategyType.Front;
        }

        private static void SetSequence(PlanogramPosition position, AxisType axis, Int16 value)
        {
            switch (axis)
            {
                case AxisType.X:
                    position.SequenceX = value;
                    break;
                case AxisType.Y:
                    position.SequenceY = value;
                    break;
                case AxisType.Z:
                    position.SequenceZ = value;
                    break;
            }
        }

        private static void IncreaseFacings(PlanogramPosition position, AxisType axis)
        {
            switch (axis)
            {
                case AxisType.X:
                    position.FacingsWide++;
                    break;
                case AxisType.Y:
                    position.FacingsHigh++;
                    break;
                case AxisType.Z:
                    position.FacingsDeep++;
                    break;
            }
        }

        private static void SetMaxStack(PlanogramPosition position, AxisType axis, Byte value = 0)
        {
            PlanogramProduct product = position.GetPlanogramProduct();
            switch (axis)
            {
                case AxisType.X:
                    product.MaxRightCap = value;
                    position.FacingsXWide = (Int16) (value + 1);
                    position.FacingsXDeep = 1;
                    position.FacingsXHigh = 1;
                    break;
                case AxisType.Y:
                    product.MaxStack = value;
                    position.FacingsWide = 1;
                    position.FacingsDeep = 1;
                    position.FacingsHigh = (Int16) (value + 1);
                    break;
                case AxisType.Z:
                    product.MaxDeep = (Byte) (value + 1);
                    position.FacingsWide = 2;
                    position.FacingsDeep = 1;
                    position.FacingsHigh = 1;
                    break;
            }
        }

        private static Single GetAvailableSize(PlanogramFixtureComponent component, PlanogramPosition position, AxisType axis)
        {
            PlanogramSubComponentPlacement subComponentPlacement = component.GetPlanogramSubComponentPlacements().First();
            switch (axis)
            {
                case AxisType.X:
                case AxisType.Y:
                    PlanogramSubComponentMerchandisingType merchandisingType = subComponentPlacement.SubComponent.MerchandisingType;
                    if ((merchandisingType == PlanogramSubComponentMerchandisingType.Hang ||
                         merchandisingType == PlanogramSubComponentMerchandisingType.HangFromBottom))
                    {
                        return subComponentPlacement.GetPlanogramRelativeBoundingBox().GetSize(axis)*0.5F;
                    }
                    break;
                case AxisType.Z:
                    Single pegDepth = position.GetPlanogramProduct().PegDepth;
                    if (pegDepth.GreaterThan(0)) return pegDepth;
                    break;
            }
            return subComponentPlacement.GetWorldMerchandisingSpaceBounds(true, component.Parent.Parent.GetPlanogramSubComponentPlacements()).GetSize(axis);
        }

        #endregion

        #region CorrectPositionSequencing

        [Test]
        public void CorrectPositionSequencing_StackedMultiY_YStaysInColumn()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var position1 = shelf.AddPosition(bay, plan.AddProduct());
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 1; position1.SequenceY = 1; position1.SequenceZ = 1;
            position2.SequenceX = 2; position2.SequenceY = 1; position2.SequenceZ = 1;
            position3.SequenceX = 2; position3.SequenceY = 2; position3.SequenceZ = 1;
            position4.SequenceX = 3; position4.SequenceY = 1; position4.SequenceZ = 1;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 2, 1, 1);
            AssertSequences(position3, 2, 2, 1);
            AssertSequences(position4, 3, 1, 1);
        }

        [Test]
        public void CorrectPositionSequencing_StackedMultiY_ResetsXTo1()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var position1 = shelf.AddPosition(bay, plan.AddProduct());
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 2; position1.SequenceY = 1; position1.SequenceZ = 1;
            position2.SequenceX = 3; position2.SequenceY = 1; position2.SequenceZ = 1;
            position3.SequenceX = 3; position3.SequenceY = 2; position3.SequenceZ = 1;
            position4.SequenceX = 4; position4.SequenceY = 1; position4.SequenceZ = 1;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 2, 1, 1);
            AssertSequences(position3, 2, 2, 1);
            AssertSequences(position4, 3, 1, 1);
        }

        [Test]
        public void CorrectPositionSequencing_StackedMultiY_ResetsYTo1()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.BottomStacked;
            var position1 = shelf.AddPosition(bay, plan.AddProduct());
            var position2 = shelf.AddPosition(bay, plan.AddProduct());
            var position3 = shelf.AddPosition(bay, plan.AddProduct());
            var position4 = shelf.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 2; position1.SequenceY = 2; position1.SequenceZ = 1;
            position2.SequenceX = 3; position2.SequenceY = 2; position2.SequenceZ = 1;
            position3.SequenceX = 3; position3.SequenceY = 3; position3.SequenceZ = 1;
            position4.SequenceX = 4; position4.SequenceY = 2; position4.SequenceZ = 1;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 2, 1, 1);
            AssertSequences(position3, 2, 2, 1);
            AssertSequences(position4, 3, 1, 1);
        }

        [Test]
        public void CorrectPositionSequencing_HangMultiX_XStaysInRow()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            var position1 = peg.AddPosition(bay, plan.AddProduct());
            var position2 = peg.AddPosition(bay, plan.AddProduct());
            var position3 = peg.AddPosition(bay, plan.AddProduct());
            var position4 = peg.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 1; position1.SequenceY = 1; position1.SequenceZ = 1;
            position2.SequenceX = 1; position2.SequenceY = 2; position2.SequenceZ = 1;
            position3.SequenceX = 2; position3.SequenceY = 2; position3.SequenceZ = 1;
            position4.SequenceX = 1; position4.SequenceY = 3; position4.SequenceZ = 1;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 1, 2, 1);
            AssertSequences(position3, 2, 2, 1);
            AssertSequences(position4, 1, 3, 1);
        }

        [Test]
        public void CorrectPositionSequencing_HangMultiX_ResetsYTo1()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            var position1 = peg.AddPosition(bay, plan.AddProduct());
            var position2 = peg.AddPosition(bay, plan.AddProduct());
            var position3 = peg.AddPosition(bay, plan.AddProduct());
            var position4 = peg.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 1; position1.SequenceY = 2; position1.SequenceZ = 1;
            position2.SequenceX = 1; position2.SequenceY = 3; position2.SequenceZ = 1;
            position3.SequenceX = 2; position3.SequenceY = 3; position3.SequenceZ = 1;
            position4.SequenceX = 1; position4.SequenceY = 4; position4.SequenceZ = 1;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 1, 2, 1);
            AssertSequences(position3, 2, 2, 1);
            AssertSequences(position4, 1, 3, 1);
        }

        [Test]
        public void CorrectPositionSequencing_HangMultiX_ResetsXTo1()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            var position1 = peg.AddPosition(bay, plan.AddProduct());
            var position2 = peg.AddPosition(bay, plan.AddProduct());
            var position3 = peg.AddPosition(bay, plan.AddProduct());
            var position4 = peg.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 2; position1.SequenceY = 2; position1.SequenceZ = 1;
            position2.SequenceX = 2; position2.SequenceY = 3; position2.SequenceZ = 1;
            position3.SequenceX = 3; position3.SequenceY = 3; position3.SequenceZ = 1;
            position4.SequenceX = 2; position4.SequenceY = 4; position4.SequenceZ = 1;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 1, 2, 1);
            AssertSequences(position3, 2, 2, 1);
            AssertSequences(position4, 1, 3, 1);
        }

        [Test]
        public void CorrectPositionSequencing_StackedMultiZ_ZStaysInRow()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var position1 = chest.AddPosition(bay, plan.AddProduct());
            var position2 = chest.AddPosition(bay, plan.AddProduct());
            var position3 = chest.AddPosition(bay, plan.AddProduct());
            var position4 = chest.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 1; position1.SequenceY = 1; position1.SequenceZ = 1;
            position2.SequenceX = 2; position2.SequenceY = 1; position2.SequenceZ = 1;
            position3.SequenceX = 2; position3.SequenceY = 1; position3.SequenceZ = 2;
            position4.SequenceX = 3; position4.SequenceY = 1; position4.SequenceZ = 1;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 2, 1, 1);
            AssertSequences(position3, 2, 1, 2);
            AssertSequences(position4, 3, 1, 1);
        }

        [Test]
        public void CorrectPositionSequencing_StackedMultiZ_ResetsZTo1()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var position1 = chest.AddPosition(bay, plan.AddProduct());
            var position2 = chest.AddPosition(bay, plan.AddProduct());
            var position3 = chest.AddPosition(bay, plan.AddProduct());
            var position4 = chest.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 1; position1.SequenceY = 1; position1.SequenceZ = 2;
            position2.SequenceX = 2; position2.SequenceY = 1; position2.SequenceZ = 2;
            position3.SequenceX = 2; position3.SequenceY = 1; position3.SequenceZ = 3;
            position4.SequenceX = 3; position4.SequenceY = 1; position4.SequenceZ = 2;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 2, 1, 1);
            AssertSequences(position3, 2, 1, 2);
            AssertSequences(position4, 3, 1, 1);
        }

        [Test]
        public void CorrectPositionSequencing_StackedMultiZ_ResetsXTo1()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var position1 = chest.AddPosition(bay, plan.AddProduct());
            var position2 = chest.AddPosition(bay, plan.AddProduct());
            var position3 = chest.AddPosition(bay, plan.AddProduct());
            var position4 = chest.AddPosition(bay, plan.AddProduct());
            position1.SequenceX = 2; position1.SequenceY = 1; position1.SequenceZ = 2;
            position2.SequenceX = 3; position2.SequenceY = 1; position2.SequenceZ = 2;
            position3.SequenceX = 3; position3.SequenceY = 1; position3.SequenceZ = 3;
            position4.SequenceX = 4; position4.SequenceY = 1; position4.SequenceZ = 2;

            // CorrectPositionSequence is called during BeginEdit.
            using (var mgs = plan.GetMerchandisingGroups()) { mgs.ForEach(mg => { mg.Process(); mg.ApplyEdit(); }); }

            AssertSequences(position1, 1, 1, 1);
            AssertSequences(position2, 2, 1, 1);
            AssertSequences(position3, 2, 1, 2);
            AssertSequences(position4, 3, 1, 1);
        }

        private void AssertSequences(PlanogramPosition position, Int16 seqX, Int16 seqY, Int16 seqZ)
        {
            Debug.WriteLine(
                "Product {0}, expected sequences [{1}, {2}, {3}], actual sequences [{4}, {5}, {6}]",
                position.GetPlanogramProduct().Gtin,
                seqX, seqY, seqZ, position.SequenceX, position.SequenceY, position.SequenceZ);
            Assert.AreEqual(seqX, position.SequenceX);
            Assert.AreEqual(seqY, position.SequenceY);
            Assert.AreEqual(seqZ, position.SequenceZ);
        }

        #endregion

        #region IsOverfilled

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentEmptyShouldNeverSayOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            String expectation = $"An empty {type} should never say it is overfilled.";
            PlanogramFixtureItem testFixture = "Test".CreatePackage().AddPlanogram().AddFixtureItem();
            PlanogramFixtureComponent testComponent = testFixture.AddFixtureComponent(type);
            CenterComponent(testFixture, testComponent);
            SetMerchStrategy(testComponent, axis);
            PlanogramMerchandisingGroup testModel = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(testComponent.GetPlanogramSubComponentPlacements());

            Boolean condition = testModel.IsOverfilled(axis);

            Assert.IsFalse(condition, expectation);
        }

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentWithExcessiveFacingsShouldSayOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            String expectation = $"A {type} with excessive facings should say it is overfilled.";
            Planogram testPlanogram = "Test".CreatePackage().AddPlanogram();
            PlanogramFixtureItem testFixture = testPlanogram.AddFixtureItem();
            PlanogramFixtureComponent testComponent = testFixture.AddFixtureComponent(type);
            CenterComponent(testFixture, testComponent);
            SetMerchStrategy(testComponent, axis);
            PlanogramPosition testPosition = testComponent.AddPosition(testFixture);
            SetSequence(testPosition, axis, 1);
            Single availableSize = GetAvailableSize(testComponent, testPosition, axis);
            // NB take double the available space.
            availableSize *= 2;
            Single sizeTaken = 0;
            while (availableSize.GreaterOrEqualThan(sizeTaken))
            {
                IncreaseFacings(testPosition, axis);
                //  NB Facings do not add up the size taken, they ARE the size taken.
                sizeTaken = testPosition.GetPositionDetails().TotalSize.GetSize(axis);
            }
            //  Assert the test is still valid.
            if (sizeTaken <= availableSize) Assert.Inconclusive("The group did not get overfilled while prepping the test.");
            testFixture.Parent.ReprocessAllMerchandisingGroups();

            PlanogramMerchandisingGroup testModel = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(testComponent.GetPlanogramSubComponentPlacements());

            Boolean condition = testModel.IsOverfilled(axis);

            Assert.IsTrue(condition, expectation);
        }

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentWithExcessivePositionsShouldSayOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            String expectation = $"A {type} with excessive positions should say it is overfilled.";
            Planogram testPlanogram = "Test".CreatePackage().AddPlanogram();
            PlanogramFixtureItem testFixture = testPlanogram.AddFixtureItem();
            PlanogramFixtureComponent testComponent = testFixture.AddFixtureComponent(type);
            CenterComponent(testFixture, testComponent);
            SetMerchStrategy(testComponent, axis);
            PlanogramPosition testPosition = testComponent.AddPosition(testFixture);
            SetSequence(testPosition, axis, 1);
            Single availableSize = GetAvailableSize(testComponent, testPosition, axis);
            // NB take double the available space.
            Single sizeTaken = -availableSize;
            while (availableSize.GreaterOrEqualThan(sizeTaken))
            {
                var nextSequence = (Int16)(testPosition.GetSequence(axis) + 1);
                testPosition = testComponent.AddPosition(testFixture);
                SetSequence(testPosition, axis, nextSequence);
                sizeTaken += testPosition.GetPositionDetails().TotalSize.GetSize(axis);
            }
            //  Assert the test is still valid.
            if (sizeTaken <= availableSize) Assert.Inconclusive("The group did not get overfilled while prepping the test.");
            testFixture.Parent.ReprocessAllMerchandisingGroups();

            PlanogramMerchandisingGroup testModel = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(testComponent.GetPlanogramSubComponentPlacements());

            Boolean condition = testModel.IsOverfilled(axis);

            Assert.IsTrue(condition, expectation);
        }

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentWithMaxPositionsShouldNeverSayOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            String expectation = $"An {type} with maximum positions should never say it is overfilled.";
            Planogram testPlanogram = "Test".CreatePackage().AddPlanogram();
            PlanogramFixtureItem testFixture = testPlanogram.AddFixtureItem();
            PlanogramFixtureComponent testComponent = testFixture.AddFixtureComponent(type);
            //  Exceptions... These should actually say overfilled.
            if (axis == AxisType.X && (type == PlanogramComponentType.ClipStrip || type == PlanogramComponentType.Rod)) Assert.Inconclusive("Clip strips and Rods cannot have more than one product on the X direction.");
            if (axis == AxisType.Y && (type == PlanogramComponentType.Rod || type == PlanogramComponentType.Bar)) Assert.Inconclusive("Rods and Bars can only have one product on the Y direction.");
            CenterComponent(testFixture, testComponent);
            SetMerchStrategy(testComponent, axis);
            PlanogramPosition testPosition = testComponent.AddPosition(testFixture);
            SetSequence(testPosition, axis, 1);
            Single positonSize = testPosition.GetPositionDetails().TotalSize.GetSize(axis);
            Single availableSize = GetAvailableSize(testComponent, testPosition, axis);
            // NB make space for an extra position.
            Single sizeTaken = 2*positonSize;
            while (availableSize.GreaterThan(sizeTaken))
            {
                var nextSequence = (Int16) (testPosition.GetSequence(axis) + 1);
                testPosition = testComponent.AddPosition(testFixture);
                SetSequence(testPosition, axis, nextSequence);
                Single taken = testPosition.GetPositionDetails().TotalSize.GetSize(axis);
                sizeTaken += taken;
            }
            //  Assert the test is still valid.
            if (sizeTaken - positonSize > availableSize) Assert.Inconclusive("The group got overfilled while prepping the test.");
            testFixture.Parent.ReprocessAllMerchandisingGroups();

            PlanogramMerchandisingGroup testModel = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(testComponent.GetPlanogramSubComponentPlacements());

            Boolean condition = testModel.IsOverfilled(axis);

            Assert.IsFalse(condition, expectation);
        }

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentWithMaxFacingsShouldNeverSayOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            String expectation = $"A {type} with maximum facings of one position should never say it is overfilled.";
            Planogram testPlanogram = "Test".CreatePackage().AddPlanogram();
            PlanogramFixtureItem testFixture = testPlanogram.AddFixtureItem();
            PlanogramFixtureComponent testComponent = testFixture.AddFixtureComponent(type);
            //  Exceptions... These should actually say overfilled.
            if (axis == AxisType.X && (type == PlanogramComponentType.ClipStrip || type == PlanogramComponentType.Rod)) Assert.Inconclusive("Clip strips and Rods cannot have more than one product on the X direction.");
            if (axis == AxisType.Y && (type == PlanogramComponentType.Rod || type == PlanogramComponentType.Bar)) Assert.Inconclusive("Rods and Bars can only have one product on the Y direction.");
            CenterComponent(testFixture, testComponent);
            SetMerchStrategy(testComponent, axis);
            PlanogramPosition testPosition = testComponent.AddPosition(testFixture);
            SetSequence(testPosition, axis, 1);
            Single positonSize = testPosition.GetPositionDetails().TotalSize.GetSize(axis);
            Single availableSize = GetAvailableSize(testComponent, testPosition, axis);
            // NB make space for an extra position.
            Single sizeTaken = 2 * positonSize;
            while (availableSize.GreaterThan(sizeTaken))
            {
                IncreaseFacings(testPosition, axis);
                //  NB Facings do not add up the size taken, they ARE the size taken.
                sizeTaken = testPosition.GetPositionDetails().TotalSize.GetSize(axis) + 2*positonSize;
            }
            //  Assert the test is still valid.
            if (sizeTaken - positonSize > availableSize) Assert.Inconclusive("The group got overfilled while prepping the test.");
            testFixture.Parent.ReprocessAllMerchandisingGroups();

            PlanogramMerchandisingGroup testModel = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(testComponent.GetPlanogramSubComponentPlacements());

            Boolean condition = testModel.IsOverfilled(axis);

            Assert.IsFalse(condition, expectation);
        }

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentWithMinPositionsShouldNeverSayOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            String expectation = $"A {type} with minimal positions should never say it is overfilled.";
            PlanogramFixtureItem testFixture = "Test".CreatePackage().AddPlanogram().AddFixtureItem();
            PlanogramFixtureComponent testComponent = testFixture.AddFixtureComponent(type);
            CenterComponent(testFixture, testComponent);
            SetMerchStrategy(testComponent, axis);
            testComponent.AddPosition(testFixture);
            testFixture.Parent.ReprocessAllMerchandisingGroups();

            PlanogramMerchandisingGroup testModel = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(testComponent.GetPlanogramSubComponentPlacements());

            Boolean condition = testModel.IsOverfilled(axis);

            Assert.IsFalse(condition, expectation);
        }

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentWithOverstackedPositionShouldSayOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            Assert.Inconclusive("Not sure if this should be here at all, or what exactly is being tested.");
            String expectation = $"A {type} with overstacked position should say it is overfilled.";
            PlanogramFixtureItem testFixture = "Test".CreatePackage().AddPlanogram().AddFixtureItem();
            PlanogramFixtureComponent testComponent = testFixture.AddFixtureComponent(type);
            CenterComponent(testFixture, testComponent);
            SetMerchStrategy(testComponent, axis);
            PlanogramPosition testPosition = testComponent.AddPosition(testFixture);
            SetSequence(testPosition, axis, 1);
            SetMaxStack(testPosition, axis);
            Single positonSize = testPosition.GetPositionDetails().TotalSize.GetSize(axis);
            Single availableSize = GetAvailableSize(testComponent, testPosition, axis);
            // NB make space for an extra position.
            Single sizeTaken = 2 * positonSize;
            while (availableSize.GreaterThan(sizeTaken))
            {
                var nextSequence = (Int16)(testPosition.GetSequence(axis) + 1);
                testPosition = testComponent.AddPosition(testFixture);
                SetSequence(testPosition, axis, nextSequence);
                SetMaxStack(testPosition, axis);
                sizeTaken += testPosition.GetPositionDetails().TotalSize.GetSize(axis);
            }
            //  Assert the test is still valid.
            if (sizeTaken - positonSize > availableSize) Assert.Inconclusive("The group got overfilled while prepping the test.");
            testFixture.Parent.ReprocessAllMerchandisingGroups();

            PlanogramMerchandisingGroup testModel = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(testComponent.GetPlanogramSubComponentPlacements());

            Boolean condition = testModel.IsOverfilled(axis);

            Assert.IsTrue(condition, expectation);
        }

        [Test, Category(MerchandisingGroupIsOverfilledCheck)]
        public void ComponentWithEvenStrategyAndReducedSpaceShouldNotBeOverfilled(
            [ValueSource(nameof(_allAxisTypes))] AxisType axis,
            [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            if (axis == AxisType.X && (type == PlanogramComponentType.Rod || type == PlanogramComponentType.ClipStrip))
            {
                Assert.Ignore("Even strategy cannot be applied in X for Rods or Clip Strips.");
            }
            if(axis == AxisType.Y && (type == PlanogramComponentType.Rod || type == PlanogramComponentType.Bar))
            {
                Assert.Ignore("Even strategy cannot be applied in Y for Rods or Bars.");
            }
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var component = bay.AddFixtureComponent(type);
            switch (axis)
            {
                case AxisType.X:
                    component.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
                    break;
                case AxisType.Y:
                    component.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
                    break;
                case AxisType.Z:
                    component.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
                    break;
                default:
                    break;
            }
            component.AddPosition(bay);
            component.AddPosition(bay);
            plan.ReprocessAllMerchandisingGroups();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                var mgSpace = mg.GetMerchandisingSpace();
                var constraint = mgSpace.UnhinderedSpace;
                constraint.SetSize(axis, constraint.GetSize(axis) / 2f);
                var space = mgSpace.ReduceToFit(constraint);

                Boolean isOverfilled = mg.IsOverfilled(axis, space);

                Assert.IsFalse(isOverfilled, "Component should not be overfilled with reduced space, because there is still space for positions.");
            }
        }

        #endregion

        #region Autofill

        [Test]
        public void Autofill_ShouldNotFillWideWhenManualMerchandisingStrategyX()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsWide = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsWide, "Shouldn't autofill when the subcomponent has a manual wide strategy.");
            }

            subCompPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsWide, "Should now have processed an autofill wide.");
            }
        }

        [Test]
        public void Autofill_ShouldNotFillHighWhenManualMerchandisingStrategyY()
        {
            throw new InconclusiveException("Autofill only works in facing axis atm therefore Y cannot yet be tested. Once autofill does all axis then this line of code can be removed and the test shall be valid");

            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsHigh = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsHigh, "Shouldn't autofill when the subcomponent has a manual high strategy.");
            }

            subCompPlacement.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsHigh, "Should now have processed an autofill high.");
            }
        }

        [Test]
        public void Autofill_ShouldNotFillDeepWhenManualProductPlacementZ()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;
            subCompPlacement.SubComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsDeep = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsDeep, "Shouldn't autofill when the subcomponent has a manual deep strategy.");
            }

            subCompPlacement.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsDeep, "Should now have processed an autofill deep.");
            }
        }

        [Test]
        public void Autofill_ShouldNotFillWideWhenManualPlanogramProductPlacementWide()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsWide = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsWide, "Shouldn't autofill when the planogram has a manual subcomponent placement type.");
            }

            planogram.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsWide, "Should now have processed an autofill wide.");
            }
        }

        [Test]
        public void Autofill_ShouldNotFillHighWhenManualPlanogramProductPlacementHigh()
        {
            throw new InconclusiveException("Autofill only works in facing axis atm therefore Y cannot yet be tested. Once autofill does all axis then this line of code can be removed and the test shall be valid");

            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsHigh = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsHigh, "Shouldn't autofill when the subcomponent has a manual high strategy.");
            }

            planogram.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsHigh, "Should now have processed an autofill high.");
            }
        }

        [Test]
        public void Autofill_ShouldNotFillDeepWhenManualPlanogramProductPlacementDeep()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementZ = PlanogramProductPlacementZType.Manual;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;
            subCompPlacement.SubComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsDeep = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsDeep, "Shouldn't autofill when the subcomponent has a manual deep strategy.");
            }

            planogram.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsDeep, "Should now have processed an autofill deep.");
            }
        }

        [Test]
        public void Autofill_ShouldNotAutoFillWideOnManuallyPlacedProducts()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsWide = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            position.IsManuallyPlaced = true;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsWide, "Shouldn't autofill when the position is manually placed.");
            }

            position.IsManuallyPlaced = false;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsWide, "Should now have processed an autofill wide.");
            }
        }

        [Test]
        public void Autofill_ShouldNotAutoFillHighOnManuallyPlacedProducts()
        {
            throw new InconclusiveException("Autofill only works in facing axis atm therefore Y cannot yet be tested. Once autofill does all axis then this line of code can be removed and the test shall be valid");

            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsHigh = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
            position.IsManuallyPlaced = true;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsHigh, "Shouldn't autofill when the position is manuallyPlaced.");
            }

            position.IsManuallyPlaced = false;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsHigh, "Should now have processed an autofill high.");
            }
        }

        [Test]
        public void Autofill_ShouldNotAutoFillDeepOnManuallyPlacedProducts()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementZ = PlanogramProductPlacementZType.FillDeep;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 100;
            subCompPlacement.SubComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsDeep = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
            position.IsManuallyPlaced = true;

            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreEqual(1, position.FacingsDeep, "Shouldn't autofill when the subcomponent has a manual deep strategy.");
            }

            position.IsManuallyPlaced = false;
            //Need to call autofill on the merch group
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsDeep, "Should now have processed an autofill deep.");
            }
        }

        [Test]
        public void Autofill_ShouldNotOverflow()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 10;
            subCompPlacement.SubComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsWide = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            position.IsManuallyPlaced = false;

            //Need to call autofill on the merch group
            Assert.AreEqual(1, position.FacingsDeep, "Shouldn't autofill when the subcomponent has a manual deep strategy.");
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsWide, "Needs to have successfully autofilled here for a valid test to continue.");
                Assert.IsFalse(merchandisingGroup.IsOverfilled(AxisType.X), "The merch group overfilled x when it shoudln't");
                Assert.IsFalse(merchandisingGroup.IsOverfilled(AxisType.Y), "The merch group overfilled Y when it shoudln't");
                Assert.IsFalse(merchandisingGroup.IsOverfilled(AxisType.Z), "The merch group overfilled Z when it shoudln't");
            }
        }

        [Test]
        public void Autofill_RemainingMerchSpaceMustBeLessThanSingleProductWide()
        {
            Planogram planogram = CreatePlanogram();
            planogram.ProductPlacementX = PlanogramProductPlacementXType.FillWide;
            PlanogramProduct product = CreateProduct(planogram);
            product.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;

            var subCompPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(planogram.Components.First().SubComponents.First(), planogram.FixtureItems.First(), planogram.Fixtures.First().Components.First());

            subCompPlacement.SubComponent.Width = 100;
            subCompPlacement.SubComponent.Height = 10;
            subCompPlacement.SubComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;

            PlanogramPosition position = CreatePosition(planogram, product, subCompPlacement, 1, 1, 1);
            position.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray;
            position.FacingsWide = 1;
            subCompPlacement.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            position.IsManuallyPlaced = false;

            //Need to call autofill on the merch group
            Assert.AreEqual(1, position.FacingsDeep, "Shouldn't autofill when the subcomponent has a manual deep strategy.");
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(subCompPlacement))
            {
                Autofill(merchandisingGroup);
                merchandisingGroup.ApplyEdit();
                Assert.AreNotEqual(1, position.FacingsWide, "Needs to have successfully autofilled here for a valid test to continue.");

                //Total merch space
                float totalAvailableSpaceWide = merchandisingGroup.SubComponentPlacements.Sum(p => p.SubComponent.Width);

                //Space used by a position
                PlanogramPositionDetails positionDetails = PlanogramPositionDetails.NewPlanogramPositionDetails(position, product, subCompPlacement);

                //Product dimensions
                Single singleProductWidth = product.Width;
                //Single singleProductHeight = product.Width;
                //Single singleProductDepth = product.Width;

                //Calculate the remaining space
                float remainingSpaceWide = (totalAvailableSpaceWide - positionDetails.TotalSize.Width);
                //float remainingSpaceHigh = (totalAvailableSpaceHigh - positionDetails.TotalSize.Height);
                //float remainingSpaceDeep = (totalAvailableSpaceDepth - positionDetails.TotalSize.Depth);

                Assert.IsTrue(remainingSpaceWide < singleProductWidth);

                //JPTODO: Once the autofill calls all three dimensions should be able to test high and deep too.
            }
        }

        private IEnumerable<Single> ProductSizes
        {
            get
            {
                Single single = 5f;
                while (single.LessOrEqualThan(6f))
                {
                    yield return single;
                    single += 0.01f;
                }
                yield break;
            }
        }

        [Test]
        public void Autofill_ProductsShouldAlwaysBePlacedAtTop_WhenTopStacked([ValueSource(nameof(ProductSizes))] Single productSize)
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.ProductPlacementY = PlanogramProductPlacementYType.FillHigh;
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg);
            var pegSub = peg.GetPlanogramComponent().SubComponents.First();
            pegSub.MerchConstraintRow1StartY = pegSub.MerchConstraintRow1SpacingY;
            pegSub.Height = 100;
            var product = plan.AddProduct(new WidthHeightDepthValue(productSize, productSize, productSize));
            product.PegX = productSize/2f;
            product.PegY = pegSub.MerchConstraintRow1StartY;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var pegMerchGroup = merchGroups.First();
                var positionPlacement = pegMerchGroup.InsertPositionPlacement(product);
                pegMerchGroup.Process();
                pegMerchGroup.ApplyEdit();
                AssertHelper.AreSinglesEqual(pegSub.Height, positionPlacement.Position.Y + positionPlacement.GetPositionDetails().TotalSpace.Height);

                Autofill(pegMerchGroup);

                AssertHelper.AreSinglesEqual(pegSub.Height, positionPlacement.Position.Y + positionPlacement.GetPositionDetails().TotalSpace.Height);
            }
        }

        [Test]
        public void HangingBarAutoFillWideUsesRightOverHangWhenLeftStacked()
        {
            const Single overhangForFullFacing = AnySize;
            Planogram planogram = nameof(HangingBarAutoFillWideUsesRightOverHangWhenLeftStacked).CreatePackage().AddPlanogram();
            planogram.AddFixtureItem();
            PlanogramFixtureComponent component = planogram
                .SetAutoFill(PlanogramProductPlacementXType.FillWide)
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Bar)
                .SetMerchStrategyType()
                .SetOverhang(right: overhangForFullFacing);
            component.AddPosition(planogram.AddProduct(new WidthHeightDepthValue(AnySize, AnySize, AnySize)));
            planogram.AddFixtureItem();
            
            ApplyAutofill(planogram);

            planogram.Positions.First().GetPositionDetails().TotalSize.Width
                     .Should().BeGreaterThan(component.GetPlanogramComponent().Width,
                                             because: "a left stacked bar with right over hang should use it when autofilled");
        }

        [Test]
        public void HangingBarAutoFillWideUsesLeftOverHangWhenRightStacked()
        {
            const Single overhangForFullFacing = AnySize;
            Planogram planogram = nameof(HangingBarAutoFillWideUsesLeftOverHangWhenRightStacked).CreatePackage().AddPlanogram();
            planogram.AddFixtureItem();
            planogram
                .SetAutoFill(PlanogramProductPlacementXType.FillWide)
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Bar)
                .SetMerchStrategyType(PlanogramSubComponentXMerchStrategyType.RightStacked)
                .SetOverhang(left: overhangForFullFacing)
                .AddPosition(planogram.AddProduct(new WidthHeightDepthValue(AnySize, AnySize, AnySize)));
            planogram.AddFixtureItem();

            ApplyAutofill(planogram);

            planogram.Positions.First().X
                     .Should().BeLessThan(0F,
                                          because: "a right stacked bar with left over hang should use it when autofilled");
        }

        #endregion

        #region HasOverlapping

        [Test]
        public void HasOverlapping_WhenDividersNoOverHang_ShouldNotReturnTrueWhenPositionsDontOverlap()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, size: new WidthHeightDepthValue(177.5f, 4.2f, 57));
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 18.2f;
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionHeight = 2;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionWidth = 0.28f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionSpacingX = 1.82f;
            var prod1 = plan.AddProduct(new WidthHeightDepthValue(18.5f, 16.2f, 2));
            var prod2 = plan.AddProduct(new WidthHeightDepthValue(19f, 16.1f, 2.2f));
            var prod3 = plan.AddProduct(new WidthHeightDepthValue(20f, 7.2f, 2.45f));
            prod3.MaxTopCap = 99;
            var prod4 = plan.AddProduct(new WidthHeightDepthValue(20f, 8.2f, 1.8f));
            var prod5 = plan.AddProduct(new WidthHeightDepthValue(19.2f, 2f, 11f));
            prod5.OrientationType = PlanogramProductOrientationType.Top0;
            var prod6 = plan.AddProduct(new WidthHeightDepthValue(15, 3f, 13f));
            prod6.OrientationType = PlanogramProductOrientationType.Top0;
            var assortmentRuleEnforcer = new AssortmentRuleEnforcer();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                var pos1 = mg.InsertPositionPlacement(prod1);
                pos1.SetUnits(28, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos2 = mg.InsertPositionPlacement(prod2, pos1, PlanogramPositionAnchorDirection.ToRight);
                pos2.SetUnits(50, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos3 = mg.InsertPositionPlacement(prod3, pos2, PlanogramPositionAnchorDirection.ToRight);
                pos3.SetUnits(53, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos4 = mg.InsertPositionPlacement(prod4, pos3, PlanogramPositionAnchorDirection.ToRight);
                pos4.SetUnits(62, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos5 = mg.InsertPositionPlacement(prod5, pos4, PlanogramPositionAnchorDirection.ToRight);
                pos5.SetUnits(28, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos6 = mg.InsertPositionPlacement(prod6, pos5, PlanogramPositionAnchorDirection.ToRight);
                pos6.SetUnits(57, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                mg.Process();
                mg.ApplyEdit();
                //plan.TakeSnapshot("HasOverlapping_WhenDividersNoOverHang_ShouldNotReturnTrueWhenPositionsDontOverlap");

                Boolean hasOverlapping = mg.HasOverlapping();

                hasOverlapping.Should().Be(false, "because the positions do not overlap on the shelf");
            }
        }

        [Test]
        public void HasOverlapping_WhenDividers_ShouldNotReturnTrueWhenPositionsDontOverlap()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, size: new WidthHeightDepthValue(180, 4.2f, 57));
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 18.2f;
            shelf.GetPlanogramComponent().SubComponents[0].LeftOverhang = -1.25f;
            shelf.GetPlanogramComponent().SubComponents[0].RightOverhang = -1.25f;
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionHeight = 2;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionWidth = 0.28f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionSpacingX = 1.82f;
            var prod1 = plan.AddProduct(new WidthHeightDepthValue(19, 5, 13));
            var prod2 = plan.AddProduct(new WidthHeightDepthValue(21.5f, 5.5f, 7));
            var prod3 = plan.AddProduct(new WidthHeightDepthValue(21.5f, 5.5f, 7));
            var prod4 = plan.AddProduct(new WidthHeightDepthValue(19.3f, 4.2f, 9.1f));
            var prod5 = plan.AddProduct(new WidthHeightDepthValue(19.3f, 4.2f, 9.1f));
            var prod6 = plan.AddProduct(new WidthHeightDepthValue(23, 15.8f, 4.6f));
            var assortmentRuleEnforcer = new AssortmentRuleEnforcer();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                var pos1 = mg.InsertPositionPlacement(prod1);
                pos1.SetUnits(24, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos2 = mg.InsertPositionPlacement(prod2, pos1, PlanogramPositionAnchorDirection.ToRight);
                pos2.SetUnits(24, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos3 = mg.InsertPositionPlacement(prod3, pos2, PlanogramPositionAnchorDirection.ToRight);
                pos3.SetUnits(24, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos4 = mg.InsertPositionPlacement(prod4, pos3, PlanogramPositionAnchorDirection.ToRight);
                pos4.SetUnits(26, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos5 = mg.InsertPositionPlacement(prod5, pos4, PlanogramPositionAnchorDirection.ToRight);
                pos5.SetUnits(26, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                var pos6 = mg.InsertPositionPlacement(prod6, pos5, PlanogramPositionAnchorDirection.ToRight);
                pos6.SetUnits(24, PlanogramPositionInventoryTargetType.ClosestOrEqualTo, assortmentRuleEnforcer);
                mg.Process();
                mg.ApplyEdit();
                //plan.TakeSnapshot("HasOverlapping_WhenDividers_ShouldNotReturnTrueWhenPositionsDontOverlap");

                Boolean hasOverlapping = mg.HasOverlapping();

                hasOverlapping.Should().Be(false, "because the positions do not overlap on the shelf");
            }
        }

        [Test]
        public void HasOverlapping_WhenNoPlacements_ShouldReturnFalse()
        {
            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixtureItem bayA = planogram.AddFixtureItem();
            PlanogramFixtureComponent shelfA = bayA.AddFixtureComponent(PlanogramComponentType.Shelf);
            Boolean hasOverlapping;

            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelfA.GetPlanogramSubComponentPlacements()))
            {
                hasOverlapping = merchandisingGroup.HasOverlapping();
            }

            Assert.IsFalse(hasOverlapping);
        }

        [Test]
        public void HasOverlapping_WhenPlacementsDoNotOverlap_ShouldReturnFalse()
        {
            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixtureItem bayA = planogram.AddFixtureItem();
            PlanogramFixtureComponent shelfA = bayA.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramPosition position = shelfA.AddPosition(bayA);
            position.SequenceX = 1;
            PlanogramPosition planogramPosition = shelfA.AddPosition(bayA);
            planogramPosition.SequenceX = 2;
            Boolean hasOverlapping;
            using (var mg = planogram.GetMerchandisingGroups())
            {
                mg.ForEach(g =>
                           {
                               g.Process();
                               g.ApplyEdit();
                           });
            }

            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelfA.GetPlanogramSubComponentPlacements()))
            {
                hasOverlapping = merchandisingGroup.HasOverlapping();
            }

            Assert.IsFalse(hasOverlapping);
        }

        [Test]
        public void HasOverlapping_WhenPlacementsDoOverlap_ShouldReturnTrue([ValueSource(nameof(_allAxisTypes))] AxisType axis, [ValueSource(nameof(_allMerchandisableComponentTypesExceptCustom))] PlanogramComponentType type)
        {
            Planogram planogram = Planogram.NewPlanogram();
            PlanogramFixtureItem fixtureA = planogram.AddFixtureItem();
            PlanogramFixture fixture = fixtureA.GetPlanogramFixture();
            Single fixtureCenterX = fixtureA.X + fixture.Width/2;
            Single fixtureCenterY = fixtureA.Y + fixture.Height/2;
            PlanogramFixtureComponent componentA = fixtureA.AddFixtureComponent(type);
            PlanogramComponent component = componentA.GetPlanogramComponent();
            componentA.X = fixtureCenterX - component.Width/2;
            componentA.Y = fixtureCenterY - component.Height/2;
            Single nextCoordinate = 0;
            Boolean componentHasSpace;
            do
            {
                PlanogramPosition position = componentA.AddPosition(fixtureA);
                switch (axis)
                {
                    case AxisType.X:
                        position.X = nextCoordinate;
                        nextCoordinate += position.GetPlanogramProduct().Width/2;
                        componentHasSpace = fixture.Width > nextCoordinate;
                        break;
                    case AxisType.Y:
                        position.Y = nextCoordinate;
                        nextCoordinate += position.GetPlanogramProduct().Height/2;
                        componentHasSpace = fixture.Height > nextCoordinate;
                        break;
                    case AxisType.Z:
                        position.Z = nextCoordinate;
                        nextCoordinate += position.GetPlanogramProduct().Depth/2;
                        componentHasSpace = fixture.Depth > nextCoordinate;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(axis));
                }
            } while (componentHasSpace);

            Boolean hasOverlapping;
            using (PlanogramMerchandisingGroup merchandisingGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(componentA.GetPlanogramSubComponentPlacements()))
            {
                hasOverlapping = merchandisingGroup.HasOverlapping();
            }

            Assert.IsTrue(hasOverlapping);
        }

        #endregion

        #region Static Helper Methods

        private static Planogram CreatePlanogram(Boolean chest = false)
        {
            Planogram planogram = Planogram.NewPlanogram();
            PlanogramComponent component = chest ? PlanogramComponent.NewPlanogramComponent("Chest", PlanogramComponentType.Chest, 120, 4, 75) : PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, 120, 4, 75);
            planogram.Components.Add(component);
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 200;
            fixture.Depth = 75;
            fixture.Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(component));
            planogram.Fixtures.Add(fixture);
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture));

            return planogram;
        }

        private static PlanogramProduct CreateProduct(Planogram planogram, Single height = 10, Single width = 5, Single depth = 2)
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Height = height;
            product.Width = width;
            product.Depth = depth;
            planogram.Products.Add(product);
            return product;
        }

        private static PlanogramPosition CreatePosition(Planogram planogram, PlanogramProduct product, PlanogramSubComponentPlacement subCompPlacement, Byte wide, Byte high, Byte deep, Byte xWide = 0, Byte xHigh = 0, Byte xDeep = 0, Byte yWide = 0, Byte yHigh = 0, Byte yDeep = 0, Byte zWide = 0, Byte zHigh = 0, Byte zDeep = 0)
        {
            PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement);
            position.FacingsWide = wide;
            position.FacingsXWide = xWide;
            position.FacingsZWide = zWide;
            position.FacingsYWide = yWide;
            position.FacingsHigh = high;
            position.FacingsXHigh = xHigh;
            position.FacingsZHigh = zHigh;
            position.FacingsYHigh = yHigh;
            position.FacingsDeep = deep;
            position.FacingsXDeep = xDeep;
            position.FacingsZDeep = zDeep;
            position.FacingsYDeep = yDeep;
            planogram.Positions.Add(position);
            return position;
        }

        private static void ApplyAutofill(Planogram planogram)
        {
            using (PlanogramMerchandisingGroupList planogramMerchandisingGroupList = planogram.GetMerchandisingGroups())
            {
                PlanogramMerchandisingGroup merchGroup = planogramMerchandisingGroupList.First();
                Boolean shouldFillX = planogram.ProductPlacementX == PlanogramProductPlacementXType.FillWide &&
                                      merchGroup.StrategyX != PlanogramSubComponentXMerchStrategyType.Manual;
                Boolean shouldFillY = planogram.ProductPlacementY == PlanogramProductPlacementYType.FillHigh &&
                                      merchGroup.StrategyY != PlanogramSubComponentYMerchStrategyType.Manual;
                Boolean shouldFillZ = planogram.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep &&
                                      merchGroup.StrategyZ != PlanogramSubComponentZMerchStrategyType.Manual;
                if (!shouldFillX &&
                    !shouldFillY &&
                    !shouldFillZ) return;

                merchGroup.Autofill(planogramMerchandisingGroupList);
                merchGroup.ApplyEdit();
            }
        }

        /// <summary>
        /// Autofills all positions on this merchandising group, giving priority to Rank and then Gtin.
        /// </summary>
        /// <returns>True when made a change and processed the merch group, otherwise false.</returns>
        private static void Autofill(PlanogramMerchandisingGroup planogramMerchandisingGroup)
        {
            Planogram plan = planogramMerchandisingGroup.SubComponentPlacements.First().Planogram;
            Boolean shouldFillX = plan.ProductPlacementX == PlanogramProductPlacementXType.FillWide &&
                                  planogramMerchandisingGroup.StrategyX != PlanogramSubComponentXMerchStrategyType.Manual;
            Boolean shouldFillY = plan.ProductPlacementY == PlanogramProductPlacementYType.FillHigh &&
                                  planogramMerchandisingGroup.StrategyY != PlanogramSubComponentYMerchStrategyType.Manual;
            Boolean shouldFillZ = plan.ProductPlacementZ == PlanogramProductPlacementZType.FillDeep &&
                                  planogramMerchandisingGroup.StrategyZ != PlanogramSubComponentZMerchStrategyType.Manual;
            if (!shouldFillX &&
                !shouldFillY &&
                !shouldFillZ) return;

            using (PlanogramMerchandisingGroupList merchandisingGroups = plan.GetMerchandisingGroups())
            {
                planogramMerchandisingGroup.Autofill(merchandisingGroups);
            }
        }

        #endregion

        #region Begin Cancel Apply

        [Test]
        public void BeginApplyEdit_InsertPosition()
        {
            // create a planogram
            Planogram plan = CreateShelfTestPlan();
            PlanogramSubComponentPlacement shelf = plan.GetPlanogramSubComponentPlacements().ElementAt(2);
            shelf.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            PlanogramProduct productOne = plan.Products[0];

            // clear all positions from the planogram
            plan.Positions.Clear();
            Assert.AreEqual(0, plan.Positions.Count);

            // add new positions to the merchandising group
            using (PlanogramMerchandisingGroup group = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelf))
            {
                // add the position placement
                group.InsertPositionPlacement(productOne);

                // assert that the position placement was added to the merchandising group
                Assert.AreEqual(1, group.PositionPlacements.Count);

                // assert that the position has not yet been added to the planogram
                Assert.AreEqual(0, plan.Positions.Count);

                // process and apply
                group.Process();
                group.ApplyEdit();

                // assert the position was added to the planogram
                Assert.AreEqual(1, plan.Positions.Count);

                // assert that the position is associated to the shelf
                Assert.AreEqual(1, shelf.GetPlanogramPositions().Count());

                // assert that the position is associated to the correct product
                Assert.AreEqual(productOne.Id, plan.Positions[0].PlanogramProductId);
            }
        }

        [Test]
        public void BeginApplyEdit_CancelInsertPosition()
        {
            // create a planogram
            Planogram plan = CreateShelfTestPlan();
            PlanogramSubComponentPlacement shelf = plan.GetPlanogramSubComponentPlacements().ElementAt(2);
            shelf.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            PlanogramProduct productOne = plan.Products[0];
            PlanogramProduct productTwo = plan.Products[2];

            // clear all positions from the planogram
            plan.Positions.Clear();
            Assert.AreEqual(0, plan.Positions.Count);

            // add new positions to the merchandising group
            using (PlanogramMerchandisingGroup group = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelf))
            {
                // add the position placement
                PlanogramPositionPlacement placementOne = group.InsertPositionPlacement(productOne);

                // assert that the position placement was added to the merchandising group
                Assert.AreEqual(1, group.PositionPlacements.Count);

                // assert that the position has not yet been added to the planogram
                Assert.AreEqual(0, plan.Positions.Count);

                //start another edit level
                group.BeginEdit();

                // insert another position to the left of the first
                PlanogramPositionPlacement placementTwo = group.InsertPositionPlacement(productTwo, placementOne, PlanogramPositionAnchorDirection.ToLeft);

                // assert that the position placement was added to the merchandising group
                Assert.AreEqual(2, group.PositionPlacements.Count);

                // assert that the position has not yet been added to the planogram
                Assert.AreEqual(0, plan.Positions.Count);

                // process the merchandising group
                group.Process();

                // assert that the sequence numbers are correct
                Assert.AreEqual(2, placementOne.Position.Sequence);
                Assert.AreEqual(1, placementTwo.Position.Sequence);

                // cancel the last edit
                group.CancelEdit();

                // assert that the position placement was removed to the merchandising group
                Assert.AreEqual(1, group.PositionPlacements.Count);

                // assert that the position has not yet been added to the planogram
                Assert.AreEqual(0, plan.Positions.Count);

                // reset all edits
                group.Reset();

                // apply all changes
                group.ApplyEdit();

                // assert that the position placement was removed to the merchandising group
                Assert.AreEqual(0, group.PositionPlacements.Count);

                // assert that the position has not yet been added to the planogram
                Assert.AreEqual(0, plan.Positions.Count);
            }
        }

        [Test]
        public void BeginApplyEdit_RemovePosition()
        {
            // create a planogram
            Planogram plan = CreateShelfTestPlan();
            PlanogramSubComponentPlacement shelf = plan.GetPlanogramSubComponentPlacements().ElementAt(2);
            shelf.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            Int32 positionCount = plan.Positions.Count;

            // remove positions from the merchandising group
            using (PlanogramMerchandisingGroup group = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelf))
            {
                // remove all positions from the merchandising group
                group.PositionPlacements.Clear();

                // assert the merchandising group is empty
                Assert.AreEqual(0, group.PositionPlacements.Count);

                // assert that positions still exist within the planogram
                Assert.AreEqual(positionCount, plan.Positions.Count);

                // process and apply
                group.Process();
                group.ApplyEdit();

                // assert the positions was removed from the planogram
                Assert.AreEqual(0, plan.Positions.Count);

                // assert that the positions were removed from the shelf
                Assert.AreEqual(0, shelf.GetPlanogramPositions().Count());
            }
        }

        [Test]
        public void BeginApplyEdit_CancelRemovePosition()
        {
            // create a planogram
            Planogram plan = CreateShelfTestPlan();
            PlanogramSubComponentPlacement shelf = plan.GetPlanogramSubComponentPlacements().ElementAt(2);
            shelf.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            shelf.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            Int32 positionCount = plan.Positions.Count;

            // modify the merchandising groups
            using (PlanogramMerchandisingGroup group = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelf))
            {
                // remove each position from the merchandising group
                Int32 currentPositionCount = positionCount;
                while (group.PositionPlacements.Count > 0)
                {
                    // begin an edit
                    group.BeginEdit();

                    // remove the position placement
                    group.RemovePositionPlacement(group.PositionPlacements[0]);

                    // reduce the current position count
                    currentPositionCount--;

                    // assert the position count has changed
                    Assert.AreEqual(currentPositionCount, group.PositionPlacements.Count);

                    // assert that the planogram position count has not changed
                    Assert.AreEqual(positionCount, plan.Positions.Count);
                }

                // reset the merchandising group
                group.Reset();

                // assert that the positions are back in the merchandising group
                Assert.AreEqual(positionCount, group.PositionPlacements.Count);

                // assert that the positions are still in the planogram
                Assert.AreEqual(positionCount, plan.Positions.Count);
            }
        }

        [Test]
        public void BeginApplyEdit_AdjustsCombinedShelfPositionValues()
        {
            //Setup data.
            Planogram plan = CreateCombinedShelfTestPlan();

            PlanogramSubComponentPlacement shelf1 = plan.GetPlanogramSubComponentPlacements().ElementAt(2);
            PlanogramSubComponentPlacement shelf2 = plan.GetPlanogramSubComponentPlacements().ElementAt(5);

            PlanogramPosition p1 = plan.Positions[0];
            PlanogramPosition p2 = plan.Positions[1];
            PlanogramPosition p3 = plan.Positions[2];
            PlanogramPosition p4 = plan.Positions[3];
            PlanogramPosition p5 = plan.Positions[4];

            shelf1.SubComponent.CombineType = PlanogramSubComponentCombineType.Both;
            shelf1.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf1.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf1.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;

            using (PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelf1.GetCombinedWithList()))
            {
                merchGroup.ApplyEdit();
            }
            //check the position sequence numbers
            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(4, p4.SequenceX);
            Assert.AreEqual(5, p5.SequenceX);


            shelf2.SubComponent.CombineType = PlanogramSubComponentCombineType.Both;
            shelf2.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            shelf2.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            shelf2.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;


            shelf2.AssociatePosition(p5);
            p5.Sequence = 1;
            p5.SequenceX = 1;


            //create the merch group
            using (PlanogramMerchandisingGroup merchGroup = PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(shelf1.GetCombinedWithList()))
            {
                Assert.AreEqual(2, merchGroup.SubComponentPlacements.Count(), "Should have both shelves as they are combined");

                //check the position sequence numbers
                Assert.AreEqual(1, merchGroup.PositionPlacements[0].Position.Sequence);
                Assert.AreEqual(2, merchGroup.PositionPlacements[1].Position.Sequence);
                Assert.AreEqual(3, merchGroup.PositionPlacements[2].Position.Sequence);
                Assert.AreEqual(4, merchGroup.PositionPlacements[3].Position.Sequence);
                Assert.AreEqual(5, merchGroup.PositionPlacements[4].Position.Sequence);

                Assert.AreEqual(1, merchGroup.PositionPlacements[0].Position.SequenceX);
                Assert.AreEqual(2, merchGroup.PositionPlacements[1].Position.SequenceX);
                Assert.AreEqual(3, merchGroup.PositionPlacements[2].Position.SequenceX);
                Assert.AreEqual(4, merchGroup.PositionPlacements[3].Position.SequenceX);
                Assert.AreEqual(5, merchGroup.PositionPlacements[4].Position.SequenceX);

                //process and apply
                merchGroup.Process();
                merchGroup.ApplyEdit();
            }

            //check sequence numbers
            Assert.AreEqual(1, p1.Sequence);
            Assert.AreEqual(2, p2.Sequence);
            Assert.AreEqual(3, p3.Sequence);
            Assert.AreEqual(1, p4.Sequence);
            Assert.AreEqual(2, p5.Sequence);

            Assert.AreEqual(1, p1.SequenceX);
            Assert.AreEqual(2, p2.SequenceX);
            Assert.AreEqual(3, p3.SequenceX);
            Assert.AreEqual(1, p4.SequenceX);
            Assert.AreEqual(2, p5.SequenceX);
        }

        #endregion

        #region InsertPositionPlacement

        [Test]
        public void InsertPositionPlacement_Chest_EvenAndStacked_ShouldPreserveSpacing()
        {
            const String expectedMessage = @"When a product is inserted in front of another on a chest that is merchandised Even in X and Back Stacked
                    in Z, products should remain spread Even in X and should not overlap the walls of the chest";

            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var chestSubComp = chest.GetPlanogramComponent().SubComponents.First();
            chestSubComp.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            chestSubComp.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;

            for (Int32 i = 0; i < 6; i++)
            {
                plan.AddProduct(new WidthHeightDepthValue(20, 20, 20));
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                // Insert the first 5 products.
                PlanogramPositionPlacement anchor = null;
                for (Int32 i = 0; i < 5; i++)
                {
                    PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(1, plan.Products[i]);
                    anchor = anchor == null
                                 ? merchGroup.InsertPositionPlacement(position, plan.Products[i])
                                 : merchGroup.InsertPositionPlacement(position, plan.Products[i], anchor, PlanogramPositionAnchorDirection.ToRight);
                    merchGroup.Process();
                }
                merchGroup.ApplyEdit();

                Dictionary<Int32, Single> xCoordsByPositionId = plan.Positions.ToDictionary(p => Convert.ToInt32(p.Id), p => p.X);
                Assert.That(plan.Positions.All(p => p.X.GreaterOrEqualThan(merchGroup.MinX)));

                // Now, insert the 6th in front of the 5th and make sure that we're still spread even and that we haven't overlapped
                // the chest walls.
                PlanogramPosition newPosition = PlanogramPosition.NewPlanogramPosition(1, plan.Products[5]);
                merchGroup.InsertPositionPlacement(newPosition, plan.Products.Last(), anchor, PlanogramPositionAnchorDirection.InFront);
                merchGroup.Process();
                merchGroup.ApplyEdit();

                foreach (var p in plan.Positions)
                {
                    Single xCoord;
                    if (xCoordsByPositionId.TryGetValue(Convert.ToInt32(p.Id), out xCoord))
                        AssertHelper.AreSinglesEqual(xCoord, p.X, $"{p.GetPlanogramProduct().Gtin} should have had an X of {xCoord}, but had {p.X}");
                }
                Assert.That(plan.Positions.All(p => p.X.GreaterOrEqualThan(merchGroup.MinX)), expectedMessage);
            }
        }

        [Test]
        public void InsertPositionPlacement_Bar_PositionShouldAdjoinSubComponent()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 150, 0));
            var product = plan.AddProduct();
            var position = PlanogramPosition.NewPlanogramPosition(1, product);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();
                merchGroup.InsertPositionPlacement(position, product);
                merchGroup.Process();
                merchGroup.ApplyEdit();
            }

            Assert.AreEqual(2f, position.Z);
        }

        [Test]
        public void InsertPositionPlacement_Pegboard_PositionsShouldNotOverlapWhenSufficientSpace()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 50, 0), new WidthHeightDepthValue(120, 120, 4));
            var pegSubComp = peg.GetPlanogramComponent().SubComponents.First();
            pegSubComp.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            pegSubComp.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            for (Int32 p = 0; p < 6; p++)
            {
                plan.AddProduct(new WidthHeightDepthValue(10, 10, 5));
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                // In order to replicate this issue, add 5 positions high before adding the sixth.
                PlanogramPositionPlacement anchor = null;
                for (Int32 i = 0; i < 5; i++)
                {
                    PlanogramPosition newPosition = PlanogramPosition.NewPlanogramPosition(1, plan.Products[i]);
                    anchor = anchor == null
                                 ? merchGroup.InsertPositionPlacement(newPosition, plan.Products[i])
                                 : merchGroup.InsertPositionPlacement(newPosition, plan.Products[i], anchor, PlanogramPositionAnchorDirection.Above);
                    merchGroup.Process();
                    merchGroup.ApplyEdit();
                }

                Assert.IsFalse(merchGroup.PositionPlacements.Select(p => p.GetReservedSpace()).HasCollisionOverlaps());

                // Now insert a sixth product, positions still shouldn't overlap.
                PlanogramPosition position = PlanogramPosition.NewPlanogramPosition(1, plan.Products.Last());
                merchGroup.InsertPositionPlacement(position, plan.Products.Last(), anchor, PlanogramPositionAnchorDirection.Above);
                merchGroup.Process();
                merchGroup.ApplyEdit();

                Assert.IsFalse(merchGroup.PositionPlacements.Select(p => p.GetReservedSpace()).HasCollisionOverlaps());
            }
        }

        [Test]
        public void InsertPositionPlacement_EvenShelfWithDividersFromStartValue_ShouldSpreadDividersEvenly([Values(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)] Int32 noOfProducts)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelfSubComp = shelf.GetPlanogramComponent().SubComponents.First();
            shelfSubComp.DividerObstructionStartX = 0;
            shelfSubComp.DividerObstructionSpacingX = 10/(noOfProducts/3f);
            shelfSubComp.DividerObstructionWidth = 1;
            shelfSubComp.DividerObstructionHeight = 10;
            shelfSubComp.DividerObstructionDepth = 10;
            shelfSubComp.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;

            for (Int32 i = 0; i < noOfProducts; i++)
            {
                plan.AddProduct(new WidthHeightDepthValue(4, 10, 10));
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                var anchor = merchGroup.InsertPositionPlacement(plan.Products[0]);

                for (Int32 i = 1; i < noOfProducts; i++)
                {
                    anchor = merchGroup.InsertPositionPlacement(plan.Products[i], anchor, PlanogramPositionAnchorDirection.ToRight);
                }

                merchGroup.Process();
                merchGroup.ApplyEdit();
                //plan.TakeSnapshot("InsertPositionPlacement_EvenShelfWithDividersFromStartValue_ShouldSpreadDividersEvenly");

                Assert.AreEqual(noOfProducts, merchGroup.PositionPlacements.Count);
                Assert.AreEqual(Math.Max(0, noOfProducts - 1), merchGroup.DividerPlacements.Count);
                Assert.That(merchGroup.PositionPlacements.All(p => merchGroup.MaxX.GreaterOrEqualThan(p.Position.X + p.GetPositionDetails().TotalSpace.Width)));
                //Assert.That(DividersBetweenAllPositionsButTheFirstTwo(merchGroup));
                Assert.That(DividersBetweenAllPositions(merchGroup));
                Assert.That(DividersDoNotOverlapPositions(merchGroup));
            }
        }

        private static Boolean DividersDoNotOverlapPositions(PlanogramMerchandisingGroup merchGroup)
        {
            var positionsAndDividers = new Stack<Tuple<Single, Single>>(merchGroup.PositionPlacements.Select(p => new Tuple<Single, Single>(p.Position.X, p.GetPositionDetails().TotalSize.Width)).Union(merchGroup.DividerPlacements.Select(d => new Tuple<Single, Single>(d.X, d.Width))).OrderBy(t => t.Item1).ToList());
            while (!positionsAndDividers.Any())
            {
                var positionOrDivider = positionsAndDividers.Pop();
                foreach (var otherPositionOrDivider in positionsAndDividers)
                {
                    Assert.IsFalse(Math.Min(positionOrDivider.Item1 + positionOrDivider.Item2, otherPositionOrDivider.Item1 + otherPositionOrDivider.Item2).GreaterThan(Math.Max(positionOrDivider.Item1, otherPositionOrDivider.Item1)));
                }
            }

            return true;
        }

        private static Boolean DividersBetweenAllPositions(PlanogramMerchandisingGroup merchGroup)
        {
            IEnumerable<Single> dividerCoords = merchGroup.DividerPlacements.Select(d => d.X);

            for (Int32 i = 0; i < merchGroup.PositionPlacements.Count; i++)
            {
                if (i == merchGroup.PositionPlacements.Count - 1) break;

                PlanogramPositionPlacement position = merchGroup.PositionPlacements.ElementAt(i);
                PlanogramPositionPlacement positionAfter = merchGroup.PositionPlacements.ElementAt(i + 1);

                Single x1 = position.Position.X + position.GetPositionDetails().TotalSpace.Width;
                Single x2 = positionAfter.Position.X;

                Assert.AreEqual(1, dividerCoords.Count(d => x1.LessOrEqualThan(d) && x2.GreaterOrEqualThan(d)),
                                $"Product: {positionAfter.Product.Gtin}");
            }

            return true;
        }

        #endregion

        #region Initialization

        private static IEnumerable<Tuple<Single, Single, Single>> InitializeCoordsSource =>
            new List<Tuple<Single, Single, Single>>
            {
                new Tuple<Single, Single, Single>(0, 0, 0),
                new Tuple<Single, Single, Single>(0, 150, 0),
                new Tuple<Single, Single, Single>(50, 150, 0),
                new Tuple<Single, Single, Single>(0, 250, 0),
                new Tuple<Single, Single, Single>(0, 150, 100)
            };

        private static Single[] RotationSource => new[] {0f, Convert.ToSingle(Math.PI/4f), Convert.ToSingle(Math.PI/2f), Convert.ToSingle(Math.PI)};

        [Test]
        public void Initialize_SetsMinAndMax_ForShelfAtCoords([ValueSource(nameof(InitializeCoordsSource))] Tuple<Single, Single, Single> shelfCoordsTuple, [ValueSource(nameof(RotationSource))] Single slope, [ValueSource(nameof(RotationSource))] Single angle, [ValueSource(nameof(RotationSource))] Single roll)
        {
            const Single planHeight = 200;
            WidthHeightDepthValue shelfSize = new WidthHeightDepthValue(120, 4, 75);
            PointValue shelfCoords = new PointValue(shelfCoordsTuple.Item1, shelfCoordsTuple.Item2, shelfCoordsTuple.Item3);

            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, shelfCoords, shelfSize);
            shelf.Slope = slope;
            shelf.Angle = angle;
            shelf.Roll = roll;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var shelfMerchGroup = merchGroups.First();

                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinX, "Merch space should start at the left side of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Width, shelfMerchGroup.MaxX, "Merch space should end at the right of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Height, shelfMerchGroup.MinY, "Merch space should start at the top of the shelf.");
                AssertHelper.AreSinglesEqual(Math.Max(planHeight - shelfCoords.Y, shelfSize.Height), shelfMerchGroup.MaxY, "Merch space should end at the top of the plan.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinZ, "Merch space should start at the back of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Depth, shelfMerchGroup.MaxZ, "Merch space should end at the front of the shelf.");
            }
        }

        [Test]
        public void Initialize_SetsMinAndMax_ForCombinedShelfAtCoords([ValueSource(nameof(InitializeCoordsSource))] Tuple<Single, Single, Single> shelfCoordsTuple, [ValueSource(nameof(RotationSource))] Single slope)
        {
            const Single planHeight = 200;
            WidthHeightDepthValue shelfSize = new WidthHeightDepthValue(120, 4, 75);
            PointValue shelfCoords = new PointValue(shelfCoordsTuple.Item1, shelfCoordsTuple.Item2, shelfCoordsTuple.Item3);

            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, shelfCoords, shelfSize);
            shelf1.Slope = slope;
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            var bay2 = plan.AddFixtureItem();
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, shelfCoords, shelfSize);
            shelf2.Slope = slope;
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var shelfMerchGroup = merchGroups.First();

                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinX, "Merch space should start at the left side of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Width*2, shelfMerchGroup.MaxX, "Merch space should end at the right of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Height, shelfMerchGroup.MinY, "Merch space should start at the top of the shelf.");
                AssertHelper.AreSinglesEqual(Math.Max(planHeight - shelfCoords.Y, shelfSize.Height), shelfMerchGroup.MaxY, "Merch space should end at the top of the plan.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinZ, "Merch space should start at the back of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Depth, shelfMerchGroup.MaxZ, "Merch space should end at the front of the shelf.");
            }
        }

        [Test]
        public void Initialize_SetsMinAndMax_ForCombinedShelfAtCoords_OnRotatedFixture([ValueSource(nameof(InitializeCoordsSource))] Tuple<Single, Single, Single> shelfCoordsTuple, [ValueSource(nameof(RotationSource))] Single rotation)
        {
            if (!(rotation%(Math.PI/2f)).EqualTo(0)) Assert.Ignore("Known issue with fixture rotations that are not a multiple of Pi/2. See V8-30644");
            const Single planHeight = 200;
            WidthHeightDepthValue shelfSize = new WidthHeightDepthValue(120, 4, 75);
            PointValue shelfCoords = new PointValue(shelfCoordsTuple.Item1, shelfCoordsTuple.Item2, shelfCoordsTuple.Item3);

            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            bay1.Angle = rotation;
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, shelfCoords, shelfSize);
            shelf1.Slope = rotation;
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            var bay2 = plan.AddFixtureItem();
            bay2.Angle = rotation;
            bay2.X = Convert.ToSingle(bay1.GetPlanogramFixture().Width*Math.Cos(rotation));
            bay2.Z = Convert.ToSingle(bay1.GetPlanogramFixture().Width*Math.Cos(Math.PI/2 - rotation));
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, shelfCoords, shelfSize);
            shelf2.Slope = rotation;
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var shelfMerchGroup = merchGroups.First();

                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinX, "Merch space should start at the left side of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Width*2, shelfMerchGroup.MaxX, "Merch space should end at the right of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Height, shelfMerchGroup.MinY, "Merch space should start at the top of the shelf.");
                AssertHelper.AreSinglesEqual(Math.Max(planHeight - shelfCoords.Y, shelfSize.Height), shelfMerchGroup.MaxY, "Merch space should end at the top of the plan.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinZ, "Merch space should start at the back of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Depth, shelfMerchGroup.MaxZ, "Merch space should end at the front of the shelf.");
            }
        }

        [Test]
        public void Initialize_SetsMinAndMax_ForShelfAtCoords_WithFixtureRotation([ValueSource(nameof(InitializeCoordsSource))] Tuple<Single, Single, Single> shelfCoordsTuple, [ValueSource(nameof(RotationSource))] Single slope, [ValueSource(nameof(RotationSource))] Single angle, [ValueSource(nameof(RotationSource))] Single roll)
        {
            const Single planHeight = 200;
            WidthHeightDepthValue shelfSize = new WidthHeightDepthValue(120, 4, 75);
            PointValue shelfCoords = new PointValue(shelfCoordsTuple.Item1, shelfCoordsTuple.Item2, shelfCoordsTuple.Item3);

            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            //bay.Slope = slope;
            bay.Angle = angle;
            //bay.Roll = roll;
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, shelfCoords, shelfSize);
            shelf.Slope = slope;
            shelf.Angle = angle;
            shelf.Roll = roll;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var shelfMerchGroup = merchGroups.First();

                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinX, "Merch space should start at the left side of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Width, shelfMerchGroup.MaxX, "Merch space should end at the right of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Height, shelfMerchGroup.MinY, "Merch space should start at the top of the shelf.");
                AssertHelper.AreSinglesEqual(Math.Max(planHeight - shelfCoords.Y, shelfSize.Height), shelfMerchGroup.MaxY, "Merch space should end at the top of the plan.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinZ, "Merch space should start at the back of the shelf.");
                AssertHelper.AreSinglesEqual(shelfSize.Depth, shelfMerchGroup.MaxZ, "Merch space should end at the front of the shelf.");
            }
        }

        [Test]
        public void Initialize_SetsMinAndMax_ForPegBoardAtCoords([ValueSource(nameof(InitializeCoordsSource))] Tuple<Single, Single, Single> pegCoordsTuple, [ValueSource(nameof(RotationSource))] Single slope, [ValueSource(nameof(RotationSource))] Single angle, [ValueSource(nameof(RotationSource))] Single roll)
        {
            WidthHeightDepthValue pegSize = new WidthHeightDepthValue(120, 100, 4);
            PointValue pegCoords = new PointValue(pegCoordsTuple.Item1, pegCoordsTuple.Item2, pegCoordsTuple.Item3);

            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, pegCoords, pegSize);
            peg.Slope = slope;
            peg.Angle = angle;
            peg.Roll = roll;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var shelfMerchGroup = merchGroups.First();

                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinX, "Merch space should start at the left side of the pegboard.");
                AssertHelper.AreSinglesEqual(pegSize.Width, shelfMerchGroup.MaxX, "Merch space should end at the right of the pegboard.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinY, "Merch space should start at the bottom of the pegboard.");
                AssertHelper.AreSinglesEqual(pegSize.Height, shelfMerchGroup.MaxY, "Merch space should end at the top of the pegboard.");
                AssertHelper.AreSinglesEqual(pegSize.Depth, shelfMerchGroup.MinZ, "Merch space should start at the front of the pegboard.");
                AssertHelper.AreSinglesEqual(pegSize.Depth, shelfMerchGroup.MaxZ, "Merch space should end at the front of the pegboard.");
            }
        }

        [Test]
        public void Initialize_SetsMinAndMax_ForRodAtCoords([ValueSource(nameof(InitializeCoordsSource))] Tuple<Single, Single, Single> rodCoordsTuple, [ValueSource(nameof(RotationSource))] Single slope, [ValueSource(nameof(RotationSource))] Single angle, [ValueSource(nameof(RotationSource))] Single roll)
        {
            WidthHeightDepthValue rodSize = new WidthHeightDepthValue(4, 4, 75);
            PointValue rodCoords = new PointValue(rodCoordsTuple.Item1, rodCoordsTuple.Item2, rodCoordsTuple.Item3);

            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var rod = bay.AddFixtureComponent(PlanogramComponentType.Rod, rodCoords, rodSize);
            rod.Slope = slope;
            rod.Angle = angle;
            rod.Roll = roll;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var shelfMerchGroup = merchGroups.First();

                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinX, "Merch space should start at the left side of the rod.");
                AssertHelper.AreSinglesEqual(rodSize.Width, shelfMerchGroup.MaxX, "Merch space should end at the right of the rod.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinY, "Merch space should start at the bottom of the rod.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MaxY, "Merch space should end at the bottom of the rod.");
                AssertHelper.AreSinglesEqual(0f, shelfMerchGroup.MinZ, "Merch space should start at the back of the rod.");
                AssertHelper.AreSinglesEqual(rodSize.Depth, shelfMerchGroup.MaxZ, "Merch space should end at the front of the rod.");
            }
        }

        #endregion

        #region GetMerchandisingSpace

        [Test]
        public void GetMerchandisingSpace_CombinesSpaceOfSubComponents()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay2.X = 120;
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                var space = merchGroup.GetMerchandisingSpace();

                var expected = new RectValue(0, 4, 0, 240, 196, 75);
                AssertHelper.AreRectValuesEqual(expected, space.UnhinderedSpace);
            }
        }

        [Test]
        public void GetMerchandisingSpace_WhenPlanRelative_IsPlanRelative()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay2.X = 120;
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                var space = merchGroup.GetMerchandisingSpace(planogramRelative: true);

                var expected = new RectValue(0, 154, 0, 240, 46, 75);
                AssertHelper.AreRectValuesEqual(expected, space.UnhinderedSpace);
            }
        }

        [Test]
        public void GetMerchandisingSpace_WhenNotPlanRelative_IsMerchGroupRelative()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay2.X = 120;
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                var space = merchGroup.GetMerchandisingSpace(planogramRelative: false);

                var expected = new RectValue(0, 4, 0, 240, 46, 75);
                AssertHelper.AreRectValuesEqual(expected, space.UnhinderedSpace);
            }
        }

        private IEnumerable<RectValue> GetMerchandisingSpace_WhenConstrainingSpace_IsReducedSource
        {
            get
            {
                yield return new RectValue(0, 0, 0, 120, 50, 80);
                yield return new RectValue(120, 0, 0, 120, 50, 80);
                yield break;
            }
        }

        [Test]
        public void GetMerchandisingSpace_WhenConstrainingSpace_IsReduced([ValueSource(nameof(GetMerchandisingSpace_WhenConstrainingSpace_IsReducedSource))] RectValue constraint)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay2.X = 120;
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                var space = merchGroup.GetMerchandisingSpace(planogramRelative: false, constrainingSpace: constraint);

                var expected = new RectValue(0, 4, 0, 240, 46, 75).Intersect(constraint).Value;
                AssertHelper.AreRectValuesEqual(expected, space.UnhinderedSpace);
            }
        }

        #endregion

        #region GetWhiteSpaceOnAxis

        [Test]
        public void GetWhiteSpaceOnAxis_ForShelf_InX()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var mg = merchGroups.First();

                Single whiteSpace = mg.GetWhiteSpaceOnAxis(AxisType.X, mg.PositionPlacements.First());

                Assert.That(whiteSpace, Is.EqualTo(110));
            }
        }

        #endregion

        #region Dividers

        [Test]
        public void Process_StillAppliesSqueeze_WhenShelfHasDividersAndNegativeOverhang(
            [Values(PlanogramSubComponentXMerchStrategyType.Even, PlanogramSubComponentXMerchStrategyType.LeftStacked)]
            PlanogramSubComponentXMerchStrategyType merchStrategy)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.GetPlanogramFixture().Width = 180;
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(), new WidthHeightDepthValue(180,22,69));
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = merchStrategy;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionHeight = 2;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionWidth = 0.36f;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionSpacingX = 1.25f;
            shelf.GetPlanogramComponent().SubComponents[0].LeftOverhang = -2.35f;
            shelf.GetPlanogramComponent().SubComponents[0].RightOverhang = -1.25f;
            var pos1 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(21.4f, 3, 29)).SetSqueeze(0.85f, 1, 0.75f)).SetMainFacings(7, 2, 2);
            var pos2 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(21.4f, 3, 29)).SetSqueeze(0.85f, 1, 0.8f)).SetMainFacings(7, 2, 2);
            var pos3 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(23f, 3, 24)).SetSqueeze(1, 1, 0.9f)).SetMainFacings(7, 4, 2);

            using (var mgs = plan.GetMerchandisingGroups())
            {
                mgs[0].Process();
                mgs.ApplyEdit();
                //plan.TakeSnapshot("Process_StillAppliesSqueeze_WhenShelfHasDividersAndNegativeOverhang");
            }

            (pos3.X + pos3.GetPositionDetails().TotalSize.Width).Should().BeLessThan(180);
        }

        [Test]
        public void Process_StillAppliesSqueeze_WhenShelfHasDividers(
            [Values(PlanogramSubComponentXMerchStrategyType.Even, PlanogramSubComponentXMerchStrategyType.LeftStacked)]
            PlanogramSubComponentXMerchStrategyType merchStrategy)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = merchStrategy;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionHeight = 10;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionWidth = 1;
            shelf.GetPlanogramComponent().SubComponents[0].DividerObstructionSpacingX = 1;
            for(int i = 0; i < 2; i++)
            {
                var prod = plan.AddProduct(new WidthHeightDepthValue(120,10,10));
                prod.SqueezeWidth = 0.5f;
                shelf.AddPosition(prod);
            }

            using (var mgs = plan.GetMerchandisingGroups())
            {
                mgs[0].Process();
                mgs.ApplyEdit();
                //plan.TakeSnapshot("Process_StillAppliesSqueeze_WhenShelfHasDividers");
            }

            plan.Positions[0].HorizontalSqueeze.Should().BeApproximately(
                0.5f, 0.01f, "because the first position should be squeezed to half its original width");
            plan.Positions[1].HorizontalSqueeze.Should().BeApproximately(
                0.5f, 0.01f, "because the second position should be squeezed to half its original width");
        }

        [Test]
        public void Process_StillAppliesSqueeze_WhenShelfHasNoDividers()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 2; i++)
            {
                var prod = plan.AddProduct(new WidthHeightDepthValue(120, 10, 10));
                prod.SqueezeWidth = 0.5f;
                shelf.AddPosition(prod);
            }

            using (var mgs = plan.GetMerchandisingGroups())
            {
                mgs[0].Process();
                mgs.ApplyEdit();
            }

            plan.Positions[0].HorizontalSqueeze.Should().Be(0.5f, "because the first position should be squeezed to half its original width");
            plan.Positions[1].HorizontalSqueeze.Should().Be(0.5f, "because the second position should be squeezed to half its original width");
        }

        [Test]
        public void Process_ProductsShouldNotOverlap_WhenDividersAndNegativeOverhang()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var shelf = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, size:new WidthHeightDepthValue(180,4.2f,57));
            var subComp = shelf.GetPlanogramComponent().SubComponents[0];
            subComp.MerchandisableHeight = 24;
            subComp.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            subComp.LeftOverhang = -1.25f;
            subComp.RightOverhang = -1.25f;
            subComp.DividerObstructionHeight = 2;
            subComp.DividerObstructionWidth = 0.28f;
            subComp.DividerObstructionSpacingX = 1.82f;
            var pos1 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(26, 6, 7.5f))).SetMainFacings(3, 2, 7);
            var pos2 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(26, 6, 7.5f))).SetMainFacings(3, 1, 7);
            var pos3 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(28, 6, 7.5f))).SetMainFacings(3, 1, 7);
            var pos4 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(28, 6, 7.5f))).SetMainFacings(3, 1, 7);
            var pos5 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(17.5f, 17.5f, 5.8f))).SetMainFacings(1, 1, 9);
            var pos6 = shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(17.5f, 17.5f, 5.8f))).SetMainFacings(1, 1, 9);

            using (var mgs = plan.GetMerchandisingGroups())
            {
                mgs[0].Process();
                mgs.ApplyEdit();
                //plan.TakeSnapshot("Process_ProductsShouldNotOverlap_WhenDividersAndNegativeOverhang");
            }

            pos1.X.Should().BeInRange(1.25f, 1.82f, "because the 1st position should be between the left overhang and the first divider slot");
            pos2.X.Should().BeInRange(30 * 1.82f + 0.28f, 31 * 1.82f, "because the 2nd position should be between the 30th and 31st divider slots");
            pos3.X.Should().BeInRange(45*1.82f + 0.28f, 46*1.82f, "because the 3rd position should be between the 45th and 46th divider slot");
            pos4.X.Should().BeInRange(61*1.82f + 0.28f, 62*1.82f, "because the 4th position should be between the 61st and 62nd divider slot");
            pos5.X.Should().BeInRange(77*1.82f + 0.28f, 78*1.82f, "because the 5th position should be between the 77th and 78th divider slot");
            pos6.X.Should().BeInRange(87*1.82f + 0.28f, 88*1.82f, "because the 6th position should be between the 87th and 88th divider slot");
        }

        [Test]
        public void ProcessSnapsLeftStackedDivider()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang()
                .SetDividers(dividerAtStart: true, spacing: new PointValue(2, 0, 0))
                .SetMerchStrategyType()
                .AddPosition();
            
            planogram.ReprocessAllMerchandisingGroups();
            
            //  Starting from 0 (left stacked), plus 2 for the divider width.
            Assert.That(position.X, Is.EqualTo(2F));
        }

        [Test]
        public void ProcessSnapsLeftStackedDividerWithOverhang()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang(-1.5F)
                .SetDividers(dividerAtStart: true, spacing: new PointValue(2, 0, 0))
                .SetMerchStrategyType()
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            //  Starting from 0 (left stacked), plus 2 for the divider width, plus 2 overhang (it is 1.5 but snapped to the immediately next divider, so 2).
            Assert.That(position.X, Is.EqualTo(4F));
        }

        [Test]
        public void ProcessSnapsLeftStackedDividerWithStartXOffset()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang()
                .SetDividers(dividerAtStart: true, spacing: new PointValue(2, 0, 0), start: new PointValue(1, 0, 0))
                .SetMerchStrategyType()
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            //  Starting from 0 (left stacked), plus 2 for the divider width, plus 1 for the start offset.
            Assert.That(position.X, Is.EqualTo(3F));
        }

        [Test]
        public void ProcessSnapsRightStackedDivider()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang()
                .SetDividers(dividerAtStart: true, spacing: new PointValue(3, 0, 0))
                .SetMerchStrategyType(PlanogramSubComponentXMerchStrategyType.RightStacked)
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            //  Starting from 120 (right stacked), minus 2 for the divider width, minus 5 for the product width.
            Assert.That(position.X, Is.EqualTo(113F));
        }

        [Test]
        public void ProcessSnapsRightStackedDividerWithStartXOffset()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang()
                .SetDividers(dividerAtStart: true, spacing: new PointValue(3, 0, 0), start: new PointValue(1, 0, 0))
                .SetMerchStrategyType(PlanogramSubComponentXMerchStrategyType.RightStacked)
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            //  Starting from 120 (right stacked), minus 2 for the divider width, minus 1 for the start offset, minus 5 for the product's width.
            Assert.That(position.X, Is.EqualTo(112F));
        }

        [Test]
        public void ProcessSnapsRightStackedDividerWithOverhang()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang(right: -1.5F)
                .SetDividers(dividerAtStart: true, spacing: new PointValue(3, 0, 0))
                .SetMerchStrategyType(PlanogramSubComponentXMerchStrategyType.RightStacked)
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            //  Starting from 120 (right stacked), minus 2 for the divider width, minus 3 for the overhang 
            //  (1.5 but snapped to the immediately previous divider), minus 5 for the product's width.
            Assert.That(position.X, Is.EqualTo(110F));
        }

        [Test]
        public void ProcessSnapsEvenDivider()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang()
                .SetDividers(dividerAtStart: true, spacing: new PointValue(2, 0, 0))
                .SetMerchStrategyType(PlanogramSubComponentXMerchStrategyType.Even)
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            Assert.That(position.X, Is.EqualTo(55F));
        }

        [Test]
        public void ProcessSnapsEvenDividerWithOverhang()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang(-1.5F, -1.5F)
                .SetDividers(dividerAtStart: true, spacing: new PointValue(2, 0, 0))
                .SetMerchStrategyType(PlanogramSubComponentXMerchStrategyType.Even)
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            //  accounting for the overhang
            Assert.That(position.X, Is.EqualTo(53.5F));
        }

        [Test]
        public void ProcessSnapsEvenDividerWithStartXOffset()
        {
            Planogram planogram = "Test".CreatePackage().AddPlanogram();
            PlanogramPosition position = planogram
                .AddFixtureItem()
                .AddFixtureComponent(PlanogramComponentType.Shelf)
                .SetOverhang()
                .SetDividers(dividerAtStart: true, spacing: new PointValue(2, 0, 0), start: new PointValue(1, 0, 0))
                .SetMerchStrategyType(PlanogramSubComponentXMerchStrategyType.Even)
                .AddPosition();

            planogram.ReprocessAllMerchandisingGroups();

            Assert.That(position.X, Is.EqualTo(55F));
        }

        #endregion

        #region Product Squeeze

        [Test]
        public void ProductSqueeze_SingleVertical_OnShelf_ShelfAbove()
        {
            //CCM-18598
            //Checks that vertical squeeze is applied to all blocks of a position
            //when merchandising space is restricted by a shelf above.

            //create a simple single bay plan with 2 shelves and a product which should squeeze.
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            var bay1Fc = plan.AddBayWithBackboardAndBase(76, 48, 24, 3.15F, 5.5F);
            var shelf1 = bay1Fc.AddShelf(new PointValue(0, 5, 0), new WidthHeightDepthValue(48, 2, 18));
            var shelf2 = bay1Fc.AddShelf(new PointValue(0, 20, 0), new WidthHeightDepthValue(48, 2, 18));

            var prod1 = plan.AddProduct(new WidthHeightDepthValue(3.38F, 5.75F, 3.38F));
            prod1.SqueezeHeight = 0.5F;

            var pos1 = shelf1.AddPosition(prod1);
            pos1.SetMainFacings(3, 1, 10);

            //process and check that squeeze is applied.
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");

            //Main +YCap  - squeeze on main only:
            pos1.FacingsHigh = 2;
            pos1.SetYCapFacings(1, 1, 1);

            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.AreEqual(pos1.VerticalSqueezeY, 1, "No Vertical squeeze Y should be applied as squeeze depth is not allowed.");

            //Main +YCap - squeeze on both:
            prod1.SqueezeDepth = 0.1F;
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.Less(pos1.VerticalSqueezeY, 1, "Vertical squeeze Y should be applied");

            //Main +YCap - squeeze on y only:
            prod1.SqueezeHeight = 1;
            plan.ReprocessAllMerchandisingGroups();
            Assert.AreEqual(pos1.VerticalSqueeze, 1, "Vertical squeeze should not be applied as it is not allowed.");
            Assert.Less(pos1.VerticalSqueezeY, 1, "Vertical squeeze Y should be applied");

            //remove the y cap
            pos1.SetYCapFacings(0, 0,0);
            pos1.FacingsHigh = 3;

            //add an x cap
            pos1.SetXCapFacings(3, 1, 1);
            prod1.SqueezeHeight = 0.5F;

            //Main +XCap - squeeze on both:
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.Less(pos1.VerticalSqueezeX, 1, "Vertical squeeze should be applied to the right cap.");

            //remove the x cap
            pos1.SetXCapFacings(0, 0, 0);

            //add z cap
            pos1.SetZCapFacings(3,1, 1);

            //Main +ZCap - squeeze on both:
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.Less(pos1.VerticalSqueezeZ, 1, "Vertical squeeze should be applied to the back cap.");
        }

        [Test]
        public void ProductSqueeze_SingleVertical_OnShelf_MerchHeight()
        {
            //CCM-18598
            //Checks that vertical squeeze is applied to the main block
            //when merchandising space is restricted by a set merch height

            //create a simple single bay plan with 2 shelves and a product which should squeeze.
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            var bay1Fc = plan.AddBayWithBackboardAndBase(76, 48, 24, 3.15F, 5.5F);
            var shelf1 = bay1Fc.AddShelf(new PointValue(0, 5, 0), new WidthHeightDepthValue(48, 2, 18));
            shelf1.SubComponent.MerchandisableHeight = 14;

            var prod1 = plan.AddProduct(new WidthHeightDepthValue(3.38F, 5.75F, 3.38F));
            prod1.SqueezeHeight = 0.5F;

            var pos1 = shelf1.AddPosition(prod1);
            pos1.FacingsHigh = 3;
            pos1.FacingsWide = 1;
            pos1.FacingsDeep = 10;

            //process and check that squeeze is applied.
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");

            //Main +YCap  - squeeze on main only:
            pos1.FacingsHigh = 2;
            pos1.FacingsYHigh = 1;
            pos1.FacingsYWide = 1;
            pos1.FacingsYDeep = 1;
            pos1.OrientationTypeY = PlanogramPositionOrientationType.Top0;

            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.AreEqual(pos1.VerticalSqueezeY, 1, "No Vertical squeeze Y should be applied as squeeze depth is not allowed.");

            //Main +YCap - squeeze on both:
            prod1.SqueezeDepth = 0.1F;
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.Less(pos1.VerticalSqueezeY, 1, "Vertical squeeze Y should be applied");

            //Main +YCap - squeeze on y only:
            prod1.SqueezeHeight = 1;
            plan.ReprocessAllMerchandisingGroups();
            Assert.AreEqual(pos1.VerticalSqueeze, 1, "Vertical squeeze should not be applied as it is not allowed.");
            Assert.Less(pos1.VerticalSqueezeY, 1, "Vertical squeeze Y should be applied");

            //remove the y cap
            pos1.SetYCapFacings(0, 0, 0);
            pos1.FacingsHigh = 3;

            //add an x cap
            pos1.FacingsXHigh = 3;
            pos1.FacingsXWide = 1;
            pos1.FacingsXDeep = 1;
            pos1.OrientationTypeX = PlanogramPositionOrientationType.Right0;
            prod1.SqueezeHeight = 0.5F;

            //Main +XCap - squeeze on both:
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.Less(pos1.VerticalSqueezeX, 1, "Vertical squeeze should be applied to the right cap.");


            //remove the x cap
            pos1.FacingsXHigh = 0;
            pos1.FacingsXWide = 0;
            pos1.FacingsXDeep = 0;

            //add z cap
            pos1.FacingsZHigh = 3;
            pos1.FacingsZWide = 1;
            pos1.FacingsZDeep = 1;
            pos1.OrientationTypeX = PlanogramPositionOrientationType.Right0;

            //Main +ZCap - squeeze on both:
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.VerticalSqueeze, 1, "Vertical squeeze should be applied.");
            Assert.Less(pos1.VerticalSqueezeZ, 1, "Vertical squeeze should be applied to the back cap.");
        }

        [Test]
        public void ProductSqueeze_SingleHorizontal_OnShelf()
        {
            //Checks that products will be squeezed horizontally if required.
            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();

            var bay1Fc = plan.AddBayWithBackboardAndBase(76, 48, 24, 3.15F, 5.5F);
            var shelf1 = bay1Fc.AddShelf(new PointValue(0, 5, 0), new WidthHeightDepthValue(48, 2, 18));

            var prod1 = plan.AddProduct(new WidthHeightDepthValue(4F, 5.75F, 3.38F));
            prod1.SqueezeWidth = 0.5F;
            prod1.SqueezeDepth = 0.5F;

            var pos1 = shelf1.AddPosition(prod1);
            pos1.SetMainFacings(3, 15, 10);

            //Main only:
            plan.ReprocessAllMerchandisingGroups();
            Assert.AreEqual(pos1.HorizontalSqueeze, 0.8F, "Horizontal squeeze should be applied.");

            //Main + X Cap:
            pos1.SetXCapFacings(3, 1, 1);
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.HorizontalSqueeze, 1, "Horizontal squeeze should be applied.");
            Assert.Less(pos1.HorizontalSqueezeX, 1, "Horizontal squeeze should be applied to the right cap");

            pos1.SetXCapFacings(0, 0, 0);

            //Main + Y Cap:
            pos1.FacingsHigh = 2;
            pos1.SetYCapFacings(1, 15, 1);
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.HorizontalSqueeze, 1, "Horizontal squeeze should be applied.");
            Assert.Less(pos1.HorizontalSqueezeY, 1, "Horizontal squeeze should be applied to the top cap.");

            pos1.SetYCapFacings(0,0,0);
            pos1.FacingsHigh = 3;

            //Main +  Z Cap:
            pos1.SetZCapFacings(3, 15, 1);
            plan.ReprocessAllMerchandisingGroups();
            Assert.Less(pos1.HorizontalSqueeze, 1, "Horizontal squeeze should be applied.");
            Assert.Less(pos1.HorizontalSqueezeZ, 1, "Horizontal squeeze should be applied to the back cap.");
        }

        [Test]
        public void ProductSqueeze_DividersXEven()
        {
            //CCM-18857 - client raised -
            //Checks that products are squeezed correctly in the x axis
            // when dividers are applied and the x strategy is even

            Package package = Package.NewPackage(DomainPrincipal.CurrentUserId, PackageLockType.User);
            Planogram plan = package.Planograms.Add();
            plan.ProductPlacementX = PlanogramProductPlacementXType.Manual;
            plan.ProductPlacementY = PlanogramProductPlacementYType.Manual;
            plan.ProductPlacementZ = PlanogramProductPlacementZType.Manual;

            //add bay 1
            PlanogramFixtureItem fixtureItem1 = plan.AddBayWithBackboardAndBase(200, 136, 67, backboardDepth: 1, baseHeight: 10);

            //add the shelf
            var bay1Shelf1S = fixtureItem1.AddShelf(new PointValue(0, 103F, 0), new WidthHeightDepthValue(136, 4, 57));
            bay1Shelf1S.SubComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            bay1Shelf1S.SubComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
            bay1Shelf1S.SubComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
            bay1Shelf1S.SubComponent.DividerObstructionHeight = 10;
            bay1Shelf1S.SubComponent.DividerObstructionWidth = 1;
            bay1Shelf1S.SubComponent.DividerObstructionDepth = 1;
            bay1Shelf1S.SubComponent.IsDividerObstructionAtStart = false;
            bay1Shelf1S.SubComponent.IsDividerObstructionAtEnd = false;
            bay1Shelf1S.SubComponent.IsDividerObstructionByFacing = false;
            bay1Shelf1S.SubComponent.DividerObstructionSpacingX = 1;

            //P1
            var prod1 = plan.AddProduct(new WidthHeightDepthValue(23.6F, 33F, 7.7F));
            prod1.SqueezeHeight = 1;
            prod1.SqueezeWidth = 0.75F;
            prod1.SqueezeDepth = 1;
            var p1 = bay1Shelf1S.AddPosition(prod1);
            p1.FacingsHigh = 1;
            p1.FacingsWide = 2;
            p1.FacingsDeep = 7;
            p1.X = 0;
            p1.Sequence = 1;
            p1.SequenceX = 1;

            //P2
            var prod2 = plan.AddProduct(new WidthHeightDepthValue(19.2F, 30F, 7F));
            prod2.SqueezeHeight = 1;
            prod2.SqueezeWidth = 0.75F;
            prod2.SqueezeDepth = 1;
            var p2 = bay1Shelf1S.AddPosition(prod2);
            p2.FacingsHigh = 1;
            p2.FacingsWide = 4;
            p2.FacingsDeep = 8;
            p2.X = 42;
            p2.Sequence = 2;
            p2.SequenceX = 2;

            //P3
            var prod3 = plan.AddProduct(new WidthHeightDepthValue(28.9F, 10.4F, 8.5F));
            prod3.SqueezeHeight = 1;
            prod3.SqueezeWidth = 0.75F;
            prod3.SqueezeDepth = 1;
            var p3 = bay1Shelf1S.AddPosition(prod3);
            p3.FacingsHigh = 3;
            p3.FacingsWide = 1;
            p3.FacingsDeep = 6;
            p3.X = 114;
            p3.Sequence = 3;
            p3.SequenceX = 3;

            //now remerch and check that squeeze has been applied.
            plan.ReprocessAllMerchandisingGroups();

            //now check:
            Assert.IsTrue(MathHelper.EqualTo(0.8686F, p1.HorizontalSqueeze,4));
            Assert.IsTrue(MathHelper.EqualTo(0.9245F, p2.HorizontalSqueeze,4));
            Assert.IsTrue(MathHelper.EqualTo(0.7612F, p3.HorizontalSqueeze,4));
        }

        #endregion
    }
}