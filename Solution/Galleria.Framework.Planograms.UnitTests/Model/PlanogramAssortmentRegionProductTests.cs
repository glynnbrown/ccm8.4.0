﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26789 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Moq;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using System.Linq.Expressions;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramAssortmentRegionProductTests
    {
        [Test]
        public void NewPlanogramAssortmentRegionProduct_Interface_AddsProperties()
        {
            var mock = new Mock<IPlanogramAssortmentRegionProduct>();
            mock.SetupGet(p => p.PrimaryProductGtin).Returns("PrimaryProductGTIN");
            mock.SetupGet(p => p.RegionalProductGtin).Returns("RegionalProductGTIN");
            var mockInterface = mock.Object;

            var model = PlanogramAssortmentRegionProduct.NewPlanogramAssortmentRegionProduct(mockInterface);

            AssertHelper.AreEqual<IPlanogramAssortmentRegionProduct>(model, mockInterface);
        }
    }
}
