﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26789 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Moq;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using System.Linq.Expressions;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramAssortmentRegionLocationTests
    {
        [Test]
        public void NewPlanogramAssortmentRegionLocation_Interface_AddsProperties()
        {
            var mock = new Mock<IPlanogramAssortmentRegionLocation>();
            mock.SetupGet(p => p.LocationCode).Returns("LocationCode");
            var mockInterface = mock.Object;

            var model = PlanogramAssortmentRegionLocation.NewPlanogramAssortmentRegionLocation(mockInterface);

            AssertHelper.AreEqual<IPlanogramAssortmentRegionLocation>(model, mockInterface);
        }
    }
}
