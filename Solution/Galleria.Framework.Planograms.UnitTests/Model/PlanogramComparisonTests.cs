﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31819 : A.Silva
//  Created.
// V8-32931 : A.Silva
//  Added testing for GetComparisonStatus.

#endregion

#endregion

using System;
using FluentAssertions;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramComparisonTests : TestBase<PlanogramComparison, PlanogramComparisonDto>
    {
        #region Coding Standards

        [Test]
        public void Serializable()
        {
            TestSerialize();
        }

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        [Test, TestCaseSource("GetMatchingDtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("GetMatchingDtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }

        [Test]
        public void GetComparisonStatusIsNotFoundWhenAtLeastOneComparedPlanItemIsNew()
        {
            PlanogramComparison sut = "Master Plan".CreatePackage().AddPlanogram().Comparison;
            sut.Parent.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            Planogram comparedPlan = "Compared Plan".CreatePackage().AddPlanogram();
            PlanogramPosition comparedPosition = comparedPlan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            sut.UpdateResults(new []{comparedPlan});

            PlanogramItemComparisonStatusType status = sut.GetComparisonStatus(comparedPosition);

            status.Should().Be(PlanogramItemComparisonStatusType.NotFound, because: "the position is missing from the Master Plan.");
        }

        [Test]
        public void GetComparisonStatusIsNotFoundWhenNoResultsForThePlanItem()
        {
            PlanogramComparison sut = "Master Plan".CreatePackage().AddPlanogram().Comparison;
            sut.Parent.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            Planogram comparedPlan = "Compared Plan".CreatePackage().AddPlanogram();
            comparedPlan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            sut.UpdateResults(new[] { comparedPlan });

            PlanogramItemComparisonStatusType status = sut.GetComparisonStatus("Not Compare Plan".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition());

            status.Should().Be(PlanogramItemComparisonStatusType.NotFound, because: "the position is missing from the Master Plan.");
        }

        #endregion
    }
}