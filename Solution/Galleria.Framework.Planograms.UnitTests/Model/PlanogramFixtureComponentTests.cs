﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26986 : A.Kuszyk
//  Created.
#endregion
#region Version History: v830
// V8-32521 : J.Pickup
//      Added Metadata test : MetaComponentIsOutsideOfFixtureArea
// CCM-18621 : A.Silva
//  Amended and refactored ClipStripWithWiderPeggedProductPositionDoesNotOverfill test.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.DataStructures;
using System.Diagnostics;
using FluentAssertions;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramFixtureComponentTests : TestBase<PlanogramFixtureComponent, PlanogramFixtureComponentDto>
    {
        #region Helpers

        /// <summary>
        /// Returns the name of all properties that can be loaded from the dto.
        /// </summary>
        private IEnumerable<String> DtoPropertyNames
        {
            get { return base.GetMatchingDtoPropertyNames(); }
        }

        /// <summary>
        ///     Product size of 32.2 wide x 8.2 high x 8.2 deep.
        /// </summary>
        private static WidthHeightDepthValue Size32X8X8 { get; } = new WidthHeightDepthValue(32.2f, 8.2f, 8.2f);

        #endregion

        #region Serializable
        /// <summary>
        /// Serializable
        /// </summary>
        [Test]
        public virtual void Serializable()
        {
            TestSerialize();
        }
        #endregion

        #region Properties

        [Test]
        public void PropertySetters()
        {
            TestPropertySetters();
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewFactoryMethod()
        {
            TestNewFactoryMethod();
        }

        #endregion

        #region Data Access

        [Test, TestCaseSource("DtoPropertyNames")]
        public void LoadDataTransferObjectProperty(String propertyName)
        {
            TestLoadDataTransferObjectProperty(propertyName);
        }

        [Test, TestCaseSource("DtoPropertyNames")]
        public void GetDataTransferObjectProperty(String propertyName)
        {
            TestGetDataTransferObjectProperty(propertyName);
        }


        #endregion

        #region Methods

        #region GetPercentage

        private PlanogramFixtureItem _fixtureItem;
        private Planogram _planogram;

        #endregion

        #endregion

        #region Metadata

        [Test]
        public void MetadataAllPropertiesTested()
        {
            String[] methodNames = this.GetType().GetMethods().Select(m=> m.Name).ToArray();

            Boolean testsMissing = false;
            foreach (var property in typeof(PlanogramFixtureComponent).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    if(!methodNames.Any(m=> m.StartsWith(property.Name + "_")))
                    {
                        testsMissing = true;
                        Debug.WriteLine(property.Name);
                    }
                }
            }

            if (testsMissing)
            {
                Assert.Ignore();
            }
        }

        [Test]
        public void ClearMetadata()
        {
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a product
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;


            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);




            foreach (var property in typeof(PlanogramFixtureComponent).GetProperties())
            {
                if (property.Name.StartsWith("Meta"))
                {
                    //set it
                    property.SetValue(shelf1FC, TestDataHelper.GetValue1(property.PropertyType), null);

                    //call clear
                    package.ClearMetadata();

                    Assert.IsNull(property.GetValue(shelf1FC, null), property.Name);
                }
            }
        }

        [Test]
        public void MetaTotalComponentCollisions_NotCausedByBayRotation()
        {
            //create a bay with backboard and base

            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            PlanogramSettings settings = PlanogramSettings.NewPlanogramSettings();

            var fixtureItem = planogram.FixtureItems.Add(settings);
            var fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            var backboardFc = fixture.Components.Add(PlanogramComponentType.Backboard, settings);
            var backboard = planogram.Components.FindById(backboardFc.PlanogramComponentId);
            backboard.Width = fixture.Width;
            backboard.Height = fixture.Height;
            backboardFc.Z = -backboard.Depth;


            var fixtureBaseFc = fixture.Components.Add(PlanogramComponentType.Base, settings);
            var fixtureBase = planogram.Components.FindById(fixtureBaseFc.PlanogramComponentId);
            fixtureBase.Width = fixture.Width;
            fixtureBaseFc.X = backboardFc.X;
            fixtureBaseFc.Y = backboardFc.Y;
            fixtureBaseFc.Z = backboardFc.Z + backboard.Depth;

            //rotate the bay and offset by the depth
            fixtureItem.Angle = ToRadians(90);
            fixtureItem.X = 75;
            fixtureItem.Y = 0;
            fixtureItem.Z = 0;

            //now check the backboard and base bounding boxes.
            RectValue baseBoundingBox = fixtureBaseFc.GetPlanogramRelativeBoundingBox(fixtureBase, fixtureItem);
            AssertHelper.AreSinglesEqual(0, baseBoundingBox.X, "base bounding box incorrect");
            AssertHelper.AreSinglesEqual(0, baseBoundingBox.Y, "base bounding box incorrect");
            AssertHelper.AreSinglesEqual(0, baseBoundingBox.Z, "base bounding box incorrect");
            AssertHelper.AreSinglesEqual(75, baseBoundingBox.Width, "base bounding box incorrect");
            AssertHelper.AreSinglesEqual(10, baseBoundingBox.Height, "base bounding box incorrect");
            AssertHelper.AreSinglesEqual(fixture.Width, baseBoundingBox.Depth, "base bounding box incorrect");

            RectValue backboardBoundingBox = backboardFc.GetPlanogramRelativeBoundingBox(backboard, fixtureItem);
            AssertHelper.AreSinglesEqual(75, backboardBoundingBox.X, "backboard bounding box incorrect");
            AssertHelper.AreSinglesEqual(0, backboardBoundingBox.Y, "backboard bounding box incorrect");
            AssertHelper.AreSinglesEqual(0, backboardBoundingBox.Z, "backboard bounding box incorrect");
            AssertHelper.AreSinglesEqual(1, backboardBoundingBox.Width, "backboard bounding box incorrect");
            AssertHelper.AreSinglesEqual(fixture.Height, backboardBoundingBox.Height, "backboard bounding box incorrect");
            AssertHelper.AreSinglesEqual(fixture.Width, backboardBoundingBox.Depth, "backboard bounding box incorrect");

            //run meta data and check that there are no collisions
            package.CalculateMetadata(false, null);

            Assert.AreEqual(0, fixtureBaseFc.MetaTotalComponentCollisions, "Base should have no collision");
            Assert.AreEqual(0, backboardFc.MetaTotalComponentCollisions, "Backboard should have no collision");
        }

        [Category(Categories.Smoke)]
        [Test]
        public void MetaComponentIsOutsideOfFixtureArea()
        {
            //create a plan with an assortment
            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add();
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //Add a shelf
            PlanogramFixtureComponent shelf1FC = fixture.Components.Add(PlanogramComponentType.Shelf, fixture.Width, 4, 76.5F);
            PlanogramComponent shelf1C = planogram.Components.FindById(shelf1FC.PlanogramComponentId);

            PlanogramSubComponentPlacement subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                shelf1C.SubComponents[0], fixtureItem, shelf1FC);

            //Add some assortment products & products
            planogram.AddAssortmentProducts(10);

            //range only the first 5
            for (Int32 i = 0; i < 5; i++) planogram.Assortment.Products[i].IsRanged = true;
            for (Int32 i = 5; i < 10; i++) planogram.Assortment.Products[i].IsRanged = false;


            //calc metadata
            package.CalculateMetadata(false, null);

            PlanogramFixtureComponent fixtureComponent = fixture.Components.First(c => c.PlanogramComponentId == shelf1C.Id);

            Assert.IsNotNull(fixtureComponent.MetaIsOutsideOfFixtureArea, "should not be null: MetaIsOutsideOfFixtureArea");
            Assert.IsFalse(fixtureComponent.MetaIsOutsideOfFixtureArea.Value, "should be false: MetaIsOutsideOfFixtureArea");

            //Make a change - move the component out.
            shelf1FC.X = 200f;

            package.CalculateMetadata(false, null);

            Assert.IsNotNull(fixtureComponent.MetaIsOutsideOfFixtureArea, "should not be null: MetaIsOutsideOfFixtureArea");
            Assert.IsTrue(fixtureComponent.MetaIsOutsideOfFixtureArea.Value, "should be true: MetaIsOutsideOfFixtureArea");
        }

        public static Single ToRadians(Single degrees)
        {
            Single d = degrees;

            if (d > 360 || degrees < -360)
            {
                d = d % 360;
            }

            if (d < 0)
            {
                d = 360 + d;
            }

            return System.Convert.ToSingle(d * (Math.PI / 180f));
        }

        #endregion

        #region Validation Warnings

        [Test]
        public void ClipStripWithWiderPeggedProductPositionDoesNotOverfill()
        {
            Planogram plan = $"{nameof(ClipStripWithWiderPeggedProductPositionDoesNotOverfill)}".CreatePackage().AddPlanogram();
            PlanogramProduct peggedProduct = plan.AddProduct(Size32X8X8);
            peggedProduct.PegDepth = 2*Size32X8X8.Depth;
            peggedProduct.NumberOfPegHoles = 1;
            plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.ClipStrip).AddPosition(peggedProduct);
            
            plan.ReprocessAllMerchandisingGroups();
            List<PlanogramValidationWarning> validationWarnings = plan.CalculateMetadataAndGetValidationWarnings(false, null);

            validationWarnings.Should()
                              .NotContain(warning => warning.WarningType == PlanogramValidationWarningType.ComponentIsOverfilled,
                                          because: "the position on the clipstrip is valid");
        }

        [Test]
        public void ComponentIsOverfilled_NoWarningForOverhang()
        {
            //V8-29192 - Checks that overhangs do not incorrectly generate
            // is overfilled warnings.

            PlanogramSettings settings = PlanogramSettings.NewPlanogramSettings();

            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a product - make sure it is wider than the clipstrip
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add(settings);
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);
            fixture.Height = 191;
            fixture.Width = 120;
            fixture.Depth = 100;

            //add a shelf with an overhang
            PlanogramFixtureComponent component1FC = fixture.Components.Add(PlanogramComponentType.Shelf, settings);
            PlanogramComponent component1C = planogram.Components.FindById(component1FC.PlanogramComponentId);
            component1C.SubComponents[0].LeftOverhang = 50;

            //add a position
            PlanogramPosition position = component1FC.GetPlanogramSubComponentPlacements().First().AddPosition(prod1);
            component1C.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;

            //process
            planogram.ReprocessAllMerchandisingGroups();

            //calc metadata
            var warnings = planogram.CalculateMetadataAndGetValidationWarnings(false, null);
            Assert.IsFalse(warnings.Any(c => c.WarningType == PlanogramValidationWarningType.ComponentIsOverfilled), "Should not have overfilled warning.");
        }

        [Test]
        public void ComponentIsOverfilled_NoWarningForBar()
        {
            //V8-29192 - Checks that bars do not incorrectly generate
            // is overfilled warnings.

            PlanogramSettings settings = PlanogramSettings.NewPlanogramSettings();

            var package = Galleria.Framework.Planograms.Model.Package.NewPackage(1, PackageLockType.User);
            package.Name = "PackageName";

            var planogram = Planogram.NewPlanogram();
            planogram.Name = "PlanogramName";
            package.Planograms.Add(planogram);

            //add a product - make sure it is wider than the clipstrip
            PlanogramProduct prod1 = PlanogramProduct.NewPlanogramProduct();
            prod1.Gtin = "p1";
            prod1.Name = "p1";
            prod1.MerchandisingStyle = PlanogramProductMerchandisingStyle.Unit;
            prod1.Height = 32.2F;
            prod1.Width = 8.2F;
            prod1.Depth = 8.2F;
            planogram.Products.Add(prod1);

            //add a fixture
            PlanogramFixtureItem fixtureItem = planogram.FixtureItems.Add(settings);
            PlanogramFixture fixture = planogram.Fixtures.FindById(fixtureItem.PlanogramFixtureId);

            //add a shelf with an overhang
            PlanogramFixtureComponent component1FC = fixture.Components.Add(PlanogramComponentType.Bar, settings);
            PlanogramComponent component1C = planogram.Components.FindById(component1FC.PlanogramComponentId);
            component1FC.Y = 50;

            //add a position
            PlanogramPosition position = component1FC.GetPlanogramSubComponentPlacements().First().AddPosition(prod1);
            component1C.SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;

            //process
            planogram.ReprocessAllMerchandisingGroups();

            //calc metadata
            var warnings = planogram.CalculateMetadataAndGetValidationWarnings(false, null);
            Assert.IsFalse(warnings.Any(c => c.WarningType == PlanogramValidationWarningType.ComponentIsOverfilled), "Should not have overfilled warning.");
        }

        #endregion
    }
}
