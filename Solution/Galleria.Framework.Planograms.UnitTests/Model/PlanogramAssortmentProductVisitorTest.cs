﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentAssertions;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Moq;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    class PlanogramAssortmentProductVisitorTest
    {
        [Test]
        public void PlanogramAssortmentDataIsCreatedWithFacingsWhenValueFitsInByte()
        {
            const Int32 valueThatFitsInByte = 1;
            PlanogramPositionList positionList = PlanogramPositionList.NewPlanogramPositionList();
            positionList.Add(PlanogramProduct.NewPlanogramProduct()).FacingsWide = valueThatFitsInByte;
            var mockAssortmentData =
                Mock.Of<PlanogramAssortmentProductVisitor.IPlanAssortmentData>(
                    data => data.PositionsInPlanogram == positionList);
            var sut = new PlanogramAssortmentProductVisitor(mockAssortmentData);
            PlanogramAssortmentProduct productToProcess = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();

            sut.UpdateRangedAssortmentProduct(productToProcess, new List<String>(), p => true);

            productToProcess.Facings.Should().Be(valueThatFitsInByte, "correct value is returned if the value will fit in a byte");
        }

        [Test]
        public void PlanogramAssortmentDataIsCreatedWithUnitsWhenValueFitsInInt16()
        {
            const Int32 valueThatFitsInInt16 = 600;
            PlanogramPositionList positionList = PlanogramPositionList.NewPlanogramPositionList();
            positionList.Add(PlanogramProduct.NewPlanogramProduct()).TotalUnits = valueThatFitsInInt16;
            var mockAssortmentData =
                Mock.Of<PlanogramAssortmentProductVisitor.IPlanAssortmentData>(
                    data => data.PositionsInPlanogram == positionList);
            var sut = new PlanogramAssortmentProductVisitor(mockAssortmentData);
            PlanogramAssortmentProduct productToProcess = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();

            sut.UpdateRangedAssortmentProduct(productToProcess, new List<String>(), p => true);

            productToProcess.Units.Should().Be(valueThatFitsInInt16, "correct value is returned if the value will fit in a Int16");
        }

        [Test]
        public void PlanogramAssortmentDataIsCreatedWithMaxFacingsWhenOver255()
        {
            PlanogramPositionList positionList = PlanogramPositionList.NewPlanogramPositionList();
            positionList.Add(PlanogramProduct.NewPlanogramProduct()).FacingsWide = Int16.MaxValue;
            var mockAssortmentData =
                Mock.Of<PlanogramAssortmentProductVisitor.IPlanAssortmentData>(
                    data => data.PositionsInPlanogram == positionList);
            var sut = new PlanogramAssortmentProductVisitor(mockAssortmentData);
            PlanogramAssortmentProduct productToProcess = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();

            sut.UpdateRangedAssortmentProduct(productToProcess, new List<String>(), p => true);

            productToProcess.Facings.Should().Be(Byte.MaxValue, "max value is returned if the value will not fit in a byte");
        }

        [Test]
        public void PlanogramAssortmentDataIsCreatedWithMaxUnitsWhenOver255()
        {
            PlanogramPositionList positionList = PlanogramPositionList.NewPlanogramPositionList();
            positionList.Add(PlanogramProduct.NewPlanogramProduct()).TotalUnits = Int32.MaxValue;
            var mockAssortmentData =
                Mock.Of<PlanogramAssortmentProductVisitor.IPlanAssortmentData>(
                    data => data.PositionsInPlanogram == positionList);
            var sut = new PlanogramAssortmentProductVisitor(mockAssortmentData);
            PlanogramAssortmentProduct productToProcess = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct();

            sut.UpdateRangedAssortmentProduct(productToProcess, new List<String>(), p => true);

            productToProcess.Units.Should().Be(Int16.MaxValue, "max value is returned if the value will not fit in a Int16");
        }
    }
}
