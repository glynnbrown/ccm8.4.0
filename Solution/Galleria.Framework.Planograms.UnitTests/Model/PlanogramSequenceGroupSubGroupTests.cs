﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32504 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Ccm.Model;

namespace Galleria.Framework.Planograms.UnitTests.Model
{
    [TestFixture]
    public class PlanogramSequenceGroupSubGroupTests : TestBase
    {
        private Package _package;
        private Entity _entity;

        private Planogram TestPlanogram
        {
            get { return _package.Planograms.FirstOrDefault(); }
        }

        [SetUp]
        public void TestSetup()
        {
            _package = CreatePackage("PackageName");
            AddPlanogram(_package);
        }

        #region General Model

        [Test]
        public void Serializable()
        {
            Serialize(PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup());
        }

        #endregion

        #region Factory Methods

        [Test]
        public void NewPlanogramSequenceGroupProduct()
        {
            //  Create a new planogram sequence group product.
            PlanogramSequenceGroupProduct testModel = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();

            Assert.IsTrue(testModel.IsNew);
            Assert.IsTrue(testModel.IsChild);
        }

        [Test]
        public void Fetch()
        {
            PlanogramSequenceGroup sequenceGroup = AddSequenceGroup(TestPlanogram);
            PlanogramSequenceGroupSubGroup expected = AddGroupSubGroup(sequenceGroup);
            SavePackage();

            ReloadPackage();

            PlanogramSequenceGroupSubGroupList actualSubGroups = TestPlanogram.Sequence.Groups[0].SubGroups;
            CollectionAssert.IsNotEmpty(actualSubGroups);
            PlanogramSequenceGroupSubGroup actual = actualSubGroups[0];
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Alignment, actual.Alignment);
        }

        #endregion

        #region Data Access

        [Test]
        public void DataAccess_Insert()
        {
            // Already tested when testing Fetch.
        }

        [Test]
        public void DataAccess_Update()
        {
            //  Save the original, update and reload to test.
            PlanogramSequenceGroup sequenceGroup = AddSequenceGroup(TestPlanogram);
            PlanogramSequenceGroupSubGroup original = AddGroupSubGroup(sequenceGroup);
            SavePackage();
            PlanogramSequenceGroupSubGroup expected = TestPlanogram.Sequence.Groups[0].SubGroups.First();
            expected.Name= "NewName";
            expected.Alignment = PlanogramSequenceGroupSubGroupAlignmentType.Top;
            SavePackage();

            ReloadPackage();

            PlanogramSequenceGroupSubGroupList actualSubGroups = TestPlanogram.Sequence.Groups[0].SubGroups;
            CollectionAssert.IsNotEmpty(actualSubGroups);
            PlanogramSequenceGroupSubGroup actual = actualSubGroups[0];
            Assert.AreNotEqual(original.Name, actual.Name);
            Assert.AreNotEqual(original.Alignment, actual.Alignment);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Alignment, actual.Alignment);
        }

        [Test]
        public void DataAcces_Delete()
        {
            PlanogramSequenceGroup sequenceGroup = AddSequenceGroup(TestPlanogram);
            PlanogramSequenceGroupSubGroup expected = AddGroupSubGroup(sequenceGroup);
            SavePackage();

            //  Delete the Planogram.
            _package.Planograms.Remove(TestPlanogram);
            SavePackage();

            Assert.IsNull(FetchDtoByModel(expected));
        }

        #endregion

        #region Test Helper Methods

        private void ReloadPackage()
        {
            _package = Package.FetchById(_package.Id);
        }

        private void SavePackage()
        {
            _package = _package.Save();
        }

        private static Package CreatePackage(String name)
        {
            Package package = Package.NewPackage(1, PackageLockType.User);
            package.Name = name;
            return package;
        }

        private static Planogram AddPlanogram(Package parent)
        {
            Planogram planogram = Planogram.NewPlanogram();
            planogram.Name = String.Format("{0}.Planogram[{1}]", parent.Name, parent.Planograms.Count);
            parent.Planograms.Add(planogram);
            return planogram;
        }

        private static PlanogramSequenceGroup AddSequenceGroup(Planogram parent)
        {
            PlanogramSequenceGroupList groups = parent.Sequence.Groups;
            PlanogramSequenceGroup sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            sequenceGroup.Name = String.Format("{0}.SequenceGroup[{1}]", parent.Name, parent.Sequence.Groups.Count);
            sequenceGroup.Colour = 1;
            groups.Add(sequenceGroup);
            return sequenceGroup;
        }

        private static PlanogramSequenceGroupSubGroup AddGroupSubGroup(PlanogramSequenceGroup parent)
        {
            PlanogramSequenceGroupSubGroup subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup();
            parent.SubGroups.Add(subGroup);
            return subGroup;
        }

        private PlanogramSequenceGroupSubGroupDto FetchDtoByModel(PlanogramSequenceGroupSubGroup model)
        {
            PlanogramSequenceGroupSubGroupDto dto;
            using (IDalContext dalContext = DalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramSequenceGroupSubGroupDal>())
                {
                    dto = dal.FetchByPlanogramSequenceGroupId(model.Parent.Id).FirstOrDefault();
                }
            }
            return dto;
        }

        #endregion
    }
}
