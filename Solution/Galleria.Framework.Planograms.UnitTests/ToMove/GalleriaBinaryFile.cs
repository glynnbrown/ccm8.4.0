﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-24658 : K.Pickup
////      Initial version.
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Security.Cryptography;
//using System.Text;

//using NUnit.Framework;

//using Galleria.Framework.Planograms.Dal.Pog;
//using Gbf = Galleria.Framework.Planograms.Dal.Pog.GalleriaBinaryFile;

//namespace Galleria.Framework.Planograms.UnitTests.ToMove
//{
//    [TestFixture]
//    public class GalleriaBinaryFile
//    {
//        private const String _fileName = "Test.pog";
//        private const String _fileTypeIdentifier = "Test File Type Identifier";
//        private String _file;

//        private readonly Byte[] _aes128Key = new Byte[] { 241, 172, 158, 65, 38, 134, 79, 197, 5, 156, 243, 53, 66, 22, 190, 124, 212, 54, 97, 37, 136, 239, 152, 200, 35, 71, 95, 245, 124, 157, 81, 68 };
//        private readonly Byte[] _aesSalt = new Byte[] { 212, 254, 172, 173, 123, 175, 59, 70, 170, 69, 124, 81, 75, 35, 98, 32 };

//        private delegate void GbfAction(Gbf file);

//        private class Item : Gbf.IItem
//        {
//            private Dictionary<String, Object> _values = new Dictionary<String, Object>();

//            public void SetItemPropertyValue(String itemPropertyName, Object value)
//            {
//                _values[itemPropertyName] = value;
//            }

//            #region IItem Members

//            public Object GetItemPropertyValue(String itemPropertyName)
//            {
//                Object returnValue = null;
//                _values.TryGetValue(itemPropertyName, out returnValue);
//                return returnValue;
//            }

//            #endregion
//        }

//        public IEnumerable<Boolean> ReloadFileOptions
//        {
//            get
//            {
//                yield return false;
//                yield return true;
//            }
//        }

//        private void Test(Boolean reloadFile, GbfAction beforeSave, params GbfAction[] afterSave)
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                beforeSave(file);
//                file.Save();
//                if (!reloadFile)
//                {
//                    foreach (GbfAction action in afterSave)
//                    {
//                        action(file);
//                        file.Save();
//                    }
//                }
//            }
//            if (reloadFile)
//            {
//                foreach (GbfAction action in afterSave)
//                {
//                    using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//                    {
//                        action(file);
//                        file.Save();
//                    }
//                }
//            }
//        }

//        #region File Header-Related Tests

//        /// <summary>
//        /// Tests that the file header is written and read correctly.
//        /// </summary>
//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void BasicFileHeaderTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.AreEqual(_fileTypeIdentifier, file.TypeIdentifier);
//                    Assert.AreEqual(1, file.FormatVersion);
//                    Assert.IsTrue(file.SectionExists(1));
//                });
//        }

//        /// <summary>
//        /// Tests that the file header is read in correctly even when it contains extra empty space.
//        /// </summary>
//        [Test]
//        public void FileHeaderWithEmptySpaceTest()
//        {
//            //using (FileStream stream = new FileStream(_file, FileMode.Create))
//            //{
//            //    using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(stream))
//            //    {
//            //        writer.Write((UInt32)100);
//            //        writer.Write(_fileTypeIdentifier);
//            //        writer.Write((Byte)1);
//            //        writer.Write("Test");
//            //        stream.Position = 100;
//            //        writer.Write((UInt32)100);
//            //        writer.Write((UInt16)1);
//            //        writer.Write((UInt32)0);
//            //        writer.Write((Byte)0);
//            //    }
//            //}
//            //using (Gbf file = new Gbf(_file, _fileTypeIdentifier))
//            //{
//            //    Assert.AreEqual(_fileTypeIdentifier, file.TypeIdentifier);
//            //    Assert.AreEqual(1, file.FormatVersion);
//            //    Assert.IsTrue(file.SectionExists(1));
//            //}
//        }

//        #endregion

//        #region Section-Related Tests

//        #region Header-Related Tests

//        /// <summary>
//        /// Tests that the section header is written and read correctly.
//        /// </summary>
//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void BasicSectionHeaderTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    // Simple unencrypted section
//                    file.CreateSection(1);
//                    // Add content so that Content Size can be tested.
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);

//                    // Simple encrypted section (header contains extra info)
//                    file.CreateSection(2);
//                    file.EnableAes128Encryption(2);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Byte, null);

//                    // Keyed unencrypted section
//                    file.CreateSection(3, Gbf.ItemPropertyType.String);
//                    file.AddItemProperty(3, "Test3", Gbf.ItemPropertyType.Byte, null);
//                    // Add items so that start positions of item groups can be tested.
//                    Item item = new Item();
//                    item.SetItemPropertyValue("Test3", (Byte)1);
//                    file.SetItems(3, "Hello", new List<Gbf.IItem> { item });
//                    item.SetItemPropertyValue("Test3", (Byte)2);
//                    file.SetItems(3, "World", new List<Gbf.IItem> { item });

//                    // Keyed encrypted section
//                    file.CreateSection(4, Gbf.ItemPropertyType.Int32);
//                    file.EnableAes128Encryption(4);
//                    file.AddItemProperty(4, "Test4", Gbf.ItemPropertyType.Byte, null);
//                    item = new Item();
//                    item.SetItemPropertyValue("Test4", (Byte)3);
//                    file.SetItems(4, 1, new List<Gbf.IItem> { item });
//                    item.SetItemPropertyValue("Test4", (Byte)4);
//                    file.SetItems(4, 2, new List<Gbf.IItem> { item });

//                    // Another section, so that we can test Header Size and Content Size of the previous section.
//                    file.CreateSection(5);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsTrue(file.SectionExists(1));
//                    // If we can find the content, then Header Size must be correct:
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test1"));

//                    // If we can find the next section, then Content Size must be correct:
//                    Assert.IsTrue(file.SectionExists(2));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test2"));

//                    Assert.IsTrue(file.SectionExists(3));
//                    Assert.IsTrue(file.ItemPropertyExists(3, "Test3"));
//                    // If we can find the item groups, the item group start positions must be right:
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(3, "Hello"));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(1, items[0].GetItemPropertyValue("Test3"));
//                    items = new List<Gbf.IItem>(file.GetItems(3, "World"));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(2, items[0].GetItemPropertyValue("Test3"));
//                    items = new List<Gbf.IItem>(file.GetItems(4, 1));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(3, items[0].GetItemPropertyValue("Test4"));
//                    items = new List<Gbf.IItem>(file.GetItems(4, 2));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(4, items[0].GetItemPropertyValue("Test4"));

//                    Assert.IsTrue(file.SectionExists(5));
//                });
//        }

//        /// <summary>
//        /// Tests that the section header is read in correctly even when it contains extra empty space.
//        /// </summary>
//        [Test]
//        public void SectionHeaderWithEmptySpaceTest()
//        {
//            // TODO
//        }

//        #endregion

//        #region Content-Related Tests

//        #region Schema-Related Tests

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void SectionExistsAndCreateSectionTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(2);
//                    file.CreateSection(3);
//                    file.CreateSection(4, Gbf.ItemPropertyType.String);
//                    file.CreateSection(5, Gbf.ItemPropertyType.String);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsFalse(file.SectionExists(1));
//                    Assert.IsTrue(file.SectionExists(2));
//                    Assert.IsTrue(file.SectionExists(3));
//                    Assert.IsTrue(file.SectionExists(4));
//                    Assert.IsTrue(file.SectionExists(5));

//                    file.RemoveSection(3);
//                    file.RemoveSection(5);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsFalse(file.SectionExists(1));
//                    Assert.IsTrue(file.SectionExists(2));
//                    Assert.IsFalse(file.SectionExists(3));
//                    Assert.IsTrue(file.SectionExists(4));
//                    Assert.IsFalse(file.SectionExists(5));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void RemoveSectionTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2, Gbf.ItemPropertyType.String);
//                    file.RemoveSection(1);
//                    file.RemoveSection(2);
//                    Assert.IsFalse(file.SectionExists(1));
//                    file.CreateSection(1);
//                    file.CreateSection(2, Gbf.ItemPropertyType.String);
//                },
//                delegate(Gbf file)
//                {
//                    file.RemoveSection(1);
//                    file.RemoveSection(2);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsFalse(file.SectionExists(1));
//                    Assert.IsFalse(file.SectionExists(2));
//                });
//        }

//        #endregion

//        #region Item-Related Tests

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void ItemPropertyExistsTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2, Gbf.ItemPropertyType.String);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Boolean, null);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Boolean, null);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test2"));
//                    Assert.IsFalse(file.ItemPropertyExists(1, "Test2"));
//                    Assert.IsFalse(file.ItemPropertyExists(2, "Test1"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void GetItemPropertyTypeTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2, Gbf.ItemPropertyType.String);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Boolean, null);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Byte, null);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.AreEqual(Gbf.ItemPropertyType.Boolean, file.GetItemPropertyType(1, "Test1"));
//                    Assert.AreEqual(Gbf.ItemPropertyType.Byte, file.GetItemPropertyType(2, "Test2"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void AddItemPropertyTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    Item item1 = new Item();
//                    item1.SetItemPropertyValue("Test1", (Byte)1);
//                    Item item2 = new Item();
//                    item2.SetItemPropertyValue("Test1", (Byte)2);
//                    file.SetItems(1, new List<Gbf.IItem> { item1, item2 });
//                },
//                delegate(Gbf file)
//                {
//                    file.AddItemProperty(1, "Test2", Gbf.ItemPropertyType.Byte, (Byte)3);
//                    file.AddItemProperty(2, "Test3", Gbf.ItemPropertyType.Boolean, null);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.AreEqual(Gbf.ItemPropertyType.Byte, file.GetItemPropertyType(1, "Test1"));
//                    Assert.AreEqual(Gbf.ItemPropertyType.Byte, file.GetItemPropertyType(1, "Test2"));
//                    Assert.AreEqual(Gbf.ItemPropertyType.Boolean, file.GetItemPropertyType(2, "Test3"));
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual(1, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(2, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(3, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(3, items[1].GetItemPropertyValue("Test2"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void RemoveItemPropertyTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(1, "Test2", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Boolean, null);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Boolean, null);
//                    Item item1 = new Item();
//                    item1.SetItemPropertyValue("Test1", (Byte)1);
//                    item1.SetItemPropertyValue("Test2", (Byte)2);
//                    Item item2 = new Item();
//                    item2.SetItemPropertyValue("Test1", (Byte)3);
//                    item2.SetItemPropertyValue("Test2", (Byte)4);
//                    file.SetItems(1, new List<Gbf.IItem> { item1, item2 });

//                    file.RemoveItemProperty(2, "Test2");
//                },
//                delegate(Gbf file)
//                {
//                    file.RemoveItemProperty(1, "Test1");
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsFalse(file.ItemPropertyExists(1, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test2"));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test1"));
//                    Assert.IsFalse(file.ItemPropertyExists(2, "Test2"));
//                    Assert.AreEqual(Gbf.ItemPropertyType.Byte, file.GetItemPropertyType(1, "Test2"));
//                    Assert.AreEqual(Gbf.ItemPropertyType.Boolean, file.GetItemPropertyType(2, "Test1"));
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual(2, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(4, items[1].GetItemPropertyValue("Test2"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void RenameItemPropertyTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(1, "Test2", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Boolean, null);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Boolean, null);

//                    file.RenameItemProperty(1, "Test1", "Test3");
//                },
//                delegate(Gbf file)
//                {
//                    file.RenameItemProperty(2, "Test2", "Test4");
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsFalse(file.ItemPropertyExists(1, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test2"));
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test3"));
//                    Assert.IsFalse(file.ItemPropertyExists(1, "Test4"));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test1"));
//                    Assert.IsFalse(file.ItemPropertyExists(2, "Test2"));
//                    Assert.IsFalse(file.ItemPropertyExists(2, "Test3"));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test4"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void GetItemsTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2);
//                    file.CreateSection(3);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(1, "Test2", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Boolean, null);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Boolean, null);
//                    Item item1 = new Item();
//                    item1.SetItemPropertyValue("Test1", (Byte)1);
//                    item1.SetItemPropertyValue("Test2", (Byte)2);
//                    Item item2 = new Item();
//                    item2.SetItemPropertyValue("Test1", (Byte)3);
//                    item2.SetItemPropertyValue("Test2", (Byte)4);
//                    Item item3 = new Item();
//                    item3.SetItemPropertyValue("Test1", true);
//                    item3.SetItemPropertyValue("Test2", false);
//                    Item item4 = new Item();
//                    item4.SetItemPropertyValue("Test1", false);
//                    item4.SetItemPropertyValue("Test2", true);
//                    file.SetItems(1, new List<Gbf.IItem> { item1, item2 });
//                    file.SetItems(2, new List<Gbf.IItem> { item3, item4 });

//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual((Byte)1, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)2, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual((Byte)3, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)4, items[1].GetItemPropertyValue("Test2"));
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(true, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(false, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(false, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(true, items[1].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(0, file.GetItems(3).Count());
//                },
//                delegate(Gbf file)
//                {
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual((Byte)1, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)2, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual((Byte)3, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)4, items[1].GetItemPropertyValue("Test2"));
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(true, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(false, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(false, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(true, items[1].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(0, file.GetItems(3).Count());
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void SetItemsTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(1, "Test2", Gbf.ItemPropertyType.Byte, null);
//                    file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Boolean, null);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Boolean, null);
//                    Item item1 = new Item();
//                    item1.SetItemPropertyValue("Test1", (Byte)5);
//                    item1.SetItemPropertyValue("Test2", (Byte)6);
//                    Item item2 = new Item();
//                    item2.SetItemPropertyValue("Test1", (Byte)3);
//                    item2.SetItemPropertyValue("Test2", (Byte)4);
//                    Item item3 = new Item();
//                    item3.SetItemPropertyValue("Test1", true);
//                    item3.SetItemPropertyValue("Test2", false);
//                    Item item4 = new Item();
//                    item4.SetItemPropertyValue("Test1", false);
//                    item4.SetItemPropertyValue("Test2", true);
//                    file.SetItems(1, new List<Gbf.IItem> { item1, item2 });
//                    file.SetItems(2, new List<Gbf.IItem> { item3, item4 });
//                    item1.SetItemPropertyValue("Test1", (Byte)1);
//                    item1.SetItemPropertyValue("Test2", (Byte)2);
//                    file.SetItems(1, new List<Gbf.IItem> { item1, item2 });

//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual((Byte)1, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)2, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual((Byte)3, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)4, items[1].GetItemPropertyValue("Test2"));
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(true, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(false, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(false, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(true, items[1].GetItemPropertyValue("Test2"));
//                },
//                delegate(Gbf file)
//                {
//                    Item item3 = new Item();
//                    item3.SetItemPropertyValue("Test1", true);
//                    item3.SetItemPropertyValue("Test2", false);
//                    Item item4 = new Item();
//                    item4.SetItemPropertyValue("Test1", false);
//                    item4.SetItemPropertyValue("Test2", true);
//                    file.SetItems(2, new List<Gbf.IItem> { item4, item3 });

//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual((Byte)1, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)2, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual((Byte)3, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)4, items[1].GetItemPropertyValue("Test2"));
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(false, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(true, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(true, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(false, items[1].GetItemPropertyValue("Test2"));
//                },
//                delegate(Gbf file)
//                {
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual((Byte)1, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)2, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual((Byte)3, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual((Byte)4, items[1].GetItemPropertyValue("Test2"));
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(false, items[0].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(true, items[0].GetItemPropertyValue("Test2"));
//                    Assert.AreEqual(true, items[1].GetItemPropertyValue("Test1"));
//                    Assert.AreEqual(false, items[1].GetItemPropertyValue("Test2"));
//                });
//        }

//        #endregion

//        #region Encryption-Related Tests

//        [Test]
//        public void EnableAes128EncryptionTest()
//        {
//            // Encryption is on from the outset.
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                Item item = new Item();
//                item.SetItemPropertyValue("Test1", (Byte)1);
//                file.SetItems(1, new List<Gbf.IItem> { item });
//                file.EnableAes128Encryption(1);
//                file.CreateSection(2);
//                file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Byte, null);
//                file.SetItems(2, new List<Gbf.IItem> { item });

//                file.Save();
//            }
//            Assert.AreEqual(GetSampleFileBytes(_aesSalt), File.ReadAllBytes(_file));

//            File.Delete(_file);

//            // Encryption is enabled on an existing file.
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                Item item = new Item();
//                item.SetItemPropertyValue("Test1", (Byte)1);
//                file.SetItems(1, new List<Gbf.IItem> { item });
//                file.CreateSection(2);
//                file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Byte, null);
//                file.SetItems(2, new List<Gbf.IItem> { item });

//                file.Save();
//            }
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.EnableAes128Encryption(1);
//                file.Save();
//            }
//            Assert.AreEqual(GetSampleFileBytes(_aesSalt), File.ReadAllBytes(_file));
//        }

//        [Test]
//        public void DisableEncryptionTest()
//        {
//            // Encryption is on and off again from the outset.
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                Item item = new Item();
//                item.SetItemPropertyValue("Test1", (Byte)1);
//                file.SetItems(1, new List<Gbf.IItem> { item });
//                file.EnableAes128Encryption(1);
//                file.DisableEncryption(1);
//                file.CreateSection(2);
//                file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Byte, null);
//                file.SetItems(2, new List<Gbf.IItem> { item });

//                file.Save();
//            }
//            Assert.AreEqual(GetSampleFileBytes(null), File.ReadAllBytes(_file));

//            File.Delete(_file);

//            // Encryption is enabled on an existing file.
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                Item item = new Item();
//                item.SetItemPropertyValue("Test1", (Byte)1);
//                file.SetItems(1, new List<Gbf.IItem> { item });
//                file.EnableAes128Encryption(1);
//                file.CreateSection(2);
//                file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Byte, null);
//                file.SetItems(2, new List<Gbf.IItem> { item });

//                file.Save();
//            }
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.DisableEncryption(1);
//                file.Save();
//            }
//            Assert.AreEqual(GetSampleFileBytes(null), File.ReadAllBytes(_file));
//        }

//        private Byte[] GetSampleFileBytes(Byte[] salt)
//        {
//            Byte[] returnValue = null;

//            using (MemoryStream memoryStream = new MemoryStream())
//            {
//                using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(memoryStream))
//                {
//                    writer.Write((UInt32)0);
//                    writer.Write(_fileTypeIdentifier);
//                    writer.Write((Byte)1);
//                    Int64 sectionStart = memoryStream.Position;
//                    memoryStream.Position = 0;
//                    writer.Write((UInt32)sectionStart);
//                    writer.BaseStream.Position = sectionStart;
//                    WriteSampleSection(writer, 1, salt);
//                    WriteSampleSection(writer, 2, null);
//                    writer.Flush();

//                    returnValue = memoryStream.ToArray();
//                }
//            }
//            return returnValue;
//        }

//        private void WriteSampleSection(ExtendedBinaryWriter writer, UInt16 sectionType, Byte[] salt)
//        {
//            Int64 sectionStart = writer.BaseStream.Position;
//            writer.Write((UInt32)0);
//            writer.Write(sectionType);
//            writer.Write((UInt32)0);
//            if (salt != null)
//            {
//                writer.Write((Byte)1);
//                writer.WriteByteArrayAsSingleValue(salt);
//            }
//            else
//            {
//                writer.Write((Byte)0);
//            }
//            Int64 contentStart = writer.BaseStream.Position;
//            writer.BaseStream.Position = sectionStart;
//            writer.Write((UInt32)(contentStart - sectionStart));
//            writer.BaseStream.Position = contentStart;
//            if (salt != null)
//            {
//                writer.Write(Encrypt(salt, GetSampleSectionContentBytes()));
//            }
//            else
//            {
//                writer.Write(GetSampleSectionContentBytes());
//            }
//            Int64 contentEnd = writer.BaseStream.Position;
//            writer.BaseStream.Position = sectionStart + 6;
//            writer.Write((UInt32)(contentEnd - contentStart));
//            writer.BaseStream.Position = contentEnd;
//        }

//        private Byte[] GetSampleSectionContentBytes()
//        {
//            Byte[] returnValue = null;
//            using (MemoryStream memoryStream = new MemoryStream())
//            {
//                using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(memoryStream))
//                {
//                    writer.Write((Byte)1);
//                    writer.Write((Byte)1);
//                    writer.Write("Test1");
//                    writer.Write((Byte)Gbf.ItemPropertyType.Byte);
//                    writer.Write((UInt32)1);
//                    writer.Write((Byte)1);
//                    writer.Write((Byte)1);
//                    writer.Write((Byte)1);
//                    writer.Flush();
//                    returnValue = memoryStream.ToArray();
//                }
//            }
//            return returnValue;
//        }

//        private Byte[] Encrypt(Byte[] salt, Byte[] content)
//        {
//            Byte[] returnValue = null;
//            Byte[] encryptedContent = null;
//            using (RijndaelManaged rijndael = new RijndaelManaged())
//            {
//                rijndael.Padding = PaddingMode.PKCS7;
//                ICryptoTransform encryptor = rijndael.CreateEncryptor(_aes128Key, salt);
//                using (MemoryStream memoryStream = new MemoryStream())
//                {
//                    using (CryptoStream cryptoStream =
//                        new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
//                    {
//                        using (ExtendedBinaryWriter encryptedWriter = new ExtendedBinaryWriter(cryptoStream))
//                        {
//                            Byte[] hash = SHA256Managed.Create().ComputeHash(content);
//                            encryptedWriter.WriteByteArrayAsSingleValue(hash);
//                            encryptedWriter.Write(content);
//                            encryptedWriter.Flush();
//                            cryptoStream.FlushFinalBlock();
//                            encryptedContent = memoryStream.ToArray();
//                        }
//                    }
//                }
//            }
//            using (MemoryStream memoryStream = new MemoryStream())
//            {
//                using (ExtendedBinaryWriter writer = new ExtendedBinaryWriter(memoryStream))
//                {
//                    writer.WriteByteArrayAsSingleValue(encryptedContent);
//                    writer.Flush();
//                    returnValue = memoryStream.ToArray();
//                }
//            }
//            return returnValue;
//        }

//        #endregion

//        #endregion

//        #endregion

//        #region File Rewrite-Related Tests

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void UnchangedSectionMustMove(Boolean reloadFile)
//        {
//            Int64 fileSizeBeforeExpansion = 0;
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);

//                    file.CreateSection(2);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Byte, null);
//                    Item item = new Item();
//                    item.SetItemPropertyValue("Test2", (Byte)2);
//                    file.SetItems(2, new List<Item> { item });
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    fileSizeBeforeExpansion = fileInfo.Length;

//                    List<Gbf.IItem> items = new List<Gbf.IItem>();
//                    // Create more than enough items to fill the 1 KB of empty space that will have been added to 
//                    // section 1's content.
//                    for (Int32 i = 0; i < 350; i++)
//                    {
//                        Item item = new Item();
//                        item.SetItemPropertyValue("Test1", (Byte)1);
//                        items.Add(item);
//                    }
//                    file.SetItems(1, items);
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    Assert.AreNotEqual(fileSizeBeforeExpansion, fileInfo.Length);

//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual(350, items.Count);
//                    foreach (Gbf.IItem item in items)
//                    {
//                        Assert.AreEqual(1, item.GetItemPropertyValue("Test1"));
//                    }
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(2, items[0].GetItemPropertyValue("Test2"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void ChangedSectionCanUseEmptySpace(Boolean reloadFile)
//        {
//            Int64 fileSizeBeforeExpansion = 0;
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);

//                    file.CreateSection(2);
//                    file.AddItemProperty(2, "Test2", Gbf.ItemPropertyType.Byte, null);
//                    Item item = new Item();
//                    item.SetItemPropertyValue("Test2", (Byte)2);
//                    file.SetItems(2, new List<Item> { item });
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    fileSizeBeforeExpansion = fileInfo.Length;

//                    List<Gbf.IItem> items = new List<Gbf.IItem>();
//                    // Create less than enough items to fill the 1 KB of empty space that will have been added to 
//                    // section 1's content.
//                    for (Int32 i = 0; i < 300; i++)
//                    {
//                        Item item = new Item();
//                        item.SetItemPropertyValue("Test1", (Byte)1);
//                        items.Add(item);
//                    }
//                    file.SetItems(1, items);
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    Assert.AreEqual(fileSizeBeforeExpansion, fileInfo.Length);

//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual(300, items.Count);
//                    foreach (Gbf.IItem item in items)
//                    {
//                        Assert.AreEqual(1, item.GetItemPropertyValue("Test1"));
//                    }
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(2, items[0].GetItemPropertyValue("Test2"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void RemovedSectionsMustStay(Boolean reloadFile)
//        {
//            Int64 fileSizeBeforeRemoval = 0;
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.CreateSection(2);
//                    file.CreateSection(3);
//                    file.AddItemProperty(3, "Test1", Gbf.ItemPropertyType.Byte, null);
//                },
//                delegate(Gbf file)
//                {
//                    Assert.IsTrue(file.SectionExists(1));
//                    Assert.IsTrue(file.SectionExists(2));

//                    FileInfo fileInfo = new FileInfo(_file);
//                    fileSizeBeforeRemoval = fileInfo.Length;

//                    file.RemoveSection(1);
//                    file.RemoveSection(2);
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    Assert.AreEqual(fileSizeBeforeRemoval, fileInfo.Length);

//                    Assert.IsFalse(file.SectionExists(1));
//                    Assert.IsFalse(file.SectionExists(2));
//                    Assert.IsTrue(file.SectionExists(3));
//                    Assert.IsTrue(file.ItemPropertyExists(3, "Test1"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void InterestingRearrangement1(Boolean reloadFile)
//        {
//            // A section has been removed, there's more empty space available than the remaining sections want
//            Int64 fileSizeBeforeRearrange = 0;
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(2);
//                    file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(3);
//                    file.AddItemProperty(3, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(4);
//                    file.AddItemProperty(4, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(5);
//                    file.AddItemProperty(5, "Test1", Gbf.ItemPropertyType.Byte, null);
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    fileSizeBeforeRearrange = fileInfo.Length;

//                    Item item = new Item();
//                    item.SetItemPropertyValue("Test1", (Byte)1);
//                    file.SetItems(1, new List<Gbf.IItem> { item });
//                    file.RenameItemProperty(2, "Test1", "Test2");
//                    file.RemoveSection(3);
//                    file.RenameItemProperty(4, "Test1", "Test2");
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    Assert.AreEqual(fileSizeBeforeRearrange, fileInfo.Length);

//                    Assert.IsTrue(file.SectionExists(1));
//                    Assert.IsTrue(file.SectionExists(2));
//                    Assert.IsFalse(file.SectionExists(3));
//                    Assert.IsTrue(file.SectionExists(4));
//                    Assert.IsTrue(file.SectionExists(5));
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test2"));
//                    Assert.IsTrue(file.ItemPropertyExists(4, "Test2"));
//                    Assert.IsTrue(file.ItemPropertyExists(5, "Test1"));
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(1, items[0].GetItemPropertyValue("Test1"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void InterestingRearrangement2(Boolean reloadFile)
//        {
//            // There's not quite enough empty space available to go around
//            Int64 fileSizeBeforeRearrange = 0;
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(2);
//                    file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(3);
//                    file.AddItemProperty(3, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(4);
//                    file.AddItemProperty(4, "Test1", Gbf.ItemPropertyType.Byte, null);
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    fileSizeBeforeRearrange = fileInfo.Length;

//                    List<Gbf.IItem> items = new List<Gbf.IItem>();
//                    for (Int32 i = 0; i < 171; i++)
//                    {
//                        Item item = new Item();
//                        item.SetItemPropertyValue("Test1", (Byte)1);
//                        items.Add(item);
//                    }
//                    file.SetItems(1, items);
//                    file.SetItems(2, items);
//                    file.SetItems(3, items);
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    Assert.AreEqual(fileSizeBeforeRearrange, fileInfo.Length);

//                    Assert.IsTrue(file.SectionExists(1));
//                    Assert.IsTrue(file.SectionExists(2));
//                    Assert.IsTrue(file.SectionExists(3));
//                    Assert.IsTrue(file.SectionExists(4));
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(3, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(4, "Test1"));
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual(171, items.Count);
//                    foreach (Gbf.IItem item in items)
//                    {
//                        Assert.AreEqual(1, item.GetItemPropertyValue("Test1"));
//                    }
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(171, items.Count);
//                    foreach (Gbf.IItem item in items)
//                    {
//                        Assert.AreEqual(1, item.GetItemPropertyValue("Test1"));
//                    }
//                    items = new List<Gbf.IItem>(file.GetItems(3));
//                    Assert.AreEqual(171, items.Count);
//                    foreach (Gbf.IItem item in items)
//                    {
//                        Assert.AreEqual(1, item.GetItemPropertyValue("Test1"));
//                    }
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void InterestingRearrangement3(Boolean reloadFile)
//        {
//            // There's not quite enough empty space available to go around
//            Int64 fileSizeBeforeRearrange = 0;
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(2);
//                    file.AddItemProperty(2, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(3);
//                    file.AddItemProperty(3, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(4);
//                    file.AddItemProperty(4, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(5);
//                    file.AddItemProperty(5, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(6);
//                    file.AddItemProperty(6, "Test1", Gbf.ItemPropertyType.Byte, null);
//                    file.CreateSection(7);
//                    file.AddItemProperty(7, "Test1", Gbf.ItemPropertyType.Byte, null);
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    fileSizeBeforeRearrange = fileInfo.Length;

//                    Item item = new Item();
//                    item.SetItemPropertyValue("Test1", (Byte)1);
//                    file.SetItems(1, new List<Gbf.IItem> { item });
//                    file.SetItems(2, new List<Gbf.IItem> { item });
//                    file.SetItems(4, new List<Gbf.IItem> { item });
//                    file.SetItems(5, new List<Gbf.IItem> { item });
//                    file.RenameItemProperty(7, "Test1", "Test2");
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    Assert.AreEqual(fileSizeBeforeRearrange, fileInfo.Length);

//                    Assert.IsTrue(file.SectionExists(1));
//                    Assert.IsTrue(file.SectionExists(2));
//                    Assert.IsTrue(file.SectionExists(3));
//                    Assert.IsTrue(file.SectionExists(4));
//                    Assert.IsTrue(file.SectionExists(5));
//                    Assert.IsTrue(file.SectionExists(6));
//                    Assert.IsTrue(file.SectionExists(7));
//                    Assert.IsTrue(file.ItemPropertyExists(1, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(2, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(3, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(4, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(5, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(6, "Test1"));
//                    Assert.IsTrue(file.ItemPropertyExists(7, "Test2"));
//                    List<Gbf.IItem> items = new List<Gbf.IItem>(file.GetItems(1));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(1, items[0].GetItemPropertyValue("Test1"));
//                    items = new List<Gbf.IItem>(file.GetItems(2));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(1, items[0].GetItemPropertyValue("Test1"));
//                    items = new List<Gbf.IItem>(file.GetItems(4));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(1, items[0].GetItemPropertyValue("Test1"));
//                    items = new List<Gbf.IItem>(file.GetItems(5));
//                    Assert.AreEqual(1, items.Count);
//                    Assert.AreEqual(1, items[0].GetItemPropertyValue("Test1"));
//                });
//        }

//        [Test, TestCaseSource("ReloadFileOptions")]
//        public void MaximumEmptySpaceTest(Boolean reloadFile)
//        {
//            Test(
//                reloadFile,
//                delegate(Gbf file)
//                {
//                    file.CreateSection(1);
//                    file.AddItemProperty(1, "Test1", Gbf.ItemPropertyType.ByteArray, null);
//                    Item item = new Item();
//                    item.SetItemPropertyValue("Test1", new Byte[2097152]);
//                    file.SetItems(1, new List<Gbf.IItem> { item });
//                },
//                delegate(Gbf file)
//                {
//                    FileInfo fileInfo = new FileInfo(_file);
//                    Assert.Greater(4194304, fileInfo.Length);
//                });
//        }

//        #endregion

//        #region Exception-Related Tests

//        [Test]
//        [ExpectedException(typeof(ArgumentException))]
//        public void SectionExistsRemovedSectionTypeTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.Save();
//                file.RemoveSection(1);
//                file.SectionExists(0);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentException))]
//        public void CreateSectionRemovedSectionTypeTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(0);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(InvalidOperationException))]
//        public void CreateSectionAlreadyExistsTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.CreateSection(1);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentException))]
//        public void RemoveSectionRemovedSectionTypeTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.Save();
//                file.RemoveSection(1);
//                file.RemoveSection(0);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentException))]
//        public void EnableAes128EncryptionRemovedSectionTypeTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.Save();
//                file.RemoveSection(1);
//                file.EnableAes128Encryption(0);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(InvalidOperationException))]
//        public void EnableAes128EncryptionSectionDoesNotExistTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.EnableAes128Encryption(1);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentNullException))]
//        public void EnableAes128EncryptionSaltNullTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.EnableAes128Encryption(1);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentException))]
//        public void DisableEncryptionRemovedSectionTypeTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.Save();
//                file.RemoveSection(1);
//                file.DisableEncryption(0);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(InvalidOperationException))]
//        public void DisableEncryptionSectionDoesNotExistTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.DisableEncryption(1);
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentException))]
//        public void ItemPropertyExistsRemovedSectionTypeTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.Save();
//                file.RemoveSection(1);
//                file.ItemPropertyExists(0, "Test");
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(InvalidOperationException))]
//        public void ItemPropertyExistsSectionDoesNotExistTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.ItemPropertyExists(1, "Test");
//            }
//        }

//        [Test]
//        [ExpectedException(typeof(ArgumentNullException))]
//        public void ItemPropertyExistsNullItemPropertyNameTest()
//        {
//            using (Gbf file = new Gbf(_file, _fileTypeIdentifier, _aes128Key))
//            {
//                file.CreateSection(1);
//                file.ItemPropertyExists(1, null);
//            }
//        }

//        #endregion

//        #region SetUp/TearDown

//        [SetUp]
//        public void SetUp()
//        {
//            String templateDir = Path.Combine(
//                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
//                "Customer Centric Merchandising",
//                "Test Files");
//            if (!Directory.Exists(templateDir))
//            {
//                // Directory doesn't exist yet: create it.
//                Directory.CreateDirectory(templateDir);
//            }
//            else
//            {
//                _file = Path.Combine(templateDir, _fileName);
//                if (File.Exists(_file))
//                {
//                    File.Delete(_file);
//                }
//            }
//        }

//        [TearDown]
//        public void TearDown()
//        {
//            if (File.Exists(_file))
//            {
//                File.Delete(_file);
//            }
//        }

//        #endregion
//    }
//}
