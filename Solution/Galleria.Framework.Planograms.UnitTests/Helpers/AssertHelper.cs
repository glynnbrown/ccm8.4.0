﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26789 : A.Kuszyk
//  Created.
// V8-27773 : A.Kuszyk
//  Added AreSinglesEqual method.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using NUnit.Framework;
using Galleria.Framework.Helpers;
using Galleria.Framework.DataStructures;

namespace Galleria.Framework.Planograms.UnitTests.Helpers
{
    public static class AssertHelper
    {
        public static void AreRectValuesEquivalent(IEnumerable<RectValue> expected, IEnumerable<RectValue> actual, Int32 precision = 3)
        {
            Assert.AreEqual(expected.Count(), actual.Count());

            List<RectValue> actuals = actual.ToList();

            foreach (RectValue expectedRectValue in expected)
            {
                RectValue? match = null;

                foreach (RectValue actualRectValue in actuals)
                {
                    Boolean isMatch =
                        actualRectValue.X.EqualTo(expectedRectValue.X, precision) &&
                        actualRectValue.Y.EqualTo(expectedRectValue.Y, precision) &&
                        actualRectValue.Z.EqualTo(expectedRectValue.Z, precision) &&
                        actualRectValue.Width.EqualTo(expectedRectValue.Width, precision) &&
                        actualRectValue.Height.EqualTo(expectedRectValue.Height, precision) &&
                        actualRectValue.Depth.EqualTo(expectedRectValue.Depth, precision);
                    if (isMatch)
                    {
                        match = actualRectValue;
                        break;
                    }
                }

                if (!match.HasValue)
                {
                    Assert.Fail("Could not match RectValue");
                }
                else
                {
                    actuals.Remove(match.Value);
                }
            }
        }

        public static void AreRectValuesEqual(RectValue expected, RectValue actual, Int32 precision = 3)
        {
            String message = String.Format("Expected [X: {0}, Y: {1}, Z: {2}, W: {3}, H: {4}, D: {5}] {6}    Actual [X: {7}, Y: {8}, Z: {9}, W: {10}, H: {11}, D: {12}]",
                expected.X,
                expected.Y,
                expected.Z,
                expected.Width,
                expected.Height,
                expected.Depth,
                Environment.NewLine,
                actual.X,
                actual.Y,
                actual.Z,
                actual.Width,
                actual.Height,
                actual.Depth);

            Assert.That(expected.X.EqualTo(actual.X, precision), message);
            Assert.That(expected.Y.EqualTo(actual.Y, precision), message);
            Assert.That(expected.Z.EqualTo(actual.Z, precision), message);
            Assert.That(expected.Width.EqualTo(actual.Width, precision), message);
            Assert.That(expected.Height.EqualTo(actual.Height, precision), message);
            Assert.That(expected.Depth.EqualTo(actual.Depth, precision), message);
        }

        public static void AreRotationValuesEqual(RotationValue expected, RotationValue actual, String message = null)
        {
            Boolean areEqual = 
                expected.Slope.EqualTo(actual.Slope) &&
                expected.Angle.EqualTo(actual.Angle) &&
                expected.Roll.EqualTo(actual.Roll);
            if(!areEqual)
            {
                Assert.Fail(String.Format(
                    "Expected [S: {0} A: {1} R: {2}] but was [S: {3} A: {4} R: {5}]. {6}",
                    expected.Slope,
                    expected.Angle,
                    expected.Roll,
                    actual.Slope,
                    actual.Angle,
                    actual.Roll,
                    message ?? String.Empty));
            }
        }

        public static void AreSinglesEquivalent(IEnumerable<Single> expected, IEnumerable<Single> actual, Int32 decimalPlaces)
        {
            Assert.AreEqual(expected.Count(), actual.Count());
            var roundedActuals = actual.Select(a => Convert.ToSingle(Math.Round(a, decimalPlaces))).ToList();
            foreach (var expectedItem in expected)
            {
                CollectionAssert.Contains(roundedActuals, Convert.ToSingle(Math.Round(expectedItem, decimalPlaces)));
            }
        }

        public static void AreSinglesEqual(Single expected, Single actual, String message = null)
        {
            if (!expected.EqualTo(actual))
            {
                if (message == null)
                {
                    Assert.Fail("Expected {0}, but was {1}", expected, actual);
                }
                else
                {
                    Assert.Fail("Expected {0}, but was {1}. {2}", expected, actual, message);
                }
            }
        }

        /// <summary>
        /// Asserts that two objects that implement the interface T are equal by iterating over the
        /// interfaces properties only. Any properties that are IEnumerable just have their counts compared.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <param name="excludedProperties"></param>
        public static void AreEqual<T>(T expected, T actual, params String[] excludedProperties)
        {
            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                if (excludedProperties.Contains(property.Name)) continue;

                var expectedValue = property.GetValue(expected, null);
                var actualValue = property.GetValue(actual, null);
                if (expectedValue == null && actualValue == null) continue;
                if (expectedValue == null || actualValue == null)
                {
                    throw new AssertionException(String.Format(
                            "Expected {0}, but was {1}, for property {2}",
                            expectedValue,
                            actualValue,
                            property.Name));
                }
                else if (expectedValue.GetType().Namespace.StartsWith("System"))
                {
                    if (!expectedValue.Equals(actualValue))
                    {
                        throw new AssertionException(String.Format(
                            "Expected {0}, but was {1}, for property {2}",
                            expectedValue,
                            actualValue, 
                            property.Name));
                    }
                }
                else if (expectedValue is IEnumerable)
                {
                    var expectedValueList = ((IEnumerable)expectedValue).GetEnumerator();
                    var actualValueList = ((IEnumerable)actualValue).GetEnumerator();
                    Int32 expectedCount = 0;
                    while (expectedValueList.MoveNext()) { expectedCount++; }
                    Int32 actualCount = 0;
                    while (actualValueList.MoveNext()) { actualCount++; }
                    Assert.AreEqual(expectedCount, actualCount, property.Name);
                    continue;
                }
                else
                {
                    AreEqual(expectedValue, actualValue);
                }
            }
        }
    }
}
