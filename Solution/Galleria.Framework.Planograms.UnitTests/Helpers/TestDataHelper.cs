﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// K.Pickup
//  Created.
// V8-26986 : A.Kuszyk
//  Added Get Blocking methods.
// V8-27058 : A.Probyn
//  Added IsPackageValid to help with unit test creation.
// V8-27926 : A.Silva.
//      Added CreateTestPosition.

#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
#endregion

#region Version History: CCM830

// V8-31819 : A.Silva
//  Added Planogram Comparison IsValid check to IsPackageValid.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using System.Diagnostics;
using System.Reflection;

namespace Galleria.Framework.Planograms.UnitTests.Helpers
{
    public static class TestDataHelper
    {

        #region General

        /// <summary>
        /// Populates the properties on the given model with values
        /// </summary>
        /// <param name="model"></param>
        public static void SetPropertyValues(Object model, Boolean createChildren = true)
        {
            SetPropertyValues(model, new List<String>(), createChildren);
        }
        public static void SetPropertyValues(Object model, List<String> excludeProperties, Boolean createChildren = true)
        {
            PropertyInfo[] properties = model.GetType().GetProperties();
            foreach (PropertyInfo pInfo in properties)
            {
                if (!excludeProperties.Contains(pInfo.Name))
                {
                    if (pInfo.CanWrite)
                    {
                        Object val = GetValue1(pInfo.PropertyType, false);
                        pInfo.SetValue(model, val, null);
                    }
                }
            }
        }

        public static Object GetValue1(Type propertyType)
        {
            Object returnValue = null;
            if (propertyType.IsEnum)
            {
                returnValue = Enum.GetValues(propertyType).GetValue(0);
            }
            else if (propertyType == typeof(Byte[]))
            {
                returnValue = new Byte[] { 2, 1, 9, 3, 8, 2 };
            }
            else if (propertyType == typeof(int))
            {
                returnValue = 219382;
            }
            else if (propertyType == typeof(float))
            {
                returnValue = 213.123123F;
            }
            else if (propertyType == typeof(Double))
            {
                returnValue = (Double)213.123123D;
            }
            else if (propertyType == typeof(string))
            {
                returnValue = "C4F-E741";
            }
            else if (propertyType == typeof(DateTime))
            {
                returnValue = DateTime.Today;
            }
            else if (propertyType == typeof(bool))
            {
                returnValue = true;
            }
            else if (propertyType == typeof(short))
            {
                returnValue = (short)12312;
            }
            else if (propertyType == typeof(byte))
            {
                returnValue = (byte)189;
            }
            else if (propertyType == typeof(Guid))
            {
                returnValue = new Guid("738B4722-5975-42B9-B8AD-19CDE92C8C20");
            }
            else if (propertyType == typeof(Int64))
            {
                returnValue = 34543535353;
            }
            else if (propertyType == typeof(Object))
            {
                returnValue = 219382;
            }
            else if (propertyType.IsGenericType && (propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                returnValue = null;
            }
            return returnValue;
        }

        public static Object GetValue1(Type propertyType, Boolean createChildren)
        {
            Object returnValue = null;
            Boolean typeFound = true;
            if (propertyType.IsEnum)
            {
                returnValue = Enum.GetValues(propertyType).GetValue(0);
            }
            else if (propertyType == typeof(Byte[]))
            {
                returnValue = new Byte[] { 2, 1, 9, 3, 8, 2 };
            }
            else if (propertyType == typeof(int))
            {
                returnValue = 219382;
            }
            else if (propertyType == typeof(float))
            {
                returnValue = 213.123123F;
            }
            else if (propertyType == typeof(Double))
            {
                returnValue = (Double)213.123123D;
            }
            else if (propertyType == typeof(string))
            {
                returnValue = "C4FC766A-E741";
            }
            else if (propertyType == typeof(DateTime))
            {
                returnValue = DateTime.Today;
            }
            else if (propertyType == typeof(bool))
            {
                returnValue = true;
            }
            else if (propertyType == typeof(short))
            {
                returnValue = (short)12312;
            }
            else if (propertyType == typeof(byte))
            {
                returnValue = (byte)189;
            }
            else if (propertyType == typeof(Guid))
            {
                returnValue = new Guid("738B4722-5975-42B9-B8AD-19CDE92C8C20");
            }
            else if (propertyType == typeof(Int64))
            {
                returnValue = 34543535353;
            }
            else if (propertyType == typeof(RowVersion))
            {
                returnValue = new RowVersion((Int64)2);
            }
            else if (propertyType == typeof(Object))
            {
                returnValue = 219382;
            }
            else if (propertyType.IsGenericType && (propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                returnValue = null;
            }

            else
            {
                ConstructorInfo constructor = propertyType.GetConstructor(Type.EmptyTypes);
                if (constructor != null)
                {
                    if (createChildren)
                    {
                        returnValue = constructor.Invoke(null);
                    }
                }
                else
                {
                    // Model object?
                    MethodInfo method = propertyType.GetMethod(
                        String.Format("New{0}", propertyType.Name),
                        Type.EmptyTypes);
                    if (method != null)
                    {
                        if (createChildren)
                        {
                            returnValue = method.Invoke(null, null);
                        }
                    }
                    else
                    {
                        typeFound = false;
                    }
                }
                if (typeFound && createChildren)
                {
                    foreach (PropertyInfo property in propertyType.GetProperties().Where(p => p.CanWrite))
                    {
                        property.SetValue(returnValue, GetValue1(property.PropertyType, false), null);
                    }
                }
            }
            if (!typeFound && createChildren)
            {
                throw new Exception(String.Format("Couldn't create dummy value for type {0}.", propertyType.Name));
            }
            return returnValue;
        }

        public static Object GetValue2(Type propertyType)
        {
            Object returnValue = null;
            if (propertyType.IsEnum)
            {
                Array values = Enum.GetValues(propertyType);
                returnValue = values.GetValue(Math.Min(1, values.Length - 1));
            }
            else if (propertyType.IsGenericType &&
                (propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) &&
                propertyType.GetGenericArguments()[0].IsEnum)
            {
                Array values = Enum.GetValues(propertyType.GetGenericArguments()[0]);
                returnValue = values.GetValue(Math.Min(1, values.Length - 1));
            }
            else if (propertyType == typeof(Byte[]))
            {
                returnValue = new Byte[] { 2, 1, 9, 3, 8, 3 };
            }
            else if ((propertyType == typeof(int)) || (propertyType == typeof(int?)))
            {
                returnValue = 219383;
            }
            else if ((propertyType == typeof(float)) || (propertyType == typeof(float?)))
            {
                returnValue = 214.123123F;
            }
            else if ((propertyType == typeof(Double)) || (propertyType == typeof(Double?)))
            {
                returnValue = (Double)214.123123D;
            }
            else if (propertyType == typeof(string))
            {
                returnValue = "C4F-E743";
            }
            else if ((propertyType == typeof(DateTime)) || (propertyType == typeof(DateTime?)))
            {
                returnValue = DateTime.Today.AddDays(-1);
            }
            else if ((propertyType == typeof(bool)) || (propertyType == typeof(bool?)))
            {
                returnValue = false;
            }
            else if ((propertyType == typeof(short)) || (propertyType == typeof(short?)))
            {
                returnValue = (short)12313;
            }
            else if ((propertyType == typeof(byte)) || (propertyType == typeof(byte?)))
            {
                returnValue = (byte)190;
            }
            else if ((propertyType == typeof(Guid)) || (propertyType == typeof(Guid?)))
            {
                returnValue = new Guid("738B4722-5975-42B9-B8AD-19CDE92C8C21");
            }
            else if ((propertyType == typeof(Int64)) || (propertyType == typeof(Int64?)))
            {
                returnValue = 34543535354;
            }
            else if (propertyType == typeof(Object))
            {
                returnValue = 219383;
            }
            return returnValue;
        } 
        #endregion

        #region Blocking Model Helpers

        internal static PlanogramBlockingGroup CreateBlockingGroup(Int32 numberOfItems)
        {
            return CreateBlockingGroup(numberOfItems, Guid.NewGuid().ToString());
        }

        internal static PlanogramBlockingGroup CreateBlockingGroup(Int32 numberOfItems, String name)
        {
            return CreateBlockingGroup(numberOfItems, name, true, false);
        }

        internal static PlanogramBlockingGroup CreateBlockingGroup(Int32 numberOfItems, String name, Boolean canMerge, Boolean isRestrictedByComponentType)
        {
            var group = PlanogramBlockingGroup.NewPlanogramBlockingGroup();
            group.Name = name;
            group.CanMerge = canMerge;
            group.IsRestrictedByComponentType = isRestrictedByComponentType;
            return group;
        }

        internal static PlanogramBlockingGroup CreateBlockingGroup(IEnumerable<String> productGtins, PlanogramBlocking blocking)
        {
            Planogram plan = blocking.Parent;

            var group = PlanogramBlockingGroup.NewPlanogramBlockingGroup(Guid.NewGuid().ToString(), blocking.GetNextBlockingGroupColour());
            blocking.Groups.Add(group);

            if (plan != null && plan.Sequence != null && productGtins.Any())
            {
                var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
                sequenceGroup.Colour = group.Colour;
                plan.Sequence.Groups.Add(sequenceGroup);
                foreach (String gtin in productGtins)
                {
                    var sequenceGroupProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
                    sequenceGroupProduct.Gtin = gtin;
                    sequenceGroup.Products.Add(sequenceGroupProduct);
                }
            }
            return group;
        }

        internal static PlanogramFixtureComponent CreateFixtureComponent(Single x, Single y, Single width, Single height, PlanogramComponentType type,
            PlanogramFixtureComponentList fixtureComponentList, PlanogramComponentList componentList)
        {
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(CreateComponent(width, height, type, componentList));
            fixtureComponent.X = x;
            fixtureComponent.Y = y;
            fixtureComponentList.Add(fixtureComponent);
            return fixtureComponent;
        }

        internal static PlanogramComponent CreateComponent(Single width, Single height, PlanogramComponentType type, PlanogramComponentList componentList)
        {
            var component = PlanogramComponent.NewPlanogramComponent(String.Empty,type,width,height,width);
            componentList.Add(component);
            return component;
        }

        internal static PlanogramBlockingLocation CreateBlockingLocation(
            PlanogramBlockingDivider left,
            PlanogramBlockingDivider right,
            PlanogramBlockingDivider top,
            PlanogramBlockingDivider bottom,
            PlanogramBlockingLocationList list)
        {
            var location = PlanogramBlockingLocation.NewPlanogramBlockingLocation();
            list.Add(location);
            location.PlanogramBlockingDividerLeftId = left == null ? null : left.Id;
            location.PlanogramBlockingDividerRightId = right == null ? null : right.Id;
            location.PlanogramBlockingDividerTopId = top == null ? null : top.Id;
            location.PlanogramBlockingDividerBottomId = bottom == null ? null : bottom.Id;
            return location;
        }

        internal static PlanogramBlockingLocation CreateBlockingLocation(
            PlanogramBlockingDivider left,
            PlanogramBlockingDivider right,
            PlanogramBlockingDivider top,
            PlanogramBlockingDivider bottom,
            PlanogramBlockingLocationList list,
            PlanogramBlockingGroup group)
        {
            var location = PlanogramBlockingLocation.NewPlanogramBlockingLocation(group);
            list.Add(location);
            location.PlanogramBlockingDividerLeftId = left == null ? null : left.Id;
            location.PlanogramBlockingDividerRightId = right == null ? null : right.Id;
            location.PlanogramBlockingDividerTopId = top == null ? null : top.Id;
            location.PlanogramBlockingDividerBottomId = bottom == null ? null : bottom.Id;
            return location;
        }

        internal static PlanogramBlockingDivider CreateBlockingDivider(Single x, Single y, Single length, PlanogramBlockingDividerType type, Byte level, PlanogramBlockingDividerList list)
        {
            var divider = PlanogramBlockingDivider.NewPlanogramBlockingDivider(level, type, x, y, length);
            divider.IsSnapped = true;
            list.Add(divider);
            return divider;
        }

        internal static PlanogramProduct CreatePlanogramProduct(Planogram planogram)
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Gtin = Guid.NewGuid().ToString();
            planogram.Products.Add(product);
            return product;
        }

        #endregion

        /// <summary>
        /// Helper method to enumerate through package and help identify which of the child objects are invalid whilst testing.
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static Boolean IsPackageValid(Package package)
        {
            foreach (Planogram plan in package.Planograms)
            {
                if (!plan.IsValid)
                {
                    Debug.WriteLine("Planogram invalid");
                }
                foreach (PlanogramFixture item in plan.Fixtures)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram fixture invalid"); }
                }
                foreach (PlanogramFixtureItem item in plan.FixtureItems)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram fixture item invalid"); }
                }
                foreach (PlanogramComponent item in plan.Components)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram component invalid"); }
                }
                foreach (PlanogramAnnotation item in plan.Annotations)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram annotation invalid"); }
                }
                foreach (PlanogramAssembly item in plan.Assemblies)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram assembly invalid"); }
                }
                foreach (PlanogramBlocking item in plan.Blocking)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram blocking invalid"); }
                }
                foreach (PlanogramEventLog item in plan.EventLogs)
                {
                    if (!item.IsValid)
                    { }
                }
                foreach (PlanogramImage item in plan.Images)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram image invalid"); }
                }
                foreach (PlanogramPosition item in plan.Positions)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram position invalid"); }
                }
                foreach (PlanogramProduct item in plan.Products)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram product invalid"); }
                }
                if (!plan.Performance.IsValid)
                { Debug.WriteLine("Planogram performance invalid"); }
                foreach (PlanogramPerformanceMetric item in plan.Performance.Metrics)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram performance metric invalid"); }
                }
                foreach (PlanogramPerformanceData item in plan.Performance.PerformanceData)
                {
                    if (!item.IsValid)
                    { Debug.WriteLine("Planogram performance data invalid"); }
                }
                if (!plan.ConsumerDecisionTree.IsValid)
                { Debug.WriteLine("Planogram CDT invalid"); }
                if (!plan.Assortment.IsValid)
                { Debug.WriteLine("Planogram assortment invalid"); }

                if (!plan.Comparison.IsValid) Debug.WriteLine("Planogram comparison invalid");
            }
            return false;
        }

        /// <summary>
        ///     Helper method to quickly create a product and its position.
        /// </summary>
        /// <returns></returns>
        public static PlanogramPosition CreateTestPosition()
        {
            Planogram testPlanogram = Planogram.NewPlanogram();
            Package testPackage = Package.NewPackage(0, PackageLockType.Unknown);
            testPackage.Planograms.Add(testPlanogram);
            PlanogramProduct testProduct = PlanogramProduct.NewPlanogramProduct();
            testPlanogram.Products.Add(testProduct);
            PlanogramFixtureItem fixtureItem = testPlanogram.FixtureItems.Add();
            WidthHeightDepthValue testSize = new WidthHeightDepthValue(1F, 1F, 1F);
            PlanogramFixtureComponent fixtureComponent =
                fixtureItem.GetPlanogramFixture()
                    .Components.Add(PlanogramComponentType.Shelf, testSize.Width, testSize.Height, testSize.Depth);
            PlanogramSubComponent subComponent = fixtureComponent.GetPlanogramComponent().SubComponents.First();
            PlanogramSubComponentPlacement testSubComponentPlacement =
                PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(subComponent, fixtureItem, fixtureComponent);
            PlanogramPosition testModel = testSubComponentPlacement.AddPosition(testProduct);
            return testModel;
        }
    }
}