﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 830

// V8-32787 : A.Silva
//  Created.

#endregion

#endregion

using System.Collections.Generic;
using FluentAssertions;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Helpers
{
    [TestFixture]
    public class AssortmentRuleEnforcerTests
    {
        [Test]
        public void PositionBreaksMaximumUnitsRuleWhenSinglePositionExceedsUnits()
        {
            var sut = new AssortmentRuleEnforcer();
            PlanogramPosition target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(10, 10, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MaxListUnits = 50;

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMaxUnits, because: "the target has a max list units set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksMaximumUnitsRuleWhenMultisitedPositionsExceedUnits()
        {
            var sut = new AssortmentRuleEnforcer();
            var target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 3, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MaxListUnits = 50;
            var secondPosition = target.GetPlanogramSubComponentPlacement().FixtureComponent.AddPosition(product);
            secondPosition.SetFacings(new WideHighDeepValue(1,3,10));

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMaxUnits, because: "the target has a max list units set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksMinimumHurdleUnitsRuleWhenSinglePositionDoesNotReachUnits()
        {
            var sut = new AssortmentRuleEnforcer();
            PlanogramPosition target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 1, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MinListUnits = 50;

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMinimumHurdleUnits, because: "the target has a min list units set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksMininimumHurdleUnitsRuleWhenMultiplePositionsDoNotReachUnits()
        {
            var sut = new AssortmentRuleEnforcer();
            var target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 1, 5));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MinListUnits = 50;
            var secondPosition = target.GetPlanogramSubComponentPlacement().FixtureComponent.AddPosition(product);
            secondPosition.SetFacings(new WideHighDeepValue(1, 1, 5));

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMinimumHurdleUnits, because: "the target has a min list units set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksPreserveUnitsRuleWhenSinglePositionDoesNotReachUnits()
        {
            var sut = new AssortmentRuleEnforcer();
            PlanogramPosition target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 1, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.PreserveListUnits = 50;

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductPreserveUnits, because: "the target has a min list units set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksPreserveUnitsRuleWhenMultiplePositionsDoNotReachUnits()
        {
            var sut = new AssortmentRuleEnforcer();
            var target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 1, 5));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.PreserveListUnits = 50;
            var secondPosition = target.GetPlanogramSubComponentPlacement().FixtureComponent.AddPosition(product);
            secondPosition.SetFacings(new WideHighDeepValue(1, 1, 5));

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductPreserveUnits, because: "the target has a min list units set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksMaximumFacingsRuleWhenSinglePositionExceedsFacingsWide()
        {
            var sut = new AssortmentRuleEnforcer();
            PlanogramPosition target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(10, 10, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MaxListFacings = 6;

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMaxFacings, because: "the target has a max list facings set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksMaximumFacingsRuleWhenMultisitedPositionsExceedFacingsWide()
        {
            var sut = new AssortmentRuleEnforcer();
            var target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(5, 10, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MaxListFacings = 6;
            var secondPosition = target.GetPlanogramSubComponentPlacement().FixtureComponent.AddPosition(product);
            secondPosition.SetFacings(new WideHighDeepValue(5, 10, 10));

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMaxFacings, because: "the target has a max list facings set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksPreserveFacingsRuleWhenSinglePositionDoesNotReachFacings()
        {
            var sut = new AssortmentRuleEnforcer();
            PlanogramPosition target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 1, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.PreserveListFacings = 5;

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductPreserveFacings, because: "the target has a min list facings set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksPreserveFacingsRuleWhenMultiplePositionsDoNotReachFacings()
        {
            var sut = new AssortmentRuleEnforcer();
            var target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 1, 5));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.PreserveListFacings = 5;
            PlanogramSubComponentPlacement subComponentPlacement = target.GetPlanogramSubComponentPlacement();
            var secondPosition = subComponentPlacement.FixtureComponent.AddPosition(product);
            secondPosition.SetFacings(new WideHighDeepValue(1, 1, 5));
            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductPreserveFacings, because: "the target has a min list facings set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksMinimumHurdleFacingsRuleWhenSinglePositionDoesNotReachFacings()
        {
            var sut = new AssortmentRuleEnforcer();
            PlanogramPosition target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 10, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MinListFacings = 5;

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMinimumHurdleFacings, because: "the target has a min list facings set and it is broken in the planogram");
        }

        [Test]
        public void PositionBreaksMinimumHurdleFacingsRuleWhenMultiplePositionsDoNotReachFacings()
        {
            var sut = new AssortmentRuleEnforcer();
            var target = "Package".CreatePackage().AddPlanogram().AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition();
            target.SetFacings(new WideHighDeepValue(1, 10, 10));
            PlanogramProduct product = target.GetPlanogramProduct();
            PlanogramAssortmentProduct assortmentProduct = target.Parent.Assortment.Products.Add(product);
            assortmentProduct.MinListFacings = 5;
            var secondPosition = target.GetPlanogramSubComponentPlacement().FixtureComponent.AddPosition(product);
            secondPosition.SetFacings(new WideHighDeepValue(1, 10, 10));

            IEnumerable<IBrokenAssortmentRule> brokenAssortmentRules = sut.GetBrokenRulesFor(assortmentProduct, null);

            brokenAssortmentRules.Should().Contain(rule => rule.Type == BrokenRuleType.ProductMinimumHurdleFacings, because: "the target has a min list facings set and it is broken in the planogram");
        }
    }
}