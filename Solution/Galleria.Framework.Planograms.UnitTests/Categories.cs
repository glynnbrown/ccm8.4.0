﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.UnitTests
{
    internal static class Categories
    {
        public const String Smoke = "Smoke";
        public const String PlanogramDal = "Planogram Dal";
    }
}
