﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-27742 : A.Silva.
//      Created.

#endregion

#region Version History: CCM810

// V8-30013 : A.Silva.
//  Updated obsolete PlanogramMetadaDetails unit tests and moved to the right file.

#endregion

#endregion

using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.HelpersTests
{
    /// <summary>
    ///     Tests for PlanogramHelper functionality.
    /// </summary>
    [TestFixture]
    public class PlanogramHelperTests : TestBase
    {
        [Test]
        public void GetOrientatedMaxSqueeze_WhenSqueezeZero_ShouldReturnOne()
        {
            var product = PlanogramProduct.NewPlanogramProduct();
            product.SqueezeWidth = 0;
            product.SqueezeHeight = 0;
            product.SqueezeDepth = 0;

            var squeeze = ProductOrientationHelper.GetOrientatedMaxSqueeze(product, PlanogramProductMerchandisingStyle.Alternate, PlanogramProductOrientationType.Back0);

            Assert.That(squeeze.Width.EqualTo(1));
            Assert.That(squeeze.Height.EqualTo(1));
            Assert.That(squeeze.Depth.EqualTo(1));
        }
    }
}
