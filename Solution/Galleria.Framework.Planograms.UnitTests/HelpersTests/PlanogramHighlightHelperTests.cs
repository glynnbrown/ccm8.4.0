﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-27742 : A.Silva.
//      Created.

#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Helpers;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.HelpersTests
{
    [TestFixture]
    public class PlanogramHighlightHelperTests
    {
        [Test]
        public void PlanogramHighlightResultGroup_WhenValueIsEnum_ShouldUseFriendlyNameForLabel()
        {
            const String expectation = "When a highlight result group is assigned an enum type value, it should use the friendlyname if available for the label.";
            const TestEnum value = TestEnum.EnumMember;
            String expected = TestEnumHelper.FriendlyNames[value];

            String actual = new PlanogramHighlightResultGroup(value).Label;

            Assert.AreEqual(expected, actual, expectation);
        }
        [Test]
        public void PlanogramHighlightResultGroup_WhenValueIsEnumButNohelper_ShouldUseMemberNameForLabel()
        {
            const String expectation = "When a highlight result group is assigned an enum type value, but no enum type helper exists, it should use the member name for the label.";
            const TestEnumNoHelper value = TestEnumNoHelper.EnumMember;
            String expected = Convert.ToString(value);

            String actual = new PlanogramHighlightResultGroup(value).Label;

            Assert.AreEqual(expected, actual, expectation);
        }

        #region Test Helpers

        /// <summary>
        ///     This enum represents an enum type with a helper class.
        /// </summary>
        private enum TestEnum
        {
            EnumMember
        }

        /// <summary>
        ///     Helper class for <see cref="TestEnum"/>.
        /// </summary>
        private static class TestEnumHelper
        {
            public static readonly Dictionary<TestEnum, String> FriendlyNames =
                new Dictionary<TestEnum, String>
                {
                    {TestEnum.EnumMember, "Enum Member"}
                };
        }

        /// <summary>
        ///     This enum represents an enum type without a helper class.
        /// </summary>
        private enum TestEnumNoHelper
        {
            EnumMember
        }

        #endregion
    }
}