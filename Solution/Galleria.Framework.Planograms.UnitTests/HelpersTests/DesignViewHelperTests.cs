﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-27510 : A.Kuszyk
//	Created.
// V8-27858 : A.Kuszyk
//  Added Test for GetMerchandisingGroupPosition.
// V8-28323 : A.Kuszyk
//  Added tests for GetPositionPlacementPosition
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using System.Diagnostics;

namespace Galleria.Framework.Planograms.UnitTests.HelpersTests
{
    [TestFixture]
    public class DesignViewHelperTests
    {
        #region Fields
        private const Single _x = 1f;
        private const Single _y = 1f;
        private const Single _z = 1f;
        private const Single _width = 1f;
        private const Single _depth = 1f;
        private const Single _height = 1f; 
        #endregion

        #region Test case sources
        private Tuple<RotationValue, RectValue>[] _transformations
        {
            get
            {
                return new[]
                {
                    new Tuple<RotationValue,RectValue>(
                        new RotationValue(Convert.ToSingle(Math.PI),0f,0f), 
                        new RectValue(_x - _width,_y,_z - _depth,_width,_height,_depth)),
                    new Tuple<RotationValue,RectValue>(
                        new RotationValue(0f, Convert.ToSingle(Math.PI),0f),
                        new RectValue(_x,_y - _height,_z - _depth,_width,_height,_depth)),
                    new Tuple<RotationValue,RectValue>(
                        new RotationValue(0f,0f,Convert.ToSingle(Math.PI)),
                        new RectValue(_x - _width,_y - _height,_z,_width,_height,_depth))
                };
            }
        }

        private IEnumerable<Double> _radianValues
        {
            get
            {
                var radianValues = new List<Double>();
                var twoPi = Math.PI * 2;
                for (Single i = 0f; Math.Round(i, 5) <= Math.Round(1f, 5); i += 0.1f)
                {
                    radianValues.Add(twoPi * i);
                }
                return radianValues;
            }
        } 
        #endregion

        #region GetPegHolePositions
        [Test]
        public void GetPegHolePositions_ReturnsCorrectPositions()
        {
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 10f, 10f, 5f, 0);
            subComponent.MerchConstraintRow1SpacingX = 1f;
            subComponent.MerchConstraintRow1StartX = 1f;
            subComponent.MerchConstraintRow1SpacingY = 1f;
            subComponent.MerchConstraintRow1StartY = 1f;
            subComponent.Angle = Convert.ToSingle(Math.PI / 2);

            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);

            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.Angle = Convert.ToSingle(Math.PI / 2);

            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);

            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);


            var actual = DesignViewHelper.GetPegHolePositions(subComponent, subComponentPosition);

            var expected = new List<PointValue>();
            for (Int32 x = 1; x < 10; x++)
            {
                for (Int32 y = 1; y < 10; y++)
                {
                    expected.Add(subComponentPosition.Transform(new PointValue(x, y, 0)));
                }
            }
            var expectedList = expected.Select(e => new PointValue(e.X, e.Y, 0)).ToList();
            var actualList = actual.Select(a => new PointValue(a.X, a.Y, 0)).ToList();
            CollectionAssert.AreEquivalent(expectedList, actualList);
        } 
        #endregion

        #region GetDividerYPositions
        [Test]
        public void GetDividerYPositions_ReturnsCorrectPositions_WithNoFaceThicknessNoRotation()
        {
            // Create the subcomponent and component.
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingY = 10f;
            subComponent.DividerObstructionStartY = 10f;
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Create the fixture and planogram.
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerYPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            for (Single i = subComponent.DividerObstructionStartY; i < subComponent.Width; i += subComponent.DividerObstructionSpacingY)
            {
                expected.Add(subComponentPosition.BoundY + i);
            }
            CollectionAssert.AreEquivalent(expected, actual.ToList());
        }

        [Test]
        [TestCaseSource("_radianValues")]
        public void GetDividerYPositions_ReturnsCorrectPositions_WithRotationNoFaceThickness(Double radianValue)
        {
            // Create the subcomponent and component.
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingY = 10f;
            subComponent.DividerObstructionStartY = 10f;
            subComponent.Slope = Convert.ToSingle(radianValue);
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Create the fixture and planogram.
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerYPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = (subComponent.DividerObstructionStartY + subComponent.FaceThicknessBottom) * Math.Cos(radianValue);
            Double iBound = subComponent.Width * Math.Cos(radianValue);
            Double iIncrement = subComponent.DividerObstructionSpacingY * Math.Cos(radianValue);
            for (Double i = iStart; Math.Abs(Math.Round(i, 5)) < Math.Abs(Math.Round(iBound, 5)); i += iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual, 2);
        }

        [Test]
        [TestCaseSource("_radianValues")]
        public void GetDividerYPositions_ReturnsCorrectPositions_WithRotationFaceThickness(Double radianValue)
        {
            // Create the subcomponent and component.
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingY = 10f;
            subComponent.DividerObstructionStartY = 10f;
            subComponent.FaceThicknessBottom = 10f;
            subComponent.FaceThicknessTop = 10f;
            subComponent.Slope = Convert.ToSingle(radianValue);
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Create the fixture and planogram.
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerYPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = (subComponent.DividerObstructionStartY + subComponent.FaceThicknessBottom) * Math.Cos(radianValue);
            Double iBound = (subComponent.Height - subComponent.FaceThicknessTop) * Math.Cos(radianValue);
            Double iIncrement = subComponent.DividerObstructionSpacingY * Math.Cos(radianValue);
            for (Double i = iStart; Math.Abs(Math.Round(i, 5)) < Math.Abs(Math.Round(iBound, 5)); i += iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual, 2);
        }

        [Test]
        public void GetDividerYPositions_ReturnsCorrectPositions_WithFaceThicknessNoRotation()
        {
            // Create the subcomponent and component.
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingY = 10f;
            subComponent.DividerObstructionStartY = 10f;
            subComponent.FaceThicknessBottom = 10f;
            subComponent.FaceThicknessTop = 10f;
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Create the fixture and planogram.
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerYPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Single iStart = subComponent.DividerObstructionStartY + subComponent.FaceThicknessBottom;
            Single iBound = (subComponent.Height - subComponent.FaceThicknessTop);
            Single iIncrement = subComponent.DividerObstructionSpacingY;
            for (Single i = iStart; i < iBound; i += iIncrement)
            {
                expected.Add(subComponentPosition.BoundY + i);
            }
            CollectionAssert.AreEquivalent(expected, actual.ToList());
        } 
        #endregion

        #region GetDividerXPositions
        [Test]
        public void GetDividerXPositions_ReturnsCorrectPositions_WithNoFaceThicknessNoRotation()
        {
            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingX = 10f;
            subComponent.DividerObstructionStartX = 10f;
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerXPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = subComponent.DividerObstructionStartX + subComponent.FaceThicknessLeft;
            Double iBound = subComponent.Width - subComponent.FaceThicknessRight;
            Double iIncrement = subComponent.DividerObstructionSpacingX;
            for (Double i = iStart; Math.Round(Math.Abs(i),5) < Math.Round(Math.Abs(iBound),5); i+=iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual,5);
        }

        [Test]
        [TestCaseSource("_radianValues")]
        public void GetDividerXPositions_ReturnsCorrectPositions_WithRotationNoFaceThickness(Double radianValue)
        {
            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingX = 10f;
            subComponent.DividerObstructionStartX = 10f;
            subComponent.Angle = Convert.ToSingle(radianValue);
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerXPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = (subComponent.DividerObstructionStartX + subComponent.FaceThicknessLeft) * Math.Cos(radianValue);
            Double iBound = (subComponent.Width - subComponent.FaceThicknessRight) * Math.Cos(radianValue);
            Double iIncrement = subComponent.DividerObstructionSpacingX * Math.Cos(radianValue);
            for (Double i = iStart; Math.Round(Math.Abs(i), 5) < Math.Round(Math.Abs(iBound), 5); i += iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual, 2);
        }

        [Test]
        [TestCaseSource("_radianValues")]
        public void GetDividerXPositions_ReturnsCorrectPositions_WithRotationFaceThickness(Double radianValue)
        {
            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingX = 10f;
            subComponent.DividerObstructionStartX = 10f;
            subComponent.FaceThicknessLeft = 10f;
            subComponent.FaceThicknessRight = 10f;
            subComponent.Angle = Convert.ToSingle(radianValue);
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerXPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = (subComponent.DividerObstructionStartX + subComponent.FaceThicknessLeft) * Math.Cos(radianValue);
            Double iBound = (subComponent.Width - subComponent.FaceThicknessRight) * Math.Cos(radianValue);
            Double iIncrement = subComponent.DividerObstructionSpacingX * Math.Cos(radianValue);
            for (Double i = iStart; Math.Round(Math.Abs(i), 5) < Math.Round(Math.Abs(iBound), 5); i += iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual, 2);
        }

        [Test]
        public void GetDividerXPositions_ReturnsCorrectPositions_WithFaceThicknessNoRotation()
        {
            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Peg, 120f, 120f, 5f, 0);
            subComponent.DividerObstructionSpacingX = 10f;
            subComponent.DividerObstructionStartX = 10f;
            subComponent.FaceThicknessLeft = 10f;
            subComponent.FaceThicknessRight = 10f;
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerXPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = (subComponent.DividerObstructionStartX + subComponent.FaceThicknessLeft);
            Double iBound = (subComponent.Width - subComponent.FaceThicknessRight);
            Double iIncrement = subComponent.DividerObstructionSpacingX;
            for (Double i = iStart; Math.Round(Math.Abs(i), 5) < Math.Round(Math.Abs(iBound), 5); i += iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual, 2);
        } 
        #endregion

        #region GetDividerTopDownPositions
        [Test]
        public void GetDividerTopDownPositions_ReturnsCorrectPositions_NoFaceThicknessNoRotation()
        {
            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Chest, 120f, 50f, 75f, 0);
            subComponent.DividerObstructionSpacingZ = 10f;
            subComponent.DividerObstructionStartZ = 10f;
            subComponent.FaceThicknessBack = 0f;
            subComponent.FaceThicknessFront = 0f;
            var component = PlanogramComponent.NewPlanogramComponent(String.Empty, PlanogramComponentType.Chest, 120f, 50f, 75f, 0);
            component.IsMerchandisedTopDown = true;
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerTopDownPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = subComponent.Depth - subComponent.DividerObstructionStartZ - subComponent.FaceThicknessBack;
            Double iBound = 0f;
            Double iIncrement = subComponent.DividerObstructionSpacingZ;
            for (Double i = iStart; i > iBound; i -= iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual, 3);
        }

        [Test]
        public void GetDividerTopDownPositions_ReturnsCorrectPositions_FaceThicknessNoRotation()
        {
            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Chest, 120f, 50f, 75f, 0);
            subComponent.DividerObstructionSpacingZ = 10f;
            subComponent.DividerObstructionStartZ = 10f;
            subComponent.FaceThicknessBack = 10f;
            subComponent.FaceThicknessFront = 10f;
            var component = PlanogramComponent.NewPlanogramComponent(String.Empty, PlanogramComponentType.Chest, 120f, 50f, 75f, 0);
            component.IsMerchandisedTopDown = true;
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            // Get positions
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);
            var subComponentPosition = new DesignViewPosition(subComponent, fixtureComponentPosition);

            var actual = DesignViewHelper.GetDividerTopDownPositions(subComponent, subComponentPosition);

            var expected = new List<Single>();
            Double iStart = subComponent.Depth - subComponent.DividerObstructionStartZ - subComponent.FaceThicknessBack;
            Double iBound = 0f + subComponent.FaceThicknessFront;
            Double iIncrement = subComponent.DividerObstructionSpacingZ;
            for (Double i = iStart; i > iBound; i -= iIncrement)
            {
                expected.Add(Convert.ToSingle(i));
            }
            AssertHelper.AreSinglesEquivalent(expected, actual, 3);
        } 
        #endregion

        private void AssertDesignViewPositionValues(
            DesignViewPosition designViewPosition,
            Single boundX, Single boundY, Single boundZ, Single boundWidth, Single boundHeight, Single boundDepth,
            Single angle, Single slope, Single roll, Int32 decimalPlaces=5)
        {
            Debug.WriteLine("Bound X: Expected [{0}], Actual [{1}]", boundX, designViewPosition.BoundX);
            Debug.WriteLine("Bound Y: Expected [{0}], Actual [{1}]", boundY, designViewPosition.BoundY);
            Debug.WriteLine("Bound Z: Expected [{0}], Actual [{1}]", boundZ, designViewPosition.BoundZ);
            Debug.WriteLine("Bound Width: Expected [{0}], Actual [{1}]", boundWidth, designViewPosition.BoundWidth);
            Debug.WriteLine("Bound Height: Expected [{0}], Actual [{1}]", boundHeight, designViewPosition.BoundHeight);
            Debug.WriteLine("Bound Depth: Expected [{0}], Actual [{1}]", boundDepth, designViewPosition.BoundDepth);
            Debug.WriteLine("Bound Slope: Expected [{0}], Actual [{1}]", slope, designViewPosition.Slope);
            Debug.WriteLine("Bound Angle: Expected [{0}], Actual [{1}]", angle, designViewPosition.Angle);
            Debug.WriteLine("Bound Roll: Expected [{0}], Actual [{1}]", roll, designViewPosition.Roll);

            Assert.AreEqual(Convert.ToSingle(Math.Round(boundX, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundX, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundY, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundY, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundZ, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundZ, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundWidth, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundWidth, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundHeight, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundHeight, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundDepth, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundDepth, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(slope, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.Slope, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(angle, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.Angle, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(roll, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.Roll, decimalPlaces)));
        }

        private PlanogramMerchandisingGroup CreateMerchandisingGroup()
        {
            var planogram = Planogram.NewPlanogram();

            // Components
            var comp1 = PlanogramComponent.NewPlanogramComponent("Shelf1", PlanogramComponentType.Shelf, 120, 10, 75);
            var comp2 = PlanogramComponent.NewPlanogramComponent("Shelf2", PlanogramComponentType.Shelf, 120, 10, 75);
            planogram.Components.AddRange(new[] { comp1, comp2 });

            // Product
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Width = 10;
            product.Height = 10;
            product.Depth = 10;
            planogram.Products.Add(product);

            // Fixture
            var fixture1 = PlanogramFixture.NewPlanogramFixture();
            fixture1.Height = 200;
            fixture1.Width = 120;
            fixture1.Depth = 75;
            var fixComp1 = PlanogramFixtureComponent.NewPlanogramFixtureComponent(comp1);
            fixComp1.Y = 60;
            fixture1.Components.Add(fixComp1);
            var fixture2 = PlanogramFixture.NewPlanogramFixture();
            fixture2.Height = 200;
            fixture2.Width = 120;
            fixture2.Depth = 75;
            var fixComp2 = PlanogramFixtureComponent.NewPlanogramFixtureComponent(comp2);
            fixComp2.Y = 60;
            fixture2.Components.Add(fixComp2);
            planogram.Fixtures.AddRange(new[] { fixture1, fixture2 });

            // Bays
            var bay1 = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture1);
            var bay2 = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture1);
            planogram.FixtureItems.AddRange(new[] { bay1, bay2 });

            // Subcomponent placements
            var subCompPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                comp1.SubComponents.First(), bay1, fixComp1);
            var subCompPlacement2 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                comp2.SubComponents.First(), bay2, fixComp2);

            // Position
            var position = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement1);
            planogram.Positions.Add(position);

            return PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(new[] { subCompPlacement1, subCompPlacement2 });
        }
    }
}
