﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-27510 : A.Kuszyk
//	Created.
#endregion

#region Version History: (CCM 803)
// V8-29137 : A.Kuszyk
//	Added tests for OverlapsWith.
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Linq;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.Planograms.UnitTests.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.HelpersTests
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public class DesignViewPositionTests
    {
        #region Fields
        private const Single _x = 1f;
        private const Single _y = 1f;
        private const Single _z = 1f;
        private const Single _width = 1f;
        private const Single _depth = 1f;
        private const Single _height = 1f;
        
        private Single[] RotationSource
        {
            get
            {
                return new Single[]
                {
                    -Convert.ToSingle(Math.PI),
                    -Convert.ToSingle(Math.PI/4f),
                    0f,
                    Convert.ToSingle(Math.PI/2f),
                    Convert.ToSingle(Math.PI),
                };
            }
        }
        #endregion

        #region Constructor_FixtureComponentPosition
        [Test]
        public void Constructor_FixtureComponentPosition_ReturnsCorrectPosition_WithNoRotation()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(PlanogramFixture.NewPlanogramFixture());
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(planogram.Fixtures.First()));
            planogram.Components.Add(PlanogramComponent.NewPlanogramComponent(String.Empty, PlanogramComponentType.Shelf, 1f, 1f, 1f));
            planogram.Fixtures.First().Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogram.Components.First()));
            var fixtureComponent = planogram.Fixtures.First().Components.First();
            fixtureComponent.X = 1f;
            fixtureComponent.Y = 1f;
            fixtureComponent.Z = 1f;

            var actual = new DesignViewPosition(
                fixtureComponent, planogram.FixtureItems.First(), 0f);

            AssertDesignViewPositionValues(actual, 1f, 1f, 1f, 1f, 1f, 1f, 0f, 0f, 0f);
        }

        [Test]
        public void Constructor_FixtureComponentPosition_ForTopDown_YShouldNotBeLessThanZero()
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(PlanogramFixture.NewPlanogramFixture());
            planogram.Fixtures.First().Height = 240;
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(planogram.Fixtures.First()));
            planogram.Components.Add(PlanogramComponent.NewPlanogramComponent(String.Empty, PlanogramComponentType.Chest, 120f, 50f, 75f));
            planogram.Fixtures.First().Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogram.Components.First()));
            var fixtureComponent = planogram.Fixtures.First().Components.First();

            Single height, width, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);

            var designViewPosition = new DesignViewPosition(fixtureComponent, planogram.FixtureItems.First(), offset);

            Assert.That(designViewPosition.BoundY >= 0);
        }

        private Tuple<RotationValue, RectValue>[] _transformations
        {
            get
            {
                return new[]
                {
                    new Tuple<RotationValue,RectValue>(
                        new RotationValue(Convert.ToSingle(Math.PI),0f,0f), 
                        new RectValue(_x - _width,_y,_z - _depth,_width,_height,_depth)),
                    new Tuple<RotationValue,RectValue>(
                        new RotationValue(0f, Convert.ToSingle(Math.PI),0f),
                        new RectValue(_x,_y - _height,_z - _depth,_width,_height,_depth)),
                    new Tuple<RotationValue,RectValue>(
                        new RotationValue(0f,0f,Convert.ToSingle(Math.PI)),
                        new RectValue(_x - _width,_y - _height,_z,_width,_height,_depth))
                };
            }
        }

        [Test]
        [TestCaseSource("_transformations")]
        public void Constructor_FixtureComponentPosition_ReturnsCorrectPosition_WithRotation(Tuple<RotationValue, RectValue> transformation)
        {
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(PlanogramFixture.NewPlanogramFixture());
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(planogram.Fixtures.First()));
            planogram.Components.Add(PlanogramComponent.NewPlanogramComponent(String.Empty, PlanogramComponentType.Shelf, _width, _height, _depth));
            planogram.Fixtures.First().Components.Add(PlanogramFixtureComponent.NewPlanogramFixtureComponent(planogram.Components.First()));
            var fixtureComponent = planogram.Fixtures.First().Components.First();
            fixtureComponent.X = _x;
            fixtureComponent.Y = _y;
            fixtureComponent.Z = _z;
            fixtureComponent.Slope = transformation.Item1.Slope;
            fixtureComponent.Angle = transformation.Item1.Angle;
            fixtureComponent.Roll = transformation.Item1.Roll;

            var actual = new DesignViewPosition(fixtureComponent, planogram.FixtureItems.First(), 0f);

            AssertDesignViewPositionValues(
                actual,
                transformation.Item2.X,
                transformation.Item2.Y,
                transformation.Item2.Z,
                transformation.Item2.Width,
                transformation.Item2.Height,
                transformation.Item2.Depth,
                transformation.Item1.Angle,
                transformation.Item1.Slope,
                transformation.Item1.Roll);
        }
        #endregion

        #region Constructor_FixtureItemPosition
        [Test]
        public void Constructor_FixtureItemPosition_ReturnsCumulativePosition()
        {
            var planogram = Planogram.NewPlanogram();
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = 120;
            planogram.Fixtures.Add(fixture);
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture, 1));
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture, 2);
            fixtureItem.X = 10000;
            fixtureItem.Y = 10;
            fixtureItem.Z = 10;
            planogram.FixtureItems.Add(fixtureItem);

            var position = new DesignViewPosition(fixtureItem);

            AssertDesignViewPositionValues(
                position,
                fixture.Width,
                fixtureItem.Y,
                0f,
                fixture.Width,
                fixture.Height,
                fixture.Depth,
                0f, 0f, 0f);
        }

        [Test]
        public void Constructor_FixtureItemPositions_FlattensPosition()
        {
            var planogram = Planogram.NewPlanogram();
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = 120;
            planogram.Fixtures.Add(fixture);
            planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(fixture, 1));
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture, 2);
            fixtureItem.Angle = Convert.ToSingle(Math.PI);
            fixtureItem.X = 10000;
            fixtureItem.Y = 10;
            fixtureItem.Z = 10;
            planogram.FixtureItems.Add(fixtureItem);

            var position = new DesignViewPosition(fixtureItem);

            AssertDesignViewPositionValues(
                position,
                fixture.Width,
                fixtureItem.Y,
                0f,
                fixture.Width,
                fixture.Height,
                fixture.Depth,
                0f, 0f, 0f);
        }
        #endregion

        #region Constructor_SubComponentPosition

        [Test]
        public void Constructor_SubComponentPosition_ReturnsCorrectPosition_WhenComponentTopDown()
        {
            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Shelf, 120f, 10f, 75f, 0);
            var component = PlanogramComponent.NewPlanogramComponent(String.Empty, PlanogramComponentType.Shelf, 120f, 10f, 75f, 0);
            component.IsMerchandisedTopDown = true;
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);

            var position = new DesignViewPosition(subComponent, fixtureComponentPosition);

            Single expX = 0f;
            Single expY = 0f;
            Single expZ = 0f;
            Single expWidth = subComponent.Width;
            Single expHeight = subComponent.Depth;
            Single expDepth = subComponent.Height;
            Single expAngle = 0f;
            Single expSlope = -Convert.ToSingle(Math.PI / 2);
            Single expRoll = 0f;

            AssertDesignViewPositionValues(position, expX, expY, expZ, expWidth, expHeight, expDepth, expAngle, expSlope, expRoll, 3);
        }

        [Test]
        public void Constructor_SubComponentPosition_ReturnsCorrectPosition_WhenComponentAngleRotation()
        {
            Single theta = Convert.ToSingle(Math.PI / 2);

            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Shelf, 120f, 10f, 75f, 0);
            var component = PlanogramComponent.NewPlanogramComponent();

            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.Angle = theta;

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);

            var position = new DesignViewPosition(subComponent, fixtureComponentPosition);

            Single expX = -Convert.ToSingle((subComponent.Depth * Math.Cos(theta)) / Math.Tan((Math.PI / 2) - theta));
            Single expY = 0f;
            Single expZ = 0f;
            Single expWidth = Convert.ToSingle(subComponent.Width * Math.Cos(theta) + subComponent.Depth * Math.Cos((Math.PI / 2) - theta));
            Single expHeight = subComponent.Height;
            Single expDepth = Convert.ToSingle(subComponent.Width * Math.Sin(theta) + subComponent.Depth * Math.Cos(theta));
            Single expAngle = theta;
            Single expSlope = 0f;
            Single expRoll = 0f;

            AssertDesignViewPositionValues(position, expX, expY, expZ, expWidth, expHeight, expDepth, expAngle, expSlope, expRoll, 3);
        }

        [Test]
        public void Constructor_SubComponentPosition_ReturnsCorrectPosition_WhenComponentRollRotation()
        {
            Single theta = Convert.ToSingle(Math.PI / 2);

            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Shelf, 120f, 10f, 75f, 0);
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.Roll = theta;

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);

            var position = new DesignViewPosition(subComponent, fixtureComponentPosition);

            Single expX = 0f;
            Single expY = -Convert.ToSingle(subComponent.Width * Math.Cos((Math.PI / 2) - theta));
            Single expZ = 0f;
            Single expWidth = Convert.ToSingle(subComponent.Width * Math.Cos(theta) + subComponent.Height * Math.Sin(theta));
            Single expHeight = Convert.ToSingle(Math.Abs(expY) + subComponent.Height * Math.Cos(theta));
            Single expDepth = subComponent.Depth;
            Single expAngle = 0f;
            Single expSlope = 0f;
            Single expRoll = theta;

            AssertDesignViewPositionValues(position, expX, expY, expZ, expWidth, expHeight, expDepth, expAngle, expSlope, expRoll, 3);
        }

        [Test]
        public void Constructor_SubComponentPosition_ReturnsCorrectPosition_WhenComponentSlopeRotation()
        {
            Single theta = Convert.ToSingle(Math.PI / 2);

            // Subcomponent and component
            var subComponent = PlanogramSubComponent.NewPlanogramSubComponent(String.Empty, PlanogramComponentType.Shelf, 120f, 10f, 75f, 0);
            var component = PlanogramComponent.NewPlanogramComponent();
            component.SubComponents.Add(subComponent);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.Slope = theta;

            // Fixture and planogram
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Components.Add(fixtureComponent);
            var planogram = Planogram.NewPlanogram();
            planogram.Fixtures.Add(fixture);
            planogram.Components.Add(component);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);
            var fixtureComponentPosition = new DesignViewPosition(fixtureComponent, fixtureItem, offset);

            var position = new DesignViewPosition(subComponent, fixtureComponentPosition);

            Single expX = 0f;
            Single expY = 0f;
            Single expZ = -Convert.ToSingle((subComponent.Height * Math.Cos(theta)) / Math.Tan((Math.PI / 2) - theta));
            Single expWidth = subComponent.Width;
            Single expHeight = Convert.ToSingle(
                Math.Sqrt(Math.Pow(subComponent.Depth, 2) - Math.Pow(subComponent.Depth * Math.Cos(theta), 2)) +
                subComponent.Height * Math.Sin((Math.PI / 2) - theta)
                );
            Single expDepth = Convert.ToSingle(subComponent.Depth * Math.Cos(theta) + subComponent.Height * Math.Cos((Math.PI / 2) - theta));
            Single expAngle = 0f;
            Single expSlope = Convert.ToSingle(theta);
            Single expRoll = 0f;

            AssertDesignViewPositionValues(position, expX, expY, expZ, expWidth, expHeight, expDepth, expAngle, expSlope, expRoll, 3);
        }
        #endregion

        #region Constructor_MerchandisingGroupPosition

        [Test]
        public void Constructor_MerchandisingGroupPosition_RotatedFixtureComponent()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf.Angle = Convert.ToSingle(Math.PI / 2f);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var shelfMerchGroup = merchGroups.First();

                var dvp = new DesignViewPosition(shelfMerchGroup);

                AssertDesignViewPositionValues(dvp, -75, 100, 0, 75, 4, 120, Convert.ToSingle(Math.PI / 2f), 0, 0,3);
            }
        }

        [Test]
        public void Constructor_MerchandisingGroupPosition_ReturnsCorrectBounds()
        {
            var merchGroup = CreateMerchandisingGroup();

            var dvp = new DesignViewPosition(merchGroup);

            AssertDesignViewPositionValues(dvp, 0, 60f, 0, 240, 10, 75, 0, 0, 0);
        }

        [Test]
        public void Constructor_MerchandisingGroupPosition_OnTopDownComponent()
        {
            var planogram = Planogram.NewPlanogram();
            var fixtureItem = planogram.AddFixtureItem();
            var fixtureComponent = fixtureItem.AddFixtureComponent(PlanogramComponentType.Chest);
            Single width, height, offset;
            planogram.GetBlockingAreaSize(out height, out width, out offset);

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                DesignViewPosition dvp = new DesignViewPosition(merchandisingGroups.First(), offset);
                AssertDesignViewPositionValues(dvp, 0, 0, 0, 120, 75, 50, 0, Convert.ToSingle(-Math.PI / 2), 0, decimalPlaces: 3);
            }
        }

        [Test]
        public void Constructor_MerchandisingGroupPosition_OnRotatedDisplacedFixture()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay2.X = 240;
            bay2.Angle = Convert.ToSingle(Math.PI / 2f);
            var shelf = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();

                var dvp = new DesignViewPosition(merchGroup);

                AssertDesignViewPositionValues(dvp,120, 100, 0, 120, 4, 75, 0, 0, 0);
            }
        }

        #endregion

        #region Constructor_AssemblyComponentPosition

        #endregion

        #region Constructor_FixtureAssemblyPosition
        [Test]
        public void Constructor_FixtureAssemblyPosition_WithNoRotationNoOffset()
        {
            const Single offset = 0;

            var planogram = Planogram.NewPlanogram();

            var component = PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, 120, 5, 75);
            planogram.Components.Add(component);

            var assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent(component);
            assemblyComponent.X = 0;
            assemblyComponent.Y = 100;
            assemblyComponent.Z = 0;

            var assembly = PlanogramAssembly.NewPlanogramAssembly();
            assembly.Width = 120;
            assembly.Height = 5;
            assembly.Depth = 75;
            assembly.Components.Add(assemblyComponent);
            planogram.Assemblies.Add(assembly);

            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 200;
            fixture.Depth = 75;
            planogram.Fixtures.Add(fixture);

            var fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
            fixtureAssembly.PlanogramAssemblyId = assembly.Id;
            fixtureAssembly.X = 0;
            fixtureAssembly.Y = 10;
            fixtureAssembly.Z = 0;
            fixture.Assemblies.Add(fixtureAssembly);

            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);

            var actual = new DesignViewPosition(fixtureAssembly, fixtureItem, offset);

            AssertDesignViewPositionValues(actual, 0, 10, 0, 120, 5, 75, 0, 0, 0);
        }
        #endregion

        #region Constructor_PositionPlacementPosition
        [Test]
        public void Constructor_PositionPlacementPosition_WithNoRotation()
        {
            var planogram = Planogram.NewPlanogram();
            var fixture = PlanogramFixture.NewPlanogramFixture();
            fixture.Width = 120;
            fixture.Height = 200;
            fixture.Depth = 75;
            planogram.Fixtures.Add(fixture);
            var fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture);
            planogram.FixtureItems.Add(fixtureItem);
            var component = PlanogramComponent.NewPlanogramComponent("Shelf", PlanogramComponentType.Shelf, 120, 4, 75);
            planogram.Components.Add(component);
            var fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent(component);
            fixtureComponent.X = 0;
            fixtureComponent.Y = 100;
            fixtureComponent.Z = 0;
            fixture.Components.Add(fixtureComponent);

            var subComponentPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                component.SubComponents.First(), fixtureItem, fixtureComponent);

            var product = PlanogramProduct.NewPlanogramProduct();
            product.Width = 10;
            product.Height = 10;
            product.Depth = 10;
            planogram.Products.Add(product);

            var position = PlanogramPosition.NewPlanogramPosition(1, product, subComponentPlacement);
            position.X = 60;
            position.Y = 4;
            position.FacingsWide = 1;
            planogram.Positions.Add(position);

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                var positionPlacement = merchandisingGroups.First().PositionPlacements.First();
                var dvp = new DesignViewPosition(positionPlacement, 0);
                AssertDesignViewPositionValues(dvp, 60, 104, 0, 10, 10, 10, 0, 0, 0);
            }
        }

        [Test]
        public void Constructor_PositionPlacementPosition_WithFixtureComponentRotation()
        {
            var planogram = Planogram.NewPlanogram();
            var fixtureItem = planogram.AddFixtureItem();
            var fixtureComponent = fixtureItem.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            fixtureComponent.Angle = Convert.ToSingle(Math.PI / 2);
            var position = fixtureComponent.AddPosition(fixtureItem, planogram.AddProduct(new WidthHeightDepthValue(20, 20, 10)));
            position.X = 60;
            position.Y = 4;
            position.Z = 0;

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                var positionPlacement = merchandisingGroups.First().PositionPlacements.First();
                DesignViewPosition dvp = new DesignViewPosition(positionPlacement, 0);
                AssertDesignViewPositionValues(dvp, -10, 104, 60, 10, 20, 20, Convert.ToSingle(Math.PI / 2), 0, 0);
            }
        }

        [Test]
        public void Constructor_PositionPlacementPosition_OnTopDownComponent()
        {
            var planogram = Planogram.NewPlanogram();
            var fixtureItem = planogram.AddFixtureItem();
            var fixtureComponent = fixtureItem.AddFixtureComponent(PlanogramComponentType.Chest);
            var position = fixtureComponent.AddPosition(fixtureItem, planogram.AddProduct(new WidthHeightDepthValue(20, 20, 10)));
            position.X = 60;
            position.Y = 4;
            position.Z = 30;

            using (PlanogramMerchandisingGroupList merchandisingGroups = planogram.GetMerchandisingGroups())
            {
                var positionPlacement = merchandisingGroups.First().PositionPlacements.First();
                Single width, height, offset;
                planogram.GetBlockingAreaSize(out height, out width, out offset);
                DesignViewPosition dvp = new DesignViewPosition(positionPlacement, offset);
                AssertDesignViewPositionValues(dvp, 60, 75 - 30 - 10, 4, 20, 10, 20, 0, Convert.ToSingle(-Math.PI / 2), 0, decimalPlaces: 3);
            }
        }
        #endregion

        #region OverlapsWith

        [Test]
        public void OverlapsWith_ReturnsTrue_WhenOverlap()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.AddBlocking().Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var location = plan.Blocking.First().Locations.First(l => l.Y.EqualTo(0));
            Single planWidth, planHeight;
            plan.GetBlockingAreaSize(out planHeight, out planWidth);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var dvp = new DesignViewPosition(merchGroups.First());

                Boolean result = dvp.OverlapsWith(location, 0.01f, planWidth, planHeight);

                Assert.IsTrue(result);
            }
        }

        [Test]
        public void OverlapsWith_ReturnsFalse_WhenNoOverlap()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            plan.AddBlocking().Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var location = plan.Blocking.First().Locations.First(l => l.Y.EqualTo(0.5f));
            Single planWidth, planHeight;
            plan.GetBlockingAreaSize(out planHeight, out planWidth);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var dvp = new DesignViewPosition(merchGroups.First());

                Boolean result = dvp.OverlapsWith(location, 0.01f, planWidth, planHeight);

                Assert.IsFalse(result);
            }
        }

        #endregion

        #region Transform
        [Test]
        public void Transform_ShouldTransformFromItemFrameToDesignViewFrame_ForShelf()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var dvp = new DesignViewPosition(shelf, bay, 0);
            var space = new RectValue(0, 4, 0, 120, 50, 75);
            var expectedSpace = new RectValue(0, 104, 0, 120, 50, 75);

            var actualSpace = dvp.TransformBounds(space);

            AssertHelper.AreRectValuesEqual(expectedSpace, actualSpace);
        }

        [Test]
        public void Transform_ShouldTransformFromItemFrameToDesignViewFrame_ForChest()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest, new PointValue(0, 0, 0));
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var dvp = new DesignViewPosition(chest, bay, offset);
            var space = new RectValue(4, 4, 4, 112, 46, 67);
            var expectedSpace = new RectValue(4, 4, 4, 112, 67, 46);

            var actualSpace = dvp.TransformBounds(space);

            AssertHelper.AreRectValuesEqual(expectedSpace, actualSpace);
        }

        [Test]
        public void Transform_ShouldTransformFromItemFrameToDesignViewFrame_ForChestMerchGroup()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest, new PointValue(0, 0, 0));
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var space = new RectValue(4, 4, 4, 112, 46, 67);
            var expectedSpace = new RectValue(4, 4, 4, 112, 67, 46);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var dvp = new DesignViewPosition(merchGroups.First(), offset);

                var actualSpace = dvp.TransformBounds(space);

                AssertHelper.AreRectValuesEqual(expectedSpace, actualSpace); 
            }
        }

        [Test]
        public void Transform_ShouldTransformFromItemFrameToDesignViewFrame_ForShelfWithFixtureItemRotation(
            [ValueSource("RotationSource")] Single angle)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.Angle = angle;
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var dvp = new DesignViewPosition(shelf, bay, 0);
            var space = new RectValue(0, 4, 0, 120, 50, 75);
            var expectedSpace = new RectValue(0, 104, 0, 120, 50, 75);

            var actualSpace = dvp.TransformBounds(space);

            AssertHelper.AreRectValuesEqual(expectedSpace, actualSpace);
        }
        #endregion

        private PlanogramMerchandisingGroup CreateMerchandisingGroup()
        {
            var planogram = Planogram.NewPlanogram();

            // Components
            var comp1 = PlanogramComponent.NewPlanogramComponent("Shelf1", PlanogramComponentType.Shelf, 120, 10, 75);
            var comp2 = PlanogramComponent.NewPlanogramComponent("Shelf2", PlanogramComponentType.Shelf, 120, 10, 75);
            planogram.Components.AddRange(new[] { comp1, comp2 });

            // Product
            var product = PlanogramProduct.NewPlanogramProduct();
            product.Width = 10;
            product.Height = 10;
            product.Depth = 10;
            planogram.Products.Add(product);

            // Fixture
            var fixture1 = PlanogramFixture.NewPlanogramFixture();
            fixture1.Height = 200;
            fixture1.Width = 120;
            fixture1.Depth = 75;
            var fixComp1 = PlanogramFixtureComponent.NewPlanogramFixtureComponent(comp1);
            fixComp1.Y = 60;
            fixture1.Components.Add(fixComp1);
            var fixture2 = PlanogramFixture.NewPlanogramFixture();
            fixture2.Height = 200;
            fixture2.Width = 120;
            fixture2.Depth = 75;
            var fixComp2 = PlanogramFixtureComponent.NewPlanogramFixtureComponent(comp2);
            fixComp2.Y = 60;
            fixture2.Components.Add(fixComp2);
            planogram.Fixtures.AddRange(new[] { fixture1, fixture2 });

            // Bays
            var bay1 = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture1);
            var bay2 = PlanogramFixtureItem.NewPlanogramFixtureItem(fixture1);
            planogram.FixtureItems.AddRange(new[] { bay1, bay2 });

            // Subcomponent placements
            var subCompPlacement1 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                comp1.SubComponents.First(), bay1, fixComp1);
            var subCompPlacement2 = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                comp2.SubComponents.First(), bay2, fixComp2);

            // Position
            var position = PlanogramPosition.NewPlanogramPosition(1, product, subCompPlacement1);
            planogram.Positions.Add(position);

            return PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(new[] { subCompPlacement1, subCompPlacement2 });
        }

        private void AssertDesignViewPositionValues(
            DesignViewPosition designViewPosition,
            Single boundX, Single boundY, Single boundZ, Single boundWidth, Single boundHeight, Single boundDepth,
            Single angle, Single slope, Single roll, Int32 decimalPlaces = 5)
        {
            Debug.WriteLine("Bound X: Expected [{0}], Actual [{1}]", boundX, designViewPosition.BoundX);
            Debug.WriteLine("Bound Y: Expected [{0}], Actual [{1}]", boundY, designViewPosition.BoundY);
            Debug.WriteLine("Bound Z: Expected [{0}], Actual [{1}]", boundZ, designViewPosition.BoundZ);
            Debug.WriteLine("Bound Width: Expected [{0}], Actual [{1}]", boundWidth, designViewPosition.BoundWidth);
            Debug.WriteLine("Bound Height: Expected [{0}], Actual [{1}]", boundHeight, designViewPosition.BoundHeight);
            Debug.WriteLine("Bound Depth: Expected [{0}], Actual [{1}]", boundDepth, designViewPosition.BoundDepth);
            Debug.WriteLine("Bound Slope: Expected [{0}], Actual [{1}]", slope, designViewPosition.Slope);
            Debug.WriteLine("Bound Angle: Expected [{0}], Actual [{1}]", angle, designViewPosition.Angle);
            Debug.WriteLine("Bound Roll: Expected [{0}], Actual [{1}]", roll, designViewPosition.Roll);

            Assert.AreEqual(Convert.ToSingle(Math.Round(boundX, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundX, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundY, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundY, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundZ, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundZ, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundWidth, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundWidth, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundHeight, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundHeight, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(boundDepth, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.BoundDepth, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(slope, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.Slope, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(angle, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.Angle, decimalPlaces)));
            Assert.AreEqual(Convert.ToSingle(Math.Round(roll, decimalPlaces)), Convert.ToSingle(Math.Round(designViewPosition.Roll, decimalPlaces)));
        }
    }
}
