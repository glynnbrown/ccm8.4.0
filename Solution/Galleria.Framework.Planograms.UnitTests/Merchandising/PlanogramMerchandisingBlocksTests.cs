﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-29421 : A.Kuszyk
//   Created.
// V8-32612 : D.Pleasance
//  Refactored PlanogramMerchandisingBlock constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
//  Added MinimumBlockSpace tests
#endregion

#region Version History : CCM830
// V8-32870 : A.Kuszyk
//  Added tests for blocking location order issues.
// CCM-18440 : A.Kuszyk
//  Added tests to ensure that block placements are always created for merchandising groups in the block.
// CCM-18320 : A.Kuszyk
//  Added tests for blocks being added in rank order.
// CCM-18462 : A.Kuszyk
//  Added test for combining blocks in connected blocks.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.Planograms.UnitTests.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using FluentAssertions;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public class PlanogramMerchandisingBlocksTests
    {
        #region Block Space Constructor

        [Test]
        public void BlockSpaceConstructor_CreatesBlockPlacementsWithDogLeggedBlock()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            leftBlock.Name = "Left";
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).PlanogramBlockingGroupId = leftBlock.Id;
            plan.AddSequenceGroup(leftBlock, plan.Products);
            plan.AddSequenceGroup(rightBlock, plan.Products);
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {

                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking, plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset)
                    .First(b => b.BlockingGroup == leftBlock);

                merchBlock.BlockPlacements.Should().HaveCount(2, "because a block placement should be created for both shelves");
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 60, 96, 75), "because the first block placement should be half the width of a shelf");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 120, 96, 75), "because the second block placement should be the full width of a shelf");
            }
        }

        [Test]
        public void BlockSpaceConstructor_CombinesBlockPlacementsForConnectedBlocks()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Backboard);
            var bay2 = plan.AddFixtureItem();
            bay2.AddFixtureComponent(PlanogramComponentType.Backboard);
            var b1Shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var b1Shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            var b2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var b2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            b1Shelf2.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            b2Shelf2.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            leftBlock.Name = "Left Block";
            plan.AddSequenceGroup(leftBlock, plan.Products);
            foreach (var l in blocking.Locations) l.PlanogramBlockingGroupId = leftBlock.Id;
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            //plan.TakeSnapshot("BlockSpaceConstructor_CombinesBlockPlacementsForConnectedBlocks");
            using (var merchGroups = plan.GetMerchandisingGroups())
            {

                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking, plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset)
                    .First(b => b.BlockingGroup == leftBlock);

                merchBlock.BlockPlacements.Should().HaveCount(
                    3, "because one combined block placement should be created for the top shelf");
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should()
                    .Be(new RectValue(0, 4, 0, 120, 96, 75), "because the first block placement should span one bay");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should()
                    .Be(new RectValue(0, 4, 0, 120, 96, 75), "because the second block placement should span one bay");
                merchBlock.BlockPlacements.ElementAt(2).Bounds.Should()
                    .Be(new RectValue(0, 4, 0, 240, 96, 75), "because the third block placement should span both bays");
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlocksInRankOrder()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf3.AddPosition(bay);
            shelf2.AddPosition(bay);
            shelf1.AddPosition(bay);
            plan.CreateAssortmentFromProducts();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.33f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.66f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var middleBlock = blocking.Locations.First(l => l.Y.EqualTo(0.33f)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.66f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Skip(2).Take(1));
            plan.AddSequenceGroup(middleBlock, plan.Products.Skip(1).Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Take(1));
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                merchBlocks.Select(b => b.BlockingGroup).Should().ContainInOrder(
                    new[] { topBlock, middleBlock, bottomBlock },
                    "because the blocks should be ordered by product rank");
            }
        }

        [Test]
        public void BlockSpaceConstructor_UsesComponentDepthsInsteadOfFixtureDepths()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            var bay = plan.AddFixtureItem();
            bay.GetPlanogramFixture().Depth = 10;
            bay.AddFixtureComponent(PlanogramComponentType.Shelf).GetPlanogramComponent().SubComponents[0].Depth = 100;

            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                merchBlocks.First().BlockPlacements.First().Bounds.Depth
                    .Should().Be(100, "because the block placement should be as deep as the component.");
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlockPlacementsInCorrectOrder_WhenMultipleLocationsPerShelf()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay2.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,50,0));
            bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            foreach (var sc in plan.Components.SelectMany(c => c.SubComponents)) { sc.CombineType = PlanogramSubComponentCombineType.Both; }
            var blocking = plan.AddBlocking();
            /*   ________________________
            *   |            |           |
            *   |            |           |
            *   |            |           |
            *   |            |           |
            *   |            |           |
            *   |____________|           |
            *   |   \  \  \  |___________|
            *   | \  \  \  \ | \  \|     |
            *   |  \  \  \   |  \  |     |
            *   | \ \  \  \  | \ \ |     |
            *   |  \ \  \  \ |  \ \|     |
            *   | \ \ \  \   | \ \ |     |
            *   |____________|_____|_____|
            */
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.75f, 0.4f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.75f, 0.25f, PlanogramBlockingDividerType.Vertical);
            var bottomLeftLocation = blocking.Locations.First(l => l.X.Equals(0) && l.Y.EqualTo(0));
            bottomLeftLocation.GetPlanogramBlockingGroup().Name = "Bottom Left";
            var bottomRightLocation = blocking.Locations.First(l => l.X.Equals(0.75f) && l.Y.EqualTo(0));
            var bottomMiddleLocation = blocking.Locations.First(l => l.X.Equals(0.5f) && l.Y.EqualTo(0));
            var topLeftLocation = blocking.Locations.First(l => l.X.Equals(0) && l.Y.EqualTo(0.5f));
            var topRightLocation = blocking.Locations.First(l => l.X.Equals(0.5f) && l.Y.EqualTo(0.4f));
            bottomMiddleLocation.PlanogramBlockingGroupId = bottomLeftLocation.GetPlanogramBlockingGroup().Id;
            bottomLeftLocation.GetPlanogramBlockingGroup().BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            foreach (var g in blocking.Groups.Where(g => !g.GetBlockingLocations().Any()).ToList()) { blocking.Groups.Remove(g); }
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            //plan.TakeSnapshot("BlockSpaceConstructor_CreatesBlockPlacementsInCorrectOrder_WhenMultipleLocationsPerShelf");
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset)
                    .First(b => b.BlockingGroup == bottomLeftLocation.GetPlanogramBlockingGroup());

                merchBlock.BlockPlacements.Should().HaveCount(2, "because combined shelves should be created as one block placement");
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 180, 46, 75), "because the first block placement should span both shelves");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 120, 46, 75), "because the second block placement should just span the one shelf");
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlockPlacements_ForOneShelf()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking().AddGroup().AddLocation(0, 1, 0, 1);
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay1);
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                Assert.AreEqual(1, merchBlocks.First().BlockPlacements.Count());
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlock_ForOneBlockPlan()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking().AddGroup().AddLocation(0, 1, 0, 1);
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay1);
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                Assert.AreEqual(1, merchBlocks.Count());
                Assert.AreEqual(plan.Blocking.First().Groups.First(), merchBlocks.First().BlockingGroup);
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlockPlacements_ForOneShelfSplitByBlocking()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            var bay1 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.AddPosition(bay1);
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                merchBlocks.Should().HaveCount(2, "because a merchandising block should be created for each blocking group");
                merchBlocks.First(b => b.BlockingGroup == leftBlock).BlockPlacements.First().Size.Width.Should().Be(
                    shelf1.GetPlanogramComponent().Width / 2,
                    "because the left block should be the width of half the shelf");
                merchBlocks.First(b => b.BlockingGroup == leftBlock).BlockPlacements.First().Coordinates.X.Should().Be(
                    0,
                    "because the left block should be at the start of the shelf");
                merchBlocks.First(b => b.BlockingGroup == rightBlock).BlockPlacements.First().Size.Width.Should().Be(
                    shelf1.GetPlanogramComponent().Width / 2,
                    "because the right block should be the width of half the shelf");
                merchBlocks.First(b => b.BlockingGroup == rightBlock).BlockPlacements.First().Coordinates.X.Should().Be(
                    shelf1.GetPlanogramComponent().Width / 2,
                    "because the right block should be at half way along the shelf");
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlockPlacements_TwoShelvesOnePegBoardFourBlocks()
        {
            /*       _______|_______
             *      |       +       |
             *      |=======|=======|   (shelf)
             *      | ______+______ |
             *      || : : :|: : : ||
             *      || : : :+: : : ||
             *   -+-+-+-+-+-|-+-+-+-+-+- (divider)
             *      || : : :+: : : ||   
             *      || : : :|: : : ||   (pegboard)
             *      ||______+______||
             *      |       |       |
             *      |=======+=======|   (shelf)
             *      |_______|_______|
             *              +
             *           (divider)
             */

            var plan = "Package".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var bay1 = plan.AddFixtureItem();
            var topShelf = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 170, 0));
            var bottomShelf = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var pegboard = bay1.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 40, 0));
            
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            Dictionary<PlanogramBlockingGroup, IEnumerable<RectValue>> expectedVolumesByBlockingGroup =
                new Dictionary<PlanogramBlockingGroup, IEnumerable<RectValue>>()
                {
                    { 
                        blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup(),
                        new[] { new RectValue(0,4,0,60,36,75), new RectValue(0,-36,4,60,96,71) }
                    },

                    { 
                        blocking.Locations.First(l => l.X.EqualTo(0.5) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup(),
                        new[] { new RectValue(60,4,0,60,36,75), new RectValue(60,-36,4,60,96,71) }
                    },

                    { 
                        blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0.5)).GetPlanogramBlockingGroup(),
                        new[] { new RectValue(0,4,0,60,26,75), new RectValue(0,60,4,60,60,71) }
                    },

                    { 
                        blocking.Locations.First(l => l.X.EqualTo(0.5) && l.Y.EqualTo(0.5)).GetPlanogramBlockingGroup(),
                        new[] { new RectValue(60,4,0,60,26,75), new RectValue(60,60,4,60,60,71) }
                    }
                };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                foreach (var block in merchBlocks)
                {
                    var expectedRectValues = expectedVolumesByBlockingGroup[block.BlockingGroup].OrderBy(r => r.X).ThenBy(r => r.Y).ThenBy(r => r.Z).ToList();
                    var actualRectValues = block.BlockPlacements.Select(p => p.Bounds).OrderBy(r => r.X).ThenBy(r => r.Y).ThenBy(r => r.Z).ToList();
                    actualRectValues.Should().HaveCount(
                        expectedRectValues.Count, 
                        $"because there should be the same number of block placements as intersecting blocks for {block.BlockingGroup.Name}");
                    for (Int32 i = 0; i < expectedRectValues.Count; i++)
                    {
                        AssertHelper.AreRectValuesEqual(expectedRectValues[i], actualRectValues[i]);
                    }
                }
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlockPlacements_ChestWithTwoBlocks()
        {
            /*       ___________
             *      |           |
             *      |           |               Expecting the "top" block to be spaced "behind" the "bottom" block.
             *      |           |
             *      |+=========+|
             *   -~-||-~-~-~-~-||-~ Divider
             *      ||_________||
             *       +=========+    (Chest)
             * 
             */

            var plan = "".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking();
            Single dividerY = 0.167f;
            blocking.Dividers.Add(0f, dividerY, PlanogramBlockingDividerType.Horizontal);
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());

            var bay1 = plan.AddFixtureItem();
            var chest = bay1.AddFixtureComponent(PlanogramComponentType.Chest);

            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            Dictionary<PlanogramBlockingGroup, IEnumerable<RectValue>> expectedVolumesByBlockingGroup =
                new Dictionary<PlanogramBlockingGroup, IEnumerable<RectValue>>()
                {
                    { 
                        blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup(),
                        new[] { new RectValue(4,4,37.43f,112,46,33.575f) }
                    },

                    { 
                        blocking.Locations.First(l => l.Y.EqualTo(0.167f)).GetPlanogramBlockingGroup(),
                        new[] { new RectValue(4,4,4,112,46,33.43f) }
                    },
                };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                foreach (var block in merchBlocks)
                {
                    var expectedRectValues = expectedVolumesByBlockingGroup[block.BlockingGroup].ToList();
                    var actualRectValues = block.BlockPlacements.Select(p => p.Bounds).ToList();
                    AssertHelper.AreRectValuesEqual(expectedRectValues.First(), actualRectValues.First(), 1);
                }
            }
        }

        [Test]
        public void BlockSpaceConstructor_CreatesBlockPlacements_BarWithOneBlock()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 150, 0));
            plan.AddBlocking().AddGroup().AddLocation(0, 1, 0, 1);
            plan.Sequence.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup(plan.Blocking.First().Groups.First()));

            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), plan.Sequence, merchGroups, planWidth, planHeight, planHeightOffset);

                var expected = new RectValue(0, -150, 2, 120, 152, 73);
                var actual = merchBlocks.First().BlockPlacements.First().Bounds;
                AssertHelper.AreRectValuesEqual(expected, actual);
            }
        }

        [Test]
        public void BlockSpaceConstructor_BlockPlacementsAreMerchGroupRelative_WithTopDownComponents()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            var expectedBlockPlacementsByBlockingGroup = new Dictionary<PlanogramBlockingGroup, RectValue>()
            {
                { 
                    blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0f)),
                    new RectValue(4,4,4,112,46,67)
                },

                { 
                    blocking.Groups.First(g => !g.GetBlockingLocations().First().Y.EqualTo(0f)),
                    new RectValue(0,4,0,120,46,75)
                }
            };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                foreach (var merchBlock in new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset))
                {
                    AssertHelper.AreRectValuesEqual(
                        expectedBlockPlacementsByBlockingGroup[merchBlock.BlockingGroup],
                        merchBlock.BlockPlacements.First().Bounds);
                }
            }
        }

        [Test]
        public void BlockSpaceConstructor_BlockPlacementsAreMerchGroupRelative_ComplexPlan()
        {
            #region Diagram of plan/blocking layout
            /*
             *  COMPONENT LAYOUT:
             *   ______________________________________
             *  |            |            | +--------+ |
             *  |============|============| | :::::: | |  
             *  |            |            | | :::::: | |  
             *  |            |============| | :::::: | |  
             *  |            |            | +--------+ |  
             *  | ++======++ |============|            |  
             *  | ||      || |            |            | 
             *  |_||      ||_|============|============|  
             *    ++======++
             *
             *  BLOCKING LAYOUT:
             *  ....................|...................
             *  :            :      |     :            :
             *  :            :      |     :            :
             *  :            :      |     :            :
             *  ____________________|___________________
             *  :            :      |     :            :
             *  :            :      |     :            :
             *  :            :      |     :            :
             *  :            :      |     :            :
             *  :............:......|.....:............:
             * 
             */

            #endregion

            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem(); bay2.X = 120;
            var bay3 = plan.AddFixtureItem(); bay3.X = 240;
            var b1Shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var b1Chest1 = bay1.AddFixtureComponent(PlanogramComponentType.Chest);
            var b2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var b2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var b2Shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 110, 0));
            var b2Shelf4 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var b3Shelf1 = bay3.AddFixtureComponent(PlanogramComponentType.Shelf);
            var b3Peg1 = bay3.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 80, 0));

            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            var expectedBlockPlacementsByBlockingGroup = new Dictionary<PlanogramBlockingGroup, RectValue[]>()
            {
                #region Bottom left block
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0f) && g.GetBlockingLocations().First().X.EqualTo(0f)),
                    new RectValue[]
                    {
                        new RectValue(4,4,4,112,46,67), // Chest
                        new RectValue(0,4,0,60,46,75), // Bottom shelf
                        new RectValue(0,4,0,60,46,75), // First shelf up from bottom
                    }
                }, 
	            #endregion

                #region Bottom right block
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0f) && g.GetBlockingLocations().First().X.EqualTo(0.5f)),
                    new RectValue[]
                    {
                        new RectValue(0,4,0,60,46,75), // Bottom shelf
                        new RectValue(0,4,0,120,76,75), // Bottom shelf
                        new RectValue(0,0,4,120,20,71), // Pegboard
                        new RectValue(60,40,0,60,46,75) // First shelf up
                    }
                }, 
	            #endregion

                #region Top left block
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0.5f) && g.GetBlockingLocations().First().X.EqualTo(0f)),
                    new RectValue[]
                    {
                        new RectValue(0,4,0,60,36,75), // Middle shelf
                        new RectValue(0,4,0,60,46,75), // Top shelf
                        new RectValue(0,4,0,120,46,75), // Top shelf
                    }
                }, 
	            #endregion

                #region Top rightblock
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0.5f) && g.GetBlockingLocations().First().X.EqualTo(0.5f)),
                    new RectValue[]
                    {
                        new RectValue(60,4,0,60,36,75), // Middle shelf
                        new RectValue(0,20,4,120,100,71), // Pegboard
                        new RectValue(60,4,0,60,46,75), // Top shelf
                    }
                }, 
	            #endregion
            };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                foreach (var merchBlock in new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset))
                {
                    Int32 i = 0;
                    foreach (var blockPlacement in merchBlock.BlockPlacements)
                    {
                        AssertHelper.AreRectValuesEqual(
                            expectedBlockPlacementsByBlockingGroup[merchBlock.BlockingGroup][i++],
                            blockPlacement.Bounds);
                    }
                }
            }
        }

        [Test]
        public void BlockSpaceConstructor_CombinedShelvesShouldYieldOneBlockPlacementPerBlock_RegardlessOfMerchHeight()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem(); bay2.X = 120;
            var bay3 = plan.AddFixtureItem(); bay3.X = 240;
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.GetPlanogramComponent().SubComponents.First().MerchandisableHeight = 10;
            shelf1.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf2.GetPlanogramComponent().SubComponents.First().MerchandisableHeight = 15;
            shelf2.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            var shelf3 = bay3.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf3.GetPlanogramComponent().SubComponents.First().MerchandisableHeight = 12;
            shelf3.GetPlanogramComponent().SubComponents.First().CombineType = PlanogramSubComponentCombineType.Both;
            plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                Assert.AreEqual(1, merchBlocks.First().BlockPlacements.Count());
                AssertHelper.AreRectValuesEqual(
                    new RectValue(0, 4, 0, 360, 10, 75),
                    merchBlocks.First().BlockPlacements.First().Bounds);
            }
        }

        [Test]
        public void BlockSpaceConstructor_TopShelfShouldNotHaveHeightOfTallestProduct()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddProduct(new WidthHeightDepthValue(10, 50, 10));
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 190, 0));
            plan.AddBlocking(new Tuple<Single, Single>[] { new Tuple<Single, Single>(0f, 0f) });
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                        plan.Blocking.First(),
                        plan.Sequence,
                        merchGroups,
                        width,
                        height,
                        offset);

                AssertHelper.AreSinglesEqual(6f, merchBlocks.First().BlockPlacements.First().Bounds.Height);
            }
        }

        [Test]
        public void BlockSpaceConstructor_CorrectlyIgnoresFixtureRotation()
        {
            #region Fixture diagram
            /*
             *                           _
             *       ____________       |\\          
             *      /___________/|      | \\          
             *      |           ||      |  \\         
             *      |           ||      |   \\        
             *      |           ||      |    ||  
             *      |           ||      |    ||
             *      |           ||      |    ||
             *      |           ||   ___|    ||
             *      |           ||  |\   \   ||
             *      |___________||  \ \   \  || 
             *     /____________/|   \ \   \ ||
             *     |___________|/     \ \___\||
             *                         \|____||
             * 
             * 
             */

            #endregion

            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay2.X = bay1.GetPlanogramFixture().Width * 2f;
            bay2.Angle = Convert.ToSingle(Math.PI / 2f);
            Int32 bayNumber = 1;
            foreach (var bay in plan.FixtureItems)
            {
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0))
                    .GetPlanogramComponent().Name = String.Format("Bay[{0}].Shelf[1]", bayNumber);
                bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 110, 0))
                    .GetPlanogramComponent().Name = String.Format("Bay[{0}].Shelf[2]", bayNumber++);
            }
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.6f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            plan.Sequence.ApplyFromBlocking(blocking);

            #region Expected Dictionary
            var expectedBlockPlacementsByBlockingGroup = new Dictionary<PlanogramBlockingGroup, RectValue[]>()
            {
                #region Bottom left block
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0f) && g.GetBlockingLocations().First().X.EqualTo(0f)),
                    new RectValue[]
                    {
                        new RectValue(0,4,0,120,86,75), // Shelf
                        new RectValue(0,4,0,24,86,75), // Shelf
                    }
                }, 
	            #endregion

                #region Bottom right block
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0f) && g.GetBlockingLocations().First().X.EqualTo(0.6f)),
                    new RectValue[]
                    {
                        new RectValue(24,4,0,96,86,75), // Shelf
                    }
                }, 
	            #endregion

                #region Top left block
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0.5f) && g.GetBlockingLocations().First().X.EqualTo(0f)),
                    new RectValue[]
                    {
                        new RectValue(0,4,0,120,86,75), // Shelf
                        new RectValue(0,4,0,24,86,75), // Shelf
                    }
                }, 
	            #endregion

                #region Top right block
		        { 
                    blocking.Groups.First(g => 
                        g.GetBlockingLocations().First().Y.EqualTo(0.5f) && g.GetBlockingLocations().First().X.EqualTo(0.6f)),
                    new RectValue[]
                    {
                        new RectValue(24,4,0,96,86,75), // Shelf
                    }
                }, 
	            #endregion
            };
            #endregion

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset);
                Assert.AreEqual(expectedBlockPlacementsByBlockingGroup.Count, merchBlocks.Count());
                foreach (var merchBlock in merchBlocks)
                {
                    Assert.AreEqual(expectedBlockPlacementsByBlockingGroup[merchBlock.BlockingGroup].Count(), merchBlock.BlockPlacements.Count());
                    Int32 i = 0;
                    foreach (var blockPlacement in merchBlock.BlockPlacements)
                    {
                        AssertHelper.AreRectValuesEqual(
                            expectedBlockPlacementsByBlockingGroup[merchBlock.BlockingGroup][i++],
                            blockPlacement.Bounds);
                    }
                }
            }
        }

        [Test]
        public void BlockSpaceConstructor_WhenConnectedBlocksShareMerchandisingGroup_DoesNotThrow()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            plan.Sequence.ApplyFromBlocking(blocking);
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            blocking.Groups.First().UpdateAssociatedLocations(blocking.Groups.SelectMany(g => g.GetBlockingLocations()));

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                TestDelegate constructor = () => new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset);
                constructor.Invoke();
                Assert.DoesNotThrow(constructor);
            }
        }

        [Test]
        public void BlockSpaceConstructor_TwoBaysWithChest_TopDownOffsetShouldBeAccountedFor()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay2.X = 120;
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 180, 0));
            bay2.AddFixtureComponent(PlanogramComponentType.Chest);
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            plan.Sequence.ApplyFromBlocking(blocking);
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var expectedPlacementsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
            {
                { blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0)), new RectValue(0,4,0,120,16,75) },
                { blocking.Groups.First(g => g.GetBlockingLocations().First().X.EqualTo(0.5f)), new RectValue(4,4,4,112,46,67) },
            };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset);
                foreach (var merchBlock in merchBlocks)
                {
                    AssertHelper.AreRectValuesEqual(
                        expectedPlacementsByBlock[merchBlock.BlockingGroup],
                        merchBlock.BlockPlacements.First().Bounds);
                }
            }
        }

        [Test]
        public void BlockSpaceConstructor_TwoBlocksOnTopComponent_OnlyTopBlockGetsTopSpace()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 100, 0), new WidthHeightDepthValue(120, 100, 4));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.75f, PlanogramBlockingDividerType.Horizontal);
            plan.Sequence.ApplyFromBlocking(blocking);
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var expectedPlacementsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
            {
                { blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0)), new RectValue(0,-100,4,120,150,71) },
                { blocking.Groups.First(g => g.GetBlockingLocations().First().Y.EqualTo(0.75f)), new RectValue(0,50,4,120,50,71) },
            };

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset);
                foreach (var merchBlock in merchBlocks)
                {
                    AssertHelper.AreRectValuesEqual(
                        expectedPlacementsByBlock[merchBlock.BlockingGroup],
                        merchBlock.BlockPlacements.First().Bounds);
                }
            }
        } 

        [Test]
        public void BlockSpaceConstructor_BlockInMiddleOfPegboard_GetsCorrectSpace()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, size: new WidthHeightDepthValue(120,200,4));
            #region Blocking layout
            //       _______________
            //      |_______________|
            //      |   |       |   |
            //      |   |       |   |
            //      |___|_______|___|
            //      |_______________|
            #endregion
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.2f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.8f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.2f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.8f, 0.5f, PlanogramBlockingDividerType.Vertical);
            var middleGroup = blocking.Locations.First(l => l.X.EqualTo(0.2f) && l.Y.EqualTo(0.2f)).GetPlanogramBlockingGroup();
            foreach (var g in blocking.Groups) plan.Sequence.Groups.Add(PlanogramSequenceGroup.NewPlanogramSequenceGroup(g));
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {

                var merchBlocks = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset);

                AssertHelper.AreRectValuesEqual(
                    new RectValue(24, 40, 4, 72, 120, 71),
                    merchBlocks.First(b => b.BlockingGroup == middleGroup).BlockPlacements.First().Bounds);
            }
        }
        #endregion

        #region Position Space Constructor

        [Test]
        public void PositionSpaceConstructor_WhenConnectedBlocksSpanCombinedShelves_BlockPlacementsGetCorrectSpaceWithinBlockSpace()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            var b1Shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var b1Shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var b2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var b2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            foreach (var sc in plan.Components.SelectMany(c => c.SubComponents)) sc.CombineType = PlanogramSubComponentCombineType.Both;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.75f, 0.4f, PlanogramBlockingDividerType.Horizontal);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.4f)).PlanogramBlockingGroupId = leftBlock.Id;
            for (var i = 0; i < 4; i++) b1Shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(25, 10, 10)));
            for (var i = 0; i < 4; i++) b1Shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(25, 10, 10)));
            plan.AddSequenceGroup(leftBlock, plan.Products);
            plan.CreateAssortmentFromProducts();
            plan.UpdatePositionSequenceData();
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products) },
            };
            //plan.TakeSnapshot("PositionSpaceConstructor_WhenConnectedBlocksSpanCombinedShelves_BlockPlacementsGetCorrectSpace");
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    planWidth,
                    planHeight,
                    planHeightOffset,
                    blocking.Groups[0],
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace)
                    .First(b => b.BlockingGroup == leftBlock);

                merchBlock.BlockPlacements.Should().HaveCount(2, "because there should be a block placement for each combined shelf in the block");
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 120, 96, 75),
                    "because the first block placement should occupy one shelf");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 240, 96, 75),
                    "because the second block placement should occupy the entire combined shelf, covering two bays");
            }
        }

        [Test]
        public void PositionSpaceConstructor_WhenConnectedBlocksSpanCombinedShelves_BlockPlacementsGetCorrectSpaceWithinBlockPresentation()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            var b1Shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var b1Shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var b2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var b2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            foreach (var sc in plan.Components.SelectMany(c => c.SubComponents)) sc.CombineType = PlanogramSubComponentCombineType.Both;
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.75f, 0.4f, PlanogramBlockingDividerType.Horizontal);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.4f)).PlanogramBlockingGroupId = leftBlock.Id;
            for (var i = 0; i < 4; i++) b1Shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(25, 10, 10)));
            for (var i = 0; i < 3; i++) b1Shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(25, 10, 10)));
            b1Shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(120, 10, 10)));
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(7));
            plan.CreateAssortmentFromProducts();
            plan.UpdatePositionSequenceData();
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products) },
            };
            //plan.TakeSnapshot("PositionSpaceConstructor_WhenConnectedBlocksSpanCombinedShelves_BlockPlacementsGetCorrectSpace");
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    planWidth,
                    planHeight,
                    planHeightOffset,
                    blocking.Groups[0],
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation)
                    .First(b => b.BlockingGroup == leftBlock);

                merchBlock.BlockPlacements.Should().HaveCount(2, "because there should be a block placement for each combined shelf in the block");
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 120, 96, 75),
                    "because the first block placement should occupy one shelf");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 240, 96, 75),
                    "because the second block placement should occupy the entire combined shelf, covering two bays");
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacementsWithDogLeggedBlock()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf2.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            for (int i = 0; i < 10; i++) shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            for (int i = 0; i < 2; i++) shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).PlanogramBlockingGroupId = leftBlock.Id;
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(3).Union(plan.Products.Skip(10)));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(3).Take(7));
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(3).Union(plan.Products.Skip(10))) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(3).Take(7)) },
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {

                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    planWidth,
                    planHeight,
                    planHeightOffset,
                    blocking.Groups[0],
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)
                    .First(b => b.BlockingGroup == leftBlock);

                merchBlock.BlockPlacements.Should().HaveCount(2, "because a block placement should be created for both shelves");
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 60, 96, 75), "because the first block placement should be half the width of a shelf");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 120, 96, 75), "because the second block placement should be the full width of a shelf");
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlocksInRankOrder()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            shelf3.AddPosition(bay);
            shelf2.AddPosition(bay);
            shelf1.AddPosition(bay);
            plan.CreateAssortmentFromProducts();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.33f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.66f, PlanogramBlockingDividerType.Horizontal);
            var bottomBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var middleBlock = blocking.Locations.First(l => l.Y.EqualTo(0.33f)).GetPlanogramBlockingGroup();
            var topBlock = blocking.Locations.First(l => l.Y.EqualTo(0.66f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(bottomBlock, plan.Products.Skip(2).Take(1));
            plan.AddSequenceGroup(middleBlock, plan.Products.Skip(1).Take(1));
            plan.AddSequenceGroup(topBlock, plan.Products.Take(1));
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { bottomBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(2).Take(1)) },
                { middleBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(1).Take(1)) },
                { topBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(1)) },
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    planWidth,
                    planHeight,
                    planHeightOffset,
                    blocking.Groups[0],
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace);

                merchBlocks.Select(b => b.BlockingGroup).Should().ContainInOrder(
                    new[] { topBlock, middleBlock, bottomBlock },
                    "because the blocks should be ordered by product rank");
            }
        }

        public enum BlockInterest { BlockIsOfInterest, BlockIsNotOfInterest }
        public enum ExceedBlockSpace { CanExceedBlockSpace, CannotExceedBlockSpace }

        [Test]
        public void PositionSpaceConstructor_CorrectlyAugmentsDepthWithWhiteSpace_WhenBeyondBlockSpace()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 60)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 65)));
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceGroupProducts(plan.Products);
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            var productStacksByBlockColour = plan.Sequence.Groups.ToDictionary(
                g => g.Colour,
                g => new Stack<PlanogramProduct>(
                    g.Products
                        .Select(p => plan.Products.First(prod => prod.Gtin == p.Gtin))
                        .Reverse()));
            using (var merchGroups = plan.GetMerchandisingGroups())
            {

                PlanogramMerchandisingBlockPlacement blockPlacement = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    planWidth,
                    planHeight,
                    planHeightOffset,
                    blocking.Groups[0],
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)
                    .First().BlockPlacements.First();

                blockPlacement.Bounds.Depth.Should().Be(75, "because when going beyond block space all available white space should be used");
                blockPlacement.Bounds.Z.Should().Be(0, "because when going beyond block space the block space should start at the back of the components merch space");
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacementsInCorrectOrder_WhenMultipleLocationsPerShelf()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Backboard);
            bay2.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            foreach (var fc in plan.Fixtures.SelectMany(f => f.Components))
            {
                for(int i=0; i<11;i++) fc.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            }
            plan.ReprocessAllMerchandisingGroups();
            foreach (var sc in plan.Components.SelectMany(c => c.SubComponents)) { sc.CombineType = PlanogramSubComponentCombineType.Both; }
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.25f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.75f, 0.4f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.75f, 0.25f, PlanogramBlockingDividerType.Vertical);
            var bottomLeftLocation = blocking.Locations.First(l => l.X.Equals(0) && l.Y.EqualTo(0));
            var bottomRightLocation = blocking.Locations.First(l => l.X.Equals(0.75f) && l.Y.EqualTo(0));
            var bottomMiddleLocation = blocking.Locations.First(l => l.X.Equals(0.5f) && l.Y.EqualTo(0));
            var topLeftLocation = blocking.Locations.First(l => l.X.Equals(0) && l.Y.EqualTo(0.5f));
            var topRightLocation = blocking.Locations.First(l => l.X.Equals(0.5f) && l.Y.EqualTo(0.4f));
            bottomMiddleLocation.PlanogramBlockingGroupId = bottomLeftLocation.GetPlanogramBlockingGroup().Id;
            bottomLeftLocation.GetPlanogramBlockingGroup().BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            foreach (var g in blocking.Groups.Where(g => !g.GetBlockingLocations().Any()).ToList()) { blocking.Groups.Remove(g); }
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            var productStacksByBlockColour = plan.Sequence.Groups.ToDictionary(
                g => g.Colour, 
                g => new Stack<PlanogramProduct>(g.Products.Select(p => plan.Products.First(prod => prod.Gtin == p.Gtin))));
            plan.UpdatePositionSequenceData();
            //plan.TakeSnapshot("PositionSpaceConstructor_CreatesBlockPlacementsInCorrectOrder_WhenMultipleLocationsPerShelf");
            using (var merchGroups = plan.GetMerchandisingGroups())
            {

                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking, 
                    productStacksByBlockColour,
                    merchGroups, 
                    planWidth, 
                    planHeight, 
                    planHeightOffset, 
                    bottomLeftLocation.GetPlanogramBlockingGroup(),
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace)
                    .First(b => b.BlockingGroup == bottomLeftLocation.GetPlanogramBlockingGroup());

                Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(2));
                Assert.That(
                    merchBlock.BlockPlacements.ElementAt(0).MerchandisingGroup.SubComponentPlacements.First().FixtureComponent,
                    Is.SameAs(shelf1));
                Assert.That(
                    merchBlock.BlockPlacements.ElementAt(1).MerchandisingGroup.SubComponentPlacements.First().FixtureComponent,
                    Is.SameAs(shelf2));
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacements_ForEachMerchandisingGroup()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0f, 0f) });
            var blockingGroup = plan.Blocking.First().Groups.First();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay1);
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay1);
            for (int i = 0; i < 4; i++) shelf3.AddPosition(bay2);
            var productsInSequence = plan.Products
                .Take(4)
                .Union(plan.Products.Skip(4).Reverse())
                .Union(plan.Products.Skip(8).Take(1))
                .Union(plan.Products.Skip(11));
            var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
            plan.Sequence.Groups.Add(sequenceGroup);
            sequenceGroup.Products.AddRange(productsInSequence.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), 
                    new Dictionary<Int32,Stack<PlanogramProduct>>() { { blockingGroup.Colour, new Stack<PlanogramProduct>(productsInSequence.Reverse()) } },
                    merchGroups,
                    width, 
                    height, 
                    offset,
                    blockingGroup,
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace).First();

                Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(3), "There should be a block placement each for shelf1 and shelf2 and two for shelf3");
                Assert.That(
                    merchBlock.BlockPlacements.Count(bp => bp.MerchandisingGroup == merchGroups.First(m => m.SubComponentPlacements.First().FixtureComponent == shelf1)),
                    Is.EqualTo(1),
                    "There should be one block placement for shelf1");
                Assert.That(
                    merchBlock.BlockPlacements.Count(bp => bp.MerchandisingGroup == merchGroups.First(m => m.SubComponentPlacements.First().FixtureComponent == shelf2)),
                    Is.EqualTo(1),
                    "There should be one block placement for shelf2");
                Assert.That(
                    merchBlock.BlockPlacements.Count(bp => bp.MerchandisingGroup == merchGroups.First(m => m.SubComponentPlacements.First().FixtureComponent == shelf3)),
                    Is.EqualTo(1),
                    "There should be one block placement for shelf3");
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacementsAtCorrectCoords(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0f, 0f) });
            var blockingGroup = blocking.Groups.First();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            var shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf2.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            var shelf3 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf3.GetPlanogramComponent().SubComponents[0].MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay1, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf3.AddPosition(bay2, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay1, plan.AddProduct(productSize));
            var productsInSequence = plan.Products
                .Take(2)
                .Union(plan.Products.Skip(6).Take(2).Reverse())
                .Union(plan.Products.Skip(8).Take(1))
                .Union(plan.Products.Skip(11));
            var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
            plan.Sequence.Groups.Add(sequenceGroup);
            sequenceGroup.Products.AddRange(productsInSequence.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            List<RectValue> expectedBounds;
            if (isBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,100, 96,75),
                        new RectValue(20,4,0,100, 196,75),
                        new RectValue(0,4,0,50, 96,75),
                        new RectValue(70,4,0,50, 96,75),
                    };
                }
                else
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,120, 96,75),
                        new RectValue(0,4,0,120, 196,75),
                        new RectValue(0,4,0,60, 96,75),
                        new RectValue(60,4,0,60, 96,75),
                    };
                }
            }
            else
            {
                Single positionSpacing = 80f / 3f;
                expectedBounds = new List<RectValue>()
                {
                    new RectValue(((20 + positionSpacing) / 2f) - 10,4,65,20,10,10),
                    new RectValue(120 - ((20 + positionSpacing) / 2f) - 10,4,65,20,10,10),
                    new RectValue(0,4,65,10,10,10),
                    new RectValue(110,4,65,10,10,10),
                };
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    new Dictionary<Int32, Stack<PlanogramProduct>>() { { blockingGroup.Colour, new Stack<PlanogramProduct>(productsInSequence.Reverse()) } },
                    merchGroups,
                    width,
                    height,
                    offset,
                    isBlockOfInterest ? blockingGroup : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace).First();

                merchBlock.BlockPlacements.Should().HaveCount(4, "because the top shelf has two position clusters");
                Int32 blockPlacementIndex = 0;
                foreach (var blockPlacement in merchBlock.BlockPlacements)
                {
                    AssertHelper.AreRectValuesEqual(expectedBounds[blockPlacementIndex++], blockPlacement.Bounds,0);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_WhenExceedsBlockSpace_BlockPlacementsHaveMaxWidth_WithOneBlock(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean blockIsOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0f, 0f) });
            var blockingGroup = plan.Blocking.First().Groups.First();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay, plan.AddProduct(productSize));
            var productsInSequence = plan.Products.Take(3).Union(plan.Products.Skip(4).Take(2).Reverse());
            plan.AddSequenceGroup(blockingGroup, productsInSequence);
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            List<RectValue> expectedBlockPlacementSizes;
            if (blockIsOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedBlockPlacementSizes = new List<RectValue>()
                    {
                        new RectValue(0,4,0,110,96,75),
                        new RectValue(0,4,0,100,96,75),
                    };
                }
                else
                {
                    expectedBlockPlacementSizes = new List<RectValue>()
                    {
                        new RectValue(0,4,0,120,96,75),
                        new RectValue(0,4,0,120,96,75),
                    };
                }
            }
            else
            {
                expectedBlockPlacementSizes = new List<RectValue>()
                {
                    new RectValue(0,4,65,30,10,10),
                    new RectValue(0,4,65,20,10,10),
                };
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    new Dictionary<Int32, Stack<PlanogramProduct>>() { { blockingGroup.Colour, new Stack<PlanogramProduct>(productsInSequence.Reverse()) } },
                    merchGroups,
                    width,
                    height,
                    offset,
                    blockIsOfInterest ? blockingGroup : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace).First();

                Assert.That(merchBlock.BlockPlacements, Has.Count.EqualTo(expectedBlockPlacementSizes.Count));
                for (int i = 0; i < merchBlock.BlockPlacements.Count(); i++)
                {
                    AssertHelper.AreRectValuesEqual(expectedBlockPlacementSizes[i], merchBlock.BlockPlacements.ElementAt(i).Bounds);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_BlockPlacementHaveMaxWidth_WithOneBlockAndUnEvenPlacement()
        {
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0f, 0f) });
            var blockingGroup = plan.Blocking.First().Groups.First();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 90, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf3.AddPosition(bay, plan.AddProduct(productSize));
            var productsInSequence = plan.Products
                .Take(3)
                .Union(plan.Products.Skip(4).Take(4).Reverse())
                .Union(plan.Products.Skip(10));
            var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
            plan.Sequence.Groups.Add(sequenceGroup);
            sequenceGroup.Products.AddRange(productsInSequence.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));
            List<RectValue> expectedBounds = new List<RectValue>()
            {
                new RectValue(0,4,0,110, 86,75),
                new RectValue(0,4,0,120, 56,75),
                new RectValue(0,4,0,100, 46,75),
            };
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    new Dictionary<Int32, Stack<PlanogramProduct>>() { { blockingGroup.Colour, new Stack<PlanogramProduct>(productsInSequence.Reverse()) } },
                    merchGroups,
                    width,
                    height,
                    offset,
                    blockingGroup,
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace).First();

                Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(expectedBounds.Count));
                Int32 blockPlacementIndex = 0;
                foreach (var blockPlacement in merchBlock.BlockPlacements)
                {
                    AssertHelper.AreRectValuesEqual(expectedBounds[blockPlacementIndex++], blockPlacement.Bounds);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_BlockPlacementsHaveMaxWidthForBlock_WithComplexBlockShape(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var blockingGroup = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).PlanogramBlockingGroupId = blockingGroup.Id;
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 90, 0));
            var shelf3 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var shelf4 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay1, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay1, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf3.AddPosition(bay1, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf4.AddPosition(bay2, plan.AddProduct(productSize));
            var productsInSequence = plan.Products
                .Take(3)
                .Union(plan.Products.Skip(4).Take(4).Reverse())
                .Union(plan.Products.Skip(10).Take(2))
                .Union(plan.Products.Skip(12));
            var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
            plan.Sequence.Groups.Add(sequenceGroup);
            sequenceGroup.Products.AddRange(productsInSequence.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));
            List<RectValue> expectedBounds;
            if(isBlockOfInterest)
            {
                if(canExceedBlockSpace)
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,110, 86,75),
                        new RectValue(0,4,0,120, 56,75),
                        new RectValue(0,4,0,100, 46,75),
                        new RectValue(0,4,0,120, 46,75),
                    };
                }
                else
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,120, 86,75),
                        new RectValue(0,4,0,120, 56,75),
                        new RectValue(0,4,0,120, 46,75),
                        new RectValue(0,4,0,120, 46,75),
                    };
                }
            }
            else
            {
                expectedBounds = new List<RectValue>()
                {
                    new RectValue(0,4,65,30,10,10),
                    new RectValue(0,4,65,40,10,10),
                    new RectValue(20,4,65,20,10,10),
                    new RectValue(0,4,65,40,10,10),
                };
            }
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    new Dictionary<Int32, Stack<PlanogramProduct>>() { { blockingGroup.Colour, new Stack<PlanogramProduct>(productsInSequence.Reverse()) } },
                    merchGroups,
                    width,
                    height,
                    offset,
                    isBlockOfInterest ? blockingGroup : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace).First();

                Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(expectedBounds.Count));
                Int32 blockPlacementIndex = 0;
                foreach (var blockPlacement in merchBlock.BlockPlacements)
                {
                    AssertHelper.AreRectValuesEqual(expectedBounds[blockPlacementIndex++], blockPlacement.Bounds);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_WhenBlockOfInterestNotNull_PlacementShouldHaveMaxWhiteSpace(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean blockIsOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 4; i++) shelf.AddPosition(bay, plan.AddProduct(productSize));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(2));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(2));
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            var productStack = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(2).Reverse()) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(2).Reverse()) },
            };
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            Dictionary<PlanogramBlockingGroup, RectValue> expectedBoundsByBlock;
            if (blockIsOfInterest)
            {
                if (canExceedSpace)
                {
                    expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                    {
                        { leftBlock, new RectValue(0,4,0,100,196,75) },
                        { rightBlock, new RectValue(20,4,65,20,10,10) },
                    };
                }
                else
                {
                    expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                    {
                        { leftBlock, new RectValue(0,4,0,60,196,75) },
                        { rightBlock, new RectValue(20,4,65,20,10,10) },
                    };
                }
            }
            else
            {
                expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                {
                    { leftBlock, new RectValue(0,4,65,20,10,10) },
                    { rightBlock, new RectValue(20,4,65,20,10,10) },
                };
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking, productStack, merchGroups, width, height, offset, blockIsOfInterest ? leftBlock : null, 
                    canExceedSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                Assert.That(merchBlocks.Count(), Is.EqualTo(2), "There should be a merch block for each blocking group");
                foreach (var merchBlock in merchBlocks)
                {
                    AssertHelper.AreRectValuesEqual(expectedBoundsByBlock[merchBlock.BlockingGroup], merchBlock.BlockPlacements.First().Bounds);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_WhenBlockOfInterestNotNull_PlacementsShouldHaveBlockPercentageSpace(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean blockIsOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 4; i++) shelf.AddPosition(bay, plan.AddProduct(productSize));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(2));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(2));
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            var productStack = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(2).Reverse()) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(2).Reverse()) },
            };
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            Dictionary<PlanogramBlockingGroup, RectValue> expectedBoundsByBlock;
            if (blockIsOfInterest)
            {
                if (canExceedSpace)
                {
                    expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                    {
                        { leftBlock, new RectValue(0,4,0,100,196,75) },
                        { rightBlock, new RectValue(20,4,65,20,10,10) },
                    };

                }
                else
                {
                    expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                    {
                        { leftBlock, new RectValue(0,4,0,60,196,75) },
                        { rightBlock, new RectValue(20,4,65,20,10,10) },
                    };
                }
            }
            else
            {
                expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                {
                    { leftBlock, new RectValue(0,4,65,20,10,10) },
                    { rightBlock, new RectValue(20,4,65,20,10,10) },
                };
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking, productStack, merchGroups, width, height, offset, blockIsOfInterest ? leftBlock : null,
                    canExceedSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                Assert.That(merchBlocks.Count(), Is.EqualTo(2), "There should be a merch block for each blocking group");
                foreach (var merchBlock in merchBlocks)
                {
                    AssertHelper.AreRectValuesEqual(expectedBoundsByBlock[merchBlock.BlockingGroup], merchBlock.BlockPlacements.First().Bounds);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_WhenBlockOfInterestNotNull_PlacementsShouldHaveMaxOfBlockPercentageSpaceAndPlacedSpace(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 12; i++) shelf.AddPosition(bay, plan.AddProduct(productSize));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            rightBlock.Name = "Right";
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(2));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(2).Take(2));
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            var productStack = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(2).Reverse()) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(2).Reverse()) },
            };
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            Dictionary<PlanogramBlockingGroup, RectValue> expectedBoundsByBlock;
            if (isBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                    {
                        { leftBlock, new RectValue(0,4,0,20,196,75) },
                        { rightBlock, new RectValue(20,4,65,20,10,10) },
                    };
                }
                else
                {
                    expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                    {
                        { leftBlock, new RectValue(0,4,0,60,196,75) },
                        { rightBlock, new RectValue(20,4,65,20,10,10) },
                    };
                }
            }
            else
            {
                expectedBoundsByBlock = new Dictionary<PlanogramBlockingGroup, RectValue>()
                {
                    { leftBlock, new RectValue(0,4,65,20,10,10) },
                    { rightBlock, new RectValue(20,4,65,20,10,10) },
                };
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking, productStack, merchGroups, width, height, offset, isBlockOfInterest ? leftBlock : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                Assert.That(merchBlocks.Count(), Is.EqualTo(2), "There should be a merch block for each blocking group");
                foreach (var merchBlock in merchBlocks)
                {
                    AssertHelper.AreRectValuesEqual(
                        expectedBoundsByBlock[merchBlock.BlockingGroup], 
                        merchBlock.BlockPlacements.First().Bounds);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesSequenceGroupWithSequencedProducts()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (int i = 0; i < 4; i++) shelf.AddPosition(bay);
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0f, 0f) });
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { blocking.Groups.First().Colour, new Stack<PlanogramProduct>(plan.Products.Reverse()) },
            };
            plan.Sequence.ApplyFromBlocking(blocking);
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();

            using (var merchandisingGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                        blocking, productStacksByBlockColour, merchandisingGroups, width, height, offset, null, PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace)
                        .First();

                Assert.That(merchBlock.SequenceGroup.Products, Has.Count.EqualTo(4));
                var sequenceProductsOrderedByGtin = merchBlock.SequenceGroup.Products.OrderBy(p => p.Gtin).ToList();
                for (int i = 0; i < 4; i++)
                {
                    Assert.That(sequenceProductsOrderedByGtin.ElementAt(i).SequenceNumber, Is.EqualTo(i + 1));
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_WhenBlockOfInterestIsNull_AllPlacementsShouldHaveSpaceOfPositions(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            for (int i = 0; i < 10; i++) shelf1.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 10; i++) shelf2.AddPosition(bay, plan.AddProduct(productSize));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var leftBlockProducts = plan.Products.Take(8).Union(plan.Products.Skip(10).Take(2).Reverse());
            plan.AddSequenceGroup(leftBlock, leftBlockProducts);
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            var rightBlockProducts = plan.Products.Skip(8).Take(2).Union(plan.Products.Skip(12).Take(8).Reverse());
            plan.AddSequenceGroup(rightBlock,rightBlockProducts);
            plan.UpdatePositionSequenceData();
            plan.ReprocessAllMerchandisingGroups();
            Single blockingWidth, blockingHeight, blockingHeightOffset;
            plan.GetBlockingAreaSize(out blockingHeight, out blockingWidth, out blockingHeightOffset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(leftBlockProducts.Reverse()) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(rightBlockProducts.Reverse()) },
            };
            Dictionary<PlanogramBlockingGroup, List<RectValue>> expectedBlockPlacementsByBlockingGroup;
            if (isBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedBlockPlacementsByBlockingGroup = new Dictionary<PlanogramBlockingGroup, List<RectValue>>()
                    {
                        { leftBlock, new List<RectValue>() { new RectValue(0,4,0,100,96,75), new RectValue(0,4,0,40,96,75) } },
                        { rightBlock, new List<RectValue>() { new RectValue(80,4,65,20,10,10), new RectValue(20,4,65,80,10,10) } },
                    };
                }
                else
                {
                    expectedBlockPlacementsByBlockingGroup = new Dictionary<PlanogramBlockingGroup, List<RectValue>>()
                    {
                        { leftBlock, new List<RectValue>() { new RectValue(0,4,0,80,96,75), new RectValue(0,4,0,60,96,75) } },
                        { rightBlock, new List<RectValue>() { new RectValue(80,4,65,20,10,10), new RectValue(20,4,65,80,10,10) } },
                    };
                }
            }
            else
            {
                expectedBlockPlacementsByBlockingGroup = new Dictionary<PlanogramBlockingGroup, List<RectValue>>()
                {
                    { leftBlock, new List<RectValue>() { new RectValue(0,4,65,80,10,10), new RectValue(0,4,65,20,10,10) } },
                    { rightBlock, new List<RectValue>() { new RectValue(80,4,65,20,10,10), new RectValue(20,4,65,80,10,10) } },
                };
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    blockingWidth,
                    blockingHeight,
                    blockingHeightOffset,
                    isBlockOfInterest ? leftBlock : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                Assert.That(merchBlocks.Count(), Is.EqualTo(2));
                foreach (var merchBlock in merchBlocks)
                {
                    Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(2));
                    List<RectValue> expectedPlacements = expectedBlockPlacementsByBlockingGroup[merchBlock.BlockingGroup];
                    for (int i = 0; i < merchBlock.BlockPlacements.Count(); i++)
			        {
                        AssertHelper.AreRectValuesEqual(expectedPlacements[i], merchBlock.BlockPlacements.ElementAt(i).Bounds);
			        }
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_ShouldCreateBlockPlacements_ForEachPositionCluster(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            for (Int32 i = 0; i < 3; i++) shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10,10,10)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            leftBlock.Name = "Left";
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(1).Union(plan.Products.Skip(2)));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(1).Take(1));
            plan.UpdatePositionSequenceData();
            plan.ReprocessAllMerchandisingGroups();
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(1).Union(plan.Products.Skip(2)).Reverse()) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(1).Take(1)) },
            };
            Single blockingWidth, blockingHeight, blockingHeightOffset;
            plan.GetBlockingAreaSize(out blockingHeight, out blockingWidth, out blockingHeightOffset);
            Dictionary<Int32, List<RectValue>> expectedBlockPlacementBoundsByBlockColour;
            if (isBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedBlockPlacementBoundsByBlockColour = new Dictionary<Int32, List<RectValue>>()
                    {
                        { leftBlock.Colour, new List<RectValue>() { new RectValue(0,4,0,55,196,75), new RectValue(65,4,0,55,196,75) } },
                        { rightBlock.Colour, new List<RectValue>() { new RectValue(55,4,65,10,10,10) } }
                    };
                }
                else
                {
                    expectedBlockPlacementBoundsByBlockColour = new Dictionary<Int32, List<RectValue>>()
                    {
                        { leftBlock.Colour, new List<RectValue>() { new RectValue(0,4,0,30,196,75), new RectValue(90,4,0,30,196,75) } },
                        { rightBlock.Colour, new List<RectValue>() { new RectValue(55,4,65,10,10,10) } }
                    };
                }
            }
            else
            {
                expectedBlockPlacementBoundsByBlockColour = new Dictionary<Int32, List<RectValue>>()
                {
                    { leftBlock.Colour, new List<RectValue>() { new RectValue(0,4,65,10,10,10), new RectValue(110,4,65,10,10,10) } },
                    { rightBlock.Colour, new List<RectValue>() { new RectValue(55,4,65,10,10,10) } }
                };
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    blockingWidth,
                    blockingHeight,
                    blockingHeightOffset,
                    isBlockOfInterest ? leftBlock : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                foreach (var merchBlock in merchBlocks)
                {
                    Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(expectedBlockPlacementBoundsByBlockColour[merchBlock.BlockingGroup.Colour].Count));
                    Int32 blockPlacementIndex = 0;
                    foreach (var blockPlacement in merchBlock.BlockPlacements)
                    {
                        AssertHelper.AreRectValuesEqual(
                            expectedBlockPlacementBoundsByBlockColour[merchBlock.BlockingGroup.Colour][blockPlacementIndex++],
                            blockPlacement.Bounds);
                    }
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_BlockPlacementsShouldGetPlacedSpace_WhenOutsideOfBlockingAreaAndWithinBlockSpace(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.AddPosition(bay1, plan.AddProduct(productSize));
            shelf1.AddPosition(bay1, plan.AddProduct(productSize));
            shelf2.AddPosition(bay2, plan.AddProduct(productSize));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.6f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            leftBlock.Name = "Left";
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f)).GetPlanogramBlockingGroup();
            rightBlock.Name = "Right";
            plan.AddSequenceGroup(leftBlock, plan.Products.Skip(1).Take(1));
            plan.AddSequenceGroup(rightBlock, plan.Products.Take(1).Union(plan.Products.Skip(2)));
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(1).Take(1)) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(1).Union(plan.Products.Skip(2)).Reverse()) },
            };
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single blockingWidth, blockingHeight, blockingHeightOffset;
            plan.GetBlockingAreaSize(out blockingHeight, out blockingWidth, out blockingHeightOffset);
            IEnumerable<RectValue> expectedBounds;
            if (isBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,110,196,75),
                        new RectValue(0,4,0,120,196,75),
                    };
                }
                else
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,65,10,10,10),
                        new RectValue(0,4,0,96,196,75),
                    };
                }
            }
            else
            {
                expectedBounds = new List<RectValue>()
                {
                    new RectValue(0,4,65,10,10,10),
                    new RectValue(0,4,65,10,10,10),
                };
            }
            //plan.TakeSnapshot("PositionSpaceConstructor_BlockPlacementsShouldGetPlacedSpace_WhenOutsideOfBlockingAreaAndWithinBlockSpace");
            using (var merchGroups = plan.GetMerchandisingGroups())
            {

                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    blockingWidth,
                    blockingHeight,
                    blockingHeightOffset,
                    isBlockOfInterest ? rightBlock : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace)
                    .First(b => b.BlockingGroup == rightBlock);

                merchBlock.BlockPlacements.Should().HaveCount(2, "because a block placement should be created for the position outside of block space");
                Int32 i = 0;
                foreach (var blockPlacement in merchBlock.BlockPlacements)
	            {
		            AssertHelper.AreRectValuesEqual(
                                expectedBounds.ElementAt(i),
                                merchBlock.BlockPlacements.ElementAt(i++).Bounds); 
	            }
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacements_ForOneShelf()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking().AddGroup().AddLocation(0, 1, 0, 1);
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf).AddPosition(bay1);
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    plan.Sequence.Groups.ToDictionary(g => g.Colour, g => new Stack<PlanogramProduct>(g.Products.Select(gp => plan.Products.First(p => p.Gtin.Equals(gp.Gtin))))),
                    merchGroups, 
                    planWidth, 
                    planHeight, 
                    planHeightOffset,
                    null,
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                Assert.AreEqual(1, merchBlocks.First().BlockPlacements.Count());
            }
        }

        [Test]
        public void PositionSpaceConstructor_BlockInMiddleOfPegboard_GetsCorrectSpace(
            [Values(BlockInterest.BlockIsNotOfInterest, BlockInterest.BlockIsOfInterest)] BlockInterest blockInterest,
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, size: new WidthHeightDepthValue(120, 200, 4));
            peg.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            peg.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Even;
            #region Blocking layout
            //       _______________
            //      |_______________|
            //      |   |       |   |
            //      |   |       |   |
            //      |___|_______|___|
            //      |_______________|
            #endregion
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.2f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.8f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.2f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.8f, 0.5f, PlanogramBlockingDividerType.Vertical);
            RectValue bottomPositionSpace, middleLeftPositionSpace, middleMiddlePositionSpace, middleRightPositionSpace, topPositionSpace;
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();
                var bottomPosition = merchGroup.InsertPositionPlacement(plan.AddProduct(productSize));
                var middleLeftPosition = merchGroup.InsertPositionPlacement(plan.AddProduct(productSize),bottomPosition, PlanogramPositionAnchorDirection.Above);
                var middleMiddlePosition = merchGroup.InsertPositionPlacement(plan.AddProduct(productSize), middleLeftPosition, PlanogramPositionAnchorDirection.ToRight);
                var middleRightPosition = merchGroup.InsertPositionPlacement(plan.AddProduct(productSize), middleMiddlePosition, PlanogramPositionAnchorDirection.ToRight);
                var topPosition = merchGroup.InsertPositionPlacement(plan.AddProduct(productSize), middleMiddlePosition, PlanogramPositionAnchorDirection.Above);
                merchGroup.Process();
                merchGroup.ApplyEdit();
                bottomPositionSpace = bottomPosition.GetReservedSpace();
                middleLeftPositionSpace = middleLeftPosition.GetReservedSpace();
                middleMiddlePositionSpace = middleMiddlePosition.GetReservedSpace();
                middleRightPositionSpace = middleRightPosition.GetReservedSpace();
                topPositionSpace = topPosition.GetReservedSpace();
            }
            var middleGroup = blocking.Locations.First(l => l.X.EqualTo(0.2f) && l.Y.EqualTo(0.2f)).GetPlanogramBlockingGroup();
            middleGroup.Name = "Middle";
            plan.Sequence.ApplyFromBlocking(blocking);
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var productStacksByBlockColour = plan.Sequence.Groups
                .ToDictionary(g => g.Colour,g => new Stack<PlanogramProduct>(g.Products.Select(p => plan.Products.First(prod => prod.Gtin.Equals(p.Gtin)))));
            RectValue expectedMiddleGroupBounds;
            if (isBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedMiddleGroupBounds = new RectValue(
                        (middleMiddlePositionSpace.X + middleMiddlePositionSpace.Width / 2) - 50,
                        0,
                        4,
                        100,
                        200,
                        71);
                }
                else
                {
                    expectedMiddleGroupBounds = new RectValue(24,39.8f,4,72,120,71);
                }
            }
            else
            {
                expectedMiddleGroupBounds = new RectValue(
                    middleMiddlePositionSpace.X, middleMiddlePositionSpace.Y, 4, 10, 10, 10);
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                //plan.Parent.SaveAs(1, @"C:\users\usr095\beforeTest.pog").Dispose();

                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking, 
                    productStacksByBlockColour, 
                    merchGroups, 
                    width, 
                    height, 
                    offset, 
                    isBlockOfInterest ? middleGroup : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                AssertHelper.AreRectValuesEqual(
                    expectedMiddleGroupBounds,
                    merchBlocks.First(b => b.BlockingGroup == middleGroup).BlockPlacements.First().Bounds);
                
            }
        }

        public enum BlockOfInterest { FrontBlockOfInterest, BackBlockOfInterest, NoBlockOfInterest }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacements_ChestWithTwoBlocks(
            [Values(ExceedBlockSpace.CanExceedBlockSpace, ExceedBlockSpace.CannotExceedBlockSpace)] ExceedBlockSpace exceedBlockSpace,
            [Values(BlockOfInterest.FrontBlockOfInterest, BlockOfInterest.BackBlockOfInterest, BlockOfInterest.NoBlockOfInterest)] BlockOfInterest blockOfInterest)
        {
            Boolean canExceedBlockSpace = exceedBlockSpace == ExceedBlockSpace.CanExceedBlockSpace;
            /*       ___________
             *      |           |
             *      |           |               Expecting the "top" block to be spaced "behind" the "bottom" block.
             *      |           |
             *      |+=========+|
             *   -~-||-~-~-~-~-||-~ Divider
             *      ||_________||
             *       +=========+    (Chest)
             * 
             */

            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var chest = bay1.AddFixtureComponent(PlanogramComponentType.Chest);
            chest.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            chest.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Even;
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var chestMerchGroup = merchGroups.First();
                var frontPosition = chestMerchGroup.InsertPositionPlacement(plan.AddProduct(productSize));
                var backPosition = chestMerchGroup.InsertPositionPlacement(plan.AddProduct(productSize),frontPosition, PlanogramPositionAnchorDirection.Behind);
                chestMerchGroup.Process();
                chestMerchGroup.ApplyEdit();
            }
            var blocking = plan.AddBlocking();
            Single dividerY = 0.167f;
            blocking.Dividers.Add(0f, dividerY, PlanogramBlockingDividerType.Horizontal);
            var frontBlock = blocking.Locations.First(l => l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            var backBlock = blocking.Locations.First(l => l.Y.EqualTo(0.167f)).GetPlanogramBlockingGroup();
            plan.Sequence.ApplyFromBlocking(plan.Blocking.First());
            plan.UpdatePositionSequenceData();
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            RectValue expectedFrontBlockBounds;
            if (blockOfInterest == BlockOfInterest.FrontBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedFrontBlockBounds = new RectValue(4,4,14,112,46,57);
                }
                else
                {
                    expectedFrontBlockBounds = new RectValue(4,4,37.4f,112,46,33.6f);
                }
            }
            else
            {
                expectedFrontBlockBounds = new RectValue(51,4,61,10,10,10);
            }
            RectValue expectedBackBlockBounds;
            if (blockOfInterest == BlockOfInterest.BackBlockOfInterest)
            {
                if (canExceedBlockSpace)
                {
                    expectedBackBlockBounds = new RectValue(4, 4, 4, 112, 46, 57);
                }
                else
                {
                    expectedBackBlockBounds = new RectValue(4, 4, 4f, 112, 46, 33.4f);
                }
            }
            else
            {
                expectedBackBlockBounds = new RectValue(51, 4, 4, 10, 10, 10);
            }

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    plan.Sequence.Groups.ToDictionary(g => g.Colour, g => new Stack<PlanogramProduct>(g.Products.Select(gp => plan.Products.First(p => p.Gtin.Equals(gp.Gtin))))),
                    merchGroups,
                    planWidth,
                    planHeight,
                    planHeightOffset,
                    blockOfInterest == BlockOfInterest.FrontBlockOfInterest ? frontBlock : blockOfInterest == BlockOfInterest.BackBlockOfInterest ? backBlock : null,
                    canExceedBlockSpace ? PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace : PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace);

                AssertHelper.AreRectValuesEqual(
                    expectedFrontBlockBounds,
                    merchBlocks.First(g => g.BlockingGroup == frontBlock).BlockPlacements.First().Bounds, 1); // Low precision due to accuracy errors in space transformations.
                AssertHelper.AreRectValuesEqual(
                    expectedBackBlockBounds,
                    merchBlocks.First(g => g.BlockingGroup == backBlock).BlockPlacements.First().Bounds, 1);
            }
        }



        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacements_Chest()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            PlanogramFixtureItem bay = plan.AddFixtureItem();
            PlanogramFixtureComponent chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            PlanogramProduct planogramProduct1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 65));
            PlanogramProduct planogramProduct2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct3 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProduct4 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            using (var mgs = plan.GetMerchandisingGroups())
            {
                foreach (var mg in mgs)
                {
                    if (mg.StrategyZ == PlanogramSubComponentZMerchStrategyType.FrontStacked)
                    {
                        PlanogramPositionPlacement planogramPositionPlacement = mg.InsertPositionPlacement(plan.Products[0]);
                        PlanogramPositionPlacement planogramPositionPlacement1 = mg.InsertPositionPlacement(plan.Products[1], planogramPositionPlacement, PlanogramPositionAnchorDirection.ToRight);
                        mg.Process(); mg.ApplyEdit();
                    }
                }
            }
            foreach (var sub in plan.Components.SelectMany(c => c.SubComponents)) sub.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.LeftStacked;
            plan.CreateAssortmentFromProducts();
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            var blockGroup = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            plan.Sequence.Groups[0].AddSequenceGroupProducts(plan.Products);
            blockGroup.BlockPlacementPrimaryType = PlanogramBlockingGroupPlacementType.FrontToBack;
            blockGroup.BlockPlacementSecondaryType = PlanogramBlockingGroupPlacementType.RightToLeft;
            blockGroup.BlockPlacementTertiaryType = PlanogramBlockingGroupPlacementType.BottomToTop;
            plan.UpdatePositionSequenceData();
            plan.ReprocessAllMerchandisingGroups();
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);
            //plan.TakeSnapshot("PositionSpaceConstructor_CreatesBlockPlacements_Chest");
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    plan.Sequence.Groups.ToDictionary(g => g.Colour, g => new Stack<PlanogramProduct>(g.Products.Select(gp => plan.Products.First(p => p.Gtin.Equals(gp.Gtin))))),
                    merchGroups,
                    planWidth,
                    planHeight,
                    planHeightOffset,
                    blockGroup,
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace);

                Assert.AreEqual(1, merchBlocks.First().BlockPlacements.Count(), "Only one block placement should be created.");
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacements_ForMultipleShelves(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace, PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)] PlanogramMerchandisingSpaceConstraintType spaceConstraintType)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            WidthHeightDepthValue productSize1 = new WidthHeightDepthValue(10, 10, 10);
            WidthHeightDepthValue productSize2 = new WidthHeightDepthValue(15, 10, 10);
            var plan = "Package".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var blockingGroup = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).PlanogramBlockingGroupId = blockingGroup.Id;
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 90, 0));
            var shelf3 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var shelf4 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay1, plan.AddProduct(productSize1));
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay1, plan.AddProduct(productSize2));
            for (int i = 0; i < 4; i++) shelf3.AddPosition(bay1, plan.AddProduct(productSize1));
            for (int i = 0; i < 4; i++) shelf4.AddPosition(bay2, plan.AddProduct(productSize1));
            var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup);
            plan.Sequence.Groups.Add(sequenceGroup);
            sequenceGroup.Products.AddRange(plan.Products.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));
            shelf1.AddPosition(bay1, plan.AddProduct(productSize1));
            shelf2.AddPosition(bay1, plan.AddProduct(productSize1));
            shelf2.AddPosition(bay1, plan.AddProduct(productSize1));
            shelf3.AddPosition(bay1, plan.AddProduct(productSize1));
            shelf3.AddPosition(bay1, plan.AddProduct(productSize1));
            shelf3.AddPosition(bay1, plan.AddProduct(productSize1));
            shelf4.AddPosition(bay2, plan.AddProduct(productSize1));
            #region Expectations
            List<RectValue> expectedBounds;
            if (isBlockOfInterest)
            {
                if (spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,110, 86,75),
                        new RectValue(0,4,0,100, 56,75),
                        new RectValue(0,4,0,90, 46,75),
                        new RectValue(0,4,0,110, 46,75),
                    };
                }
                else if (spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation)
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,90, 86,75),
                        new RectValue(0,4,0,90, 56,75),
                        new RectValue(0,4,0,90, 46,75),
                        new RectValue(0,4,0,110, 46,75),
                    };
                }
                else
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,120, 86,75),
                        new RectValue(0,4,0,120, 56,75),
                        new RectValue(0,4,0,120, 46,75),
                        new RectValue(0,4,0,120, 46,75),
                    };
                }
            }
            else
            {
                expectedBounds = new List<RectValue>()
                {
                    new RectValue(0,4,65,40,10,10),
                    new RectValue(0,4,65,60,10,10),
                    new RectValue(0,4,65,40,10,10),
                    new RectValue(0,4,65,40,10,10),
                };
            } 
            #endregion
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            //plan.TakeSnapshot("PositionSpaceConstructor_CreatesBlockPlacements_ForMultipleShelves");
            
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    new Dictionary<Int32, Stack<PlanogramProduct>>() { { blockingGroup.Colour, new Stack<PlanogramProduct>(plan.Products.Reverse()) } },
                    merchGroups,
                    width,
                    height,
                    offset,
                    isBlockOfInterest ? blockingGroup : null,
                    spaceConstraintType)
                    .First(g => g.BlockingGroup == blockingGroup);

                Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(expectedBounds.Count));
                Int32 blockPlacementIndex = 0;
                foreach (var blockPlacement in merchBlock.BlockPlacements)
                {
                    blockPlacement.Bounds.Should().Be(
                        expectedBounds[blockPlacementIndex++], 
                        $"because block placement {blockPlacementIndex} should have these bounds");
                }
            }
        }


        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacements_ForMultipleShelves_MultipleCompactPlacementGroupings(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)] BlockInterest blockInterest,
            [Values(PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace, PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)] PlanogramMerchandisingSpaceConstraintType spaceConstraintType)
        {
            Boolean isBlockOfInterest = blockInterest == BlockInterest.BlockIsOfInterest;
            WidthHeightDepthValue productSize1 = new WidthHeightDepthValue(10, 10, 10);
            WidthHeightDepthValue productSize2 = new WidthHeightDepthValue(15, 10, 10);
            var plan = "Package".CreatePackage().AddPlanogram();
            var blocking = plan.AddBlocking();

            blocking.Dividers.Add(0.25f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.75f, 0f, PlanogramBlockingDividerType.Vertical);

            var blockingGroupCompactPlacementGroupings = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0f)).PlanogramBlockingGroupId = blockingGroupCompactPlacementGroupings.Id;

            var blockingGroup2 = blocking.Locations.First(l => l.X.EqualTo(0.25f)).GetPlanogramBlockingGroup();
            var blockingGroup3 = blocking.Locations.First(l => l.X.EqualTo(0.75f)).GetPlanogramBlockingGroup();

            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 90, 0));
            
            var shelf1Bay2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2Bay2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 90, 0));
            
            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay1, plan.AddProduct(productSize1));
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay1, plan.AddProduct(productSize2));
            for (int i = 0; i < 4; i++) shelf1Bay2.AddPosition(bay2, plan.AddProduct(productSize1));
            for (int i = 0; i < 4; i++) shelf2Bay2.AddPosition(bay2, plan.AddProduct(productSize1));

            var sequenceGroup = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroupCompactPlacementGroupings);
            plan.Sequence.Groups.Add(sequenceGroup);
            sequenceGroup.Products.AddRange(plan.Products.Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));
            List<PlanogramProduct> compactBlockingGroupProducts = plan.Products.ToList();
            compactBlockingGroupProducts.Reverse();

            for (int i = 0; i < 4; i++) shelf1.AddPosition(bay1, plan.AddProduct(productSize1));
            for (int i = 0; i < 4; i++) shelf2.AddPosition(bay1, plan.AddProduct(productSize1));

            var sequenceGroup2 = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup2);
            plan.Sequence.Groups.Add(sequenceGroup2);
            sequenceGroup2.Products.AddRange(plan.Products.Skip(16).Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));

            for (int i = 0; i < 4; i++) shelf1Bay2.AddPosition(bay2, plan.AddProduct(productSize1));
            for (int i = 0; i < 2; i++) shelf2Bay2.AddPosition(bay2, plan.AddProduct(productSize1));

            var sequenceGroup3 = PlanogramSequenceGroup.NewPlanogramSequenceGroup(blockingGroup3);
            plan.Sequence.Groups.Add(sequenceGroup3);
            sequenceGroup3.Products.AddRange(plan.Products.Skip(24).Select(PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct));

            List<RectValue> expectedBounds;
            if (isBlockOfInterest)
            {
                if (spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,80, 86,75),
                        new RectValue(0,4,0,80, 86,75),
                        new RectValue(0,4,0,100, 106,75),
                        new RectValue(0,4,0,80, 106,75),
                    };
                }
                else if (spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation)
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,80, 86,75),
                        new RectValue(0,4,0,80, 86,75),
                        new RectValue(0,4,0,80, 106,75),
                        new RectValue(0,4,0,80, 106,75),
                    };
                }
                else
                {
                    expectedBounds = new List<RectValue>()
                    {
                        new RectValue(0,4,0,60, 86,75),
                        new RectValue(0,4,0,60, 86,75),
                        new RectValue(0,4,0,60, 106,75),
                        new RectValue(0,4,0,60, 106,75),
                    };
                }
            }
            else
            {
                expectedBounds = new List<RectValue>()
                {
                    new RectValue(0,4,65,40,10,10),
                    new RectValue(0,4,65,40,10,10),
                    new RectValue(0,4,65,40,10,10),
                    new RectValue(0,4,65,60,10,10),
                };
            }

            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            //plan.TakeSnapshot("PositionSpaceConstructor_CreatesBlockPlacements_ForMultipleShelves_MultipleCompactPlacementGroupings");
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    new Dictionary<Int32, Stack<PlanogramProduct>>() { { blockingGroupCompactPlacementGroupings.Colour, new Stack<PlanogramProduct>(compactBlockingGroupProducts) } },
                    merchGroups,
                    width,
                    height,
                    offset,
                    isBlockOfInterest ? blockingGroupCompactPlacementGroupings : null,
                    spaceConstraintType).First();

                Assert.That(merchBlock.BlockPlacements.Count(), Is.EqualTo(expectedBounds.Count));
                Int32 blockPlacementIndex = 0;
                foreach (var blockPlacement in merchBlock.BlockPlacements)
                {
                    AssertHelper.AreRectValuesEqual(expectedBounds[blockPlacementIndex++], blockPlacement.Bounds);
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacementsForEachBlockMerchGroup(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)]
            BlockInterest blockInterest,
            [Values(PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace, PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)]
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,75,0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for(var i = 0; i < 10; i++) shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceGroupProducts(plan.Products);
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { blocking.Groups[0].Colour, new Stack<PlanogramProduct>(plan.Products) }
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking, 
                    productStacksByBlockColour, 
                    merchGroups, 
                    width, 
                    height, 
                    offset, 
                    blockInterest == BlockInterest.BlockIsOfInterest ? blocking.Groups[0] : null, 
                    spaceConstraintType)
                    .First();

                if(blockInterest == BlockInterest.BlockIsOfInterest)
                {
                    merchBlock.BlockPlacements.Should().HaveCount(
                        3, "because a block placement should be created for each merchandising group in the block when the block is of interest");
                }
                else
                {
                    merchBlock.BlockPlacements.Should().HaveCount(
                        1, "because a block placement should be created only for the merchandising groups that have positions when the block is not of interest");
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacementsForEachBlockMerchGroup_WhenOtherBlockPositionsAreOnMerchGroup(
            [Values(BlockInterest.BlockIsOfInterest, BlockInterest.BlockIsNotOfInterest)]
            BlockInterest blockInterest,
            [Values(PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace, PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)]
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (var i = 0; i < 10; i++) shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf3.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.8f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.8f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(10));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(10));
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(10)) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(10)) }
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    width,
                    height,
                    offset,
                    blockInterest == BlockInterest.BlockIsOfInterest ? blocking.Groups[0] : null,
                    spaceConstraintType)
                    .First(b => b.BlockingGroup == blocking.Groups[0]);

                if (blockInterest == BlockInterest.BlockIsOfInterest)
                {
                    merchBlock.BlockPlacements.Should().HaveCount(
                        3, "because a block placement should be created for each merchandising group in the block when the block is of interest");
                }
                else
                {
                    merchBlock.BlockPlacements.Should().HaveCount(
                        1, "because a block placement should be created only for the merchandising groups that have positions when the block is not of interest");
                }
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacementsWithBlockSpace_WhenBlockHasMissingPositions(
            [Values(PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace, PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)]
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (var i = 0; i < 10; i++) shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf3.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.8f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.8f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(10));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(10));
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(10)) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(10)) }
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    width,
                    height,
                    offset,
                    blocking.Groups[0],
                    spaceConstraintType)
                    .First(b => b.BlockingGroup == blocking.Groups[0]);

                merchBlock.BlockPlacements.Should().HaveCount(
                    3, "because a block placement should be created for each merchandising group in the block when the block is of interest");
                Single expectedFirstBlockWidth =
                    spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace ?
                    120 :
                    100;
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should().Be(
                    new RectValue(0, 4, 0, expectedFirstBlockWidth, 71, 75),
                    "because the first block placement should span the entire shelf");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 96, 71, 75),
                    "because the second block placement should have all the white space available");
                merchBlock.BlockPlacements.ElementAt(2).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 96, 46, 75),
                    "because the third block placement should have all the white space available");
            }
        }

        [Test]
        public void PositionSpaceConstructor_CreatesBlockPlacementsWithWhiteSpace_WhenBlockHasMissingPositions(
            [Values(PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace, PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)]
            PlanogramMerchandisingSpaceConstraintType spaceConstraintType)
        {
            Assert.Ignore("At present block placements are created with the full merchandising space of the merchandising group within the block, when no positions are present on the merchandising groups");
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (var i = 0; i < 10; i++) shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            for (var i = 0; i < 5; i++) shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            for (var i = 0; i < 5; i++) shelf3.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.8f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.8f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(10));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(10));
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(10)) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(10)) }
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    width,
                    height,
                    offset,
                    blocking.Groups[0],
                    spaceConstraintType)
                    .First();

                merchBlock.BlockPlacements.Should().HaveCount(
                    3, "because a block placement should be created for each merchandising group in the block when the block is of interest");
                Single expectedFirstBlockWidth =
                    spaceConstraintType == PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace ?
                    120 :
                    100;
                merchBlock.BlockPlacements.ElementAt(0).Bounds.Should().Be(
                    new RectValue(0, 4, 0, expectedFirstBlockWidth, 71, 75),
                    "because the first block placement should span the entire shelf");
                merchBlock.BlockPlacements.ElementAt(1).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 50, 71, 75),
                    "because the second block placement should have all the white space available");
                merchBlock.BlockPlacements.ElementAt(2).Bounds.Should().Be(
                    new RectValue(0, 4, 0, 50, 46, 75),
                    "because the third block placement should have all the white space available");
            }
        }

        #endregion
    }
}
