﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31804 : A.Kuszyk
//  Created.
// V8-32505 : A.Kuszyk
//  Added intitial tests for sequence sub-groups.
// V8-32612 : D.Pleasance
//  Refactored PlanogramMerchandisingBlock constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.UnitTests.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.Helpers;
using FluentAssertions;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public class PlanogramMerchandisingBlockTests : Galleria.Framework.Planograms.UnitTests.Model.TestBase
    {
        [Test]
        public void Layout_WhenMaxUnitsRule_ProductShouldBePlacedAtMaxUnits()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents[0].MerchandisableHeight = 13f;
            plan.AddProduct(new WidthHeightDepthValue(6, 8, 5));
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceProducts(plan.Products);
            plan.CreateAssortmentFromProducts();
            plan.Assortment.Products[0].Units = 100;
            plan.Assortment.Products[0].MaxListUnits = 12;
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 20);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(plan.Blocking[0], plan.Sequence, merchGroups, width, height, offset, assortmentRuleEnforcer:new AssortmentRuleEnforcer()).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin,
                    plan,
                    true,
                    new Stack<PlanogramProduct>(plan.Products.Reverse()),
                    PlanogramMerchandisingInventoryChangeType.ByFacings));
                merchGroups.ApplyEdit();

                plan.Positions[0].TotalUnits.Should().BeLessOrEqualTo(12, "because there is a max units rule place");
            }
        }

        [Test]
        public void Layout_WhenByFacings_IncreasesInventoryByFacings()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            shelf.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceProducts(plan.Products);
            plan.CreateAssortmentFromProducts();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 6);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(plan.Blocking[0], plan.Sequence, merchGroups, width, height, offset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin, 
                    plan, 
                    true, 
                    new Stack<PlanogramProduct>(plan.Products.Reverse()),
                    PlanogramMerchandisingInventoryChangeType.ByFacings));
                merchGroups.ApplyEdit();

                plan.Positions.Should().OnlyContain(p => p.TotalUnits == 6, "because each position should end up with 6 units in total");
                plan.Positions.Should().OnlyContain(p => p.FacingsWide == 6, "because each position should end up with 6 facings wide in total");
            }
        }

        [Test]
        public void Layout_WhenByFacings_IncreasesOriginalUnitsByUnitsThenByFacings()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var prod1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var prod2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var pos1 = shelf.AddPosition(prod1);
            var pos2 = shelf.AddPosition(prod2);
            pos1.FacingsDeep = 5;
            pos2.FacingsDeep = 6;
            plan.ReprocessAllMerchandisingGroups();
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceProducts(plan.Products);
            plan.UpdatePositionSequenceData();
            plan.CreateAssortmentFromProducts();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => p.TotalUnits * 6);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(plan.Blocking[0], plan.Sequence, merchGroups, width, height, offset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin,
                    plan,
                    true,
                    new Stack<PlanogramProduct>(plan.Products.Reverse()),
                    PlanogramMerchandisingInventoryChangeType.ByFacings));
                merchGroups.ApplyEdit();

                plan.Positions.Should().OnlyContain(p => p.FacingsWide == 6, "because each position should end up with 6 facings wide in total");
                plan.Positions.First(p => p.GetPlanogramProduct() == prod1).TotalUnits
                    .Should().Be(30, "because the product was only 5 units deep to start off with");
                plan.Positions.First(p => p.GetPlanogramProduct() == prod2).TotalUnits
                    .Should().Be(36, "because the product was 6 units deep to start off with");
            }
        }

        [Test]
        public void Layout_WhenByFacings_ShouldLeaveOriginalUnitsAlone()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var prod1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var prod2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var pos1 = shelf.AddPosition(prod1);
            var pos2 = shelf.AddPosition(prod2);
            pos1.FacingsWide = 3;
            pos2.FacingsWide = 3;
            pos2.FacingsDeep = 2;
            plan.ReprocessAllMerchandisingGroups();
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceProducts(plan.Products);
            plan.UpdatePositionSequenceData();
            plan.CreateAssortmentFromProducts();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = new Dictionary<String, Int32>()
            {
                { prod1.Gtin, 6 },
                { prod2.Gtin, 12 },
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(plan.Blocking[0], plan.Sequence, merchGroups, width, height, offset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin,
                    plan,
                    true,
                    new Stack<PlanogramProduct>(plan.Products.Reverse()),
                    PlanogramMerchandisingInventoryChangeType.ByFacings));
                merchGroups.ApplyEdit();

                plan.Positions.Should().OnlyContain(p => p.FacingsWide == 6, "because each position should end up with 6 facings wide in total");
                plan.Positions.First(p => p.GetPlanogramProduct() == prod1).TotalUnits
                    .Should().Be(6, "because the product should have 6 facings wide and 1 deep");
                plan.Positions.First(p => p.GetPlanogramProduct() == prod2).TotalUnits
                    .Should().Be(12, "because the product should have 6 facings wide and 2 deep");
            }
        }

        [Test]
        public void Layout_WhenBlockIsHorizontal_ShouldCompactBlockSpace_UsingBlockingSpace()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < 10; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.1f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.5f, PlanogramBlockingDividerType.Vertical);
            var topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0.1f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(topLeftBlock, plan.Products);
            plan.CreateAssortmentFromProducts();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 1);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin, plan, true, new Stack<PlanogramProduct>(plan.Products.Reverse())));
                merchGroups.ApplyEdit();

                shelf1.GetPositions().Should().HaveCount(
                    5, "because products should be distributed evenly across the shelves");
                shelf2.GetPositions().Should().HaveCount(
                    5, "because products should be distributed evenly across the shelves");
            }
        }

        [Test]
        public void Layout_WhenBlockIsHorizontal_ShouldCompactBlockSpace_UsingPositionSpace()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < 5; i++) shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            for (Int32 i = 0; i < 5; i++) shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.1f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.5f, PlanogramBlockingDividerType.Vertical);
            var topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0.1f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(topLeftBlock, plan.Products);
            plan.CreateAssortmentFromProducts();
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 1);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    new Dictionary<Int32,Stack<PlanogramProduct>>()
                    {
                        { topLeftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Reverse())}
                    },
                    merchGroups,
                    width, 
                    height, 
                    offset,
                    topLeftBlock,
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin, plan, true, new Stack<PlanogramProduct>(plan.Products.Reverse())));
                merchGroups.ApplyEdit();

                shelf1.GetPositions().Should().HaveCount(
                    5, "because products should be distributed evenly across the shelves");
                shelf2.GetPositions().Should().HaveCount(
                    5, "because products should be distributed evenly across the shelves");
            }
        }

        [Test]
        public void Layout_ShouldCompactSubGroups_ByPushingWholeSubGroupOut()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,100,0));
            for (Int32 i = 0; i < 10; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceGroupProducts(plan.Products);
            plan.Sequence.Groups[0].SubGroups.Add(
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(
                    plan.Sequence.Groups[0].Products.Skip(5)));
            plan.CreateAssortmentFromProducts();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 1);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin, plan, true, new Stack<PlanogramProduct>(plan.Products.Reverse())));

                merchGroups.ApplyEdit();
            }

            //plan.TakeSnapshot("Layout_ShouldCompactSubGroups_ByPushingWholeSubGroupOut");
            plan.Positions.Should().HaveCount(10, "all products should be placed");
            shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .Should().HaveCount(5, "the first five products were not in a sub group");
            shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .Should().HaveCount(5, "the second five products were in a sub group");
        }

        [Test]
        public void Layout_ShouldCompactSubGroupedProductsEvenly()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < 10; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceGroupProducts(plan.Products);
            plan.CreateAssortmentFromProducts();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 1);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin, plan, true, new Stack<PlanogramProduct>(plan.Products.Reverse())));

                merchGroups.ApplyEdit();
            }

            //plan.TakeSnapshot("Layout_ShouldCompactSubGroups_ByPushingWholeSubGroupOut");
            plan.Positions.Should().HaveCount(10, "all products should be placed");
            shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .Should().HaveCount(5, "the first five products were not in a sub group");
            shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .Should().HaveCount(5, "the second five products were in a sub group");
        }

        [Test]
        public void Layout_ShouldCompactBlockSpace()
        {
            var productSize = new WidthHeightDepthValue(25, 10, 10);
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 20, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 40, 0));
            var shelf4 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 80, 0));
            for (int i = 0; i < 12; i++) plan.AddProduct(productSize);
            plan.CreateAssortmentFromProducts();
            var blocking = plan.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            plan.Sequence.Groups.First().AddSequenceGroupProducts(plan.Products);
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 1);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin, plan, true, new Stack<PlanogramProduct>(plan.Products.Reverse())));

                merchGroups.ApplyEdit();
            }
            //plan.TakeSnapshot("Layout_ShouldCompactBlockSpace");

            shelf1.GetPositions().Should().HaveCount(3, "because there should be 3 products on shelf 1");
            shelf2.GetPositions().Should().HaveCount(3, "because there should be 3 products on shelf 2");
            shelf3.GetPositions().Should().HaveCount(3, "because there should be 3 products on shelf 3");
            shelf4.GetPositions().Should().HaveCount(3, "because there should be 3 products on shelf 4");
        }

        [Test]
        public void Layout_CompactsBlockSpace_ShouldDistributeProductsEvenly_UsingBlocking()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            for (int i = 0; i < 6; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(blocking.Groups[0], plan.Products);
            plan.CreateAssortmentFromProducts();
            using(var merchGroups = plan.GetMerchandisingGroups())
	        {
		        var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset).First();
                var context = new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1),
                    plan,
                    /*shouldCompactSpace*/ true,
                    new Stack<PlanogramProduct>(plan.Products.Reverse()));

                merchBlock.Layout(context);
                merchGroups.ApplyEdit();

                plan.Positions.Should().HaveCount(6, "because all products should be placed");
                shelf1.GetPositions().Should().HaveCount(2, "because 2 products should be placed per shelf");
                shelf2.GetPositions().Should().HaveCount(2, "because 2 products should be placed per shelf");
                shelf3.GetPositions().Should().HaveCount(2, "because 2 products should be placed per shelf");
	        }
        }

        [Test]
        public void Layout_CompactsBlockSpace_ShouldDistributeProductsEvenly_UsingPositions()
        {
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            for (int i = 0; i < 6; i++) shelf1.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 2; i++) shelf2.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 4; i++) shelf3.AddPosition(bay, plan.AddProduct(productSize));
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(
                blocking.Groups[0], 
                plan.Products
                .Take(6)
                .Union(plan.Products.Skip(6).Take(2).Reverse())
                .Union(plan.Products.Skip(8).Take(4)));
            plan.CreateAssortmentFromProducts();
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { 
                    blocking.Groups.First().Colour, 
                    new Stack<PlanogramProduct>(plan.Products
                        .OrderByDescending(p => 
                            plan.Sequence.Groups[0].Products.First(ap => ap.Gtin.Equals(p.Gtin)).SequenceNumber))
                }
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking, 
                    productStacksByBlockColour,
                    merchGroups, 
                    width, 
                    height, 
                    offset,
                    blocking.Groups.First(),
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace).First();
                var context = new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1),
                    plan,
                    /*shouldCompactSpace*/ true,
                    productStacksByBlockColour.First().Value);
                
                //plan.Parent.SaveAs(1, @"C:\users\usr145\before.pog");
                merchBlock.Layout(context);
                merchGroups.ApplyEdit();
                //plan.Parent.SaveAs(1, @"C:\users\usr145\after.pog");

                Assert.IsTrue(plan.Products.All(p => p.GetPlanogramPositions().Any()),"All products should be placed");
                Assert.That(shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(4));
                Assert.That(shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(4));
                Assert.That(shelf3.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(4));
            }
        }

        [Test]
        public void Layout_CompactsBlockSpace_ShouldDistributeProductsEvenly_UsingPositionsWithBigDogLegNotInBlocking()
        {
            var productSize = new WidthHeightDepthValue(12, 10, 10);
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.333f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            for (int i = 0; i < 6; i++) shelf1.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 2; i++) shelf2.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 1; i++) shelf3.AddPosition(bay, plan.AddProduct(productSize));
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(
                leftBlock,
                plan.Products
                .Take(6)
                .Union(plan.Products.Skip(6).Take(2).Reverse())
                .Union(plan.Products.Skip(8).Take(1)));
            plan.CreateAssortmentFromProducts();
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { 
                    leftBlock.Colour, 
                    new Stack<PlanogramProduct>(plan.Products
                        .OrderByDescending(p => 
                            plan.Sequence.Groups[0].Products.First(ap => ap.Gtin.Equals(p.Gtin)).SequenceNumber))
                }
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    width,
                    height,
                    offset,
                    leftBlock,
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace).First();
                var context = new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1),
                    plan,
                    /*shouldCompactSpace*/ true,
                    productStacksByBlockColour.First().Value);

                //plan.Parent.SaveAs(1, @"C:\users\usr145\before.pog");
                merchBlock.Layout(context);
                merchGroups.ApplyEdit();
                //plan.Parent.SaveAs(1, @"C:\users\usr145\after.pog");

                Assert.IsTrue(plan.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
                Assert.That(shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(3));
                Assert.That(shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(3));
                Assert.That(shelf3.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(3));
            }
        }

        [Test]
        public void Layout_CompactsBlockSpace_ShouldDistributeProductsEvenly_UsingPositionsWithBigDogLegInBlocking()
        {
            var productSize = new WidthHeightDepthValue(12, 10, 10);
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0f, 0.33f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.333f, 0.5f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0.33f)).GetPlanogramBlockingGroup();
            blocking.Locations.First(l => l.X.EqualTo(0) && l.Y.EqualTo(0)).PlanogramBlockingGroupId = leftBlock.Id;
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            for (int i = 0; i < 12; i++) shelf1.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 3; i++) shelf2.AddPosition(bay, plan.AddProduct(productSize));
            for (int i = 0; i < 1; i++) shelf3.AddPosition(bay, plan.AddProduct(productSize));
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(
                leftBlock,
                plan.Products
                .Take(12)
                .Union(plan.Products.Skip(12).Take(3).Reverse())
                .Union(plan.Products.Skip(15).Take(1)));
            plan.CreateAssortmentFromProducts();
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { 
                    leftBlock.Colour, 
                    new Stack<PlanogramProduct>(plan.Products
                        .OrderByDescending(p => 
                            plan.Sequence.Groups[0].Products.First(ap => ap.Gtin.Equals(p.Gtin)).SequenceNumber))
                }
            };
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByBlockColour,
                    merchGroups,
                    width,
                    height,
                    offset,
                    leftBlock,
                    PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace).First();
                var context = new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1),
                    plan,
                    /*shouldCompactSpace*/ true,
                    productStacksByBlockColour.First().Value);

                //plan.Parent.SaveAs(1, @"C:\users\usr145\before.pog");
                merchBlock.Layout(context);
                merchGroups.ApplyEdit();
                //plan.Parent.SaveAs(1, @"C:\users\usr145\after.pog");

                Assert.IsTrue(plan.Products.All(p => p.GetPlanogramPositions().Any()), "All products should be placed");
                Assert.That(shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(10));
                Assert.That(shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(3));
                Assert.That(shelf3.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Count(), Is.EqualTo(3));
            }
        }

        [Test]
        public void Layout_WhenCompactingMakesWorse_ShouldNotCompactSpace()
        {
            var plan = "".CreatePackage().AddPlanogram();
            PlanogramFixtureItem bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 20, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 40, 0));
            var sizes = new List<WidthHeightDepthValue>
                        {
                            new WidthHeightDepthValue(50, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(40, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(20, 10, 10),
                            new WidthHeightDepthValue(10, 10, 10),
                            new WidthHeightDepthValue(110, 10, 10)
                        };
            foreach (var size in sizes) plan.AddProduct(size);
            var blocking = plan.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(blocking.Groups[0], plan.Products);
            plan.CreateAssortmentFromProducts();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset).First();
                var context = new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1),
                    plan,
                    /*shouldCompactSpace*/ true,
                    new Stack<PlanogramProduct>(plan.Products.Reverse()));

                merchBlock.Layout(context);
                merchGroups.ApplyEdit();
            }

            Int32[] expected = { 3, 5, 1 }; // Three products on the first shelf, five on the second and 1 on the third.
            Assert.IsTrue(plan.GetPlanogramSubComponentPlacements().Select((p, i) => p.GetPlanogramPositions().Count() == expected[i]).All(b => b), "There should be the correct number of products on each one of the shelves");
        }

        [Test]
        public void Layout_WhenCompactingIncreasesRange_ShouldCompactSpace()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var prod0 = plan.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod1 = plan.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod2 = plan.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod3 = plan.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            var prod4 = plan.AddProduct(new WidthHeightDepthValue(10, 50, 10));
            plan.CreateAssortmentFromProducts(plan.Products);
            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(blocking.Groups[0], new[] { prod0, prod1, prod2, prod4, prod3 });
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(blocking, plan.Sequence, merchGroups, width, height, offset).First();
                var context = new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1),
                    plan,
                    /*shouldCompactSpace*/ true,
                    new Stack<PlanogramProduct>(new[] { prod3, prod4, prod2, prod1, prod0 }));

                //plan.Parent.SaveAs(1, @"C:\users\usr145\before.pog");
                merchBlock.Layout(context);
                merchGroups.ApplyEdit();
                //plan.Parent.SaveAs(1, @"C:\users\usr145\after.pog");
            }

            var actualShelf1Gtins = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf1Gtins = new List<String>() { prod0.Gtin, prod1.Gtin, };
            var actualShelf2Gtins = shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .OrderByDescending(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf2Gtins = new List<String>() { prod2.Gtin };
            var actualShelf3Gtins = shelf3.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
            var expectedShelf3Gtins = new List<String>() { prod4.Gtin, prod3.Gtin };
            CollectionAssert.AreEqual(expectedShelf1Gtins, actualShelf1Gtins);
            CollectionAssert.AreEqual(expectedShelf2Gtins, actualShelf2Gtins);
            CollectionAssert.AreEqual(expectedShelf3Gtins, actualShelf3Gtins);
        }

        [Test]
        public void Layout_WhenMultiplePositionClusters_CompactsIntoEachCluster()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.GetPlanogramComponent().SubComponents.First().MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Even;
            for (Int32 i = 0; i < 3; i++) shelf.AddPosition(bay, plan.AddProduct(new WidthHeightDepthValue(10, 10, 10)));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            plan.AddSequenceGroup(leftBlock, plan.Products.Take(1).Union(plan.Products.Skip(2)));
            plan.AddSequenceGroup(rightBlock, plan.Products.Skip(1).Take(1));
            var productStacksByColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Take(1).Union(plan.Products.Skip(2)).Reverse()) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(plan.Products.Skip(1).Take(1)) },
            };
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchBlocks = new PlanogramMerchandisingBlocks(
                    blocking,
                    productStacksByColour,
                    merchGroups,
                    width,
                    height,
                    offset,
                    leftBlock,
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace);
                var merchBlock = merchBlocks.First(b => b.BlockingGroup == leftBlock);

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    productStacksByColour[leftBlock.Colour].ToDictionary(p => p.Gtin, p => 1), 
                    plan, 
                    true, 
                    productStacksByColour[leftBlock.Colour]));
                merchGroups.ApplyEdit();

                CollectionAssert.AreEqual(
                    plan.Products.Select(p => p.Gtin).ToList(),
                    plan.Positions.OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList());
            }
        }

        [Test]
        public void Layout_IsCancelledBy_MerchGroupCancelEdit()
        {
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var p1 = plan.AddProduct(productSize);
            var p2 = plan.AddProduct(productSize);
            var p3 = plan.AddProduct(productSize);
            shelf.AddPosition(bay, p1);
            shelf.AddPosition(bay, p2);
            plan.ReprocessAllMerchandisingGroups();
            var blocking = plan.AddBlocking(new List<Tuple<Single,Single>>() { new Tuple<Single,Single>(0,0) });
            var block = blocking.Groups.First();
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            plan.AddSequenceGroup(block, plan.Products);
            plan.UpdatePositionSequenceData();
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { block.Colour, new Stack<PlanogramProduct>(plan.Products.Reverse()) }
            };
            var targetUnitsByGtin = plan.Products.ToDictionary(p => p.Gtin, p => 2);

            using(var merchGroups = plan.GetMerchandisingGroups())
	        {
		        var merchBlock = new PlanogramMerchandisingBlocks(
                        blocking,
                        productStacksByBlockColour,
                        merchGroups,
                        width,
                        height,
                        offset,
                        block,
                        PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace)
                        .First(); 
                var merchGroup = merchGroups.First();
                Int32 editLevel = merchGroup.EditLevel;

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    targetUnitsByGtin, plan, false, productStacksByBlockColour.First().Value));

                while (merchGroup.EditLevel > editLevel) { merchGroup.CancelEdit(); }
	        }

            Assert.That(plan.Positions, Has.Count.EqualTo(2));
            foreach (var p in plan.Positions) Assert.That(p, Has.Property("TotalUnits").EqualTo(1));
        }

        [Test]
        public void Layout_SubGroupedProducts_ShouldStickTogether()
        {
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(25, 10, 10);
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < 6; i++) plan.AddProduct(productSize);
            plan.CreateAssortmentFromProducts();
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            var sequenceGroup = plan.Sequence.Groups.First();
            sequenceGroup.AddSequenceProducts(plan.Products);
            sequenceGroup.SubGroups.Add(
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(
                sequenceGroup.Products.Skip(2)));
            Single width, height, heightOffset;
            plan.GetBlockingAreaSize(out height, out width, out heightOffset);

            using (var mgs = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(plan.Blocking.First(),plan.Sequence, mgs, width, height, heightOffset).First();
                
                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1), plan, true, new Stack<PlanogramProduct>(plan.Products.Reverse())));
                mgs.ApplyEdit();

                //plan.TakeSnapshot("Layout_SubGroupedProducts_ShouldStickTogether");
                shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin)
                    .Should().Equal(
                        plan.Products.Take(2).Select(p => p.Gtin),
                        "shelf1 should have the first two products on");
                shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderByDescending(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin)
                    .Should().Equal(
                        plan.Products.Skip(2).Select(p => p.Gtin),
                        "shelf2 should have the last four products on");
            }
        }

        [Test]
        public void Layout_ShouldCompactBlockSpaceEvenly()
        {
            var productSize = new WidthHeightDepthValue(10, 10, 10);
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            var shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var shelves = new[] { shelf1, shelf2, shelf3 };
            for (int i = 0; i < 8; i++) shelf1.AddPosition(plan.AddProduct(productSize));
            for (int i = 0; i < 8; i++) shelf2.AddPosition(plan.AddProduct(productSize));
            for (int i = 0; i < 8; i++) shelf3.AddPosition(plan.AddProduct(productSize));
            plan.AddProduct(productSize);
            plan.CreateAssortmentFromProducts();
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            leftBlock.Name = "Left";
            var rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            rightBlock.Name = "Right";
            var leftBlockProducts = plan.Products.Take(2).Union(plan.Products.Skip(8).Take(2)).Union(plan.Products.Skip(16).Take(3)).Union(plan.Products.Skip(24));
            var rightBlockProducts = plan.Products.Skip(2).Take(6).Union(plan.Products.Skip(10).Take(6)).Union(plan.Products.Skip(19).Take(5));
            plan.AddSequenceGroup(leftBlock, leftBlockProducts);
            plan.AddSequenceGroup(rightBlock, rightBlockProducts);
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            Single width, height, heightOffset;
            plan.GetBlockingAreaSize(out height, out width, out heightOffset);
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(), 
                    new Dictionary<Int32,Stack<PlanogramProduct>>()
                    {
                        { leftBlock.Colour, new Stack<PlanogramProduct>(leftBlockProducts.Reverse())},
                        { rightBlock.Colour, new Stack<PlanogramProduct>(rightBlockProducts.Reverse())},
                    },
                    mgs, 
                    width, 
                    height, 
                    heightOffset,
                    leftBlock,
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)
                    .First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1), plan, true, new Stack<PlanogramProduct>(leftBlockProducts.Reverse())));
                mgs.ApplyEdit();
                //plan.TakeSnapshot("Layout_ShouldCompactBlockSpaceEvenly");

                shelves
                    .Select(s => 
                        s.GetPositions().Where(p => p.SequenceColour.Value == leftBlock.Colour).Count())
                    .Should().BeEquivalentTo(
                        new[] { 2, 3, 3 },
                        "because the products should be distributed evenly amongst the shelves");
            }
        }

        [Test]
        public void CreatePlanWithOneBayThreeShelvesOneDividerSubGroupedByShelf_LeftBlockSequenceProductsShouldBeSubGrouped()
        {
            const Int32 leftBlockShelf1ProdCount = 3;
            const Int32 rightBlockShelf1ProdCount = 9;
            const Int32 leftBlockShelf2ProdCount = 6;
            const Int32 rightBlockShelf2ProdCount = 4;
            const Int32 leftBlockShelf3ProdCount = 1;
            const Int32 rightBlockShelf3ProdCount = 1;
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 10, 10);
            PlanogramBlockingGroup leftBlock, rightBlock;
            IEnumerable<PlanogramProduct> leftBlockProductsInSequence, rightBlockProductsInSequence;
            PlanogramFixtureComponent shelf1, shelf2, shelf3;

            var plan = CreatePlanWithOneBayThreeShelvesOneDividerSubGroupedByShelf(
                leftBlockShelf1ProdCount,
                rightBlockShelf1ProdCount,
                leftBlockShelf2ProdCount,
                rightBlockShelf2ProdCount,
                leftBlockShelf3ProdCount,
                rightBlockShelf3ProdCount,
                productSize,
                out leftBlock,
                out rightBlock,
                out leftBlockProductsInSequence,
                out rightBlockProductsInSequence,
                out shelf1,
                out shelf2,
                out shelf3);

            leftBlock.GetPlanogramSequenceGroup().Products.Should().OnlyContain(
                p => p.GetSubGroup() != null, 
                "because all products should be in a sub group");
        }

        public class OneBayThreeShelfProductArrangements
        {
            private OneBayThreeShelfProductArrangements() { }

            public Int32 LeftBlockShelf1ProdCount { get; private set; }
            public Int32 RightBlockShelf1ProdCount { get; private set; }
            public Int32 LeftBlockShelf2ProdCount { get; private set; }
            public Int32 RightBlockShelf2ProdCount { get; private set; }
            public Int32 LeftBlockShelf3ProdCount { get; private set; }
            public Int32 RightBlockShelf3ProdCount { get; private set; }
            public String Name { get; private set; }

            public static OneBayThreeShelfProductArrangements BottomShelfFullTopShelfSparse
            {
                get 
                {
                    return new OneBayThreeShelfProductArrangements()
                    {
                        LeftBlockShelf1ProdCount = 3,
                        RightBlockShelf1ProdCount = 8,
                        LeftBlockShelf2ProdCount = 6,
                        RightBlockShelf2ProdCount = 4,
                        LeftBlockShelf3ProdCount = 1,
                        RightBlockShelf3ProdCount = 1,
                        Name = "BottomShelfFullTopShelfSparse",
                    };
                }
            }

            public static OneBayThreeShelfProductArrangements BottomShelfFullOtherShelvesSparse
            {
                get
                {
                    return new OneBayThreeShelfProductArrangements()
                    {
                        LeftBlockShelf1ProdCount = 10,
                        RightBlockShelf1ProdCount = 1,
                        LeftBlockShelf2ProdCount = 3,
                        RightBlockShelf2ProdCount = 3,
                        LeftBlockShelf3ProdCount = 3,
                        RightBlockShelf3ProdCount = 3,
                        Name = "BottomShelfFullOtherShelvesSparse",
                    };
                }
            }

            public override string ToString()
            {
                return Name;
            }
        }

        private IEnumerable<OneBayThreeShelfProductArrangements> _productArrangements =
            new List<OneBayThreeShelfProductArrangements>()
            {
                OneBayThreeShelfProductArrangements.BottomShelfFullOtherShelvesSparse, 
                OneBayThreeShelfProductArrangements.BottomShelfFullTopShelfSparse
            };

        [Test]
        public void Layout_CompactBlockSpaceWithSubGroups_ShouldNotDropPlacements_WhenBeyondBlockSpace(
            [ValueSource("_productArrangements")] OneBayThreeShelfProductArrangements productArrangement)
        {
            Int32 shelf1ProdCount = productArrangement.LeftBlockShelf1ProdCount + productArrangement.RightBlockShelf1ProdCount;
            Int32 shelf2ProdCount = productArrangement.LeftBlockShelf2ProdCount + productArrangement.RightBlockShelf2ProdCount;
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(10, 10, 10);

            PlanogramBlockingGroup leftBlock, rightBlock;
            IEnumerable<PlanogramProduct> leftBlockProductsInSequence, rightBlockProductsInSequence;
            PlanogramFixtureComponent shelf1, shelf2, shelf3;
            var plan = CreatePlanWithOneBayThreeShelvesOneDividerSubGroupedByShelf(
                productArrangement.LeftBlockShelf1ProdCount,
                productArrangement.RightBlockShelf1ProdCount,
                productArrangement.LeftBlockShelf2ProdCount,
                productArrangement.RightBlockShelf2ProdCount,
                productArrangement.LeftBlockShelf3ProdCount,
                productArrangement.RightBlockShelf3ProdCount,
                productSize,
                out leftBlock,
                out rightBlock,
                out leftBlockProductsInSequence,
                out rightBlockProductsInSequence,
                out shelf1,
                out shelf2,
                out shelf3);
            
            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();
            var productStacksByBlockColour = new Dictionary<Int32, Stack<PlanogramProduct>>()
            {
                { leftBlock.Colour, new Stack<PlanogramProduct>(leftBlockProductsInSequence.Reverse()) },
                { rightBlock.Colour, new Stack<PlanogramProduct>(rightBlockProductsInSequence.Reverse()) },
            };
            Single width, height, heightOffset;
            plan.GetBlockingAreaSize(out height, out width, out heightOffset);
            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var leftMerchBlock = new PlanogramMerchandisingBlocks(
                    plan.Blocking.First(),
                    productStacksByBlockColour,
                    merchGroups,
                    width,
                    height,
                    heightOffset,
                    leftBlock,
                    PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)
                    .First(b => b.BlockingGroup == leftBlock);
                //plan.TakeSnapshot("Layout_CompactBlockSpaceWithSubGroups_ShouldNotDropPlacements_WhenBeyondBlockSpace_Before");
                leftMerchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1),
                    plan,
                    true,
                    productStacksByBlockColour[leftBlock.Colour]));

                merchGroups.ApplyEdit();
            }
            //plan.TakeSnapshot("Layout_CompactBlockSpaceWithSubGroups_ShouldNotDropPlacements_WhenBeyondBlockSpace_After");
            shelf1.GetPositions().Select(p => p.SequenceColour)
                .Should().Contain(plan.Blocking.First().Groups.Select(g => g.Colour),
                    "because all plan.Blocking.First() groups should be represented on shelf1");
            shelf2.GetPositions().Select(p => p.SequenceColour)
                .Should().Contain(plan.Blocking.First().Groups.Select(g => g.Colour),
                    "because all plan.Blocking.First() groups should be represented on shelf2");
            shelf3.GetPositions().Select(p => p.SequenceColour)
                .Should().Contain(plan.Blocking.First().Groups.Select(g => g.Colour),
                    "because all blocking groups should be represented on shelf3");
            shelf1.GetPositionGtins(orderXAsc: true)
                .Should().Equal(plan.Products.Take(shelf1ProdCount).Select(p => p.Gtin),
                    "because sub-groups should have kept the same products on the shelf1");
            shelf2.GetPositionGtins(orderXAsc: true)
                .Should().Equal(plan.Products.Skip(shelf1ProdCount).Take(shelf2ProdCount).Select(p => p.Gtin),
                    "because sub-groups should have kept the same products on the shelf2");
            shelf3.GetPositionGtins(orderXAsc: true)
                .Should().Equal(plan.Products.Skip(shelf1ProdCount + shelf2ProdCount).Select(p => p.Gtin),
                    "because sub-groups should have kept the same products on the shelf3");
        }

        private static Planogram CreatePlanWithOneBayThreeShelvesOneDividerSubGroupedByShelf(
            int leftBlockShelf1ProdCount, 
            int rightBlockShelf1ProdCount, 
            int leftBlockShelf2ProdCount, 
            int rightBlockShelf2ProdCount, 
            int leftBlockShelf3ProdCount, 
            int rightBlockShelf3ProdCount, 
            WidthHeightDepthValue productSize,
            out PlanogramBlockingGroup leftBlock,
            out PlanogramBlockingGroup rightBlock,
            out IEnumerable<PlanogramProduct> leftBlockProductsInSequence,
            out IEnumerable<PlanogramProduct> rightBlockProductsInSequence,
            out PlanogramFixtureComponent shelf1,
            out PlanogramFixtureComponent shelf2,
            out PlanogramFixtureComponent shelf3)
        {
            Int32 shelf1ProdCount = leftBlockShelf1ProdCount + rightBlockShelf1ProdCount;
            Int32 shelf2ProdCount = leftBlockShelf2ProdCount + rightBlockShelf2ProdCount;
            Int32 shelf3ProdCount = leftBlockShelf3ProdCount + rightBlockShelf3ProdCount;

            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 75, 0));
            shelf3 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            for (int i = 0; i < shelf1ProdCount; i++) shelf1.AddPosition(plan.AddProduct(productSize));
            for (int i = 0; i < shelf2ProdCount; i++) shelf2.AddPosition(plan.AddProduct(productSize));
            for (int i = 0; i < shelf3ProdCount; i++) shelf3.AddPosition(plan.AddProduct(productSize));

            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0.7f, 0f, PlanogramBlockingDividerType.Vertical);
            leftBlock = blocking.Locations.First(l => l.X.EqualTo(0)).GetPlanogramBlockingGroup();
            rightBlock = blocking.Locations.First(l => l.X.EqualTo(0.7f)).GetPlanogramBlockingGroup();

            leftBlockProductsInSequence =
                plan.Products.Take(leftBlockShelf1ProdCount).Union(
                plan.Products.Skip(shelf1ProdCount).Take(leftBlockShelf2ProdCount).Reverse()).Union(
                plan.Products.Skip(shelf1ProdCount + shelf2ProdCount).Take(leftBlockShelf3ProdCount))
                .ToList();
            rightBlockProductsInSequence =
                plan.Products.Skip(leftBlockShelf1ProdCount).Take(rightBlockShelf1ProdCount).Union(
                plan.Products.Skip(shelf1ProdCount + leftBlockShelf2ProdCount).Take(rightBlockShelf2ProdCount).Reverse()).Union(
                plan.Products.Skip(shelf1ProdCount + shelf2ProdCount + leftBlockShelf3ProdCount).Take(rightBlockShelf3ProdCount))
                .ToList();

            plan.AddSequenceGroup(leftBlock, leftBlockProductsInSequence);
            plan.AddSequenceGroup(rightBlock, rightBlockProductsInSequence);
            leftBlock.GetPlanogramSequenceGroup().SubGroups.AddRange(new[]
            {
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(leftBlock.GetPlanogramSequenceGroup().Products.Take(leftBlockShelf1ProdCount)),
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(leftBlock.GetPlanogramSequenceGroup().Products.Skip(leftBlockShelf1ProdCount).Take(leftBlockShelf2ProdCount)),
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(leftBlock.GetPlanogramSequenceGroup().Products.Skip(leftBlockShelf1ProdCount + leftBlockShelf2ProdCount).Take(leftBlockShelf3ProdCount)),
            });
            rightBlock.GetPlanogramSequenceGroup().SubGroups.AddRange(new[]
            {
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(rightBlock.GetPlanogramSequenceGroup().Products.Take(rightBlockShelf1ProdCount)),
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(rightBlock.GetPlanogramSequenceGroup().Products.Skip(rightBlockShelf1ProdCount).Take(rightBlockShelf2ProdCount)),
                PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup(rightBlock.GetPlanogramSequenceGroup().Products.Skip(rightBlockShelf1ProdCount + rightBlockShelf2ProdCount).Take(rightBlockShelf3ProdCount)),
            });

            return plan;
        }

        [Test]
        public void Layout_ConflictingSubGroupedProducts_ShouldStickTogether()
        {
            WidthHeightDepthValue productSize = new WidthHeightDepthValue(25, 10, 10);
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            for (Int32 i = 0; i < 16; i++) plan.AddProduct(productSize);
            plan.CreateAssortmentFromProducts(new[] 
                {
                    plan.Products[0],
                    plan.Products[4],
                    plan.Products[8],
                    plan.Products[12],
                    plan.Products[1],
                    plan.Products[5],
                    plan.Products[9],
                    plan.Products[13],
                    plan.Products[2],
                    plan.Products[6],
                    plan.Products[10],
                    plan.Products[14],
                    plan.Products[3],
                    plan.Products[7],
                    plan.Products[11],
                    plan.Products[15],
                });
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups.First().AddSequenceProducts(plan.Products);
            for (int i = 0; i < 4; i++)
            {
                var subGroup = PlanogramSequenceGroupSubGroup.NewPlanogramSequenceGroupSubGroup();
                plan.Sequence.Groups.First().SubGroups.Add(subGroup);
                foreach (var sp in plan.Sequence.Groups.First().Products.OrderBy(p => p.SequenceNumber).Skip(4 * i).Take(4))
                {
                    sp.PlanogramSequenceGroupSubGroupId = subGroup.Id;
                }
            }
            Single width, height, heightOffset;
            plan.GetBlockingAreaSize(out height, out width, out heightOffset);

            using (var mgs = plan.GetMerchandisingGroups())
            {
                var merchBlock = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, mgs, width, height, heightOffset).First();

                merchBlock.Layout<Object>(new PlanogramMerchandisingLayoutContext<Object>(
                    plan.Products.ToDictionary(p => p.Gtin, p => 1), plan, true, new Stack<PlanogramProduct>(plan.Products.Reverse())));
                mgs.ApplyEdit();
                //plan.TakeSnapshot("Layout_ConflictingSubGroupedProducts_ShouldStickTogether");

                CollectionAssert.AreEqual(
                    new[] { plan.Products[0], plan.Products[1], plan.Products[2], plan.Products[3] }.Select(p => p.Gtin).ToList(),
                    shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList());
                CollectionAssert.AreEqual(
                    new[] { plan.Products[4], plan.Products[5], plan.Products[6], plan.Products[7] }.Select(p => p.Gtin).ToList(),
                    shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().OrderByDescending(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin).ToList());
            }
        }
    }
}
