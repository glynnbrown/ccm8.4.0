﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802

// V8-29137 : A.Kuszyk
//   Created.

#endregion

#region Version History: CCM810

// V8-28878 : A. Silva
//  Updated IsOverfilled tests to account for changes in the code.

#endregion

#region Version History: CCM830

// V8-32433 : D.Pleasance
//  Added AddPosition tests
// CCM-31871 : A.Silva
//  Fixed add position to chest failing tests, some of the values were not the correct ones.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using FluentAssertions;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    [Category(Categories.Smoke)]
    internal class PlanogramMerchandisingBlockPlacementTests
    {
        #region IsOverFilled

        [Test]
        public void IsOverFilled_ReturnsTrue_WhenMoreSpaceIsUsedByPositionsThanIsAvailable_AndPlacementCoversWholeMerchGroup()
        {
            Planogram plan = "".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixItem = plan.AddFixtureItem();
            PlanogramFixtureComponent fixComp = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf);
            for (var i = 0; i < 100; i++)
                fixComp.AddPosition(fixItem);
            plan.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            plan.ReprocessAllMerchandisingGroups();
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlockPlacement blockPlacement = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset)
                    .First().BlockPlacements.First();
                Boolean result = blockPlacement.IsOverFilled();
                Assert.IsTrue(result);
            }
        }

        [Test]
        public void IsOverFilled_ReturnsFalse_WhenFreeSpaceIsAvailable_AndPlacementCoversWholeMerchGroup()
        {
            Planogram plan = "".CreatePackage().AddPlanogram();
            PlanogramFixtureItem fixItem = plan.AddFixtureItem();
            PlanogramFixtureComponent fixComp = fixItem.AddFixtureComponent(PlanogramComponentType.Shelf);
            fixComp.AddPosition(fixItem);
            plan.AddBlocking(new List<Tuple<Single, Single>> { new Tuple<Single, Single>(0.0F, 0.0F) });
            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            plan.ReprocessAllMerchandisingGroups();
            PlanogramMerchandisingBlockPlacement blockPlacement;
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                blockPlacement = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset)
                    .First().BlockPlacements.First();
            }

            Boolean result = blockPlacement.IsOverFilled();

            Assert.IsFalse(result);
        }

        #endregion

        #region AddPosition

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn1Direction(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);

                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn1Direction{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn2DirectionsWithProductBelow(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);
            productBlocks.Add(bottomRightBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);

                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn2DirectionsWithProductBelow{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn2Directions(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn2Directions{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn2DirectionsWithProductBelow2(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);
            productBlocks.Add(bottomRightBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopRightWithOverlapIn2DirectionsWithProductBelow2{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn1Direction(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            Assert.Ignore("This test is currently being ignored and is to be revisited. Positions will not be placed because no anchor position can be found.");

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topRightBlock);
            productBlocks.Add(bottomLeftBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);

                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == topRightBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn1Direction{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            Assert.AreEqual(3, actual.Count, "3 positions should have been placed.");

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn2DirectionsWithProductBelow(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            Assert.Ignore("This test is currently being ignored and is to be revisited. Positions will not be placed because no anchor position can be found.");

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topRightBlock);
            productBlocks.Add(bottomLeftBlock);
            productBlocks.Add(bottomRightBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);

                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn2DirectionsWithProductBelow{0}.pog", componentType.ToString()));


            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(4, actual.Count, "4 positions should have been placed.");

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn2Directions(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            Assert.Ignore("This test is currently being ignored and is to be revisited. Positions will not be placed because no anchor position can be found.");

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topRightBlock);
            productBlocks.Add(bottomLeftBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn2Directions{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(3, actual.Count, "3 positions should have been placed.");

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn2DirectionsWithProductBelow2(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topRightBlock);
            productBlocks.Add(bottomLeftBlock);
            productBlocks.Add(bottomRightBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)3, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)1));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockTopLeftWithOverlapIn2DirectionsWithProductBelow2{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(4, actual.Count, "4 positions should have been placed.");

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }

            Assert.Inconclusive("Position placement deemed acceptable atm, to be reviewed again for futher improvements.");
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockCentretWithOverlapIn2Directions(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();

            blocking.Dividers.Add(0, 0.3f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.6f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Add(0.5f, 0.2f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.2f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.7f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 5; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.2f) && l.Y.EqualTo(0.3f)).GetPlanogramBlockingGroup();

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.6f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.6f)).GetPlanogramBlockingGroup();

            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(topRightBlock);
            productBlocks.Add(bottomLeftBlock);
            productBlocks.Add(bottomRightBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(3).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(4).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)3));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)3 : (Int16)1, 1));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)3 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr144\AddPosition_OnComponent_TargetBlockCentretWithOverlapIn2Directions{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            expectedProductSequences.Select(GetKeyAndSequence).Should().Contain(actual.Select(GetKeyAndSequence), because: "should place all products in their blocks");
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockCentreWithOverlapIn1Direction(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();

            blocking.Dividers.Add(0, 0.3f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.6f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Add(0.5f, 0.2f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.2f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.7f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 5; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.2f) && l.Y.EqualTo(0.3f)).GetPlanogramBlockingGroup();

            PlanogramBlockingGroup middleLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.3f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup middleRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.7f) && l.Y.EqualTo(0.3f)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(middleLeftBlock);
            productBlocks.Add(middleRightBlock);

            PlanogramSequenceGroup middleLeftSequence = plan.AddSequenceGroup(middleLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup middleRightSequence = plan.AddSequenceGroup(middleRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(middleLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, 1));
            expectedProductSequences.Add(middleRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(3, 1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockCentreWithOverlapIn1Direction{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockCentreWithOverlapIn2Directions2(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();

            blocking.Dividers.Add(0, 0.3f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0, 0.6f, PlanogramBlockingDividerType.Horizontal);

            blocking.Dividers.Add(0.5f, 0.2f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.2f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.7f, 0.5f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);

            blocking.Dividers.Add(0.5f, 0.4f, PlanogramBlockingDividerType.Horizontal);

            for (Int32 i = 0; i < 6; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.2f) && l.Y.EqualTo(0.4f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.6f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.6f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup middleLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.3f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup middleCentreBelowBlock = blocking.Locations.First(l => l.X.EqualTo(0.2f) && l.Y.EqualTo(0.3f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup middleRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.7f) && l.Y.EqualTo(0.3f)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(topRightBlock);
            productBlocks.Add(middleLeftBlock);
            productBlocks.Add(middleCentreBelowBlock);
            productBlocks.Add(middleRightBlock);

            PlanogramSequenceGroup middleLeftSequence = plan.AddSequenceGroup(middleLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup middleCentreBelowSequence = plan.AddSequenceGroup(middleCentreBelowBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup middleRightSequence = plan.AddSequenceGroup(middleRightBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(3).Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(4).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(5).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(middleLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(middleCentreBelowSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)3));
            expectedProductSequences.Add(middleRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(3, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)1));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)3 : (Int16)1, 1));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)2, componentType == PlanogramComponentType.Peg ? (Int16)3 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockCentreWithOverlapIn2Directions2{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }

            Assert.Inconclusive("Position placement deemed acceptable atm, to be reviewed again for futher improvements.");
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomRightWithOverlapIn1Direction(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);

                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockBottomRightWithOverlapIn1Direction{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomRightWithOverlapIn2DirectionsWithProductBelow(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);
            productBlocks.Add(topRightBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);

                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomRightWithOverlapIn2Directions(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockBottomRightWithOverlapIn2Directions{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomRightWithOverlapIn2DirectionsWithProductBelow2(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomLeftBlock);
            productBlocks.Add(topRightBlock);

            PlanogramSequenceGroup bottomLeftSequence = plan.AddSequenceGroup(bottomLeftBlock, plan.Products.Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(bottomLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn1Direction(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);
            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Skip(2).Take(1));
            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>()
            {
                {
                    bottomRightSequence.Products.First().Gtin,
                    new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2)
                },
                {
                    topLeftSequence.Products.First().Gtin,
                    new Tuple<Int16, Int16, Int16>(componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1)
                },
                {
                    targetSequence.Products.First().Gtin,
                    new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)1, 1)
                },
            };
            Single width, height, offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);
                foreach (PlanogramBlockingGroup productBlock in new[] { topLeftBlock, bottomRightBlock })
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }
                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();


                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);


                merchGroups.ApplyEdit();
                //plan.TakeSnapshot("AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn1Direction");
            }
            plan.Positions.Should().HaveCount(3, "because all three products should have been placed");
            foreach(var p in plan.Positions)
            {
                var expectedSequences = expectedProductSequences[p.GetPlanogramProduct().Gtin];
                p.SequenceX.Should().Be(expectedSequences.Item1, $"because {p.GetPlanogramProduct().Gtin} should have been sequenced in X correctly");
                p.SequenceY.Should().Be(expectedSequences.Item2, $"because {p.GetPlanogramProduct().Gtin} should have been sequenced in Y correctly");
                p.SequenceZ.Should().Be(expectedSequences.Item3, $"because {p.GetPlanogramProduct().Gtin} should have been sequenced in Z correctly");
            }

            Assert.Inconclusive("Position placement deemed acceptable atm, to be reviewed again for futher improvements.");
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn2DirectionsWithProductBelow(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.4f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.6f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topRightBlock);
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomRightBlock);

            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, (Int16) (componentType == PlanogramComponentType.Peg ? 1 : 2)));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);

                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn2DirectionsWithProductBelow{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(4, actual.Count, "4 positions should have been placed.");

            expectedProductSequences.Select(GetKeyAndSequence).Should().Contain(actual.Select(GetKeyAndSequence), because: "should place all products in their blocks");
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn2Directions(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            Assert.Ignore("This test is currently being ignored and is to be revisited. Positions will not be placed because no anchor position can be found.");

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(bottomRightBlock);
            productBlocks.Add(topLeftBlock);

            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(2).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn2Directions{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(3, actual.Count, "3 positions should have been placed.");

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn2DirectionsWithProductBelow2(
            [Values(PlanogramComponentType.Peg, PlanogramComponentType.Chest)] PlanogramComponentType componentType)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(componentType, new PointValue(), new WidthHeightDepthValue(100, 100, componentType == PlanogramComponentType.Peg ? 4 : 100));
            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.6f, 0.25f, PlanogramBlockingDividerType.Vertical);
            blocking.Dividers.Add(0.4f, 0.75f, PlanogramBlockingDividerType.Vertical);

            for (Int32 i = 0; i < 4; i++) plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.4f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup targetBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.6f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();

            List<PlanogramBlockingGroup> productBlocks = new List<PlanogramBlockingGroup>();
            productBlocks.Add(topRightBlock);
            productBlocks.Add(topLeftBlock);
            productBlocks.Add(bottomRightBlock);

            PlanogramSequenceGroup targetSequence = plan.AddSequenceGroup(targetBlock, plan.Products.Take(1));
            PlanogramSequenceGroup bottomRightSequence = plan.AddSequenceGroup(bottomRightBlock, plan.Products.Skip(1).Take(1));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(2).Take(1));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(3).Take(1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(targetSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(bottomRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, 1, componentType == PlanogramComponentType.Peg ? (Int16)1 : (Int16)2));
            expectedProductSequences.Add(topRightSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(2, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));
            expectedProductSequences.Add(topLeftSequence.Products.First().Gtin, new Tuple<Int16, Int16, Int16>(1, componentType == PlanogramComponentType.Peg ? (Int16)2 : (Int16)1, 1));

            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                foreach (PlanogramBlockingGroup productBlock in productBlocks)
                {
                    PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == productBlock).BlockPlacements.First();
                    blockPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == blockPlacement.Parent.SequenceGroup.Products.First().Gtin), productBlock.BlockPlacementPrimaryType);
                    merchGroups.ApplyEdit();
                }

                PlanogramMerchandisingBlockPlacement targetPlacement = blocks.First(p => p.BlockingGroup == targetBlock).BlockPlacements.First();
                PlanogramPositionPlacement positionPlacement = targetPlacement.AddPosition(plan.Products.FirstOrDefault(p => p.Gtin == targetPlacement.Parent.SequenceGroup.Products.First().Gtin), targetBlock.BlockPlacementPrimaryType);

                merchGroups.ApplyEdit();
            }

            //plan.Parent.SaveAs(1, String.Format(@"C:\users\usr045\AddPosition_OnComponent_TargetBlockBottomLeftWithOverlapIn2DirectionsWithProductBelow2{0}.pog", componentType.ToString()));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(4, actual.Count, "4 positions should have been placed.");

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        [Test]
        public void AddPosition_Peg_WithExistingBlockPlacementPositions_AddInCorrectPositionAboveAndToTheRight()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(), new WidthHeightDepthValue(100, 100, 4));

            PlanogramSubComponent subComponent = peg.GetPlanogramComponent().SubComponents.First();
            subComponent.MerchConstraintRow1StartX = 1;
            subComponent.MerchConstraintRow1StartY = 1;
            subComponent.MerchConstraintRow1SpacingX = 1;
            subComponent.MerchConstraintRow1SpacingY = 1;

            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);
            
            PlanogramProduct planogramProductBottomBlock1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductBottomBlock2 = plan.AddProduct(new WidthHeightDepthValue(30, 10, 10));
            PlanogramProduct planogramProductBottomBlock3 = plan.AddProduct(new WidthHeightDepthValue(20, 10, 10));
            PlanogramProduct planogramProductBottomBlock4 = plan.AddProduct(new WidthHeightDepthValue(30, 10, 10));
            PlanogramProduct planogramProductBottomBlock5 = plan.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            PlanogramProduct planogramProductBottomBlock6 = plan.AddProduct(new WidthHeightDepthValue(25, 10, 10));

            PlanogramProduct planogramProductTopLeftBlock1 = plan.AddProduct(new WidthHeightDepthValue(15, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock3 = plan.AddProduct(new WidthHeightDepthValue(20, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock4 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock5 = plan.AddProduct(new WidthHeightDepthValue(35, 10, 10));

            PlanogramProduct planogramProductTopRightBlock1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopRightBlock2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopRightBlock3 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopRightBlock4 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopRightBlock5 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopRightBlock6Above = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            
            PlanogramSequenceGroup bottomSequence = plan.AddSequenceGroup(bottomBlock, plan.Products.Take(6));
            PlanogramSequenceGroup topLeftSequence = plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(6).Take(5));
            PlanogramSequenceGroup topRightSequence = plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(11).Take(6));
            
            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == bottomBlock).BlockPlacements.First();
                blockPlacement.AddPosition(planogramProductBottomBlock1, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock2, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock3, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock4, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock5, PlanogramBlockingGroupPlacementType.BottomToTop);
                blockPlacement.AddPosition(planogramProductBottomBlock6, PlanogramBlockingGroupPlacementType.RightToLeft);

                blockPlacement = blocks.First(p => p.BlockingGroup == topLeftBlock).BlockPlacements.First();
                blockPlacement.AddPosition(planogramProductTopLeftBlock1, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopLeftBlock2, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopLeftBlock3, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopLeftBlock4, PlanogramBlockingGroupPlacementType.BottomToTop);
                blockPlacement.AddPosition(planogramProductTopLeftBlock5, PlanogramBlockingGroupPlacementType.RightToLeft);

                blockPlacement = blocks.First(p => p.BlockingGroup == topRightBlock).BlockPlacements.First();
                blockPlacement.AddPosition(planogramProductTopRightBlock1, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock2, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock3, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock4, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock5, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock6Above, PlanogramBlockingGroupPlacementType.BottomToTop);
                
                foreach (PlanogramMerchandisingGroup merchGroup in merchGroups)
                {
                    merchGroup.Process();
                    merchGroups.ApplyEdit();
                }
            }
            
            //plan.Parent.SaveAs(1, @"C:\users\usr045\AddPosition_Peg_WithExistingBlockPlacementPositions_AddInCorrectPositionAboveAndToTheRight.pog");

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(17, actual.Count, "17 positions should have been placed.");

            Assert.AreEqual(3, actual[planogramProductTopRightBlock6Above.Gtin].Item1); // Sequence X 3
            Assert.AreEqual(4, actual[planogramProductTopRightBlock6Above.Gtin].Item2); // Sequence Y 4
            Assert.AreEqual(1, actual[planogramProductTopRightBlock6Above.Gtin].Item3); // Sequence Z 1
        }
        
        [Test]
        public void AddPosition_Peg_WithExistingBlockPlacementPositions_AddInCorrectPositionAboveAndToTheLeft()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem(new RectValue(0, 0, 0, 100, 100, 100));
            var peg = bay.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(), new WidthHeightDepthValue(100, 100, 4));

            PlanogramSubComponent subComponent = peg.GetPlanogramComponent().SubComponents.First();
            subComponent.MerchConstraintRow1StartX = 1;
            subComponent.MerchConstraintRow1StartY = 1;
            subComponent.MerchConstraintRow1SpacingX = 1;
            subComponent.MerchConstraintRow1SpacingY = 1;

            var blocking = plan.AddBlocking();
            blocking.Dividers.Add(0, 0.5f, PlanogramBlockingDividerType.Horizontal);
            blocking.Dividers.Add(0.5f, 0.75f, PlanogramBlockingDividerType.Vertical);

            PlanogramProduct planogramProductBottomBlock1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductBottomBlock2 = plan.AddProduct(new WidthHeightDepthValue(30, 10, 10));
            PlanogramProduct planogramProductBottomBlock3 = plan.AddProduct(new WidthHeightDepthValue(20, 10, 10));
            PlanogramProduct planogramProductBottomBlock4 = plan.AddProduct(new WidthHeightDepthValue(30, 10, 10));
            PlanogramProduct planogramProductBottomBlock5 = plan.AddProduct(new WidthHeightDepthValue(40, 10, 10));
            PlanogramProduct planogramProductBottomBlock6 = plan.AddProduct(new WidthHeightDepthValue(25, 10, 10));

            PlanogramProduct planogramProductTopRightBlock1 = plan.AddProduct(new WidthHeightDepthValue(15, 10, 10));
            PlanogramProduct planogramProductTopRightBlock2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopRightBlock3 = plan.AddProduct(new WidthHeightDepthValue(20, 10, 10));
            PlanogramProduct planogramProductTopRightBlock4 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopRightBlock5 = plan.AddProduct(new WidthHeightDepthValue(35, 10, 10));

            PlanogramProduct planogramProductTopLeftBlock1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock3 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock4 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopleftBlock5 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            PlanogramProduct planogramProductTopLeftBlock6Above = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramBlockingGroup topRightBlock = blocking.Locations.First(l => l.X.EqualTo(0.5f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup topLeftBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0.5f)).GetPlanogramBlockingGroup();
            PlanogramBlockingGroup bottomBlock = blocking.Locations.First(l => l.X.EqualTo(0f) && l.Y.EqualTo(0)).GetPlanogramBlockingGroup();
            
            plan.AddSequenceGroup(bottomBlock, plan.Products.Take(6));
            plan.AddSequenceGroup(topRightBlock, plan.Products.Skip(6).Take(5));
            plan.AddSequenceGroup(topLeftBlock, plan.Products.Skip(11).Take(6));
            
            Single width;
            Single height;
            Single offset;
            plan.GetBlockingAreaSize(out height, out width, out offset);
            
            using (PlanogramMerchandisingGroupList merchGroups = plan.GetMerchandisingGroups())
            {
                PlanogramMerchandisingBlocks blocks = new PlanogramMerchandisingBlocks(plan.Blocking.First(), plan.Sequence, merchGroups, width, height, offset);

                PlanogramMerchandisingBlockPlacement blockPlacement = blocks.First(p => p.BlockingGroup == bottomBlock).BlockPlacements.First();
                blockPlacement.AddPosition(planogramProductBottomBlock1, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock2, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock3, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock4, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductBottomBlock5, PlanogramBlockingGroupPlacementType.BottomToTop);
                blockPlacement.AddPosition(planogramProductBottomBlock6, PlanogramBlockingGroupPlacementType.RightToLeft);

                blockPlacement = blocks.First(p => p.BlockingGroup == topRightBlock).BlockPlacements.First();
                blockPlacement.AddPosition(planogramProductTopRightBlock1, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock2, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock3, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopRightBlock4, PlanogramBlockingGroupPlacementType.BottomToTop);
                blockPlacement.AddPosition(planogramProductTopRightBlock5, PlanogramBlockingGroupPlacementType.RightToLeft);
                
                blockPlacement = blocks.First(p => p.BlockingGroup == topLeftBlock).BlockPlacements.First();
                blockPlacement.AddPosition(planogramProductTopLeftBlock1, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopLeftBlock2, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopLeftBlock3, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopLeftBlock4, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopleftBlock5, PlanogramBlockingGroupPlacementType.LeftToRight);
                blockPlacement.AddPosition(planogramProductTopLeftBlock6Above, PlanogramBlockingGroupPlacementType.BottomToTop);
                
                foreach (PlanogramMerchandisingGroup merchGroup in merchGroups)
                {
                    merchGroup.Process();
                    merchGroups.ApplyEdit();
                }
            }
            
            //plan.Parent.SaveAs(1, @"C:\users\usr045\AddPosition_Peg_WithExistingBlockPlacementPositions_AddInCorrectPositionAboveAndToTheLeft.pog");

            Dictionary<String, Tuple<Int16, Int16, Int16>> expectedProductSequences = new Dictionary<String, Tuple<Int16, Int16, Int16>>();
            expectedProductSequences.Add(planogramProductTopLeftBlock6Above.Gtin, new Tuple<Int16, Int16, Int16>(1, 4, 1));
            expectedProductSequences.Add(planogramProductTopRightBlock4.Gtin, new Tuple<Int16, Int16, Int16>(3, 4, 1));
            expectedProductSequences.Add(planogramProductTopRightBlock5.Gtin, new Tuple<Int16, Int16, Int16>(2, 4, 1));

            Dictionary<String, Tuple<Int16, Int16, Int16>> actual = plan.Positions.ToDictionary(p => p.GetPlanogramProduct().Gtin, p => new Tuple<Int16, Int16, Int16>(p.SequenceX, p.SequenceY, p.SequenceZ));
            Assert.AreEqual(17, actual.Count, "17 positions should have been placed.");

            foreach (String productGtin in expectedProductSequences.Keys)
            {
                Assert.AreEqual(expectedProductSequences[productGtin].Item1, actual[productGtin].Item1); // sequence x
                Assert.AreEqual(expectedProductSequences[productGtin].Item2, actual[productGtin].Item2); // sequence y
                Assert.AreEqual(expectedProductSequences[productGtin].Item3, actual[productGtin].Item3); // sequence z
            }
        }

        #endregion

        private String GetKeyAndSequence(KeyValuePair<String, Tuple<Int16, Int16, Int16>> arg)
        {
            return $"{arg.Key} : {arg.Value.Item1}, {arg.Value.Item2}, {arg.Value.Item3}";
        }
    }
}