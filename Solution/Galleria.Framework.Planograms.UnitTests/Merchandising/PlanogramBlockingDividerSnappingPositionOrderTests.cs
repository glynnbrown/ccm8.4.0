﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27485 : A.Kuszyk
//  Created.
// V8-27581 : A.Kuszyk
//  Removed leap-frog rule test.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved into common namespace.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Moved to framework
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    public class PlanogramBlockingDividerSnappingPositionOrderTests
    {
        private PlanogramFixture _fixture;
        private Planogram _planogram;
        private PlanogramBlocking _blocking;
        private const Single _fixtureSize = 10f;

        [SetUp]
        public void PlanogramBlockingDividerSetup()
        {
            _fixture = PlanogramFixture.NewPlanogramFixture();
            _fixture.Width = _fixtureSize;
            _fixture.Height = _fixtureSize;
            _blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            _planogram = Planogram.NewPlanogram();
            _planogram.Fixtures.Add(_fixture);
            _planogram.Blocking.Add(_blocking);
            _planogram.Width = _fixtureSize;
            _planogram.Height = _fixtureSize;
        }

        #region GetFirstList
        [Test]
        public void GetFirstList_ReturnsEdges()
        {
            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(
                new Single[] { 1f }, new Single[] { 2f }, new Single[] { 3f }, false, PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs);

            var list = snappableBoundaries.GetFirstList();

            Assert.AreEqual(snappableBoundaries.Edges, list);
        }
        #endregion

        #region GetNextList
        [Test]
        public void GetNextList_FirstReturnsEdges()
        {
            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(
                new Single[] { 1f }, new Single[] { 2f }, new Single[] { 3f }, false, PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs);

            var list = snappableBoundaries.GetNextList();

            Assert.AreEqual(snappableBoundaries.Edges, list);
        }

        [Test]
        public void GetNextList_SecondReturnsDividers()
        {
            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(
                new Single[] { 1f }, new Single[] { 2f }, new Single[] { 3f }, false, PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs);
            snappableBoundaries.GetNextList();

            var list = snappableBoundaries.GetNextList();

            Assert.AreEqual(snappableBoundaries.Dividers, list);
        }

        [Test]
        public void GetNextList_ThirdReturnsPegs()
        {
            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(
                new Single[] { 1f }, new Single[] { 2f }, new Single[] { 3f }, false, PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs);
            snappableBoundaries.GetNextList();
            snappableBoundaries.GetNextList();

            var list = snappableBoundaries.GetNextList();

            Assert.AreEqual(snappableBoundaries.Pegs, list);
        }
        #endregion

        #region GetSnappingPositionsForDivider
        [Test]
        public void GetSnappingPositionsForDivider_EdgesReturnsCorrectBoundaries_ForShelvesAndPegs()
        {
            //   _______________
            //  |               |   /--> Component
            //  |_______________| _/
            // .|...............|. <---- Divider
            //  |_______________| _
            //  |               |  \
            //  |_______________|   \--> Component
            //

            _planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            var y1 = _fixtureSize / 3f;
            var y2 = 2 * (_fixtureSize / 3f);
            TestDataHelper.CreateFixtureComponent(0f, y1, _fixtureSize, 1f, PlanogramComponentType.Shelf, _fixture.Components, _planogram.Components);
            TestDataHelper.CreateFixtureComponent(0f, y2, _fixtureSize, 1f, PlanogramComponentType.Peg, _fixture.Components, _planogram.Components);
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, PlanogramBlockingDivider.RootLevelNumber, _blocking.Dividers);
            var expectedEdges = new Single[] { y1 / _fixtureSize, y2 / _fixtureSize };
            Single width, height, offset;
            _planogram.GetBlockingAreaSize(out height, out width, out offset);

            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(divider, width, height, offset);

            CollectionAssert.AreEquivalent(expectedEdges, snappableBoundaries.Edges);
        }

        [Test]
        public void GetSnappingPositionsForDivider_EdgesReturnsCorrectBoundaries_ForBarsAndRods()
        {
            //   _______________
            //  |               |   /--> Component
            //  |_______________| _/
            // .|...............|. <---- Divider
            //  |_______________| _
            //  |               |  \
            //  |_______________|   \--> Component
            //

            _planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            var y1 = _fixtureSize / 3f;
            var y2 = 2 * (_fixtureSize / 3f);
            var compWidth = _fixtureSize;
            var compHeight = 1f;
            TestDataHelper.CreateFixtureComponent(0f, y1, compWidth, compHeight, PlanogramComponentType.Bar, _fixture.Components, _planogram.Components);
            TestDataHelper.CreateFixtureComponent(0f, y2, compWidth, compHeight, PlanogramComponentType.Rod, _fixture.Components, _planogram.Components);
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, PlanogramBlockingDivider.RootLevelNumber, _blocking.Dividers);
            var expectedEdges = new Single[] { (y1) / _fixtureSize, (y2 + compHeight) / _fixtureSize };
            Single width, height, offset;
            _planogram.GetBlockingAreaSize(out height, out width, out offset);

            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(divider, width, height, offset);

            CollectionAssert.AreEquivalent(expectedEdges, snappableBoundaries.Edges);
        }

        [Test]
        public void GetSnappingPositionsForDivider_ReturnsPegPositions_ForHorizontalDivider()
        {
            //   _______________
            //  | : : : : : : : |   ,--> Pegboard
            //  | : : : : : : : | _/
            //  |_______________|   
            // .|...............|. <---- Divider
            //  |_______________|   
            //

            _planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(0f, _fixtureSize / 2, _fixtureSize, _fixtureSize / 2, PlanogramComponentType.Peg, _fixture.Components, _planogram.Components);
            var component = fixtureComponent.GetPlanogramComponent();
            var subComponent = component.SubComponents.First(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None);
            subComponent.MerchConstraintRow1SpacingX = 1f;
            subComponent.MerchConstraintRow1SpacingY = 1f;
            subComponent.MerchConstraintRow1StartX = 0f;
            subComponent.MerchConstraintRow1StartY = 0f;
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, PlanogramBlockingDivider.RootLevelNumber, _blocking.Dividers);
            var expectedPegs = new Single[] { 0.6f, 0.7f, 0.8f, 0.9f };
            Single width, height, offset;
            _planogram.GetBlockingAreaSize(out height, out width, out offset);

            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(divider, width, height, offset);

            CollectionAssert.AreEquivalent(expectedPegs, snappableBoundaries.Pegs);
        }

        [Test]
        public void GetSnappingPositionsForDivider_ReturnsPegPositions_ForVerticalDivider()
        {
            //   _______:_______
            //  | : : : : : : : |   ,--> Pegboard
            //  | : : : : : : : | _/
            //  |_______:_______|   
            //  |       :       | 
            //  |_______:_______|   
            //          :  <---- Divider

            _planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(0f, _fixtureSize / 2, _fixtureSize, _fixtureSize / 2, PlanogramComponentType.Peg, _fixture.Components, _planogram.Components);
            var component = fixtureComponent.GetPlanogramComponent();
            var subComponent = component.SubComponents.First(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None);
            subComponent.MerchConstraintRow1SpacingX = 1f;
            subComponent.MerchConstraintRow1SpacingY = 1f;
            subComponent.MerchConstraintRow1StartX = 0f;
            subComponent.MerchConstraintRow1StartY = 0f;
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, PlanogramBlockingDivider.RootLevelNumber, _blocking.Dividers);
            var expectedPegs = new Single[] { 0.1f, 0.2f, 0.3f, 0.4f, 0.6f, 0.7f, 0.8f, 0.9f };
            Single width, height, offset;
            _planogram.GetBlockingAreaSize(out height, out width, out offset);

            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(divider, width, height, offset);

            AssertHelper.AreSinglesEquivalent(expectedPegs, snappableBoundaries.Pegs, 3);
        }

        [Test]
        public void GetSnappingPositionsForDivider_ReturnsDividersOnly_ForHorizontalDividerOverComponent()
        {
            // Planogram and divider
            _planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.4f, 1f, PlanogramBlockingDividerType.Horizontal, PlanogramBlockingDivider.RootLevelNumber, _blocking.Dividers);
            Single width, height, offset;
            _planogram.GetBlockingAreaSize(out height, out width, out offset);

            // Component
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(0f, 0, _fixtureSize, _fixtureSize / 2, PlanogramComponentType.Chest, _fixture.Components, _planogram.Components);
            var component = fixtureComponent.GetPlanogramComponent();
            component.Depth = _fixtureSize / 2;
            var subComponent = component.SubComponents.First(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None);
            subComponent.Depth = _fixtureSize / 2;
            subComponent.FaceThicknessBack = 1f;
            subComponent.FaceThicknessFront = 1f;
            subComponent.DividerObstructionSpacingX = 1f;
            subComponent.DividerObstructionSpacingZ = 1f;
            subComponent.DividerObstructionStartX = 1f;
            subComponent.DividerObstructionStartZ = 1f;

            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(divider, width, height, offset);

            var expectedDividers = new Single[] { 0.2f, 0.3f };
            AssertHelper.AreSinglesEquivalent(expectedDividers, snappableBoundaries.Dividers, 5);
            CollectionAssert.IsEmpty(snappableBoundaries.Edges);
            CollectionAssert.IsEmpty(snappableBoundaries.Pegs);
        }

        [Test]
        public void GetSnappingPositionsForDivider_ReturnsEdgesOnly_ForHorizontalDividerNotOverComponent()
        {
            // Planogram and divider
            _planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.6f, 1f, PlanogramBlockingDividerType.Horizontal, PlanogramBlockingDivider.RootLevelNumber, _blocking.Dividers);
            Single width, height, offset;
            _planogram.GetBlockingAreaSize(out height, out width, out offset);

            // Component
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(0f, 0, _fixtureSize, _fixtureSize / 2, PlanogramComponentType.Chest, _fixture.Components, _planogram.Components);
            var component = fixtureComponent.GetPlanogramComponent();
            component.Depth = _fixtureSize / 2;
            var subComponent = component.SubComponents.First(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None);
            subComponent.Depth = _fixtureSize / 2;
            subComponent.FaceThicknessBack = 1f;
            subComponent.FaceThicknessFront = 1f;
            subComponent.DividerObstructionSpacingX = 1f;
            subComponent.DividerObstructionSpacingZ = 1f;
            subComponent.DividerObstructionStartX = 1f;
            subComponent.DividerObstructionStartZ = 1f;

            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(divider, width, height, offset);

            //var expectedDividers = new Single[] { 0f };
            //AssertHelper.AreSinglesEquivalent(expectedDividers, snappableBoundaries.Edges, 5);
            // V8-30188 - Edges of the plan should never be returned.
            CollectionAssert.IsEmpty(snappableBoundaries.Edges);
            CollectionAssert.IsEmpty(snappableBoundaries.Dividers);
            CollectionAssert.IsEmpty(snappableBoundaries.Pegs);
        }

        [Test]
        public void GetSnappingPositionsForDivider_ReturnsDividerPositions_ForVerticalDivider()
        {
            //   _______________         _______________
            //  |               |       |               |
            //  |               |       |               |
            //  |               |       |               |
            //  |               |       |               |
            //  |===============|       |===============|
            //  ||             ||       ||             ||
            //  ||_____________||       ||_ _ _ _ _ _ _||
            //                          ''=============''

            _planogram.FixtureItems.Add(PlanogramFixtureItem.NewPlanogramFixtureItem(_fixture));
            var fixtureComponent = TestDataHelper.CreateFixtureComponent(0f, 0, _fixtureSize, _fixtureSize / 2, PlanogramComponentType.Chest, _fixture.Components, _planogram.Components);
            var component = fixtureComponent.GetPlanogramComponent();
            component.Depth = _fixtureSize / 2;
            var subComponent = component.SubComponents.First(s => s.MerchandisingType != PlanogramSubComponentMerchandisingType.None);
            subComponent.Depth = _fixtureSize / 2;
            subComponent.DividerObstructionSpacingX = 1f;
            subComponent.DividerObstructionStartX = 1f;
            subComponent.FaceThicknessLeft = 1f;
            subComponent.FaceThicknessRight = 1f;
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, PlanogramBlockingDivider.RootLevelNumber, _blocking.Dividers);
            var expectedDividers = new Single[] { 0.2f, 0.3f, 0.4f, 0.6f, 0.7f, 0.8f };
            Single width, height, offset;
            _planogram.GetBlockingAreaSize(out height, out width, out offset);

            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(divider, width, height, offset);

            CollectionAssert.AreEquivalent(expectedDividers, snappableBoundaries.Dividers);
        }
        #endregion

        #region Sanitise

        [Test]
        public void Sanitise_WhenPositionsAlreadyOccupied_RemovesPositions()
        {
            //       _______________ 
            //      |               |
            //      |_(1)___________|       Divider 1
            //      |               |
            //      |_(2)___________|       Divider 2
            //      |               |
            //      |_(3)___________|       Divider 3
            //      |               |
            //      |_______________|
            //

            // Create Planogram.
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Create dividers.
            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.75f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0f, 0.25f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);

            // Create snappable boundaries
            var positions = new Single[] { 0.25f, 0.75f };
            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(positions, positions, positions, false, PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs);

            var result = snappableBoundaries.Sanitise(divider2);

            CollectionAssert.IsEmpty(result.Edges);
            CollectionAssert.IsEmpty(result.Dividers);
            CollectionAssert.IsEmpty(result.Pegs);
        }

        [Test]
        public void Sanitise_WhenPositionsUnOccupiedAndBetweenDividers_PositionsRemain()
        {
            // Create Planogram.
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Create dividers.
            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.75f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0f, 0.25f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);

            // Create snappable boundaries
            var positions = new Single[] { 0.3f, 0.6f };
            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(positions, positions, positions, false, PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs);

            var result = snappableBoundaries.Sanitise(divider2);

            CollectionAssert.AreEquivalent(positions, result.Edges);
            CollectionAssert.AreEquivalent(positions, result.Dividers);
            CollectionAssert.AreEquivalent(positions, result.Pegs);
        }

        private IEnumerable<Single> _sanitiseSingles
        {
            get
            {
                return new Single[] { 1f / 9f, 1f / 5f, 1f / 3f, 3f / 5f, 8f / 9f };
            }
        }

        [Test]
        [TestCaseSource("_sanitiseSingles")]
        public void Sanitise_WhenPositionOccupiedByAdjacentDivider_PositionRemains(Single singleValue)
        {
            // Create Planogram.
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);
            var div1 = blocking.Dividers.Add(singleValue, 0f, PlanogramBlockingDividerType.Vertical);
            var div2 = blocking.Dividers.Add(0f, 0.5f, PlanogramBlockingDividerType.Horizontal);
            var div3 = blocking.Dividers.Add(singleValue, singleValue, PlanogramBlockingDividerType.Horizontal);
            var expected = new[] { singleValue };
            var snappableBoundaries = new PlanogramBlockingDividerSnappingPositions(expected, new List<Single>(), new List<Single>(), false, PlanogramBlockingDividerSnappingPositionsOrder.EdgesDividersPegs);

            var actual = snappableBoundaries.Sanitise(div2);

            CollectionAssert.AreEqual(expected, actual.Edges);
        }

        #endregion
    }
}
