﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31807 : D.Pleasance
//  Created.
// V8-32612 : D.Pleasance
//  Refactored PlanogramMerchandisingProductLayout constructor to provide PlanogramMerchandisingSpaceConstraintType instead of canExceedBlockSpace.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.UnitTests.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using FluentAssertions;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public class PlanogramMerchandisingProductLayoutTests : TestBase
    {
        #region PlanogramMerchandisingProductLayout

        [Test]
        public void PlanogramMerchandisingProductLayout_ProductShouldBePlaced()
        {
            Planogram plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking().Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var bay1 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.AddPosition(bay1);
            PlanogramBlocking planogramBlocking = plan.Blocking.First();
            plan.Sequence.ApplyFromBlocking(planogramBlocking);
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            PlanogramProduct planogramProduct = plan.AddProduct();
            plan.Sequence.Groups.First().AddSequenceGroupProduct(planogramProduct);

            PlanogramMerchandisingGroupList merchandisingGroups = plan.GetMerchandisingGroups();

            Dictionary<Int32, Stack<PlanogramProduct>> productStacksBySequenceColour = planogramBlocking.GetAndUpdateProductStacksByColour(merchandisingGroups);
            Dictionary<Int32, Dictionary<String, Int32>> targetUnitsBySequenceColour = new Dictionary<Int32, Dictionary<String, Int32>>();

            foreach (PlanogramPosition position in plan.Positions)
            {
                if (!position.SequenceColour.HasValue) continue;

                Dictionary<String, Int32> targetUnits;
                if (!targetUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out targetUnits))
                {
                    targetUnits = new Dictionary<String, Int32>();
                    targetUnitsBySequenceColour.Add(position.SequenceColour.Value, targetUnits);
                }

                String productGtin = position.GetPlanogramProduct().Gtin;
                if (targetUnits.ContainsKey(productGtin)) continue;
                targetUnits.Add(productGtin, position.TotalUnits);
            }

            PlanogramMerchandisingProductLayout planogramMerchandisingProductLayout = new PlanogramMerchandisingProductLayout(plan, merchandisingGroups, plan.Sequence.Groups.ToList(),
                                        planogramBlocking, planWidth, planHeight, planHeightOffset, planogramProduct, productStacksBySequenceColour, targetUnitsBySequenceColour, 1, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace);

            Boolean success = planogramMerchandisingProductLayout.TryRelayoutBlockWithNewProduct();

            Assert.IsTrue(success, "The product should have been added.");
        }

        [Test]
        public void PlanogramMerchandisingProductLayout_ProductShouldNotBePlaced()
        {
            Planogram plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking().Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var bay1 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.AddPosition(bay1);
            PlanogramBlocking planogramBlocking = plan.Blocking.First();
            plan.Sequence.ApplyFromBlocking(planogramBlocking);
            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            PlanogramProduct planogramProduct = plan.AddProduct(new WidthHeightDepthValue(120, 10, 10));
            plan.Sequence.Groups.First().AddSequenceGroupProduct(planogramProduct);

            PlanogramMerchandisingGroupList merchandisingGroups = plan.GetMerchandisingGroups();

            Dictionary<Int32, Stack<PlanogramProduct>> productStacksBySequenceColour = planogramBlocking.GetAndUpdateProductStacksByColour(merchandisingGroups);
            Dictionary<Int32, Dictionary<String, Int32>> targetUnitsBySequenceColour = new Dictionary<Int32, Dictionary<String, Int32>>();

            foreach (PlanogramPosition position in plan.Positions)
            {
                if (!position.SequenceColour.HasValue) continue;

                Dictionary<String, Int32> targetUnits;
                if (!targetUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out targetUnits))
                {
                    targetUnits = new Dictionary<String, Int32>();
                    targetUnitsBySequenceColour.Add(position.SequenceColour.Value, targetUnits);
                }

                String productGtin = position.GetPlanogramProduct().Gtin;
                if (targetUnits.ContainsKey(productGtin)) continue;
                targetUnits.Add(productGtin, position.TotalUnits);
            }

            PlanogramMerchandisingProductLayout planogramMerchandisingProductLayout = new PlanogramMerchandisingProductLayout(plan, merchandisingGroups, plan.Sequence.Groups.ToList(),
                                        planogramBlocking, planWidth, planHeight, planHeightOffset, planogramProduct, productStacksBySequenceColour, targetUnitsBySequenceColour, 1, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace);

            Boolean success = planogramMerchandisingProductLayout.TryRelayoutBlockWithNewProduct();

            Assert.IsFalse(success, "The product should not have been added.");
        }

        [Test]
        public void PlanogramMerchandisingProductLayout_ProductShouldBePlacedInCorrectPosition()
        {
            Planogram plan = "Package".CreatePackage().AddPlanogram();
            plan.AddBlocking().Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var bay1 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 20, 0));

            PlanogramProduct planogramProduct1 = plan.AddProduct(new WidthHeightDepthValue(20, 10, 10));
            shelf1.AddPosition(bay1, planogramProduct1);
            PlanogramProduct planogramProduct2 = plan.AddProduct(new WidthHeightDepthValue(20, 10, 10));
            shelf1.AddPosition(bay1, planogramProduct2);
            PlanogramProduct planogramProduct3 = plan.AddProduct(new WidthHeightDepthValue(60, 10, 10));
            shelf1.AddPosition(bay1, planogramProduct3);

            PlanogramProduct planogramProduct4 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));

            PlanogramProduct planogramProduct5 = plan.AddProduct(new WidthHeightDepthValue(90, 10, 10));
            shelf2.AddPosition(bay1, planogramProduct5);
            PlanogramProduct planogramProduct6 = plan.AddProduct(new WidthHeightDepthValue(20, 10, 10));
            shelf2.AddPosition(bay1, planogramProduct6);

            PlanogramBlocking planogramBlocking = plan.Blocking.First();
            plan.Sequence.ApplyFromBlocking(planogramBlocking);

            plan.Sequence.Groups.First().Products.Clear();
            plan.Sequence.Groups.First().AddSequenceGroupProducts(
                plan.Products.Take(4).Union(plan.Products.Skip(4).Reverse()));

            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            plan.ReprocessAllMerchandisingGroups();
            plan.UpdatePositionSequenceData();

            using (var merchandisingGroups = plan.GetMerchandisingGroups())
            {
                Dictionary<Int32, Stack<PlanogramProduct>> productStacksBySequenceColour = planogramBlocking.GetAndUpdateProductStacksByColour(merchandisingGroups);
                Dictionary<Int32, Dictionary<String, Int32>> targetUnitsBySequenceColour = new Dictionary<Int32, Dictionary<String, Int32>>();

                foreach (PlanogramPosition position in plan.Positions)
                {
                    if (!position.SequenceColour.HasValue) continue;

                    Dictionary<String, Int32> targetUnits;
                    if (!targetUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out targetUnits))
                    {
                        targetUnits = new Dictionary<String, Int32>();
                        targetUnitsBySequenceColour.Add(position.SequenceColour.Value, targetUnits);
                    }

                    String productGtin = position.GetPlanogramProduct().Gtin;
                    if (targetUnits.ContainsKey(productGtin)) continue;
                    targetUnits.Add(productGtin, position.TotalUnits);
                }

                PlanogramMerchandisingProductLayout planogramMerchandisingProductLayout = new PlanogramMerchandisingProductLayout(plan, merchandisingGroups, plan.Sequence.Groups.ToList(),
                                            planogramBlocking, planWidth, planHeight, planHeightOffset, planogramProduct4, productStacksBySequenceColour, targetUnitsBySequenceColour, 1, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace);

                Boolean success = planogramMerchandisingProductLayout.TryRelayoutBlockWithNewProduct();
                Assert.IsTrue(success, "The product should have been placed");
                //plan.TakeSnapshot("PlanogramMerchandisingProductLayout_ProductShouldBePlacedInCorrectPosition");
                var actualShelf1Gtins = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                .OrderBy(p => p.SequenceX)
                .Select(p => p.GetPlanogramProduct().Gtin)
                .ToList();
                var expectedShelf1Gtins = new List<String>() { planogramProduct1.Gtin, planogramProduct2.Gtin, planogramProduct3.Gtin };

                shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                    .OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin)
                    .Should().Equal(plan.Products.Select(p => p.Gtin).Take(4), "the first four products should be placed on shelf 1");
                shelf2.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions()
                    .OrderBy(p => p.SequenceX).Select(p => p.GetPlanogramProduct().Gtin)
                    .Should().Equal(plan.Products.Select(p => p.Gtin).Skip(4), "the last two products should be placed on shelf 2");
                
            }
        }

        [Test]
        public void PlanogramMerchandisingProductLayout_CCM18815()
        {
            Planogram plan = "".CreatePackage().AddPlanogram();
            plan.AddBlocking().Dividers.Add(0.5f, 0f, PlanogramBlockingDividerType.Vertical);
            var bay1 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            PlanogramPosition tempPosition = shelf1.AddPosition(bay1);
            PlanogramBlocking planogramBlocking = plan.Blocking.First();
            plan.Sequence.ApplyFromBlocking(planogramBlocking);

            //Remove position from plan so there are no positions in the sequence group initially
            //this is the cause of CCM-18815
            plan.Positions.Remove(tempPosition);

            Single planWidth, planHeight, planHeightOffset;
            plan.GetBlockingAreaSize(out planHeight, out planWidth, out planHeightOffset);

            PlanogramProduct planogramProduct = plan.AddProduct();
            plan.Sequence.Groups.First().AddSequenceGroupProduct(planogramProduct);

            PlanogramMerchandisingGroupList merchandisingGroups = plan.GetMerchandisingGroups();

            Dictionary<Int32, Stack<PlanogramProduct>> productStacksBySequenceColour = planogramBlocking.GetAndUpdateProductStacksByColour(merchandisingGroups);
            Dictionary<Int32, Dictionary<String, Int32>> targetUnitsBySequenceColour = plan.Sequence.Groups.ToDictionary(g => g.Colour, k => new Dictionary<String, Int32>());

            foreach (PlanogramPosition position in plan.Positions)
            {
                if (!position.SequenceColour.HasValue) continue;

                Dictionary<String, Int32> targetUnits;
                if (!targetUnitsBySequenceColour.TryGetValue(position.SequenceColour.Value, out targetUnits))
                {
                    targetUnits = new Dictionary<String, Int32>();
                    targetUnitsBySequenceColour.Add(position.SequenceColour.Value, targetUnits);
                }

                String productGtin = position.GetPlanogramProduct().Gtin;
                if (targetUnits.ContainsKey(productGtin)) continue;
                targetUnits.Add(productGtin, position.TotalUnits);
            }

            PlanogramMerchandisingProductLayout planogramMerchandisingProductLayout = new PlanogramMerchandisingProductLayout(plan, merchandisingGroups, plan.Sequence.Groups.ToList(),
                                        planogramBlocking, planWidth, planHeight, planHeightOffset, planogramProduct, productStacksBySequenceColour, targetUnitsBySequenceColour, 1, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace);

            Boolean success = planogramMerchandisingProductLayout.TryRelayoutBlockWithNewProduct();

            Assert.IsTrue(success, "The product should have been added.");
        }
        #endregion



        #region ProductSequenceGroupHelper

        [Test]
        public void ProductSequenceGroupHelper_GetCorrectAnchorWhenDeviationIsTheSame()
        {
            // Planogram Positions
            // A, C, F, G, H, D, J, L

            // Planogram Sequence
            // A, B, C, D, E, F, G, H, I, J, K, L

            // Add B after A.

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));            
            for (int i = 0; i < 12; i++) plan.AddProduct();

            shelf1.AddPosition(bay, plan.Products[0]);
            shelf1.AddPosition(bay, plan.Products[2]);
            shelf1.AddPosition(bay, plan.Products[5]);
            shelf1.AddPosition(bay, plan.Products[6]);
            shelf1.AddPosition(bay, plan.Products[7]);
            shelf1.AddPosition(bay, plan.Products[3]);
            shelf1.AddPosition(bay, plan.Products[9]);
            shelf1.AddPosition(bay, plan.Products[11]); 

            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(blocking.Groups[0], plan.Products);
            
            List<PlanogramProduct> planogramProducts = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Select(p=> p.GetPlanogramProduct()).ToList();
            ProductSequenceGroupHelper productSequenceGroupHelper = new ProductSequenceGroupHelper(plan.Sequence.Groups[0], planogramProducts, plan.Products[1].Gtin);

            Assert.AreEqual(plan.Products[0].Gtin, productSequenceGroupHelper.AnchorProductGtin, String.Format("Anchor Product should be {0}", plan.Products[0].Gtin));
            Assert.AreEqual(true, productSequenceGroupHelper.AnchorAddAfter);
        }

        [Test]
        public void ProductSequenceGroupHelper_GetCorrectAnchorAddBefore()
        {
            // Planogram Positions
            // A, C, F, G, H, D, J, L

            // Planogram Sequence
            // A, B, C, D, E, F, G, H, I, J, K, L

            // Add E, Left of F.

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            for (int i = 0; i < 12; i++) plan.AddProduct();

            shelf1.AddPosition(bay, plan.Products[0]);
            shelf1.AddPosition(bay, plan.Products[2]);
            shelf1.AddPosition(bay, plan.Products[5]);
            shelf1.AddPosition(bay, plan.Products[6]);
            shelf1.AddPosition(bay, plan.Products[7]);
            shelf1.AddPosition(bay, plan.Products[3]);
            shelf1.AddPosition(bay, plan.Products[9]);
            shelf1.AddPosition(bay, plan.Products[11]);

            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(blocking.Groups[0], plan.Products);

            List<PlanogramProduct> planogramProducts = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Select(p => p.GetPlanogramProduct()).ToList();
            ProductSequenceGroupHelper productSequenceGroupHelper = new ProductSequenceGroupHelper(plan.Sequence.Groups[0], planogramProducts, plan.Products[4].Gtin);

            Assert.AreEqual(plan.Products[5].Gtin, productSequenceGroupHelper.AnchorProductGtin, String.Format("Anchor Product should be {0}", plan.Products[5].Gtin));
            Assert.AreEqual(false, productSequenceGroupHelper.AnchorAddAfter);
        }


        [Test]
        public void ProductSequenceGroupHelper_GetCorrectAnchorAddAfter()
        {
            // Planogram Positions
            // A, D, F, G, H, B, J, L

            // Planogram Sequence
            // A, B, C, D, E, F, G, H, I, J, K, L

            // Add C, right of A

            var plan = "Test".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0));
            for (int i = 0; i < 12; i++) plan.AddProduct();

            shelf1.AddPosition(bay, plan.Products[0]);
            shelf1.AddPosition(bay, plan.Products[2]);
            shelf1.AddPosition(bay, plan.Products[5]);
            shelf1.AddPosition(bay, plan.Products[6]);
            shelf1.AddPosition(bay, plan.Products[7]);
            shelf1.AddPosition(bay, plan.Products[3]);
            shelf1.AddPosition(bay, plan.Products[9]);
            shelf1.AddPosition(bay, plan.Products[11]);

            var blocking = plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups.Clear();
            plan.AddSequenceGroup(blocking.Groups[0], plan.Products);

            List<PlanogramProduct> planogramProducts = shelf1.GetPlanogramSubComponentPlacements().First().GetPlanogramPositions().Select(p => p.GetPlanogramProduct()).ToList();
            ProductSequenceGroupHelper productSequenceGroupHelper = new ProductSequenceGroupHelper(plan.Sequence.Groups[0], planogramProducts, plan.Products[2].Gtin);

            Assert.AreEqual(plan.Products[0].Gtin, productSequenceGroupHelper.AnchorProductGtin, String.Format("Anchor Product should be {0}", plan.Products[0].Gtin));
            Assert.AreEqual(true, productSequenceGroupHelper.AnchorAddAfter);
        }
        
        #endregion
    }
}