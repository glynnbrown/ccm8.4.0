﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27439 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved into common namespace.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Moved to framework
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using NUnit.Framework;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    public class PerformanceDataTotalsTests
    {
        [Test]
        public void Constructor_AggregatesValues()
        {
            var planogram = Planogram.NewPlanogram();
            var performanceData = new[] 
        { 
            PlanogramPerformanceData.NewPlanogramPerformanceData(), 
            PlanogramPerformanceData.NewPlanogramPerformanceData(), 
            PlanogramPerformanceData.NewPlanogramPerformanceData(), 
            PlanogramPerformanceData.NewPlanogramPerformanceData(), 
            PlanogramPerformanceData.NewPlanogramPerformanceData() 
        };
            planogram.Performance.PerformanceData.AddRange(performanceData);
            SetDataValues(performanceData);

            var totals = new PlanogramPerformanceDataTotals(performanceData);

            foreach (var property in typeof(PlanogramPerformanceDataTotals).GetProperties())
            {
                Assert.AreEqual(
                    performanceData.Select(p => p.GetType().GetProperty(property.Name).GetValue(p, null)).Cast<Single>().Sum(),
                    property.GetValue(totals, null));
            }
        }

        private void SetDataValues(IEnumerable<PlanogramPerformanceData> dataItems)
        {
            foreach (var data in dataItems)
            {
                var properties = data.GetType().GetProperties();
                var random = new Random();
                foreach (var property in properties)
                {
                    if (!property.Name.StartsWith("P") || property.Name.Length > 3) continue;
                    property.SetValue(data, (Single?)random.Next(10), null);
                }
            }
        }
    }
}
