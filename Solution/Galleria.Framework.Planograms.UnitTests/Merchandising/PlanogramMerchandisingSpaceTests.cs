﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-28484 : A.Kuszyk
//   Created.
#endregion

#region Version History : CCM810
// V8-30032 : A.Kuszyk
//  Added tests for top shelf using height of tallest product.
#endregion
#region Version History : CCM830
// V8-31674 : A.Kuszyk
//  Reemoved tests for top shelf using height of tallest product.
// V8-32239 : A.Kuszyk
//  Fixed issue with AddToRight not accounting for rotation.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Galleria.Framework.Planograms.UnitTests.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Helpers;
using FluentAssertions;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    [Category(Categories.Smoke)]
    public class PlanogramMerchandisingSpaceTests
    {
        private Single[] RotationSource
        {
            get
            {
                return new Single[]
                {
                    -Convert.ToSingle(Math.PI),
                    -Convert.ToSingle(Math.PI/4f),
                    0f,
                    Convert.ToSingle(Math.PI/2f),
                    Convert.ToSingle(Math.PI),
                };
            }
        }

        private Single[] MultiplesOfPiByTwo
        {
            get
            {
                return new Single[]
                {
                    -Convert.ToSingle(Math.PI),
                    -Convert.ToSingle(Math.PI/2f),
                    0f,
                    Convert.ToSingle(Math.PI/2f),
                    Convert.ToSingle(Math.PI),
                };
            }
        }

        #region Constructor

        #region Stack

        #region No Face Thickness
        [Test]
        public void Constructor_ForStack_NoFaceThickness_NoMerchHeight_UnhinderedSpaceIsUpToPlanHeight([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                subCompPlacementBounds.Y + subCompPlacementBounds.Height,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                plan.Fixtures.First().Height - subCompPlacementBounds.Y - subCompPlacementBounds.Height,
                subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForStack_NoFaceThickness_WithMerchHeight_UnhinderedSpaceIsUpToMerchHeight([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 10;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                subCompPlacementBounds.Y + subCompPlacementBounds.Height,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                subCompPlacement.SubComponent.MerchandisableHeight,
                subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForStack_NoFaceThickness_OnlyComponent_ObstructionsIsEmpty([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.IsEmpty(merchSpace.Obstructions);
        }

        [Test]
        public void Constructor_ForStack_NoFaceThickness_OtherStackComponents_ObstructionsContainsOverlaps([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue obstruction = new RectValue(
                overlappingBounds.X,
                overlappingBounds.Y,
                overlappingBounds.Z,
                overlappingBounds.Width,
                plan.Fixtures.First().Height - overlappingBounds.Y,
                overlappingBounds.Depth);
            CollectionAssert.Contains(
                merchSpace.Obstructions,
                obstruction);
        }

        [Test]
        public void Constructor_ForStack_NoFaceThickness_MultiBayOtherStackComponents_ObstructionsContainsOverlaps([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0,120,0));
            var bay2 = plan.AddFixtureItem(); bay2.X = 120;
            var fixComp = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var overlappingFixComp = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var bay3 = plan.AddFixtureItem(); bay3.X = 240;
            bay3.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 120, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), bay2);
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue obstruction = new RectValue(
                overlappingBounds.X,
                overlappingBounds.Y,
                overlappingBounds.Z,
                overlappingBounds.Width,
                plan.Fixtures.First().Height - overlappingBounds.Y,
                overlappingBounds.Depth);
            CollectionAssert.Contains(
                merchSpace.Obstructions,
                obstruction);
        }

        [Test]
        public void Constructor_ForStack_NoFaceThickness_HangComponents_ObstructionsContainsOverlaps([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 150, 0));
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.Contains(
                merchSpace.Obstructions,
                overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First()));
        }

        [Test]
        public void Constructor_ForStack_NoFaceThickness_HangFromBottomComponents_ObstructionsContainsOverlaps([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 150, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = ignoreSpaceOverlaps ?
                new RectValue(
                    overlappingBounds.X,
                    overlappingBounds.Y,
                    overlappingBounds.Z,
                    overlappingBounds.Width,
                    plan.Fixtures.First().Height - overlappingBounds.Y,
                    overlappingBounds.Depth) :
                new RectValue(
                    overlappingBounds.X,
                    subCompPlacementBounds.Y + subCompPlacementBounds.Height,
                    overlappingBounds.Z,
                    overlappingBounds.Width,
                    plan.Fixtures.First().Height - subCompPlacementBounds.Y - subCompPlacementBounds.Height,
                    overlappingBounds.Depth);
            CollectionAssert.Contains(
                merchSpace.Obstructions,
                expected);
        }

        [Test]
        public void Constructor_ForStack_NoFaceThickness_WithObstructions_SpaceReducedObservesObstructions()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 150, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, true);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                subCompPlacementBounds.Y + subCompPlacementBounds.Height,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                overlappingBounds.Y - subCompPlacementBounds.Y - subCompPlacementBounds.Height,
                subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.SpaceReducedByObstructions);
        }

        #endregion

        #region Face Thickness
        [Test]
        public void Constructor_ForStack_FaceThickness_NoMerchHeight_UnhinderedSpaceIsUpSubComponentTop([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Chest);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X + subCompPlacement.SubComponent.FaceThicknessLeft,
                subCompPlacementBounds.Y + subCompPlacement.SubComponent.FaceThicknessBottom,
                subCompPlacementBounds.Z + subCompPlacement.SubComponent.FaceThicknessBack,
                subCompPlacementBounds.Width - subCompPlacement.SubComponent.FaceThicknessRight - subCompPlacement.SubComponent.FaceThicknessLeft,
                subCompPlacementBounds.Height - subCompPlacementBounds.Y - subCompPlacement.SubComponent.FaceThicknessBottom,
                subCompPlacementBounds.Depth - subCompPlacement.SubComponent.FaceThicknessBack - subCompPlacement.SubComponent.FaceThicknessFront);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForStack_FaceThickness_WithMerchHeight_UnhinderedSpaceIsUpToMerchHeight([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Chest);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 10;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X + subCompPlacement.SubComponent.FaceThicknessLeft,
                subCompPlacementBounds.Y + subCompPlacement.SubComponent.FaceThicknessBottom,
                subCompPlacementBounds.Z + subCompPlacement.SubComponent.FaceThicknessBack,
                subCompPlacementBounds.Width - subCompPlacement.SubComponent.FaceThicknessRight - subCompPlacement.SubComponent.FaceThicknessLeft,
                subCompPlacement.SubComponent.MerchandisableHeight,
                subCompPlacementBounds.Depth - subCompPlacement.SubComponent.FaceThicknessBack - subCompPlacement.SubComponent.FaceThicknessFront);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForStack_FaceThickness_OnlyComponent_ObstructionsIsEmpty([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Chest);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.IsEmpty(merchSpace.Obstructions);
        }

        [Test]
        public void Constructor_ForStack_FaceThickness_OtherStackComponents_ObstructionsIsEmpty([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Chest, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.IsEmpty(merchSpace.Obstructions);
        }

        [Test]
        public void Constructor_ForStack_FaceThickness_HangComponents_ObstructionsIsEmpty([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Chest, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 150, 0));
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.IsEmpty(merchSpace.Obstructions);
        }

        [Test]
        public void Constructor_ForStack_FaceThickness_HangFromBottomComponents_ObstructionsIsEmpty([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Chest, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 150, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.IsEmpty(merchSpace.Obstructions);
        }

        [Test]
        public void Constructor_ForStack_FaceThickness_WithObstructions_SpaceReducedObservesObstructions()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Chest, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 150, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 10, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, true);

            AssertHelper.AreRectValuesEqual(merchSpace.UnhinderedSpace, merchSpace.SpaceReducedByObstructions);
        }
        #endregion

        #region Rotated

        [Test]
        public void Constructor_ForStack_WithFixtureComponentRotation_DoesChangeDimensions(
            [ValueSource("RotationSource")] Single slope,
            [ValueSource("RotationSource")] Single angle,
            [ValueSource("RotationSource")] Single roll)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.Slope = slope;
            shelf.Angle = angle;
            shelf.Roll = roll;

            var merchSpace = new PlanogramMerchandisingSpace(
                plan.GetPlanogramSubComponentPlacements().First(), plan.GetPlanogramSubComponentPlacements());

            AssertHelper.AreRectValuesEqual(
                new RectValue(0, 4, 0, 120, 196, 75),
                merchSpace.SpaceReducedByObstructions);
        }

        [Test]
        public void Constructor_ForStack_WithFixtureItemRotation_DoesChangeDimensions(
            [ValueSource("RotationSource")] Single slope,
            [ValueSource("RotationSource")] Single angle,
            [ValueSource("RotationSource")] Single roll)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.Angle = angle;
            bay.Slope = slope;
            bay.Roll = roll;
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);

            var merchSpace = new PlanogramMerchandisingSpace(
                plan.GetPlanogramSubComponentPlacements().First(), plan.GetPlanogramSubComponentPlacements());

            AssertHelper.AreRectValuesEqual(
                new RectValue(0, 4, 0, 120, 196, 75),
                merchSpace.SpaceReducedByObstructions);
        }

        [Test]
        public void Constructor_ForStack_WithFixtureItemAndComponentRotation_DoesChangeDimensions(
            [ValueSource("RotationSource")] Single slope,
            [ValueSource("RotationSource")] Single angle,
            [ValueSource("RotationSource")] Single roll)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.Angle = angle;
            bay.Slope = slope;
            bay.Roll = roll;
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf.Slope = slope;
            shelf.Angle = angle;
            shelf.Roll = roll;

            var merchSpace = new PlanogramMerchandisingSpace(
                plan.GetPlanogramSubComponentPlacements().First(), plan.GetPlanogramSubComponentPlacements());

            AssertHelper.AreRectValuesEqual(
                new RectValue(0, 4, 0, 120, 196, 75),
                merchSpace.SpaceReducedByObstructions);
        }

        #endregion

        #endregion

        #region Hang

        [Test]
        public void Constructor_ForHang_WhenMaximiseWidthTrue_AndObstructionsBelow_ShouldReduceWidth()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 0, 0), new WidthHeightDepthValue(60, 4, 75));
            var bar = bay.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(60, 50, 0), new WidthHeightDepthValue(60, 4, 4));


            var merchSpace = new PlanogramMerchandisingSpace(
                bar.GetPlanogramSubComponentPlacements().First(),
                plan.GetPlanogramSubComponentPlacements(),
                ignoreSpaceOverlaps: false,
                maximiseWidthForHang: true,
                maximiseDepthForHang: true);

            merchSpace.UnhinderedSpace.Width.Should().Be(
                60, "because the merchandisable width of the bar is constrained by the shelf");
        }

        [Test]
        public void Constructor_ForHang_WhenMaximiseWidthTrue_AndObstructionsImmediatelyBelow_ShouldNotReduceWidth()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var bar = bay.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 100, 0));


            var merchSpace = new PlanogramMerchandisingSpace(
                bar.GetPlanogramSubComponentPlacements().First(),
                plan.GetPlanogramSubComponentPlacements(),
                ignoreSpaceOverlaps: false,
                maximiseWidthForHang: true,
                maximiseDepthForHang: true);

            merchSpace.UnhinderedSpace.Width.Should().Be(
                120, "because the merchandisable width of the bar should not be constrained by the shelf");
        }

        [Test]
        public void Constructor_ForHang_WhenMaximiseWidthTrue_AndObstructionsPartiallyBelow_ShouldNotReduceWidth()
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(-60,0,0));
            var bar = bay.AddFixtureComponent(PlanogramComponentType.Bar, new PointValue(0, 100, 0));


            var merchSpace = new PlanogramMerchandisingSpace(
                bar.GetPlanogramSubComponentPlacements().First(),
                plan.GetPlanogramSubComponentPlacements(),
                ignoreSpaceOverlaps: false,
                maximiseWidthForHang: true,
                maximiseDepthForHang: true);

            merchSpace.UnhinderedSpace.Width.Should().Be(
                120, "because the merchandisable width of the bar should not be constrained by the shelf");
        }

        [Test]
        public void Constructor_ForHang_WhenMaximiseDepthTrue_NoMerchDepth_UnhinderedSpaceIsUpToPlanFront([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Peg);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableDepth = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(
                subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps, maximiseDepthForHang:true);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                subCompPlacementBounds.Y,
                subCompPlacementBounds.Z + subCompPlacementBounds.Depth,
                subCompPlacementBounds.Width,
                subCompPlacementBounds.Height,
                plan.Fixtures.First().Depth - subCompPlacementBounds.Z - subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHang_WhenMaximiseDepthFalse_NoMerchDepth_UnhinderedSpaceIsUpToPlanFront([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Peg);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableDepth = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(
                subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps, maximiseDepthForHang: false);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                subCompPlacementBounds.Y,
                subCompPlacementBounds.Z + subCompPlacementBounds.Depth,
                subCompPlacementBounds.Width,
                subCompPlacementBounds.Height,
                0);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHang_WithMerchDepth_UnhinderedSpaceIsMerchDepthDeep([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Peg);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableDepth = 10;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                subCompPlacementBounds.Y,
                subCompPlacementBounds.Z + subCompPlacementBounds.Depth,
                subCompPlacementBounds.Width,
                subCompPlacementBounds.Height,
                subCompPlacement.SubComponent.MerchandisableDepth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHang_WhenOtherComponents_ObstructionsIsEmpty([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 0, 0));
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 180, 0));
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableDepth = 10;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.IsEmpty(merchSpace.Obstructions);
        }

        [Test]
        public void Constructor_ForHangAndMaximiseWidth_UsesSpaceOnEitherSide()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem(); bay2.X = bay1.GetPlanogramFixture().Width;
            var bay3 = plan.AddFixtureItem(); bay3.X = bay1.GetPlanogramFixture().Width * 2;
            plan.Width = 120 * 3;
            var peg = bay2.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 80, 0));

            var subComponentPlacement = peg.GetPlanogramSubComponentPlacements().First();
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();

            PlanogramMerchandisingSpace merchSpace = new PlanogramMerchandisingSpace(subComponentPlacement, allSubComponentPlacements, false,true);

            var expected = new RectValue(0, 0, 4, 120 * 3, 200, 0);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHangAndMaximiseWidth_UsesSpaceOnEitherSideUpToOtherComponents()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var bay2 = plan.AddFixtureItem(); bay2.X = bay1.GetPlanogramFixture().Width;
            var bay3 = plan.AddFixtureItem(); bay3.X = bay1.GetPlanogramFixture().Width * 2;
            bay3.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            plan.Width = 120 * 3;
            var peg = bay2.AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 80, 0));

            var subComponentPlacement = peg.GetPlanogramSubComponentPlacements().First();
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();

            PlanogramMerchandisingSpace merchSpace = new PlanogramMerchandisingSpace(subComponentPlacement, allSubComponentPlacements, false, true);

            var expected = new RectValue(120, 0, 4, 120, 200, 0);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        #endregion

        #region Hang From Bottom

        [Test]
        public void Constructor_ForHangFromBottom_NoMerchDepth_UnhinderedSpaceIsDeepAsSubComponent([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 100, 0));
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            subCompPlacement.SubComponent.MerchandisableDepth = 0;
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                0,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                subCompPlacementBounds.Y,
                subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHangFromBottom_WithMerchDepth_UnhinderedSpaceIsMerchDepthDeep([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 100, 0));
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            subCompPlacement.SubComponent.MerchandisableDepth = 20;
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                0,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                subCompPlacementBounds.Y,
                subCompPlacement.SubComponent.MerchandisableDepth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHangFromBottom_NoMerchHeight_UnhinderedSpaceIsFromBottomToPlanBottom([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 100, 0));
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                0,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                subCompPlacementBounds.Y,
                subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHangFromBottom_WithMerchHeight_UnhinderedSpaceIsFromBottomToMerchHeightDown([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 100, 0));
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            subCompPlacement.SubComponent.MerchandisableHeight = 20;
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                subCompPlacementBounds.Y - subCompPlacement.SubComponent.MerchandisableHeight,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                subCompPlacement.SubComponent.MerchandisableHeight,
                subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.UnhinderedSpace);
        }

        [Test]
        public void Constructor_ForHangFromBottom_OnlyComponent_ObstructionsIsEmpty([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod);
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            CollectionAssert.IsEmpty(merchSpace.Obstructions);
        }

        [Test]
        public void Constructor_ForHangFromBottom_OtherStackComponents_ObstructionsContainsOverlaps([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 100, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 50, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue obstruction = new RectValue(
                subCompPlacementBounds.X,
                0,
                overlappingBounds.Z,
                subCompPlacementBounds.Width,
                overlappingBounds.Y + overlappingBounds.Height,
                overlappingBounds.Depth);
            CollectionAssert.Contains(
                merchSpace.Obstructions,
                obstruction);
        }

        [Test]
        public void Constructor_ForHangFromBottom_HangComponents_ObstructionsContainsOverlaps([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 150, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Peg, new PointValue(0, 0, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 200, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue obstruction = new RectValue(
                subCompPlacementBounds.X,
                overlappingBounds.Y,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                overlappingBounds.Height,
                overlappingBounds.Depth);
            CollectionAssert.Contains(
                merchSpace.Obstructions,
                obstruction);
        }

        [Test]
        public void Constructor_ForHangFromBottom_HangFromBottomComponents_ObstructionsContainsOverlaps([Values(true, false)] Boolean ignoreSpaceOverlaps)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 150, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 100, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 190, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, ignoreSpaceOverlaps);

            RectValue expected = new RectValue(
                overlappingBounds.X,
                0,
                overlappingBounds.Z,
                overlappingBounds.Width,
                overlappingBounds.Y + overlappingBounds.Height,
                overlappingBounds.Depth);
            CollectionAssert.Contains(
                merchSpace.Obstructions,
                expected);
        }

        [Test]
        public void Constructor_ForHangFromBottom_WithObstructions_SpaceReducedObservesObstructions()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var fixComp = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 150, 0));
            var overlappingFixComp = plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Rod, new PointValue(0, 100, 0));
            var overlappingBounds = overlappingFixComp.GetPlanogramRelativeBoundingBox(overlappingFixComp.GetPlanogramComponent(), plan.FixtureItems.First());
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 190, 0)); // Not overlapping
            var subCompPlacement = fixComp.GetPlanogramSubComponentPlacements().First();
            var subCompPlacementBounds = subCompPlacement.GetPlanogramRelativeBoundingBox();
            subCompPlacement.SubComponent.MerchandisableHeight = 0;
            var allSubCompPlacements = plan.GetPlanogramSubComponentPlacements();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, allSubCompPlacements, true);

            RectValue expected = new RectValue(
                subCompPlacementBounds.X,
                overlappingBounds.Y + overlappingBounds.Height,
                subCompPlacementBounds.Z,
                subCompPlacementBounds.Width,
                subCompPlacementBounds.Y - overlappingBounds.Y - overlappingBounds.Height,
                subCompPlacementBounds.Depth);
            AssertHelper.AreRectValuesEqual(expected, merchSpace.SpaceReducedByObstructions);
        }

        #endregion

        #region Planogram Relative Coordinates
        [Test]
        public void Constructor_ReturnsPlanogramRelativeCoordinates_AcrossFixtures()
        {
            var plan = "".CreatePackage().AddPlanogram();
            plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlacement = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();

            var merchSpace = new PlanogramMerchandisingSpace(subCompPlacement, plan.GetPlanogramSubComponentPlacements());

            Assert.AreEqual(plan.Fixtures.First().Width, merchSpace.UnhinderedSpace.X);
        }

        [Test]
        public void Constructor_ReturnsPlanogramRelativeCoordinates_OnRotatedFixture()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var bay2 = plan.AddFixtureItem();
            bay2.X = 180;
            bay2.Angle = Convert.ToSingle(Math.PI / 2f);
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var shelf2SubCompPlacement = shelf2.GetPlanogramSubComponentPlacements().First();

            var merchSpace = new PlanogramMerchandisingSpace(shelf2SubCompPlacement, plan.GetPlanogramSubComponentPlacements());

            AssertHelper.AreSinglesEqual(bay2.X, merchSpace.UnhinderedSpace.X);
            AssertHelper.AreSinglesEqual(Convert.ToSingle(Math.PI / 2f), merchSpace.Rotation.Angle);
        } 
        #endregion

        #endregion

        #region CombineWith

        [Test]
        public void CombineWith_CombinesUnhinderedSpace()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace1 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(subCompPlac2, plan.GetPlanogramSubComponentPlacements());

            var combinedSpace = merchSpace1.CombineWith(merchSpace2);

            Assert.AreEqual(merchSpace1.UnhinderedSpace.Union(merchSpace2.UnhinderedSpace), combinedSpace.UnhinderedSpace);
        }

        [Test]
        public void CombineWith_CombinesObstructions()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            plan.FixtureItems.First().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            plan.FixtureItems[1].AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var merchSpace1 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(subCompPlac2, plan.GetPlanogramSubComponentPlacements());

            var combinedSpace = merchSpace1.CombineWith(merchSpace2);

            CollectionAssert.IsSubsetOf(merchSpace1.Obstructions, combinedSpace.Obstructions);
            CollectionAssert.IsSubsetOf(merchSpace2.Obstructions, combinedSpace.Obstructions);
        }

        [Test]
        public void CombineWith_RecalculatesReducedSpace()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem(); bay2.X = 120;
            var b1Shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var b1Shelf2 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 150, 0));
            var b2Shelf1 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var b2Shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 120, 0));
            var b1Shelf1MerchSpace = new PlanogramMerchandisingSpace(
                b1Shelf1.GetPlanogramSubComponentPlacements().First(), plan.GetPlanogramSubComponentPlacements());
            var b2Shelf1MerchSpace = new PlanogramMerchandisingSpace(
                b2Shelf1.GetPlanogramSubComponentPlacements().First(), plan.GetPlanogramSubComponentPlacements());

            var combinedSpace = b1Shelf1MerchSpace.CombineWith(b2Shelf1MerchSpace);

            RectValue expected = new RectValue(0, 104, 0, 240, 16, 75);
            AssertHelper.AreRectValuesEqual(expected, combinedSpace.SpaceReducedByObstructions);
        }

        #endregion

        #region CombineSpaces

        [Test]
        public void CombineSpaces_ReturnsOriginalList_WhenNoSpacesAreContinuous()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 110, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace1 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(subCompPlac2, plan.GetPlanogramSubComponentPlacements());
            var spacesToCombine = new List<PlanogramMerchandisingSpace>() { merchSpace1, merchSpace2 };

            var result = PlanogramMerchandisingSpace.CombineSpaces(spacesToCombine);

            CollectionAssert.AreEqual(spacesToCombine, result);
        }

        [Test]
        public void CombineSpaces_CombinesAllSpaces_WhenAllAreContinuous()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac3 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace1 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(subCompPlac2, plan.GetPlanogramSubComponentPlacements());
            var merchSpace3 = new PlanogramMerchandisingSpace(subCompPlac3, plan.GetPlanogramSubComponentPlacements());
            var spacesToCombine = new List<PlanogramMerchandisingSpace>() { merchSpace1, merchSpace2, merchSpace3 };

            var result = PlanogramMerchandisingSpace.CombineSpaces(spacesToCombine);

            Assert.AreEqual(1, result.Count());
            AssertHelper.AreRectValuesEqual(merchSpace1.UnhinderedSpace.Union(merchSpace2.UnhinderedSpace).Union(merchSpace3.UnhinderedSpace), result.First().UnhinderedSpace);
        }

        [Test]
        public void CombineSpaces_CombinesContinuousSpacesOnly_WhenSomeAreContinuous()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac3 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 110, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace1 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(subCompPlac2, plan.GetPlanogramSubComponentPlacements());
            var merchSpace3 = new PlanogramMerchandisingSpace(subCompPlac3, plan.GetPlanogramSubComponentPlacements());
            var spacesToCombine = new List<PlanogramMerchandisingSpace>() { merchSpace1, merchSpace2, merchSpace3 };

            var result = PlanogramMerchandisingSpace.CombineSpaces(spacesToCombine);

            Assert.AreEqual(2, result.Count());
            AssertHelper.AreRectValuesEqual(merchSpace1.UnhinderedSpace.Union(merchSpace2.UnhinderedSpace), result.First().UnhinderedSpace);
            Assert.AreEqual(merchSpace3, result.ElementAt(1));
        }

        [Test]
        public void CombineSpaces_AccountsForRotationWhenCombiningVolumes()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            bay1.Angle = Convert.ToSingle(Math.PI / 2f);
            var bay2 = plan.AddFixtureItem();
            bay2.Angle = Convert.ToSingle(Math.PI / 2f);
            bay2.X = 0;
            bay2.Z = 120;

            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;

            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf2.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            RectValue expected = new RectValue(0, 4, 0, 240, 96, 75);

            //plan.Parent.SaveAs(1, @"C:\Users\usr145\ztest.pog");

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                Assert.AreEqual(1, merchGroups.Count);
                var merchGroup = merchGroups.First();

                var shelf1MerchSpace = new PlanogramMerchandisingSpace(
                     merchGroup.SubComponentPlacements.First(), merchGroup.SubComponentPlacements);
                var shelf2MerchSpace = new PlanogramMerchandisingSpace(
                     merchGroup.SubComponentPlacements.Last(), merchGroup.SubComponentPlacements);

                var combinedSpaces = PlanogramMerchandisingSpace.CombineSpaces(
                    new List<PlanogramMerchandisingSpace> { shelf1MerchSpace, shelf2MerchSpace });

                Assert.AreEqual(1, combinedSpaces.Count());
                AssertHelper.AreRectValuesEqual(expected, combinedSpaces.First().ToMerchandisingGroupRelative(merchGroup).UnhinderedSpace);
            }
        }

        #endregion

        #region Equals

        [Test]
        public void Equals_ReturnsTrue_WhenObjectsHaveSameValues()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace1 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());

            Boolean result = merchSpace1.Equals(merchSpace2);

            Assert.IsTrue(result);
        }

        [Test]
        public void Equals_ReturnsFalse_WhenObjectsHaveDifferentValues()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 110, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace1 = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(subCompPlac2, plan.GetPlanogramSubComponentPlacements());

            Boolean result = merchSpace1.Equals(merchSpace2);

            Assert.IsFalse(result);
        }

        #endregion

        #region ReduceToFit

        [Test]
        public void ReduceToFit_DoesNothing_WhenBoundsExceedSpace()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace = new PlanogramMerchandisingSpace(subCompPlac, plan.GetPlanogramSubComponentPlacements());
            RectValue boundingSpace = new RectValue(0, 0, 0, 1000, 1000, 1000);

            var result = merchSpace.ReduceToFit(boundingSpace);

            Assert.AreEqual(merchSpace, result);
        }

        [Test]
        public void ReduceToFit_ReturnsNull_WhenBoundsDoNotIntersectSpace()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace = new PlanogramMerchandisingSpace(subCompPlac, plan.GetPlanogramSubComponentPlacements());
            RectValue boundingSpace = new RectValue(0, 1000, 0, 1, 1, 1);

            var result = merchSpace.ReduceToFit(boundingSpace);

            Assert.IsNull(result);
        }

        [Test]
        public void ReduceToFit_ReturnsIntersectingUnhinderedSpace_WhenBoundsIntersect()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace = new PlanogramMerchandisingSpace(subCompPlac, plan.GetPlanogramSubComponentPlacements());
            RectValue boundingSpace = new RectValue(0, 50, 0, 60, 100, 50);

            var result = merchSpace.ReduceToFit(boundingSpace);

            RectValue expected = new RectValue(0, 104, 0, 60, 46, 50);
            Assert.AreEqual(expected, result.UnhinderedSpace);
        }

        [Test]
        public void ReduceToFit_ReturnsIntersectingObstructions_WhenBoundsIntersect()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.FixtureItems[0].AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 120, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            RectValue boundingSpace = new RectValue(0, 50, 0, 60, 100, 50);

            var result = merchSpace.ReduceToFit(boundingSpace);

            RectValue expected = new RectValue(0, 120, 0, 60, 30, 50);
            CollectionAssert.IsNotEmpty(result.Obstructions);
            AssertHelper.AreRectValuesEqual(expected, result.Obstructions.First());
        }

        [Test]
        public void ReduceToFit_ReturnsIntersectingReducedSpace_WhenBoundsIntersect()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var subCompPlac1 = plan.AddFixtureItem().AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0)).GetPlanogramSubComponentPlacements().First();
            var subCompPlac2 = plan.FixtureItems[0].AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 120, 0)).GetPlanogramSubComponentPlacements().First();
            var merchSpace = new PlanogramMerchandisingSpace(subCompPlac1, plan.GetPlanogramSubComponentPlacements());
            RectValue boundingSpace = new RectValue(0, 50, 0, 60, 100, 50);

            var result = merchSpace.ReduceToFit(boundingSpace);

            RectValue expected = new RectValue(0, 104, 0, 60, 16, 50);
            AssertHelper.AreRectValuesEqual(expected, result.SpaceReducedByObstructions);
        }

        [Test]
        public void ReduceToFit_AccountsForRotation_OnChest()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();
            var boundingSpace = new RectValue(0, 0, 0, 120, 25, 75);

            using(var merchGroups = plan.GetMerchandisingGroups())
	        {
                var merchGroup = merchGroups.First();
                var dvp = new DesignViewPosition(merchGroup);

                var merchSpace = new PlanogramMerchandisingSpace(allSubComponentPlacements.First(), allSubComponentPlacements);
                var merchGroupMerchSpace = merchSpace.ToMerchandisingGroupRelative(merchGroup);
                var designViewMerchSpace = merchGroupMerchSpace.Transform(dvp.TranslationMatrix, dvp.TransformationRotation);
                var reducedMerchSpace = designViewMerchSpace.ReduceToFit(boundingSpace);

                Assert.IsNotNull(reducedMerchSpace);
                Assert.AreEqual(new RotationValue(0, -Convert.ToSingle(Math.PI / 2f), 0), reducedMerchSpace.Rotation);
                AssertHelper.AreRectValuesEqual(new RectValue(4, 79, 50, 112, 46, 21), reducedMerchSpace.UnhinderedSpace);
	        }
        }

        #endregion

        #region RotatedUnhinderedSpace

        [Test]
        public void RotatedUnhinderedSpace_TransformsCorrectly()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();
            var boundingSpace = new RectValue(0, 0, 0, 120, 25, 75);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();
                var dvp = new DesignViewPosition(merchGroup);

                var merchSpace = new PlanogramMerchandisingSpace(allSubComponentPlacements.First(), allSubComponentPlacements);
                var merchGroupMerchSpace = merchSpace.ToMerchandisingGroupRelative(merchGroup);
                var designViewMerchSpace = merchGroupMerchSpace.Transform(dvp.TranslationMatrix, dvp.TransformationRotation);

                var expectedUnRotatedSpace = new RectValue(4, 79, 4, 112, 46, 67);
                var expectedRotation = new RotationValue(0, -Convert.ToSingle(Math.PI / 2f), 0);
                AssertHelper.AreRotationValuesEqual(expectedRotation, designViewMerchSpace.Rotation);
                AssertHelper.AreRectValuesEqual(expectedUnRotatedSpace, designViewMerchSpace.UnhinderedSpace);
                var expectedRotatedSpace = new RectValue(4, 4, 4, 112, 67, 46);
                AssertHelper.AreRectValuesEqual(expectedRotatedSpace, designViewMerchSpace.RotatedUnhinderedSpace);
            }
        }

        [Test]
        public void RotatedUnhinderedSpace_TransformsCorrectly_AfterPriorTransform()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();
            var boundingSpace = new RectValue(0, 0, 0, 120, 25, 75);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();
                var dvp = new DesignViewPosition(merchGroup);

                var merchSpace = new PlanogramMerchandisingSpace(allSubComponentPlacements.First(), allSubComponentPlacements);
                var merchGroupMerchSpace = merchSpace.ToMerchandisingGroupRelative(merchGroup);
                var designViewMerchSpace = merchGroupMerchSpace.Transform(dvp.TranslationMatrix, dvp.TransformationRotation);
                var reducedMerchSpace = designViewMerchSpace.ReduceToFit(boundingSpace);

                Assert.IsNotNull(reducedMerchSpace);
                Assert.AreEqual(new RotationValue(0, -Convert.ToSingle(Math.PI / 2f), 0), reducedMerchSpace.Rotation);
                AssertHelper.AreRectValuesEqual(new RectValue(4, 4, 4, 112, 21, 46), reducedMerchSpace.RotatedUnhinderedSpace);
            }
        }

        [Test]
        public void RotatedUnhinderedSpace_TransformsCorrectly_WithBayRotationAndTwoCombinedShelves()
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            bay1.Angle = Convert.ToSingle(Math.PI / 2f);
            var bay2 = plan.AddFixtureItem();
            bay2.Angle = Convert.ToSingle(Math.PI / 2f);
            bay2.X = 0;
            bay2.Z = 120;

            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            var expected1 = new RectValue(-75, 104, 0, 75, 96, 120);

            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf2.GetPlanogramComponent().SubComponents[0].CombineType = PlanogramSubComponentCombineType.Both;
            var expected2 = new RectValue(-75, 104, 120, 75, 96, 120);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                Assert.AreEqual(1, merchGroups.Count);
                var merchGroup = merchGroups.First();

                var shelf1MerchSpace = new PlanogramMerchandisingSpace(
                     merchGroup.SubComponentPlacements.First(), merchGroup.SubComponentPlacements);
                var shelf2MerchSpace = new PlanogramMerchandisingSpace(
                     merchGroup.SubComponentPlacements.Last(), merchGroup.SubComponentPlacements);

                AssertHelper.AreRectValuesEqual(expected1, shelf1MerchSpace.RotatedUnhinderedSpace);
                AssertHelper.AreRectValuesEqual(expected2, shelf2MerchSpace.RotatedUnhinderedSpace);
            }
        }
        #endregion

        #region Transform
        [Test]
        public void Transform_AppliesTranslationAndRotation()
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var chest = bay.AddFixtureComponent(PlanogramComponentType.Chest);
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();
            var boundingSpace = new RectValue(0, 0, 0, 120, 25, 75);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroup = merchGroups.First();
                var dvp = new DesignViewPosition(merchGroup);

                var merchSpace = new PlanogramMerchandisingSpace(allSubComponentPlacements.First(), allSubComponentPlacements);
                var merchGroupMerchSpace = merchSpace.ToMerchandisingGroupRelative(merchGroup);
                var designViewMerchSpace = merchGroupMerchSpace.Transform(dvp.TranslationMatrix, dvp.TransformationRotation);

                var expectedUnRotatedSpace = new RectValue(4, 79, 4, 112, 46, 67);
                var expectedRotation = new RotationValue(0, -Convert.ToSingle(Math.PI / 2f), 0);
                AssertHelper.AreRotationValuesEqual(expectedRotation, designViewMerchSpace.Rotation);
                AssertHelper.AreRectValuesEqual(expectedUnRotatedSpace, designViewMerchSpace.UnhinderedSpace);
            }
        } 
        #endregion

        #region ToMerchandisingGroupRelative

        [Test]
        public void ToMerchandisingGroupRelative_PerformsTransformation_OnRotatedFixtureComponent(
            [ValueSource("RotationSource")] Single slope,
            [ValueSource("RotationSource")] Single angle,
            [ValueSource("RotationSource")] Single roll)
        {
            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf.Slope = slope;
            shelf.Angle = angle;
            shelf.Roll = roll;
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();
            var subComponentPlacement = allSubComponentPlacements.First(s => s.FixtureComponent == shelf);
            var merchSpace = new PlanogramMerchandisingSpace(subComponentPlacement, allSubComponentPlacements);
            RectValue expected = new RectValue(0, 4, 0, 120, 96, 75);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroupMerchSpace = merchSpace.ToMerchandisingGroupRelative(merchGroups.First());

                Assert.IsTrue(merchGroupMerchSpace.IsMerchandisingGroupRelative);
                Assert.AreEqual(new RotationValue(), merchGroupMerchSpace.Rotation);
                AssertHelper.AreRectValuesEqual(expected, merchGroupMerchSpace.UnhinderedSpace);
            }
        }

        [Test]
        public void ToMerchandisingGroupRelative_PerformsTransformation_OnRotatedFixtureItem(
            [Values(0f)] Single slope,
            [ValueSource("RotationSource")] Single angle,
            [Values(0f)] Single roll)
        {
            // Note that fixtures can only have angle rotation.

            var plan = "".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.Slope = slope;
            bay.Angle = angle;
            bay.Roll = roll;
            var shelf = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            var allSubComponentPlacements = plan.GetPlanogramSubComponentPlacements();
            var subComponentPlacement = allSubComponentPlacements.First(s => s.FixtureComponent == shelf);
            var merchSpace = new PlanogramMerchandisingSpace(subComponentPlacement, allSubComponentPlacements);
            RectValue expected = new RectValue(0, 4, 0, 120, 96, 75);

            using (var merchGroups = plan.GetMerchandisingGroups())
            {
                var merchGroupMerchSpace = merchSpace.ToMerchandisingGroupRelative(merchGroups.First());

                Assert.IsTrue(merchGroupMerchSpace.IsMerchandisingGroupRelative);
                Assert.AreEqual(new RotationValue(), merchGroupMerchSpace.Rotation);
                AssertHelper.AreRectValuesEqual(expected, merchGroupMerchSpace.UnhinderedSpace);
            }
        }

        #endregion

        #region AddToRight

        [Test]
        public void AddToRight_ExcludesOverhangs_WithRotation(
            [ValueSource("MultiplesOfPiByTwo")] Single angle,
            [ValueSource("MultiplesOfPiByTwo")] Single slope)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay1.Angle = angle;
            bay2.Angle = angle;
            bay2.X = Convert.ToSingle(120f * Math.Cos(angle));
            bay2.Z = Convert.ToSingle(120f * Math.Sin(angle));
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.Slope = slope;
            AddLeftRightOverhang(shelf1, 10);
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf2.Slope = slope;
            AddLeftRightOverhang(shelf2, 10);
            var merchSpace1 = new PlanogramMerchandisingSpace(
                shelf1.GetPlanogramComponent().GetSubComponentPlacements().First(),
                plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(
                shelf2.GetPlanogramComponent().GetSubComponentPlacements().First(),
                plan.GetPlanogramSubComponentPlacements());

            var combinedMerchSpace = merchSpace1.AddToRight(merchSpace2);

            AssertHelper.AreRectValuesEqual(new RectValue(-10, 4, 0, 260, 196, 75), combinedMerchSpace.UnhinderedSpace);
        }

        private static void AddLeftRightOverhang(PlanogramFixtureComponent shelf1,Single overhang)
        {
            shelf1.GetPlanogramComponent().SubComponents.First().LeftOverhang = overhang;
            shelf1.GetPlanogramComponent().SubComponents.First().RightOverhang = overhang;
        }

        [Test]
        public void AddToRight_SumsWidth_WithRotation(
            [ValueSource("MultiplesOfPiByTwo")] Single angle, 
            [ValueSource("MultiplesOfPiByTwo")] Single slope)
        {
            var plan = "Test".CreatePackage().AddPlanogram();
            var bay1 = plan.AddFixtureItem();
            var bay2 = plan.AddFixtureItem();
            bay1.Angle = angle;
            bay2.Angle = angle;
            bay2.X = Convert.ToSingle(120f * Math.Cos(angle));
            bay2.Z = Convert.ToSingle(120f * Math.Sin(angle));
            var shelf1 = bay1.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf1.Slope = slope;
            var shelf2 = bay2.AddFixtureComponent(PlanogramComponentType.Shelf);
            shelf2.Slope = slope;
            var merchSpace1 = new PlanogramMerchandisingSpace(
                shelf1.GetPlanogramComponent().GetSubComponentPlacements().First(),
                plan.GetPlanogramSubComponentPlacements());
            var merchSpace2 = new PlanogramMerchandisingSpace(
                shelf2.GetPlanogramComponent().GetSubComponentPlacements().First(),
                plan.GetPlanogramSubComponentPlacements());

            var combinedMerchSpace = merchSpace1.AddToRight(merchSpace2);

            AssertHelper.AreRectValuesEqual(new RectValue(0, 4, 0, 240, 196, 75), combinedMerchSpace.UnhinderedSpace);
        }

        #endregion
    }
}
