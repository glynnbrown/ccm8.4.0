﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27439 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM802
// V8-28989 : A.Kuszyk
//  Moved into common namespace.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Moved to framework
#endregion
#region Version History : CCM830
// V8-32686 : A.Kuszyk
//  Updated calls to CalculateNormalisedPositions to use group scores.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Model;
using Galleria.Framework.Enums;
using NUnit.Framework;
using Galleria.Framework.Planograms.UnitTests.Helpers;
using Moq;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    public class PlanogramBlockingDividerSpaceCalculationTests : TestBase
    {
        #region Constructor
        [Test]
        public void Constructor_Throws_WhenDividersIsNull()
        {
            TestDelegate test = () => new BlockingDividerSpaceCalculation(null, 0f, 0f);

            Assert.Throws<ArgumentNullException>(test);
        }

        [Test]
        public void Constructor_Throws_WhenDividersIsEmpty()
        {
            TestDelegate test = () => new BlockingDividerSpaceCalculation(new List<PlanogramBlockingDivider>(), 0f, 0f);

            Assert.Throws<ArgumentException>(test);
        }
        #endregion

        #region CalculateNormalisedPositions
        [Test]
        public void CalculateNormalisedPositions_Throws_WhenMetricProfileIsNull()
        {
            var productPsis = new Dictionary<PlanogramProduct, Double>();
            var dividers = new[] { PlanogramBlockingDivider.NewPlanogramBlockingDivider() };
            var BlockingDividerSpaceCalculation = new BlockingDividerSpaceCalculation(dividers, 0f, 0f);

            TestDelegate test = () => BlockingDividerSpaceCalculation.CalculateNormalisedPositions(null, null);

            Assert.Throws<ArgumentNullException>(test);
        }

        [Test]
        public void CalculateNormalisedPositions_Throws_WhenProductPsisIsNull()
        {
            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            var dividers = new[] { PlanogramBlockingDivider.NewPlanogramBlockingDivider() };
            var BlockingDividerSpaceCalculation = new BlockingDividerSpaceCalculation(dividers, 0f, 0f);

            TestDelegate test = () => BlockingDividerSpaceCalculation.CalculateNormalisedPositions(metricProfile, null);

            Assert.Throws<ArgumentNullException>(test);
        }

        [Test]
        public void CalculateNormalisedPositions_ReturnsOrderedList()
        {
            //       ___________
            //      |           |
            //      |    (C)    |
            //      |___________|
            //      |           |
            //      |    (B)    |
            //      |___________|
            //      |           |
            //      |    (A)    |
            //      |___________|
            //

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Products
            var product1 = PlanogramProduct.NewPlanogramProduct();
            product1.Gtin = Guid.NewGuid().ToString();
            var product2 = PlanogramProduct.NewPlanogramProduct();
            product2.Gtin = Guid.NewGuid().ToString();
            var product3 = PlanogramProduct.NewPlanogramProduct();
            product3.Gtin = Guid.NewGuid().ToString();
            planogram.Products.AddRange(new[] { product1, product2, product3 });
            planogram.CreateAssortmentFromProducts();

            // Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product1.Id).P1 = 0.75f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product2.Id).P1 = 0.75f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product3.Id).P1 = 0.75f;

            // Groups
            var groupA = CreateBlockingGroup(planogram, product1, blocking);
            var groupB = CreateBlockingGroup(planogram, product2, blocking);
            var groupC = CreateBlockingGroup(planogram, product3, blocking);

            // Locations & divider
            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.25f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.6f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider1, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(null, null, divider2, divider1, blocking.Locations, groupB);
            var locationC = TestDataHelper.CreateBlockingLocation(null, null, null, divider2, blocking.Locations, groupC);

            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));
            var BlockingDividerSpaceCalculation = new BlockingDividerSpaceCalculation(blocking.Dividers, 0f, 1f);
            var scoresByGroup = planogram.Blocking[0].Groups.ToDictionary(
                g => g,
                g => g.EnumerateRangedPlanogramProducts().Sum(p => productPsis[p]));

            var results = BlockingDividerSpaceCalculation.CalculateNormalisedPositions(metricProfile, scoresByGroup);

            CollectionAssert.IsOrdered(results.Select(r => r.Divider.Y));
        }

        private static PlanogramBlockingGroup CreateBlockingGroup(Planogram planogram, PlanogramProduct product1, PlanogramBlocking blocking)
        {
            var groupA = PlanogramBlockingGroup.NewPlanogramBlockingGroup("A", BlockingHelper.GetNextBlockingGroupColour(blocking));
            var sequenceGroupA = PlanogramSequenceGroup.NewPlanogramSequenceGroup();
            sequenceGroupA.Colour = groupA.Colour;
            var sequenceGroupAProduct = PlanogramSequenceGroupProduct.NewPlanogramSequenceGroupProduct();
            sequenceGroupAProduct.Gtin = product1.Gtin;
            sequenceGroupA.Products.Add(sequenceGroupAProduct);
            planogram.Sequence.Groups.Add(sequenceGroupA);
            blocking.Groups.Add(groupA);
            return groupA;
        }

        [Test]
        public void CalculateNormalisedPositions_ReturnsExpectedDelta_ForSimpleHorizontalCase()
        {
            //       ___________
            //      |           |
            //      |    (B)    |
            //      |___________|
            //      |           |
            //      |    (A)    |
            //      |___________|
            //

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Products
            var product1 = PlanogramProduct.NewPlanogramProduct();
            product1.Gtin = Guid.NewGuid().ToString();
            var product2 = PlanogramProduct.NewPlanogramProduct();
            product2.Gtin = Guid.NewGuid().ToString();
            planogram.Products.AddRange(new[] { product1, product2 });
            planogram.CreateAssortmentFromProducts();

            // Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product1.Id).P1 = 0.75f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product2.Id).P1 = 0.25f;

            // Groups
            var groupA = CreateBlockingGroup(planogram, product1, blocking);
            var groupB = CreateBlockingGroup(planogram, product2, blocking);

            // Locations & divider
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(null, null, null, divider, blocking.Locations, groupB);

            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));
            var BlockingDividerSpaceCalculation = new BlockingDividerSpaceCalculation(blocking.Dividers, 0f, 1f);
            var scoresByGroup = planogram.Blocking[0].Groups.ToDictionary(
                g => g,
                g => g.EnumerateRangedPlanogramProducts().Sum(p => productPsis[p]));

            var results = BlockingDividerSpaceCalculation.CalculateNormalisedPositions(metricProfile, scoresByGroup);

            Assert.AreEqual(0.25f, results.First().Delta);
        }

        [Test]
        public void CalculateNormalisedPositions_ReturnsExpectedDelta_ForSimpleHorizontalCase_WhenPerformanceSumsMoreThanOne()
        {
            //       ___________
            //      |           |
            //      |    (B)    |
            //      |___________|
            //      |           |
            //      |    (A)    |
            //      |___________|
            //

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Products
            var product1 = PlanogramProduct.NewPlanogramProduct();
            product1.Gtin = Guid.NewGuid().ToString();
            var product2 = PlanogramProduct.NewPlanogramProduct();
            product2.Gtin = Guid.NewGuid().ToString();
            planogram.Products.AddRange(new[] { product1, product2 });
            planogram.CreateAssortmentFromProducts();

            // Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product1.Id).P1 = 0.75f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product2.Id).P1 = 0.75f;

            // Groups
            var groupA = CreateBlockingGroup(planogram, product1, blocking);
            var groupB = CreateBlockingGroup(planogram, product2, blocking);

            // Locations & divider
            var divider = TestDataHelper.CreateBlockingDivider(0f, 0.25f, 1f, PlanogramBlockingDividerType.Horizontal, 1, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, null, divider, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(null, null, null, divider, blocking.Locations, groupB);

            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));
            var BlockingDividerSpaceCalculation = new BlockingDividerSpaceCalculation(blocking.Dividers, 0f, 1f);
            var scoresByGroup = planogram.Blocking[0].Groups.ToDictionary(
                g => g,
                g => g.EnumerateRangedPlanogramProducts().Sum(p => productPsis[p]));

            var results = BlockingDividerSpaceCalculation.CalculateNormalisedPositions(metricProfile, scoresByGroup);

            Assert.AreEqual(0.25f, results.First().Delta);
        }

        [Test]
        public void CalculateNormalisedPositions_ReturnsExpectedDelta_ForSimpleVerticalCase()
        {
            //       _______________________
            //      |           |           |
            //      |           |           |
            //      |    (A)    |    (B)    |
            //      |           |           |
            //      |           |           |
            //      |___________|___________|
            //

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Products
            var product1 = PlanogramProduct.NewPlanogramProduct();
            product1.Gtin = Guid.NewGuid().ToString();
            var product2 = PlanogramProduct.NewPlanogramProduct();
            product2.Gtin = Guid.NewGuid().ToString();
            planogram.Products.AddRange(new[] { product1, product2 });
            planogram.CreateAssortmentFromProducts();

            // Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product1.Id).P1 = 0.75f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product2.Id).P1 = 0.25f;

            // Groups
            var groupA = CreateBlockingGroup(planogram, product1, blocking);
            var groupB = CreateBlockingGroup(planogram, product2, blocking);

            // Locations & divider
            var divider = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, divider, null, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(divider, null, null, null, blocking.Locations, groupB);

            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));
            var BlockingDividerSpaceCalculation = new BlockingDividerSpaceCalculation(blocking.Dividers, 0f, 1f);
            var scoresByGroup = planogram.Blocking[0].Groups.ToDictionary(
                g => g,
                g => g.EnumerateRangedPlanogramProducts().Sum(p => productPsis[p]));

            var results = BlockingDividerSpaceCalculation.CalculateNormalisedPositions(metricProfile, scoresByGroup);

            Assert.AreEqual(0.25f, results.First().Delta);
        }

        [Test]
        public void CalculateNormalisedPositions_ReturnsExpectedDelta_ForSimpleVerticalCase_WhenPerformanceSumsMoreThanOne()
        {
            //       _______________________
            //      |           |           |
            //      |           |           |
            //      |    (A)    |    (B)    |
            //      |           |           |
            //      |           |           |
            //      |___________|___________|
            //

            var metricProfile = new Mock<IPlanogramMetricProfile>().SetupAllProperties().Object;
            metricProfile.Metric1Ratio = 1F;

            // Planogram
            var planogram = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial);
            planogram.Blocking.Add(blocking);

            // Products
            var product1 = PlanogramProduct.NewPlanogramProduct();
            product1.Gtin = Guid.NewGuid().ToString();
            var product2 = PlanogramProduct.NewPlanogramProduct();
            product2.Gtin = Guid.NewGuid().ToString();
            planogram.Products.AddRange(new[] { product1, product2 });
            planogram.CreateAssortmentFromProducts();

            // Performance
            var metric1 = PlanogramPerformanceMetric.NewPlanogramPerformanceMetric(1);
            metric1.Direction = MetricDirectionType.MaximiseIncrease;
            planogram.Performance.Metrics.Add(metric1);
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product1.Id).P1 = 0.75f;
            planogram.Performance.PerformanceData.First(p => p.PlanogramProductId == product2.Id).P1 = 0.75f;

            // Groups
            var groupA = CreateBlockingGroup(planogram, product1, blocking);
            var groupB = CreateBlockingGroup(planogram, product2, blocking);

            // Locations & divider
            var divider = TestDataHelper.CreateBlockingDivider(0.25f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);
            var locationA = TestDataHelper.CreateBlockingLocation(null, divider, null, null, blocking.Locations, groupA);
            var locationB = TestDataHelper.CreateBlockingLocation(divider, null, null, null, blocking.Locations, groupB);

            var productPsis = PlanogramBlocking.GetNormalisedPsis(metricProfile, planogram.Products, new PlanogramPerformanceDataTotals(planogram.Performance.PerformanceData));
            var BlockingDividerSpaceCalculation = new BlockingDividerSpaceCalculation(blocking.Dividers, 0f, 1f);
            var scoresByGroup = planogram.Blocking[0].Groups.ToDictionary(
                g => g,
                g => g.EnumerateRangedPlanogramProducts().Sum(p => productPsis[p]));

            var results = BlockingDividerSpaceCalculation.CalculateNormalisedPositions(metricProfile, scoresByGroup);

            Assert.AreEqual(0.25f, results.First().Delta);
        }
        #endregion

        #region BlockingDividerSpaceCalculationItem

        [Test]
        public void GetDividersBelowOrAbove_ReturnsDividersAbove_InBand()
        {
            //   _______________________
            //  |____(2)____|           |
            //  |          (4)          |
            //  |____(1)____|____(3)____|
            //  |           |           |
            //  |___________|___________|

            var plan = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial); plan.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider4 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);

            var calculationItem = new BlockingDividerSpaceCalculation.BlockingDividerSpaceCalculationItem(divider1);

            var actual = calculationItem.GetDividersBelowOrAbove(locationsFromAbove: true);

            var expected = new[] { divider2 };
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GetDividersBelowOrAbove_ReturnsDividersBelow_InBand()
        {
            //   _______________________
            //  |____(2)____|           |
            //  |          (4)          |
            //  |____(1)____|____(3)____|
            //  |           |           |
            //  |___________|___________|

            var plan = Planogram.NewPlanogram();
            var blocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Initial); plan.Blocking.Add(blocking);

            var divider1 = TestDataHelper.CreateBlockingDivider(0f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider2 = TestDataHelper.CreateBlockingDivider(0f, 0.75f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider3 = TestDataHelper.CreateBlockingDivider(0.5f, 0.5f, 0.5f, PlanogramBlockingDividerType.Horizontal, 2, blocking.Dividers);
            var divider4 = TestDataHelper.CreateBlockingDivider(0.5f, 0f, 1f, PlanogramBlockingDividerType.Vertical, 1, blocking.Dividers);

            var calculationItem = new BlockingDividerSpaceCalculation.BlockingDividerSpaceCalculationItem(divider2);

            var actual = calculationItem.GetDividersBelowOrAbove(locationsFromAbove: false);

            var expected = new[] { divider1 };
            CollectionAssert.AreEquivalent(expected, actual);
        }

        #endregion
    }
}
