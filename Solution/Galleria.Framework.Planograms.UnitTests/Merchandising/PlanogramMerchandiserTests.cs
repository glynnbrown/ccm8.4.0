﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-18552 : A.Kuszyk
//  Created.
#endregion
#endregion

using FluentAssertions;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.UnitTests.Model;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Framework.Planograms.UnitTests.Merchandising
{
    [TestFixture]
    public class PlanogramMerchandiserTests
    {
        [Test]
        public void MerchandiseInventoryChange_DoesNotThrow_WhenDuplicateProductInSequenceGroup(
            [Values(PlanogramMerchandisingSpaceConstraintType.WithinBlockSpace, PlanogramMerchandisingSpaceConstraintType.WithinBlockPresentation, PlanogramMerchandisingSpaceConstraintType.BeyondBlockSpace)]
            PlanogramMerchandisingSpaceConstraintType spaceConstraint)
        {
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var prod1 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            var prod2 = plan.AddProduct(new WidthHeightDepthValue(10, 10, 10));
            plan.AddBlocking(new[] { new Tuple<Single, Single>(0, 0) });
            plan.Sequence.Groups[0].AddSequenceGroupProducts(plan.Products);
            plan.CreateAssortmentFromProducts();
            using (var mgs = plan.GetMerchandisingGroups())
            {
                var mg = mgs.First();
                var pos1 = mg.InsertPositionPlacement(prod1);
                var pos2 = mg.InsertPositionPlacement(prod2, pos1, PlanogramPositionAnchorDirection.ToRight);
                var pos3 = mg.InsertPositionPlacement(prod2, pos2, PlanogramPositionAnchorDirection.ToRight);
                mg.Process();
                mg.ApplyEdit();
                plan.UpdatePositionSequenceData();
                var contextMock = new Mock<IPlanogramMerchandiserContext>();
                contextMock.Setup(c => c.SpaceConstraint).Returns(spaceConstraint);
                contextMock.Setup(c => c.BlockingGroupsByColour).Returns(plan.Blocking[0].Groups.ToDictionary(g => g.Colour));
                contextMock.Setup(c => c.ProductStacksBySequenceColour).Returns(new Dictionary<int, Stack<PlanogramProduct>>()
                {
                    { plan.Blocking[0].Groups[0].Colour, new Stack<PlanogramProduct>(plan.Products) },
                });
                contextMock.Setup(c => c.TargetUnitsBySequenceColour).Returns(new Dictionary<int, Dictionary<string, int>>()
                {
                    {plan.Blocking[0].Groups[0].Colour, plan.Products.ToDictionary(p => p.Gtin, p => 2) },
                });
                contextMock.Setup(c => c.Blocking).Returns(plan.Blocking[0]);
                contextMock.Setup(c => c.MerchandisingGroups).Returns(mgs);
                contextMock.Setup(c => c.Planogram).Returns(plan);
                var positionsToIgnore = new List<String>();
                contextMock.Setup(c => c.PositionsToIgnore).Returns(positionsToIgnore);
                contextMock.Setup(c => c.RanksByProductGtin).Returns(plan.Assortment.Products.ToDictionary(p => p.Gtin, p => (Int32)p.Rank));
                var planMerchandiser = new PlanogramMerchandiser<IPlanogramMerchandiserContext>(contextMock.Object);

                Action merchandiseInventoryChange = () => planMerchandiser.MerchandiseInventoryChange(mgs.First().PositionPlacements.First());

                merchandiseInventoryChange.ShouldNotThrow<ArgumentException>("because planogram merchandiser should be able to deal with duplicate products");
            }
        }

        [Test]
        public void CleanPositionsToIgnore_ShouldRemovePositionsWithSpaceToMerchandise()
        {
            var contextMock = new Mock<IPlanogramMerchandiserContext>();
            var plan = "Package".CreatePackage().AddPlanogram();
            var bay = plan.AddFixtureItem();
            bay.AddFixtureComponent(PlanogramComponentType.Backboard);
            var shelf1 = bay.AddFixtureComponent(PlanogramComponentType.Shelf);
            var shelf2 = bay.AddFixtureComponent(PlanogramComponentType.Shelf, new PointValue(0, 100, 0));
            shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(60, 10, 10)));
            shelf1.AddPosition(plan.AddProduct(new WidthHeightDepthValue(60, 10, 10)));
            shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(40, 10, 10)));
            shelf2.AddPosition(plan.AddProduct(new WidthHeightDepthValue(40, 10, 10)));
            contextMock.Setup(p => p.Planogram).Returns(plan);
            contextMock.Setup(p => p.PositionsToIgnore).Returns(plan.Products.Select(p => p.Gtin).ToList());
            using (var mgs = plan.GetMerchandisingGroups())
            {
                contextMock.Setup(p => p.MerchandisingGroups).Returns(mgs);
                var planMerchandiser = new PlanogramMerchandiser<IPlanogramMerchandiserContext>(contextMock.Object);

                planMerchandiser.CleanPositionsToIgnore();

                contextMock.Object.PositionsToIgnore.Should().BeEquivalentTo(
                    plan.Products.Take(2).Select(p => p.Gtin),
                    "because the last two products have enough space on their shelf");
            }
        }
    }
}
