﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Ccm.Model;
using System.Globalization;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance
{
    /// <summary>
    /// Interaction logic for LocationMaintenanceOrganiser.xaml
    /// </summary>
    public partial class LocationMaintenanceOrganiser : ExtendedRibbonWindow
    {

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationMaintenanceViewModel), typeof(LocationMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationMaintenanceViewModel ViewModel
        {
            get { return (LocationMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationMaintenanceOrganiser senderControl = (LocationMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                LocationMaintenanceViewModel oldModel = (LocationMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationMaintenanceViewModel newModel = (LocationMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }



        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.LocationMaintenance);

            this.ViewModel = new LocationMaintenanceViewModel();


            this.Loaded += new RoutedEventHandler(LocationMaintenanceOrganiser_Loaded);
        }



        private void LocationMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(LocationMaintenanceOrganiser_Loaded);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }


        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        /// <summary>
        /// Brings up the google maps page with the relevant location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdShowLocation_Click(object sender, RoutedEventArgs e)
        {
            //Cast selected store as a store to be used
            Location st = this.ViewModel.SelectedLocation;

            if (st != null)
            {
                String urlString = "http://maps.google.com/maps?q=";

                //check if long & lat are available
                if (st.Latitude != null && st.Longitude != null)
                {
                    urlString = String.Format(CultureInfo.CurrentCulture, "{0}{1},{2}", urlString, st.Latitude, st.Longitude);
                }
                else
                {
                    //use the address
                    urlString = String.Format(CultureInfo.CurrentCulture, "{0}{1} {2} {3} {4} {5} {6}",
                        urlString, st.Address1, st.Address2, st.City, st.County,
                        st.PostalCode, st.Country);
                }


                System.Diagnostics.Process.Start(urlString);
            }

        }

        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// On the application being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }

            }

        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}
