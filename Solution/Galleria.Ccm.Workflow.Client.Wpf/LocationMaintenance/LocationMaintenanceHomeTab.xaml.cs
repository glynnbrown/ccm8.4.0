﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance
{
    /// <summary>
    /// Interaction logic for LocationMaintenanceHomeTab.xaml
    /// </summary>
    public partial class LocationMaintenanceHomeTab : RibbonTabItem
    {

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationMaintenanceViewModel), typeof(LocationMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationMaintenanceViewModel ViewModel
        {
            get { return (LocationMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationMaintenanceHomeTab senderControl = (LocationMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                LocationMaintenanceViewModel oldModel = (LocationMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationMaintenanceViewModel newModel = (LocationMaintenanceViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion

    }
}
