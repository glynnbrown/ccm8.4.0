﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion
#region Version History: GFS 2.2.0
// V8-32750 : N.Haywood
//  Added sort on search results
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using System.IO;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance
{
    /// <summary>
    /// Interaction logic for LocationMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class LocationMaintenanceBackstageOpen : UserControl
    {

        #region Fields
        private ObservableCollection<LocationInfo> _searchResults = new ObservableCollection<LocationInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationMaintenanceViewModel), typeof(LocationMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationMaintenanceViewModel ViewModel
        {
            get { return (LocationMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationMaintenanceBackstageOpen senderControl = (LocationMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                LocationMaintenanceViewModel oldModel = (LocationMaintenanceViewModel)e.OldValue;
                oldModel.AvailableLocations.BulkCollectionChanged -= senderControl.ViewModel_AvailableLocationsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                LocationMaintenanceViewModel newModel = (LocationMaintenanceViewModel)e.NewValue;
                newModel.AvailableLocations.BulkCollectionChanged += senderControl.ViewModel_AvailableLocationsBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(LocationMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationMaintenanceBackstageOpen senderControl = (LocationMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<LocationInfo>),
            typeof(LocationMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<LocationInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<LocationInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<LocationInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<LocationInfo> results = this.ViewModel.GetMatchingLocations(this.SearchText);

                foreach (LocationInfo locInfo in results.OrderBy(l => l.Code))
                {
                    _searchResults.Add(locInfo);
                }
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Updates the search results when the available locations list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableLocationsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                LocationInfo storeToOpen = (LocationInfo)senderControl.SelectedItem;

                if (storeToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                }
            }
        }

        #endregion

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = searchResultsListBox.SelectedItem as LocationInfo;
                if(selected != null)
                    this.ViewModel.OpenCommand.Execute(selected.Id);
            }
        }
    }
}
