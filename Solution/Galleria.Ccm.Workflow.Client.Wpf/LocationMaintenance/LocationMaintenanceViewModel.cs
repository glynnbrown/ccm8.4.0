﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion
#region Version History: (CCM 8.1.1)
// CCM:30325 : I.George
// Added friendly description message to SaveAs and Delete commands
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance
{
    /// <summary>
    /// Viewmodel controller for LocationMaintenanceOrganiser
    /// </summary>
    public sealed class LocationMaintenanceViewModel : ViewModelAttachedControlObject<LocationMaintenanceOrganiser>, IDataErrorInfo
    {
        #region Fields

        const String _exCategory = "LocationMaintenance"; //Category name for any exceptions raised to gibraltar from here

        private Boolean _userHasLocationCreatePerm;
        private Boolean _userHasLocationFetchPerm;
        private Boolean _userHasLocationEditPerm;
        private Boolean _userHasLocationDeletePerm;
        private Boolean _userHasLocationTypeCreatePerm;
        private Boolean _userHasLocationTypeDeletePerm;

        private readonly LocationInfoListViewModel _masterLocationInfoListView = new LocationInfoListViewModel();
        private Location _selectedLocation;

        private LocationGroupInfo _selectedLocationGroup;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedLocationProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.SelectedLocation);
        public static readonly PropertyPath SelectedLocationGroupProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.SelectedLocationGroup);
        public static readonly PropertyPath ManagerEmailProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.ManagerEmail);
        public static readonly PropertyPath RegionalManagerEmailProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.RegionalManagerEmail);
        public static readonly PropertyPath DivisionalManagerEmailProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.DivisionalManagerEmail);

        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(v => v.CloseCommand);
        public static PropertyPath SelectLocationGroupCommandProperty = WpfHelper.GetPropertyPath<LocationMaintenanceViewModel>(p => p.SelectLocationGroupCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of available locations 
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> AvailableLocations
        {
            get { return _masterLocationInfoListView.BindingView; }
        }

        /// <summary>
        /// Gets/Sets the selected location
        /// </summary>
        public Location SelectedLocation
        {
            get { return _selectedLocation; }
            private set
            {
                _selectedLocation = value;
                OnPropertyChanged(SelectedLocationProperty);

                OnSelectedLocationChanged(value);
            }
        }

        public String ManagerEmail
        {
            get { return _selectedLocation.ManagerEmail; }
            set
            {
                _selectedLocation.ManagerEmail = value;
                OnPropertyChanged(ManagerEmailProperty);
            }
        }

        public String RegionalManagerEmail
        {
            get { return _selectedLocation.RegionalManagerEmail; }
            set
            {
                _selectedLocation.RegionalManagerEmail = value;
                OnPropertyChanged(RegionalManagerEmailProperty);
            }
        }

        public String DivisionalManagerEmail
        {
            get { return _selectedLocation.DivisionalManagerEmail; }
            set
            {
                _selectedLocation.DivisionalManagerEmail = value;
                OnPropertyChanged(DivisionalManagerEmailProperty);
            }
        }

        /// <summary>
        /// Returns the selected LocationGroup
        /// </summary>
        public LocationGroupInfo SelectedLocationGroup
        {
            get { return _selectedLocationGroup; }
            set
            {
                _selectedLocationGroup = value;
                OnPropertyChanged(SelectedLocationGroupProperty);

                if (value != null)
                {
                    _selectedLocation.LocationGroupId = _selectedLocationGroup.Id;
                }
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            //location infos
            _masterLocationInfoListView.FetchAllForEntity();

            //load a new item
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Loads a new store
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasLocationCreatePerm)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //create a new item
                Location newLocation = Location.NewLocation(App.ViewState.EntityId);

                //re-initialize as property was changed.
                newLocation.MarkGraphAsInitialized();

                this.SelectedLocation = newLocation;

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int16?> _openCommand;
        /// <summary>
        /// Loads the requested store
        /// parameter = the id of the store to open
        /// </summary>
        public RelayCommand<Int16?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int16?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int16? locId)
        {
            //user must have get permission
            if (!_userHasLocationFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (locId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int16? locId)
        {
            //check if current item requires save first
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the requested item
                    this.SelectedLocation = Location.FetchById(locId.Value);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }


                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasLocationEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.SelectedLocation == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }
            if (this.SelectedLocationGroup == null)
            {
                this.SaveCommand.DisabledReason = Message.LocationMaintenance_NoLocationGroup;
                this.SaveAndNewCommand.DisabledReason = Message.LocationMaintenance_NoLocationGroup;
                this.SaveAndCloseCommand.DisabledReason = Message.LocationMaintenance_NoLocationGroup;
                return false;
            }


            //must be valid
            if (!this.SelectedLocation.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            if (this.AttachedControl != null)
            {
                //bindings must be valid
                if (!LocalHelper.AreBindingsValid(this.AttachedControl))
                {
                    this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    return false;
                }
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            Int32 locId = this.SelectedLocation.Id;

            //** check the item unique value
            String newCode;

            Boolean codeAccepted = true;
            if (this.AttachedControl != null)
            {


                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (LocationInfo locInfo in
                           LocationInfoList.FetchByEntityIdIncludingDeleted(App.ViewState.EntityId))
                       {
                           if (locInfo.Id != locId
                               && locInfo.Code.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };

                codeAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(String.Empty, String.Empty,
                    Message.LocationMaintenance_CodeAlreadyInUse, Location.CodeProperty.FriendlyName, 50,
                    /*forceFirstShow*/false, isUniqueCheck, this.SelectedLocation.Code, out newCode);
            }
            else
            {
                //force a unique value for unit testing
                newCode = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedLocation.Code,
                    this.AvailableLocations.Where(p => p.Id != locId).Select(p => p.Code));
            }

            //set the code
            if (codeAccepted)
            {
                if (this.SelectedLocation.Code != newCode)
                {
                    this.SelectedLocation.Code = newCode;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.SelectedLocation = this.SelectedLocation.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedLocation.Name);
                        if (itemReloadRequired)
                        {
                            this.SelectedLocation = Location.FetchById(this.SelectedLocation.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.SelectedLocation.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterLocationInfoListView.FetchAllForEntity();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_userHasLocationCreatePerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.SelectedLocation == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedLocation.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the code to save as
            String copyCode;

            Boolean codeAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (LocationInfo locInfo in
                           LocationInfoList.FetchByEntityIdLocationCodes(App.ViewState.EntityId, new List<String> { s }))
                       {
                           if (locInfo.Code.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };

                codeAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(Message.Generic_SaveAs, Message.Generic_SaveAs_CodeDescription,
                    Message.Generic_CodeIsNotUnique, Location.CodeProperty.FriendlyName, 50,
                    /*forceFirstShow*/true, isUniqueCheck, String.Empty, out copyCode);
            }
            else
            {
                //force a unique value for unit testing
                copyCode = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedLocation.Code, this.AvailableLocations.Select(p => p.Code));
            }

            if (codeAccepted)
            {
                base.ShowWaitCursor(true);

                Location itemCopy = this.SelectedLocation.Copy();
                itemCopy.Code = copyCode;
                this.SelectedLocation = itemCopy;

                base.ShowWaitCursor(false);

                SaveCurrentItem();
            }

        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_userHasLocationDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.SelectedLocation == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.SelectedLocation.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedLocation.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                Location itemToDelete = this.SelectedLocation;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //update the available items
                _masterLocationInfoListView.FetchAllForEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(),
                        p => Close_CanExecute(),
                        attachToCommandManager: false)
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SelectLocationGroupCommand

        private RelayCommand _selectLocationGroupCommand;

        /// <summary>
        /// Shows the select location group dialog
        /// </summary>
        public RelayCommand SelectLocationGroupCommand
        {
            get
            {
                if (_selectLocationGroupCommand == null)
                {
                    _selectLocationGroupCommand = new RelayCommand(
                        p => SelectLocationGroup_Executed(),
                        p => SelectLocationGroup_CanExecute())
                    {
                    };
                    base.ViewModelCommands.Add(_selectLocationGroupCommand);
                }
                return _selectLocationGroupCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SelectLocationGroup_CanExecute()
        {
            //user must have get permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(LocationGroup)))
            {
                this.SelectLocationGroupCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SelectLocationGroup_Executed()
        {
            if (this.AttachedControl != null)
            {
                LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(App.ViewState.EntityId);
                List<Int32> excludeIds = new List<Int32>();
                foreach (LocationGroup group in hierarchy.FetchAllGroups())
                {
                    if (group.ChildList.Count > 0)
                    {
                        excludeIds.Add(group.Id);
                    }
                }

                LocationGroupSelectionWindow dialog = new LocationGroupSelectionWindow(hierarchy, excludeIds, DataGridSelectionMode.Single);
                App.ShowWindow(dialog, this.AttachedControl, true);

                if (dialog.DialogResult == true)
                {
                    LocationGroup selectedGroup = dialog.SelectionResult.FirstOrDefault();
                    if (selectedGroup != null)
                    {
                        this.SelectedLocationGroup = LocationGroupInfo.NewLocationGroupInfo(selectedGroup);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected location.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedLocationChanged(Location newValue)
        {
            OnPropertyChanged(ManagerEmailProperty);
            OnPropertyChanged(RegionalManagerEmailProperty);
            OnPropertyChanged(DivisionalManagerEmailProperty);

            LocationGroupInfo newSelectedLocationGroup = null;

            if (newValue != null)
            {
                if (newValue.LocationGroupId != 0)
                {
                    newSelectedLocationGroup = LocationGroupInfo.FetchById(newValue.LocationGroupId);
                }
            }

            //update the selected location group.
            this.SelectedLocationGroup = newSelectedLocationGroup;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //location premissions
            //create
            _userHasLocationCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(Location));

            //fetch
            _userHasLocationFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(Location));

            //edit
            _userHasLocationEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(Location));

            //delete
            _userHasLocationDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(Location));
        }

        /// <summary>
        /// Returns a list of locs that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<LocationInfo> GetMatchingLocations(String codeOrNameCriteria)
        {
            String invariantCriteria = codeOrNameCriteria.ToLowerInvariant();

            return this.AvailableLocations.Where(
                p => p.Name.ToLowerInvariant().Contains(invariantCriteria) ||
                    p.Code.ToLowerInvariant().Contains(invariantCriteria));
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedLocation, this.SaveCommand);
            }
            return continueExecute;
        }

        #endregion

        #region IDataErrorInfo

        /// <summary>
        /// Check that the Email Address is valid or Not?
        /// </summary>
        /// <param name="emailAddress">Validate the email address string inputted by the user</param>
        /// <returns>True/False Dependant on the email address</returns>
        public bool IsEmailValid(String emailAddress)
        {
            bool isValid = true;

            if (emailAddress != null && emailAddress != "")
            {
                // Return true if strIn is in valid e-mail format.
                isValid = Regex.IsMatch(emailAddress,
                           @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                           @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
            }

            return isValid;
        }

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == ManagerEmailProperty.Path)
                {
                    if (!IsEmailValid(SelectedLocation.ManagerEmail))
                        result = String.Format(CultureInfo.CurrentCulture, Message.LocationMaintenance_EmailValidationError);
                }
                else if (columnName == RegionalManagerEmailProperty.Path)
                {
                    if (!IsEmailValid(SelectedLocation.RegionalManagerEmail))
                        result = String.Format(CultureInfo.CurrentCulture, Message.LocationMaintenance_EmailValidationError);
                }
                else if (columnName == DivisionalManagerEmailProperty.Path)
                {
                    if (!IsEmailValid(SelectedLocation.DivisionalManagerEmail))
                        result = String.Format(CultureInfo.CurrentCulture, Message.LocationMaintenance_EmailValidationError);
                }

                return result; // return result
            }
        }
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //save the locations type list so that any edits are committed

                    _selectedLocation = null;

                    this.AttachedControl = null;

                    _masterLocationInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
