﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27079 : L.Luong ~ Created.

#endregion

#endregion


using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstageMasterDataSetupTabContent.xaml
    /// </summary>
    public sealed partial class BackstageMasterDataSetupTabContent : UserControl
    {
        #region ViewModelProperty
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(BackstageMasterDataSetupTabContent));
       
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion


        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public BackstageMasterDataSetupTabContent()
        {
            //dont initialize until we are actually visible.
            this.IsVisibleChanged += BackstageMasterDataSetupTabContent_IsVisibleChanged;
        }

        /// <summary>
        /// Called when we are first made visible
        /// </summary>
        private void BackstageMasterDataSetupTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible)
            {
                //Unsubscribe
                this.IsVisibleChanged -= BackstageMasterDataSetupTabContent_IsVisibleChanged;

                //Initialize
                InitializeComponent();
                HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageMasterDataSetup);
            }
        }

    }
}
