﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from GFS
// V8-26564 : A.Probyn
//  Extended to create empty cells where some rows have some missing.
#endregion

#region Version History: (CCM 802)
//V8-26144 : M.Pettit ~ Imports do not correctly analyse tab-delimited files
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.ViewModel;

using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Imports;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Text;
using Galleria.Framework.Model;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public class ImportDataTextColumnsViewModel : ViewModelObject
    {

        #region Fields / Constants

        const Int32 _numOfPreviewLines = 25;

        private Boolean _isFixedWidth;
        private String _loadFilePath;
        private ObservableCollection<String> _rawPreviewData = new ObservableCollection<String>();
        private ReadOnlyObservableCollection<String> _rawPreviewDataRO;
        private ImportDefinitionTextDelimiterType _selectedDelimiter = ImportDefinitionTextDelimiterType.Comma;
        private ImportFileData _fileData;
        private DataGridColumnCollection _previewColumnSet = new DataGridColumnCollection();
        private ObservableCollection<ImportFixedLineDef> _fixedWidthLines = new ObservableCollection<ImportFixedLineDef>();

        List<Int32> _linePositionList = new List<int>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsFixedWidthProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.IsFixedWidth);
        public static readonly PropertyPath LoadFilePathProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.LoadFilePath);
        public static readonly PropertyPath RawPreviewDataProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.RawPreviewData);
        public static readonly PropertyPath SelectedDelimiterProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.SelectedDelimiter);
        public static readonly PropertyPath FileDataProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.FileData);
        public static readonly PropertyPath FixedWidthLinesProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.FixedWidthLines);
        public static readonly PropertyPath PreviewDataColumnSetProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.PreviewDataColumnSet);

        public static readonly PropertyPath FixedWidthLineValueListProperty = WpfHelper.GetPropertyPath<ImportDataTextColumnsViewModel>(p => p.FixedWidthLineValueList);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets if fixed width is selected.
        /// </summary>
        public Boolean IsFixedWidth
        {
            get { return _isFixedWidth; }
            set
            {
                _isFixedWidth = value;
                OnPropertyChanged(IsFixedWidthProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the file path to load from
        /// </summary>
        public String LoadFilePath
        {
            get { return _loadFilePath; }
            set
            {
                _loadFilePath = value;
                OnPropertyChanged(LoadFilePathProperty);
                OnLoadFilePathChanged();
            }
        }

        /// <summary>
        /// Returns the collection of raw data for review
        /// </summary>
        public ReadOnlyObservableCollection<String> RawPreviewData
        {
            get
            {
                if (_rawPreviewDataRO == null)
                {
                    _rawPreviewDataRO = new ReadOnlyObservableCollection<String>(_rawPreviewData);
                }
                return _rawPreviewDataRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected delimiter
        /// </summary>
        public ImportDefinitionTextDelimiterType SelectedDelimiter
        {
            get { return _selectedDelimiter; }
            set
            {
                _selectedDelimiter = value;
                OnPropertyChanged(SelectedDelimiterProperty);
                UpdateColumns();
            }
        }

        /// <summary>
        /// Returns column separated file data
        /// </summary>
        public ImportFileData FileData
        {
            get { return _fileData; }
            set
            {
                ImportFileData oldValue = _fileData;
                _fileData = value;
                OnPropertyChanged(FileDataProperty);
                OnFileDataChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the collection of columns used to preview the data
        /// </summary>
        public DataGridColumnCollection PreviewDataColumnSet
        {
            get { return _previewColumnSet; }
        }

        /// <summary>
        /// Returns the collection containing the defined fixed width lines
        /// </summary>
        public ObservableCollection<ImportFixedLineDef> FixedWidthLines
        {
            get { return _fixedWidthLines; }
        }

        #endregion

        #region Constructor

        public ImportDataTextColumnsViewModel(ImportFileData fileData)
        {
            this.FileData = fileData;
        }

        internal List<Int32> FixedWidthLineValueList
        {
            get
            {
                return _linePositionList;
            }
            set
            {
                _linePositionList = value;
                OnPropertyChanged(FixedWidthLineValueListProperty);
            }

        }

        #endregion

        #region Event Handlers

        private void OnFileDataChanged(ImportFileData oldValue, ImportFileData newValue)
        {
            if (oldValue != null)
            {
                oldValue.Columns.BulkCollectionChanged -= FileColumnSetDefinitions_BulkCollectionChanged;
                _previewColumnSet.Clear();
            }

            if (newValue != null)
            {
                newValue.Columns.BulkCollectionChanged += FileColumnSetDefinitions_BulkCollectionChanged;

                foreach (ImportFileDataColumn colDef in newValue.Columns)
                {
                    DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                                   colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                    BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                        new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                    _previewColumnSet.Add(col);

                }
            }
        }

        private void OnLoadFilePathChanged()
        {
            _rawPreviewData.Clear();

            //attempt to detect file encoding
            Encoding currentEncoding = FileHelper.DetectFileEncoding(this.LoadFilePath);

            //start reading the file line by line adding the data rows, using the current character encoding
            StreamReader reader = new StreamReader(this.LoadFilePath, currentEncoding);
            while (!reader.EndOfStream)
            {
                if (!reader.EndOfStream)
                {
                    _rawPreviewData.Add(reader.ReadLine());
                }
                else
                {
                    break;
                }
            }
            reader.Close();

            //try to assess what format the data is in
            GetColumnSeparationType(this.RawPreviewData);
        }

        private void FileColumnSetDefinitions_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ImportFileDataColumn colDef in e.ChangedItems)
                    {
                        DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                                colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                        _previewColumnSet.Add(col);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ImportFileDataColumn colDef in e.ChangedItems)
                    {
                        DataGridColumn col = _previewColumnSet.FirstOrDefault(c => (c.Header as String) == colDef.Header);
                        _previewColumnSet.Remove(col);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    _previewColumnSet.Clear();
                    foreach (ImportFileDataColumn colDef in this.FileData.Columns)
                    {
                        DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                               colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                        _previewColumnSet.Add(col);
                    }
                    break;
            }
        }

        private void OnFixedWidthLinesChanged()
        {
            _linePositionList.Clear();

            _linePositionList.Add(-1); //add start value of the very first column; will become 0 below
            foreach (ImportFixedLineDef line in this.FixedWidthLines.ToList())
            {
                _linePositionList.Add(line.LineValue);
            }

            _linePositionList.Sort();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the column separation of the data
        /// </summary>
        public void UpdateColumns()
        {
            //clear down the existing structure
            this.FileData.ClearAll();
            List<ImportFileDataRow> importRows = new List<ImportFileDataRow>();

            //get the delimiter if required
            Char[] delimiter = (!this.IsFixedWidth) ? GetDelimiterString(this.SelectedDelimiter).ToCharArray() : null;


            //cycle through the rows
            for (Int32 i = 0; i < this.RawPreviewData.Count; i++) //[GFS:17878] Int16 -> Int32 Fix
            {
                String row = this.RawPreviewData[i];

                if (String.IsNullOrEmpty(row))
                {
                    //skip this item if the row is null or empty.
                    continue;
                }

                //split the string into the defined columns
                List<String> splitLine = new List<String>();
                if (this.IsFixedWidth)
                {
                    Int32 lastPoint = 0;
                    foreach (ImportFixedLineDef lineDef in this.FixedWidthLines.OrderBy(l => l.LineValue))
                    {
                        //The start and end of the substring cannot be greater than the string length
                        Int32 startIndex = Math.Min(row.Length, lastPoint);
                        Int32 length = Math.Min(Math.Max(row.Length - lastPoint, 0), lineDef.LineValue - lastPoint);

                        String colString = row.Substring(startIndex, length);
                        splitLine.Add(colString);
                        lastPoint = lineDef.LineValue;
                    }
                }
                else
                {
                    splitLine.AddRange(row.Split(delimiter));
                }


                //create a new import data row
                ImportFileDataRow dataRow = ImportFileDataRow.NewImportFileDataRow(i + 1);

                Int32 colNumber = 1;
                foreach (String colString in splitLine)
                {
                    String cellValue = colString.TrimEnd(' ');

                    //check if we have a column, if not then add
                    if (this.FileData.Columns.Count < colNumber)
                    {
                        ImportFileDataColumn column = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                        column.Header = String.Format(Message.DataManagement_ImportColumnName, colNumber);
                        column.CellAlignment = ImportFileDataHorizontalAlignment.Left;

                        this.FileData.Columns.Add(column);
                    }

                    //add the cell data
                    dataRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(colNumber, cellValue));

                    colNumber++;
                }

                //V826564 : Check enough cells have been created
                if (dataRow.Cells.Count < this.FileData.Columns.Count)
                {
                    Int32 numberBlankCellsRequired = (this.FileData.Columns.Count - dataRow.Cells.Count);
                    for (int j = 0; j < numberBlankCellsRequired; j++)
                    {
                        //Add blank cells
                        dataRow.Cells.Add(ImportFileDataCell.NewImportFileDataCell(colNumber, String.Empty));

                        //continue col number
                        colNumber++;
                    }
                }

                importRows.Add(dataRow);
            }
            this.FileData.Rows.AddRange(importRows);
        }

        /// <summary>
        /// Gets the delimiter string for the given value
        /// </summary>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        private String GetDelimiterString(ImportDefinitionTextDelimiterType delimiter)
        {
            switch (delimiter)
            {
                case ImportDefinitionTextDelimiterType.Comma: return ",";
                case ImportDefinitionTextDelimiterType.Pipe: return "|";
                case ImportDefinitionTextDelimiterType.Semicolon: return ";";
                case ImportDefinitionTextDelimiterType.Tab: return "\t";
                default: return ",";
            }
        }

        /// <summary>
        /// Translates user defined line positions into column start end values
        /// </summary>
        internal void MapFixedColumnWidthPositions()
        {
            _linePositionList.Clear();
            OnFixedWidthLinesChanged();

            foreach (ImportMapping mapping in this.FileData.MappingList)
            {
                mapping.FixedWidthColumnEnd = null;
                mapping.FixedWidthColumnStart = null;

                //use column number and lines to create column start and end values
                if (!String.IsNullOrEmpty(mapping.PropertyNotes))
                {
                    try
                    {
                        Int32 columnNumber = Int32.Parse(mapping.PropertyNotes);
                        if (columnNumber > 0)
                        {
                            mapping.FixedWidthColumnEnd = FixedWidthLineValueList[columnNumber];
                            mapping.FixedWidthColumnStart = FixedWidthLineValueList[columnNumber - 1] + 1;
                        }
                    }
                    catch
                    {
                        //parse failed; cannot autopopulate start / end
                    }
                }
            }
        }


        /// <summary>
        /// Makes an attempt to guess if the file is fixed width or not
        /// </summary>
        private void GetColumnSeparationType(IEnumerable<String> dataLines)
        {
            //get the first 2 lines
            String[] dataLinesCut = dataLines.Take(2).ToArray();
            Int32 numLines = dataLinesCut.Length;

            //check through each delimiter to see if it repeats in each line
            Char delimiter;
            Int32 line1Count = 0;
            Int32 line2Count = 0;

            //check through the delimiters
            foreach (ImportDefinitionTextDelimiterType type in Enum.GetValues(typeof(ImportDefinitionTextDelimiterType)))
            {
                //dont check for other or tab
                if (type != ImportDefinitionTextDelimiterType.Tab)
                {
                    delimiter = GetDelimiterString(type)[0];
                    line1Count = dataLinesCut[0].Count(c => c == delimiter);
                    if (numLines > 1)
                    {
                        //We have 2 lines, so check the number of delimiters are equal
                        line2Count = dataLinesCut[1].Count(c => c == delimiter);
                        if ((line1Count == line2Count) && (line1Count != 0))
                        {
                            this.IsFixedWidth = false;
                            this.SelectedDelimiter = type;
                            return;
                        }
                    }
                    else
                    {
                        //we have only one line, so make a best guess
                        if (line1Count > 1)
                        {
                            //this delimiter character exists more than once in this line, so assume it is valid
                            this.IsFixedWidth = false;
                            this.SelectedDelimiter = type;
                            return;
                        }
                    }
                }
            }

            //if we still havent returned out then mark the file as fixed width
            this.IsFixedWidth = true;

        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    public class ImportFixedLineDef
    {
        #region Fields

        private Int32 _lineValue;

        #endregion

        #region Properties

        public Int32 LineValue
        {
            get { return _lineValue; }
            set { _lineValue = value; }
        }

        #endregion

        #region Constructor

        public ImportFixedLineDef(Int32 lineValue)
        {
            _lineValue = lineValue;
        }

        #endregion
    }
}
