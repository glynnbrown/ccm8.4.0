﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Imports.Mappings;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Collections;
using Galleria.Ccm.Model;
using System.Windows.Input;
using Csla;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationSpaceProvider : IViewDataProvider
    {
        #region Fields
        private BulkObservableCollection<ViewDataLocationSpaceProviderRowEntry> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationSpaceProviderRowEntry>();
        private DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private Boolean _isLoadingData;
        private Object _locationId = 0;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to use
        /// </summary>
        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get
            {
                return _columnSet;
            }
        }

        public Object SelectedLocationId
        {
            get { return _locationId; }
            set
            {
                _locationId = value;
                OnPropertyChanged(IViewDataProviderHelper.SelectedLocationIdProperty.Path);

                OnSelectedLocationIdChanged(value);
            }
        }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewDataLocationSpaceProvider()
        {
            CreateColumns();
        }

        #endregion

        #region Event Handlers

        private void OnSelectedLocationIdChanged(Object newValue)
        {
            //reload data.
            BeginDataLoad((Int16)newValue);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the location import mapping list
            LocationSpaceImportMappingList mappingList =
                LocationSpaceImportMappingList.NewLocationSpaceImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(
                mappingList,
                ViewDataLocationSpaceProviderRowEntry.OriginalLocationSpaceProductGroupProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationSpaceImportMappingList.LocationCodeMappingId, ViewDataLocationSpaceProviderRowEntry.LocationCodeProperty.Path},
                    {LocationSpaceImportMappingList.ProductGroupCodeMappingId, ViewDataLocationSpaceProviderRowEntry.ProductGroupCodeProperty.Path}
                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }
        }

        public void EditItem(Object selectedItem, Window sourcecontrol)
        {
            ViewDataLocationSpaceProviderRowEntry selectedLocationSpaceBayItem = (ViewDataLocationSpaceProviderRowEntry)selectedItem;
            LocationSpaceMaintenance.LocationSpaceMaintenanceOrganiser spaceOrganiser = new LocationSpaceMaintenance.LocationSpaceMaintenanceOrganiser();
            spaceOrganiser.ViewModel.OpenCommand.Execute(selectedLocationSpaceBayItem.OriginalLocationSpaceInfo.Id);
            App.ShowWindow(spaceOrganiser, sourcecontrol,/*isModel*/true);
        }

        public void DeleteItem(Object selectedItem)
        {
            ViewDataLocationSpaceProviderRowEntry dataRow = (ViewDataLocationSpaceProviderRowEntry)selectedItem;
            LocationSpaceProductGroup selectedLocationSpaceProductGroup = dataRow.OriginalLocationSpaceProductGroup;
            Int32 bayId = selectedLocationSpaceProductGroup.Id;
            Int32 parentGroupId = selectedLocationSpaceProductGroup.ProductGroupId;
            Int32 parentSpaceId = dataRow.OriginalLocationSpaceInfo.Id;

            if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(selectedLocationSpaceProductGroup.ToString()))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                LocationSpace parentLocationSpace = LocationSpace.GetLocationSpaceById(parentSpaceId);

                parentLocationSpace.LocationSpaceProductGroups.Remove(selectedLocationSpaceProductGroup);

                //commit the delete
                try
                {
                    parentLocationSpace.Save();
                }
                catch (DataPortalException ex)
                {
                    Mouse.OverrideCursor = null;

                    LocalHelper.RecordException(ex, "DataManagement");
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(selectedLocationSpaceProductGroup.ToString(), OperationType.Delete);

                    Mouse.OverrideCursor = Cursors.Wait;
                }

                //remove the item from the data source
                _dataSourceEntries.Remove(dataRow);


                Mouse.OverrideCursor = null;

            }
        }

        private void BeginDataLoad(Int16 locationId)
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(new Object[] { App.ViewState.EntityId, locationId });
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Object[] args = (Object[])e.Argument;
            Int32 entityId = (Int32)args[0];
            Int16 locationId = (Int16)args[1];

            List<ViewDataLocationSpaceProviderRowEntry> rows = new List<ViewDataLocationSpaceProviderRowEntry>();

            LocationSpaceInfoList locationSpaceInfoList = LocationSpaceInfoList.FetchByEntityId(App.ViewState.EntityId);

            //create lookups
            Dictionary<Int32, String> productGroupLookup =
                ProductHierarchy.FetchByEntityId(App.ViewState.EntityId).EnumerateAllGroups().ToDictionary(g => g.Id, g => g.Code);

            foreach (LocationSpaceInfo info in locationSpaceInfoList.Where(p => p.LocationId == locationId))
            {
                LocationSpace locationSpace = LocationSpace.GetLocationSpaceById(info.Id);
                foreach (LocationSpaceProductGroup productGroup in locationSpace.LocationSpaceProductGroups)
                {
                    if (productGroupLookup.ContainsKey(productGroup.ProductGroupId))
                    {
                        String groupCode = String.Empty;
                        productGroupLookup.TryGetValue(productGroup.ProductGroupId, out groupCode);
                        rows.Add(new ViewDataLocationSpaceProviderRowEntry(info, groupCode, productGroup));
                    }
                }
            }

            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationSpaceProviderRowEntry> rows = e.Result as List<ViewDataLocationSpaceProviderRowEntry>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);
            }

            this.IsLoadingData = false;
        }

        #endregion

        #region IViewDataProvider Members

        Boolean IViewDataProvider.IsDeleteSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }

    public sealed class ViewDataLocationSpaceProviderRowEntry
    {
        #region Fields
        private LocationSpaceInfo _originalLocationSpaceInfo;
        private String _productGroupCode;
        private LocationSpaceProductGroup _originalLocationSpaceProductGroup;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath OriginalLocationSpaceProductGroupProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceProviderRowEntry>(p => p.OriginalLocationSpaceProductGroup);
        public static readonly PropertyPath LocationCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceProviderRowEntry>(p => p.LocationCode);
        public static readonly PropertyPath ProductGroupCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceProviderRowEntry>(p => p.ProductGroupCode);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the original location space bay
        /// </summary>
        public LocationSpaceProductGroup OriginalLocationSpaceProductGroup
        {
            get { return _originalLocationSpaceProductGroup; }
        }

        /// <summary>
        /// Returns the original location space info
        /// </summary>
        public LocationSpaceInfo OriginalLocationSpaceInfo
        {
            get { return _originalLocationSpaceInfo; }
        }

        /// <summary>
        /// Returns the location code
        /// </summary>
        public String LocationCode
        {
            get { return _originalLocationSpaceInfo.LocationCode; }
        }

        /// <summary>
        /// Returns the product group code
        /// </summary>
        public String ProductGroupCode
        {
            get { return _productGroupCode; }
        }




        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="constraintObject"></param>
        public ViewDataLocationSpaceProviderRowEntry(LocationSpaceInfo locationSpaceInfo, String productGroupCode, LocationSpaceProductGroup originalLocationSpaceProductGroup)
        {
            _originalLocationSpaceInfo = locationSpaceInfo;
            _productGroupCode = productGroupCode;
            _originalLocationSpaceProductGroup = originalLocationSpaceProductGroup;
        }


        #endregion
    }
}
