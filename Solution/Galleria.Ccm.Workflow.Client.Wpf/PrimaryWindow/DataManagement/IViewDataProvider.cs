﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;

using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Defines the properties/ methods that must be made available
    /// to the ViewDataViewModel
    /// </summary>
    public interface IViewDataProvider : INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        /// Returns the actual data
        /// </summary>
        IList DataSource { get; }

        /// <summary>
        /// Returns true if data is currently being loaded.
        /// </summary>
        Boolean IsLoadingData { get; }

        /// <summary>
        /// Returns the columnset to use to display the data
        /// </summary>
        DataGridColumnCollection ColumnSet { get; }


        /// <summary>
        /// Returns true if the performance source selector should be visible
        /// </summary>
        Boolean IsPerformanceSourceSelectorVisible { get; }

        /// <summary>
        /// Gets/Sets the selected performance source id
        /// </summary>
        Object SelectedPerformanceSourceId { get; set; }

        /// <summary>
        /// Returns true if the location selector should be visible
        /// </summary>
        Boolean IsLocationSelectorVisible { get; }

        /// <summary>
        /// Gets/Sets the selected location id
        /// </summary>
        Object SelectedLocationId { get; set; }

        /// <summary>
        /// Returns true if the category selector should be visible
        /// </summary>
        Boolean IsCategorySelectorVisible { get; }

        /// <summary>
        /// Gets/Sets the selected category id
        /// </summary>
        Object SelectedCategoryId { get; set; }

        /// <summary>
        /// Returns true if object delete is supported.
        /// </summary>
        Boolean IsDeleteSupported { get;  }

        /// <summary>
        /// Returns true if object edit is supported.
        /// </summary>
        Boolean IsEditSupported { get;  }


        #endregion

        #region Methods

        /// <summary>
        /// Edit the selected item
        /// </summary>
        /// <param name="selectedItem"></param>
        void EditItem(Object selectedItem, Window sendercontrol);

        /// <summary>
        /// Delete the selected item
        /// </summary>
        void DeleteItem(Object selectedItem);

        #endregion
    }

    public static class IViewDataProviderHelper
    {
        public static readonly PropertyPath DataSourceProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.DataSource);
        public static readonly PropertyPath IsLoadingDataProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.IsLoadingData);
        public static readonly PropertyPath ColumnSetProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.ColumnSet);
        public static readonly PropertyPath IsPerformanceSourceSelectorVisibleProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.IsPerformanceSourceSelectorVisible);
        public static readonly PropertyPath SelectedPerformanceSourceIdProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.SelectedPerformanceSourceId);
        public static readonly PropertyPath IsLocationSelectorVisibleProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.IsLocationSelectorVisible);
        public static readonly PropertyPath SelectedLocationIdProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.SelectedLocationId);
        public static readonly PropertyPath IsCategorySelectorVisibleProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.IsCategorySelectorVisible);
        public static readonly PropertyPath SelectedCategoryIdProperty = WpfHelper.GetPropertyPath<IViewDataProvider>(p => p.SelectedCategoryId);

    }
}
