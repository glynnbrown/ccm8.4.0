﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.ViewModel;

using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Model;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public class ImportDataColumnDefinitionViewModel : ViewModelAttachedControlObject<ImportDataColumnDefinition>
    {
        #region Fields

        private Boolean _suppressMappingChangedHandler;
        private ImportFileData _fileData;

        private Int32 _importStartRowNumber = 1;
        private Boolean _isFirstRowColumnHeaders;
        private ObservableCollection<String> _availableWorksheets = new ObservableCollection<String>();
        private ReadOnlyObservableCollection<String> _availableWorksheetsRO;
        private String _selectedWorksheet;

        private DataGridColumnCollection _previewColumnSet = new DataGridColumnCollection();
        private ImportFileDataRow _headerRow;

        private ObservableCollection<String> _availableColumns = new ObservableCollection<String>();
        private ReadOnlyObservableCollection<String> _availableColumnsRO;

        private ImportDataSelectFileViewModel _selectedFileViewModel;
        private ObservableCollection<ImportMapping> _selectMappingFromGrid = new ObservableCollection<ImportMapping>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ImportStartRowNumberProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.ImportStartRowNumber);
        public static readonly PropertyPath IsFirstRowColumnHeadersProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.IsFirstRowColumnHeaders);
        public static readonly PropertyPath AvailableWorksheetsProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.AvailableWorksheets);
        public static readonly PropertyPath SelectedWorksheetProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.SelectedWorksheet);
        public static readonly PropertyPath FileDataProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.FileData);
        public static readonly PropertyPath AreMappingsAllValidProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.AreMappingsAllValid);
        public static readonly PropertyPath AreMappingsAllUniqueProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.AreMappingsAllUnique);

        public static readonly PropertyPath ClearSelectedMappingsCommandProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.ClearSelectedMappingsCommand);
        public static readonly PropertyPath ClearAllMappingsCommandProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.ClearAllMappingsCommand);
        public static readonly PropertyPath AutoMapCommandProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.AutoMapCommand);


        public static readonly PropertyPath SelectedMappingFromGridProperty = WpfHelper.GetPropertyPath<ImportDataColumnDefinitionViewModel>(p => p.SelectedMappingFromGrid);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the row number to start the import from
        /// </summary>
        public Int32 ImportStartRowNumber
        {
            get { return _importStartRowNumber; }
            set
            {
                _importStartRowNumber = value;
                OnPropertyChanged(ImportStartRowNumberProperty);
            }
        }

        /// <summary>
        /// Gets/Sets if the first import row contains column headers
        /// </summary>
        public Boolean IsFirstRowColumnHeaders
        {
            get { return _isFirstRowColumnHeaders; }
            set
            {
                if (OnIsFirstRowColumnHeadersChanging())
                {
                    _isFirstRowColumnHeaders = value;
                    OnPropertyChanged(IsFirstRowColumnHeadersProperty);
                    OnIsFirstRowColumnHeadersChanged();
                }
            }
        }

        /// <summary>
        /// Returns the collection of available excel worksheets
        /// </summary>
        public ReadOnlyObservableCollection<String> AvailableWorksheets
        {
            get
            {
                if (_availableWorksheetsRO == null)
                {
                    _availableWorksheetsRO = new ReadOnlyObservableCollection<String>(_availableWorksheets);
                }
                return _availableWorksheetsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected excel worksheet
        /// </summary>
        public String SelectedWorksheet
        {
            get { return _selectedWorksheet; }
            set
            {
                _selectedWorksheet = value;
                OnPropertyChanged(SelectedWorksheetProperty);
            }
        }

        /// <summary>
        /// Returns the file data
        /// </summary>
        public ImportFileData FileData
        {
            get { return _fileData; }
            set
            {
                ImportFileData oldValue = _fileData;
                _fileData = value;
                OnPropertyChanged(FileDataProperty);
                OnFileDataChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns true if all columns are mapped correctly
        /// </summary>
        public Boolean AreMappingsAllValid
        {
            get
            {
                if (this.FileData != null)
                {
                    return
                        (_fileData.MappingList.Count > 0) && // SA-14461
                        (_fileData.MappingList
                        .Where(m => m.PropertyIsMandatory)
                        .All(m => !String.IsNullOrEmpty(m.ColumnReference)));

                }
                else return false;
            }
        }

        /// <summary>
        /// Returns true if all columns are uniqely mapped
        /// </summary>
        public Boolean AreMappingsAllUnique
        {
            get
            {
                if (this.FileData != null)
                {
                    return
                        (_fileData.MappingList.Count > 0) &&
                        (_fileData.MappingList
                        .Where(m => m.IsDuplicateMapping)
                        .All(m => false));
                }
                else return false;
            }
        }

        /// <summary>
        /// Returns the collection of columns used to preview the data
        /// </summary>
        public DataGridColumnCollection PreviewDataColumnSet
        {
            get { return _previewColumnSet; }
        }

        /// <summary>
        /// Returns the collection containing columns available for mapping
        /// </summary>
        public ReadOnlyObservableCollection<String> AvailableColumns
        {
            get
            {
                if (_availableColumnsRO == null)
                {
                    _availableColumnsRO = new ReadOnlyObservableCollection<String>(_availableColumns);
                }
                return _availableColumnsRO;
            }
        }

        /// <summary>
        /// Returns the selected file view model
        /// </summary>
        public ImportDataSelectFileViewModel SelectedFileViewModel
        {
            get { return _selectedFileViewModel; }
        }



        public ObservableCollection<ImportMapping> SelectedMappingFromGrid
        {
            get { return _selectMappingFromGrid; }
            set
            {
                _selectMappingFromGrid = value;
                OnPropertyChanged(SelectedMappingFromGridProperty);
            }
        }

        #endregion

        #region Constructor

        public ImportDataColumnDefinitionViewModel(ImportFileData fileData, ImportDataSelectFileViewModel selectedFileViewModel)
        {
            _selectedFileViewModel = selectedFileViewModel;
            this.FileData = fileData;
        }

        #endregion

        #region Commands

        #region ClearSelectedMappingsCommand

        private RelayCommand<ImportMapping> _clearSelectedMappingsCommand;

        /// <summary>
        /// Clears all the selected column mappings
        /// </summary>
        public RelayCommand<ImportMapping> ClearSelectedMappingsCommand
        {
            get
            {
                if (_clearSelectedMappingsCommand == null)
                {
                    _clearSelectedMappingsCommand = new RelayCommand<ImportMapping>
                        (
                        p => ClearSelectedMappings_Executed(),
                        p => ClearSelectedMappings_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_ColumnDefinition_ClearSelectedMapping,
                        FriendlyDescription = Message.DataManagement_ColumnDefinition_ClearSelectedMapping_Description
                    };
                    return _clearSelectedMappingsCommand;
                }
                return _clearSelectedMappingsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ClearSelectedMappings_CanExecute()
        {
            foreach (ImportMapping mapping in _selectMappingFromGrid)
            {
                if (!String.IsNullOrEmpty(mapping.ColumnReference))
                {
                    this.ClearSelectedMappingsCommand.DisabledReason = String.Empty;
                    return true;
                }
            }
            return false;
        }

        private void ClearSelectedMappings_Executed()
        {
            foreach (ImportMapping mapping in _selectMappingFromGrid)
            {
                mapping.ColumnReference = ImportMapping.EmptyColumnReference;
            }
        }

        #endregion

        #region ClearSelectedMappingCommand

        private RelayCommand<ImportMapping> _clearSelectedMappingCommand;

        /// <summary>
        /// Clears all the selected column Mapping
        /// </summary>
        public RelayCommand<ImportMapping> ClearSelectedMappingCommand
        {
            get
            {
                if (_clearSelectedMappingCommand == null)
                {
                    _clearSelectedMappingCommand = new RelayCommand<ImportMapping>(
                        p => ClearSelectedMappingCommand_Executed(p),
                        p => ClearSelectedMappingCommand_CanExecute(p))
                    {
                        FriendlyName = Message.DataManagement_ColumnDefinition_ClearSelectedMapping
                    };
                    base.ViewModelCommands.Add(_clearSelectedMappingCommand);
                }
                return _clearSelectedMappingCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ClearSelectedMappingCommand_CanExecute(ImportMapping mapping)
        {
            if (mapping == null)
            {
                return false;
            }
            return true;
        }

        private void ClearSelectedMappingCommand_Executed(ImportMapping mapping)
        {
            mapping.ColumnReference = ImportMapping.EmptyColumnReference;
        }

        #endregion

        #region ClearAllMappingsCommand

        private RelayCommand _clearAllMappingsCommand;

        /// <summary>
        /// Clears all the column mappings
        /// </summary>
        public RelayCommand ClearAllMappingsCommand
        {
            get
            {
                if (_clearAllMappingsCommand == null)
                {
                    _clearAllMappingsCommand = new RelayCommand(
                        p => ClearAllMappings_Executed(),
                        p => ClearAllMappings_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_ColumnDefinition_ClearAllMappings,
                        FriendlyDescription = Message.DataManagement_ColumnDefinition_ClearAllMappings_Description
                    };
                }
                return _clearAllMappingsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ClearAllMappings_CanExecute()
        {
            if (this.FileData != null)
            {
                foreach (ImportMapping mapping in this.FileData.MappingList)
                {
                    if (!String.IsNullOrEmpty(mapping.ColumnReference))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void ClearAllMappings_Executed()
        {
            _suppressMappingChangedHandler = true;

            //Clear all column mapping source column references
            foreach (ImportMapping item in this.FileData.MappingList)
            {
                item.ColumnReference = ImportMapping.EmptyColumnReference;
            }

            _suppressMappingChangedHandler = false;
            OnPropertyChanged(AreMappingsAllValidProperty);
        }

        #endregion

        #region AutoMapCommand

        private RelayCommand _autoMapCommand;

        /// <summary>
        /// Trys to auto map columns
        /// </summary>
        public RelayCommand AutoMapCommand
        {
            get
            {
                if (_autoMapCommand == null)
                {
                    _autoMapCommand = new RelayCommand(
                        p => AutoMap_Executed(),
                        p => AutoMap_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_ColumnDefinition_AutoMap,
                        FriendlyDescription = Message.DataManagement_ColumnDefinition_AutoMap_Description
                    };
                }
                return _autoMapCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AutoMap_CanExecute()
        {
            //data should not be null
            if (this.FileData == null)
            {
                return false;

            }

            //data should have columns.
            if (this.FileData.Columns.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AutoMap_Executed()
        {
            _suppressMappingChangedHandler = true;


            //if the first row is headers try to use these to do the match
            if (this.IsFirstRowColumnHeaders)
            {
                foreach (ImportMapping item in this.FileData.MappingList)
                {
                    String itemColName = item.PropertyName.Replace(" ", "").ToLowerInvariant();

                    //try to find a matching header
                    ImportFileDataColumn col =
                        this.FileData.Columns.FirstOrDefault(c => c.Header.Replace(" ", "").ToLowerInvariant() == itemColName);
                    if (col != null)
                    {
                        item.ColumnReference = col.Header;
                    }
                }
            }
            else
            {
                Int32 availableColumnsCount = this.FileData.Columns.Count;
                Int32 currentColIndex = 1;

                foreach (ImportMapping item in this.FileData.MappingList)
                {
                    //Check index value is valid
                    if (currentColIndex <= availableColumnsCount)
                    {
                        //Update column reference to match available column
                        item.ColumnReference = this.FileData.Columns[currentColIndex - 1].Header;
                    }
                    //Increase index number
                    currentColIndex += 1;
                }
            }


            _suppressMappingChangedHandler = false;
            OnPropertyChanged(AreMappingsAllValidProperty);
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of the loaded file data
        /// </summary>
        private void OnFileDataChanged(ImportFileData oldValue, ImportFileData newValue)
        {
            if (oldValue != null)
            {
                oldValue.Columns.BulkCollectionChanged -= FileColumnSetDefinitions_BulkCollectionChanged;
                oldValue.MappingList.ChildColumnReferenceChanged -= FileData_ColumnReferenceMappingChanged;

                _previewColumnSet.Clear();
                _availableColumns.Clear();
                //[SA-14587]Clear old header row
                _headerRow = null;
            }

            if (newValue != null)
            {
                newValue.Columns.BulkCollectionChanged += FileColumnSetDefinitions_BulkCollectionChanged;
                newValue.MappingList.ChildColumnReferenceChanged += FileData_ColumnReferenceMappingChanged;

                //[SA-14587]Ensure row columns headers is set to false
                this.IsFirstRowColumnHeaders = false;

                //add in row number col
                DataGridColumn rowNumCol = ExtendedDataGrid.CreateReadOnlyTextColumn(
                       Message.DataManagement_RowNumberColHeader, ImportFileDataRow.RowNumberProperty.Name, HorizontalAlignment.Center);
                _previewColumnSet.Add(rowNumCol);

                //add in all the existing columns
                foreach (ImportFileDataColumn colDef in _fileData.Columns)
                {
                    DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                                   colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                    BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                        new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                    _previewColumnSet.Add(col);
                }

                //automap if we suspect this can be done
                GuessMapping();


                //if available columns isn't empty the columns were already added in the GuessMapping stage
                if (_availableColumns.Count == 0)
                {
                    //update available columns + add an empty ref
                    _availableColumns.Add(ImportMapping.EmptyColumnReference);
                    foreach (ImportFileDataColumn col in _fileData.Columns)
                    {
                        _availableColumns.Add(col.Header);
                    }
                }
            }

            //only update this if the source file changed - may have just changed worksheets
            if ((oldValue != null && newValue != null) ? (oldValue.SourceFilePath != newValue.SourceFilePath) : true)
            {
                UpdateAvailableWorksheets();
            }

        }

        /// <summary>
        /// Responds to changes in the column definitions set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileColumnSetDefinitions_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ImportFileDataColumn colDef in e.ChangedItems)
                    {
                        DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                               colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                        _previewColumnSet.Add(col);
                        _availableColumns.Add(colDef.Header);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ImportFileDataColumn colDef in e.ChangedItems)
                    {
                        DataGridColumn col = _previewColumnSet.FirstOrDefault(c => (c.Header as String) == colDef.Header);
                        _previewColumnSet.Remove(col);
                        _availableColumns.Remove(colDef.Header);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _previewColumnSet.Clear();

                    //add the row number column back in 
                    DataGridColumn rowNumCol = ExtendedDataGrid.CreateReadOnlyTextColumn(
                       Message.DataManagement_RowNumberColHeader, ImportFileDataRow.RowNumberProperty.Name, HorizontalAlignment.Center);
                    _previewColumnSet.Add(rowNumCol);

                    foreach (ImportFileDataColumn colDef in (IEnumerable)sender)
                    {
                        DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                               colDef.GetBindingPath(), FileHelper.ConvertAlignment(colDef.CellAlignment));
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                        _previewColumnSet.Add(col);
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to an attemped change to the value of IsFirstRowColumnHeaders
        /// </summary>
        /// <returns> true if the change should continue</returns>
        private Boolean OnIsFirstRowColumnHeadersChanging()
        {
            Boolean mappingsAreDefined = false;
            Boolean continueUpdate = true;

            //Check if any column mappings have been set
            foreach (ImportMapping item in this.FileData.MappingList)
            {
                if (!String.IsNullOrEmpty(item.ColumnReference))
                {
                    //user has set some mappings so confirm delete
                    mappingsAreDefined = true;
                    break;
                }
            }

            if (mappingsAreDefined)
            {

                //[TODO] Check if automap warning is suppressed in config
                //Galleria.Midi.Model.Config sessionConfig = this.ConfigViewModel.Model;
                //    Boolean hideAutomapWarnings = true;//sessionConfig.SuppressImportAutomapWarnings;
                //    if (!hideAutomapWarnings)
                //    {

                //        //Ask user if they are sure they wish to over write any current mapping
                //        ModalMessage msg =
                //        new ModalMessage(String.Format(CultureInfo.CurrentUICulture, Message.Ribbon_DataManagementLoseMappingsConfirm))
                //        {
                //            IsOkButtonVisible = true,
                //            IsCancelButtonVisible = true,
                //            MessageHeader = Message.Ribbon_DataManagementLoseMappingsConfirmHeader,
                //            ModalMessageDialogType = ModalMessageDialogType.Warning,
                //            IsCheckBoxVisible = true,
                //            IsCheckBoxChecked = false,
                //            CheckBoxText = Message.Ribbon_DataManagementLoseMappingsSuppressWarning
                //        };

                //        if (this.AttachedControl != null)
                //        {
                //            msg.Owner = this.AttachedControl.FindVisualAncestor<ExtendedRibbonWindow>();
                //            msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                //        }

                //        continueUpdate = (bool)msg.ShowDialog();
                //        //Check if user changed whether to show/hide in future
                //        Boolean newHideAutomapWarnings = msg.CheckBoxChecked;
                //        if (hideAutomapWarnings != newHideAutomapWarnings)
                //        {
                //            sessionConfig.SuppressImportAutomapWarnings = newHideAutomapWarnings;
                //        }
                //    }
            }

            return continueUpdate;

        }

        /// <summary>
        /// Responds to the change of the IsFirstRowColumnHeaders
        /// </summary>
        /// <returns></returns>
        private void OnIsFirstRowColumnHeadersChanged()
        {
            //Reset all column mapping prior to update
            this.ClearAllMappingsCommand.Execute();

            _suppressMappingChangedHandler = true;

            //Clear available columns
            _availableColumns.Clear();
            //Add an empty column reference
            _availableColumns.Add(ImportMapping.EmptyColumnReference);

            if (this.FileData.Rows.Count > 0)
            {
                if (this.IsFirstRowColumnHeaders)
                {
                    //Get first row containing the headers
                    _headerRow = this.FileData.Rows[0];//.Clone();

                    //Remove from the main dataset
                    this.FileData.Rows.RemoveAt(0);

                    //Manually Update Column headers 
                    foreach (ImportFileDataColumn colDef in this.FileData.Columns)
                    {
                        colDef.Header = _headerRow[colDef.ColumnNumber].Value.ToString();
                        //Add header to available columns while already looping
                        _availableColumns.Add(colDef.Header);
                    }

                }
                else
                {
                    //Manually Update Column headers 
                    int columnIndex = 0;
                    foreach (ImportFileDataColumn colDef in this.FileData.Columns)
                    {
                        colDef.Header = String.Format(CultureInfo.CurrentCulture, "Column {0}", (columnIndex + 1));
                        //Add header to available columns while already looping
                        _availableColumns.Add(colDef.Header);
                        columnIndex++;
                    }

                    //Add first row back in
                    if (_headerRow != null)//[SA-14587]
                    {
                        this.FileData.Rows.Insert(0, _headerRow);
                        _headerRow = null;
                    }
                }
            }

            _suppressMappingChangedHandler = false;
            OnPropertyChanged(AreMappingsAllValidProperty);

        }


        /// <summary>
        /// File Data Column Reference changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileData_ColumnReferenceMappingChanged(object sender, EventArgs e)
        {
            if (!_suppressMappingChangedHandler)
            {
                OnPropertyChanged(AreMappingsAllValidProperty);
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Perfoms a quick check on the data to see if the first row is headers
        /// if so then sets the check and automaps
        /// </summary>
        private void GuessMapping()
        {
            if (this.FileData.Rows.Count > 0)
            {

                //check if the first row is possibly headers
                ImportFileDataRow row1 = this.FileData.Rows[0];
                Int32 colsCount = row1.Cells.Count;
                Int32 matchCount = 0;

                foreach (ImportFileDataCell cell in row1.Cells)
                {
                    //remove all spaces & convert to lowercase
                    String cellValue = cell.Value.ToString().Replace(" ", "").ToLowerInvariant();

                    ImportMapping matchedMapping =
                        this.FileData.MappingList.FirstOrDefault(m => m.PropertyName.Replace(" ", "").ToLowerInvariant() == cellValue);

                    if (matchedMapping != null)
                    {
                        matchCount++;
                    }
                }


                //if over half match then automap
                if (matchCount > (colsCount / 2))
                {
                    this.IsFirstRowColumnHeaders = true;
                    this.AutoMapCommand.Execute();
                }
            }
        }

        /// <summary>
        /// Returns the index for the selected worksheet
        /// </summary>
        /// <returns></returns>
        public Int32 GetSelectedWorksheetIndex()
        {
            Int32 index = 0;

            if (!String.IsNullOrEmpty(this.SelectedWorksheet))
            {
                if (this.AvailableWorksheets.Contains(this.SelectedWorksheet))
                {
                    index = this.AvailableWorksheets.IndexOf(this.SelectedWorksheet);
                }
            }

            return index;
        }

        public void UpdateAvailableWorksheets()
        {
            //populate available worksheets
            _availableWorksheets.Clear();

            if (this.FileData != null)
            {
                String fileExt = Path.GetExtension(_fileData.SourceFilePath);
                if (fileExt == ".xlsx" || fileExt == ".xls")
                {
                    foreach (String str in FileHelper.LoadSelectedFileWorksheets(_fileData.SourceFilePath))
                    {
                        _availableWorksheets.Add(str);
                    }
                }
            }

            this.SelectedWorksheet = _availableWorksheets.FirstOrDefault();

        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _fileData.Columns.BulkCollectionChanged -= FileColumnSetDefinitions_BulkCollectionChanged;
                    _fileData.ColumnReferenceMappingChanged -= FileData_ColumnReferenceMappingChanged;
                    _fileData = null;
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }
}
