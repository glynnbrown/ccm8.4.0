﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationHierarchyProvider : IViewDataProvider, IDisposable
    {
        #region Fields
        private Boolean _isLoadingData;
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private readonly BulkObservableCollection<ViewDataLocationHierarchyProviderRowEntry> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationHierarchyProviderRowEntry>();
        #endregion

        #region Properties

        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        public Boolean IsDeleteSupported { get; private set; }

        public Boolean IsEditSupported { get; private set; }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        public ViewDataLocationHierarchyProvider()
        {
            this.IsEditSupported = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.EditObject, typeof(LocationGroup));
            this.IsDeleteSupported = false; //force use of maintenance screen.

            //nb - columns must be loaded after data for this.
            BeginDataLoad();

        }
        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            if (_dataSourceEntries.Count > 0)
            {
                LocationHierarchy hierarchy = _dataSourceEntries[0].LocationGroup.ParentHierarchy;

                foreach (LocationLevel level in hierarchy.EnumerateAllLevels())
                {
                    String searchKey = level.Name;

                    if (!level.IsRoot)
                    {
                        //create the code column
                        DataGridTextColumn levelCodeCol = new DataGridTextColumn();
                        levelCodeCol.IsReadOnly = false;

                        //bind the header to the level name + Code
                        MultiBinding headerBinding = new MultiBinding();
                        headerBinding.Bindings.Add(new Binding() { Source = "{0} {1}" });
                        headerBinding.Bindings.Add(new Binding(LocationLevel.NameProperty.Name) { Source = level });
                        headerBinding.Bindings.Add(new Binding() { Source = LocationGroup.CodeProperty.FriendlyName });
                        headerBinding.Converter = App.Current.Resources[Galleria.Framework.Controls.Wpf.ResourceKeys.ConverterStringFormat] as IMultiValueConverter;
                        BindingOperations.SetBinding(levelCodeCol, DataGridColumn.HeaderProperty, headerBinding);

                        //constuct the data binding path
                        //nb source collection is flattenedUnits
                        String bindingPath = String.Format("LevelPathValues[{0}].Code", searchKey);
                        levelCodeCol.Binding = new Binding(bindingPath) { Mode = BindingMode.TwoWay };
                        //add the column to the set
                        _columnSet.Add(levelCodeCol);

                        //create the name column for the level
                        DataGridTextColumn levelNameCol = new DataGridTextColumn();
                        levelNameCol.IsReadOnly = false;
                        //bind the header to the level name
                        MultiBinding headerNameBinding = new MultiBinding();
                        headerNameBinding.Bindings.Add(new Binding() { Source = "{0} {1}" });
                        headerNameBinding.Bindings.Add(new Binding(LocationLevel.NameProperty.Name) { Source = level });
                        headerNameBinding.Bindings.Add(new Binding() { Source = LocationGroup.NameProperty.FriendlyName });
                        headerNameBinding.Converter = App.Current.Resources[Galleria.Framework.Controls.Wpf.ResourceKeys.ConverterStringFormat] as IMultiValueConverter;
                        BindingOperations.SetBinding(levelNameCol, DataGridColumn.HeaderProperty, headerNameBinding);
                        //constuct the data binding path
                        //nb source collection is flattenedUnits
                        String nameBindingPath = String.Format("LevelPathValues[{0}].Name", searchKey);
                        levelNameCol.Binding = new Binding(nameBindingPath) { Mode = BindingMode.TwoWay };
                        //add the column to the set
                        _columnSet.Add(levelNameCol);
                    }
                }

            }

            OnPropertyChanged(IViewDataProviderHelper.ColumnSetProperty.Path);
        }

        /// <summary>
        /// Starts loading data aysnc
        /// </summary>
        private void BeginDataLoad()
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(App.ViewState.EntityId);
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 entityId = (Int32)e.Argument;

            List<ViewDataLocationHierarchyProviderRowEntry> rows = new List<ViewDataLocationHierarchyProviderRowEntry>();

            LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(entityId);
            if (hierarchy != null)
            {
                foreach (LocationGroup group in hierarchy.EnumerateAllGroups())
                {
                    if (!group.IsRoot)
                    {
                        rows.Add(new ViewDataLocationHierarchyProviderRowEntry(group));
                    }
                }

            }

            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationHierarchyProviderRowEntry> rows = e.Result as List<ViewDataLocationHierarchyProviderRowEntry>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);
            }

            //re-create columns as level structure may have changed.
            CreateColumns();

            this.IsLoadingData = false;
        }

        /// <summary>
        /// Shows the edit window for the selected item.
        /// </summary>
        /// <param name="selectedItem"></param>
        public void EditItem(Object selectedItem, Window sourcecontrol)
        {
            ViewDataLocationHierarchyProviderRowEntry locationGroupView = selectedItem as ViewDataLocationHierarchyProviderRowEntry;
            if (locationGroupView != null)
            {
                LocationHierarchyOrganiser locationHierarchyWindow = new LocationHierarchyOrganiser();

                locationHierarchyWindow.Closed += EditWindow_Closed;

                App.ShowWindow(locationHierarchyWindow, sourcecontrol, false);


                locationHierarchyWindow.ViewModel.SetSelectedGroup(locationGroupView.LocationGroup);
                locationHierarchyWindow.ViewModel.EditUnitCommand.Execute();
            }
        }
        private void EditWindow_Closed(object sender, EventArgs e)
        {
            Window win = (Window)sender;
            win.Closed -= EditWindow_Closed;

            //refresh data in case of changes.
            BeginDataLoad();
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataLocationHierarchyProviderRowEntry
        {
            #region Fields
            private LocationGroup _productGroup;
            private Dictionary<String, LocationGroup> _levelPathValues;
            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath LocationGroupProperty = WpfHelper.GetPropertyPath<ViewDataLocationHierarchyProviderRowEntry>(p => p.LocationGroup);
            public static readonly PropertyPath LevelPathValuesProperty = WpfHelper.GetPropertyPath<ViewDataLocationHierarchyProviderRowEntry>(p => p.LevelPathValues);

            #endregion

            #region Properties

            /// <summary>
            /// Returns the the source product group for this view
            /// </summary>
            public LocationGroup LocationGroup
            {
                get { return _productGroup; }
            }

            /// <summary>
            /// Returns the dictionary collection of level path values
            /// </summary>
            public Dictionary<String, LocationGroup> LevelPathValues
            {
                get
                {
                    if (_levelPathValues == null)
                    {
                        _levelPathValues = ConstructLevelPath();
                    }
                    return _levelPathValues;
                }
            }

            #endregion

            #region Constructor
            public ViewDataLocationHierarchyProviderRowEntry(LocationGroup group)
            {
                _productGroup = group;
            }
            #endregion

            #region Methods

            /// <summary>
            /// Constructs the level path dictionary for the source group
            /// </summary>
            /// <returns></returns>
            private Dictionary<String, LocationGroup> ConstructLevelPath()
            {
                Dictionary<String, LocationGroup> levelPathDict = new Dictionary<String, LocationGroup>();

                LocationGroup group = _productGroup;
                LocationHierarchy structure = group.ParentHierarchy;

                List<LocationGroup> pathPartList = group.GetParentPath();
                pathPartList.Add(group);


                LocationLevel[] levelList = structure.EnumerateAllLevels().ToArray();

                for (Int32 i = 0; i < levelList.Count(); i++)
                {
                    if (pathPartList.Count > i)
                    {
                        levelPathDict.Add(levelList[i].Name, pathPartList[i]);
                    }
                    else
                    {
                        levelPathDict.Add(levelList[i].Name, null);
                    }
                }

                return levelPathDict;
            }

            #endregion

        }

        #endregion

        #region IViewDataProvider Members

        void IViewDataProvider.DeleteItem(object selectedItem)
        {
            throw new NotSupportedException();
        }

        public bool IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get {return 0;}
            set {}
        }

        public bool IsLocationSelectorVisible
        {
            get { return false; }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public bool IsCategorySelectorVisible
        {
            get { return false; }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region IPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _dataSourceEntries.Clear();
                    //_locationHierarchyView.ModelChanged -= LocationHierarchyView_ModelChanged;
                    //_locationHierarchyView = null;
                }
                _isDisposed = true;
            }
        }
        #endregion



       
    }
}
