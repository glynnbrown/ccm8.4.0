﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Copied from SA.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Windows.Media;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Imports;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    
    /// <summary>
    /// Backstage Data Management Data Item
    /// </summary>
    public class BackstageDataManagementDataItem : ViewModelObject
    {
        #region Fields

        private String _name;
        private String _description;
        private ImageSource _icon = ImageResources.DataManagement_DataImage;
        private ImageSource _stateIcon;
        private Boolean _isSelected = false;
        private CCMDataItemType _dataType;
        private Boolean _isEnabled = true;
        private Boolean _hasErrors = false;

        #endregion

        #region Property Paths

        public static PropertyPath NameProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.Name);
        public static PropertyPath DescriptionProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.Description);
        public static PropertyPath DataTypeProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.DataType);
        public static PropertyPath IsSelectedProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.IsSelected);
        public static PropertyPath IconProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.Icon);
        public static PropertyPath StateIconProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.StateIcon);
        public static PropertyPath IsEnabledProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.IsEnabled);
        public static PropertyPath HasErrorsProperty = WpfHelper.GetPropertyPath<BackstageDataManagementDataItem>(e => e.HasErrors);

        #endregion

        #region Properties

        public String Name
        {
            get { return _name; }
        }

        public String Description
        {
            get { return _description; }
        }

        public ImageSource Icon
        {
            get { return _icon; }
        }

        public ImageSource StateIcon
        {
            get { return _stateIcon; }
            set
            {
                _stateIcon = value;
                OnPropertyChanged(StateIconProperty);
            }
        }

        public CCMDataItemType DataType
        {
            get { return _dataType; }
        }

        /// <summary>
        /// Gets/Sets whether the data item is selected
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged(IsSelectedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether data item is enabled
        /// </summary>
        public Boolean IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                OnPropertyChanged(IsEnabledProperty);
            }
        }

        public Boolean HasErrors
        {
            get { return _hasErrors; }
            set
            {
                _hasErrors = value;
                OnPropertyChanged(HasErrorsProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="entity"></param>
        public BackstageDataManagementDataItem(String name, String description, CCMDataItemType dataType, ImageSource stateIcon)
        {
            _name = name;
            _description = description;
            _dataType = dataType;
            _stateIcon = stateIcon;
        }

        #endregion

        #region Dispose

        /// <summary>
        /// Handles an IDisposable dispose call
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {

                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }
}
