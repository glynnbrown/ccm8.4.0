﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Controls;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Controls.Wpf;
using System.Globalization;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using System.Collections;
using Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataProductProvider : IViewDataProvider
    {
        #region Fields

        private const Int32 _maxReturnedRecords = 1000;
        private Int32 _currentFirstRecord;
        private Int32 _currentLastRecord;
        private List<Int32> _idList = new List<Int32>();
        private Boolean _previousEnabled = false;
        private Boolean _nextEnabled = false;

        private ProductList _productList;
        private DataGridColumnCollection _columnSet;
        private BulkObservableCollection<Product> _dataSourceEntries = new BulkObservableCollection<Product>();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to use
        /// </summary>
        public IList DataSource
        {
            get { return _productList; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Returns a friendly string describing the current page
        /// </summary>
        public String ProductSetPosition
        {
            get
            {
                return String.Format(CultureInfo.CurrentCulture,
                    Message.DataManagement_ViewData_ProductsSetPosition, _currentFirstRecord + 1, _currentLastRecord + 1, _idList.Count);
            }
        }

        /// <summary>
        /// Returns true if the next button should be enabled
        /// </summary>
        public Boolean NextEnabled
        {
            get { return _nextEnabled; }
        }

        /// <summary>
        /// Returns true if the previous button should be enabled
        /// </summary>
        public Boolean PreviousEnabled
        {
            get { return _previousEnabled; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewDataProductProvider()
        {
            _columnSet = new DataGridColumnCollection();

            #region Set Product List

            _idList = ProductInfoList.FetchByEntityId(App.ViewState.EntityId).Select(p => p.Id).ToList();

            //Use info list to check if over max returned record limit, then retrive only max amout of records
            //using entityid/product ids fetch
            List<Int32> productIds = new List<Int32>();

            //check if more than _maxReturnedRecords records exist
            if (_idList.Count > _maxReturnedRecords)
            {
                //set productGTINs
                for (Int16 i = 0; i < _maxReturnedRecords; i++)
                {
                    productIds.Add(_idList[i]);
                }

                //Populate product list
                _productList = ProductList.FetchByProductIds(productIds);

                //Set last record number
                _currentFirstRecord = 0;
                _currentLastRecord = _maxReturnedRecords - 1;

                _previousEnabled = false;
                _nextEnabled = true;
            }
            else
            {
                //Fetch all as less than max records exist
                _productList = ProductList.FetchByEntityId(App.ViewState.EntityId);

                //Set last record number
                _currentFirstRecord = 0;
                _currentLastRecord = _idList.Count - 1;
            }

            productIds = null;

            #endregion

            //create the product rows
            _dataSourceEntries.AddRange(_productList);

            //create the columns
            CreateColumns();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the product import mapping list
            //ProductAttributeList attributeList = ProductAttributeList.FetchByEntityId(App.ViewState.EntityId);

            ProductImportMappingList mappingList =
                ProductImportMappingList.NewProductImportMappingList(false);


            List<DataGridColumn> columns = DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList);
            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }

        }

        public void EditItem(Object selectedItem, Window sourcecontrol)
        {
            Product selectedProduct = (Product)selectedItem;
            ProductMaintenanceOrganiser productOrganiser = new ProductMaintenanceOrganiser();
            productOrganiser.ViewModel.OpenCommand.Execute(selectedProduct.Id);
            App.ShowWindow(productOrganiser, sourcecontrol,/*isModel*/true);
        }

        public void DeleteItem(Object selectedItem)
        {
            Product selectedProduct = (Product)selectedItem;

            if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(selectedProduct.ToString()))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                //mark the item as deleted so item changed does not show.
                Product itemToDelete = Product.FetchById(selectedProduct.Id);
                itemToDelete.Delete();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    Mouse.OverrideCursor = null;

                    LocalHelper.RecordException(ex, "DataManagement");
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    Mouse.OverrideCursor = Cursors.Wait;
                }

                //remove the item from the data source
                _dataSourceEntries.Remove(selectedProduct);


                Mouse.OverrideCursor = null;
            }
        }

        public void NextDataSourcePage()
        {
            //check if the end of the list will be reached
            Int32 firstRecord = _currentLastRecord + 1;
            Int32 lastRecord = firstRecord + _maxReturnedRecords;

            if (lastRecord > _idList.Count)
            {
                lastRecord = _idList.Count;
            }

            List<Int32> productIds = new List<Int32>();
            for (int i = firstRecord; i < lastRecord; i++)
            {
                productIds.Add(_idList[i]);
            }
            _productList = ProductList.FetchByProductIds(productIds);


            //populate the data source
            _dataSourceEntries.Clear();
            _dataSourceEntries.AddRange(_productList);


            _previousEnabled = firstRecord == 0 ? false : true;
            _nextEnabled = lastRecord < _idList.Count ? true : false;

            _currentFirstRecord = firstRecord;
            _currentLastRecord = lastRecord - 1;

        }

        public void PreviousDataSourcePage()
        {
            Int32 lastRecord = _currentFirstRecord;
            Int32 firstRecord = lastRecord - _maxReturnedRecords;

            if (firstRecord < 0)
            {
                firstRecord = 0;
                lastRecord = firstRecord + _maxReturnedRecords;
            }

            List<Int32> productIds = new List<Int32>();
            for (int i = firstRecord; i < lastRecord; i++)
            {
                productIds.Add(_idList[i]);
            }
            _productList = ProductList.FetchByProductIds(productIds);

            //populate the data source
            _dataSourceEntries.Clear();
            _dataSourceEntries.AddRange(_productList);

            //move counters
            _previousEnabled = firstRecord == 0 ? false : true;
            _nextEnabled = lastRecord < _idList.Count ? true : false;

            _currentFirstRecord = firstRecord;
            _currentLastRecord = lastRecord - 1;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IViewDataProvider members

        Boolean IViewDataProvider.IsDeleteSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLoadingData
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

    }
}
