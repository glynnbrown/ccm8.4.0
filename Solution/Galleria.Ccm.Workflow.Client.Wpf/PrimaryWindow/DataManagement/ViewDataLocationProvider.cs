﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25444 : N.Haywood
//  Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using System.ComponentModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using System.Collections;
using Galleria.Ccm.Imports.Mappings;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Input;
using Csla.Server;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationProvider : IViewDataProvider
    {
        #region Fields
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private readonly BulkObservableCollection<ViewDataLocationRow> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationRow>();
        private Boolean _isLoadingData;
        #endregion

        #region Properties

        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display 
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        public ViewDataLocationProvider()
        {
            CreateColumns();
            BeginDataLoad();
        }

        #endregion

        #region Methods

        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the location import mapping list
            LocationImportMappingList mappingList =
                LocationImportMappingList.NewLocationImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList,
                ViewDataLocationRow.LocationProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationImportMappingList.GroupCodeMapId, ViewDataLocationRow.LocationGroupCodeProperty.Path},
                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }
        }

        public void EditItem(Object selectedItem, Window sendercontrol)
        {
            Location selectedLocation = ((ViewDataLocationRow)selectedItem).Location;
            LocationMaintenance.LocationMaintenanceOrganiser locationOrganiser = new LocationMaintenance.LocationMaintenanceOrganiser();
            locationOrganiser.ViewModel.OpenCommand.Execute(selectedLocation.Id);
            App.ShowWindow(locationOrganiser, sendercontrol,/*isModel*/true);
        }

        public void DeleteItem(Object selectedItem)
        {
            ViewDataLocationRow dataRow = (ViewDataLocationRow)selectedItem;
            Location selectedLocation = dataRow.Location;

            if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(selectedLocation.ToString()))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                //mark the item as deleted so item changed does not show.
                Location itemToDelete = Location.FetchById(selectedLocation.Id);
                itemToDelete.Delete();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    Mouse.OverrideCursor = null;

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    Mouse.OverrideCursor = Cursors.Wait;
                }

                //remove the item from the data source
                _dataSourceEntries.Remove(dataRow);


                Mouse.OverrideCursor = null;

            }
        }

        private void BeginDataLoad()
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(App.ViewState.EntityId);
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 entityId = (Int32)e.Argument;

            List<ViewDataLocationRow> rows = new List<ViewDataLocationRow>();

            LocationList list = LocationList.FetchByEntityId(entityId);
            if (list != null && list.Count > 0)
            {
                //Populate group ids dictionary for fast lookup of group codes
                Dictionary<Int32, String> groupIds = new Dictionary<Int32, String>();
                foreach (LocationGroup group in LocationHierarchy.FetchByEntityId(entityId).FetchAllGroups())
                {
                    groupIds.Add(group.Id, group.Code);
                }

                //Create view data rows with correct group code
                foreach (Location location in list)
                {
                    String code = String.Empty;
                    groupIds.TryGetValue(location.LocationGroupId, out code);

                    rows.Add(new ViewDataLocationRow(location, code));
                }

            }

            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationRow> rows = e.Result as List<ViewDataLocationRow>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);
            }

            this.IsLoadingData = false;
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataLocationRow
        {
            #region Fields

            private Location _location;
            private String _locationGroupCode;

            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<ViewDataLocationRow>(c => c.Location);
            public static readonly PropertyPath LocationGroupCodeProperty = WpfHelper.GetPropertyPath<ViewDataLocationRow>(c => c.LocationGroupCode);

            #endregion

            #region Properties

            public Location Location
            {
                get { return _location; }
            }

            public String LocationGroupCode
            {
                get { return _locationGroupCode; }
                set { _locationGroupCode = value; }
            }

            #endregion

            #region Constructor

            public ViewDataLocationRow(Location location, String groupCode)
            {
                _location = location;
                _locationGroupCode = groupCode;
            }

            #endregion
        }

        #endregion

        #region INotifyPropertyChnaged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IViewDataProvider members

        Boolean IViewDataProvider.IsDeleteSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }
        
        #endregion

    }
}
