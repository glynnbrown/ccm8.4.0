﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// A generic interface to allow custom parameters to be gathered during import process.
    /// ViewModels created that follow this interface can be plugged striaght into the 
    /// ImportDataSelectFile window.
    /// </summary>
    public interface IImportDataSelectFileProvider : INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        /// Returns true if an object has been selected and is valid
        /// </summary>
        Boolean CanProceed { get; }

        /// <summary>
        /// contains the item that has been selected and will be returned
        /// to the parent window
        /// </summary>
        Object ReturnObject { get; }

        #endregion
    }
}
