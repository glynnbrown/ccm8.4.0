﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDefinitionTextColumnsDelimited.xaml
    /// </summary>
    public partial class ImportDataTextColumnsDelimited : UserControl
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
           DependencyProperty.Register("ViewModel", typeof(ImportDataTextColumnsViewModel),
           typeof(ImportDataTextColumnsDelimited));

        public ImportDataTextColumnsViewModel ViewModel
        {
            get { return (ImportDataTextColumnsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public ImportDataTextColumnsDelimited(ImportDataTextColumnsViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
        }

        #endregion
    }
}
