﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataMerchHierarchyProvider : IViewDataProvider, IDisposable
    {
        #region Fields
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private readonly BulkObservableCollection<ViewDataMerchProviderRowEntry> _dataSourceEntries = new BulkObservableCollection<ViewDataMerchProviderRowEntry>();
        private Boolean _isLoadingData;
        #endregion

        #region Properties

        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="hierarchyView"></param>
        public ViewDataMerchHierarchyProvider()
        {
            //nb - columns must be loaded after data for this.
            BeginDataLoad();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            if (_dataSourceEntries.Count > 0)
            {

                ProductHierarchy hierarchy = _dataSourceEntries[0].ProductGroup.ParentHierarchy;

                //create the columnset
                foreach (ProductLevel level in hierarchy.EnumerateAllLevels())
                {
                    String searchKey = level.Name;

                    if (!level.IsRoot)
                    {
                        //create the code column
                        DataGridTextColumn levelCodeCol = new DataGridTextColumn();
                        levelCodeCol.IsReadOnly = true;

                        //bind the header to the level name + Code
                        MultiBinding headerBinding = new MultiBinding();
                        headerBinding.Bindings.Add(new Binding() { Source = "{0} {1}" });
                        headerBinding.Bindings.Add(new Binding(ProductLevel.NameProperty.Name) { Source = level });
                        headerBinding.Bindings.Add(new Binding() { Source = ProductGroup.CodeProperty.FriendlyName });
                        headerBinding.Converter = App.Current.Resources[Galleria.Framework.Controls.Wpf.ResourceKeys.ConverterStringFormat] as IMultiValueConverter;
                        BindingOperations.SetBinding(levelCodeCol, DataGridColumn.HeaderProperty, headerBinding);


                        //constuct the data binding path
                        //nb source collection is flattenedUnits
                        String bindingPath = String.Format("LevelPathValues[{0}].Code", searchKey);
                        levelCodeCol.Binding = new Binding(bindingPath) { Mode = BindingMode.TwoWay };
                        //add the column to the set
                        _columnSet.Add(levelCodeCol);



                        //create the name column for the level
                        DataGridExtendedTextColumn levelNameCol = new DataGridExtendedTextColumn();
                        levelNameCol.IsReadOnly = true;
                        //bind the header to the level name
                        MultiBinding headerNameBinding = new MultiBinding();
                        headerNameBinding.Bindings.Add(new Binding() { Source = "{0} {1}" });
                        headerNameBinding.Bindings.Add(new Binding(ProductLevel.NameProperty.Name) { Source = level });
                        headerNameBinding.Bindings.Add(new Binding() { Source = ProductGroup.NameProperty.FriendlyName });
                        headerNameBinding.Converter = App.Current.Resources[Galleria.Framework.Controls.Wpf.ResourceKeys.ConverterStringFormat] as IMultiValueConverter;
                        BindingOperations.SetBinding(levelNameCol, DataGridColumn.HeaderProperty, headerNameBinding);
                        //constuct the data binding path
                        //nb source collection is flattenedUnits
                        String nameBindingPath = String.Format("LevelPathValues[{0}].Name", searchKey);
                        levelNameCol.Binding = new Binding(nameBindingPath) { Mode = BindingMode.TwoWay };
                        //add the column to the set
                        _columnSet.Add(levelNameCol);
                    }
                }
            }

            OnPropertyChanged(IViewDataProviderHelper.ColumnSetProperty.Path);
        }

        public void EditItem(Object selectedItem, Window senderControl)
        {
            MerchHierarchyMaintenance.MerchHierarchyOrganiser merchHierarchyOrganiser = new MerchHierarchyMaintenance.MerchHierarchyOrganiser();
            App.ShowWindow(merchHierarchyOrganiser, senderControl,/*isModel*/true);
        }

        private void BeginDataLoad()
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(App.ViewState.EntityId);
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 entityId = (Int32)e.Argument;

            List<ViewDataMerchProviderRowEntry> rows = new List<ViewDataMerchProviderRowEntry>();

            ProductHierarchy merchHierarchy = ProductHierarchy.FetchByEntityId(entityId);
            if (merchHierarchy != null)
            {
                foreach (ProductGroup group in merchHierarchy.EnumerateAllGroups())
                {
                    if (!group.IsRoot)
                    {
                        rows.Add(new ViewDataMerchProviderRowEntry(group));
                    }
                }

            }

            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataMerchProviderRowEntry> rows = e.Result as List<ViewDataMerchProviderRowEntry>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);
            }

            //re-create columns as level structure may have changed.
            CreateColumns();

            this.IsLoadingData = false;
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataMerchProviderRowEntry
        {
            #region Fields
            private ProductGroup _productGroup;
            private Dictionary<String, ProductGroup> _levelPathValues;
            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath ProductGroupProperty = WpfHelper.GetPropertyPath<ViewDataMerchProviderRowEntry>(p => p.ProductGroup);
            public static readonly PropertyPath LevelPathValuesProperty = WpfHelper.GetPropertyPath<ViewDataMerchProviderRowEntry>(p => p.LevelPathValues);

            #endregion

            #region Properties

            /// <summary>
            /// Returns the the source product group for this view
            /// </summary>
            public ProductGroup ProductGroup
            {
                get { return _productGroup; }
            }

            /// <summary>
            /// Returns the dictionary collection of level path values
            /// </summary>
            public Dictionary<String, ProductGroup> LevelPathValues
            {
                get
                {
                    if (_levelPathValues == null)
                    {
                        _levelPathValues = ConstructLevelPath();
                    }
                    return _levelPathValues;
                }
            }

            #endregion

            #region Constructor
            public ViewDataMerchProviderRowEntry(ProductGroup group)
            {
                _productGroup = group;
            }
            #endregion

            #region Methods

            /// <summary>
            /// Constructs the level path dictionary for the source group
            /// </summary>
            /// <returns></returns>
            private Dictionary<String, ProductGroup> ConstructLevelPath()
            {
                Dictionary<String, ProductGroup> levelPathDict = new Dictionary<String, ProductGroup>();

                ProductGroup group = _productGroup;
                ProductHierarchy structure = group.ParentHierarchy;

                List<ProductGroup> pathPartList = group.GetParentPath();
                pathPartList.Add(group);

                ProductLevel[] levelList = structure.EnumerateAllLevels().ToArray();

                for (Int32 i = 0; i < levelList.Count(); i++)
                {
                    if (pathPartList.Count > i)
                    {
                        levelPathDict.Add(levelList[i].Name, pathPartList[i]);
                    }
                    else
                    {
                        levelPathDict.Add(levelList[i].Name, null);
                    }
                }

                return levelPathDict;
            }

            #endregion

        }

        #endregion

        #region IPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        #endregion

        #region IViewDataProvider members

        Boolean IViewDataProvider.IsDeleteSupported
        {
            //force items to be deleted using the maintenance screen
            // as deleting 1 group may also remove others.
            get { return false; }
        }

        void IViewDataProvider.DeleteItem(Object selectedItem) { }

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }


        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Object IViewDataProvider.SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return false; }
        }

        Object IViewDataProvider.SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }

        Object IViewDataProvider.SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _dataSourceEntries.Clear();
                }
                _isDisposed = true;
            }
        }
        #endregion

    }
}
