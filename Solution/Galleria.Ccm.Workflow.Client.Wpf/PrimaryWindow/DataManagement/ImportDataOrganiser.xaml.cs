﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Input;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDefinitionOrganiser.xaml
    /// </summary>
    public partial class ImportDataOrganiser : ExtendedRibbonWindow
    {
        #region Fields

        private ImportDataSelectFile _selectFileStep;
        private ImportDataTextColumnsPreview _textColumnsPreviewStep;
        private ImportDataTextColumnsFixedWidth _textColumnsFixedWidthStep;
        private ImportDataTextColumnsDelimited _textColumnsDelimitedStep;
        private ImportDataColumnDefinition _columnDefinitionStep;
        private ImportDataValidation _validationStep;

        #endregion

        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportDataViewModel), typeof(ImportDataOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportDataViewModel ViewModel
        {
            get { return (ImportDataViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportDataOrganiser senderControl = (ImportDataOrganiser)obj;

            if (e.OldValue != null)
            {
                ImportDataViewModel oldModel = (ImportDataViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                senderControl.DestroyAllPages();
            }


            if (e.NewValue != null)
            {
                ImportDataViewModel newModel = (ImportDataViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                senderControl.OnCurrentStepChanged();
            }

        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ImportDataOrganiser(ImportDataViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //start loading
            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageDataManagementImportDataWizard);

            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(ImportDataOrganiser_Loaded);
        }

        private void ImportDataOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ImportDataOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }
        #endregion

        #region Event Handlers

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ImportDataViewModel.CurrentStepProperty.Path)
            {
                OnCurrentStepChanged();
            }
        }

        /// <summary>
        /// Loads the current step
        /// </summary>
        private void OnCurrentStepChanged()
        {
            MDMImportDataStep newValue = this.ViewModel.CurrentStep;

            this.contentArea.Children.Clear();

            switch (newValue)
            {
                case MDMImportDataStep.SelectDataType:
                    if (_selectFileStep == null)
                    {
                        _selectFileStep = new ImportDataSelectFile(this.ViewModel.SelectDataTypeViewModel);
                    }
                    this.contentArea.Children.Add(_selectFileStep);
                    break;

                case MDMImportDataStep.TextColumnsPreview:
                    if (_textColumnsPreviewStep == null)
                    {
                        _textColumnsPreviewStep = new ImportDataTextColumnsPreview(this.ViewModel.TextColumnsViewModel);
                    }
                    this.contentArea.Children.Add(_textColumnsPreviewStep);
                    break;

                case MDMImportDataStep.TextColumnsFixedWidth:
                    if (_textColumnsFixedWidthStep == null)
                    {
                        _textColumnsFixedWidthStep = new ImportDataTextColumnsFixedWidth(this.ViewModel.TextColumnsViewModel);
                    }
                    this.contentArea.Children.Add(_textColumnsFixedWidthStep);
                    break;

                case MDMImportDataStep.TextColumnsDelimited:
                    if (_textColumnsDelimitedStep == null)
                    {
                        _textColumnsDelimitedStep = new ImportDataTextColumnsDelimited(this.ViewModel.TextColumnsViewModel);
                    }
                    this.contentArea.Children.Add(_textColumnsDelimitedStep);
                    break;

                case MDMImportDataStep.ColumnDefinition:
                    if (_columnDefinitionStep == null)
                    {
                        _columnDefinitionStep = new ImportDataColumnDefinition(this.ViewModel.ColumnDefinitionViewModel);
                    }
                    this.contentArea.Children.Add(_columnDefinitionStep);
                    break;

                case MDMImportDataStep.Validation:
                    if (_validationStep == null)
                    {
                        _validationStep = new ImportDataValidation(this.ViewModel.ValidationViewModel);
                    }
                    this.contentArea.Children.Add(_validationStep);
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Drops all references to all content pages
        /// </summary>
        private void DestroyAllPages()
        {
            this.contentArea.Children.Clear();

            if (_selectFileStep != null)
            {
                _selectFileStep.ViewModel = null;
                _selectFileStep = null;
            }

            if (_textColumnsPreviewStep != null)
            {
                _textColumnsPreviewStep.ViewModel = null;
                _textColumnsPreviewStep = null;
            }

            if (_textColumnsFixedWidthStep != null)
            {
                _textColumnsFixedWidthStep.ViewModel = null;
                _textColumnsFixedWidthStep = null;
            }

            if (_textColumnsDelimitedStep != null)
            {
                _textColumnsDelimitedStep.ViewModel = null;
                _textColumnsDelimitedStep = null;
            }

            if (_columnDefinitionStep != null)
            {
                _columnDefinitionStep.ViewModel = null;
                _columnDefinitionStep = null;
            }

            if (_validationStep != null)
            {
                _validationStep.ViewModel = null;
                _validationStep = null;
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Calls the cancel command on cross clicked
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            if (this.ViewModel != null)
            {
                e.Cancel = true;
                this.ViewModel.CancelCommand.Execute();
            }
        }

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ViewModel != null)
                    {
                        ImportDataViewModel oldModel = this.ViewModel;
                        this.ViewModel = null;
                        if (oldModel != null)
                        {
                            oldModel.Dispose();
                        }
                    }

                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
