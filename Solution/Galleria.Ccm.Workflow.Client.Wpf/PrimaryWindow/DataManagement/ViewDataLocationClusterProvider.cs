﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#region Version History: (CCM 8.3.0)
// V8-31877 : N.Haywood
//  Added Product Group Code
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Ccm.Imports.Mappings;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationClusterProvider : IViewDataProvider
    {
        #region Fields
        private readonly BulkObservableCollection<ViewDataLocationClusterProviderRowEntry> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationClusterProviderRowEntry>();
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private Boolean _isLoadingData;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to use
        /// </summary>
        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }


        public Boolean IsDeleteSupported
        {
            get
            {
                //Cannot support delete of cluster locations as
                //we would not know what to do with them.
                return false;
            }
        }



        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewDataLocationClusterProvider()
        {
            CreateColumns();
            BeginDataLoad();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the location import mapping list
            LocationClusterImportMappingList mappingList =
                LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList,
                ViewDataLocationClusterProviderRowEntry.OrginalClusterLocationProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationClusterImportMappingList.SchemeNameMappingId, ViewDataLocationClusterProviderRowEntry.SchemeNameProperty.Path},
                    {LocationClusterImportMappingList.ClusterNameMappingId, ViewDataLocationClusterProviderRowEntry.ClusterNameProperty.Path},
                    {LocationClusterImportMappingList.ProductGroupCodeMappingId, ViewDataLocationClusterProviderRowEntry.ProductGroupCodeProperty.Path}

                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }
        }

        public void EditItem(object selectedItem, Window sourcecontrol)
        {
            ViewDataLocationClusterProviderRowEntry selectedClusterItem = (ViewDataLocationClusterProviderRowEntry)selectedItem;

            LocationClusterMaintenance.LocationClusterMaintenanceOrganiser clusterOrganiser = new LocationClusterMaintenance.LocationClusterMaintenanceOrganiser();
            clusterOrganiser.ViewModel.OpenCommand.Execute(selectedClusterItem.OriginalClusterScheme.Id);

            App.ShowWindow(clusterOrganiser,sourcecontrol,/*isModel*/true);
        }

        /// <summary>
        /// Starts loading data aysnchronously
        /// </summary>
        private void BeginDataLoad()
        {
            this.IsLoadingData = true;


            //clear existing results
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(App.ViewState.EntityId);
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 entityId = (Int32)e.Argument;

            List<ViewDataLocationClusterProviderRowEntry> rows = new List<ViewDataLocationClusterProviderRowEntry>();

            ClusterSchemeInfoList list = ClusterSchemeInfoList.FetchByEntityId(entityId);

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(entityId);

            var groups = hierarchy.FetchAllGroups();
            if (list != null && list.Count > 0)
            {

                foreach (ClusterSchemeInfo info in list)
                {
                    ClusterScheme clusterScheme = ClusterScheme.FetchById(info.Id);
                    if (clusterScheme != null)
                    {
                        ProductGroup productGroup = null;
                        if(clusterScheme.ProductGroupId != null)
                        {
                            productGroup = groups.FirstOrDefault(p => p.Id ==clusterScheme.ProductGroupId);
                        }
                        foreach (Cluster cluster in clusterScheme.Clusters)
                        {
                            foreach (ClusterLocation location in cluster.Locations)
                            {
                                //Construct data object
                                rows.Add(new ViewDataLocationClusterProviderRowEntry(clusterScheme, cluster, location, productGroup));
                            }
                        }
                    }
                }

            }



            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationClusterProviderRowEntry> rows = e.Result as List<ViewDataLocationClusterProviderRowEntry>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);
            }

            this.IsLoadingData = false;
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataLocationClusterProviderRowEntry
        {
            #region Fields
            private ClusterScheme _originalClusterSchemeObject;
            private Cluster _originalClusterObject;
            private ClusterLocation _originalClusterLocationObject;
            private ProductGroup _originalProductGroup;
            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath OrginalClusterLocationProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationClusterProviderRowEntry>(p => p.OrginalClusterLocation);
            public static readonly PropertyPath SchemeNameProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationClusterProviderRowEntry>(p => p.SchemeName);
            public static readonly PropertyPath ClusterNameProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationClusterProviderRowEntry>(p => p.ClusterName);
            public static readonly PropertyPath ProductGroupCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationClusterProviderRowEntry>(p => p.ProductGroupCode);

            #endregion

            #region Properties

            /// <summary>
            /// Returns the original cluster scheme model object
            /// </summary>
            public ClusterScheme OriginalClusterScheme
            {
                get { return _originalClusterSchemeObject; }
            }

            /// <summary>
            /// Returns the original cluster model object
            /// </summary>
            public Cluster OriginalCluster
            {
                get { return _originalClusterObject; }
            }

            /// <summary>
            /// Returns the original cluster location model object
            /// </summary>
            public ClusterLocation OrginalClusterLocation
            {
                get { return _originalClusterLocationObject; }
            }

            public ProductGroup OriginalProductGroup
            {
                get { return _originalProductGroup; }
            }

            /// <summary>
            /// Returns the scheme name
            /// </summary>
            public String SchemeName
            {
                get { return this.OriginalClusterScheme.Name; }
            }

            /// <summary>
            /// Returns the cluster name
            /// </summary>
            public String ClusterName
            {
                get { return this.OriginalCluster.Name; }
                set { this.OriginalCluster.Name = value; }
            }

            public String ProductGroupCode
            {
                get 
                {
                    if (this.OriginalProductGroup != null)
                    {
                        return this.OriginalProductGroup.Code;
                    }
                    return "";
                }
            }


            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="constraintObject"></param>
            public ViewDataLocationClusterProviderRowEntry(ClusterScheme clusterScheme, Cluster cluster, ClusterLocation location, ProductGroup productGroup)
            {
                _originalClusterSchemeObject = clusterScheme;
                _originalClusterObject = cluster;
                _originalClusterLocationObject = location;
                _originalProductGroup = productGroup;
            }


            #endregion

        }

        #endregion

        #region IViewDataProvider Members

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }

        void IViewDataProvider.DeleteItem(Object selectedItem)
        {
            //cannot delete a cluster as what will we do with the unassigned locs?
            throw new NotSupportedException();
        }


        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }


        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
