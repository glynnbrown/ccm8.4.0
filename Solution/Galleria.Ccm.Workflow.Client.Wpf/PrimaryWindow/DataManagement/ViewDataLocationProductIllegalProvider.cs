﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using System.Collections;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Csla;
using System.Windows.Input;
using System.ComponentModel;
using Galleria.Ccm.Imports.Mappings;
using System.Windows.Controls;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationProductIllegalProvider : IViewDataProvider
    {
        #region Fields
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private Boolean _isLoadingData;
        private BulkObservableCollection<ViewDataLocationProductIllegalRow> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationProductIllegalRow>();
        private LocationProductIllegalList _masterList;
        #endregion

        #region Properties

        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        public Boolean IsEditSupported { get; private set; }

        public Boolean IsDeleteSupported { get; private set; }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        public ViewDataLocationProductIllegalProvider()
        {
            this.IsEditSupported = false;//Cannot change the available products
            this.IsDeleteSupported = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.DeleteObject, typeof(LocationProductIllegal));

            CreateColumns();
            BeginDataLoad();
        }

        #endregion

        #region Methods

        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the locationproductillegal import mapping list
            LocationProductIllegalImportMappingList mappingList =
                LocationProductIllegalImportMappingList.NewLocationProductIllegalImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList,
                ViewDataLocationProductIllegalRow.LocationProductIllegalProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationProductAttributeImportMappingList.LocationCodeMapId, ViewDataLocationProductIllegalRow.LocationCodeProperty.Path},
                    {LocationProductAttributeImportMappingList.ProductGTINMapId, ViewDataLocationProductIllegalRow.ProductGtinProperty.Path},
                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }

            
        }


        /// <summary>
        /// Start loading data
        /// </summary>
        private void BeginDataLoad()
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(App.ViewState.EntityId);
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 entityId = (Int32)e.Argument;

            List<ViewDataLocationProductIllegalRow> rows = new List<ViewDataLocationProductIllegalRow>();

            LocationProductIllegalList list = LocationProductIllegalList.FetchByEntityId(entityId);
            if (list != null && list.Count > 0)
            {
                //create lookups
                Dictionary<Int16, String> locationCodeLookup = LocationInfoList.FetchByEntityId(entityId).ToDictionary(l => l.Id, l => l.Code);

                List<Int32> productIds = list.Select(l => l.ProductId).Distinct().ToList();
                Dictionary<Int32, String> productCodeLookup;
                if (productIds.Count < 5000)
                {
                    productCodeLookup = ProductInfoList.FetchByEntityIdProductIds(entityId, productIds).ToDictionary(p => p.Id, p => p.Gtin);
                }
                else
                {
                    productCodeLookup = ProductInfoList.FetchByEntityId(entityId).ToDictionary(p => p.Id, p => p.Gtin);
                }


                //create rows
                foreach (LocationProductIllegal link in list)
                {
                    String gtin = String.Empty;
                    productCodeLookup.TryGetValue(link.ProductId, out gtin);

                    String locCode = String.Empty;
                    locationCodeLookup.TryGetValue(link.LocationId, out locCode);

                    rows.Add(new ViewDataLocationProductIllegalRow(link, locCode, gtin));
                }

            }


            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationProductIllegalRow> rows = e.Result as List<ViewDataLocationProductIllegalRow>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);

                _masterList = rows[0].OriginalLocationProductIllegal.Parent as LocationProductIllegalList;
            }

            this.IsLoadingData = false;
        }


        public void DeleteItem(object selectedItem)
        {
            ViewDataLocationProductIllegalRow row = selectedItem as ViewDataLocationProductIllegalRow;
            if (row != null && _masterList != null)
            {
                if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(LocationProductIllegal.FriendlyName))
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    Int16 locId = row.OriginalLocationProductIllegal.LocationId;
                    Int32 productId = row.OriginalLocationProductIllegal.ProductId;

                    LocationProductIllegal deleteItem =
                        _masterList.FirstOrDefault(l => l.ProductId == productId && l.LocationId == locId);
                    if (deleteItem != null)
                    {
                        try
                        {
                            _masterList.Remove(deleteItem);
                            _masterList = _masterList.Save();

                            _dataSourceEntries.Remove(row);
                        }
                        catch (DataPortalException ex)
                        {
                            Mouse.OverrideCursor = null;

                            LocalHelper.RecordException(ex);
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(LocationProductIllegal.FriendlyName, OperationType.Delete);

                            Mouse.OverrideCursor = Cursors.Wait;
                        }
                    }


                    Mouse.OverrideCursor = null;
                }
            }
        }

        #endregion

        #region IViewDataProvider Members

        void IViewDataProvider.EditItem(object selectedItem, Window sourcecontrol)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataLocationProductIllegalRow
        {
            #region Fields
            private String _productGtin;
            private String _locationCode;
            private LocationProductIllegal _originalLocationProductIllegal;
            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath LocationProductIllegalProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationProductIllegalRow>(c => c.OriginalLocationProductIllegal);
            public static readonly PropertyPath LocationCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationProductIllegalRow>(p => p.LocationCode);
            public static readonly PropertyPath ProductGtinProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationProductIllegalRow>(p => p.ProductGtin);

            #endregion

            #region Properties

            /// <summary>
            /// Returns the origional location product illegal
            /// </summary>
            public LocationProductIllegal OriginalLocationProductIllegal
            {
                get { return _originalLocationProductIllegal; }
            }

            /// <summary>
            /// Returns the location code
            /// </summary>
            public String LocationCode
            {
                get { return _locationCode; }
            }

            /// <summary>
            /// Returns the product code
            /// </summary>
            public String ProductGtin
            {
                get { return _productGtin; }
            }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="constraintObject"></param>
            public ViewDataLocationProductIllegalRow(LocationProductIllegal locationProductIllegal, String location, String productGtin)
            {
                _originalLocationProductIllegal = locationProductIllegal;
                _locationCode = location;
                _productGtin = productGtin;
                
            }


            #endregion

        }

        #endregion

        #region IViewDataProvider Members

        //void IViewDataProvider.DeleteItem(object selectedItem)
        //{
        //    throw new NotSupportedException();
        //}

        public bool IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public bool IsLocationSelectorVisible
        {
            get { return false; }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public bool IsCategorySelectorVisible
        {
            get { return false; }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
