﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion

using System.Windows;
using System.Windows.Controls;
namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDefinitionTextColumnsPreview.xaml
    /// </summary>
    public partial class ImportDataTextColumnsPreview : UserControl
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportDataTextColumnsViewModel),
            typeof(ImportDataTextColumnsPreview));

        public ImportDataTextColumnsViewModel ViewModel
        {
            get { return (ImportDataTextColumnsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        public ImportDataTextColumnsPreview(ImportDataTextColumnsViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
        }
    }
}
