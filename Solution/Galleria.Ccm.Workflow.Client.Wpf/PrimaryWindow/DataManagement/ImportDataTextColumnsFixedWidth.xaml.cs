﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDefinitionTextColumnsFixedWidth.xaml
    /// </summary>
    public partial class ImportDataTextColumnsFixedWidth : UserControl
    {
        #region Constants

        const Int32 charSize = 7;
        const Int32 rulerHeight = 35;

        #endregion


        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
          DependencyProperty.Register("ViewModel", typeof(ImportDataTextColumnsViewModel),
          typeof(ImportDataTextColumnsFixedWidth),
          new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportDataTextColumnsViewModel ViewModel
        {
            get { return (ImportDataTextColumnsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportDataTextColumnsFixedWidth senderControl = (ImportDataTextColumnsFixedWidth)obj;

            if (e.OldValue != null)
            {
                ImportDataTextColumnsViewModel oldModel = (ImportDataTextColumnsViewModel)e.OldValue;
                oldModel.FixedWidthLines.CollectionChanged -= senderControl.FixedWidthLines_CollectionChanged;

            }

            if (e.NewValue != null)
            {
                ImportDataTextColumnsViewModel newModel = (ImportDataTextColumnsViewModel)e.NewValue;
                newModel.FixedWidthLines.CollectionChanged += senderControl.FixedWidthLines_CollectionChanged;

            }

        }

        #endregion


        #region Constructor

        public ImportDataTextColumnsFixedWidth(ImportDataTextColumnsViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(ImportDataTextColumnsFixedWidth_Loaded);
        }

        void ImportDataTextColumnsFixedWidth_Loaded(object sender, RoutedEventArgs e)
        {
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction((Action)(() => OnViewBoundryChanged()));
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the fixed width lines collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FixedWidthLines_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //OnViewBoundryChanged();

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ImportFixedLineDef lineDef in e.NewItems)
                    {
                        AddLine(lineDef, this.rulerCanvas);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ImportFixedLineDef lineDef in e.OldItems)
                    {
                        ImportDataFixedWidthLine line =
                            this.rulerCanvas.Children.OfType<ImportDataFixedWidthLine>()
                            .FirstOrDefault(l => l.LineDef == lineDef);

                        this.rulerCanvas.Children.Remove(line);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    OnViewBoundryChanged();
                    break;
            }


        }

        /// <summary>
        /// Adds a new line for the clicked point
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RulerCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point hitPoint = e.GetPosition(this.rulerCanvas);
            Int32 horizPoint = Convert.ToInt32(hitPoint.X / charSize);


            //if the line point does not already exist then add it
            ImportFixedLineDef existingDef = this.ViewModel.FixedWidthLines.FirstOrDefault(l => l.LineValue == horizPoint);
            if (existingDef == null)
            {
                this.ViewModel.FixedWidthLines.Add(new ImportFixedLineDef(horizPoint));
            }
        }


        private void OnViewBoundryChanged()
        {
            //remove all children from the canvas
            List<ImportDataFixedWidthLine> childLines =
                this.rulerCanvas.Children.OfType<ImportDataFixedWidthLine>().ToList();
            foreach (ImportDataFixedWidthLine line in childLines)
            {
                this.rulerCanvas.Children.Remove(line);
            }

            //add in the new lines
            if (this.ViewModel != null)
            {
                DrawRuler();

                foreach (ImportFixedLineDef linePoint in this.ViewModel.FixedWidthLines)
                {
                    AddLine(linePoint, this.rulerCanvas);
                }
            }

        }

        private void line_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ImportDataFixedWidthLine line = (ImportDataFixedWidthLine)sender;
            line.MouseDoubleClick += new MouseButtonEventHandler(line_MouseDoubleClick);

            //remove the line
            this.ViewModel.FixedWidthLines.Remove(line.LineDef);
            this.rulerCanvas.Children.Remove(line);
        }

        private void rawDataGrid_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            //update the canvas position
            Canvas.SetLeft(rulerCanvas, -e.HorizontalOffset);
        }

        #endregion

        #region Methods

        private void DrawRuler()
        {
            //get the max width of the raw lines
            Int32 maxLineLength = this.ViewModel.RawPreviewData.Max(s => s.Length);

            //round this up to 10.
            Int32 roundedMaxLineLength = maxLineLength;
            while (!(roundedMaxLineLength % 10 == 0))
            {
                roundedMaxLineLength++;
            }

            Int32 drawWidth = roundedMaxLineLength * charSize;
            this.rulerCanvas.Width = drawWidth;

            //for each point draw a line
            //if divisable by 10 draw a major line
            for (Int32 i = 1; i <= roundedMaxLineLength; i++)
            {
                Int32 left = i * charSize;

                //major line
                if (i % 10 == 0)
                {
                    //create a marker textblock
                    TextBlock nbr = new TextBlock();
                    nbr.Text = String.Format("{0}", i);
                    nbr.FontSize = 11;
                    nbr.Width = nbr.Text.Length * charSize;
                    Canvas.SetLeft(nbr, left - (nbr.Width / 2)); //set to center
                    Canvas.SetTop(nbr, 5);
                    this.rulerCanvas.Children.Add(nbr);

                    //add the marker line
                    Rectangle markerLine = new Rectangle();
                    Canvas.SetLeft(markerLine, i * charSize); //set to center
                    Canvas.SetTop(markerLine, 25);
                    markerLine.Height = 10;
                    markerLine.Width = 1;
                    markerLine.Stroke = Brushes.Black;
                    this.rulerCanvas.Children.Add(markerLine);
                }
                else
                {
                    //add the marker line
                    Rectangle markerLine = new Rectangle();
                    Canvas.SetLeft(markerLine, i * charSize); //set to center
                    Canvas.SetTop(markerLine, 30);
                    markerLine.Height = 5;
                    markerLine.Width = 1;
                    markerLine.Stroke = Brushes.Black;
                    this.rulerCanvas.Children.Add(markerLine);
                }
            }

        }

        /// <summary>
        /// Returns the boundry in which lines should be displayed
        /// </summary>
        /// <returns></returns>
        private Rect GetViewBoundary()
        {
            Rect boundry = new Rect();
            boundry.Height = this.rulerCanvas.ActualHeight;
            boundry.Width = this.rulerCanvas.Width;
            //boundry.X = rawDataGrid.CellsPanelHorizontalOffset;

            return boundry;
        }

        /// <summary>
        /// Adds a new line to the given canvas
        /// </summary>
        /// <param name="linePoint"></param>
        /// <param name="cnvas"></param>
        private void AddLine(ImportFixedLineDef linePoint, Canvas cnvas)
        {
            ImportDataFixedWidthLine line = new ImportDataFixedWidthLine(linePoint);
            line.Height = cnvas.ActualHeight;
            Canvas.SetTop(line, rulerHeight);
            cnvas.Children.Add(line);

            //subscribe to the child dragged and double clicked
            line.MouseDoubleClick += new MouseButtonEventHandler(line_MouseDoubleClick);
        }




        #endregion

    }


    /// <summary>
    /// Control representing a line used to indicated a fixed width break point
    /// </summary>
    public class ImportDataFixedWidthLine : Thumb
    {
        /* Line actions - 
         *  Drag to move the line point
         *  double click to delete the line
        */

        #region Constants

        const Int32 charWidth = 7;
        #endregion

        #region Properties

        #region LineDef Property

        public static readonly DependencyProperty LineDefProperty =
            DependencyProperty.Register("LineDef", typeof(ImportFixedLineDef),
            typeof(ImportDataFixedWidthLine));

        public ImportFixedLineDef LineDef
        {
            get { return (ImportFixedLineDef)GetValue(LineDefProperty); }
            set { SetValue(LineDefProperty, value); }
        }

        #endregion



        #endregion


        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static ImportDataFixedWidthLine()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
               typeof(ImportDataFixedWidthLine), new FrameworkPropertyMetadata(typeof(ImportDataFixedWidthLine)));
        }

        public ImportDataFixedWidthLine(ImportFixedLineDef def)
        {
            this.LineDef = def;
            this.DragDelta += new DragDeltaEventHandler(ImportDataFixedWidthLine_DragDelta);
            this.DragCompleted += new DragCompletedEventHandler(ImportDataFixedWidthLine_DragCompleted);

            Canvas.SetLeft(this, def.LineValue * charWidth);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to the attempt to drag the line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDataFixedWidthLine_DragDelta(object sender, DragDeltaEventArgs e)
        {
            //only allow a horizontal drag
            Int32 newHorizPostion = Convert.ToInt32(Canvas.GetLeft(this) + e.HorizontalChange);
            if (newHorizPostion > 0)
            {
                //correct to the nearest point
                Int32 mod = newHorizPostion % charWidth;
                if (mod != 0)
                {
                    newHorizPostion = newHorizPostion - mod;
                }

                Canvas.SetLeft(this, newHorizPostion);
            }
        }

        private void ImportDataFixedWidthLine_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            Double left = Canvas.GetLeft(this);
            this.LineDef.LineValue = Convert.ToInt32(left / charWidth);
        }

        #endregion


    }
}
