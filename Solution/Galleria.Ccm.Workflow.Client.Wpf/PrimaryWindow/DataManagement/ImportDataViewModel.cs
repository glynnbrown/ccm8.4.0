﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25557 : L.Hodson ~ Copied from SA.
// V8-25444 : N.Haywood
//  Added location imports
// CCM-25449 : N.Haywood
//  Added cluster locations
// CCM-25242 : N.Haywood
//  Added product imports
// CCM-25448 : N.Haywood
//  Added location space
// CCM-25446 : N.Haywood
//  Added Location Product Attributes
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
#endregion
#region Version History: CCM802
// V8-29230 : N.Foster
//  Removed unused parameter from import products process
#endregion
#region Version History: (CCM 8.1.0)
// V8-30130 : A.Probyn
//  Extended file data load process to handle any unhandled exceptions correctly.
#endregion
#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Added LocationProductLegal
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Imports.Processes;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Denotes the available views when on the ImportDefinition screen
    /// </summary>
    public enum MDMImportDataStep
    {
        SelectDataType,
        TextColumnsPreview,
        TextColumnsDelimited,
        TextColumnsFixedWidth,
        ColumnDefinition,
        Validation
    }

    public enum MDMImportDataFileType
    {
        Excel,
        Text
    }

    /// <summary>
    /// ViewModel controller for ImportDefinitionOrganiser
    /// </summary>
    public class ImportDataViewModel : ViewModelAttachedControlObject<ImportDataOrganiser>
    {
        #region Fields

        private Stopwatch _timer = new Stopwatch();
        private ModalBusy _busyDialog;
        private MDMImportDataStep _currentStep;
        private ImportDataSelectFileViewModel _selectDataTypeViewModel;
        private ImportDataTextColumnsViewModel _textColumnsViewModel;
        private ImportDataColumnDefinitionViewModel _columnDefinitionViewModel;
        private ImportDataValidationViewModel _validationViewModel;
        private ImportFileData _fileData;
        private Boolean _hasOutOfMemoryException;
        private Boolean _hasIOException;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath SelectDataTypeViewModelProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.SelectDataTypeViewModel);
        public static readonly PropertyPath TextColumnsViewModelProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.TextColumnsViewModel);
        public static readonly PropertyPath ColumnDefinitionViewModelProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.ColumnDefinitionViewModel);
        public static readonly PropertyPath ValidationViewModelProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.ValidationViewModel);
        public static readonly PropertyPath IsPreviousCommandVisibleProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.IsPreviousCommandVisible);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.IsNextCommandVisible);
        public static readonly PropertyPath IsExportErrorsCommandVisibleProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.IsExportErrorsCommandVisible);

        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.NextCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath ImportCommandProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.ImportCommand);
        public static readonly PropertyPath ExportErrorsCommandProperty = WpfHelper.GetPropertyPath<ImportDataViewModel>(p => p.ExportErrorsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the current screen that should be displayed in the window.
        /// </summary>
        public MDMImportDataStep CurrentStep
        {
            get { return _currentStep; }
            private set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                OnPropertyChanged(IsPreviousCommandVisibleProperty);
                OnPropertyChanged(IsNextCommandVisibleProperty);
                OnPropertyChanged(IsExportErrorsCommandVisibleProperty);
            }
        }

        /// <summary>
        /// Returns the viewmodel for the select data type screen
        /// </summary>
        public ImportDataSelectFileViewModel SelectDataTypeViewModel
        {
            get
            {
                if (_selectDataTypeViewModel == null)
                {
                    _selectDataTypeViewModel = new ImportDataSelectFileViewModel();
                }
                return _selectDataTypeViewModel;
            }
        }

        /// <summary>
        /// Returns the viewmodel for the text columns steps
        /// </summary>
        public ImportDataTextColumnsViewModel TextColumnsViewModel
        {
            get
            {
                if (_textColumnsViewModel == null)
                {
                    _textColumnsViewModel = new ImportDataTextColumnsViewModel(_fileData);
                }
                return _textColumnsViewModel;
            }
        }

        /// <summary>
        /// Returns the viewmodel for the column definition step
        /// </summary>
        public ImportDataColumnDefinitionViewModel ColumnDefinitionViewModel
        {
            get
            {
                if (_columnDefinitionViewModel == null)
                {
                    _columnDefinitionViewModel = new ImportDataColumnDefinitionViewModel(_fileData, _selectDataTypeViewModel);
                    _columnDefinitionViewModel.PropertyChanged += new PropertyChangedEventHandler(ColumnDefinitionViewModel_PropertyChanged);
                }
                return _columnDefinitionViewModel;
            }
        }

        /// <summary>
        /// Returns the viewmodel for the validation step
        /// </summary>
        public ImportDataValidationViewModel ValidationViewModel
        {
            get
            {
                if (_validationViewModel == null)
                {
                    _validationViewModel = new ImportDataValidationViewModel();
                }
                return _validationViewModel;
            }
        }

        /// <summary>
        /// Returns true if the cancel command should be available on the current step
        /// </summary>
        public Boolean IsPreviousCommandVisible
        {
            get
            {
                if (
                    this.CurrentStep == MDMImportDataStep.TextColumnsPreview ||
                   this.CurrentStep == MDMImportDataStep.TextColumnsDelimited ||
                    this.CurrentStep == MDMImportDataStep.TextColumnsFixedWidth ||
                    this.CurrentStep == MDMImportDataStep.ColumnDefinition ||
                    this.CurrentStep == MDMImportDataStep.Validation
                    )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns true if the next command should be available
        /// for the current step
        /// </summary>
        public Boolean IsNextCommandVisible
        {
            get
            {
                if (
                    this.CurrentStep == MDMImportDataStep.TextColumnsPreview ||
                   this.CurrentStep == MDMImportDataStep.TextColumnsDelimited ||
                    this.CurrentStep == MDMImportDataStep.TextColumnsFixedWidth ||
                    this.CurrentStep == MDMImportDataStep.ColumnDefinition ||
                    this.CurrentStep == MDMImportDataStep.Validation ||
                    this.CurrentStep == MDMImportDataStep.SelectDataType
                    )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns true if the export errors command should be available
        /// for the current step
        /// </summary>
        public Boolean IsExportErrorsCommandVisible
        {
            get
            {
                return (this.CurrentStep == MDMImportDataStep.Validation) ? true : false;
            }
        }

        #endregion

        #region Constructor

        public ImportDataViewModel()
        {
            //_fileData = ImportFileData.NewImportFileData();
            this.CurrentStep = MDMImportDataStep.SelectDataType;
        }

        #endregion

        #region Commands

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }
        #endregion

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the process to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next,
                        DisabledReason = "There are no valid records to import."
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Next_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case MDMImportDataStep.SelectDataType:
                    //Set disabled reason
                    this.NextCommand.DisabledReason = Message.DataManagement_NextCommand_NoFileSelectedDisabledReason;
                    Boolean canProcced = true;

                    //a valid file path must be selected
                    return this.SelectDataTypeViewModel.IsFilePathValid && canProcced; ;


                case MDMImportDataStep.TextColumnsPreview:
                case MDMImportDataStep.TextColumnsDelimited:
                case MDMImportDataStep.TextColumnsFixedWidth:
                    //nothing to check
                    return true;

                case MDMImportDataStep.ColumnDefinition:
                    if (this.SelectDataTypeViewModel.SelectedDataType == CCMDataItemType.MerchandisingHierarchy)
                    {
                        //Make sure all columns are mapped
                        foreach (ImportMapping mapping in this._fileData.MappingList)
                        {
                            if (!mapping.IsColumnReferenceSet)
                            {
                                this.NextCommand.DisabledReason = Message.DataManagement_NextCommand_IncompleteHierarchy;
                                return false;
                            }
                        }
                    }

                    //Set disabled reason
                    this.NextCommand.DisabledReason = Message.DataManagement_NextCommand_MissingMappingsDisabledReason;

                    //mappings must be complete and valid
                    if (this.ColumnDefinitionViewModel.AreMappingsAllValid)
                    {
                        //Set disabled reason
                        this.NextCommand.DisabledReason = Message.DataManagement_NextCommand_MappingsNotUniqueDisabledReason;
                        if (this.ColumnDefinitionViewModel.AreMappingsAllUnique)
                        {
                            return true;
                        };
                    }
                    return false;


                case MDMImportDataStep.Validation:
                    //Set disabled reason
                    this.NextCommand.DisabledReason = Message.DataManagement_NextCommand_ValidationDisabledReason;
                    //must be ok to proceed
                    return this.ValidationViewModel.IsDataValid();
            }

            return false;
        }

        private void Next_Executed()
        {
            base.ShowWaitCursor(true);

            switch (this.CurrentStep)
            {
                case MDMImportDataStep.SelectDataType:

                    //if import is null or source does not match selected filepath we need a new file
                    bool requiresNewImportFile = (_fileData == null);
                    if (!requiresNewImportFile) { requiresNewImportFile = (_fileData.SourceFilePath != this.SelectDataTypeViewModel.FilePath); }

                    switch (this.SelectDataTypeViewModel.SelectedFileType)
                    {
                        case MDMImportDataFileType.Text:
                            if (requiresNewImportFile)
                            {
                                try
                                {
                                    _fileData = ImportFileData.NewImportFileData();
                                    OnImportFileDataChanged();
                                    //Only reset if we need to as it clears some values user has set in _fileData
                                    //  SA-22473 : or file data has no rows which may indicate that the file was in use when attempted to load originally
                                    if (this.TextColumnsViewModel.LoadFilePath != this.SelectDataTypeViewModel.FilePath
                                        || _fileData.Rows.Count == 0)
                                    {
                                        this.TextColumnsViewModel.LoadFilePath = this.SelectDataTypeViewModel.FilePath;
                                        _fileData.SourceFilePath = this.SelectDataTypeViewModel.FilePath;
                                    }
                                    this.CurrentStep = MDMImportDataStep.TextColumnsPreview;
                                }
                                catch (IOException exception)
                                {
                                    DisplayImportExceptionMessage(exception);
                                    break;
                                }
                                catch (OutOfMemoryException exception)
                                {
                                    DisplayImportMemoryExceptionMessage(exception);
                                    break;
                                }
                            }
                            else
                            {
                                //update the mapping list if required
                                Type mappingListType = GetMappingListType(this.SelectDataTypeViewModel.SelectedDataType);
                                if (_fileData.MappingList.GetType() != mappingListType)
                                {
                                    _fileData.MappingList = GetMappingList(this.SelectDataTypeViewModel.SelectedDataType);
                                }
                                this.CurrentStep = MDMImportDataStep.TextColumnsPreview;
                            }
                            break;

                        case MDMImportDataFileType.Excel:
                            if (requiresNewImportFile)
                            {
                                Int32 workSheetIndex = (_columnDefinitionViewModel != null) ? _columnDefinitionViewModel.GetSelectedWorksheetIndex() : 0;
                                ImportFromExcelFile(this.SelectDataTypeViewModel.FilePath, workSheetIndex);
                            }
                            else
                            {
                                this.CurrentStep = MDMImportDataStep.ColumnDefinition;
                            }
                            break;

                        default: throw new NotImplementedException();
                    }

                    break;


                case MDMImportDataStep.TextColumnsPreview:
                    if (this.TextColumnsViewModel.IsFixedWidth)
                    {
                        this.CurrentStep = MDMImportDataStep.TextColumnsFixedWidth;
                    }
                    else
                    {
                        this.TextColumnsViewModel.UpdateColumns();
                        this.CurrentStep = MDMImportDataStep.TextColumnsDelimited;
                    }
                    break;


                case MDMImportDataStep.TextColumnsDelimited:
                case MDMImportDataStep.TextColumnsFixedWidth:
                    this.TextColumnsViewModel.UpdateColumns();
                    this.CurrentStep = MDMImportDataStep.ColumnDefinition;
                    break;


                case MDMImportDataStep.ColumnDefinition:

                    //[TODO] Check if the import has changed since validation
                    //in case we have gone back then forward again

                    //begin the validation process
                    Validate(this.SelectDataTypeViewModel.SelectedDataType, this.ColumnDefinitionViewModel.ImportStartRowNumber);
                    break;


                case MDMImportDataStep.Validation:
                    //[SA-14467]Trigger import
                    if (_validationViewModel != null)
                    {
                        InsertToDatabase(this.SelectDataTypeViewModel.SelectedDataType,
                            this.ValidationViewModel.ValidatedFileData);
                    }

                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Moves the process back a step
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Previous_CanExecute()
        {
            return (this.CurrentStep != MDMImportDataStep.SelectDataType);
        }

        private void Previous_Executed()
        {
            switch (this.CurrentStep)
            {
                case MDMImportDataStep.SelectDataType:
                    //show not be shown for this
                    break;


                case MDMImportDataStep.TextColumnsPreview:
                    this.CurrentStep = MDMImportDataStep.SelectDataType;
                    break;


                case MDMImportDataStep.TextColumnsDelimited:
                case MDMImportDataStep.TextColumnsFixedWidth:
                    this.CurrentStep = MDMImportDataStep.TextColumnsPreview;
                    break;


                case MDMImportDataStep.ColumnDefinition:
                    if (this.SelectDataTypeViewModel.SelectedFileType == MDMImportDataFileType.Text)
                    {
                        this.CurrentStep =
                            (this.TextColumnsViewModel.IsFixedWidth) ?
                            MDMImportDataStep.TextColumnsFixedWidth :
                            MDMImportDataStep.TextColumnsDelimited;
                    }
                    else
                    {
                        this.CurrentStep = MDMImportDataStep.SelectDataType;
                    }
                    break;


                case MDMImportDataStep.Validation:
                    this.CurrentStep = MDMImportDataStep.ColumnDefinition;
                    break;
            }

        }

        #endregion

        #region ImportCommand

        private RelayCommand _importCommand;

        /// <summary>
        /// Executes the import
        /// </summary>
        public RelayCommand ImportCommand
        {
            get
            {
                if (_importCommand == null)
                {
                    _importCommand = new RelayCommand(
                        p => Import_Executed())
                    {
                        FriendlyName = Message.DataManagement_Import,
                        Icon = ImageResources.DataManagement_ImportData
                    };
                    base.ViewModelCommands.Add(_importCommand);
                }
                return _importCommand;
            }
        }

        private void Import_Executed()
        {
            if (_validationViewModel != null)
            {
                InsertToDatabase(this.SelectDataTypeViewModel.SelectedDataType,
                    this.ValidationViewModel.ValidatedFileData);
            }
        }

        #endregion

        #region ExportErrorsCommand

        private RelayCommand _exportErrorsCommand;

        /// <summary>
        /// Exports the errors to excel
        /// </summary>
        public RelayCommand ExportErrorsCommand
        {
            get
            {
                if (_exportErrorsCommand == null)
                {
                    _exportErrorsCommand = new RelayCommand(
                        p => ExportErrors_Executed(),
                        p => ExportErrors_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_ExportErrors,
                        FriendlyDescription = Message.DataManagement_ExportErrors_Description,
                        DisabledReason = Message.DataManagement_ExportErrors_DisabledReason,
                    };
                    base.ViewModelCommands.Add(_exportErrorsCommand);
                }
                return _exportErrorsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ExportErrors_CanExecute()
        {
            if (this.CurrentStep == MDMImportDataStep.Validation && this.ValidationViewModel != null)
            {
                return this.ValidationViewModel.WorksheetErrors.Count > 0;
            }

            return false;
        }

        private void ExportErrors_Executed()
        {
            if (this.AttachedControl != null)
            {
                //get the filename to save as
                Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
                saveDialog.CreatePrompt = false;
                saveDialog.Filter = Message.DataManagement_ExportFilter;
                saveDialog.FilterIndex = 2;
                saveDialog.FileName = String.Format(Message.DataManagement_ExportErrorsFileName, Path.GetFileNameWithoutExtension(this.ValidationViewModel.ValidatedFileData.SourceFilePath));
                saveDialog.OverwritePrompt = true;

                if (saveDialog.ShowDialog() == true)
                {
                    Boolean canContinue = true;


                    //If the selected file type is XLS
                    if (saveDialog.FilterIndex == 1)
                    {
                        //Show warning dialog informing user that if > 65k rows, they will not be outputted
                        //as it exceeds this file type row limit.
                        ModalMessage modelMessageWindow = new ModalMessage();
                        modelMessageWindow.Description = Message.DataManagement_RowNumberExceedsMaxLimitWarning;
                        modelMessageWindow.ButtonCount = 2;
                        modelMessageWindow.Button1Content = Message.Generic_Ok;
                        modelMessageWindow.Button2Content = Message.Generic_Cancel;
                        modelMessageWindow.Icon = ImageResources.Warning_32;
                        App.ShowWindow(modelMessageWindow, true);

                        //set the result
                        canContinue = (modelMessageWindow.Result == ModalMessageResult.Button1);
                    }


                    if (canContinue)
                    {
                        String fileName = saveDialog.FileName;

                        //Try to delete existing
                        if (System.IO.File.Exists(fileName))
                        {
                            canContinue = FileHelper.DeleteExistingFile(fileName);
                        }

                        if (canContinue)
                        {
                            ExportErrors(this.ValidationViewModel.ValidatedFileData, this.ValidationViewModel.WorksheetErrors, saveDialog.FileName);
                        }

                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        private void OnImportFileDataChanged()
        {
            //get its mapping list
            _fileData.MappingList = GetMappingList(this.SelectDataTypeViewModel.SelectedDataType);
            _fileData.SourceFilePath = this.SelectDataTypeViewModel.FilePath;

            if (_textColumnsViewModel != null)
            {
                _textColumnsViewModel.FileData = _fileData;
            }

            //update the views as required.
            if (_columnDefinitionViewModel != null)
            {
                //drop handler while we do this
                _columnDefinitionViewModel.PropertyChanged -= ColumnDefinitionViewModel_PropertyChanged;

                _columnDefinitionViewModel.FileData = _fileData;

                _columnDefinitionViewModel.PropertyChanged += ColumnDefinitionViewModel_PropertyChanged;
            }
        }

        private void ColumnDefinitionViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ImportDataColumnDefinitionViewModel.SelectedWorksheetProperty.Path)
            {
                ImportFromExcelFile(this.SelectDataTypeViewModel.FilePath,
                            this.ColumnDefinitionViewModel.GetSelectedWorksheetIndex());
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the mapping list for the given type
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        private IImportMappingList GetMappingList(CCMDataItemType dataType)
        {
            //nb: if using any viewstate objects we must check for null
            //if it is null then load independently as we dont want the data getting 'stuck'

            switch (dataType)
            {
                case CCMDataItemType.Location:
                    return LocationImportMappingList.NewLocationImportMappingList();

                case CCMDataItemType.Product:
                    return ProductImportMappingList.NewProductImportMappingList(true);

                case CCMDataItemType.LocationClusters:
                    return LocationClusterImportMappingList.NewLocationClusterImportMappingList();

                case CCMDataItemType.MerchandisingHierarchy:
                    return MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList
                        (ProductHierarchy.FetchByEntityId(App.ViewState.EntityId).EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name).ToList());


                case CCMDataItemType.LocationHierarchy:
                    {
                        LocationHierarchy hierarchy = LocationHierarchy.FetchByEntityId(App.ViewState.EntityId);
                        return LocationHierarchyImportMappingList.NewList
                            (hierarchy.EnumerateAllLevels().Where(l => l.IsRoot == false).Select(p => p.Name).ToList());
                    }

                case CCMDataItemType.LocationSpace:
                    return LocationSpaceImportMappingList.NewLocationSpaceImportMappingList();

                case CCMDataItemType.LocationSpaceBay:
                    return LocationSpaceBayImportMappingList.NewLocationSpaceBayImportMappingList();

                case CCMDataItemType.LocationSpaceElement:
                    return LocationSpaceElementImportMappingList.NewLocationSpaceElementImportMappingList();

                case CCMDataItemType.LocationProductAttributes:
                    return LocationProductAttributeImportMappingList.NewLocationProductAttributeImportMappingList();

                case CCMDataItemType.LocationProductIllegal:
                    return LocationProductIllegalImportMappingList.NewLocationProductIllegalImportMappingList();
                
                case CCMDataItemType.LocationProductLegal:
                    return LocationProductLegalImportMappingList.NewLocationProductLegalImportMappingList();

                case CCMDataItemType.Assortment:
                    return AssortmentImportMappingList.NewAssortmentImportMappingList();

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the type of mapping list expected for the given data type.
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        private Type GetMappingListType(CCMDataItemType dataType)
        {
            switch (dataType)
            {
                case CCMDataItemType.Location:
                    return typeof(LocationImportMappingList);

                case CCMDataItemType.Product:
                    return typeof(ProductImportMappingList);

                case CCMDataItemType.LocationClusters:
                    return typeof(LocationClusterImportMappingList);

                case CCMDataItemType.MerchandisingHierarchy:
                    return typeof(MerchHierarchyImportMappingList);

                case CCMDataItemType.LocationHierarchy:
                    return typeof(LocationHierarchyImportMappingList);


                case CCMDataItemType.LocationSpace:
                    return typeof(LocationSpaceImportMappingList);

                case CCMDataItemType.LocationSpaceBay:
                    return typeof(LocationSpaceBayImportMappingList);

                case CCMDataItemType.LocationSpaceElement:
                    return typeof(LocationSpaceElementImportMappingList);

                case CCMDataItemType.LocationProductAttributes:
                    return typeof(LocationProductAttributeImportMappingList);

                case CCMDataItemType.LocationProductIllegal:
                    return typeof(LocationProductIllegalImportMappingList);


                default: throw new NotImplementedException();
            }
        }

        #region Helper Methods

        /// <summary>
        /// Displays a message box containing the exception's message
        /// </summary>
        /// <param name="exception"></param>
        private void DisplayImportExceptionMessage(IOException exception)
        {
            base.ShowWaitCursor(false);
            _fileData = null;

            ModalMessage message = new ModalMessage();
            message.Header = Message.ImportData_FileAccessException;
            message.Description = exception.Message;
            message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            message.ButtonCount = 1;
            message.Button1Content = Message.Generic_Ok;
            message.DefaultButton = ModalMessageButton.Button1;
            message.MessageIcon = ImageResources.DialogError;
            App.ShowWindow(message, true);
        }

        /// <summary>
        /// Displays a message box containing the exception's message
        /// </summary>
        /// <param name="exception"></param>
        private void DisplayImportMemoryExceptionMessage(OutOfMemoryException exception)
        {
            base.ShowWaitCursor(false);
            _fileData = null;

            ModalMessage message = new ModalMessage();
            message.Header = Message.ImportData_SystemMemoryException;
            message.Description = String.Format(Message.ImportData_SystemMemoryExceptionDescription, exception.Message);
            message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            message.ButtonCount = 1;
            message.Button1Content = Message.Generic_Ok;
            message.DefaultButton = ModalMessageButton.Button1;
            message.MessageIcon = ImageResources.DialogError;
            App.ShowWindow(message, true);
        }

        /// <summary>
        /// Shows a message box informing the user the import file is valid, providing an import and cancel button
        /// </summary>
        private Boolean ShowImportMessage()
        {
            if (this.AttachedControl != null)
            {
                //Show import message box
                ModalMessage messageBox = new ModalMessage();
                messageBox.Title = String.Format(Message.BackstageDataManagement_Validation_ImportFrom, this.SelectDataTypeViewModel.SelectedFileType);
                messageBox.Header = Message.BackstageDataManagement_Validation_FileChecked;
                messageBox.Description = Message.BackstageDataManagement_Validation_FileChecked_Description;
                messageBox.ButtonCount = 2;
                messageBox.Button1Content = Message.BackstageDataManagement_Import;
                messageBox.Button2Content = Message.Generic_Cancel;
                messageBox.DefaultButton = ModalMessageButton.Button1;
                messageBox.CancelButton = ModalMessageButton.Button2;

                App.ShowWindow(messageBox, this.AttachedControl, true);

                return messageBox.Result == ModalMessageResult.Button1;
            }

            return true;
        }

        #endregion

        #endregion

        #region Processes

        #region Import

        /// <summary>
        /// Loads the given excel file into the importfiledata object
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileData"></param>
        private void ImportFromExcelFile(String filePath, Int32 worksheetIndex)
        {
            _timer.Restart();

            //start the import worker
            BackgroundWorker importWorker = new BackgroundWorker();
            importWorker.DoWork += ImportWorker_DoWork;
            importWorker.RunWorkerCompleted += ImportWorker_RunWorkerCompleted;
            importWorker.WorkerReportsProgress = false;
            importWorker.WorkerSupportsCancellation = false;

            importWorker.RunWorkerAsync(new object[] { filePath, worksheetIndex });

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.DataManagement_Validation_BusyImportingHeader;
                _busyDialog.Description = Message.DataManagement_Validation_BusyImportingDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }


        private void ImportWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] args = (object[])e.Argument;
            String filePath = (String)args[0];
            Int32 worksheetIndex = (Int32)args[1];
            _hasOutOfMemoryException = false;
            _hasIOException = false;


            //create this as a new object so that we dont get threading issues
            ImportFileData fileData = ImportFileData.NewImportFileData();

            fileData.SourceFilePath = filePath;

            DataTable tableData = FileHelper.LoadSpecificWorksheet(filePath, worksheetIndex, /*isFirstRowHeaders*/false, ref _hasOutOfMemoryException, ref _hasIOException);

            if (tableData != null)
            {
                //setup columns
                Int32 colNumber = 1;
                foreach (DataColumn col in tableData.Columns)
                {
                    ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                    importColumn.Header = col.ColumnName;
                    importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                    fileData.Columns.Add(importColumn);

                    colNumber++;
                }

                //setup rows
                Int32 rowNumber = 1;
                foreach (DataRow row in tableData.Rows)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);
                    Int32 rowItemArrayCount = row.ItemArray.Count();

                    //cycle through adding cells
                    for (Int32 cellIndex = 0; cellIndex < rowItemArrayCount; cellIndex++)
                    {
                        ImportFileDataCell importCell =
                            ImportFileDataCell.NewImportFileDataCell(cellIndex + 1, row.ItemArray[cellIndex]);
                        importRow.Cells.Add(importCell);
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }
            }

            e.Result = fileData;
        }

        private void ImportWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker importWorker = (BackgroundWorker)sender;
            importWorker.DoWork -= ImportWorker_DoWork;
            importWorker.RunWorkerCompleted -= ImportWorker_RunWorkerCompleted;

            if (_hasOutOfMemoryException)
            {
                //Hide busy cursor if exception is thrown
                try { Mouse.OverrideCursor = null; }
                catch (InvalidOperationException) { } //stop busy cursor

                ModalMessage errorMessageBox = new ModalMessage();
                errorMessageBox.Header = Message.DataManagement_BackstageImportTitle_FileTooBig;
                errorMessageBox.Description = Message.DataManagement_BackstageFileTooLarge;
                errorMessageBox.ButtonCount = 1;
                errorMessageBox.Button1Content = Message.Generic_Ok;
                App.ShowWindow(errorMessageBox, true);
            }
            else if (_hasIOException)
            {
                //Hide busy cursor if exception is thrown
                try { Mouse.OverrideCursor = null; }
                catch (InvalidOperationException) { } //stop busy cursor

                ModalMessage errorMessageBox = new ModalMessage();
                errorMessageBox.Header = Message.DataManagement_BackstageImportTitle;
                errorMessageBox.Description = Message.DataManagement_BackstageFileInUse;
                errorMessageBox.ButtonCount = 1;
                errorMessageBox.Button1Content = Message.Generic_Ok;
                App.ShowWindow(errorMessageBox, true);
            }
            else if (e.Error != null)
            {
                //Hide busy 
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                //If any other exception is detected which is not handled
                if (AttachedControl != null)
                {
                    var errorMessageBox = new ModalMessage();
                    errorMessageBox.Header = Message.DataManagement_Import_FailHeader;
                    errorMessageBox.Description = Message.DataManagement_Import_FailMessage;
                    errorMessageBox.ButtonCount = 1;
                    errorMessageBox.Button1Content = Message.Generic_Ok;
                    errorMessageBox.ShowInTaskbar = true;
                    App.ShowWindow(errorMessageBox, true);
                }
            }
            else
            {
                //The file data was imported so we can continue
                _fileData = (ImportFileData)e.Result;
                OnImportFileDataChanged();

                //move to the next step
                this.CurrentStep = MDMImportDataStep.ColumnDefinition;
            }

            //Hide busy - queued as render of step might take a sec
            //- not sure how im getting away with this as technically the thread should be blocked?
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            _timer.Stop();
            Debug.WriteLine(String.Format("Import from excel: {0} secs", _timer.Elapsed.TotalSeconds));

        }

        #endregion

        #region Validation

        /// <summary>
        /// Starts the validation process for the given type
        /// </summary>
        /// <param name="dataType"></param>
        private void Validate(CCMDataItemType dataType, Int32 startRow)
        {
            _timer.Restart();
            Int32 entityId = App.ViewState.EntityId;

            _fileData.RaiseChangeEvents = false;

            switch (dataType)
            {

                case CCMDataItemType.Location:
                    ProcessFactory<ValidateLocationsProcess> locationProcessFactory = new ProcessFactory<ValidateLocationsProcess>();
                    locationProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationsProcess>>(OnValidationProcessCompleted);
                    locationProcessFactory.Execute(new ValidateLocationsProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.LocationClusters:
                    ProcessFactory<ValidateLocationClustersProcess> locationClustersProcessFactory = new ProcessFactory<ValidateLocationClustersProcess>();
                    locationClustersProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationClustersProcess>>(OnValidationProcessCompleted);
                    locationClustersProcessFactory.Execute(new ValidateLocationClustersProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.Product:
                    ProcessFactory<ValidateProductProcess> productProcessFactory = new ProcessFactory<ValidateProductProcess>();
                    productProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateProductProcess>>(OnValidationProcessCompleted);
                    productProcessFactory.Execute(new ValidateProductProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.MerchandisingHierarchy:
                    ProcessFactory<ValidateMerchandisingHierarchyProcess> merchHierarchyProcessFactory = new ProcessFactory<ValidateMerchandisingHierarchyProcess>();
                    merchHierarchyProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateMerchandisingHierarchyProcess>>(OnValidationProcessCompleted);
                    merchHierarchyProcessFactory.Execute(new ValidateMerchandisingHierarchyProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.LocationHierarchy:
                    ProcessFactory<ValidateLocationHierarchyProcess> locationHierarchyProcessFactory = new ProcessFactory<ValidateLocationHierarchyProcess>();
                    locationHierarchyProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationHierarchyProcess>>(OnValidationProcessCompleted);
                    locationHierarchyProcessFactory.Execute(new ValidateLocationHierarchyProcess(entityId, _fileData, startRow, false));
                    break;

                //case CCMDataItemType.LocationProductPerformance:
                //    ProcessFactory<ValidateProductPerformanceProcess> productPerformanceProcessFactory = new ProcessFactory<ValidateProductPerformanceProcess>();
                //    productPerformanceProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateProductPerformanceProcess>>(OnValidationProcessCompleted);
                //    productPerformanceProcessFactory.Execute(new ValidateProductPerformanceProcess(entityId, _fileData, startRow, false));
                //    break;

                case CCMDataItemType.LocationSpace:
                    ProcessFactory<ValidateLocationSpaceProcess> locationSpaceProcessFactory = new ProcessFactory<ValidateLocationSpaceProcess>();
                    locationSpaceProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationSpaceProcess>>(OnValidationProcessCompleted);
                    locationSpaceProcessFactory.Execute(new ValidateLocationSpaceProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.LocationSpaceBay:
                    ProcessFactory<ValidateLocationSpaceBayProcess> locationSpaceBayProcessFactory = new ProcessFactory<ValidateLocationSpaceBayProcess>();
                    locationSpaceBayProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationSpaceBayProcess>>(OnValidationProcessCompleted);
                    locationSpaceBayProcessFactory.Execute(new ValidateLocationSpaceBayProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.LocationSpaceElement:
                    ProcessFactory<ValidateLocationSpaceElementProcess> locationSpaceElementProcessFactory = new ProcessFactory<ValidateLocationSpaceElementProcess>();
                    locationSpaceElementProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationSpaceElementProcess>>(OnValidationProcessCompleted);
                    locationSpaceElementProcessFactory.Execute(new ValidateLocationSpaceElementProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.LocationProductAttributes:
                    ProcessFactory<ValidateLocationProductAttributeProcess> locationProductAttributeProcessFactory = new ProcessFactory<ValidateLocationProductAttributeProcess>();
                    locationProductAttributeProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationProductAttributeProcess>>(OnValidationProcessCompleted);
                    locationProductAttributeProcessFactory.Execute(new ValidateLocationProductAttributeProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.LocationProductIllegal:
                    ProcessFactory<ValidateLocationProductIllegalProcess> locationProductIllegalProcessFactory = new ProcessFactory<ValidateLocationProductIllegalProcess>();
                    locationProductIllegalProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationProductIllegalProcess>>(OnValidationProcessCompleted);
                    locationProductIllegalProcessFactory.Execute(new ValidateLocationProductIllegalProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.LocationProductLegal:
                    ProcessFactory<ValidateLocationProductLegalProcess> locationProductLegalProcessFactory = new ProcessFactory<ValidateLocationProductLegalProcess>();
                    locationProductLegalProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateLocationProductLegalProcess>>(OnValidationProcessCompleted);
                    locationProductLegalProcessFactory.Execute(new ValidateLocationProductLegalProcess(entityId, _fileData, startRow, false));
                    break;

                case CCMDataItemType.Assortment:
                    ProcessFactory<ValidateAssortmentProcess> ValidateAssortmentProcessFactory = new ProcessFactory<ValidateAssortmentProcess>();
                    ValidateAssortmentProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidateAssortmentProcess>>(OnValidationProcessCompleted);
                    ValidateAssortmentProcessFactory.Execute(new ValidateAssortmentProcess(entityId, _fileData, startRow, false));
                    break;

                default: throw new NotImplementedException();
            }


            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.DataManagement_Validation_BusyValidatingHeader;
                _busyDialog.Description = Message.DataManagement_Validation_BusyValidatingDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }

        }


        /// <summary>
        /// Handles the completion of the validation process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnValidationProcessCompleted<T>(object sender, ProcessCompletedEventArgs<T> e)
            where T : Galleria.Ccm.Imports.Processes.ValidateProcessBase<T>
        {
            // unsubscribe events
            ProcessFactory<T> processFactory = (ProcessFactory<T>)sender;
            processFactory.ProcessCompleted -= OnValidationProcessCompleted;

            if (e.Error != null)
            {
                if (this.AttachedControl != null)
                {
                    ModalMessage errorWindow = new ModalMessage();
                    errorWindow.Description = Message.DataManagement_Validation_FailMessage;
                    errorWindow.Header = Message.DataManagement_Validation_FailHeader;
                    errorWindow.Button1Content = Message.Generic_Ok;
                    errorWindow.ButtonCount = 1;
                    errorWindow.MessageIcon = ImageResources.ErrorCritical;
                    App.ShowWindow(errorWindow, AttachedControl, true);
                }
            }
            else
            {
                this.ValidationViewModel.LoadProcessResults(e);
            }

            _fileData.RaiseChangeEvents = true;

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            if (e.Error == null)
            {
                Int32 issueCount = this.ValidationViewModel.WorksheetErrors.SelectMany(f => f.Errors).Count();
                if (issueCount == 0)
                {
                    base.ShowWaitCursor(false);

                    if (ShowImportMessage())
                    {
                        _timer.Stop();
                        Debug.WriteLine(String.Format("Validation: {0} secs", _timer.Elapsed.TotalSeconds));

                        base.ShowWaitCursor(true);
                        //Execute the import
                        this.ImportCommand.Execute();
                    }
                    //[SA-14467]Skip straight to importing
                    //if (_validationViewModel != null)
                    //{
                    //    InsertToDatabase(this.SelectDataTypeViewModel.SelectedDataType,
                    //        this.ValidationViewModel.ValidatedFileData);
                    //}
                }
                else
                {
                    //data not valid so show the errors screen
                    this.CurrentStep = MDMImportDataStep.Validation;
                }
            }

            _timer.Stop();
            Debug.WriteLine(String.Format("Validation: {0} secs", _timer.Elapsed.TotalSeconds));
        }

        #endregion

        #region Database Insert

        /// <summary>
        /// Inserts the validated data to the database
        /// </summary>
        private void InsertToDatabase(CCMDataItemType dataType, ImportFileData importData)
        {
            _timer.Restart();
            Int32 entityId = App.ViewState.EntityId;

            switch (dataType)
            {
                case CCMDataItemType.Location:
                    ProcessFactory<ImportLocationsProcess> locationProcessFactory = new ProcessFactory<ImportLocationsProcess>();
                    locationProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationsProcess>>(OnImportProcessCompleted);
                    locationProcessFactory.Execute(new ImportLocationsProcess(entityId, importData, String.Format(Message.DataManagement_NewLocationGroup, DateTime.Now)));
                    break;

                case CCMDataItemType.Product:
                    ProcessFactory<ImportProductsProcess> productProcessFactory = new ProcessFactory<ImportProductsProcess>();
                    productProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportProductsProcess>>(OnImportProcessCompleted);
                    productProcessFactory.Execute(new ImportProductsProcess(entityId, importData));
                    break;

                case CCMDataItemType.LocationClusters:
                    ProcessFactory<ImportLocationClustersProcess> locationClustersProcessFactory = new ProcessFactory<ImportLocationClustersProcess>();
                    locationClustersProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationClustersProcess>>(OnImportProcessCompleted);
                    locationClustersProcessFactory.Execute(new ImportLocationClustersProcess(entityId, importData));
                    break;

                case CCMDataItemType.MerchandisingHierarchy:
                    ProcessFactory<ImportMerchandisingHierarchyProcess> merchHierarchyProcessFactory = new ProcessFactory<ImportMerchandisingHierarchyProcess>();
                    merchHierarchyProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportMerchandisingHierarchyProcess>>(OnImportProcessCompleted);
                    merchHierarchyProcessFactory.Execute(new ImportMerchandisingHierarchyProcess(entityId, importData));
                    break;

                case CCMDataItemType.LocationHierarchy:
                    ProcessFactory<ImportLocationHierarchyProcess> locationHierarchyProcessFactory = new ProcessFactory<ImportLocationHierarchyProcess>();
                    locationHierarchyProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationHierarchyProcess>>(OnImportProcessCompleted);
                    locationHierarchyProcessFactory.Execute(new ImportLocationHierarchyProcess(entityId, importData));
                    break;


                case CCMDataItemType.LocationSpace:
                    ProcessFactory<ImportLocationSpaceProcess> locationSpaceProcessFactory = new ProcessFactory<ImportLocationSpaceProcess>();
                    locationSpaceProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationSpaceProcess>>(OnImportProcessCompleted);
                    locationSpaceProcessFactory.Execute(new ImportLocationSpaceProcess(entityId, importData));
                    break;
                case CCMDataItemType.LocationSpaceBay:
                    ProcessFactory<ImportLocationSpaceBayProcess> locationSpaceBayProcessFactory = new ProcessFactory<ImportLocationSpaceBayProcess>();
                    locationSpaceBayProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationSpaceBayProcess>>(OnImportProcessCompleted);
                    locationSpaceBayProcessFactory.Execute(new ImportLocationSpaceBayProcess(entityId, importData));
                    break;
                case CCMDataItemType.LocationSpaceElement:
                    ProcessFactory<ImportLocationSpaceElementProcess> locationSpaceElementProcessFactory = new ProcessFactory<ImportLocationSpaceElementProcess>();
                    locationSpaceElementProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationSpaceElementProcess>>(OnImportProcessCompleted);
                    locationSpaceElementProcessFactory.Execute(new ImportLocationSpaceElementProcess(entityId, importData));
                    break;
                case CCMDataItemType.LocationProductAttributes:
                    ProcessFactory<ImportLocationProductAttributeProcess> locationProductAttributeProcessFactory = new ProcessFactory<ImportLocationProductAttributeProcess>();
                    locationProductAttributeProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationProductAttributeProcess>>(OnImportProcessCompleted);
                    locationProductAttributeProcessFactory.Execute(new ImportLocationProductAttributeProcess(entityId, importData));
                    break;

                case CCMDataItemType.LocationProductIllegal:
                    ProcessFactory<ImportLocationProductIllegalProcess> locationProductIllegalProcessFactory = new ProcessFactory<ImportLocationProductIllegalProcess>();
                    locationProductIllegalProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationProductIllegalProcess>>(OnImportProcessCompleted);
                    locationProductIllegalProcessFactory.Execute(new ImportLocationProductIllegalProcess(entityId, importData));
                    break;

                case CCMDataItemType.LocationProductLegal:
                    ProcessFactory<ImportLocationProductLegalProcess> locationProductLegalProcessFactory = new ProcessFactory<ImportLocationProductLegalProcess>();
                    locationProductLegalProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportLocationProductLegalProcess>>(OnImportProcessCompleted);
                    locationProductLegalProcessFactory.Execute(new ImportLocationProductLegalProcess(entityId, importData));
                    break;

                case CCMDataItemType.Assortment:
                    ProcessFactory<ImportAssortmentProcess> assortmentProcessFactory = new ProcessFactory<ImportAssortmentProcess>();
                    assortmentProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportAssortmentProcess>>(OnImportProcessCompleted);
                    assortmentProcessFactory.Execute(new ImportAssortmentProcess(entityId, importData));
                    break;

                default: throw new NotImplementedException();
            }

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.DataManagement_Validation_BusyUpdatingDatabaseHeader;
                _busyDialog.Description = Message.DataManagement_Validation_BusyUpdatingDatabaseDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        /// Handles the complete of the import process
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnImportProcessCompleted<T>(object sender, ProcessCompletedEventArgs<T> e)
             where T : Galleria.Ccm.Imports.Processes.ImportProcessBase<T>
        {
            // unsubscribe events
            ProcessFactory<T> processFactory = (ProcessFactory<T>)sender;
            processFactory.ProcessCompleted -= OnImportProcessCompleted;

            Int32 numRowsImported = e.Process.ImportData.Rows.Count;

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            _timer.Stop();
            Debug.WriteLine(String.Format("Import to database: {0} secs", _timer.Elapsed.TotalSeconds));

            if (e.Error != null)
            {
                if (this.AttachedControl != null)
                {
                    ModalMessage errorWindow = new ModalMessage();
                    errorWindow.Description =
                        String.Format("{0}{1}{2}",
                        Message.DataManagement_Import_FailMessage, Environment.NewLine, e.Error.Message);
                    errorWindow.Header = Message.DataManagement_Import_FailHeader;
                    errorWindow.Button1Content = Message.Generic_Ok;
                    errorWindow.ButtonCount = 1;
                    errorWindow.MessageIcon = ImageResources.ErrorCritical;
                    App.ShowWindow(errorWindow, AttachedControl, true);
                }
            }
            else
            {
                //base.ShowWaitCursor(true);

                //if any related data is being held in the viewstate then we need to update it here

                //base.ShowWaitCursor(false);

                //close this window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }

                //Fire import process complete for any listeners
                OnImportProcessComplete(numRowsImported, CCMDataItemTypeHelper.FriendlyNames[this.SelectDataTypeViewModel.SelectedDataType]);

            }
        }

        #endregion

        #region Export Errors

        private void ExportErrors(ImportFileData importData, IEnumerable<ValidationErrorGroup> errors, String fileName)
        {
            _timer.Restart();

            ProcessFactory<ExportErrorsProcess> exportProcessFactory = new ProcessFactory<ExportErrorsProcess>();
            exportProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ExportErrorsProcess>>(OnExportErrorsProcessCompleted);
            exportProcessFactory.Execute(new ExportErrorsProcess(importData, errors, fileName));

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.DataManagement_Validation_BusyExportingErrorsHeader;
                _busyDialog.Description = Message.DataManagement_Validation_BusyExportingErrorsDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        /// Handles the complete of the export errors process
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnExportErrorsProcessCompleted(object sender, ProcessCompletedEventArgs<ExportErrorsProcess> e)
        {
            // unsubscribe events
            ProcessFactory<ExportErrorsProcess> processFactory = (ProcessFactory<ExportErrorsProcess>)sender;
            processFactory.ProcessCompleted -= OnExportErrorsProcessCompleted;

            //hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }


            //show a dialog window displaying the result
            String description = null;
            String header = null;

            if (e.Error == null)
            {
                header = Message.DataManagement_ExportErrorsSuccess;
            }
            else
            {
                header = Message.DataManagement_ExportErrorsFailed;
                description = e.Error.Message;
            }


            if (this.AttachedControl != null)
            {
                ModalMessage errorWindow = new ModalMessage();
                errorWindow.Description = description;
                errorWindow.Header = header;
                errorWindow.ButtonCount = 1;
                errorWindow.Button1Content = Message.Generic_Ok;
                App.ShowWindow(errorWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #endregion

        #region Events

        /// <summary>
        /// Delclaration of ImportProcessComplete event
        /// </summary>
        public event EventHandler<ImportCompletedEventArgs> ImportProcessComplete;
        /// <summary>
        /// Method for event to fire
        /// </summary>
        public void OnImportProcessComplete(int numRowsImported, String importDataType)
        {
            if (this.ImportProcessComplete != null)
            {
                this.ImportProcessComplete(this, new ImportCompletedEventArgs(numRowsImported, importDataType));
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// ImportCompleted event args
    /// </summary>
    public sealed class ImportCompletedEventArgs : EventArgs
    {
        private int _numRowsImported;
        private String _importType;

        public int NumRowsImported
        {
            get { return _numRowsImported; }
        }

        public String ImportDataType
        {
            get { return _importType; }
        }

        public ImportCompletedEventArgs(int numRowsImported, String importDataType)
        {
            _numRowsImported = numRowsImported;
            _importType = importDataType;
        }
    }
}
