﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using System.Collections;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using System.Windows.Input;
using Csla;
using System.ComponentModel;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationSpaceBayProvider : IViewDataProvider
    {
        #region Fields
        private readonly BulkObservableCollection<ViewDataLocationSpaceBayProviderRowEntry> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationSpaceBayProviderRowEntry>();
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private Boolean _isLoadingData;
        private Object _locationId = 0;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to use
        /// </summary>
        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get
            {
                return _columnSet;
            }
        }

        public Object SelectedLocationId
        {
            get { return _locationId; }
            set
            {
                _locationId = value;
                OnPropertyChanged(IViewDataProviderHelper.SelectedLocationIdProperty.Path);

                OnSelectedLocationIdChanged(value);
            }
        }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewDataLocationSpaceBayProvider()
        {
            CreateColumns();
        }

        #endregion

        #region Event Handlers

        private void OnSelectedLocationIdChanged(Object newValue)
        {
            //reload data.
            BeginDataLoad((Int16)newValue);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the location import mapping list
            LocationSpaceBayImportMappingList mappingList =
                LocationSpaceBayImportMappingList.NewLocationSpaceBayImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(
                mappingList,
                ViewDataLocationSpaceBayProviderRowEntry.OriginalLocationSpaceBayProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationSpaceBayImportMappingList.LocationCodeMappingId, ViewDataLocationSpaceBayProviderRowEntry.LocationCodeProperty.Path},
                    {LocationSpaceBayImportMappingList.ProductGroupCodeMappingId, ViewDataLocationSpaceBayProviderRowEntry.ProductGroupCodeProperty.Path}
                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }

        }

        public void EditItem(Object selectedItem, Window sourcecontrol)
        {
            ViewDataLocationSpaceBayProviderRowEntry selectedLocationSpaceBayItem = (ViewDataLocationSpaceBayProviderRowEntry)selectedItem;
            LocationSpaceMaintenance.LocationSpaceMaintenanceOrganiser spaceBayOrganiser = new LocationSpaceMaintenance.LocationSpaceMaintenanceOrganiser();
            spaceBayOrganiser.ViewModel.OpenCommand.Execute(selectedLocationSpaceBayItem.OriginalLocationSpaceInfo.Id);
            App.ShowWindow(spaceBayOrganiser, sourcecontrol,/*isModel*/true);
        }

        public void DeleteItem(Object selectedItem)
        {
            ViewDataLocationSpaceBayProviderRowEntry dataRow = (ViewDataLocationSpaceBayProviderRowEntry)selectedItem;
            LocationSpaceBay selectedLocationSpaceBay = dataRow.OriginalLocationSpaceBay;
            Int32 bayId = selectedLocationSpaceBay.Id;
            Int32 parentGroupId = selectedLocationSpaceBay.Parent.Id;
            Int32 parentSpaceId = dataRow.OriginalLocationSpaceInfo.Id;

            if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(selectedLocationSpaceBay.ToString()))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                LocationSpace parentLocationSpace = LocationSpace.GetLocationSpaceById(parentSpaceId);
                LocationSpaceProductGroup parentGroup = parentLocationSpace.LocationSpaceProductGroups.First(p => p.Id == parentGroupId);
                LocationSpaceBay itemToDelete = parentGroup.Bays.First(b => b.Id == bayId);
                parentGroup.Bays.Remove(itemToDelete);

                //commit the delete
                try
                {
                    parentLocationSpace.Save();
                }
                catch (DataPortalException ex)
                {
                    Mouse.OverrideCursor = null;

                    LocalHelper.RecordException(ex, "DataManagement");
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.ToString(), OperationType.Delete);

                    Mouse.OverrideCursor = Cursors.Wait;
                }

                //remove the item from the data source
                _dataSourceEntries.Remove(dataRow);


                Mouse.OverrideCursor = null;

            }
        }

        private void BeginDataLoad(Int16 locationId)
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(new Object[] { App.ViewState.EntityId, locationId });
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Object[] args = (Object[])e.Argument;
            Int32 entityId = (Int32)args[0];
            Int16 locationId = (Int16)args[1];

            List<ViewDataLocationSpaceBayProviderRowEntry> rows = new List<ViewDataLocationSpaceBayProviderRowEntry>();

            LocationSpaceInfoList list = LocationSpaceInfoList.FetchByEntityId(entityId);
            if (list != null && list.Count > 0)
            {
                //create lookups
                Dictionary<Int32, String> groupCodeLookup =
                    ProductHierarchy.FetchByEntityId(App.ViewState.EntityId).EnumerateAllGroups().ToDictionary(g=>g.Id, g=> g.Code);

                //create rows
                foreach (LocationSpaceInfo info in list)
                {
                    if (info.LocationId == locationId)
                    {
                        LocationSpace locationSpace = LocationSpace.GetLocationSpaceById(info.Id);
                        foreach (LocationSpaceProductGroup productGroup in locationSpace.LocationSpaceProductGroups)
                        {
                            String groupCode = String.Empty;
                            groupCodeLookup.TryGetValue(productGroup.ProductGroupId, out groupCode);

                            foreach (LocationSpaceBay bay in productGroup.Bays)
                            {
                                rows.Add(new ViewDataLocationSpaceBayProviderRowEntry(info, groupCode, bay));
                            }

                        }
                    }
                }
            }

            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationSpaceBayProviderRowEntry> rows = e.Result as List<ViewDataLocationSpaceBayProviderRowEntry>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);
            }

            this.IsLoadingData = false;
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataLocationSpaceBayProviderRowEntry
        {
            #region Fields
            private LocationSpaceInfo _originalLocationSpaceInfo;
            private String _productGroupCode;
            private LocationSpaceBay _originalLocationSpaceBay;
            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath OriginalLocationSpaceBayProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceBayProviderRowEntry>(p => p.OriginalLocationSpaceBay);
            public static readonly PropertyPath LocationCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceBayProviderRowEntry>(p => p.LocationCode);
            public static readonly PropertyPath ProductGroupCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceBayProviderRowEntry>(p => p.ProductGroupCode);

            #endregion

            #region Properties

            /// <summary>
            /// Returns the original location space bay
            /// </summary>
            public LocationSpaceBay OriginalLocationSpaceBay
            {
                get { return _originalLocationSpaceBay; }
            }

            /// <summary>
            /// Returns the original location space info
            /// </summary>
            public LocationSpaceInfo OriginalLocationSpaceInfo
            {
                get { return _originalLocationSpaceInfo; }
            }

            /// <summary>
            /// Returns the location code
            /// </summary>
            public String LocationCode
            {
                get { return _originalLocationSpaceInfo.LocationCode; }
            }

            /// <summary>
            /// Returns the product group code
            /// </summary>
            public String ProductGroupCode
            {
                get { return _productGroupCode; }
            }


            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="constraintObject"></param>
            public ViewDataLocationSpaceBayProviderRowEntry(LocationSpaceInfo locationSpaceInfo, String productGroupCode, LocationSpaceBay bay)
            {
                _originalLocationSpaceInfo = locationSpaceInfo;
                _productGroupCode = productGroupCode;
                _originalLocationSpaceBay = bay;
            }


            #endregion

        }

        #endregion

        #region IViewDataProvider Members

        Boolean IViewDataProvider.IsDeleteSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
