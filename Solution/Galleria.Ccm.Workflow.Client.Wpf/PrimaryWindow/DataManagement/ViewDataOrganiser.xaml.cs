﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Imports;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Windows.Data;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ViewDataOrganiser.xaml
    /// </summary>
    public sealed partial class ViewDataOrganiser : ExtendedRibbonWindow
    {
        #region Fields

        //private Boolean _isEditButtonCreated = false;

        #endregion

        #region Properties

        #region ViewModel Property
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ViewDataViewModel), typeof(ViewDataOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ViewDataViewModel ViewModel
        {
            get { return (ViewDataViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ViewDataOrganiser senderControl = (ViewDataOrganiser)obj;

            if (e.OldValue != null)
            {
                ViewDataViewModel oldModel = (ViewDataViewModel)e.OldValue;
                oldModel.AttachedControl = null;

            }


            if (e.NewValue != null)
            {
                ViewDataViewModel newModel = (ViewDataViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }
        #endregion

        //#region SelectedCategory Property

        //public static readonly DependencyProperty SelectedCategoryProperty =
        //    DependencyProperty.Register("SelectedCategory", typeof(ProductGroup), typeof(ViewDataOrganiser),
        //    new PropertyMetadata(null, OnSelectedCategoryPropertyChanged));

        //public ProductGroup SelectedCategory
        //{
        //    get { return (ProductGroup)GetValue(SelectedCategoryProperty); }
        //    set { SetValue(SelectedCategoryProperty, value); }
        //}


        //private static void OnSelectedCategoryPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    ViewDataOrganiser senderControl = (ViewDataOrganiser)obj;
        //    if (senderControl.ViewModel != null)
        //    {
        //        if (e.NewValue != null && ((ProductGroup)e.NewValue).Id != senderControl.ViewModel.SelectedCategoryId)
        //        {
        //            senderControl.ViewModel.SelectedCategoryId = ((ProductGroup)e.NewValue).Id;
        //        }
        //    }
        //}

        //#endregion

        #endregion

        #region Constructor

        public ViewDataOrganiser(CCMDataItemType dataType)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //start loading
            InitializeComponent();
            this.ViewModel = new ViewDataViewModel(dataType);

            this.Loaded += new RoutedEventHandler(ViewDataOrganiser_Loaded);
        }

        private void ViewDataOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ViewDataOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        private void ExtendedDataGrid_MouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            //execute the edit command
            this.ViewModel.EditCommand.Execute();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.EditCommand.Execute();
        }
    
        private void XActionsGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;

                this.ViewModel.EditCommand.Execute(this);
                e.Handled = true;
               

            }
        }

        #endregion

        #region Window Close

        /// <summary>
        /// Calls the close command on cross clicked
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            if (this.ViewModel != null)
            {
                e.Cancel = true;
                this.ViewModel.CloseCommand.Execute();
            }
        }

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke((Action)(() =>
            {
                IDisposable disposeModel = this.ViewModel;
                this.ViewModel = null;
                if (disposeModel != null)
                {
                    disposeModel.Dispose();
                }
            }), priority: System.Windows.Threading.DispatcherPriority.Background);

        }

        #endregion
    }
}
