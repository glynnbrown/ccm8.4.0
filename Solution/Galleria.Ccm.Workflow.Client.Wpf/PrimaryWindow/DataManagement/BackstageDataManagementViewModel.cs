﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
// V8-25444 : N.Haywood
//  Added location imports
// CCM-25449 : N.Haywood
//  Added cluster locations
// CCM-25452 : N.Haywood
//  Added products
// CCM-25448 : N.Haywood
//  Added location space
// CCM-25446 : N.Haywood
//  Added Location Product Attributes
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-25455 : J.Pickup
//  Added Assortment to the mandatory list
#endregion
#region Version History: (CCM 8.0.3)
// V8-29135 : D.Pleasance
//  Implemented UpdateDataTypeStatus()
#endregion

#region Version History: (CCM 811)
// V8-30389 : M.Pettit
//  Export/Export Template messages clarified
#endregion

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Added LocationProductLegal
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using Galleria.Ccm.Imports;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Framework.Processes;
using Galleria.Ccm.Imports.Processes;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Collections.Generic;
using Galleria.Framework.Imports;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{

    /// <summary>
    /// BackstageDataManagementViewModel
    /// </summary>
    public sealed class BackstageDataManagementViewModel : ViewModelAttachedControlObject<BackstageDataManagementTabContent>
    {

        #region Fields

        private ModalBusy _busyDialog;
        //private Entity _currentEntity;
        private DataInfo _dataInfo;

        private BackstageDataManagementDataItem _selectedDataItem;
        private ObservableCollection<BackstageDataManagementDataItem> _mandatoryDataList = new ObservableCollection<BackstageDataManagementDataItem>();
        private ObservableCollection<BackstageDataManagementDataItem> _optionalDataList = new ObservableCollection<BackstageDataManagementDataItem>();
        private ObservableCollection<BackstageDataManagementDataItem> _setupEntriesList = new ObservableCollection<BackstageDataManagementDataItem>();

        #endregion

        #region Events

        #endregion

        #region Property Paths

        //Properties
        //public static PropertyPath CurrentServerAndEntityProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.CurrentServerAndEntity);
        public static PropertyPath SelectedDataItemProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.SelectedDataItem);
        public static PropertyPath MandatoryDataListProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.MandatoryDataList);
        public static PropertyPath OptionalDataListProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.OptionalDataList);
        public static PropertyPath SetupEntriesListProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.SetupEntriesList);


        //Commands
        public static PropertyPath ImportCommandProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.ImportCommand);
        public static PropertyPath ViewCommandProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.ViewCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.DeleteCommand);
        public static PropertyPath ExportCommandProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.ExportCommand);
        public static PropertyPath SaveTemplateCommandProperty = WpfHelper.GetPropertyPath<BackstageDataManagementViewModel>(p => p.SaveTemplateCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the available mandatory data items
        /// </summary>
        public ObservableCollection<BackstageDataManagementDataItem> MandatoryDataList
        {
            get { return _mandatoryDataList; }
        }

        /// <summary>
        /// Gets the available option data items
        /// </summary>
        public ObservableCollection<BackstageDataManagementDataItem> OptionalDataList
        {
            get { return _optionalDataList; }
        }

        /// <summary>
        /// Gets the available setup entries data items
        /// </summary>
        public ObservableCollection<BackstageDataManagementDataItem> SetupEntriesList
        {
            get { return _setupEntriesList; }
        }


        /// <summary>
        /// Gets/Sets the selected data item
        /// </summary>
        public BackstageDataManagementDataItem SelectedDataItem
        {
            get { return _selectedDataItem; }
            set
            {
                if (_selectedDataItem != null)
                {
                    _selectedDataItem.IsSelected = false; //unselect old
                }
                _selectedDataItem = value;
                OnPropertyChanged(SelectedDataItemProperty);
            }
        }



        #endregion

        #region Constructor

        /// <summary>
        /// constructor
        /// </summary>
        public BackstageDataManagementViewModel()
        {
            ////Get current entity
            //_currentEntity = Entity.FetchById(App.ViewState.EntityId);
            ////Get current connection
            //_currentConnection = App.SettingsState.GetCurrentConnection();

            //Setup data options
            PopulateDataOptionsList();

            //Check status for each data type
            UpdateDataTypeStatus();

            //select the first item.
            this.SelectedDataItem = this.MandatoryDataList.FirstOrDefault();
        }

        #endregion

        #region Commands

        #region ImportCommand

        private RelayCommand _importCommand;
        /// <summary>
        /// Import new data
        /// </summary>
        public RelayCommand ImportCommand
        {
            get
            {
                if (_importCommand == null)
                {
                    _importCommand = new RelayCommand(p => Import_Executed(), p => Import_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_ImportCommand,
                        FriendlyDescription = Message.DataManagement_ImportCommand_Tooltip,
                        Icon = ImageResources.DataManagement_ImportData,
                        DisabledReason = Message.DataManagement_ImportCommand_DisabledReason,
                    };
                    this.ViewModelCommands.Add(_importCommand);
                }
                return _importCommand;
            }
        }

        private bool Import_CanExecute()
        {
            if (this.SelectedDataItem != null)
            {
                //user must have permissions to create the selected data type
                Type dataType = CCMDataItemTypeHelper.GetPermissionType(this.SelectedDataItem.DataType);

                if (dataType == null)
                {
                    return false;
                }
                if ((!Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.CreateObject, dataType)) &&
                    (!Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.EditObject, dataType)))
                {
                    this.DeleteCommand.DisabledReason = Message.Generic_NoCreatePermission;
                    return false;
                }

                return this.SelectedDataItem != null && this.SelectedDataItem.IsEnabled;
            }
            return false;
        }

        private void Import_Executed()
        {
            if (this.AttachedControl != null && this.SelectedDataItem != null)
            {
                ImportDataViewModel importViewModel = new ImportDataViewModel();
                importViewModel.SelectDataTypeViewModel.SelectedDataType = this.SelectedDataItem.DataType;

                importViewModel.ImportProcessComplete += ImportDataViewModel_ImportProcessComplete;

                CommonHelper.GetWindowService().ShowDialog<ImportDataOrganiser>(importViewModel);

                importViewModel.ImportProcessComplete -= ImportDataViewModel_ImportProcessComplete;
            }

        }

        /// <summary>
        /// Responds to the notification of an import completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportDataViewModel_ImportProcessComplete(object sender, ImportCompletedEventArgs e)
        {
            if (this.AttachedControl != null)
            {
                base.ShowWaitCursor(false);
                App.ShowWindow(new ModalMessage()
                {
                    Description = String.Format(Message.DataManagement_SummaryDescription, e.NumRowsImported, e.ImportDataType),
                    Header = Message.DataManagement_SummaryTitle,
                    IsIconVisible = true,
                    MessageIcon = ImageResources.DialogInformation,
                    ButtonCount = 1,
                    Button1Content = Message.Generic_Ok,
                }, /*isModal*/true);
            }

            UpdateDataTypeStatus();
        }


        #endregion

        #region ViewCommand

        private RelayCommand _viewCommand;
        /// <summary>
        /// View imported data
        /// </summary>
        public RelayCommand ViewCommand
        {
            get
            {
                if (_viewCommand == null)
                {
                    _viewCommand = new RelayCommand(p => View_Executed(), p => View_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_ViewCommand,
                        FriendlyDescription = Message.DataManagement_ViewCommand_Tooltip,
                        Icon = ImageResources.DataManagement_ViewData,
                    };
                    this.ViewModelCommands.Add(_viewCommand);
                }
                return _viewCommand;
            }
        }

        private Boolean View_CanExecute()
        {
            if (this.SelectedDataItem != null)
            {
                //user must have permissions to view the selected data type
                Type dataType = CCMDataItemTypeHelper.GetPermissionType(this.SelectedDataItem.DataType);

                if (dataType == null)
                {
                    return false;
                }
                if (!Csla.Rules.BusinessRules.HasPermission(
                            Csla.Rules.AuthorizationActions.GetObject, dataType))
                {
                    _viewCommand.DisabledReason = Message.Generic_NoFetchPermission;
                    return false;
                }

                if (!CheckIfDataExists(this.SelectedDataItem.DataType))
                {
                    return false;
                }

                return true;
            }
            return false;
        }

        private void View_Executed()
        {
            if (this.AttachedControl != null)
            {
                ViewDataOrganiser win = new ViewDataOrganiser(this.SelectedDataItem.DataType);
                App.ShowWindow(win, /*isModal*/true);
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Delete imported data
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_DeleteCommand,
                        FriendlyDescription = Message.DataManagement_DeleteCommand_Tooltip,
                        Icon = ImageResources.DataManagementt_DeleteData,
                    };
                    this.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            if (this.SelectedDataItem != null)
            {
                //user must have permissions to delete the selected data type
                Type dataType = CCMDataItemTypeHelper.GetPermissionType(this.SelectedDataItem.DataType);

                if (dataType == null)
                {
                    return false;
                }
                if (!Csla.Rules.BusinessRules.HasPermission(
                            Csla.Rules.AuthorizationActions.DeleteObject, dataType))
                {
                    _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                    return false;
                }

                if (!CheckIfDataExists(this.SelectedDataItem.DataType))
                {
                    return false;
                }

                return true; //TEMP
            }
            return false;
        }

        private void Delete_Executed()
        {
            //Warn the user about deleting all of the data
            ModalMessage win = new ModalMessage();
            win.Header = Message.DataManagement_DeleteDataWarningHeader;
            win.Description = String.Format(Message.DataManagement_DeleteDataWarning,
                CCMDataItemTypeHelper.FriendlyNames[this.SelectedDataItem.DataType]);
            win.MessageIcon = ImageResources.Warning_32;
            win.ButtonCount = 2;
            win.Button1Content = Message.Generic_Ok;
            win.Button2Content = Message.Generic_Cancel;
            App.ShowWindow(win, /*isModal*/true);


            if (win.Result == ModalMessageResult.Button1)
            {
                //Int32? performanceSourceId = null;
                //Boolean? deleteExistingProductUniverses = null;
                Boolean canContinue = false;

                if (this.SelectedDataItem.DataType == CCMDataItemType.MerchandisingHierarchy)
                {
                    ////If there are product universes that need dealing with
                    //ProductUniverseInfoList productUniverseInfos = ProductUniverseInfoList.FetchByEntityId(App.ViewState.EntityId);
                    //if (productUniverseInfos.Count > 0)
                    //{
                    //    //Show dialog asking whether to delete all or reassign.
                    //    ModalMessageResult returnAction = ModalMessageResult.Button1;

                    //    ModalMessage dialog = new ModalMessage()
                    //    {
                    //        MessageIcon = ImageResources.Warning_32,
                    //        Header = Message.DataManagement_DeleteExistingProductUniverse_Header,
                    //        ButtonCount = 2,
                    //        Button1Content = Message.Generic_Delete,
                    //        Button2Content = Message.Generic_Update,
                    //        Description = Message.DataManagement_DeleteExistingProductUniverse_Description
                    //    };
                    //    App.ShowWindow(dialog, true);
                    //    returnAction = dialog.Result;

                    //    //process user input
                    //    switch (returnAction)
                    //    {
                    //        case ModalMessageResult.Button1:
                    //            deleteExistingProductUniverses = true;
                    //            break;
                    //        case ModalMessageResult.Button2:
                    //            deleteExistingProductUniverses = false;
                    //            break;
                    //        case ModalMessageResult.CrossClicked:
                    //        default:
                    //            deleteExistingProductUniverses = null;
                    //            break;
                    //    }
                    //}
                    canContinue = true;
                }
                else
                {
                    canContinue = true;
                }

                if (canContinue)
                {
                    //execute the delete process
                    DeleteDataProcess deleteProcess = new DeleteDataProcess(this.SelectedDataItem.DataType, App.ViewState.EntityId/*, performanceSourceId, deleteExistingProductUniverses*/);
                    ProcessFactory<DeleteDataProcess> deleteProcessFactory = new ProcessFactory<DeleteDataProcess>();
                    deleteProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<DeleteDataProcess>>(OnDeleteDataProcessCompleted);
                    deleteProcessFactory.Execute(deleteProcess);


                    //show a busy dialog
                    if (this.AttachedControl != null)
                    {
                        _busyDialog = new ModalBusy();
                        _busyDialog.Header = Message.DataManagement_BusyDeletingHeader;
                        _busyDialog.Description = Message.DataManagement_BusyDeletingDescription;
                        _busyDialog.IsDeterminate = false;
                        App.ShowWindow(_busyDialog, /*isModal*/true);
                    }
                }
            }
        }

        /// <summary>
        /// Delete data process completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDeleteDataProcessCompleted(object sender, ProcessCompletedEventArgs<DeleteDataProcess> e)
        {
            // unsubscribe events & hdie busy service
            ProcessFactory<DeleteDataProcess> processFactory = (ProcessFactory<DeleteDataProcess>)sender;
            processFactory.ProcessCompleted -= OnDeleteDataProcessCompleted;

            //hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            if (e.Error != null)
            {
                if (this.AttachedControl != null)
                {
                    App.ShowWindow(
                        new ModalMessage()
                        {
                            Description = Message.DataManagement_DeleteErrorDescription,
                            Header = String.Format(Message.DataManagement_DeleteErrorHeader, e.Error.Message),
                            ButtonCount = 1,
                            Button1Content = Message.Generic_Ok,
                        },  /*isModal*/true);
                }
            }
            else
            {
                //carry out any required post processing here

                UpdateDataTypeStatus();
            }
        }



        #endregion

        #region ExportCommand

        private RelayCommand _exportCommand;
        /// <summary>
        /// Export imported data
        /// </summary>
        public RelayCommand ExportCommand
        {
            get
            {
                if (_exportCommand == null)
                {
                    _exportCommand = new RelayCommand(p => Export_Executed(), p => Export_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_ExportCommand,
                        FriendlyDescription = Message.DataManagement_ExportCommand_Tooltip,
                        Icon = ImageResources.DataManagement_ExportData,
                    };
                    this.ViewModelCommands.Add(_exportCommand);
                }
                return _exportCommand;
            }
        }

        private Boolean Export_CanExecute()
        {
            if (this.SelectedDataItem != null)
            {
                //user must have permissions to view the selected data type
                Type dataType = CCMDataItemTypeHelper.GetPermissionType(this.SelectedDataItem.DataType);

                if (dataType == null)
                {
                    return false;
                }
                if (!Csla.Rules.BusinessRules.HasPermission(
                            Csla.Rules.AuthorizationActions.GetObject, dataType))
                {
                    _deleteCommand.DisabledReason = Message.Generic_NoFetchPermission;
                    return false;
                }

                if (!CheckIfDataExists(this.SelectedDataItem.DataType))
                {
                    return false;
                }

                return true;
            }
            return false;
        }

        private void Export_Executed()
        {
            SelectExportToFile(/*isHeadersOnly*/false);
        }

        #endregion

        #region SaveTemplateCommand

        private RelayCommand _saveTemplateCommand;
        /// <summary>
        /// Save a copy of import data template
        /// </summary>
        public RelayCommand SaveTemplateCommand
        {
            get
            {
                if (_saveTemplateCommand == null)
                {
                    _saveTemplateCommand = new RelayCommand(p => SaveTemplate_Executed(), p => SaveTemplate_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_SaveTemplate,
                        FriendlyDescription = Message.DataManagement_SaveTemplate_Tooltip,
                        Icon = ImageResources.DataManagement_SaveImportTemplate
                    };
                    this.ViewModelCommands.Add(_saveTemplateCommand);
                }
                return _saveTemplateCommand;
            }
        }

        private Boolean SaveTemplate_CanExecute()
        {
            return (this.SelectedDataItem != null);
        }

        private void SaveTemplate_Executed()
        {
            SelectExportToFile(/*isHeadersOnly*/true);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Method to populate the data options list
        /// </summary>
        private void PopulateDataOptionsList()
        {
            //Setup data list items - put everything as mandatory for now.
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_MerchandisingHeirarchy_Title, Message.DataManagement_MerchandisingHeirarchy_Description, CCMDataItemType.MerchandisingHierarchy, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationHeirarchy_Title, Message.DataManagement_LocationHeirarchy_Description, CCMDataItemType.LocationHierarchy, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_Location_Title, Message.DataManagement_Location_Description, CCMDataItemType.Location, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_Product_Title, Message.DataManagement_Product_Description, CCMDataItemType.Product, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_Assortment_Title, Message.DataManagement_Assortment_Description, CCMDataItemType.Assortment, ImageResources.DataManagement_OKState));

            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationSpecificCategorySpace_Title, Message.DataManagement_LocationSpecificCategorySpace_Description, CCMDataItemType.LocationSpace, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationSpecificCategoryBaySpace_Title, Message.DataManagement_LocationSpecificCategoryBaySpace_Description, CCMDataItemType.LocationSpaceBay, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationSpecificCategoryElementSpace_Title, Message.DataManagement_LocationSpecificCategoryElementSpace_Description, CCMDataItemType.LocationSpaceElement, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationClusters_Title, Message.DataManagement_LocationClusters_Description, CCMDataItemType.LocationClusters, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationProductAttribute_Title, Message.DataManagement_LocationProductAttribute_Description, CCMDataItemType.LocationProductAttributes, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationProductIllegal_Title, Message.DataManagement_LocationProductIllegal_Description, CCMDataItemType.LocationProductIllegal, ImageResources.DataManagement_OKState));
            _mandatoryDataList.Add(new BackstageDataManagementDataItem(Message.DataManagement_LocationProductLegal_Title, Message.DataManagement_LocationProductLegal_Description, CCMDataItemType.LocationProductLegal, ImageResources.DataManagement_OKState));

        }


        /// <summary>
        /// Called by the attached control
        /// Will update the data type statuses but only if the last update was over 5 mins ago.
        /// </summary>
        internal void ReloadRefreshDataTypeStatus()
        {
            if (_dataInfo.DateCreated < DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0)))
            {
                UpdateDataTypeStatus();
            }
        }


        /// <summary>
        /// Method to update each of the data type status'
        /// </summary>
        /// <param name="info">Information about the data in the database</param>
        private void UpdateDataTypeStatus()
        {
            //update the data info.
            _dataInfo = DataInfo.FetchDataInfo(App.ViewState.EntityId);

            #region Mandatory Data
            // enumerate through data types
            foreach (BackstageDataManagementDataItem dataItem in this.MandatoryDataList)
            {
                switch (dataItem.DataType)
                {
                    case (CCMDataItemType.MerchandisingHierarchy):
                        //data item should always be enabled
                        dataItem.IsEnabled = true;
                        //set icon to be error / ok dependent on whether merchandising hierarchy exists or not
                        dataItem.StateIcon = (_dataInfo.HasProductHierarchy) ? ImageResources.DataManagement_OKState : ImageResources.DataManagement_ErrorState;
                        //update has errors property
                        dataItem.HasErrors = (_dataInfo.HasProductHierarchy) ? false : true;
                        break;

                    case (CCMDataItemType.LocationHierarchy):
                        //data item should always be enabled
                        dataItem.IsEnabled = true;
                        //set icon to be error / ok dependent on whether location hierarchy exists or not
                        dataItem.StateIcon = (_dataInfo.HasLocationHierarchy) ? ImageResources.DataManagement_OKState : ImageResources.DataManagement_ErrorState;
                        //update has errors property
                        dataItem.HasErrors = (_dataInfo.HasLocationHierarchy) ? false : true;
                        break;

                    case (CCMDataItemType.Location):
                        //data item should always be enabled
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasLocation)
                        {
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                            dataItem.HasErrors = false;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocationHierarchy) ? true : false;
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.Product):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasProduct)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasProductHierarchy);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;

                        }
                        break;
                    case (CCMDataItemType.Assortment):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasAssortment)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasProductHierarchy);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.LocationSpace):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasLocationSpace)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocation && _dataInfo.HasProductHierarchy);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.LocationSpaceBay):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasLocationSpaceBay)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocationSpace);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.LocationSpaceElement):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasLocationSpaceElement)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocationSpace);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.LocationClusters):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasClusterScheme)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocation);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.LocationProductAttributes):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasLocationProductAttribute)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocation && _dataInfo.HasProduct);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.LocationProductIllegal):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasLocationProductIllegal)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocation && _dataInfo.HasProduct);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                    case (CCMDataItemType.LocationProductLegal):
                        dataItem.IsEnabled = true;
                        if (_dataInfo.HasLocationProductLegal)
                        {
                            dataItem.HasErrors = false;
                            dataItem.StateIcon = ImageResources.DataManagement_OKState;
                        }
                        else
                        {
                            dataItem.IsEnabled = (_dataInfo.HasLocation && _dataInfo.HasProduct);
                            dataItem.StateIcon = ImageResources.DataManagement_ErrorState;
                            dataItem.HasErrors = true;
                        }
                        break;
                }
            }
            #endregion
        }

        /// <summary>
        /// Method to check if data exists for a specified data type
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        private Boolean CheckIfDataExists(CCMDataItemType dataType)
        {
            bool returnValue = false;

            //only update if null as this is called by canexecutes.
            if (_dataInfo == null)
            {
                _dataInfo = DataInfo.FetchDataInfo(App.ViewState.EntityId);
            }

            switch (dataType)
            {
                case (CCMDataItemType.MerchandisingHierarchy):
                    returnValue = _dataInfo.HasProductHierarchy;
                    break;
                case (CCMDataItemType.LocationHierarchy):
                    returnValue = _dataInfo.HasLocationHierarchy;
                    break;
                case (CCMDataItemType.Location):
                    returnValue = _dataInfo.HasLocation;
                    break;
                case (CCMDataItemType.Product):
                    returnValue = _dataInfo.HasProduct;
                    break;
                case (CCMDataItemType.Assortment):
                    returnValue = _dataInfo.HasAssortment;
                    break;
                case (CCMDataItemType.LocationSpace):
                    returnValue = _dataInfo.HasLocationSpace;
                    break;
                case (CCMDataItemType.LocationSpaceBay):
                    returnValue = _dataInfo.HasLocationSpaceBay;
                    break;
                case (CCMDataItemType.LocationSpaceElement):
                    returnValue = _dataInfo.HasLocationSpaceElement;
                    break;
                case (CCMDataItemType.LocationClusters):
                    returnValue = _dataInfo.HasClusterScheme;
                    break;
                case (CCMDataItemType.LocationProductAttributes):
                    returnValue = _dataInfo.HasLocationProductAttribute;
                    break;
                case (CCMDataItemType.LocationProductIllegal):
                    returnValue = _dataInfo.HasLocationProductIllegal;
                    break;
                case (CCMDataItemType.LocationProductLegal):
                    returnValue = _dataInfo.HasLocationProductLegal;
                    break;
            }

            return returnValue;
        }

        #region ExportProcess

        /// <summary>
        /// Shows the dialogs to select to file to export to
        /// </summary>
        private void SelectExportToFile(Boolean isHeadersOnly)
        {
            if (this.AttachedControl != null)
            {
                //get the filename to save as
                Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
                saveDialog.CreatePrompt = false;
                saveDialog.Filter = Message.DataManagement_ExportFilter;
                saveDialog.FilterIndex = 2;
                saveDialog.FileName = CCMDataItemTypeHelper.FriendlyNames[this.SelectedDataItem.DataType];
                saveDialog.OverwritePrompt = true;

                if (isHeadersOnly == true)
                {
                    saveDialog.FileName = 
                        String.Format("{0} {1}", 
                        CCMDataItemTypeHelper.FriendlyNames[this.SelectedDataItem.DataType],  Message.Template);
                }
                else
                {
                    saveDialog.FileName = CCMDataItemTypeHelper.FriendlyNames[this.SelectedDataItem.DataType];
                }

                if (saveDialog.ShowDialog() == true)
                {
                    Boolean canContinue = true;


                    //If the selected file type is XLS
                    if (saveDialog.FilterIndex == 1)
                    {
                        //Show warning dialog informing user that if > 65k rows, they will not be outputted
                        //as it exceeds this file type row limit.
                        ModalMessage modelMessageWindow = new ModalMessage();
                        modelMessageWindow.Description = Message.DataManagement_RowNumberExceedsMaxLimitWarning;
                        modelMessageWindow.ButtonCount = 2;
                        modelMessageWindow.Button1Content = Message.Generic_Ok;
                        modelMessageWindow.Button2Content = Message.Generic_Cancel;
                        modelMessageWindow.Icon = ImageResources.Warning_32;
                        App.ShowWindow(modelMessageWindow, true);

                        //set the result
                        canContinue = (modelMessageWindow.Result == ModalMessageResult.Button1);
                    }


                    if (canContinue)
                    {
                        String fileName = saveDialog.FileName;

                        //Try to delete existing
                        if (System.IO.File.Exists(fileName))
                        {
                            canContinue = FileHelper.DeleteExistingFile(fileName);
                        }

                        if (canContinue)
                        {
                            Export(this.SelectedDataItem.DataType, fileName, isHeadersOnly);
                        }

                    }
                }
            }
        }

        private void Export(CCMDataItemType type, String fileName, Boolean isHeadersOnly)
        {
            Boolean processStarted = true;
            Int32 entityId = App.ViewState.EntityId;

            //start the process
            switch (type)
            {

                case CCMDataItemType.Location:
                    ExportLocationsProcess locationProcess = new ExportLocationsProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationsProcess> locationProcessFactory = new ProcessFactory<ExportLocationsProcess>();
                    locationProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationProcessFactory.Execute(locationProcess);
                    break;

                case CCMDataItemType.Product:
                    ExportProductsProcess productProcess = new ExportProductsProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportProductsProcess> productProcessFactory = new ProcessFactory<ExportProductsProcess>();
                    productProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    productProcessFactory.Execute(productProcess);
                    break;

                case CCMDataItemType.LocationClusters:
                    ExportLocationClustersProcess locationClustersProcess = new ExportLocationClustersProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationClustersProcess> locationClusterProcessFactory = new ProcessFactory<ExportLocationClustersProcess>();
                    locationClusterProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationClusterProcessFactory.Execute(locationClustersProcess);
                    break;

                case CCMDataItemType.MerchandisingHierarchy:
                    ExportMerchHierarchyProcess merchHierarchyProcess = new ExportMerchHierarchyProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportMerchHierarchyProcess> merchHierarchyProcessFactory = new ProcessFactory<ExportMerchHierarchyProcess>();
                    merchHierarchyProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    merchHierarchyProcessFactory.Execute(merchHierarchyProcess);
                    break;

                case CCMDataItemType.LocationHierarchy:
                    ExportLocationHierarchyProcess locationHierarchyProcess = new ExportLocationHierarchyProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationHierarchyProcess> locationHierarchyProcessFactory = new ProcessFactory<ExportLocationHierarchyProcess>();
                    locationHierarchyProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationHierarchyProcessFactory.Execute(locationHierarchyProcess);
                    break;


                case CCMDataItemType.LocationSpace:
                    ExportLocationSpaceProcess locationSpaceProcess = new ExportLocationSpaceProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationSpaceProcess> locationSpaceProcessFactory = new ProcessFactory<ExportLocationSpaceProcess>();
                    locationSpaceProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationSpaceProcessFactory.Execute(locationSpaceProcess);
                    break;

                case CCMDataItemType.LocationSpaceBay:
                    ExportLocationSpaceBayProcess locationSpaceBayProcess = new ExportLocationSpaceBayProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationSpaceBayProcess> locationSpaceBayProcessFactory = new ProcessFactory<ExportLocationSpaceBayProcess>();
                    locationSpaceBayProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationSpaceBayProcessFactory.Execute(locationSpaceBayProcess);
                    break;

                case CCMDataItemType.LocationSpaceElement:
                    ExportLocationSpaceElementProcess locationSpaceElementProcess = new ExportLocationSpaceElementProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationSpaceElementProcess> locationSpaceElementProcessFactory = new ProcessFactory<ExportLocationSpaceElementProcess>();
                    locationSpaceElementProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationSpaceElementProcessFactory.Execute(locationSpaceElementProcess);
                    break;

                case CCMDataItemType.LocationProductAttributes:
                    ExportLocationProductAttributeProcess locationProductAttributeProcess = new ExportLocationProductAttributeProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationProductAttributeProcess> locationProductAttributeProcessFactory = new ProcessFactory<ExportLocationProductAttributeProcess>();
                    locationProductAttributeProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationProductAttributeProcessFactory.Execute(locationProductAttributeProcess);
                    break;

                case CCMDataItemType.LocationProductIllegal:
                    ExportLocationProductIllegalProcess locationProductIllegalProcess = new ExportLocationProductIllegalProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationProductIllegalProcess> locationProductIllegalProcessFactory = new ProcessFactory<ExportLocationProductIllegalProcess>();
                    locationProductIllegalProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationProductIllegalProcessFactory.Execute(locationProductIllegalProcess);
                    break;

                case CCMDataItemType.LocationProductLegal:
                    ExportLocationProductLegalProcess locationProductLegalProcess = new ExportLocationProductLegalProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportLocationProductLegalProcess> locationProductLegalProcessFactory = new ProcessFactory<ExportLocationProductLegalProcess>();
                    locationProductLegalProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    locationProductLegalProcessFactory.Execute(locationProductLegalProcess);
                    break;

                case CCMDataItemType.Assortment:
                    ExportAssortmentProcess assortmentProcess = new ExportAssortmentProcess(entityId, fileName, isHeadersOnly);
                    ProcessFactory<ExportAssortmentProcess> assortmentProcessFactory = new ProcessFactory<ExportAssortmentProcess>();
                    assortmentProcessFactory.ProcessCompleted += OnExportProcessCompleted;
                    assortmentProcessFactory.Execute(assortmentProcess);
                    break;


                default: throw new NotImplementedException();
            }

            //show a busy dialog
            if (this.AttachedControl != null && processStarted)
            {
                _busyDialog = new ModalBusy();
                if (isHeadersOnly)
                {
                    //we are exporting a template with no data
                    _busyDialog.Header = Message.DataManagement_BusyExportingTemplateHeader;
                    _busyDialog.Description = Message.DataManagement_BusyExportingTemplateDescription;
                }
                else
                {
                    //we are exporting a full data set
					_busyDialog.Header = Message.DataManagement_BusyExportingHeader;
                    _busyDialog.Description = Message.DataManagement_BusyExportingDescription;
                }

                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        /// Handles the completion of the export process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnExportProcessCompleted<T>(Object sender, ProcessCompletedEventArgs<T> e)
            where T : Galleria.Ccm.Imports.Processes.ExportProcessBase<T>
        {
            // unsubscribe events
            ProcessFactory<T> processFactory = (ProcessFactory<T>)sender;
            processFactory.ProcessCompleted -= OnExportProcessCompleted;

            // check which export just finished (template or data)
            Boolean isTemplateExport = false;
            //hide busy
            if (_busyDialog != null)
            {
                // if template export set template export flag correctly
                if (_busyDialog.Header == Message.DataManagement_BusyExportingTemplateHeader)
                {
                    isTemplateExport = true;
                }
                _busyDialog.Close();
                _busyDialog = null;
            }


            //show a dialog window displaying the result
            String title = null;
            String description = null;
            String header = null;

            if (e.Error == null)
            {
                //title = isTemplateExport ? Message.DataManagement_ExportTemplateSuccess : Message.DataManagement_ExportSuccess;
                header = isTemplateExport ? Message.DataManagement_ExportTemplateSuccess : Message.DataManagement_ExportSuccess;
                description = isTemplateExport ? Message.DataManagement_ExportTemplateSuccessDescription : Message.DataManagement_ExportSuccessDescription;
            }
            else
            {
                //title = Message.DataManagement_ExportFailedTitle;
                header = Message.DataManagement_ExportFailed;
                description = String.Format(Message.DataManagement_ExportFailedDescription, e.Error.Message);
            }

            if (this.AttachedControl != null)
            {
                ModalMessage errorWindow = new ModalMessage();
                //errorWindow.Title = title;
                errorWindow.Description = description;
                errorWindow.Header = header;
                errorWindow.ButtonCount = 1;
                errorWindow.Button1Content = Message.Generic_Ok;
                App.ShowWindow(errorWindow, /*isModal*/true);
            }
        }


        #endregion

        #endregion

        #region Dispose

        /// <summary>
        /// Handles an IDisposable dispose call
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(Boolean disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    //TODO
                }
                this.IsDisposed = true;
            }
        }

        #endregion

    }

}
