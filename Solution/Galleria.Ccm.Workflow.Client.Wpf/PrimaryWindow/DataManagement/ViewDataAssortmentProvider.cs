﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (GFS 1.0)
// GFS-25455 : J.Pickup
//  Created
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivalents.
#endregion

#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion


using System;
using System.Linq;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Collections.ObjectModel;
using System.Collections;
using System.Globalization;
using System.Windows;
using Galleria.Framework.Helpers;
using System.ComponentModel;
using System.Windows.Input;
using Csla.Server;
using System.Diagnostics;
using Galleria.Ccm.Imports.Mappings;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup;
using Galleria.Framework.Planograms.Model;


namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataAssortmentProvider: IViewDataProvider
    {
        #region Fields
        private const Int32 _maxReturnedRecords = 1000;
        private Int32 _currentFirstRecord;
        private Int32 _currentLastRecord;
        private List<Int32> _assortmentIdList = new List<Int32>();
        private Dictionary<Int32, String> _groupIds = new Dictionary<Int32, String>();
        private Boolean _previousEnabled = false;
        private Boolean _nextEnabled = false;

        private AssortmentInfoList _assortmentList;
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private ObservableCollection<ViewDataAssortmentRow> _dataSourceEntries = new ObservableCollection<ViewDataAssortmentRow>();
        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to use
        /// </summary>
        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Returns true if the edit command is supported
        /// </summary>
        public Boolean IsEditSupported { get; private set; }

        /// <summary>
        /// Returns true if the delete command is supported
        /// </summary>
        public Boolean IsDeleteSupported { get; private set; }

       
        public String ProductSetPosition
        {
            get 
            //{ return String.Format(CultureInfo.CurrentCulture,
            //    Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.BackstageDataManagement_ViewData_ProductsSetPosition,
            //    _currentFirstRecord + 1, _currentLastRecord + 1, _gtinList.Count); 
            {
                return "TODO: Not yet implemented!";
            }
        }

        public Boolean NextEnabled
        {
            get { return _nextEnabled; }
        }

        public Boolean PreviousEnabled
        {
            get { return _previousEnabled; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewDataAssortmentProvider()
        {
            this.IsEditSupported = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.EditObject, typeof(Assortment));
            this.IsDeleteSupported = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.DeleteObject, typeof(Assortment));

            #region Set Assortment List
            //Use info list to check if over max returned record limit, then retrive only max amout of records
            //using entityid/gtin fetch
            List<Int32> productIds = new List<Int32>();
            _assortmentIdList = AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId).Select(p => p.Id).ToList();
            //check if more than _maxReturnedRecords records exist
            if (_assortmentIdList.Count > _maxReturnedRecords)
            {
                //set productGTINs
                for (Int16 i = 0; i < _maxReturnedRecords; i++)
                {
                    productIds.Add(_assortmentIdList[i]);
                }
                //Populate product list
                _assortmentList = AssortmentInfoList.FetchByEntityIdAssortmentIds(App.ViewState.EntityId, productIds);

                //clear gtin list
                productIds.Clear();

                //Set last record number
                _currentFirstRecord = 0;
                _currentLastRecord = _maxReturnedRecords - 1;

                _previousEnabled = false;
                _nextEnabled = true;
            }
            else
            {
                //Fetch all as less than max records exist
                _assortmentList = AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId);
                //Set last record number
                _currentFirstRecord = 0;
                _currentLastRecord = _assortmentIdList.Count - 1;
            }
            #endregion


            #region Populate View Data Rows

            //Create view data rows (one for each child assortment product within GFS. Using info as returns product group name?
            foreach (AssortmentInfo assortment in _assortmentList)
            {
                Assortment currentAssortment = Assortment.GetById(assortment.Id);
                foreach (AssortmentProduct currentProduct in currentAssortment.Products)
                {
                    ViewDataAssortmentRow entry =
                    new ViewDataAssortmentRow(assortment, currentProduct);
                    _dataSourceEntries.Add(entry);
                }
                
            } 
            #endregion

           

            CreateColumns();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }


            AssortmentImportMappingList mappingList =
                AssortmentImportMappingList.NewAssortmentImportMappingList();


            List<DataGridColumn> columns = DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList);
            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }
        }

        public void EditItem(object selectedItem, Window sourcecontrol)
        {
            ViewDataAssortmentRow assortmentRow = selectedItem as ViewDataAssortmentRow;
            if (assortmentRow != null)
            {
                //Assortment assortment = Assortment.GetById(assortmentRow.AssortmentRow.Id);

                AssortmentSetupOrganiser assortmentWindow = new AssortmentSetupOrganiser();
                assortmentWindow.ViewModel.OpenCommand.Execute(assortmentRow.AssortmentRow.Id);

                App.ShowWindow(assortmentWindow,sourcecontrol, false); 
            }
        }

        public void DeleteItem(object selectedItem)
        {
            ViewDataAssortmentRow dataRow = (ViewDataAssortmentRow)selectedItem;
            AssortmentInfo toDelete = dataRow.AssortmentRow;

            if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(toDelete.ToString()))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                //commit the delete
                try
                {
                    Assortment itemToDelete = Assortment.GetById(toDelete.Id);
                    itemToDelete.Delete();
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    Mouse.OverrideCursor = null;

                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(toDelete.Name, OperationType.Delete);

                    Mouse.OverrideCursor = Cursors.Wait;
                }

                //remove the item from the data source
                _dataSourceEntries.Remove(dataRow);


                Mouse.OverrideCursor = null;
            }
        }


        public void DataSource_Next()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            //check if the end of the list will be reached
            Int32 firstRecord = _currentLastRecord + 1;
            Int32 lastRecord = firstRecord + _maxReturnedRecords;

            if (lastRecord > _assortmentList.Count)
            {
                lastRecord = _assortmentList.Count;
            }

            List<Int32> assortmentIds = new List<Int32>();
            for (int i = firstRecord; i < lastRecord; i++)
            {
                assortmentIds.Add(_assortmentIdList[i]);
            }
            _assortmentList.Clear();
            _assortmentList = AssortmentInfoList.FetchByEntityIdAssortmentIds(App.ViewState.EntityId, assortmentIds);

            #region Populate View Data Rows
            //Create view data rows with correct group code
            _dataSourceEntries.Clear();

            //Create view data rows (one for each child assortment product within GFS. Using info as returns product group name?
            foreach (AssortmentInfo assortment in _assortmentList)
            {
                Assortment currentAssortment = Assortment.GetById(assortment.Id);
                foreach (AssortmentProduct currentProduct in currentAssortment.Products)
                {
                    ViewDataAssortmentRow entry =
                    new ViewDataAssortmentRow(assortment, currentProduct);
                    _dataSourceEntries.Add(entry);
                }

            } 

            #endregion

            _previousEnabled = firstRecord == 0 ? false : true;
            _nextEnabled = lastRecord < assortmentIds.Count ? true : false;

            _currentFirstRecord = firstRecord;
            _currentLastRecord = lastRecord - 1;
            timer.Stop();
            Debug.WriteLine("return assortments stopped - " + timer.Elapsed);
        }

        public void DataSource_Previous()
        {
            Int32 lastRecord = _currentFirstRecord;
            Int32 firstRecord = lastRecord - _maxReturnedRecords;

            if (firstRecord < 0)
            {
                firstRecord = 0;
                lastRecord = firstRecord + _maxReturnedRecords;
            }

            List<Int32> assortmentIds = new List<Int32>();
            for (int i = firstRecord; i < lastRecord; i++)
            {
                assortmentIds.Add(_assortmentIdList[i]);
            }
            _assortmentList.Clear();
            _assortmentList = AssortmentInfoList.FetchByEntityIdAssortmentIds(App.ViewState.EntityId, assortmentIds);

            #region Populate View Data Rows
            //Create view data rows with correct group code
            _dataSourceEntries.Clear();

            //Create view data rows (one for each child assortment product within GFS. Using info as returns product group name?
            foreach (AssortmentInfo assortment in _assortmentList)
            {
                Assortment currentAssortment = Assortment.GetById(assortment.Id);
                foreach (AssortmentProduct currentProduct in currentAssortment.Products)
                {
                    ViewDataAssortmentRow entry =
                    new ViewDataAssortmentRow(assortment, currentProduct);
                    _dataSourceEntries.Add(entry);
                }

            } 

            #endregion

            _previousEnabled = firstRecord == 0 ? false : true;
            _nextEnabled = lastRecord < _assortmentIdList.Count ? true : false;

            _currentFirstRecord = firstRecord;
            _currentLastRecord = lastRecord - 1;
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataAssortmentRow
        {
            #region Fields

            private AssortmentInfo _assortment;
            private AssortmentProduct _assortmentProduct;

            #endregion

            #region BindingPropertyPaths

            public static readonly PropertyPath ProductRowProperty = WpfHelper.GetPropertyPath<ViewDataAssortmentRow>(c => c.AssortmentRow);

            #endregion

            #region Properties

            public AssortmentInfo AssortmentRow
            {
                get { return _assortment; }
            }

            public String Name
            {
                get { return _assortment.Name; }
            }

            public String Gtin
            {
                get { return this._assortmentProduct.Gtin; }
            }

            public Boolean IsRanged
            {
                get { return _assortmentProduct.IsRanged; }
            }

            public Int32 Rank
            {
                get { return _assortmentProduct.Rank; }
            }

            public Byte Facings
            {
                get { return _assortmentProduct.Facings; }
            }

            public Int16 Units
            {
                get { return _assortmentProduct.Units; }
            }

            public String Segmentation
            {
                get { return _assortmentProduct.Segmentation; }
            }

            public String ProductTreatmentType
            {
                get { return PlanogramAssortmentProductTreatmentTypeHelper.FriendlyNames[_assortmentProduct.ProductTreatmentType]; }
            }

            public String Comments
            {
                get { return _assortmentProduct.Comments; }
            }

            public String ProductGroupId
            {
                get { return _assortment.ProductGroupCode; }
            }


            #endregion

            #region Constructor

            public ViewDataAssortmentRow(AssortmentInfo assortment, AssortmentProduct assortmentProduct)
            {
                _assortment = assortment;
                _assortmentProduct = assortmentProduct;
            }

            #endregion
        }

        #endregion

        #region IViewDataProvider members

        Boolean IViewDataProvider.IsDeleteSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLoadingData
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if(this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
        
    }

   
}
