﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for BackstageDataManagementTabContent.xaml
    /// </summary>
    public partial class BackstageDataManagementTabContent : UserControl
    {
        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstageDataManagementViewModel), typeof(BackstageDataManagementTabContent),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public BackstageDataManagementViewModel ViewModel
        {
            get { return (BackstageDataManagementViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageDataManagementTabContent senderControl = (BackstageDataManagementTabContent)obj;

            if (e.OldValue != null)
            {
                BackstageDataManagementViewModel oldModel = (BackstageDataManagementViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                BackstageDataManagementViewModel newModel = (BackstageDataManagementViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BackstageDataManagementTabContent()
        {
            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageDataManagement);

            this.Loaded += new RoutedEventHandler(BackstageDataManagementTabContent_Loaded);
            this.IsVisibleChanged += BackstageDataManagementTabContent_IsVisibleChanged;
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when this becomes visible for the first time.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackstageDataManagementTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible)
            {
                if (this.ViewModel == null)
                {
                    this.ViewModel = new BackstageDataManagementViewModel();
                }
                this.IsVisibleChanged -= BackstageDataManagementTabContent_IsVisibleChanged;
            }
        }

        /// <summary>
        /// Called whenever this is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackstageDataManagementTabContent_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel == null) return;

            //update the type status on a delay each time this is accessed
            this.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    this.ViewModel.ReloadRefreshDataTypeStatus();

                }), priority: System.Windows.Threading.DispatcherPriority.ContextIdle);
        }

        #endregion
    }
}
