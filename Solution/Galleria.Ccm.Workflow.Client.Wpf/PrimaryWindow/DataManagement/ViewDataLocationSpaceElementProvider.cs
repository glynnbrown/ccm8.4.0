﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using System.Collections;
using Galleria.Ccm.Imports.Mappings;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using System.Windows.Input;
using Csla;
using System.ComponentModel;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationSpaceElementProvider : IViewDataProvider
    {
        #region Fields
        private readonly BulkObservableCollection<ViewDataLocationSpaceElementProviderRowEntry> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationSpaceElementProviderRowEntry>();
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private Boolean _isLoadingData;
        private Object _locationId = 0;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to use
        /// </summary>
        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        public Object SelectedLocationId
        {
            get { return _locationId; }
            set
            {
                _locationId = value;
                OnPropertyChanged(IViewDataProviderHelper.SelectedLocationIdProperty.Path);

                OnSelectedLocationIdChanged(value);
            }
        }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewDataLocationSpaceElementProvider()
        {
            CreateColumns();
        }

        #endregion

        #region Event Handlers

        private void OnSelectedLocationIdChanged(Object newValue)
        {
            //reload data.
            BeginDataLoad((Int16)newValue);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the location import mapping list
            LocationSpaceElementImportMappingList mappingList =
                LocationSpaceElementImportMappingList.NewLocationSpaceElementImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(
                mappingList,
                ViewDataLocationSpaceElementProviderRowEntry.OriginalLocationSpaceElementProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationSpaceElementImportMappingList.LocationCodeMappingId, ViewDataLocationSpaceElementProviderRowEntry.LocationCodeProperty.Path},
                    {LocationSpaceElementImportMappingList.ProductGroupCodeMappingId, ViewDataLocationSpaceElementProviderRowEntry.ProductGroupCodeProperty.Path},
                    {LocationSpaceElementImportMappingList.BayOrderMappingId, ViewDataLocationSpaceElementProviderRowEntry.BayNumberProperty.Path}
                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }
        }

        public void EditItem(object selectedItem, Window sourcecontrol)
        {
            ViewDataLocationSpaceElementProviderRowEntry selectedLocationSpaceElementItem = (ViewDataLocationSpaceElementProviderRowEntry)selectedItem;
            LocationSpaceMaintenance.LocationSpaceMaintenanceOrganiser spaceElementOrganiser = new LocationSpaceMaintenance.LocationSpaceMaintenanceOrganiser();
            spaceElementOrganiser.ViewModel.OpenCommand.Execute(selectedLocationSpaceElementItem.OriginalLocationSpaceInfo.Id);
            App.ShowWindow(spaceElementOrganiser, sourcecontrol,/*isModel*/true);
        }

        public void DeleteItem(Object selectedItem)
        {
            ViewDataLocationSpaceElementProviderRowEntry dataRow = (ViewDataLocationSpaceElementProviderRowEntry)selectedItem;
            LocationSpaceBay selectedLocationSpaceBay = dataRow.OriginalLocationSpaceBay;
            LocationSpaceElement selectedLocationSpaceElement = dataRow.OriginalLocationSpaceElement;
            Int32 elementId = selectedLocationSpaceElement.Id;
            Int32 bayId = selectedLocationSpaceBay.Id;
            Int32 parentGroupId = selectedLocationSpaceBay.Parent.Id;
            Int32 parentSpaceId = dataRow.OriginalLocationSpaceInfo.Id;

            if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(selectedLocationSpaceBay.ToString()))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                LocationSpace parentLocationSpace = LocationSpace.GetLocationSpaceById(parentSpaceId);
                LocationSpaceProductGroup parentGroup = parentLocationSpace.LocationSpaceProductGroups.First(p => p.Id == parentGroupId);
                LocationSpaceBay parentBay = parentGroup.Bays.First(b => b.Id == bayId);
                LocationSpaceElement itemToDelete = parentBay.Elements.First(e => e.Id == elementId);
                parentBay.Elements.Remove(itemToDelete);

                //commit the delete
                try
                {
                    parentLocationSpace.Save();
                }
                catch (DataPortalException ex)
                {
                    Mouse.OverrideCursor = null;

                    LocalHelper.RecordException(ex, "DataManagement");
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.ToString(), OperationType.Delete);

                    Mouse.OverrideCursor = Cursors.Wait;
                }

                //remove the item from the data source
                _dataSourceEntries.Remove(dataRow);


                Mouse.OverrideCursor = null;

            }
        }

        private void BeginDataLoad(Int16 locationId)
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(new Object[] { App.ViewState.EntityId, locationId });
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Object[] args = (Object[])e.Argument;
            Int32 entityId = (Int32)args[0];
            Int16 locationId = (Int16)args[1];

            List<ViewDataLocationSpaceElementProviderRowEntry> rows = new List<ViewDataLocationSpaceElementProviderRowEntry>();

            //Build data structure
            LocationSpaceInfoList list = LocationSpaceInfoList.FetchByEntityId(entityId);
            if (list != null && list.Count > 0)
            {
                //create lookups
                Dictionary<Int32, String> groupCodeLookup =
                    ProductHierarchy.FetchByEntityId(App.ViewState.EntityId).EnumerateAllGroups().ToDictionary(g => g.Id, g => g.Code);

                foreach (LocationSpaceInfo info in list)
                {
                    if (info.LocationId == locationId)
                    {
                        LocationSpace locationSpace = LocationSpace.GetLocationSpaceById(info.Id);
                        foreach (LocationSpaceProductGroup productGroup in locationSpace.LocationSpaceProductGroups)
                        {
                            String groupCode = String.Empty;
                            groupCodeLookup.TryGetValue(productGroup.ProductGroupId, out groupCode);


                            foreach (LocationSpaceBay bay in productGroup.Bays)
                            {
                                foreach (LocationSpaceElement element in bay.Elements)
                                {
                                    //Construct data object
                                    rows.Add(new ViewDataLocationSpaceElementProviderRowEntry(info, groupCode, bay, element));
                                }
                            }

                        }
                    }
                }
            }

            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationSpaceElementProviderRowEntry> rows = e.Result as List<ViewDataLocationSpaceElementProviderRowEntry>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);
            }

            this.IsLoadingData = false;
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataLocationSpaceElementProviderRowEntry
        {
            #region Fields
            private LocationSpaceInfo _originalLocationSpaceInfo;
            private String _productGroupCode;
            private LocationSpaceBay _originalLocationSpaceBay;
            private LocationSpaceElement _originalLocationSpaceElement;
            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath OriginalLocationSpaceElementProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceElementProviderRowEntry>(p => p.OriginalLocationSpaceElement);
            public static readonly PropertyPath LocationCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceElementProviderRowEntry>(p => p.LocationCode);
            public static readonly PropertyPath ProductGroupCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceElementProviderRowEntry>(p => p.ProductGroupCode);
            public static readonly PropertyPath BayNumberProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationSpaceElementProviderRowEntry>(p => p.BayNumber);
            #endregion

            #region Properties

            /// <summary>
            /// Returns the original location space bay
            /// </summary>
            public LocationSpaceBay OriginalLocationSpaceBay
            {
                get { return _originalLocationSpaceBay; }
            }

            /// <summary>
            /// Returns the original location space element
            /// </summary>
            public LocationSpaceElement OriginalLocationSpaceElement
            {
                get { return _originalLocationSpaceElement; }
            }

            /// <summary>
            /// Returns the original location space info
            /// </summary>
            public LocationSpaceInfo OriginalLocationSpaceInfo
            {
                get { return _originalLocationSpaceInfo; }
            }

            /// <summary>
            /// Returns the location code
            /// </summary>
            public String LocationCode
            {
                get { return this._originalLocationSpaceInfo.LocationCode; }
            }

            /// <summary>
            /// Returns the product group code
            /// </summary>
            public String ProductGroupCode
            {
                get { return this._productGroupCode; }
            }

            /// <summary>
            /// Returns the bay number
            /// </summary>
            public Byte BayNumber
            {
                get { return this.OriginalLocationSpaceBay.Order; }
            }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="constraintObject"></param>
            public ViewDataLocationSpaceElementProviderRowEntry(LocationSpaceInfo locationSpaceInfo, String productGroupCode, LocationSpaceBay bay, LocationSpaceElement element)
            {
                _originalLocationSpaceInfo = locationSpaceInfo;
                _productGroupCode = productGroupCode;
                _originalLocationSpaceBay = bay;
                _originalLocationSpaceElement = element;
            }


            #endregion

        }

        #endregion

        #region IViewDataProvider Members


        Boolean IViewDataProvider.IsDeleteSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsEditSupported
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        Boolean IViewDataProvider.IsLocationSelectorVisible
        {
            get { return true; }
        }

        Boolean IViewDataProvider.IsCategorySelectorVisible
        {
            get { return false; }
        }


        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
