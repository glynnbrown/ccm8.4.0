﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion

using System.Windows;
using System.Windows.Controls;
using System.Collections.Specialized;
using Galleria.Ccm.Imports.Mappings;
using System;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Imports;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDataColumnDefinition.xaml
    /// </summary>
    public partial class ImportDataColumnDefinition : UserControl
    {
        #region Constants
        const String AvailableColumnsCollectionKey = "ImportDataColumnDefinition_AvailableColumnsCollection";
        #endregion

        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportDataColumnDefinitionViewModel), typeof(ImportDataColumnDefinition),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportDataColumnDefinitionViewModel ViewModel
        {
            get { return (ImportDataColumnDefinitionViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportDataColumnDefinition senderControl = (ImportDataColumnDefinition)obj;

            String clearSelectedMappingsCommandKey = "ClearSelectedMappingsCommand";
            String clearSelectedMappingCommandKey = "ClearSelectedMappingCommand";

            if (e.OldValue != null)
            {
                ImportDataColumnDefinitionViewModel oldModel = (ImportDataColumnDefinitionViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(clearSelectedMappingsCommandKey);
                senderControl.Resources.Remove(clearSelectedMappingCommandKey);
                ((INotifyCollectionChanged)oldModel.AvailableColumns).CollectionChanged -= senderControl.ViewModelAvailableColumns_CollectionChanged;
            }

            if (e.NewValue != null)
            {
                ImportDataColumnDefinitionViewModel newModel = (ImportDataColumnDefinitionViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(clearSelectedMappingsCommandKey, newModel.ClearSelectedMappingsCommand);
                senderControl.Resources.Add(clearSelectedMappingCommandKey, newModel.ClearSelectedMappingCommand);
                ((INotifyCollectionChanged)newModel.AvailableColumns).CollectionChanged += senderControl.ViewModelAvailableColumns_CollectionChanged;
                senderControl.ResetAvailableColumnsCollection(newModel.AvailableColumns);

            }
        }


        #endregion

        #region Constructor

        public ImportDataColumnDefinition(ImportDataColumnDefinitionViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the view model available columns collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModelAvailableColumns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ColumnHeaderCollection resCollection = this.Resources[AvailableColumnsCollectionKey] as ColumnHeaderCollection;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (String s in e.NewItems)
                    {
                        resCollection.Add(s);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (String s in e.OldItems)
                    {
                        resCollection.Remove(s);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    ResetAvailableColumnsCollection((IEnumerable<String>)sender);
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Resynchs the available columns resource collection
        /// </summary>
        private void ResetAvailableColumnsCollection(IEnumerable<String> sourceCollection)
        {
            ColumnHeaderCollection resCollection = this.Resources[AvailableColumnsCollectionKey] as ColumnHeaderCollection;
            if (resCollection != null)
            {
                resCollection.Clear();

                if (sourceCollection != null)
                {
                    if (this.ViewModel.IsFirstRowColumnHeaders)
                    {
                        foreach (String columnName in sourceCollection.OrderBy(s => s))
                        {
                            resCollection.Add(columnName);
                        }
                    }
                    else
                    {
                        foreach (String columnName in sourceCollection)
                        {
                            resCollection.Add(columnName);
                        }
                    }
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// Custom string collection for the available columns drop down list
    /// </summary>
    public class ColumnHeaderCollection : ObservableCollection<String>
    {
        public ColumnHeaderCollection() { }
    }
}
