﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25446 : N.Haywood
//  Copied from GFS.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using System.Collections;
using System.ComponentModel;
using Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Helpers;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationProductAttributeProvider : IViewDataProvider
    {
        #region Fields
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private readonly BulkObservableCollection<ViewDataLocationProductAttributeRow> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationProductAttributeRow>();
        private Boolean _isLoadingData;
        private LocationProductAttributeList _masterList;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to use
        /// </summary>
        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Returns true if the edit command is supported
        /// </summary>
        public Boolean IsEditSupported { get; private set; }

        /// <summary>
        /// Returns true if the delete command is supported
        /// </summary>
        public Boolean IsDeleteSupported { get; private set; }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewDataLocationProductAttributeProvider()
        {
            this.IsEditSupported = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.EditObject, typeof(LocationProductAttribute));
            this.IsDeleteSupported = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.DeleteObject, typeof(LocationProductAttribute));

            CreateColumns();
            BeginDataLoad();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the column set to be used to display the data
        /// </summary>
        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the product import mapping list
            LocationProductAttributeImportMappingList mappingList =
               LocationProductAttributeImportMappingList.NewLocationProductAttributeImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList,
                ViewDataLocationProductAttributeRow.LocationProductAttributeProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationProductAttributeImportMappingList.LocationCodeMapId, ViewDataLocationProductAttributeRow.LocationProperty.Path},
                    {LocationProductAttributeImportMappingList.ProductGTINMapId, ViewDataLocationProductAttributeRow.ProductProperty.Path},
                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }
        }

        /// <summary>
        /// Start loading data
        /// </summary>
        private void BeginDataLoad()
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(App.ViewState.EntityId);
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 entityId = (Int32)e.Argument;

            List<ViewDataLocationProductAttributeRow> rows = new List<ViewDataLocationProductAttributeRow>();

            LocationProductAttributeList list = LocationProductAttributeList.FetchByEntityId(entityId);
            if (list != null && list.Count > 0)
            {
                //create lookups
                List<Int32> productIds = list.Select(l => l.ProductId).Distinct().ToList();
                Dictionary<Int32, String> productCodeLookup;
                if (productIds.Count < 5000)
                {
                    productCodeLookup = ProductInfoList.FetchByEntityIdProductIds(entityId, productIds).ToDictionary(p => p.Id, p => p.Gtin);
                }
                else
                {
                    productCodeLookup = ProductInfoList.FetchByEntityId(entityId).ToDictionary(p => p.Id, p => p.Gtin);
                }

                Dictionary<Int16, String> locationCodeLookup = LocationInfoList.FetchByEntityId(entityId).ToDictionary(p => p.Id, p => p.Code);

                //create rows
                foreach (LocationProductAttribute locationProductAttribute in list)
                {
                    String productGtin = String.Empty;
                    productCodeLookup.TryGetValue(locationProductAttribute.ProductId, out productGtin);

                    String locationCode = String.Empty;
                    locationCodeLookup.TryGetValue(locationProductAttribute.LocationId, out locationCode);

                    rows.Add(new ViewDataLocationProductAttributeRow(locationProductAttribute, locationCode, productGtin));

                }

            }


            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationProductAttributeRow> rows = e.Result as List<ViewDataLocationProductAttributeRow>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);

                //take a ref to the master parent list.
                _masterList = rows[0].LocationProductAttribute.Parent as LocationProductAttributeList;
            }

            this.IsLoadingData = false;
        }

        public void EditItem(object selectedItem, Window sourcecontrol)
        {
            ViewDataLocationProductAttributeRow locationProductAttribute = selectedItem as ViewDataLocationProductAttributeRow;
            if (locationProductAttribute != null)
            {
                LocationProductAttributeMaintenanceOrganiser locationProductAttributeWindow =
                    new LocationProductAttributeMaintenanceOrganiser();
                locationProductAttributeWindow.ViewModel.OpenCommand.Execute(
                    new Tuple<short?, int?>
                        (locationProductAttribute.LocationProductAttribute.LocationId,
                        locationProductAttribute.LocationProductAttribute.ProductId));

                locationProductAttributeWindow.Closed += EditWindow_Closed;

                App.ShowWindow(locationProductAttributeWindow, sourcecontrol, false);
            }
        }
        private void EditWindow_Closed(object sender, EventArgs e)
        {
            Window win = (Window)sender;
            win.Closed -= EditWindow_Closed;

            //refresh data
            BeginDataLoad();
        }

        public void DeleteItem(object selectedItem)
        {
            ViewDataLocationProductAttributeRow dataRow = selectedItem as ViewDataLocationProductAttributeRow;
            if (dataRow != null && _masterList != null)
            {
                if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(LocationProductAttribute.FriendlyName))
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    Int16 locId = dataRow.LocationProductAttribute.LocationId;
                    Int32 productId = dataRow.LocationProductAttribute.ProductId;

                    LocationProductAttribute toDelete =
                        _masterList.FirstOrDefault(l => l.ProductId == productId && l.LocationId == locId);
                    if (toDelete != null)
                    {
                        try
                        {
                            _masterList.Remove(toDelete);
                            _masterList = _masterList.Save();
                        }
                        catch (DataPortalException ex)
                        {
                            Mouse.OverrideCursor = null;

                            LocalHelper.RecordException(ex);
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(LocationProductAttribute.FriendlyName, OperationType.Delete);

                            Mouse.OverrideCursor = Cursors.Wait;
                        }


                        _dataSourceEntries.Remove(dataRow);
                    }

                    Mouse.OverrideCursor = null;
                }
            }
        }

        #endregion

        //#region Nested Classes

        //private sealed class ViewDataLocationProductAttributeDataRow
        //{
        //    #region Fields

        //    private LocationProductAttribute _locationProductAttribute;

        //    #endregion

        //    #region Binding Property Paths

        //    public static readonly PropertyPath OriginalLocationProductAttributeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.OriginalLocationProductAttribute);
        //    public static readonly PropertyPath Location_CodeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Location_Code);
        //    public static readonly PropertyPath Product_GTINProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Product_GTIN);
        //    public static readonly PropertyPath GTINProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.GTIN);
        //    public static readonly PropertyPath HeightProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Height);
        //    public static readonly PropertyPath WidthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Width);
        //    public static readonly PropertyPath DepthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Depth);
        //    public static readonly PropertyPath CasePackUnitsProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CasePackUnits);
        //    public static readonly PropertyPath CaseHighProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CaseHigh);
        //    public static readonly PropertyPath CaseWideProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CaseWide);
        //    public static readonly PropertyPath CaseDeepProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CaseDeep);
        //    public static readonly PropertyPath CaseHeightProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CaseHeight);
        //    public static readonly PropertyPath CaseDepthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CaseDepth);
        //    public static readonly PropertyPath CaseWidthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CaseWidth);
        //    public static readonly PropertyPath DaysOfSupplyProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.DaysOfSupply);
        //    public static readonly PropertyPath MinDeepProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.MinDeep);
        //    public static readonly PropertyPath MaxDeepProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.MaxDeep);
        //    public static readonly PropertyPath TrayPackUnitsProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayPackUnits);
        //    public static readonly PropertyPath TrayHighProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayHigh);
        //    public static readonly PropertyPath TrayWideProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayWide);
        //    public static readonly PropertyPath TrayDeepProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayDeep);
        //    public static readonly PropertyPath TrayHeightProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayHeight);
        //    public static readonly PropertyPath TrayDepthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayDepth);
        //    public static readonly PropertyPath TrayWidthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayWidth);
        //    public static readonly PropertyPath TrayThickHeightProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayThickHeight);
        //    public static readonly PropertyPath TrayThickDepthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayThickDepth);
        //    public static readonly PropertyPath TrayThickWidthProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.TrayThickWidth);
        //    public static readonly PropertyPath StatusTypeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.StatusType);
        //    public static readonly PropertyPath ShelfLifeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.ShelfLife);
        //    public static readonly PropertyPath DeliveryFrequencyDaysProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.DeliveryFrequencyDays);
        //    public static readonly PropertyPath DeliveryMethodProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.DeliveryMethod);
        //    public static readonly PropertyPath VendorCodeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.VendorCode);
        //    public static readonly PropertyPath VendorProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Vendor);
        //    public static readonly PropertyPath ManufacturerCodeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.ManufacturerCode);
        //    public static readonly PropertyPath ManufacturerProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Manufacturer);
        //    public static readonly PropertyPath SizeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Size);
        //    public static readonly PropertyPath UnitOfMeasureProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.UnitOfMeasure);
        //    public static readonly PropertyPath SellPriceProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.SellPrice);
        //    public static readonly PropertyPath SellPackCountProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.SellPackCount);
        //    public static readonly PropertyPath SellPackDescriptionProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.SellPackDescription);
        //    public static readonly PropertyPath RecommendedRetailPriceProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.RecommendedRetailPrice);
        //    public static readonly PropertyPath CostPriceProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CostPrice);
        //    public static readonly PropertyPath CaseCostProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CaseCost);
        //    public static readonly PropertyPath ConsumerInformationProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.ConsumerInformation);
        //    public static readonly PropertyPath PatternProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Pattern);
        //    public static readonly PropertyPath ModelProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.Model);
        //    public static readonly PropertyPath CorporateCodeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeDataRow>(p => p.CorporateCode);

        //    #endregion

        //    #region Properties

        //    /// <summary>
        //    /// Returns the original constraint model object
        //    /// </summary>
        //    public LocationProductAttribute OriginalLocationProductAttribute
        //    {
        //        get { return _locationProductAttribute; }
        //    }

        //    public String Location_Code { get; set; }
        //    public String Product_GTIN { get; set; }

        //    public String GTIN
        //    {
        //        get { return _locationProductAttribute.GTIN; }
        //        set { _locationProductAttribute.GTIN = value; }
        //    }

        //    public String Description
        //    {
        //        get { return _locationProductAttribute.Description; }
        //        set { _locationProductAttribute.Description = value; }
        //    }

        //    public Single? Height
        //    {
        //        get { return _locationProductAttribute.Height; }
        //        set { _locationProductAttribute.Height = value; }
        //    }

        //    public Single? Width
        //    {
        //        get { return _locationProductAttribute.Width; }
        //        set { _locationProductAttribute.Width = value; }
        //    }

        //    public Single? Depth
        //    {
        //        get { return _locationProductAttribute.Depth; }
        //        set { _locationProductAttribute.Depth = value; }
        //    }

        //    public Int16? CasePackUnits
        //    {
        //        get { return _locationProductAttribute.CasePackUnits; }
        //        set { _locationProductAttribute.CasePackUnits = value; }
        //    }

        //    public Byte? CaseHigh
        //    {
        //        get { return _locationProductAttribute.CaseHigh; }
        //        set { _locationProductAttribute.CaseHigh = value; }
        //    }

        //    public Byte? CaseWide
        //    {
        //        get { return _locationProductAttribute.CaseWide; }
        //        set { _locationProductAttribute.CaseWide = value; }
        //    }

        //    public Byte? CaseDeep
        //    {
        //        get { return _locationProductAttribute.CaseDeep; }
        //        set { _locationProductAttribute.CaseDeep = value; }
        //    }

        //    public Single? CaseHeight
        //    {
        //        get { return _locationProductAttribute.CaseHeight; }
        //        set { _locationProductAttribute.CaseHeight = value; }
        //    }

        //    public Single? CaseDepth
        //    {
        //        get { return _locationProductAttribute.CaseDepth; }
        //        set { _locationProductAttribute.CaseDepth = value; }
        //    }

        //    public Single? CaseWidth
        //    {
        //        get { return _locationProductAttribute.CaseWidth; }
        //        set { _locationProductAttribute.CaseWidth = value; }
        //    }

        //    public Byte? DaysOfSupply
        //    {
        //        get { return _locationProductAttribute.DaysOfSupply; }
        //        set { _locationProductAttribute.DaysOfSupply = value; }
        //    }

        //    public Byte? MinDeep
        //    {
        //        get { return _locationProductAttribute.MinDeep; }
        //        set { _locationProductAttribute.MinDeep = value; }
        //    }

        //    public Byte? MaxDeep
        //    {
        //        get { return _locationProductAttribute.MaxDeep; }
        //        set { _locationProductAttribute.MaxDeep = value; }
        //    }

        //    public Int16? TrayPackUnits
        //    {
        //        get { return _locationProductAttribute.TrayPackUnits; }
        //        set { _locationProductAttribute.TrayPackUnits = value; }
        //    }

        //    public Byte? TrayHigh
        //    {
        //        get { return _locationProductAttribute.TrayHigh; }
        //        set { _locationProductAttribute.TrayHigh = value; }
        //    }

        //    public Byte? TrayWide
        //    {
        //        get { return _locationProductAttribute.TrayWide; }
        //        set { _locationProductAttribute.TrayWide = value; }
        //    }

        //    public Byte? TrayDeep
        //    {
        //        get { return _locationProductAttribute.TrayDeep; }
        //        set { _locationProductAttribute.TrayDeep = value; }
        //    }

        //    public Single? TrayHeight
        //    {
        //        get { return _locationProductAttribute.TrayHeight; }
        //        set { _locationProductAttribute.TrayHeight = value; }
        //    }

        //    public Single? TrayDepth
        //    {
        //        get { return _locationProductAttribute.TrayDepth; }
        //        set { _locationProductAttribute.TrayDepth = value; }
        //    }

        //    public Single? TrayWidth
        //    {
        //        get { return _locationProductAttribute.TrayWidth; }
        //        set { _locationProductAttribute.TrayWidth = value; }
        //    }

        //    public Single? TrayThickHeight
        //    {
        //        get { return _locationProductAttribute.TrayThickHeight; }
        //        set { _locationProductAttribute.TrayThickHeight = value; }
        //    }

        //    public Single? TrayThickDepth
        //    {
        //        get { return _locationProductAttribute.TrayThickDepth; }
        //        set { _locationProductAttribute.TrayThickDepth = value; }
        //    }

        //    public Single? TrayThickWidth
        //    {
        //        get { return _locationProductAttribute.TrayThickWidth; }
        //        set { _locationProductAttribute.TrayThickWidth = value; }
        //    }

        //    public ProductStatusType? StatusType
        //    {
        //        get { return _locationProductAttribute.StatusType; }
        //        set { _locationProductAttribute.StatusType = value; }
        //    }

        //    public Int16? ShelfLife
        //    {
        //        get { return _locationProductAttribute.ShelfLife; }
        //        set { _locationProductAttribute.ShelfLife = value; }
        //    }

        //    public Single? DeliveryFrequencyDays
        //    {
        //        get { return _locationProductAttribute.DeliveryFrequencyDays; }
        //        set { _locationProductAttribute.DeliveryFrequencyDays = value; }
        //    }

        //    public String DeliveryMethod
        //    {
        //        get { return _locationProductAttribute.DeliveryMethod; }
        //        set { _locationProductAttribute.DeliveryMethod = value; }
        //    }

        //    public String VendorCode
        //    {
        //        get { return _locationProductAttribute.VendorCode; }
        //        set { _locationProductAttribute.VendorCode = value; }
        //    }

        //    public String Vendor
        //    {
        //        get { return _locationProductAttribute.Vendor; }
        //        set { _locationProductAttribute.Vendor = value; }
        //    }

        //    public String ManufacturerCode
        //    {
        //        get { return _locationProductAttribute.ManufacturerCode; }
        //        set { _locationProductAttribute.ManufacturerCode = value; }
        //    }

        //    public String Manufacturer
        //    {
        //        get { return _locationProductAttribute.Manufacturer; }
        //        set { _locationProductAttribute.Manufacturer = value; }
        //    }

        //    public String Size
        //    {
        //        get { return _locationProductAttribute.Size; }
        //        set { _locationProductAttribute.Size = value; }
        //    }

        //    public String UnitOfMeasure
        //    {
        //        get { return _locationProductAttribute.UnitOfMeasure; }
        //        set { _locationProductAttribute.UnitOfMeasure = value; }
        //    }

        //    public Single? SellPrice
        //    {
        //        get { return _locationProductAttribute.SellPrice; }
        //        set { _locationProductAttribute.SellPrice = value; }
        //    }

        //    public Int16? SellPackCount
        //    {
        //        get { return _locationProductAttribute.SellPackCount; }
        //        set { _locationProductAttribute.SellPackCount = value; }
        //    }

        //    public String SellPackDescription
        //    {
        //        get { return _locationProductAttribute.SellPackDescription; }
        //        set { _locationProductAttribute.SellPackDescription = value; }
        //    }

        //    public Single? RecommendedRetailPrice
        //    {
        //        get { return _locationProductAttribute.RecommendedRetailPrice; }
        //        set { _locationProductAttribute.RecommendedRetailPrice = value; }
        //    }

        //    public Single? CostPrice
        //    {
        //        get { return _locationProductAttribute.CostPrice; }
        //        set { _locationProductAttribute.CostPrice = value; }
        //    }

        //    public Single? CaseCost
        //    {
        //        get { return _locationProductAttribute.CaseCost; }
        //        set { _locationProductAttribute.CaseCost = value; }
        //    }

        //    public String ConsumerInformation
        //    {
        //        get { return _locationProductAttribute.ConsumerInformation; }
        //        set { _locationProductAttribute.ConsumerInformation = value; }
        //    }

        //    public String Pattern
        //    {
        //        get { return _locationProductAttribute.Pattern; }
        //        set { _locationProductAttribute.Pattern = value; }
        //    }

        //    public String Model
        //    {
        //        get { return _locationProductAttribute.Model; }
        //        set { _locationProductAttribute.Model = value; }
        //    }

        //    public String CorporateCode
        //    {
        //        get { return _locationProductAttribute.CorporateCode; }
        //        set { _locationProductAttribute.CorporateCode = value; }
        //    }

        //    #endregion

        //    #region Constructor

        //    public ViewDataLocationProductAttributeDataRow(LocationProductAttribute locationProductAttribute)
        //    {
        //        _locationProductAttribute = locationProductAttribute;
        //    }

        //    #endregion
        //}

        //#endregion

        #region Nested Classes

        private sealed class ViewDataLocationProductAttributeRow
        {
            #region Fields

            private LocationProductAttribute _locationProductAttribute;
            private String _location;
            private String _product;

            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath LocationProductAttributeProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeRow>(c => c.LocationProductAttribute);
            public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeRow>(c => c.Location);
            public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<ViewDataLocationProductAttributeRow>(c => c.Product);

            #endregion

            #region Properties

            public LocationProductAttribute LocationProductAttribute
            {
                get { return _locationProductAttribute; }
            }

            public String Location
            {
                get { return _location; }
            }

            public String Product
            {
                get { return _product; }
            }

            #endregion

            #region Constructor

            public ViewDataLocationProductAttributeRow(LocationProductAttribute locationProductAttribute, String location, String product)
            {
                _locationProductAttribute = locationProductAttribute;
                _location = location;
                _product = product;
            }

            #endregion
        }

        #endregion

        #region IViewDataProvider Members

        void IViewDataProvider.DeleteItem(object selectedItem)
        {
            throw new NotSupportedException();
        }

        public bool IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public bool IsLocationSelectorVisible
        {
            get { return false; }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public bool IsCategorySelectorVisible
        {
            get { return false; }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
