﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Microsoft.Win32;
using Galleria.Ccm.Imports;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public class ImportDataSelectFileViewModel : ViewModelObject
    {
        #region Fields

        private Dictionary<CCMDataItemType, String> _availableDataTypes;
        private CCMDataItemType _selectedDataType;
        private Dictionary<MDMImportDataFileType, String> _availableFileTypes;
        private MDMImportDataFileType _selectedFileType;
        private String _filePath;
        private Boolean _isFilePathValid;
        private IImportDataSelectFileProvider _dataTypeProvider;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailableDataTypesProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.AvailableDataTypes);
        public static readonly PropertyPath SelectedDataTypeProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.SelectedDataType);
        public static readonly PropertyPath AvailableFileTypesProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.AvailableFileTypes);
        public static readonly PropertyPath SelectedFileTypeProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.SelectedFileType);
        public static readonly PropertyPath FilePathProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.FilePath);
        public static readonly PropertyPath IsFilePathValidProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.IsFilePathValid);

        public static readonly PropertyPath BrowseForFileCommandProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.BrowseForFileCommand);
        public static readonly PropertyPath DataTypeProviderProperty = WpfHelper.GetPropertyPath<ImportDataSelectFileViewModel>(p => p.DataTypeProvider);

        #endregion

        #region Properties

        /// <summary>
        /// Returns a source containing all data types available for selection
        /// </summary>
        public Dictionary<CCMDataItemType, String> AvailableDataTypes
        {
            get
            {
                //[TODO]
                if (_availableDataTypes == null)
                {
                    _availableDataTypes = new Dictionary<CCMDataItemType, String>();
                    //[TODO] will have to be edited based on master data
                    foreach (CCMDataItemType type in Enum.GetValues(typeof(CCMDataItemType)))
                    {
                        //if (type != CCMDataItemType.OtherSetupActivities)
                        {
                            _availableDataTypes.Add(type, CCMDataItemTypeHelper.FriendlyNames[type]);
                        }
                    }
                }
                return _availableDataTypes;
            }
        }

        /// <summary>
        /// Gets/Sets the selected data type
        /// </summary>
        public CCMDataItemType SelectedDataType
        {
            get { return _selectedDataType; }
            set
            {
                _selectedDataType = value;
                GetDataTypeProvider();
                OnPropertyChanged(SelectedDataTypeProperty);
            }
        }

        /// <summary>
        /// Returns a source containing all file types available for selection
        /// </summary>
        public Dictionary<MDMImportDataFileType, String> AvailableFileTypes
        {
            get
            {
                if (_availableFileTypes == null)
                {
                    _availableFileTypes = new Dictionary<MDMImportDataFileType, String>()
                    {
                        {MDMImportDataFileType.Excel, Message.ImportFileTitle_Excel},
                        {MDMImportDataFileType.Text, Message.ImportFileTitle_Text}
                    };
                }
                return _availableFileTypes;
            }
        }

        /// <summary>
        /// Gets/Sets the selected file type
        /// </summary>
        public MDMImportDataFileType SelectedFileType
        {
            get { return _selectedFileType; }
            set
            {
                _selectedFileType = value;
                OnPropertyChanged(SelectedFileTypeProperty);

                //reset the file path
                this.FilePath = null;
            }
        }

        /// <summary>
        /// Gets/Sets the selected file path
        /// </summary>
        public String FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                OnPropertyChanged(FilePathProperty);
                OnFilePathChanged();
            }
        }

        /// <summary>
        /// Flag to indicate if the file path is valid.
        /// </summary>
        public Boolean IsFilePathValid
        {
            get { return _isFilePathValid; }
            private set
            {
                _isFilePathValid = value;
                OnPropertyChanged(IsFilePathValidProperty);
            }
        }

        /// <summary>
        /// Determines which (if any) black box provider object
        /// should appear in the ImportDataSelectFile user control,
        /// depending on the SelectedDataType chosen.
        /// </summary>
        public IImportDataSelectFileProvider DataTypeProvider
        {
            get { return _dataTypeProvider; }
            set
            {
                _dataTypeProvider = value;
                OnPropertyChanged(DataTypeProviderProperty);
            }
        }

        #endregion

        #region Constructor

        public ImportDataSelectFileViewModel()
        { }

        #endregion

        #region Commands

        #region BrowseForFile

        private RelayCommand _browseForFileCommand;

        /// <summary>
        /// Shows the file open dialog
        /// </summary>
        public RelayCommand BrowseForFileCommand
        {
            get
            {
                if (_browseForFileCommand == null)
                {
                    _browseForFileCommand = new RelayCommand(
                        p => BrowseForFile_Executed())
                    {
                        FriendlyName = Message.Generic_Browse
                    };
                }
                return _browseForFileCommand;
            }
        }

        private void BrowseForFile_Executed()
        {
            //show the file open dialog
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;

            if (this.SelectedFileType == MDMImportDataFileType.Excel)
            {
                ofd.Filter = Message.ImportFileTypeFilter_Excel2007 + "|" + Message.ImportFileTypeFilter_Excel2003;
            }
            else if (this.SelectedFileType == MDMImportDataFileType.Text)
            {
                ofd.Filter = Message.ImportFileTypeFilter_Text + "|" + Message.ImportFileTypeFilter_Csv;
            }
            else
            {
                throw new NotImplementedException();
            }

            //update the selected file to the filepath
            if (ofd.ShowDialog() == true)
            {
                this.FilePath = ofd.FileName;
            }
        }


        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the datatype provider for the current data type
        /// </summary>
        /// <returns></returns>
        private IImportDataSelectFileProvider GetDataTypeProvider()
        {
            switch (_selectedDataType)
            {
                default:
                    _dataTypeProvider = null;
                    break;
            }
            return _dataTypeProvider;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reponds to a change of file path. Checks that it is valid.
        /// </summary>
        private void OnFilePathChanged()
        {
            bool isValid = false;

            if (!String.IsNullOrEmpty(this.FilePath))
            {
                //check if the file exists and is of the correct extension
                if (File.Exists(this.FilePath))
                {
                    String ext = Path.GetExtension(this.FilePath);

                    if (this.SelectedFileType == MDMImportDataFileType.Text)
                    {
                        isValid = (ext == ".txt" || ext == ".csv");
                    }
                    else if (this.SelectedFileType == MDMImportDataFileType.Excel)
                    {
                        isValid = (ext == ".xlsx" || ext == ".xls");
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                }
            }

            this.IsFilePathValid = isValid;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //nothing to kill
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
