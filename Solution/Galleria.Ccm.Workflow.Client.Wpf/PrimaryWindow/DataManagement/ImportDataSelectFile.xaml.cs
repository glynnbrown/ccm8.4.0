﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Imports;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDefinitionSelectFile.xaml
    /// </summary>
    public partial class ImportDataSelectFile : UserControl
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportDataSelectFileViewModel),
            typeof(ImportDataSelectFile), new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportDataSelectFileViewModel ViewModel
        {
            get { return (ImportDataSelectFileViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportDataSelectFile senderControl = (ImportDataSelectFile)obj;

            if (e.OldValue != null)
            {
                ImportDataSelectFileViewModel oldModel = (ImportDataSelectFileViewModel)e.OldValue;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ImportDataSelectFileViewModel newModel = (ImportDataSelectFileViewModel)e.NewValue;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="viewModel"></param>
        public ImportDataSelectFile(ImportDataSelectFileViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles any updates that are required when on eof the ViewModel's properties changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ImportDataSelectFileViewModel.SelectedDataTypeProperty.Path)
            {
                OnDataTypeChanged();
            }
        }

        /// <summary>
        /// Loads the appropriate child DataTypeProvider control (if any) when the DataType property changes
        /// </summary>
        private void OnDataTypeChanged()
        {
            CCMDataItemType dataType = this.ViewModel.SelectedDataType;
            this.dataProviderContentArea.Children.Clear();

            switch (dataType)
            {
                default:
                    this.dataProviderContentArea.Children.Clear();
                    break;
            }
        }

        #endregion
    }
}
