﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using System.Collections;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Csla;
using System.Windows.Input;
using System.ComponentModel;
using Galleria.Ccm.Imports.Mappings;
using System.Windows.Controls;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataLocationProductLegalProvider : IViewDataProvider
    {
        #region Fields
        private readonly DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private Boolean _isLoadingData;
        private BulkObservableCollection<ViewDataLocationProductLegalRow> _dataSourceEntries = new BulkObservableCollection<ViewDataLocationProductLegalRow>();
        private LocationProductLegalList _masterList;
        #endregion

        #region Properties

        public IList DataSource
        {
            get { return _dataSourceEntries; }
        }

        /// <summary>
        /// Returns the column set collection to be used to display
        /// the datasource
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        public Boolean IsEditSupported { get; private set; }

        public Boolean IsDeleteSupported { get; private set; }

        /// <summary>
        /// Returns true if data is loading
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return _isLoadingData; }
            private set
            {
                _isLoadingData = value;
                OnPropertyChanged(IViewDataProviderHelper.IsLoadingDataProperty.Path);
            }
        }

        #endregion

        #region Constructor

        public ViewDataLocationProductLegalProvider()
        {
            this.IsEditSupported = false;//Cannot change the available products
            this.IsDeleteSupported = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.DeleteObject, typeof(LocationProductLegal));

            CreateColumns();
            BeginDataLoad();
        }

        #endregion

        #region Methods

        private void CreateColumns()
        {
            //clear existing columns
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //get the locationproductLegal import mapping list
            LocationProductLegalImportMappingList mappingList =
                LocationProductLegalImportMappingList.NewLocationProductLegalImportMappingList();

            List<DataGridColumn> columns =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList,
                ViewDataLocationProductLegalRow.LocationProductLegalProperty.Path,
                /*visibleMapIds*/null,
                new Dictionary<Int32, String>
                {
                    {LocationProductAttributeImportMappingList.LocationCodeMapId, ViewDataLocationProductLegalRow.LocationCodeProperty.Path},
                    {LocationProductAttributeImportMappingList.ProductGTINMapId, ViewDataLocationProductLegalRow.ProductGtinProperty.Path},
                });

            foreach (DataGridColumn col in columns)
            {
                _columnSet.Add(col);
            }


        }


        /// <summary>
        /// Start loading data
        /// </summary>
        private void BeginDataLoad()
        {
            this.IsLoadingData = true;

            //clear existing data
            if (_dataSourceEntries.Count > 0)
            {
                _dataSourceEntries.Clear();
            }

            //fetch data using background worker.
            Csla.Threading.BackgroundWorker dataWorker = new Csla.Threading.BackgroundWorker();
            dataWorker.DoWork += dataWorker_DoWork;
            dataWorker.RunWorkerCompleted += dataWorker_RunWorkerCompleted;
            dataWorker.RunWorkerAsync(App.ViewState.EntityId);
        }
        private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 entityId = (Int32)e.Argument;

            List<ViewDataLocationProductLegalRow> rows = new List<ViewDataLocationProductLegalRow>();

            LocationProductLegalList list = LocationProductLegalList.FetchByEntityId(entityId);
            if (list != null && list.Count > 0)
            {
                //create lookups
                Dictionary<Int16, String> locationCodeLookup = LocationInfoList.FetchByEntityId(entityId).ToDictionary(l => l.Id, l => l.Code);

                List<Int32> productIds = list.Select(l => l.ProductId).Distinct().ToList();
                Dictionary<Int32, String> productCodeLookup;
                if (productIds.Count < 5000)
                {
                    productCodeLookup = ProductInfoList.FetchByEntityIdProductIds(entityId, productIds).ToDictionary(p => p.Id, p => p.Gtin);
                }
                else
                {
                    productCodeLookup = ProductInfoList.FetchByEntityId(entityId).ToDictionary(p => p.Id, p => p.Gtin);
                }


                //create rows
                foreach (LocationProductLegal link in list)
                {
                    String gtin = String.Empty;
                    productCodeLookup.TryGetValue(link.ProductId, out gtin);

                    String locCode = String.Empty;
                    locationCodeLookup.TryGetValue(link.LocationId, out locCode);

                    rows.Add(new ViewDataLocationProductLegalRow(link, locCode, gtin));
                }

            }


            e.Result = rows;
        }
        private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker dataWorker = (Csla.Threading.BackgroundWorker)sender;
            dataWorker.DoWork -= dataWorker_DoWork;
            dataWorker.RunWorkerCompleted -= dataWorker_RunWorkerCompleted;

            List<ViewDataLocationProductLegalRow> rows = e.Result as List<ViewDataLocationProductLegalRow>;
            if (rows != null && rows.Count > 0)
            {
                _dataSourceEntries.AddRange(rows);

                _masterList = rows[0].OriginalLocationProductLegal.Parent as LocationProductLegalList;
            }

            this.IsLoadingData = false;
        }


        public void DeleteItem(object selectedItem)
        {
            ViewDataLocationProductLegalRow row = selectedItem as ViewDataLocationProductLegalRow;
            if (row != null && _masterList != null)
            {
                if (Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(LocationProductLegal.FriendlyName))
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    Int16 locId = row.OriginalLocationProductLegal.LocationId;
                    Int32 productId = row.OriginalLocationProductLegal.ProductId;

                    LocationProductLegal deleteItem =
                        _masterList.FirstOrDefault(l => l.ProductId == productId && l.LocationId == locId);
                    if (deleteItem != null)
                    {
                        try
                        {
                            _masterList.Remove(deleteItem);
                            _masterList = _masterList.Save();

                            _dataSourceEntries.Remove(row);
                        }
                        catch (DataPortalException ex)
                        {
                            Mouse.OverrideCursor = null;

                            LocalHelper.RecordException(ex);
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(LocationProductLegal.FriendlyName, OperationType.Delete);

                            Mouse.OverrideCursor = Cursors.Wait;
                        }
                    }


                    Mouse.OverrideCursor = null;
                }
            }
        }

        #endregion

        #region IViewDataProvider Members

        void IViewDataProvider.EditItem(object selectedItem, Window sourcecontrol)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region Nested Classes

        private sealed class ViewDataLocationProductLegalRow
        {
            #region Fields
            private String _productGtin;
            private String _locationCode;
            private LocationProductLegal _originalLocationProductLegal;
            #endregion

            #region Binding Property Paths

            public static readonly PropertyPath LocationProductLegalProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationProductLegalRow>(c => c.OriginalLocationProductLegal);
            public static readonly PropertyPath LocationCodeProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationProductLegalRow>(p => p.LocationCode);
            public static readonly PropertyPath ProductGtinProperty = Galleria.Framework.Controls.Wpf.Helpers.GetPropertyPath<ViewDataLocationProductLegalRow>(p => p.ProductGtin);

            #endregion

            #region Properties

            /// <summary>
            /// Returns the origional location product Legal
            /// </summary>
            public LocationProductLegal OriginalLocationProductLegal
            {
                get { return _originalLocationProductLegal; }
            }

            /// <summary>
            /// Returns the location code
            /// </summary>
            public String LocationCode
            {
                get { return _locationCode; }
            }

            /// <summary>
            /// Returns the product code
            /// </summary>
            public String ProductGtin
            {
                get { return _productGtin; }
            }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="constraintObject"></param>
            public ViewDataLocationProductLegalRow(LocationProductLegal locationProductLegal, String location, String productGtin)
            {
                _originalLocationProductLegal = locationProductLegal;
                _locationCode = location;
                _productGtin = productGtin;

            }


            #endregion

        }

        #endregion

        #region IViewDataProvider Members

        //void IViewDataProvider.DeleteItem(object selectedItem)
        //{
        //    throw new NotSupportedException();
        //}

        public bool IsPerformanceSourceSelectorVisible
        {
            get { return false; }
        }

        public object SelectedPerformanceSourceId
        {
            get { return 0; }
            set { }
        }

        public bool IsLocationSelectorVisible
        {
            get { return false; }
        }

        public object SelectedLocationId
        {
            get { return 0; }
            set { }
        }

        public bool IsCategorySelectorVisible
        {
            get { return false; }
        }

        public object SelectedCategoryId
        {
            get { return 0; }
            set { }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
