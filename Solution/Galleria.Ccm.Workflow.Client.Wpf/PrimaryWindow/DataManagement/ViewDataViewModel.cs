﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
// V8-25444 : N.Haywood
//  Added locations
// CCM-25449 : N.Haywood
//  Added cluster locations
// CCM-25452 : N.Haywood
//	Added products
// CCM-25451 : N.Haywood
//  Added product next/previous
// CCM-25448 : N.Haywood
//  Added location space
// CCM-25446 : N.Haywood
//  Added Location Product Attributes
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-25455 : J.Pickup
//  Added Assortment
#endregion
#region Version History: (CCM 8.1.1)
// CCM-29932 : J.Pickup
//  Now updates ProductSetPosition from the offset.
#endregion
#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Added LocationProductLegal
#endregion
#endregion


using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Imports;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    public sealed class ViewDataViewModel : ViewModelAttachedControlObject<ViewDataOrganiser>
    {
        #region Fields

        public const String EditColumnDTKey = "ViewDataOrganiser_DTEditColumn";

        private Boolean _userHasEditPerm;
        private Boolean _userHasDeletePerm;

        private CCMDataItemType _selectedDataType;
        private IViewDataProvider _dataProvider; // do not access this directly!
        private Object _selectedItem;


        private LocationInfoList _masterLocationList;
        //private PerformanceSourceList _masterPerformanceSourceList;

        private String _productSetPosition; //String denoting what position in the product list is currently showing
        private Boolean _showProductButtons = false; //Determines whether product next/previous buttons should be visible

        private DataGridColumnCollection _columnSet = new DataGridColumnCollection();

        private ProductGroup _selectedCategory;

        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath DataSourceProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.DataSource);
        public static readonly PropertyPath IsLoadingDataProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.IsLoadingData);
        public static readonly PropertyPath ColumnSetProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.ColumnSet);
        public static readonly PropertyPath SelectedItemProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.SelectedItem);

        //public static readonly PropertyPath MasterPerformanceSourceListProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.MasterPerformanceSourceList);
        public static readonly PropertyPath IsPerformanceSourceSelectorVisibleProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.IsPerformanceSourceSelectorVisible);
        public static readonly PropertyPath SelectedPerformanceSourceIdProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.SelectedPerformanceSourceId);

        public static readonly PropertyPath MasterLocationListProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.MasterLocationList);
        public static readonly PropertyPath IsLocationSelectorVisibleProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.IsLocationSelectorVisible);
        public static readonly PropertyPath SelectedLocationIdProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.SelectedLocationId);
        
        public static readonly PropertyPath IsCategorySelectorVisibleProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.IsCategorySelectorVisible);
        public static readonly PropertyPath SelectedCategoryIdProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.SelectedCategoryId);
        

        public static readonly PropertyPath IsProductDataProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(c => c.IsProductData);
        public static readonly PropertyPath ProductSetPositionProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(c => c.ProductSetPosition);
        public static readonly PropertyPath ShowProductButtonsProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(c => c.ShowProductButtons);

        public static readonly PropertyPath SelectedCategoryProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(c => c.SelectedCategory);

        //commands
        public static readonly PropertyPath EditCommandProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.EditCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath NextProductSetCommandProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.NextProductSetCommand);
        public static readonly PropertyPath PreviousProductSetCommandProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.PreviousProductSetCommand);
        public static readonly PropertyPath SetSelectedCategoryCommandProperty = WpfHelper.GetPropertyPath<ViewDataViewModel>(p => p.SetSelectedCategoryCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the data source to be displayed
        /// </summary>
        public IList DataSource
        {
            get
            {
                return GetIDataProvider().DataSource;
            }
        }

        /// <summary>
        /// Returns true if the data provide is currently loading data.
        /// </summary>
        public Boolean IsLoadingData
        {
            get { return GetIDataProvider().IsLoadingData; }
        }

        /// <summary>
        /// Gets the selected data type
        /// </summary>
        public CCMDataItemType SelectedDataType
        {
            get { return _selectedDataType; }
        }

        /// <summary>
        /// Gets/Sets the selected item in the grid
        /// </summary>
        public Object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged(SelectedItemProperty);
            }
        }

        /// <summary>
        /// Returns the columnset for the display
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get
            {
                if (_columnSet.Count == 0)
                {
                    CreateColumnSet();
                }
                return _columnSet;
            }
        }

        /// <summary>
        /// Returns the is performance source selector visible flag
        /// </summary>
        public Boolean IsPerformanceSourceSelectorVisible
        {
            get { return GetIDataProvider().IsPerformanceSourceSelectorVisible; }
        }

        /// <summary>
        /// Gets/Sets the selected performance source id
        /// </summary>
        public Object SelectedPerformanceSourceId
        {
            get { return GetIDataProvider().SelectedPerformanceSourceId; }
            set
            {
                GetIDataProvider().SelectedPerformanceSourceId = value;
            }
        }

        /// <summary>
        /// Returns the is location selector visible flag
        /// </summary>
        public Boolean IsLocationSelectorVisible 
        {
            get { return GetIDataProvider().IsLocationSelectorVisible; } 
        }

        /// <summary>
        /// Gets/Sets the selected location id
        /// </summary>
        public Object SelectedLocationId 
        {
            get { return GetIDataProvider().SelectedLocationId; }
            set { GetIDataProvider().SelectedLocationId = value; }  
        }

        /// <summary>
        /// Returns the is category selector visible flag
        /// </summary>
        public Boolean IsCategorySelectorVisible
        {
            get { return GetIDataProvider().IsCategorySelectorVisible; }
        }

        /// <summary>
        /// Gets/Sets the selected category id
        /// </summary>
        public Object SelectedCategoryId 
        {
            get { return GetIDataProvider().SelectedCategoryId; }
            private set { GetIDataProvider().SelectedCategoryId = value; }  
        }

        /// <summary>
        /// Gets/Sets the selected category.
        /// </summary>
        public ProductGroup SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                _selectedCategory = value;
                OnPropertyChanged(SelectedCategoryProperty);

                this.SelectedCategoryId = (value != null) ? value.Id : 0;
            }
        }


        /// <summary>
        /// Gets the master location list
        /// </summary>
        public LocationInfoList MasterLocationList
        {
            get { return _masterLocationList; }
        }

        ///// <summary>
        ///// Gets the master performance source list
        ///// </summary>
        //public PerformanceSourceList MasterPerformanceSourceList
        //{
        //    get { return _masterPerformanceSourceList; }
        //}


        #region Product Specific

        /// <summary>
        /// Returns true if selected data type is product
        /// </summary>
        public Boolean IsProductData
        {
            get { return _selectedDataType == CCMDataItemType.Product; }
        }

        /// <summary>
        /// Returns true if product next/previous buttons should be shown.
        /// </summary>
        /// <remarks>Starts false, will be set to true if either of the button commands can execute</remarks>
        public Boolean ShowProductButtons
        {
            get { return _showProductButtons; }
            private set
            {
                _showProductButtons = value;
                OnPropertyChanged(ShowProductButtonsProperty);
            }
        }

        /// <summary>
        /// String to show what position in the product list is currently showing
        /// </summary>
        public String ProductSetPosition
        {
            get { return _productSetPosition; }
            private set
            {
                _productSetPosition = value;
                OnPropertyChanged(ProductSetPositionProperty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dataType"></param>
        public ViewDataViewModel(CCMDataItemType dataType)
        {
            _selectedDataType = dataType;

            //set permissions flags based on the type
            Type permObjectType = CCMDataItemTypeHelper.GetPermissionType(this.SelectedDataType);
            if (permObjectType != null)
            {
                _userHasEditPerm = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.EditObject, dataType);
                _userHasDeletePerm = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.DeleteObject, dataType);
            }
            else
            {
                _userHasEditPerm = false;
                _userHasDeletePerm = false;
            }
                

            //populate the initial selected location and performance source
            if (this.IsPerformanceSourceSelectorVisible)
            {
                //_masterPerformanceSourceList = PerformanceSourceList.FetchByEntityId(App.ViewState.EntityId);
                //this.SelectedPerformanceSourceId = (!this.MasterPerformanceSourceList.IsCollectionNullorEmpty()) ? this.MasterPerformanceSourceList.FirstOrDefault().Id : 1;
            }
            if (this.IsLocationSelectorVisible)
            {
                _masterLocationList = LocationInfoList.FetchByEntityId(App.ViewState.EntityId);
                if (this.MasterLocationList.Any())
                {
                    this.SelectedLocationId = this.MasterLocationList.FirstOrDefault().Id;
                }
            }

            // Make sure we start with product set positon correctly.
            ViewDataProductProvider productProvider = this._dataProvider as ViewDataProductProvider;
            if (productProvider != null)
            {
                this.ProductSetPosition = productProvider.ProductSetPosition;
            }
        }

        #endregion

        #region Commands

        #region Edit

        private RelayCommand _editCommand;

        /// <summary>
        /// Edit the selected item if available
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand = new RelayCommand(
                        p => Edit_Executed((Window)p), p => Edit_CanExecute())
                    {
                        FriendlyName = Message.DataManagement_View_EditCommand,
                        SmallIcon = ImageResources.DataManagement_ViewEditData_Edit
                    };
                    base.ViewModelCommands.Add(_editCommand);
                }
                return _editCommand;
            }
        }

        private Boolean Edit_CanExecute()
        {
            if (this.SelectedItem == null)
            {
                return false;
            }

            if (!GetIDataProvider().IsEditSupported)
            {
                this.EditCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (!_userHasEditPerm)
            {
                this.EditCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

           
            return true;
        }


        private void Edit_Executed(Window sendercontrol)
        {
            //call the dataprovider edit.
            GetIDataProvider().EditItem(this.SelectedItem, sendercontrol);

            //Refresh data
            RefreshIDataProvider();

            //Refresh property
            OnPropertyChanged(DataSourceProperty);
        }

        #endregion

        #region Delete

        private RelayCommand _deleteCommand;

        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => DeleteCommand_Executed(),
                        p => DeleteCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean DeleteCommand_CanExecute()
        {
            if (this.SelectedItem == null)
            {
                return false;
            }

            if (!GetIDataProvider().IsDeleteSupported)
            {
                return false;
            }

            if (!_userHasDeletePerm)
            {
                this.DeleteCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            return true;
        }

        private void DeleteCommand_Executed()
        {
            GetIDataProvider().DeleteItem(this.SelectedItem);
        }

        #endregion

        #region NextProductSet

        private RelayCommand _nextProductSetCommand;

        /// <summary>
        /// [GFS-16791] Command used to move to the next x amount of products in the list
        /// </summary>
        /// <remarks>Currently only used when the data source is products</remarks>
        public RelayCommand NextProductSetCommand
        {
            get
            {
                if (_nextProductSetCommand == null)
                {
                    _nextProductSetCommand = new RelayCommand(p => NextProductSet_Executed(), p => NextProductSet_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextProductSetCommand);
                }
                return _nextProductSetCommand;
            }
        }

        private Boolean NextProductSet_CanExecute()
        {
            //data source should be products
            if (IsProductData)
            {
                //Cast datasource as ProductProvider as that is what it will be
                ViewDataProductProvider productProvider = this._dataProvider as ViewDataProductProvider;
                //However may be null if window is loading
                if (productProvider != null)
                {
                    //Check if ProductProvider allows next
                    if (productProvider.NextEnabled)
                    {
                        //If next is enabled then show buttons, there should never be a reason for the buttons to become hidden
                        //when they have previously been shown
                        ShowProductButtons = true;
                        return true;
                    }
                }
            }
            return false;
        }
     

        private void NextProductSet_Executed()
        {
            //Cast to ProductProvider to acces Next method
            ViewDataProductProvider productProvider = this._dataProvider as ViewDataProductProvider;
            if (productProvider != null)
            {
                //Call next method to update data source
                productProvider.NextDataSourcePage();

                //Update product set position string
                ProductSetPosition = productProvider.ProductSetPosition;
                OnPropertyChanged(DataSourceProperty);
            }
            ShowWaitCursor(false);
        }

        #endregion

        #region PreviousProductSet

        private RelayCommand _previousProductSetCommand;

        /// <summary>
        /// [GFS-16791] Command used to move to the previous x amount of products in the list
        /// </summary>
        /// <remarks>Currently only used when the data source is products</remarks>
        public RelayCommand PreviousProductSetCommand
        {
            get
            {
                if (_previousProductSetCommand == null)
                {
                    _previousProductSetCommand = new RelayCommand(p => PreviousProductSet_Executed(), p => PreviousProductSet_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousProductSetCommand);
                }
                return _previousProductSetCommand;
            }
        }

        private Boolean PreviousProductSet_CanExecute()
        {
            //data source should be products
            if (IsProductData)
            {
                //Cast datasource as ProductProvider as that is what it will be
                ViewDataProductProvider productProvider = this._dataProvider as ViewDataProductProvider;
                //However may be null if window is loading
                if (productProvider != null)
                {
                    //Check if ProductProvider allows previous
                    if (productProvider.PreviousEnabled)
                    {
                        //If previous is enabled then show buttons, there should never be a reason for the buttons to become hidden
                        //when they have previously been shown
                        ShowProductButtons = true;
                        return true;
                    }
                }
            }

            return false;
        }

        private void PreviousProductSet_Executed()
        {
            ShowWaitCursor(true);
            //Cast to ProductProvider to acces Previous method
            ViewDataProductProvider productProvider = this._dataProvider as ViewDataProductProvider;
            if (productProvider != null)
            {
                //Call previous method to update data source
                productProvider.PreviousDataSourcePage();
                //Update product set position string
                ProductSetPosition = productProvider.ProductSetPosition;
                OnPropertyChanged(DataSourceProperty);
            }
            ShowWaitCursor(false);
        }

        #endregion

        #region Close

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the window
        /// Prompts the user to save or cancel any outstanding changes
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }


        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SetSelectedCategory

        private RelayCommand _setSelectedCategoryCommand;

        /// <summary>
        /// Shows the diaglog for the user to select a merchandsing group.
        /// </summary>
        public RelayCommand SetSelectedCategoryCommand
        {
            get
            {
                if (_setSelectedCategoryCommand == null)
                {
                    _setSelectedCategoryCommand = new RelayCommand(
                        p => SetSelectedCategory_Executed());
                }
                return _setSelectedCategoryCommand;
            }
        }

        private void SetSelectedCategory_Executed()
        {
            throw new NotImplementedException();
            //Common.MerchGroupSelectionWindow win = new Common.MerchGroupSelectionWindow();
            //win.SelectionMode = DataGridSelectionMode.Single;
            //App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

            //if (win.SelectionResult != null && win.SelectionResult.Count > 0)
            //{
            //    this.SelectedCategory = win.SelectionResult[0];
            //}
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Populates the columnset property.
        /// </summary>
        private void CreateColumnSet()
        {
            if (_columnSet.Count > 0)
            {
                _columnSet.Clear();
            }

            //create the fixed columns
            if (this.AttachedControl != null && GetIDataProvider().IsEditSupported)
            {
                DataGridExtendedTemplateColumn templateCol = new DataGridExtendedTemplateColumn();
                templateCol.Header = String.Empty;
                templateCol.IsReadOnly = true;
                templateCol.Width = 30;
                templateCol.CanUserResize = false;
                templateCol.CanUserReorder = false;
                templateCol.CellTemplate = this.AttachedControl.Resources[EditColumnDTKey] as DataTemplate;
                templateCol.CanUserFilter = false;
                templateCol.CanUserHide = false;

                _columnSet.Add(templateCol);
            }

            //add the datatype columns 
            foreach (DataGridColumn col in GetIDataProvider().ColumnSet)
            {
                _columnSet.Add(col);
            }
        }

        

        /// <summary>
        /// Returns the data provider for the current type
        /// </summary>
        /// <returns></returns>
        private IViewDataProvider GetIDataProvider()
        {
            if (_dataProvider == null)
            {
                switch (_selectedDataType)
                {
                    case CCMDataItemType.Location:
                        _dataProvider = new ViewDataLocationProvider();
                        break;

                    case CCMDataItemType.LocationClusters:
                        _dataProvider = new ViewDataLocationClusterProvider();
                        break;

                    case CCMDataItemType.Product:
                        _dataProvider = new ViewDataProductProvider();
                        break;


                    case CCMDataItemType.MerchandisingHierarchy:
                        _dataProvider = new ViewDataMerchHierarchyProvider();
                        break;

                    case CCMDataItemType.LocationHierarchy:
                        _dataProvider = new ViewDataLocationHierarchyProvider();
                        break;

                    //case CCMDataItemType.LocationProductPerformance:
                    //    _dataProvider = new ViewDataProductPerformanceProvider();
                    //    break;

                    case CCMDataItemType.LocationSpace:
                        _dataProvider = new ViewDataLocationSpaceProvider();
                        break;

                    case CCMDataItemType.LocationSpaceBay:
                        _dataProvider = new ViewDataLocationSpaceBayProvider();
                        break;

                    case CCMDataItemType.LocationSpaceElement:
                        _dataProvider = new ViewDataLocationSpaceElementProvider();
                        break;

                    case CCMDataItemType.LocationProductAttributes:
                        _dataProvider = new ViewDataLocationProductAttributeProvider();
                        break;

                    case CCMDataItemType.LocationProductIllegal:
                        _dataProvider = new ViewDataLocationProductIllegalProvider();
                        break;

                    case CCMDataItemType.LocationProductLegal:
                        _dataProvider = new ViewDataLocationProductLegalProvider();
                        break;

                    case CCMDataItemType.Assortment:
                        _dataProvider = new ViewDataAssortmentProvider();
                        break;

                    default: throw new NotImplementedException();
                }

                _dataProvider.PropertyChanged += DataProvider_PropertyChanged;
            }
            return _dataProvider;
        }

        /// <summary>
        /// Recreates the data provider to refresh the data
        /// </summary>
        /// <returns></returns>
        private IViewDataProvider RefreshIDataProvider()
        {
            if (_dataProvider != null)
            {
                //Unsubscrube from old property changed
                _dataProvider.PropertyChanged -= DataProvider_PropertyChanged;

                //Refresh data provider
                _dataProvider = GetIDataProvider();
                
                _dataProvider.PropertyChanged += DataProvider_PropertyChanged;
            }
            return _dataProvider;
        }


        #endregion

        #region EventHandlers

        private void DataProvider_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == IViewDataProviderHelper.DataSourceProperty.Path)
            {
                OnPropertyChanged(DataSourceProperty);
            }
            else if (e.PropertyName == IViewDataProviderHelper.ColumnSetProperty.Path)
            {
                CreateColumnSet(); //recreate the col set.
                OnPropertyChanged(ColumnSetProperty);
            }
            else if (e.PropertyName == IViewDataProviderHelper.IsLoadingDataProperty.Path)
            {
                //fire property changed for this on a delay so that any grid loading can complete first
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Dispatcher.BeginInvoke(
                        (Action)(() =>
                            {
                                OnPropertyChanged(IsLoadingDataProperty);
                            }), priority: System.Windows.Threading.DispatcherPriority.ContextIdle);
                }
                else
                {
                    OnPropertyChanged(IsLoadingDataProperty);
                }
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (_dataProvider != null)
                    {
                        _dataProvider.PropertyChanged -= DataProvider_PropertyChanged;

                        //dispose of the data provider if required
                        IDisposable disposableProvider = _dataProvider as IDisposable;
                        if (disposableProvider != null)
                        {
                            disposableProvider.Dispose();
                        }
                        _dataProvider = null;
                    }
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
