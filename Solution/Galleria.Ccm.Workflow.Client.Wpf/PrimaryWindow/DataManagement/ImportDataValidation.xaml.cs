﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25557 : L.Hodson ~ Copied from SA.
#endregion

#endregion


using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDefinitionValidation.xaml
    /// </summary>
    public partial class ImportDataValidation : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportDataValidationViewModel), typeof(ImportDataValidation),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportDataValidationViewModel ViewModel
        {
            get { return (ImportDataValidationViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportDataValidation senderControl = (ImportDataValidation)obj;

            if (e.OldValue != null)
            {
                ImportDataValidationViewModel oldModel = (ImportDataValidationViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ImportDataValidationViewModel newModel = (ImportDataValidationViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }



        #endregion

        #endregion

        #region Constructor

        public ImportDataValidation(ImportDataValidationViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to property changes in the viewModel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ImportDataValidationViewModel.SelectedErrorItemProperty.Path)
            {
                BringSelectedErrorItemIntoView();
            }
        }

        /// <summary>
        /// Event handler for the error row entry being expanded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRowExpander_Expanded(object sender, RoutedEventArgs e)
        {
            //Cast object to dependency
            DependencyObject obj = (DependencyObject)e.OriginalSource;

            //Find the objects parent in the visual tree
            obj = VisualTreeHelper.GetParent(obj);

            //Find the visual ancester of the parent
            DataGridRow dataGridRow = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<DataGridRow>(obj);

            //If data grid row is found
            if (dataGridRow != null)
            {
                dataGridRow.DetailsVisibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Event handler for the error row entry being collapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRowExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            //Cast object to dependency
            DependencyObject obj = (DependencyObject)e.OriginalSource;

            //Find the objects parent in the visual tree
            obj = VisualTreeHelper.GetParent(obj);

            //Find the visual ancester of the parent
            DataGridRow dataGridRow = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<DataGridRow>(obj);

            //If data grid row is found
            if (dataGridRow != null)
            {
                dataGridRow.DetailsVisibility = Visibility.Collapsed;
            }
        }
        /// <summary>
        /// 508 compliance - expand/collapse using Space Key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProblemGridExpander_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                var senderControl = sender as Expander;
                if (senderControl == null) return;
                senderControl.IsExpanded = !senderControl.IsExpanded;
                e.Handled = true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reponds to a change of selected error item -
        /// scrolls to the cell containing the error
        /// </summary>
        private void BringSelectedErrorItemIntoView()
        {
            ValidationErrorItem errorItem = this.ViewModel.SelectedErrorItem;

            //Check if attached control is not null
            if (errorItem != null)
            {
                int columnIndex = (errorItem.ColumnNumber + 1); //-1 for 0 based index, +2 to compensate for first 2 columns

                //[ISO-13318] Actually find the row in question and get its index
                ImportFileDataRow matchingRow = this.ViewModel.ValidatedFileData.Rows.FirstOrDefault(r => r.RowNumber == errorItem.RowNumber);
                if (matchingRow != null)
                {
                    //Get index
                    int rowIndex = this.ViewModel.ValidatedFileData.Rows.IndexOf(matchingRow);

                    //Check indexes are valid
                    if (columnIndex <= (this.validationDataGrid.Columns.Count - 1))
                    {
                        //Get column at the index
                        DataGridColumn viewColumn = this.validationDataGrid.Columns[columnIndex];
                        //Scroll to view cell
                        this.validationDataGrid.ScrollIntoView(this.validationDataGrid.Items[rowIndex], viewColumn);
                        //Select this row
                        this.validationDataGrid.SelectedIndex = rowIndex;
                    }
                }
            }
        }

        #endregion
    }
}
