﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for OptionsWindow.xaml
    /// </summary>
    public sealed partial class OptionsWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(OptionsViewModel), typeof(OptionsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            OptionsWindow senderControl = (OptionsWindow)obj;

            if (e.OldValue != null)
            {
                OptionsViewModel oldModel = (OptionsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                OptionsViewModel newModel = (OptionsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        public OptionsViewModel ViewModel
        {
            get { return (OptionsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public OptionsWindow(OptionsViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.Options);

            this.ViewModel = viewModel;

            this.Loaded += This_Loaded;
        }

        /// <summary>
        /// Called on initial load of this window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void This_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= This_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers



        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
