﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
#endregion
#region Version History : CCM 830
// V8-32823  : J.Pickup
//  Export visbiltity work (New properties).
#endregion

#endregion

using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstageSetupTabContent.xaml
    /// </summary>
    public sealed partial class BackstageSetupTabContent : UserControl
    {
        #region ViewModelProperty
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(BackstageSetupTabContent));
       
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion


        /// <summary>
        /// Gets/Sets the visibility of the export functionality.
        /// </summary>
        public Visibility IsExportFunctionalityAvailable
        {
            get
            {
                if (App.ViewState.IsExportApolloAvailable || App.ViewState.IsExportJDAAvailable || App.ViewState.IsExportSpacemanAvailable)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }


        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public BackstageSetupTabContent()
        {
            //dont initialize until we are visible.
            this.IsVisibleChanged += BackstageSetupTabContent_IsVisibleChanged;
        }

        /// <summary>
        /// Called when this is first made visible.
        /// </summary>
        private void BackstageSetupTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(this.IsVisible)
            {
                //Unsubscribe
                this.IsVisibleChanged -= BackstageSetupTabContent_IsVisibleChanged;

                //Initialize
                InitializeComponent();
                HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageSetup);

            }
        }

    }
}
