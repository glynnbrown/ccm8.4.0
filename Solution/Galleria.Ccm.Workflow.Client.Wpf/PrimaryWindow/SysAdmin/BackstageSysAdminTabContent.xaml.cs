﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014.

#region Version History: (CCM 8.0)
// CCM-25970 : L.Ineson
//  Created
#endregion
#endregion

using System.Windows;
using System.Windows.Controls;


namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin
{
    /// <summary>
    /// Interaction logic for BackstageSysAdminTabContent.xaml
    /// </summary>
    public sealed partial class BackstageSysAdminTabContent : UserControl
    {

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstageSysAdminViewModel), typeof(BackstageSysAdminTabContent),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public BackstageSysAdminViewModel ViewModel
        {
            get { return (BackstageSysAdminViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageSysAdminTabContent senderControl = (BackstageSysAdminTabContent)obj;

            if (e.OldValue != null)
            {
                BackstageSysAdminViewModel oldModel = (BackstageSysAdminViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                BackstageSysAdminViewModel newModel = (BackstageSysAdminViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

            }
        }

        #endregion


        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public BackstageSysAdminTabContent()
        {
            //Nb- dont initialize until this is made visible.
            this.IsVisibleChanged += BackstageSysAdminTabContent_IsVisibleChanged;
        }

        /// <summary>
        /// Called when this is first made visible.
        /// </summary>
        private void BackstageSysAdminTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
           if(this.IsVisible)
            {
                //unsubscribe
                this.IsVisibleChanged -= BackstageSysAdminTabContent_IsVisibleChanged;

                //Initialize
                InitializeComponent();
                HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageSysAdmin);
                this.ViewModel = new BackstageSysAdminViewModel();

            }
        }

    }
}
