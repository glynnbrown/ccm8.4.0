﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName property, null for now but will be expanded.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EventLogMaintenance
{
    public sealed class EventLogViewModel : ViewModelAttachedControlObject<EventLogWindow>
    {
        #region Fields
        //Entity Info Buttons
        private EntityInfoListViewModel _masterEntityInfoListView;
        private BulkObservableCollection<EntityInfo> _availableEntityInfos = new BulkObservableCollection<EntityInfo>();
        private ReadOnlyBulkObservableCollection<EntityInfo> _availableEntityInfoRO;
        private EntityInfo _selectedEntityInfo;
        // Event Log Name ListBox
        private EventLogNameListViewModel _masterEventLogNameListView;
        private BulkObservableCollection<EventLogName> _availableEventLogNames = new BulkObservableCollection<EventLogName>();
        private ReadOnlyBulkObservableCollection<EventLogName> _availableEventLogNameRO;
        private EventLogName _selectedEventLogName;
        //Event Log view model
        private EventLogListViewModel _masterEventLogListView;

        //Event Log view model wrap
        private ReadOnlyBulkObservableCollection<EventLogWindowWrap> _availableEventLogsWrapRO;
        private BulkObservableCollection<EventLogWindowWrap> _availableEventLogsWrap = new BulkObservableCollection<EventLogWindowWrap>();
        private ObservableCollection<EventLogWindowWrap> _selectedEventLogsWrap = new ObservableCollection<EventLogWindowWrap>();
        private EventLogWindowWrap _selectedEventLogWrap;

        private Boolean _addedAllLogs;
        private const Int32 _AllLogsId = -1;

        private SummariseType _currentSelectedDateTimeGroup;
        private Nullable<Int32> _currentSelectedEventLogNameId;
        private Nullable<Int16> _currentSelectedEntryType;
        private Nullable<Int32> _currentSelectedEntityId;
        private Int16 _currentSelectedRecordTopCount;
        private Boolean _currrentClipboardAll;


        //private Nullable<Int32> _currentSelectedEventLogNameId;
        private String _currentSelectedSource;
        private Nullable<Int32> _currentSelectedEventId;
        //private Nullable<Int16> _currentSelectedEntryType;
        private Nullable<Int32> _currentSelectedUserId;
        //private Nullable<Int32> _currentSelectedEntityId;
        private Nullable<DateTime> _currentSelectedDateTime;
        private String _currentSelectedProcess;
        private String _currentSelectedComputerName;
        private String _currentSelectedUrlHelperLink;
        private String _currentSelectedDescritpion;
        private String _currentSelectedContent;
        private String _currentSelectedGibraltarSessionId;

        private DateTime _startDate;
        private DateTime _endDate;

        #endregion

        #region Events
        /// <summary>
        /// Summarise Data Event
        /// </summary>
        public event EventHandler SummariseData;
        public void OnSummariseData()
        {
            if (SummariseData != null)
            {
                this.SummariseData(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Clipboard Data Event
        /// </summary>
        public event EventHandler ClipboardData;
        public void OnClipboardData()
        {
            if (ClipboardData != null)
            {
                this.ClipboardData(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Property Changed Event
        /// </summary>
        public event EventHandler PropertyChanged;
        public void OnPropertyChanged()
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Binding Property Paths
        //Entity Info Buttons
        public static readonly PropertyPath SelectedEntityInfoProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.SelectedEntityInfo);
        // Event Log Name ListBox
        public static readonly PropertyPath AvailableEventLogNamesProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.AvailableEventLogNames);
        public static readonly PropertyPath SelectedEventLogNameProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.SelectedEventLogName);
        //Event Log Data Grid (wrap)
        public static PropertyPath SelectedEventLogWrapProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(m => m.SelectedEventLogWrap);
        public static PropertyPath AvailableEventLogsWrapProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(m => m.AvailableEventLogsWrap);
        public static PropertyPath SelectedEventLogsWrapProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(m => m.SelectedEventLogsWrap);
        // ComboBox for Record Top Count (NOT SHOWN AN THE MOMENT)
        public static readonly PropertyPath SelectedRecordTopCountProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.SelectedRecordTopCount);

        public static readonly PropertyPath AvailableEntityInfoProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.AvailableEntityInfo);
        public static readonly PropertyPath CurrentRecordTopCountProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.CurrentRecordTopCount);
        public static readonly PropertyPath CurrentDataTimeGroupProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.CurrentDataTimeGroup);
        public static readonly PropertyPath CurrentCopyToClipboardProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.CurrentCopyToClipboard);

        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.CloseCommand);

        public static readonly PropertyPath StartDateProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.StartDate);
        public static readonly PropertyPath EndDateProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.EndDate);

        public static readonly PropertyPath StartTimeHourProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.StartTimeHour);
        public static readonly PropertyPath EndTimeHourProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.EndTimeHour);
        public static readonly PropertyPath StartTimeMinuteProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.StartTimeMinute);
        public static readonly PropertyPath EndTimeMinuteProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.EndTimeMinute);

        #endregion

        #region Properties

        /// <summary>
        /// Returns a readonly collection of entites available for selection
        /// </summary>
        public ReadOnlyBulkObservableCollection<EntityInfo> AvailableEntityInfo
        {
            get
            {
                if (_availableEntityInfoRO == null)
                {
                    _availableEntityInfoRO = new ReadOnlyBulkObservableCollection<EntityInfo>(_availableEntityInfos);
                }
                return _availableEntityInfoRO;
            }
        }

        /// <summary>
        /// Returns the current selected entity for the entity filter button
        /// </summary>
        public EntityInfo SelectedEntityInfo
        {
            get { return _selectedEntityInfo; }
            set
            {
                _selectedEntityInfo = value;

            }
        }

        /// <summary>
        /// Gets/Sets the comboBox (NOT SHOWN AT THIS TIME)
        /// </summary>
        public Int16 SelectedRecordTopCount
        {
            get { return _currentSelectedRecordTopCount; }
            set
            {
                _currentSelectedRecordTopCount = value;
                LoadEventLogGrid();
            }
        }

        /// <summary>
        /// Gets/Sets the event log window wrap
        /// </summary>
        public EventLogWindowWrap SelectedEventLogWrap
        {
            get { return _selectedEventLogWrap; }
            set
            {
                _selectedEventLogWrap = value;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected metrics
        /// </summary>
        public ObservableCollection<EventLogWindowWrap> SelectedEventLogsWrap
        {

            get { return _selectedEventLogsWrap; }
            set
            {
                _selectedEventLogsWrap = value;
            }
        }

        /// <summary>
        /// Returns a readonly collection of event log names for the wraper
        /// </summary>
        public ReadOnlyBulkObservableCollection<EventLogWindowWrap> AvailableEventLogsWrap
        {
            get
            {
                if (_availableEventLogsWrapRO == null)
                {
                    _availableEventLogsWrapRO = new ReadOnlyBulkObservableCollection<EventLogWindowWrap>(_availableEventLogsWrap);
                }
                return _availableEventLogsWrapRO;
            }
        }

        /// <summary>
        /// Returns a readonly collection of event log names
        /// </summary>
        public ReadOnlyBulkObservableCollection<EventLogName> AvailableEventLogNames
        {
            get
            {
                if (_availableEventLogNameRO == null)
                {
                    _availableEventLogNameRO = new ReadOnlyBulkObservableCollection<EventLogName>(_availableEventLogNames);
                }
                return _availableEventLogNameRO;
            }
        }

        /// <summary>
        /// Returns the current selected event log name
        /// </summary>
        public EventLogName SelectedEventLogName
        {
            get { return _selectedEventLogName; }
            set
            {
                _selectedEventLogName = value;
                //Need to check to see if this value is null 
                // because when the event log is clear the 'value' returns null                
                if (_selectedEventLogName != null)
                {
                    EventLogNameSelected(_selectedEventLogName.Id);
                }
            }
        }

        /// <summary>
        /// Current Record Top Count from ComboBox (NOT SHOWN AT THIS TIME)
        /// </summary>
        public Int16 CurrentRecordTopCount
        {
            get { return _currentSelectedRecordTopCount; }
            set
            {
                _currentSelectedRecordTopCount = value;

            }
        }

        /// <summary>
        /// Property passed to show if user selected a summarise value
        /// </summary>
        public SummariseType CurrentDataTimeGroup
        {
            get { return _currentSelectedDateTimeGroup; }
            set
            {
                _currentSelectedDateTimeGroup = value;
            }
        }

        /// <summary>
        /// Property passed to show if user selected All or Selected row copy to clipboard
        /// 
        /// </summary>
        public Boolean CurrentCopyToClipboard
        {
            get { return _currrentClipboardAll; }
            set
            {
                _currrentClipboardAll = value;
            }
        }

        /// <summary>
        /// The requested Start date for visible logs in local time
        /// </summary>
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                value = value.AddHours(_startDate.Hour);
                value = value.AddMinutes(_startDate.Minute);
                value = value.AddSeconds(_startDate.Second);
                _startDate = value;
                LoadEventLogGrid();
            }
        }

        /// <summary>
        /// The requested End date for visible logs in local time
        /// </summary>
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                value = value.AddHours(_endDate.Hour);
                value = value.AddMinutes(_endDate.Minute);
                value = value.AddSeconds(_endDate.Second);
                _endDate = value;
                LoadEventLogGrid();
            }
        }

        /// <summary>
        /// The requested Start time minutes for visible logs
        /// </summary>
        public Int32 StartTimeHour
        {
            get { return _startDate.Hour; }
            set
            {
                Int32 hours = value;
                if (hours > 23)
                {
                    hours = 23;
                }
                if (hours < 0)
                {
                    hours = 0;
                }
                _startDate = new DateTime(_startDate.Year, _startDate.Month, _startDate.Day, hours, _startDate.Minute, _startDate.Second);
                OnPropertyChanged(StartTimeHourProperty);
                LoadEventLogGrid();
            }
        }

        /// <summary>
        /// The requested Start date minutes for visible logs
        /// </summary>
        public Int32 StartTimeMinute
        {
            get { return _startDate.Minute; }
            set
            {
                Int32 mins = value;
                if (mins > 59)
                {
                    mins = 59;
                }
                if (mins < 0)
                {
                    mins = 0;
                }
                _startDate = new DateTime(_startDate.Year, _startDate.Month, _startDate.Day, _startDate.Hour, mins, _startDate.Second);
                OnPropertyChanged(StartTimeMinuteProperty);
                LoadEventLogGrid();
            }
        }

        /// <summary>
        /// The requested End date hour for visible logs
        /// </summary>
        public Int32 EndTimeHour
        {
            get { return _endDate.Hour; }
            set
            {
                Int32 hours = value;
                if (hours > 23)
                {
                    hours = 23;
                }
                if (hours < 0)
                {
                    hours = 0;
                }
                _endDate = new DateTime(_endDate.Year, _endDate.Month, _endDate.Day, hours, _endDate.Minute, _endDate.Second);
                OnPropertyChanged(EndTimeHourProperty);
                LoadEventLogGrid();
            }
        }

        /// <summary>
        /// The requested End date minutes for visible logs
        /// </summary>
        public Int32 EndTimeMinute
        {
            get { return _endDate.Minute; }
            set
            {
                Int32 mins = value;
                if (mins > 59)
                {
                    mins = 59;
                }
                if (mins < 0)
                {
                    mins = 0;
                }
                _endDate = new DateTime(_endDate.Year, _endDate.Month, _endDate.Day, _endDate.Hour, mins, _endDate.Second);
                OnPropertyChanged(EndTimeMinuteProperty);
                LoadEventLogGrid();
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor if no fitlers has been passed to open up the event log form
        /// </summary>
        public EventLogViewModel()
            : this(new EventLogNameListViewModel(), new EventLogListViewModel(), new EntityInfoListViewModel(),
                    null, null, null, null, null, null, null, null, null, null, null, null, null)
        { }

        /// <summary>
        /// Main Constructor if one or more filters has been passed to open up the event log form
        /// </summary>
        public EventLogViewModel(Nullable<Int32> eventLogNameId, String source, Nullable<Int32> eventId,
            Nullable<Int16> entryType, Nullable<Int32> userId, Nullable<Int32> entityId,
            Nullable<DateTime> dateTime, String process, String computerName,
            String urlHelperLink, String description, String content, String gibraltarSessionId)
            : this(new EventLogNameListViewModel(), new EventLogListViewModel(), new EntityInfoListViewModel(),
            eventLogNameId, source, eventId, entryType, userId, entityId, dateTime, process, computerName, urlHelperLink, description,
            content, gibraltarSessionId)
        { }

        /// <summary>
        /// event log view model start up.
        /// </summary>
        public EventLogViewModel(EventLogNameListViewModel EventLogNameView, EventLogListViewModel EventLogView, EntityInfoListViewModel EntityInfoView,
                                Nullable<Int32> eventLogNameId, String source, Nullable<Int32> eventId, Nullable<Int16> entryType,
                                Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> dateTime,
                                String process, String computerName, String urlHelperLink, String description,
                                String content, String gibraltarSessionId)
        {
            //Set filter values to (reset)
            if (entryType == null) entryType = 1; // Default Entry Type is set to Error Entry Type
            _currentSelectedDateTimeGroup = SummariseType.None;
            _currentSelectedEventLogNameId = eventLogNameId;
            _currentSelectedEntryType = entryType;
            _currentSelectedEntityId = entityId;
            _currentSelectedRecordTopCount = 100;

            _currentSelectedSource = source;
            _currentSelectedEventId = eventId;
            _currentSelectedUserId = userId;
            _currentSelectedDateTime = dateTime;
            _currentSelectedProcess = process;
            _currentSelectedComputerName = computerName;
            _currentSelectedUrlHelperLink = urlHelperLink;
            _currentSelectedDescritpion = description;
            _currentSelectedContent = content;
            _currentSelectedGibraltarSessionId = gibraltarSessionId;


            //DateTime nowDate = DateTime.UtcNow.ToLocalTime();
            DateTime localNowDate = DateTime.Now.ToLocalTime();
            _endDate = new DateTime(localNowDate.Year, localNowDate.Month, localNowDate.Day, 23, 59, 59);
            _startDate = new DateTime(localNowDate.Year, localNowDate.Month, localNowDate.Day, 0, 0, 0);



            StartUpEventLogName(EventLogNameView);

            StartUpEventLog(EventLogView);

            StartUpEntityInfoView(EntityInfoView);

            //load a new entity
            this.NewCommand.Execute();
        }

        //on start up event log set up the event log grid master list
        private void StartUpEventLog(EventLogListViewModel EventLogView)
        {
            //Load All Event Logs List
            _masterEventLogListView = EventLogView;
            _masterEventLogListView.ModelChanged += MasterEventLogListView_ModelChanged;
            if (_masterEventLogListView.Model == null)
            {
                LoadEventLogGrid();
            }
        }

        //on start up event log set up the event log name master list
        private void StartUpEventLogName(EventLogNameListViewModel EventLogNameView)
        {
            //Event Log Name List
            _masterEventLogNameListView = EventLogNameView;
            _masterEventLogNameListView.ModelChanged += MasterEventLogNameListView_ModelChanged;

            if (_masterEventLogNameListView.Model == null)
            {
                //Add "All Logs" to ListBox
                Galleria.Ccm.Model.EventLogName newEventLogName =
                    Galleria.Ccm.Model.EventLogName.NewEventLogName(_AllLogsId, Message.EventLog_LogName_AllLogs);
                _availableEventLogNames.Add(newEventLogName);

                //Load Log Names to ListBox (Don't wipe clean because of "All Logs" item)
                _addedAllLogs = true;
                _masterEventLogNameListView.FetchAll();
                _addedAllLogs = false;
            }
        }

        // fetch a list of entitys for the entity filter button
        private void StartUpEntityInfoView(EntityInfoListViewModel EntityInfoView)
        {
            //Event Log Name List
            _masterEntityInfoListView = EntityInfoView;
            _masterEntityInfoListView.ModelChanged += MasterEntityInfoListView_ModelChanged;

            if (_masterEntityInfoListView.Model == null)
            {
                _addedAllLogs = true;
                _masterEntityInfoListView.FetchAll();
                _addedAllLogs = false;
            }
        }

        /// <summary>
        /// Load Event Log Grid (FetchBy EventLogNameId; EntryType; EntityId)
        /// </summary>
        private void LoadEventLogGrid()
        {
            // fetch the data need for the data grid
            this.ShowWaitCursor(true);

            //start/end dates are in local time,but we fetch 
            _masterEventLogListView.FetchByMixedCriteria(_currentSelectedRecordTopCount, _currentSelectedEventLogNameId,
                            _currentSelectedSource, _currentSelectedEventId, _currentSelectedEntryType, _currentSelectedUserId,
                       _currentSelectedEntityId, _currentSelectedDateTime, _currentSelectedProcess, _currentSelectedComputerName, _currentSelectedUrlHelperLink, _currentSelectedDescritpion,
                       _currentSelectedContent, _currentSelectedGibraltarSessionId, _startDate.ToUniversalTime(), _endDate.ToUniversalTime(), null, null);

            this.ShowWaitCursor(false);
        }

        /// <summary>
        /// Populate Event Log Grid (used to add Wrap around model for summerise data)
        /// </summary>
        private void PopulateEventLogGrid()
        {
            if (_availableEventLogsWrap.Count > 0)
            {
                _availableEventLogsWrap.Clear();
            }

            List<EventLogWindowWrap> wrappedObjects = new List<EventLogWindowWrap>();
            foreach (Galleria.Ccm.Model.EventLog log in _masterEventLogListView.Model.OrderByDescending(l => l.DateTime))
            {
                _availableEventLogsWrap.Add(new EventLogWindowWrap(log, _currentSelectedDateTimeGroup));
            }
        }

        /// <summary>
        /// Event log name has been selected
        /// </summary>
        private void EventLogNameSelected(Nullable<Int32> eventLogNameId)
        {
            //Check to see if new event log name is not the same as old.
            if (eventLogNameId != _currentSelectedEventLogNameId)
            {
                //Check to see if "All Logs" item has been selected
                if (eventLogNameId == _AllLogsId)
                {
                    //set current selected event log name to null (All Logs)
                    _currentSelectedEventLogNameId = null;
                }
                else
                {
                    //set current seleced event  log name to correct event log name ID                   
                    _currentSelectedEventLogNameId = eventLogNameId;
                }
                //Load Grid
                LoadEventLogGrid();

                //Check summarise groupings
                PopulateEventLogGrid();
            }
        }

        /// <summary>
        /// Show Save Export Dialog box and populate and save excel export
        /// </summary>
        private void ExportExcelSave(Boolean AllGridExported)
        {

            #region Build Filename Dialog
            //get the filename to save as
            Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
            saveDialog.CreatePrompt = false;
            saveDialog.Filter = Message.BackstageDataManagement_ExportFilter;
            saveDialog.FilterIndex = 2;
            saveDialog.FileName = Message.EventLog_Tab_EventLog + ".xlsx";
            saveDialog.OverwritePrompt = true;
            #endregion

            if (saveDialog.ShowDialog() == true)
            {
                Boolean canContinue = true;

                //If the selected file type is XLS
                if (saveDialog.FilterIndex == 1)
                {
                    #region Change Excel File Type
                    //Show warning dialog informing user that if > 65k rows, they will not be outputted
                    //as it exceeds this file type row limit.
                    ModalMessage modelMessageWindow = new ModalMessage();
                    modelMessageWindow.Description = Message.BackstageDataManagement_RowNumberExceedsMaxLimitWarning;
                    modelMessageWindow.ButtonCount = 2;
                    modelMessageWindow.Button1Content = Message.Generic_Ok;
                    modelMessageWindow.Button2Content = Message.Generic_Cancel;
                    modelMessageWindow.DefaultButton = ModalMessageButton.Button1;
                    modelMessageWindow.CancelButton = ModalMessageButton.Button2;
                    modelMessageWindow.Icon = ImageResources.Warning_16;
                    modelMessageWindow.ShowDialog();
                    #endregion

                    //set the result
                    canContinue = (modelMessageWindow.Result == ModalMessageResult.Button1);
                }

                if (canContinue)
                {
                    IEnumerable<Model.EventLog> selectedEventLogs;

                    //has user selected All grid to export or just the selected rows to export
                    if (AllGridExported == true)
                    {
                        //Select all the rows from the grid
                        selectedEventLogs = this.AvailableEventLogsWrap.Select(i => i.DataType);
                    }
                    else
                    {
                        //Select only the selected rows from the grid
                        selectedEventLogs = this.SelectedEventLogsWrap.Select(i => i.DataType);
                    }


                    try
                    {
                        //Process won't be able to report back if the file's in use, so check here first
                        //  if file is ok now but in use later on, a warning will be logged from within the process
                        if (System.IO.File.Exists(saveDialog.FileName))
                        {
                            Stream stream = System.IO.File.Open(saveDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.None);
                            stream.Close();
                        }

                        Galleria.Ccm.Processes.ExportEventLog.Process process =
                        new Processes.ExportEventLog.Process(saveDialog.FileName, false, selectedEventLogs);

                        ProcessFactory<Galleria.Ccm.Processes.ExportEventLog.Process> processFactory =
                            new ProcessFactory<Processes.ExportEventLog.Process>();

                        processFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<Processes.ExportEventLog.Process>>(processFactory_ProcessCompleted);
                        processFactory.Execute(process);
                    }
                    catch (IOException exception)
                    {
                        //file is already opened elsewhere
                        ModalMessage msg = new ModalMessage();
                        msg.Title = Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                        msg.Header = Message.Generic_Error;
                        msg.Description = exception.Message;
                        msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                        msg.Owner = this.AttachedControl != null ? this.AttachedControl : Application.Current.MainWindow;
                        msg.ButtonCount = 1;
                        msg.Button1Content = Message.Generic_Ok;
                        msg.DefaultButton = ModalMessageButton.Button1;
                        msg.MessageIcon = ImageResources.Dialog_Error;
                        msg.ShowDialog();
                        canContinue = false;
                    }

                }
            }
        }

        void processFactory_ProcessCompleted(object sender, ProcessCompletedEventArgs<Processes.ExportEventLog.Process> e)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new event log object
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => NewCommand_Executed(),
                        p => NewCommand_CanExecute())
                    {
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean NewCommand_CanExecute()
        {
            //user must have create permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(Galleria.Ccm.Model.EventLog)))
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void NewCommand_Executed()
        {
            if (ContinueWithItemChange())
            {
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region ExportToExcelCommand

        private RelayCommand _exportToExcelCommand;

        /// <summary>
        /// Exports the resources list to a selected excel file
        /// </summary>
        public RelayCommand ExportToExcelCommand
        {
            get
            {
                if (_exportToExcelCommand == null)
                {
                    _exportToExcelCommand = new RelayCommand(
                        p => ExportToExcel_Executed())
                    {
                        FriendlyName = Message.EventLog_ExportToExcel,
                        FriendlyDescription = Message.EventLog_ExportToExcel_Description,
                        Icon = ImageResources.EventLog_ExportToExcel
                    };
                    base.ViewModelCommands.Add(_exportToExcelCommand);
                }
                return _exportToExcelCommand;
            }
        }

        private void ExportToExcel_Executed()
        {
            //Menu button
        }

        #endregion

        #region ExportToExcelAllCommand

        private RelayCommand _exportToExcelAllCommand;

        /// <summary>
        /// Exports the data grid to a selected excel file
        /// </summary>
        public RelayCommand ExportToExcelAllCommand
        {
            get
            {
                if (_exportToExcelAllCommand == null)
                {
                    _exportToExcelAllCommand = new RelayCommand(
                        p => ExportToExcelAll_Executed(),
                        p => ExportToExcelAll_CanExecute())
                    {
                        FriendlyName = Message.EventLog_ExportToExcelAll,
                        FriendlyDescription = Message.EventLog_ExportToExcelAll_Description,
                        Icon = ImageResources.EventLog_ExportToExcelAll
                    };
                    base.ViewModelCommands.Add(_exportToExcelAllCommand);
                }
                return _exportToExcelAllCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ExportToExcelAll_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any())
            {
                _exportToExcelAllCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }

            return true;
        }

        private void ExportToExcelAll_Executed()
        {
            //Export to excel with save dialog box
            ExportExcelSave(true);
        }

        #endregion

        #region ExportToExcelSelectionCommand

        private RelayCommand _exportToExcelSelectionCommand;

        /// <summary>
        /// Exports the selected rowsfrom the data grid to a selected excel file
        /// </summary>
        public RelayCommand ExportToExcelSelectionCommand
        {
            get
            {
                if (_exportToExcelSelectionCommand == null)
                {
                    _exportToExcelSelectionCommand = new RelayCommand(
                        p => ExportToExcelSelection_Executed(),
                        p => ExportToExcelSelection_CanExecute())
                    {
                        FriendlyName = Message.EventLog_ExportToExcelSelection,
                        FriendlyDescription = Message.EventLog_ExportToExcelSelection_Description,
                        Icon = ImageResources.EventLog_ExportToExcelSelection
                    };
                    base.ViewModelCommands.Add(_exportToExcelSelectionCommand);
                }
                return _exportToExcelSelectionCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ExportToExcelSelection_CanExecute()
        {
            //event log must have entries
            if (!this.SelectedEventLogsWrap.Any())
            {
                _exportToExcelSelectionCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntriesSelected;
                return false;
            }

            return true;
        }

        private void ExportToExcelSelection_Executed()
        {
            //Export to excel with save dialog box
            ExportExcelSave(false);
        }

        #endregion

        #region RefreshCommand

        private RelayCommand _RefreshCommand;

        /// <summary>
        /// Refersh the event log name list and event log main grid
        /// </summary>
        public RelayCommand RefreshCommand
        {
            get
            {
                if (_RefreshCommand == null)
                {
                    _RefreshCommand = new RelayCommand(
                        p => Refresh_Executed(),
                        p => Refresh_CanExecute())
                    {
                        FriendlyName = Message.EventLog_Refresh,
                        FriendlyDescription = Message.EventLog_Refresh_Description,
                        Icon = ImageResources.EventLog_Refresh
                    };
                    base.ViewModelCommands.Add(_RefreshCommand);
                }
                return _RefreshCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Refresh_CanExecute()
        {
            return true;
        }

        //refresh both event log name and event log grid
        private void Refresh_Executed()
        {
            // Clear event log name list
            _availableEventLogNames.Clear();

            //Add "All Logs" to ListBox
            Galleria.Ccm.Model.EventLogName newEventLogName =
                Galleria.Ccm.Model.EventLogName.NewEventLogName(_AllLogsId, Message.EventLog_LogName_AllLogs);
            _availableEventLogNames.Add(newEventLogName);

            //Load Log Names to ListBox (Don't wipe clean because of "All Logs" item)
            _addedAllLogs = true;
            _masterEventLogNameListView.FetchAll();
            _addedAllLogs = false;

            //re-populate the main grid
            LoadEventLogGrid();
        }

        #endregion

        #region CopyToClipboardCommand

        private RelayCommand _CopyToClipboardCommand;

        /// <summary>
        /// copy the data grid to the clipboard
        /// </summary>
        public RelayCommand CopyToClipboardCommand
        {
            get
            {
                if (_CopyToClipboardCommand == null)
                {
                    _CopyToClipboardCommand = new RelayCommand(
                        p => CopyToClipboard_Executed())
                    {
                        FriendlyName = Message.EventLog_CopyToClipboard,
                        FriendlyDescription = Message.EventLog_CopyToClipboard_Description,
                        Icon = ImageResources.EventLog_CopyToClipboard
                    };
                    base.ViewModelCommands.Add(_CopyToClipboardCommand);
                }
                return _CopyToClipboardCommand;
            }
        }

        private void CopyToClipboard_Executed()
        {
            //Menu button

        }

        #endregion

        #region CopyToClipboardAllCommand

        private RelayCommand _CopyToClipboardAllCommand;

        /// <summary>
        /// Copy the data grid to the clipboard
        /// </summary>
        public RelayCommand CopyToClipboardAllCommand
        {
            get
            {
                if (_CopyToClipboardAllCommand == null)
                {
                    _CopyToClipboardAllCommand = new RelayCommand(
                        p => CopyToClipboardAll_Executed(),
                        p => CopyToClipboardAll_CanExecute())
                    {
                        FriendlyName = Message.EventLog_CopyToClipboardAll,
                        FriendlyDescription = Message.EventLog_CopyToClipboardAll_Description,
                        Icon = ImageResources.EventLog_CopyToClipboardAll
                    };
                    base.ViewModelCommands.Add(_CopyToClipboardAllCommand);
                }
                return _CopyToClipboardAllCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean CopyToClipboardAll_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any())
            {
                _CopyToClipboardAllCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }
            return true;
        }

        private void CopyToClipboardAll_Executed()
        {
            //copy all data grid to clipboard
            _currrentClipboardAll = true;
            OnClipboardData();
        }

        #endregion

        #region CopyToClipboardSelectionCommand

        private RelayCommand _CopyToClipboardSelectionCommand;

        /// <summary>
        /// Copy the only selected rows from the data grid to the clipboard
        /// </summary>
        public RelayCommand CopyToClipboardSelectionCommand
        {
            get
            {
                if (_CopyToClipboardSelectionCommand == null)
                {
                    _CopyToClipboardSelectionCommand = new RelayCommand(
                        p => CopyToClipboardSelection_Executed(),
                        p => CopyToClipboardSelection_CanExecute())
                    {
                        FriendlyName = Message.EventLog_CopyToClipboardSelection,
                        FriendlyDescription = Message.EventLog_CopyToClipboardSelection_Description,
                        Icon = ImageResources.EventLog_CopyToClipboardSelection
                    };
                    base.ViewModelCommands.Add(_CopyToClipboardSelectionCommand);
                }
                return _CopyToClipboardSelectionCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean CopyToClipboardSelection_CanExecute()
        {
            //event log must have entries
            if (!this.SelectedEventLogsWrap.Any())
            {
                _CopyToClipboardSelectionCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntriesSelected;
                return false;
            }

            return true;
        }

        private void CopyToClipboardSelection_Executed()
        {
            //Copy the only selected rows from the data grid to the clipboard
            _currrentClipboardAll = false;
            OnClipboardData();
        }

        #endregion

        #region SummariseCommand

        private RelayCommand _SummariseCommand;

        /// <summary>
        /// Summerise group my date time
        /// </summary>
        public RelayCommand SummariseCommand
        {
            get
            {
                if (_SummariseCommand == null)
                {
                    _SummariseCommand = new RelayCommand(
                        p => Summarise_Executed())
                    {
                        FriendlyName = Message.EventLog_SummariseTime,
                        FriendlyDescription = Message.EventLog_SummariseTime_Description,
                        Icon = ImageResources.EventLog_SummariseTime
                    };
                    base.ViewModelCommands.Add(_SummariseCommand);
                }
                return _SummariseCommand;
            }
        }

        private void Summarise_Executed()
        {
            // Menu button
        }

        #endregion

        #region SummariseCommand00Sec

        private RelayCommand _Summarise00SecCommand;

        /// <summary>
        /// Turn off the summarise grouping
        /// </summary>
        public RelayCommand Summarise00SecCommand
        {
            get
            {
                if (_Summarise00SecCommand == null)
                {
                    _Summarise00SecCommand = new RelayCommand(
                        p => Summarise00Sec_Executed(),
                        p => Summarise00Sec_CanExecute())
                    {
                        FriendlyName = Message.EventLog_Summarise00sec,
                        FriendlyDescription = Message.EventLog_Summarise00sec_Description,
                        Icon = ImageResources.EventLog_SummariseTime00Sec
                    };
                    base.ViewModelCommands.Add(_Summarise00SecCommand);
                }
                return _Summarise00SecCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Summarise00Sec_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any())
            {
                _Summarise00SecCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }

            return true;
        }

        private void Summarise00Sec_Executed()
        {
            //Set current date time group to none
            CurrentDataTimeGroup = SummariseType.None;
            OnSummariseData();
            //Populate Event Log Grid (used to add Wrap around model for summerise data)
            PopulateEventLogGrid();
        }

        #endregion

        #region SummariseCommand03Sec

        private RelayCommand _Summarise03SecCommand;

        /// <summary>
        /// set summarise the dat time by 3 seconds
        /// </summary>
        public RelayCommand Summarise03SecCommand
        {
            get
            {
                if (_Summarise03SecCommand == null)
                {
                    _Summarise03SecCommand = new RelayCommand(
                        p => Summarise03Sec_Executed(),
                        p => Summarise03Sec_CanExecute())
                    {
                        FriendlyName = Message.EventLog_Summarise03sec,
                        FriendlyDescription = Message.EventLog_Summarise03sec_Description,
                        Icon = ImageResources.EventLog_SummariseTime03Sec
                    };
                    base.ViewModelCommands.Add(_Summarise03SecCommand);
                }
                return _Summarise03SecCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Summarise03Sec_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any())
            {
                _Summarise03SecCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }

            return true;
        }

        private void Summarise03Sec_Executed()
        {
            //Set current date time group to 3 seconds
            CurrentDataTimeGroup = SummariseType.Seconds3;
            OnSummariseData();
            //Populate Event Log Grid (used to add Wrap around model for summerise data)
            PopulateEventLogGrid();
        }

        #endregion

        #region SummariseCommand05Sec

        private RelayCommand _Summarise05SecCommand;

        /// <summary>
        /// set summarise the dat time by 5 seconds
        /// </summary>
        public RelayCommand Summarise05SecCommand
        {
            get
            {
                if (_Summarise05SecCommand == null)
                {
                    _Summarise05SecCommand = new RelayCommand(
                        p => Summarise05Sec_Executed(),
                        p => Summarise05Sec_CanExecute())
                    {
                        FriendlyName = Message.EventLog_Summarise05sec,
                        FriendlyDescription = Message.EventLog_Summarise05sec_Description,
                        Icon = ImageResources.EventLog_SummariseTime05Sec
                    };
                    base.ViewModelCommands.Add(_Summarise05SecCommand);
                }
                return _Summarise05SecCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Summarise05Sec_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any())
            {
                _Summarise05SecCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }

            return true;
        }

        private void Summarise05Sec_Executed()
        {
            //Set current date time group to 5 seconds
            CurrentDataTimeGroup = SummariseType.Seconds5;
            OnSummariseData();
            //Populate Event Log Grid (used to add Wrap around model for summerise data)
            PopulateEventLogGrid();
        }

        #endregion

        #region SummariseCommand10Sec

        private RelayCommand _Summarise10SecCommand;

        /// <summary>
        /// set summarise the dat time by 10 seconds
        /// </summary>
        public RelayCommand Summarise10SecCommand
        {
            get
            {
                if (_Summarise10SecCommand == null)
                {
                    _Summarise10SecCommand = new RelayCommand(
                        p => Summarise10Sec_Executed(),
                        p => Summarise10Sec_CanExecute())
                    {
                        FriendlyName = Message.EventLog_Summarise10sec,
                        FriendlyDescription = Message.EventLog_Summarise10sec_Description,
                        Icon = ImageResources.EventLog_SummariseTime10Sec
                    };
                    base.ViewModelCommands.Add(_Summarise10SecCommand);
                }
                return _Summarise10SecCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Summarise10Sec_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any())
            {
                _Summarise10SecCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }

            return true;
        }


        private void Summarise10Sec_Executed()
        {
            //Set current date time group to 10 seconds
            CurrentDataTimeGroup = SummariseType.Seconds10;
            OnSummariseData();
            //Populate Event Log Grid (used to add Wrap around model for summerise data)
            PopulateEventLogGrid();
        }

        #endregion

        #region SummariseCommand60Sec

        private RelayCommand _Summarise60SecCommand;

        /// <summary>
        /// set summarise the dat time by 60 seconds
        /// </summary>
        public RelayCommand Summarise60SecCommand
        {
            get
            {
                if (_Summarise60SecCommand == null)
                {
                    _Summarise60SecCommand = new RelayCommand(
                        p => Summarise60Sec_Executed(),
                        p => Summarise60Sec_CanExecute())
                    {
                        FriendlyName = Message.EventLog_Summarise60sec,
                        FriendlyDescription = Message.EventLog_Summarise60sec_Description,
                        Icon = ImageResources.EventLog_SummariseTime60Sec
                    };
                    base.ViewModelCommands.Add(_Summarise60SecCommand);
                }
                return _Summarise60SecCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Summarise60Sec_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any())
            {
                _Summarise60SecCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }

            return true;
        }

        private void Summarise60Sec_Executed()
        {
            //Set current date time group to 60 seconds
            CurrentDataTimeGroup = SummariseType.Seconds60;
            OnSummariseData();
            //Populate Event Log Grid (used to add Wrap around model for summerise data)
            PopulateEventLogGrid();
        }

        #endregion

        #region FilterErrorsCommand

        private RelayCommand _FilterErrorsCommand;

        /// <summary>
        /// filter button to only show errors
        /// </summary>
        public RelayCommand FilterErrorsCommand
        {
            get
            {
                if (_FilterErrorsCommand == null)
                {
                    _FilterErrorsCommand = new RelayCommand(
                        p => FilterErrors_Executed(),
                        p => FilterErrors_CanExecute())
                    {
                        FriendlyName = Message.EventLog_FilterErrors,
                        FriendlyDescription = Message.EventLog_FilterErrors_Description,
                        Icon = ImageResources.EventLog_FilterErrors
                    };
                    base.ViewModelCommands.Add(_FilterErrorsCommand);
                }
                return _FilterErrorsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean FilterErrors_CanExecute()
        {
            //event log must have entries
            // GFS-16338: Added check against _currentSelectedEntryType to allow the filter to be taken off if it's on,
            // even if there are no error entries, as there might be non-error entries.
            //if ((_currentSelectedEntryType == null) && (_masterEventLogListView.Model.Count() <= 0))
            //{
            //    _FilterErrorsCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
            //    return false;
            //}

            return true;
        }

        private void FilterErrors_Executed()
        {
            //Check to see if filer is switched off
            if (_currentSelectedEntryType == null)
            {
                //if filter is off the turn on and set current selected filer Entry Type to only show Errors
                _currentSelectedEntryType = (short)EventLogEntryType.Error;
            }
            else
            {
                //if filter is on the turn off the filer by setting the current filter Entry Type to null (show all Entry Types)
                _currentSelectedEntryType = null;
            }
            //Load the grid with the new filter Entry Type setting
            LoadEventLogGrid();
            //Event
            OnSummariseData();
            //Set grouping date time values
            PopulateEventLogGrid();
        }

        #endregion

        #region SelectEntityInfoCommand

        private RelayCommand _selectEntityInfoCommand;
        /// <summary>
        /// filter button to show only one type of entity
        /// </summary>
        public RelayCommand SelectEntityInfoCommand
        {
            get
            {
                if (_selectEntityInfoCommand == null)
                {
                    _selectEntityInfoCommand = new RelayCommand(p => SelectEntityInfo_Executed((Nullable<Int32>)p),
                        p => SelectEntityInfo_CanExecute())
                    {
                        FriendlyName = Message.EventLog_FilterEntity,
                        FriendlyDescription = Message.EventLog_FilterEntity_Description,
                        Icon = ImageResources.EventLog_FilterEntity
                    };
                    this.ViewModelCommands.Add(_selectEntityInfoCommand);
                }
                return _selectEntityInfoCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SelectEntityInfo_CanExecute()
        {
            //event log must have entries
            if (!_masterEventLogListView.Model.Any()
                && _currentSelectedEntityId == null) // (GFS-16683) user should be able to change filer if no data for selected entity
            {
                _selectEntityInfoCommand.DisabledReason = Message.EventLog_CommandDisabledReason_NoEntries;
                return false;
            }

            return true;
        }

        private void SelectEntityInfo_Executed(Nullable<Int32> entityId)
        {
            //Don't do any thing if nothing is going to change
            if (_currentSelectedEntityId != entityId)
            {
                //Set current selected Entity ID to which ever the user has selected
                _currentSelectedEntityId = entityId;
                //Load the grid with the new filter Entry Type setting
                LoadEventLogGrid();
                //Event
                OnSummariseData();
                //Set grouping date time values
                PopulateEventLogGrid();
            }
        }

        #endregion

        #region ClearAllLogsCommand

        private RelayCommand _clearAllLogsCommand;
        /// <summary>
        /// Marks all event log entries as deleted
        /// </summary>
        public RelayCommand ClearAllLogsCommand
        {
            get
            {
                if (_clearAllLogsCommand == null)
                {
                    _clearAllLogsCommand = new RelayCommand
                        (
                            p => ClearAllLogs_Executed(),
                            p => ClearAllLogs_CanExecute())
                    {
                        FriendlyName = Message.EventLogs_ClearAll,
                        FriendlyDescription = Message.EventLogs_ClearAll_Description,
                        Icon = ImageResources.EventLog_RemoveAll
                    };

                    this.ViewModelCommands.Add(_clearAllLogsCommand);
                }
                return _clearAllLogsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ClearAllLogs_CanExecute()
        {
            //user must have execute permission - this is equivalent to the update action for command objects
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject,
                typeof(Model.EventLog.DeleteAllEventLogsCommand)))
            {
                _clearAllLogsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void ClearAllLogs_Executed()
        {
            Boolean canContinue = true;

            if (this.AttachedControl != null)
            {
                //Ask user for confirmation
                ModalMessage win = new ModalMessage();
                win.Title = Message.EventLogs_DeleteAllWindowTitle;
                win.Header = Message.EventLogs_DeleteAllMessageHeader;
                win.Description = Message.EventLogs_DeleteAllMessageDescription;
                win.Owner = this.AttachedControl;
                win.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                win.MessageIcon = ImageResources.DialogWarning;
                win.ButtonCount = 2;
                win.Button1Content = Message.Generic_Delete;
                win.Button2Content = Message.Generic_Cancel;
                win.DefaultButton = ModalMessageButton.Button1;
                win.CancelButton = ModalMessageButton.Button2;
                win.Owner = this.AttachedControl;
                App.ShowWindow(win, /*isModal*/true);

                canContinue = (win.Result == ModalMessageResult.Button1);
            }

            if (canContinue)
            {
                base.ShowWaitCursor(true);

                //Delete logs
                Model.EventLog.DeleteAllEventLogs();
                this.RefreshCommand.Execute();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            return continueExecute;
        }

        public void ChangeTime(Boolean isStartDate, Boolean isHours, Boolean increase)
        {
            if (isStartDate)
            {
                if (isHours)
                {
                    if (increase)
                    {
                        StartTimeHour++;
                    }
                    else
                    {
                        StartTimeHour--;
                    }
                }
                else
                {
                    if (increase)
                    {
                        StartTimeMinute++;
                    }
                    else
                    {
                        StartTimeMinute--;
                    }
                }
            }
            else
            {
                if (isHours)
                {
                    if (increase)
                    {
                        EndTimeHour++;
                    }
                    else
                    {
                        EndTimeHour--;
                    }
                }
                else
                {
                    if (increase)
                    {
                        EndTimeMinute++;
                    }
                    else
                    {
                        EndTimeMinute--;
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        /// Reponds to changes in the master event log name list
        private void MasterEventLogNameListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<EventLogNameList> e)
        {
            if (_availableEventLogNames.Count > 0 && !_addedAllLogs)
            {
                _availableEventLogNames.Clear();
            }
            _availableEventLogNames.AddRange(e.NewModel);
        }

        // Reponds to changes in the master entity list
        private void MasterEntityInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<EntityInfoList> e)
        {
            if (_availableEntityInfos.Count > 0 && !_addedAllLogs)
            {
                _availableEntityInfos.Clear();
            }
            _availableEntityInfos.AddRange(e.NewModel);
        }

        // Reponds to changes in the master event log list
        private void MasterEventLogListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<EventLogList> e)
        {
            PopulateEventLogGrid();
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterEventLogNameListView.ModelChanged -= MasterEventLogNameListView_ModelChanged;
                    _masterEntityInfoListView.ModelChanged -= MasterEntityInfoListView_ModelChanged;
                    _masterEventLogListView.ModelChanged -= MasterEventLogListView_ModelChanged;

                    _availableEntityInfos.Clear();
                    _availableEventLogNames.Clear();
                    _availableEventLogsWrap.Clear();
                    _selectedEventLogsWrap.Clear();

                    _masterEventLogNameListView = null;
                    _masterEntityInfoListView = null;

                    _availableEntityInfos = null;
                    _availableEventLogNames = null;
                    _availableEventLogsWrap = null;
                    _selectedEventLogsWrap = null;
                    _selectedEntityInfo = null;
                    _selectedEventLogWrap = null;
                    _availableEntityInfoRO = null;
                    _availableEventLogNameRO = null;
                    _availableEventLogsWrapRO = null;

                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
