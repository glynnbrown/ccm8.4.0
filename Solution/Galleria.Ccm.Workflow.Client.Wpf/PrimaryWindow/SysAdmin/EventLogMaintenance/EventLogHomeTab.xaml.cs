﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System.Windows;
using System.Windows.Input;
using Fluent;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EventLogMaintenance
{
    /// <summary>
    /// Interaction logic for EventLogHomeTab.xaml
    /// </summary>
    public partial class EventLogHomeTab : RibbonTabItem
    {
        #region fields
        System.Windows.Controls.TextBox _currentlySelectedTextBox;
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(EventLogViewModel), typeof(EventLogHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public EventLogViewModel ViewModel
        {
            get { return (EventLogViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Methods

        // Code used to populate the entity button menu
        public void PopulateEntityInfoMenu()
        {
            // First add the No entity filter button
            Fluent.MenuItem EntityInfoMenuItemNoFilter = new Fluent.MenuItem()
            {
                Header = Message.EventLog_FilterOffEntity,
                Icon = ImageResources.EventLog_FilterOffEntity,
                Command = ViewModel.SelectEntityInfoCommand,
                Size = RibbonControlSize.Large,
                CommandParameter = null
            };
            cmdEntityInfo.Items.Add(EntityInfoMenuItemNoFilter);

            //Now add each entity to the button menu
            foreach (Model.EntityInfo property in ViewModel.AvailableEntityInfo)
            {

                Fluent.MenuItem EntityInfoMenuItem = new Fluent.MenuItem()
                {
                    Header = property.Name,
                    Icon = ImageResources.EntitySetup_TabHeader,
                    Command = ViewModel.SelectEntityInfoCommand,
                    Size = RibbonControlSize.Large,
                    CommandParameter = property.Id
                };
                cmdEntityInfo.Items.Add(EntityInfoMenuItem);
            }
        }

        #endregion

        #region Constructor

        public EventLogHomeTab()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(EntityInfo_Loaded);
        }

        void EntityInfo_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= EntityInfo_Loaded;
            PopulateEntityInfoMenu();
        }

        #endregion

        #region events

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                System.Windows.Controls.TextBox tb = (System.Windows.Controls.TextBox)sender;
                if (tb != null)
                {
                    tb.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                }

            }
        }

        private void DecreaseStartTime(object sender, RoutedEventArgs e)
        {
            ViewModel.ChangeTime(true, (_currentlySelectedTextBox == this.startHour), false);
        }

        private void IncreaseStartTime(object sender, RoutedEventArgs e)
        {
            ViewModel.ChangeTime(true, (_currentlySelectedTextBox == this.startHour), true);
        }

        private void DecreaseEndTime(object sender, RoutedEventArgs e)
        {
            ViewModel.ChangeTime(false, (_currentlySelectedTextBox == this.endHour), false);
        }

        private void IncreaseEndTime(object sender, RoutedEventArgs e)
        {
            ViewModel.ChangeTime(false, (_currentlySelectedTextBox == this.endHour), true);
        }

        private void TextGotFocus(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.TextBox selectedBox = (System.Windows.Controls.TextBox)sender;

            _currentlySelectedTextBox = selectedBox;

            selectedBox.SelectAll();
        }

        #endregion

    }
}
