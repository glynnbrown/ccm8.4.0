﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion
#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//  Corrected EventLogNameName to be EventLogName
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EventLogMaintenance
{
    public class EventLogWindowWrap : ViewModelAttachedControlObject<EventLogWindow>
    {

        #region Fields

        private EventLog _dataType;

        private String _summariseGroupName;
        private String _entryTypeName;

        #endregion

        #region Properties

        public EventLog DataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }

        public String SummariseGroupName
        {
            get { return _summariseGroupName; }
            set { _summariseGroupName = value; }
        }

        public String EntryTypeName
        {
            get { return _entryTypeName; }
            set { _entryTypeName = value; }
        }

        #endregion

        #region Constructor

        public EventLogWindowWrap(EventLog dataType, SummariseType type)
        {
            if (type != SummariseType.None)
            {
                string getDateTime = string.Format("{0:d/M/yyyy HH:mm:}", dataType.DateTime);
                int getSec = dataType.DateTime.Second;
                int roundSec;
                double countRound;

                if (type == SummariseType.Seconds3)
                {
                    roundSec = 3;
                    countRound = (getSec / roundSec);
                    getSec = (int)Math.Truncate(countRound + 0.5) * roundSec;
                    _summariseGroupName = getDateTime + getSec.ToString("00");
                }
                else if (type == SummariseType.Seconds5)
                {
                    roundSec = 5;
                    countRound = (getSec / roundSec);
                    getSec = (int)Math.Truncate(countRound + 0.5) * roundSec;
                    _summariseGroupName = getDateTime + getSec.ToString("00");
                }
                else if (type == SummariseType.Seconds10)
                {
                    roundSec = 10;
                    countRound = (getSec / roundSec);
                    getSec = (int)Math.Truncate(countRound + 0.5) * roundSec;

                    _summariseGroupName = getDateTime + getSec.ToString("00");
                }
                else if (type == SummariseType.Seconds60)
                {
                    getSec = 0;
                    _summariseGroupName = getDateTime + getSec.ToString("00");
                }

                //Set Group Name
                _summariseGroupName = dataType.Process + "; " + dataType.Source + "; " + dataType.EventLogName + "; " + dataType.ComputerName + "; " + _summariseGroupName;

            }
            else
            {
                _summariseGroupName = "";
            }

            //Need to convert Entry Type into Entry Type Name (using Enum Helper)
            try
            {
                _entryTypeName = EventLogEntryTypeHelper.FriendlyNames[dataType.EntryType];
            }
            catch
            {
                _entryTypeName = "";
            }

            //Pass data Type
            _dataType = dataType;

        }

        #endregion

        #region Dispose

        /// <summary>
        /// Handles an IDisposable dispose call
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    //dispose control
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }
}
