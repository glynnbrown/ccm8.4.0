﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#region Version History: (CCM 8.1.0)
// V8-29851 : M.Shelley
//  Set the correct help file key in the default constructor
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EventLogMaintenance
{
    /// <summary>
    /// Interaction logic for EntitySetupBackstageOpen.xaml
    /// </summary>
    public partial class EventLogWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(EventLogViewModel), typeof(EventLogWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public EventLogViewModel ViewModel
        {
            get { return (EventLogViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            EventLogWindow senderControl = (EventLogWindow)obj;

            if (e.OldValue != null)
            {
                EventLogViewModel oldModel = (EventLogViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                EventLogViewModel newModel = (EventLogViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor
        //Load form with filters set
        public EventLogWindow(Nullable<Int32> eventLogNameId, String source, Nullable<Int32> eventId, Nullable<Int16> levelId,
                                Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> dateTime,
                                String process, String computerName, String urlHelperLink, String description,
                                String content, String gibraltarSessionId)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Set help file key
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.EventLogs);

            this.ViewModel = new EventLogViewModel(eventLogNameId, source, eventId, levelId, userId, entityId, dateTime,
                process, computerName, urlHelperLink, description, content, gibraltarSessionId);
            this.Loaded += new RoutedEventHandler(EventLogWindow_Loaded);
        }

        // Load form without any filters
        public EventLogWindow()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //The DataGridTextColumn auto-formats DateTime to en-US, change format to be based on CurrentCulture
            DateTimeColumn.Binding.StringFormat
                = String.Format("{0} {1}",
                                System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern,
                                System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern);

            //Set help file key
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.EventLogs);

            this.ViewModel = new EventLogViewModel();
            this.Loaded += new RoutedEventHandler(EventLogWindow_Loaded);
        }

        //Summarise grouping for event log grid
        void ViewModel_SummariseData(object sender, EventArgs e)
        {
            //Remove groupings
            this.EventLogMainGrid.GroupByDescriptions.Clear();

            //If not none grouping
            if (this.ViewModel.CurrentDataTimeGroup != SummariseType.None)
            {
                //set up grouping grid to current field
                this.EventLogMainGrid.GroupByDescriptions.Add(new PropertyGroupDescription("SummariseGroupName"));
            }
        }

        //Copy grid to clipboard (all or selected)
        void ViewModel_CopyToClipboard(object sender, EventArgs e)
        {
            //Clear Clipboard
            Clipboard.Clear();

            //Check to see if we're copying all or selected rows
            if (this.ViewModel.CurrentCopyToClipboard == true)
            {
                //Copy to clipboard all rows
                this.EventLogMainGrid.CopyToClipboard();
            }
            else
            {
                //Copy to clipboard selected rows
                this.EventLogMainGrid.CopySelectedToClipboard();
            }
        }

        //set up loaded event after the foirm as loaded
        private void EventLogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= EventLogWindow_Loaded;

            //set up the two eventHandlers for the summarise and copt to clipboard buttons
            this.ViewModel.SummariseData += new EventHandler(ViewModel_SummariseData);
            this.ViewModel.ClipboardData += new EventHandler(ViewModel_CopyToClipboard);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region window close

        //on close form event
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }

            }
        }

        //After form closed
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }
        #endregion
    }

}
