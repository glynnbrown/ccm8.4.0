﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25534 : I.George
//	Initial version
#endregion

#region Version History: (CCM 8.01)
// V8-28928 : L.Ineson
// Removed property changed handler.
#endregion

#endregion

using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EntityMaintenance
{
    /// <summary>
    /// Interaction logic for EntityMaintenanceHomeTab.xaml
    /// </summary>
    public sealed partial class EntityMaintenanceHomeTab : RibbonTabItem
    {
        #region ViewModelproperty

        public static readonly DependencyProperty ViewModelProperty
           = DependencyProperty.Register("ViewModel", typeof(EntityMaintenanceViewModel), typeof(EntityMaintenanceHomeTab),
           new PropertyMetadata(null));

        public EntityMaintenanceViewModel ViewModel
        {
            get { return (EntityMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public EntityMaintenanceHomeTab()
        {
            InitializeComponent();
        }
 
        #endregion
    }
}
