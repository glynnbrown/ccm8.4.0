﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25534 : I.George
//	Initial version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EntityMaintenance
{
    /// <summary>
    /// Interaction logic for EntityMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class EntityMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private ObservableCollection<EntityInfo> _searchResults = new ObservableCollection<EntityInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(EntityMaintenanceViewModel), typeof(EntityMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public EntityMaintenanceViewModel ViewModel
        {
            get { return (EntityMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            EntityMaintenanceBackstageOpen senderControl = (EntityMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                EntityMaintenanceViewModel oldModel = (EntityMaintenanceViewModel)e.OldValue;
                oldModel.AvailableEntities.BulkCollectionChanged -= senderControl.ViewModel_AvailableEntityBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                EntityMaintenanceViewModel newModel = (EntityMaintenanceViewModel)e.NewValue;
                newModel.AvailableEntities.BulkCollectionChanged += senderControl.ViewModel_AvailableEntityBulkCollectionChanged;
            }

            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(EntityMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChange));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public string SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChange(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            EntityMaintenanceBackstageOpen senderControl = (EntityMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultProperty

        public static readonly DependencyProperty SearchResultProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<EntityInfo>),
            typeof(EntityMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of searched results
        /// </summary>
        public ReadOnlyObservableCollection<EntityInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<EntityInfo>)GetValue(SearchResultProperty); }
            private set { SetValue(SearchResultProperty, value); }
        }
        #endregion

        #endregion

        #region Constructor

        public EntityMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<EntityInfo>(_searchResults);

            InitializeComponent();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Finds result matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<EntityInfo> results = this.ViewModel.GettingMatchingEntity(this.SearchText);

                foreach (EntityInfo entInfo in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(entInfo);
                }
            }
        }
        #endregion

        #region event handlers

        /// <summary>
        /// updates the search results wheb the available Entities list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableEntityBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                EntityInfo storeToOpen = (EntityInfo)senderControl.SelectedItem;

                if (storeToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                }
            }
        }
        #endregion
    }
}
