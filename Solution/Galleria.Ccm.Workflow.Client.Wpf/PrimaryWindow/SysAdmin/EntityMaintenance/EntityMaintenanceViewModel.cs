﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25534 : I.George
//	Initial version

// CCM- 26314
// checks that the opened entity cannot be deleted
// V8-27155 : L.Luong
//  Added Child Property changes
// V8-27964 : A.Silva
//      Added ProductLevel selection.
//      Added SelectionItem to wrap the ProductLevel selection.

#endregion

#region Version History: (CCM v8.01)

// V8-28702 : L.Luong
//	Added SyncMerchandisingHierarchyCommand
// V8-28679 : A.Silva
//      Added SetLocation and SetImageProductAttributeFilter commands.
// V8-28928 : L.Ineson
//  Amended sync command to always confirm and be disabled when no level is set.
// Updated to use new WindowViewModelBase and WindowService.
#endregion

#region Version History: (CCM v8.03)
// V8-29750 : L.Ineson
//  Added new planogram image settings
#endregion

#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendly description to and Delete Command
#endregion

#region Version History: (CCM 8.2.0)
// V8-31533 : A.Probyn
//  Added defensive code to field selectors SelectedField value
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Csla;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using ApplicationContext = Csla.ApplicationContext;
using Message = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message;
using System.Windows.Media;
using Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EntityMaintenance
{
    /// <summary>
    /// View model controller for EntityMaintenanceOrganiser
    /// </summary>
    public sealed class EntityMaintenanceViewModel : WindowViewModelBase
    {
        private Boolean _fieldPickerViewModelsInitialized;
        
        #region Nested classes

        /// <summary>
        ///     Nested class to wrap the selection for the selected product level.
        /// </summary>
        public sealed class SynchroniseSelectionItem
        {
            /// <summary>
            ///     The display name for the wrapped item.
            /// </summary>
            public String DisplayName { get; private set; }

            /// <summary>
            ///     The value for the wrapped item.
            /// </summary>
            public Object Value { get; private set; }

            /// <summary>
            ///     Initializes a new selection item.
            /// </summary>
            /// <param name="displayName">String to display</param>
            /// <param name="value">Wrapped value.</param>
            public SynchroniseSelectionItem(String displayName, Object value)
            {
                DisplayName = displayName;
                Value = value;
            }

            /// <summary>
            /// Returns a null selection item.
            /// </summary>
            public static SynchroniseSelectionItem None
            {
                get { return new SynchroniseSelectionItem(Message.EntityMaintenance_DoNotSynchronise, null); }
            }
        }

        /// <summary>
        ///     Nested class to wrap the selection for the selected product level.
        /// </summary>
        public sealed class SelectionItem
        {
            /// <summary>
            ///     The display name for the wrapped item.
            /// </summary>
            public String DisplayName { get; private set; }

            /// <summary>
            ///     The value for the wrapped item.
            /// </summary>
            public Object Value { get; private set; }

            /// <summary>
            ///     Initializes a new selection item.
            /// </summary>
            /// <param name="displayName">String to display</param>
            /// <param name="value">Wrapped value.</param>
            public SelectionItem(String displayName, Object value)
            {
                DisplayName = displayName;
                Value = value;
            }

            /// <summary>
            /// Returns a null selection item.
            /// </summary>
            public static SelectionItem None
            {
                get { return new SelectionItem(Message.Generic_None, String.Empty); }
            }
        }
        #endregion

        #region Fields

        const String _exCategory = "EntityMaintenance"; //Category name for any exceptions raised to gibraltar from here

        private readonly ModelPermission<Entity> _itemPerms = new ModelPermission<Entity>();
        private readonly Boolean _userCanSyncMerchHierarchy;

        private readonly EntityInfoListViewModel _masterEntityInfoListView = new EntityInfoListViewModel();

        private Entity _selectedEntity;

        private readonly BulkObservableCollection<SynchroniseSelectionItem> _masterProductLevels = new BulkObservableCollection<SynchroniseSelectionItem>();
        private readonly ReadOnlyBulkObservableCollection<SynchroniseSelectionItem> _availableProductLevels;
        private Int32? _selectedProductLevel;
        private Int32? _originalProductLevel;

        private ReadOnlyCollection<SelectionItem> _availableProductLabels;
        private ReadOnlyCollection<SelectionItem> _availableFixtureLabels;
        private ReadOnlyCollection<SelectionItem> _availableHighlights;

        #endregion

        #region Binding Property Path

        //Properties
        public static readonly PropertyPath AvailableEntitiesProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.AvailableEntities);
        public static readonly PropertyPath SelectedEntityProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.SelectedEntity);
        public static readonly PropertyPath HasProductLevelsProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.HasProductLevels);
        public static readonly PropertyPath AvailableProductLevelsProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.AvailableProductLevels);
        public static readonly PropertyPath SelectedProductLevelProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.SelectedProductLevel);
        public static readonly PropertyPath DisplayUomProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.DisplayUom);
        public static readonly PropertyPath AvailableProductLabelsProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.AvailableProductLabels);
        public static readonly PropertyPath AvailableFixtureLabelsProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.AvailableFixtureLabels);
        public static readonly PropertyPath AvailableHighlightsProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.AvailableHighlights);

        //Commands
        public static readonly PropertyPath NewCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath SaveCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath DeleteCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(v => v.CloseCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SyncMerchandisingHierarchyCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(p => p.SyncMerchandisingHierarchyCommand);
        public static readonly PropertyPath SetImageProductAttributeFilterCommandProperty = GetPropertyPath<EntityMaintenanceViewModel>(o => o.SetImageProductAttributeFilterCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of available entities 
        /// </summary>
        public ReadOnlyBulkObservableCollection<EntityInfo> AvailableEntities
        {
            get { return _masterEntityInfoListView.BindableCollection; }
        }

        /// <summary>
        /// Gets/Sets the selected entity
        /// </summary>
        public Entity SelectedEntity
        {
            get { return _selectedEntity; }
            private set
            {
                Entity oldEntity = _selectedEntity;
                _selectedEntity = value;

                OnPropertyChanged(SelectedEntityProperty);
                OnSelectedEntityChanged(oldEntity, _selectedEntity);

                OnPropertyChanged(SelectedProductLevelProperty);
                OnPropertyChanged(HasProductLevelsProperty);
            }
        }

        public Boolean HasProductLevels
        {
            get { return _availableProductLevels.Count > 0; }
        }

        public ReadOnlyBulkObservableCollection<SynchroniseSelectionItem> AvailableProductLevels
        {
            get { return _availableProductLevels; }
        }

        public Int32? SelectedProductLevel
        {
            get { return _selectedProductLevel; }
            set
            {
                _selectedProductLevel = value;
                OnPropertyChanged(SelectedProductLevelProperty);
                OnSelectedProductLevelChanged(value);
            }
        }

        /// <summary>
        /// Returns the display unit of measure.
        /// </summary>
        public DisplayUnitOfMeasure DisplayUom
        {
            get
            {
                if (this.SelectedEntity.SystemSettings != null)
                {
                    UnitOfMeasureLengthType planUom = UnitOfMeasureLengthType.Unknown;
                    Enum.TryParse<UnitOfMeasureLengthType>(SelectedEntity.SystemSettings.LengthUnitsOfMeasure.ToString(), out planUom);
                    return
                        new DisplayUnitOfMeasure(
                        UnitOfMeasureLengthTypeHelper.FriendlyNames[planUom],
                        UnitOfMeasureLengthTypeHelper.Abbreviations[planUom],
                        false);

                }
                return null;
            }
        }

        /// <summary>
        /// Returns the collection of available product labels
        /// </summary>
        public ReadOnlyCollection<SelectionItem> AvailableProductLabels
        {
            get { return _availableProductLabels; }
            private set
            {
                _availableProductLabels = value;
                OnPropertyChanged(AvailableProductLabelsProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available fixture labels.
        /// </summary>
        public ReadOnlyCollection<SelectionItem> AvailableFixtureLabels
        {
            get { return _availableFixtureLabels; }
            private set
            {
                _availableFixtureLabels = value;
                OnPropertyChanged(AvailableFixtureLabelsProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available highlights.
        /// </summary>
        public ReadOnlyCollection<SelectionItem> AvailableHighlights
        {
            get { return _availableHighlights; }
            private set
            {
                _availableHighlights = value;
                OnPropertyChanged(AvailableHighlightsProperty);
            }
        }

        /// <summary>
        /// Returns a collection of available fonts.
        /// </summary>
        public ReadOnlyCollection<FontFamily> AvailableFonts { get; private set; }

        #endregion

        private readonly IPlanogramComparisonSettings originalSettings;

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public EntityMaintenanceViewModel()
        {
            _userCanSyncMerchHierarchy = ApplicationContext.User.IsInRole(DomainPermission.SyncWithMerchandisingHierarchy.ToString());

            _masterEntityInfoListView.FetchAll();

            _availableProductLevels = new ReadOnlyBulkObservableCollection<SynchroniseSelectionItem>(_masterProductLevels);

            AvailableFonts = Fonts.SystemFontFamilies.OrderBy(f => f.ToString()).ToList().AsReadOnly();

            NewCommand.Execute();

            InitializeFieldPickerViewModels();
        }

        private void InitializeFieldPickerViewModels(Boolean dispose = false)
        {
            if (_fieldPickerViewModelsInitialized)
            {
                _fieldPickerViewModelsInitialized = false;

                _productFieldPickerViewModel.Dispose();

                _productFieldPickerViewModel = null;
            }

            if (dispose) return;

            List<ObjectFieldInfo> fieldInfos = GetModelObjectFieldInfos();
            _productFieldPickerViewModel = new Ccm.Common.Wpf.Selectors.FieldPickerViewModel(fieldInfos.Where(IsProductFieldGroup));

            _fieldPickerViewModelsInitialized = true;
        }

        private static List<ObjectFieldInfo> GetModelObjectFieldInfos()
        {
            List<ObjectFieldInfo> fieldInfos = PlanogramFieldHelper.EnumerateAllFields().ToList();
            foreach (ObjectFieldInfo field in fieldInfos)
            {
                field.GroupName = field.OwnerFriendlyName;
                field.PropertyName = $"{field.OwnerType}.{field.PropertyName}";
            }
            return fieldInfos;
        }

        private static Boolean IsProductFieldGroup(ObjectFieldInfo objectFieldInfo)
        {
            var allowedTypes = new List<Type> { typeof(PlanogramProduct)};
            Boolean result = allowedTypes.Contains(objectFieldInfo.OwnerType);
            return result;
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        ///<summary>
        ///loads a new entity
        ///</summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    RegisterCommand(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_itemPerms.CanCreate)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            return true;
        }

        private void New_Executed()
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;

            //create a new item
            Entity newEntity = Entity.NewEntity();

            //re-initialize as property was changed.
            newEntity.MarkGraphAsInitialized();

            this.SelectedEntity = newEntity;

            //close the backstage
            CloseRibbonBackstage();
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Loads the requested store
        /// parameter = the id of the entity to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private Boolean Open_CanExecute(Int32? entId)
        {
            //user must have get permission
            if (!_itemPerms.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (entId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? entId)
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;

            ShowWaitCursor(true);

            try
            {
                //fetch the requested item
                this.SelectedEntity = Entity.FetchById(entId.Value);

                //take a note of the original product level
                _originalProductLevel = this.SelectedEntity.ProductLevelId;
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            //close the backstage
            CloseRibbonBackstage();

            ShowWaitCursor(false);

        }
        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current entity
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                    new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    RegisterCommand(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedEntity == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }


            //if item is new, must have create
            if (this.SelectedEntity.IsNew && !_itemPerms.CanCreate)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //if item is old, user must have edit permission
            if (!this.SelectedEntity.IsNew && !_itemPerms.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedEntity.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //bindings must be valid
            if (!AreBindingsValid())
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Int32 entId = this.SelectedEntity.Id;

            //** check the item unique value;
            String newName;

            Predicate<String> isUniqueCheck =
                    (s) =>
                    {
                        Boolean returnValue = true;
                        ShowWaitCursor(true);
                        foreach (EntityInfo entInfo in EntityInfoList.FetchAllEntityInfos())
                        {
                            if (entInfo.Id != entId && entInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                            {
                                returnValue = false;
                                break;
                            }
                        }

                        ShowWaitCursor(false);
                        return returnValue;
                    };


            Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, this.SelectedEntity.Name, out newName);
            if (!nameAccepted) return false;

            //set the name
            if (this.SelectedEntity.Name != newName) this.SelectedEntity.Name = newName;

            UpdateComparisonAttributes();

            ShowWaitCursor(true);

            //check if the sync date needs resetting.
            if (this.SelectedEntity.DateLastMerchSynced != null)
            {
                // if the selected productlevelId changed set DateLastMerchSynced to null to force entity resync
                if (this.SelectedEntity.ProductLevelId != _originalProductLevel)
                {
                    this.SelectedEntity.DateLastMerchSynced = null;
                    _originalProductLevel = this.SelectedEntity.ProductLevelId;
                }
            }

            //save
            try
            {
                this.SelectedEntity = this.SelectedEntity.Save();

            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                RecordException(ex, _exCategory);
                Exception rootException = ex.GetBaseException();

                //if it is a concurrency exception, check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.SelectedEntity.Name);
                    if (itemReloadRequired) this.SelectedEntity = Entity.FetchById(this.SelectedEntity.Id);
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.SelectedEntity.Name, OperationType.Save);
                }
                return false;
            }

            //refresh the info list
            _masterEntityInfoListView.FetchAll();


            ShowWaitCursor(false);
            return true;
        }

        private void UpdateComparisonAttributes()
        {
            
            //Update Product Field Picker Rows
            UpdateFieldPickerRows(PlanogramItemType.Product);
            //Update Position Field Picker Rows
            UpdateFieldPickerRows(PlanogramItemType.Position);

        }

        private void UpdateFieldPickerRows(PlanogramItemType itemType)
        {
            FieldPickerViewModel fieldPickerViewModel = null;
            ReadOnlyBulkObservableCollection<FieldPickerRow> fieldPickerRows = null;

            if (itemType == PlanogramItemType.Product)
            {
                fieldPickerViewModel = ProductFieldPickerViewModel;
                fieldPickerRows = ProductFieldPickerViewModel.FieldPickerRows;
            }
           
            if (fieldPickerRows != null)
            {

                if (SelectedEntity.ComparisonAttributes.Any())
                {
                    EntityComparisonAttribute[] auxComparisonAttributes = new EntityComparisonAttribute[SelectedEntity.ComparisonAttributes.Count()];
                    SelectedEntity.ComparisonAttributes.ToList().CopyTo(auxComparisonAttributes);

                    //Delete removed field picker rows
                    foreach (EntityComparisonAttribute comparisonAttribute in auxComparisonAttributes)
                    {
                        if (comparisonAttribute.ItemType != itemType) continue;
                        if (fieldPickerViewModel.AvailableFieldPickerFields.Any(r => r.FieldPlaceholder == comparisonAttribute.PropertyName))
                        {
                            SelectedEntity.ComparisonAttributes.Remove(comparisonAttribute);
                        }
                    }
                }

                if (fieldPickerRows.Any())
                {
                    FieldPickerRow[] auxFieldPickerRows = new FieldPickerRow[fieldPickerRows.Count()];
                    fieldPickerRows.ToList().CopyTo(auxFieldPickerRows);

                    //Update existing Comparison Attributes if any
                    if (SelectedEntity.ComparisonAttributes.Any())
                    {
                        foreach (EntityComparisonAttribute comparisonAttribute in SelectedEntity.ComparisonAttributes)
                        {
                            if (comparisonAttribute.ItemType != itemType) continue;
                            FieldPickerRow foundField = fieldPickerRows.FirstOrDefault(r => r.FieldPlaceholder == comparisonAttribute.PropertyName);
                            if (foundField != null)
                            {
                                FieldPickerRow field = foundField;
                                comparisonAttribute.PropertyName = field.FieldPlaceholder;
                                comparisonAttribute.PropertyDisplayName = field.DisplayName;
                                //comparisonAttribute.ItemType = field.ItemType;
                                auxFieldPickerRows = auxFieldPickerRows.Where(val => val.Field.FieldPlaceholder != field.FieldPlaceholder).ToArray();
                            }
                        }
                    }

                    //Add new Comparison Attributes if any
                    foreach (FieldPickerRow row in auxFieldPickerRows)
                    {
                        EntityComparisonAttribute newRow = EntityComparisonAttribute.NewEntityComparisonAttribute(row.Field.FieldPlaceholder, row.Field.DisplayName, itemType);
                        SelectedEntity.ComparisonAttributes.Add(newRow);                           
                    }
                }

            }

        }


        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_itemPerms.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.SelectedEntity == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;

            }

            //must be valid
            if (!this.SelectedEntity.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the code to save as
            String copyName = null;

            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (EntityInfo entInfo in
                       EntityInfoList.FetchAllEntityInfos())
                       {
                           if (entInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };

            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            //Copy the item and rename
            ShowWaitCursor(true);

            Entity itemCopy = this.SelectedEntity.Copy();
            itemCopy.Name = copyName;
            this.SelectedEntity = itemCopy;

            ShowWaitCursor(false);

            //save it.
            SaveCurrentItem();
        }
        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                CloseWindow();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    RegisterCommand(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_itemPerms.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.SelectedEntity == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.SelectedEntity.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            //if the entity opened is from the database
            if (App.ViewState.EntityId == this.SelectedEntity.Id)
            {
                _deleteCommand.DisabledReason = Message.EntityMaintenance_Delete_DisabledIsCurrent;
                return false;
            }



            return true;
        }

        private void Delete_Executed()
        {
            //confirm with user
            if (!GetWindowService().ConfirmDeleteWithUser(this.SelectedEntity.ToString())) return;


            ShowWaitCursor(true);

            //mark the item as deleted so item changed does not show.
            Entity itemToDelete = this.SelectedEntity;
            itemToDelete.Delete();

            //load a new item
            NewCommand.Execute();

            //commit the delete
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);
                return;
            }

            //update the available items
            _masterEntityInfoListView.FetchAll();

            ShowWaitCursor(false);

        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            CloseWindow();
        }

        #endregion

        #region SyncMerchandisingHierarchyCommand

        private RelayCommand _syncMerchandisingHierarchyCommand;

        /// <summary>
        /// Syncs the merch hierarchy now
        /// </summary>
        public RelayCommand SyncMerchandisingHierarchyCommand
        {
            get
            {
                if (_syncMerchandisingHierarchyCommand == null)
                {
                    _syncMerchandisingHierarchyCommand = new RelayCommand(
                        p => SyncMerchandisingHierarchy_Executed(),
                        p => SyncMerchandisingHierarchy_CanExecute())
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    RegisterCommand(_syncMerchandisingHierarchyCommand);
                }
                return _syncMerchandisingHierarchyCommand;
            }
        }

        private Boolean SyncMerchandisingHierarchy_CanExecute()
        {
            //must have an entity selected
            if (this.SelectedEntity == null)
            {
                SyncMerchandisingHierarchyCommand.DisabledReason =
                    Message.EntitySetup_SynchroniseMerchandisingHierarchy_DisabledInvalidData;
                return false;
            }

            //if no level is selected then disable the button
            if (!this.SelectedProductLevel.HasValue)
            {
                SyncMerchandisingHierarchyCommand.DisabledReason = Message.EntitySetup_SynchroniseMerchandisingHierarchy_NoLevelSelected;
                return false;
            }

            //Must have permission
            if (!_userCanSyncMerchHierarchy)
            {
                SyncMerchandisingHierarchyCommand.DisabledReason = Message.EntitySetup_SynchroniseMerchandisingHierarchy_NoPerm;
                return false;
            }

            //entity must be valid
            if (!this.SelectedEntity.IsValid)
            {
                SyncMerchandisingHierarchyCommand.DisabledReason =
                    Message.EntitySetup_SynchroniseMerchandisingHierarchy_DisabledInvalidData;
                return false;
            }

            return true;
        }

        private void SyncMerchandisingHierarchy_Executed()
        {
            if (!this.SelectedProductLevel.HasValue) return;

            SynchroniseSelectionItem selectedLevel =
                this.AvailableProductLevels.FirstOrDefault(p => (Int32?)p.Value == this.SelectedProductLevel.Value);

            Debug.Assert(selectedLevel != null, "level not found");
            if (selectedLevel == null) return;


            //check if we need to save the entity first
            if (this.SelectedEntity.IsDirty)
            {
                if (SaveCommand.CanExecute())
                {
                    ModalMessageResult result =
                        CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                        Message.EntityMaintenance_SynchMerchHierarchy_WarningHeader,
                        String.Format(CultureInfo.CurrentCulture, Message.EntityMaintenance_SynchMerchHierarchy_WarningDescWithSave, selectedLevel.DisplayName),
                        Message.Generic_Continue,
                        Message.Generic_Cancel);

                    if (result != ModalMessageResult.Button1) return;

                    //perform the save making sure it completed.
                    if (!SaveCurrentItem()) return;
                }
                else
                {
                    //show a message saying that the entity cannot be saved - should never actually get here 
                    // but its just to be safe.
                    CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Info,
                        String.Format(Message.EntityMaintenance_SaveBeforeSync_InvalidHeader, SelectedEntity.Name),
                        Message.EntityMaintenance_SaveBeforeSync_InvalidDescription);
                    return;
                }
            }
            else
            {
                //just confirm the action with the user.
                ModalMessageResult result =
                    CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                    Message.EntityMaintenance_SynchMerchHierarchy_WarningHeader,
                    String.Format(CultureInfo.CurrentCulture, Message.EntityMaintenance_SynchMerchHierarchy_WarningDesc, selectedLevel.DisplayName),
                    Message.Generic_Continue,
                    Message.Generic_Cancel);

                if (result != ModalMessageResult.Button1) return;
            }


            //Now perform the actual sync:
            SyncMerchandisingHierarchyWork work = new SyncMerchandisingHierarchyWork(SelectedEntity);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);

            if (work.Result != null)
            {
                // show message if sync fails 
                CommonHelper.GetWindowService().ShowErrorMessage(Message.EntitySetup_SynchroniseNow_SyncFailedHeader,
                    String.Format(Message.EntitySetup_SynchroniseNow_SyncFailedDescription + "\n" + "{0}", work.Result));
            }
            else
            {
                // show message to say it is complete
                CommonHelper.GetWindowService().ShowMessage(
                    MessageWindowType.Info, Message.EntitySetup_SynchroniseNow_SyncCompleteHeader,
                    Message.EntitySetup_SynchroniseNow_SyncCompleteDescription, 1, Message.Generic_Ok, null, null);
            }

        }

        /// <summary>
        /// Worker to sync the plan hierarchy whilst showing a modal busy.
        /// </summary>
        private sealed class SyncMerchandisingHierarchyWork : IModalBusyWork
        {
            #region Nested classes

            private sealed class WorkArgs
            {
                public Entity Entity { get; set; }
            }

            #endregion

            #region Fields
            private Entity _entity;
            private String _result;
            #endregion

            #region Properties

            public String Result
            {
                get { return _result; }
                private set { _result = value; }
            }

            public Boolean ReportsProgress
            {
                get { return false; }
            }

            public Boolean SupportsCancellation
            {
                get { return false; }
            }

            #endregion

            #region Constructor
            public SyncMerchandisingHierarchyWork(Entity entity)
            {
                _entity = entity;
            }
            #endregion

            #region Methods

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowIndeterminateBusy(
                    Message.EntitySetup_SynchroniseNow_SyncBusyHeader,
                    Message.EntitySetup_SynchroniseNow_SyncBusyDescription);
            }

            public object GetWorkerArgs()
            {
                return new WorkArgs()
                {
                    Entity = _entity
                };
            }

            public void OnDoWork(IModalBusyWorkerService sender, System.ComponentModel.DoWorkEventArgs e)
            {
                WorkArgs args = (WorkArgs)e.Argument;

                args.Entity.SyncPlanHierarchy();
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                //nothing to report.
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    this.Result = e.Error.GetBaseException().Message;
                }
                else if (!e.Cancelled)
                {
                    this.Result = null;
                }

            }

            #endregion
        }

        #endregion

        #region SetLocation

        private RelayCommand _setLocationCommand;

        /// <summary>
        /// Sets the ocation value
        /// </summary>
        public RelayCommand SetLocationCommand
        {
            get
            {
                if (_setLocationCommand == null)
                {
                    _setLocationCommand = new RelayCommand(
                        p => SetLocation_Executed(p as String),
                        p => SetLocation_CanExecute(p as String))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    RegisterCommand(_setLocationCommand);
                }
                return _setLocationCommand;
            }
        }

        private Boolean SetLocation_CanExecute(String propertyName)
        {
            return this.SelectedEntity != null;
        }

        private void SetLocation_Executed(String propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return;

            String currentLocation = GetLocation(propertyName);

            //TODO: Refactor this out.
            if (!HasAttachedControl()) return;

            FolderBrowserDialog dia = new FolderBrowserDialog
            {
                SelectedPath = currentLocation,
                ShowNewFolderButton = true
            };

            if (dia.ShowDialog() == DialogResult.OK)
            {
                SetLocation(propertyName, dia.SelectedPath);
            }
        }

        private String GetLocation(String propertyName)
        {
            switch (propertyName)
            {
                case "ProductImageLocation":
                    return this.SelectedEntity.SystemSettings.ProductImageLocation;

                default:
                    Debug.Fail("Not handled");
                    return null;
            }
        }

        private void SetLocation(String propertyName, String newValue)
        {
            switch (propertyName)
            {
                case "ProductImageLocation":
                    this.SelectedEntity.SystemSettings.ProductImageLocation = newValue;
                    break;

                default:
                    Debug.Fail("Not handled");
                    break;
            }
        }


        #endregion

        #region SetImageProductAttributeFilter

        private RelayCommand _setImageProductAttributeFilterCommand;

        /// <summary>
        /// Sets the image attribute filter value.
        /// </summary>
        public RelayCommand SetImageProductAttributeFilterCommand
        {
            get
            {
                if (_setImageProductAttributeFilterCommand == null)
                {
                    _setImageProductAttributeFilterCommand = new RelayCommand(
                            o => SetImageProductAttributeFilter_Executed(),
                            o => SetImageProductAttributeFilter_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    RegisterCommand(_setImageProductAttributeFilterCommand);
                }
                return _setImageProductAttributeFilterCommand;
            }
        }

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramComparisonEditorViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<EntityMaintenanceViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #region Product Field Picker View Model

        private Ccm.Common.Wpf.Selectors.FieldPickerViewModel _allFieldPickerViewModel;

        public Ccm.Common.Wpf.Selectors.FieldPickerViewModel AllFieldPickerViewModel { get { return _allFieldPickerViewModel; } }

        public static readonly PropertyPath AllFieldPickerViewModelProperty = GetPropertyPath(o => o.AllFieldPickerViewModel);

        #endregion

        #region Product Field Picker View Model

        private Ccm.Common.Wpf.Selectors.FieldPickerViewModel _productFieldPickerViewModel;

        public Ccm.Common.Wpf.Selectors.FieldPickerViewModel ProductFieldPickerViewModel { get { return _productFieldPickerViewModel; } }

        public static readonly PropertyPath ProductFieldPickerViewModelProperty = GetPropertyPath(o => o.ProductFieldPickerViewModel);

        #endregion

        

        #endregion

        private Boolean SetImageProductAttributeFilter_CanExecute()
        {
            Entity selectedEntity = this.SelectedEntity;
            return selectedEntity != null && selectedEntity.SystemSettings.ProductImageSource == RealImageProviderType.Folder;
        }

        private void SetImageProductAttributeFilter_Executed()
        {
            // Prompt the user for the product attribute to fetch images with.
            String fieldValue = String.Empty;

            //  Add the product attributes available when creating the field selector view model.
            FieldSelectorGroup[] availableFieldGroups =
            {
                new FieldSelectorGroup(Message.FieldSelectorGroup_ProductAttributes, 
                    PlanogramProduct.EnumerateDisplayableFieldInfos(/*incMetadata*/false, /*incCustom*/true, /*incPerformance*/false)),
            };

            FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                       FieldSelectorInputType.SingleField, FieldSelectorResolveType.SingleValue,
                       fieldValue, availableFieldGroups);
            GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);

            if (viewModel.DialogResult != true) return;

            if (viewModel.SelectedField != null)
            {
                this.SelectedEntity.SystemSettings.ProductImageAttribute = viewModel.SelectedField.FieldPlaceholder;
            }
        }

        #endregion


        #region Event Handlers

        /// <summary>
        /// Called whenever the selected entity changes
        /// </summary>
        private void OnSelectedEntityChanged(Entity oldModel, Entity newModel)
        {
            _masterProductLevels.Clear(); // Clear any previous master product levels list.
            _selectedProductLevel = null;

            List<SelectionItem> productLabels = new List<SelectionItem>{SelectionItem.None };
            List<SelectionItem> fixtureLabels = new List<SelectionItem>{SelectionItem.None };
            List<SelectionItem> highlights = new List<SelectionItem>{SelectionItem.None };

            if (oldModel != null)
            {
                oldModel.ChildChanged -= Settings_PropertyChanged;
            }

            if (newModel != null)
            {
                newModel.ChildChanged += Settings_PropertyChanged;

                //if the model is not new, try to fetch the product hierarchy.
                if (!newModel.IsNew)
                {
                    _selectedProductLevel = newModel.ProductLevelId;
                    _masterProductLevels.Add(SynchroniseSelectionItem.None);

                    try
                    {
                        //Hierarchy levels
                        ProductHierarchy entityProductHierarchy = ProductHierarchy.FetchByEntityId(newModel.Id);
                        foreach (ProductLevel level in entityProductHierarchy.EnumerateAllLevels())
                        {
                            if (level != entityProductHierarchy.RootLevel)
                            {
                                _masterProductLevels.Add(new SynchroniseSelectionItem(level.Name, level.Id));
                            }
                        }

                        //Labels
                        LabelInfoList labelInfos = LabelInfoList.FetchByEntityId(newModel.Id);
                        productLabels.AddRange(labelInfos.Where(l=> l.Type == LabelType.Product).OrderBy(l=> l.Name).Select(l=> new SelectionItem(l.Name, l.Name)));
                        fixtureLabels.AddRange(labelInfos.Where(l=> l.Type == LabelType.Fixture).OrderBy(l=> l.Name).Select(l=> new SelectionItem(l.Name, l.Name)));

                        //Highlights
                        HighlightInfoList highlightInfos = HighlightInfoList.FetchByEntityId(newModel.Id);
                        highlights.AddRange(highlightInfos.OrderBy(h => h.Name).Select(l => new SelectionItem(l.Name, l.Name)));

                        UpdateEntityComparisonAttributes(newModel);
                    }
                    catch (DataPortalException ex) 
                    {
                        RecordException(ex, _exCategory);
                        // No need to worry if it does not exist, the user will just not get a list of product levels.
                    } 
                }


                
            }

            //set labels and highlights
            this.AvailableProductLabels = productLabels.AsReadOnly();
            this.AvailableFixtureLabels = fixtureLabels.AsReadOnly();
            this.AvailableHighlights = highlights.AsReadOnly();
        }

        private void UpdateEntityComparisonAttributes(Entity newEntity)
        {
            ClearSelectedComparisonAttributes();

            if (!newEntity.ComparisonAttributes.Any()) return;
            var selectedProductFields = new List<IPlanogramComparisonSettingsField>();

            foreach (EntityComparisonAttribute attribute in newEntity.ComparisonAttributes)
            {
                FieldPickerField foundField =
                    ProductFieldPickerViewModel.AvailableFieldPickerFields.FirstOrDefault(
                        field => field.FieldPlaceholder == attribute.PropertyName);

                if (foundField != null)
                    selectedProductFields.Add(FieldPickerRow.CreateFromField(foundField, true));
            }

            ProductFieldPickerViewModel.SetObjectFieldInfos(selectedProductFields);
        }

        private void ClearSelectedComparisonAttributes()
        {
            ProductFieldPickerViewModel.SetObjectFieldInfos(new List<IPlanogramComparisonSettingsField>());
        }

        /// <summary>
        /// Called whenever the selected product level changes.
        /// </summary>
        private void OnSelectedProductLevelChanged(Int32? newValue)
        {
            if (SelectedEntity == null) return;

            SelectedEntity.ProductLevelId = newValue;
            OnPropertyChanged(SelectedEntityProperty);
        }

        /// <summary>
        /// Called whenever a property changes on the SystemSettings model.
        /// </summary>
        private void Settings_PropertyChanged(object sender, ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null)
            {
                if (e.PropertyChangedArgs.PropertyName == SystemSettings.LengthUnitOfMeasureProperty.Name)
                {
                    OnPropertyChanged(DisplayUomProperty);
                }
                OnPropertyChanged(SelectedEntityProperty);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// returns a list of entities that match the given criteria
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityInfo> GettingMatchingEntity(String codeOrNameCriteria)
        {
            string invariantCriteria = codeOrNameCriteria.ToLowerInvariant();
            return this.AvailableEntities.Where(
                    p => p.Name.ToLowerInvariant().Contains(invariantCriteria));
        }

        /// <summary>
        /// shows a warning requestung user ok to continue if current item is dirty
        /// </summary>
        /// <returns> true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.SelectedEntity, this.SaveCommand);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //save the entity type list so that any edits are committed
                    _selectedEntity = null;
                    _masterEntityInfoListView.Dispose();

                    DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    
}
