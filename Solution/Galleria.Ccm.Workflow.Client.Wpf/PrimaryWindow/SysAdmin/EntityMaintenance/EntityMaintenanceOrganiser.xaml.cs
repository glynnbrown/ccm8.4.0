﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25534 : I.George
//	Initial version
#endregion

#region Version History: (CCM 8.01)
// V8-28928 : L.Ineson
//  Amended to use new windowviewmodelbase.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EntityMaintenance
{
    /// <summary>
    /// Interaction logic for EntityMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class EntityMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(EntityMaintenanceViewModel), typeof(EntityMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public EntityMaintenanceViewModel ViewModel
        {
            get { return (EntityMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            EntityMaintenanceOrganiser senderControl = (EntityMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                EntityMaintenanceViewModel oldModel = (EntityMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                EntityMaintenanceViewModel newModel = (EntityMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public EntityMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.EntityMaintenance);

            this.ViewModel = new EntityMaintenanceViewModel();
            this.Loaded += new RoutedEventHandler(EntityMaintenanceOrganiser_Loaded);
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        private void EntityMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(EntityMaintenanceOrganiser_Loaded);

            //cancel the busy cursor 
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                // this.xBackstageOpen.IsSelected = true;
            }
        }

        #endregion

        #region Window Close
        /// <summary>
        /// when the application wants to be closed, check if it 
        /// needs to be saved
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }
            }
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

    }
}
