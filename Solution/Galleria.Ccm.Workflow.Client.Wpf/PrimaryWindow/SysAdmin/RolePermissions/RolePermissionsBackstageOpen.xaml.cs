﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Collections.Generic;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.RolePermissions
{
    /// <summary>
    /// Interaction logic for RolePermissionsBackstageOpen.xaml
    /// </summary>
    public partial class RolePermissionsBackstageOpen : UserControl
    {
        #region Fields
        private ObservableCollection<RoleInfo> _searchResults = new ObservableCollection<RoleInfo>();
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(RolePermissionsViewModel), typeof(RolePermissionsBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public RolePermissionsViewModel ViewModel
        {
            get { return (RolePermissionsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            RolePermissionsBackstageOpen senderControl = (RolePermissionsBackstageOpen)obj;

            if (e.OldValue != null)
            {
                RolePermissionsViewModel oldModel = (RolePermissionsViewModel)e.OldValue;
                oldModel.AvailableRoles.BulkCollectionChanged -= senderControl.ViewModel_AvailableRolesBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                RolePermissionsViewModel newModel = (RolePermissionsViewModel)e.NewValue;
                newModel.AvailableRoles.BulkCollectionChanged += senderControl.ViewModel_AvailableRolesBulkCollectionChanged;
            }
        }

        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(RolePermissionsBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            RolePermissionsBackstageOpen senderControl = (RolePermissionsBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<RoleInfo>),
            typeof(RolePermissionsBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<RoleInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<RoleInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }


        #endregion

        #endregion

        #region Constructor

        public RolePermissionsBackstageOpen()
        {
            InitializeComponent();
            this.Loaded += RolePermissionsBackstageOpen_Loaded;
        }

        private void RolePermissionsBackstageOpen_Loaded(Object sender, RoutedEventArgs e)
        {
            this.Loaded -= RolePermissionsBackstageOpen_Loaded;
            UpdateSearchResults();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<RoleInfo> results = this.ViewModel.GetMatchingRoles(this.SearchText.ToUpperInvariant());

                foreach (RoleInfo roleInfo in results)
                {
                    _searchResults.Add(roleInfo);
                }

                searchResultsListBox.ItemsSource = _searchResults;
            }
        }

        public void ClearSearchResults()
        {
            _searchResults.Clear();
        }

        #endregion

        #region Event Handler

        private void ViewModel_AvailableRolesBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                UpdateSearchResults();
            }
        }

        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                RoleInfo roleToOpen = (RoleInfo)senderControl.SelectedItem;

                if (roleToOpen != null)
                {
                    this.ViewModel.OpenCommand.Execute(roleToOpen.Id);
                }
            }
        }

        #endregion
    }
}
