﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//  Copied from GFS
#endregion
#endregion

using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.RolePermissions
{
    /// <summary>
    /// Interaction logic for RolePermissionsBackstageNew.xaml
    /// </summary>
    public partial class RolePermissionsBackstageNew : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(RolePermissionsViewModel),
            typeof(RolePermissionsBackstageNew),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public RolePermissionsViewModel ViewModel
        {
            get { return (RolePermissionsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion
        #endregion

        #region Constructor

        public RolePermissionsBackstageNew()
        {
            InitializeComponent();
        }

        #endregion
    }
}
