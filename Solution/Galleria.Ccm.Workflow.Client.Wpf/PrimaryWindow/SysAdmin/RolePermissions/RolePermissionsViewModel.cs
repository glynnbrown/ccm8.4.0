﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//  Copied from GFS
// V8-24779 : D.Pleasance
//  Added PlanogramImportTemplate
// V8-28252 : J.Pickup
//  Added Planogram Permission. 
// V8-28393 : J.Pickup
//  Open command now resets the TabItem to general. 
#endregion
#region Version History: (CCM 801)
// V8-28701 : J.Pickup
// Unlock Planogram child object has now been introduced to the permissions tree. 
// V8-28702 : J.Pickup
// Sync Planogram hierrarchy with merch hierrarchy child object has now been introduced to the permissions tree. 
#endregion
#region Version History: (CCM 802)
// V8-29010 : D.Pleasance
//  Added PlanogramNameTemplate
#endregion
#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendly description to saveAsCommand and Delete Command
#endregion
#region Version History : CCM820
// V8-30527 : A.Kuszyk
//  Added permissions item for planogram hierarchy group delete.
// V8-30738 : L.Ineson
//  Added permissions for Printing Template
#endregion
#region Version History : CCM830
// V8-32854 : M.Brumby
//  Save check for number of entities was backwards
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.RolePermissions
{
    public enum RoleTab
    {
        General,
        Members,
        Permissions,
        ADIntegration,
        Entity
    }

    public sealed class RolePermissionsViewModel : ViewModelAttachedControlObject<RolePermissionsWindow>
    {
        #region Fields

        const String _exCategory = "RolePermissionsMaintenance";
        private ModelPermission<Role> _rolePermissions = new ModelPermission<Role>();

        private RoleTab _currentTab = RoleTab.General;

        //Master View Models
        private RoleInfoListViewModel _masterRoleInfoListView;
        private UserInfoListViewModel _masterUserInfoListView;
        private RoleMemberListViewModel _masterRoleMemberListView = new RoleMemberListViewModel();

        //Roles
        private BulkObservableCollection<RoleInfo> _availableRoles = new BulkObservableCollection<RoleInfo>();
        private ReadOnlyBulkObservableCollection<RoleInfo> _availableRolesRO;
        private Role _selectedRole;
        private Boolean _isNotAdmin = true;

        //Permissions
        private BulkObservableCollection<RolePermissionGroupObject> _availablePermissions = new BulkObservableCollection<RolePermissionGroupObject>();

        //Available Users
        private BulkObservableCollection<UserInfo> _availableUsers = new BulkObservableCollection<UserInfo>();
        private BulkObservableCollection<UserInfo> _selectedAvailableUsers = new BulkObservableCollection<UserInfo>();

        //Taken Users
        private BulkObservableCollection<UserInfo> _takenUsers = new BulkObservableCollection<UserInfo>();
        private ReadOnlyBulkObservableCollection<UserInfo> _takenUsersRO;
        private BulkObservableCollection<UserInfo> _selectedTakenUsers = new BulkObservableCollection<UserInfo>();

        //Entities
        private BulkObservableCollection<EntityInfo> _availableEntities = new BulkObservableCollection<EntityInfo>();
        private ReadOnlyBulkObservableCollection<EntityInfo> _availableEntitiesRO;
        private BulkObservableCollection<EntityInfo> _selectedEntities = new BulkObservableCollection<EntityInfo>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CurrentTabProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.CurrentTab);
        //Roles
        public static readonly PropertyPath SelectedRoleProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SelectedRole);
        public static readonly PropertyPath AvailableRolesProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.AvailableRoles);
        public static readonly PropertyPath AvailablePermissionsProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.AvailablePermissions);
        public static readonly PropertyPath IsNotAdminProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.IsNotAdmin);
        //Available Users
        public static readonly PropertyPath AvailableUsersProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.AvailableUsers);
        public static readonly PropertyPath SelectedAvailableUsersProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SelectedAvailableUsers);
        //Taken Users
        public static readonly PropertyPath TakenUsersProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.TakenUsers);
        public static readonly PropertyPath SelectedTakenUsersProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SelectedTakenUsers);
        //Entities
        public static readonly PropertyPath AvailableEntitiesProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.AvailableEntities);
        public static readonly PropertyPath SelectedEntitiesProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SelectedEntities);

        //Commands
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<RolePermissionsViewModel>(p => p.CloseCommand);

        #endregion

        #region Properties

        public RoleTab CurrentTab
        {
            get { return _currentTab; }
            set
            {
                _currentTab = value;
                OnPropertyChanged(CurrentTabProperty);
            }
        }

        #region Role Related

        /// <summary>
        /// Returns the collection of available Roles
        /// </summary>
        public ReadOnlyBulkObservableCollection<RoleInfo> AvailableRoles
        {
            get
            {
                if (_availableRolesRO == null)
                {
                    _availableRolesRO = new ReadOnlyBulkObservableCollection<RoleInfo>(_availableRoles);
                }
                return _availableRolesRO;
            }
        }


        /// <summary>
        /// Gets/Sets the selected role
        /// </summary>
        public Role SelectedRole
        {
            get { return _selectedRole; }
            set
            {
                Role oldValue = _selectedRole;
                _selectedRole = value;

                OnPropertyChanged(SelectedRoleProperty);
                OnSelectedRoleChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns if the current role is not an admin role
        /// </summary>
        public Boolean IsNotAdmin
        {
            get { return _isNotAdmin; }
            private set
            {
                _isNotAdmin = value;
                OnPropertyChanged(IsNotAdminProperty);
            }
        }

        /// <summary>
        /// Returns the available permissions
        /// </summary>
        public BulkObservableCollection<RolePermissionGroupObject> AvailablePermissions
        {
            get { return _availablePermissions; }
        }

        #endregion

        #region User Related

        /// <summary>
        /// Returns the Available Users
        /// </summary>
        public BulkObservableCollection<UserInfo> AvailableUsers
        {
            get { return _availableUsers; }
            set { _availableUsers = value; }
        }

        /// <summary>
        /// Returns the Selected Available Users
        /// </summary>
        public BulkObservableCollection<UserInfo> SelectedAvailableUsers
        {
            get { return _selectedAvailableUsers; }
            set { _selectedAvailableUsers = value; }
        }

        /// <summary>
        /// Returns the Selected Taken Users
        /// </summary>
        public BulkObservableCollection<UserInfo> SelectedTakenUsers
        {
            get { return _selectedTakenUsers; }
            set { _selectedTakenUsers = value; }
        }

        /// <summary>
        /// Returns the Taken Users
        /// </summary>
        public ReadOnlyBulkObservableCollection<UserInfo> TakenUsers
        {
            get
            {
                if (_takenUsersRO == null)
                {
                    _takenUsersRO = new ReadOnlyBulkObservableCollection<UserInfo>(_takenUsers);
                }
                return _takenUsersRO;
            }
        }

        #endregion

        /// <summary>
        /// The Entity List
        /// </summary>
        public ReadOnlyBulkObservableCollection<EntityInfo> AvailableEntities
        {
            get
            {
                if (_availableEntitiesRO == null)
                {
                    _availableEntitiesRO = new ReadOnlyBulkObservableCollection<EntityInfo>(_availableEntities);
                }
                return _availableEntitiesRO;
            }
        }

        /// <summary>
        /// The selected entities
        /// </summary>
        public BulkObservableCollection<EntityInfo> SelectedEntities
        {
            get { return _selectedEntities; }
            set
            {
                _selectedEntities = value;
                OnPropertyChanged(SelectedEntitiesProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public RolePermissionsViewModel()
        {
            _availableEntities.AddRange(EntityInfoList.FetchAllEntityInfos(true));
            _selectedEntities.BulkCollectionChanged += SelectedEntities_BulkCollectionChanged;

            //UserInfoListViewModel
            //(GFS-17888) load the _masterUserInfoListView here
            _masterUserInfoListView = new UserInfoListViewModel();
            _masterUserInfoListView.FetchAll();
            _masterUserInfoListView.ModelChanged += MasterUserInfoListView_ModelChanged;

            //RoleInfoListViewModel
            _masterRoleInfoListView = new RoleInfoListViewModel();
            _masterRoleInfoListView.FetchAll();
            _masterRoleInfoListView.ModelChanged += MasterRoleInfoListViewModel_Changed;
            OnMasterRoleInfoListViewModelChanged(_masterRoleInfoListView.Model);

            //Subscribe to role member list model changed event
            _masterRoleMemberListView.ModelChanged += MasterRoleMemberListView_ModelChanged;

            //Construct permissions
            ConstructPermissions();

            //load a new item
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new user object
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        SmallIcon = ImageResources.New_16,
                        Icon = ImageResources.New_32,
                        InputGestureKey = Key.N,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }
        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permissions
            if (!_rolePermissions.CanCreate)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }


        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                this.SelectedRole = Role.NewRole();
                this.AttachedControl.SetRibbonBackstageState(/*isOpen*/false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens the role object with the given id
        /// param: Int32 id
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed((Int32)p),
                        p => Open_CanExecute((Int32?)p))
                    {
                        FriendlyName = Message.Generic_Open,
                        InputGestureKey = Key.O,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        private bool Open_CanExecute(Int32? roleId)
        {
            //must not be null
            if (roleId == null)
            {
                _openCommand.DisabledReason = Message.RolePermissions_OpenCommand_DisabledReason;
                return false;
            }

            //user must have get permission
            if (!_rolePermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32 roleId)
        {
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.SelectedRole = Role.FetchById(roleId);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }

                //close the backstage after setting the current tab (Some get disabled).
                this.CurrentTab = RoleTab.General;
                this.AttachedControl.SetRibbonBackstageState(false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current user
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        DisabledReason = Message.Generic_Save_DisabledReasonInvalidData,
                        SmallIcon = ImageResources.Save_16,
                        Icon = ImageResources.Save_32,
                        InputGestureKey = Key.S,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }

        }

        private bool Save_CanExecute()
        {
            //may not be null
            if (this.SelectedRole == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have edit permission
            if (!_rolePermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedRole.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //must have acess to at least one entity
            if (!this.SelectedRole.IsAdministratorRole && !this.SelectedRole.Entities.Any())
            {
                this.SaveCommand.DisabledReason = Message.RolesPermissions_SaveDisabled_NoEntity;
                this.SaveAndCloseCommand.DisabledReason = Message.RolesPermissions_SaveDisabled_NoEntity;
                this.SaveAndNewCommand.DisabledReason = Message.RolesPermissions_SaveDisabled_NoEntity;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** check the item unique value
            String newRoleName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Int32 id = this.SelectedRole.Id;
                       IEnumerable<String> takenValues =
                           this.AvailableRoles.Where(p => p.Id != id).Select(p => p.Name.ToUpperInvariant().Trim());

                       return !takenValues.Contains(s.ToUpperInvariant().Trim());
                   };

                nameAccepted =
                    Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(isUniqueCheck, this.SelectedRole.Name, out newRoleName);
            }
            else
            {
                //force a unique value for unit testing
                Int32 id = this.SelectedRole.Id;
                newRoleName = Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedRole.Name,
                    this.AvailableRoles.Where(p => p.Id != id).Select(p => p.Name));
            }

            //set the code
            if (nameAccepted)
            {
                if (this.SelectedRole.Name != newRoleName)
                {
                    this.SelectedRole.Name = newRoleName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //Check that the current role has access to at least one entity
            if (continueWithSave)
            {
                if ((!this.SelectedRole.IsAdministratorRole) && (!this.SelectedRole.Entities.Any()))
                {
                    continueWithSave = false;
                    if (this.AttachedControl != null)
                    {
                        ModalMessage msg = new ModalMessage();
                        msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                        msg.Owner = this.AttachedControl;
                        msg.Title = Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                        msg.Header = Message.RolesPermissions_RoleHasNoEntityHeader;
                        msg.Description = Message.RolesPermissions_RoleHasNoEntityDescription;
                        msg.ButtonCount = 1;
                        msg.Button1Content = Message.Generic_Ok;
                        msg.DefaultButton = ModalMessageButton.Button1;
                        msg.MessageIcon = ImageResources.Dialog_Warning;
                        msg.ShowDialog();
                    }
                }
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    //AvailablePermissions.ResetInitialLinks(); ?

                    this.SelectedRole = this.SelectedRole.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedRole.Name);
                        if (itemReloadRequired)
                        {
                            this.SelectedRole = Role.FetchById(this.SelectedRole.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.SelectedRole.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterRoleInfoListView.FetchAll();
                //_masterUserInfoListView.FetchAll();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        Icon = ImageResources.SaveAndClose_16,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes the save command then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        Icon = ImageResources.SaveAndNew_16,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }


        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current details as a new item
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureKey = Key.F12,
                        InputGestureModifiers = ModifierKeys.None
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //may not be null
            if (this.SelectedRole == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have edit permission
            if (!_rolePermissions.CanEdit)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedRole.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyRoleName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                (s) =>
                {
                    return !this.AvailableRoles.Select(p => p.Name.ToUpperInvariant().Trim()).Contains(s.ToUpperInvariant().Trim());
                };

                nameAccepted =
                    Framework.Controls.Wpf.Helpers.PromptForSaveAsName(isUniqueCheck, String.Empty, out copyRoleName);
            }
            else
            {
                //force a unique value for unit testing
                copyRoleName = Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedRole.Name, this.AvailableRoles.Select(p => p.Name));
            }

            if (nameAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item and save
                Role itemCopy = this.SelectedRole.Copy();
                itemCopy.Name = copyRoleName;
                itemCopy.MarkGraphAsNew();
                itemCopy.MarkGraphAsInitialized();
                itemCopy = itemCopy.Save();

                //set the copy as the new selected item
                this.SelectedRole = itemCopy;

                //update the available infos
                _masterRoleInfoListView.FetchAll();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the current item
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //must not be null
            if (this.SelectedRole == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have delete permission
            if (!_rolePermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be new
            if (this.SelectedRole.IsNew)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //[GFS-16189] Must not be the built in admin role.
            if (this.SelectedRole.IsAdministratorRole)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedRole.ToString());
            }

            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                Role itemToDelete = this.SelectedRole;
                itemToDelete.Delete();

                //Load a new role
                this.NewCommand.Execute();

                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.ToString(), OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //Refresh the RoleInfo list
                _masterRoleInfoListView.FetchAll();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region AddUserCommand

        private RelayCommand _adduserCommand;

        public RelayCommand AddUserCommand
        {
            get
            {
                if (_adduserCommand == null)
                {
                    _adduserCommand = new RelayCommand(
                        p => AddUser_Executed(),
                        p => AddUser_CanExecute())
                    {
                        FriendlyName = Message.RolePermissions_AddUser,
                        FriendlyDescription = Message.RolePermissions_AddUser_Description,
                        SmallIcon = ImageResources.Add_16,
                        Icon = ImageResources.Add_32
                    };
                    base.ViewModelCommands.Add(_adduserCommand);
                }
                return _adduserCommand;
            }
        }

        private Boolean AddUser_CanExecute()
        {
            //Must have at least one available users selected
            if (_selectedAvailableUsers.Count == 0)
            {
                _adduserCommand.DisabledReason = Message.RolePermissions_AddUser_DisabledNoSelectedUser;
                return false;
            }

            return true;
        }

        private void AddUser_Executed()
        {
            foreach (UserInfo userInfo in _selectedAvailableUsers.ToList())
            {
                RoleMember roleMember = RoleMember.NewRoleMember(userInfo.Id, this.SelectedRole.Id);

                this.SelectedRole.Members.Add(roleMember);
                _takenUsers.Add(userInfo);
                _availableUsers.Remove(userInfo);
            }
        }

        #endregion

        #region RemoveUserCommand

        private RelayCommand<UserInfo> _removeUserCommand;

        /// <summary>
        /// Removes the user from the role
        /// </summary>
        public RelayCommand<UserInfo> RemoveUserCommand
        {
            get
            {
                if (_removeUserCommand == null)
                {
                    _removeUserCommand = new RelayCommand<UserInfo>(
                        p => RemoveUser_Executed(),
                        p => RemoveUser_CanExecute())
                    {
                        FriendlyName = Message.RolePermissions_RemoveUser,
                        FriendlyDescription = Message.RolePermissions_RemoveUser_Description,
                        SmallIcon = ImageResources.Delete_16,
                        Icon = ImageResources.Delete_32
                    };
                    base.ViewModelCommands.Add(_removeUserCommand);
                }
                return _removeUserCommand;
            }
        }

        private Boolean RemoveUser_CanExecute()
        {
            //user must have delete permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(RoleMember)))
            {
                _removeUserCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //Must have at least one available users selected
            if (_selectedTakenUsers.Count == 0)
            {
                _removeUserCommand.DisabledReason = Message.RolePermissions_RemoveUser_DisabledNoSelectedUser;
                return false;
            }

            return true;
        }

        private void RemoveUser_Executed()
        {
            //Get the RoleMember and remove it
            foreach (UserInfo userInfo in _selectedTakenUsers.ToList())
            {
                RoleMember toRemove = this.SelectedRole.Members.FirstOrDefault(p => p.UserId == userInfo.Id);
                if (toRemove != null)
                {
                    this.SelectedRole.Members.Remove(toRemove);
                    _takenUsers.Remove(userInfo);
                    _availableUsers.Add(userInfo);
                }
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(), p => Close_CanExecute())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// MasterUserInfoView
        /// </summary>
        private void MasterUserInfoListView_ModelChanged(Object sender, ViewStateObjectModelChangedEventArgs<UserInfoList> e)
        {
            OnMasterUserInfoViewModelChanged(e.NewModel);
        }
        private void OnMasterUserInfoViewModelChanged(UserInfoList newModel)
        {
            _availableUsers.Clear();
            _availableUsers.AddRange(newModel);
        }

        /// <summary>
        /// MasterRoleMembersView
        /// </summary>
        private void MasterRoleMemberListView_ModelChanged(Object sender, ViewStateObjectModelChangedEventArgs<RoleMemberList> e)
        {
            OnMasterRoleMemberListViewModelChanged(e.NewModel);
        }
        private void OnMasterRoleMemberListViewModelChanged(RoleMemberList newModel)
        {
            _takenUsers.Clear();

            if (newModel != null)
            {
                foreach (RoleMember member in newModel)
                {
                    UserInfo userInfo = _masterUserInfoListView.Model.FirstOrDefault(p => p.Id == member.UserId);
                    if (userInfo != null)
                    {
                        _takenUsers.Add(userInfo);
                        _availableUsers.Remove(userInfo);
                    }
                }
            }
        }

        /// <summary>
        /// MasterRoleInfoView
        /// </summary>
        private void MasterRoleInfoListViewModel_Changed(Object sender, ViewStateObjectModelChangedEventArgs<RoleInfoList> e)
        {
            OnMasterRoleInfoListViewModelChanged(e.NewModel);
        }
        private void OnMasterRoleInfoListViewModelChanged(RoleInfoList newModel)
        {
            _availableRoles.Clear();
            _availableRoles.AddRange(newModel);
        }

        private void SelectedEntities_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EntityInfo entityInfo in e.ChangedItems)
                    {
                        this.SelectedRole.Entities.Add(RoleEntity.NewRoleEntity(entityInfo.Id));
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (EntityInfo entityInfo in e.ChangedItems)
                    {
                        RoleEntity roleEntity = this.SelectedRole.Entities.FirstOrDefault(p => p.EntityId == entityInfo.Id);
                        if (roleEntity != null)
                        {
                            this.SelectedRole.Entities.Remove(roleEntity);
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    SelectEntities();
                    break;
            }
        }

        /// <summary>
        /// SelectedRole Changed
        /// </summary>
        private void OnSelectedRoleChanged(Role oldModel, Role newModel)
        {
            //Update the permissions and members
            if (newModel != null)
            {
                IsNotAdmin = !newModel.IsAdministratorRole;

                //refresh the roleList
                _masterRoleInfoListView.FetchAll();

                RoleInfo roleInfo = _masterRoleInfoListView.Model.FirstOrDefault(p => p.Id == newModel.Id);
                if (roleInfo != null)
                {
                    //Add existing permissions
                    this.UpdatePermissions(newModel.Permissions);

                    OnMasterUserInfoViewModelChanged(_masterUserInfoListView.Model);

                    _masterRoleMemberListView.Model = newModel.Members;
                }
                else
                {
                    //Add existing permissions
                    this.UpdatePermissions(null);

                    OnMasterUserInfoViewModelChanged(_masterUserInfoListView.Model);

                    _masterRoleMemberListView.Model = RoleMemberList.NewList();
                }
                SelectEntities();
            }
        }


        private void RolePermissionGroupObject_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == RolePermissionGroupObject.IsCheckedProperty.Path)
            {
                RolePermissionGroupObject senderObj = (RolePermissionGroupObject)sender;

                //If role level
                if (senderObj.SourcePermission.HasValue)
                {
                    if (senderObj.IsChecked.HasValue)
                    {
                        if ((Boolean)senderObj.IsChecked)
                        {
                            //Remove if needed from role
                            RolePermission permissionToAdd = this.SelectedRole.Permissions.FirstOrDefault(p => p.PermissionType == (Int16)(DomainPermission)senderObj.SourcePermission);
                            //Add if needed to role - stop duplicates forming
                            if (permissionToAdd == null)
                            {
                                this.SelectedRole.Permissions.Add(RolePermission.NewRolePermission((Int16)(DomainPermission)senderObj.SourcePermission));
                            }
                        }
                        else
                        {
                            //Remove if exists from role
                            RolePermission permissionToRemove = this.SelectedRole.Permissions.FirstOrDefault(p => p.PermissionType == (Int16)(DomainPermission)senderObj.SourcePermission);
                            if (permissionToRemove != null)
                            {
                                this.SelectedRole.Permissions.Remove(permissionToRemove);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the selected entities for the existing role
        /// Or selected the first available entity for a new role
        /// </summary>
        private void SelectEntities()
        {
            _selectedEntities.BulkCollectionChanged -= SelectedEntities_BulkCollectionChanged;
            this.SelectedEntities.Clear();

            foreach (RoleEntity roleEntity in this.SelectedRole.Entities)
            {
                EntityInfo entityInfo = _availableEntities.FirstOrDefault(p => p.Id == roleEntity.EntityId);

                if (entityInfo != null)
                {
                    //Select all the entities that are stored
                    this.SelectedEntities.Add(entityInfo);
                }
            }

            _selectedEntities.BulkCollectionChanged += SelectedEntities_BulkCollectionChanged;
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;

            //do not show save dialog on delete
            if (this.SelectedRole != null)
            {
                if (this.SelectedRole.IsDeleted)
                {
                    return continueExecute;
                }
            }

            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedRole, this.SaveCommand);
            }

            return continueExecute;
        }

        /// <summary>
        /// Returns a filtered list of RoleInfos to display in the backstage open
        /// </summary>
        public IEnumerable<RoleInfo> GetMatchingRoles(String nameCriteria)
        {
            return _masterRoleInfoListView.Model.Where(
                        p => p.Name.ToUpperInvariant().Contains(nameCriteria) ||
                             p.Description.ToUpperInvariant().Contains(nameCriteria));
        }

        /// <summary>
        /// Returns a filtered list of UserInfos to display in the Role Members tab
        /// </summary>
        public IEnumerable<UserInfo> GetMatchingUsers(String criteria)
        {
            return _masterUserInfoListView.Model.Where(
                p => p.UserName.ToUpperInvariant().Contains(criteria) ||
                     p.FirstName.ToUpperInvariant().Contains(criteria) ||
                     p.LastName.ToUpperInvariant().Contains(criteria));
        }

        /// <summary>
        /// Construct the permission objects
        /// </summary>
        public void ConstructPermissions()
        {
            _availablePermissions.Clear();

            // Create root group
            RolePermissionGroupObject rootGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions, false);

            // Area groups
            RolePermissionGroupObject contentSetupGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions_ContentSetup, false);
            RolePermissionGroupObject dataManagementGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions_DataManagement, false);
            RolePermissionGroupObject homeGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions_Home, false);
            RolePermissionGroupObject masterDataSetupGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions_MasterDataSetup, false);
            RolePermissionGroupObject systemAdministrationGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions_SystemAdministration, false);
            RolePermissionGroupObject systemSetupGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions_SystemSetup, false);
            RolePermissionGroupObject planogramRootGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Permissions_Planogram, false);

            #region Content Setup

            #region Assortment
            RolePermissionGroupObject assortmentGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Assortment, false);
            assortmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentGet, false));
            assortmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentCreate, false));
            assortmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentEdit, false));
            assortmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentDelete, false));
            #endregion

            #region Assortment Minor Revision
            RolePermissionGroupObject assortmentMinorGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_AssortmentMinorRevision, false);
            assortmentMinorGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentMinorRevisionGet, false));
            assortmentMinorGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentMinorRevisionCreate, false));
            assortmentMinorGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentMinorRevisionEdit, false));
            assortmentMinorGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.AssortmentMinorRevisionDelete, false));
            #endregion

            #region Blocking
            RolePermissionGroupObject blockingGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Blocking, false);
            blockingGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.BlockingGet, false));
            blockingGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.BlockingCreate, false));
            blockingGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.BlockingEdit, false));
            blockingGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.BlockingDelete, false));
            #endregion

            #region ConsumerDecisionTree
            RolePermissionGroupObject consumerDecisionTreeGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_ConsumerDecisionTree, false);
            consumerDecisionTreeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ConsumerDecisionTreeGet, false));
            consumerDecisionTreeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ConsumerDecisionTreeCreate, false));
            consumerDecisionTreeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ConsumerDecisionTreeEdit, false));
            consumerDecisionTreeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ConsumerDecisionTreeDelete, false));
            #endregion

            #region Content Lookup
            RolePermissionGroupObject contentLookupGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_ContentLookup, false);
            contentLookupGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ContentLookupGet, false));
            contentLookupGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ContentLookupCreate, false));
            contentLookupGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ContentLookupEdit, false));
            contentLookupGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ContentLookupDelete, false));
            #endregion

            #region Inventory Profile
            RolePermissionGroupObject inventoryProfileGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_InventoryProfiles, false);
            inventoryProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.InventoryProfileGet, false));
            inventoryProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.InventoryProfileCreate, false));
            inventoryProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.InventoryProfileEdit, false));
            inventoryProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.InventoryProfileDelete, false));
            #endregion

            #region Location Cluster
            RolePermissionGroupObject clusterGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_LocationClusters, false);
            clusterGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ClusterSchemeGet, false));
            clusterGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ClusterSchemeCreate, false));
            clusterGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ClusterSchemeEdit, false));
            clusterGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ClusterSchemeDelete, false));
            #endregion

            #region MetricProfile
            RolePermissionGroupObject metricProfileGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_MetricProfile, false);
            metricProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricProfileGet, false));
            metricProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricProfileCreate, false));
            metricProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricProfileEdit, false));
            metricProfileGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricProfileDelete, false));
            #endregion

            #region PerformanceSelection
            RolePermissionGroupObject performanceSelectionGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_PerformanceSelectionMaintenance, false);
            performanceSelectionGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PerformanceSelectionGet, false));
            performanceSelectionGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PerformanceSelectionCreate, false));
            performanceSelectionGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PerformanceSelectionEdit, false));
            performanceSelectionGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PerformanceSelectionDelete, false));
            #endregion

            #region ProductUniverse
            RolePermissionGroupObject productUniverseGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_ProductUniverse, false);
            productUniverseGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductUniverseGet, false));
            productUniverseGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductUniverseCreate, false));
            productUniverseGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductUniverseEdit, false));
            productUniverseGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductUniverseDelete, false));
            #endregion

            #region Renumbering Strategy
            RolePermissionGroupObject renumberingStrategyGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_RenumberingStrategy, false);
            renumberingStrategyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RenumberingStrategyGet, false));
            renumberingStrategyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RenumberingStrategyCreate, false));
            renumberingStrategyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RenumberingStrategyEdit, false));
            renumberingStrategyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RenumberingStrategyDelete, false));
            #endregion

            #region Sequence
            RolePermissionGroupObject sequenceGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Sequence, false);
            sequenceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.SequenceGet, false));
            sequenceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.SequenceCreate, false));
            sequenceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.SequenceEdit, false));
            sequenceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.SequenceDelete, false));
            #endregion

            contentSetupGroupObject.ChildObjects.Add(assortmentGroupObject);
            contentSetupGroupObject.ChildObjects.Add(assortmentMinorGroupObject);
            contentSetupGroupObject.ChildObjects.Add(blockingGroupObject);
            contentSetupGroupObject.ChildObjects.Add(consumerDecisionTreeGroupObject);
            contentSetupGroupObject.ChildObjects.Add(contentLookupGroupObject);
            contentSetupGroupObject.ChildObjects.Add(inventoryProfileGroupObject);
            contentSetupGroupObject.ChildObjects.Add(clusterGroupObject);
            contentSetupGroupObject.ChildObjects.Add(metricProfileGroupObject);
            contentSetupGroupObject.ChildObjects.Add(performanceSelectionGroupObject);
            contentSetupGroupObject.ChildObjects.Add(productUniverseGroupObject);
            contentSetupGroupObject.ChildObjects.Add(renumberingStrategyGroupObject);
            contentSetupGroupObject.ChildObjects.Add(sequenceGroupObject);
            #endregion

            #region Data Management

            #region Delete
            RolePermissionGroupObject deleteDataGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_DeleteData, false);
            deleteDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.DeleteData, false));
            #endregion

            #region Export
            RolePermissionGroupObject exportDataGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_ExportData, false);
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportAssortmentData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportLocationData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportLocationHierarchyData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportLocationProductAttributeData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportLocationProductIllegalData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportLocationSpaceData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportMerchandisingHierarchyData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportProductData, false));
            //exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportProductUniverseData, false));
            exportDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ExportLocationClusterData, false));
            #endregion

            #region Import
            RolePermissionGroupObject importDataGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_ImportData, false);
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportAssortmentData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportLocationData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportLocationHierarchyData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportLocationProductAttributeData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportLocationProductIllegalData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportLocationSpaceData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportMerchandisingHierarchyData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportProductData, false));
            //importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportProductUniverseData, false));
            importDataGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ImportLocationClusterData, false));
            #endregion

            dataManagementGroupObject.ChildObjects.Add(deleteDataGroupObject);
            dataManagementGroupObject.ChildObjects.Add(exportDataGroupObject);
            dataManagementGroupObject.ChildObjects.Add(importDataGroupObject);
            #endregion

            #region Home

            #region LocationPlanAssignment
            RolePermissionGroupObject locationPlanAssignmentGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_LocationPlanAssignment, false);
            //locationPlanAssignmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationPlanAssignmentGet, false));
            locationPlanAssignmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationPlanAssignmentCreate, false));
            locationPlanAssignmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationPlanAssignmentEdit, false));
            locationPlanAssignmentGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationPlanAssignmentDelete, false));
            #endregion

            #region Workpackage
            RolePermissionGroupObject workpackageGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Workpackage, false);
            //workpackageGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkpackageGet, false));
            workpackageGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkpackageCreate, false));
            workpackageGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkpackageEdit, false));
            workpackageGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkpackageDelete, false));
            workpackageGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkpackageExecute, false));
            #endregion

            homeGroupObject.ChildObjects.Add(locationPlanAssignmentGroupObject);
            homeGroupObject.ChildObjects.Add(workpackageGroupObject);
            #endregion

            #region Master Data Management

            #region Location
            RolePermissionGroupObject locationGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Location, false);
            locationGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationGet, false));
            locationGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationCreate, false));
            locationGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationEdit, false));
            locationGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationDelete, false));
            #endregion

            #region Location Hierarchy
            RolePermissionGroupObject locationHierarchyGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_LocationHierarchy, false);
            //locationHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationHierarchyGet, false));
            //locationHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationHierarchyCreate, false));
            locationHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationHierarchyEdit, false));
            //locationHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationHierarchyDelete, false));
            #endregion

            #region LocationSpace
            RolePermissionGroupObject locationSpaceGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_LocationSpace, false);
            locationSpaceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationSpaceGet, false));
            locationSpaceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationSpaceCreate, false));
            locationSpaceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationSpaceEdit, false));
            locationSpaceGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationSpaceDelete, false));
            #endregion

            #region Location Product Attribute
            RolePermissionGroupObject locationProductAttributeGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_LocationProduct, false);
            locationProductAttributeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductAttributeGet, false));
            locationProductAttributeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductAttributeCreate, false));
            locationProductAttributeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductAttributeEdit, false));
            locationProductAttributeGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductAttributeDelete, false));
            #endregion

            #region Location Product Illegal
            RolePermissionGroupObject locationProductIllegalGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_LocationProductIllegal, false);
            locationProductIllegalGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductIllegalGet, false));
            locationProductIllegalGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductIllegalCreate, false));
            locationProductIllegalGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductIllegalEdit, false));
            locationProductIllegalGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LocationProductIllegalDelete, false));
            #endregion

            #region Metric
            RolePermissionGroupObject metricGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_GlobalMetrics, false);
            metricGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricGet, false));
            metricGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricCreate, false));
            metricGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricEdit, false));
            metricGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.MetricDelete, false));
            #endregion

            #region Product
            RolePermissionGroupObject productGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_ProductMaintenance, false);
            productGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductGet, false));
            productGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductCreate, false));
            productGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductEdit, false));
            productGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductDelete, false));
            #endregion

            #region Product Hierarchy
            RolePermissionGroupObject productHierarchyGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_MerchandisingHierarchy, false);
            //productHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductHierarchyGet, false));
            //productHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductHierarchyCreate, false));
            productHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductHierarchyEdit, false));
            //productHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ProductHierarchyDelete, false));
            #endregion

            masterDataSetupGroupObject.ChildObjects.Add(locationGroupObject);
            masterDataSetupGroupObject.ChildObjects.Add(locationHierarchyGroupObject);
            masterDataSetupGroupObject.ChildObjects.Add(locationSpaceGroupObject);
            masterDataSetupGroupObject.ChildObjects.Add(locationProductAttributeGroupObject);
            masterDataSetupGroupObject.ChildObjects.Add(locationProductIllegalGroupObject);
            masterDataSetupGroupObject.ChildObjects.Add(metricGroupObject);
            masterDataSetupGroupObject.ChildObjects.Add(productGroupObject);
            masterDataSetupGroupObject.ChildObjects.Add(productHierarchyGroupObject);
            #endregion

            #region System Administration

            #region Entity
            RolePermissionGroupObject entityGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Entity, false);
            //entityGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EntityGet, false));
            entityGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EntityCreate, false));
            entityGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EntityEdit, false));
            entityGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EntityDelete, false));
            #endregion

            #region EventLog
            RolePermissionGroupObject eventLogGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_EventLog, false);
            eventLogGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EventLogGet, false));
            //eventLogGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EventLogCreate, false));
            //eventLogGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EventLogEdit, false));
            eventLogGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.EventLogDelete, false));
            #endregion

            #region Role
            RolePermissionGroupObject roleGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_RolePermissions, false);
            roleGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RoleGet, false));
            roleGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RoleCreate, false));
            roleGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RoleEdit, false));
            roleGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.RoleDelete, false));
            #endregion

            #region User
            RolePermissionGroupObject userGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_User, false);
            userGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.UserGet, false));
            userGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.UserCreate, false));
            userGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.UserEdit, false));
            userGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.UserDelete, false));
            #endregion

            systemAdministrationGroupObject.ChildObjects.Add(entityGroupObject);
            systemAdministrationGroupObject.ChildObjects.Add(eventLogGroupObject);
            systemAdministrationGroupObject.ChildObjects.Add(roleGroupObject);
            systemAdministrationGroupObject.ChildObjects.Add(userGroupObject);
            #endregion

            #region System Setup

            #region Label
            RolePermissionGroupObject labelGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Label, false);
            labelGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LabelGet, false));
            labelGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LabelCreate, false));
            labelGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LabelEdit, false));
            labelGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.LabelDelete, false));
            #endregion

            #region Highlight
            RolePermissionGroupObject highlightGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Highlight, false);
            highlightGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.HighlightGet, false));
            highlightGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.HighlightCreate, false));
            highlightGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.HighlightEdit, false));
            highlightGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.HighlightDelete, false));
            #endregion

            #region Planogram Hierarchy

            RolePermissionGroupObject planogramHierarchyGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_PlanogramHierarchy, false);
            
            //planogramHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramHierarchyGet, false));
            //planogramHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramHierarchyCreate, false));
            planogramHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramHierarchyEdit, false));
            planogramHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramHierarchyGroupDelete, false));
            planogramHierarchyGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.SyncWithMerchandisingHierarchy, false));

            #endregion

            #region Workflow
            RolePermissionGroupObject workflowGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Workflow, false);
            workflowGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkflowGet, false));
            workflowGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkflowCreate, false));
            workflowGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkflowEdit, false));
            workflowGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.WorkflowDelete, false));
            #endregion

            #region Validation Template
            RolePermissionGroupObject validationTemplateGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_ValidationTemplate, false);
            validationTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ValidationTemplateGet, false));
            validationTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ValidationTemplateCreate, false));
            validationTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ValidationTemplateEdit, false));
            validationTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.ValidationTemplateDelete, false));
            #endregion

            #region PlanogramImportTemplate
            RolePermissionGroupObject planogramImportTemplateGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_PlanogramImportTemplate, false);
            planogramImportTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramImportTemplateGet, false));
            planogramImportTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramImportTemplateCreate, false));
            planogramImportTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramImportTemplateEdit, false));
            planogramImportTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramImportTemplateDelete, false));
            #endregion

            #region PlanogramNameTemplate
            RolePermissionGroupObject planogramNameTemplateGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_PlanogramNameTemplate, false);
            planogramNameTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramNameTemplateGet, false));
            planogramNameTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramNameTemplateCreate, false));
            planogramNameTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramNameTemplateEdit, false));
            planogramNameTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramNameTemplateDelete, false));
            #endregion

            #region PrintingTemplate
            RolePermissionGroupObject printingTemplateGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_PrintingTemplate, false);
            printingTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PrintTemplateGet, false));
            printingTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PrintTemplateCreate, false));
            printingTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PrintTemplateEdit, false));
            printingTemplateGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PrintTemplateDelete, false));
            #endregion

            systemSetupGroupObject.ChildObjects.Add(labelGroupObject);
            systemSetupGroupObject.ChildObjects.Add(highlightGroupObject);
            systemSetupGroupObject.ChildObjects.Add(planogramHierarchyGroupObject);
            systemSetupGroupObject.ChildObjects.Add(workflowGroupObject);
            systemSetupGroupObject.ChildObjects.Add(validationTemplateGroupObject);
            systemSetupGroupObject.ChildObjects.Add(planogramImportTemplateGroupObject);
            systemSetupGroupObject.ChildObjects.Add(planogramNameTemplateGroupObject);
            systemSetupGroupObject.ChildObjects.Add(printingTemplateGroupObject);
            #endregion

            #region Planogram (Root)

            #region Planogram
            RolePermissionGroupObject planogramGroupObject = new RolePermissionGroupObject(null, Message.RolePermissions_Group_Planogram, false);
            planogramGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramGet, false));
            planogramGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramCreate, false));
            planogramGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramEdit, false));
            planogramGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.PlanogramDelete, false));
            planogramGroupObject.ChildObjects.Add(new RolePermissionGroupObject(DomainPermission.UnlockPlanogram, false));
            #endregion

            planogramRootGroupObject.ChildObjects.Add(planogramGroupObject);

            #endregion

            // add Areas
            rootGroupObject.ChildObjects.Add(contentSetupGroupObject);
            rootGroupObject.ChildObjects.Add(dataManagementGroupObject);
            rootGroupObject.ChildObjects.Add(homeGroupObject);
            rootGroupObject.ChildObjects.Add(masterDataSetupGroupObject);
            rootGroupObject.ChildObjects.Add(systemAdministrationGroupObject);
            rootGroupObject.ChildObjects.Add(systemSetupGroupObject);
            rootGroupObject.ChildObjects.Add(planogramRootGroupObject);
 
            // add root group
            _availablePermissions.Add(rootGroupObject);

            IEnumerable<RolePermissionGroupObject> groups = rootGroupObject.GetAllChildGroups();
            foreach (RolePermissionGroupObject rolePermissionGroup in groups)
            {
                rolePermissionGroup.PropertyChanged += new PropertyChangedEventHandler(RolePermissionGroupObject_PropertyChanged);
            }
        }

        /// <summary>
        /// Update any permissions with existing permissions
        /// </summary>
        /// <param name="permissions"></param>
        public void UpdatePermissions(RolePermissionList rolePermissions)
        {
            foreach (RolePermissionGroupObject permissionGroup in this.AvailablePermissions)
            {
                permissionGroup.PropertyChanged -= RolePermissionGroupObject_PropertyChanged;
                permissionGroup.ResetChecked();
                permissionGroup.PropertyChanged += new PropertyChangedEventHandler(RolePermissionGroupObject_PropertyChanged);

                ////Reset all first
                foreach (RolePermissionGroupObject rolePermissionGroup in permissionGroup.GetAllChildGroups())
                {
                    rolePermissionGroup.PropertyChanged -= RolePermissionGroupObject_PropertyChanged;
                    rolePermissionGroup.ResetChecked();
                }

                //Get all lowest level permission groups
                IEnumerable<RolePermissionGroupObject> rolePermissionGroups = permissionGroup.GetAllChildGroups().Where(p => p.SourcePermission != null);

                //For each permission
                if (rolePermissions != null)
                {
                    foreach (RolePermission rolePermission in rolePermissions.ToList())
                    {
                        //Lookup domain permission
                        DomainPermission domainPermission = (DomainPermission)rolePermission.PermissionType;

                        //Find matching
                        RolePermissionGroupObject matchingPermission = rolePermissionGroups.FirstOrDefault(p => (DomainPermission)p.SourcePermission == domainPermission);

                        if (matchingPermission != null)
                        {
                            //Set value
                            if (matchingPermission.IsChecked != true)
                            {
                                matchingPermission.IsChecked = true;
                            }
                        }
                    }
                }

                //Resubscribe to property changed
                foreach (RolePermissionGroupObject rolePermissionGroup in rolePermissionGroups)
                {
                    rolePermissionGroup.PropertyChanged += new PropertyChangedEventHandler(RolePermissionGroupObject_PropertyChanged);
                }
            }
        }

        #endregion

        #region Screen Permissions

        /// <summary>
        /// Returns true if the current user has sufficient permissions to access this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            //Role fetch
            return new ModelPermission<Role>().CanFetch;
        }

        #endregion

        #region IDisposable members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                    _availableRoles.Clear();
                    //Get all groups
                    foreach (RolePermissionGroupObject permissionGroup in this.AvailablePermissions)
                    {
                        permissionGroup.PropertyChanged -= RolePermissionGroupObject_PropertyChanged;
                        permissionGroup.Dispose();

                        IEnumerable<RolePermissionGroupObject> rolePermissionGroups = permissionGroup.GetAllChildGroups();
                        //Unsubscribe to property changed
                        foreach (RolePermissionGroupObject rolePermissionGroup in rolePermissionGroups)
                        {
                            rolePermissionGroup.PropertyChanged -= RolePermissionGroupObject_PropertyChanged;
                            rolePermissionGroup.Dispose();
                        }
                    }
                    _availablePermissions.Clear();
                    _availableUsers.Clear();
                    _availableEntities.Clear();
                    _takenUsers.Clear();
                    _selectedAvailableUsers.Clear();

                    OnSelectedRoleChanged(_selectedRole, null);

                    _masterRoleInfoListView.ModelChanged -= MasterRoleInfoListViewModel_Changed;
                    _masterRoleMemberListView.ModelChanged -= MasterRoleMemberListView_ModelChanged;
                    _masterUserInfoListView.ModelChanged -= MasterUserInfoListView_ModelChanged;
                    _selectedEntities.BulkCollectionChanged -= SelectedEntities_BulkCollectionChanged;


                    _selectedRole = null;
                    _masterRoleMemberListView = null;
                    _selectedEntities.Clear();
                    _masterRoleInfoListView.Dispose();
                    _masterUserInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
