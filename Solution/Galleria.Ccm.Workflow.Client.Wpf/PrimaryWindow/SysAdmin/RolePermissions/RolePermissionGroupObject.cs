﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Csla.Threading;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.ComponentModel;
using System.Collections.Generic;
using Galleria.Framework.Collections;
using Galleria.Ccm.Security;
using System.Collections.Specialized;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.RolePermissions
{
    /// <summary>
    /// Group Object for each Role Permission
    /// </summary>
    public sealed class RolePermissionGroupObject : INotifyPropertyChanged
    {
        #region Fields

        private DomainPermission? _sourcePermission;
        private String _friendlyGroupName;
        private BulkObservableCollection<RolePermissionGroupObject> _childObjects = new BulkObservableCollection<RolePermissionGroupObject>();
        private Boolean? _isChecked = false;

        #endregion

        #region PropertyPaths

        public static readonly PropertyPath IsCheckedProperty = WpfHelper.GetPropertyPath<RolePermissionGroupObject>(p => p.IsChecked);
        public static readonly PropertyPath ChildObjectsProperty = WpfHelper.GetPropertyPath<RolePermissionGroupObject>(p => p.ChildObjects);
        public static readonly PropertyPath FriendlyGroupNameProperty = WpfHelper.GetPropertyPath<RolePermissionGroupObject>(p => p.FriendlyGroupName);
        public static readonly PropertyPath HasChildrenProperty = WpfHelper.GetPropertyPath<RolePermissionGroupObject>(p => p.HasChildren);
        public static readonly PropertyPath SourcePermissionProperty = WpfHelper.GetPropertyPath<RolePermissionGroupObject>(p => p.SourcePermission);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the source permission
        /// </summary>
        public DomainPermission? SourcePermission
        {
            get { return _sourcePermission; }
        }

        /// <summary>
        /// Returns the group display friendly name
        /// </summary>
        public String FriendlyGroupName
        {
            get
            {
                if (_sourcePermission != null)
                {
                    return DomainPermissionHelper.FriendlyNames[(DomainPermission)_sourcePermission];
                }
                return _friendlyGroupName;
            }
        }

        /// <summary>
        /// Returns the child objects for this group
        /// </summary>
        public BulkObservableCollection<RolePermissionGroupObject> ChildObjects
        {
            get { return _childObjects; }
        }

        /// <summary>
        /// Gets/Sets whether the permission is selected
        /// </summary>
        public Boolean? IsChecked
        {
            get
            {
                if (HasChildren)
                {
                    if (this.ChildObjects.All(p => p.IsChecked == true))
                    {
                        return true;
                    }
                    else if (this.ChildObjects.All(p => p.IsChecked == false))
                    {
                        return false;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return _isChecked;
                }
            }
            set
            {
                Boolean? oldValue = _isChecked;
                _isChecked = value;
                OnIsCheckedChanged(oldValue, value);
                OnPropertyChanged(IsCheckedProperty);
            }
        }

        /// <summary>
        /// Returns whether the item has children
        /// </summary>
        public Boolean HasChildren
        {
            get { return this.ChildObjects.Count > 0; }
        }

        #endregion

        #region Constructor

        public RolePermissionGroupObject(DomainPermission? permission, Boolean isChecked) :
            this(permission, String.Empty, isChecked)
        { }

        public RolePermissionGroupObject(DomainPermission? permission, String friendlyName, Boolean isChecked)
        {
            _sourcePermission = permission;
            _friendlyGroupName = friendlyName;
            _isChecked = isChecked;

            //Subscribe to the collection changed event
            _childObjects.BulkCollectionChanged += new EventHandler<Framework.Model.BulkCollectionChangedEventArgs>(ChildObjects_BulkCollectionChanged);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Helper method to reset object isChecked without triggering anything
        /// </summary>
        public void ResetChecked()
        {
            this._isChecked = false;
            OnPropertyChanged(IsCheckedProperty);
        }

        /// <summary>
        /// Helper method to clear event handlers, no need for full fat view model object
        /// </summary>
        public void Dispose()
        {
            this.PropertyChanged -= ChildObject_PropertyChanged;
            this.ChildObjects.BulkCollectionChanged -= ChildObjects_BulkCollectionChanged;
        }

        /// <summary>
        /// Method to handle the isChecked property changing
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnIsCheckedChanged(Boolean? oldValue, Boolean? newValue)
        {
            //If its changed
            if (this.ChildObjects.Count > 0 && this.SourcePermission == null)
            {
                if (newValue == true)
                {
                    //Set all child down to lowest level
                    List<RolePermissionGroupObject> childObjects = this.ChildObjects.ToList();

                    //Set all to true
                    childObjects.ForEach(p => p.IsChecked = true);
                }
                else if (newValue == false)
                {
                    //Set all child down to lowest level
                    List<RolePermissionGroupObject> childObjects = this.ChildObjects.ToList();

                    //Set all to true
                    childObjects.ForEach(p => p.IsChecked = false);
                }
            }
        }


        /// <summary>
        /// Returns a list  of this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RolePermissionGroupObject> GetAllChildGroups()
        {
            IEnumerable<RolePermissionGroupObject> returnList =
                new List<RolePermissionGroupObject>() { this };

            foreach (RolePermissionGroupObject child in this.ChildObjects)
            {
                returnList = returnList.Union(child.GetAllChildGroups());
            }

            return returnList;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Collection changed event for the child objects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChildObjects_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (RolePermissionGroupObject obj in e.ChangedItems)
                {
                    obj.PropertyChanged += new PropertyChangedEventHandler(ChildObject_PropertyChanged);
                }
            }
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (RolePermissionGroupObject obj in e.ChangedItems)
                {
                    obj.PropertyChanged -= ChildObject_PropertyChanged;
                }
            }

            this.OnPropertyChanged(IsCheckedProperty);
        }

        /// <summary>
        /// Property changed event for the child objects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChildObject_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == RolePermissionGroupObject.IsCheckedProperty.Path)
            {
                this.OnPropertyChanged(IsCheckedProperty);
            }
        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
