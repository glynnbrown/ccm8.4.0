﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26234 : L.Ineson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.RolePermissions
{
    /// <summary>
    /// Interaction logic for RolePermissionsWindow.xaml
    /// </summary>
    public partial class RolePermissionsWindow : ExtendedRibbonWindow
    {
        #region Constants

        const String removeUserCommandKey = "RemoveUserCommand";

        #endregion

        #region Fields

        private String _previousText = String.Empty;

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(RolePermissionsViewModel), typeof(RolePermissionsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public RolePermissionsViewModel ViewModel
        {
            get { return (RolePermissionsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            RolePermissionsWindow senderControl = (RolePermissionsWindow)obj;

            if (e.OldValue != null)
            {
                RolePermissionsViewModel oldModel = (RolePermissionsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(removeUserCommandKey);
            }

            if (e.NewValue != null)
            {
                RolePermissionsViewModel newModel = (RolePermissionsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(removeUserCommandKey, newModel.RemoveUserCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public RolePermissionsWindow(RolePermissionsViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.RolePermissions);

            this.ViewModel = viewModel;

            this.Loaded += RolePermissionsWindow_Loaded;

            this.KeyDown += new KeyEventHandler(RolePermissionsWindow_KeyDown);
        }

        /// <summary>
        /// Event handler for the key down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RolePermissionsWindow_KeyDown(object sender, KeyEventArgs e)
        {
            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                this.SetRibbonBackstageState(true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        private void RolePermissionsWindow_Loaded(Object sender, RoutedEventArgs e)
        {
            this.Loaded -= RolePermissionsWindow_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Mark the Role as dirty if the user checks/unchecks a permission
        /// </summary>
        private void CheckBox_CheckChanged(object sender, RoutedEventArgs e)
        {
            CheckBox senderControl = (CheckBox)sender;

            Int16 permissionId;
            if (!Int16.TryParse(senderControl.Uid, out permissionId))
            {
                permissionId = -1;
            }

            if (this.ViewModel != null)
            {
                if (this.ViewModel.SelectedRole.Permissions != null)
                {
                    //Check for this permission in the role
                    RolePermission rolePermission = this.ViewModel.SelectedRole.Permissions.FirstOrDefault(
                                p => p.PermissionType == permissionId);
                    if (rolePermission != null)
                    {
                        //If the box is now null or false remove it from the role
                        if (senderControl.IsChecked != true)
                        {
                            this.ViewModel.SelectedRole.Permissions.Remove(rolePermission);
                        }
                    }
                    else
                    {
                        //If the permission has been added and isn't a header checkbox
                        if (senderControl.IsChecked == true)
                        {
                            if (permissionId > 0)
                            {
                                this.ViewModel.SelectedRole.Permissions.Add(
                                    RolePermission.NewRolePermission(permissionId));
                            }
                        }
                    }
                }
            }
        }

        ///// <summary>
        ///// Responds to user changing the active directory to show the user a continue dialog
        ///// </summary>
        //private void ExtendedTextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    ExtendedTextBox senderControl = sender as ExtendedTextBox;

        //    if (this.ViewModel != null)
        //    {
        //        //If the user declines the changes
        //        if (!this.ViewModel.OnSelectedDirectoryChanged())
        //        {
        //            senderControl.TextChanged -= ExtendedTextBox_TextChanged;
        //            senderControl.Text = _previousText;
        //            senderControl.CaretIndex = _previousText.Length;
        //            senderControl.TextChanged += ExtendedTextBox_TextChanged;

        //            e.Handled = true;
        //        }
        //        else
        //        {
        //            _previousText = senderControl.Text;
        //        }
        //    }
        //}

        /// <summary>
        /// Adds the dropped users to the role
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedUsers_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel.AddUserCommand.CanExecute())
            {
                this.ViewModel.AddUserCommand.Execute();
            }
        }

        /// <summary>
        /// Removes the dropped users from the role
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedUsers_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (this.ViewModel.RemoveUserCommand.CanExecute())
            {
                this.ViewModel.RemoveUserCommand.Execute();
            }
        }

        private void xUnassignedUsers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel.AddUserCommand.CanExecute())
            {
                this.ViewModel.AddUserCommand.Execute();
            }
        }

        private void xAssignedUsers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel.RemoveUserCommand.CanExecute())
            {
                this.ViewModel.RemoveUserCommand.Execute();
            }
        }
        #endregion

        #region Window close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    this.KeyDown -= RolePermissionsWindow_KeyDown;

                    IDisposable dis = this.ViewModel;
                    try { this.ViewModel = null; }
                    catch { } //GFS-18913
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }
            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
