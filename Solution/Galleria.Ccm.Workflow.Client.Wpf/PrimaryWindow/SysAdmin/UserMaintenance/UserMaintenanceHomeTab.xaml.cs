﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.UserMaintenance
{
    /// <summary>
    /// Interaction logic for UserMaintananceHomeTab.xaml
    /// </summary>
    public partial class UserMaintenanceHomeTab : RibbonTabItem
    {

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(UserMaintenanceViewModel), typeof(UserMaintenanceHomeTab));

        public UserMaintenanceViewModel ViewModel
        {
            get { return (UserMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public UserMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
