﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.UserMaintenance
{
    /// <summary>
    /// Interaction logic for UserMaintanaceBackstageOpen.xaml
    /// </summary>
    public partial class UserMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private ObservableCollection<UserInfo> _searchResults = new ObservableCollection<UserInfo>();
        #endregion

        #region Property

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(UserMaintenanceViewModel), typeof(UserMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public UserMaintenanceViewModel ViewModel
        {
            get { return (UserMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UserMaintenanceBackstageOpen senderControl = (UserMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                UserMaintenanceViewModel oldModel = (UserMaintenanceViewModel)e.OldValue;
                oldModel.AvailableUsers.BulkCollectionChanged -= senderControl.ViewModel_AvailableUsersBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                UserMaintenanceViewModel newModel = (UserMaintenanceViewModel)e.NewValue;
                newModel.AvailableUsers.BulkCollectionChanged += senderControl.ViewModel_AvailableUsersBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(UserMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UserMaintenanceBackstageOpen senderControl = (UserMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<UserInfo>),
            typeof(UserMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<UserInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<UserInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion


        #region Constructor

        public UserMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<UserInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        /// 
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<UserInfo> results = this.ViewModel.GetMatchingUsers(this.SearchText);

                foreach (UserInfo locInfo in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(locInfo);
                }
            }

        }

        private void ViewModel_AvailableUsersBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        private void searchResults_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;

            UserInfo storeToOpen = (UserInfo)senderControl.SelectedItem;

            if (storeToOpen != null)
            {
               //send the store to be opened
               this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
            }
        
        }

        #endregion

        #region EventHandler
        private void XActionsGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;

                UserInfo storeToOpen = (UserInfo)senderControl.SelectedItem;

                if (storeToOpen != null)
                {
                    this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                }
            }
        }
        #endregion
    }

}
