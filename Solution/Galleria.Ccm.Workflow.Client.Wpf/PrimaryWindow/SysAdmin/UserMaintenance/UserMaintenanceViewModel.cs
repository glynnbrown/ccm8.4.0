﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendly description to saveAsCommand and Delete Command
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using Csla.Server;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.UserMaintenance
{
    public partial class UserMaintenanceViewModel : ViewModelAttachedControlObject<UserMaintenanceOrganiser>, IDataErrorInfo
    {

        #region Fields

        private ModelPermission<User> _userModelPermissions = new ModelPermission<User>();
        const String _exCategory = "UserMaintenance"; //Category name for any exceptions raised to gibraltar from here

        private readonly UserInfoListViewModel _masterUserInfoListView = new UserInfoListViewModel();
        private readonly RoleInfoListViewModel _masterRoleInfoListView = new RoleInfoListViewModel();
        private readonly RoleMemberListViewModel _masterRoleMemberListView = new RoleMemberListViewModel();

        private User _selectedUser;

        //Roles
        private RoleInfo _selectedRole;
        private RoleInfo _selectedCurrentMemberRole;
        private BulkObservableCollection<RoleInfo> _currentMemberRoles = new BulkObservableCollection<RoleInfo>();
        private ReadOnlyBulkObservableCollection<RoleInfo> _currentMemberRolesRO;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableUsersProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.AvailableUsers);
        public static readonly PropertyPath SelectedUserProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.SelectedUser);
        public static readonly PropertyPath EmailAddressProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.EmailAddress);
        public static readonly PropertyPath AvailableRolesProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.AvailableRoles);
        public static readonly PropertyPath SelectedRoleProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.SelectedRole);
        public static readonly PropertyPath SelectedCurrentMemberRoleProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(c => c.SelectedCurrentMemberRole);
        public static readonly PropertyPath CurrentMemberRolesProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.CurrentMemberRoles);

        //Commands
        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.CloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath AddUserToRoleCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(p => p.AddUserToRoleCommand);
        public static PropertyPath RemoveUserFromRoleCommandProperty = WpfHelper.GetPropertyPath<UserMaintenanceViewModel>(c => c.RemoveUserFromRoleCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of available users 
        /// </summary>
        public ReadOnlyBulkObservableCollection<UserInfo> AvailableUsers
        {
            get { return _masterUserInfoListView.BindingView; }
        }

        /// <summary>
        /// Gets/Sets the selected user
        /// </summary>
        public User SelectedUser
        {
            get { return _selectedUser; }
            private set
            {
                _selectedUser = value;
                OnPropertyChanged(SelectedUserProperty);

                OnSelectedUserChanged(value);
            }
        }

        public String EmailAddress
        {
            get { return _selectedUser.EMailAddress; }
            set
            {
                _selectedUser.EMailAddress = value;
                OnPropertyChanged(EmailAddressProperty);
            }
        }

        /// <summary>
        /// Returns a collection of roles that the user can be added to
        /// </summary>
        public ReadOnlyBulkObservableCollection<RoleInfo> AvailableRoles
        {
            get { return _masterRoleInfoListView.BindingView; }
        }

        /// <summary>
        /// Gets/Sets the selected role
        /// </summary>
        public RoleInfo SelectedRole
        {
            get { return _selectedRole; }
            set
            {
                _selectedRole = value;
                OnPropertyChanged(SelectedRoleProperty);
            }
        }

        /// <summary>
        /// Get/Sets the selected current member role
        /// </summary>
        public RoleInfo SelectedCurrentMemberRole
        {
            get { return _selectedCurrentMemberRole; }
            set
            {
                _selectedCurrentMemberRole = value;
                OnPropertyChanged(SelectedCurrentMemberRoleProperty);
            }
        }

        /// <summary>
        /// Gets the Roles that the user is currently a member of
        /// </summary>
        public ReadOnlyBulkObservableCollection<RoleInfo> CurrentMemberRoles
        {
            get
            {
                if (_currentMemberRolesRO == null)
                {
                    _currentMemberRolesRO = new ReadOnlyBulkObservableCollection<RoleInfo>(_currentMemberRoles);
                }
                return _currentMemberRolesRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UserMaintenanceViewModel()
        {
            //infos
            _masterUserInfoListView.FetchAll();
            _masterRoleInfoListView.FetchAll();

            _masterRoleMemberListView.ModelChanged += MasterRoleMemberListView_ModelChanged;

            //Loads a new user
            this.NewCommand.Execute();
        }
        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Loads a new user
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute())
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }

        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userModelPermissions.CanCreate)
            {
                this.NewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //create a new user
                User newUser = User.NewUser();

                //re-initialize as property was changed.
                newUser.MarkGraphAsInitialized();

                this.SelectedUser = newUser;

                //close the backstage
                this.AttachedControl.SetRibbonBackstageState(/*isOpen*/false);
            }
        }


        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current user
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {

            //user must have edit permission
            if (!_userModelPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.SelectedUser == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedUser.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            if (this.AttachedControl != null)
            {
                //bindings must be valid
                if (!LocalHelper.AreBindingsValid(this.AttachedControl))
                {
                    this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    return false;
                }
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current user
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            Int32 locId = this.SelectedUser.Id;

            //** check the username unique value
            String newUserName;

            Boolean UserNameAccepted = true;
            if (this.AttachedControl != null)
            {


                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (UserInfo locInfo in _masterUserInfoListView.Model)
                       {
                           if (locInfo.Id != locId
                               && locInfo.UserName.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };

                UserNameAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(String.Empty, String.Empty,
                    Message.UserMaintenance_UserNameAlreadyInUse, User.UserNameProperty.FriendlyName, 50,
                    /*forceFirstShow*/false, isUniqueCheck, this.SelectedUser.UserName, out newUserName);
            }
            else
            {
                //force a unique value for unit testing
                newUserName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedUser.UserName,
                    this.AvailableUsers.Where(p => p.Id != locId).Select(p => p.UserName));
            }

            //set the username
            if (UserNameAccepted)
            {
                if (this.SelectedUser.UserName != newUserName)
                {
                    this.SelectedUser.UserName = newUserName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.SelectedUser = this.SelectedUser.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedUser.UserName);
                        if (itemReloadRequired)
                        {
                            this.SelectedUser = User.FetchById(this.SelectedUser.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.SelectedUser.UserName, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterUserInfoListView.FetchAll();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }


        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }

        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {

            //may not be null
            if (this.SelectedUser == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have create permission
            if (!_userModelPermissions.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //must be valid
            if (!this.SelectedUser.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the code to save as
            String copyUserName;

            Boolean UserNameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (UserInfo locInfo in
                          _masterUserInfoListView.Model)
                       {
                           if (locInfo.UserName.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };

                UserNameAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(Message.Generic_SaveAs, Message.UserMaintenance_SaveAs_UserNameDescription,
                    Message.UserMaintenance_UserNameIsNotUnique, User.UserNameProperty.FriendlyName, 50,
                    /*forceFirstShow*/true, isUniqueCheck, String.Empty, out copyUserName);
            }
            else
            {
                //force a unique value for unit testing
                copyUserName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedUser.UserName, this.AvailableUsers.Select(p => p.UserName));
            }

            if (UserNameAccepted)
            {
                base.ShowWaitCursor(true);

                User itemCopy = this.SelectedUser.Copy();
                itemCopy.UserName = copyUserName;
                this.SelectedUser = itemCopy;

                base.ShowWaitCursor(false);

                SaveCurrentItem();
            }

        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the passed user
        /// Parameter = user to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //must not be null
            if (this.SelectedUser == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have delete permission
            if (!_userModelPermissions.CanDelete)
            {
                this.DeleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be new
            if (this.SelectedUser.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedUser.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                User userToDelete = this.SelectedUser;
                userToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    userToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(userToDelete.UserName, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //update the available items
                _masterUserInfoListView.FetchAll();

                base.ShowWaitCursor(false);
            }
        }


        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(),
                        p => Close_CanExecute(),
                        attachToCommandManager: false)
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }


        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean userSaved = SaveCurrentItem();

            if (userSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean userSaved = SaveCurrentItem();

            //if save completed
            if (userSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Loads the requested user
        /// parameter = the username of the user to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? locId)
        {
            //user must have fetch permission
            if (!_userModelPermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (locId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }
            return true;
        }

        private void Open_Executed(Int32? locId)
        {
            //check if current user requires save first
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the requested user
                    this.SelectedUser = User.FetchById(locId.Value);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }


                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #region AddUserToRoleCommand

        private RelayCommand _addUserToRoleCommand;

        /// <summary>
        /// Adds the user to the given role
        /// param: role
        /// </summary>
        public RelayCommand AddUserToRoleCommand
        {
            get
            {
                if (_addUserToRoleCommand == null)
                {
                    _addUserToRoleCommand = new RelayCommand(
                        p => AddUserToRole_Executed(),
                        p => AddUserToRole_CanExecute())
                    {
                        FriendlyName = Message.UserMaintenance_AddUserToRole,
                        Icon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addUserToRoleCommand);
                }
                return _addUserToRoleCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddUserToRole_CanExecute()
        {
            //may not be a null role
            if (this.SelectedRole == null)
            {
                this.AddUserToRoleCommand.DisabledReason = Message.UserMaintenance_AddToRole_DisabledNoSelectedRole;
                return false;
            }

            //Role should not contain this already
            if (_currentMemberRoles.Contains(_selectedRole))
            {
                this.AddUserToRoleCommand.DisabledReason = Message.UserMaintenance_AddToRole_DisabledRoleAlreadyAdded;
                return false;
            }

            return true;
        }

        private void AddUserToRole_Executed()
        {
            //Add the role to the current roles
            _currentMemberRoles.Add(this.SelectedRole);
            OnPropertyChanged(CurrentMemberRolesProperty);

            //If the roleMember isn't already in the user model add it
            if (!_masterRoleMemberListView.Model.ToList().Exists(
                p => p.UserId == this.SelectedUser.Id && p.RoleId == this.SelectedRole.Id))
            {
                this.SelectedUser.RoleMembers.Add(
                    RoleMember.NewRoleMember(this.SelectedUser.Id, this.SelectedRole.Id));
            }
        }

        #endregion

        #region RemoveUserFromRoleCommand

        /// <summary>
        /// Removes user from selected current member role
        /// </summary>
        private RelayCommand _removeUserFromRoleCommand;

        public RelayCommand RemoveUserFromRoleCommand
        {
            get
            {
                if (_removeUserFromRoleCommand == null)
                {
                    _removeUserFromRoleCommand = new RelayCommand(p => RemoveUserFromRole_Executed(), p => RemoveUserFromRole_CanExecute())
                    {
                        FriendlyName = String.Empty,
                        Icon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeUserFromRoleCommand);
                }
                return _removeUserFromRoleCommand;
            }
        }

        private Boolean RemoveUserFromRole_CanExecute()
        {
            //current member role must be selected
            if (SelectedCurrentMemberRole == null)
            {
                this.RemoveUserFromRoleCommand.DisabledReason = Message.UserMaintenance_RemoveFromRole_DisabledReason;
                return false;
            }
            return true;
        }

        private void RemoveUserFromRole_Executed()
        {
            //If the roleMember isn't already in the user model add it
            Int32 selectedUserId = this.SelectedUser.Id;
            Int32 selectedRoleId = this.SelectedCurrentMemberRole.Id;

            if (_masterRoleMemberListView.Model.Any(p => p.UserId == selectedUserId && p.RoleId == selectedRoleId))
            {
                RoleMember roleToRemove = this.SelectedUser.RoleMembers.FirstOrDefault(p => p.UserId == selectedUserId && p.RoleId == selectedRoleId);
                this.SelectedUser.RoleMembers.Remove(roleToRemove);

            }

            //Remove the role from the current roles
            _currentMemberRoles.Remove(SelectedCurrentMemberRole);
            OnPropertyChanged(CurrentMemberRolesProperty);
        }

        #endregion

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected user.
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        private void OnSelectedUserChanged(User newModel)
        {
            if (newModel != null)
            {
                //Get the Role this user is a member of
                _masterRoleMemberListView.Model = newModel.RoleMembers;
            }
            else
            {
                _masterRoleMemberListView.Model = null;
            }
        }

        /// <summary>
        /// Responds to changes to the master role member list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MasterRoleMemberListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<RoleMemberList> e)
        {
            _currentMemberRoles.Clear();
            if (e.NewModel != null)
            {
                foreach (RoleMember member in e.NewModel)
                {
                    Int32 roleId = member.RoleId;
                    _currentMemberRoles.Add(_masterRoleInfoListView.Model.FirstOrDefault(p => p.Id == roleId));
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedUser, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Returns a list of locs that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<UserInfo> GetMatchingUsers(String UserNameCriteria)
        {
            String invariantCriteria = UserNameCriteria.ToLowerInvariant();

            return this.AvailableUsers.Where(
                p => p.UserName.ToLowerInvariant().Contains(invariantCriteria) 
                || p.FirstName.ToLowerInvariant().Contains(invariantCriteria)
                || p.LastName.ToLowerInvariant().Contains(invariantCriteria));
        }

        #endregion

        #region IDataErrorInfo

        /// <summary>
        /// Check that the Email Address is valid or Not?
        /// </summary>
        /// <param name="emailAddress">Validate the email address string inputted by the user</param>
        /// <returns>True/False Dependant on the email address</returns>
        public bool IsEmailValid(String emailAddress)
        {
            bool isValid = true;

            if (emailAddress != null && emailAddress != "")
            {
                // Return true if strIn is in valid e-mail format.
                isValid = Regex.IsMatch(emailAddress,
                           @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                           @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
            }

            return isValid;
        }


        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == EmailAddressProperty.Path)
                {
                    if (!IsEmailValid(SelectedUser.EMailAddress))
                        result = String.Format(CultureInfo.CurrentCulture, Message.LocationMaintenance_EmailValidationError);
                }

                return result;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //save the user type list so that any edits are committed
                    _masterRoleMemberListView.ModelChanged -= MasterRoleMemberListView_ModelChanged;

                    _selectedUser = null;

                    this.AttachedControl = null;

                    _masterUserInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
   
}
