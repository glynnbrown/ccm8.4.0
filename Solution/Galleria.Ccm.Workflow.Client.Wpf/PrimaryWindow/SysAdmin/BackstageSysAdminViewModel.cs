﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25970 : L.Ineson
//  Copied from SA
// CCM-26222 : L.Luong
// Added User Maintenance
// CCM-26255 : L.Ineson
//  Added in placeholders for RolesAndPermissions & GFS setup screens.
// CCM-26911 : L.Luong
//  Added Event Logs
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.EventLogMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.GFSSetup;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.RolePermissions;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.UserMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using FrameworkControls = Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin
{
    /// <summary>
    /// Viewmodel controller for BackstageSysAdminTabContent
    /// </summary>
    public sealed class BackstageSysAdminViewModel : ViewModelAttachedControlObject<BackstageSysAdminTabContent>
    {
        #region Binding Property Paths

        public static readonly PropertyPath ShowUserMaintenanceCommandProperty = WpfHelper.GetPropertyPath<BackstageSysAdminViewModel>(p => p.ShowUserMaintenanceCommand);
        public static readonly PropertyPath ShowEntityMaintenanceCommandProperty = WpfHelper.GetPropertyPath<BackstageSysAdminViewModel>(p => p.ShowEntityMaintenanceCommand);
        public static readonly PropertyPath ShowGFSSetupCommandProperty = WpfHelper.GetPropertyPath<BackstageSysAdminViewModel>(p => p.ShowGFSSetupCommand);
        public static readonly PropertyPath ShowRolesAndPermissionsCommandProperty = WpfHelper.GetPropertyPath<BackstageSysAdminViewModel>(p => p.ShowRolesAndPermissionsCommand);
        public static readonly PropertyPath ShowEventLogCommandProperty = WpfHelper.GetPropertyPath<BackstageSysAdminViewModel>(p => p.ShowEventLogCommand);

        #endregion

        #region Constructor

        public BackstageSysAdminViewModel() { }

        #endregion

        #region Commands

        #region ShowUserMaintenanceCommand

        private RelayCommand _showUserMaintenanceCommand;

        public RelayCommand ShowUserMaintenanceCommand
        {
            get
            {
                if (_showUserMaintenanceCommand == null)
                {
                    _showUserMaintenanceCommand = new RelayCommand(
                        p => ShowUserMaintenance_Executed(),
                        p => ShowUserMaintenance_CanExecute(),
                            attachToCommandManager: false)
                    {
                        FriendlyName = Message.SysAdmin_UserMaintenance,
                        Icon = ImageResources.UserMaintenance_32,
                        DisabledReason = Message.UserMaintenance_DisabledReason
                    };
                    base.ViewModelCommands.Add(_showUserMaintenanceCommand);
                }
                return _showUserMaintenanceCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ShowUserMaintenance_CanExecute()
        {
            return new ModelPermission<User>().CanFetch;
        }

        private void ShowUserMaintenance_Executed()
        {
            if (this.AttachedControl != null)
            {
                Window existingWin = FrameworkControls.Helpers.GetWindow<UserMaintenanceOrganiser>();
                if (existingWin == null)
                {
                    UserMaintenanceOrganiser window = new UserMaintenanceOrganiser();
                    App.ShowWindow(window, false);
                }
                else
                {
                    if (existingWin.WindowState == WindowState.Minimized)
                    {
                        existingWin.WindowState = WindowState.Normal;
                    }
                    existingWin.Activate();
                }
            }
        }

        #endregion

        #region ShowRolesAndPermissionsCommand

        private RelayCommand _showRolesAndPermissionsCommand;

        public RelayCommand ShowRolesAndPermissionsCommand
        {
            get
            {
                if (_showRolesAndPermissionsCommand == null)
                {
                    _showRolesAndPermissionsCommand = new RelayCommand(
                        p => ShowRolesAndPermissions_Executed(),
                        p => ShowRolesAndPermissions_CanExecute(),
                            attachToCommandManager: false)
                    {
                        FriendlyName = Message.SysAdmin_RolesAndPermissions,
                        Icon = ImageResources.RolesAndPermissions_32,
                        DisabledReason = Message.DisabledReason_NotAdmin
                    };
                    base.ViewModelCommands.Add(_showRolesAndPermissionsCommand);
                }
                return _showRolesAndPermissionsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ShowRolesAndPermissions_CanExecute()
        {
            return new ModelPermission<Role>().CanFetch;
        }

        private void ShowRolesAndPermissions_Executed()
        {
            if (this.AttachedControl != null)
            {
                Window existingWin = FrameworkControls.Helpers.GetWindow<UserMaintenanceOrganiser>();
                if (existingWin == null)
                {
                    new RolePermissionsViewModel().ShowDialog();
                }
                else
                {
                    if (existingWin.WindowState == WindowState.Minimized)
                    {
                        existingWin.WindowState = WindowState.Normal;
                    }
                    existingWin.Activate();
                }
            }
        }

        #endregion

        #region ShowEntityMaintenanceCommand

        private RelayCommand _showEntityMaintenanceCommand;

        /// <summary>
        /// Launches the entity maintenance window.
        /// </summary>
        public RelayCommand ShowEntityMaintenanceCommand
        {
            get
            {
                if (_showEntityMaintenanceCommand == null)
                {
                    _showEntityMaintenanceCommand = new RelayCommand(
                        p => ShowEntitySetup_Executed(),
                        p => ShowEntitySetup_CanExecute(),
                        attachToCommandManager: false)
                    {
                        FriendlyName = Message.SysAdmin_EntityMaintenance,
                        Icon = ImageResources.SysAdmin_EntityMaintenance,
                        DisabledReason = Message.SysAdmin_EntityMaintenance_DisabledReason
                    };
                    base.ViewModelCommands.Add(_showEntityMaintenanceCommand);
                }
                return _showEntityMaintenanceCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ShowEntitySetup_CanExecute()
        {
            ModelPermission<Entity> entityPermissions = new ModelPermission<Entity>();
            return
                entityPermissions.CanCreate ||
                entityPermissions.CanEdit ||
                entityPermissions.CanDelete;
        }

        private void ShowEntitySetup_Executed()
        {
            PrimaryWindow.SysAdmin.EntityMaintenance.EntityMaintenanceOrganiser win = new PrimaryWindow.SysAdmin.EntityMaintenance.EntityMaintenanceOrganiser();
            App.ShowWindow(win, true);

            //refetch the current entity just to be safe
            App.ViewState.CurrentEntityView.FetchCurrent();
        }

        #endregion

        #region ShowGFSSetupCommand

        private RelayCommand _showGFSSetupCommand;

        public RelayCommand ShowGFSSetupCommand
        {
            get
            {
                if (_showGFSSetupCommand == null)
                {
                    _showGFSSetupCommand = new RelayCommand(
                        p => ShowGFSSetup_Executed(),
                        p => ShowGFSSetup_CanExecute(),
                            attachToCommandManager: false)
                    {
                        FriendlyName = Message.SysAdmin_GFSSetup,
                        Icon = ImageResources.GFSSetup_32,
                        DisabledReason = Message.DisabledReason_NotAdmin
                    };
                    base.ViewModelCommands.Add(_showGFSSetupCommand);
                }
                return _showGFSSetupCommand;
            }
        }

        private Boolean ShowGFSSetup_CanExecute()
        {
            return new ModelPermission<Entity>().CanEdit;
        }

        private void ShowGFSSetup_Executed()
        {
            new GFSSetupViewModel().ShowDialog();
        }

        #endregion

        #region ShowEventLogCommand

        private RelayCommand _showEventLogCommand;

        public RelayCommand ShowEventLogCommand
        {
            get
            {
                if (_showEventLogCommand == null)
                {
                    _showEventLogCommand = new RelayCommand(
                        p => ShowEventLog_Executed(),
                        p => ShowEventLog_CanExecute(),
                            attachToCommandManager: false)
                    {
                        FriendlyName = Message.SysAdmin_EventLogs,
                        Icon = ImageResources.EventLog_32,
                        DisabledReason = Message.DisabledReason_NotAdmin
                    };
                    base.ViewModelCommands.Add(_showEventLogCommand);
                }
                return _showEventLogCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ShowEventLog_CanExecute()
        {
            return new ModelPermission<Model.EventLog>().CanFetch;
        }

        private void ShowEventLog_Executed()
        {
            ShowEventLogs();
        }

        internal static void ShowEventLogs()
        {
            EventLogWindow win = new EventLogWindow();
            App.ShowWindow(win, /*isModal*/ true);
        }

        #endregion

        #endregion

        #region IDisposable
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
