﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0.0)

// V8-26157 : L.Ineson
//  Copied from SA
// V8-27911 : A.Silva
//      Amended TestConnection_Executed to use the temporary connection values instead of the current ones.

#endregion

#region Version History: (CCM 8.0.1)
// V8-27937 : M.Shelley
//  Added code to reset the GFS connection state in App.ViewState.CurrentEntityView after a change
//  to the GFS connection settings
#endregion

#region Version History: (CCM 8.3.3)

// CCM-19023 : J.Mendes
//  Code added to update the RegisterRealImageProvider everytime the GFS settings changes
// CCM-19088 : J.Mendes
//  Code changed to fix the bug and improve the way we pick the host, port and directory values from the GFS endPointAddress

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Csla.Threading;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.GFSSetup
{
    /// <summary>
    /// Viewmodel controller for GFSSetupWindow
    /// </summary>
    public sealed class GFSSetupViewModel : ViewModelAttachedControlObject<GFSSetupWindow>
    {
        #region Fields
        private EntityViewModel _entityView;
        private String _hostName;
        private String _portNumber;
        private String _directoryName;
        private String _fullConnectionPath;
        private Boolean _isConnectionPathValid;
        private String _connectionTestResult;
        const String _exCategory = "GFSConnectionSetup"; //category name for any exceptions raised to gibraltar from here
        private String _defaultPortNumber = "80"; // set a default port number for the GFS web service
        private String _defaultHostName = String.Empty; // set a default server name for the GFS web service
        private String _defaultDirectoryName = String.Empty; // set a default site name for the GFS web service

        private Boolean _isbusyProgress = false; // show or hide the busy progress
        private Boolean _isGFSConnected = false; // do we have a connection to GFS

        private ReadOnlyCollection<GFSModel.Compression> _availableCompressionsRO; // available compression types from gfs
        private GFSModel.Compression _selectedCompression; // selected compression type from combo box

        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath HostNameProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.HostName);
        public static readonly PropertyPath PortNumberProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.PortNumber);
        public static readonly PropertyPath DirectoryNameProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.DirectoryName);
        public static readonly PropertyPath FullConnectionPathProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.FullConnectionPath);
        public static readonly PropertyPath ConnectionTestResultProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.ConnectionTestResult);
        public static readonly PropertyPath IsBusyProgressProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.IsBusyProgress);
        public static readonly PropertyPath IsGFSConnectedProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.IsGFSConnected);
        public static readonly PropertyPath SelectedCompressionProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.SelectedCompression);
        public static readonly PropertyPath AvailableCompressionsProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.AvailableCompressions);
        public static readonly PropertyPath IsConnectionStringSetupProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.IsConnectionStringSetup);

        //commands
        public static readonly PropertyPath TestConnectionCommandProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.TestConnectionCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath ResetConnectionCommandProperty = WpfHelper.GetPropertyPath<GFSSetupViewModel>(p => p.ResetConnectionCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the host name part of the connection string
        /// </summary>
        public String HostName
        {
            get { return _hostName; }
            set
            {
                _hostName = value;
                OnPropertyChanged(HostNameProperty);

                BuildConnectionPath();
            }
        }

        /// <summary>
        /// Gets/Sets the port number part of the connection string
        /// </summary>
        public String PortNumber
        {
            get { return _portNumber; }
            set
            {
                _portNumber = value;
                OnPropertyChanged(PortNumberProperty);

                BuildConnectionPath();
            }
        }

        /// <summary>
        /// Gets/Sets the directory name part of the connection string
        /// </summary>
        public String DirectoryName
        {
            get { return _directoryName; }
            set
            {
                _directoryName = value;
                OnPropertyChanged(DirectoryNameProperty);

                BuildConnectionPath();
            }
        }

        /// <summary>
        /// Returns the built connection path.
        /// </summary>
        public String FullConnectionPath
        {
            get { return _fullConnectionPath; }
            private set
            {
                _fullConnectionPath = value;
                OnPropertyChanged(FullConnectionPathProperty);
            }
        }

        /// <summary>
        /// Returns the friendly connection test result description.
        /// </summary>
        public String ConnectionTestResult
        {
            get { return _connectionTestResult; }
            private set
            {
                _connectionTestResult = value;
                OnPropertyChanged(ConnectionTestResultProperty);
            }

        }

        /// <summary>
        /// Gets/Sets the visibility of busy progress
        /// </summary>        
        public Boolean IsBusyProgress
        {
            get { return _isbusyProgress; }
            set
            {
                _isbusyProgress = value;
                OnPropertyChanged(IsBusyProgressProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the gfs connection boolean
        /// </summary>        
        public Boolean IsGFSConnected
        {
            get { return _isGFSConnected; }
            set
            {
                _isGFSConnected = value;
                OnPropertyChanged(IsGFSConnectedProperty);
            }
        }

        /// <summary>
        /// Returns the readonly collection of available compressions.
        /// </summary>
        public ReadOnlyCollection<GFSModel.Compression> AvailableCompressions
        {
            get { return _availableCompressionsRO; }
            set
            {
                _availableCompressionsRO = value;
                OnPropertyChanged(AvailableCompressionsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the the selected compression
        /// </summary>
        public GFSModel.Compression SelectedCompression
        {
            get { return _selectedCompression; }
            set
            {
                _selectedCompression = value;
                OnPropertyChanged(SelectedCompressionProperty);
            }
        }

        /// <summary>
        /// Returns whether a connection string is setup
        /// </summary>
        public Boolean IsConnectionStringSetup
        {
            get
            {
                return !(this.HostName.Equals(String.Empty)
                              && (this.PortNumber.Equals(String.Empty) || this.PortNumber.Equals(_defaultPortNumber)));
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public GFSSetupViewModel()
            : this(App.ViewState.CurrentEntityView)
        { }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="configView"></param>
        public GFSSetupViewModel(EntityViewModel entityView)
        {
            //process the config data
            _entityView = entityView;
            _entityView.ModelChanged += EntityView_ModelChanged;

            //refetch to ensure that we are up to date.
            _entityView.FetchCurrent();


            //Establish gfs connection
            if (this.TestConnectionCommand.CanExecute())
            {
                this.TestConnectionCommand.Execute();
            }
        }

        #endregion

        #region Commands

        #region Test Connection

        private RelayCommand _testConnectionCommand;

        /// <summary>
        /// Attempts to connect to the gfs service at the given path
        /// </summary>
        public RelayCommand TestConnectionCommand
        {
            get
            {
                if (_testConnectionCommand == null)
                {
                    _testConnectionCommand = new RelayCommand(
                        p => TestConnection_Executed(),
                        p => TestConnection_CanExecute())
                    {
                        FriendlyName = Message.GFSConnectionSetup_TestConnection
                    };
                    base.ViewModelCommands.Add(_testConnectionCommand);
                }
                return _testConnectionCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean TestConnection_CanExecute()
        {
            //full path muts not be null
            if (String.IsNullOrEmpty(this.FullConnectionPath))
            {
                return false;
            }
            if (String.IsNullOrEmpty(this.HostName))
            {
                return false;
            }
            if (String.IsNullOrEmpty(this.PortNumber))
            {
                return false;
            }

            return true;
        }

        public void TestConnection_Executed()
        {
            base.ShowWaitCursor(true);

            _isConnectionPathValid = false;
            this.ConnectionTestResult = Message.GFSConnectionSetup_TestResult_InProgress;

            //Show progress
            IsBusyProgress = true;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork +=
                (s, e) =>
                {
                    String result;
                    // Set up a test entity to test the new values.
                    Entity testEntity = App.ViewState.CurrentEntityView.Model.Copy();
                    testEntity.SystemSettings.FoundationServicesEndpoint = this.FullConnectionPath;

                    // update the entity viewmodel GFS connection state
                    App.ViewState.CurrentEntityView.IsValidGFSConnection = null;

                    try
                    {
                        GFSModel.EntityList gfsEntities =
                            GFSModel.EntityList.FetchAll(testEntity);

                        if (gfsEntities.Count == 0)
                        {
                            _isConnectionPathValid = false;
                            IsGFSConnected = false;
                            result = Message.GFSConnectionSetup_TestResult_Fail;
                        }
                        else
                        {
                            // load all compression types from gfs
                            AvailableCompressions = GFSModel.CompressionList.FetchAll(testEntity).ToList().AsReadOnly();

                            _isConnectionPathValid = true;
                            IsGFSConnected = true;
                            result = Message.GFSConnectionSetup_TestResult_Sucess;
                        }
                    }
                    catch (Exception ex)
                    {
                        LocalHelper.RecordException(ex, _exCategory);
                        result = /*Message.GFSConnectionSetup_TestResult_Fail*/ ex.Message;
                        IsGFSConnected = false;
                    }
                    e.Result = result;
                };

            worker.RunWorkerCompleted +=
                (s, e) =>
                {
                    if (_entityView == null) return; // There is no entity view available!

                    //set selected compression if still available
                    String compressionName = _entityView.Model.SystemSettings.CompressionName;
                    if (AvailableCompressions != null && AvailableCompressions.Count > 0)
                    {
                        this.SelectedCompression = AvailableCompressions.FirstOrDefault(c => c.Name == compressionName);
                        if (this.SelectedCompression == null)
                        {
                            this.SelectedCompression = AvailableCompressions.FirstOrDefault();
                        }
                    }

                    this.ConnectionTestResult = (String)e.Result;
                    //force can execute changed as command manager will delay here.
                    this.SaveAndCloseCommand.RaiseCanExecuteChanged();
                    base.ShowWaitCursor(false);
                    //Complete busy progress
                    IsBusyProgress = false;
                };

            worker.RunWorkerAsync();
        }


        #endregion

        #region Save And Close

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the connection path change and closes.
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAndClose_CanExecute()
        {
            //the connection path must be valid.
            if (!_isConnectionPathValid && this.IsConnectionStringSetup)
            {
                return false;
            }

            return true;
        }

        private void SaveAndClose_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean saveSuccessful = true;
            try
            {
                if (this.IsConnectionStringSetup)
                {
                    _entityView.Model.SystemSettings.FoundationServicesEndpoint = this.FullConnectionPath;
                    _entityView.Model.SystemSettings.CompressionName = this.SelectedCompression.Name;
                }
                else
                {
                    _entityView.Model.SystemSettings.FoundationServicesEndpoint = String.Empty;
                    _entityView.Model.SystemSettings.CompressionName = String.Empty;
                }
                _entityView.Save();

                // update the entity viewmodel GFS connection state
                App.ViewState.CurrentEntityView.IsValidGFSConnection = null;
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                saveSuccessful = false;

                LocalHelper.RecordException(ex, _exCategory);
                Exception rootException = ex.GetBaseException();

                //just show the user an error has occurred.
                Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(Message.GFSConnectionSetup_SaveErrorName, OperationType.Save);

                base.ShowWaitCursor(true);
            }

            base.ShowWaitCursor(false);

            if (saveSuccessful)
            {
                //update the RegisterRealImageProvider everytime the GFS settings changes
                LocalHelper.UpdateGFSRegisterRealImageProvider();

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region Reset Connection

        private RelayCommand _resetConnectionCommand;

        /// <summary>
        /// Clears the connection details back to defaults
        /// </summary>
        public RelayCommand ResetConnectionCommand
        {
            get
            {
                if (_resetConnectionCommand == null)
                {
                    _resetConnectionCommand = new RelayCommand(
                         p => ResetConnection_Executed(),
                         p => ResetConnection_CanExecute())
                    {
                        FriendlyName = Message.GFSConnectionSetup_ResetConnection
                    };
                    base.ViewModelCommands.Add(_resetConnectionCommand);
                }
                return _resetConnectionCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ResetConnection_CanExecute()
        {
            if (this.IsBusyProgress == true)
            {
                return false;
            }
            if (this.IsGFSConnected == false)
            {
                return false;
            }
            return true;
        }
        private void ResetConnection_Executed()
        {
            base.ShowWaitCursor(true);

            this.HostName = this._defaultHostName;
            this.PortNumber = this._defaultPortNumber;
            this.DirectoryName = this._defaultDirectoryName;
            this.ConnectionTestResult = String.Empty;
            this.FullConnectionPath = String.Empty;

            base.ShowWaitCursor(false);
        }


        #endregion

        #endregion

        #region Event Handlers

        private void EntityView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<Model.Entity> e)
        {
            SplitConnectionAddress();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Splits the config address out into the 
        /// address parts
        /// </summary>
        private void SplitConnectionAddress()
        {
            String endPointAddress = _entityView.Model.SystemSettings.FoundationServicesEndpoint;
            String host = String.Empty;
            String port = String.Empty;
            String directory = String.Empty;


            if (!String.IsNullOrEmpty(endPointAddress))
            {

                Uri path = new Uri(endPointAddress);
                host = path.Host;                 //get the host part
                port = path.Port.ToString();      //get the port part
                directory = path.AbsolutePath;    //get the directory part

                if (directory.StartsWith("/")) {
                    directory = directory.Remove(0, 1);
                }
  
            }
            else
            {
                //set the default
                host = _defaultHostName;
                port = _defaultPortNumber;
                directory = _defaultDirectoryName;
            }

            //update the main properties
            //dont go through set though as we dont want to trigger a path rebuild.
            _hostName = host;
            OnPropertyChanged(HostNameProperty);
            _portNumber = port;
            OnPropertyChanged(PortNumberProperty);
            _directoryName = directory;
            OnPropertyChanged(DirectoryNameProperty);

            this.FullConnectionPath = endPointAddress;
        }

        /// <summary>
        /// Builds the connection path from the parts
        /// </summary>
        private void BuildConnectionPath()
        {
            this.ConnectionTestResult = String.Empty;

            this.FullConnectionPath =
                String.Format(
                "http://{0}:{1}/{2}",
                this.HostName, this.PortNumber, this.DirectoryName);

            //if changes have been made then can't be valid until the connection is tested
            _isConnectionPathValid = false;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (IsBusyProgress)
                    {
                        Mouse.OverrideCursor = null;
                    }

                    _entityView.ModelChanged -= EntityView_ModelChanged;
                    _entityView = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
