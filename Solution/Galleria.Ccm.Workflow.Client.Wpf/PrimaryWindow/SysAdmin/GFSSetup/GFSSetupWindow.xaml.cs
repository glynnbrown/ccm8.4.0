﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26157 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.SysAdmin.GFSSetup
{
    /// <summary>
    /// Interaction logic for GFSSetupWindow.xaml
    /// </summary>
    public partial class GFSSetupWindow : ExtendedRibbonWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(GFSSetupViewModel), typeof(GFSSetupWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public GFSSetupViewModel ViewModel
        {
            get { return (GFSSetupViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            GFSSetupWindow senderControl = (GFSSetupWindow)obj;

            if (e.OldValue != null)
            {
                GFSSetupViewModel oldModel = (GFSSetupViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                GFSSetupViewModel newModel = (GFSSetupViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        public GFSSetupWindow(GFSSetupViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.GFSSetup);

            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(GFSSetupWindow_Loaded);
        }

        private void GFSSetupWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= GFSSetupWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to the request to navigate to the service path
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FullPathHyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //just close without committing changes.
            this.Close();
        }

        #endregion

        #region Window close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
