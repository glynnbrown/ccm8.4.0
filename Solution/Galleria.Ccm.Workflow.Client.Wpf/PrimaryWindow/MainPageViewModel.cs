﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// CCM-25438 : L.Ineson
//  Created.
// CCM-25444 : N.Haywood
//  Added location maintenance
// CCM-25449 : N.Haywood
//  Added location cluster maintenance
// CCM-25732 : L.Ineson
//  Added workflow maintenance
// V8-25453 : A.Kuszyk
//  Added CDT maintenance.
// CCM-25813 : N.Haywood
//  Changed ShowLocationHierarchyMaintenanceCommand's display name
// CCM-25446 : N.Haywood
//  Added LocationProductAttribute
// V8-25871 : A.Kuszyk
//  Added ShowPlanogramHierarchyMaintenanceCommand.
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-25454 : J.Pickup
//  Added ShowAssortmentMaintenanceCommand_Executed logic
// CCM-25455 : J.Pickup
//  Added ShowAssortmentMinorRevisoionMaintenanceCommand_Executed logic
// V8-26159 : L.Ineson
//  Added ShowPerformanceSelectionMaintenanceCommand
// V8-26400 : A.Silva 
//  Added ShowValidationTemplateMaintenanceCommand.
// V8-26520 : J.Pickup
//  Added ShowContentLookupMaintenanceCommand.
// V8-26158 : I.George
// Added the InventoryMaintenanceCommand
// V8-26560 : A.Probyn
// Added the ShowSequenceMaintenanceCommand.
// V8-26891 : L.Ineson
//  Added Blocking maintenance
// V8-26902 : M.Pettit
//  Added reporting resource registration
// V8-27153 : A.Silva
//      Added ShowRenumberingStrategyMaintenanceCommand.
// V8-27475 : A.Silva
//      Localized Renumbering Strategy Command.
// V8-27940 : L.Luong
//  Added LabelMaintenanceCommand
// V8-28149 : M.Pettit
//  Added framework resources message file registration for Reporting

#endregion

#region Version History: (CCM 8.0.1)
// V8-28669 : M.Shelley
//  Added the PlanogramComponentType to the resource manager
// V8-27937 : M.Shelley
//  Added a check on startup to test if there is a valid GFS connection and if not, disable the 
//  "Manage Performance Selections" button in the content setup page backstage.
#endregion

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Added PlanogramNameTemplate
// V8-29255 : L.Ineson
//  Removed command for old sequence maintenance form.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30044 : M.Pettit
//  Added Planogram Type enum to the reports resource registration
// V8-29975 : M.Shelley
//  Use the entity viewmodel GFS state delegate check to allow the F1 help key to 
//  display the correct help page
#endregion

#region Version History: (CCM 8.2.0)
// V8-30738 : L.Ineson
//  Added ShowPrintTemplateSetupCommand
// V8-31171 : M.Pettit
//	Added more reporting resources
#endregion

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Added LocationProductLegal
// V8-32041 : A.Silva
//  Added ShowPlanogramComparisonTemplateMaintenance Command.
// V8-31746 : M.Pettit
//  Registered further types as Report Resources (PlanogramAssortmentProductTreatmentType, PlanogramAssortmentProductLocalizationType,PlanogramAssortmentProductFamilyRuleType)
// CCM-18516 : M.Pettit
//  Registered further types as Report Resources (PlanogramItemComparisonStatusType)
// CCM-19023 : J.Mendes
//  Code added to initialize and update the real image provider. This is needed to make the export of a planograms with images to work as per the Editor.
//  The code added is similar to Editor but we kept separated because there may be different needs. 
//  So for all future changes to this initializer we should consider the Editor initializer as well.   
#endregion

#endregion

using System;
using System.Linq.Expressions;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup;
using Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.BlockingMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.HighlightMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.InventoryProfileMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.LabelMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.LocationClusterMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.LocationMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.LocationProductIllegalMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.MetricMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.MetricProfileMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramHierarchyMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramImportTemplateMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramExportTemplateMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramNameTemplateMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence;
using Galleria.Ccm.Workflow.Client.Wpf.ProductUniverseMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.RenumberingStrategyMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Reporting.Resources;
using FrameworkControls = Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.PrintTemplateSetup;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramComparisonTemplateMaintenance;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Viewmodel controller for MainPageOrganiser
    /// </summary>
    public sealed class MainPageViewModel : ViewModelAttachedControlObject<MainPageOrganiser>
    {
        #region Fields

        private UIScreen _selectedScreen = UIScreen.PlanRepository;
        private PlanogramHierarchyViewModel _masterPlanogramHierarchyView;
        private SystemStatusViewModel _systemStatusView = new SystemStatusViewModel();

        EntityViewModel _curEntityView = App.ViewState.CurrentEntityView;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedScreenProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SelectedScreen);

        //Commands
        public static readonly PropertyPath ShowLocationMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowLocationMaintenanceCommand);
        public static readonly PropertyPath ShowLocationHierarchyMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowLocationHierarchyMaintenanceCommand);
        public static readonly PropertyPath ShowLocationProductAttributeMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowLocationProductAttributeMaintenanceCommand);
        public static readonly PropertyPath ShowLocationProductIllegalMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowLocationProductIllegalMaintenanceCommand);
        public static readonly PropertyPath ShowLocationProductLegalMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowLocationProductLegalMaintenanceCommand);
        public static readonly PropertyPath ShowLocationSpaceMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowLocationSpaceMaintenanceCommand);
        public static readonly PropertyPath ShowClusterMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowClusterMaintenanceCommand);
        public static readonly PropertyPath ShowMerchandisingHierarchyMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowMerchandisingHierarchyMaintenanceCommand);
        public static readonly PropertyPath ShowPlanogramHierarchyMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowPlanogramHierarchyMaintenanceCommand);
        public static readonly PropertyPath ShowProductMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowProductMaintenanceCommand);
        public static readonly PropertyPath ShowProductUniverseMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowProductUniverseMaintenanceCommand);
        public static readonly PropertyPath ShowConsumerDecisionTreeMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowConsumerDecisionTreeMaintenanceCommand);
        public static readonly PropertyPath ShowAssortmentMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowAssortmentMaintenanceCommand);
        public static readonly PropertyPath ShowMinorAssortmentMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowMinorAssortmentMaintenanceCommand);
        public static readonly PropertyPath ShowWorkflowMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowWorkflowMaintenanceCommand);
        public static readonly PropertyPath ShowInventoryProfileMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowInventoryProfileMaintenanceCommand);
        public static readonly PropertyPath ShowMetricProfileMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowMetricProfileMaintenanceCommand);
        public static readonly PropertyPath ShowMetricMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowMetricMaintenanceCommand);
        public static readonly PropertyPath ShowPerformanceSelectionMaintenanceCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ShowPerformanceSelectionMaintenanceCommand);
        public static readonly PropertyPath ShowValidationTemplateMaintenanceCommandProperty = GetPropertyPath(p => p.ShowValidationTemplateMaintenanceCommand);
        public static readonly PropertyPath ShowContentLookupMaintenanceCommandProperty = GetPropertyPath(p => p.ShowContentLookupMaintenanceCommand);
        public static readonly PropertyPath ShowBlockingMaintenanceCommandProperty = GetPropertyPath(p => p.ShowBlockingMaintenanceCommand);
        public static readonly PropertyPath ShowLabelMaintenanceCommandProperty = GetPropertyPath(p => p.ShowLabelMaintenanceCommand);
        public static readonly PropertyPath ShowHighlightMaintenanceCommandProperty = GetPropertyPath(p => p.ShowHighlightMaintenanceCommand);
        public static readonly PropertyPath ShowPlanogramImportTemplateMaintenanceCommandProperty = GetPropertyPath(p => p.ShowPlanogramImportTemplateMaintenanceCommand);
        public static readonly PropertyPath ShowPlanogramExportTemplateMaintenanceCommandProperty = GetPropertyPath(p => p.ShowPlanogramExportTemplateMaintenanceCommand);
        public static readonly PropertyPath ShowPlanogramNameTemplateMaintenanceCommandProperty = GetPropertyPath(p => p.ShowPlanogramNameTemplateMaintenanceCommand);
        public static readonly PropertyPath ShowRenumberingStrategyMaintenanceCommandProperty = GetPropertyPath(p => p.ShowRenumberingStrategyMaintenanceCommand);
        public static readonly PropertyPath ShowPrintTemplateSetupCommandProperty = GetPropertyPath(p => p.ShowPrintTemplateSetupCommand);
        public static readonly PropertyPath OptionsCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.OptionsCommand);
        public static readonly PropertyPath ExitApplicationCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ExitApplicationCommand);


        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the selected screen
        /// </summary>
        public UIScreen SelectedScreen
        {
            get { return _selectedScreen; }
            set
            {
                _selectedScreen = value;
                OnPropertyChanged(SelectedScreenProperty);
            }
        }

        /// <summary>
        /// Returns the viewmodel holding the master planogram hierarchy
        /// </summary>
        public PlanogramHierarchyViewModel MasterPlanogramHierarchyView
        {
            get
            {
                if (_masterPlanogramHierarchyView == null)
                {
                    _masterPlanogramHierarchyView = new PlanogramHierarchyViewModel();
                    _masterPlanogramHierarchyView.FetchForCurrentEntity();
                }
                return _masterPlanogramHierarchyView;
            }
        }

        /// <summary>
        /// Returns a reference to the CCM engine
        /// so we can view the engine state
        /// </summary>
        public Engine.Engine Engine
        {
            get { return App.Engine; }
        }

        /// <summary>
        /// Returns the viewmodel holding the current engine status information
        /// </summary>
        public SystemStatusViewModel SystemStatusView
        {
            get
            {
                if (_systemStatusView == null)
                {
                    _systemStatusView = new SystemStatusViewModel();
                }
                return _systemStatusView;
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public MainPageViewModel()
        {
            //Register resource file for reporting
            RegisterResources();

            //Initialize the real image provider with the user settings.
            LocalHelper.RegisterRealImageProvider();

            //Start monitoring engine status
            SystemStatusView.StartPolling();
        }

        #endregion

        #region Commands

        #region Assortment Maintenance

        private RelayCommand _showAssortmentMaintenanceCommand;

        public RelayCommand ShowAssortmentMaintenanceCommand
        {
            get
            {
                if (_showAssortmentMaintenanceCommand == null)
                {
                    _showAssortmentMaintenanceCommand = new RelayCommand(
                        p => ShowAssortmentMaintenance_Executed(),
                        p => ShowAssortmentMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_AssortmentMaintenance_Short,
                        FriendlyDescription = Message.Setup_AssortmentMaintenance_Description,
                        Icon = ImageResources.AssortmentMaintenance,
                    };
                    base.ViewModelCommands.Add(_showAssortmentMaintenanceCommand);
                }
                return _showAssortmentMaintenanceCommand;
            }
        }

        private Boolean ShowAssortmentMaintenance_CanExecute()
        {
            return new ModelPermission<Assortment>().CanFetch;
        }

        private void ShowAssortmentMaintenance_Executed()
        {
            ShowOrActivateWindow<AssortmentSetupOrganiser>();
        }

        #endregion

        #region Blocking Maintenance

        private RelayCommand _showBlockingMaintenanceCommand;

        public RelayCommand ShowBlockingMaintenanceCommand
        {
            get
            {
                if (_showBlockingMaintenanceCommand == null)
                {
                    _showBlockingMaintenanceCommand = new RelayCommand(
                        p => ShowBlockingMaintenance_Executed(),
                        p => ShowBlockingMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_BlockingMaintenance_Short,
                        FriendlyDescription = Message.Setup_BlockingMaintenance_Description,
                        Icon = ImageResources.BlockingMaintenance
                    };
                    base.ViewModelCommands.Add(_showBlockingMaintenanceCommand);
                }
                return _showBlockingMaintenanceCommand;
            }
        }

        private Boolean ShowBlockingMaintenance_CanExecute()
        {
            return new ModelPermission<Blocking>().CanFetch;
        }

        private void ShowBlockingMaintenance_Executed()
        {
            ShowOrActivateWindow<BlockingMaintenanceOrganiser>();
        }

        #endregion

        #region Content Lookup Maintenance

        private RelayCommand _showContentLookupMaintenanceCommand;

        public RelayCommand ShowContentLookupMaintenanceCommand
        {
            get
            {
                if (_showContentLookupMaintenanceCommand == null)
                {
                    _showContentLookupMaintenanceCommand = new RelayCommand(
                        p => ShowContentLookupMaintenance_Executed(),
                        p => ShowContentLookupMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_ContentLookupMaintenance_Short,
                        FriendlyDescription = Message.Setup_ContentLookupMaintenance_Description,
                        Icon = ImageResources.ContentLookup
                    };
                    base.ViewModelCommands.Add(_showContentLookupMaintenanceCommand);
                }
                return _showContentLookupMaintenanceCommand;
            }
        }

        private Boolean ShowContentLookupMaintenance_CanExecute()
        {
            // NF - This permission is handled slightly differently than normal
            return ApplicationContext.User.IsInRole(DomainPermission.ContentLookupGet.ToString());
        }

        private void ShowContentLookupMaintenance_Executed()
        {
            ShowOrActivateWindow<ContentLookupMaintenanceOrganiser>();
        }

        #endregion

        #region Consumer Decision Tree Maintenance

        private RelayCommand _showConsumerDecisionTreeMaintenanceCommand;

        /// <summary>
        /// Opens the cdt maintenance window
        /// </summary>
        public RelayCommand ShowConsumerDecisionTreeMaintenanceCommand
        {
            get
            {
                if (_showConsumerDecisionTreeMaintenanceCommand == null)
                {
                    _showConsumerDecisionTreeMaintenanceCommand = new RelayCommand(
                        p => ShowConsumerDecisionTreeMaintenance_Executed(),
                        p => ShowConsumerDecisionTreeMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_ConsumerDecisionTreeMaintenance_Short,
                        FriendlyDescription = Message.Setup_ConsumerDecisionTreeMaintenance_Description,
                        Icon = ImageResources.ConsumerDecisionTreeMaintenance
                    };
                    base.ViewModelCommands.Add(_showConsumerDecisionTreeMaintenanceCommand);
                }
                return _showConsumerDecisionTreeMaintenanceCommand;
            }
        }

        private Boolean ShowConsumerDecisionTreeMaintenance_CanExecute()
        {
            return new ModelPermission<ConsumerDecisionTree>().CanFetch;
        }

        private void ShowConsumerDecisionTreeMaintenance_Executed()
        {
            ShowOrActivateWindow<CdtMaintenanceOrganiser>();
        }

        #endregion

        #region Cluster Scheme Maintenance

        private RelayCommand _showClusterMaintenanceCommand;

        /// <summary>
        /// Launches the cluster maintenance screen
        /// </summary>
        public RelayCommand ShowClusterMaintenanceCommand
        {
            get
            {
                if (_showClusterMaintenanceCommand == null)
                {
                    _showClusterMaintenanceCommand = new RelayCommand(
                    p => ShowClusterMaintenance_Executed(),
                    p => ShowClusterMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LocationClusterMaintenance_Short,
                        FriendlyDescription = Message.Setup_LocationClusterMaintenance_Description,
                        Icon = ImageResources.LocationClusterMaintenance,
                        DisabledReason = Message.Setup_LocationClusterMaintenance_DisabledReason
                    };
                    base.ViewModelCommands.Add(_showClusterMaintenanceCommand);
                }
                return _showClusterMaintenanceCommand;
            }
        }

        private Boolean ShowClusterMaintenance_CanExecute()
        {
            return new ModelPermission<ClusterScheme>().CanFetch;
        }

        private void ShowClusterMaintenance_Executed()
        {
            ShowOrActivateWindow<LocationClusterMaintenanceOrganiser>();
        }

        #endregion

        #region Highlight Maintenance

        private RelayCommand _showHighlightMaintenanceCommand;

        /// <summary>
        /// Launches the Highlight maintenace window.
        /// </summary>
        public RelayCommand ShowHighlightMaintenanceCommand
        {
            get
            {
                if (_showHighlightMaintenanceCommand == null)
                {
                    _showHighlightMaintenanceCommand = new RelayCommand(
                        p => ShowHighlightMaintenance_Executed(),
                        p => ShowHighlightMaintenance_CanExecute())
                    {
                        FriendlyName = Message.HighlightMaintenance_Highlights,
                        FriendlyDescription = Message.Setup_HighlightMaintenance_Description,
                        Icon = ImageResources.View_ManageHighlights_32
                    };
                    base.ViewModelCommands.Add(_showHighlightMaintenanceCommand);
                }
                return _showHighlightMaintenanceCommand;
            }
        }

        private Boolean ShowHighlightMaintenance_CanExecute()
        {
            return ApplicationContext.User.IsInRole(DomainPermission.HighlightGet.ToString());
        }

        private void ShowHighlightMaintenance_Executed()
        {
            ShowOrActivateWindow<HighlightMaintenanceOrganiser>();
        }

        #endregion

        #region Inventory Profile Maintenance

        private RelayCommand _showInventoryProfileCommand;

        /// <summary>
        /// launches the inventory profile Maintenance window
        /// </summary>
        public RelayCommand ShowInventoryProfileMaintenanceCommand
        {
            get
            {
                if (_showInventoryProfileCommand == null)
                {
                    _showInventoryProfileCommand = new RelayCommand(
                        p => ShowInventoryProfile_Executed(),
                        p => ShowInventoryProfile_CanExecute())
                    {
                        FriendlyName = Message.Setup_InventoryProfile_Short,
                        FriendlyDescription = Message.Setup_InventoryprofileMaintenance_Description,
                        Icon = ImageResources.InventoryProfileMaintenance_32
                    };
                    base.ViewModelCommands.Add(_showInventoryProfileCommand);
                }
                return _showInventoryProfileCommand;

            }
        }

        private Boolean ShowInventoryProfile_CanExecute()
        {
            return new ModelPermission<InventoryProfile>().CanFetch;
        }

        private void ShowInventoryProfile_Executed()
        {
            ShowOrActivateWindow<InventoryProfileMaintenanceOrganiser>();
        }

        #endregion

        #region Label Maintenance

        private RelayCommand _showLabelMaintenanceCommand;

        /// <summary>
        /// Launches the label maintenace window.
        /// </summary>
        public RelayCommand ShowLabelMaintenanceCommand
        {
            get
            {
                if (_showLabelMaintenanceCommand == null)
                {
                    _showLabelMaintenanceCommand = new RelayCommand(
                        p => ShowLabelMaintenance_Executed(),
                        p => ShowLabelMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LabelMaintenance_Short,
                        FriendlyDescription = Message.Setup_LabelMaintenance_Description,
                        Icon = ImageResources.LabelMaintenance
                    };
                    base.ViewModelCommands.Add(_showLabelMaintenanceCommand);
                }
                return _showLabelMaintenanceCommand;
            }
        }

        private Boolean ShowLabelMaintenance_CanExecute()
        {
            return ApplicationContext.User.IsInRole(DomainPermission.LabelGet.ToString());
        }

        private void ShowLabelMaintenance_Executed()
        {
            ShowOrActivateWindow<LabelMaintenanceOrganiser>();
        }

        #endregion

        #region Location Maintenance

        private RelayCommand _showLocationMaintenanceCommand;

        /// <summary>
        /// Launches the location maintenace window.
        /// </summary>
        public RelayCommand ShowLocationMaintenanceCommand
        {
            get
            {
                if (_showLocationMaintenanceCommand == null)
                {
                    _showLocationMaintenanceCommand = new RelayCommand(
                        p => ShowLocationMaintenance_Executed(),
                        p => ShowLocationMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LocationMaintenance_Short,
                        FriendlyDescription = Message.Setup_LocationMaintenance_Description,
                        Icon = ImageResources.LocationMaintenance,
                        DisabledReason = Message.Setup_LocationMaintenance_DisabledReason
                    };
                    base.ViewModelCommands.Add(_showLocationMaintenanceCommand);
                }
                return _showLocationMaintenanceCommand;
            }
        }

        private Boolean ShowLocationMaintenance_CanExecute()
        {
            return new ModelPermission<Location>().CanFetch;
        }

        private void ShowLocationMaintenance_Executed()
        {
            ShowOrActivateWindow<LocationMaintenanceOrganiser>();
        }

        #endregion

        #region Location Hierarchy Maintenance

        private RelayCommand _showLocationHierarchyMaintenanceCommand;

        /// <summary>
        /// Launches the location hierarchy maintenance window.
        /// </summary>
        public RelayCommand ShowLocationHierarchyMaintenanceCommand
        {
            get
            {
                if (_showLocationHierarchyMaintenanceCommand == null)
                {
                    _showLocationHierarchyMaintenanceCommand = new RelayCommand(
                        p => ShowLocationHierarchyMaintenance_Executed(),
                        p => ShowLocationHierarchyMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LocationHierarchyMaintenance_Short,
                        FriendlyDescription = Message.Setup_LocationHierarchyMaintenance_Description,
                        Icon = ImageResources.LocationHierarchyMaintenance
                    };
                    base.ViewModelCommands.Add(_showLocationHierarchyMaintenanceCommand);
                }
                return _showLocationHierarchyMaintenanceCommand;

            }

        }

        private Boolean ShowLocationHierarchyMaintenance_CanExecute()
        {
            return new ModelPermission<LocationHierarchy>().CanEdit;
        }

        private void ShowLocationHierarchyMaintenance_Executed()
        {
            ShowOrActivateWindow<LocationHierarchyOrganiser>();
        }

        #endregion

        #region LocationProductAttribute Maintenance

        private RelayCommand _showLocationProductAttributeMaintenanceCommand;

        /// <summary>
        /// Launches the LocationProductAttributeMaintenance window
        /// </summary>
        public RelayCommand ShowLocationProductAttributeMaintenanceCommand
        {
            get
            {
                if (_showLocationProductAttributeMaintenanceCommand == null)
                {
                    _showLocationProductAttributeMaintenanceCommand = new RelayCommand(
                        p => ShowLocationProductAttributeMaintenance_Executed(),
                        p => ShowLocationProductAttributeMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LocationProductAttributeMaintenance_Short,
                        FriendlyDescription = Message.Setup_LocationProductAttributeMaintenance_Description,
                        Icon = ImageResources.LocationProductAttributeMaintenance,
                    };
                    base.ViewModelCommands.Add(_showLocationProductAttributeMaintenanceCommand);
                }
                return _showLocationProductAttributeMaintenanceCommand;
            }
        }

        private Boolean ShowLocationProductAttributeMaintenance_CanExecute()
        {
            return new ModelPermission<LocationProductAttribute>().CanFetch;
        }

        private void ShowLocationProductAttributeMaintenance_Executed()
        {
            ShowOrActivateWindow<LocationProductAttributeMaintenanceOrganiser>();
        }

        #endregion

        #region LocationProductIllegal Maintenance

        private RelayCommand _showLocationProductIllegalMaintenanceCommand;

        /// <summary>
        /// Launches the Location Product Illegal Maintenance window.
        /// </summary>
        public RelayCommand ShowLocationProductIllegalMaintenanceCommand
        {
            get
            {
                if (_showLocationProductIllegalMaintenanceCommand == null)
                {
                    _showLocationProductIllegalMaintenanceCommand = new RelayCommand(
                        p => ShowLocationProductIllegalMaintenance_Executed(),
                        p => ShowLocationProductIllegalMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LocationProductIllegalMaintenance_Short,
                        FriendlyDescription = Message.Setup_LocationProductIllegalMaintenance_Description,
                        Icon = ImageResources.LocationProductIllegalMaintenance,
                    };
                    base.ViewModelCommands.Add(_showLocationProductIllegalMaintenanceCommand);
                }
                return _showLocationProductIllegalMaintenanceCommand;
            }
        }

        private Boolean ShowLocationProductIllegalMaintenance_CanExecute()
        {
            return new ModelPermission<LocationProductIllegal>().CanFetch;
        }

        private void ShowLocationProductIllegalMaintenance_Executed()
        {
            ShowOrActivateWindow<LocationToProductIllegalOrganiser>();
        }

        #endregion

        #region LocationProductLegal Maintenance

        private RelayCommand _showLocationProductLegalMaintenanceCommand;

        /// <summary>
        /// Launches the Location Product Legal Maintenance window.
        /// </summary>
        public RelayCommand ShowLocationProductLegalMaintenanceCommand
        {
            get
            {
                if (_showLocationProductLegalMaintenanceCommand == null)
                {
                    _showLocationProductLegalMaintenanceCommand = new RelayCommand(
                        p => ShowLocationProductLegalMaintenance_Executed(),
                        p => ShowLocationProductLegalMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LocationProductLegalMaintenance_Short,
                        FriendlyDescription = Message.Setup_LocationProductLegalMaintenance_Description,
                        Icon = ImageResources.LocationProductLegalMaintenance,
                    };
                    base.ViewModelCommands.Add(_showLocationProductLegalMaintenanceCommand);
                }
                return _showLocationProductLegalMaintenanceCommand;
            }
        }

        private Boolean ShowLocationProductLegalMaintenance_CanExecute()
        {
            return new ModelPermission<LocationProductLegal>().CanFetch;
        }

        private void ShowLocationProductLegalMaintenance_Executed()
        {
            ShowOrActivateWindow<LocationProductLegalOrganiser>();
        }

        #endregion

        #region Location Space Maintenance

        private RelayCommand _showLocationSpaceMaintenanceCommand;

        /// <summary>
        /// Launches the location space window
        /// </summary>
        public RelayCommand ShowLocationSpaceMaintenanceCommand
        {
            get
            {
                if (_showLocationSpaceMaintenanceCommand == null)
                {
                    _showLocationSpaceMaintenanceCommand = new RelayCommand(
                    p => ShowLocationSpaceMaintenance_Executed(),
                    p => ShowLocationSpaceMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_LocationSpaceMaintenance_Short,
                        FriendlyDescription = Message.Setup_LocationSpaceMaintenance_Description,
                        Icon = ImageResources.LocationSpaceMaintenance
                    };
                    base.ViewModelCommands.Add(_showLocationSpaceMaintenanceCommand);
                }
                return _showLocationSpaceMaintenanceCommand;
            }
        }

        private Boolean ShowLocationSpaceMaintenance_CanExecute()
        {
            return new ModelPermission<LocationSpace>().CanFetch;
        }

        private void ShowLocationSpaceMaintenance_Executed()
        {
            ShowOrActivateWindow<LocationSpaceMaintenanceOrganiser>();
        }

        #endregion

        #region MinorAssortment Maintenance

        private RelayCommand _showMinorAssortmentMaintenanceCommand;

        public RelayCommand ShowMinorAssortmentMaintenanceCommand
        {
            get
            {
                if (_showMinorAssortmentMaintenanceCommand == null)
                {
                    _showMinorAssortmentMaintenanceCommand = new RelayCommand(
                        p => ShowMinorAssortmentMaintenance_Executed(),
                        p => ShowMinorAssortmentMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_MinorAssortmentMaintenance_Short,
                        FriendlyDescription = Message.Setup_MinorAssortmentMaintenance_Description,
                        Icon = ImageResources.MinorAssortmentMaintenance,
                    };
                    base.ViewModelCommands.Add(_showMinorAssortmentMaintenanceCommand);
                }
                return _showMinorAssortmentMaintenanceCommand;
            }
        }

        private Boolean ShowMinorAssortmentMaintenance_CanExecute()
        {
            return new ModelPermission<AssortmentMinorRevision>().CanFetch;
        }

        private void ShowMinorAssortmentMaintenance_Executed()
        {
            ShowOrActivateWindow<AssortmentMinorRevisionSetupOrganiser>();
        }

        #endregion

        #region Metric Profile Maintenance

        private RelayCommand _showMetricProfileMaintenanceCommand;

        /// <summary>
        /// launches the metric profile Maintenance window
        /// </summary>

        public RelayCommand ShowMetricProfileMaintenanceCommand
        {
            get
            {
                if (_showMetricProfileMaintenanceCommand == null)
                {
                    _showMetricProfileMaintenanceCommand = new RelayCommand(
                        p => ShowMetricProfileMaintenance_Executed(),
                        p => ShowMetricProfileMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_MetricProfileMaintenance_Short,
                        FriendlyDescription = Message.Setup_MetricProfileMaintenance_Description,
                        Icon = ImageResources.MetricProfileMaintenance_32
                    };
                    base.ViewModelCommands.Add(_showMetricProfileMaintenanceCommand);
                }
                return _showMetricProfileMaintenanceCommand;

            }
        }

        private Boolean ShowMetricProfileMaintenance_CanExecute()
        {
            return new ModelPermission<MetricProfile>().CanFetch;
        }

        private void ShowMetricProfileMaintenance_Executed()
        {
            ShowOrActivateWindow<MetricProfileMaintenanceOrganiser>();
        }

        #endregion

        #region Metric Maintenance

        private RelayCommand _showMetricMaintenanceCommand;

        /// <summary>
        /// launches the metric profile Maintenance window
        /// </summary>

        public RelayCommand ShowMetricMaintenanceCommand
        {
            get
            {
                if (_showMetricMaintenanceCommand == null)
                {
                    _showMetricMaintenanceCommand = new RelayCommand(
                        p => ShowMetricMaintenance_Executed(),
                        p => ShowMetricMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_MetricMaintenance_Short,
                        FriendlyDescription = Message.Setup_MetricProfileMaintenance_Text1,
                        Icon = ImageResources.MetricMaintenance_32
                    };
                    base.ViewModelCommands.Add(_showMetricMaintenanceCommand);
                }
                return _showMetricMaintenanceCommand;

            }
        }

        private Boolean ShowMetricMaintenance_CanExecute()
        {
            return new ModelPermission<Metric>().CanFetch;
        }

        private void ShowMetricMaintenance_Executed()
        {
            ShowOrActivateWindow<MetricMaintenanceOrganiser>();
        }

        #endregion

        #region Merchandising Hierarchy Maintenance

        private RelayCommand _showMerchandisingHierarchyMaintenanceCommand;

        /// <summary>
        /// Launches the merchandising hierarchy screen
        /// </summary>
        public RelayCommand ShowMerchandisingHierarchyMaintenanceCommand
        {
            get
            {
                if (_showMerchandisingHierarchyMaintenanceCommand == null)
                {
                    _showMerchandisingHierarchyMaintenanceCommand = new RelayCommand(
                        p => ShowMerchandisingHierarchyMaintenance_Executed(),
                        p => ShowMerchandisingHierarchyMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_MerchHierarchyMaintenance_Short,
                        FriendlyDescription = Message.Setup_MerchHierarchyMaintenance_Description,
                        Icon = ImageResources.MerchHierarchyMaintenance
                    };
                    base.ViewModelCommands.Add(_showMerchandisingHierarchyMaintenanceCommand);
                }
                return _showMerchandisingHierarchyMaintenanceCommand;

            }

        }

        private Boolean ShowMerchandisingHierarchyMaintenance_CanExecute()
        {
            return new ModelPermission<ProductHierarchy>().CanEdit;
        }

        private void ShowMerchandisingHierarchyMaintenance_Executed()
        {
            ShowOrActivateWindow<MerchHierarchyOrganiser>();
        }

        #endregion

        #region PlanogramComparisonTemplateMaintenance

        /// <summary>
        ///     The <see cref="RelayCommand"/> that shows the Planogram Comparison Template Maintenance Screen to the user.
        /// </summary>
        private RelayCommand _showPlanogramComparisonTemplateMaintenanceCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="ShowPlanogramComparisonTemplateMaintenanceCommand"/> property.
        /// </summary>
        public static readonly PropertyPath ShowPlanogramComparisonTemplateMaintenanceCommandProperty =
            GetPropertyPath(p => p.ShowPlanogramComparisonTemplateMaintenanceCommand);

        /// <summary>
        ///     Gets the <see cref="_showPlanogramComparisonTemplateMaintenanceCommand" />.
        /// </summary>
        /// <remarks>If the command is not yet initialized, it does so before returning it.</remarks>
        public RelayCommand ShowPlanogramComparisonTemplateMaintenanceCommand
        {
            get
            {
                if (_showPlanogramComparisonTemplateMaintenanceCommand != null) return _showPlanogramComparisonTemplateMaintenanceCommand;

                _showPlanogramComparisonTemplateMaintenanceCommand = new RelayCommand(
                    p => ShowPlanogramComparisonTemplateMaintenance_Executed(),
                    p => ShowPlanogramComparisonTemplateMaintenance_CanExecute())
                {
                    FriendlyName = Message.Setup_PlanogramComparisonMaintenance_Short,
                    FriendlyDescription = Message.Setup_PlanogramComparisonMaintenance_Description,
                    Icon = CommonImageResources.PlanogramComparisonSetup
                };
                ViewModelCommands.Add(_showPlanogramComparisonTemplateMaintenanceCommand);

                return _showPlanogramComparisonTemplateMaintenanceCommand;
            }
        }

        /// <summary>
        ///     Invoked whenever the app needs to determine if the <see cref="ShowPlanogramComparisonTemplateMaintenanceCommand"/> can be executed.
        /// </summary>
        /// <returns><c>True</c> if the command can be executed now, <c>false</c> otherwise.</returns>
        private Boolean ShowPlanogramComparisonTemplateMaintenance_CanExecute()
        {
            return new ModelPermission<ValidationTemplate>().CanFetch;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="ShowPlanogramComparisonTemplateMaintenanceCommand"/> is executed.
        /// </summary>
        private void ShowPlanogramComparisonTemplateMaintenance_Executed()
        {
            ShowOrActivateWindow<PlanogramComparisonTemplateMaintenanceOrganiser>();
        }

        #endregion

        #region Planogram Hierarchy Maintenance

        private RelayCommand _showPlanogramHierarchyMaintenanceCommand;

        /// <summary>
        /// Launches the Planogram Hiearchy Maintenance screen.
        /// </summary>
        public RelayCommand ShowPlanogramHierarchyMaintenanceCommand
        {
            get
            {
                if (_showPlanogramHierarchyMaintenanceCommand == null)
                {
                    _showPlanogramHierarchyMaintenanceCommand = new RelayCommand(
                        p => ShowPlanogramHierarchyMaintenanceCommand_Executed(),
                        p => ShowPlanogramHierarchyMaintenanceCommand_CanExecute())
                    {
                        FriendlyName = Message.Setup_PlanogramHierarchyMaintenance_Short,
                        FriendlyDescription = Message.Setup_PlanogramHierarchyMaintenance_Description,
                        Icon = ImageResources.PlanogramHierarchyMaintenance
                    };
                    base.ViewModelCommands.Add(_showPlanogramHierarchyMaintenanceCommand);
                }
                return _showPlanogramHierarchyMaintenanceCommand;
            }
        }

        private Boolean ShowPlanogramHierarchyMaintenanceCommand_CanExecute()
        {
            return new ModelPermission<PlanogramHierarchy>().CanEdit;
        }

        private void ShowPlanogramHierarchyMaintenanceCommand_Executed()
        {
            ShowOrActivateWindow<PlanogramHierarchyMaintenanceOrganiser>();
        }

        #endregion

        #region PlanogramImportTemplate Maintenance

        private RelayCommand _showPlanogramImportTemplateMaintenanceCommand;

        /// <summary>
        /// Launches the PlanogramImportTemplate maintenace window.
        /// </summary>
        public RelayCommand ShowPlanogramImportTemplateMaintenanceCommand
        {
            get
            {
                if (_showPlanogramImportTemplateMaintenanceCommand == null)
                {
                    _showPlanogramImportTemplateMaintenanceCommand = new RelayCommand(
                        p => ShowPlanogramImportTemplateMaintenance_Executed(),
                        p => ShowPlanogramImportTemplateMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_PlanogramImportTemplateMaintenance_Short,
                        FriendlyDescription = Message.Setup_PlanogramImportTemplateMaintenance_Description,
                        Icon = ImageResources.PlanogramImportTemplateMaintenance
                    };
                    base.ViewModelCommands.Add(_showPlanogramImportTemplateMaintenanceCommand);
                }
                return _showPlanogramImportTemplateMaintenanceCommand;
            }
        }

        private Boolean ShowPlanogramImportTemplateMaintenance_CanExecute()
        {
            return ApplicationContext.User.IsInRole(DomainPermission.PlanogramImportTemplateGet.ToString());
        }

        private void ShowPlanogramImportTemplateMaintenance_Executed()
        {
            ShowOrActivateWindow<PlanogramImportTemplateMaintenanceOrganiser>();
        }

        #endregion

        #region PlanogramExportTemplate Maintenance

        private RelayCommand _showPlanogramExportTemplateMaintenanceCommand;

        /// <summary>
        /// Launches the PlanogramExportTemplate maintenace window.
        /// </summary>
        public RelayCommand ShowPlanogramExportTemplateMaintenanceCommand
        {
            get
            {
                if (_showPlanogramExportTemplateMaintenanceCommand == null)
                {
                    _showPlanogramExportTemplateMaintenanceCommand = new RelayCommand(
                        p => ShowPlanogramExportTemplateMaintenance_Executed(),
                        p => ShowPlanogramExportTemplateMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_PlanogramExportTemplateMaintenance_Short,
                        FriendlyDescription = Message.Setup_PlanogramExportTemplateMaintenance_Description,
                        Icon = ImageResources.PlanogramExportTemplateMaintenance
                    };
                    base.ViewModelCommands.Add(_showPlanogramExportTemplateMaintenanceCommand);
                }
                return _showPlanogramExportTemplateMaintenanceCommand;
            }
        }

        private Boolean ShowPlanogramExportTemplateMaintenance_CanExecute()
        {
            return ApplicationContext.User.IsInRole(DomainPermission.PlanogramExportTemplateGet.ToString());
        }

        private void ShowPlanogramExportTemplateMaintenance_Executed()
        {
            ShowOrActivateWindow<PlanogramExportTemplateMaintenanceOrganiser>();
        }

        #endregion

        #region PlanogramNameTemplate Maintenance

        private RelayCommand _showPlanogramNameTemplateMaintenanceCommand;

        /// <summary>
        /// Launches the PlanogramNameTemplate maintenace window.
        /// </summary>
        public RelayCommand ShowPlanogramNameTemplateMaintenanceCommand
        {
            get
            {
                if (_showPlanogramNameTemplateMaintenanceCommand == null)
                {
                    _showPlanogramNameTemplateMaintenanceCommand = new RelayCommand(
                        p => ShowPlanogramNameTemplateMaintenance_Executed(),
                        p => ShowPlanogramNameTemplateMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_PlanogramNameTemplateMaintenance_Short,
                        FriendlyDescription = Message.Setup_PlanogramNameTemplateMaintenance_Description,
                        Icon = ImageResources.PlanogramNameTemplateMaintenance
                    };
                    base.ViewModelCommands.Add(_showPlanogramNameTemplateMaintenanceCommand);
                }
                return _showPlanogramNameTemplateMaintenanceCommand;
            }
        }

        private Boolean ShowPlanogramNameTemplateMaintenance_CanExecute()
        {
            return ApplicationContext.User.IsInRole(DomainPermission.PlanogramNameTemplateGet.ToString());
        }

        private void ShowPlanogramNameTemplateMaintenance_Executed()
        {
            ShowOrActivateWindow<PlanogramNameTemplateMaintenanceOrganiser>();
        }

        #endregion

        #region ShowPrintTemplateSetup

        private RelayCommand _showPrintTemplateSetupCommand;

        /// <summary>
        /// Launches the PrintTemplateSetup window.
        /// </summary>
        public RelayCommand ShowPrintTemplateSetupCommand
        {
            get
            {
                if (_showPrintTemplateSetupCommand == null)
                {
                    _showPrintTemplateSetupCommand = new RelayCommand(
                        p => ShowPrintTemplateSetup_Executed())
                    {
                        FriendlyName = Message.Setup_PrintTemplateSetup_Short,
                        FriendlyDescription = Message.Setup_PrintTemplateSetup_Description,
                        Icon = ImageResources.PrintTemplateMaintenance_32
                    };
                    base.ViewModelCommands.Add(_showPrintTemplateSetupCommand);
                }
                return _showPrintTemplateSetupCommand;
            }
        }

        private void ShowPrintTemplateSetup_Executed()
        {
            if (!TryActivateWindow<PrintTemplateSetupWindow>()) 
            {
                PrintTemplateSetupViewModel viewModel = new PrintTemplateSetupViewModel(App.ViewState.EntityId);
                CommonHelper.GetWindowService().ShowDialog<PrintTemplateSetupWindow>(viewModel);
            }
        }

        #endregion

        #region Product Maintenance

        private RelayCommand _showProductMaintenanceCommand;

        /// <summary>
        /// Launches the product maintenance window.
        /// </summary>
        public RelayCommand ShowProductMaintenanceCommand
        {
            get
            {
                if (_showProductMaintenanceCommand == null)
                {
                    _showProductMaintenanceCommand = new RelayCommand(
                        p => ShowProductMaintenance_Executed(),
                        p => ShowProductMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_ProductMaintenance_Short,
                        FriendlyDescription = Message.Setup_ProductMaintenance_Description,
                        Icon = ImageResources.ProductMaintenance
                    };
                    base.ViewModelCommands.Add(_showProductMaintenanceCommand);
                }
                return _showProductMaintenanceCommand;
            }
        }

        private Boolean ShowProductMaintenance_CanExecute()
        {
            return new ModelPermission<Product>().CanFetch;
        }

        private void ShowProductMaintenance_Executed()
        {
            ShowOrActivateWindow<ProductMaintenanceOrganiser>();
        }

        #endregion

        #region Product Universe Maintenance

        private RelayCommand _showProductUniverseMaintenanceCommand;

        /// <summary>
        /// Launches the product universe maintenance window.
        /// </summary>
        public RelayCommand ShowProductUniverseMaintenanceCommand
        {
            get
            {
                if (_showProductUniverseMaintenanceCommand == null)
                {
                    _showProductUniverseMaintenanceCommand = new RelayCommand(
                        p => ShowProductUniverseMaintenance_Executed(),
                        p => ShowProductUniverseMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_ProductUniverseMaintenance_Short,
                        FriendlyDescription = Message.Setup_ProductUniverseMaintenance_Description,
                        Icon = ImageResources.ProductUniverseMaintenance
                    };
                    base.ViewModelCommands.Add(_showProductUniverseMaintenanceCommand);
                }
                return _showProductUniverseMaintenanceCommand;
            }
        }

        private Boolean ShowProductUniverseMaintenance_CanExecute()
        {
            return new ModelPermission<ProductUniverse>().CanFetch;
        }

        private void ShowProductUniverseMaintenance_Executed()
        {
            ShowOrActivateWindow<ProductUniverseMaintenanceOrganiser>();
        }

        #endregion

        #region Performance Selection Maintenance

        private RelayCommand _showPerformanceSelectionMaintenanceCommand;

        public RelayCommand ShowPerformanceSelectionMaintenanceCommand
        {
            get
            {
                if (_showPerformanceSelectionMaintenanceCommand == null)
                {
                    _showPerformanceSelectionMaintenanceCommand = new RelayCommand(
                        p => ShowPerformanceSelectionMaintenance_Executed(),
                        p => ShowPerformanceSelectionMaintenance_CanExecute())

                    {
                        FriendlyName = Message.Setup_PerformanceSelectionMaintenance_Short,
                        FriendlyDescription = Message.Setup_PerformanceSelectionMaintenance_Text1,
                        Icon = ImageResources.PerformanceSelectionMaintenance_32
                    };
                    base.ViewModelCommands.Add(_showPerformanceSelectionMaintenanceCommand);
                }
                return _showPerformanceSelectionMaintenanceCommand;
            }
        }

        private Boolean ShowPerformanceSelectionMaintenance_CanExecute()
        {
            // First check the model permissions allow the user access the performance selections
            bool arePermissionsValid = new ModelPermission<PerformanceSelection>().CanFetch;

            // Check there is actually a valid GFS connection
            if (_curEntityView.IsValidGFSConnection == null)
            {
                _curEntityView.IsValidGFSConnection = IsValidGFSConnectionState();
            }

            return (arePermissionsValid &&
                (_curEntityView.IsValidGFSConnection != null && _curEntityView.IsValidGFSConnection == true));
        }

        private void ShowPerformanceSelectionMaintenance_Executed()
        {
            var curEntityView = App.ViewState.CurrentEntityView;
            //curEntityView.CheckGFSConnectionState();

            // Check the gfs connection using the delegate rather than the modal window
            // to prevent the modal window taking the focus and preventing the F1 help
            // appearing to display the wrong help page
            curEntityView.ValidateGFSConnectionState(ShowPerformanceSelectionMaintenanceWindow);
        }

        // object sender, System.ComponentModel.RunWorkerCompletedEventArgs e
        private void ShowPerformanceSelectionMaintenanceWindow(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            var args = (EntityViewModel.ConnectionCheckWorkArgs)e.Result;

            if (args.Result == EntityViewModel.GFSConnectionState.Successful)
            {
                ShowOrActivateWindow<PerformanceSelectionMaintenanceWindow>();
            }
            else
            {
                // the connection has failed so display the warning window
                var curEntityView = App.ViewState.CurrentEntityView;
                curEntityView.CheckGFSConnectionState();
            }
        }

        #endregion

        #region ValidationTemplateMaintenance

        private RelayCommand _showValidationTemplateMaintenanceCommand;

        /// <summary>
        ///     Gets the <see cref="_showValidationTemplateMaintenanceCommand" />.
        /// </summary>
        /// <remarks>If the command is not yet initialized, it does so before returning it.</remarks>
        public RelayCommand ShowValidationTemplateMaintenanceCommand
        {
            get
            {
                if (_showValidationTemplateMaintenanceCommand != null) return _showValidationTemplateMaintenanceCommand;

                _showValidationTemplateMaintenanceCommand = new RelayCommand(
                    p => ShowValidationTemplateMaintenance_Executed(),
                    p => ShowValidationTemplateMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_ValidationTemplateMaintenance_Title,
                        FriendlyDescription = Message.Setup_ValidationTemplateMaintenance_Description,
                        Icon = ImageResources.ValidationTemplateMaintenance_32
                    };
                ViewModelCommands.Add(_showValidationTemplateMaintenanceCommand);

                return _showValidationTemplateMaintenanceCommand;
            }
        }

        private Boolean ShowValidationTemplateMaintenance_CanExecute()
        {
            return new ModelPermission<ValidationTemplate>().CanFetch;
        }

        /// <summary>
        ///     Shows the <see cref="ValidationTemplateMaintenanceOrganiser"/> screen.
        /// </summary>
        private void ShowValidationTemplateMaintenance_Executed()
        {
            ShowOrActivateWindow<ValidationTemplateMaintenanceOrganiser>();
        }

        #endregion

        #region ShowRenumberingStrategyMaintenance

        /// <summary>
        ///     Reference to the current instance for <see cref="ShowRenumberingStrategyMaintenanceCommand"/>.
        /// </summary>
        private RelayCommand _showRenumberingStrategyMaintenanceCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="ShowRenumberingStrategyMaintenanceCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand ShowRenumberingStrategyMaintenanceCommand
        {
            get
            {
                if (_showRenumberingStrategyMaintenanceCommand != null) return _showRenumberingStrategyMaintenanceCommand;

                _showRenumberingStrategyMaintenanceCommand = new RelayCommand(o => ShowRenumberingStrategyMaintenance_Executed(), o => ShowRenumberingStrategyMaintenance_CanExecute())
                {
                    FriendlyName = Message.Setup_RenumberingStrategy_Title,
                    FriendlyDescription = Message.Setup_RenumberingStrategy_Description,
                    Icon = ImageResources.RenumberingStrategyMaintenance_32
                };
                ViewModelCommands.Add(_showRenumberingStrategyMaintenanceCommand);
                return _showRenumberingStrategyMaintenanceCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="ShowRenumberingStrategyMaintenanceCommand"/>.
        /// </summary>
        private static Boolean ShowRenumberingStrategyMaintenance_CanExecute()
        {
            return new ModelPermission<RenumberingStrategy>().CanFetch;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="ShowRenumberingStrategyMaintenanceCommand"/> is executed.
        /// </summary>
        private void ShowRenumberingStrategyMaintenance_Executed()
        {
            ShowOrActivateWindow<RenumberingStrategyMaintenanceOrganiser>();
        }

        #endregion

        #region Workflow Maintenance

        private RelayCommand _showWorkflowMaintenanceCommand;

        /// <summary>
        /// Launches the location maintenace window.
        /// </summary>
        public RelayCommand ShowWorkflowMaintenanceCommand
        {
            get
            {
                if (_showWorkflowMaintenanceCommand == null)
                {
                    _showWorkflowMaintenanceCommand = new RelayCommand(
                        p => ShowWorkflowMaintenance_Executed(),
                        p => ShowWorkflowMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Setup_WorkflowMaintenance_Short,
                        FriendlyDescription = Message.Setup_WorkflowMaintenance_Description,
                        Icon = ImageResources.WorkflowMaintenance,
                        DisabledReason = Message.Setup_WorkflowMaintenance_DisabledReason
                    };
                    base.ViewModelCommands.Add(_showWorkflowMaintenanceCommand);
                }
                return _showWorkflowMaintenanceCommand;
            }
        }

        private Boolean ShowWorkflowMaintenance_CanExecute()
        {
            // NF - this permission is handled a little differently
            // than usual
            return ApplicationContext.User.IsInRole(DomainPermission.WorkflowGet.ToString());
        }

        private void ShowWorkflowMaintenance_Executed()
        {
            ShowOrActivateWindow<WorkflowMaintenanceOrganiser>();
        }

        #endregion

        #region OptionsCommand

        private RelayCommand _optionsCommand;

        /// <summary>
        /// Launches the options window
        /// </summary>
        public RelayCommand OptionsCommand
        {
            get
            {
                if (_optionsCommand == null)
                {
                    _optionsCommand = new RelayCommand(
                        p => Options_Executed())
                    {
                        FriendlyName = Message.Ribbon_Options,
                        SmallIcon = ImageResources.Ribbon_Options
                    };
                    base.ViewModelCommands.Add(_optionsCommand);
                }
                return _optionsCommand;
            }
        }

        private void Options_Executed()
        {
            new OptionsViewModel().ShowDialog();
        }

        #endregion

        #region ExitApplication

        private RelayCommand _exitApplicationCommand;

        /// <summary>
        /// Close the application
        /// </summary>
        public RelayCommand ExitApplicationCommand
        {
            get
            {
                if (_exitApplicationCommand == null)
                {
                    _exitApplicationCommand =
                        new RelayCommand(p => ExitApplication_Executed())
                        {
                            FriendlyName = Message.Ribbon_Exit,
                            SmallIcon = ImageResources.Ribbon_Exit_16
                        };
                    this.ViewModelCommands.Add(_exitApplicationCommand);
                }
                return _exitApplicationCommand;
            }
        }

        private void ExitApplication_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        private void RegisterResources()
        {
            ResourceManager.RegisterResourceFile(typeof(Message));
            ResourceManager.RegisterResourceFile(typeof(Galleria.Framework.Planograms.Resources.Language.Message));
            ResourceManager.RegisterEnum(typeof(AssortmentMinorRevisionActionType));
            ResourceManager.RegisterEnum(typeof(ContentLookupItemType));
            ResourceManager.RegisterEnum(typeof(FixtureAngleUnitOfMeasureType));
            ResourceManager.RegisterEnum(typeof(FixtureAreaUnitOfMeasureType));
            ResourceManager.RegisterEnum(typeof(FixtureComponentType));
            ResourceManager.RegisterEnum(typeof(FixtureCurrencyUnitOfMeasureType));
            ResourceManager.RegisterEnum(typeof(FixtureLengthUnitOfMeasureType));
            ResourceManager.RegisterEnum(typeof(FixtureSubComponentMerchandisingType));
            ResourceManager.RegisterEnum(typeof(FixtureSubComponentNotchStyleType));
            ResourceManager.RegisterEnum(typeof(FixtureVolumeUnitOfMeasureType));
            ResourceManager.RegisterEnum(typeof(FixtureWeightUnitOfMeasureType));
            ResourceManager.RegisterEnum(typeof(LocationSpaceBayType));
            ResourceManager.RegisterEnum(typeof(LocationSpaceFixtureType));
            ResourceManager.RegisterEnum(typeof(MetricDirectionType));
            ResourceManager.RegisterEnum(typeof(MetricElasticityDataModelType));
            ResourceManager.RegisterEnum(typeof(MetricSpecialType));
            ResourceManager.RegisterEnum(typeof(MetricType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramPositionOrientationType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramProductOrientationType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramStatusType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramPositionMerchandisingStyle));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramProductMerchandisingStyle));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramProductShapeType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramProductStatusType));
            ResourceManager.RegisterEnum(typeof(PerformanceSelectionTimeType));
            ResourceManager.RegisterEnum(typeof(PerformanceSelectionType));
            ResourceManager.RegisterEnum(typeof(SequenceType));
            ResourceManager.RegisterEnum(typeof(UnitOfMeasureLengthType));
            ResourceManager.RegisterEnum(typeof(UnitOfMeasureVolumeType));
            ResourceManager.RegisterEnum(typeof(UnitOfMeasureWeightType));
            ResourceManager.RegisterEnum(typeof(UnitOfMeasureCurrencyType));
            ResourceManager.RegisterEnum(typeof(UnitOfMeasureAngleType));
            ResourceManager.RegisterEnum(typeof(UnitOfMeasureAreaType));
            ResourceManager.RegisterEnum(typeof(Galleria.Ccm.Model.LocationSpaceBayType));
            ResourceManager.RegisterEnum(typeof(Galleria.Ccm.Model.LocationSpaceFixtureType));
            ResourceManager.RegisterEnum(typeof(Galleria.Ccm.Model.LocationSpaceFixtureShapeType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramComponentType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramType));
            ResourceManager.RegisterEnum(typeof(Galleria.Ccm.Model.WorkpackageType));
			ResourceManager.RegisterEnum(typeof(Galleria.Ccm.Model.PlanAssignmentPublishStatusType));
			ResourceManager.RegisterEnum(typeof(Galleria.Ccm.Model.PlanPublishType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramAssortmentProductTreatmentType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramAssortmentProductLocalizationType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramAssortmentProductFamilyRuleType));
            ResourceManager.RegisterEnum(typeof(Galleria.Framework.Planograms.Model.PlanogramItemComparisonStatusType));

            //Reporting.Model.DataModel.LoadDataModelFromFile(@"C:\Dev\SS-CCM_v8.0\Solution\Galleria.Ccm.Dal.Mssql\Scripts\0006.0000\DataModels\Planogram CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(@"C:\Dev\SS-CCM_v8.0\Solution\Galleria.Ccm.Dal.Mssql\Scripts\0006.0000\DataModels\Planogram Performance CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(@"C:\Dev\SS-CCM_v8.0\Solution\Galleria.Ccm.Dal.Mssql\Scripts\0006.0000\DataModels\Workpackage Summary CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(@"C:\Dev\SS-CCM_v8.0\Solution\Galleria.Ccm.Dal.Mssql\Scripts\0006.0000\DataModels\Product Distribution Overview CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(@"C:\Dev\SS-CCM_v8.0\Solution\Galleria.Ccm.Dal.Mssql\Scripts\0006.0000\DataModels\Planogram Assignment CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(@"C:\Dev\SS-CCM_v8.0\Solution\Galleria.Ccm.Dal.Mssql\Scripts\0006.0000\DataModels\Location Space CCM V8 Data Model.xml");
        }


        /// <summary>
        /// Activates the existing window of the given type or creates a new one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private void ShowOrActivateWindow<T>()
            where T : Window
        {
            if (this.AttachedControl != null)
            {
                Window existingWin = FrameworkControls.Helpers.GetWindow<T>();
                if (existingWin == null)
                {
                    //show a new window
                    App.ShowWindow(Activator.CreateInstance<T>(), false);
                }
                else
                {
                    //activate the existign window
                    if (existingWin.WindowState == WindowState.Minimized)
                    {
                        existingWin.WindowState = WindowState.Normal;
                    }
                    existingWin.Activate();
                }

            }
        }


        /// <summary>
        /// Returns true if the window was activated.
        /// </summary>
        private Boolean TryActivateWindow<T>()
            where T : Window
        {
            if (this.AttachedControl == null) return false;

            Window existingWin = FrameworkControls.Helpers.GetWindow<T>();
            if (existingWin != null)
            {
                //activate the existign window
                if (existingWin.WindowState == WindowState.Minimized)
                {
                    existingWin.WindowState = WindowState.Normal;
                }
                existingWin.Activate();
                return true;
            }

            return false;
        }


        /// <summary>
        ///     Helper method to get the property path for the <see cref="MainPageViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<MainPageViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }


        #region GFS Connection Check

        /// <summary>
        /// Method to setup an asynchronous check on the status of the GFS connection
        /// </summary>
        /// <returns></returns>
        private Boolean IsValidGFSConnectionState()
        {
            Boolean _isValidGFSConn = true;

            // pass the delegate event handler method
            _curEntityView.ValidateGFSConnectionState(GFSConnection_RunWorkerCompleted);

            return _isValidGFSConn;
        }

        /// <summary>
        /// Completed worker event handler.
        /// </summary>
        /// <param name="connectionPath"></param>
        private void GFSConnection_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            var worker = (Csla.Threading.BackgroundWorker)sender;
            Boolean localValidGFSConnection = true;

            String connectionError = null;

            // Get the result and error status values
            var errorStatus = e.Error;
            var resultStatus = e.Result as EntityViewModel.ConnectionCheckWorkArgs;

            if (errorStatus == null && resultStatus == null)
            {
                // There was no error, happy days...
                localValidGFSConnection = true;
            }
            else
            {
                localValidGFSConnection = true;

                // There was an error, so disable the ShowPerformanceSelectionMaintenanceCommand and 
                // set the disabled reason
                if (errorStatus != null)
                {
                    connectionError = errorStatus.GetBaseException().Message;
                    localValidGFSConnection = false;
                }
                else if (resultStatus != null && resultStatus.Error != null)
                {
                    connectionError = resultStatus.Error.Message;
                    localValidGFSConnection = false;
                }

                // If there was an error, set the disabled reason indicating this is the case
                ShowPerformanceSelectionMaintenanceCommand.DisabledReason = connectionError;
            }

            _curEntityView.IsValidGFSConnection = localValidGFSConnection;
        }

        #endregion

        #endregion

        #region IDisposable


        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    SystemStatusView.StopPollingAsync();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
