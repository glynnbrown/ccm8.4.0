﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 : J.Pickup
//  Label Settings fetchall() uses new VM wrapper.
// V8-27709 : L.Luong
//  Copied GFS Ui
#endregion

#endregion

using System;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// ViewModel controller for OptionsWindow
    /// </summary>
    public sealed class OptionsViewModel : ViewModelAttachedControlObject<OptionsWindow>
    {
        #region Fields

        private UserSystemSetting _settings;
        private Boolean _isDatabaseSelectionRemembered;
        private Boolean _isEntitySelectionRemembered;
        private Boolean _requiresRestart;
        private String _displayLanguage;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsDatabaseSelectionRememberedProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.IsDatabaseSelectionRemembered);
        public static readonly PropertyPath IsEntitySelectionRememberedProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.IsEntitySelectionRemembered);
        public static readonly PropertyPath DisplayLanguageProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.DisplayLanguage);
        public static readonly PropertyPath RequiresRestartProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.RequiresRestart);
        
        //Commands
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.ApplyCommand);

        #endregion

        #region Properties

        /// <summary>
        /// gets the UserSystem settings 
        /// </summary>
        public UserSystemSetting Settings
        {
            get { return _settings; }
        }

        /// <summary>
        /// returns if the user has the database been remembered or not
        /// </summary>
        public Boolean IsDatabaseSelectionRemembered
        {
            get { return _isDatabaseSelectionRemembered; }
            set
            {
                _isDatabaseSelectionRemembered = value;

                OnPropertyChanged(IsDatabaseSelectionRememberedProperty);
            }
        }

        /// <summary>
        /// returns if the user has the entity been remembered or not
        /// </summary>
        public Boolean IsEntitySelectionRemembered
        {
            get { return _isEntitySelectionRemembered; }
            set
            {
                _isEntitySelectionRemembered = value;

                OnPropertyChanged(IsEntitySelectionRememberedProperty);
            }
        }

        /// <summary>
        /// returns the Display Language the user has set
        /// </summary>
        public String DisplayLanguage
        {
            get { return _displayLanguage; }
            set
            {
                _displayLanguage = value;

                OnPropertyChanged(DisplayLanguageProperty);
            }
        }
       
        /// <summary>
        /// returns if the user saves a setting that requires a restart
        /// </summary>
        public Boolean RequiresRestart
        {
            get { return _requiresRestart; }
            private set
            {
                _requiresRestart = value;

                OnPropertyChanged(RequiresRestartProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="settingsView"></param>
        public OptionsViewModel()
        {
            // Get copy of the system settings
            _settings = App.GetSystemSettings().Copy();

            _settings.PropertyChanged += Settings_PropertyChanged;

            _isDatabaseSelectionRemembered = _settings.IsDatabaseSelectionRemembered;
            _isEntitySelectionRemembered = _settings.IsEntitySelectionRemembered;

            _displayLanguage = _settings.DisplayLanguage;
        }

        #endregion

        #region Commands

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Closes the attached window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies the current changes and closes the window
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand
                        (p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok

                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //If settings list or settings state hasn't changed
            if (!HaveSettingsChanged())
            {
                ApplyCommand.DisabledReason = Message.Generic_Ok_DisabledReasonNoChanges;
                return false;
            }
            return true;
        }

        private void Apply_Executed()
        {
            base.ShowWaitCursor(true);

            this.Settings.IsDatabaseSelectionRemembered = IsDatabaseSelectionRemembered;
            this.Settings.IsEntitySelectionRemembered = IsEntitySelectionRemembered;

            //Save
            this.Settings.Save();

            base.ShowWaitCursor(false);

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }

        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Event to handle properties on the settings state changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Settings_PropertyChanged(object sender, EventArgs e)
        {
            //Update require restart property
            this.RequiresRestart = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves down changes to the model
        /// </summary>
        private void Save()
        {
            //TODO

            //base.ShowWaitCursor(true);

            ////set the field list properties
            //this.Settings.PositionProperties = GetFieldString(this.AssignedPositionProperties);
            //this.Settings.ComponentProperties = GetFieldString(this.AssignedComponentProperties);

            //try
            //{
            //    //save changes
            //    _settingsView.Save();
            //}
            //catch (DataPortalException ex)
            //{
            //    base.ShowWaitCursor(false);

            //    Exception baseException = ex.GetBaseException();

            //    LocalHelper.RecordException(baseException);

            //    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(null, OperationType.Save);
            //}

            //base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Checks whether the settings has changed
        /// </summary>
        private Boolean HaveSettingsChanged()
        {
            Boolean returnValue = false;

            if (this.Settings.IsDatabaseSelectionRemembered != this.IsDatabaseSelectionRemembered)
            {
                returnValue = true;
            }

            if (this.Settings.IsEntitySelectionRemembered != this.IsEntitySelectionRemembered)
            {
                returnValue = true;
            }

            if (this.Settings.DisplayLanguage != this.DisplayLanguage)
            {
                returnValue = true;
            }
            return returnValue;
        }  

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _settings.PropertyChanged -= Settings_PropertyChanged;

                    base.IsDisposed = true;
                }
            }
        }

        #endregion

    }
}
