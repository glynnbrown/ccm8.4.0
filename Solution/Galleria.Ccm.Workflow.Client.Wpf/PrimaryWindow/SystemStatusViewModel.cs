﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27411 : M.Pettit
//  Created
#endregion
#region Version History: (CCM 830)
// V8-32294 : M.Pettit
//  Sync dates are now displayed in local date time not UTC
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.ViewModel;
using System.Threading;
using Galleria.Framework.Helpers;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    public sealed class SystemStatusViewModel : INotifyPropertyChanged
    {
        #region Constants
        private const Double _syncFailureMaxHoursToDisplay = 24D;
        #endregion

        #region Fields
        private EngineStatusInfo _engineStatus;

        private ImageSource _processorStatusIcon = null;
        private String _processorStatusToolTip = String.Empty;

        private String _queueStatusDescription = String.Empty;
        private String _queueStatusToolTip = String.Empty;

        private Visibility _gfsSyncStatusVisibility = Visibility.Visible; 
        private ImageSource _gfsSyncStatusIcon = null;
        private String _gfsSyncStatusToolTip = String.Empty;

        private Boolean _isPolling;
        #endregion

        #region Binding properties
        public static readonly PropertyPath IsPollingProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.IsPolling);
        public static readonly PropertyPath EngineStatusInfoProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.EngineStatusInfo);
        public static readonly PropertyPath ProcessorStatusIconProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.ProcessorStatusIcon);
        public static readonly PropertyPath ProcessorStatusToolTipProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.ProcessorStatusToolTip);
        public static readonly PropertyPath QueueStatusDescriptionProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.QueueStatusDescription);
        public static readonly PropertyPath QueueStatusToolTipProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.QueueStatusToolTip);
        public static readonly PropertyPath GfsSyncStatusIconProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.GfsSyncStatusIcon);
        public static readonly PropertyPath GfsSyncStatusToolTipProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.GfsSyncStatusToolTip);
        public static readonly PropertyPath GfsSyncStatusVisibilityProperty = WpfHelper.GetPropertyPath<SystemStatusViewModel>(o => o.GfsSyncStatusVisibility);
        #endregion

        #region Properties
        public EngineStatusInfo EngineStatusInfo
        {
            get { return _engineStatus; }
            private set
            {
                _engineStatus = value;
                OnPropertyChanged(EngineStatusInfoProperty);
            }
        }

        public ImageSource ProcessorStatusIcon
        {
            get { return _processorStatusIcon; }
            private set
            {
                _processorStatusIcon = value;
                OnPropertyChanged(ProcessorStatusIconProperty);
            }
        }

        public String ProcessorStatusToolTip
        {
            get { return _processorStatusToolTip; }
            private set
            {
                _processorStatusToolTip = value;
                OnPropertyChanged(ProcessorStatusToolTipProperty);
            }
        }

        public String QueueStatusDescription
        {
            get { return _queueStatusDescription; }
            private set
            {
                _queueStatusDescription = value;
                OnPropertyChanged(QueueStatusDescriptionProperty);
            }
        }

        public String QueueStatusToolTip
        {
            get { return _queueStatusToolTip; }
            private set
            {
                _queueStatusToolTip = value;
                OnPropertyChanged(QueueStatusToolTipProperty);
            }
        }

        public ImageSource GfsSyncStatusIcon
        {
            get { return _gfsSyncStatusIcon; }
            private set
            {
                _gfsSyncStatusIcon = value;
                OnPropertyChanged(GfsSyncStatusIconProperty);
            }
        }

        public String GfsSyncStatusToolTip
        {
            get { return _gfsSyncStatusToolTip; }
            private set
            {
                _gfsSyncStatusToolTip = value;
                OnPropertyChanged(GfsSyncStatusToolTipProperty);
            }
        }

        public Visibility GfsSyncStatusVisibility
        {
            get { return _gfsSyncStatusVisibility; }
            private set
            {
                _gfsSyncStatusVisibility = value;
                OnPropertyChanged(GfsSyncStatusVisibilityProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether data is updating
        /// </summary>
        public Boolean IsPolling
        {
            get { return _isPolling; }
            private set 
            { 
                _isPolling = value;
                OnPropertyChanged(IsPollingProperty);
            }
        }

        #endregion

        #region Constructors
        public SystemStatusViewModel() { }
        #endregion


        #region Methods

        private void UpdateEngineInformation()
        {
            try
            {
                this.EngineStatusInfo = EngineStatusInfo.FetchEngineStatusInfo();
            }
            catch (Csla.DataPortalException dataExc)
            {
                DisplayWarning();
                return;
            }

            SetProcessorStatusProperties(_engineStatus, out _processorStatusIcon, out _processorStatusToolTip);
            OnPropertyChanged(ProcessorStatusIconProperty);
            OnPropertyChanged(ProcessorStatusToolTipProperty);

            SetQueueStatusProperties(_engineStatus, out _queueStatusDescription, out _queueStatusToolTip);
            OnPropertyChanged(QueueStatusDescriptionProperty);
            OnPropertyChanged(QueueStatusToolTipProperty);

            SetGfsSyncStatusProperties(_engineStatus, out _gfsSyncStatusIcon, out _gfsSyncStatusToolTip, out _gfsSyncStatusVisibility);
            OnPropertyChanged(GfsSyncStatusIconProperty);
            OnPropertyChanged(GfsSyncStatusToolTipProperty);
            OnPropertyChanged(GfsSyncStatusVisibilityProperty);
        }

        #endregion

        #region Static Methods

        private static void SetProcessorStatusProperties(EngineStatusInfo engineStatus, out ImageSource icon, out String toolTip)
        {
            if (engineStatus != null)
            {
                switch (engineStatus.EngineInstanceCount)
                {
                    case 0:
                        //There are no processors online
                        //msg = All processors are offline. Please contact support. No planogram workflows will be processed but other jobs may continue.
                        icon = ImageResources.EngineStatusError_16;
                        toolTip = Message.EngineStatusUI_ProcessorStatus_ProcessorToolTip_Offline;
                        return;

                    default:
                        Int32 unhealthyInstances = (engineStatus.EngineInstanceCount - engineStatus.HealthyEngineInstanceCount);
                        if (unhealthyInstances == 0)
                        {
                            //All processors are online
                            //msg = {0} Processor(s) are online
                            icon = ImageResources.EngineStatusOk_16;
                            String msg = Message.EngineStatusUI_ProcessorStatus_ProcessorToolTip_Online;
                            if (engineStatus.EngineInstanceCount == 1)
                            {
                                //single processor-specific message
                                msg = Message.EngineStatusUI_ProcessorStatus_ProcessorToolTip_OneOnline;
                            }
                            toolTip = String.Format(msg, engineStatus.EngineInstanceCount.ToString());
                        }
                        else
                        {
                            //One of more online processors are not reporting a heartbeat
                            //msg = {0} processor(s) are online but {1} have not reported a heartbeat for the last {2} minutes. 
                            //      Unless this corrects itself please contact support.
                            String msg = Message.EngineStatusUI_ProcessorStatus_ProcessorToolTip_HeartbeatOutOfDate;
                            icon = ImageResources.EngineStatusInformation_16;
                            toolTip = String.Format(msg,
                                engineStatus.EngineInstanceCount.ToString(),
                                unhealthyInstances.ToString(),
                                EngineInstanceInfo.MaxHeartbeatInterval.ToString());
                        }
                        return;
                }
            }
            icon = null;
            toolTip = String.Empty;
        }

        private static void SetQueueStatusProperties(EngineStatusInfo engineStatus, out String queueStatusDescription, out String toolTip)
        {
            if (engineStatus != null)
            {
                if (engineStatus.EngineInstanceCount > 0)
                {
                    if (engineStatus.MessageCount == 0)
                    {
                        //No messages,so mark as idle
                        queueStatusDescription = Message.EngineStatusUI_QueueStatus_Idle;
                        toolTip = Message.EngineStatusUI_QueueStatusToolTip_Idle;
                        return;
                    }
                    else if (engineStatus.MessageCount > 0)
                    {
                        //message exist, so status = "Working"

                        queueStatusDescription = Message.EngineStatusUI_QueueStatus_Working;
                        //msg = There are {0} jobs in the queue.
                        String msg = Message.EngineStatusUI_QueueStatusToolTip_Working;
                        toolTip = String.Format(msg, engineStatus.MessageCount.ToString());
                        return;
                    }
                }
                else
                {
                    //There are no processors online
                    queueStatusDescription = Message.EngineStatusUI_QueueStatus_Offline;
                    toolTip = Message.EngineStatusUI_QueueStatusToolTip_Offline;
                    return;
                }
            }
            queueStatusDescription = String.Empty;
            toolTip = String.Empty;
        }

        private static void SetGfsSyncStatusProperties(EngineStatusInfo engineStatus, out ImageSource icon, out String toolTip, out Visibility statusVisibility)
        {
            statusVisibility = Visibility.Collapsed;
            icon = null;
            toolTip = String.Empty;

            Entity currentEntity = null;
            try
            {
                currentEntity = Entity.FetchById(App.ViewState.EntityId);
            }
            catch (Csla.DataPortalException exc)
            {
                DisplayWarning();
                return;
            }

            // If no entity or entity has not been linked to a GFS database, hide the GFS Sync Status notification area
            if (currentEntity == null) { return; }
            if (currentEntity != null && currentEntity.GFSId == null) { return; }

            //Get the last sync dates as a snapshot in case they are updated soon ...
            DateTime? lastSuccessSync = currentEntity.SystemSettings.LastSuccessfulSyncDate;
            DateTime? lastFailedSync = currentEntity.SystemSettings.LastFailedSyncDate;

            //Case 1: Sync has never been attempted, hide the GFS Sync Status notification area
            if ((lastSuccessSync == null) && (lastFailedSync == null))  { return; }

            //Case 2: Sync has run and only ever failed
            if (lastSuccessSync == null && lastFailedSync != null)
            {
                statusVisibility = Visibility.Visible;
                icon = ImageResources.EngineStatusError_16;
                //"The data synchronisation from GFS has never completed successfully.\r\nThe last failed attempt occurred on {0}";
                String msg = Message.EngineStatusUI_GFSSyncStatusToolTip_AlwaysFailed;
                toolTip = String.Format(msg, ((DateTime)lastFailedSync).ToLocalTime().ToString("F"));
                return;
            }

            //Case 3: Sync has run and only ever passed
            if (lastSuccessSync != null && lastFailedSync == null)
            {
                statusVisibility = Visibility.Visible;
                icon = ImageResources.EngineStatusOk_16;
                String msg = Message.EngineStatusUI_GFSSyncStatusToolTip_Connected;
                toolTip = String.Format(msg, ((DateTime)lastSuccessSync).ToLocalTime().ToString("F"));
                return;
            }

            //Case 4 : We have both success and fail syncs recorded
            if (lastSuccessSync != null && lastFailedSync != null)
            {
                //Check which is the most recent
                if (((DateTime)lastSuccessSync).CompareTo(((DateTime)lastFailedSync)) < 0)
                {
                    //most recent sync was a failure
                    statusVisibility = Visibility.Visible;
                    icon = ImageResources.EngineStatusError_16;
                    //"The most recent data synchronisation from GFS on {0} encountered errors which should be investigated\r\nPlease see event logs."
                    String msg = Message.EngineStatusUI_GFSSyncStatusToolTip_MostRecentFailed;
                    toolTip = String.Format(msg, ((DateTime)lastFailedSync).ToLocalTime().ToString("F"));
                    return;
                }
                else
                {
                    //most recent sync was a success
                    TimeSpan interval = (DateTime.UtcNow).Subtract(((DateTime)lastFailedSync));
                    if (interval.TotalHours < _syncFailureMaxHoursToDisplay)
                    {
                        //display informational icon as last error occurred within the last 24 hours
                        statusVisibility = Visibility.Visible;
                        icon = ImageResources.EngineStatusInformation_16;
                        //The data synchronisation from GFS was last run on {0} and was successful.
                        //An earlier synchronisation on {1} encountered errors which should be investigated.
                        //Please see event logs." 
                        String msg = Message.EngineStatusUI_GFSSyncStatusToolTip_SyncErrorBeforeSuccess;
                        toolTip = String.Format(msg, ((DateTime)lastSuccessSync).ToLocalTime().ToString("F"), ((DateTime)lastFailedSync).ToLocalTime().ToString("F"));
                        return;
                    }
                    else
                    {
                        //Last failure was more than 24 hours ago and a success has occurred since, so ignore it
                        statusVisibility = Visibility.Visible;
                        icon = ImageResources.EngineStatusOk_16;
                        String msg = Message.EngineStatusUI_GFSSyncStatusToolTip_Connected;
                        toolTip = String.Format(msg, ((DateTime)lastSuccessSync).ToLocalTime().ToString("F"));
                        return;
                    }
                }
            }
        }

        private static void DisplayWarning()
        {
            CommonHelper.GetWindowService().ShowErrorMessage(Message.Application_Database_ConnectionLostHeader, Message.Application_Database_ConnectionLostDescription);
        }

        #endregion

        #region Polling

        internal void StartPolling()
        {
            if (!IsPolling)
            {
                IsPolling = true;

                this.PropertyChanged += OnPropertyChangedWhenPolling;
                UpdateEngineInformation();
            }
        }

        /// <summary>
        /// Called when a view property changes and we are polling.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPropertyChangedWhenPolling(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == EngineStatusInfoProperty.Path)
            {
                if (IsPolling)
                {
                    BeingPollingWait();
                }
                else
                {
                    this.PropertyChanged -= OnPropertyChangedWhenPolling;
                }
            }
        }

        private void BeingPollingWait()
        {
            Csla.Threading.BackgroundWorker worker = new Csla.Threading.BackgroundWorker();
            worker.DoWork += PollingWorker_DoWork;
            worker.RunWorkerCompleted += PollingWorker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        private void PollingWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Thread.Sleep(Galleria.Ccm.Common.Wpf.Constants.PollingPeriod);
        }

        private void PollingWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker worker = (Csla.Threading.BackgroundWorker)sender;
            worker.DoWork += PollingWorker_DoWork;
            worker.RunWorkerCompleted += PollingWorker_RunWorkerCompleted;

            if (IsPolling)
            {
                UpdateEngineInformation();
            }
            else
            {
                this.PropertyChanged -= OnPropertyChangedWhenPolling;
            }
        }

        /// <summary>
        /// Stops any ongoing polling operation 
        /// asynchronously.
        /// </summary>
        public void StopPollingAsync()
        {
            _isPolling = false;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
